//
//  AppControl.h
//  TCiPad
//
//  Created by n2n on 26/10/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#ifndef AppControl_h
#define AppControl_h

// for AFE broker screenshot
#define DEBUG_MODE 0
#define ISDUMPDATAFORNEWFUNCS NO
#define BUNDLEID_HARDCODE_TESTING @"com.n2nconnect.tcipaduat"//@"com.n2n.testingproduct.tc30"//@"com.n2nconnect.tcipaduat"

#define BUNDLEID_MULTILOGIN_UAT @"com.n2nconnect.tcipaduat"
#define BUNDLEID_INTERPAC_UAT @"com.n2n.ipad.ipaconlineuat"
#define BUNDLEID_CIMBMY_UAT @"com.n2n.ipad.itradecimbuat"
#define BUNDLEID_CIMBSG_UAT @"com.n2n.ipad.itradecimbsguat"
#define BUNDLEID_AMSEC_UAT @"com.n2n.ipad.amsecuat"
#define BUNDLEID_AMFUTURES_UAT @"com.n2n.ipad.amfuturesuat"
#define BUNDLEID_AMACCESS @"com.n2n.ipad.ammultiloginuat"

#define BUNDLEID_ABACUS_UAT @"com.abacus.mytradetabuat"
#define BUNDLEID_ABACUS @"com.abacus.mytradetab"


#define BUNDLEID_MULTILOGIN @"com.n2nconnect.tcipad"
#define BUNDLEID_INTERPAC @"com.n2n.ipad.ipaconline"
#define BUNDLEID_CIMBMY @"com.n2n.ipad.itradecimb"
#define BUNDLEID_CIMBSG @"com.n2n.ipad.itradecimbsg"
#define BUNDLEID_CIMBSGSTAG @"com.n2n.ipad.itradecimbsgstag"

typedef enum {
    QUOTESCREEN,
    WATCHLIST_HIDDENLIST,
    WATCHLIST_NONHIDDENLIST
} QuoteScreenMode;

#define kTimeOutDuration        1800          //Maximum idle time // 30 minutes = 1800

#define SET_IF_NOT_NULL(TARGET, VAL) if([VAL length]>0) { TARGET = VAL; } else { TARGET = @"0"; }

#define SYMBOL_TRIANGLE_DOWN @"▼"
#define SYMBOL_TRIANGLE_UP @"▲"

#define K_DAY 1
#define K_WEEK 5
#define K_1MONTH 20
#define K_3MONTH 60
#define K_6MONTH 120
#define K_1YEAR 240
#define K_2YEAR 480

#define kDataLoadCannot 1
#define kDataLoadNext 2
#define kDataLoadPrev 3
#define kPaginationTrigger 80.0 // distance of scroll to activate load next/prev page

#define kButtonRadius 0.15

// ================ PANEL BACKGROUND COLOR ================
#define kPanelColor [UIColor colorWithRed:0.13 green:0.13 blue:0.13 alpha:1.00]

// ================ TABLE CELL ALTERNATE GRAYS ================
#define kCellGray1 [UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.00]
#define kCellGray2 [UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1.00]
#define kCellGray3 [UIColor colorWithRed:0.18 green:0.18 blue:0.18 alpha:1.00]

#define kCellLightGray1 [UIColor colorWithRed:0.90 green:0.90 blue:0.90 alpha:1.00]
#define kCellLightGray2 [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.00]

// ================ FIELDFLASH COLORS ================
#define kRedRFlash 0.72
#define kRedGFlash 0.00
#define kRedBFlash 0.00
#define kRedFlash   [UIColor colorWithRed:kRedRFlash green:kRedGFlash blue:kRedBFlash alpha:1.0]

#define kGrnRFlash 0.00
#define kGrnGFlash 0.72
#define kGrnBFlash 0.00
#define kGrnFlash [UIColor colorWithRed:kGrnRFlash green:kGrnGFlash blue:kGrnBFlash alpha:1.0]

// ================ SCOREBOARD COLORS ================

// [UIColor colorWithRed:0.34 green:0.01 blue:0.03 alpha:1.00]
//[UIColor colorWithRed:0.82 green:0.01 blue:0.11 alpha:1.00]

#define kRedRed 0.54
#define kRedGrn 0.01
#define kRedBlu 0.03
#define kRedColor   [UIColor colorWithRed:kRedRed green:kRedGrn blue:kRedBlu alpha:1.0]
#define kRedColorPie [CPTColor colorWithComponentRed:kRedRed green:kRedGrn blue:kRedBlu alpha:1.0]

// [UIColor colorWithRed:0.07 green:0.16 blue:0.07 alpha:1.00]
// [UIColor colorWithRed:0.25 green:0.46 blue:0.02 alpha:1.00]

#define kGrnRed 0.25
#define kGrnGrn 0.46
#define kGrnBlu 0.02
#define kGreenColor [UIColor colorWithRed:kGrnRed green:kGrnGrn blue:kGrnBlu alpha:1.0]
#define kGreenColorPie [CPTColor colorWithComponentRed:kGrnRed green:kGrnGrn blue:kGrnBlu alpha:1.0]

#define kGrayLevel 0.45
#define kGrayColor [UIColor colorWithRed:kGrayLevel green:kGrayLevel blue:kGrayLevel alpha:1.0]
#define kYellowColor [UIColor yellowColor]
#define kGrayColorPie [CPTColor colorWithComponentRed:kGrayLevel green:kGrayLevel blue:kGrayLevel alpha:1.0]

#define kWhiteLevel 0.6
#define kWhiteColor [UIColor colorWithRed:kWhiteLevel green:kWhiteLevel blue:kWhiteLevel alpha:1.0]
#define kWhiteColorPie [CPTColor colorWithComponentRed:kWhiteLevel green:kWhiteLevel blue:kWhiteLevel alpha:1.0]

// ================ FLAG COLORS ================

#define kRedRedF 0.34
#define kRedGrnF 0.01
#define kRedBluF 0.03
#define kRedColorF   [UIColor colorWithRed:kRedRedF green:kRedGrnF blue:kRedBluF alpha:1.0]

#define kGrnRedF 0.07
#define kGrnGrnF 0.16
#define kGrnBluF 0.07
#define kGreenColorF [UIColor colorWithRed:kGrnRedF green:kGrnGrnF blue:kGrnBluF alpha:1.0]

#define kGrayLevelF 0.25
#define kGrayColorF [UIColor colorWithRed:kGrayLevelF green:kGrayLevelF blue:kGrayLevelF alpha:1.0]


////================== Main Mobile Color ============================
//#define kNavigationBarTintColor								RGB(24, 24, 24)
//#define AppColor_Nav_Background_Color						RGB(12, 60, 76)
//
//#define AppColor_MainAppTintColor                       RGB(12, 60, 76)
//#define AppColor_LightGray_Font_Color					RGB(170, 170, 170)
//#define AppColor_TabbarMainAppTintColor                 RGB(255, 147, 52)
//#define AppColor_MainAppBackgroundColor                 RGB(217, 181, 147)

//=================== Nested Collection View =========
//For CollectionView
#define kMINIMUM_LINE_SPACING		5
#define kMINIMUM_INTERITEM_SPACING	0

//For TypeCell
#define kMARGIN_CONTENT_LEFT_RIGHT			16
#define kMARGIN_CONTENT_TOP_BOTTOM			8

#define kSPACING_ITEM			4

//================== STORYBOARD NAME ==================
#define kMainStoryboardName				@"Main"
#define kSettingStoryboardName			@"Setting"
#define kCommonMobileStoryboardName		@"CommonMobile"
#define kMainMobileStoryboardName		@"MainMobile"
#define kMPortfolioStoryboardName		@"MPortfolio"
#define kMWatchlistStoryboardName		@"MWatchlist"
#define kMQuoteStoryboardName			@"MQuote"
#define kMIndicesStoryboardName         @"MIndices"
#define kMMarketStoryboardName          @"MMarket"
#define kTradeStoryboard                @"TradeStoryboard"
#define kStockPreviewStoryboard         @"StockPreview"
#define kStockInfoStoryboardName		@"StockInfo"
#define kMDashBoardStoryboardName       @"MDashBoard"
#define kSectorStoryboardName	        @"MSector"
#define kMSearchStoryboardName	        @"MSearch"
#define kAssetStoryboardName            @"AssetManagement"

#pragma mark - ENUM TYPE HERE
//Code Response
typedef NS_ENUM (NSInteger, ResponseCode) {
	RESP_CODE_SUCCESS = 0,
	RESP_CODE_SUCCESS_NORMAL = 200,
	RESP_CODE_UNKNOWN = -1,
	RESP_CODE_EXCEPTION = 2000,
	RESP_CODE_EXCEPTION_NO_INTERNET = 50,
	RESP_CODE_EXCEPTION_ERROR_SERVER = 61,
	RESP_CODE_EXCEPTION_UNKNOWN_SESSION = 70,
	RESP_CODE_EXCEPTION_EXPIRE_SESSION  = 71,
	RESP_CODE_REQUEST_INPROCESS  = 201,
	RESP_CODE_REQUEST_UNAUTHENTICATED = 401,
	RESP_CODE_REQUEST_EXISTED_STOCKE  = 422,
	RESP_CODE_USER_NOT_FOUND = 1000,
	RESP_CODE_USER_NOT_ACTIVE	= 1003,
	RESP_CODE_USER_ALREADY_ACTIVE = 1004,
	RESP_CODE_CANCEL_LOGIN_FACEBOOK = 20001,
	RESP_CODE_EXCEPTION_ERROR_SOCKET = 20002,
	RESP_CODE_EXCEPTION_PARSE_DATA 	= 30001,
    RESP_CODE_EXCEPTION_CHANGE_PASSWORDPIN_REQUIRED = 40001,
    RESP_CODE_EXCEPTION_CHANGE_HINTANSWER_REQUIRED = 40002
};

//Code Change Password or Pin
typedef NS_ENUM(NSInteger, ChangePasswordPinCode) {
    CHANGE_PASSWORD_CODE = 1,
    CHANGE_PIN_CODE = 2
};

//Code TouchID or FaceID Available
typedef NS_ENUM(NSInteger, TouchIDOrFaceIDAvailable) {
    NO_TOUCHID_FACEID_AVAILABLE = 0,
    TOUCHID_AVAILABLE = 1,
    FACEID_AVAILABLE = 2,
    TOUCHID_FACEID_AVAILABLE = 3
};

//=================== LEFT COMMON MENU TYPE =============
//BarItemType
typedef enum : NSInteger{
	BarItemType_Left,
	BarItemType_Right
}BarItemType;

typedef NS_ENUM (NSInteger, TabbarItemView) {
	TabbarItemView_Unknow		= -1,
	TabbarItemView_Home			= 0,
	TabbarItemView_Quote 		= 1,
	TabbarItemView_OrderBook 	= 2,
	TabbarItemView_Portfolio 	= 3,
	//TabbarItemView_Trade		= 2,
	//TabbarItemView_Watchlist 	= 3,
	//TabbarItemView_Indices	    = 4
};
//Menu
typedef enum : NSInteger{
	TCMenuType_Common,
	TCMenuType_Main
}TCMenuType;

typedef enum : NSInteger{
	MenuContentType_Login = 0,
	MenuContentType_DashBoard,
	MenuContentType_ForgotPassword,
	MenuContentType_Announcement,
	MenuContentType_TermAndCondition,
	MenuContentType_AboutUs,
	MenuContentType_Setting,
	MenuContentType_Tutorial,
    MenuContentType_OrderBook,
    MenuContentType_StockTracker,
    MenuContentType_Portfolio,
    MenuContentType_Alert,
    MenuContentType_Barcode,
	MenuContentType_LogOut
}MenuContentType;

typedef enum: NSInteger {
	Searching = 0,
	Dashboard = 1001,
	QuoteScreen = 1002,
	Watchlist = 1003,
	News = 1004,
	MarketSummary = 1005,
	Indices = 1006,
	OrderBook = 1007,
	Portfolio = 1008,
	StockNote = 1009,
	StockAlert = 1010,
	MarketStreamer = 1011,
    Report=1012,
    AccountInformation=1013,
    AssetManagement=1014,
    HomeScreen=1015,
	Ideas = 2001,
	IScreener = 2002,
	IBillionaire = 2003,
	QRScanner = 2004,
	Settings = 3001,
	Help = 3002,
	Logout = 3003
}MenuItemType;

//=================== LEFT MOBILE MAIN MENU TYPE =============
typedef enum : NSInteger{
	MenuType_Home = 0,
	MenuType_Quote,
	MenuType_WatchList,
	MenuType_News,
	MenuType_Indices,
	MenuType_Market,
	MenuType_Announcement,
	MenuType_Setting,
	MenuType_Logout
}MenuType;



//Setting
typedef enum : NSInteger{
	SettingType_Unknown = -1,
	SettingType_ChangePassword,
	SettingType_ChangePin,
	SettingType_ForgotPin,
	SettingType_ActiveTouchID,
	SettingType_PushNotification,
	SettingType_RefreshRate,
	
	SettingType_ViewPreference,
	
	SettingType_AboutUs,
	SettingType_ContactUs,
	SettingType_RateUs,
	SettingType_TermOfService,
	SettingType_PrivacyPolicy,
	SettingType_DisclaimerOfWarranty,
	SettingType_Notification
}SettingType;

typedef enum : NSInteger{
	DefaultPage_Dashboard = 0,
	DefaultPage_Quotes = 1,
	DefaultPage_Markets = 2, //same Indices
	DefaultPage_Watchlists = 3,
	DefaultPage_Portfolios = 4,
    DefaultPage_News = 5
}DefaultPage;

typedef enum : NSInteger{
	LanguageType_EN = 0,	//English
	LanguageType_CN,		//Chinese - simply
    LanguageType_CN_EXTEND, //Chinese - extend
    LanguageType_VN         //VietNam
}LanguageType;

//=============== State Login ============
typedef enum LoginStages {
	LOGIN_INIT, // before PK
	LOGIN_PK, // before Key2
	LOGIN_KEY2, // before session2
	LOGIN_SESSION2, // before e2ee or login
	LOGIN_E2EE, 	// before e2ee
	LOGIN_LOGIN, //  before login
	LOGIN_GET_EXCHANGE,
	LOGIN_GET_CLIENT_ACCOUNT,
	LOGIN_SUCCESS
} LoginStages;


//=================== Chart =================================
typedef enum: NSInteger {
	ChartType_Day = 51,
	ChartType_Week,
	ChartType_1Month,
	ChartType_3Month,
	ChartType_6Month,
	ChartType_1Year,
	ChartType_2Year
} ChartType;


typedef enum: NSInteger {
	NewsType_Unknown,
	NewsType_AllStock,
	NewsType_Archive,
	NewsType_Elastic,
	NewsType_Jar,
	NewsType_StockCompany
} NewsType;

//PopUp Stock More Info
typedef enum: NSInteger {
	StockMoreInfoType_Fundamentals = 0,
	StockMoreInfoType_News,
	StockMoreInfoType_Chat,
	StockMoreInfoType_TimeSale,
	StockMoreInfoType_Close
} StockMoreInfoType;

//PopUp Stock More Info
typedef enum: NSInteger {
	MoreItemType_AddNew = 0,
	MoreItemType_Rename,
	MoreItemType_Delete,
	MoreItemType_Close
} MoreItemType;

//SCREEN TYPE
typedef enum: NSInteger {
	ScreenType_Normal = 0,
	ScreenType_Rename,
	ScreenType_Delete
} ScreenType;

//SCREEN TYPE
typedef enum: NSInteger {
	MContentType_Single = 0,
	MContentType_Double,
	MContentType_Three,
	MContentType_Heatmap
} MContentType;

//SCREEN TYPE
typedef enum: NSInteger {
	MContextMenu_Buy = 0,
	MContextMenu_Sell,
	MContextMenu_Info,
	MContextMenu_Favorite,
	MContextMenu_Trading
} MContextMenu;

//===== Home Section ==========
//=================== Header Type =================================
typedef enum: NSInteger {
	HomeSectionType_Unknown = -1,
	HomeSectionType_Market,
	HomeSectionType_Indices,
	HomeSectionType_Portfolios,
	HomeSectionType_Research,
	HomeSectionType_Alert,
	HomeSectionType_Advertisement
} HomeSectionType;

typedef enum: NSInteger {
	StockCellType_One = 0,
	StockCellType_Two,
	StockCellType_Three
} StockCellType;

//================= Height Cell =============================
#define kHeightNewsCell				67
//PAGING
#define kNUMBER_ITEMS_PER_PAGE		30

//========================= NOTIFICATION KEY =======================
#define kNotification_DidSelectBroker			@"kNotification_DidSelectBroker"
#define kDidFinishLoadDataNotification			@"kDidFinishLoadDataNotification"
#define kDidSelectNewExchangeNotification 		@"kDidSelectNewExchangeNotification"
#define kSwitchValueChangedNotification 		@"kSwitchValueChangedNotification"
#define kDidChangeLanguageNotification    @"kDidChangeLanguageNotification"

#define kDidUpdateSettingFloatTradingButtonNotification  @"kDidUpdateSettingFloatTradingButtonNotification"

//================= Format Date ======================================
#define kSERVER_FORMAT_DATE			@"yyyy-MM-dd HH:mm:ss.S"
#define kSERVER_NEWS_FORMAT_DATE	@"dd MMM yyyy HH:mm"
#define kSERVER_Research_FORMAT_DATE    @"yyyy-MM-dd HH:mm:ss"
#define kUPDATE_FORMAT_TIME			@"HH:mm:ss"
#define kSERVER_FORMAT_TIME			@"HHmmss"
#define kSERVER_FORMAT_ORDERBOOK_DATE            @"yyyyMMddHHmmss.S"
#define kCALENDARPICKER_FORMAT_DATE    @"EEE, MMM yy"

#define kEncryptKey                 @"4ba2dca2cd70234103611b7f7858cb15"
#define kDomainName                 @"https://sans-uat.asiaebroker.com/"
#define kProjectName                @"gcSANS"
#define kNewClientPath              @"/srvs/newClient"
#define kGetAlertListPath           @"/srvs/alrtLs"
#define kAddAlertPath               @"/srvs/addAlrt"
#define kUpdateAlertListPath        @"/srvs/updAlrt"
#define kSettingAlertListPath        @"/srvs/setLs"
#define kUpdateSettingAlertPath        @"/srvs/updateSetting"

//HARD_CODE SOCKET
#define kDumpmySocketURL        @"wss://nogfv.itradecimb.com.my/eventbus/websocket"
#define IS_DumpmySocketSV   NO
#endif /* AppControl_h */


#define kNotiPortFolio   @"openPortFolio"
#define kNotiOrderBook   @"openOrderBook"
#define kNotiQuote       @"openQuote"

