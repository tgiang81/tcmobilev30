//
//  NSObject_AppConstants.h
//  TCMobileVertx
//
//  Created by Scott Thoo on 5/23/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

//define for brokers
#define kDefaultBrokerLogo @"AppLogoTCPRO"
#define kDefaultBrokerLogoName @"AppLogoTCPROName"
#define kDefaultTextColor @"283674"
#define kDefaultPlaceholderColor @"939AB9"
#define kDefaultBgColor @"FFFFFF"

#define kMNABrokerLogo @"AppLogoMNA"
#define kMNABrokerLogoName @"AppLogoMNAName"
#define kMNATextColor @"E02831"
#define kMNAPlaceholderColor @"F5BABC"
#define kMNABgColor @"FFFFFF"

#define kSSIBrokerLogo @"AppLogoSSI"
#define kSSIBrokerLogoName @"AppLogoSSIName"
#define kSSITextColor @"E02831"
#define kSSIPlaceholderColor @"F5BABC"
#define kSSIBgColor @"FFFFFF"

#define kABACUSBrokerLogo @"AppLogoABACUS"
#define kABACUSBrokerLogoName @"AppLogoABACUSName"
#define kABACUSTextColor @"00ff00"
#define kABACUSPlaceholderColor @"8AE08B"
#define kABACUSBgColor @"FFFFFF"

#define kAFFINHWANGBrokerLogo @"AppLogoAFFINHWANG"
#define kAFFINHWANGBrokerLogoName @"AppLogoAFFINHWANGName"
#define kAFFINHWANGTextColor @"183864"
#define kAFFINHWANGPlaceholderColor @"7486A1"
#define kAFFINHWANGBgColor @"FFFFFF"

#define kCIMBBANKBrokerLogo @"AppLogoCIMBBANK"
#define kCIMBBANKBrokerLogoName @"AppLogoCIMBBANKName"
#define kCIMBBANKTextColor @"991932"
#define kCIMBBANKPlaceholderColor @"D196A0"
#define kCIMBBANKBgColor @"FFFFFF"

typedef enum : NSInteger{
    BrokerTag_Unknown 		= -1,
    BrokerTag_N2N		 	= 1,
	BrokerTag_CIMB			= 2,
	BrokerTag_AffinHwang	= 3,
	BrokerTag_iPacOnline	= 4,
	BrokerTag_TAOnline		= 5,
	BrokerTag_PMLink2U		= 6,
	BrokerTag_SJenie		= 7,
	BrokerTag_Am			= 10,
	BrokerTag_FA			= 12,
	BrokerTag_PSE			= 13,
	BrokerTag_Apex			= 17,
	BrokerTag_KAF			= 18,
	BrokerTag_HD_Capital	= 20,
	BrokerTag_DBSV_SG		= 21,
	BrokerTag_Abacus		= 22,
	BrokerTag_SSI			= 23, //Add more - Maybe update by server later
    BrokerTag_MNA            = 24
}BrokerTag;

//SCREEN TYPE
typedef enum: NSInteger {
	MInfoContentType_Unknown = 0,
	MInfoContentType_Sector,
	MInfoContentType_Indices,
	MInfoContentType_ISIN, MInfoContentType_Status,
	MInfoContentType_Open, MInfoContentType_Volume,
	MInfoContentType_HighPrice, MInfoContentType_Value,
	MInfoContentType_LowPrice,  MInfoContentType_Trades,
	MInfoContentType_TradingBoard, MInfoContentType_BuyRate,
	MInfoContentType_LotSize, MInfoContentType_TotBVol,
	MInfoContentType_Currency, MInfoContentType_TotSVol,
	MInfoContentType_SharedIssue, MInfoContentType_Tot_Short_SVol,
	MInfoContentType_MktCap, MInfoContentType_Tot_B_Trans,
	MInfoContentType_ParValue, MInfoContentType_Tot_S_Trans,
	MInfoContentType_Ceiling, MInfoContentType_DaySpread,
	MInfoContentType_Floor, MInfoContentType_Mkt_Avg_Price,
	MInfoContentType_EPS, MInfoContentType_DynamicHigh,
	MInfoContentType_PeRatio, MInfoContentType_DynamicLow,
	MInfoContentType_NTA, MInfoContentType_52Wks_High,
	MInfoContentType_DevidendYield, MInfoContentType_52Wks_Low,
	MInfoContentType_CPF, MInfoContentType_52Wks_Spread,
	MInfoContentType_DeliveryBasis, MInfoContentType_Remark,
	MInfoContentType_ForeignLimit, 	MInfoContentType_TypeIDSS,
	MInfoContentType_ForeignShare, MInfoContentType_IDSS_Tot_Vol,
	MInfoContentType_ForeignOwnerShipLimit, MInfoContentType_IDSS_Tot_Val,
	MInfoContentType_LACP,
	//AddMore for SSI
	MInfoContentType_FullName, MInfoContentType_Exchange,
	MInfoContentType_Intraday_MktAvgPrice, MInfoContentType_RemainForeign,
	MInfoContentType_AvgVolumeOfTenDay, MInfoContentType_Beta,
	MInfoContentType_ROA, MInfoContentType_ROE,
	MInfoContentType_P_B, MInfoContentType_OI,
	MInfoContentType_Underlying,
	MInfoContentType_UnderlyingPrice,
	MInfoContentType_ForeignBuy, MInfoContentType_ForeignSell,
	MInfoContentType_Basis, MInfoContentType_ExpiryDate
} MInfoContentType;
#define kINFO_CELL_HIGH	20

@interface AppConstants : NSObject
+ (NSDictionary *) FixedExchangeListIndex;
+ (NSDictionary *) SortingList;
+ (NSDictionary *) SortByList;
+ (NSArray*) SortByListArray;
+ (NSArray*) DerivativeExchanges;

//CONTEXT MENU
+ (NSArray*) contextMenu;
+ (NSArray*) contextInfoMenu;
+ (NSArray*) contextWatchlistMenu;

//Define constant
#pragma mark - Strings
#pragma mark - Vertx API
extern NSString *const kVertxAPI_SortByServer;

//============= Notification Key ===============
extern NSString *const kVertxNo_didUpdate;
extern NSString *const kVertxNo_didFinishedSortByServer;
extern NSString *const kVertxNo_pnWebSocketConnected;
extern NSString *const kVertxNo_didReceivedWatchListItems;
extern NSString *const kVertxNo_getWatchlistSuccess;
extern NSString *const kVertxNo_didFinishedGetStockNews;

//News
extern NSString *const kVertxNo_didFinishedGetAllStockNews;
extern NSString *const kVertxNo_didFinishGetArchiveNews;
extern NSString *const kVertxNo_didFinishGetElasticNews;
extern NSString *const kVertxNo_didReceiveJarNews;

//KLCI
extern NSString *const kVertxNo_DoneGetKLCI;
//ScoreBoard
extern NSString *const kVertxNo_doneVertxGetScoreBoardWithExchange;

//Stock Changed Notification
extern NSString *const kDidUpdateStockNotification;

//====== Stock Detail Content Notification ========
extern NSString *const kDidFinishMarketDepthNotification;
extern NSString *const kdidFinishedBusinessDoneNotification;
extern NSString *const kDidFinishedTimeAndSalesNotification;

//Watchlist
extern NSString *const kDidFinishAddStockToWatchlistNotification;
extern NSString *const kDidErrorLoadingContentOfWatchlist;
//Stock Info
extern NSString *const kDidGetStockInfo_Notification;

//PORTFOLIO
extern NSString *const kDidUpdatePortfolioContentNotification;

//LineChart
extern NSString *const kDidLoadDataChartNotification;

@end
