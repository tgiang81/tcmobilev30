//
//  NSObject_AppConstants.h
//  TCMobileVertx
//
//  Created by Scott Thoo on 5/23/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//


#import "AppConstants.h"
#import "QCConstants.h"
#import "AppControl.h"

@implementation AppConstants


+ (NSArray*) DerivativeExchanges {

    return @[@"MY",@"YM",@"ES",@"ZS",@"UC"];
}

+ (NSArray*) contextMenu {
	return @[@(MContextMenu_Trading), @(MContextMenu_Info), @(MContextMenu_Favorite)];
}

+ (NSArray*) contextInfoMenu {
	return @[@(MContextMenu_Trading), @(MContextMenu_Favorite)];
}

+ (NSArray*) contextWatchlistMenu {
	return @[@(MContextMenu_Trading), @(MContextMenu_Info)];
}

// Subscribe fields
+ (NSDictionary *) SortingList
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
			@"Indices", FID_100002_S_INDICES,
			@"ISIN", FID_157_S_ISIN,
			@"Trading Board", FID_100001_S_TRD_BOARD,
			@"Buy Rate", FID_CUSTOM_F_BUY_RATE,
            @"Volume",FID_101_I_VOLUME,
            @"High",FID_56_D_HIGH_PRICE,
            @"Low",FID_57_D_LOW_PRICE,
            @"Last",FID_98_D_LAST_DONE_PRICE,
            @"L-Done",FID_99_I_LAST_DONE_LOT,
            @"LACP",FID_51_D_REF_PRICE,
            @"Open",FID_55_D_OPEN_PRICE,
            @"Par",FID_123_D_PAR_VALUE,
            @"EPS",FID_127_D_EPS,
            @"Stock Name",FID_38_S_STOCK_NAME,
            @"Stock Code",FID_33_S_STOCK_CODE,
            @"Company Name",FID_39_S_COMPANY,
            @"Sector",FID_37_S_SECTOR_NAME,
            @"Prev Close",FID_50_D_CLOSE,
            //Added trades/tradeval/top Dec2016
            @"Trades",FID_132_I_TOT_OF_TRD,
            @"TradeVal",FID_102_D_VALUE,
            @"Top",FID_153_D_THEORETICAL_PRICE,
            @"Change%",FID_CUSTOM_F_CHANGE_PER,
            @"Change",FID_CUSTOM_F_CHANGE,
            @"Currency",FID_134_S_CURRENCY,
            // Bid/Ask
            @"BuyQuantity1",FID_58_I_BUY_QTY_1,
            @"BuyPrice1",FID_68_D_BUY_PRICE_1,
            @"SellQuantity1",FID_78_I_SELL_QTY_1,
            @"SellPrice1",FID_88_D_SELL_PRICE_1,
            @"Trns_No",FID_103_I_TRNS_NO,
            @"TotalBuyVol",FID_238_I_TOTAL_BVOL,
            @"TotalSellVol",FID_237_I_TOTAL_SVOL,
            @"TotalBuyTrans",FID_241_I_TOTAL_BTRANS,
            @"TotalSellTrans",FID_242_I_TOTAL_STRANS,
            @"M-Cap",FID_100004_F_MKT_CAP,
            @"ShareIssued",FID_41_D_SHARES_ISSUED,
            @"TradingPhase",FID_156_S_TRD_PHASE,
            @"Stock Name CN",FID_130_S_SYSBOL_2,
            @"Stock Sector CN",FID_158_S_SYSBOL2_SECNAME,
            @"Stock Comp CN",FID_159_S_SYSBOL2_COMP,
            @"Stock Remarks",FID_135_S_REMARK,
			@"Foreign Limit", FID_155_S_FOREIGN_LIMIT,
			@"Ownership Limit", FID_CUSTOM_FOREIGN_OWNERSHIP,
			@"Total Short Sell Vol", FID_239_D_TOTAL_SHORT_SELL_VOL,
			@"Type", FID_119_S_TEXT_TYPE,
			@"NTA", FID_113_I_REG_NO,
			@"PE Ratio", FID_138_S_WINNING,
			@"Foreign Shared", FID_191_I_BO_QTY_2,
			@"Status", FID_48_S_STATUS,
			@"Path Name", FID_CUSTOM_S_PATH_NAME,
			@"STATE", FID_44_S_STATE,
            nil];
}

// USE ARRAY SO WE CAN REARRANGE THE ORDER IF NECESSARY
// This is Quote Screen's sorting fields (ASC = ascending order, DESC= descending order)

// should have 6 drop down list, no scroll 1. top Volume 2. top Gainer, 3. top gainer %, 4 Top loser, 5. top loser %, 6. Top value
+(NSArray*) SortByListArray {
    return [NSArray arrayWithObjects:
                [NSDictionary dictionaryWithObjectsAndKeys:@"Volume",@"101|DESC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Gainers",@"change|DESC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Gainers%",@"changePer|DESC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Losers",@"change|ASC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Losers%",@"changePer|ASC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Name",@"38|ASC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Trade",@"132|DESC", nil],
				[NSDictionary dictionaryWithObjectsAndKeys:@"Value",@"102|DESC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Close",@"50|DESC", nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"LACP",@"51|DESC",nil],
                [NSDictionary dictionaryWithObjectsAndKeys:@"Bid.Qty",@"58|DESC", nil],
            nil];
}

// Deprecated (use SortByListArray above pls).
// NSDictionaries sorting is not straight forward. 
+ (NSDictionary *) SortByList
{
    //keys are String combine with saperator "|".substring it when want to use em.
    //Values are the titles for show
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"Sort By: Volume",@"101|DESC",
            @"Sort By: Gainers",@"change|DESC",
            @"Sort By: Gainers%",@"changePer|DESC",
            @"Sort By: Losers",@"change|ASC",
            @"Sort By: Losers%",@"changePer|ASC",
            @"Sort By: Name",@"38|ASC",
            @"Sort By: Trade",@"132|DESC",
            @"Sort By: Value",@"102|DESC",
            @"Sort By: Close",@"50|DESC",
            @"Sort By: LACP",@"51|DESC",
            @"Sort By: Bid.Qty",@"58|DESC",
            nil];
}

// this can be gotten from ExchangeInfo
// Todo: populate from ExchangeInfo?

+ (NSDictionary *) FixedExchangeListIndex
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"INDEX_HSI.HK",@"HK",
            @"INDEX_HSI.HKD",@"HKD",
            @"INDEX_JKSE.JK",@"JK",
            @"INDEX_JKSE.JKD",@"JKD",
            @"INDEX_XAX.A",@"A",
            @"INDEX_XAX.AD",@"AD",
            @"INDEX_IXIC.O",@"O",
            @"INDEX_IXIC.OD",@"OD",
            @"INDEX_NYA.N",@"N",
            @"INDEX_NYA.ND",@"ND",
            @"INDEX_STI.SG",@"SG",
            @"INDEX_STI.SGD",@"SGD",
            @"INDEX_STI.SI",@"SI",
            @"INDEX_STI.SID",@"SID",
            @"INDEX_SETI.BK",@"BK",
            @"INDEX_SETI.BKD",@"BKD",
            @"020000000.KL",@"KL",
            @"020000000.KLD",@"KLD",
            @"020000000.KL",@"MY",
            @"PSEI.PH",@"PH",
            nil];
}

#pragma mark - String Constant
#pragma mark - 1: Vertx API
NSString *const kVertxAPI_SortByServer    = @"SortByServer";


#pragma mark - 2: Notification from Vertx
NSString *const kVertxNo_didUpdate    = @"didUpdate";
NSString *const kVertxNo_didFinishedSortByServer	= @"didFinishedSortByServer";
NSString *const kVertxNo_pnWebSocketConnected		= @"pnWebSocketConnected";
//================= ON MAIN SCREEN =================
NSString *const kVertxNo_didReceivedWatchListItems		= @"didReceivedWatchListItems";
NSString *const kVertxNo_getWatchlistSuccess			= @"getWatchlistSuccess";
//News
NSString *const kVertxNo_didFinishedGetAllStockNews 	= @"didFinishedGetAllStockNews";
NSString *const kVertxNo_didFinishGetArchiveNews 	= @"didFinishGetArchiveNews";
NSString *const kVertxNo_didFinishGetElasticNews 	= @"didFinishGetElasticNews";
NSString *const kVertxNo_didReceiveJarNews 			= @"didReceiveJarNews";
NSString *const kVertxNo_didFinishedGetStockNews	= @"didFinishedGetStockNews";

//KLCI
NSString *const kVertxNo_DoneGetKLCI 	= @"DoneGetKLCI";
//ScoreBoard
NSString *const kVertxNo_doneVertxGetScoreBoardWithExchange 	= @"doneVertxGetScoreBoardWithExchange";

//Stock Changed Notification
NSString *const kDidUpdateStockNotification	= @"kDidUpdateStockNotification";

//====Stock Detail Content Notification
NSString *const kDidFinishMarketDepthNotification		= @"didFinishMarketDepth";
NSString *const kdidFinishedBusinessDoneNotification	= @"didFinishedBusinessDone";
NSString *const kDidFinishedTimeAndSalesNotification	= @"didFinishedTimeAndSales";

//Watchlist
NSString *const kDidFinishAddStockToWatchlistNotification = @"kDidFinishAddStockToWatchlistNotification";
NSString *const kDidErrorLoadingContentOfWatchlist = @"kDidErrorLoadingContentOfWatchlist";
//Stock Info
NSString *const kDidGetStockInfo_Notification = @"kDidGetStockInfo_Notification";

//PORTFOLIO
NSString *const kDidUpdatePortfolioContentNotification = @"kDidUpdatePortfolioContentNotification";

//LineChart
NSString *const kDidLoadDataChartNotification = @"kDidLoadDataChartNotification";
@end
