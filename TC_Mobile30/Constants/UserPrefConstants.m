//
//  UserPrefConstants.m
//  TCPlusUniversal
//
//  Created by Scott Thoo on 5/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "UserPrefConstants.h"
#import "QCConstants.h"
#import <AudioToolbox/AudioServices.h>
#import "AppControl.h"

@interface UserPrefConstants(){
    
}
@end

@implementation UserPrefConstants

@synthesize userSelectingStockCode, is2FARequired, deviceListFor2FA,byPass2FA, verified2FA, rdsData, localExchgList;
@synthesize userCurrentExchange, userWatchListArray, Register2FAPgURL;
@synthesize userSelectingStockCodeArr, detailReportType, tickerStockCodes, logoutDueToKicked;
@synthesize isConnected, isCollectionView, collectionViewHeaderMode, showDisclaimer;
@synthesize userQuoteScreenDynamic1,userQuoteScreenDynamic2,userQuoteScreenDynamic3,userQuoteScreenDynamic4,
userQuoteScreenDynamic5,skipPin, searchPage, PFDisclaimerWebView, PFDisclaimerWebViewURL;
@synthesize userQuoteScreenColumn, summaryReportArray, detailDayReportArray, detailNightReportArray, subDetailReportArray;
@synthesize userATPServerIP,userName,userPassword,userExchagelist,brokerName,SponsorID,Vertexaddress,fromWatchlist,
usersearchStockCodeArr,sortByProperty,watchlistFavID,quoteScreenFavID,tradingRules,currencyRateDict,
userAccountlist,TradeStatusDict,orderbooklist,userSelectedAccount,eqtyUnrealPFResultList,orderbookliststkCodeArr,
eqtyUnrealPFResultStkCodeArr,orderDetails,userTradeConfirmData,interactiveChartURL,
interactiveChartByExchange, callBuySellReviseCancel, currentExchangeData;
@synthesize brokerCode, indicesEnableByExchange, tickerStocks, isBigIpad, sectorToFilter;
@synthesize termAndConditions_EN, termAndConditions_CN, orderPadAdvance, PrimaryExchg, quoteScrPage, quantityIsLot;
@synthesize isEnabledESettlement, atpPort, atpPublicIP, atpPrivateIP, defaultCurrency;
@synthesize eSettlementURL, imgChartCache, ATPLoadBalance, HTTPSEnabled, E2EEEncryptionEnabled, orderTabIndex, isEncryption, AES_EncryptionEnabled;
@synthesize defaultView, selectedMarket, currentLanguage, vertxExchangeInfo;
@synthesize interactiveChartEnabled, enableWarrant, LoginHelpText_EN, LoginHelpText_CN;
@synthesize ATPServer,CreditLimitOrdPad, CreditLimitOrdBook, CreditLimitPortfolio, TrdShortSellIndicator;
@synthesize SessionKey, clientLimitOptionDict;
@synthesize userEmail;
@synthesize overrideExchangesName,currentExchangeInfo;
@synthesize privateIP;
@synthesize isEnabledElasticNews;
@synthesize phoneNumber;

@synthesize isEnabledPassword;
@synthesize isEnabledPin;
// This is for new login page for AmSec
@synthesize isLoginPage2Activated;
@synthesize isEnabledCustomBackground;
@synthesize customBackgroundURL;

// Setting / About US Data
@synthesize companyName;
@synthesize companyAddress;
@synthesize companyTel;
@synthesize companyFax;
@synthesize termsAndConditions;
@synthesize companyLogo;
@synthesize shareText;
@synthesize facebookLink;
@synthesize twitterText;
@synthesize linkInText;
@synthesize websiteLink;
@synthesize emailLink;
@synthesize aboutUs;
@synthesize appName;
@synthesize isActivatePushNotification;
@synthesize isHideAccountNameAtTradeView;

@synthesize qcServer;

//Banner URL
@synthesize bannerURL1;
@synthesize bannerURL2;
@synthesize bannerURL3;
@synthesize bannerImage1;
@synthesize bannerImage2;
@synthesize bannerImage3;

@synthesize filterShowMarket;

@synthesize  newsFontSize;

@synthesize pointerDecimal;

@synthesize isSupportedMultipleLanguage;

@synthesize knowledgeTitle;
@synthesize knowledgeURL;

@synthesize accountID;
@synthesize userParamQC;
@synthesize elasticCat;
@synthesize tempQcServer;
@synthesize tempUserParamQC;
@synthesize isQCCompression;

@synthesize allNewsResultList;
@synthesize isShowTradingLimit;
@synthesize deviceListForSMSOTP;
@synthesize smsOTPHeaderKey;
@synthesize lastPFPage;
@synthesize futuresUnrealPFResultStkCodeArr;
@synthesize ElasticNewsServerAddress;
@synthesize researchReportDetailNewsAddress;
@synthesize appID;

// Annnoucement
@synthesize isBrokerAnnoucement;
@synthesize brokerAnnoucement;

// TRXFormula
@synthesize trxFeeFormulaDict;
@synthesize isEnabledTRXFee;

+ (UserPrefConstants *)singleton{
	static UserPrefConstants *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[UserPrefConstants alloc] init];
	});
	return _instance;
}
- (instancetype)init{
    if(self = [super init])
    {
        // NewLogin Page
        isLoginPage2Activated = YES;
		userExchagelist = @[].mutableCopy;
        //Default
        orderDetails =[[OrderDetails alloc]init];
        userTradeConfirmData =[[UserAccountClientData alloc]init];
        tradingRules =[NSMutableDictionary dictionary];
        currencyRateDict = [NSMutableDictionary dictionary];
        userAccountlist =[NSMutableArray array];
        TradeStatusDict =[NSMutableDictionary dictionary];
        interactiveChartByExchange = [NSMutableArray array];
        indicesEnableByExchange = [NSMutableArray array];
        tickerStocks =  [[NSMutableArray alloc] init];
        tickerStockCodes = [[NSMutableArray alloc] init];
        imgChartCache = [[NSCache alloc] init];
        userWatchListArray = [[NSMutableArray alloc] init];
        deviceListFor2FA = [[NSMutableArray alloc] init];
        deviceListForSMSOTP = [[NSMutableArray alloc] init];
        localExchgList = [[NSMutableArray alloc] init];
        filterShowMarket = [[NSMutableArray alloc] init];
        elasticCat = [[NSMutableArray alloc] init];
        allNewsResultList = [[NSMutableArray alloc] init];
        futuresUnrealPFResultStkCodeArr = [[NSMutableArray alloc] init];
        trxFeeFormulaDict = [[NSMutableDictionary alloc] init];     //[TrxFeeFormula]
        
        self.rdsData = [[RDSData alloc] init];
        appID = @"MD";
        if(IS_IPHONE){
            appID = @"MI";
        }
        enableWarrant = NO;
        isEnabledTRXFee = NO;
        isSupportedMultipleLanguage = YES;
        lastPFPage = 0;
        
        newsFontSize = 100;
        logoutDueToKicked = [NSDictionary new];
        ATPLoadBalance = NO;
        HTTPSEnabled = NO;
        isEncryption = NO;
        AES_EncryptionEnabled = NO;
        E2EEEncryptionEnabled = NO;
        isEnabledPassword = NO;
        isEnabledPin = NO;
        atpPrivateIP = @"";
        atpPublicIP = @"";
        atpPort = @"";
        defaultCurrency = @"MYR";
        orderTabIndex = 0; // first selection = orderbook
        selectedMarket = @"10"; // default boardlot is Normal Board Lot (list of markets available from exchangeinfo vertx).
        currentLanguage = @"en"; // english is default language
        
        userCurrentExchange = @"";
        userQuoteScreenDynamic1 =FID_101_I_VOLUME;
        userQuoteScreenDynamic2 =FID_78_I_SELL_QTY_1;
        userQuoteScreenDynamic3 =FID_88_D_SELL_PRICE_1;
        userQuoteScreenDynamic4 =FID_68_D_BUY_PRICE_1;
        userQuoteScreenDynamic5 =FID_58_I_BUY_QTY_1;
        
        collectionViewHeaderMode = QUOTESCREEN;
        
        pointerDecimal = 3;
        isEnabledCustomBackground = YES;
        customBackgroundURL = @"";
        quoteScrPage = 0;
        callBuySellReviseCancel = 0;
        isQCCompression = YES;
        sectorToFilter = nil;
        
        // isBigIpad Can be used for whether to sizeToFit fonts or not
        // On big ipad (ipad pro), labels frames are enlarged, but font size remain..
        // Use sizeToFit to enlarge based on frame size.
        if ([UIScreen mainScreen].bounds.size.width >= 1366) {
            isBigIpad = YES;
        } else {
            isBigIpad = NO;
        }
        
        // SETTINGS PREFENCES
        
        NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
        
        // commented because default setting is in PLIST Config.
//        if ([def objectForKey:@"QtyIsLot"]==nil) {
//            quantityIsLot = YES;
//            [def setBool:quantityIsLot forKey:@"QtyIsLot"];
//            [def synchronize];
//        } else {
//            quantityIsLot = [def boolForKey:@"QtyIsLot"];
//        }
        
        if ([def objectForKey:@"QuoteScreenCardView"]==nil) {
            isCollectionView = NO; // default is listview
            [def setBool:NO forKey:@"QuoteScreenCardView"];
            [def synchronize];
        } else {
            isCollectionView = [def boolForKey:@"QuoteScreenCardView"];
            
        }
		
		_supportedExchanges = @[].copy;
		vertxExchangeInfo = @[].mutableCopy;
        
    }
    return self;
}
- (NSString *)userSelectingStockCode{
    return _userSelectingThisStock.stockCode;
}
#pragma mark - Exchange UTILS
- (NSArray *)supportedExchanges{
	if (self.userExchagelist.count == 0 || !self.userSelectedAccount) {
		return @[].copy;
	}
	NSMutableArray *supportExs = @[].mutableCopy;
	/* +++ Why can not use self.userSelectedAccount.supported_exchanges for each account
	for (ExchangeData *ex in self.userExchagelist) {
		if ([self.userSelectedAccount.supported_exchanges containsObject:ex.feed_exchg_code]) {
			[supportExs addObject:ex];
		}
	}
	 */
	supportExs = self.userExchagelist;
	//Filter from VertxExchange Info
	NSMutableArray *filteredEx_List = @[].mutableCopy;
	for (ExchangeData *exData in supportExs) {
		if (vertxExchangeInfo.count == 0) {
			return [supportExs copy];
		}
		
		if ([vertxExchangeInfo.allKeys containsObject:exData.feed_exchg_code]) {
			[filteredEx_List addObject:exData];
		}
	}
	return [filteredEx_List copy];
}

- (NSArray *)userQuoteScreenColumn{
    NSArray *result = @[@"33", FID_38_S_STOCK_NAME, FID_98_D_LAST_DONE_PRICE, FID_CUSTOM_F_CHANGE_PER, userQuoteScreenDynamic1, userQuoteScreenDynamic2, userQuoteScreenDynamic3, userQuoteScreenDynamic4, userQuoteScreenDynamic5];
    return result;
}

- (NSString *)dataFormatingNumber:(int)number{
    
    NSString *stringNumber;
    
    return stringNumber;
    
}

-(NSString*)abbreviatePrice:(double)price {
    NSString *abbrevNum;

    NSString *sign = @"";
    if (price<0) {
        price = -price;
        sign = @"-";
    }
    //Prevent numbers smaller than 1000 to return NULL
    if (price >= 1000) {
        NSArray *abbrev = @[@"K", @"M", @"B"];
        
        for (int i = (int)abbrev.count - 1; i >= 0; i--) {
            
            // Convert array index to "1000", "1000000", etc
            long long size = pow(10,(i+1)*3);
            
            if(size <= price) {
                // Removed the round and dec to make sure small numbers are included like: 1.1K instead of 1K
                price = price/size;
                NSString *numberString = [self floatToString:price];
                
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
            
        }
    } else {
        
        // Numbers like: 999 returns 999 instead of NULL
        abbrevNum = [NSString stringWithFormat:@"%@%.2f",sign, price];
    }
    
    
    
    return abbrevNum;
}

// this method assume num is positive.
-(NSString *)abbreviateNumber:(long long)num {
    
    NSString *abbrevNum;
    double number = (double)num;
    
    //Prevent numbers smaller than 1000 to return NULL
    if (num >= 1000) {
        NSArray *abbrev = @[@"K", @"M", @"B"];
        
        for (int i = (int)abbrev.count - 1; i >= 0; i--) {
            
            // Convert array index to "1000", "1000000", etc
            long long size = pow(10,(i+1)*3);
            
            if(size <= number) {
                // Removed the round and dec to make sure small numbers are included like: 1.1K instead of 1K
                number = number/size;
                NSString *numberString = [self floatToString:number];
                
                // Add the letter for the abbreviation
                abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
            }
        }
        
    } else {
        
        // Numbers like: 999 returns 999 instead of NULL
        abbrevNum = [NSString stringWithFormat:@"%lld", (long long)number];
    }
    
    return abbrevNum;
}

- (NSString *) floatToString:(double) val {
    NSString *ret = @"";

    ret = [NSString stringWithFormat:@"%.3f", val];
   
    unichar c = [ret characterAtIndex:[ret length] - 1];
    
    while (c == 48) { // 0
        ret = [ret substringToIndex:[ret length] - 1];
        c = [ret characterAtIndex:[ret length] - 1];
        
        //After finding the "." we know that everything left is the decimal number, so get a substring excluding the "."
        if(c == 46) { // .
            ret = [ret substringToIndex:[ret length] - 1];
        }
    }
    
    return ret;
}

- (void) playClickSoundEffect{
    NSString *path  = [[NSBundle mainBundle] pathForResource:@"soundeffect" ofType:@"m4a"];
    NSURL *pathURL = [NSURL fileURLWithPath : path];
    
    SystemSoundID audioEffect;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) pathURL, &audioEffect);
    AudioServicesPlaySystemSound(audioEffect);
    // Using GCD, we can use a block to dispose of the audio effect without using a NSTimer or something else to figure out when it'll be finished playing.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        AudioServicesDisposeSystemSoundID(audioEffect);
    });
}


/* JAVA CODE (ANDROID)
 
 String [] currentTime = currentDate();
 String time = currentTime[1]; //current time with GMT+0 in hhmmss
 System.out.println("timechart: " + time);
 String encryptedTime = "";
 String key = currentTime[0]; //today date with GMT+0 in yymmdd
 
 char[] timeArray = time.toCharArray();
 char[] keyArray = key.toCharArray();
 
 for(int i=0;i<timeArray.length;i++){
 //Using bitwise xor operator
 int xor =  ((char) timeArray[i] ^ keyArray[i]);
 //convert integer to hex with 2 digit
 encryptedTime += "%" + String.format("%02X", xor);
 System.out.println("timechart: " + encryptedTime);
 }
 return encryptedTime;
 
 */

// added in 1.8.5 (for use in Teletrader chart url parameter)


-(BOOL)isFloatZero:(double)floatNum {
    
    BOOL isZero = NO;
    
    if ((floatNum<0.000000000001)&&(floatNum>-0.000000000001)) isZero = YES;
    
    return isZero;
}


-(NSString*)encryptTime {
    NSString *res = @"";
    
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hhmmss"];
    NSString *dataStr = [dateFormatter stringFromDate: currentTime];
    
    [dateFormatter setDateFormat:@"yyMMdd"];
    NSString *keyStr = [dateFormatter stringFromDate: currentTime];
    
    NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
    
    char *dataArr = (char *) [data bytes];
    char *keyArr = (char *) [[keyStr dataUsingEncoding:NSUTF8StringEncoding] bytes];
    
    // For each character in data, xor with current value in key
    for (int x = 0; x < [data length]; x++)
    {
        int xor = dataArr[x] ^ keyArr[x];
        
        res = [NSString stringWithFormat:@"%@%%%02X",res, xor];
        
    }
    
    return res;
}

-(void)overrideChartSettings {
    // override chart type MUST BE AFTER EXCHANGE SELECTED

    __block NSString *chartTypeToLoad = @"Image";
    
    // override chartType based on exchange
    NSArray *interChartExchangeArr = [UserPrefConstants singleton].interactiveChartByExchange;
    if ([interChartExchangeArr count]>0) {
        // NSLog(@"interChartExchangeArr %@", interChartExchangeArr);
        [interChartExchangeArr enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
            
            // TODO: this is problem when changing exhcanges back and forth
            NSString *eachExch = (NSString*)obj;
            if ([eachExch isEqualToString:[UserPrefConstants singleton].userCurrentExchange] && interactiveChartEnabled) {
                chartTypeToLoad = [UserPrefConstants singleton].chartType; // Modulus or Teletrader
            }
            
        }];
    }
    // TODO: just for debug
    // chartTypeToLoad = @"Image";
    
    //NSLog(@"chartTypeToLoad %@", chartTypeToLoad);
    
    [UserPrefConstants singleton].chartTypeToLoad = chartTypeToLoad;
}


-(NSString*)stripDelayedSymbol:(NSString*)code {
    NSString *result = code;
    NSString *lastChar = [code substringFromIndex:[code length] - 1];
    if ([lastChar isEqualToString:@"D"]) {
        result = [result substringToIndex:[result length] - 1];
    }
    return result;
}

// dateStr input example: 2016-10-13 13:07:10.0
-(NSString*)getLogicalDate:(NSString*)dateStr {
    NSString *result = @"";
    
    NSArray *dateArray = [dateStr componentsSeparatedByString:@" "];
    
    if ([dateArray count]>0) {
        NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.0"];
        NSDate *yourDate                =   [dateFormatter dateFromString:dateStr];
        //[dateFormatter setDateFormat:@"d-MMM / HH:mm"]; <-- if you want to display time together
        [dateFormatter setDateFormat:@"d MMM yyyy HH:mm"];
        result          =   [dateFormatter stringFromDate:yourDate];
        if ([result length]<=0) result = dateStr; // if server change this date format, then return the whole date str.
    } else {
        result = dateStr;
    }
    return result;
}

-(NSString*)getOrderSourceStr:(NSString *)orderSrcCode {
    
    NSString *res = @"";
    
    if ([orderSrcCode isEqualToString:@"I"]) {
        res = [LanguageManager stringForKey:@"Internet Retail"];
    }
    else if ([orderSrcCode isEqualToString:@"D"]) {
        res = [LanguageManager stringForKey:@"Dealer"];
    }
    else if ([orderSrcCode isEqualToString:@"W"]) {
        res = [LanguageManager stringForKey:@"Wap, Mobile"];
    }
    else if ([orderSrcCode isEqualToString:@"S"]) {
        res = [LanguageManager stringForKey:@"SMS"];
    }
    else if ([orderSrcCode isEqualToString:@"P"]) {
        res = [LanguageManager stringForKey:@"Phone"];
    }
    else if ([orderSrcCode isEqualToString:@"F"]) {
        res = [LanguageManager stringForKey:@"Fax"];
    }
    
    return res;
}
+ (NSString *) getIDSSStatus:(NSString *)status{
    unichar uc2 = (unichar)[status characterAtIndex:0];
    NSString *statusToCheck = [NSString stringWithCharacters:&uc2 length:1];
    NSString *statusString = @"-";
    if ([statusToCheck isEqualToString:@"2"]) {
        statusString = [LanguageManager stringForKey:@"PDT, RSS"]; //1
    }else if([statusToCheck isEqualToString:@"3"]){
        statusString = [LanguageManager stringForKey:@"PDT, IDSS"];
    }
    else if([statusToCheck isEqualToString:@"4"]){
        statusString = [LanguageManager stringForKey:@"RSS, IDSS"];
    }
    else if([statusToCheck isEqualToString:@"5"]){
        statusString = [LanguageManager stringForKey:@"RSS, PDT, IDSS"];
    }
    else if([statusToCheck isEqualToString:@"R"]){
        statusString = [LanguageManager stringForKey:@"RSS"];
    }
    else if([statusToCheck isEqualToString:@"P"]){
        statusString = [LanguageManager stringForKey:@"PDT"];
    }
    else if([statusToCheck isEqualToString:@"I"]){
        statusString = [LanguageManager stringForKey:@"IDSS"];
    }
    
    
    return statusString;
    
}


- (NSString *) getBoardStatus:(NSString *)statusChar{
    
    NSString *statusString = @"";
    if ([statusChar isEqualToString:@"N"]) {
        statusString = [LanguageManager stringForKey:@"NEW"]; //1
    }else if([statusChar isEqualToString:@"W"]){
        statusString = [LanguageManager stringForKey:@"NEW WITH ALERT ANNOUNCEMENT"]; //2
    }else if([statusChar isEqualToString:@"A"]){
        statusString = [LanguageManager stringForKey:@"ACTIVE"]; //3
    }else if([statusChar isEqualToString:@"T"]){
        statusString = [LanguageManager stringForKey:@"ACTIVE WITH ALERT ANNOUNCEMENT"]; //4
    }else if([statusChar isEqualToString:@"Q"]){
        statusString = [LanguageManager stringForKey:@"DESIGNATED"]; //5
    }else if([statusChar isEqualToString:@"S"]){
        statusString = [LanguageManager stringForKey:@"SUSPENDED"]; //6
    }else if([statusChar isEqualToString:@"P"]){
        statusString = [LanguageManager stringForKey:@"SUSPENDED WITH ALERT ANNOUNCEMENT"]; //7
    }else if([statusChar isEqualToString:@"D"]){
        statusString = [LanguageManager stringForKey:@"DELISTED"]; //8
    }else if([statusChar isEqualToString:@"E"]){
        statusString = [LanguageManager stringForKey:@"EXPIRED"]; //9
    }else if([statusChar isEqualToString:@"R"]){
        statusString = [LanguageManager stringForKey:@"READY"]; //10
    }else if([statusChar isEqualToString:@"H"]){
        statusString = [LanguageManager stringForKey:@"HALTED"]; //11
    }else if([statusChar isEqualToString:@"Y"]){
        statusString = [LanguageManager stringForKey:@"DELAYED"]; //12
    }else if([statusChar isEqualToString:@"X"]){
        statusString = [LanguageManager stringForKey:@"SHUTTING DOWN"]; //13
    }else if([statusChar isEqualToString:@"I"]){
        statusString = [LanguageManager stringForKey:@"IMMEDIATE DELIVERY"]; //14
    }else if([statusChar isEqualToString:@"L"]){
        statusString = [LanguageManager stringForKey:@"PENDING LISTING"]; //15
    }else if([statusChar isEqualToString:@"M"]){
        statusString = [LanguageManager stringForKey:@"PENDING ANNOUNCEMENT"]; //16
    }else if([statusChar isEqualToString:@"C"]){
        statusString = [LanguageManager stringForKey:@"CONDITIONAL TRADING"]; //17
    }else if([statusChar isEqualToString:@"U"]){
        statusString = [LanguageManager stringForKey:@"CONDITIONAL WHEN ISSUE"]; //18
    }else if([statusChar isEqualToString:@"Z"]){
        statusString = [LanguageManager stringForKey:@"WHEN ISSUE"]; //19
    }else if([statusChar isEqualToString:@"B"]){
        statusString = [LanguageManager stringForKey:@"BUYING IN MARKET ONLY"]; //20
    }else if([statusChar isEqualToString:@"J"]){
        statusString = [LanguageManager stringForKey:@"ADJUSTED"]; //21
    }else if([statusChar isEqualToString:@"G"]){
        statusString = [LanguageManager stringForKey:@"FROZEN"]; //22
    }else if([statusChar isEqualToString:@"V"]){
        statusString = [LanguageManager stringForKey:@"RESERVE"]; //23
    }else if([statusChar isEqualToString:@"*"]){
        statusString = [LanguageManager stringForKey:@"CHART"]; //24
    }else if([statusChar isEqualToString:@"F"]){
        statusString = [LanguageManager stringForKey:@"CIRCUIT BREAKER"]; //25
    }else if([statusChar isEqualToString:@"K"]){
        statusString = [LanguageManager stringForKey:@"HALT DUE TO CIRCUIT BREAKER"]; //26
    }else{
        statusString = [LanguageManager stringForKey:@"NO STATUS"]; //27
    }
    
    return statusString;
    
}

-(NSString *)stringByStrippingHTML:(NSString*)toStrip {
    NSRange r;
    NSString *s = [toStrip copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@"\n"];
    return s;
}

- (void)showLocalReleaseNotesInView:(UIView*)view
{
    // Create the release notes view
    NSString *currentAppVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    
    NSString *notes = @"";
    
    notes = [NSString stringWithFormat:
             @"• [Fix] Enhancement on application stability \n• [Fix] LACP and LastDone value on Quote Screen"
             ];
    
    ReleaseNotesView *releaseNotesView = [ReleaseNotesView viewWithReleaseNotesTitle:[NSString stringWithFormat:@"What's new in this version\n%@:", currentAppVersion] text:notes closeButtonTitle:@"Close"];    // Show the release notes view
    [releaseNotesView showInView:view];
    // Show the release notes view
    [releaseNotesView showInView:view];
}

- (void)showRemoteReleaseNotesInView:(UIView*)view
{
    NSString *currentAppVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    [ReleaseNotesView setupViewWithAppIdentifier:@"972011951" releaseNotesTitle:[NSString stringWithFormat:@"What's new in version %@:", currentAppVersion] closeButtonTitle:@"Close" completionBlock:^(ReleaseNotesView *releaseNotesView, NSString *releaseNotesText, NSError *error){
        if (error)
        {
            //NSLog(@"An error occurred: %@", [error localizedDescription]);
        }
        else
        {
            // Create and show release notes view
            [releaseNotesView showInView:view];
        }
    }];
}


-(void)popMessage:(NSString *)message inController:(UIViewController *)controller  withTitle:(NSString*)title {
    
    NSString *resolvedTitle = title;
    
    if ([resolvedTitle length]<=0) resolvedTitle = [LanguageManager stringForKey:@"Info"];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:resolvedTitle
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:[LanguageManager stringForKey:@"Ok"]
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             // do nothing
                             
                         }];
    
    
    [alert addAction:ok];
    
    [controller presentViewController:alert animated:YES completion:nil];
}

+(long)checkAuthenticateByTouchId {
    
    NSError *error;
    [[[LAContext alloc] init] canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
    return (long)error.code;
}

-(NSMutableArray*)stringsArrayBetweenString:(NSString*)start andString:(NSString*)end fromString:(NSString *)originalString
{
    
    NSMutableArray* strings = [NSMutableArray arrayWithCapacity:0];
    
    NSRange startRange = [originalString rangeOfString:start];
    
    for( ;; )
    {
        
        if (startRange.location != NSNotFound)
        {
            
            NSRange targetRange;
            
            targetRange.location = startRange.location + startRange.length;
            targetRange.length = [originalString length] - targetRange.location;
            
            NSRange endRange = [originalString rangeOfString:end options:0 range:targetRange];
            
            if (endRange.location != NSNotFound)
            {
                
                targetRange.length = endRange.location - targetRange.location;
                [strings addObject:[originalString substringWithRange:targetRange]];
                
                NSRange restOfString;
                
                restOfString.location = endRange.location + endRange.length;
                restOfString.length = [originalString length] - restOfString.location;
                
                startRange = [originalString rangeOfString:start options:0 range:restOfString];
                
            }
            
            else
            {
                break;
            }
            
        }
        else
        {
            break;
        }
        
    }
    
    return strings;
    
}

- (NSString*) stringsBetweenString:(NSString*)start andString:(NSString*)end fromString:(NSString *)originalString{
    NSRange startRange = [originalString rangeOfString:start];
    if (startRange.location != NSNotFound) {
        NSRange targetRange;
        targetRange.location = startRange.location + startRange.length;
        targetRange.length = [originalString length] - targetRange.location;
        NSRange endRange = [originalString rangeOfString:end options:0 range:targetRange];
        if (endRange.location != NSNotFound) {
            targetRange.length = endRange.location - targetRange.location;
            return [originalString substringWithRange:targetRange];
        }
    }
    return nil;
}

- (NSString *)urlencodedString:(NSString *)input{
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)input,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingISOLatin1));
    return encodedString;
}

//dynamicFID1 =FID_101_I_VOLUME;
//dynamicFID2 =FID_78_I_SELL_QTY_1;
//dynamicFID3 =FID_88_D_SELL_PRICE_1;
//dynamicFID4 =FID_68_D_BUY_PRICE_1;
//dynamicFID5 =FID_58_I_BUY_QTY_1;
@end
