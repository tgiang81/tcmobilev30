//
//  ErrorCodeConstants.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 1/11/13.
//
//

#import <Foundation/Foundation.h>

@interface ErrorCodeConstants : NSObject

+ (NSString *) Default_Advice;
+ (NSString *) Login_Process_Failure;
+ (NSString *) Login_Connection_Failure;
+ (NSString *) ATP_TradeGetE2EEParams_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeGetPK_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeGetKey2_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeGetSession2_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeGetSession_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeLogin_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeClient_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeChgPasswd_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeChgPin_Fail:(BOOL)completion;
+ (NSString *) ATP_TradeTicket_Fail:(BOOL)completion;
+ (NSString *) LMS_Fail:(BOOL)completion;
+ (NSString *) QC_GetKey_Fail:(BOOL)completion;
+ (NSString *) QC_Login_Fail:(BOOL)completion;
+ (NSString *) QC_SectorFast_Fail:(BOOL)completion;
+ (NSString *) QC_GetNewKey_Fail:(BOOL)completion;
@end
