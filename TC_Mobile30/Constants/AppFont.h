//
//  AppFont.h
//  PlastApp
//
//  Created by Kaka on 7/12/17.
//  Copyright © 2017 ITSOL. All rights reserved.
//

#ifndef AppFont_h
#define AppFont_h


#endif /* AppFont_h */

#define kMainFontNameRegular						@"ArialMT" // - Arial
#define kMainFontNameItalic                         @"Arial-ItalicMT"
#define kMainFontNameBold                           @"Arial-BoldMT"
#define kMainFontNameBoldItalic                     @"Arial-BoldItalicMT"
#define kMainFontNameMedium                         @"Arial-BoldMT"

//Medium font with size
#define AppFont_MainFontThinWithSize(sz)							[UIFont fontWithName : kMainFontNameThin size : sz]
#define AppFont_MainFontMediumWithSize(sz)							[UIFont fontWithName : kMainFontNameMedium size : sz]
//Regular font with size
#define AppFont_MainFontRegularWithSize(sz)							[UIFont fontWithName : kMainFontNameRegular size : sz]
//Bold font with size
#define AppFont_MainFontBoldWithSize(sz)							[UIFont fontWithName : kMainFontNameBold size : sz]
//Italic font with size
#define AppFont_MainFontItalicWithSize(sz)							[UIFont fontWithName : kMainFontNameItalic size : sz]

//Bold Italic with size
#define AppFont_MainFontBoldItalicWithSize(sz)						[UIFont fontWithName : kMainFontNameBoldItalic size : sz]
