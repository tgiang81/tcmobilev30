//
//  UserPrefConstants.h
//  TCPlusUniversal
//
//  Created by Scott Thoo on 5/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderDetails.h"
#import "UserAccountClientData.h"
#import <UIKit/UIKit.h>
#import "ReleaseNotesView.h"
#import "AppControl.h"
#import "ExchangeData.h"
#import "ExchangeInfo.h"
#import "RDSData.h"
#import "SectorInfo.h"
#import "DeviceDataFor2FA.h"
#import "LanguageManager.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "StockModel.h"

@interface UserPrefConstants : NSObject

//====================================================================
//                        New Focus Oct2014
//====================================================================

//Login items
@property (nonatomic, assign) BrokerTag brTag;
@property (nonatomic, retain) NSString *userATPServerIP;
@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *uid;
@property (nonatomic, retain) NSString *pushNotificationChanelId;
@property (nonatomic, retain) NSString *brokerName;
@property (nonatomic, retain) NSString *SponsorID;
@property (nonatomic, retain) NSString *LoginHelpText_EN, *LoginHelpText_CN;
@property (nonatomic, retain) NSString *userPassword;
@property (nonatomic, retain) NSString *Vertexaddress;
@property (nonatomic, retain) NSString *LMSServerAddress;
@property (nonatomic, retain) NSString *NewsServerAddress;
@property (nonatomic, retain) NSString *ElasticNewsServerAddress;
@property (nonatomic, assign) BOOL enabledNewiBillionaire;
@property (nonatomic, assign) BOOL enableIDSS;
@property (nonatomic, assign) BOOL isCIMBSG;
@property (nonatomic, retain) NSString *iBillionareStock;
@property (nonatomic, retain) NSArray *filteriBillionaire;
@property (nonatomic, retain) NSString *researchReportDetailNewsAddress;
@property (nonatomic, retain) NSString *PrimaryExchg;
@property (nonatomic, retain) NSString *atpPublicIP;
@property (nonatomic, retain) NSString *atpPrivateIP;
@property (nonatomic, retain) NSString *atpPort;

@property (nonatomic, retain) NSString *fundamentalNewsLabel;
@property (nonatomic, retain) NSString *fundamentalNewsServer;
@property (nonatomic, retain) NSString *fundamentalSourceCode;

@property (nonatomic, retain) NSString *fundDataCompInfoURL;
@property (nonatomic, retain) NSString *fundFinancialInfoURL;
@property (nonatomic, retain) NSString *fundamentalDefaultReportCode;
@property (nonatomic, retain) NSString *userEmail;
@property (nonatomic, retain) NSString *lastLoginTime;
@property (nonatomic, retain) NSString *senderName;
@property (nonatomic) BOOL ATPLoadBalance;
@property (nonatomic) BOOL HTTPSEnabled;
@property (nonatomic) BOOL isEncryption;
@property (nonatomic) BOOL AES_EncryptionEnabled;
@property (nonatomic) BOOL E2EEEncryptionEnabled;
@property (nonatomic) BOOL isAuthOrderBook;
@property (nonatomic) BOOL isAuthByTouchId;
@property (nonatomic) NSUInteger newsFontSize;
@property (nonatomic) int sessionTimeoutSeconds;

@property (nonatomic, strong) NSDictionary *logoutDueToKicked;
@property (nonatomic) BOOL isLoginPage2Activated;
@property (nonatomic) BOOL isEnabledCustomBackground;
@property (nonatomic, retain) NSString *customBackgroundURL;

// Password/Pin
@property (nonatomic) BOOL isEnabledPassword;
@property (nonatomic) BOOL isEnabledPin;

// Ticker
@property (nonatomic, retain) NSMutableArray *tickerStocks, *tickerStockCodes;

@property (nonatomic) BOOL isArchiveNews;
@property (nonatomic) BOOL isElasticNews;
@property (nonatomic) BOOL isBigIpad;
@property (nonatomic) int callBuySellReviseCancel;
@property (nonatomic, retain) NSDictionary *reviseCancelDict;

// Default View after login
@property (nonatomic) int defaultView;

// Selected Board Lot
@property (nonatomic, strong) NSString *selectedMarket;
@property (nonatomic, strong) NSString *currentLanguage;

// Currency
@property (nonatomic, strong) NSString *defaultCurrency;
@property (nonatomic, strong) ExchangeData *currentExchangeData;

//Quote Screen
@property (atomic, assign) UserAccountClientData *userSelectedAccount;
@property (nonatomic, retain) NSMutableArray *userSelectingStockCodeArr;
@property (nonatomic, retain) NSString *interactiveChartURL;
@property (nonatomic, retain) NSString *intradayChartURL;
@property (nonatomic, retain) NSString *historicalChartURL;
@property (nonatomic, retain) NSString *tvChart;
@property (nonatomic, retain) NSString *chartType;
@property (nonatomic, retain) NSString *appID;
@property (nonatomic, retain) NSString *chartTypeToLoad;
@property (nonatomic, retain) NSMutableArray *interactiveChartByExchange;
@property (nonatomic, retain) NSMutableArray *indicesEnableByExchange;
@property (nonatomic, retain) NSMutableArray *dashBoard;
@property (nonatomic, strong) SectorInfo *sectorToFilter;
@property (nonatomic) BOOL interactiveChartEnabled;
@property (nonatomic) BOOL enableWarrant;
@property (nonatomic) bool isEnabledElasticNews;
@property (nonatomic, strong) SectorInfo *selectedSector; //Select local object
@property (nonatomic, strong) SectorInfo *selectedSubsector; //Select local object

@property (nonatomic, strong) StockModel *userSelectingThisStock;                     //Holding single stockCode. For Stock Details.

@property (nonatomic, strong) NSString *userSelectingStockCode;                     //Holding single stockCode. For Stock Details.
@property (nonatomic, strong) NSString *userCurrentExchange;                        //Holding exhange. Current Default : "KL"
@property (nonatomic, retain) NSArray *userQuoteScreenColumn;                       //QuoteScreen headers FIDs for Vertx API. (38,36,101... ... ..)
@property (nonatomic, retain) NSString *userQuoteScreenDynamic1;
@property (nonatomic, retain) NSString *userQuoteScreenDynamic2;
@property (nonatomic, retain) NSString *userQuoteScreenDynamic3;
@property (nonatomic, retain) NSString *userQuoteScreenDynamic4;
@property (nonatomic, retain) NSString *userQuoteScreenDynamic5;

@property (nonatomic) bool fromWatchlist;
@property (nonatomic) bool skipPin;
@property (nonatomic) bool orderPadAdvance;
@property (nonatomic) bool quantityIsLot;
@property (nonatomic) bool quantityModifierMode;
@property (strong, nonatomic) NSString *sortByProperty;
@property(nonatomic, assign) int watchlistFavID;
@property(nonatomic, strong) NSNumber *quoteScreenFavID;
@property (strong, nonatomic) NSString *watchListName;
@property(nonatomic, assign) int prevWatchlistFavID;
@property(nonatomic, assign) int quoteScrPage;

//For More info: Such as: Term of Use, Privacy link...
@property (strong, nonatomic) NSArray *linksOnHomePage;
@property (strong, nonatomic) NSString *termOfUseLink;
// Home Screen announcement URL
@property (nonatomic, strong) NSString *AnnouncementURL;

// Data get from ATP server
@property (retain, nonatomic) OrderDetails *orderDetails;
@property (retain, nonatomic) UserAccountClientData *userTradeConfirmData;


@property (nonatomic, retain) NSMutableDictionary   *TradeStatusDict;                  //User Infomations from ATP. Broker Code, etc.
@property (nonatomic, retain) NSMutableDictionary   *userInfoDict;                  //User Infomations from ATP. Broker Code, etc.
@property (nonatomic, strong) NSMutableDictionary   *userWatchListDict_FavId_Name;  //User WatchList (key)FavID---Name(object)

@property (nonatomic, strong) NSMutableArray        *userWatchListArray;

@property (nonatomic, strong) NSMutableArray        *userWatchListStockCodeArr;     //User WatchList items. Stock Codes Array.
@property (nonatomic, retain) NSMutableArray        *usersearchStockCodeArr;

#pragma mark - ExChange
@property (nonatomic, strong) NSMutableArray        *userExchagelist;               //User ExchangeList
@property (strong, nonatomic) NSArray *supportedExchanges;
@property (nonatomic, strong) NSMutableArray        *userAccountlist;               //User Account
@property (nonatomic, strong) NSMutableDictionary   *tradingRules;
@property (nonatomic, strong) NSMutableDictionary   *currencyRateDict;
@property (nonatomic, strong) NSMutableDictionary   *clientLimitOptionDict;
@property (nonatomic, strong) NSString *idssValue;

// ORDER BOOK/ PORTFOLIO
@property (nonatomic, assign) BOOL showDisclaimer;
@property (nonatomic, assign) NSInteger             orderTabIndex;
@property (nonatomic, strong) NSMutableArray        *orderbooklist;
@property (nonatomic, strong) NSMutableArray        *orderbookliststkCodeArr;
@property (nonatomic, strong) NSMutableArray        *eqtyUnrealPFResultList;
@property (nonatomic, strong) NSMutableArray        *eqtyUnrealPFResultStkCodeArr;
@property (nonatomic, strong) NSMutableArray        *futuresUnrealPFResultStkCodeArr;
@property (nonatomic, strong) NSMutableArray *summaryReportArray, *detailDayReportArray, *detailNightReportArray, *subDetailReportArray;
@property (nonatomic, assign) NSInteger detailReportType;
@property (nonatomic) BOOL PFDisclaimerWebView;
@property (nonatomic, strong) NSString *PFDisclaimerWebViewURL;
@property (nonatomic, assign) BOOL CreditLimitOrdPad, CreditLimitOrdBook, CreditLimitPortfolio;
@property (nonatomic, strong) NSDictionary *TrdShortSellIndicator;

@property (nonatomic) BOOL isEnabledESettlement;
@property (nonatomic, strong) NSString *eSettlementURL;

// Connection
@property (nonatomic) BOOL isConnected;


// Push Notification Parameters
@property (nonatomic, retain) NSString *decryptionKey;
@property (nonatomic, retain) NSString *deviceToken;
@property (nonatomic, retain) NSString *deviceUDID;
@property (nonatomic, retain) NSString *accountName;
@property (nonatomic, retain) NSString *exchange;
@property (nonatomic, retain) NSString *bundleID;
@property (nonatomic, retain) NSString *ipAddress;
@property (nonatomic, retain) NSString *activationCode;
@property (nonatomic, retain) NSString *bhCode;
@property (nonatomic, retain) NSString *ATPServer;
@property (nonatomic, retain) NSString *SessionKey;

// Stock Alert
@property (nonatomic, retain) NSString *token;
@property (nonatomic, retain) NSString *stockAlertURL;
@property (nonatomic, retain) NSString *stockAlertGetPublicURL;
@property (nonatomic, retain) NSString *pushNotificationRegisterURL;
@property (nonatomic) BOOL isShownStockAlertDisclaimer;
@property (nonatomic) BOOL isEnabledStockAlert;
@property (nonatomic, retain) NSString *privateIP;

// Setting / About US Data
@property (nonatomic, retain) NSString *companyName;
@property (nonatomic, retain) NSString *companyAddress;
@property (nonatomic, retain) NSString *companyTel;
@property (nonatomic, retain) NSString *companyFax;
@property (nonatomic, retain) NSString *termsAndConditions;
@property (nonatomic, retain) NSString *companyLogo;
@property (nonatomic, retain) NSString *shareText;
@property (nonatomic, retain) NSString *facebookLink;
@property (nonatomic, retain) NSString *twitterText;
@property (nonatomic, retain) NSString *linkInText;
@property (nonatomic, retain) NSString *websiteLink;
@property (nonatomic, retain) NSString *emailLink;
@property (nonatomic, retain) NSString *phoneNumber;
@property (nonatomic, retain) NSString *aboutUs;
@property (nonatomic, retain) NSString *appName;
@property (nonatomic) BOOL isActivatePushNotification;


//Banner Button Image and URL
@property (nonatomic, retain) NSString *bannerImage1;
@property (nonatomic, retain) NSString *bannerImage2;
@property (nonatomic, retain) NSString *bannerImage3;

@property (nonatomic, retain) NSString *bannerURL1;
@property (nonatomic, retain) NSString *bannerURL2;
@property (nonatomic, retain) NSString *bannerURL3;

// SaveData Parameters
@property (nonatomic, strong) NSString *brokerCode;
@property (nonatomic, strong) NSString *termAndConditions_EN, *termAndConditions_CN;
@property (nonatomic) BOOL isActivatingTouchID;

// Collection View
@property (nonatomic, assign) BOOL isCollectionView;
@property (nonatomic, assign) QuoteScreenMode collectionViewHeaderMode;
@property (nonatomic, strong) NSCache *imgChartCache;

// Override Exchange name
@property (nonatomic, strong) NSDictionary *overrideExchangesName;

// Vertx Exchange Info
@property (nonatomic, strong) NSMutableDictionary *vertxExchangeInfo;
@property (nonatomic, strong) ExchangeInfo *currentExchangeInfo;

// Order history limit
@property (nonatomic, strong) NSString *orderHistLimit;
@property (nonatomic, strong) NSString *defaultStartingScreen;

// Search screen
@property (assign, nonatomic) NSInteger searchPage;

// Trader
@property (nonatomic, strong) NSMutableArray *localExchgList;
@property (nonatomic, assign) BOOL isEnableRDS;
@property (nonatomic, assign) BOOL is2FARequired;
@property (nonatomic, assign) BOOL verified2FA;
@property (nonatomic, assign) BOOL msgPromptForStopNIfTouchOrdType;
@property (nonatomic, strong) NSString *byPass2FA;
@property (nonatomic, strong) NSMutableArray *deviceListFor2FA;
@property (nonatomic, strong) NSMutableArray *deviceListForSMSOTP;
@property (nonatomic) int smsOTPInterval;
@property (nonatomic, strong) RDSData *rdsData;
@property (nonatomic, retain) DeviceDataFor2FA *dvData2FA;
@property (nonatomic, strong) NSString *Register2FAPgURL;
@property (nonatomic, strong) NSString *smsOTPHeaderKey;
@property (nonatomic) int currentOTPView;

// Language
@property (nonatomic) BOOL isSupportedMultipleLanguage;

// QCServer
@property (nonatomic, strong) NSString *qcServer;
@property (nonatomic, strong) NSString *tempQcServer;

// Filter Market
@property (nonatomic, strong) NSArray *filterShowMarket;

@property (nonatomic, assign) int pointerDecimal;

@property (nonatomic, strong) NSString *knowledgeURL;
@property (nonatomic, strong) NSString *knowledgeTitle;

@property (nonatomic, strong) NSString *accountID;
@property (nonatomic, strong) NSString *userParamQC;
@property (nonatomic, strong) NSString *tempUserParamQC;
@property (nonatomic, strong) NSMutableArray *elasticCat;
@property (nonatomic, strong) NSMutableArray *allNewsResultList;
@property (nonatomic) BOOL isQCCompression;

// Trading Limit
@property (nonatomic, assign) BOOL isShowTradingLimit;
@property (nonatomic, assign) BOOL isShowMarketStreamer;
@property (nonatomic, assign) BOOL isHideAccountNameAtTradeView;

// Portfolio
@property (nonatomic, assign) int lastPFPage;

// Annnoucement
@property (nonatomic, assign) BOOL isBrokerAnnoucement;
@property (nonatomic, strong) NSString *brokerAnnoucement;

//TRXFormula
@property (nonatomic, strong) NSMutableDictionary *trxFeeFormulaDict;
@property (nonatomic, assign) BOOL isEnabledTRXFee;

//Filter
@property (nonatomic, strong) NSDictionary *filterOrderBook;
+ (UserPrefConstants *) singleton;
- (NSString *)dataFormatingNumber:(int)number;
-(NSString*)abbreviatePrice:(double)price;
-(NSString *)abbreviateNumber:(long long)num;
- (NSString *) floatToString:(double) val;
- (void) playClickSoundEffect;
-(BOOL)isFloatZero:(double)floatNum;
-(NSString*)encryptTime;
- (NSString *) getBoardStatus:(NSString *)statusChar;
-(NSString*)getLogicalDate:(NSString*)dateStr;
-(void)overrideChartSettings;
- (void)showLocalReleaseNotesInView:(UIView*)view;
- (void)showRemoteReleaseNotesInView:(UIView*)view;
-(void)popMessage:(NSString*)message inController:(UIViewController*)view withTitle:(NSString*)title;
-(NSString *)stringByStrippingHTML:(NSString*)toStrip;
-(NSString*)stripDelayedSymbol:(NSString*)code;
-(NSString*)getOrderSourceStr:(NSString *)orderSrcCode;
+(long)checkAuthenticateByTouchId;
-(NSMutableArray*)stringsArrayBetweenString:(NSString*)start andString:(NSString*)end fromString:(NSString *)originalString;
- (NSString *) stringsBetweenString:(NSString*)start andString:(NSString*)end fromString:(NSString *)originalString;
+ (NSString *) getIDSSStatus:(NSString *)statusChar;
- (NSString *)urlencodedString:(NSString *)input;
@end
