//
//  TCService.m
//  TCMobile
//
//  Created by Kaka on 9/7/17.
//  Copyright © 2017 MMS. All rights reserved.
//

#ifndef AppColor_h
#define AppColor_h


#endif /* AppColor_h */

//#define kNavigationBarTintColor								RGB(24, 24, 24)
//#define AppColor_Nav_Background_Color						RGB(12, 60, 76)
//
//#define AppColor_MainAppTintColor                       RGB(12, 60, 76)
//#define AppColor_LightGray_Font_Color					RGB(170, 170, 170)
//#define AppColor_TabbarMainAppTintColor                 RGB(255, 147, 52)
//#define AppColor_MainAppBackgroundColor                 RGB(217, 181, 147)
//
//#define AppColor_TintBorderColor						RGB(51, 51, 51)
//
////Text Color
//#define App_Color_MainTextBoldColor						[UIColor blackColor]
//#define AppColor_MainTextColor                          RGB(102, 102, 102)
//#define AppColor_HightLightTextColor					RGB(255, 150, 0)

#define AppColor_TintColor								RGB(0, 186, 216)
#define AppColor_LoginTintColor							RGB(226, 85, 69) //COLOR_FROM_HEX(0xE25545)
#define AppColor_MainBackgroundColor                    RGB(66, 90, 108)
#define AppColor_TabbarBackgroundColor                  RGB(56, 80, 98)

//TextColor
#define AppColor_MainTextColor                          	RGB(230, 231, 232)
#define AppColor_HighlightTextColor                     	RGB(101, 132, 151)
#define AppColor_PriceUp_TextColor                       	RGB(0, 255, 0)
#define AppColor_PriceDown_TextColor                     	RGB(255, 0, 0)
#define AppColor_PriceUnChanged_TextColor   				TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)

#define AppColor_NormalTitleColor  			RGB(68, 91, 110)
#define AppColor_HighlightTitleColor  		RGB(0, 186, 216)

#define AppColor_StatusDataColor		                    RGB(191, 61, 141)

//Cell Color
#define AppColor_MainBackgroundCellColor		           	RGB(55, 77, 90)

//Note Pie Chart Color
#define AppColor_Untrade_PieChart_Color		COLOR_FROM_HEX(0x66C0C3)
#define AppColor_Unchange_PieChart_Color	COLOR_FROM_HEX(0x009EAB)
#define AppColor_Up_PieChart_Color			COLOR_FROM_HEX(0x44B891)
#define AppColor_Down_PieChart_Color		COLOR_FROM_HEX(0xF26522)

//BaseLine Color
#define kAppColor_BaseLine_Color			RGB(99, 132, 149) //638495


// Border for button cancel
#define AppColor_BorderForCancelButton              RGB(233, 79, 81)
#define UIColorFromHex(hex) [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0 green:((float)((hex & 0xFF00) >> 8))/255.0 blue:((float)(hex & 0xFF))/255.0 alpha:1.0]
// Common Colors
#define CommonColor_Red UIColorFromHex(0xf05253)
#define CommonColor_Orange UIColorFromHex(0xfe9d3e)
#define CommonColor_Blue UIColorFromHex(0x129cdb)
#define CommonColor_DarkBlue UIColorFromHex(0x0380ba)
#define CommonColor_Purple UIColorFromHex(0x7c2691)
#define CommonColor_Green UIColorFromHex(0x22b291)
#define CommonColor_LightGreen UIColorFromHex(0x6fddd2)
#define CommonColor_Gray UIColorFromHex(0xb3b8be)
#define CommonColor_LightGray UIColorFromHex(0xcdd1da)
#define CommonColor_GreenTimeKeeping UIColorFromHex(0x449F01)







