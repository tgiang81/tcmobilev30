//
//  ErrorCodeConstants.m
//  TCiPhone_CIMB
//
//  Created by Don Lee on 1/11/13.
//
//

#import "ErrorCodeConstants.h"

@implementation ErrorCodeConstants

+ (NSString *) Default_Advice {
    return @"Please try again later. If problem persists, please contact our support for further assistance.";
}

+ (NSString *) Login_Process_Failure {
    return @"Login - Process Failure.";
}

+ (NSString *) Login_Connection_Failure {
    return @"Login - Connection Failure.";
}

+ (NSString *) ATP_TradeGetE2EEParams_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1001 - ATP %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) ATP_TradeGetPK_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1002 - ATP %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) ATP_TradeGetKey2_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1003 - ATP %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) ATP_TradeGetSession2_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1004 - ATP %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) ATP_TradeGetSession_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1005 - ATP %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) ATP_TradeLogin_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1006 - ATP %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) ATP_TradeClient_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1007 - ATP %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) ATP_TradeChgPasswd_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1008 - ATP Change Password - %@", completion ? @"Process Failure." : @"Connection Failure."];
}

+ (NSString *) ATP_TradeChgPin_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1009 - ATP Change Pin - %@", completion ? @"Process Failure." : @"Connection Failure."];
}

+ (NSString *) ATP_TradeTicket_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_1010 - ATP Order Submission - %@", completion ? @"Process Failure." : @"Connection Failure."];
}

+ (NSString *) LMS_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_2001 - LMS %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) QC_GetKey_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_3001 - QC %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) QC_Login_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_3002 - QC %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) QC_SectorFast_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_3003 - QC %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}

+ (NSString *) QC_GetNewKey_Fail:(BOOL)completion {
    return [NSString stringWithFormat:@"ERROR_3004 - QC %@ %@", completion ? [self Login_Process_Failure] : [self Login_Connection_Failure], [self Default_Advice]];
}


@end
