//
//  AppIcons.h
//  TCiPad
//
//  Created by Kaka on 5/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#ifndef AppIcons_h
#define AppIcons_h

#endif /* AppIcons_h */


//Define icon name
#define AppIcon_Up_Price			@"up_price_icon"
#define AppIcon_Down_Price			@"down_price_icon"
#define AppIcon_UnChange_Price		@"unchange_price_icon"
