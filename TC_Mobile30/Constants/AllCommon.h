//
//  AllCommon.h
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#ifndef AllCommon_h
#define AllCommon_h
#import "AppMacro.h"
#import "AppControl.h"
#import "AppDelegate.h"
#import "LanguageManager.h"
#import "UserSession.h"
#import "QCConstants.h"
#import "AppConstants.h"
#import "AppFont.h"
#import "AppColor.h"
#import "AppIcons.h"
#import "SettingManager.h"
#import "BrokerManager.h"
#import "ThemeManager.h"
#import "HexColors.h"
#import "IQKeyboardManager.h"
#import "NetworkManager.h"
#import "BrokerManager.h"

typedef void (^callBackValue) (NSInteger index, NSDictionary *value);

#endif /* AllCommon_h */
