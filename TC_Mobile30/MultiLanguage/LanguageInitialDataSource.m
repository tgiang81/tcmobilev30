//
//  LanguageInitialDataSource.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 07/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "LanguageInitialDataSource.h"

@implementation LanguageInitialDataSource

// If we can bring this to LMS will be great.

- (NSArray *)supportedLanguages
{
    return @[];
}
-(NSArray *)supportedLanguageName
{
    return @[];
}

- (NSString *)defaultLanguage
{
    return @"en";
}

- (NSDictionary *)stringsForLanguage:(NSString *)language
{
    return @{};
}


@end
