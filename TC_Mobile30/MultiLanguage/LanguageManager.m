//
//  LanguageManager.m
//  TCiPad
//
//  Created by n2n on 06/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "LanguageManager.h"
#import "LanguageOneJSONDataSource.h"
#import "LanguageInitialDataSource.h"

@implementation LanguageManager{
    id<LanguageDataSource> _dataSource;
    NSString * _language;
    NSDictionary * _cachedStrings;
}



+ (LanguageManager *)defaultManager
{
    __strong static id _sharedObject = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
    
}
+ (NSString *) getCurrentLanguage{
    
    NSString *len = [[NSUserDefaults standardUserDefaults] stringForKey:N2NLOCALIZATION_KEY];
    DLog(@"getCurrentLanguage %@",len);
    
    if (len.length==0) {
        len = [[LanguageManager defaultManager] getInitialLanguage];
    }
    
    return len;
}
-(NSArray*)supportedLanguages {
   // NSLog(@"supported Language %@",[self.dataSource supportedLanguages]);
    return [self.dataSource supportedLanguages];
}
-(NSArray *)supportedLanguageName{
    return [self.dataSource supportedLanguagesName];
}

#pragma mark - Data source

- (void)setDataSource:(id<LanguageDataSource>)dataSource
{
    _dataSource = dataSource;
    [self reloadStrings];
}

- (id) init {
    self = [super init];
    if (self) {
        
        self.dataSource = [LanguageInitialDataSource new];


    }
    return self;
}

#pragma mark - Loading
+ (void)loadFromJSONFile:(NSString *)JSONFilePath defaultLanguage:(NSString *)defaultLanguage
{
    NSURL * URL = [NSURL fileURLWithPath:JSONFilePath];
    return [self loadFromURL:URL defaultLanguage:[[LanguageManager defaultManager] getInitialLanguage]];
}

+ (void)loadFromURL:(NSURL *)JSONFileURL defaultLanguage:(NSString *)defaultLanguage
{
    [self defaultManager].dataSource = [[LanguageOneJSONDataSource alloc] initWithURL:JSONFileURL defaultLanguage:defaultLanguage];
}



#pragma mark - Language

- (NSString *)checkLanguage:(NSString *)language
{
    NSMutableArray * preferences = [NSMutableArray new];
    if (language) {
        [preferences addObject:language];
    }
    [preferences addObjectsFromArray:[NSLocale preferredLanguages]];
    if (self.dataSource) {
        [preferences addObject:[self.dataSource defaultLanguage]];
    }
    return [NSBundle preferredLocalizationsFromArray:self.supportedLanguages forPreferences:preferences].firstObject;
}

// GET
- (NSString *)getInitialLanguage
{
   // NSLog(@"Set Language %@",_language);
    if (!_language) {
        NSString *preferredLanguage = [[NSUserDefaults standardUserDefaults] stringForKey:N2NLOCALIZATION_KEY];
        
        if ([preferredLanguage length]<=0) {
            preferredLanguage = [self checkLanguage:preferredLanguage];
        } else {
            ; // do nothing
        }
        
        _language = preferredLanguage;
        
       // [self setLanguage:preferredLanguage];
    }
    return _language;
}

// SET
- (void)setLanguage:(NSString *)language
{
    
    NSString * checkLanguageSame = [self checkLanguage:language];
    
   //  NSLog(@"checkLanguageSame Language %@=%@",checkLanguageSame,_language);
    if (![checkLanguageSame isEqualToString:_language]) {
        if ([self.supportedLanguages indexOfObject:checkLanguageSame] != NSNotFound) {
            _language = [checkLanguageSame copy];
        } else {
            _language = nil;
        }
        
       // NSLog(@"Set Language %@",_language);
        
        [self reloadStrings];
        
        [[NSUserDefaults standardUserDefaults] setObject:_language forKey:N2NLOCALIZATION_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)reloadStrings
{
    _cachedStrings = nil;
    
    if (_language && _dataSource && [_dataSource.supportedLanguages indexOfObject:_language] != NSNotFound ) {
        _cachedStrings = [_dataSource stringsForLanguage:_language];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLanguageDidChangeNotification object:nil];
}


- (NSDictionary *)stringsForLanguage:(NSString *)language
{
    if ([language isEqualToString:self.language]) {
        return _cachedStrings;
    }
    
    return [self.dataSource stringsForLanguage:language];
}

- (NSString *)getLanguage:(NSString *)language andKey:(NSString *)key{
     return [self.dataSource getLanguageByKey:language andKey:key];
}

- (NSString *)stringForKey:(NSString *)key language:(NSString *)language
{
    NSDictionary * langugeStrings = [self stringsForLanguage:language];
    
    NSObject * lookupResult = langugeStrings[key];
    NSString * string = nil;
    if ([lookupResult isKindOfClass:NSString.class]) {
        string = (NSString *)lookupResult;
    } else if ([lookupResult isKindOfClass:NSNumber.class]) {
        string = [(NSNumber *)lookupResult stringValue];
    }
    
    if (!string) {
        if (self.noKeyPlaceholder) {
            string = self.noKeyPlaceholder;
            string = [string stringByReplacingOccurrencesOfString:@"{key}" withString:key];
            string = [string stringByReplacingOccurrencesOfString:@"{language}" withString:language];
        }
#if DEBUG
        NSLog(@"LanguageManager: no string for key %@ in language %@", key, language);
#endif
    }
    
    return string;
}

- (NSString *)stringForKey:(NSString *)key
{
    
    //NSLog(@"self.language %@",self.language);
    return [self stringForKey:key language:self.language];
}

+ (NSString *)stringForKey:(NSString *)key
{
    return [[LanguageManager defaultManager] stringForKey:key];
}

+ (NSString *)getLanguage:(NSString *)language andKey:(NSString *)key
{
    return [[LanguageManager defaultManager] getLanguage:language andKey:key];
}

+(NSArray *)getSupportLanguage
{
    return[[LanguageManager defaultManager]supportedLanguages ];
}
+(NSArray *)getNameSupportLanguage
{
    return [[LanguageManager defaultManager]supportedLanguageName];
}

+ (NSString *)stringForKey:(NSString *)key withPlaceholders:(NSDictionary *)placeholders
{
    return [[LanguageManager defaultManager] stringForKey:key withPlaceholders:placeholders];
}

- (NSString *)stringForKey:(NSString *)localizationKey withPlaceholders:(NSDictionary *)placeholders
{
    __block NSString * result = [self stringForKey:localizationKey];
    
    if (result) {
        [placeholders enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if ([key isKindOfClass:NSString.class] && [obj isKindOfClass:NSString.class]) {
                result = [result stringByReplacingOccurrencesOfString:key withString:obj];
            }
        }];
    }
    
    return result;
}


//
// this method is used to store "keys" in accessibleIdentifier property so when the object texts
// are changed, the original text of the label which is in english is still stored and can
// be used as keys to retrieve other languages.

-(void)applyAccessibilityId:(UIViewController*)vc {
    
    // accessibility id only apply the EN language as ids
    NSDictionary *wordsDict = [self.dataSource stringsForLanguage:@"en"]; // <-- dont change! "en" is our master key
    
    //NSDictionary *wordsDict = [_languageDict objectForKey:@"en"];
    NSArray *strList = [wordsDict allKeys];
    
    for(UIView *v in [vc.view allSubViews])
    {
        
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *lbl = (UILabel*)v;
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                if ([key isEqualToString:lbl.text]) lbl.accessibilityIdentifier = [wordsDict objectForKey:key];
                
            }];
            
        }
        
        if([v isKindOfClass:[UITextField class]])
        {
            UITextField *lbl = (UITextField*)v;
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                if ([key isEqualToString:lbl.placeholder]) lbl.accessibilityIdentifier = [wordsDict objectForKey:key];
                
            }];
        }
        
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *lbl = (UIButton*)v;
            
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                if ([key isEqualToString:lbl.titleLabel.text])
                    lbl.accessibilityIdentifier = [wordsDict objectForKey:key];
            }];
        }
        
        if([v isKindOfClass:[UITextView class]])
        {
            UITextView *lbl = (UITextView*)v;
            // for textview, we only add some characters as key
            // thus we search by substring
            
            __block NSString *subText = @"";
            
            if ([lbl.text length]>=35) {
                subText = [lbl.text substringToIndex:36];
                // NSLog(@"subText %@",subText);
            }
            
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                if ([subText isEqualToString:key]) {
                    lbl.accessibilityIdentifier = key;
                }
            }];
        }
        
        // may add images name too in future if needed
    }
    
}

// you can use this method to translate views that are reusable (for example custom cells
// that was created in storyboard). make sure you manually set the accessibilityIdentifier
// for each of the items in reusable views and it should be the same as the text in english.

-(void)translateStringsForView:(UIView*)view {
    NSDictionary *wordsDict = [self.dataSource stringsForLanguage:_language];
    
    // NSLog(@"_language %@",_language);
    // NSLog(@"wordsDict %@",wordsDict);
    NSArray *strList = [wordsDict allKeys];
    
    for(UIView *v in [view allSubViews])
    {
        
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *lbl = (UILabel*)v;
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                if ([key isEqualToString:lbl.accessibilityIdentifier]) lbl.text = [wordsDict objectForKey:key];
                
            }];
        }
        
        // also need textfield placeholder
        if([v isKindOfClass:[UITextField class]])
        {
            UITextField *lbl = (UITextField*)v;
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                if ([key isEqualToString:lbl.accessibilityIdentifier]) lbl.placeholder = [wordsDict objectForKey:key];
                
            }];
        }
        
        
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *lbl = (UIButton*)v;
            // NSLog(@"button key: %@",lbl.accessibilityIdentifier);
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                
                if ([key isEqualToString:lbl.accessibilityIdentifier])
                    [lbl setTitle:[wordsDict objectForKey:key] forState:UIControlStateNormal];
            }];
        }
        
        if([v isKindOfClass:[UITextView class]])
        {
            UITextView *lbl = (UITextView*)v;
            [strList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL*stop) {
                NSString *key = (NSString *)obj;
                
                if ([key isEqualToString:lbl.accessibilityIdentifier])
                    [lbl setText:[wordsDict objectForKey:key]];
            }];
        }
    }
}

// This method is to translate every single Button/Label/Textview available in a scene without using
// IBOutlets. It make use of UIView+Recursion category to iterate every single views available in
// a UIViewController (however I have modified the category so that it does not iterate UIContainerView
// because we want to limit every view to its own viewcontroller)

-(void)translateStringsForViewController:(UIViewController*)vc {
    
    [self translateStringsForView:vc.view];
    
}


#pragma mark - Helper to get strings in viewcontroller

// call this in viewDidload to get all static strings available in viewController


-(void)printOutStringsInViewController:(UIViewController*)vc {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    for(UIView *v in [vc.view allSubViews])
    {
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *lbl = (UILabel*)v;
            if ([lbl.text length]>0)[dict setObject:lbl.text forKey:lbl.text];
        }
        
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *lbl = (UIButton*)v;
            if ([lbl.titleLabel.text length]>0) [dict setObject:lbl.titleLabel.text forKey:lbl.titleLabel.text];
        }
        
        if([v isKindOfClass:[UITextView class]])
        {
            UITextView *lbl = (UITextView*)v;
            if ([lbl.text length]>0)[dict setObject:lbl.text forKey:lbl.text];
        }
    }
    
    NSLog(@"STRINGS: %@",dict);
}


@end
