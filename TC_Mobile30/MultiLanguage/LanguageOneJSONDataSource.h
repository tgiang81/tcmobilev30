//
//  LanguageOneJSONDataSource.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 07/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LanguageDataSource.h"

@interface LanguageOneJSONDataSource : NSObject <LanguageDataSource>

- (id)initWithURL:(NSURL *)URL defaultLanguage:(NSString *)defaultLanguage;
@end
