//
//  LanguageOneJSONDataSource.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 07/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "LanguageOneJSONDataSource.h"

@implementation LanguageOneJSONDataSource{
    NSDictionary * _strings;
    NSString * _defaultLanguage;
    NSMutableArray * _supportedLanguages;
     NSMutableArray * _supportedLanguageName;
}

- (id)initWithStrings:(NSDictionary *)strings defaultLanguage:(NSString *)defaultLanguage {
    self = [super init];
    
    if (self) {
        _strings = strings;
        NSArray *arrKeys=[_strings.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        _supportedLanguages=[[NSMutableArray alloc] init];
        _supportedLanguageName=[[NSMutableArray alloc] init];
        for (NSString *key in arrKeys) {
            NSDictionary *lang=_strings[key];
            BOOL enable = [lang[@"isEnable"] boolValue];
            NSString *languageKey=lang[@"languageKey"];
            NSString *languageName=lang[@"languageName"];
            if (enable) {
                [_supportedLanguages addObject:languageKey];
                [_supportedLanguageName addObject:languageName];
            }
        }
         _defaultLanguage = [defaultLanguage copy];
    }
    
    return self;
}

- (id)initWithURL:(NSURL *)URL defaultLanguage:(NSString *)defaultLanguage
{
    NSData * JSONData = [NSData dataWithContentsOfURL:URL];
    _strings = [NSJSONSerialization JSONObjectWithData:JSONData options:0 error:nil];
    _defaultLanguage = defaultLanguage;
    
    return [self initWithStrings:_strings defaultLanguage:_defaultLanguage];
}

- (NSArray *)supportedLanguages
{
    return _supportedLanguages;
}
-(NSArray *)supportedLanguagesName
{
    return _supportedLanguageName;
}

- (NSString *)getLanguageByKey:(NSString *)language andKey:(NSString *)key
{
    NSString *returnLanguage = @"";
  
    returnLanguage = [[_strings objectForKey:language] objectForKey:key];
    
    return returnLanguage;
}

- (NSString *)defaultLanguage
{
    return _defaultLanguage;
}

- (NSDictionary *)stringsForLanguage:(NSString *)language
{
    NSDictionary *dictLang;
    NSDictionary *dictTemp=_strings[language];
    NSString*key=@"keys";
    if (dictTemp!=nil) {
        dictLang =dictTemp[key];
    }
    return dictLang;
}

@end
