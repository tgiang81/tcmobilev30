//
//  UIView+Recursion.m
//  TCiPad
//
//  Created by n2n on 06/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "UIView+Recursion.h"

// UIView+viewRecursion.m
@implementation UIView (viewRecursion)
- (NSMutableArray*)allSubViews
{
 
    NSMutableArray *arr= [[NSMutableArray alloc] init];
    [arr addObject:self];
    for (UIView *subview in self.subviews)
    {
        // set all UIContainerView tag to 10101 (we dont want iterate inside COntainerView)
        // we also avoid traversing inside SegmentedControl (there is a label in there and
        // label uses attributed text)
        if ((subview.tag==10101)||([subview isKindOfClass:[UISegmentedControl class]])) {
            
        } else {
            [arr addObjectsFromArray:(NSArray*)[subview allSubViews]];
        }
    }
    return arr;
}

@end
