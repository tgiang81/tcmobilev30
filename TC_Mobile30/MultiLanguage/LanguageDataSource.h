//
//  LanguageDataSource.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 07/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LanguageDataSource <NSObject>

- (NSArray *)supportedLanguages;
- (NSArray *)supportedLanguagesName;
- (NSString *)defaultLanguage;
- (NSDictionary *)stringsForLanguage:(NSString *)language;
@optional
- (NSString *)getLanguageByKey:(NSString *)language andKey:(NSString *)key;
@end
