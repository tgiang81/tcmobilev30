//
//  LanguageManager.h
//  TCiPad
//
//  Created by n2n on 06/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIView+Recursion.h"
#import "UserPrefConstants.h"
#import "LanguageDataSource.h"

#define kLanguageDidChangeNotification @"kLanguageDidChangeNotification"
#define N2NLOCALIZATION_KEY @"N2NLOCALIZATION_KEY"
/*
 
 USAGE -
 1. init in AppDelegate
 2. apply in each VC
 
 1. Init
 [LanguageManager loadFromJSONFile:[[NSBundle mainBundle] pathForResource:@"language.json" ofType:nil] defaultLanguage:@"en"];
 [LanguageManager defaultManager].noKeyPlaceholder = @"[No '{key}' in '{language}']";

 
 Verify the language.json file content in online tool
 for example: https://jsonformatter.curiousconcept.com/
 It should be free from errors.
 
 2. Apply
 In each VC, in viewDidload or ViewDidLayoutSubviews (depending on how all objects were created),
 
 [LanguageManager defaultManager] applyAccessibilityId:self]; <- execute AFTER all objects are created
 
 [LanguageManager defaultManager] translateStringsForViewController:self]; <-- execute in viewDidAppear
 
 [LanguageManager defaultManager] translateStringsForView:cell.contentView]; <-- execute in reusable delegates (cellForRow etc).
 
 
 */

@interface LanguageManager : NSObject{

}

@property (nonatomic, retain) id<LanguageDataSource>dataSource;
@property (nonatomic, strong) NSDictionary *languageDict;
@property (nonatomic, readonly) NSArray * supportedLanguages;
@property (nonatomic, readonly) NSString * systemLanguage;
@property (nonatomic, copy) NSString * noKeyPlaceholder;
@property (nonatomic, copy) NSString * language;

-(id)init;
+ (LanguageManager *)defaultManager;

// Methods
-(void)applyAccessibilityId:(UIViewController*)vc;
-(void)printOutStringsInViewController:(UIViewController*)vc;
-(NSArray*)supportedLanguages;
-(NSArray*)supportedLanguageName;
-(void)translateStringsForViewController:(UIViewController*)vc;
-(void)translateStringsForView:(UIView*)view; 
+ (NSString *) getCurrentLanguage;
- (NSString *)getInitialLanguage;
+ (void)loadFromURL:(NSURL *)JSONFileURL defaultLanguage:(NSString *)defaultLanguage;
- (void)reloadStrings;
+ (NSString *)getLanguage:(NSString *)language andKey:(NSString *)key;
- (NSDictionary *)stringsForLanguage:(NSString *)language;
- (NSString *)stringForKey:(NSString *)key language:(NSString *)language;
+ (void)loadFromJSONFile:(NSString *)fileName defaultLanguage:(NSString *)defaultLanguage;
+ (NSString *)stringForKey:(NSString *)key;
+ (NSString *)stringForKey:(NSString *)key withPlaceholders:(NSDictionary *)placeholders;
+ (NSArray *)getSupportLanguage;//Get all language isEnable=True
+ (NSArray *)getNameSupportLanguage;//Get all languageName is suppored
@end
