//
//  ExchangeData.h
//  N2NTrader
//
//  Created by Adrian Lo on 5/13/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"

@interface ExchangeData : TCBaseModel {
//	int equityOrDer;
//    NSString *feed_exchg_code;
//	NSString *trade_exchange_code;
//	NSString *exchange_name;
//    NSString *exchange_description;
//	NSString *exchange_qc_server;
//	NSString *currency;
//    NSString *exchange_type;
//    BOOL subsCPIQCompSynp;
//    BOOL subsCPIQCompInfo;
//    BOOL subsCPIQAnnounce;
//    BOOL subsCPIQCompKeyPer;
//    BOOL subsCPIQShrHoldSum;
//    BOOL subsCPIQFinancialRep;
//    NSString *mktDepthLevel;
//    BOOL MDLfeature;
}
@property (nonatomic, assign) int equityOrDer;
@property (nonatomic, strong) NSString *feed_exchg_code;
@property (nonatomic, strong) NSString *trade_exchange_code;
@property (nonatomic, strong) NSString *exchange_name;
@property (nonatomic, strong) NSString *exchange_description;
@property (nonatomic, strong) NSString *exchange_qc_server;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *exchange_type;
@property (nonatomic, assign) BOOL subsCPIQCompSynp;
@property (nonatomic, assign) BOOL subsCPIQCompInfo;
@property (nonatomic, assign) BOOL subsCPIQAnnounce;
@property (nonatomic, assign) BOOL subsCPIQCompKeyPer;
@property (nonatomic, assign) BOOL subsCPIQShrHoldSum;
@property (nonatomic, assign) BOOL subsCPIQFinancialRep;
@property (nonatomic, strong) NSString *mktDepthLevel;
@property (nonatomic, assign) BOOL MDLfeature; // flag to indicate MDL (0=MDL/not override, 1=NON-MDL/override)
@end
