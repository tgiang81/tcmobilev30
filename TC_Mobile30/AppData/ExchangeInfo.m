//
//  ExchangeInfo.m
//  TCiPad
//
//  Created by n2n on 14/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "ExchangeInfo.h"

@implementation ExchangeInfo

- (instancetype)init {
    self = [super init];
    if (self) {
        self.qcParams = [[QCParams alloc] init];
    }
    return self;
}


// to enable NSLog to work on array of this object (this is cool trick).
-(NSString *)description{
    
    return [NSString stringWithFormat:
            @"<ExchangeInfo>ExchCode: %@, indicesCode: %@, qcParams: %@, marketInfos: %@, sectorInfos: %@, sectorParents: %@",
            _exchCode, _indicesCode, _qcParams, _marketInfos, _sectorInfos, _sectorParents];
	
}


@end
