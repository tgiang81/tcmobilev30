//
//  QCParams.m
//  TCiPad
//
//  Created by n2n on 14/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "QCParams.h"

@implementation QCParams

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}



-(NSString*)description {
    
    NSString *result;
    
    result = [NSString stringWithFormat:
              @"<QCParams>\n indices:%@\n encryption:%@\n qcType:%@\n exDate:%@\n timeSync:%@\n exchgDate:%@\n"
              @"fullModeExchange:%@\n sector:%@\n session:%@\n unitOrLot:%@\n userParams:%@, aliveTimeOut:%@"
              @"transactWithBrokerID:%@\n exchgInfo:%@\n exchange:%@\n version:%@\n frontEndLoadLocalData:%@\n"
              ,_indices, _encryption, _qcType, _exDate, _timeSync, _exchgDate, _fullModeExchange,
              _sector, _session, _unitOrLot, _userParams, _aliveTimeOut, _transactWithBrokerID,
              _exchgInfo, _exchange, _version, _frontEndLoadLocalData];
    
    return result;
    
}


@end
