//
//  SectorInfo.m
//  TCiPad
//
//  Created by n2n on 12/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "SectorInfo.h"

@implementation SectorInfo

- (instancetype)init {
    self = [super init];
    if (self) {
        self.subItemsArray = @[].mutableCopy;
    }
    return self;
}


/*
 @property (nonatomic, strong) NSString *sectorName;
 @property (nonatomic, strong) NSString *sectorCode;
 @property (nonatomic, strong) NSMutableArray *subItemsArray;
 */
// to enable NSLog to work on array of this object (this is cool trick).
-(NSString *)description{
    
    return [NSString stringWithFormat:
            @"<SectorInfo> sectorName: %@, sectorCode: %@, subItemsArray: %@",
            _sectorName, _sectorCode, _subItemsArray];
    
    
}


@end
