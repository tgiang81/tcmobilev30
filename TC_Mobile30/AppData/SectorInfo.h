//
//  SectorInfo.h
//  TCiPad
//
//  Created by n2n on 12/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"

@interface SectorInfo : TCBaseModel

@property (nonatomic, strong) NSString *sectorName;
@property (nonatomic, strong) NSString *sectorCode;
@property (nonatomic, strong) NSMutableArray *subItemsArray;

- (id)init;

@end
