//
//  QCData.h
//  TCPlusUniversal
//
//  Created by Scott Thoo on 3/21/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QCData : NSObject
//Vertx
@property (nonatomic, strong) NSMutableDictionary *qcFeedDataDict;      // Major Feed Data store and modify here. Eg:QuoteScreen, Stock Detail and they update constantsly (Real time)
@property (nonatomic, strong) NSMutableDictionary *qcScoreBoardDict;    // Just for ScoreBoard. Update less offen.
@property (nonatomic, strong) NSMutableDictionary *qcIndicesDict;       // Just for Indices. Update less offen.
@property (nonatomic, strong) NSMutableDictionary *qcKlciDict;          // Just for KLCI. Start getting after login, will stay global and notify globally.
@property (nonatomic, strong) NSMutableDictionary *qcTickerDict;

+(QCData *) singleton;
-(void)removeAllQcFeedExceptThese:(NSArray*)stockKeys;
-(void)clearAllData;

@end
