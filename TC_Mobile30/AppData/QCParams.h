//
//  QCParams.h
//  TCiPad
//
//  Created by n2n on 14/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"

@interface QCParams : TCBaseModel

@property (nonatomic, strong) NSArray *indices;
@property (nonatomic, strong) NSArray *encryption;
@property (nonatomic, strong) NSArray *qcType;
@property (nonatomic, strong) NSArray *exDate;
@property (nonatomic, strong) NSArray *timeSync;
@property (nonatomic, strong) NSArray *exchgDate;
@property (nonatomic, strong) NSArray *fullModeExchange;
@property (nonatomic, strong) NSArray *sector;
@property (nonatomic, strong) NSArray *session;
@property (nonatomic, strong) NSArray *unitOrLot;
@property (nonatomic, strong) NSArray *userParams;
@property (nonatomic, strong) NSArray *aliveTimeOut;
@property (nonatomic, strong) NSArray *transactWithBrokerID;
@property (nonatomic, strong) NSMutableArray *exchgInfo;
@property (nonatomic, strong) NSArray *exchange;
@property (nonatomic, strong) NSArray *version;
@property (nonatomic, strong) NSArray *frontEndLoadLocalData;


@end
