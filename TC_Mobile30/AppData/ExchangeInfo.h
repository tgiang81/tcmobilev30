//
//  ExchangeInfo.h
//  TCiPad
//
//  Created by n2n on 14/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

/*
 Object to hold Individual Exchange Information from Vertx
 (vertxGetExchangeInfo)
 */

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"
#import "QCParams.h"

@interface ExchangeInfo : TCBaseModel

@property (nonatomic, strong) NSString *exchCode;
@property (nonatomic, strong) NSString *indicesCode;
@property (nonatomic, strong) QCParams *qcParams;
@property (nonatomic, strong) NSDictionary *marketInfos;
@property (nonatomic, strong) NSDictionary *sectorInfos;
@property (nonatomic, strong) NSDictionary *sectorParents;

@end
