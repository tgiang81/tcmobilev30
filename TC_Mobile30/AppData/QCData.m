//
//  QCData.m
//  TCPlusUniversal
//
//  Created by Scott Thoo on 3/21/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "QCData.h"

@implementation QCData
//@synthesize qcFeedDataDict;
//@synthesize qcScoreBoardDict;
//@synthesize qcIndicesDict;
//@synthesize qcKlciDict;
/*
 Comment Jul 2017: Should use Object really.
 
 //Structure of an QC Dict
 KEY                 Value                Key         Value
 <<StockCODE>> --- <<NSDictionary>>  ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 ------ <<FID>> --- <<VALUE>>
 
 //Example :
 
 <<0017.KL>>------<<NSDICT>>--------------<<100>>------<<31003000>>
 */

+ (QCData *)singleton
{
	static QCData *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[QCData alloc] init];
		
	});
	return _instance;
}

- (instancetype)init{
	if(self = [super init]){
		_qcFeedDataDict    = [NSMutableDictionary dictionary];
		_qcScoreBoardDict  = [NSMutableDictionary dictionary];
		_qcIndicesDict     = [NSMutableDictionary dictionary];
		_qcKlciDict        = [NSMutableDictionary dictionary];
	}
	return self;
}

- (void)clearAllData {
	_qcFeedDataDict    = [NSMutableDictionary dictionary];
	_qcScoreBoardDict  = [NSMutableDictionary dictionary];
	_qcIndicesDict     = [NSMutableDictionary dictionary];
	_qcKlciDict        = [NSMutableDictionary dictionary];
}

- (void)removeAllQcFeedExceptThese:(NSArray*)stockKeys {
	
	NSMutableArray *toRemove = [[NSMutableArray alloc] init];
	
	for (NSString *key in [_qcFeedDataDict allKeys]) {
		
		if ([stockKeys containsObject:key]) {
			// do nothing ;)
		} else {
			[toRemove addObject:key];
		}
	}
	[self.qcFeedDataDict removeObjectsForKeys:toRemove];
	
}

@end


