{
	"alertType_065" : [ {
		"key" : "98",
		"value" : "Stock Price"
	}, {
		"key" : "101",
		"value" : "Stock Volume"
	}, {
		"key" : "68",
		"value" : "Bid Price"
	}, {
		"key" : "88",
		"value" : "Ask Price"
	}, {
		"key" : "58",
		"value" : "Bid Volume"
	}, {
		"key" : "78",
		"value" : "Ask Volume"
	}, {
		"key" : "257",
		"value" : "In Queue"
	}, {
		"key" : "258",
		"value" : "Fail"
	}, {
		"key" : "259",
		"value" : "Matched"
	}, {
		"key" : "260",
		"value" : "Corporate Action"
	}, {
		"key" : "261",
		"value" : "Outstanding Contract"
	} ],
	"compareType_065" : [ {
		"key" : "0",
		"value" : "Absolute Value"
	}, {
		"key" : "1",
		"value" : "% Change"
	} ],
	"condition_065" : [ {
		"key" : "2",
		"value" : ">"
	}, {
		"key" : "4",
		"value" : "<"
	}, {
		"key" : "3",
		"value" : ">="
	}, {
		"key" : "5",
		"value" : "<="
	}, {
		"key" : "1",
		"value" : "="
	} ],
	"exchange_065" : [ {
		"key" : "KL",
		"value" : "Bursa(KL)"
	} ],
	"media_065" : [ {
		"key" : "PN",
		"value" : "Push Notifications",
		"id" : "-1",
		"Sts" : "P"
	} ],
	"alertType_098" : [ {
		"key" : "98",
		"value" : "Stock Price"
	}, {
		"key" : "101",
		"value" : "Stock Volume"
	}, {
		"key" : "68",
		"value" : "Bid Price"
	}, {
		"key" : "88",
		"value" : "Ask Price"
	}, {
		"key" : "58",
		"value" : "Bid Volume"
	}, {
		"key" : "78",
		"value" : "Ask Volume"
	}, {
		"key" : "257",
		"value" : "In Queue"
	}, {
		"key" : "258",
		"value" : "Fail"
	}, {
		"key" : "259",
		"value" : "Matched"
	}, {
		"key" : "260",
		"value" : "Corporate Action"
	}, {
		"key" : "261",
		"value" : "Outstanding Contract"
	} ],
	"compareType_098" : [ {
		"key" : "0",
		"value" : "Absolute Value"
	}, {
		"key" : "1",
		"value" : "% Change"
	} ],
	"condition_098" : [ {
		"key" : "2",
		"value" : ">"
	}, {
		"key" : "4",
		"value" : "<"
	}, {
		"key" : "3",
		"value" : ">="
	}, {
		"key" : "5",
		"value" : "<="
	}, {
		"key" : "1",
		"value" : "="
	} ],
	"exchange_098" : [ {
		"key" : "MY",
		"value" : "Bursa(MY)"
	}, {
		"key" : "KLL",
		"value" : "Bursa(KLL)"
	}, {
		"key" : "KL",
		"value" : "Bursa(KL)"
	}, {
		"key" : "JK",
		"value" : "IDX"
	}, {
		"key" : "PH",
		"value" : "PSE"
	}, {
		"key" : "SG",
		"value" : "SGX"
	}, {
		"key" : "HN",
		"value" : "HSX(HN)"
	}, {
		"key" : "HC",
		"value" : "HSX(HC)"
	}, {
		"key" : "O",
		"value" : "NASDAQ"
	}, {
		"key" : "N",
		"value" : "NYSE(N)"
	}, {
		"key" : "A",
		"value" : "NYSE(A)"
	}, {
		"key" : "HK",
		"value" : "HKSE"
	}, {
		"key" : "MT",
		"value" : "CME Metal"
	}, {
		"key" : "FX",
		"value" : "Forex"
	}, {
		"key" : "ENG",
		"value" : "CME Energy"
	}, {
		"key" : "BK",
		"value" : "SET"
	} ],
    "media_2920" : [ {
        "key" : "PN",
        "value" : "Push Notifications",
        "id" : "-1",
        "Sts" : "P"
    }, {
        "key" : "S",
        "value" : "Sms",
        "id" : "2",
        "Sts" : "P"
    }, {
        "key" : "E",
        "value" : "Email",
        "id" : "1",
        "Sts" : "P"
    } ],
	"media_098" : [ {
		"key" : "PN",
		"value" : "Push Notifications",
		"id" : "-1",
		"Sts" : "P"
	}, {
		"key" : "S",
		"value" : "Sms",
		"id" : "2",
		"Sts" : "P"
	}, {
		"key" : "E",
		"value" : "Email",
		"id" : "1",
		"Sts" : "P"
	} ],
	"alertType_017" : [ {
		"key" : "98",
		"value" : "Stock Price"
	}, {
		"key" : "101",
		"value" : "Stock Volume"
	}, {
		"key" : "68",
		"value" : "Bid Price"
	}, {
		"key" : "88",
		"value" : "Ask Price"
	}, {
		"key" : "58",
		"value" : "Bid Volume"
	}, {
		"key" : "78",
		"value" : "Ask Volume"
	}, {
		"key" : "257",
		"value" : "In Queue"
	}, {
		"key" : "258",
		"value" : "Fail"
	}, {
		"key" : "259",
		"value" : "Matched"
	}, {
		"key" : "260",
		"value" : "Corporate Action"
	}, {
		"key" : "261",
		"value" : "Outstanding Contract"
	} ],
	"compareType_017" : [ {
		"key" : "0",
		"value" : "Absolute Value"
	}, {
		"key" : "1",
		"value" : "% Change"
	} ],
	"condition_017" : [ {
		"key" : "2",
		"value" : ">"
	}, {
		"key" : "4",
		"value" : "<"
	}, {
		"key" : "3",
		"value" : ">="
	}, {
		"key" : "5",
		"value" : "<="
	}, {
		"key" : "1",
		"value" : "="
	} ],
	"exchange_017" : [ {
		"key" : "SG",
		"value" : "SGX"
	} ],
	"media_017" : [ {
		"key" : "PN",
		"value" : "Push Notifications",
		"id" : "-1",
		"Sts" : "P"
	} ],
	"alertType_086" : [ {
		"key" : "98",
		"value" : "Stock Price"
	}, {
		"key" : "101",
		"value" : "Stock Volume"
	}, {
		"key" : "68",
		"value" : "Bid Price"
	}, {
		"key" : "88",
		"value" : "Ask Price"
	}, {
		"key" : "58",
		"value" : "Bid Volume"
	}, {
		"key" : "78",
		"value" : "Ask Volume"
	}, {
		"key" : "257",
		"value" : "In Queue"
	}, {
		"key" : "258",
		"value" : "Fail"
	}, {
		"key" : "259",
		"value" : "Matched"
	}, {
		"key" : "260",
		"value" : "Corporate Action"
	}, {
		"key" : "261",
		"value" : "Outstanding Contract"
	} ],
	"compareType_086" : [ {
		"key" : "0",
		"value" : "Absolute Value"
	}, {
		"key" : "1",
		"value" : "% Change"
	} ],
	"condition_086" : [ {
		"key" : "2",
		"value" : ">"
	}, {
		"key" : "4",
		"value" : "<"
	}, {
		"key" : "3",
		"value" : ">="
	}, {
		"key" : "5",
		"value" : "<="
	}, {
		"key" : "1",
		"value" : "="
	} ],
	"exchange_086" : [ {
		"key" : "MY",
		"value" : "Bursa(MY)"
	} ],
	"media_086" : [ {
		"key" : "PN",
		"value" : "Push Notifications",
		"id" : "-1",
		"Sts" : "P"
	} ]
}
