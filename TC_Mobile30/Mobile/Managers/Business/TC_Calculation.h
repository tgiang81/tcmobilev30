//
//  TC_Calculation.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/25/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TC_Calculation : NSObject
+ (NSNumber *)getPriceWithStockLotSize:(NSInteger)stockLotSize settcurrency:(NSString *)settcurrency stkPrice:(double)stkPrice stockCurrency:(NSString *)stockCurrency quantity:(double)quantity;
@end
