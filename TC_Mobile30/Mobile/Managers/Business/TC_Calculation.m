//
//  TC_Calculation.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/25/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TC_Calculation.h"
#import "UserPrefConstants.h"
#import "CurrencyRate.h"
#import "NSNumber+Formatter.h"
#import "QCData.h"
#import "SettingManager.h"
@implementation TC_Calculation
+ (NSNumber *)getPriceWithStockLotSize:(NSInteger)stockLotSize settcurrency:(NSString *)settcurrency stkPrice:(double)stkPrice stockCurrency:(NSString *)stockCurrency quantity:(double)quantity{
    if(stockLotSize == -1){
        stockLotSize = 1;
    }
    long qtyFactor = 1;
    if ([SettingManager shareInstance].currentQuantityType == 1) {
        qtyFactor = stockLotSize*100;
        //        qtyFactor = lotSizeStringInt;
    }
    
    if (settcurrency==nil){
        settcurrency = [UserPrefConstants singleton].defaultCurrency;
    }

    if ([stockCurrency length] <=0 ) {
        stockCurrency = [UserPrefConstants singleton].defaultCurrency;
    }
    CurrencyRate *cr = [[UserPrefConstants singleton].currencyRateDict objectForKey:stockCurrency];
    
    
    // convert price if settcurrency is not equal to stockcurrency
    if (![settcurrency isEqualToString:@""] && ![settcurrency isEqualToString:stockCurrency] && cr != nil) {
        
        stkPrice = cr.buy_rate * stkPrice; // use buy rate or sell rate???? idk man too many rules
    }

    return [NSNumber numberWithDouble:quantity*qtyFactor*stkPrice];
    
}
@end
