//
//  VertxManager.h
//  TCiPad
//
//  Created by Kaka on 3/28/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AllCommon.h"

typedef void (^TCVertxCompletionHandler2)(id result);

@interface VertxManager : NSObject{
	
}
//Completion block
@property (strong, nonatomic) TCVertxCompletionHandler2 sortByServerCompletion;

@property (assign, nonatomic) BOOL connected;
//Singleton
+ (VertxManager *)shareInstance;
#pragma mark - Main Action State Socket
- (void)connect;
- (void)reconnectSocket;
- (void)close;

#pragma mark - Main Send Data
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange completion:(TCVertxCompletionHandler2)completion;

@end
