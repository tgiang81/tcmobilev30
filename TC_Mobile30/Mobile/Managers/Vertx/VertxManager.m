//
//  VertxManager.m
//  TCiPad
//
//  Created by Kaka on 3/28/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "VertxManager.h"
#import "SRWebSocket.h"
#import "AppConstants.h"

#define kCloseLogoutCode	111
#define kReasonLogout		@"User Logout"

//KEY FROM DATA
NSString *kSortByServer_KEY = @"SortByServer";

@interface VertxManager()<SRWebSocketDelegate>{
	SRWebSocket *_webSocket;
	
	//Info server
	NSString *cmdCall;
	
	//Incoming type;
	NSString *incomingType;
	NSString *incomingMessage;
	NSString *incomingEventId;
	NSString *incomingQid;
	
	BOOL incomingEnd;
	BOOL incomingHasNext;
}
@end
@implementation VertxManager
//================== Singleton ==============
+ (VertxManager *)shareInstance{
	static VertxManager *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[VertxManager alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
		self.connected = NO;
	}
	return  self;
}
#pragma mark - Main Action State Socket
- (void)connect{
	//Remove old socket
	_webSocket.delegate = nil;
	[_webSocket close];
	//Make new session
	//_webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"wss://%@/eventbus/websocket",[UserPrefConstants singleton].Vertexaddress]]]];
	_webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kDumpmySocketURL]]];
	_webSocket.delegate = self;
	[_webSocket open];
}
- (void)reconnectSocket{
	DLog(@"+++ Reconnecting...");
	[self performSelector:@selector(connect) withObject:nil afterDelay:0.3];
}

- (void)close{
	if (_webSocket) {
		[_webSocket closeWithCode:kCloseLogoutCode reason:kReasonLogout];
		_connected = NO;
	}
}

#pragma mark - Main Actions
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange completion:(TCVertxCompletionHandler2)completion{
	cmdCall = @"SortByServer";
	
	NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
	NSMutableArray *filterArr = [[NSMutableArray alloc]init];
	NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
	[filterDict setObject:FID_38_S_STOCK_NAME forKey:@"field"];
	[filterDict setObject:@"" forKey:@"value"];
	[filterDict setObject:@"ne" forKey:@"comparison"];
	[filterArr addObject:[filterDict copy]];
	
	// NSString *formattedBoardLot = [NSString stringWithFormat:@"|%@|", [UserPrefConstants singleton].selectedMarket];
	NSString *marketStr = [UserPrefConstants singleton].selectedMarket;
	int marketInt = [marketStr intValue];
	
	if (marketInt>=1000) {
		[filterDict setObject:FID_36_I_SECTOR forKey:@"field"];
	} else {
		[filterDict setObject:FID_151_I_MARKET forKey:@"field"];
	}
	[filterDict setObject:@"eq" forKey:@"comparison"];
	[filterDict setObject:marketStr forKey:@"value"];
	[filterArr addObject:[filterDict copy]];
	
	// if filter by sector
	if ([UserPrefConstants singleton].sectorToFilter!=nil) {
		
		//      old sector filtering
		//      NSString *sectorName = [UserPrefConstants singleton].sectorToFilter.sectorName;
		//      [filterDict setObject:@"pathName" forKey:@"field"];
		//      [filterDict setObject:[NSString stringWithFormat:@"|%@|",sectorName] forKey:@"value"];
		
		int sectorCode = [[UserPrefConstants singleton].sectorToFilter.sectorCode intValue];
		
		if ((sectorCode>=2201)&&(sectorCode<=2299)) {
			[filterDict setObject:FID_35_I_INDEX forKey:@"field"];
			[filterDict setObject:@"biton" forKey:@"comparison"];
			[filterDict setObject:[NSNumber numberWithInt:(1<<(sectorCode%2201))] forKey:@"value"];
		} else {
			[filterDict setObject:FID_36_I_SECTOR forKey:@"field"];
			[filterDict setObject:@"eq" forKey:@"comparison"];
			[filterDict setObject:[NSNumber numberWithInt:sectorCode] forKey:@"value"];
		}
		[filterArr addObject:[filterDict copy]];
	}
	
	
	if ([UserPrefConstants singleton].enableWarrant) {
		
		NSDictionary *andDict = @{@"and":@[
										  @{@"field" : FID_119_S_TEXT_TYPE,@"value":[NSNumber numberWithInt:4999],@"comparison":@"gt"},
										  @{@"field" : FID_119_S_TEXT_TYPE,@"value":[NSNumber numberWithInt:10000],@"comparison":@"lt"}
										  ]};
		
		NSDictionary *fieldsDict = @{@"field": FID_119_S_TEXT_TYPE,
									 @"value":@[@"8",@"16",@"17",@"19",@"20",@"31",@"32",@"35",@"36",@"37",@"38",@"39",@"40",@"41",@"42"],
									 @"comparison": @"in"};
		
		NSDictionary *orDict = @{@"or": @[fieldsDict, andDict]};
		
		NSDictionary *notDict = @{@"not":@[orDict]};
		
		[filterArr addObject:notDict];
		
	}
	
	NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
	[sortDict setObject:property forKey:@"property"];
	[sortDict setObject:direction forKey:@"direction"];
	
	NSMutableArray *sortArr = [[NSMutableArray alloc]init];
	[sortArr addObject:sortDict];
	NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
	[sendMessage setObject:@"query" forKey:@"action"]; //String
	[sendMessage setObject:@[COLL_05_QUOTE, COLL_07_TRANS_PUSH] forKey:@"coll"]; //Array
	
	int startIndex = [UserPrefConstants singleton].quoteScrPage * 20;
	
	[sendMessage setObject:[NSNumber numberWithInt:startIndex] forKey:@"start"]; //int
	[sendMessage setObject:[NSNumber numberWithInt:20] forKey:@"limit"]; //int
	[sendMessage setObject:collumnArr forKey:@"column"]; //Array
	[sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
	[sendMessage setObject:exchange forKey:@"exch"]; //String
	[sendMessage setObject:sortArr forKey:@"sort"]; //Array
	[sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
	
	NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
													   options:NSJSONWritingPrettyPrinted
														 error: nil];
	NSLog(@"sortByServer sendMessage : %@",sendMessage);
	///NSLog(@"%@", [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding]);
	//if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
	[_webSocket send:jsonData];
	//eventIdCounter ++;
	self.sortByServerCompletion = completion;
	completion = nil;
}

///--------------------------------------
#pragma mark - SRWebSocketDelegate
///--------------------------------------
- (void)webSocketDidOpen:(SRWebSocket *)webSocket{
	DLog(@"Websocket Connected");
	self.connected = YES;
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
	DLog(@":( Websocket Failed With Error %@", error);
	self.connected = NO;
	[self reconnectSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
	DLog(@"WebSocket closed: Code - %@, reason - %@", @(code), reason);
	if (code != kCloseLogoutCode) {
		[self reconnectSocket];
	}else{
		//Close by user: Logout...
		_webSocket = nil;
	}
}


#pragma mark - DidReceive Data
- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload;
{
	DLog(@"WebSocket received pong");
}

//=========== Did recerve data =========================================
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessageWithString:(NSString *)string{
	DLog(@"+++Received \"%@\"", string);
}
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
	DLog(@"Received \"%@\"", message);
	NSError *localError = nil;
	NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:message options:0 error:&localError];
	
	incomingType    = [ parsedObject objectForKey:@"type"];
	incomingEventId = [ parsedObject objectForKey:@"eventId"];
	incomingQid     = [[parsedObject objectForKey:@"qId"]stringValue];
	incomingMessage = [ parsedObject objectForKey:@"message"];
	incomingHasNext = [[parsedObject objectForKey:@"hasNext"]boolValue];
	incomingEnd     = [[parsedObject objectForKey:@"end"]boolValue];
	NSMutableDictionary *responseDict = @{}.mutableCopy;
	if([incomingEventId isEqualToString:kSortByServer_KEY]){
		NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]]; // array of stocknames (NSString)
		responseDict = [NSMutableDictionary dictionary];
		NSLog(@"tmpArray %@",tmpArray);
		for (long i=0; i<[tmpArray count]; i++) {
//			NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
//			[sortedresults addObject:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
//			[responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
//			[[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
		}
		if (self.sortByServerCompletion) {
			self.sortByServerCompletion(message);
		}
	}
}

@end
