//
//  ParserUtil.h
//  TC_Mobile30
//
//  Created by Kaka on 10/24/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PortfolioData;
@class PrtfDtlRptData;
@class PrtfSummRptData;
@class AssetManagementModel;
@interface ParserUtil : NSObject{
	
}
//================== Singleton ==============
+ (ParserUtil *)shareInstance;

#pragma mark - Main
#pragma mark - Parse Porfolio
//Parse Realised - Unrealised data
- (NSArray<PortfolioData *> *)parse_REALISED_PortfoliosDataResponse:(NSString *)content;
//++++ PARSE VIRTUAL PF
- (NSArray<PrtfDtlRptData *> *)parseVirtualPortfolioFrom:(NSString *)content;
//+++ PARSE SUMMARY PF
- (NSArray<PrtfSummRptData *> *)parseSummaryPortfolioFrom:(NSString *)content;

- (NSArray<PrtfDtlRptData *> *)parseDetailPortfolioFrom:(NSString *)content;

#pragma mark - doLoginParser
- (NSError *)parseResultOfLoginData:(NSString *)data userInfoEncryptionKey:(NSString *)userInfoEncryptionKey;
#pragma mark - doAccountClientList Parser
- (NSError *)parseAccountClientList:(NSString *)data;
- (NSArray<AssetManagementModel *> *)parseAssetMamagement:(NSString *)content;
@end
