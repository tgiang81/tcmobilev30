//
//  ParserUtil.m
//  TC_Mobile30
//
//  Created by Kaka on 10/24/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "ParserUtil.h"
#import "GTMRegex.h"

#import "PortfolioData.h"
#import "PrtfSubDtlRptData.h"
#import "PrtfSummRptData.h"

#import "ATPAuthenticate.h"
#import "TradingRules.h"
#import "CurrencyRate.h"
#import "AdditionalPaymentData.h"
#import "OrderControl.h"
#import "Utils.h"
#import "NSString+Util.h"
#import "AssetManagementModel.h"
#import "AssetManagementMapper.h"
@interface ParserUtil(){
	ATPAuthenticate *_atp;
}
//Properties
@end

@implementation ParserUtil
//================== Singleton ==============
+ (ParserUtil *)shareInstance{
	static ParserUtil *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[ParserUtil alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
		_atp = [ATPAuthenticate singleton];
	}
	return  self;
}

#pragma mark - Main
#pragma mark - PORTFOLIO PARSE DATA
//++++ PARSE UNREALISED-REALISED PF
- (NSArray<PortfolioData *> *)parse_REALISED_PortfoliosDataResponse:(NSString *)content{
	NSMutableArray *_portDataArr = @[].mutableCopy;
	content = [_atp atpDecryptionProcess:content];
	GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
	GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
	
	int stockcode = -1;					//+
	int stockname = -1;					//,
	int stockcode_cn = -1;				//-
	int price_high = -1;				//.
	int price_low = -1;					///
	int price_year_high = -1;			//0
	int price_year_low = -1;			//1
	int exchange_ref_number = -1;		//2
	int client_account = -1;			//3
	int branch_code = -1;				//4
	int brokerage = -1;					//5
	int quantity_available = -1;		//6
	int quantity_on_hand = -1;			//7
	int total_price_P = -1;				//8
	int total_price_B = -1;				//9
	int total_quantity_P = -1;			//:
	int total_comm_P = -1;				//;
	int sell_qty_in_proc = -1;          //ascii132
	
	int total_price_S = -1;				//>
	int rms_code = -1;					//?
	int price_last_done = -1;			//@
	int price_ref = -1;					//A
	int price_AS = -1;					//B
	int price_AP = -1;					//C
	int volume = -1;					//D
	int lot_size = -1;					//E
	int change_amount = -1;				//F
	int change_percentage = -1;			//G
	int sector_code = -1;				//H
	int stock_class = -1;				//I
	int stock_status = -1;				//J
	int price_base = -1;				//K
	int price_day_high = -1;			//L
	int price_day_low = -1;				//M
	int price_avg_purchase = -1;		//N
	int limit_upper = -1;				//O
	int limit_lower = -1;				//P
	int gross_market_value = -1;		//Q//last done * qty on hadn
	int unrealized_gain_loss_amount = -1;		//R.
	int unrealized_gain_loss_percentage = -1;	//S
	int total_value_P = -1;				//T
	int total_value_S = -1;				//U
	int total_quantity_S = -1;			//V
	int total_brokerage = -1;			//W
	int gain_loss = -1;					//X
	int aggregated_buy_price = -1;		//Y
	int aggregated_sell_price = -1;		//Z
	int total_quantity_short = -1;		//[
	int total_comm_S = -1;				//]
	int currency = -1;					//^
	int settlement_mode = -1;           //Ascii 151
	int realized_gain_loss_amount = -1;	//}
	int realized_gain_loss_percentage = -1;		//~
    int qty_sold = -1;        // <
    int bought = -1;        // ¯
    int sold = -1;        // °
    int unsettled_bought_t1 = -1;        // ±
    int unsettled_sold_t1 = -1;        // ²
    int settled_bought_t2 = -1;        // ³
    int settled_sold_t2 = -1;        // ´
    int pledge = -1;        // µ
    int rights = -1;        // ¶
    int registered = -1;        // ·
    int cash_dividend =-1;      // ®
    
	NSArray *arr = [content componentsSeparatedByString:@"\r"];
	NSString *s;
	for (s in arr) {
		s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
		if ([metaregex matchesString:s]) {
			s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			if ([tokens count] > 5) {
				int pos = 0;
				for (NSString *token in tokens) {
					if ([token isEqualToString:@"+"]) {
						stockcode = pos;
					}
					else if ([token isEqualToString:@","]) {
						stockname = pos;
					}
					else if ([token isEqualToString:@"-"]) {
						stockcode_cn = pos;
					}
					else if ([token isEqualToString:@"."]) {
						price_high = pos;
					}
					else if ([token isEqualToString:@"/"]) {
						price_low = pos;
					}
					else if ([token isEqualToString:@"0"]) {
						price_year_high = pos;
					}
					else if ([token isEqualToString:@"1"]) {
						price_year_low = pos;
					}
					else if ([token isEqualToString:@"2"]) {
						exchange_ref_number = pos;
					}
					else if ([token isEqualToString:@"3"]) {
						client_account = pos;
					}
					else if ([token isEqualToString:@"4"]) {
						branch_code = pos;
					}
					else if ([token isEqualToString:@"5"]) {
						brokerage = pos;
					}
					else if ([token isEqualToString:@"6"]) {
						quantity_available = pos;
					}
					
					else if ([token isEqualToString:@"7"]) {
						quantity_on_hand = pos;
					}
					else if ([token isEqualToString:@"8"]) {
						total_price_P = pos;
					}
					else if ([token isEqualToString:@"9"]) {
						total_price_B = pos;
					}
					else if ([token isEqualToString:@":"]) {
						total_quantity_P = pos;
					}
					else if ([token isEqualToString:@";"]) {
						total_comm_P = pos;
					}
					else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)132]]) {
						sell_qty_in_proc = pos;
					}
					else if ([token isEqualToString:@">"]) {
						total_price_S = pos;
					}
					else if ([token isEqualToString:@"?"]) {
						rms_code = pos;
					}
					else if ([token isEqualToString:@"@"]) {
						price_last_done = pos;
					}
					else if ([token isEqualToString:@"A"]) {
						price_ref = pos;
					}
					
					else if ([token isEqualToString:@"B"]) {
						price_AS = pos;
					}
					else if ([token isEqualToString:@"C"]) {
						price_AP = pos;
					}
					else if ([token isEqualToString:@"D"]) {
						volume = pos;
					}
					else if ([token isEqualToString:@"E"]) {
						lot_size = pos;
					}
					else if ([token isEqualToString:@"F"]) {
						change_amount = pos;
					}
					else if ([token isEqualToString:@"G"]) {
						change_percentage = pos;
					}
					else if ([token isEqualToString:@"H"]) {
						sector_code = pos;
					}
					else if ([token isEqualToString:@"I"]) {
						stock_class = pos;
					}
					else if ([token isEqualToString:@"J"]) {
						stock_status = pos;
					}
					else if ([token isEqualToString:@"K"]) {
						price_base = pos;
					}
					
					else if ([token isEqualToString:@"L"]) {
						price_day_high = pos;
					}
					else if ([token isEqualToString:@"M"]) {
						price_day_low = pos;
					}
					else if ([token isEqualToString:@"N"]) {
						price_avg_purchase = pos;
					}
					else if ([token isEqualToString:@"O"]) {
						limit_upper = pos;
					}
					else if ([token isEqualToString:@"P"]) {
						limit_lower = pos;
					}
					else if ([token isEqualToString:@"Q"]) {
						gross_market_value = pos;
					}
					else if ([token isEqualToString:@"R"]) {
						unrealized_gain_loss_amount = pos;
					}
					else if ([token isEqualToString:@"S"]) {
						unrealized_gain_loss_percentage = pos;
					}
					else if ([token isEqualToString:@"T"]) {
						total_value_P = pos;
					}
					else if ([token isEqualToString:@"U"]) {
						total_quantity_S = pos;
					}
					else if ([token isEqualToString:@"V"]) {
						total_value_S = pos;
					}
					else if ([token isEqualToString:@"W"]) {
						total_brokerage = pos;
					}
					else if ([token isEqualToString:@"X"]) {
						gain_loss = pos;
					}
					else if ([token isEqualToString:@"Y"]) {
						aggregated_buy_price = pos;
					}
					else if ([token isEqualToString:@"Z"]) {
						aggregated_sell_price = pos;
					}
					else if ([token isEqualToString:@"["]) {
						total_quantity_short = pos;
					}
					else if ([token isEqualToString:@"]"]) {
						total_comm_S = pos;
					}
					else if ([token isEqualToString:@"^"]) {
						currency = pos;
					}
					else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)151]]) {
						settlement_mode = pos;
					}
					else if ([token isEqualToString:@"r"]) {
						realized_gain_loss_amount = pos;
					}
					else if ([token isEqualToString:@"~"]) {
						realized_gain_loss_percentage = pos;
					}
                    else if ([token isEqualToString:@"<"]) {
                        qty_sold = pos;
                    }
                    else if ([token isEqualToString:@"¯"]){
                        bought=pos;
                    }
                    else if ([token isEqualToString:@"°"]){
                        sold=pos;
                    }
                    else if ([token isEqualToString:@"±"]){
                        unsettled_bought_t1=pos;
                    }
                    else if ([token isEqualToString:@"²"]){
                        unsettled_sold_t1=pos;
                    }
                    else if ([token isEqualToString:@"³"]){
                        settled_bought_t2=pos;
                    }
                    else if ([token isEqualToString:@"´"]){
                        settled_sold_t2=pos;
                    }
                    else if ([token isEqualToString:@"µ"]){
                        pledge=pos;
                    }
                    else if ([token isEqualToString:@"¶"]){
                        rights=pos;
                    }
                    else if ([token isEqualToString:@"·"]){
                        registered=pos;
                    }
                    else if ([token isEqualToString:@"®"])
                    {
                        cash_dividend=pos;
                    }
                    pos++;
				}
			}
		}
		else if ([dataregex matchesString:s]) {
			s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			if ([tokens count] > 5) {
				PortfolioData *pd = [[PortfolioData alloc] init];
				if (stockcode != -1) {
					pd.stockcode = [tokens objectAtIndex:stockcode];
					//What it for ???
					[[UserPrefConstants singleton].eqtyUnrealPFResultStkCodeArr addObject:pd.stockcode];
				}
				if (stockname != -1) {
					pd.stockname = [tokens objectAtIndex:stockname];
				}
				if (stockcode_cn != -1) {
					pd.stockcode_cn = [tokens objectAtIndex:stockcode_cn];
				}
				if (price_high != -1) {
					pd.price_high = [tokens objectAtIndex:price_high];
				}
				if (price_low != -1) {
					pd.price_low = [tokens objectAtIndex:price_low];
				}
				if (price_year_high != -1) {
					pd.price_year_high = [tokens objectAtIndex:price_year_high];
				}
				if (price_year_low != -1) {
					pd.price_year_low = [tokens objectAtIndex:price_year_low];
				}
				if (exchange_ref_number != -1) {
					pd.exchange_ref_number = [tokens objectAtIndex:exchange_ref_number];
				}
				if (client_account != -1) {
					pd.client_account = [tokens objectAtIndex:client_account];
				}
				if (branch_code != -1) {
					pd.branch_code = [tokens objectAtIndex:branch_code];
				}
				if (brokerage != -1) {
					pd.brokerage = [tokens objectAtIndex:brokerage];
				}
				
				if (quantity_available != -1) {
					pd.quantity_available = [tokens objectAtIndex:quantity_available];
				}
				if (quantity_on_hand != -1) {
					pd.quantity_on_hand = [tokens objectAtIndex:quantity_on_hand];
				}
				if (total_price_P != -1) {
					pd.total_price_P = [tokens objectAtIndex:total_price_P];
				}
				if (total_price_B != -1) {
					pd.total_price_B = [tokens objectAtIndex:total_price_B];
				}
				if (total_quantity_P != -1) {
					pd.total_quantity_P = [tokens objectAtIndex:total_quantity_P];
				}
				if (total_comm_P != -1) {
					pd.total_comm_P = [tokens objectAtIndex:total_comm_P];
				}
				if (sell_qty_in_proc != -1) {
					pd.sell_qty_in_proc = [tokens objectAtIndex:sell_qty_in_proc];
				}
				if (total_price_S != -1) {
					pd.total_price_S = [tokens objectAtIndex:total_price_S];
				}
				if (rms_code != -1) {
					pd.rms_code = [tokens objectAtIndex:rms_code];
				}
				if (price_last_done != -1) {
					pd.price_last_done = [tokens objectAtIndex:price_last_done];
				}
				if (price_ref != -1) {
					pd.price_ref = [tokens objectAtIndex:price_ref];
				}
				
				if (price_AS != -1) {
					pd.price_AS = [tokens objectAtIndex:price_AS];
				}
				if (price_AP != -1) {
					pd.price_AP = [tokens objectAtIndex:price_AP];
				}
				if (volume != -1) {
					pd.volume = [tokens objectAtIndex:volume];
				}
				if (lot_size != -1) {
					pd.lot_size = [tokens objectAtIndex:lot_size];
				}
				if (change_amount != -1) {
					pd.change_amount = [tokens objectAtIndex:change_amount];
				}
				if (change_percentage != -1) {
					pd.change_percentage = [tokens objectAtIndex:change_percentage];
				}
				if (sector_code != -1) {
					pd.sector_code = [tokens objectAtIndex:sector_code];
				}
				if (stock_class != -1) {
					pd.stock_class = [tokens objectAtIndex:stock_class];
				}
				if (stock_status != -1) {
					pd.stock_status = [tokens objectAtIndex:stock_status];
				}
				if (price_base != -1) {
					pd.price_base = [tokens objectAtIndex:price_base];
				}
				if (price_day_high != -1) {
					pd.price_day_high = [tokens objectAtIndex:price_day_high];
				}
				if (price_day_low != -1) {
					pd.price_day_low = [tokens objectAtIndex:price_day_low];
				}
				if (price_avg_purchase != -1) {
					pd.price_avg_purchase = [tokens objectAtIndex:price_avg_purchase];
				}
				if (limit_upper != -1) {
					pd.limit_upper = [tokens objectAtIndex:limit_upper];
				}
				if (limit_lower != -1) {
					pd.limit_lower = [tokens objectAtIndex:limit_lower];
				}
				if (gross_market_value != -1) {
					pd.gross_market_value = [tokens objectAtIndex:gross_market_value];
				}
				if (unrealized_gain_loss_amount != -1) {
					pd.unrealized_gain_loss_amount = [tokens objectAtIndex:unrealized_gain_loss_amount];
				}
				if (unrealized_gain_loss_percentage != -1) {
					pd.unrealized_gain_loss_percentage = [tokens objectAtIndex:unrealized_gain_loss_percentage];
				}
				if (total_value_P != -1) {
					pd.total_value_P = [tokens objectAtIndex:total_value_P];
				}
				if (total_value_S != -1) {
					pd.total_value_S = [tokens objectAtIndex:total_value_S];
				}
				if (total_quantity_S != -1) {
					pd.total_quantity_S = [tokens objectAtIndex:total_quantity_S];
				}
				if (total_brokerage != -1) {
					pd.total_brokerage = [tokens objectAtIndex:total_brokerage];
				}
				if (gain_loss != -1) {
					pd.gain_loss = [tokens objectAtIndex:gain_loss];
				}
				if (aggregated_buy_price != -1) {
					pd.aggregated_buy_price = [tokens objectAtIndex:aggregated_buy_price];
				}
				if (aggregated_sell_price != -1) {
					pd.aggregated_sell_price = [tokens objectAtIndex:aggregated_sell_price];
				}
				if (total_quantity_short != -1) {
					pd.total_quantity_short = [tokens objectAtIndex:total_quantity_short];
				}
				if (total_comm_S != -1) {
					pd.total_comm_S = [tokens objectAtIndex:total_comm_S];
				}
				if (currency != -1) {
					pd.currency = [tokens objectAtIndex:currency];
				}
				if (settlement_mode != -1) {
					pd.settlement_mode = [tokens objectAtIndex:settlement_mode];
				}
				if (realized_gain_loss_amount != -1) {
					pd.realized_gain_loss_amount = [tokens objectAtIndex:realized_gain_loss_amount];
				}
				if (realized_gain_loss_percentage != -1) {
					pd.realized_gain_loss_percentage = [tokens objectAtIndex:realized_gain_loss_percentage];
				}
                if (qty_sold != -1) {
                    pd.qty_sold = [tokens objectAtIndex:qty_sold];
                }
                if (bought != -1) {
                    pd.bought = [tokens objectAtIndex:bought];
                }
                if (sold != -1) {
                    pd.sold = [tokens objectAtIndex:sold];
                }
                if (unsettled_bought_t1 != -1) {
                    pd.unsettled_bought_t1 = [tokens objectAtIndex:unsettled_bought_t1];
                }
                if (unsettled_sold_t1 != -1) {
                    pd.unsettled_sold_t1 = [tokens objectAtIndex:unsettled_sold_t1];
                }
                if (settled_bought_t2 != -1) {
                    pd.settled_bought_t2 = [tokens objectAtIndex:settled_bought_t2];
                }
                if (pledge != -1) {
                    pd.pledge = [tokens objectAtIndex:pledge];
                }
                if (rights != -1) {
                    pd.rights = [tokens objectAtIndex:rights];
                }
                if (registered != -1) {
                    pd.registered = [tokens objectAtIndex:registered];
                }
                if (cash_dividend != -1) {
                    pd.cash_dividend = [tokens objectAtIndex:cash_dividend];
                }
                //[[UserPrefConstants singleton].eqtyUnrealPFResultList addObject:pd];
				[_portDataArr addObject:pd];
			}
		}
	}
	return [_portDataArr copy];
}
#pragma mark PF Detail
- (NSArray<PrtfDtlRptData *> *)parseDetailPortfolioFrom:(NSString *)content{
    NSMutableArray *_detailArr = @[].mutableCopy;
    content = [_atp atpDecryptionProcess:content];
    DLog(@"\n\n---- TradePrtfDtlRpt Returned Data ----\n%@\n\n", content);
    
    GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
    GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
    
    int sequence_no = -1;                //* Ascii 42
    int side = -1;                      //r Ascii 114
    int matched_qty = -1;                //t Ascii 116
    int matched_price = -1;             //u Ascii 117
    int contract_per_val = -1;            //v Ascii 118
    int home_currency = -1;             //Ascii 153
    int forex_exchange_rate= -1;        //Ascii 154
    int stock_currency = -1;            //Ascii 155
    
    NSArray *arr = [content componentsSeparatedByString:@"\r"];
    NSString *s;
    for (s in arr) {
        s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if ([metaregex matchesString:s]) {
            s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
            NSArray *tokens = [s componentsSeparatedByString:@"|"];
            int pos = 0;
            for (NSString *token in tokens) {
                if ([token isEqualToString:@"*"]) {
                    //sequence no
                    sequence_no = pos;
                }
                else if ([token isEqualToString:@"r"]) {
                    //side
                    side = pos;
                }
                else if ([token isEqualToString:@"t"]) {
                    //matched quantity
                    matched_qty = pos;
                }
                else if ([token isEqualToString:@"u"]) {
                    //matched price
                    matched_price = pos;
                }
                else if ([token isEqualToString:@"v"]) {
                    //contract per val
                    contract_per_val = pos;
                }
                else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)153]]) {
                    //home currency
                    home_currency = pos;
                }
                else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)154]]) {
                    //exchange rate
                    forex_exchange_rate = pos;
                }
                else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)155]]) {
                    //stock currency
                    stock_currency = pos;
                }
                pos++;
            }
        }
        else if ([dataregex matchesString:s]) {
            PrtfDtlRptData *prtfDtlRptData = [[PrtfDtlRptData alloc] init];
            
            s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
            NSArray *tokens = [s componentsSeparatedByString:@"|"];
            
            if (sequence_no != -1) {
                prtfDtlRptData.sequence_no = [[tokens objectAtIndex:sequence_no] intValue];
            }
            if (side != -1) {
                prtfDtlRptData.side = [tokens objectAtIndex:side];
            }
            if (matched_qty != -1) {
                prtfDtlRptData.matched_qty = [tokens objectAtIndex:matched_qty];
            }
            if (matched_price != -1) {
                prtfDtlRptData.matched_price = [tokens objectAtIndex:matched_price];
            }
            if (contract_per_val != -1) {
                prtfDtlRptData.contractPerVal = [[tokens objectAtIndex:contract_per_val] doubleValue];
            }
            if (home_currency != -1) {
                prtfDtlRptData.home_currency = [tokens objectAtIndex:home_currency];
            }
            if (forex_exchange_rate != -1) {
                prtfDtlRptData.exchange_rate = [tokens objectAtIndex:forex_exchange_rate];
            }
            if (stock_currency != -1) {
                prtfDtlRptData.stock_currency = [tokens objectAtIndex:stock_currency];
            }
            [_detailArr addObject:prtfDtlRptData];
        }
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sequence_no" ascending:YES];
    [_detailArr sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return _detailArr;
}
//++++ PARSE VIRTUAL PF
- (NSArray<PrtfDtlRptData *> *)parseVirtualPortfolioFrom:(NSString *)content{
	NSMutableArray *_virtualArr = @[].mutableCopy;

	content = [_atp atpDecryptionProcess:content];
	//NSLog(@"\n\n---- TradePrtfSubDtlRpt Returned Data ----\n%@\n\n", content);
	
	GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
	GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
	
	int sequence_no = -1;				//* Ascii 42
	int stock_code = -1;				//+ Ascii 43
	int stock_name = -1;                //, Ascii 44
	int average_purchase_price = -1;	//n Ascii 78
	int unrealized_pl = -1;             //r Ascii 82
	int gross_buy = -1;                 //_ Ascii 95
	int gross_sell = -1;                //` Ascii 96
	int day_buy = -1;                   //a Ascii 97
	int day_sell = -1;                  //b Ascii 98
	int open_buy = -1;                  //c Ascii 99
	int open_sell = -1;                 //d Ascii 100
	int product_code = -1;              //o Ascii 111
	int contract_per_val = -1;          //v Ascii 118
	int realized_pl = -1;               //} Ascii 125
	int home_currency = -1;             //Ascii 153
	int forex_exchange_rate= -1;        //Ascii 154
	int stock_currency = -1;            //Ascii 155
	
	NSArray *arr = [content componentsSeparatedByString:@"\r"];
	
	//NSLog(@"SubDetailReportDone %@",arr);
	
	NSString *s;
	for (s in arr) {
		s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
		if ([metaregex matchesString:s]) {
			s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			int pos = 0;
			for (NSString *token in tokens) {
				if ([token isEqualToString:@"*"]) {
					//sequence no
					sequence_no = pos;
				}
				else if ([token isEqualToString:@"+"]) {
					//stock code
					stock_code = pos;
				}
				else if ([token isEqualToString:@","]) {
					//stock name
					stock_name = pos;
				}
				else if ([token isEqualToString:@"N"]) {
					//average purchse price
					average_purchase_price = pos;
				}
				else if ([token isEqualToString:@"R"]) {
					//unrealized gl
					unrealized_pl = pos;
				}
				else if ([token isEqualToString:@"_"]) {
					//gross buy
					gross_buy = pos;
				}
				else if ([token isEqualToString:@"`"]) {
					//gross sell
					gross_sell = pos;
				}
				else if ([token isEqualToString:@"a"]) {
					//day buy
					day_buy = pos;
				}
				else if ([token isEqualToString:@"b"]) {
					//day sell
					day_sell = pos;
				}
				else if ([token isEqualToString:@"c"]) {
					//open buy
					open_buy = pos;
				}
				else if ([token isEqualToString:@"d"]) {
					//open sell
					open_sell = pos;
				}
				else if ([token isEqualToString:@"o"]) {
					//product code
					product_code = pos;
				}
				else if ([token isEqualToString:@"v"]) {
					//contract per val
					contract_per_val = pos;
				}
				else if ([token isEqualToString:@"}"]) {
					//realized gl
					realized_pl = pos;
				}
				else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)153]]) {
					//home currency
					home_currency = pos;
				}
				else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)154]]) {
					//exchange rate
					forex_exchange_rate = pos;
				}
				else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)155]]) {
					//stock currency
					stock_currency = pos;
				}
				pos++;
			}
		}
		else if ([dataregex matchesString:s]) {
			PrtfSubDtlRptData *prtfSubDtlRptData = [[PrtfSubDtlRptData alloc] init];
			
			s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			
			if (sequence_no != -1) {
				prtfSubDtlRptData.sequence_no = [[tokens objectAtIndex:sequence_no] intValue];
			}
			if (stock_code != -1) {
				prtfSubDtlRptData.stock_code = [tokens objectAtIndex:stock_code];
				[[UserPrefConstants singleton].futuresUnrealPFResultStkCodeArr addObject:prtfSubDtlRptData.stock_code];
			}
			if (stock_name != -1) {
				prtfSubDtlRptData.stock_name = [tokens objectAtIndex:stock_name];
			}
			if (average_purchase_price != -1) {
				prtfSubDtlRptData.average_purchase_price = [tokens objectAtIndex:average_purchase_price];
			}
			if (unrealized_pl != -1) {
				prtfSubDtlRptData.unrealized_pl = [[tokens objectAtIndex:unrealized_pl] doubleValue];
			}
			if (gross_buy != -1) {
				prtfSubDtlRptData.gross_buy = [tokens objectAtIndex:gross_buy];
			}
			if (gross_sell != -1) {
				prtfSubDtlRptData.gross_sell = [tokens objectAtIndex:gross_sell];
			}
			if (day_buy != -1) {
				prtfSubDtlRptData.day_buy = [tokens objectAtIndex:day_buy];
			}
			if (day_sell != -1) {
				prtfSubDtlRptData.day_sell = [tokens objectAtIndex:day_sell];
			}
			if (open_buy != -1) {
				prtfSubDtlRptData.bf_buy = [tokens objectAtIndex:open_buy];
			}
			if (open_sell != -1) {
				prtfSubDtlRptData.bf_sell = [tokens objectAtIndex:open_sell];
			}
			if (product_code != -1) {
				NSString *productCode = [tokens objectAtIndex:product_code];
				NSArray *arr = [productCode componentsSeparatedByString:@"~"];
				if ([arr count] == 2) {
					prtfSubDtlRptData.unrealized_pl = [[arr objectAtIndex:0] doubleValue];
					prtfSubDtlRptData.price_current = [[arr objectAtIndex:1] doubleValue];
				}
			}
			if (contract_per_val != -1) {
				prtfSubDtlRptData.contractPerVal = [[tokens objectAtIndex:contract_per_val] doubleValue];
			}
			if (realized_pl != -1) {
				prtfSubDtlRptData.realized_pl = [tokens objectAtIndex:realized_pl];
			}
			if (home_currency != -1) {
				prtfSubDtlRptData.home_currency = [tokens objectAtIndex:home_currency];
			}
			if (forex_exchange_rate != -1) {
				prtfSubDtlRptData.exchange_rate = [tokens objectAtIndex:forex_exchange_rate];
			}
			if (stock_currency != -1) {
				prtfSubDtlRptData.stock_currency = [tokens objectAtIndex:stock_currency];
			}
			//Calculation: Nett Position = Gross Buy - Gross Sell
			if (gross_buy != -1 && gross_sell != -1) {
				prtfSubDtlRptData.nett_position = [prtfSubDtlRptData.gross_buy intValue] - [prtfSubDtlRptData.gross_sell intValue];
			}
			[_virtualArr addObject:prtfSubDtlRptData];
		}
	}
	return [_virtualArr copy];
}

//PARSE Summary PF data
- (NSArray<PrtfSummRptData *> *)parseSummaryPortfolioFrom:(NSString *)content{
	NSMutableArray *_summaryArr = @[].mutableCopy;
	content = [_atp atpDecryptionProcess:content];
	GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
	GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
	
	int open_long= -1;					//c Ascii 99
	int open_short = -1;				//d Ascii 100
	int cash_balance = -1;				//g Ascii 103
	int deposit = -1;                   //h Ascii 104
	int withdrawal = -1;				//i Ascii 105
	int eligible_collateral = -1;		//j Ascii 106
	int initial_margin = -1;			//k Ascii 107
	int maintenance_margin = -1;		//l Ascii 108
	int margin_call = -1;               //m Ascii 109
	int realized_pl = -1;				//} Ascii 125
	
	NSArray *arr = [content componentsSeparatedByString:@"\r"];
	NSString *s;
	for (s in arr) {
		s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
		if ([metaregex matchesString:s]) {
			s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			int pos = 0;
			for (NSString *token in tokens) {
				if ([token isEqualToString:@"c"]) {
					//open long
					open_long = pos;
				}
				else if ([token isEqualToString:@"d"]) {
					//open short
					open_short = pos;
				}
				else if ([token isEqualToString:@"g"]) {
					//cash balance
					cash_balance = pos;
				}
				else if ([token isEqualToString:@"h"]) {
					//deposit
					deposit = pos;
				}
				else if ([token isEqualToString:@"i"]) {
					//withdrawal
					withdrawal = pos;
				}
				else if ([token isEqualToString:@"j"]) {
					//eligible collateral
					eligible_collateral = pos;
				}
				else if ([token isEqualToString:@"k"]) {
					//initial margin
					initial_margin = pos;
				}
				else if ([token isEqualToString:@"l"]) {
					//maintenance margin
					maintenance_margin = pos;
				}
				else if ([token isEqualToString:@"m"]) {
					//margin call
					margin_call = pos;
				}
				else if ([token isEqualToString:@"}"]) {
					//realized gl
					realized_pl = pos;
				}
				pos++;
			}
		}
		else if ([dataregex matchesString:s]) {
			PrtfSummRptData *prtfSumRptData = [[PrtfSummRptData alloc] init];
			
			s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			
			if (open_long != -1) {
				prtfSumRptData.open_long = [tokens objectAtIndex:open_long];
			}
			if (open_short != -1) {
				prtfSumRptData.open_short = [tokens objectAtIndex:open_short];
			}
			if (cash_balance != -1) {
				prtfSumRptData.bf_cash_balance = [tokens objectAtIndex:cash_balance];
			}
			if (deposit != -1) {
				prtfSumRptData.deposit = [tokens objectAtIndex:deposit];
			}
			if (withdrawal != -1) {
				prtfSumRptData.withdrawal = [tokens objectAtIndex:withdrawal];
			}
			if (eligible_collateral != -1) {
				prtfSumRptData.eligible_collateral = [tokens objectAtIndex:eligible_collateral];
			}
			if (initial_margin != -1) {
				prtfSumRptData.initial_margin = [tokens objectAtIndex:initial_margin];
			}
			if (maintenance_margin != -1) {
				prtfSumRptData.maintenance_margin = [tokens objectAtIndex:maintenance_margin];
			}
			if (margin_call != -1) {
				prtfSumRptData.margin_call = [tokens objectAtIndex:margin_call];
			}
			if (realized_pl != -1) {
				prtfSumRptData.realized_pl = [tokens objectAtIndex:realized_pl];
			}
			//Calculation: Current Balance = Cash Balance + Deposit - Withdrawal + Realized PL
			if (cash_balance != -1 && deposit != -1 && withdrawal != -1 && realized_pl != -1) {
				prtfSumRptData.current_balance = ([prtfSumRptData.bf_cash_balance doubleValue] + [prtfSumRptData.deposit doubleValue]) - [prtfSumRptData.withdrawal doubleValue] + [prtfSumRptData.realized_pl doubleValue];
			}
			[_summaryArr addObject:prtfSumRptData];
		}
	}
	return [_summaryArr copy];
}

#pragma mark - doLogin Results
- (NSError *)parseResultOfLoginData:(NSString *)data userInfoEncryptionKey:(NSString *)userInfoEncryptionKey{
	NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:data currentUserInfoEncryptionKey:userInfoEncryptionKey];
	GTMRegex *skipPin = [GTMRegex regexWithPattern:@"^.SkipPin.*"];
	GTMRegex *exchange = [GTMRegex regexWithPattern:@"^\\[Exchange\\].*"];
	GTMRegex *currencyRate = [GTMRegex regexWithPattern:@"^\\[CurrencyRate\\].*"];
	
	GTMRegex *is2FARequired = [GTMRegex regexWithPattern:@"^\\[Is2FARequired\\].*"];
	GTMRegex *byPass2FA = [GTMRegex regexWithPattern:@"^\\[2FAByPass\\].*"];
	GTMRegex *deviceList = [GTMRegex regexWithPattern:@"^\\[DeviceList\\].*"];
	
	GTMRegex *localExchg = [GTMRegex regexWithPattern:@"^\\[LocalExchange\\].*"];
	GTMRegex *rdsVersion = [GTMRegex regexWithPattern:@"^\\[RDSVersion\\].*"];
	GTMRegex *rdsStatus = [GTMRegex regexWithPattern:@"^\\[RDSStatus\\].*"];
	GTMRegex *resetPWReg = [GTMRegex regexWithPattern:@"^\\[ResetPwdPin\\].*"];
	GTMRegex *actionReg = [GTMRegex regexWithPattern:@"^\\[Action\\].*"];
	GTMRegex *orderTypeReg = [GTMRegex regexWithPattern:@"^\\[OrderType\\].*"];
	GTMRegex *validityReg = [GTMRegex regexWithPattern:@"^\\[Validity\\].*"];
	GTMRegex *paymentReg = [GTMRegex regexWithPattern:@"^\\[Payment\\].*"];
	GTMRegex *PaymentCfg_Payment = [GTMRegex regexWithPattern:@"^\\[PaymentCfg_Payment\\].*"];
	GTMRegex *currencyReg = [GTMRegex regexWithPattern:@"^\\[Currency\\].*"];
	GTMRegex *currencyNotSupported = [GTMRegex regexWithPattern:@"^\\[CurrencyNotSupported\\].*"];
	GTMRegex *reviseReg = [GTMRegex regexWithPattern:@"^\\[Revise\\].*"];
	GTMRegex *rev_orderTypeReg = [GTMRegex regexWithPattern:@"^\\[ReviseOT\\].*"];
	GTMRegex *rev_validityReg = [GTMRegex regexWithPattern:@"^\\[ReviseV\\].*"];
	GTMRegex *rev_currencyReg = [GTMRegex regexWithPattern:@"^\\[ReviseC\\].*"];
	GTMRegex *rev_paymentReg = [GTMRegex regexWithPattern:@"^\\[ReviseP\\].*"];
	GTMRegex *triggerPriceType = [GTMRegex regexWithPattern:@"^\\[TriggerPriceType\\].*"];
	GTMRegex *triggerPriceDirection = [GTMRegex regexWithPattern:@"^\\[TriggerPriceDirection\\].*"];
	GTMRegex *orderCtrlReg = [GTMRegex regexWithPattern:@"^\\[OrderCtrl\\].*"];
	GTMRegex *orderCtrlReg2 = [GTMRegex regexWithPattern:@"^\\[OrderCtrl2\\].*"];
	GTMRegex *reviseCtrlReg = [GTMRegex regexWithPattern:@"^\\[ReviseCtrl\\].*"];
	GTMRegex *email = [GTMRegex regexWithPattern:@"^.Email.*"];
	GTMRegex *privateIP = [GTMRegex regexWithPattern:@"^.PrivateIP.*"];
	GTMRegex *mobileNumber2FA = [GTMRegex regexWithPattern:@"^\\[2FAMobPhone\\].*"];
	GTMRegex *smsOTPInterval = [GTMRegex regexWithPattern:@"^\\[2FASMSOTPInterval\\].*"];
	GTMRegex *lastLoginDateTime = [GTMRegex regexWithPattern:@"^.LastLoginDateTime.*"];
	GTMRegex *phoneNumber = [GTMRegex regexWithPattern:@"^.Mobile.*"];
	GTMRegex *senderName = [GTMRegex regexWithPattern:@"^.SenderName.*"];
	//             GTMRegex *tncReg = [GTMRegex regexWithPattern:@"^\\[TNCInfo\\].*"];
	GTMRegex *clientLimitOptionReg = [GTMRegex regexWithPattern:@"^\\[ClientLimitOption\\].*"];
	GTMRegex *trxFeeFormulaReg = [GTMRegex regexWithPattern:@"^.TrxFeeFormula.*"];
	GTMRegex *idssEnabled = [GTMRegex regexWithPattern:@"^.IDSSEnabled.*"];
	NSArray *errorAndMsg = [content componentsSeparatedByString:@","];
	
	//+++ Why has errors here ???******
	if([[errorAndMsg[0] uppercaseString] containsString:@"ERROR"]){
		//Create Error
		NSDictionary *userInfo = @{
								   NSLocalizedDescriptionKey: [LanguageManager stringForKey:content],
								   NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The response data error.", nil),
								   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Please configuration plist", nil)
								   };
		NSError *aError = [NSError errorWithDomain:@"TradeLogin" code:RESP_CODE_EXCEPTION_PARSE_DATA userInfo:userInfo];
		return aError;
	}
	//+++Start parsing data
	//Prepare private variable
	NSString *userParam;
	NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
	NSMutableDictionary *userInfoDict = @{}.mutableCopy;
	int resetPwdPinMode = 0; // 0=no reset, 1=new login, 2=pwpin expire
	if(arr){
		for (int i=0; i<arr.count; i++)
		{
			NSArray *userInfoItem = [arr[i] componentsSeparatedByString:@"="];
			if(userInfoItem)
			{
				if(userInfoItem.count == 2){ // Item have both key and value. Eg:"[END]= " is invalid, "[UserParam]=abc" is valid
					if ([userInfoItem[0] length]>0)
						[userInfoDict setObject:userInfoItem[1] forKey:userInfoItem[0]];
				}
			}
		}
		NSArray *userParamArr = [arr[0] componentsSeparatedByString:@"[UserParam]="];
		if ([userParamArr count]>1) {
			userParam = userParamArr[1];
		}
        
	}
	for (NSString *s in arr) {
		if ([exchange matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Exchange]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				for (NSString *exchg in tok) {
					TradingRules *trdRules = [[TradingRules alloc] init];
					trdRules.exchg_code = exchg;
					if ([exchg length]>0)
						[[UserPrefConstants singleton].tradingRules setObject:trdRules forKey:trdRules.exchg_code];
				}
			}
		}else if ([currencyRate matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[CurrencyRate]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@";"];
				for (NSString *str in tok) {
					CurrencyRate *curr = [[CurrencyRate alloc] init];
					NSArray *tok2 = [str componentsSeparatedByString:@","];
					int i = 0;
					for (NSString *item in tok2) {
						switch (i) {
							case 1: curr.currency = item;
								break;
							case 4: curr.buy_rate = [item doubleValue];
								break;
							case 5: curr.sell_rate = [item doubleValue];
								break;
							case 6: curr.denomination = [item intValue];
								break;
							default:
								break;
						}
						i++;
					}
					if ([curr.currency length]>0)
						[[UserPrefConstants singleton].currencyRateDict setObject:curr forKey:curr.currency];
				}
			}
		}else if([resetPWReg matchesString:s]){
			int number = [[[ATPAuthenticate singleton] getValue:s] intValue];
			//Enhancement - 12/8/2014
			//Current possible number = 0-7, will need to change logic if in future more numbers adding in
			//0 = Bypass,
			//1 = ChgPwd,
			//2 = ChgPin,
			//3 = 1(ChgPwd) + 2(ChgPin),
			//4 = ChgHint&Answer,
			//5 = 4(ChgHint&Answer) + 1(ChgPwd),
			//6 = 4(ChgHint&Answer) + 2(ChgPin),
			//7 = 4(ChgHint&Answer) + 1(ChgPwd) + 2(ChgPin)
			//only cater number 0-7
			if ((number & 8) == 0) {
				//number 4,5,6,7 will enter
				if ((number & 4) > 0) {
                    resetPwdPinMode = 2;
                    NSString *requiredType = [LanguageManager stringForKey:@"Dear Customer, for first time login, please visit TC Plus to change your password, trading pin and security questions."];
                    //The Hint & Answer is required so User have to change it before login when it is expired
                    NSDictionary *userInfo = @{
                                               NSLocalizedRecoverySuggestionErrorKey: requiredType
                                               };
                    NSError *aError = [NSError errorWithDomain:@"TradeLogin" code:RESP_CODE_EXCEPTION_CHANGE_HINTANSWER_REQUIRED userInfo:userInfo];
                    return aError;
				}
				else {
                    //number 0,1,2,3
					int bitwiseResult = number & 3;
                    if (bitwiseResult != 0) {
                        resetPwdPinMode = 2;
                        
                        NSString *requiredType = @"Unknow";
                        if (bitwiseResult == 1){
                            requiredType = @"ChangePasswordRequired";
                        }
                        else if (bitwiseResult == 2){
                            requiredType = @"ChangePinRequired";
                        }
                        else {
                            requiredType = @"ChangePasswordAndPinRequired";
                        }
                        NSString *messageSuggestion = @"The Password or Pin is expired, please change it first.";
                        //The ChangePasswordPin is required so User have to change it before login when it is expired
                        NSDictionary *userInfo = @{
                                                   NSLocalizedDescriptionKey: requiredType,
                                                   NSLocalizedRecoverySuggestionErrorKey: messageSuggestion
                                                   };
                        NSError *aError = [NSError errorWithDomain:@"TradeLogin" code:RESP_CODE_EXCEPTION_CHANGE_PASSWORDPIN_REQUIRED userInfo:userInfo];
                        return aError;
                    }
				}
			}
			//Discontinue Login Process to LMS and QC once force change password/pin is required
			if (resetPwdPinMode>0) {
				//  [delegate ASIHttpAuthenticateCallback:self];
				
			}
		}
		else if ([is2FARequired matchesString:s]) {
			NSString *ss = [[ATPAuthenticate singleton] getValue:s];
			[UserPrefConstants singleton].is2FARequired = NO;
			if ([ss isEqualToString:@"Y"] || [ss isEqualToString:@"y"]) {
				[UserPrefConstants singleton].is2FARequired = YES;
			}
		}
		else if ([byPass2FA matchesString:s]) {
			[UserPrefConstants singleton].byPass2FA = [[ATPAuthenticate singleton] getValue:s] ?: @"";
		}else if ([deviceList matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[DeviceList]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				for (NSString *device in tok) {
					DeviceDataFor2FA *dvData = [[DeviceDataFor2FA alloc] init];
					NSArray *tok2 = [device componentsSeparatedByString:@"|"];
					int i = 0;
					for (NSString *item in tok2) {
						switch (i) {
							case 0: dvData.deviceID = item;
								break;
							case 1: dvData.icNo = item;
								break;
							default:
								break;
						}
						i++;
					}
					[[UserPrefConstants singleton].deviceListFor2FA addObject:dvData];
				}
			}
		}
		else if([mobileNumber2FA matchesString:s]){
			NSString *ss = [[ATPAuthenticate singleton] getValue:s];
			NSLog(@"mobileNumber2FA %@",ss);
			if (ss.length>3) {
				[[UserPrefConstants singleton].deviceListForSMSOTP addObject:ss];
			}
		}else if([smsOTPInterval matchesString:s]){
			[UserPrefConstants singleton].smsOTPInterval = [[[ATPAuthenticate singleton] getValue:s] intValue];
		}else if ([localExchg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[LocalExchange]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				for (NSString *exchg in tok) {
					[[UserPrefConstants singleton].localExchgList addObject:exchg];
				}
			}
		}else if ([rdsVersion matchesString:s]) {
			if ([UserPrefConstants singleton].isEnableRDS) {
				[UserPrefConstants singleton].rdsData.rdsVersion = [[ATPAuthenticate singleton] getValue:s];
			}
		}else if ([rdsStatus matchesString:s]) {
			if ([UserPrefConstants singleton].isEnableRDS) {
				NSString *ss = [s stringByReplacingOccurrencesOfString:@"[RDSStatus]=" withString:@""];
				//For debugging purpose
				//ss = @"N|http://stgntp.itradecimb.com/gcCIMB/doc/TermCondRDSv1.0.htm";
				//ss = @"B|http://stgntp.itradecimb.com/gcCIMB/doc/TermCondRDSBlockv1.0.htm";
				if (![ss isEqualToString:@""]) {
					NSArray *tok = [ss componentsSeparatedByString:@"|"];
					int i = 0;
					for (NSString *item in tok) {
						switch (i) {
							case 0: [UserPrefConstants singleton].rdsData.rdsStatus = item;
								break;
							case 1: [UserPrefConstants singleton].rdsData.rdsURL = item;
								break;
							default:
								break;
						}
						i++;
					}
				}
			}
		}
		
		
		
		else if ([actionReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Action]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						[trdRules.actionList addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([orderTypeReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[OrderType]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						[trdRules.orderTypeList addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([validityReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Validity]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						[trdRules.validityList addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		
		
		// Added PaymentCfg_Payment for CIMB SG only (Mar 2017)- this is for CUT payment
		else if ([PaymentCfg_Payment matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[PaymentCfg_Payment]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@"|"];
				if ([tok count] > 1) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules
															  objectForKey:(NSString *)[tok objectAtIndex:0]];
					PaymentCUT *paymentConfigCutData=[[PaymentCUT alloc]init];
					paymentConfigCutData.paymentCountry=[tok objectAtIndex:0];
					paymentConfigCutData.AccountType=[[tok objectAtIndex:1] stringByReplacingOccurrencesOfString:@"AccType:" withString:@""];
					paymentConfigCutData.paymentOptions=[[[tok objectAtIndex:2] stringByReplacingOccurrencesOfString:@"Payment:" withString:@""]componentsSeparatedByString:@","];
					if ([tok count]>3 )
						paymentConfigCutData.paymentSettings=[[tok objectAtIndex:3] stringByReplacingOccurrencesOfString:@"Sett:" withString:@""];
					
					[trdRules.paymentCfgList addObject:paymentConfigCutData];
				}
			}
		}
		
		else if ([paymentReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Payment]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						if ([(NSString *)[tok objectAtIndex:i] rangeOfString:@"=+"].location != NSNotFound) {
							NSArray *tok2 = [(NSString *)[tok objectAtIndex:i] componentsSeparatedByString:@"=+"];
							if ([tok2 count] > 1) {
								AdditionalPaymentData *payment = [[AdditionalPaymentData alloc] init];
								payment.payment_name = (NSString *)[tok2 objectAtIndex:0];
								if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Buy"].location != NSNotFound) {
									payment.isAllowedForBuyAction = YES;
								}
								if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Sell"].location != NSNotFound) {
									payment.isAllowedForSellAction = YES;
								}
								[trdRules.paymentPlusList addObject:payment];
							}
						}
						else {
							if (![trdRules.paymentList containsObject:[tok objectAtIndex:i]]) {
								[trdRules.paymentList addObject:[tok objectAtIndex:i]];
							}
						}
					}
				}
			}
		}
		else if ([currencyReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Currency]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						if (![trdRules.currencyList containsObject:[tok objectAtIndex:i]]) {
							[trdRules.currencyList addObject:[tok objectAtIndex:i]];
						}
					}
				}
			}
		}
		else if ([currencyNotSupported matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[CurrencyNotSupported]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					trdRules.currencyNotSupported = [tok objectAtIndex:2];
				}
			}
		}
		else if ([reviseReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Revise]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						[trdRules.reviseParam addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([rev_orderTypeReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseOT]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						[trdRules.reviseOrdTypeParam addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([rev_validityReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseV]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						[trdRules.reviseValidityParam addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([rev_currencyReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseC]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						[trdRules.reviseCurrencyParam addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([rev_paymentReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseP]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 2) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 2; i < [tok count]; i++) {
						if ([(NSString *)[tok objectAtIndex:i] rangeOfString:@"=+"].location != NSNotFound) {
							NSArray *tok2 = [(NSString *)[tok objectAtIndex:i] componentsSeparatedByString:@"=+"];
							if ([tok2 count] > 1) {
								AdditionalPaymentData *payment = [[AdditionalPaymentData alloc] init];
								payment.payment_name = (NSString *)[tok2 objectAtIndex:0];
								if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Buy"].location != NSNotFound) {
									payment.isAllowedForBuyAction = YES;
								}
								if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Sell"].location != NSNotFound) {
									payment.isAllowedForSellAction = YES;
								}
								[trdRules.revisePaymentPlusParam addObject:payment];
							}
						}
						else {
							if (![trdRules.revisePaymentParam containsObject:[tok objectAtIndex:i]]) {
								[trdRules.revisePaymentParam addObject:[tok objectAtIndex:i]];
							}
						}
					}
				}
			}
		}
		else if ([triggerPriceType matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[TriggerPriceType]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 1) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 1; i < [tok count]; i++) {
						[trdRules.triggerPriceTypeList addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([triggerPriceDirection matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[TriggerPriceDirection]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] > 1) {
					TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
					for (int i = 1; i < [tok count]; i++) {
						[trdRules.triggerPriceDirectionList addObject:[tok objectAtIndex:i]];
					}
				}
			}
		}
		else if ([clientLimitOptionReg matchesString:s]) {
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ClientLimitOption]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] == 2) {
					[[UserPrefConstants singleton].clientLimitOptionDict setObject:[tok objectAtIndex:1] forKey:[tok objectAtIndex:0]];
				}
			}
		}
		else if([idssEnabled matchesString:s]){
			[UserPrefConstants singleton].idssValue = [[ATPAuthenticate singleton] getValue:s];
			//IDSS
			//                                                            0 - enable both IDSS buy and sell
			//                                                            1 - enable IDSS buy
			//                                                            2 - only enable IDSS sell
			
			
		}
		else if ([orderCtrlReg matchesString:s]) {
			NSString *content = [s stringByReplacingOccurrencesOfString:@"[OrderCtrl]=" withString:@""];
			if (![content isEqualToString:@""]) {
				NSArray *temp = [content componentsSeparatedByString:@";"];
				OrderControl *ordCtrl = [[OrderControl alloc] init];
				NSString *exchg = @"";
				if ([temp count] > 1) {
					NSArray *part1 = [(NSString *)[temp objectAtIndex:0] componentsSeparatedByString:@","];
					int i = 0;
					for (NSString *item in part1) {
						switch (i) {
							case 0: exchg = item;
								break;
							case 1: ordCtrl.orderType = item;
								break;
							default: [ordCtrl.validityList addObject:item];
								break;
						}
						i++;
					}
					
					NSArray *part2 = [(NSString *)[temp objectAtIndex:1] componentsSeparatedByString:@","];
					for (NSString *field in part2) {
						NSArray *field_parts = [field componentsSeparatedByString:@"="];
						int i = 0;
						NSString *enable_field = @"";
						for (NSString *part in field_parts) {
							switch (i) {
								case 0: [ordCtrl.enable_fields addObject:part];
									enable_field = part;
									break;
								case 1:
								{
									if ([enable_field isEqualToString:@"Disclosed"]) {
										[ordCtrl.disclosed_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
									}
									else if ([enable_field isEqualToString:@"Min"]) {
										[ordCtrl.min_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
									}
									break;
								}
								default:
									break;
							}
							i++;
						}
					}
				}
				TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:exchg];
				if ([ordCtrl.orderType length]>0)
					[trdRules.ordCtrlConditions setObject:ordCtrl forKey:ordCtrl.orderType];
			}
		}
		else if ([skipPin matchesString:s]) {
			BOOL found = NO;
			for (NSString *category in (NSArray *)[[[ATPAuthenticate singleton] getValue:s] componentsSeparatedByString:@","]) {
				if ([category isEqualToString:[userInfoDict objectForKey:@"[SenderCategory]"]]) {
					found = YES;
					break;
				}
			}
			if (found) {
				[UserPrefConstants singleton].skipPin = YES;
			}
			else {
				[UserPrefConstants singleton].skipPin = NO;
			}
		}
		else if([email matchesString:s]){
			[UserPrefConstants singleton].userEmail = [[ATPAuthenticate singleton] getValue:s];
		}
		else if([lastLoginDateTime matchesString:s]){
			[UserPrefConstants singleton].lastLoginTime = [[ATPAuthenticate singleton] getValue:s];
		}
		else if([phoneNumber matchesString:s]){
			[UserPrefConstants singleton].phoneNumber = [[ATPAuthenticate singleton] getValue:s];
		}
		else if([senderName matchesString:s]){
			[UserPrefConstants singleton].senderName = [[ATPAuthenticate singleton] getValue:s];
		}
		else if ([orderCtrlReg2 matchesString:s]) {
			NSString *content = [s stringByReplacingOccurrencesOfString:@"[OrderCtrl2]=" withString:@""];
			if (![content isEqualToString:@""]) {
				NSArray *temp = [content componentsSeparatedByString:@";"];
				
				OrderControl *ordCtrl = [[OrderControl alloc] init];
				NSString *exchg = @"";
				if ([temp count] > 1) {
					NSArray *part1 = [(NSString *)[temp objectAtIndex:0] componentsSeparatedByString:@","];
					int i = 0;
					for (NSString *item in part1) {
						switch (i) {
							case 0: exchg = item;
								break;
							case 1: ordCtrl.orderType = item;
								break;
							default: [ordCtrl.validityList addObject:item];
								break;
						}
						i++;
					}
					NSArray *part2 = [(NSString *)[temp objectAtIndex:1] componentsSeparatedByString:@","];
					for (NSString *field in part2) {
						NSArray *field_parts = [field componentsSeparatedByString:@"="];
						int i = 0;
						NSString *enable_field = @"";
						for (NSString *part in field_parts) {
							switch (i) {
								case 0: [ordCtrl.enable_fields addObject:part];
									enable_field = part;
									break;
								case 1:
								{
									if ([enable_field isEqualToString:@"Disclosed"]) {
										[ordCtrl.disclosed_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
									}
									else if ([enable_field isEqualToString:@"Min"]) {
										[ordCtrl.min_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
									}
									break;
								}
								default:
									break;
							}
							i++;
						}
					}
				}
				TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:exchg];
				if ([ordCtrl.orderType length]>0)
					[trdRules.ordCtrlConditions setObject:ordCtrl forKey:ordCtrl.orderType];
			}
		}else if ([privateIP matchesString:s]){
			[UserPrefConstants singleton].privateIP = [[ATPAuthenticate singleton] getValue:s];
		}
		else if([trxFeeFormulaReg matchesString:s]){
			
			NSString *ss = [s stringByReplacingOccurrencesOfString:@"[TrxFeeFormula]=" withString:@""];
			if (![ss isEqualToString:@""]) {
				NSArray *tok = [ss componentsSeparatedByString:@","];
				if ([tok count] == 3) {
					[[UserPrefConstants singleton].trxFeeFormulaDict setObject:[tok objectAtIndex:2] forKey:[tok objectAtIndex:1]];
				}else if([tok count] == 4){
					
					DLog(@"MinValue %@",[tok objectAtIndex:3]);
					NSString *minValue = [[[tok objectAtIndex:3] componentsSeparatedByString:@"="] objectAtIndex:1];
					DLog(@"MinValue %@",minValue);
					NSString *keyValue =  [[[tok objectAtIndex:3] componentsSeparatedByString:@"="] objectAtIndex:0];
					[[UserPrefConstants singleton].trxFeeFormulaDict setObject:[tok objectAtIndex:2] forKey:[tok objectAtIndex:1]];
					[[UserPrefConstants singleton].trxFeeFormulaDict setObject:minValue forKey:keyValue];
				}
			}
		}
		else if ([reviseCtrlReg matchesString:s]) {
			NSString *content = [s stringByReplacingOccurrencesOfString:@"[ReviseCtrl]=" withString:@""];
			if (![content isEqualToString:@""]) {
				NSArray *temp = [content componentsSeparatedByString:@";"];
				OrderControl *ordCtrl = [[OrderControl alloc] init];
				NSString *exchg = @"";
				if ([temp count] > 1) {
					NSArray *part1 = [(NSString *)[temp objectAtIndex:0] componentsSeparatedByString:@","];
					int i = 0;
					for (NSString *item in part1) {
						switch (i) {
							case 0: exchg = item;
								break;
							case 1: ordCtrl.orderType = item;
								break;
							default: [ordCtrl.validityList addObject:item];
								break;
						}
						i++;
					}
					NSArray *part2 = [(NSString *)[temp objectAtIndex:1] componentsSeparatedByString:@","];
					for (NSString *field in part2) {
						NSArray *field_parts = [field componentsSeparatedByString:@"="];
						int i = 0;
						NSString *enable_field = @"";
						for (NSString *part in field_parts) {
							switch (i) {
								case 0: [ordCtrl.enable_fields addObject:part];
									enable_field = part;
									break;
								case 1:
								{
									if ([enable_field isEqualToString:@"Disclosed"]) {
										[ordCtrl.disclosed_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
									}
									else if ([enable_field isEqualToString:@"Min"]) {
										[ordCtrl.min_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
									}
									break;
								}
								default:
									break;
							}
							i++;
						}
					}
				}
				TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:exchg];
				if ([ordCtrl.orderType length]>0)
					[trdRules.reviseCtrlConditions setObject:ordCtrl forKey:ordCtrl.orderType];
				
			}
		}
	}
    [UserPrefConstants singleton].userInfoDict = userInfoDict;
	return nil;
}

#pragma mark - Account Client List
- (NSError *)parseAccountClientList:(NSString *)data{
	NSString *content = data;
	content = [[ATPAuthenticate singleton] atpDecompressionProcess:[[ATPAuthenticate singleton] atpDecryptionProcess:content]];
	NSArray *contentCheckingArr = [content componentsSeparatedByString:@","];
	
	GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
	GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
	
	if([[contentCheckingArr[0] uppercaseString] isEqualToString:@"ERROR"])
	{
		//Create Error
		NSDictionary *userInfo = @{
								   NSLocalizedDescriptionKey: [LanguageManager stringForKey:content],
								   NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The response data error.", nil),
								   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Please configuration plist", nil)
								   };
		NSError *aError = [NSError errorWithDomain:@"TradeLogin" code:RESP_CODE_EXCEPTION_PARSE_DATA userInfo:userInfo];
		return aError;
	}
	//Start parsing data here
	int client_account_number = -1;
	int client_code = -1;
	int client_name = -1;
	int broker_code = -1;
	int broker_branch_code = -1;
	int credit_limit = -1;
	int net_cash_limit = -1; //Cash
	int buy_limit = -1;
	int sell_limit = -1;
	int account_status = -1;
	int supported_exchanges = -1;
	int client_acc_exchange = -1;
	int isMarginAcc = -1;
	int account_type = -1;
	
	NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
	NSString *s;
	for (s in arr) {
		if ([metaregex matchesString:s]) {
			s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			int pos = 0;
			for (NSString *token in tokens) {
				if ([token isEqualToString:@","]) {
					client_account_number = pos;
				}
				else if ([token isEqualToString:@"+"]) {
					client_code = pos;
				}
				else if ([token isEqualToString:@"-"]) {
					client_name = pos;
				}
				else if ([token isEqualToString:@"0"]) {
					broker_code = pos;
				}
				else if ([token isEqualToString:@"1"]) {
					broker_branch_code = pos;
				}
				else if ([token isEqualToString:@"2"]) {
					credit_limit = pos;
				}
				else if ([token isEqualToString:@"K"]) {
					net_cash_limit = pos;
				}
				else if ([token isEqualToString:@"3"]) {
					buy_limit = pos;
				}
				else if ([token isEqualToString:@"4"]) {
					sell_limit = pos;
				}
				else if ([token isEqualToString:@"6"]) {
					account_status = pos;
				}
				else if ([token isEqualToString:@"J"]) {
					supported_exchanges = pos;
				}
				else if ([token isEqualToString:@"?"]) {
					client_acc_exchange = pos;
				}
				else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)136]]) {
					isMarginAcc = pos;
				}
				else if ([token isEqualToString:@"P"]) {
					account_type = pos;
				}
				pos++;
			}
		}
		else if ([dataregex matchesString:s]) {
			s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
			NSArray *tokens = [s componentsSeparatedByString:@"|"];
			UserAccountClientData *uacd = [[UserAccountClientData alloc] init];
			
			if (client_account_number != -1) {
				uacd.client_account_number = [tokens objectAtIndex:client_account_number];
			}
			if (client_code != -1) {
				uacd.client_code = [tokens objectAtIndex:client_code];
			}
			if (client_name != -1) {
				uacd.client_name = [tokens objectAtIndex:client_name];
				DLog(@"Client Name %@",[tokens objectAtIndex:client_name]);
			}
			if (broker_code != -1) {
				uacd.broker_code = [tokens objectAtIndex:broker_code];
			}
			if (broker_branch_code != -1) {
				uacd.broker_branch_code = [tokens objectAtIndex:broker_branch_code];
			}
			if (credit_limit != -1) {
				uacd.credit_limit = [[tokens objectAtIndex:credit_limit] doubleValue];
			}
			if (net_cash_limit != -1) {
				uacd.net_cash_limit = [[tokens objectAtIndex:net_cash_limit] doubleValue];
			}
			if (buy_limit != -1) {
				uacd.buy_limit = [[tokens objectAtIndex:buy_limit] doubleValue];
			}
			if (sell_limit != -1) {
				uacd.sell_limit = [[tokens objectAtIndex:sell_limit] doubleValue];
			}
			if (account_status != -1) {
				uacd.account_status = [[tokens objectAtIndex:account_status] intValue];
			}
			if (supported_exchanges != -1) {
				NSString *se = [tokens objectAtIndex:supported_exchanges];
				if (se != nil && [se length] > 0) {
					[uacd.supported_exchanges removeAllObjects];
					[uacd.supported_exchanges addObjectsFromArray:[se componentsSeparatedByString:@","]];
				}
			}
			if (client_acc_exchange != -1) {
				uacd.client_acc_exchange = [tokens objectAtIndex:client_acc_exchange];
			}
			if (isMarginAcc != -1) {
				uacd.isMarginAccount = [[tokens objectAtIndex:isMarginAcc] intValue];
			}
			if (account_type != -1) {
				uacd.account_type = [tokens objectAtIndex:account_type];
			}
			//DLog(@"ACCOUNT-Authen: %@ %f %f %f %d %@ %@", uacd.client_account_number, uacd.credit_limit, uacd.buy_limit, uacd.sell_limit, uacd.account_status, uacd.supported_exchanges, uacd.isMarginAccount == 0 ? @"Non-Margin" : @"Margin");
			[[UserPrefConstants singleton].userAccountlist addObject:uacd];
		}
	}
	//Select first account
	if ([[UserPrefConstants singleton].userAccountlist count]>0) {
		[UserPrefConstants singleton].userSelectedAccount = [[UserPrefConstants singleton].userAccountlist objectAtIndex:0];
		//+++
		//+++keep select first for iphone: Maybe need change logic here
		//Selected Exchange
		NSArray *supportedExchange = [UserPrefConstants singleton].supportedExchanges;
		if (supportedExchange.count > 0) {
			ExchangeData *defaultEx = [Utils getExchange:[SettingManager shareInstance].defaultExchangeCode];
			if (defaultEx) {
				[Utils selectedNewExchange:defaultEx];
			}else{
				ExchangeData *firstEx = supportedExchange.firstObject;
				[SettingManager shareInstance].defaultExchangeCode = firstEx.feed_exchg_code;
				[Utils selectedNewExchange:firstEx];
				[[SettingManager shareInstance] save];
			}
		}
	}
	 [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveUserAccountsList" object:nil];
	return nil;
}



//
- (NSArray<AssetManagementModel *> *)parseAssetMamagement:(NSString *)content{
    NSMutableArray *assets = @[].mutableCopy;
    content = [_atp atpDecryptionProcess:content];
    GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
    GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
    
    AssetManagementMapper * mapper = [AssetManagementMapper new];
    
    NSArray *arr = [content componentsSeparatedByString:@"\r"];
    NSString *s;
    for (s in arr) {
        s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if ([metaregex matchesString:s]) {
            s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
            NSArray *tokens = [s componentsSeparatedByString:@"|"];
            [mapper getIndex:tokens];
        }
        else if ([dataregex matchesString:s]) {
            s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
            NSArray *tokens = [s componentsSeparatedByString:@"|"];
            [assets addObject:[mapper getModel:tokens]];
        }
    }
    return [assets copy];
}
@end
