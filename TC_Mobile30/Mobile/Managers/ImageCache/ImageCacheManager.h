//
//  ImageCacheManager.h
//  TC_Mobile30
//
//  Created by Kaka on 2/15/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface ImageCacheManager : NSObject{
	
}
//Singleton
+ (ImageCacheManager *)shareInstance;

#pragma mark - Main
- (void)clearAllCache;
- (void)clearMemoryCache;
- (void)clearDiskCache;

#pragma mark - Remove
//Default remove from Memory
- (void)removeImageForKey:(NSString *)key completion:(void(^)(void))completion;
//From Disk
- (void)removeImageFromDiskByKey:(NSString *)key completion:(void(^)(void))completion;
#pragma mark - Store
//Cache to memory
- (void)storeImage:(UIImage *)image forKey:(NSString *)key completion:(void(^)(void))completion;
//Cache to disk
- (void)storeImageToDisk:(UIImage *)image forKey:(NSString *)key completion:(void(^)(void))completion;
#pragma mark - Get Image From Cache
- (UIImage *)getImageFromKey:(NSString *)key;
- (UIImage *)getImageMemoryCacheByKey:(NSString *)key;
- (UIImage *)getImageDiskCacheByKey:(NSString *)key;

//***************** No use Assynch **********************************
//#pragma mark - Main
//- (void)storeImage:(UIImage *)image forKey:(NSString *)key;
//- (void)removeImageForKey:(NSString *)key;
//- (UIImage *)getImageForKey:(NSString *)key;

@end
