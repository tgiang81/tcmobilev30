//
//  ImageCacheManager.m
//  TC_Mobile30
//
//  Created by Kaka on 2/15/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "ImageCacheManager.h"
#import "SDImageCache.h"

@interface ImageCacheManager(){
	SDImageCache *_myCache;
	//NSCache *_myCache;
}
@end
@implementation ImageCacheManager
//================== Singleton ==============
+ (ImageCacheManager *)shareInstance{
	static ImageCacheManager *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[ImageCacheManager alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		_myCache = [SDImageCache sharedImageCache];
		//_myCache = [[NSCache alloc] init];
	}
	return  self;
}

#pragma mark - Main
#pragma mark - Clear Cache
- (void)clearAllCache{
	[_myCache clearMemory];
	[_myCache clearDiskOnCompletion:nil];
}

- (void)clearMemoryCache{
	[_myCache clearMemory];
}

- (void)clearDiskCache{
	[_myCache clearDiskOnCompletion:nil];
}
//Default remove from Memory
- (void)removeImageForKey:(NSString *)key completion:(void(^)(void))completion{
	[_myCache removeImageForKey:key withCompletion:completion];
}

//From Disk
- (void)removeImageFromDiskByKey:(NSString *)key completion:(void(^)(void))completion{
	[_myCache removeImageForKey:key fromDisk:YES withCompletion:completion];
}
#pragma mark - Do Cache
//Cache to memory or disk
- (void)storeImage:(UIImage *)image forKey:(NSString *)key completion:(void(^)(void))completion{
	[_myCache storeImage:image forKey:key completion:completion];
}
//Cache to disk
- (void)storeImageToDisk:(UIImage *)image forKey:(NSString *)key completion:(void(^)(void))completion{
	[_myCache storeImage:image forKey:key toDisk:YES completion:completion];
}

#pragma mark - Get Image From Cache
- (UIImage *)getImageFromKey:(NSString *)key{
	return [_myCache imageFromCacheForKey:key];
}

- (UIImage *)getImageMemoryCacheByKey:(NSString *)key{
	return [_myCache imageFromMemoryCacheForKey:key];
}

- (UIImage *)getImageDiskCacheByKey:(NSString *)key{
	return [_myCache imageFromDiskCacheForKey:key];
}

//#pragma mark - Custom Cache
//- (void)storeImage:(UIImage *)image forKey:(NSString *)key{
//	[_myCache setObject:image forKey:key];
//}
//- (void)removeImageForKey:(NSString *)key{
//	[_myCache removeObjectForKey:key];
//}
//
//- (UIImage *)getImageForKey:(NSString *)key{
//	return [_myCache objectForKey:key];
//}
//- (void)clearAllCache{
//	[_myCache removeAllObjects];
//}
@end
