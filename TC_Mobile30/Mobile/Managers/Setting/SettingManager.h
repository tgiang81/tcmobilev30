//
//  SettingManager.h
//  TCiPad
//
//  Created by Kaka on 6/18/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AllCommon.h"
#import "BaseVC.h"

#define DEFAULT_PAGE_LIST  @[@(DefaultPage_Dashboard), @(DefaultPage_Portfolios), @(DefaultPage_Markets),  @(DefaultPage_Quotes), @(DefaultPage_Watchlists)]

#define kUP_COLOR_PRICE		@"009D5F"
#define kDOWN_COLOR_PRICE	@"FE0017"

#define kBuy_COLOR			@"009D5F"
#define kSell_COLOR			@"FE0017"

#define kACCEPT_TOUCH_ID		@"YES"
#define kNOT_ACCEPT_TOUCH_ID	@"NO"
@class BaseVC;
@interface SettingManager : NSObject{
	
}
@property (assign, nonatomic) DefaultPage defaultPage;
@property (assign, nonatomic) BOOL isVisibleNameStock; //Show name or code on Stock Cell
@property (assign, nonatomic) BOOL isVisibleChangePer; //Show ChangePer or Change on Stock Cell
@property (assign, nonatomic) BOOL isActivateTouchID;
@property (assign, nonatomic) BOOL didShowSetupFirst;
@property (strong, nonatomic) NSString *userExperience;
//Color For Up-Down Price
@property (strong, nonatomic) NSString *upPriceColor;
@property (strong, nonatomic) NSString *downPriceColor;
//Buy Sell
@property (strong, nonatomic) NSString *buyColor;
@property (strong, nonatomic) NSString *sellColor;
@property (strong, nonatomic) NSString *unChangedPriceColor;
//Notification
@property (assign, nonatomic) BOOL isOnNotification;
@property (assign, nonatomic) BOOL isOnQuantity;
@property (assign, nonatomic) int currentQuantityType; //0 Unit, 1 Lot
@property (assign, nonatomic) BOOL isOnPrice;
@property (assign, nonatomic) BOOL isOnOrderType2;
// Custom quantity
@property (strong, nonatomic) NSString *customQuantity;
//Exchange
@property (assign, nonatomic) NSString *defaultExchangeCode;

//Float Trading Button
@property (assign, nonatomic) BOOL enableFloatTradingButton;


//Singleton
+ (SettingManager *)shareInstance;
//Show Terms and Conditions
@property (assign, nonatomic) BOOL isShowTermAndCondition;
//Agreed
@property (assign, nonatomic) BOOL isAgreeWithTermAndCondition;

#pragma mark - Main
- (void)save;
- (void)clearSessionData;
- (void)restoreSessionIfNeeded;
+ (NSString *)getQuantityString:(int)type;
+ (NSString *)getSettingDefaultPageName;
+ (BaseVC *)getDefaultPage;


#pragma mark - TOUCH ID UTILS
- (NSString *)getQuickAccessName;
- (NSString *)getQuickAccessPass;

- (void)saveQuickAccess:(NSString *)name pass:(NSString *)pass;
- (void)deleteQuickAccesForBroker:(NSString *)brokerID;
- (void)deleteCurrentQuickAccessBroker;
@end
