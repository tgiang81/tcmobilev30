//
//  SettingManager.m
//  TCiPad
//
//  Created by Kaka on 6/18/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "SettingManager.h"
#import "NSObject+Extension.h"
#import "JNKeychain.h"
#import "MDashBoardVCV3.h"
#import "MWatchlistVC.h"
#import "MPortfolioViewController.h"
#import "MQuoteViewController.h"
#import "MarketsVC.h"
#import "ResearchVC.h"
#import "AppMacro.h"

//======= Key for Setting ======================
#define kSettingKey					@"kSettingKey"
#define kSettingVersionKey			@"kSettingVersionKey"

#define kQUICK_ACCESS_KEY			@"kQUICK_ACCESS_KEY"
#define kQUICK_ACCESS_PASSWORD	@"kQUICK_ACCESS_PASSWORD"
#define kQUICK_ACCESS_USERNAME	@"kQUICK_ACCESS_USERNAME"
@implementation SettingManager

#pragma mark - Init/Singleton
//================== Singleton ==============
+ (SettingManager *)shareInstance{
    static SettingManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[SettingManager alloc] init];
    });
    return _instance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self initDefaultData];
    }
    return  self;
}

#pragma mark - Configs
- (BOOL)isActivateTouchID{
	NSDictionary *quickDict = [USER_DEFAULT dictionaryForKey:kQUICK_ACCESS_KEY];
	if (quickDict) {
		NSString *curBroker = [BrokerManager shareInstance].broker.brokerID;
		if ([quickDict.allKeys containsObject:curBroker]) {
			NSDictionary *quick_BrokerDict = [quickDict objectForKey:curBroker];
			return (quick_BrokerDict != nil);
		}
	}
	return NO;
}

#pragma mark - TOUCH ID UTILS
- (NSString *)getQuickAccessName{
	NSDictionary *quickDict = [USER_DEFAULT dictionaryForKey:kQUICK_ACCESS_KEY];
	if (quickDict) {
		NSString *curBroker = [BrokerManager shareInstance].broker.brokerID;
		NSDictionary *curDict = quickDict[curBroker];
		if (curDict) {
			return curDict[kQUICK_ACCESS_USERNAME];
		}
	}
	return nil;
}
- (NSString *)getQuickAccessPass{
	NSDictionary *quickDict = [USER_DEFAULT dictionaryForKey:kQUICK_ACCESS_KEY];
	if (quickDict) {
		NSString *curBroker = [BrokerManager shareInstance].broker.brokerID;
		NSDictionary *curBrokDict = quickDict[curBroker];
		if (curBrokDict) {
			return curBrokDict[kQUICK_ACCESS_PASSWORD];
		}
	}
	return nil;
}

- (void)saveQuickAccess:(NSString *)name pass:(NSString *)pass{
	NSMutableDictionary *quickDict = [[USER_DEFAULT dictionaryForKey:kQUICK_ACCESS_KEY] mutableCopy];
	if (!quickDict) {
		quickDict = @{}.mutableCopy;
	}
	NSDictionary *curBrokDict = @{kQUICK_ACCESS_USERNAME: name, kQUICK_ACCESS_PASSWORD: pass};
	NSString *curBroker = [BrokerManager shareInstance].broker.brokerID;
	[quickDict setObject:curBrokDict forKey:curBroker];
	[USER_DEFAULT setObject:quickDict forKey:kQUICK_ACCESS_KEY];
	[USER_DEFAULT synchronize];
}

- (void)deleteQuickAccesForBroker:(NSString *)brokerID{
	NSMutableDictionary *quickDict = [[USER_DEFAULT dictionaryForKey:kQUICK_ACCESS_KEY] mutableCopy];
	if (quickDict) {
		[quickDict removeObjectForKey:brokerID];
	}
	[USER_DEFAULT setObject:quickDict forKey:kQUICK_ACCESS_KEY];
	[USER_DEFAULT synchronize];
}

- (void)deleteCurrentQuickAccessBroker{
	NSString *curBroker = [BrokerManager shareInstance].broker.brokerID;
	[self deleteQuickAccesForBroker:curBroker];
}
#pragma mark - Main
//Save
- (void)save{
    NSDictionary *allDataTmp = [self propertiesDictionary];
    NSMutableDictionary *allData = [[NSMutableDictionary alloc] initWithDictionary:allDataTmp];
    
    for (id key in[allDataTmp allKeys]) {
        id value = [allDataTmp objectForKey:key];
        if (!([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]])) {
            [allData removeObjectForKey:key];
        }
    }
    
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSettingKey, kSettingVersionKey];
    [[NSUserDefaults standardUserDefaults] setObject:allData forKey:sessionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//Restore
- (void)restoreSessionIfNeeded {
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSettingKey, kSettingVersionKey];
    NSDictionary *allData = [[NSUserDefaults standardUserDefaults] objectForKey:sessionKey];
    
    if (allData && ![allData isEqual:[NSNull null]]) {
        [self initDefaultData];
        NSArray *keyArray =  [allData allKeys];
        NSUInteger count = [keyArray count];
        for (int i = 0; i < count; i++) {
            NSString *key = [keyArray objectAtIndex:i];
            id obj = [allData objectForKey:key];
            if ([self respondsToSelector:NSSelectorFromString([keyArray objectAtIndex:i])]) {
                [self setValue:obj forKey:[keyArray objectAtIndex:i]];
            }
        }
    }
    else {
        [self initDefaultData];
    }
    [self save];
}

//Default data
- (void)initDefaultData{
    _defaultPage = DefaultPage_Dashboard;
    _isVisibleNameStock = YES;
    _isVisibleChangePer = YES;
    _didShowSetupFirst = NO;
    _isOnQuantity = NO;
    _customQuantity = @"";
    _isOnPrice = YES;
    _downPriceColor = kDOWN_COLOR_PRICE;
    _upPriceColor = kUP_COLOR_PRICE;
    _unChangedPriceColor = @"FFFFFF";
    _buyColor = kBuy_COLOR;
    _sellColor = kSell_COLOR;
    _isOnOrderType2 = YES;
    _currentQuantityType = -1;
    //Notification
    _isOnNotification = NO;
    //Exchange
    _defaultExchangeCode = @"KL"; //Default
	_enableFloatTradingButton = YES;
}
+ (NSString *)getQuantityString:(int)type{
    if(type == 0){
        return @"Unit";
    }else if(type == 1){
        return @"Lot"; // 1 lot = 100 Units
    }
    return @"";
}

//Clear session
- (void)clearSessionData {
    [self initDefaultData];
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSettingKey, kSettingVersionKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:sessionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self save];
}

//Print data
- (void)printDescription {
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSettingKey, kSettingVersionKey];
    NSDictionary *allData = [[NSUserDefaults standardUserDefaults] objectForKey:sessionKey];
    DLog(@"%@", allData);
}
+ (NSString *)getSettingDefaultPageName{
    DefaultPage page = [SettingManager shareInstance].defaultPage;
    return [Utils textTitleFromPage:page];
}

+ (BaseVC *)getDefaultPage{
    DefaultPage page = [SettingManager shareInstance].defaultPage;
    switch (page) {
        case DefaultPage_Dashboard:
            return NEW_VC_FROM_NIB([MDashBoardVCV3 class], [MDashBoardVCV3 nibName]);
        case DefaultPage_News:
            return NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [ResearchVC storyboardID]);
        case DefaultPage_Quotes:
            return NEW_VC_FROM_STORYBOARD(kMQuoteStoryboardName, [MQuoteViewController storyboardID]);
        case DefaultPage_Markets:
            return NEW_VC_FROM_STORYBOARD(kMMarketStoryboardName, [MarketsVC storyboardID]);
        case DefaultPage_Watchlists:
            return NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [MWatchlistVC storyboardID]);
        case DefaultPage_Portfolios:
            return NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [MPortfolioViewController storyboardID]);
    }
    
}
@end
