//
//  LogManager.swift
//  TC_Mobile30
//
//  Created by Tu on 9/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

import Foundation
class LogManager: NSObject{
    //const
    private let logFolderName = ("LogFolder")
    @objc static let loginIdentier = "loginFlow"
    
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    var logs = [XCGLogger]()
    @objc override init() {
        super.init()
        createLogFolder()
    }
    
    // MARK: Shared Instance
    
    func createLogFolder(){
        if let dataPath = LogManager.getDirectoryURL()?.appendingPathComponent(logFolderName)
        {
            do {
                if FileManager.default.fileExists(atPath: dataPath.path) == false{
                    try FileManager.default.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: false, attributes: nil)
                }
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        

    }
    
    @objc static let shared = LogManager()
    @objc func setup(identifier: String = "advancedLogger.fileDestination"){
        let destination = (LogManager.getDirectoryURL()?.absoluteString ?? "") + "/" + logFolderName + "/" + identifier
        self.setup(destination: destination, identifier: identifier)
    }
    @objc func setup(destination: String, identifier: String = "advancedLogger.fileDestination"){
        // Create a file log destination
//        let systemDestination = AppleSystemLogDestination(identifier: "advancedLogger.systemDestination")
//
//        // Optionally set some configuration options
//        systemDestination.outputLevel = .debug
//        systemDestination.showLogIdentifier = false
////        systemDestination.showFunctionName = true
//        systemDestination.showThreadName = true
//        systemDestination.showLevel = true
//        systemDestination.showFileName = true
//        systemDestination.showLineNumber = true
//        systemDestination.showDate = true
//        // Add the destination to the logger
//        log.add(destination: systemDestination)
        
        
        let log = self.getLog(identifier: identifier)
        
        // Create a file log destination
        let fileDestination = FileDestination(writeToFile: destination, identifier: identifier)
        
        // Optionally set some configuration options
        fileDestination.outputLevel = .debug
        fileDestination.showLogIdentifier = false
        fileDestination.showFunctionName = false
        fileDestination.showThreadName = true
        fileDestination.showLevel = false
        fileDestination.showFileName = false
        fileDestination.showLineNumber = false
        fileDestination.showDate = true
        
        
        // Process this destination in the background
        fileDestination.logQueue = XCGLogger.logQueue
        
        // Add the destination to the logger
        log.log.add(destination: fileDestination)
        // Add basic app info, version info etc, to the start of the logs
        log.log.logAppDetails()

        
    }
    
    func getLog(identifier: String) -> (index:Int, log:XCGLogger){
        for (index, tmpLog) in self.logs.enumerated(){
            if(tmpLog.identifier == identifier){
                return (index, tmpLog)
            }
        }
        let log = XCGLogger(identifier: identifier, includeDefaultDestinations: false)
        logs.append(log)
        return (logs.count, log)
    }
    
    @objc func write(methodName: String, content: String){
        self.write(log: logs.last, methodName: methodName, content: content)
    }
    @objc func writeIdentifier(identifier: String, methodName: String, content: String){
        self.write(log: self.getLog(identifier: identifier).log, methodName: methodName, content: content)
    }
    func write(log: XCGLogger?,methodName: String, content: String){
        log?.debug("\(methodName):\(content)")
    }
    
    
    @objc func removeLog(identifier: String){
        let log = self.getLog(identifier: identifier)
        self.logs.remove(at: log.index)
        
        let fileManager = FileManager.default
        do {
            if let documentPath = self.getUrlLogFile(log: log.log, identifier: identifier)
            {
//                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                try fileManager.removeItem(at: documentPath)
            }
            
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    @objc func getUrlLogFile(identifier: String) -> URL?{
        let log = self.getLog(identifier: identifier).log
        return getUrlLogFile(log: log, identifier: identifier)
    }
    func getUrlLogFile(log: XCGLogger, identifier: String) -> URL?{
        if let destimation = log.destination(withIdentifier: identifier) as? FileDestination{
            return destimation.writeToFileURL
        }
        return nil
    }
    static func getDirectoryURL() -> URL?{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if let pathURL = URL.init(string: paths[0]) {
            return pathURL
        }
        return nil
    }
}


