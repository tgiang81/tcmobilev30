//
//  BrokerManager.m
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BrokerManager.h"

static NSString *kSortKeyBy_AppName = @"AppName";
static NSString *kSortKeyBy_Tag		= @"Tag";

//Broker
#define kBrokerModel 		    @"kBrokerModel"
#define kDetailBrokerModel		@"kDetailBrokerModel"

@implementation BrokerManager

//================== Singleton ==============
+ (BrokerManager *)shareInstance{
	static BrokerManager *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[BrokerManager alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
		_showBrokerListInTable = 0;
		_sortBrokerListBy = nil;
		_version = nil;
		_listBrokers = nil;
		_broker = nil;
		_detailBroker = nil;
	}
	return  self;
}

#pragma mark - Overide Main: Cache Broker
- (BrokerModel *)broker{
	if (!_broker || _broker == nil) {
		// Get Current Profile
		NSDictionary *comDict = [USER_DEFAULT objectForKey:kBrokerModel];
		if (!comDict && ![comDict isKindOfClass:[NSDictionary class]]) {
			return nil;
		}
		_broker = [[BrokerModel alloc] initWithDictionary:comDict error:nil];
	}
	return _broker;
}

- (void)setBrokerFromDict:(NSDictionary *)brokerDict{
	_broker = [[BrokerModel alloc] initWithDictionary:brokerDict error:nil];
	[USER_DEFAULT setObject:brokerDict forKey:kBrokerModel];
	[USER_DEFAULT synchronize];
}

//Clear Data
- (void)clearData{
	_broker = nil;
	[USER_DEFAULT removeObjectForKey:kBrokerModel];
	[USER_DEFAULT synchronize];
}

//Save
- (void)save{
	if (_broker) {
		NSDictionary *toDictCart = [_broker toDictionary];
		[USER_DEFAULT setObject:toDictCart forKey:kBrokerModel];
		[USER_DEFAULT synchronize];
	}
}
#pragma mark - Main
- (void)parseDataFromDict:(NSDictionary *)result{
	for (NSString *key in result.allKeys) {
		if ([key isEqualToString:@"ShowBrokerListInTable"]) {
			_showBrokerListInTable = [result[key] integerValue];
		}
		if ([key isEqualToString:@"SortBrokerListBy"]) {
			_sortBrokerListBy = result[key];
		}
		if ([key isEqualToString:@"Version"]) {
			_version = result[key];
		}
		if ([key isEqualToString:@"BrokerList"]) {
			_listBrokers = [BrokerModel arrayOfModelsFromDictionaries:result[key] error:nil].copy;
		}
	}
	
	NSMutableArray *_newListBroker = @[].mutableCopy;
	[_listBrokers enumerateObjectsUsingBlock:^(BrokerModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
		if (obj.enable) {
			[_newListBroker addObject:obj];
		}
	}];
	_listBrokers = [_newListBroker copy];
	//resort listBroker
	if (_listBrokers && _listBrokers.count > 0) {
		_listBrokers = [self sortArray:_listBrokers byKey:_sortBrokerListBy];
	}
}

#pragma mark - Utils VC
- (NSArray *)sortArray:(NSArray *)array byKey:(NSString *)key{
	NSArray *sortedArr;
	if ([key isEqualToString:kSortKeyBy_Tag]) {
		sortedArr = [array sortedArrayUsingComparator: ^(BrokerModel *a, BrokerModel *b) {
			if ( a.tag < b.tag) {
				return (NSComparisonResult)NSOrderedAscending;
			} else if ( a.tag > b.tag) {
				return (NSComparisonResult)NSOrderedDescending;
			}
			return (NSComparisonResult)NSOrderedSame;
		}];
		/* +++ Sort on MutableArray
		 [_listBrokers sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
		 return ([(BrokerModel *)obj1 tag] < [(BrokerModel *)obj2 tag]);
		 }];
		 */
	}else if ([key isEqualToString:kSortKeyBy_AppName]){
		sortedArr = [array sortedArrayUsingComparator: ^(BrokerModel *a, BrokerModel *b) {
			return [a.appName compare:b.appName];
		}];
	}
	return sortedArr;
}
@end
