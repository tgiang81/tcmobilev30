//
//  BrokerManager.h
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BrokerModel.h"
#import "DetailBrokerModel.h"
@class ExchangeInfo;
@interface BrokerManager : NSObject{
	
}
@property (assign, nonatomic) NSInteger showBrokerListInTable;
@property (strong, nonatomic) NSString *sortBrokerListBy;
@property (strong, nonatomic) NSString *version;
@property (strong, nonatomic) NSArray *listBrokers;

@property (strong, nonatomic) BrokerModel *broker;
@property (strong, nonatomic) DetailBrokerModel *detailBroker;

//Singleton
+ (BrokerManager *)shareInstance;

//Parse data from dict
- (void)parseDataFromDict:(NSDictionary *)result;

#pragma mark - mark - Broker Util
- (void)setBrokerFromDict:(NSDictionary *)brokerDict;
//Clear Data
- (void)clearData;
//Save
- (void)save;

#pragma mark - Utils VC
- (NSArray *)sortArray:(NSArray *)array byKey:(NSString *)key;
@end
