//
//  NetworkManager.m
//  TC_Mobile30
//
//  Created by Kaka on 12/25/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "NetworkManager.h"
#import "AppMacro.h"
#import "AuthenticationAPI.h"

@interface NetworkManager(){
	Reachability *_reach;
}
@property (strong, nonatomic) NSTimer *timer;
@end

@implementation NetworkManager
//================== Singleton ==============
+ (NetworkManager *)shared{
	static NetworkManager *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[NetworkManager alloc] init];
	});
	return _instance;
}


- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
		//Add Notification
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReiceveNetworkChangedNotification:) name:kReachabilityChangedNotification object:nil];
	}
	return  self;
}
#pragma mark - Main
- (void)startNotifying{
	_reach = [Reachability reachabilityForInternetConnection];
	// Set the blocks
	_reach.reachableBlock = ^(Reachability*reach){
		// keep in mind this is called on a background thread
		// and if you are updating the UI it needs to happen
		// on the main thread, like this:
		dispatch_async(dispatch_get_main_queue(), ^{
			NSLog(@"REACHABLE!");
		});
	};
	_reach.unreachableBlock = ^(Reachability*reach){
		NSLog(@"UNREACHABLE!");
	};
	[_reach startNotifier];
}

#pragma mark - Check Single Log On
- (void)checkSigleSignOn{
	if (self.networkStatus == NotReachable) {
		return;
	}
	[[AuthenticationAPI shared] checkSingleLogOn];
}
#pragma mark - AutoRefresh
- (NSTimer *) timer {
	if (!_timer) {
		NSTimeInterval timeFire = 1.0;
		_timer = [NSTimer timerWithTimeInterval:timeFire target:self selector:@selector(checkSigleSignOn) userInfo:nil repeats:YES];
	}
	return _timer;
}
#pragma mark - Handler Timer
- (void)startTimerCheckingSigleSignOn{
	[[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (void)stopCheckingSigleSignOn{
	if (_timer) {
		[_timer invalidate];
	}
}
#pragma mark - Configs
- (NetworkStatus)networkStatus{
	if (_reach) {
		return [_reach currentReachabilityStatus];
	}
	return NotReachable;
}

#pragma mark - Handle Notification
- (void)didReiceveNetworkChangedNotification:(NSNotification *)noti{
	if (_reach) {
		[[NSNotificationCenter defaultCenter] postNotificationName:kNetworkDidChangeNotification object:@(self.networkStatus)];
	}
	[self startNotifying];
}
@end
