//
//  NetworkManager.h
//  TC_Mobile30
//
//  Created by Kaka on 12/25/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

#define kNetworkDidChangeNotification	@"kNetworkDidChangeNotification"
@interface NetworkManager : NSObject{
	
}
//================== Singleton ==============
+ (NetworkManager *)shared;
//MAIN
@property (assign, nonatomic) NetworkStatus networkStatus;

//Functions
- (void)startNotifying;

//For Sigle Sig On
- (void)startTimerCheckingSigleSignOn;
- (void)stopCheckingSigleSignOn;

@end
