//
//  ThemeManager.m
//  TC_Mobile30
//
//  Created by Kaka on 11/2/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "ThemeManager.h"
#import "NSObject+Extension.h"
#import "AllCommon.h"

//======= Key for Setting ======================
#define kThemeKey					@"kThemeKey"
#define kThemeVersionKey			@"kThemeVersionKey"

@interface ThemeManager(){
    
}
//No use this
@property (assign, nonatomic) ThemeType curTheme;
@end

@implementation ThemeManager
#pragma mark - Init/Singleton
//================== Singleton ==============
+ (ThemeManager *)shareInstance{
    static ThemeManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[ThemeManager alloc] init];
    });
    return _instance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self initDefaultData];
    }
    return  self;
}

#pragma mark - Main
//Save
- (void)save{
    NSDictionary *allDataTmp = [self propertiesDictionary];
    NSMutableDictionary *allData = [[NSMutableDictionary alloc] initWithDictionary:allDataTmp];
    
    for (id key in[allDataTmp allKeys]) {
        id value = [allDataTmp objectForKey:key];
        if (!([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]])) {
            [allData removeObjectForKey:key];
        }
    }
    
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kThemeKey, kThemeVersionKey];
    [[NSUserDefaults standardUserDefaults] setObject:allData forKey:sessionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//Restore
- (void)restoreSessionIfNeeded {
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kThemeKey, kThemeVersionKey];
    NSDictionary *allData = [[NSUserDefaults standardUserDefaults] objectForKey:sessionKey];
    
    if (allData && ![allData isEqual:[NSNull null]]) {
        [self initDefaultData];
        NSArray *keyArray =  [allData allKeys];
        NSUInteger count = [keyArray count];
        for (int i = 0; i < count; i++) {
            NSString *key = [keyArray objectAtIndex:i];
            id obj = [allData objectForKey:key];
            if ([self respondsToSelector:NSSelectorFromString([keyArray objectAtIndex:i])]) {
                [self setValue:obj forKey:[keyArray objectAtIndex:i]];
            }
        }
    }
    else {
        [self initDefaultData];
    }
    [self setTheme:_curTheme];
    [self save];
}

//Default data
- (void)initDefaultData{
    [self updateNewTheme:ThemeType_Light];
}

//Clear session
- (void)clearSessionData {
    [self initDefaultData];
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kThemeKey, kThemeVersionKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:sessionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self save];
}

//Print data
- (void)printDescription {
    NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kThemeKey, kThemeVersionKey];
    NSDictionary *allData = [[NSUserDefaults standardUserDefaults] objectForKey:sessionKey];
    DLog(@"%@", allData);
}

#pragma mark - ACTION
- (void)setTheme:(ThemeType)theme{
    switch (theme) {
        case ThemeType_Dark:{
			[SettingManager shareInstance].unChangedPriceColor = @"000000";
			//Main
			_tintColor = @"119BF4";
			_subTintColor = @"000000";
			_mainBgColor = @"ffffff";
			_subBgColor = @"B2B2B2";
			_deleteColor = @"ef7474";
			
			_navTintColor = @"ffffff";
			_navBgColor = @"000000";
			
			_tabTintColor = @"000000";
			_tabBgColor = @"ffffff";
			
			_navTextTintColor = @"ffffff";
			_instructionColor = @"00BAD8";
			_baselineColor = @"DFDFDF";
			_shadowColor = @"EFEFEF";
			
			_logoTintColor = @"000000";
			
			//Text
			_mainTextColor = @"000000";
			_subTextColor = @"B2B2B2";
			
			//ITEMS
			_switchOnColor = @"119BF4";
			_switchOffColor = @"545D63";
			//PageMenu
			_unselectedTextColor = @"B2B2B2";
			_selectedTextColor = @"000000";
			_indicatorColor = @"000000";
			
			//Quote
			_quote_subHeaderBg = @"E0E0E0";
			_quote_tintIconColor = @"7F7F7F";
			_quote_subTintIconColor = @"000000";
			
			//Cell
			_bgCellColor = @"ffffff";
			_headerCellColor = @"F0F0F0";

			
			//Dropdown
			_bgDropdownColor = @"B2B2B2";
			
			
			//All News
			_allNews_ContentCell = @"ffffff";
			
			//Stock Tracker Screen
			_stt_bgColor = @"ffffff";
			_stt_headerBgColor = @"B2B2B2";
			_stt_bgTopColor = @"F5F5F5";
			_stt_bgExChangeColor = @"ffffff";
			_stt_bgWatchListColor = @"ffffff";
			_stt_bgFilterColor = @"ffffff";
			_stt_bgApplyColor = @"ffffff";
			_stt_tintActionButtonColor = @"000000";
			_stt_tintDropDownButtonColor = @"000000";
			_stt_tintExchangeDropDownButtonColor = @"000000";
			_stt_textExChangeColor = @"353535";
			_stt_textWatchListColor = @"353535";
			_stt_textFilterColor = @"353535";
			_stt_textApplyColor = @"000000";
			_stt_textItemDropDownColor = @"000000";
			_stt_textCellColor = @"000000";
			_stt_borderCellColor = @"000000";
			_stt_bgCellColor = @"ffffff";
			_stt_bgContentCellColor = @"ffffff";
			
			//Order Books
			_ob_bgTop1 = @"F5F5F5";
			_ob_bgTop2 = @"ffffff";
			_ob_bgTop3 = @"ffffff";
			_ob_textColor = @"000000";
			_ob_borderColor = @"353535";
			_ob_unSelectTextColor = @"B2B2B2";
			_ob_seperatorButtons = @"000000";
			_ob_DateTextColor = @"000000";
			_ob_HeaderBg = @"F5F5F5";
			_ob_Cell1Bg = @"ffffff";
			_ob_Cell2Bg = @"ffffff";
			_ob_CellTextColor = @"000000";
			
			_ob_FilterBorderCell = @"B2B2B2";
			_ob_FilterSelectedCellBg = @"3F97FC";
			_ob_FilterTextColorCell = @"353535";
            _ob_FilterTextHeader = _ob_FilterTextColorCell;
			_ob_FilterSelectedTextColorCell = @"ffffff";
			_ob_FilterUnSelectedCellBg = @"ffffff";
			
			_ob_FilterBorderButton = @"ffffff";
			_ob_FilterSelectedButtonBg = @"3F97FC";
			_ob_FilterUnSelectedButtonBg = @"999982";
			_ob_FilterSelectedTextColorButton = @"ffffff";
			_ob_FilterUnSelectedTextColorButton = @"ffffff";
			
			//Date Picker
			_dp_tintColor = @"353535";
			_dp_today = @"000000";
			_dp_textColor = @"000000";
			_dp_unSelectTextColor = @"ffffff";
			
			
			//Search Stock
			_st_searchBarTintColor = @"000000";
			_st_cellBg1 = @"ffffff";
			_st_cellBg2 = @"F5F5F5";
			_st_cellTextColor = @"000000";
			
			//TradeVC
			_trade_StockInfoBg = @"ffffff";
			_trade_StockInfoText = @"000000";
            _trade_StockCompanyText = @"888888";
			_trade_MarketBg = @"F5F5F5";
			_trade_MarketText = @"000000";
			_trade_MarketHeaderBg = @"B2B2B2";
			_trade_MarketHeaderText = @"000000";
			_trade_MarketCellBg = @"ffffff";
			_trade_MarketCellText = @"000000";
			_trade_OrderInfoBg = @"F5F5F5";
			_trade_OrderInfoText = @"000000";
			_trade_AccountBg = @"F5F5F5";
			_trade_AccountText = @"000000";
			_trade_InfoDropdownCellBg = @"F5F5F5";
			_trade_InfoDropdownCellText = @"000000";
			_trade_InfoInputTextCellBg = @"F5F5F5";
			_trade_InfoInputTextCellColor = @"000000";
			_trade_InfoCheckBoxBg = @"ffffff";
			_trade_InfoCheckBoxTint = @"000000";
			_trade_InfoCellText = @"000000";
			_trade_BuyBg = @"19B393";
			_trade_SellBg = @"F15054";
			_trade_UnSelectBg = @"BDBEBD";
			_trade_BuyText = @"ffffff";
			_trade_SellText = @"ffffff";
			_trade_BorderSelect = @"374D59";
			_trade_BorderUnSelect = @"374D59";
			_trade_SearchButtonTint = @"000000";
			
			_pin_topBg = @"F5F5F5";
			_pin_titleColor = @"000000";
			_pin_okBg = @"B2B2B2";
			_pin_cancelBg = @"B2B2B2";
			_pin_okTitle = @"000000";
			_pin_cancelTitle = @"000000";
			_pin_rememberTitle = @"000000";
			
			_st_titleColor = @"ffffff";
			_st_textColor = @"000000";
			_st_hightLightTextColor = @"000000";
			_st_submitBg = @"B2B2B2";
			_st_closeBg = @"B2B2B2";
			_st_submitTitle = @"000000";
			_st_closeTitle = @"000000";
			_st_bgCell1 = @"ffffff";
			_st_bgCell2 = @"F5F5F5";
			
			_os_topBg = @"F5F5F5";
			_os_topTextColor = @"000000";
			_os_LabelTextColor = @"000000";
			_os_TextColor = @"000000";
			_os_openBg = @"B2B2B2";
			_os_doneBg = @"B2B2B2";
			_os_openTextColor = @"000000";
			_os_doneTextColor = @"000000";
			
			//Order Detail
			_ot_topBg = @"ffffff";
			_ot_topTextColor = @"B2B2B2";
			_ot_topHightLightTextColor = @"000000";
			_ot_HightLightTextColor = @"000000";
			_ot_TextColor = @"000000";
			_ot_NormalButtonBg = @"3F97FC";
			_ot_SpecialButtonBg = @"F20000";
			_ot_NormalButtonTextColor = @"ffffff";
			_ot_SpecialButtonTextColor = @"ffffff";
			_ot_OtherButtonBg = @"000000";
			_ot_OtherButtonTextColor = @"ffffff";
			
			//Alert
			_a_topBg = @"F5F5F5";
			_a_unSelectedTop = _ob_unSelectTextColor;
			_a_textColor = _ob_textColor;
			_a_viewBg = @"ffffff";
			_a_editBg = @"B2B2B2";
			_a_deleteBg = @"C90030";
			_a_cellBg = @"ffffff";
			_a_cellSwitchTint = _switchOnColor;
			_a_cellTextColor = @"000000";
			
			_a_settingTintColor = @"000000";
            _a_settingTitleColor = @"000000";
            _a_settingLabelColor = @"565656";
			_a_settingDropDownBg = @"EFEFEF";
			_a_settingConfirmBg = @"B2B2B2";
			_a_settingConfirmText = @"ffffff";
			_a_settingCancelBg = @"F7F7F7";
			_a_settingCancelText = _a_settingTintColor;
			
			_as_tintColor = @"B2B2B2";
			_as_messColor = @"000000";
			_as_TitleColor = @"000000";
			_as_bg = @"ffffff";
			
			//Setting Alert
			_sta_bgView = _a_viewBg;
			_sta_bgCell = _a_cellBg;
			_sta_textColor = @"000000";
			_sta_switchTintColor = _a_cellSwitchTint;
            _sta_bgHightLightCell = @"E2E2E2";
			//DashBoard
			_db_bgDropDown = @"ffffff";
            _db_borderColor = @"EFEFEF";
			_db_bg = @"F5F5F5";
			_db_ItemViewBg = @"ffffff";
            _db_NewsBgCell = @"ffffff";
			_db_textColor = @"000000";
            _db_headerTextColor = @"000000";
            _db_headerSubtextColor = @"#8E8E8E";
            _db_subItemTextColor = @"AAAAAA";
            _db_textHightLightColor = @"000000";
			_db_textColorDropDown = @"000000";
			_db_textColorItemDropDown = @"000000";
			_db_tintColorIconDropDown = @"000000";
			
			//Account
			_acc_bg = @"ffffff";
			_acc_tint = @"000000";
			_acc_bgCell = @"ffffff";
			_acc_bgImageCell = @"000000";
			_acc_textCell = @"000000";
			_acc_searchPlaceHolder = @"D5D5D5";
			_accDetail_textCell = @"ffffff";
			_acc_searchBorder = @"000000";
			
			//Check Auth
			_ca_textColor = @"000000";
			_ca_mainColor = @"ffffff";
			_ca_tintColor = _tintColor;
            _ca_cancelColor = _deleteColor;
        }
            break;
        case ThemeType_Red:{
			[SettingManager shareInstance].unChangedPriceColor = @"000000";
			//Main
			_tintColor = @"119BF4";
			_subTintColor = @"000000";
			_mainBgColor = @"ffffff";
			_subBgColor = @"B2B2B2";
			_deleteColor = @"ef7474";
			
			_navTintColor = @"ffffff";
			_navBgColor = @"FF0000";
			
			_tabTintColor = @"000000";
			_tabBgColor = @"ffffff";
			
			_navTextTintColor = @"ffffff";
			_instructionColor = @"00BAD8";
			_baselineColor = @"DFDFDF";
			_shadowColor = @"EFEFEF";

			_logoTintColor = @"FF0000";
			//Text
			_mainTextColor = @"000000";
			_subTextColor = @"B2B2B2";
			
			//ITEMS
			_switchOnColor = @"119BF4";
			_switchOffColor = @"545D63";
			
			//PageMenu
			_unselectedTextColor = @"6C6C6C";
			_selectedTextColor = @"000000";
			_indicatorColor = @"000000";
			
			//Quote
			_quote_subHeaderBg = @"E0E0E0";
			_quote_tintIconColor = @"B2B2B2";
			_quote_subTintIconColor = @"000000";
			
			//Cell
			_bgCellColor = @"ffffff";
			_headerCellColor = @"F0F0F0";

			//Dropdown
			_bgDropdownColor = @"B2B2B2";
			
			
			//All News
			_allNews_ContentCell = @"ffffff";
			
			//Stock Tracker Screen
			_stt_bgColor = @"ffffff";
			_stt_headerBgColor = @"B2B2B2";
			_stt_bgTopColor = @"F5F5F5";
			_stt_bgExChangeColor = @"ffffff";
			_stt_bgWatchListColor = @"ffffff";
			_stt_bgFilterColor = @"ffffff";
			_stt_bgApplyColor = @"ffffff";
			_stt_tintActionButtonColor = @"000000";
			_stt_tintDropDownButtonColor = @"000000";
			_stt_tintExchangeDropDownButtonColor = @"000000";
			_stt_textExChangeColor = @"353535";
			_stt_textWatchListColor = @"353535";
			_stt_textFilterColor = @"353535";
			_stt_textApplyColor = @"000000";
			_stt_textItemDropDownColor = @"000000";
			_stt_textCellColor = @"000000";
			_stt_borderCellColor = @"000000";
			_stt_bgCellColor = @"ffffff";
			_stt_bgContentCellColor = @"ffffff";
			
			//Order Books
			_ob_bgTop1 = @"F5F5F5";
			_ob_bgTop2 = @"ffffff";
			_ob_bgTop3 = @"ffffff";
			_ob_textColor = @"000000";
			_ob_borderColor = @"353535";
			_ob_unSelectTextColor = @"B2B2B2";
			_ob_seperatorButtons = @"000000";
			_ob_DateTextColor = @"000000";
			_ob_HeaderBg = @"F5F5F5";
			_ob_Cell1Bg = @"ffffff";
			_ob_Cell2Bg = @"ffffff";
			_ob_CellTextColor = @"000000";
			
			_ob_FilterBorderCell = @"B2B2B2";
			_ob_FilterSelectedCellBg = @"3F97FC";
			_ob_FilterTextColorCell = @"353535";
            _ob_FilterTextHeader = _ob_FilterTextColorCell;
			_ob_FilterSelectedTextColorCell = @"ffffff";
			_ob_FilterUnSelectedCellBg = @"ffffff";
			
			_ob_FilterBorderButton = @"ffffff";
			_ob_FilterSelectedButtonBg = @"3F97FC";
			_ob_FilterUnSelectedButtonBg = @"999982";
			_ob_FilterSelectedTextColorButton = @"ffffff";
			_ob_FilterUnSelectedTextColorButton = @"ffffff";
			
			//Date Picker
			_dp_tintColor = @"353535";
			_dp_today = @"000000";
			_dp_textColor = @"000000";
			_dp_unSelectTextColor = @"ffffff";
			
			
			//Search Stock
			_st_searchBarTintColor = @"000000";
			_st_cellBg1 = @"ffffff";
			_st_cellBg2 = @"F5F5F5";
			_st_cellTextColor = @"000000";
			
			//TradeVC
			_trade_StockInfoBg = @"ffffff";
			_trade_StockInfoText = @"000000";
            _trade_StockCompanyText = @"888888";
			_trade_MarketBg = @"F5F5F5";
			_trade_MarketText = @"000000";
			_trade_MarketHeaderBg = @"B2B2B2";
			_trade_MarketHeaderText = @"000000";
			_trade_MarketCellBg = @"ffffff";
			_trade_MarketCellText = @"000000";
			_trade_OrderInfoBg = @"F5F5F5";
			_trade_OrderInfoText = @"000000";
			_trade_AccountBg = @"F5F5F5";
			_trade_AccountText = @"000000";
			_trade_InfoDropdownCellBg = @"F5F5F5";
			_trade_InfoDropdownCellText = @"000000";
			_trade_InfoInputTextCellBg = @"F5F5F5";
			_trade_InfoInputTextCellColor = @"000000";
			_trade_InfoCheckBoxBg = @"ffffff";
			_trade_InfoCheckBoxTint = @"000000";
			_trade_InfoCellText = @"000000";
			_trade_BuyBg = @"19B393";
			_trade_SellBg = @"F15054";
			_trade_UnSelectBg = @"BDBEBD";
			_trade_BuyText = @"ffffff";
			_trade_SellText = @"ffffff";
			_trade_BorderSelect = @"374D59";
			_trade_BorderUnSelect = @"374D59";
			_trade_SearchButtonTint = @"000000";
			
			_pin_topBg = @"F5F5F5";
			_pin_titleColor = @"000000";
			_pin_okBg = @"B2B2B2";
			_pin_cancelBg = @"B2B2B2";
			_pin_okTitle = @"000000";
			_pin_cancelTitle = @"000000";
			_pin_rememberTitle = @"000000";
			
			_st_titleColor = @"ffffff";
			_st_textColor = @"000000";
			_st_hightLightTextColor = @"000000";
			_st_submitBg = @"B2B2B2";
			_st_closeBg = @"B2B2B2";
			_st_submitTitle = @"000000";
			_st_closeTitle = @"000000";
			_st_bgCell1 = @"ffffff";
			_st_bgCell2 = @"F5F5F5";
			
			_os_topBg = @"F5F5F5";
			_os_topTextColor = @"000000";
			_os_LabelTextColor = @"000000";
			_os_TextColor = @"000000";
			_os_openBg = @"B2B2B2";
			_os_doneBg = @"B2B2B2";
			_os_openTextColor = @"000000";
			_os_doneTextColor = @"000000";
			
			//Order Detail
			_ot_topBg = @"ffffff";
			_ot_topTextColor = @"B2B2B2";
			_ot_topHightLightTextColor = @"000000";
			_ot_HightLightTextColor = @"000000";
			_ot_TextColor = @"000000";
			_ot_NormalButtonBg = @"3F97FC";
			_ot_SpecialButtonBg = @"F20000";
			_ot_NormalButtonTextColor = @"ffffff";
			_ot_SpecialButtonTextColor = @"ffffff";
			_ot_OtherButtonBg = @"000000";
			_ot_OtherButtonTextColor = @"ffffff";
			
			//Alert
			_a_topBg = @"F5F5F5";
			_a_unSelectedTop = _ob_unSelectTextColor;
			_a_textColor = _ob_textColor;
			_a_viewBg = @"ffffff";
			_a_editBg = @"B2B2B2";
			_a_deleteBg = @"C90030";
			_a_cellBg = @"ffffff";
			_a_cellSwitchTint = _switchOnColor;
			_a_cellTextColor = @"000000";
			
			_a_settingTintColor = @"000000";
            _a_settingTitleColor = @"000000";
            _a_settingLabelColor = @"565656";
            _a_settingSwitchTintColor = _switchOnColor;
			_a_settingDropDownBg = @"EFEFEF";
			_a_settingConfirmBg = @"B2B2B2";
			_a_settingConfirmText = @"ffffff";
			_a_settingCancelBg = @"F7F7F7";
			_a_settingCancelText = _a_settingTintColor;
			
			_as_tintColor = @"B2B2B2";
			_as_messColor = @"000000";
			_as_TitleColor = @"000000";
			_as_bg = @"ffffff";
			
			//Setting Alert
			_sta_bgView = _a_viewBg;
			_sta_bgCell = _a_cellBg;
			_sta_textColor = @"000000";
			_sta_switchTintColor = _a_cellSwitchTint;
			_sta_bgHightLightCell = @"E2E2E2";
			//DashBoard
			_db_bgDropDown = @"ffffff";
            _db_borderColor = @"EFEFEF";
			_db_bg = @"F5F5F5";
			_db_ItemViewBg = @"ffffff";
            _db_NewsBgCell = @"ffffff";
			_db_textColor = @"000000";
            _db_headerTextColor = @"000000";
            _db_headerSubtextColor = @"#8E8E8E";
            _db_subItemTextColor = @"AAAAAA";
            _db_textHightLightColor = @"000000";
			_db_textColorDropDown = @"000000";
			_db_textColorItemDropDown = @"000000";
			_db_tintColorIconDropDown = @"000000";
			
			//Account
			_acc_bg = @"ffffff";
			_acc_tint = @"000000";
			_acc_bgCell = @"ffffff";
			_acc_bgImageCell = @"000000";
			_acc_textCell = @"000000";
			_acc_searchPlaceHolder = @"D5D5D5";
			_accDetail_textCell = @"ffffff";
			_acc_searchBorder = @"000000";
			
			//Check Auth
			_ca_textColor = @"000000";
			_ca_mainColor = @"ffffff";
			_ca_tintColor = _tintColor;
            _ca_cancelColor = _deleteColor;
        }
            break;
        case ThemeType_WhiteBlue:{
			[SettingManager shareInstance].unChangedPriceColor = @"000000";
			//Main
			_tintColor = @"119BF4";
			_subTintColor = @"000000";
			_mainBgColor = @"ffffff";
			_subBgColor = @"B2B2B2";
			_deleteColor = @"ef7474";
			
			_navTintColor = @"ffffff";
			_navBgColor = @"0D31C9";
			
			_tabTintColor = @"000000";
			_tabBgColor = @"ffffff";
			
			_navTextTintColor = @"ffffff";
			_instructionColor = @"00BAD8";
			_baselineColor = @"DFDFDF";
			_shadowColor = @"EFEFEF";

			_logoTintColor = @"0D31C9";
			
			//Text
			_mainTextColor = @"000000";
			_subTextColor = @"B2B2B2";
			
			//ITEMS
			_switchOnColor = @"119BF4";
			_switchOffColor = @"545D63";
			
			//PageMenu
			_unselectedTextColor = @"B2B2B2";
			_selectedTextColor = @"000000";
			_indicatorColor = @"000000";
			
			//Quote
			_quote_subHeaderBg = @"E0E0E0";
			_quote_tintIconColor = @"B2B2B2";
			_quote_subTintIconColor = @"000000";
			
			//Cell
			_bgCellColor = @"ffffff";
			_headerCellColor = @"F0F0F0";

			//Dropdown
			_bgDropdownColor = @"B2B2B2";
			
			
			//All News
			_allNews_ContentCell = @"ffffff";
			
			//Stock Tracker Screen
			_stt_bgColor = @"ffffff";
			_stt_headerBgColor = @"B2B2B2";
			_stt_bgTopColor = @"F5F5F5";
			_stt_bgExChangeColor = @"ffffff";
			_stt_bgWatchListColor = @"ffffff";
			_stt_bgFilterColor = @"ffffff";
			_stt_bgApplyColor = @"ffffff";
			_stt_tintActionButtonColor = @"000000";
			_stt_tintDropDownButtonColor = @"000000";
			_stt_tintExchangeDropDownButtonColor = @"000000";
			_stt_textExChangeColor = @"353535";
			_stt_textWatchListColor = @"353535";
			_stt_textFilterColor = @"353535";
			_stt_textApplyColor = @"000000";
			_stt_textItemDropDownColor = @"000000";
			_stt_textCellColor = @"000000";
			_stt_borderCellColor = @"000000";
			_stt_bgCellColor = @"ffffff";
			_stt_bgContentCellColor = @"ffffff";
			
			//Order Books
			_ob_bgTop1 = @"F5F5F5";
			_ob_bgTop2 = @"ffffff";
			_ob_bgTop3 = @"ffffff";
			_ob_textColor = @"000000";
			_ob_borderColor = @"353535";
			_ob_unSelectTextColor = @"B2B2B2";
			_ob_seperatorButtons = @"000000";
			_ob_DateTextColor = @"000000";
			_ob_HeaderBg = @"F5F5F5";
			_ob_Cell1Bg = @"ffffff";
			_ob_Cell2Bg = @"ffffff";
			_ob_CellTextColor = @"000000";
			
			_ob_FilterBorderCell = @"B2B2B2";
			_ob_FilterSelectedCellBg = @"3F97FC";
			_ob_FilterTextColorCell = @"353535";
            _ob_FilterTextHeader = _ob_FilterTextColorCell;
			_ob_FilterSelectedTextColorCell = @"ffffff";
			_ob_FilterUnSelectedCellBg = @"ffffff";
			
			_ob_FilterBorderButton = @"ffffff";
			_ob_FilterSelectedButtonBg = @"3F97FC";
			_ob_FilterUnSelectedButtonBg = @"999982";
			_ob_FilterSelectedTextColorButton = @"ffffff";
			_ob_FilterUnSelectedTextColorButton = @"ffffff";
			
			//Date Picker
			_dp_tintColor = @"353535";
			_dp_today = @"000000";
			_dp_textColor = @"000000";
			_dp_unSelectTextColor = @"ffffff";
			
			
			//Search Stock
			_st_searchBarTintColor = @"000000";
			_st_cellBg1 = @"ffffff";
			_st_cellBg2 = @"F5F5F5";
			_st_cellTextColor = @"000000";
			
			//TradeVC
			_trade_StockInfoBg = @"ffffff";
			_trade_StockInfoText = @"000000";
            _trade_StockCompanyText = @"888888";
			_trade_MarketBg = @"F5F5F5";
			_trade_MarketText = @"000000";
			_trade_MarketHeaderBg = @"B2B2B2";
			_trade_MarketHeaderText = @"000000";
			_trade_MarketCellBg = @"ffffff";
			_trade_MarketCellText = @"000000";
			_trade_OrderInfoBg = @"F5F5F5";
			_trade_OrderInfoText = @"000000";
			_trade_AccountBg = @"F5F5F5";
			_trade_AccountText = @"000000";
			_trade_InfoDropdownCellBg = @"F5F5F5";
			_trade_InfoDropdownCellText = @"000000";
			_trade_InfoInputTextCellBg = @"F5F5F5";
			_trade_InfoInputTextCellColor = @"000000";
			_trade_InfoCheckBoxBg = @"ffffff";
			_trade_InfoCheckBoxTint = @"000000";
			_trade_InfoCellText = @"000000";
			_trade_BuyBg = @"19B393";
			_trade_SellBg = @"F15054";
			_trade_UnSelectBg = @"BDBEBD";
			_trade_BuyText = @"ffffff";
			_trade_SellText = @"ffffff";
			_trade_BorderSelect = @"374D59";
			_trade_BorderUnSelect = @"374D59";
			_trade_SearchButtonTint = @"000000";
			
			_pin_topBg = @"F5F5F5";
			_pin_titleColor = @"000000";
			_pin_okBg = @"B2B2B2";
			_pin_cancelBg = @"B2B2B2";
			_pin_okTitle = @"000000";
			_pin_cancelTitle = @"000000";
			_pin_rememberTitle = @"000000";
			
			_st_titleColor = @"ffffff";
			_st_textColor = @"000000";
			_st_hightLightTextColor = @"000000";
			_st_submitBg = @"B2B2B2";
			_st_closeBg = @"B2B2B2";
			_st_submitTitle = @"000000";
			_st_closeTitle = @"000000";
			_st_bgCell1 = @"ffffff";
			_st_bgCell2 = @"F5F5F5";
			
			_os_topBg = @"F5F5F5";
			_os_topTextColor = @"000000";
			_os_LabelTextColor = @"000000";
			_os_TextColor = @"000000";
			_os_openBg = @"B2B2B2";
			_os_doneBg = @"B2B2B2";
			_os_openTextColor = @"000000";
			_os_doneTextColor = @"000000";
			
			//Order Detail
			_ot_topBg = @"ffffff";
			_ot_topTextColor = @"B2B2B2";
			_ot_topHightLightTextColor = @"000000";
			_ot_HightLightTextColor = @"000000";
			_ot_TextColor = @"000000";
			_ot_NormalButtonBg = @"3F97FC";
			_ot_SpecialButtonBg = @"F20000";
			_ot_NormalButtonTextColor = @"ffffff";
			_ot_SpecialButtonTextColor = @"ffffff";
			_ot_OtherButtonBg = @"000000";
			_ot_OtherButtonTextColor = @"ffffff";
			
			//Alert
			_a_topBg = @"F5F5F5";
			_a_unSelectedTop = _ob_unSelectTextColor;
			_a_textColor = _ob_textColor;
			_a_viewBg = @"ffffff";
			_a_editBg = @"B2B2B2";
			_a_deleteBg = @"C90030";
			_a_cellBg = @"ffffff";
			_a_cellSwitchTint = _switchOnColor;
			_a_cellTextColor = @"000000";
			
			_a_settingTintColor = @"000000";
            _a_settingTitleColor = @"000000";
            _a_settingLabelColor = @"565656";
            _a_settingSwitchTintColor = _switchOnColor;
			_a_settingDropDownBg = @"EFEFEF";
			_a_settingConfirmBg = @"B2B2B2";
			_a_settingConfirmText = @"ffffff";
			_a_settingCancelBg = @"F7F7F7";
			_a_settingCancelText = _a_settingTintColor;
			
			_as_tintColor = @"B2B2B2";
			_as_messColor = @"000000";
			_as_TitleColor = @"000000";
			_as_bg = @"ffffff";
			
			//Setting Alert
			_sta_bgView = _a_viewBg;
			_sta_bgCell = _a_cellBg;
			_sta_textColor = @"000000";
			_sta_switchTintColor = _a_cellSwitchTint;
			_sta_bgHightLightCell = @"E2E2E2";
			//DashBoard
			_db_bgDropDown = @"ffffff";
            _db_borderColor = @"EFEFEF";
			_db_bg = @"F5F5F5";
			_db_ItemViewBg = @"ffffff";
            _db_NewsBgCell = @"ffffff";
			_db_textColor = @"000000";
            _db_headerTextColor = @"000000";
            _db_headerSubtextColor = @"#8E8E8E";
            _db_subItemTextColor = @"AAAAAA";
            _db_textHightLightColor = @"000000";
			_db_textColorDropDown = @"000000";
			_db_textColorItemDropDown = @"000000";
			_db_tintColorIconDropDown = @"000000";
			
			//Account
			_acc_bg = @"ffffff";
			_acc_tint = @"000000";
			_acc_bgCell = @"ffffff";
			_acc_bgImageCell = @"000000";
			_acc_textCell = @"000000";
			_acc_searchPlaceHolder = @"D5D5D5";
			_accDetail_textCell = @"ffffff";
			_acc_searchBorder = @"000000";
			
			//Check Auth
			_ca_textColor = @"000000";
			_ca_mainColor = @"ffffff";
			_ca_tintColor = _tintColor;
            _ca_cancelColor = _deleteColor;
            
        }
            break;
        case ThemeType_Green:{
			[SettingManager shareInstance].unChangedPriceColor = @"000000";
			//Main
			_tintColor = @"119BF4";
			_subTintColor = @"000000";
			_mainBgColor = @"ffffff";
			_subBgColor = @"B2B2B2";
			_deleteColor = @"ef7474";
			
			_navTintColor = @"ffffff";
			_navBgColor = @"22AC11";
			
			_tabTintColor = @"000000";
			_tabBgColor = @"ffffff";
			
			_navTextTintColor = @"ffffff";
			_instructionColor = @"00BAD8";
			_baselineColor = @"DFDFDF";
			_shadowColor = @"EFEFEF";

			_logoTintColor = @"22AC11";
			//Text
			_mainTextColor = @"000000";
			_subTextColor = @"B2B2B2";
			
			//ITEMS
			_switchOnColor = @"119BF4";
			_switchOffColor = @"545D63";
			
			//PageMenu
			_unselectedTextColor = @"B2B2B2";
			_selectedTextColor = @"000000";
			_indicatorColor = @"000000";
			
			//Quote
			_quote_subHeaderBg = @"E0E0E0";
			_quote_tintIconColor = @"B2B2B2";
			_quote_subTintIconColor = @"000000";
			
			//Cell
			_bgCellColor = @"ffffff";
			_headerCellColor = @"F0F0F0";

			//Dropdown
			_bgDropdownColor = @"B2B2B2";
			
			
			//All News
			_allNews_ContentCell = @"ffffff";
			
			//Stock Tracker Screen
			_stt_bgColor = @"ffffff";
			_stt_headerBgColor = @"B2B2B2";
			_stt_bgTopColor = @"F5F5F5";
			_stt_bgExChangeColor = @"ffffff";
			_stt_bgWatchListColor = @"ffffff";
			_stt_bgFilterColor = @"ffffff";
			_stt_bgApplyColor = @"ffffff";
			_stt_tintActionButtonColor = @"000000";
			_stt_tintDropDownButtonColor = @"000000";
			_stt_tintExchangeDropDownButtonColor = @"000000";
			_stt_textExChangeColor = @"353535";
			_stt_textWatchListColor = @"353535";
			_stt_textFilterColor = @"353535";
			_stt_textApplyColor = @"000000";
			_stt_textItemDropDownColor = @"000000";
			_stt_textCellColor = @"000000";
			_stt_borderCellColor = @"000000";
			_stt_bgCellColor = @"ffffff";
			_stt_bgContentCellColor = @"ffffff";
			
			//Order Books
			_ob_bgTop1 = @"F5F5F5";
			_ob_bgTop2 = @"ffffff";
			_ob_bgTop3 = @"ffffff";
			_ob_textColor = @"000000";
			_ob_borderColor = @"353535";
			_ob_unSelectTextColor = @"B2B2B2";
			_ob_seperatorButtons = @"000000";
			_ob_DateTextColor = @"000000";
			_ob_HeaderBg = @"F5F5F5";
			_ob_Cell1Bg = @"ffffff";
			_ob_Cell2Bg = @"ffffff";
			_ob_CellTextColor = @"000000";
			
			_ob_FilterBorderCell = @"B2B2B2";
			_ob_FilterSelectedCellBg = @"3F97FC";
			_ob_FilterTextColorCell = @"353535";
            _ob_FilterTextHeader = _ob_FilterTextColorCell;
			_ob_FilterSelectedTextColorCell = @"ffffff";
			_ob_FilterUnSelectedCellBg = @"ffffff";
			
			_ob_FilterBorderButton = @"ffffff";
			_ob_FilterSelectedButtonBg = @"3F97FC";
			_ob_FilterUnSelectedButtonBg = @"999982";
			_ob_FilterSelectedTextColorButton = @"ffffff";
			_ob_FilterUnSelectedTextColorButton = @"ffffff";
			
			//Date Picker
			_dp_tintColor = @"353535";
			_dp_today = @"000000";
			_dp_textColor = @"000000";
			_dp_unSelectTextColor = @"ffffff";
			
			
			//Search Stock
			_st_searchBarTintColor = @"000000";
			_st_cellBg1 = @"ffffff";
			_st_cellBg2 = @"F5F5F5";
			_st_cellTextColor = @"000000";
			
			//TradeVC
			_trade_StockInfoBg = @"ffffff";
			_trade_StockInfoText = @"000000";
            _trade_StockCompanyText = @"888888";
			_trade_MarketBg = @"F5F5F5";
			_trade_MarketText = @"000000";
			_trade_MarketHeaderBg = @"B2B2B2";
			_trade_MarketHeaderText = @"000000";
			_trade_MarketCellBg = @"ffffff";
			_trade_MarketCellText = @"000000";
			_trade_OrderInfoBg = @"F5F5F5";
			_trade_OrderInfoText = @"000000";
			_trade_AccountBg = @"F5F5F5";
			_trade_AccountText = @"000000";
			_trade_InfoDropdownCellBg = @"F5F5F5";
			_trade_InfoDropdownCellText = @"000000";
			_trade_InfoInputTextCellBg = @"F5F5F5";
			_trade_InfoInputTextCellColor = @"000000";
			_trade_InfoCheckBoxBg = @"ffffff";
			_trade_InfoCheckBoxTint = @"000000";
			_trade_InfoCellText = @"000000";
			_trade_BuyBg = @"19B393";
			_trade_SellBg = @"F15054";
			_trade_UnSelectBg = @"BDBEBD";
			_trade_BuyText = @"ffffff";
			_trade_SellText = @"ffffff";
			_trade_BorderSelect = @"374D59";
			_trade_BorderUnSelect = @"374D59";
			_trade_SearchButtonTint = @"000000";
			
			_pin_topBg = @"F5F5F5";
			_pin_titleColor = @"000000";
			_pin_okBg = @"B2B2B2";
			_pin_cancelBg = @"B2B2B2";
			_pin_okTitle = @"000000";
			_pin_cancelTitle = @"000000";
			_pin_rememberTitle = @"000000";
			
			_st_titleColor = @"ffffff";
			_st_textColor = @"000000";
			_st_hightLightTextColor = @"000000";
			_st_submitBg = @"B2B2B2";
			_st_closeBg = @"B2B2B2";
			_st_submitTitle = @"000000";
			_st_closeTitle = @"000000";
			_st_bgCell1 = @"ffffff";
			_st_bgCell2 = @"F5F5F5";
			
			_os_topBg = @"F5F5F5";
			_os_topTextColor = @"000000";
			_os_LabelTextColor = @"000000";
			_os_TextColor = @"000000";
			_os_openBg = @"B2B2B2";
			_os_doneBg = @"B2B2B2";
			_os_openTextColor = @"000000";
			_os_doneTextColor = @"000000";
			
			//Order Detail
			_ot_topBg = @"ffffff";
			_ot_topTextColor = @"B2B2B2";
			_ot_topHightLightTextColor = @"000000";
			_ot_HightLightTextColor = @"000000";
			_ot_TextColor = @"000000";
			_ot_NormalButtonBg = @"3F97FC";
			_ot_SpecialButtonBg = @"F20000";
			_ot_NormalButtonTextColor = @"ffffff";
			_ot_SpecialButtonTextColor = @"ffffff";
			_ot_OtherButtonBg = @"000000";
			_ot_OtherButtonTextColor = @"ffffff";
			
			//Alert
			_a_topBg = @"F5F5F5";
			_a_unSelectedTop = _ob_unSelectTextColor;
			_a_textColor = _ob_textColor;
			_a_viewBg = @"ffffff";
			_a_editBg = @"B2B2B2";
			_a_deleteBg = @"C90030";
			_a_cellBg = @"ffffff";
			_a_cellSwitchTint = _switchOnColor;
			_a_cellTextColor = @"000000";
			
			_a_settingTintColor = @"000000";
            _a_settingTitleColor = @"000000";
            _a_settingLabelColor = @"565656";
            _a_settingSwitchTintColor = _switchOnColor;
			_a_settingDropDownBg = @"EFEFEF";
			_a_settingConfirmBg = @"B2B2B2";
			_a_settingConfirmText = @"ffffff";
			_a_settingCancelBg = @"F7F7F7";
			_a_settingCancelText = _a_settingTintColor;
			
			_as_tintColor = @"B2B2B2";
			_as_messColor = @"000000";
			_as_TitleColor = @"000000";
			_as_bg = @"ffffff";
			
			//Setting Alert
			_sta_bgView = _a_viewBg;
			_sta_bgCell = _a_cellBg;
			_sta_textColor = @"000000";
			_sta_switchTintColor = _a_cellSwitchTint;
			_sta_bgHightLightCell = @"E2E2E2";
			//DashBoard
			_db_bgDropDown = @"ffffff";
            _db_borderColor = @"EFEFEF";
			_db_bg = @"F5F5F5";
			_db_ItemViewBg = @"ffffff";
            _db_NewsBgCell = @"ffffff";
			_db_textColor = @"000000";
            _db_headerTextColor = @"000000";
            _db_headerSubtextColor = @"#8E8E8E";
            _db_subItemTextColor = @"AAAAAA";
            _db_textHightLightColor = @"000000";
			_db_textColorDropDown = @"000000";
			_db_textColorItemDropDown = @"000000";
			_db_tintColorIconDropDown = @"000000";
			
			//Account
			_acc_bg = @"ffffff";
			_acc_tint = @"000000";
			_acc_bgCell = @"ffffff";
			_acc_bgImageCell = @"000000";
			_acc_textCell = @"000000";
			_acc_searchPlaceHolder = @"D5D5D5";
			_accDetail_textCell = @"ffffff";
			_acc_searchBorder = @"000000";
			
			//Check Auth
			_ca_textColor = @"000000";
			_ca_mainColor = @"ffffff";
			_ca_tintColor = _tintColor;
            _ca_cancelColor = _deleteColor;
        }
            break;
        case ThemeType_Orange:{
			[SettingManager shareInstance].unChangedPriceColor = @"000000";
			//Main
			_tintColor = @"119BF4";
			_subTintColor = @"000000";
			_mainBgColor = @"ffffff";
			_subBgColor = @"B2B2B2";
			_deleteColor = @"ef7474";
			
			_navTintColor = @"ffffff";
			_navBgColor = @"F7941E";
			
			_tabTintColor = @"000000";
			_tabBgColor = @"ffffff";
			
			_navTextTintColor = @"ffffff";
			_instructionColor = @"00BAD8";
			_baselineColor = @"DFDFDF";
			_shadowColor = @"EFEFEF";

			_logoTintColor = @"F7941E";
			//Text
			_mainTextColor = @"000000";
			_subTextColor = @"B2B2B2";
			
			//ITEMS
			_switchOnColor = @"119BF4";
			_switchOffColor = @"545D63";
			
			//PageMenu
			_unselectedTextColor = @"B2B2B2";
			_selectedTextColor = @"000000";
			_indicatorColor = @"000000";
			
			//Quote
			_quote_subHeaderBg = @"E0E0E0";
			_quote_tintIconColor = @"B2B2B2";
			_quote_subTintIconColor = @"000000";
			
			//Cell
			_bgCellColor = @"ffffff";
			_headerCellColor = @"F0F0F0";

			//Dropdown
			_bgDropdownColor = @"B2B2B2";
			
			
			//All News
			_allNews_ContentCell = @"ffffff";
			
			//Stock Tracker Screen
			_stt_bgColor = @"ffffff";
			_stt_headerBgColor = @"B2B2B2";
			_stt_bgTopColor = @"F5F5F5";
			_stt_bgExChangeColor = @"ffffff";
			_stt_bgWatchListColor = @"ffffff";
			_stt_bgFilterColor = @"ffffff";
			_stt_bgApplyColor = @"ffffff";
			_stt_tintActionButtonColor = @"000000";
			_stt_tintDropDownButtonColor = @"000000";
			_stt_tintExchangeDropDownButtonColor = @"000000";
			_stt_textExChangeColor = @"353535";
			_stt_textWatchListColor = @"353535";
			_stt_textFilterColor = @"353535";
			_stt_textApplyColor = @"000000";
			_stt_textItemDropDownColor = @"000000";
			_stt_textCellColor = @"000000";
			_stt_borderCellColor = @"000000";
			_stt_bgCellColor = @"ffffff";
			_stt_bgContentCellColor = @"ffffff";
			
			//Order Books
			_ob_bgTop1 = @"F5F5F5";
			_ob_bgTop2 = @"ffffff";
			_ob_bgTop3 = @"ffffff";
			_ob_textColor = @"000000";
			_ob_borderColor = @"353535";
			_ob_unSelectTextColor = @"B2B2B2";
			_ob_seperatorButtons = @"000000";
			_ob_DateTextColor = @"000000";
			_ob_HeaderBg = @"F5F5F5";
			_ob_Cell1Bg = @"ffffff";
			_ob_Cell2Bg = @"ffffff";
			_ob_CellTextColor = @"000000";
			
			_ob_FilterBorderCell = @"B2B2B2";
			_ob_FilterSelectedCellBg = @"3F97FC";
			_ob_FilterTextColorCell = @"353535";
            _ob_FilterTextHeader = _ob_FilterTextColorCell;
			_ob_FilterSelectedTextColorCell = @"ffffff";
			_ob_FilterUnSelectedCellBg = @"ffffff";
			
			_ob_FilterBorderButton = @"ffffff";
			_ob_FilterSelectedButtonBg = @"3F97FC";
			_ob_FilterUnSelectedButtonBg = @"999982";
			_ob_FilterSelectedTextColorButton = @"ffffff";
			_ob_FilterUnSelectedTextColorButton = @"ffffff";
			
			//Date Picker
			_dp_tintColor = @"353535";
			_dp_today = @"000000";
			_dp_textColor = @"000000";
			_dp_unSelectTextColor = @"ffffff";
			
			
			//Search Stock
			_st_searchBarTintColor = @"000000";
			_st_cellBg1 = @"ffffff";
			_st_cellBg2 = @"F5F5F5";
			_st_cellTextColor = @"000000";
			
			//TradeVC
			_trade_StockInfoBg = @"ffffff";
			_trade_StockInfoText = @"000000";
            _trade_StockCompanyText = @"888888";
			_trade_MarketBg = @"F5F5F5";
			_trade_MarketText = @"000000";
			_trade_MarketHeaderBg = @"B2B2B2";
			_trade_MarketHeaderText = @"000000";
			_trade_MarketCellBg = @"ffffff";
			_trade_MarketCellText = @"000000";
			_trade_OrderInfoBg = @"F5F5F5";
			_trade_OrderInfoText = @"000000";
			_trade_AccountBg = @"F5F5F5";
			_trade_AccountText = @"000000";
			_trade_InfoDropdownCellBg = @"F5F5F5";
			_trade_InfoDropdownCellText = @"000000";
			_trade_InfoInputTextCellBg = @"F5F5F5";
			_trade_InfoInputTextCellColor = @"000000";
			_trade_InfoCheckBoxBg = @"ffffff";
			_trade_InfoCheckBoxTint = @"000000";
			_trade_InfoCellText = @"000000";
			_trade_BuyBg = @"19B393";
			_trade_SellBg = @"F15054";
			_trade_UnSelectBg = @"BDBEBD";
			_trade_BuyText = @"ffffff";
			_trade_SellText = @"ffffff";
			_trade_BorderSelect = @"374D59";
			_trade_BorderUnSelect = @"374D59";
			_trade_SearchButtonTint = @"000000";
			
			_pin_topBg = @"F5F5F5";
			_pin_titleColor = @"000000";
			_pin_okBg = @"B2B2B2";
			_pin_cancelBg = @"B2B2B2";
			_pin_okTitle = @"000000";
			_pin_cancelTitle = @"000000";
			_pin_rememberTitle = @"000000";
			
			_st_titleColor = @"ffffff";
			_st_textColor = @"000000";
			_st_hightLightTextColor = @"000000";
			_st_submitBg = @"B2B2B2";
			_st_closeBg = @"B2B2B2";
			_st_submitTitle = @"000000";
			_st_closeTitle = @"000000";
			_st_bgCell1 = @"ffffff";
			_st_bgCell2 = @"F5F5F5";
			
			_os_topBg = @"F5F5F5";
			_os_topTextColor = @"000000";
			_os_LabelTextColor = @"000000";
			_os_TextColor = @"000000";
			_os_openBg = @"B2B2B2";
			_os_doneBg = @"B2B2B2";
			_os_openTextColor = @"000000";
			_os_doneTextColor = @"000000";
			
			//Order Detail
			_ot_topBg = @"ffffff";
			_ot_topTextColor = @"B2B2B2";
			_ot_topHightLightTextColor = @"000000";
			_ot_HightLightTextColor = @"000000";
			_ot_TextColor = @"000000";
			_ot_NormalButtonBg = @"3F97FC";
			_ot_SpecialButtonBg = @"F20000";
			_ot_NormalButtonTextColor = @"ffffff";
			_ot_SpecialButtonTextColor = @"ffffff";
			_ot_OtherButtonBg = @"000000";
			_ot_OtherButtonTextColor = @"ffffff";
			
			//Alert
			_a_topBg = @"F5F5F5";
			_a_unSelectedTop = _ob_unSelectTextColor;
			_a_textColor = _ob_textColor;
			_a_viewBg = @"ffffff";
			_a_editBg = @"B2B2B2";
			_a_deleteBg = @"C90030";
			_a_cellBg = @"ffffff";
			_a_cellSwitchTint = _switchOnColor;
			_a_cellTextColor = @"000000";
			
			_a_settingTintColor = @"000000";
            _a_settingTitleColor = @"000000";
            _a_settingLabelColor = @"565656";
            _a_settingSwitchTintColor = _switchOnColor;
			_a_settingDropDownBg = @"EFEFEF";
			_a_settingConfirmBg = @"B2B2B2";
			_a_settingConfirmText = @"ffffff";
			_a_settingCancelBg = @"F7F7F7";
			_a_settingCancelText = _a_settingTintColor;
			
			_as_tintColor = @"B2B2B2";
			_as_messColor = @"000000";
			_as_TitleColor = @"000000";
			_as_bg = @"ffffff";
			
			//Setting Alert
			_sta_bgView = _a_viewBg;
			_sta_bgCell = _a_cellBg;
			_sta_textColor = @"000000";
			_sta_switchTintColor = _a_cellSwitchTint;
			_sta_bgHightLightCell = @"E2E2E2";
			//DashBoard
			_db_bgDropDown = @"ffffff";
            _db_borderColor = @"EFEFEF";
			_db_bg = @"F5F5F5";
			_db_ItemViewBg = @"ffffff";
            _db_NewsBgCell = @"ffffff";
			_db_textColor = @"000000";
            _db_headerTextColor = @"000000";
            _db_headerSubtextColor = @"#8E8E8E";
            _db_subItemTextColor = @"AAAAAA";
            _db_textHightLightColor = @"000000";
			_db_textColorDropDown = @"000000";
			_db_textColorItemDropDown = @"000000";
			_db_tintColorIconDropDown = @"000000";
			
			//Account
			_acc_bg = @"ffffff";
			_acc_tint = @"000000";
			_acc_bgCell = @"ffffff";
			_acc_bgImageCell = @"000000";
			_acc_textCell = @"000000";
			_acc_searchPlaceHolder = @"D5D5D5";
			_accDetail_textCell = @"ffffff";
			_acc_searchBorder = @"000000";
			
			//Check Auth
			_ca_textColor = @"000000";
			_ca_mainColor = @"ffffff";
			_ca_tintColor = _tintColor;
            _ca_cancelColor = _deleteColor;
        }
            break;
            
            //For NEW
        case ThemeType_Light:{
            [SettingManager shareInstance].unChangedPriceColor = @"000000";
            //Main
            _tintColor = @"119BF4";
            _subTintColor = @"000000";
            _mainBgColor = @"ffffff";
			_subBgColor = @"B2B2B2";
            _deleteColor = @"ef7474";
			
            _navTintColor = @"000000";
			_navBgColor = @"ffffff";//@"F5F5F5";
            _navTextTintColor = @"000000";
			
			_tabTintColor = @"000000";
			_tabBgColor = @"ffffff";
			
            _instructionColor = @"00BAD8";
			_baselineColor = @"DFDFDF";
			_shadowColor = @"000000";

			_logoTintColor = @"33559D";
            //Text
            _mainTextColor = @"000000";
            _subTextColor = @"B2B2B2";
            
            //ITEMS
            _switchOnColor = @"119BF4";
            _switchOffColor = @"545D63";
			
            //PageMenu
            _unselectedTextColor = @"B2B2B2";
            _selectedTextColor = @"000000";
            _indicatorColor = @"000000";
            
            //Quote
            _quote_subHeaderBg = @"E0E0E0";
            _quote_tintIconColor = @"B2B2B2";
            _quote_subTintIconColor = @"000000";
            
            //Cell
            _bgCellColor = @"ffffff";
			_headerCellColor = @"F0F0F0";
            
            //Dropdown
            _bgDropdownColor = @"B2B2B2";
            
            
            //All News
            _allNews_ContentCell = @"ffffff";

            //Stock Tracker Screen
            _stt_bgColor = @"ffffff";
            _stt_headerBgColor = @"B2B2B2";
            _stt_bgTopColor = @"F5F5F5";
            _stt_bgExChangeColor = @"ffffff";
            _stt_bgWatchListColor = @"ffffff";
            _stt_bgFilterColor = @"ffffff";
            _stt_bgApplyColor = @"ffffff";
            _stt_tintActionButtonColor = @"000000";
            _stt_tintDropDownButtonColor = @"000000";
            _stt_tintExchangeDropDownButtonColor = @"000000";
            _stt_textExChangeColor = @"353535";
            _stt_textWatchListColor = @"353535";
            _stt_textFilterColor = @"353535";
            _stt_textApplyColor = @"000000";
            _stt_textItemDropDownColor = @"000000";
            _stt_textCellColor = @"000000";
            _stt_borderCellColor = @"000000";
            _stt_bgCellColor = @"ffffff";
            _stt_bgContentCellColor = @"ffffff";
            
            //Order Books
            _ob_bgTop1 = @"F5F5F5";
            _ob_bgTop2 = @"ffffff";
            _ob_bgTop3 = @"ffffff";
            _ob_textColor = @"000000";
            _ob_borderColor = @"353535";
            _ob_unSelectTextColor = @"B2B2B2";
            _ob_seperatorButtons = @"000000";
            _ob_DateTextColor = @"000000";
            _ob_HeaderBg = @"E0E0E0";
            _ob_Cell1Bg = @"ffffff";
            _ob_Cell2Bg = @"ffffff";
            _ob_CellTextColor = @"000000";
            
            _ob_FilterBorderCell = @"B2B2B2";
            _ob_FilterSelectedCellBg = @"3F97FC";
            _ob_FilterTextColorCell = @"353535";
            _ob_FilterTextHeader = @"000000";
            _ob_FilterSelectedTextColorCell = @"ffffff";
            _ob_FilterUnSelectedCellBg = @"ffffff";
            
            _ob_FilterBorderButton = @"ffffff";
            _ob_FilterSelectedButtonBg = @"3F97FC";
            _ob_FilterUnSelectedButtonBg = @"999982";
            _ob_FilterSelectedTextColorButton = @"ffffff";
            _ob_FilterUnSelectedTextColorButton = @"ffffff";
            
            //Date Picker
            _dp_tintColor = @"353535";
            _dp_today = @"000000";
            _dp_textColor = @"000000";
            _dp_unSelectTextColor = @"ffffff";
            
            
            //Search Stock
            _st_searchBarTintColor = @"000000";
            _st_cellBg1 = @"ffffff";
            _st_cellBg2 = @"F5F5F5";
            _st_cellTextColor = @"000000";
            
            //TradeVC
            _trade_StockInfoBg = @"ffffff";
            _trade_StockInfoText = @"000000";
            _trade_StockCompanyText = @"888888";
            _trade_MarketBg = @"F5F5F5";
            _trade_MarketText = @"000000";
            _trade_MarketHeaderBg = @"B2B2B2";
            _trade_MarketHeaderText = @"000000";
            _trade_MarketCellBg = @"ffffff";
            _trade_MarketCellText = @"000000";
            _trade_OrderInfoBg = @"F5F5F5";
            _trade_OrderInfoText = @"000000";
            _trade_AccountBg = @"F5F5F5";
            _trade_AccountText = @"000000";
            _trade_InfoDropdownCellBg = @"F5F5F5";
            _trade_InfoDropdownCellText = @"000000";
            _trade_InfoInputTextCellBg = @"F5F5F5";
            _trade_InfoInputTextCellColor = @"000000";
            _trade_InfoCheckBoxBg = @"ffffff";
            _trade_InfoCheckBoxTint = @"000000";
            _trade_InfoCellText = @"000000";
            _trade_BuyBg = @"19B393";
            _trade_SellBg = @"F15054";
            _trade_UnSelectBg = @"BDBEBD";
            _trade_BuyText = @"ffffff";
            _trade_SellText = @"ffffff";
            _trade_BorderSelect = @"374D59";
            _trade_BorderUnSelect = @"374D59";
            _trade_SearchButtonTint = @"000000";
            
            _pin_topBg = @"F5F5F5";
            _pin_titleColor = @"000000";
            _pin_okBg = @"B2B2B2";
            _pin_cancelBg = @"B2B2B2";
            _pin_okTitle = @"000000";
            _pin_cancelTitle = @"000000";
            _pin_rememberTitle = @"000000";
            
            _st_titleColor = @"ffffff";
            _st_textColor = @"000000";
            _st_hightLightTextColor = @"000000";
            _st_submitBg = @"B2B2B2";
            _st_closeBg = @"B2B2B2";
            _st_submitTitle = @"000000";
            _st_closeTitle = @"000000";
            _st_bgCell1 = @"ffffff";
            _st_bgCell2 = @"F5F5F5";
            
            _os_topBg = @"F5F5F5";
            _os_topTextColor = @"000000";
            _os_LabelTextColor = @"000000";
            _os_TextColor = @"000000";
            _os_openBg = @"0D94FA";
            _os_doneBg = @"B2B2B2";
            _os_openTextColor = @"ffffff";
            _os_doneTextColor = @"000000";
            
            //Order Detail
            _ot_topBg = @"ffffff";
            _ot_topTextColor = @"B2B2B2";
            _ot_topHightLightTextColor = @"000000";
            _ot_HightLightTextColor = @"000000";
            _ot_TextColor = @"000000";
            _ot_NormalButtonBg = @"3F97FC";
            _ot_SpecialButtonBg = @"F20000";
            _ot_NormalButtonTextColor = @"ffffff";
            _ot_SpecialButtonTextColor = @"ffffff";
            _ot_OtherButtonBg = @"000000";
            _ot_OtherButtonTextColor = @"ffffff";
            
            //Alert
            _a_topBg = @"ffffff";
            _a_unSelectedTop = _ob_unSelectTextColor;
            _a_textColor = _ob_textColor;
            _a_viewBg = @"ffffff";
            _a_editBg = @"B2B2B2";
            _a_deleteBg = @"C90030";
            _a_cellBg = @"ffffff";
            _a_cellSwitchTint = _switchOnColor;
            _a_cellTextColor = @"000000";
            
            _a_settingTintColor = _switchOnColor;
            _a_settingTitleColor = @"000000";
            _a_settingLabelColor = @"565656";
            _a_settingSwitchTintColor = _switchOnColor;
            _a_settingDropDownBg = @"EFEFEF";
            _a_settingConfirmBg = @"B2B2B2";
            _a_settingConfirmText = @"ffffff";
            _a_settingCancelBg = @"F7F7F7";
            _a_settingCancelText = _a_settingTintColor;
            
            _as_tintColor = @"B2B2B2";
            _as_messColor = @"000000";
            _as_TitleColor = @"000000";
            _as_bg = @"ffffff";
            
            //Setting Alert
            _sta_bgView = _a_viewBg;
            _sta_bgCell = _a_cellBg;
            _sta_textColor = @"000000";
            _sta_switchTintColor = _a_cellSwitchTint;
            _sta_bgHightLightCell = @"E2E2E2";
            //DashBoard
            _db_bgDropDown = @"ffffff";
            _db_borderColor = @"E1E1E1";
            _db_bg = @"f4f4f4";
            _db_ItemViewBg = @"ffffff";
            _db_NewsBgCell = @"ffffff";
            _db_textColor = @"000000";
            _db_headerTextColor = @"000000";
            _db_headerSubtextColor = @"#8E8E8E";
            _db_subItemTextColor = @"AAAAAA";
            _db_textHightLightColor = @"000000";
            _db_textColorDropDown = @"000000";
            _db_textColorItemDropDown = @"000000";
            _db_tintColorIconDropDown = @"000000";
            
            //Account
            _acc_bg = @"ffffff";
            _acc_tint = @"000000";
            _acc_bgCell = @"ffffff";
            _acc_bgImageCell = @"000000";
            _acc_textCell = @"000000";
            _acc_searchPlaceHolder = @"D5D5D5";
            _accDetail_textCell = @"000000";
            _acc_searchBorder = @"000000";
            
            //Check Auth
            _ca_textColor = @"000000";
            _ca_mainColor = @"ffffff";
            _ca_tintColor = _tintColor;
            _ca_cancelColor = @"ffffff";
        }
            break;
	
		default:{
			//Main
			_tintColor = @"119BF4";
			_subTintColor = @"000000";
			_mainBgColor = @"ffffff";
			_subBgColor = @"B2B2B2";
			_deleteColor = @"ef7474";
			
			_navTintColor = @"000000";
			_navBgColor = @"ffffff";//@"F5F5F5";
			
			_tabTintColor = @"000000";
			_tabBgColor = @"ffffff";
			
			_navTextTintColor = @"000000";
			_instructionColor = @"00BAD8";
			_baselineColor = @"DFDFDF";
			_shadowColor = @"000000";
			
			_logoTintColor = @"33559D";
			//Text
			_mainTextColor = @"000000";
			_subTextColor = @"B2B2B2";
		}
            break;
    }
//	//For private SSI
//	if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
//		_navBgColor = @"E02831";
//		_navTintColor = @"ffffff";
//		_navTextTintColor = @"ffffff";
//		
//
//	}
	
	_navBgColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	_navTintColor = @"ffffff";
	_navTextTintColor = @"ffffff";
	_tabTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	_tabBgColor = @"ffffff";
	_tintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	_navTintColor = @"ffffff";
    _a_cellSwitchTint = _tintColor;
}

#pragma mark - Main
- (void)updateNewTheme:(ThemeType)newTheme{
    _curTheme = newTheme;
    [self setTheme:newTheme];
    [self save];
	[[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeThemeNotification object:nil];
}
- (ThemeType)getCurrentTheme{
    return _curTheme;
}

- (UIStatusBarStyle)rootViewStatusBarStyle{
	if (![UserSession shareInstance].didLogin) {
		return UIStatusBarStyleDefault;
	}
    return UIStatusBarStyleLightContent;
}
@end
