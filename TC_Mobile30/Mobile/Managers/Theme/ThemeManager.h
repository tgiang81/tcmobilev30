//
//  ThemeManager.h
//  TC_Mobile30
//
//  Created by Kaka on 11/2/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSInteger{
	ThemeType_Light = 0,
	ThemeType_Dark,
	ThemeType_Red,
	ThemeType_WhiteBlue,
	ThemeType_Green,
	ThemeType_Orange
}ThemeType;

#define kDidChangeThemeNotification	@"kDidChangeThemeNotification"

@interface ThemeManager : NSObject{
	
}

//Main
@property (strong, readonly, nonatomic) NSString *mainBgColor;
@property (strong, readonly, nonatomic) NSString *subBgColor;
@property (strong, readonly, nonatomic) NSString *tintColor;
@property (strong, readonly, nonatomic) NSString *subTintColor;

@property (strong, readonly, nonatomic) NSString *navTintColor;
@property (strong, readonly, nonatomic) NSString *navBgColor;
@property (strong, readonly, nonatomic) NSString *navTextTintColor;

@property (strong, readonly, nonatomic) NSString *tabTintColor;
@property (strong, readonly, nonatomic) NSString *tabBgColor;

@property (strong, readonly, nonatomic) NSString *instructionColor; //For loading, hud, refresh, special text...
@property (strong, readonly, nonatomic) NSString *baselineColor;
@property (strong, readonly, nonatomic) NSString *shadowColor;

@property (strong, readonly, nonatomic) NSString *bgCellColor;
@property (strong, readonly, nonatomic) NSString *headerCellColor;
@property (strong, readonly, nonatomic) NSString *deleteColor;

@property (strong, readonly, nonatomic) NSString *mainSelectedDropdownColor;
@property (strong, readonly, nonatomic) NSString *bgDropdownColor;

//Login
@property (strong, readonly, nonatomic) NSString *logoTintColor;

//******** ITEMS **************
@property (strong, nonatomic) NSString *switchOnColor;
@property (strong, nonatomic) NSString *switchOffColor;

//PageMenu
@property (strong, readonly, nonatomic) NSString *selectedTextColor;
@property (strong, readonly, nonatomic) NSString *unselectedTextColor;
@property (strong, readonly, nonatomic) NSString *indicatorColor;

//Text Color
@property (strong, readonly, nonatomic) NSString *mainTextColor;
@property (strong, readonly, nonatomic) NSString *textTintColor;
@property (strong, readonly, nonatomic) NSString *subTextColor;

//Stock Info
@property (strong, readonly, nonatomic) NSString *bgTopDetailColor;
@property (strong, readonly, nonatomic) NSString *bgInstructionBarColor;
@property (strong, readonly, nonatomic) NSString *bgCellOddColor;
@property (strong, readonly, nonatomic) NSString *bgCellEvenColor;

//Private Controller
@property (strong, readonly,  nonatomic) NSString *bgHomePageColor;
@property (strong, readonly,  nonatomic) NSString *textHomePageColor;
@property (strong, readonly,  nonatomic) NSString *textTintHomePageColor;

//Main Menu
@property (strong, readonly, nonatomic) NSString *infoViewBgMainMenuColor;

//Portfolio
@property (strong, readonly, nonatomic) NSString *portfolioTextColor;

//Setting Screen
@property (strong, readonly, nonatomic) NSString *st_tintColor;
@property (strong, readonly, nonatomic) NSString *st_switchTintColor;

//Quote Screen
@property (strong, readonly, nonatomic) NSString *quote_subHeaderBg;
@property (strong, readonly, nonatomic) NSString *quote_tintIconColor;
@property (strong, readonly, nonatomic) NSString *quote_subTintIconColor;

//Stock Cell
@property (strong, readonly, nonatomic) NSString *stCell_textTintColor;

//All News
@property (strong, readonly, nonatomic) NSString *allNews_ContentCell;

//Stock Tracker Screen
@property (strong, readonly, nonatomic) NSString *stt_bgTopColor;
@property (strong, readonly, nonatomic) NSString *stt_bgColor;
@property (strong, readonly, nonatomic) NSString *stt_headerBgColor;
@property (strong, readonly, nonatomic) NSString *stt_bgExChangeColor;
@property (strong, readonly, nonatomic) NSString *stt_bgWatchListColor;
@property (strong, readonly, nonatomic) NSString *stt_bgFilterColor;
@property (strong, readonly, nonatomic) NSString *stt_bgApplyColor;
@property (strong, readonly, nonatomic) NSString *stt_tintActionButtonColor;
@property (strong, readonly, nonatomic) NSString *stt_tintExchangeDropDownButtonColor;
@property (strong, readonly, nonatomic) NSString *stt_tintDropDownButtonColor;

@property (strong, readonly, nonatomic) NSString *stt_textExChangeColor;
@property (strong, readonly, nonatomic) NSString *stt_textWatchListColor;
@property (strong, readonly, nonatomic) NSString *stt_textFilterColor;
@property (strong, readonly, nonatomic) NSString *stt_textApplyColor;
@property (strong, readonly, nonatomic) NSString *stt_textItemDropDownColor;

@property (strong, readonly, nonatomic) NSString *stt_textCellColor;
@property (strong, readonly, nonatomic) NSString *stt_borderCellColor;
@property (strong, readonly, nonatomic) NSString *stt_bgCellColor;
@property (strong, readonly, nonatomic) NSString *stt_bgContentCellColor;
//Order Book
@property (strong, readonly, nonatomic) NSString *ob_bgTop1;
@property (strong, readonly, nonatomic) NSString *ob_bgTop2;
@property (strong, readonly, nonatomic) NSString *ob_bgTop3;
@property (strong, readonly, nonatomic) NSString *ob_textColor;
@property (strong, readonly, nonatomic) NSString *ob_unSelectTextColor;
@property (strong, readonly, nonatomic) NSString *ob_borderColor;
@property (strong, readonly, nonatomic) NSString *ob_seperatorButtons;
@property (strong, readonly, nonatomic) NSString *ob_DateTextColor;
@property (strong, readonly, nonatomic) NSString *ob_HeaderBg;
@property (strong, readonly, nonatomic) NSString *ob_Cell1Bg;
@property (strong, readonly, nonatomic) NSString *ob_Cell2Bg;
@property (strong, readonly, nonatomic) NSString *ob_CellTextColor;



@property (strong, readonly, nonatomic) NSString *ob_FilterBorderCell;
@property (strong, readonly, nonatomic) NSString *ob_FilterSelectedCellBg;
@property (strong, readonly, nonatomic) NSString *ob_FilterUnSelectedCellBg;
@property (strong, readonly, nonatomic) NSString *ob_FilterTextColorCell;
@property (strong, readonly, nonatomic) NSString *ob_FilterTextHeader;
@property (strong, readonly, nonatomic) NSString *ob_FilterSelectedTextColorCell;

@property (strong, readonly, nonatomic) NSString *ob_FilterBorderButton;
@property (strong, readonly, nonatomic) NSString *ob_FilterSelectedButtonBg;
@property (strong, readonly, nonatomic) NSString *ob_FilterUnSelectedButtonBg;
@property (strong, readonly, nonatomic) NSString *ob_FilterSelectedTextColorButton;
@property (strong, readonly, nonatomic) NSString *ob_FilterUnSelectedTextColorButton;

//Date Picker
@property (strong, readonly, nonatomic) NSString *dp_tintColor;
@property (strong, readonly, nonatomic) NSString *dp_today;
@property (strong, readonly, nonatomic) NSString *dp_textColor;
@property (strong, readonly, nonatomic) NSString *dp_unSelectTextColor;

//Search Stock
@property (strong, readonly, nonatomic) NSString *st_searchBarTintColor;
@property (strong, readonly, nonatomic) NSString *st_cellBg1;
@property (strong, readonly, nonatomic) NSString *st_cellBg2;
@property (strong, readonly, nonatomic) NSString *st_cellTextColor;

//TradeVC
@property (strong, readonly, nonatomic) NSString *trade_StockInfoBg;
@property (strong, readonly, nonatomic) NSString *trade_StockInfoText;
@property (strong, readonly, nonatomic) NSString *trade_StockCompanyText;
@property (strong, readonly, nonatomic) NSString *trade_MarketHeaderBg;
@property (strong, readonly, nonatomic) NSString *trade_MarketHeaderText;
@property (strong, readonly, nonatomic) NSString *trade_MarketCellBg;
@property (strong, readonly, nonatomic) NSString *trade_MarketCellText;
@property (strong, readonly, nonatomic) NSString *trade_MarketBg;
@property (strong, readonly, nonatomic) NSString *trade_MarketText;
@property (strong, readonly, nonatomic) NSString *trade_OrderInfoBg;
@property (strong, readonly, nonatomic) NSString *trade_OrderInfoText;
@property (strong, readonly, nonatomic) NSString *trade_AccountBg;
@property (strong, readonly, nonatomic) NSString *trade_AccountText;
@property (strong, readonly, nonatomic) NSString *trade_InfoDropdownCellBg;
@property (strong, readonly, nonatomic) NSString *trade_InfoDropdownCellText;
@property (strong, readonly, nonatomic) NSString *trade_InfoInputTextCellBg;
@property (strong, readonly, nonatomic) NSString *trade_InfoInputTextCellColor;
@property (strong, readonly, nonatomic) NSString *trade_InfoCellText;
@property (strong, readonly, nonatomic) NSString *trade_InfoCheckBoxBg;
@property (strong, readonly, nonatomic) NSString *trade_InfoCheckBoxTint;
@property (strong, readonly, nonatomic) NSString *trade_BuyBg;
@property (strong, readonly, nonatomic) NSString *trade_SellBg;
@property (strong, readonly, nonatomic) NSString *trade_UnSelectBg;
@property (strong, readonly, nonatomic) NSString *trade_BuyText;
@property (strong, readonly, nonatomic) NSString *trade_SellText;
@property (strong, readonly, nonatomic) NSString *trade_BorderSelect;
@property (strong, readonly, nonatomic) NSString *trade_BorderUnSelect;
@property (strong, readonly, nonatomic) NSString *trade_SearchButtonTint;

@property (strong, readonly, nonatomic) NSString *pin_topBg;
@property (strong, readonly, nonatomic) NSString *pin_titleColor;
@property (strong, readonly, nonatomic) NSString *pin_okBg;
@property (strong, readonly, nonatomic) NSString *pin_okTitle;
@property (strong, readonly, nonatomic) NSString *pin_cancelBg;
@property (strong, readonly, nonatomic) NSString *pin_cancelTitle;
@property (strong, readonly, nonatomic) NSString *pin_rememberTitle;

@property (strong, readonly, nonatomic) NSString *st_titleColor;
@property (strong, readonly, nonatomic) NSString *st_textColor;
@property (strong, readonly, nonatomic) NSString *st_hightLightTextColor;
@property (strong, readonly, nonatomic) NSString *st_submitBg;
@property (strong, readonly, nonatomic) NSString *st_submitTitle;
@property (strong, readonly, nonatomic) NSString *st_closeBg;
@property (strong, readonly, nonatomic) NSString *st_closeTitle;
@property (strong, readonly, nonatomic) NSString *st_bgCell1;
@property (strong, readonly, nonatomic) NSString *st_bgCell2;

@property (strong, readonly, nonatomic) NSString *os_topBg;
@property (strong, readonly, nonatomic) NSString *os_topTextColor;
@property (strong, readonly, nonatomic) NSString *os_TextColor;
@property (strong, readonly, nonatomic) NSString *os_LabelTextColor;
@property (strong, readonly, nonatomic) NSString *os_openBg;
@property (strong, readonly, nonatomic) NSString *os_doneTextColor;
@property (strong, readonly, nonatomic) NSString *os_openTextColor;
@property (strong, readonly, nonatomic) NSString *os_doneBg;

@property (strong, readonly, nonatomic) NSString *ot_topBg;
@property (strong, readonly, nonatomic) NSString *ot_topTextColor;
@property (strong, readonly, nonatomic) NSString *ot_topHightLightTextColor;
@property (strong, readonly, nonatomic) NSString *ot_TextColor;
@property (strong, readonly, nonatomic) NSString *ot_HightLightTextColor;
@property (strong, readonly, nonatomic) NSString *ot_NormalButtonBg;
@property (strong, readonly, nonatomic) NSString *ot_SpecialButtonBg;
@property (strong, readonly, nonatomic) NSString *ot_NormalButtonTextColor;
@property (strong, readonly, nonatomic) NSString *ot_SpecialButtonTextColor;
@property (strong, readonly, nonatomic) NSString *ot_OtherButtonBg;
@property (strong, readonly, nonatomic) NSString *ot_OtherButtonTextColor;

@property (strong, readonly, nonatomic) NSString *a_topBg;
@property (strong, readonly, nonatomic) NSString *a_unSelectedTop;
@property (strong, readonly, nonatomic) NSString *a_textColor;
@property (strong, readonly, nonatomic) NSString *a_viewBg;
@property (strong, readonly, nonatomic) NSString *a_editBg;
@property (strong, readonly, nonatomic) NSString *a_deleteBg;
@property (strong, readonly, nonatomic) NSString *a_cellBg;
@property (strong, readonly, nonatomic) NSString *a_cellSwitchTint;
@property (strong, readonly, nonatomic) NSString *a_cellTextColor;


@property (strong, readonly, nonatomic) NSString *a_settingTintColor;
@property (strong, readonly, nonatomic) NSString *a_settingTitleColor;
@property (strong, readonly, nonatomic) NSString *a_settingLabelColor;
@property (strong, readonly, nonatomic) NSString *a_settingSwitchTintColor;
@property (strong, readonly, nonatomic) NSString *a_settingDropDownBg;
@property (strong, readonly, nonatomic) NSString *a_settingConfirmBg;
@property (strong, readonly, nonatomic) NSString *a_settingConfirmText;
@property (strong, readonly, nonatomic) NSString *a_settingCancelBg;
@property (strong, readonly, nonatomic) NSString *a_settingCancelText;

@property (strong, readonly, nonatomic) NSString *as_tintColor;
@property (strong, readonly, nonatomic) NSString *as_messColor;
@property (strong, readonly, nonatomic) NSString *as_TitleColor;
@property (strong, readonly, nonatomic) NSString *as_bg;

//DashBoard
@property (strong, readonly, nonatomic) NSString *db_borderColor;
@property (strong, readonly, nonatomic) NSString *db_bgDropDown;
@property (strong, readonly, nonatomic) NSString *db_bg;
@property (strong, readonly, nonatomic) NSString *db_ItemViewBg;
@property (strong, readonly, nonatomic) NSString *db_NewsBgCell;
@property (strong, readonly, nonatomic) NSString *db_headerTextColor;
@property (strong, readonly, nonatomic) NSString *db_headerSubtextColor;
@property (strong, readonly, nonatomic) NSString *db_textColor;
@property (strong, readonly, nonatomic) NSString *db_subItemTextColor;
@property (strong, readonly, nonatomic) NSString *db_textHightLightColor;
@property (strong, readonly, nonatomic) NSString *db_textColorDropDown;
@property (strong, readonly, nonatomic) NSString *db_textColorItemDropDown;
@property (strong, readonly, nonatomic) NSString *db_tintColorIconDropDown;

//Account
@property (strong, readonly, nonatomic) NSString *acc_bg;
@property (strong, readonly, nonatomic) NSString *acc_tint;
@property (strong, readonly, nonatomic) NSString *acc_bgCell;
@property (strong, readonly, nonatomic) NSString *acc_bgImageCell;
@property (strong, readonly, nonatomic) NSString *acc_textCell;
@property (strong, readonly, nonatomic) NSString *acc_searchPlaceHolder;
@property (strong, readonly, nonatomic) NSString *acc_searchBorder;

@property (strong, readonly, nonatomic) NSString *accDetail_textCell;
@property (strong, readonly, nonatomic) NSString *accDetail_topTextCell;
@property (strong, readonly, nonatomic) NSString *accDetail_topBgCell;

@property (strong, readonly, nonatomic) NSString *sta_bgView;
@property (strong, readonly, nonatomic) NSString *sta_bgCell;
@property (strong, readonly, nonatomic) NSString *sta_textColor;
@property (strong, readonly, nonatomic) NSString *sta_switchTintColor;
@property (strong, readonly, nonatomic) NSString *sta_bgHightLightCell;

//TCCheckAuthVC
@property (strong, readonly, nonatomic) NSString *ca_textColor;
@property (strong, readonly, nonatomic) NSString *ca_mainColor;
@property (strong, readonly, nonatomic) NSString *ca_tintColor;
@property (strong, readonly, nonatomic) NSString *ca_cancelColor;



//Singleton
+ (ThemeManager *)shareInstance;
#pragma mark - Main
- (void)save;
- (void)clearSessionData;
- (void)restoreSessionIfNeeded;

#pragma mark - ACTION
- (void)updateNewTheme:(ThemeType)newTheme;
- (ThemeType)getCurrentTheme;
- (UIStatusBarStyle) rootViewStatusBarStyle;
@end
