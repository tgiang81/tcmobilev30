//
//  DataManager.m
//  TC_Mobile30
//
//  Created by Kaka on 2/18/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager
//================== Singleton ==============
+ (DataManager *)shared{
	static DataManager *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[DataManager alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		_lineChartDict = @{}.mutableCopy;
	}
	return  self;
}

#pragma mark - Main
- (void)cleareDataLineChart{
	[_lineChartDict removeAllObjects];
}

- (void)storeDataLineCharts:(id)dataCharts forKey:(NSString *)key{
	if (dataCharts) {
		[_lineChartDict setObject:dataCharts forKey:key];
	}
}
- (id)getDataLineChartForKey:(NSString *)key{
	return [_lineChartDict objectForKey:key];
}

- (void)removeDataLineChartForKey:(NSString *)key{
	if ([_lineChartDict.allKeys containsObject:key]) {
		[_lineChartDict removeObjectForKey:key];
	}
}
@end
