//
//  DataManager.h
//  TC_Mobile30
//
//  Created by Kaka on 2/18/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject{
	
}
@property (strong, nonatomic) NSMutableDictionary *lineChartDict;
+ (DataManager *)shared;

//MAIN
- (void)cleareDataLineChart;
- (void)storeDataLineCharts:(id)dataCharts forKey:(NSString *)key;
- (id)getDataLineChartForKey:(NSString *)key;
- (void)removeDataLineChartForKey:(NSString *)key;
@end
