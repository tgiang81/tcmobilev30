//
//  HashUtils.m
//  TCiPad
//
//  Created by Kaka on 3/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "HashUtils.h"
#import "NSString-Hash.h"
#import "NSData-AES.h"
#import "Base64.h"
#import "RSACrypt.h"
#import "NSData-Zlib.h"
#import "NSDateFormatter-Locale.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

@implementation HashUtils
//================== Singleton ==============
+ (HashUtils *)shareInstance{
	static HashUtils *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[HashUtils alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
	}
	return  self;
}

#pragma mark - Main
#pragma mark - ASH KEY
//Generate Random AES Encryption Key
+ (NSString *)generateAESKey {
	CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
	NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
	CFRelease(uuidObject);
	return uuidStr;
}

//TimeStampURLEncoded
+ (NSString *) generateTimeStampURLEncoded {
	NSDateFormatter *ndf = [[NSDateFormatter alloc] initWithSafeLocale];
	[ndf setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:28800]];
	[ndf setDateFormat:@"yyyyMMddHHmmss.SSS"];
	NSString *timestamp = [ndf stringFromDate:[NSDate date]];
	
	NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
																									NULL,
																									(CFStringRef)timestamp,
																									NULL,
																									(CFStringRef)@"!*'();:@&=+$,/?%#[]",
																									kCFStringEncodingISOLatin1));
	
	return encodedString;
}

//TimeStamp
+ (NSString *)generateTimeStamp {
	NSDateFormatter *ndf = [[NSDateFormatter alloc] initWithSafeLocale];
	[ndf setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:28800]];
	[ndf setDateFormat:@"yyyyMMddHHmmss.SSS"];
	NSString *timestamp = [ndf stringFromDate:[NSDate date]];
	return timestamp;
}

#pragma mark - IP Address
/*
+ (NSString *)getIPAddress {
	
	NSString *address = @"error";
	struct ifaddrs *interfaces = NULL;
	struct ifaddrs *temp_addr = NULL;
	int success = 0;
	// retrieve the current interfaces - returns 0 on success
	success = getifaddrs(&interfaces);
	if (success == 0) {
		// Loop through linked list of interfaces
		temp_addr = interfaces;
		while(temp_addr != NULL) {
			if(temp_addr->ifa_addr->sa_family == AF_INET) {
				// Check if interface is en0 which is the wifi connection on the iPhone
				if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
					// Get NSString from C String
					address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
					
				}
				
			}
			
			temp_addr = temp_addr->ifa_next;
		}
	}
	// Free memory
	freeifaddrs(interfaces);
	return address;
}
*/
+ (NSString *)getIPAddress {
	struct ifaddrs *interfaces = NULL;
	struct ifaddrs *temp_addr = NULL;
	NSString *wifiAddress = nil;
	NSString *cellAddress = nil;
	
	if(!getifaddrs(&interfaces)) {
		// Loop through linked list of interfaces
		temp_addr = interfaces;
		while(temp_addr != NULL) {
			sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
			if(sa_type == AF_INET || sa_type == AF_INET6) {
				NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
				NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]; // pdp_ip0
				//NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
				
				if([name isEqualToString:@"en0"]) {
					// Interface is the wifi connection on the iPhone
					wifiAddress = addr;
				} else
					if([name isEqualToString:@"pdp_ip0"]) {
						// Interface is the cell connection on the iPhone
						cellAddress = addr;
					}
			}
			temp_addr = temp_addr->ifa_next;
		}
		// Free memory
		freeifaddrs(interfaces);
	}
	NSString *addr = wifiAddress ? wifiAddress : cellAddress;
	
	return addr;
}

@end
