//
//  ApplicationUtils.h
//  TC_Mobile30
//
//  Created by Kaka on 4/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApplicationUtils : NSObject
+ (void)open:(NSString *)url;

@end

NS_ASSUME_NONNULL_END
