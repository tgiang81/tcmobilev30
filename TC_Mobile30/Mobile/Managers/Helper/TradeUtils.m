//
//  TradeUtils.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/13/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TradeUtils.h"
#import "UserPrefConstants.h"
#import "NumberHandler.h"
#import "UserAccountClientData.h"
#import "CurrencyRate.h"
@implementation TradeUtils
+ (NSArray *)getHeaderAndValueWith:(AccountSubInfo)type uacd:(UserAccountClientData *)uacd{
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGeneratesDecimalNumbers:YES];
    switch (type) {
        case AccountSubInfo_TradingLimit:
            return @[@"Trading Limit:", [NSString stringWithFormat:@"%@ %@", [UserPrefConstants singleton].defaultCurrency, [NumberHandler numberFormat2D:uacd.credit_limit]]];
        case AccountSubInfo_BuyLimit:
            return @[@"Buy Limit:", [NSString stringWithFormat:@"%@ %@" ,
                                     [UserPrefConstants singleton].defaultCurrency,
                                     
                                     [priceFormatter stringFromNumber: [NSNumber numberWithDouble:uacd.buy_limit]] ]];
        case AccountSubInfo_SellLimit:
            return @[@"Sell Limit:", [NSString stringWithFormat:@"%@ %@" , [UserPrefConstants singleton].defaultCurrency,
                                      [priceFormatter stringFromNumber:[NSNumber numberWithDouble:uacd.sell_limit]]]];
        case AccountSubInfo_InvPowerLimit:
            return @[@"Inv. Power:", [NSString stringWithFormat:@"%@ %@", [UserPrefConstants singleton].defaultCurrency, [NumberHandler numberFormat2D:uacd.credit_limit]]];
            
    }
    return @[@"", @""];
}

+ (NSString *)getCurrencyRateOf:(NSString *)currency forAction:(int)action {
    CurrencyRate *curr = [self getCurrencyComponentOf:currency];
    
    NSString *result = @"";
    //In Case ATP does not return currency rate to us. We display  1 Stock Currency : 1.000 Stock Currency
    if (curr == nil) {
        result = [NSString stringWithFormat:@"1 %@ : %@ %@", currency, [NumberHandler numberFormatCurrencyRate:1], currency];
    }
    else {
        result = [NSString stringWithFormat:@"%d %@ : %@ %@", curr.denomination, curr.currency, ((action == 1) ? [NumberHandler numberFormatCurrencyRate:curr.sell_rate] : [NumberHandler numberFormatCurrencyRate:curr.buy_rate]), [UserPrefConstants singleton].defaultCurrency];
    }
    
    return result;
}
+ (NSArray *)getTradeLimitString:(UserAccountClientData *)uacd orderDetail:(OrderDetails *)orderDetails stkExchange:(NSString *)stkExchange action:(int)action currencyPlus:(NSString *)currencyPlus{
    
    NSString *header =  [LanguageManager stringForKey:@"Trading Limit"];
    double balance = (double)uacd.credit_limit;
    NSString *value = [NSString stringWithFormat:@"%@ %@", [[UserPrefConstants singleton] defaultCurrency], [NumberHandler numberFormat2D:balance]];
    if ([orderDetails.payment_type isEqualToString:@"CUT"]) {
        header = [LanguageManager stringForKey:@"Investment Power"];
        balance = (double)uacd.credit_limit;
        value = [NSString stringWithFormat:@"%@ %@", orderDetails.currency, [NumberHandler numberFormat2D:balance]];
    }
    else {
        if ([[[UserPrefConstants singleton].clientLimitOptionDict objectForKey:stkExchange] intValue] == 3) {
            if (uacd.isMarginAccount == 0) {
                if (action == 1) {
                    header =  [LanguageManager stringForKey:@"Buy Limit"];
                    balance = (double)uacd.buy_limit;
                    
                    DLog(@"Buy Limit %.f",balance);
                    
                }
                else if (action == 2){
                    header =  [LanguageManager stringForKey:@"Sell Limit"];
                    balance = (double)uacd.sell_limit;
                }
                value = [NSString stringWithFormat:@"%@ %@", [[UserPrefConstants singleton] defaultCurrency], [NumberHandler numberFormat2D:balance]];
            }
        }
        
        
        if (currencyPlus != [NSNull null] && ![[[UserPrefConstants singleton] defaultCurrency] isEqualToString:currencyPlus]) {
            double creditLimit_stockCurrency = [self convertFromDefaultCurrency:balance toCurrency:currencyPlus forAction:action];
            if (isnan(creditLimit_stockCurrency)) {
            }
            else {
                value = [NSString stringWithFormat:@"%@ / %@ %@", value, currencyPlus, [NumberHandler numberFormat2D:creditLimit_stockCurrency]];
            }
        }else{
            value = @"-";
        }
    }
    
    return @[header, value];
}

+ (double)convertFromDefaultCurrency:(double)amount toCurrency:(NSString *)outputCurrency forAction:(int)action {
    
    CurrencyRate *curr = [self getCurrencyComponentOf:outputCurrency];
    
    double outputAmount = 0;
    if (action == 1) {
        outputAmount = amount / curr.sell_rate * curr.denomination;
    }
    else if (action == 2) {
        outputAmount = amount / curr.buy_rate * curr.denomination;
    }
    
    return outputAmount;
}

+ (CurrencyRate *)getCurrencyComponentOf:(NSString *)currency {
    CurrencyRate *curr = [[UserPrefConstants singleton].currencyRateDict objectForKey:currency];
    return curr;
}

@end
