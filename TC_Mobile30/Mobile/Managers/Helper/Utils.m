//
//  Utils.m
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "Utils.h"
#import "BrokerModel.h"
#import "DetailBrokerModel.h"
#import "SVProgressHUD.h"
#import "AppConstants.h"
#import "StockModel.h"
#import "QCData.h"
#import "YBHud.h"
#import "NewsModel.h"
#import "VertxConnectionManager.h"
#import "TradingRules.h"
#import "PaymentCUT.h"
#import "UserPrefConstants.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <sys/utsname.h>
#import "TimeSaleModel.h"
#import "NSDate+Utilities.h"
#import "NSString+Util.h"
#import "JNKeychain.h"
#import "BusinessDoneModel.h"
#import "LanguageKey.h"

@interface Utils(){
    YBHud *_hudWindow;
	SVProgressHUD *_svHUD;
}
@end

static NSString *kKeywordMultiBroker = @"multilogin";
@implementation Utils
//================== Singleton ==============
+ (Utils *)shareInstance{
    static Utils *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[Utils alloc] init];
    });
    return _instance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        //Custom data here
		_svHUD = [[SVProgressHUD alloc] init];
    }
    return  self;
}

#pragma mark - HUD
//SVProgressHUD
+ (void)showSVProgressHUD:(NSString *)message{
	[SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
	UIColor *tint = TC_COLOR_FROM_HEX([BrokerManager shareInstance].detailBroker.color.mainTint);
	[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
	[SVProgressHUD setFont:AppFont_MainFontBoldWithSize(18)];
	[SVProgressHUD setForegroundColor:tint];
	[SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
	[SVProgressHUD showWithStatus:message];
}

+ (void)dismissSVProgressHUD{
	[SVProgressHUD dismiss];
}


+ (void)showApplicationHUD{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}
+ (void)dismissApplicationHUD{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

//YBHud
- (void)showWindowHudWithTitle:(NSString *)title{
	//[Utils showSVProgressHUD:title];
	/*
    if (_hudWindow) {
        _hudWindow.text = !title ? @"" : title;
        return;
    }
    _hudWindow = [[YBHud alloc]initWithHudType:DGActivityIndicatorAnimationTypeLineScale andText:title];
    _hudWindow.UserInteractionDisabled = NO;
    _hudWindow.text = !title ? @"" : title;
    _hudWindow.textFont = AppFont_MainFontRegularWithSize(17);
    _hudWindow.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].instructionColor);
    [_hudWindow showInView:[[UIApplication sharedApplication] keyWindow] animated:YES];
	*/
}

- (void)updateProgressForWindowHUD:(NSString *)progressString{
    if (_hudWindow) {
        _hudWindow.text = progressString;
    }
}

- (void)dismissWindowHUD{
	//[Utils dismissSVProgressHUD];
	/*
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->_hudWindow) {
            [self->_hudWindow dismissAnimated:YES];
            self->_hudWindow = nil;
        }
    });
	*/
}

//+++ Create Instance of private hud to show in View +++
- (DGActivityIndicatorView *)createPrivateActivity{
	UIColor *mainTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallRotate  tintColor:mainTintColor];
    activityIndicatorView.frame = CGRectMake(0, 0, 30, 30);
    activityIndicatorView.size = 16;
    return activityIndicatorView;
}

- (void)showActivity:(DGActivityIndicatorView *)activity inView:(UIView *)view{
    float constrain_Width_Height = 30;
    activity.center = view.center;
    [view addSubview:activity];
    [activity setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *widthConstrain = [NSLayoutConstraint constraintWithItem:activity
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute: NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:constrain_Width_Height];
    NSLayoutConstraint *heightConstrain = [NSLayoutConstraint constraintWithItem:activity
                                                                       attribute:NSLayoutAttributeHeight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute: NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1
                                                                        constant:constrain_Width_Height];
    NSLayoutConstraint *centerX =  [NSLayoutConstraint constraintWithItem:activity
                                                                attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:activity.superview
                                                                attribute:NSLayoutAttributeCenterX
                                                               multiplier:1.f constant:0.f];
    NSLayoutConstraint *centerY =  [NSLayoutConstraint constraintWithItem:activity
                                                                attribute:NSLayoutAttributeCenterY
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:activity.superview
                                                                attribute:NSLayoutAttributeCenterY
                                                               multiplier:1.f constant:0.f];
    [NSLayoutConstraint activateConstraints:@[widthConstrain, heightConstrain, centerX, centerY]];
    [activity startAnimating];
}

- (void)dismissActivity:(DGActivityIndicatorView *)activity{
	if (activity) {
		[activity stopAnimating];
		[activity removeFromSuperview];
	}
}
#pragma mark - Generate With Random
//Random In Value
+ (int)generateRandomNumberWithlowerBound:(int)lowerBound
                               upperBound:(int)upperBound{
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    return rndValue;
}

+ (int)getRandomNumberBetween:(int)from to:(int)to{
    return (int)from + arc4random() % (to-from+1);
}
#pragma mark - Version
+ (NSString *)currentVersionApp{
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
}

#pragma mark - Util String
//Trim leading and trailing white space
+ (NSString *)stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:(NSString *)string
{
    // TODO: To be implemented from tutorial.
    NSString *leadingTrailingWhiteSpacesPattern = @"(?:^\\s+)|(?:\\s+$)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:leadingTrailingWhiteSpacesPattern options:NSRegularExpressionCaseInsensitive error:NULL];
    NSRange stringRange = NSMakeRange(0, string.length);
    NSString *trimmedString = [regex stringByReplacingMatchesInString:string options:NSMatchingReportProgress range:stringRange withTemplate:@"$1"];
    return trimmedString;
}

#pragma mark - Main

//=================== COMMON MENU TITLE===================
+ (NSString *)commonMenuTitleBy:(MenuContentType)type{
    NSString *title = @"";
    switch (type) {
        case MenuContentType_DashBoard:
            title = [LanguageManager stringForKey:@"DashBoard"];
            break;
        case MenuContentType_Login:
            title = [LanguageManager stringForKey:@"Login"];
            break;
        case MenuContentType_ForgotPassword:
            title = [LanguageManager stringForKey:@"Forgot password"];
            break;
        case MenuContentType_Announcement:
            title = [LanguageManager stringForKey:@"Annoucement"];
            break;
            
        case MenuContentType_OrderBook:
            title = [LanguageManager stringForKey:@"Order Book"];
            break;
            
        case MenuContentType_Portfolio:
            title = [LanguageManager stringForKey:@"Portfolio"];
            break;
            
        case MenuContentType_StockTracker:
            //111
            if([[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"PH"]){
                title = [LanguageManager stringForKey:@"Stock Tracker"];
            }else{
                title = [LanguageManager stringForKey:@"Market Streamer"];
            }
            
            break;
            
        case MenuContentType_TermAndCondition:
            title = [LanguageManager stringForKey:@"Term & cond"];
            break;
        case MenuContentType_Tutorial:
            title = [LanguageManager stringForKey:@"Tutorial"];
            break;
        case MenuContentType_Setting:
            title = [LanguageManager stringForKey:@"Setting"];
            break;
        case MenuContentType_AboutUs:
            title = [LanguageManager stringForKey:@"About Us"];
            break;
        case MenuContentType_LogOut:
            title = [LanguageManager stringForKey:@"Logout"];
            break;
        case MenuContentType_Alert:
            title = [LanguageManager stringForKey:@"Alert"];
            break;
        case MenuContentType_Barcode:
            title = [LanguageManager stringForKey:@"Barcode"];
            break;
        default:
            break;
    }
    return title;
}

+ (NSString *)menuTitleBy:(MenuType)type{
    NSString *title = @"";
    switch (type) {
        case MenuType_Home:
            title = [LanguageManager stringForKey:Home];
            break;
        case MenuType_Quote:
            title = [LanguageManager stringForKey:Quote];
            break;
        case MenuType_WatchList:
            title = [LanguageManager stringForKey:_WatchList];
            break;
        case MenuType_News:
            title = [LanguageManager stringForKey:_News];
            break;
        case MenuType_Indices:
            title = [LanguageManager stringForKey:_Indices];
            break;
        case MenuType_Market:
            title = [LanguageManager stringForKey:Market];
            break;
        case MenuType_Announcement:
            title = [LanguageManager stringForKey:Annoucement];
            break;
        case MenuType_Setting:
            title = [LanguageManager stringForKey:Setting];
            break;
        case MenuType_Logout:
            title = [LanguageManager stringForKey:_Logout];
            break;
        default:
            break;
    }
    return title;
}

//========= Status Login For iPad Version ==========
+ (NSString *)statusLoginFrom:(LoginStages)state{
    NSString *msg = @"";
    switch (state) {
        case LOGIN_INIT:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"0%"}];
            break;
        case LOGIN_PK:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"20%"}];
            break;
        case LOGIN_KEY2:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"40%"}];
            break;
        case LOGIN_SESSION2:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"60%"}];
            break;
        case LOGIN_E2EE:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"80%"}];
            break;
        case LOGIN_LOGIN:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"95%"}];
            break;
        case LOGIN_SUCCESS:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"100%"}];
            break;
        default:
            break;
    }
    return msg;
}

//========= Status Login For Mobile Version ==========
+ (NSString *)statusLoginForMobileVersion:(LoginStages)state{
    NSString *msg = @"";
    switch (state) {
        case LOGIN_INIT:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], 0, @"%"];
			msg = @"Loggin on Securely\n(1/6)";
            break;
        case LOGIN_PK:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], [self getRandomNumberBetween:5 to:15], @"%"];
			msg = @"Loggin on Securely\n(1/6)";
            break;
        case LOGIN_KEY2:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], [self getRandomNumberBetween:16 to:30], @"%"];
			msg = @"Loggin on Securely\n(2/6)";
            break;
        case LOGIN_SESSION2:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], [self getRandomNumberBetween:31 to:45], @"%"];
			msg = @"Loggin on Securely\n(3/6)";
            break;
        case LOGIN_E2EE:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], [self getRandomNumberBetween:46 to:60], @"%"];
			msg = @"Loggin on Securely\n(4/6)";
            break;
        case LOGIN_LOGIN:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], [self getRandomNumberBetween:61 to:73], @"%"];
			msg = @"Loggin on Securely\n(4/6)";
            break;
        case LOGIN_GET_EXCHANGE:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], [self getRandomNumberBetween:74 to:85], @"%"];
			msg = @"Loggin on Securely\n(5/6)";
            break;
        case LOGIN_GET_CLIENT_ACCOUNT:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], [self getRandomNumberBetween:86 to:96], @"%"];
			msg = @"Loggin on Securely\n(5/6)";
            break;
        case LOGIN_SUCCESS:
            msg = [NSString stringWithFormat:@"%@%d%@", [LanguageManager stringForKey:@"Sign in..."], 100, @"%"];
			msg = @"Loggin on Securely\n(6/6)";
            break;
        default:
            break;
    }
    return msg;
}

//======= Language By Type=======
+ (NSString *)languageByType:(LanguageType)type{
    if (type == LanguageType_EN) {
        return @"en";
    }
    else if (type == LanguageType_CN) {
        return @"cn";
    }
    else if (type == LanguageType_CN_EXTEND) {
        return @"cn_ex";
    }
    else if (type == LanguageType_VN) {
        return @"vn";
    }
    return @"en";
}
/*
 #pragma mark - HUDr
 - (void)showHUD{
 [SVProgressHUD setDefaultMaskType:(SVProgressHUDMaskTypeGradient)];
 [SVProgressHUD show];
 
 }
 - (void)showHUDWithStatus:(NSString *)status{
 
 [SVProgressHUD showWithStatus:status];
 }
 
 - (void)showSuccessWithStatus:(NSString *)successStatus{
 
 [SVProgressHUD showSuccessWithStatus:successStatus];
 }
 - (void)showErrorWithStatus:(NSString *)errorStatus{
 
 [SVProgressHUD showErrorWithStatus:errorStatus];
 }
 - (void)dismissHUD{
 [SVProgressHUD dismiss];
 }
 */
#pragma mark - Broker Utils
+ (BOOL)isMultiInBroker:(BrokerModel *)broker{
    if ([broker.plistLink rangeOfString:kKeywordMultiBroker].location == NSNotFound) {
        return NO;
    }
    return YES;
}

#pragma mark - Selected Broker
+ (void)selectedBroker:(DetailBrokerModel *)detailBroker{
	//Add More
	[UserPrefConstants singleton].linksOnHomePage = detailBroker.linksOnHomePage;
	//---------------********-------------------------
	
    [UserPrefConstants singleton].brokerCode = detailBroker.BrokerID;
    [UserPrefConstants singleton].SponsorID = detailBroker.LMSSponsor;//[brokerSettings objectForKey:@"LMSSponsor"];
    [UserPrefConstants singleton].Vertexaddress = detailBroker.VertexServer;//[brokerSettings objectForKey:@"VertexServer"];
    [UserPrefConstants singleton].bhCode = detailBroker.BHCode;//[brokerSettings objectForKey:@"BHCode"];
    
    [UserPrefConstants singleton].sessionTimeoutSeconds = (int)(detailBroker.LogoutCountdownTime * 60);//[[brokerSettings objectForKey:@"LogoutCountdownTime"] intValue]*60;
    
    if ([UserPrefConstants singleton].sessionTimeoutSeconds<=0)
        [UserPrefConstants singleton].sessionTimeoutSeconds = kTimeOutDuration; // default 30 mins
    
    [UserPrefConstants singleton].CreditLimitOrdPad = detailBroker.CreditLimitOrdPad;//[[brokerSettings objectForKey:@"CreditLimitOrdPad"] boolValue];
    [UserPrefConstants singleton].CreditLimitOrdBook = detailBroker.CreditLimitOrdBook;//[[brokerSettings objectForKey:@"CreditLimitOrdBook"] boolValue];
    [UserPrefConstants singleton].CreditLimitPortfolio = detailBroker.CreditLimitPortfolio;//[[brokerSettings objectForKey:@"CreditLimitPortfolio"] boolValue];
    [UserPrefConstants singleton].TrdShortSellIndicator = detailBroker.TrdShortSellIndicator;//[brokerSettings objectForKey:@"TrdShortSellIndicator"];
    
    [UserPrefConstants singleton].msgPromptForStopNIfTouchOrdType = detailBroker.msgPromptForStopNIfTouchOrdType;//[[brokerSettings objectForKey:@"msgPromptForStopNIfTouchOrdType"] boolValue];
    
    [UserPrefConstants singleton].LoginHelpText_EN = detailBroker.LoginHelpText;//[brokerSettings objectForKey:@"LoginHelpText"];
    [UserPrefConstants singleton].LoginHelpText_CN = detailBroker.LoginHelpTextCN;//[brokerSettings objectForKey:@"LoginHelpTextCN"];
    // MARK: testing Chinese language (N2N only)
    //  [UserPrefConstants singleton].Vertexaddress = @"nogf1-uat.asiaebroker.com";
    
    //[UserPrefConstants singleton].userATPServerIP = [brokerSettings objectForKey:@"ATPServer"];
    
    [UserPrefConstants singleton].userATPServerIP = detailBroker.ATPServer;//[brokerSettings objectForKey:@"ATPServer"];
    
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
        [UserPrefConstants singleton].LMSServerAddress = @"http://125.5.209.29/lms/websub/authen?";
        
    }else{
        [UserPrefConstants singleton].LMSServerAddress = detailBroker.LMSServer;//[brokerSettings objectForKey:@"LMSServer"];
    }
    
    [UserPrefConstants singleton].isEnabledESettlement = detailBroker.ESettlementEnabled;//[[brokerSettings objectForKey:@"ESettlementEnabled"] boolValue];
    [UserPrefConstants singleton].eSettlementURL = detailBroker.ESettlementURL;//[brokerSettings objectForKey:@"ESettlementURL"];
    [UserPrefConstants singleton].ATPLoadBalance = detailBroker.ATPLoadBalance;//[[brokerSettings objectForKey:@"ATPLoadBalance"] boolValue];
    [UserPrefConstants singleton].HTTPSEnabled = detailBroker.HTTPSEnabled;//[[brokerSettings objectForKey:@"HTTPSEnabled"] boolValue];
    [UserPrefConstants singleton].isEncryption = detailBroker.isEncryption;
    [UserPrefConstants singleton].AES_EncryptionEnabled = detailBroker.AES_Encryption;
    [UserPrefConstants singleton].isEnabledPassword = detailBroker.EnablePassword;
    [UserPrefConstants singleton].isEnabledPin = detailBroker.EnablePin; //-> 7_ hardcode EnabledPin
    [UserPrefConstants singleton].E2EEEncryptionEnabled = detailBroker.E2EE_Encryption;//[[brokerSettings objectForKey:@"E2EE_Encryption"] boolValue];
    NSString *brokername = detailBroker.BrokerName;//[brokerSettings objectForKey:@"BrokerName"];
    if ([brokername length]>0) [UserPrefConstants singleton].brokerName = brokername;
    
    [UserPrefConstants singleton].PFDisclaimerWebView = detailBroker.PFDisclaimerWebView;//[[brokerSettings objectForKey:@"PFDisclaimerWebView"] boolValue];
    [UserPrefConstants singleton].PFDisclaimerWebViewURL = detailBroker.PFDisclaimerWebViewURL;//[brokerSettings objectForKey:@"PFDisclaimerWebViewURL"];
    [UserPrefConstants singleton].filterShowMarket = detailBroker.filterShowMarket;//[brokerSettings objectForKey:@"filterShowMarket"];
    
    [UserPrefConstants singleton].isArchiveNews = detailBroker.ArchiveNewsEnabled;//[[brokerSettings objectForKey:@"ArchiveNewsEnabled"] boolValue];
    [UserPrefConstants singleton].termAndConditions_EN = detailBroker.TermsNCondContentOnLoginPg;//[brokerSettings objectForKey:@"TermsNCondContentOnLoginPg"];
    [UserPrefConstants singleton].termAndConditions_CN = detailBroker.TermsNCondContentOnLoginPgCN;//[brokerSettings objectForKey:@"TermsNCondContentOnLoginPgCN"];
    [UserPrefConstants singleton].PrimaryExchg = detailBroker.PrimaryExchg;//[brokerSettings objectForKey:@"PrimaryExchg"];
    [UserPrefConstants singleton].NewsServerAddress = detailBroker.NewsServer;//[brokerSettings objectForKey:@"NewsServer"];
    
    if(ISDUMPDATAFORNEWFUNCS){
        //        [UserPrefConstants singleton].enableIDSS = YES;
        [UserPrefConstants singleton].enabledNewiBillionaire = YES;
        
        [UserPrefConstants singleton].filteriBillionaire = @[@"O",
                                                             @"N",
                                                             @"A",
                                                             @"ND",
                                                             @"OD",
                                                             @"AD"];
        
        [UserPrefConstants singleton].ElasticNewsServerAddress = @"http://sgnews.cgs-cimb.com/gcNEWS/srvs/displayNews";
    }else{
        [UserPrefConstants singleton].enabledNewiBillionaire = detailBroker.EnabledNewiBillionaire;
        
        [UserPrefConstants singleton].filteriBillionaire = detailBroker.FilteriBillionaire;
        
        [UserPrefConstants singleton].ElasticNewsServerAddress = detailBroker.ElasticNewsServer;//[brokerSettings objectForKey:@"ElasticNewsServer"];
        
    }
    
    [UserPrefConstants singleton].enableIDSS = detailBroker.EnableIDSS;
    // Fundamentals/Factset/CPIQ things
    [UserPrefConstants singleton].fundamentalNewsLabel = detailBroker.FundamentalNewsLabel;//[brokerSettings objectForKey:@"FundamentalNewsLabel"];
    [UserPrefConstants singleton].fundamentalNewsServer= detailBroker.FundamentalNewsServer;//[brokerSettings objectForKey:@"FundamentalNewsServer"];
    [UserPrefConstants singleton].fundamentalSourceCode= detailBroker.FundamentalSourceCode;//[brokerSettings objectForKey:@"FundamentalSourceCode"];
    [UserPrefConstants singleton].fundDataCompInfoURL= detailBroker.FundDataCompInfoURL;//[brokerSettings objectForKey:@"FundDataCompInfoURL"];
    [UserPrefConstants singleton].fundFinancialInfoURL= detailBroker.FundDataFinancialInfoURL;//[brokerSettings objectForKey:@"FundDataFinancialInfoURL"];
    [UserPrefConstants singleton].fundamentalDefaultReportCode= detailBroker.FundamentalDefaultReportCode;//[brokerSettings objectForKey:@"FundamentalDefaultReportCode"];
    
    // chart
    [UserPrefConstants singleton].chartType = detailBroker.chartType;//[brokerSettings objectForKey:@"chartType"];
    [UserPrefConstants singleton].Register2FAPgURL = detailBroker.Register2FAPgURL;//[brokerSettings objectForKey:@"2FARegisterPgURL"];
    
    //stock Alert
    //http://noglu.asiaebroker.com/mpn/srvs/registerDevice
    [UserPrefConstants singleton].isEnabledStockAlert = detailBroker.EnabledStockAlert;//[[brokerSettings objectForKey:@"EnabledStockAlert"] boolValue];
    [UserPrefConstants singleton].isShownStockAlertDisclaimer = detailBroker.EnabledStockAlertDisclaimer;//[[brokerSettings objectForKey:@"EnabledStockAlertDisclaimer"] boolValue];
    [UserPrefConstants singleton].stockAlertURL = detailBroker.StockAlertPageURL;//[brokerSettings objectForKey:@"StockAlertPageURL"];//@"https://sans-uat.asiaebroker.com/gcSANS/web/html/StockAlert.html";
    [UserPrefConstants singleton].pushNotificationRegisterURL = detailBroker.PushNotificationRegisterURL;//[brokerSettings objectForKey:@"PushNotificationRegisterURL"];//@"https://sans-uat.asiaebroker.com/gcSANS/srvs/registerDevice";
    [UserPrefConstants singleton].stockAlertGetPublicURL =  detailBroker.StockAlertAuthKey;//[brokerSettings objectForKey:@"StockAlertAuthKey"];//@"https://sans-uat.asiaebroker.com/gcSANS/srvs/Auth/getRecKey";
    
    [UserPrefConstants singleton].interactiveChartEnabled= detailBroker.InteractiveChartEnabled;//[[brokerSettings objectForKey:@"InteractiveChartEnabled"] boolValue];
    [UserPrefConstants singleton].quantityIsLot = detailBroker.QtyIsLot;
    [UserPrefConstants singleton].quantityModifierMode = detailBroker.QtyModifierMode;
    if([SettingManager shareInstance].currentQuantityType == -1){
        if(detailBroker.QtyIsLot == YES){
            [SettingManager shareInstance].currentQuantityType = 1;
        }else{
            [SettingManager shareInstance].currentQuantityType = 0;
        }
    }
    //    _canChangeQuantity = [UserPrefConstants singleton].quantityModifierMode;
    //    _currentQuantityType = 0;
    
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    //    if ([def objectForKey:@"QtyIsLot"]==nil) {
    //        [UserPrefConstants singleton].quantityIsLot =  !detailBroker.DefaultTradePrefQtyUnit;//![[brokerSettings objectForKey:@"DefaultTradePrefQtyUnit"] boolValue];
    //    } else {
    //        [UserPrefConstants singleton].quantityIsLot = [def boolForKey:@"QtyIsLot"];
    //    }
    //
    
    
    
    
	if ([[UserPrefConstants singleton].chartType length]<=0){
		[UserPrefConstants singleton].chartType = @"Image";
	}
    [UserPrefConstants singleton].interactiveChartByExchange = [detailBroker.InteractiveChartForExch mutableCopy];//[brokerSettings objectForKey:@"InteractiveChartForExch"];
    [UserPrefConstants singleton].chartTypeToLoad = [UserPrefConstants singleton].chartType;
    
    
    
    /* NSLog(@"\n\n ------------------------------------------------------ \n");
     NSLog(@"Vertex: %@",[UserPrefConstants singleton].Vertexaddress);
     NSLog(@"ATP: %@",[UserPrefConstants singleton].userATPServerIP);
     NSLog(@"LMS: %@",[UserPrefConstants singleton].LMSServerAddress);
     NSLog(@"News Server: %@", [UserPrefConstants singleton].NewsServerAddress);
     NSLog(@"ChartToLoadDefault: %@",[UserPrefConstants singleton].chartTypeToLoad);
     NSLog(@"quantityIsLot: %d",[UserPrefConstants singleton].quantityIsLot);
     NSLog(@"\n\n\n");*/
    
    //default starting screen
    if ([def objectForKey:@"ScreenViewMode"]==nil) {
        [UserPrefConstants singleton].defaultStartingScreen = detailBroker.DefaultStartingScreen;//[brokerSettings objectForKey:@"DefaultStartingScreen"];
        if ([[UserPrefConstants singleton].defaultStartingScreen length]<=0)
            [UserPrefConstants singleton].defaultStartingScreen = @"Home";
        
        if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Home"])
            [UserPrefConstants singleton].defaultView = 1;
        else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Quote"])
            [UserPrefConstants singleton].defaultView = 2;
        else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"WatchList"])
            [UserPrefConstants singleton].defaultView = 3;
        
        
    } else {
        int defView =  [[def objectForKey:@"ScreenViewMode"] intValue];
        [UserPrefConstants singleton].defaultView = defView;
        if (defView==1)
            [UserPrefConstants singleton].defaultStartingScreen = @"Home";
        else if (defView==2)
            [UserPrefConstants singleton].defaultStartingScreen = @"Quote";
        else if (defView==3)
            [UserPrefConstants singleton].defaultStartingScreen = @"WatchList";
    }
    [UserPrefConstants singleton].dashBoard = detailBroker.DashBoard;
    [UserPrefConstants singleton].interactiveChartURL = detailBroker.InteractiveChartURL;//[brokerSettings objectForKey:@"InteractiveChartURL"];
	[UserPrefConstants singleton].tvChart = detailBroker.tvChart;
	
	
    [UserPrefConstants singleton].intradayChartURL = detailBroker.IntradayChartServer;//[brokerSettings objectForKey:@"IntradayChartServer"];
    [UserPrefConstants singleton].historicalChartURL = detailBroker.HistoricalChartServer;//[brokerSettings objectForKey:@"HistoricalChartServer"];
    //Settings
    [UserPrefConstants singleton].companyName = detailBroker.CompanyName;//[brokerSettings objectForKey:@"CompanyName"];
    [UserPrefConstants singleton].companyTel = detailBroker.CompanyTel;//[brokerSettings objectForKey:@"CompanyTel"];
    [UserPrefConstants singleton].companyAddress = detailBroker.CompanyAddress;//[brokerSettings objectForKey:@"CompanyAddress"];
    [UserPrefConstants singleton].companyFax = detailBroker.CompanyFax;//[brokerSettings objectForKey:@"CompanyFax"];
    [UserPrefConstants singleton].companyLogo = detailBroker.CompanyLogo;//[brokerSettings objectForKey:@"CompanyLogo"];
    [UserPrefConstants singleton].shareText = detailBroker.ShareText;//[brokerSettings objectForKey:@"ShareText"];
    [UserPrefConstants singleton].facebookLink = detailBroker.FacebookLink;//[brokerSettings objectForKey:@"FacebookLink"];
    [UserPrefConstants singleton].twitterText = detailBroker.TwitterLink;//[brokerSettings objectForKey:@"TwitterLink"];
    [UserPrefConstants singleton].linkInText = detailBroker.LinkedInLink;//[brokerSettings objectForKey:@"LinkedInLink"];
    [UserPrefConstants singleton].websiteLink = detailBroker.WebsiteLink;//[brokerSettings objectForKey:@"WebsiteLink"];
    [UserPrefConstants singleton].emailLink = detailBroker.EmailLink;//[brokerSettings objectForKey:@"EmailLink"];
    [UserPrefConstants singleton].aboutUs = detailBroker.AboutUs;//[brokerSettings objectForKey:@"AboutUs"];
    [UserPrefConstants singleton].appName = detailBroker.AppName;//[brokerSettings objectForKey:@"AppName"];
    
    [UserPrefConstants singleton].tempQcServer = @"ndgfvu.mytrade.com.ph";//[brokerSettings objectForKey:@"QCServer"];
    [UserPrefConstants singleton].isShowTradingLimit = detailBroker.TradingLimitEnabled;//[[brokerSettings objectForKey:@"TradingLimitEnabled"] boolValue];
    
    [UserPrefConstants singleton].tempUserParamQC = @"[vUpJYKw4QvGRMBmhATUxRwv4JrU9aDnwNEuangVyy6OuHxi2YiY=]";
    
    [UserPrefConstants singleton].AnnouncementURL = detailBroker.AnnouncementURL;//[brokerSettings objectForKey:@"AnnouncementURL"];
    
    [UserPrefConstants singleton].orderHistLimit = detailBroker.OrderHistoryLimit;//[brokerSettings objectForKey:@"OrderHistoryLimit"];
    // default to 30 days
    if ([[UserPrefConstants singleton].orderHistLimit length]<=0) [UserPrefConstants singleton].orderHistLimit = @"30";
    
    // Indices button
    [UserPrefConstants singleton].indicesEnableByExchange = [detailBroker.ExchangesEnableIndices mutableCopy];//[brokerSettings objectForKey:@"ExchangesEnableIndices"];
    
    // OrderPad
    [UserPrefConstants singleton].defaultCurrency = detailBroker.DefaultCurrency;//[brokerSettings objectForKey:@"DefaultCurrency"];
    [UserPrefConstants singleton].isEnableRDS = detailBroker.RDS;//[[brokerSettings objectForKey:@"RDS"] boolValue];
    
    // Override Exchange name
    [UserPrefConstants singleton].overrideExchangesName = detailBroker.OverrideExchangeName;//[NSDictionary dictionaryWithDictionary:[brokerSettings objectForKey:@"OverrideExchangeName"]];
    
    // Market Streamer
    [UserPrefConstants singleton].isShowMarketStreamer = detailBroker.MarketStreamer;//[[brokerSettings objectForKey:@"MarketStreamer"] boolValue];
}

#pragma mark - Array Util / FILTER STOCK
+ (NSMutableArray *)rankedByNameArr{
    NSArray *sortList = [AppConstants SortByListArray];
    NSMutableArray *_arr = @[].mutableCopy;
    for (int i=0; i< [sortList count]; i++) {
        NSDictionary *eachDict = (NSDictionary*)[[AppConstants SortByListArray] objectAtIndex:i];
        [_arr addObject:[[eachDict allValues] objectAtIndex:0]];
    }
    return _arr;
}

+ (NSMutableArray *)rankedByKeyArr{
    NSArray *sortList = [AppConstants SortByListArray];
    NSMutableArray *_arr = @[].mutableCopy;
    for (int i=0; i< [sortList count]; i++) {
        NSDictionary *eachDict = (NSDictionary*)[[AppConstants SortByListArray] objectAtIndex:i];
        [_arr addObject:[[eachDict allKeys] objectAtIndex:0]];
    }
    return _arr;
}

#pragma mark - RawValue ChartType
+ (NSInteger)rawValueFrom:(ChartType)type{
    NSInteger rawValue = K_DAY;
    switch (type) {
        case ChartType_Day:
            rawValue = K_DAY;
            break;
        case ChartType_Week:
            rawValue = K_WEEK;
            break;
        case ChartType_1Month:
            rawValue = K_1MONTH;
            break;
        case ChartType_3Month:
            rawValue = K_3MONTH;
            break;
        case ChartType_6Month:
            rawValue = K_6MONTH;
            break;
        case ChartType_1Year:
            rawValue = K_1YEAR;
            break;
        case ChartType_2Year:
            rawValue = K_2YEAR;
            break;
            
        default:
            break;
    }
    return rawValue;
}

#pragma mark - Index of Object from Array
+ (NSInteger)indexOfStockByCode:(NSString *)stCode fromArray:(NSArray *)stocks{
    NSInteger index = -1; //Default unknown state (not found index of item)
    for (int i = 0; i < stocks.count; i ++) {
        if ([stCode isEqualToString:[stocks[i] stockCode]]) {
            index = i;
            break;
        }
    }
    return index;
}

#pragma mark - StockDetail from QCFeed by Code
+ (StockModel *)stockQCFeedDataFromCode:(NSString *)stockCode{
    id aaa = [[[QCData singleton] qcFeedDataDict] allKeys];
    if (stockCode && ([stockCode length] > 0) && ([[[QCData singleton] qcFeedDataDict] objectForKey:stockCode])) {
        if ([[[[QCData singleton] qcFeedDataDict] allKeys] containsObject:stockCode]) {
            NSDictionary *stockDetail = [[[QCData singleton] qcFeedDataDict]objectForKey:stockCode];
            return [[StockModel alloc] initWithDictionary:stockDetail error:nil];
        }
    }
    return  nil;
}

#pragma mark - Parse News to model
+ (NSArray<NewsModel *> *)parseNewsArrFrom:(NSArray *)newsArr byType:(NewsType)type{
    NSMutableArray *pasredArr = @[].mutableCopy;
    switch (type) {
        case NewsType_Archive:{
            for (NSDictionary *item in newsArr) {
                NewsModel *model = [[NewsModel alloc] init];
                for (NSString *key in item.allKeys) {
                    if ([key isEqualToString:@"stk"]) {
                        model.exchange = item[key];
                    }
                    if ([key isEqualToString:@"dt"]) {
                        model.timeStamp = [[UserPrefConstants singleton] getLogicalDate:item[key]];
                    }
                    if ([key isEqualToString:@"t"]) {
                        model.title = [[UserPrefConstants singleton] stringByStrippingHTML:item[key]];
                    }
                    if ([key isEqualToString:@"id"]) {
                        model.id = item[key];
                    }
                    [pasredArr addObject:model];
                }
            }
        }
            break;
        case NewsType_Jar:{
            for (NSString *item in newsArr) {
                NSArray *subItems = [item componentsSeparatedByString:@"|"];
                NewsModel *model = [[NewsModel alloc] init];
                NSString *exchange = subItems[10];
                if (exchange.length <= 0) {
                    exchange = [LanguageManager stringForKey:@"Announcement"];
                }
                model.id = subItems.firstObject;
                model.exchange = exchange;
                model.title = subItems[4];
                model.timeStamp = [[UserPrefConstants singleton] getLogicalDate:[subItems objectAtIndex:3]];
                [pasredArr addObject:model];
            }
        }
            break;
        default:{
            //Use default - company new, elastic....
            for (id item in newsArr) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *_item = (NSDictionary *)item;
                    NewsModel *model = [[NewsModel alloc] init];
                    for (NSString *key in _item.allKeys) {
                        if ([key isEqualToString:@"3"]) {
                            model.title = _item[key];
                        }
                        if ([key isEqualToString:@"4"]) {
                            model.timeStamp = [[UserPrefConstants singleton] getLogicalDate:item[key]];
                        }
                        if ([key isEqualToString:@"6"]) {
                            model.id = item[key];
                        }
                        
                        //Follow this on 2.0
                        if ([key isEqualToString:@"8"]) {
                            model.sourceNews = item[key];
                        }
                    }
                    model.exchange = model.sourceNews;
                    [pasredArr addObject:model];
                }
            }
        }
            break;
    }
    return [pasredArr copy];
}
#pragma mark - Load Sector For Mobile Version
//Using for Mobile Version
+ (NSArray <SectorInfo *> *)loadInfoSectors{
    return [self loadInfoSectorsExcludeSectorAll:NO];
}

+ (NSArray <SectorInfo *> *)loadInfoSectorsExcludeSectorAll:(BOOL)isExclude{
    //[UserPrefConstants singleton].currentExchangeInfo = [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:[UserPrefConstants singleton].userCurrentExchange];
    ExchangeInfo *currInfo = [UserPrefConstants singleton].currentExchangeInfo;
    if (!currInfo) {
        return @[].copy;
    }
    NSMutableArray *_sectorInfos = @[].mutableCopy;
    NSArray *sectArr = [currInfo.sectorParents allKeys];
    SectorInfo *sectorAll;
    for (int i=0; i<[sectArr count]; i++) {
        NSString *key = [sectArr objectAtIndex:i];
        SectorInfo *inf = [[SectorInfo alloc] init];
        inf.sectorCode = key;
        inf.sectorName = [currInfo.sectorInfos objectForKey:key];
        inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"[" withString:@""];
        inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"]" withString:@""];
        if ([inf.sectorName isEqualToString:@"TOTAL"]){
            inf.sectorName = @"All Stocks";
            sectorAll = inf;
            continue;
        }
        if ([inf.sectorName removeAllWhitespace].length == 0) {
            inf.sectorName = @"All Stocks";
        }
        // add parents only
        NSString *parent = [currInfo.sectorParents objectForKey:key];
        if ([parent isEqualToString:@"0"]){
            [_sectorInfos addObject:inf];
        }
    }
    //Sort Alpha
    NSSortDescriptor *sortDescriptor =
    [NSSortDescriptor sortDescriptorWithKey:@"sectorName"
                                  ascending:YES
                                   selector:@selector(caseInsensitiveCompare:)];
    _sectorInfos = [[_sectorInfos sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    if (sectorAll) {
        if (!isExclude) {
            [_sectorInfos insertObject:sectorAll atIndex:0];
        }
    }
    //+++ Follow on iPad Version: Populate childrens
    for (int i=0; i<[_sectorInfos count]; i++) {
        SectorInfo *inf = [_sectorInfos objectAtIndex:i];
        for (int j=0; j<[sectArr count]; j++) {
            NSString *key = [sectArr objectAtIndex:j];
            NSString *parent = [currInfo.sectorParents objectForKey:key];
            if ([parent isEqualToString:inf.sectorCode]) {
                SectorInfo *subInf = [[SectorInfo alloc] init];
                subInf.sectorName = [currInfo.sectorInfos objectForKey:key];
                subInf.sectorCode = key;
                [inf.subItemsArray addObject:subInf];
                [_sectorInfos replaceObjectAtIndex:i withObject:inf];
            }
        }
    }
    
    return [_sectorInfos copy];
}

//************************** SECTOR FOR INDICES ****************************

+ (NSArray *)loadAllSectorsNotTreeData{
    ExchangeInfo *currInfo = [UserPrefConstants singleton].currentExchangeInfo;
    if (!currInfo) {
        return @[].copy;
    }
    NSMutableArray *_sectorInfos = @[].mutableCopy;
    NSDictionary *sectorDict = currInfo.sectorInfos;
    for (NSString *key in sectorDict.allKeys) {
        SectorInfo *inf = [[SectorInfo alloc] init];
        inf.sectorCode = key;
        inf.sectorName = sectorDict[key];
        inf.sectorName = [currInfo.sectorInfos objectForKey:key];
        inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"[" withString:@""];
        inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"]" withString:@""];
        [_sectorInfos addObject:inf];
    }
    return [_sectorInfos copy];
}
+ (SectorInfo *)getSectorInfoObjFor:(NSString*)sectName {
    //NSArray *allSectors = [self loadAllSectorsNotTreeData];
    NSArray *allSectors = [self loadInfoSectors];
    return [self sectorFromList:allSectors byName:sectName];
    //	NSArray *secotors = [self loadInfoSectors];
    //	for (int i = 0; i < [secotors count]; i++) {
    //
    //		SectorInfo *inf = [secotors objectAtIndex:i];
    //		if ([inf.sectorName isEqualToString:sectName]) {
    //			return inf;
    //		}
    //
    //		if ([inf.subItemsArray count]>0) {
    //			for (int j=0; j<[inf.subItemsArray count]; j++) {
    //				SectorInfo *subInf = [inf.subItemsArray objectAtIndex:j];
    //				if ([subInf.sectorName containsString:sectName]) {
    //					return subInf;
    //				}
    //			}
    //		}
    //	}
    //	return nil;
}

+ (SectorInfo *)sectorFromList:(NSArray *)sectors byName:(NSString *)name{
    NSString *specialChars = @"()- /&_.*~\\[]";
    SectorInfo *supperContainSector = nil;
    for (SectorInfo *info in sectors) {
        if ([info.sectorName.uppercaseString isEqualToString:name.uppercaseString] || [[info.sectorName.uppercaseString removeAllWhitespace] isEqualToString:[name.uppercaseString removeAllWhitespace]] || [[info.sectorName.uppercaseString removeSpecialChars:specialChars] isEqualToString:[name.uppercaseString removeSpecialChars:specialChars]]) {
            return info;
        }
        if ([info.sectorName.uppercaseString containsString:name.uppercaseString] || [name.uppercaseString containsString:info.sectorName.uppercaseString] || [[info.sectorName.uppercaseString removeSpecialChars:specialChars] containsString:[name.uppercaseString removeSpecialChars:specialChars]] || [[name.uppercaseString removeSpecialChars:specialChars] containsString:[info.sectorName.uppercaseString removeSpecialChars:specialChars]]) {
            supperContainSector = info;
        }
        if (info.subItemsArray.count > 0) {
            for (SectorInfo *subInfo in info.subItemsArray) {
                if ([subInfo.sectorName.uppercaseString isEqualToString:name.uppercaseString] || [[subInfo.sectorName.uppercaseString removeAllWhitespace] isEqualToString:[name.uppercaseString removeAllWhitespace]] || [[info.sectorName.uppercaseString removeSpecialChars:specialChars] isEqualToString:[name.uppercaseString removeSpecialChars:specialChars]]) {
                    return subInfo;
                }
                if ([subInfo.sectorName.uppercaseString containsString:name.uppercaseString] || [name.uppercaseString containsString:subInfo.sectorName.uppercaseString] || [[subInfo.sectorName.uppercaseString removeSpecialChars:specialChars] containsString:[name.uppercaseString removeSpecialChars:specialChars]] || [[name.uppercaseString removeSpecialChars:specialChars] containsString:[subInfo.sectorName.uppercaseString removeSpecialChars:specialChars]]) {
                    return info;
                }
                if (subInfo.subItemsArray.count > 0) {
                    return [self sectorFromList:info.subItemsArray byName:name];
                }
            }
            if (supperContainSector) {
                return supperContainSector;
            }
        }
    }
    return nil;
}

#pragma mark - Check Account is Existed
+ (BOOL)isExistedAccountLogined{
    if ([UserSession shareInstance].userName && [UserSession shareInstance].password) {
        return YES;
    }
    return NO;
}

#pragma mark - List Exchange Name
+ (NSArray *)getNamesOfExchange:(NSArray *)exchanges{
    NSMutableArray *names = @[].mutableCopy;
    for (ExchangeData *ex in exchanges) {
        [names addObject:[Utils getShortExChangeName:ex]];
    }
    return [names copy];
}
+ (NSString *)getShortExChangeName:(ExchangeData *)ex{
	return ex.exchange_name;
    NSDictionary *sortNameDict = [FIXED_SORTED_EXCHANGES_DICT copy];
    NSString *sortedName = ex.trade_exchange_code;
    NSString *suffix = nil;
    if ([sortNameDict.allKeys containsObject:sortedName]) {
        sortedName = sortNameDict[sortedName];
    }
    //Check suffix name
    if ([ex.exchange_description.lowercaseString containsString:@"derivative".lowercaseString] || [ex.exchange_description.lowercaseString containsString:[LanguageManager stringForKey:@"Derivative".lowercaseString]] || [ex.exchange_description.lowercaseString containsString:@"Derivatives".lowercaseString]) {
        if (![sortedName containsString:@"DT"]) {
            suffix = [LanguageManager stringForKey:@"DT"];
        }
    }
    if ([ex.exchange_description.lowercaseString containsString:@"equity".lowercaseString] || [ex.exchange_description.lowercaseString containsString:[LanguageManager stringForKey:@"Equity".lowercaseString]] || [ex.exchange_description.lowercaseString containsString:@"Equities".lowercaseString]) {
        if (![sortedName containsString:@"EQ"]) {
            suffix = [LanguageManager stringForKey:@"EQ"];
        }
    }
    if (suffix) {
        sortedName = [NSString stringWithFormat:@"%@ - %@", sortedName, suffix];
    }
    return sortedName;
}

//list all exchange
+ (NSArray *)getListExchangeName{
    NSArray *exchangeList = [[UserPrefConstants singleton].supportedExchanges copy];
    return [self getNamesOfExchange:exchangeList];
}

+ (ExchangeData *)getExchange:(NSString *)code{
    NSArray *exchangeList = [[UserPrefConstants singleton].supportedExchanges copy];
    for (ExchangeData *ex in exchangeList){
        if([ex.trade_exchange_code isEqualToString:code]){
            return ex;
        }
    }
    return nil;
}
+ (NSInteger)inexOfCurExchangeData:(ExchangeData *)curExData{
    if ([UserPrefConstants singleton].supportedExchanges.count > 0) {
        return [[UserPrefConstants singleton].supportedExchanges indexOfObject:curExData];
    }
    return -1;
}
+ (NSInteger)getIndexOfCurrentExchange{
    __block NSInteger _curIndex = 0;
    NSArray *exchangeList = [[UserPrefConstants singleton].supportedExchanges copy];
    NSString *userCurEx = [UserPrefConstants singleton].userCurrentExchange;
    [exchangeList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ExchangeData *ex =  (ExchangeData *)obj;
        if ([userCurEx isEqualToString:ex.feed_exchg_code]) {
            _curIndex = idx;
        }
    }];
    return _curIndex;
}
#pragma mark - Selected New Exchange
+ (void)selectedNewExchange:(ExchangeData *)newEx{
    [UserPrefConstants singleton].currentExchangeData = newEx;
    [[VertxConnectionManager singleton] vertxUnsubscribeAllInQcFeed];
    
    // CLEAR PREVIOUS QCFEED DATA WHEN CHANGING EXCHANGE
    [[[QCData singleton]qcFeedDataDict] removeAllObjects];
    [[UserPrefConstants singleton].tickerStocks removeAllObjects];
    
    // CLEAR PREVIOUS SCOREBOARD DATA WHEN CHANGING EXCHANGE
    [[QCData singleton].qcScoreBoardDict removeAllObjects];
    
    [UserPrefConstants singleton].userCurrentExchange = newEx.feed_exchg_code;
    
    NSString *exchgStr = [NSString stringWithFormat:@"Exchange%@",[UserPrefConstants singleton].brokerCode];
    [[NSUserDefaults standardUserDefaults]setObject:newEx.feed_exchg_code forKey:exchgStr];
    [[NSUserDefaults standardUserDefaults]synchronize];
    //[[VertxConnectionManager singleton]  vertxGetKLCI];
    
    [[UserPrefConstants singleton] overrideChartSettings];
    [UserPrefConstants singleton].userSelectingStockCode = nil;
    
    //+++What is it for ???
    //[[VertxConnectionManager singleton] vertxSortForTicker:@"101" Direction:@"DESC" exchg:[UserPrefConstants singleton].userCurrentExchange];
    
    // update current Exchange Info
    if ([UserPrefConstants singleton].vertxExchangeInfo.count > 0) {
        if ([[UserPrefConstants singleton].vertxExchangeInfo.allKeys containsObject:newEx.feed_exchg_code]) {
            [UserPrefConstants singleton].currentExchangeInfo = [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:newEx.feed_exchg_code];
        }
    }
    
    //For Sector, Market info
    NSDictionary *marketInfos = [UserPrefConstants singleton].currentExchangeInfo.marketInfos;
    if (marketInfos.count > 0) {
        NSArray *allKeys = [marketInfos.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
            return obj1.integerValue > obj2.integerValue;
        }];
        //Check selected
        [UserPrefConstants singleton].selectedMarket = allKeys.firstObject;
    }
    [UserPrefConstants singleton].sectorToFilter = nil;
    [UserPrefConstants singleton].selectedSector = nil;
    [UserPrefConstants singleton].selectedSubsector = nil;
    
    //+++ For Decimal
    if ([newEx.feed_exchg_code isEqualToString:@"JKD"]) {
        //Set decimal to 0 ?
    }else if ([newEx.feed_exchg_code isEqualToString:@"PHD"]){
        [UserPrefConstants singleton].pointerDecimal = 4;
    }else{
        //Do nothings...
    }
    
    // Delay execution of my block for 1 seconds.
    //+++ Use this at Menu Page
    /*
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
     //Post Notification here
     [[NSNotificationCenter defaultCenter] postNotificationName:kDidSelectNewExchangeNotification object:newEx];
     });
     */
}

//UserAccountClientData
//tunv
+ (TradingRules *)getTradeRules{
    NSString *currentExchange = [UserPrefConstants singleton].userCurrentExchange;
    return [[UserPrefConstants singleton].tradingRules objectForKey:currentExchange];
}
+ (NSMutableArray *)getOrderTypeList
{
    if([[self getTradeRules].orderTypeList isKindOfClass:[NSMutableArray class]]){
        return [self getTradeRules].orderTypeList;
    }else{
        return [[NSMutableArray alloc] initWithObjects:[self getTradeRules].orderTypeList, nil];
    }
}

+ (NSMutableArray *)getValidityList
{
    if([[self getTradeRules].validityList isKindOfClass:[NSMutableArray class]]){
        return [self getTradeRules].validityList;
    }else{
        return [[NSMutableArray alloc] initWithObjects:[self getTradeRules].validityList, nil];
    }
}

+ (NSMutableArray *)getExchangeList{
    return [[UserPrefConstants singleton].supportedExchanges mutableCopy];
}

+ (NSMutableArray *)getPaymentList{
    NSArray *stkCodeAndExchangeArr= [[UserPrefConstants singleton].userSelectingStockCode  componentsSeparatedByString:@"."];
    TradingRules *trdRules = [[[UserPrefConstants singleton]tradingRules] objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
    NSString *exChange = [stkCodeAndExchangeArr objectAtIndex:1];
    
    NSString *stkCode = [UserPrefConstants singleton].userSelectingStockCode;
    NSRange range = [stkCode rangeOfString:@"."];
    NSString *stockCountry = [[stkCode substringFromIndex:NSMaxRange(range)]
                              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    UserAccountClientData *uacd = [UserPrefConstants singleton].userSelectedAccount;
    
    if (([trdRules.paymentCfgList count]>0)&&([exChange isEqualToString:@"SG"])) {
        
        long index = -1;
        
        for(int i=0;i<[trdRules.paymentCfgList count];i++)
        {
            PaymentCUT *cut = [trdRules.paymentCfgList objectAtIndex:i];
            if(([stockCountry isEqualToString:cut.paymentCountry]) &&([uacd.account_type  isEqualToString:cut.AccountType]))
            {
                return [[NSMutableArray alloc]initWithArray:[cut paymentOptions]];
            }
        }
        
        for(int i=0;i<[trdRules.paymentCfgList count];i++)
        {
            PaymentCUT *cut = [trdRules.paymentCfgList objectAtIndex:i];
            if(([stockCountry isEqualToString:[cut paymentCountry]]) && ([@"Def" isEqual:[cut AccountType]]) )
            {
                index=i;
                PaymentCUT *cut2 = [trdRules.paymentCfgList objectAtIndex:index];
                return [[NSMutableArray alloc]initWithArray:[cut2 paymentOptions]];
            }
        }
    }
    return [[NSMutableArray alloc]initWithArray:trdRules.paymentList];
}

//MDInfo
+ (MDInfo *)mdInfoFrom:(NSArray *)mdInfoArr byExchange:(NSString *)exchange{
    for (MDInfo *mdInfo in mdInfoArr) {
        if ([mdInfo.exchange isEqualToString:exchange]) {
            return mdInfo;
        }
    }
    return nil;
}

+ (ExchangeData *)curExchangeDataFrom:(NSArray *)userExList byExchange:(NSString *)exchangeName{
    for (ExchangeData *mdInfo in userExList) {
        if ([mdInfo.feed_exchg_code isEqualToString:exchangeName]) {
            return mdInfo;
        }
    }
    return nil;
}

#pragma mark - Devie Name
+ (NSString*) deviceName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

+ (BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    if (!carrier.isoCountryCode) {
        // NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    
    return YES;
}

+ (NSString *)getCarriedName{
    if ([Utils hasCellularCoverage]) {
        CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
        CTCarrier *carrier = [netinfo subscriberCellularProvider];
        return [carrier carrierName];
    }else{
        return @"NoCarrierName";
    }
}


+ (NSArray *)getTradeObjectOrderType:(NSString *)orderType
                             trigger:(NSString *)trigger
                        triggerPrice:(NSString *)triggerPrice
                           direction:(NSString *)direction
                           disclosed:(NSString *)disclosed
                            quantity:(NSString *)quantity
                         minquantity:(NSString *)minquantity
                               price:(NSString *)price
                   statementCurrency:(NSString *)statementCurrency
                            validity:(NSString *)validity
                         paymentType:(NSString *)paymentType
                           shortSell:(NSString *)shortSell
                    skipConfirmation:(NSString *)skipConfirmation
                               gross:(NSString *)gross
{
    return @[
             @{@"control_type" : @(CONTROL_ORDERTYPE),
               @"value" : orderType
               },
             @{@"control_type" : @(CONTROL_TRIGGER),
               @"value" : trigger
               },
             @{@"control_type" : @(CONTROL_TRIGGERPRICE),
               @"value" : triggerPrice
               },
             @{@"control_type" : @(CONTROL_DIRECTION),
               @"value" : direction
               },
             @{@"control_type" : @(CONTROL_DISCLOSED),
               @"value" : disclosed
               },
             @{@"control_type" : @(CONTROL_QUANTITY),
               @"value" : quantity
               },
             @{@"control_type" : @(CONTROL_MINQUANTITY),
               @"value" : minquantity
               },
             @{@"control_type" : @(CONTROL_PRICE),
               @"value" : [price componentsSeparatedByString:@" "].lastObject
               },
             @{@"control_type" : @(CONTROL_STATEMENTCURRENCY),
               @"value" : statementCurrency
               },
             @{@"control_type" : @(CONTROL_VALIDITY),
               @"value" : validity
               },
             @{@"control_type" : @(CONTROL_PAYMENTTYPE),
               @"value" : paymentType
               },
             @{@"control_type" : @(CONTROL_SHORTSELL),
               @"value" : shortSell
               },
             @{@"control_type" : @(CONTROL_SKIPCONFIRMATION),
               @"value" : skipConfirmation
               },
             @{@"control_type" : @(CONTROL_GROSS),
               @"value" : gross
               }
             ];
}

+ (NSArray *)getDefaultTradeObject{
    
    NSString *customQuantity = @"";
    //    if([SettingManager shareInstance].currentQuantityType == 1){
    customQuantity = [SettingManager shareInstance].customQuantity;
    //    }
    return [Utils getTradeObjectOrderType:@"" trigger:@"" triggerPrice:@"" direction:@"" disclosed:@"" quantity:customQuantity minquantity:@"" price:@"" statementCurrency:@"" validity:@"" paymentType:@"" shortSell:@"" skipConfirmation:@"" gross:@""];
}

+ (NSMutableArray *)getTimeSalesFrom:(NSArray *)dictArr{
    NSMutableArray *models = @[].mutableCopy;
    [dictArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        TimeSaleModel *model = [TimeSaleModel new];
        //Parse properties
        NSDictionary *dict = (NSDictionary *)obj;
        model.time = dict[@"0"];
        model.PI = dict[@"1"];
        model.incomingPrice = [NSNumber numberWithFloat:[dict[@"2"] floatValue]];
        model.vol = [NSNumber numberWithLongLong:[dict[@"3"] longLongValue]];
        NSDate *date = [NSDate dateFromString:model.time format:kSERVER_FORMAT_TIME];
        if (date) {
            //if date is validate -> allow add data
            [models addObject:model];
        }
    }];
    return models;
}

+ (NSString *)stripExchgCodeFromStockCode:(NSString *)stock_code {
    NSRange dotRange = [stock_code rangeOfString:@"." options:NSBackwardsSearch];
    if (dotRange.location != NSNotFound) {
        return [stock_code substringFromIndex:dotRange.location+1];
    }
    
    return nil;
}
+ (NSString *)stripSymbolFromStockCode:(NSString *)stock_code {
    NSRange dotRange = [stock_code rangeOfString:@"." options:NSBackwardsSearch];
    if (dotRange.location != NSNotFound) {
        return [stock_code substringToIndex:dotRange.location];
    }
    
    return stock_code;
}
#pragma mark - TITLE PAGES
+ (NSString *)textTitleFromPage:(DefaultPage)dfPage{
    NSString *title = @"";
    switch (dfPage) {
        case DefaultPage_Dashboard:
            title = [LanguageManager stringForKey:@"Dashboard"];
            break;
        case DefaultPage_News:
            title = [LanguageManager stringForKey:@"News"];
            break;
        case DefaultPage_Portfolios:
            title = [LanguageManager stringForKey:@"Portfolio"];
            break;
        case DefaultPage_Quotes:
            title = [LanguageManager stringForKey:@"Quote Screen"];
            break;
        case DefaultPage_Watchlists:
            title = [LanguageManager stringForKey:@"Watchlist"];
            break;
        case DefaultPage_Markets:
            title = [LanguageManager stringForKey:@"Indices"];
            break;
        default:
            break;
    }
    return title;
}

#pragma mark - Default Selected Tabbar Item
+ (TabbarItemView)shouldSelectedTabbarItemFrom:(DefaultPage)dfPage{
    TabbarItemView item = TabbarItemView_Unknow;
    switch (dfPage) {
        case DefaultPage_Dashboard:
            item = TabbarItemView_Home;
            break;
        case DefaultPage_Portfolios:
            //item = TabbarItemView_Trade;
            break;
        case DefaultPage_Quotes:
            item = TabbarItemView_Quote;
            break;
        case DefaultPage_Watchlists:
            //item = TabbarItemView_Watchlist;
            break;
        case DefaultPage_Markets:
            //item = TabbarItemView_OrderBook;
            break;
        default:
            break;
    }
    return item;
}

+ (NSString *)appendDtoExchangeCode:(NSString *)wli_exchg_code {
    
    BOOL exist = NO;
    for (NSString *exchg in [self getExchangeList]) {
        if ([exchg isEqualToString:wli_exchg_code]) {
            exist = YES;
        }
    }
    if (!exist) {
        wli_exchg_code = [wli_exchg_code stringByAppendingString:@"D"];
    }
    
    return wli_exchg_code;
}

+ (NSArray *)dataBDoneModelsFrom:(NSArray *)businessDictArr{
    NSMutableArray *models = @[].mutableCopy;
    [businessDictArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BusinessDoneModel *model = [BusinessDoneModel new];
        //Set data
        NSDictionary *busDict = (NSDictionary *)obj;
        model.sVol = [NSNumber numberWithLongLong:[busDict[@"1"] longLongValue]];
        model.bVol = [NSNumber numberWithLongLong:[busDict[@"2"] longLongValue]];
        model.tVol = [NSNumber numberWithLongLong:[busDict[@"3"] longLongValue]];
        model.tVal = [NSNumber numberWithLongLong:[busDict[@"4"] longLongValue]];
        model.price = [NSNumber numberWithFloat:[busDict[@"0"] floatValue]];
        model.buyPercent = model.bVol.floatValue / (model.bVol.floatValue + model.sVol.floatValue);
        [models addObject:model];
    }];
    return [models copy];
}

@end
