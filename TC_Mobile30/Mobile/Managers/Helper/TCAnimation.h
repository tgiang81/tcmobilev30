//
//  TCAnimation.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface TCAnimation : NSObject
@property (strong, nonatomic) UIView *animationView;
@property (strong, nonatomic) UIView *parent;
- (void)showChartDetail:(UIView *)animationView;
- (void)showChartDetail:(UIView *)animationView isCell:(BOOL)isCell;

@end
