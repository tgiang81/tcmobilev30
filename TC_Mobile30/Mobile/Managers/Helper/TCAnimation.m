//
//  TCAnimation.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCAnimation.h"

@implementation TCAnimation
{
    CGRect originalFrame;
    CGRect parentOriginalFrame;
    BOOL isShowing;
}
- (void)showChartDetail:(UIView *)animationView isCell:(BOOL)isCell{
    if(isShowing == NO){
        self.parent = [animationView superview];
        parentOriginalFrame = self.parent.layer.frame;
        originalFrame = [[animationView superview] convertRect:animationView.frame toView:[UIScreen mainScreen].focusedView];
    }
    
    CGRect rect = [UIScreen mainScreen].bounds;
    if (isShowing == NO) {
        isShowing = YES;
        [self animateView:animationView toFrame:CGRectMake(0, 20, rect.size.width, rect.size.height - 20) isCell:isCell completion:^{
            
        }];
        
    }else{
        
        CGRect rect = originalFrame;
        isShowing = NO;
        [self animateView:_animationView toFrame:rect isCell:isCell completion:^{
            self.animationView.frame = self.animationView.bounds;
            [self.parent addSubview:self.animationView];
            self.animationView = nil;
        }];
    }
}

- (void)showChartDetail:(UIView *)animationView{
    
    [self showChartDetail:animationView isCell:NO];
}
- (UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
- (void)animateView:(UIView *)view toFrame:(CGRect)frame isCell:(BOOL)isCell completion:(void (^)(void))completion
{
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[self imageWithView:view]];
    imgView.frame = view.frame;
    CGFloat start = 0;
    CGFloat end = 0;
    CGFloat duration = 0.5;
    CGFloat alpha = 0.7;
    [view setAlpha:0];
    
    
    if(isShowing == YES){
        start = 0;
        end = M_PI_2;
    }else{
        start = M_PI_2;
        end = 0;
    }
    if(isCell == YES){
        if (isShowing == YES) {
            self.parent.layer.frame = frame;
            [self.parent layoutIfNeeded];
        }else{
            self.parent.layer.frame = parentOriginalFrame;
            [self.parent layoutIfNeeded];
        }
    }
    if(_animationView == nil){
        [currentWindow addSubview:view];
        _animationView = view;
    }
    [currentWindow addSubview:imgView];
    view.transform = CGAffineTransformMakeRotation(end);
    view.layer.frame = frame;
    imgView.alpha = alpha;
    [self animationBasic:imgView view:view isShowing:isShowing start:start end:end frame:frame duration:duration completion:^{
        [UIView animateWithDuration:0.3 animations:^{
            imgView.layer.frame = frame;
        } completion:^(BOOL finished) {
            view.layer.frame = frame;
            [view setAlpha:1];
            [imgView removeFromSuperview];
            if(completion){
                completion();
            }
        }];
    }];
}
- (void)animationBasic:(UIImageView *)imgView view:(UIView *)view isShowing:(BOOL)isShowing start:(CGFloat)start end:(CGFloat)end frame:(CGRect)frame duration:(CGFloat)duration completion:(void (^)(void))completion
{
    [CATransaction begin];
    CABasicAnimation *animation = [CABasicAnimation   animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = duration;
    animation.additive = YES;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    animation.fromValue = [NSNumber numberWithFloat:start];
    animation.toValue = [NSNumber numberWithFloat:end];
    
    [CATransaction setCompletionBlock:^{
        if(isShowing == YES){
            imgView.transform = CGAffineTransformMakeRotation(end);
        }
        if(completion){
            completion();
        }
    }];
    [imgView.layer addAnimation:animation forKey:@"rotation"];
    [CATransaction commit];
}
@end
