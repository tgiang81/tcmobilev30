//
//  TradeUtils.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/13/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
@class OrderDetails;
typedef enum
{
    AccountSubInfo_TradingLimit,
    AccountSubInfo_InvPowerLimit,
    AccountSubInfo_BuyLimit,
    AccountSubInfo_SellLimit
}AccountSubInfo;
NS_ASSUME_NONNULL_BEGIN
@class UserAccountClientData;
@interface TradeUtils : NSObject
+ (NSString *)getCurrencyRateOf:(NSString *)currency forAction:(int)action;
+ (NSArray *)getHeaderAndValueWith:(AccountSubInfo)type uacd:(UserAccountClientData *)uacd;
+ (NSArray *)getTradeLimitString:(UserAccountClientData *)uacd orderDetail:(OrderDetails *)orderDetails stkExchange:(NSString *)stkExchange action:(int)action currencyPlus:(NSString *)currencyPlus;
@end

NS_ASSUME_NONNULL_END
