//
//  DeviceUtils.h
//  SidebarDemo
//
//  Created by Kaka on 1/12/18.
//  Copyright © 2018 N2N Connect Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceUtils : NSObject{
	
}
+ (DeviceUtils *)shareInstance;

//Main
+ (NSString *) getDeviceSystemVersion;
+ (NSString *) getDeviceModel;
+ (NSString *) getNetworkType;
+ (NSString *) getCarrierName;
+ (NSString *) getBundleVersion;
+ (NSString *) getDeviceName;
+ (BOOL)isIphoneX;
+ (BOOL)isIpad;


+ (BOOL)isFaceIdSupported;
+ (BOOL)isTouchIdSupported;
+ (NSInteger)getLABiometryType;

//+ (void)detectDeviceSupportTouchID:(void(^)(BOOL isSupported))completion;
+ (void)isAvailableToucOrFaceID:(void(^)(BOOL isAvailable))completion;


@end
