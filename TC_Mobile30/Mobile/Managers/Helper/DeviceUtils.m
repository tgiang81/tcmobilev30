//
//  DeviceUtils.m
//  SidebarDemo
//
//  Created by Kaka on 1/12/18.
//  Copyright © 2018 N2N Connect Bhd. All rights reserved.
//

#import "DeviceUtils.h"
#import "AllCommon.h"
#import <sys/utsname.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@implementation DeviceUtils
+ (DeviceUtils *)shareInstance{
	static DeviceUtils *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[DeviceUtils alloc] init];
	});
	return _instance;
}

#pragma mark - Main
+ (NSString *) getDeviceSystemVersion {
	if ([[UIDevice currentDevice] systemVersion] != nil) {
		return [[UIDevice currentDevice] systemVersion];
	}
	return @"";
}

+ (NSString *) getDeviceModel {
	struct utsname systemInfo;
	uname(&systemInfo);
	
	//NSString *iOSDeviceModelsPath = [[NSBundle mainBundle] pathForResource:@"iOSDeviceModelMapping" ofType:@"plist"];
	//NSDictionary *iOSDevices = [NSDictionary dictionaryWithContentsOfFile:iOSDeviceModelsPath];
	
	NSString* deviceModel = [NSString stringWithCString:systemInfo.machine
											   encoding:NSUTF8StringEncoding];
	
	//return [iOSDevices valueForKey:deviceModel];
	
	if (deviceModel != nil) {
		return deviceModel;
	}
	
	return @"";
}

+ (NSString *) getNetworkType {
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *subviews = nil;
	NSString *deviceName = [[self class] getDeviceName];
	if ([deviceName isEqualToString:@"iPhone X"]) {
		subviews = [[[[app valueForKey:@"statusBar"] valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
	} else {
		subviews = [[[app valueForKey:@"statusBar"] valueForKey:@"foregroundView"] subviews];
	}
	NSNumber *dataNetworkItemView = nil;
	
	for (id subview in subviews) {
		if([subview isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
			dataNetworkItemView = subview;
			break;
		}
	}
	
	if (dataNetworkItemView != nil) {
		switch ([[dataNetworkItemView valueForKey:@"dataNetworkType"] integerValue]) {
			case 0:
				//DLog(@"Network Type = No wifi or cellular");
				return @"No wifi or cellular";
				break;
				
			case 1:
				//DLog(@"Network Type = 2G");
				return @"2G";
				break;
				
			case 2:
				//DLog(@"Network Type = 3G");
				return @"3G";
				break;
				
			case 3:
				//DLog(@"Network Type = 4G");
				return @"4G";
				break;
				
			case 4:
				//DLog(@"Network Type = LTE");
				return @"LTE";
				break;
				
			case 5:
				//DLog(@"Network Type = Wifi");
				return @"Wifi";
				break;
			default:
				break;
		}
	}
	
	return @"";
}

+ (NSString *) getCarrierName {
	CTTelephonyNetworkInfo *phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
	CTCarrier *phoneCarrier = [phoneInfo subscriberCellularProvider];
	
	if ([phoneCarrier carrierName] != nil) {
		//Scott 14 Nov 2014 : Solve Chinese Carrier error by adding UTF8encoding
		return [(NSString *)[phoneCarrier carrierName] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
	}
	
	return @"";
}

+ (NSString *) getBundleVersion {
	
	if ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] != nil) {
		return (NSString *)[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
	}
	
	return @"";
}

+ (NSString*) getDeviceName
{
	struct utsname systemInfo;
	
	uname(&systemInfo);
	
	NSString* code = [NSString stringWithCString:systemInfo.machine
										encoding:NSUTF8StringEncoding];
	
	NSDictionary* deviceNamesByCode = @{@"i386"      : @"Simulator",
										@"x86_64"    : @"Simulator",
										@"iPod1,1"   : @"iPod Touch",        // (Original)
										@"iPod2,1"   : @"iPod Touch",        // (Second Generation)
										@"iPod3,1"   : @"iPod Touch",        // (Third Generation)
										@"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
										@"iPod7,1"   : @"iPod Touch",        // (6th Generation)
										@"iPhone1,1" : @"iPhone",            // (Original)
										@"iPhone1,2" : @"iPhone",            // (3G)
										@"iPhone2,1" : @"iPhone",            // (3GS)
										@"iPad1,1"   : @"iPad",              // (Original)
										@"iPad2,1"   : @"iPad 2",            //
										@"iPad3,1"   : @"iPad",              // (3rd Generation)
										@"iPhone3,1" : @"iPhone 4",          // (GSM)
										@"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
										@"iPhone4,1" : @"iPhone 4S",         //
										@"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
										@"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
										@"iPad3,4"   : @"iPad",              // (4th Generation)
										@"iPad2,5"   : @"iPad Mini",         // (Original)
										@"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
										@"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
										@"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
										@"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
										@"iPhone7,1" : @"iPhone 6 Plus",     //
										@"iPhone7,2" : @"iPhone 6",          //
										@"iPhone8,1" : @"iPhone 6S",         //
										@"iPhone8,2" : @"iPhone 6S Plus",    //
										@"iPhone8,4" : @"iPhone SE",         //
										@"iPhone9,1" : @"iPhone 7",          //
										@"iPhone9,3" : @"iPhone 7",          //
										@"iPhone9,2" : @"iPhone 7 Plus",     //
										@"iPhone9,4" : @"iPhone 7 Plus",     //
										@"iPhone10,1": @"iPhone 8",          // CDMA
										@"iPhone10,4": @"iPhone 8",          // GSM
										@"iPhone10,2": @"iPhone 8 Plus",     // CDMA
										@"iPhone10,5": @"iPhone 8 Plus",     // GSM
										@"iPhone10,3": @"iPhone X",          // CDMA
										@"iPhone10,6": @"iPhone X",          // GSM
										
										@"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
										@"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
										@"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
										@"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
										@"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
										@"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
										@"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
										@"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
										@"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
										};
	
	
	NSString* deviceName = [deviceNamesByCode objectForKey:code];
	
	if (!deviceName) {
		// Not found on database. At least guess main device type from string contents:
		
		if ([code rangeOfString:@"iPod"].location != NSNotFound) {
			deviceName = @"iPod Touch";
		}
		else if([code rangeOfString:@"iPad"].location != NSNotFound) {
			deviceName = @"iPad";
		}
		else if([code rangeOfString:@"iPhone"].location != NSNotFound){
			deviceName = @"iPhone";
		}
		else {
			deviceName = @"Unknown";
		}
	}
	
	return deviceName;
}

+ (BOOL)isIphoneX{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
		CGSize screenSize = [[UIScreen mainScreen] bounds].size;
		if (screenSize.height == 812.0f){
			DLog(@"iPhone X");
			return YES;
		}
	}
	return NO;
}

#pragma mark - Support
+ (BOOL)isFaceIdSupported{
	if (SYSTEM_VERSION_LESS_THAN(@"11")) {
		if (IS_IPHONE_X || IS_IPHONE_XS || IS_IPHONE_X_MAX) {
			return YES;
		}
		return NO;
	}
	
	if (@available(iOS 11.0, *)) {
		LAContext *context = [[LAContext alloc] init];
		if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]){
			return ( context.biometryType == LABiometryTypeFaceID);
		}
	}
	return NO;
}

+ (BOOL) isTouchIdSupported {
	if (![LAContext class]) return NO;
	LAContext *myContext = [[LAContext alloc] init];
	NSError *authError = nil;
	if (![myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
		NSLog(@"%@", [authError localizedDescription]);
		return NO;
	}
	return YES;
}
#pragma mark - Available

//+ (BOOL)isTouchIDAvailable {
//	if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
//		return [[[LAContext alloc] init] canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil];
//	}
//	return NO;
//}

+ (BOOL) isFaceIDAvailable {
	if (![LAContext class]) return NO;
	LAContext *myContext = [[LAContext alloc] init];
	NSError *authError = nil;
	if (![myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
		NSLog(@"%@", [authError localizedDescription]);
		return NO;
	}
	
	if (@available(iOS 11.0, *)) {
		if (myContext.biometryType == LABiometryTypeFaceID){
			return YES;
		} else {
			return NO;
		}
	} else {
		return NO;
	}
}

+ (BOOL) isTouchIDAvailable {
	if (![LAContext class]) return NO;
	LAContext *myContext = [[LAContext alloc] init];
	NSError *authError = nil;
	if (![myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
		NSLog(@"%@", [authError localizedDescription]);
		return NO;
		// if (authError.code == LAErrorTouchIDNotAvailable) {}
	}
	
	if (@available(iOS 11.0, *)) {
		if (myContext.biometryType == LABiometryTypeTouchID){
			return YES;
		} else {
			return NO;
		}
	} else {
		return YES;
	}
}
#pragma mark - Check Availebl TouchOrFaceID
+ (void)detectDeviceSupportTouchID:(void(^)(BOOL isSupported))completion{
	LAContext *myContext = [[LAContext alloc] init];
	NSError *authError = nil;
	if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
		completion(YES);
	}else{
		switch (authError.code) {
			case kLAErrorTouchIDNotAvailable:
				dispatch_async(dispatch_get_main_queue(), ^{
					completion(NO);
					return ;
				});
				break;
		}
		completion(YES);
	}
}

+ (void)isAvailableToucOrFaceID:(void(^)(BOOL isAvailable))completion{
	if ([self isFaceIDAvailable]) {
		completion(YES);
		return;
	}
	if ([self isTouchIDAvailable]) {
		completion(YES);
		return;
	}
	completion(NO);
}

#pragma mark - Device
+ (BOOL)isIpad{
	if ( [(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] ) {
		return YES; /* Device is iPad */
	}
	return NO;
}

+ (NSInteger)getLABiometryType {
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&error]) {
        // get BiometryType
        if (@available(iOS 11.0, *)) {
            if (context.biometryType == LABiometryTypeTouchID) {
                return TOUCHID_AVAILABLE;
            }
            else if (context.biometryType == LABiometryTypeFaceID) {
                return FACEID_AVAILABLE;
            }
            else {
                return NO_TOUCHID_FACEID_AVAILABLE;
            }
        } else {
            // Fallback on earlier versions
            return NO_TOUCHID_FACEID_AVAILABLE;
        }
    }
    else {
        return NO_TOUCHID_FACEID_AVAILABLE;
    }
}

@end

