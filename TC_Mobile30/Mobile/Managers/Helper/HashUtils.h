//
//  HashUtils.h
//  TCiPad
//
//  Created by Kaka on 3/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface HashUtils : NSObject{
	
}
//Singleton
+ (HashUtils *)shareInstance;

#pragma mark - Generate HASH KEY
+ (NSString *)generateAESKey;
//TimeStampURLEncoded
+ (NSString *) generateTimeStampURLEncoded;
+ (NSString *)generateTimeStamp;

#pragma mark - Get IPAddress
+ (NSString *)getIPAddress;

@end
