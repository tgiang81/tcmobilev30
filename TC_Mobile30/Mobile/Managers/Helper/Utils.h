//
//  Utils.h
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AllCommon.h"
@class BrokerModel;
@class DetailBrokerModel;
@class StockModel;
@class NewsModel;
@class SectorInfo;
@class DGActivityIndicatorView;
@class ExchangeData;
@class TradingRules;
@class MDInfo;

typedef enum
{
    CONTROL_ORDERTYPE,
    CONTROL_TRIGGER,
    CONTROL_TRIGGERPRICE,
    CONTROL_DIRECTION,
    CONTROL_DISCLOSED,
    CONTROL_QUANTITY,
    CONTROL_MINQUANTITY,
    CONTROL_PRICE,
    CONTROL_STATEMENTCURRENCY,
    CONTROL_VALIDITY,
    CONTROL_PAYMENTTYPE,
    CONTROL_SHORTSELL,
    CONTROL_SKIPCONFIRMATION,
    CONTROL_GROSS
}CONTROL_INPUTFIELD_TYPE;
@interface Utils : NSObject{
	
}
//Singleton
+ (Utils *)shareInstance;

#pragma mark - Util String
//Trim leading and trailing white space
+ (NSString *)stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:(NSString *)string;
#pragma mark - Class Function return String
+ (NSString *)commonMenuTitleBy:(MenuContentType)type;
+ (NSString *)menuTitleBy:(MenuType)type;
+ (NSString *)languageByType:(LanguageType)type;
+ (NSString *)getShortExChangeName:(ExchangeData *)ex;
#pragma mark - Broker Utils
+ (BOOL)isMultiInBroker:(BrokerModel *)broker;

#pragma mark - HUD
//SVProgressHUD
+ (void)showSVProgressHUD:(NSString *)message;
+ (void)dismissSVProgressHUD;

//Window Activity
+ (void)showApplicationHUD;
+ (void)dismissApplicationHUD;

//STATE LOGIN
+ (NSString *)statusLoginFrom:(LoginStages)state;
//STATE LOGINMobile version
+ (NSString *)statusLoginForMobileVersion:(LoginStages)state;

#pragma mark - Selected Broker
+ (void)selectedBroker:(DetailBrokerModel *)detailBroker;
#pragma mark - HUD
//- (void)showHUD;
//- (void)showHUDWithStatus:(NSString *)status;
//- (void)showSuccessWithStatus:(NSString *)successStatus;
//- (void)showErrorWithStatus:(NSString *)errorStatus;

//Show Window HUD
- (void)showWindowHudWithTitle:(NSString *)title;
- (void)updateProgressForWindowHUD:(NSString *)progressString;
- (void)dismissWindowHUD;


//For Private Activity
- (DGActivityIndicatorView *)createPrivateActivity;
- (void)showActivity:(DGActivityIndicatorView *)activity inView:(UIView *)view;
- (void)dismissActivity:(DGActivityIndicatorView *)activity;

#pragma mark - Version
+ (NSString *)currentVersionApp;

#pragma mark - Array Util / FILTER STOCK
+ (NSMutableArray *)rankedByNameArr;
+ (NSMutableArray *)rankedByKeyArr;

#pragma mark - RawValue ChartType
+ (NSInteger)rawValueFrom:(ChartType)type;

#pragma mark - Index of Object from Array
+ (NSInteger)indexOfStockByCode:(NSString *)stCode fromArray:(NSArray *)stocks;
#pragma mark - StockDetail from QCFeed by Code
+ (StockModel *)stockQCFeedDataFromCode:(NSString *)stockCode;
#pragma mark - Parse News to model
+ (NSArray<NewsModel *> *)parseNewsArrFrom:(NSArray *)newsArr byType:(NewsType)type;
#pragma mark - Load Sector For Mobile Version
+ (NSArray <SectorInfo *> *)loadInfoSectors;
+ (NSArray <SectorInfo *> *)loadInfoSectorsExcludeSectorAll:(BOOL)isExclude;
+ (SectorInfo*)getSectorInfoObjFor:(NSString*)sectName;

#pragma mark - List Exchange Name
+ (NSArray *)getNamesOfExchange:(NSArray *)exchanges;
+ (NSArray *)getListExchangeName;
+ (NSInteger)getIndexOfCurrentExchange;
+ (NSInteger)inexOfCurExchangeData:(ExchangeData *)curExData;
#pragma mark - Selected New Exchange
+ (void)selectedNewExchange:(ExchangeData *)newEx;
+ (ExchangeData *)getExchange:(NSString *)code;


+ (MDInfo *)mdInfoFrom:(NSArray *)mdInfoArr byExchange:(NSString *)exchange;
+ (ExchangeData *)curExchangeDataFrom:(NSArray *)userExList byExchange:(NSString *)exchangeName;

#pragma mark - Check Account is Existed
+ (BOOL)isExistedAccountLogined;

//
+ (NSMutableArray *)getOrderTypeList;
+ (NSMutableArray *)getValidityList;
+ (NSMutableArray *)getExchangeList;
+ (NSMutableArray *)getPaymentList;
+ (NSString*) deviceName;
+ (BOOL)hasCellularCoverage;
+ (NSString *)getCarriedName;
+ (NSArray *)getDefaultTradeObject;
+ (NSArray *)getTradeObjectOrderType:(NSString *)orderType
                             trigger:(NSString *)trigger
                        triggerPrice:(NSString *)triggerPrice
                           direction:(NSString *)direction
                           disclosed:(NSString *)disclosed
                            quantity:(NSString *)quantity
                         minquantity:(NSString *)minquantity
                               price:(NSString *)price
                   statementCurrency:(NSString *)statementCurrency
                            validity:(NSString *)validity
                         paymentType:(NSString *)paymentType
                           shortSell:(NSString *)shortSell
                    skipConfirmation:(NSString *)skipConfirmation
                               gross:(NSString *)gross;
+ (NSMutableArray *)getTimeSalesFrom:(NSArray *)dictArr;
+ (NSString *)stripExchgCodeFromStockCode:(NSString *)stock_code;
+ (NSString *)stripSymbolFromStockCode:(NSString *)stock_code;
+ (NSString *)appendDtoExchangeCode:(NSString *)wli_exchg_code;
#pragma mark - TITLE PAGES
+ (NSString *)textTitleFromPage:(DefaultPage)dfPage;
#pragma mark - Default Selected Tabbar Item
+ (TabbarItemView)shouldSelectedTabbarItemFrom:(DefaultPage)dfPage;
+ (NSArray *)dataBDoneModelsFrom:(NSArray *)businessDictArr;
@end
