//
//  ApplicationUtils.m
//  TC_Mobile30
//
//  Created by Kaka on 4/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "ApplicationUtils.h"
#import "AppMacro.h"
@implementation ApplicationUtils
#pragma mark - OPEN URL WEB LINK
+ (void)open:(NSString *)url{
	if (@available(iOS 10.0, *)) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:^(BOOL success) {
			
		}];
	}else{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
	}
}
@end
