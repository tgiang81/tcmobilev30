//
//  StockAPI.h
//  TCiPad
//
//  Created by Kaka on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"

@interface StockAPI : NSObject
//Singleton
+ (StockAPI *)shareInstance;
@end
