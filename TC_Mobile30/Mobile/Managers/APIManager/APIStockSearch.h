//
//  APIStockSearch.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/4/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AFBaseRequest.h"
@class ExchangeData;
@interface APIStockSearch : AFBaseRequest
@property(assign, nonatomic) BOOL isJavaQC;
- (void) doStockSearchRequest:(NSString *)searchStr forExchange:(ExchangeData *)ed isDerivative:(BOOL)isDer;
@end
