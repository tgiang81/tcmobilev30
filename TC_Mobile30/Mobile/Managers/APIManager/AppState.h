//
//  AppState.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/9/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef enum: NSUInteger {
    AppStateType_Loading,
    AppStateType_Done
} AppStateType;
@interface AppState : NSObject
@property(assign, nonatomic) AppStateType state;

+ (instancetype)sharedInstance;
- (void)forceRemoveLoadingView;
- (void)forceRemoveLoadingView:(UIView *)view;
- (void)addLoadingView:(UIView *)view;
- (void)addNetworkIndicator;
- (void)removeLoadingView:(UIView *)view;
- (void)removeNetworkIndicator;
@end
