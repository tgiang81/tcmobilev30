//
//  ChangePasswordAPI.m
//  TC_Mobile30
//
//  Created by Michael Seven on 2/18/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ChangePasswordAPI.h"
#import "UserPrefConstants.h"
#import "UserAccountClientData.h"
#import "BrokerManager.h"
#import "DetailBrokerModel.h"
#import "ParserUtil.h"
#import "AllCommon.h"
#import "PortfolioData.h"
#import "Utils.h"
#import "VertxConnectionManager.h"
#import "PrtfSubDtlRptData.h"
#import "ASIHttpGeneric.h"
#import "HashUtils.h"
#import "AuthenticationAPI.h"
#import "ATPAuthenticate.h"
#import "ErrorCodeConstants.h"

#define kURLRequestTimeout      30 // seconds
#define START_OF_TEXT 2
#define END_OF_TEXT 3

@interface ChangePasswordAPI (){
    ASIHttpGeneric *_asiGeneric;
    NSString *encryptionKey;
}

@end

@implementation ChangePasswordAPI
//================== Singleton ==============
+ (ChangePasswordAPI *)shareInstance{
    static ChangePasswordAPI *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[ChangePasswordAPI alloc] init];
    });
    return _instance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        //Custom data here
        _asiGeneric = [[ASIHttpGeneric alloc] init];
    }
    return  self;
}

#pragma mark - Main
- (void)doChangePassword:(NSString *) currentPassword withNewPassword:(NSString *) newPassword completion:(CompletionHandler)handler{
    encryptionKey = [AuthenticationAPI shared].userInfoEncryptionKey;
    NSString *postStr = [NSString stringWithFormat:@"'=%@|*=%@|+=%@|,=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         [_asiGeneric encryptString:[UserSession shareInstance].userName],
                         [_asiGeneric encryptString:currentPassword],
                         [_asiGeneric encryptString:newPassword]
                         ];
    
    //For Philippine Stock Exchange
    //7 do something at here
    
    NSString *userParamStr = [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"];
    NSString *atpServer = [BrokerManager shareInstance].detailBroker.ATPServer;
    NSString *urlStr;
    
    if ([UserPrefConstants singleton].AES_EncryptionEnabled) {
        
        if([UserPrefConstants singleton].HTTPSEnabled){
            
            NSString *paramData = [NSString stringWithFormat:@"10|%c%@|%@%cE",START_OF_TEXT,
                                   [_asiGeneric base64Encode:userParamStr],
                                   [_asiGeneric AESEncryptAndBase64Encode:postStr withKey:encryptionKey],
                                   END_OF_TEXT];
            
            urlStr = [NSString stringWithFormat:@"https://%@/[0]TradeChgPasswd?%@",atpServer,[[UserPrefConstants singleton] urlencodedString:paramData]];
            
        }else{
            urlStr = [NSString stringWithFormat:@"http://%@/[0]TradeChgPasswd?10|%c%@|%@%cE",
                      atpServer,
                      START_OF_TEXT,
                      [_asiGeneric base64Encode:userParamStr],
                      [_asiGeneric AESEncryptAndBase64Encode:postStr withKey:encryptionKey],
                      END_OF_TEXT
                      ];
        }
    }
    else {
        urlStr = [UserPrefConstants singleton].HTTPSEnabled?[NSString stringWithFormat:@"https://%@/[%@]TradeChgPasswd?%@",
                                                  atpServer,
                                                  userParamStr,
                                                  postStr
                                                  ]: [NSString stringWithFormat:@"http://%@/[%@]TradeChgPasswd?%@",
                                                      atpServer,
                                                      userParamStr,
                                                      postStr
                                                      ];
        DLog(@"\n\n---- TradeChgPasswd (Plain) ----\n%@\n\n", urlStr);
    }
    DLog(@"\n\n---- TradeChgPasswd (Plain) ----\n%@\n\n", urlStr);

    [CPService requestDataUsingOldVersionFromUrl:urlStr completion:^(id result, NSError *error) {
        if (error) {
            //Failure
            NSString *errorMessage = [[ChangePasswordAPI shareInstance] atpDecryptionProcessCP:[error localizedDescription]];
            if (!errorMessage || errorMessage.length ==0) {
                errorMessage = [ErrorCodeConstants ATP_TradeChgPasswd_Fail:NO];
            }
            NSDictionary *userInfo = @{
                                       NSLocalizedDescriptionKey: errorMessage
                                       };
            NSError *errorDecrypted = [NSError errorWithDomain:@"TradeChgPasswd" code:RESP_CODE_EXCEPTION_NO_INTERNET userInfo:userInfo];
            DLog(@"TradeChgPasswd Error:%@", errorMessage);
            handler(result, errorDecrypted);
        }
        else {
            NSString *contentData = (NSString *)result;
            self->security = [[ChangePasswordAPI shareInstance] atpDecryptionProcessCP:contentData];
            if (!self->security || self->security.length == 0) {
                handler(self->security, error);
                return ;
            }
            //Parse data to model
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    //Completion
                    handler(self->security, error);
                });
            });
        }
    }];
    
}

- (NSString *)atpDecryptionProcessCP:(NSString *)atpResponseStr {
    
//        if ([UserPrefConstants singleton].isEncryption) { //7_ need to check isEncryption further
    if(1) {
        NSRange range = [atpResponseStr rangeOfString:[NSString stringWithFormat:@"10|%c", START_OF_TEXT]];
        if (range.location != NSNotFound) {
            int index = range.location;
            NSString *encryptedStr = [_asiGeneric getATPEncryptedStr:[atpResponseStr substringFromIndex:index]];
            return [NSString stringWithFormat:@"%@%@", [atpResponseStr substringToIndex:index], [_asiGeneric Base64DecodeAndAESDecrypt:encryptedStr withKey:encryptionKey]];
        }
        else {
            return atpResponseStr;
        }
    }
    else {
        return atpResponseStr;
    }
}

@end
