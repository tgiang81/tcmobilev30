//
//  AssetManagementAPI.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/6/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"
#import "UserAccountClientData.h"
NS_ASSUME_NONNULL_BEGIN

@interface AssetManagementAPI : NSObject
+ (void)getAssetManagement:(UserAccountClientData *)uacd ordRouteSource:(NSString *)ordRouteSource completion:(CompletionHandler)handler;
@end

NS_ASSUME_NONNULL_END
