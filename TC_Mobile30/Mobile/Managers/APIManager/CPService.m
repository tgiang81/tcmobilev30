//
//  CPService.m
//  SidebarDemo
//
//  Created by Kaka on 11/8/17.
//  Copyright © 2017 N2N Connect Bhd. All rights reserved.
//

#import "CPService.h"
#import "AppConstants.h"
#import "UserSession.h"
//#import "ErrorModel.h"



static NSTimeInterval TIME_OUT = 60.0;

@implementation CPService
#pragma mark - Main
+ (void)sendRequestWithMethod:(MethodService)method url:(NSString *)url params:(NSDictionary *)params completionHandler:(CompletionHandler)handler{
	NSString *token = [UserSession shareInstance].token;
	NSDictionary *headers = @{ @"content-type": @"application/json",
							   @"cache-control": @"no-cache",
							   @"Authorization": token ? [NSString stringWithFormat:@"Bearer %@", token] : @""
							   };
	if (!params) {
		params = @{};
	}
	
	NSDictionary *parameters = [params copy];
	//For get Method
	if (method == MethodService_Get) {
		url = [self addQueryStringToUrlString:url withDictionary:parameters];
	}
	//Create Request
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
														   cachePolicy:NSURLRequestUseProtocolCachePolicy
													   timeoutInterval:TIME_OUT];
	[request setAllHTTPHeaderFields:headers];
	//For other method - Not GET
	if (method != MethodService_Get) {
		NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
		[request setHTTPBody:postData];
	}
	
	switch (method) {
		case MethodService_Post:{
			[request setHTTPMethod:@"POST"];
		}
			break;
		case MethodService_Put:{
			[request setHTTPMethod:@"PUT"];
		}
            break;
        case MethodService_Get:{
            [request setHTTPMethod:@"GET"];
        }
            break;
        case MethodService_Delete:{
            [request setHTTPMethod:@"DELETE"];
        }
            break;
		default:{
			[request setHTTPMethod:@"POST"];
		}
		break;
	}
	DLog(@"Request on URL: %@", url);
	NSURLSession *session = [NSURLSession sharedSession];
	NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
												completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
													if (error) {
														//Failure
														handler(nil, error);
													} else {
														NSDictionary *resultDict;
														//Parse XML
														NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
														if (content.length > 0 && [[content substringToIndex:1] isEqualToString:@"<"]) {
															//XML file
															NSData* plistData = [content dataUsingEncoding:NSUTF8StringEncoding];
															NSPropertyListFormat fmt;
															resultDict = [NSPropertyListSerialization propertyListWithData:plistData options: NSPropertyListImmutable format:&fmt error:&error];
														}else{
															//parse json file
															resultDict = [NSJSONSerialization
																							JSONObjectWithData:data
																							options:NSJSONReadingMutableContainers//kNilOptions
																							error:nil];
														}
														
														dispatch_async(dispatch_get_main_queue(), ^{
															handler(resultDict, nil);
														});
													}
												}];
	[dataTask resume];
}


#pragma mark - Get Request Data
#pragma mark - Request data from NSURL session
+ (void)requestDataFromURL:(NSString *)url completion:(CompletionHandler)handler{
	//NSURL *URL = [NSURL URLWithString:url];
	NSURL *URL = [NSURL URLWithString:[self urlEscapeString:url]];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL
														   cachePolicy:NSURLRequestUseProtocolCachePolicy
													   timeoutInterval:TIME_OUT];
	NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
	sessionConfig.timeoutIntervalForRequest = 30.0;
	//sessionConfig.timeoutIntervalForResource = 60.0;
	//[sessionConfig setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
	NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
	[[session dataTaskWithRequest:request
				completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
					if (!data || error) {
						//Failure
						dispatch_async(dispatch_get_main_queue(), ^{
							handler(data, error);
						});
					} else {
						//Success
						NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
						dispatch_async(dispatch_get_main_queue(), ^{
							handler(dataStr, nil);
						});
					}
				}] resume];
}

+ (void)requestPostData:(NSString *)dataSTR url:(NSString *)url completion:(CompletionHandler)handler{
	NSURL *URL = [NSURL URLWithString:[self urlEscapeString:url]];
	NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT];
	
	NSMutableData *postMutableData = [[NSMutableData alloc] initWithData:[dataSTR dataUsingEncoding:NSISOLatin1StringEncoding]];
	[urlRequest setHTTPMethod:@"POST"];
	[urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)postMutableData.length] forHTTPHeaderField:@"Content-Length"];
	[urlRequest setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[urlRequest setHTTPBody:postMutableData];
	urlRequest.timeoutInterval = TIME_OUT;
	
	NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
	[[session dataTaskWithRequest:urlRequest
				completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
					if (!data || error) {
						//Failure
						dispatch_async(dispatch_get_main_queue(), ^{
							handler(data, error);
						});
					} else {
						//Success
						NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
						dispatch_async(dispatch_get_main_queue(), ^{
							handler(dataStr, error);
						});
					}
				}] resume];
}

#pragma mark - Request data Using Old Version
+ (void)requestDataUsingOldVersionFromUrl:(NSString *)url completion:(CompletionHandler)handler{
	NSURL *URL = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
	NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:URL];
	urlRequest.timeoutInterval = 30.0;
	NSOperationQueue *queue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
		if (!data || error) {
			//Failure
			dispatch_async(dispatch_get_main_queue(), ^{
				handler(nil, error);
			});
		} else {
			//Success
			NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
			dispatch_async(dispatch_get_main_queue(), ^{
				handler(dataStr, error);
			});
		}
	}];
}
#pragma mark - Util VC
+ (NSString*)urlEscapeString:(NSString *)url{	
	//NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
	//NSString * encodedString = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
	//NSString *encodedString  = [url stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];
	return [url stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];
}


+ (NSString*)addQueryStringToUrlString:(NSString *)urlString withDictionary:(NSDictionary *)dictionary{
	NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:urlString];
	for (id key in dictionary) {
		NSString *keyString = [key description];
		NSString *valueString = [[dictionary objectForKey:key] description];
		
		if ([urlWithQuerystring rangeOfString:@"?"].location == NSNotFound) {
			[urlWithQuerystring appendFormat:@"?%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
		} else {
			[urlWithQuerystring appendFormat:@"&%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
		}
	}
	return urlWithQuerystring;
}
@end
