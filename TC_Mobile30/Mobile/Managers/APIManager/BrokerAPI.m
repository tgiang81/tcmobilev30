//
//  APIManager.m
//  TCiPad
//
//  Created by Kaka on 3/21/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BrokerAPI.h"
#import "AllCommon.h"
#import "BrokerModel.h"

@implementation BrokerAPI
//================== Singleton ==============
+ (BrokerAPI *)shareInstance{
	static BrokerAPI *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[BrokerAPI alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
	}
	return  self;
}

#pragma mark - Main
//Get Plis file
- (void)getInitialPlistFile:(CompletionHandler)handler{
	NSString *url = @"";
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;
    
	if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
		url = [NSString stringWithFormat:@"http://nopl-ph.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?id=%@&ver=0&device=I", bundleIdentifier];
	}else{
		
		url = [NSString stringWithFormat:@"http://nogl.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?id=%@&ver=0&device=I", bundleIdentifier];
	}
    
	[CPService sendRequestWithMethod:MethodService_Get url:url params:nil completionHandler:^(id result, NSError *error) {
		handler(result, error);
	}];

	
//	[CPService requestDataFromURL:url completion:^(id result, NSError *error) {
//		NSString *dataString = (NSString *)result;
//		NSData* plistData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
//		NSPropertyListFormat fmt;
//		NSDictionary *resultDict = [NSPropertyListSerialization propertyListWithData:plistData options: NSPropertyListImmutable format:&fmt error:&error];
//		handler(resultDict, error);
//	}];
}

//============ Open sublist brokers ==============================
- (void)openSubListBrokersFrom:(BrokerModel *)broker completion:(CompletionHandler)handler{
	NSString *url = broker.plistLink;
	[CPService sendRequestWithMethod:MethodService_Get url:url params:nil completionHandler:^(id result, NSError *error) {
		handler(result, error);
	}];
}

//=========== Get detail broker ============================================================
- (void)getDetailBroker:(BrokerModel *)broker completion:(CompletionHandler)handler{
	[self openSubListBrokersFrom:broker completion:handler];
}

@end
