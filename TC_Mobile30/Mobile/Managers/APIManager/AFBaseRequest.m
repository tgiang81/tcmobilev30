#import "AFBaseRequest.h"
#import "AFNetworking.h"
#import "AppState.h"
//Constant key
static NSString *BASE_URL = @"";
static NSTimeInterval TIME_OUT = 30;

@implementation AFBaseRequest
/*
 + (BaseAPI *)shareInstance{
 static BaseAPI *_instance = nil;
 static dispatch_once_t onceToken;
 dispatch_once(&onceToken, ^{
 _instance = [[BaseAPI alloc] init];
 });
 return _instance;
 }
 */
//@"application/json"
+ (void)getRequestWithURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler{
    NSString *apiURL = [NSString stringWithFormat:@"%@/%@", BASE_URL, URI];
    [AFBaseRequest getRequestWithInstantURI:apiURL params:params serializer:serializer completeHandler:completeHandler];
}

+ (void)getRequestWithURI:(NSString *)URI params:(NSDictionary *)params completeHandler:(CompleteHandler)completeHandler{
    NSString *apiURL = [NSString stringWithFormat:@"%@/%@", BASE_URL, URI];
    [AFBaseRequest getRequestWithInstantURI:apiURL params:params serializer:@"application/json" completeHandler:completeHandler];
}

+ (void)getRequestWithURL:(NSString *)url params:(NSDictionary *)params completeHandler:(CompleteHandler)completeHandler{
	[AFBaseRequest getRequestWithInstantURI:url params:params serializer:@"application/json" completeHandler:completeHandler];
}

+ (void)getRequestWithInstantURI:(NSString *)URI params:(NSDictionary *)params completeHandler:(CompleteHandler)completeHandler{
    [AFBaseRequest getRequestWithInstantURI:URI params:params serializer:@"application/json" completeHandler:completeHandler];
}
+ (void)getRequestWithInstantURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if([serializer isEqualToString:@"application/json"]){
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }else{
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    
    manager.requestSerializer.timeoutInterval = TIME_OUT;
    [manager.requestSerializer setValue:serializer forHTTPHeaderField:@"Content-Type"];
    [manager GET:URI parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completeHandler(YES, responseObject, nil);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        completeHandler(NO, nil, error);
    }];
}



+ (void)postRequestWithInstantURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if([serializer isEqualToString:@"application/json"]){
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }else{
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    manager.requestSerializer.timeoutInterval = TIME_OUT;
    [manager.requestSerializer setValue:serializer forHTTPHeaderField:@"Content-Type"];
    [manager POST:URI parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completeHandler(YES, responseObject, nil);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        completeHandler(NO, nil, error);
    }];
}

+ (void)postRequestWithURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler{
    NSString *apiURL = [NSString stringWithFormat:@"%@/%@", BASE_URL, URI];
    [self postRequestWithInstantURI:apiURL params:params serializer:serializer completeHandler:completeHandler];
}

#pragma mark - Request with Progress
+ (void)postRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler{
    [AFBaseRequest postRequestWithProgressFromURL:url params:params serializer:@"application/json" progress:progressHandler completeHandler:completeHandler];
}
+ (void)postRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params serializer:(NSString *)serializer progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler{
    NSString *apiURL = [NSString stringWithFormat:@"%@/%@", BASE_URL, url];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if([serializer isEqualToString:@"application/json"]){
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }else{
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    manager.requestSerializer.timeoutInterval = TIME_OUT;
    [manager.requestSerializer setValue:serializer forHTTPHeaderField:@"Content-Type"];
    
    //Call request
    [manager POST:apiURL parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        progressHandler(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completeHandler(YES, responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completeHandler(NO, nil, error);
    }];
}


+ (void)getRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler{
    [AFBaseRequest getRequestWithProgressFromURL:url params:params serializer:@"application/json" progress:progressHandler completeHandler:completeHandler];
}
+ (void)getRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params serializer:(NSString *)serializer progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler{
    NSString *apiURL = [NSString stringWithFormat:@"%@/%@", BASE_URL, url];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    if([serializer isEqualToString:@"application/json"]){
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }else{
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    manager.requestSerializer.timeoutInterval = TIME_OUT;
    [manager.requestSerializer setValue:serializer forHTTPHeaderField:@"Content-Type"];
    //Call request
    [manager GET:apiURL parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        progressHandler(downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completeHandler(YES, responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completeHandler(NO, nil, error);
    }];
}

@end
