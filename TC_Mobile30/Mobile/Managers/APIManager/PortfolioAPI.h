//
//  PortfolioAPI.h
//  TC_Mobile30
//
//  Created by Kaka on 10/24/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"
@class PrtfSubDtlRptData;
@class UserAccountClientData;


@interface PortfolioAPI : NSObject{
	
}
//Singleton
+ (PortfolioAPI *)shareInstance;

#pragma mark - Main
//AccountType: type: 0-Realised, 1-Unrealisez
//Completion: NSArray of PortfolioData
- (void)getEquityPortfolioFrom:(UserAccountClientData *)uacd forType:(NSInteger)type completion:(CompletionHandler)handler;
//FOR VIRTUAL PF
//Completion: NSArray of PrtfSubDtlRptData
- (void)getVirtualPortfolioFrom:(UserAccountClientData *)uacd isDerivative:(BOOL)isDerivative completion:(CompletionHandler)handler;

//FOR SUMMARY PF
//Completion: NSArray of PrtfSummRptData
- (void)getSummaryPortfolioFrom:(UserAccountClientData *)uacd isDerivative:(BOOL)isDerivative completion:(CompletionHandler)handler;

- (void)getDetailPrtfDtlRpt:(UserAccountClientData *)uacd isDerivative:(BOOL)isDerivative ofSubDtlRpt:(PrtfSubDtlRptData *)psdrd detail_type:(int)detail_type completion:(CompletionHandler)handler;
@end
