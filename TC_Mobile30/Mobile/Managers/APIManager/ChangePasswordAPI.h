//
//  ChangePasswordAPI.h
//  TC_Mobile30
//
//  Created by Michael Seven on 2/18/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"
@class PrtfSubDtlRptData;
@class UserAccountClientData;


@interface ChangePasswordAPI : NSObject{
    NSString *security;
    
}
//Singleton
+ (ChangePasswordAPI *)shareInstance;

#pragma mark - Main
- (void)doChangePassword:(NSString *) currentPassword withNewPassword:(NSString *) newPassword completion:(CompletionHandler)handler;
- (NSString *)atpDecryptionProcessCP:(NSString *)atpResponseStr;
@end
