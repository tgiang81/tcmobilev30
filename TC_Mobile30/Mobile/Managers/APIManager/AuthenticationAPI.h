//
//  AuthenticationAPI.h
//  TCiPad
//
//  Created by Kaka on 3/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"
#import "AllCommon.h"
@class DetailBrokerModel;
@class E2EEInfo;

#define kFAILED_KEY 		@"Failed"
typedef void (^AUProgressHandler)(LoginStages progress);

@interface AuthenticationAPI : NSObject{
	int timeout;
	BOOL isEncryption;
	BOOL isE2EE_Encryption;
	BOOL isATPCompression;
	BOOL isQCCompression;
	BOOL isJavaQC;
	BOOL connerror;
}

//========= For progress ======================
@property (strong, nonatomic) E2EEInfo *e2eeInfo;
@property (strong, nonatomic) NSString *userInfoEncryptionKey;
@property (strong, nonatomic) NSString *atpServer;

//Singleton
+ (AuthenticationAPI *)shared;
- (void)clearData;

#pragma mark - Main
//Main Login
- (void)startLoginFrom:(NSString *)userID password:(NSString *)password progress:(AUProgressHandler)progressHandler completion:(CompletionHandler)completionHandler;
//Logout
- (void)doLogout:(CompletionHandler)handler;
- (void)checkSingleLogOn;


#pragma mark - TRADING API
- (void)getExchangeList:(CompletionHandler)handler;
- (void)atpGetAccountClientList:(CompletionHandler)handler;

@end
