//
//  ChangePinAPI.h
//  TC_Mobile30
//
//  Created by Michael Seven on 3/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#ifndef ChangePinAPI_h
#define ChangePinAPI_h

#import <Foundation/Foundation.h>
#import "CPService.h"
@class PrtfSubDtlRptData;
@class UserAccountClientData;


@interface ChangePinAPI : NSObject{
    NSString *security;
    
}
//Singleton
+ (ChangePinAPI *)shareInstance;

#pragma mark - Main
- (void)doChangePin:(NSString *) currentPin withNewPin:(NSString *) newPin completion:(CompletionHandler)handler;
- (NSString *)atpDecryptionProcessCP:(NSString *)atpResponseStr;
@end


#endif /* ChangePinAPI_h */
