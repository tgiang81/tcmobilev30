//
//  AFBaseRequest.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/21/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompleteHandler)(BOOL success, id responseObject, NSError *error);
typedef void (^ProgressHandler)(NSProgress *progress);

@interface AFBaseRequest : NSObject{
    
}
//+ (BaseAPI *)shareInstance;

#pragma mark - API perform request from server
//MAIN API
+ (void)getRequestWithURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler;
+ (void)getRequestWithURI:(NSString *)URI params:(NSDictionary *)params completeHandler:(CompleteHandler)completeHandler;
+ (void)getRequestWithInstantURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler;
+ (void)getRequestWithInstantURI:(NSString *)URI params:(NSDictionary *)params completeHandler:(CompleteHandler)completeHandler;
+ (void)getRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params serializer:(NSString *)serializer progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler;
+ (void)getRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler;
//URL
+ (void)getRequestWithURL:(NSString *)url params:(NSDictionary *)params completeHandler:(CompleteHandler)completeHandler;


+ (void)postRequestWithURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler;
+ (void)postRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params serializer:(NSString *)serializer progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler;
+ (void)postRequestWithInstantURI:(NSString *)URI params:(NSDictionary *)params serializer:(NSString *)serializer completeHandler:(CompleteHandler)completeHandler;
+ (void)postRequestWithProgressFromURL:(NSString *)url params:(NSDictionary *)params progress:(ProgressHandler)progressHandler completeHandler:(CompleteHandler)completeHandler;

@end
