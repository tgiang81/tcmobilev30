//
//  AssetManagementAPI.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/6/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AssetManagementAPI.h"

#import "UserPrefConstants.h"
#import "ParserUtil.h"
#import "BrokerManager.h"
#import "DetailBrokerModel.h"
@implementation AssetManagementAPI
+ (void)getAssetManagement:(UserAccountClientData *)uacd ordRouteSource:(NSString *)ordRouteSource completion:(CompletionHandler)handler
{
    NSString *postStr = [NSString stringWithFormat:@"'=%@|+=%@|þ=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.    client account no
                         ordRouteSource
                         ];
    
    //NSString *atpServer = [BrokerManager shareInstance].detailBroker.ATPServer;
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
    NSString *userParamStr = [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]AssetManagement?%@", atpServer, userParamStr, postStr];
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]AssetManagement?%@",
                  atpServer, userParamStr, postStr];
    }
    [CPService requestDataUsingOldVersionFromUrl:urlStr completion:^(id result, NSError *error) {
        if (error) {
            //Failure
            handler(result, error);
            return ;
        }
        NSString *contentData = (NSString *)result;
        if (!contentData || contentData.length == 0) {
            handler(contentData, error);
            return ;
        }
        //Parse data to model
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *responseArr = [[ParserUtil shareInstance] parseAssetMamagement:contentData];
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(responseArr, error);
            });
        });
    }];
}
@end
