//
//  ResearchAPI.m
//  TCiPad
//
//  Created by Kaka on 6/26/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ResearchAPI.h"
#import "UserPrefConstants.h"
#import "NSDate+Utilities.h"
#import "ResearchAPI.h"

@implementation ResearchAPI
//================== Singleton ==============
+ (ResearchAPI *)shareInstance{
	static ResearchAPI *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[ResearchAPI alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
	}
	return  self;
}

#pragma mark - Main
//Using this to load Research
- (void)requestResearchFrom:(NSDate *)fromDate toDate:(NSDate *)toDate source:(NSString *)source category:(NSString *)category keyword:(NSString *)keyword target:(NSString *)target completion:(CompletionHandler)handler{
	if (![UserPrefConstants singleton].qcServer || [UserPrefConstants singleton].qcServer.length == 0) {
		[UserPrefConstants singleton].qcServer = @"118.201.69.222";
	}
	NSString *dataUrl = [NSString stringWithFormat:@"http://%@/%@V2NEWS?[KWORD]=%@&[TARGET]=%@&[GUID]=&[CAT]=%@&[SOURCE]=%@&[ACTIVE]=&[FRDT]=%@&[TODT]=%@&[SIZE]=100&[FROM]=0|||", [UserPrefConstants singleton].qcServer, [UserPrefConstants singleton].tempUserParamQC, keyword, target, category, source,[fromDate formatDateWithTimeZone], [toDate formatDateWithTimeZone]];
	
	[CPService requestDataFromURL:dataUrl completion:^(id result, NSError *error) {
		handler(result, error);
	}];
}

//Stockcode raplace keyword params
- (void)getResearReportForStock:(NSString *)stockCode fromDate:(NSDate *)fromDate toDate:(NSDate *)toDate source:(NSString *)source category:(NSString *)category keyword:(NSString *)keyword target:(NSString *)target completion:(CompletionHandler)handler{
	[self requestResearchFrom:fromDate toDate:toDate source:source category:category keyword:stockCode target:target completion:^(id result, NSError *error) {
		handler(result, error);
	}];
}


- (void) doAllElasticNews:(NSDate *)fromDate toDate:(NSDate *)toDate andSource:(NSString *)source andCategory:(NSString *)category andKeyword:(NSString *)keyword andTargetID:(NSString *)targetID completion:(CompletionHandler)handler{
	if (![UserPrefConstants singleton].qcServer || [UserPrefConstants singleton].qcServer.length == 0) {
		[UserPrefConstants singleton].qcServer = @"118.201.69.222";
	}
	NSString *dataUrl = [NSString stringWithFormat:@"http://%@/%@V2NEWS?[KWORD]=%@&[TARGET]=%@&[GUID]=&[CAT]=%@&[SOURCE]=%@&[ACTIVE]=&[FRDT]=%@&[TODT]=%@&[SIZE]=100&[FROM]=0|||", [UserPrefConstants singleton].qcServer, [UserPrefConstants singleton].tempUserParamQC,keyword,targetID,category,source,[fromDate formatDateWithTimeZone],[toDate formatDateWithTimeZone]];
	[CPService requestDataFromURL:dataUrl completion:^(id result, NSError *error) {
		handler(result, error);
	}];
//	NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded",
//							   @"cache-control": @"no-cache",
//							   };
//	NSURL *URL = [NSURL URLWithString:[CPService urlEscapeString:dataUrl]];
//	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
//	[request setHTTPMethod:@"GET"];
//	[request setAllHTTPHeaderFields:headers];
//
//	NSURLSession *session = [NSURLSession sharedSession];
//	NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//												completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//													if (error) {
//														//Failure
//														dispatch_async(dispatch_get_main_queue(), ^{
//															handler(data, error);
//														});
//													} else {
//														//Success
//														NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//														dispatch_async(dispatch_get_main_queue(), ^{
//															handler(dataStr, nil);
//														});
//													}
//												}];
//	[dataTask resume];
//	extracted(dataUrl, handler);
	
//	NSString* webName = [dataUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//
//	NSURL *url = [NSURL URLWithString:webName];
//
//	NSLog(@"url elastic News %@",url);
//	NSURLRequest *request = [NSURLRequest requestWithURL:url];
//
//	NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
//
//	sessionConfig.timeoutIntervalForRequest = 10.0;
//	sessionConfig.timeoutIntervalForResource = 60.0;
//
//	// Configure Session Configuration
//	[sessionConfig setAllowsCellularAccess:YES];
//	[sessionConfig setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
//
//	// NSURLSession *session = [NSURLSession sharedSession];
//
//	NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
//
//	NSURLSessionDataTask *downloadTask = [session
//										  dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//											  // 4: Handle response here
//											  dispatch_async(dispatch_get_main_queue(), ^{
//												  NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//												  NSLog(@"Status code: %ld",(long)[httpResponse statusCode]);
//
//												  NSLog(@"dataStr %@",data);
//												  NSString *responseData= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//
//												  NSLog(@"dataStr %@",responseData);
//												  NSLog(@"dataStr %lu",(unsigned long)responseData.length);
//
//												  if (responseData.length==0) {
//													  [self performSelector:@selector(addingNewsArray) withObject:nil afterDelay:0.5f];
//												  }
//
//												  NSString *content = [self qcDecompressionProcess:responseData];
//
//
//
//												  NSArray *lineArr = [content componentsSeparatedByString:@"\r\n"];
//
//												  //Find the index of [] and [END]
//												  int header = -1;
//												  int footer = -1;
//												  for (int j = 0; j < [lineArr count]; j++) {
//													  NSString *line = [lineArr objectAtIndex:j];
//													  if ([line isEqualToString:@"[]"]) {
//														  header = j;
//													  }
//													  if ([line isEqualToString:@"[END]"]) {
//														  footer = j;
//													  }
//													  if([line localizedCaseInsensitiveContainsString:@"[END]"]){
//														  footer = j;
//													  }
//
//												  }
//
//
//												  if (header != -1 && footer != -1) {
//													  for (int i = header+1; i < footer; i++) {
//														  NSString *dataRow = [lineArr objectAtIndex:i];
//														  NSArray *tokens = [dataRow componentsSeparatedByString:@","];
//														  if ([tokens count] >= 3) {
//															  NewsMetadata *nmd = [[NewsMetadata alloc] init];
//															  int i = 0;
//															  for (NSString *token in tokens) {
//																  switch (i) {
//																	  case 0: nmd.newsID = token;
//																		  break;
//																	  case 1: nmd.codeID = token;
//																		  break;
//																	  case 2: nmd.date1 = token;
//																		  break;
//
//																	  case 4:
//																		  //  DLog(@"NEWS Stock Token %@",token);
//																		  nmd.stockcode = token;
//																		  break;
//																	  case 5: nmd.newsTitle = [token stripHtml];
//																		  break;
//																	  case 8: nmd.sourceNews = token;
//																	  default:
//																		  break;
//																  }
//																  i++;
//															  }
//
//															  BOOL found = NO;
//															  for (NewsMetadata *current_nmd in _newsArray) {
//																  if ([current_nmd.newsID isEqualToString:nmd.newsID]) {
//																	  found = YES;
//																  }
//															  }
//															  if (!found) {
//
//																  [_newsArray addObject:nmd];
//															  }
//
//														  }
//													  }
//
//													  NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date1" ascending:NO];
//													  [_newsArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
//													  //NSLog(@"All News %@",[UserPrefConstants singleton].allNewsResultList);
//
//
//
//													  [self performSelector:@selector(addingNewsArray) withObject:nil afterDelay:1.0f];
//
//												  }
//
//											  });
//
//										  }];
//	// 3
//	[downloadTask resume];
	
}

@end
