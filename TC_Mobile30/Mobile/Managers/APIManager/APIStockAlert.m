//
//  APIStockAlert.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/21/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "APIStockAlert.h"
#import "NSString+MD5.h"
#import "UserPrefConstants.h"
#import "NSData-AES.h"
#import "Utils.h"
#import "Reachability.h"
#import "JWT.h"
#import "AESCryptAESCryptAdditions.h"
@implementation APIStockAlert
+ (void) storePushNotificationInfo:(NSString *)accountName
                           andUDID:(NSString *)deviceUDID
                        andExhange:(NSString *)exchange
                    andSponsorCode:(NSString *)sponsorCode
                         andBHCode:(NSString *)bhCode
                          andAppID:(NSString *)appID
                       andBundleId:(NSString *)bundleID
                     andClientCode:(NSString *)clientCode
                    andDeviceToken:(NSString *)deviceToken
                      andIPAddress:(NSString *)ipAddress
                 andActivationCode:(NSString *)activationCode
                   completeHandler:(CompleteHandler)completeHandler{
    
    
    
    // NSLog(@"BhCode %@",bhCode);
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:exchange forKey:@"ex"];
    [sendMessage setObject:sponsorCode forKey:@"spc"];
    [sendMessage setObject:bhCode forKey:@"bh"];
    [sendMessage setObject:accountName forKey:@"lid"];
    [sendMessage setObject:appID forKey:@"appId"];
    [sendMessage setObject:clientCode forKey:@"cc"];
    //ggtt
    //    [sendMessage setObject:bundleID forKey:@"bdlID"];
    
    //App bundle ggtt
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    [sendMessage setObject:bundleIdentifier forKey:@"bdlID"];
    [sendMessage setObject:deviceToken forKey:@"dvTkn"];
    
    //platform name
    [sendMessage setObject:@"iOS" forKey:@"pfmNm"];
    //application version
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [sendMessage setObject:appVersion forKey:@"appV"];
    
    //devicemodel
    [sendMessage setObject:[Utils deviceName] forKey:@"dvMdl"];
    
    //devicenewtype
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status == ReachableViaWiFi)
    {
        //WiFi
        [sendMessage setObject:@"Wifi" forKey:@"dvNwT"];
    }
    else if (status == ReachableViaWWAN)
    {
        //3G
        [sendMessage setObject:@"3G" forKey:@"dvNwT"];
    }
    
    //devicenewprovider
    [sendMessage setObject:[Utils getCarriedName] forKey:@"dvNwPvd"];
    
    // Device UDID
    [sendMessage setObject:[UserPrefConstants singleton].deviceUDID forKey:@"imei"];
    
    //platforminfo
    float osSystem = [[UIDevice currentDevice].systemVersion floatValue];
    [sendMessage setObject:[NSString stringWithFormat:@"OSVer/%.2f",osSystem] forKey:@"pfmInfo"];
    //platformscreensize
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    NSString *screenSize = [NSString stringWithFormat:@"%.0fx%.0f",screenWidth,screenHeight];
    [sendMessage setObject:screenSize forKey:@"pfmScSz"];
    
    NSArray *keys = [sendMessage allKeys];
    
    keys = [[keys mutableCopy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableString *result = [NSMutableString string];
    for (NSString *key in keys) {
        id value = sendMessage[key];
        if (result.length) {
            [result appendString:@","];
        }
        [result appendFormat:@"\"%@\":\"%@\"", key, [value description]];
    }
    
    NSString *md5String = [NSString stringWithFormat:@"{%@}",result];
    
    NSLog(@"MD5 String %@",result);
    
    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
    NSDate* newDate = [[NSDate date] dateByAddingTimeInterval:600];
    NSDate* oldDate = [[NSDate date] dateByAddingTimeInterval:-600];
    long distantFuture = (long)(NSTimeInterval)([newDate timeIntervalSince1970]);
    long notBeforeAt = (long)(NSTimeInterval)([oldDate timeIntervalSince1970]);
    
    NSDictionary *payload = @{@"iss" : bhCode,
                              @"aud": @"SANS",
                              @"exp": [NSNumber numberWithUnsignedInteger:distantFuture],
                              @"jti": @"",
                              @"iat": [NSNumber numberWithUnsignedInteger:currentTime],
                              @"nbf": [NSNumber numberWithUnsignedInteger:notBeforeAt],
                              @"sub": @"",
                              @"ev": md5String
                              };
    
    NSString *algorithmName = @"RS256";
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Secret_Key_Certificates" ofType:@"p12"];
    NSData *privateKeySecretData = [NSData dataWithContentsOfFile:filePath];
    
    NSString *passphraseForPrivateKey = @"secret";
    
    
    
    
    JWTBuilder *builder = [JWTBuilder encodePayload:payload].secretData(privateKeySecretData).privateKeyCertificatePassphrase(passphraseForPrivateKey).algorithmName(algorithmName);
    NSString *jwtToken = builder.encode;
    
    NSMutableDictionary *sendMessageJwt = [[NSMutableDictionary alloc]init];
    [sendMessageJwt setObject:jwtToken forKey:@"jwt"];
    [sendMessageJwt setObject:bhCode forKey:@"bh"];
    [sendMessageJwt setObject:appID forKey:@"appId"];
    
    
    NSArray *keysjwt = [sendMessageJwt allKeys];
    
    keysjwt = [[keysjwt mutableCopy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableString *resultJwt = [NSMutableString string];
    for (NSString *key in keysjwt) {
        id value = sendMessageJwt[key];
        if (resultJwt.length) {
            [resultJwt appendString:@","];
        }
        [resultJwt appendFormat:@"\"%@\":\"%@\"", key, [value description]];
    }
    
    // NSLog(@"resultJwt %@",resultJwt);
    
    NSString *evStringJWT = [NSString stringWithFormat:@"{%@}",resultJwt];

    NSString *signatureString = [AESCryptAESCryptAdditions encrypt:evStringJWT password:kEncryptKey];
    
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",[UserPrefConstants singleton].pushNotificationRegisterURL,signatureString,[signatureString MD5]];
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"urlPushStoreInfo %@",urlPushStoreInfo);
    //NSLog(@"encoded %@",encoded);
    [APIStockAlert getRequestWithInstantURI:encoded params:sendMessage serializer:@"application/x-www-form-urlencoded" completeHandler:completeHandler];
    
    
}
+ (void)getUid:(NSString *)jwtToken bhCode:(NSString *)bhCode appID:(NSString *)appID completeHandler:(CompleteHandler)completeHandler{
    NSString *signatureString = [self getSignature:jwtToken bhCode:bhCode appID:appID status:nil];
    NSString *svURL = [NSString stringWithFormat:@"%@%@%@", kDomainName, kProjectName, kNewClientPath];
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",svURL,signatureString,[signatureString MD5]];
    //    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    NSLog(@"AlertFlow:getUid %@ rawParams:jwtToken bhCode AppId",urlPushStoreInfo);
    //NSLog(@"encoded %@",encoded);
    [APIStockAlert postRequestWithInstantURI:encoded params:nil serializer:@"application/json" completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            completeHandler(NO, nil, error);
        }else{
            NSError *error2;
            if([responseObject valueForKey:@"ev"] != [NSNull null]){
                NSData *data = [AESCryptAESCryptAdditions decryptRaw:[responseObject valueForKey:@"ev"] password:kEncryptKey];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error2];
                NSArray *uids = [json valueForKey:@"result"];
                if(uids.count > 0){
                    [UserPrefConstants singleton].uid = [[uids[0] objectForKey:@"UID"] stringValue];
                    for(NSDictionary *objc in uids){
                        if([[objc objectForKey:@"BundleId"] longValue] != 1 && [[objc objectForKey:@"BundleId"] longValue] != 2){
                            [UserPrefConstants singleton].pushNotificationChanelId = [[objc objectForKey:@"BundleId"] stringValue];
                        }
                    }
                }
                completeHandler(YES, [UserPrefConstants singleton].uid, nil);
            }else{
                completeHandler(YES, nil, nil);
            }
        }
        
        
        
    }];
}

+ (NSString *)getSignature:(NSString *)jwtToken bhCode:(NSString *)bhCode appID:(NSString *)appID status:(NSString *)status{
    NSMutableDictionary *sendMessageJwt = [[NSMutableDictionary alloc]init];
    [sendMessageJwt setObject:jwtToken forKey:@"jwt"];
    [sendMessageJwt setObject:bhCode forKey:@"bh"];
    [sendMessageJwt setObject:appID forKey:@"appId"];
    if(status == nil){
        [sendMessageJwt setObject:@"0" forKey:@"s"];
    }else{
        [sendMessageJwt setObject:status forKey:@"s"];
    }
    
    
    NSString *evStringJWT = [NSString stringWithFormat:@"{%@}",[self convertMessToString:sendMessageJwt]];
    
    NSString *signatureString = [AESCryptAESCryptAdditions encrypt:evStringJWT password:kEncryptKey];
    return signatureString;
}

+ (NSMutableString *)convertMessToString:(NSMutableDictionary *)mess{
    NSArray *keysjwt = [mess allKeys];
    
    keysjwt = [[keysjwt mutableCopy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableString *resultJwt = [NSMutableString string];
    for (NSString *key in keysjwt) {
        id value = mess[key];
        if (resultJwt.length) {
            [resultJwt appendString:@","];
        }
        [resultJwt appendFormat:@"\"%@\":\"%@\"", key, [value description]];
    }
    return resultJwt;
}



+ (void)addAlert:(NSString *)jwtToken
          bhCode:(NSString *)bhCode
           appID:(NSString *)appID
            cond:(NSString *)cond
           compt:(NSString *)compt
           alrtt:(NSString *)alrtt
             mdn:(NSString *)mdn
              ex:(NSString *)ex
             rmk:(NSString *)rmk
             lmt:(NSString *)lmt
           stkCd:(NSString *)stkCd
           stkNm:(NSString *)stkNm
 completeHandler:(CompleteHandler)completeHandler{
    
//    mdn
//    Alert  
//    Email = 1
//    SMS = 2
//      64
//    Push Notification = other

//    alrtT
//    Alert Type
//    Stock Price = 98
//    Stock Volume = 101
//    Bid Price = 68
//    Ask Price = 88
//    Bid Volume = 58
//    Ask Volume = 78

//    compT
//    Compare Type
//    Absolute Value = 0
//    % Change = 1

//    Cond
//    Condition
//    “=” = 1
//    “>” = 2
//    “>=” = 3
//    “<” = 4
//    “<=” = 5
    if([UserPrefConstants singleton].pushNotificationChanelId == nil){
        return;
    }
    NSString *tmpEx = ex;
    if(tmpEx == nil){
        tmpEx = [Utils stripExchgCodeFromStockCode:stkCd];
    }
    if(tmpEx == nil){
        tmpEx = [UserPrefConstants singleton].userCurrentExchange;
    }
    if(tmpEx == nil){
        tmpEx = @"KL";
    }
    NSMutableDictionary *ev = [[NSMutableDictionary alloc]init];
    [ev setObject:tmpEx forKey:@"ex"];
    [ev setObject:rmk == nil ? @"" : rmk forKey:@"rmk"];
    [ev setObject:lmt forKey:@"lmt"];
    [ev setObject:stkCd forKey:@"stkCd"];
    [ev setObject:stkNm forKey:@"stkNm"];
    if(mdn && ![mdn isEqualToString:@"PN"]){
        [ev setObject:mdn forKey:@"mdn"];
    }else{
        [ev setObject:[UserPrefConstants singleton].pushNotificationChanelId forKey:@"mdn"];
    }
    
    [ev setObject:alrtt == nil ? @"98":alrtt forKey:@"alrtT"];
    [ev setObject:alrtt == nil ? @"0":compt forKey:@"compT"];
    [ev setObject:cond forKey:@"cond"];
    [ev setObject:appID forKey:@"appId"];
    [ev setObject:bhCode forKey:@"bh"];
    [ev setObject:jwtToken forKey:@"jwt"];
    
    NSString *evStringJWT = [NSString stringWithFormat:@"{%@}",[self convertMessToString:ev]];
    
    NSString *evString = [AESCryptAESCryptAdditions encrypt:evStringJWT password:kEncryptKey];
    
    NSString *svURL = [NSString stringWithFormat:@"%@%@%@", kDomainName, kProjectName, kAddAlertPath];
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",svURL,evString,[evString MD5]];
    
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    NSLog(@"urlPushStoreInfo %@",urlPushStoreInfo);
    //NSLog(@"encoded %@",encoded);
    [APIStockAlert postRequestWithInstantURI:encoded params:nil serializer:@"application/json" completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            completeHandler(NO, nil, error);
        }else{
            NSError *error2;
            if([responseObject valueForKey:@"ev"] != [NSNull null]){
                NSData *data = [AESCryptAESCryptAdditions decryptRaw:[responseObject valueForKey:@"ev"] password:kEncryptKey];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error2];
                completeHandler(YES, json, nil);
            }else{
                completeHandler(YES, nil, nil);
            }
        }
        
        
        
    }];
    
}

+ (void)updateAlert:(NSString *)jwtToken
             bhCode:(NSString *)bhCode
              appID:(NSString *)appID
                  s:(NSString *)s
               cond:(NSString *)cond
              compt:(NSString *)compt
              alrtt:(NSString *)alrtt
                mdn:(NSString *)mdn
                rmk:(NSString *)rmk
                lmt:(NSString *)lmt
                aid:(NSString *)aid
    completeHandler:(CompleteHandler)completeHandler{
    /*
     s:
    A = activate
    X = deactivate
    C = complete
    L = remove
    J = Delisted
     */
    NSMutableDictionary *ev = [[NSMutableDictionary alloc]init];
    [ev setObject:aid forKey:@"aid"];
    [ev setObject:lmt forKey:@"lmt"];
    [ev setObject:rmk forKey:@"rmk"];
    if([mdn isEqualToString:@"PN"]){
        [ev setObject:[UserPrefConstants singleton].pushNotificationChanelId forKey:@"mdn"];
    }else{
        [ev setObject:mdn forKey:@"mdn"];
    }
    
    [ev setObject:alrtt forKey:@"alrtT"];
    [ev setObject:compt forKey:@"compT"];
    [ev setObject:cond forKey:@"cond"];
    [ev setObject:s forKey:@"s"];
    [ev setObject:appID forKey:@"appId"];
    [ev setObject:bhCode forKey:@"bh"];
    [ev setObject:jwtToken forKey:@"jwt"];
    
    
    NSString *evStringJWT = [NSString stringWithFormat:@"{%@}",[self convertMessToString:ev]];
    
    NSString *evString = [AESCryptAESCryptAdditions encrypt:evStringJWT password:kEncryptKey];
    
    NSString *svURL = [NSString stringWithFormat:@"%@%@%@", kDomainName, kProjectName, kUpdateAlertListPath];
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",svURL,evString,[evString MD5]];
    
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    NSLog(@"urlPushStoreInfo %@",urlPushStoreInfo);
    //NSLog(@"encoded %@",encoded);
    [APIStockAlert postRequestWithInstantURI:encoded params:nil serializer:@"application/json" completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            completeHandler(NO, nil, error);
        }else{
            NSError *error2;
            if([responseObject valueForKey:@"ev"] != [NSNull null]){
                NSData *data = [AESCryptAESCryptAdditions decryptRaw:[responseObject valueForKey:@"ev"] password:kEncryptKey];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error2];
                completeHandler(YES, json, nil);
            }else{
                completeHandler(YES, nil, nil);
            }
        }
        
        
        
    }];
    
}

+ (void)getListAlerts:(NSString *)jwtToken
               bhCode:(NSString *)bhCode
                appID:(NSString *)appID
               status:(NSString *)status
      completeHandler:(CompleteHandler)completeHandler{
    //status 0:All A:On 1:Off
//    A = activate
//    C = Complete
//    X = deactivate
//    L = Remove
//    J = Delisted
//    1 = C,X
//    0 = A,C,X
    
    NSString *svURL = [NSString stringWithFormat:@"%@%@%@", kDomainName, kProjectName, kGetAlertListPath];
    NSString *signatureString = [self getSignature:jwtToken bhCode:bhCode appID:appID status:status];
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",svURL,signatureString,[signatureString MD5]];

    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    NSLog(@"AlertFlow: getListAlerts %@ prams:jwt, bhCode, AppId, Status",urlPushStoreInfo);
    //NSLog(@"encoded %@",encoded);
    [APIStockAlert getRequestWithInstantURI:encoded params:nil serializer:@"application/json" completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            completeHandler(NO, nil, error);
        }else{
            NSError *error2;
            if([responseObject valueForKey:@"ev"] != [NSNull null]){
                NSData *data = [AESCryptAESCryptAdditions decryptRaw:[responseObject valueForKey:@"ev"] password:kEncryptKey];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error2];
                completeHandler(YES, json, nil);
            }else{
                completeHandler(YES, nil, nil);
            }
        }
        
        
        
    }];
}

+ (void) getPublicKeyWithAppURL:(NSString *)url appId:(NSString *)appId bhCode:(NSString *)bhCode completeHandler:(CompleteHandler)completeHandler{
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:bhCode forKey:@"bhCode"];
    [sendMessage setObject:appId forKey:@"appId"];
    
    NSArray *keys = [sendMessage allKeys];
    
    keys = [[keys mutableCopy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableString *result = [NSMutableString string];
    for (NSString *key in keys) {
        //DLog(@"Key %@",key);
        id value = sendMessage[key];
        if (result.length) {
            [result appendString:@","];
        }
        [result appendFormat:@"\"%@\":\"%@\"", key, [value description]];
    }
    
    NSString *md5String = [NSString stringWithFormat:@"{%@}",result];
    
    NSString *signatureString = [AESCryptAESCryptAdditions encrypt:md5String password:kEncryptKey];
    
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",url,signatureString,[md5String MD5]];
    
    NSLog(@"AlertFlow:getPublicKeyWithAppURL %@, rawParams:%@", urlPushStoreInfo, md5String);
    
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [APIStockAlert getRequestWithInstantURI:encoded params:sendMessage serializer:@"application/x-www-form-urlencoded" completeHandler:completeHandler];
    
}


+(NSString *)generateJWTToken:(NSString *)algSring
                   andEString:(NSString *)eString
                 andKidString:(NSString *)kidString
                 andKtyString:(NSString *)ktyString
                   andNString:(NSString *)nString
                     andAppId:(NSString *)appId{
    NSString *bhCode = [UserPrefConstants singleton].bhCode;
    NSString *exchange = [UserPrefConstants singleton].PrimaryExchg; //[AppConfigManager getPrimaryDefaultExchange];
    NSString *clientCode = [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"];
    NSString *userEmail = [UserPrefConstants singleton].userEmail;
    // NSLog(@"user Email %@",bhCode);
    
    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
    NSDate* newDate = [[NSDate date] dateByAddingTimeInterval:600];
    NSDate* oldDate = [[NSDate date] dateByAddingTimeInterval:-600];
    long distantFuture = (long)(NSTimeInterval)([newDate timeIntervalSince1970]);
    long notBeforeAt = (long)(NSTimeInterval)([oldDate timeIntervalSince1970]);
    
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    //[sendMessage setObject:@"102" forKey:@"bh"];
    [sendMessage setObject:bhCode forKey:@"bh"];
    [sendMessage setObject:exchange forKey:@"ex"];
    [sendMessage setObject:clientCode forKey:@"lid"];
    [sendMessage setObject:appId forKey:@"appId"];
    [sendMessage setObject:userEmail forKey:@"mail"];
    [sendMessage setObject:@"" forKey:@"mbn"];
    [sendMessage setObject:[UserPrefConstants singleton].SponsorID forKey:@"spr"];
    [sendMessage setObject:@"Y" forKey:@"reGen"];
    if([UserPrefConstants singleton].uid == nil || [[UserPrefConstants singleton].uid isEqualToString:@""]){
        [sendMessage setObject:[UserPrefConstants singleton].userName forKey:@"uid"];
    }else{
        [sendMessage setObject:[UserPrefConstants singleton].uid forKey:@"uid"];
    }
    
    NSArray *keys = [sendMessage allKeys];
    
    keys = [[keys mutableCopy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableString *result = [NSMutableString string];
    for (NSString *key in keys) {
        id value = sendMessage[key];
        if (result.length) {
            [result appendString:@","];
        }
        [result appendFormat:@"\"%@\":\"%@\"", key, [value description]];
    }
    
    NSString *evString = [NSString stringWithFormat:@"{%@}",result];
    //  NSLog(@"evString %@",evString);
    
    NSDictionary *payload = @{@"iss" : bhCode,
                              @"aud": @"SANS",
                              @"exp": [NSNumber numberWithUnsignedInteger:distantFuture],
                              @"jti": @"",
                              @"iat": [NSNumber numberWithUnsignedInteger:currentTime],
                              @"nbf": [NSNumber numberWithUnsignedInteger:notBeforeAt],
                              @"sub": @"",
                              @"ev": evString
                              };
    
    NSString *algorithmName = @"RS256";
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Secret_Key_Certificates" ofType:@"p12"];
    NSData *privateKeySecretData = [NSData dataWithContentsOfFile:filePath];
    NSString *passphraseForPrivateKey = @"secret";
    JWTBuilder *builder = [JWTBuilder encodePayload:payload].secretData(privateKeySecretData).privateKeyCertificatePassphrase(passphraseForPrivateKey).algorithmName(algorithmName);
    NSString *jwtToken = builder.encode;
    [UserPrefConstants singleton].token = jwtToken;
    return jwtToken;
}
//#define kSettingAlertListPath        @"/srvs/setLs"
//#define kUpdateSettingAlertPath        @"/srvs/updateSetting"
+ (void)getSettingList:(NSString *)jwtToken appId:(NSString *)appId bhCode:(NSString *)bhCode s:(NSString *)s completeHandler:(CompleteHandler)completeHandler{

    NSMutableDictionary *ev = [[NSMutableDictionary alloc]init];
    [ev setObject:bhCode forKey:@"bh"];
    [ev setObject:jwtToken forKey:@"jwt"];
    [ev setObject:appId forKey:@"appId"];
    [ev setObject:@"0" forKey:@"s"];
    
    NSString *evStringJWT = [NSString stringWithFormat:@"{%@}",[self convertMessToString:ev]];
    
    NSString *evString = [AESCryptAESCryptAdditions encrypt:evStringJWT password:kEncryptKey];
    
    NSString *svURL = [NSString stringWithFormat:@"%@%@%@", kDomainName, kProjectName, kSettingAlertListPath];
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",svURL,evString,[evString MD5]];
    
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    NSLog(@"urlPushStoreInfo %@",urlPushStoreInfo);
    //NSLog(@"encoded %@",encoded);
    [APIStockAlert postRequestWithInstantURI:encoded params:nil serializer:@"application/json" completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            completeHandler(NO, nil, error);
        }else{
            NSError *error2;
            if([responseObject valueForKey:@"ev"] != [NSNull null]){
                NSData *data = [AESCryptAESCryptAdditions decryptRaw:[responseObject valueForKey:@"ev"] password:kEncryptKey];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error2];
                completeHandler(YES, json, nil);
            }else{
                if([responseObject valueForKey:@"msg"] != [NSNull null]){
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:[responseObject valueForKey:@"msg"] forKey:NSLocalizedDescriptionKey];
                    error2 = [NSError errorWithDomain:@"" code:[[responseObject valueForKey:@"s"] integerValue] userInfo:details];
                    completeHandler(YES, nil, error2);
                }else{
                    completeHandler(YES, nil, nil);
                }
            }
        }
        
        
        
    }];
}
+ (void)updateSetting:(NSString *)jwtToken appId:(NSString *)appId bhCode:(NSString *)bhCode type:(NSString *)type val:(NSString *)val completeHandler:(CompleteHandler)completeHandler{
    
    NSMutableDictionary *ev = [[NSMutableDictionary alloc]init];
    [ev setObject:bhCode forKey:@"bh"];
    [ev setObject:jwtToken forKey:@"jwt"];
    [ev setObject:appId forKey:@"appId"];
    [ev setObject:type forKey:@"type"];
    [ev setObject:val forKey:@"val"];
    
    NSString *evStringJWT = [NSString stringWithFormat:@"{%@}",[self convertMessToString:ev]];
    
    NSString *evString = [AESCryptAESCryptAdditions encrypt:evStringJWT password:kEncryptKey];
    
    NSString *svURL = [NSString stringWithFormat:@"%@%@%@", kDomainName, kProjectName, kUpdateSettingAlertPath];
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",svURL,evString,[evString MD5]];
    
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    NSLog(@"urlPushStoreInfo %@",urlPushStoreInfo);
    //NSLog(@"encoded %@",encoded);
    [APIStockAlert postRequestWithInstantURI:encoded params:nil serializer:@"application/json" completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            completeHandler(NO, nil, error);
        }else{
            NSError *error2;
            if([responseObject valueForKey:@"ev"] != [NSNull null]){
                NSData *data = [AESCryptAESCryptAdditions decryptRaw:[responseObject valueForKey:@"ev"] password:kEncryptKey];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error2];
                if([[json allKeys] count] == 0){
                    completeHandler(YES, responseObject, nil);
                }else{
                    completeHandler(YES, json, nil);
                }
                
            }else{
                completeHandler(YES, responseObject, nil);
            }
        }
        
        
        
    }];
}
@end
