//
//  AppState.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/9/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AppState.h"
#import <UIKit/UIKit.h>
#import "AppMacro.h"
#import "UIVIew+TCUtil.h"
@interface AppState()
@property(strong, nonatomic) UIView *bgView;
@property(strong, nonatomic) UIActivityIndicatorView *stateView;
@property(assign, nonatomic) NSInteger numberOfLoadingViews;
@end
@implementation AppState
+ (instancetype)sharedInstance
{
    static AppState *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AppState alloc] init];
    });
    return sharedInstance;
}
- (void)setState:(AppStateType)state{
    _state = state;
    if(!IS_IPAD){
        if(state == AppStateType_Loading){
            [self addLoadingView];
        }else{
            [self removeLoadingView];
        }
    }
    
}
- (void)addLoadingView{
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [self addLoadingView:vc.view];
}
- (void)addLoadingView:(UIView *)view{
    [self addNetworkIndicator];
    if(_numberOfLoadingViews == 0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [view showPrivateHUD];
        });
    }
    _state = AppStateType_Loading;
    _numberOfLoadingViews++;
    
}
- (void)removeLoadingView:(UIView *)view{
    [self removeNetworkIndicator];
    self->_numberOfLoadingViews--;
    if (self->_numberOfLoadingViews < 0) {
        self->_numberOfLoadingViews = 0;
    }
    if(self->_numberOfLoadingViews == 0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [view dismissPrivateHUD];
        });
    }
    
}
- (void)addNetworkIndicator{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    });
    
}
- (void)removeNetworkIndicator{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    });
    
}
- (void)removeLoadingView{
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [self removeLoadingView:vc.view];
}
- (void)forceRemoveLoadingView{
    [self reloadState];
    [self removeLoadingView];
}
- (void)forceRemoveLoadingView:(UIView *)view{
    [self reloadState];
    [self removeLoadingView:view];
}
- (void)reloadState{
    _numberOfLoadingViews = 0;
    _state = AppStateType_Done;
}
@end
