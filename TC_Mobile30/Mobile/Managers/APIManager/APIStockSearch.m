//
//  APIStockSearch.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/4/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "APIStockSearch.h"
#import "ExchangeData.h"
#import "UserPrefConstants.h"
#import "TradeStatus.h"
#import "NSData-Zlib.h"
#import "Utils.h"
@implementation APIStockSearch

- (void) doStockSearchRequest:(NSString *)searchStr forExchange:(ExchangeData *)ed isDerivative:(BOOL)isDer{
    if (ed == nil) {
        if (_isJavaQC) {
            [self doStockCrossExchgSearchRequestToJavaQC:searchStr];
        }
        else {
            [self doStockCrossExchgSearchRequestToDelphiQC:searchStr];
        }
    }
    else {
        if (_isJavaQC) {
            [self doStockSearchRequestToJavaQC:searchStr forExchange:ed.feed_exchg_code isDerivative:isDer];
        }
        else {
            [self doStockSearchRequestToDelphiQC:searchStr forExchange:ed.feed_exchg_code isDerivative:isDer];
        }
    }
}

- (void) doStockCrossExchgSearchRequestToJavaQC:(NSString *)searchStr {
    //    @"http://118.201.69.222/[d3935aba3aa6406a9ffb2b3708ca47ae]Image?[MSEARCH]=KL&[EXCHANGE]=SZD,SSD,SG,O,N,KL,JK,HK,BK,A&[MARKET]=10&[FIELD]=33,38,40,51|||"
    
    ExchangeInfo *info = [UserPrefConstants singleton].currentExchangeInfo;
    NSString *currentExchange = [UserPrefConstants singleton].userCurrentExchange;
    NSString *marketCode = @"10";
    NSString *userParamQC = info.qcParams.userParams.firstObject;
    NSString *exchg_list = [APIStockSearch getExchangeList];
    NSString *urlString = [NSString stringWithFormat:@"http://%@/[%@]Image?[MSEARCH]=%@&[EXCHANGE]=%@&[MARKET]=%@&[FIELD]=33,38,40,51|||", [Utils getExchange:currentExchange].exchange_qc_server, userParamQC
                     , searchStr, exchg_list, marketCode];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    [APIStockSearch getRequestWithInstantURI:url.absoluteString params:nil serializer:@"text/html" completeHandler:^(BOOL success, id responseObject, NSError *error) {

        if(error){
            
        }else{
            NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];
            NSString *content = [APIStockSearch qcDecompressionProcess:responseData isQCCompression:NO];
            
            [APIStockSearch calculateStockData:content];
        }
    }];
}

- (void) doStockCrossExchgSearchRequestToDelphiQC:(NSString *)searchStr {
    
//    @"http://118.201.69.222/[d3935aba3aa6406a9ffb2b3708ca47ae]Image?[MSEARCH]=KL&[EXCHANGE]=SZD,SSD,SG,O,N,KL,JK,HK,BK,A&[MARKET]=10&[FIELD]=33,38,40,51|||"
    
    searchStr = [searchStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    ExchangeInfo *info = [UserPrefConstants singleton].currentExchangeInfo;
    NSString *marketCode = @"10";
    NSString *userParamQC = info.qcParams.userParams.firstObject;
    NSString *exchg_list = [APIStockSearch getExchangeList];
    NSString *urlString = [NSString stringWithFormat:@"http://%@/[%@]Image?[MSEARCH]=%@&[EXCHANGE]=%@&[MARKET]=%@&[FIELD]=33,38,40,51", [Utils getExchange:@"KL"].exchange_qc_server, userParamQC
                           , searchStr, exchg_list, marketCode];
    
    [APIStockSearch getRequestWithInstantURI:urlString params:nil completeHandler:^(BOOL success, id responseObject, NSError *error) {
        
        
        if(error){
            
        }else{
            NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];
            NSString *content = [APIStockSearch qcDecompressionProcess:responseData isQCCompression:NO];
            
            [APIStockSearch calculateStockData:content];
        }
    }];
}

- (void) doStockSearchRequestToJavaQC:(NSString *)searchStr forExchange:(NSString *)exchg isDerivative:(BOOL)isDer {
    NSString *urlString;
    if (isDer == NO) {
        urlString = [NSString stringWithFormat:@"http://%@/[%@]Image?[SEARCH]=0,2,0,10,30,0,%@,%@&[FIELD]=33,38,40,51|||", @"QCserver", @"appData.userAccountInfo.userparamQC", searchStr, exchg];
    }
    else {
        urlString = [NSString stringWithFormat:@"http://%@/[%@]Image?[SEARCH]=0,2,0,%@,30,0,%@,%@&[FIELD]=33,38,40,51|||", @"QCserver", @"appData.userAccountInfo.userparamQC", @"der_code", searchStr, exchg];
    }
    
    [APIStockSearch getRequestWithInstantURI:urlString params:nil completeHandler:^(BOOL success, id responseObject, NSError *error) {
        
        
        if(error){
            
        }else{
            NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];
            NSString *content = [APIStockSearch qcDecompressionProcess:responseData isQCCompression:NO];
            
            [APIStockSearch calculateStockData:content];
        }
    }];
}

- (void) doStockSearchRequestToDelphiQC:(NSString *)searchStr forExchange:(NSString *)exchg isDerivative:(BOOL)isDer {
    NSString *urlString;
    if (isDer == NO) {
        urlString = [NSString stringWithFormat:@"http://%@/[%@]Image?[SEARCH]=0,16,1,10,30,0,%@,%@", @"QCserver", @"appData.userAccountInfo.userparamQC", searchStr, exchg];
    }
    else {
        urlString = [NSString stringWithFormat:@"http://%@/[%@]Image?[SEARCH]=0,16,1,%@,30,0,%@,%@", @"QCserver", @"appData.userAccountInfo.userparamQC", @"der_code", searchStr, exchg];
    }
    
    [APIStockSearch getRequestWithInstantURI:urlString params:nil completeHandler:^(BOOL success, id responseObject, NSError *error) {
        
        
        if(error){
            
        }else{
            NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSASCIIStringEncoding];
            NSString *content = [APIStockSearch qcDecompressionProcess:responseData isQCCompression:NO];
            
            [APIStockSearch calculateStockData:content];
        }
    }];
}


//Calculate
+ (NSString *)getExchangeList{
    NSString *exchg_list = @"";
    for (ExchangeData *exchg in [Utils getExchangeList]) {
        if ([exchg_list isEqualToString:@""]) {
            exchg_list = exchg.trade_exchange_code;
        }
        else {
            exchg_list = [NSString stringWithFormat:@"%@,%@", exchg_list, exchg.trade_exchange_code];
        }
    }
    return  exchg_list;
}
+ (NSMutableArray *)calculateStockData:(NSString *)content{
    NSMutableArray *resultList = [NSMutableArray new];
    NSArray *lineArr = [content componentsSeparatedByString:@"\r\n"];
    
    //Find the index of [] and [END]
    int header = -1;
    int footer = -1;
    for (int j = 0; j < [lineArr count]; j++) {
        NSString *line = [lineArr objectAtIndex:j];
        if ([line isEqualToString:@"[]"]) {
            header = j;
        }
        if ([line isEqualToString:@"[END]"]) {
            footer = j;
        }
    }
    
    //if [] and [END] are found, processing the content
    if (header != -1 && footer != -1) {
        for (int i = header + 1; i < footer; i++) {
            NSString *dataRow = [lineArr objectAtIndex:i];
            
            if ([dataRow isEqualToString:@"[SUMMARY]"]) {
                break;
            }
            
            NSArray *tokens = [dataRow componentsSeparatedByString:@","];
            TradeStatus *sd = [[TradeStatus alloc] init];
            int i = 0;
            for (NSString *token in tokens) {
                switch (i) {
                    case 0:
                        sd.stock_code = (NSString *)token;
                        break;
                    case 1:
                        sd.ticker = (NSString *)token;
                        break;
                    case 2:
                        sd.lot_size = token;
                        break;
                    case 3:
                        sd.price_ref = token;
                        break;
                    default:
                        break;
                }
                i++;
            }
            [resultList addObject:sd];
        }
    }
    return resultList;
}

+ (NSString *)qcDecompressionProcess:(NSString *)qcResponseStr isQCCompression:(BOOL)isQCCompression {
    
    if (isQCCompression) {
        NSRange range1 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)13]];
        NSRange range2 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%C", (unichar)129]];
        NSRange range3 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)30]];
        NSRange range4 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)10]];
        
        if (range1.location != NSNotFound && range2.location != NSNotFound && range3.location != NSNotFound && range4.location != NSNotFound) {
            
            qcResponseStr = [qcResponseStr substringWithRange:NSMakeRange(range3.location+1, [qcResponseStr length]-range3.location-2)];
            NSString *resultStr = [[NSString alloc] initWithData:[[qcResponseStr dataUsingEncoding:NSISOLatin1StringEncoding] zlibInflate] encoding:NSISOLatin1StringEncoding];
            return resultStr;
        }
        else {
            return qcResponseStr;
        }
    }
    else {
        return qcResponseStr;
    }
}
@end
