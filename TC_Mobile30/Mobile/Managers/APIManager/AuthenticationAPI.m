//
//  AuthenticationAPI.m
//  TCiPad
//
//  Created by Kaka on 3/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "AuthenticationAPI.h"
#import "DetailBrokerModel.h"
#import "Utils.h"
#import "AllCommon.h"
#import "GTMRegex.h"
#import "ConfigureServerSession.h"
#import "BrokerManager.h"
#import "E2EEInfo.h"
#import "E2EECrypt.h"
#import "GCDQueue.h"
#import "ParserUtil.h"
#import "ATPAuthenticate.h"
#import "GCDQueue.h"
#import "VertxConnectionManager.h"
#import "BrokerModel.h"
#import "NSString+Util.h"
#import "TAKUUID.h"

//TRADE PK
#define kTRADE_PK2A            	@"TradeGetPK2A"
#define kTRADE_PK            	@"TradeGetPK"

//TRADE KEY
#define kTRADE_KEY2A        	@"TradeGetKey2A"
#define kTRADE_KEY2             @"TradeGetKey2"

//TRADE SESSION
#define kTRADE_SESSION_2A    @"TradeGetSession2A"
#define KTRADE_SESSION_2     @"TradeGetSession2"

//TRADE E2EEPARAMS
#define kTRADE_E2EE_PARAMS    @"TradeGetE2EEParams"

//TRADE LOGIN
#define kTRADE_LOGIN        @"TradeLogin"


@interface AuthenticationAPI (){
    
    //Login items
    NSString *_userName;
    NSString *_password;
    NSString *_currentSessionKey;
    
    NSString *_moduleKey;
    NSString *_exponentKey;
    NSString *_publicKey;
    
    NSString *_rsaEncryptedKey;
    //NSString *_userInfoEncryptionKey;
    //============ Broker =============
    DetailBrokerModel *_detailBroker;
}



@end
@implementation AuthenticationAPI
//================== Singleton ==============
+ (AuthenticationAPI *)shared{
    static AuthenticationAPI *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[AuthenticationAPI alloc] init];
        
    });
    return _instance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
		//init default
		[self clearData];
    }
    return  self;
}

- (void)clearData{
	isE2EE_Encryption = [UserPrefConstants singleton].E2EEEncryptionEnabled;
	_rsaEncryptedKey = nil;
	_userInfoEncryptionKey = nil;
	_detailBroker = [BrokerManager shareInstance].detailBroker;
	_e2eeInfo = nil;
	_userName = nil;
	_password = nil;
	_currentSessionKey = nil;
	
	//Key for call: Get Key2 api
	_moduleKey = nil;
	_exponentKey = nil;
	_publicKey = nil;
	_atpServer = nil;
}
#pragma mark - Override
- (void)setAtpServer:(NSString *)atpServer{
	[atpServer removeAllWhitespace];
	_atpServer = atpServer;
	[ConfigureServerSession shareInstance].atpServer = atpServer;
}

#pragma mark - Main
//1: Load balance
- (void)getATP_Balance:(CompletionHandler)handler{
    NSString *cmdStr = kTRADE_PK;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
        cmdStr = kTRADE_PK2A;
    }
    isE2EE_Encryption = [UserPrefConstants singleton].E2EEEncryptionEnabled;
    _atpServer = _detailBroker.ATPServer;

	//save to config file
    NSString *url = [NSString stringWithFormat:@"http://%@/%@?+=1|%@", _atpServer, cmdStr, [[ATPAuthenticate singleton] generateTimeStamp]];
    if (_detailBroker.HTTPSEnabled) {
        url = [NSString stringWithFormat:@"https://%@/%@?+=1|%@", _atpServer, cmdStr, [[ATPAuthenticate singleton] generateTimeStampURLEncoded]];
    }
    DLog(@"+++ getBalance URL: %@", url);
    [CPService requestDataFromURL:url completion:^(id result, NSError *error) {
        handler(result, error);
    }];
}

//2: ============ getATPKey2 ================
- (void)getATP_Key2_From:(NSString *)cer exponent:(NSString *)exponent completion:(CompletionHandler)handler{
    //Prepare data
    _userInfoEncryptionKey = [[ATPAuthenticate singleton] generateAESKey];
	[ATPAuthenticate singleton].userInfoEncryptionKey = _userInfoEncryptionKey; //Use this for old case
    _rsaEncryptedKey = [[ATPAuthenticate singleton] rsaEncryptData:_userInfoEncryptionKey withCertContent:cer andTag:@"atp_publicKey" andExponent:exponent];
    [[ATPAuthenticate singleton] removeKeyRefWithTag:@"atp_publicKey"];
    NSString *cmdStr = kTRADE_KEY2;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
        cmdStr = kTRADE_KEY2A;
    }
    //NSString *atpServer = _detailBroker.ATPServer;
    NSString *urlStr;
    if (_detailBroker.HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", _atpServer,cmdStr, _rsaEncryptedKey];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", _atpServer,cmdStr, _rsaEncryptedKey];
    }
    //Call service
    [CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
        DLog(@"+++ATP Key2 Result: %@", result);
        NSString *dataSTR = (NSString *)result;
        if ([dataSTR isEqualToString:@"Error"] || [dataSTR isEqualToString:@"ERROR"]) {
            dataSTR = kFAILED_KEY;
        }
        handler(result, error);
    }];
}

//3: Get atpSession2
- (void)getATP_Sesion2:(NSString *)rsaEncryptedKey completion:(CompletionHandler)handler{
    NSString *cmdStr = KTRADE_SESSION_2;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
        cmdStr = kTRADE_SESSION_2A;
    }
    _atpServer = _detailBroker.ATPServer;
    NSString *urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", _atpServer, cmdStr, rsaEncryptedKey];;
    if (_detailBroker.HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", _atpServer, cmdStr, rsaEncryptedKey];
    }
    DLog(@"+++TradeGetSession URL: %@",urlStr);
    //+++
    //    NSString *cmdStr = @"TradeGetSession2A";
    //    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
    //        cmdStr = @"TradeGetSession2A";
    //    }else{
    //        cmdStr = @"TradeGetSession2";
    //    }
    //
    //    atpServer = [UserPrefConstants singleton].userATPServerIP;
    //    if ([UserPrefConstants singleton].HTTPSEnabled) {
    //        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", atpServer,cmdStr, rsaEncryptedKey];
    //    } else {
    //        urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", atpServer,cmdStr, rsaEncryptedKey];
    //    }
    //    NSLog(@"TradeGetSession URL: %@",urlStr);
    //
    //    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    //Call Service
    [CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
        DLog(@"+++ATP Session 2 Result: %@", result);
        handler(result, error);
    }];
}

//4: geATP_E2EEParams
- (void)getATP_E2EEParams:(CompletionHandler)handler{
    NSString *cmdStr  = @"TradeGetE2EEParams";
    NSString *urlStr;
    NSString *atpServer = _detailBroker.ATPServer;
    if (_detailBroker.HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", atpServer,cmdStr, [[ATPAuthenticate singleton] generateTimeStamp]];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", atpServer,cmdStr, [[ATPAuthenticate singleton] generateTimeStamp]];
    }
    //Call service
    [CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
        if (error || [self isE2EE_ErrorContent:result]) {
            result = kFAILED_KEY;
        }
        handler(result, error);
    }];
}

- (void)apt_doLoginWithCompletion:(CompletionHandler)handler{
    NSString *cmdStr = kTRADE_LOGIN;
    NSString *urlStr = [NSString stringWithFormat:@"http://%@/[0]%@", _atpServer, cmdStr];
    if (_detailBroker.HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]%@", _atpServer, cmdStr];
    }
    DLog(@"+++doLogin: %@", urlStr);
    //====== ++++ Need to clear 2FA, SMS cache ==========
    // clear the 2FA for new user login
    // clear the SMS OTP for new user login
    NSString *postStr = [self preparePostDataStringFromUsername:_userName password:_password];
    [CPService requestPostData:postStr url:urlStr completion:^(id result, NSError *error) {
        if (!error) {
            [[GCDQueue backgroundPriorityGlobalQueue] queueBlock:^{
                __block NSError *parseError = [[ParserUtil shareInstance] parseResultOfLoginData:(NSString *)result userInfoEncryptionKey:self->_userInfoEncryptionKey];
				[[GCDQueue mainQueue] queueBlock:^{
					handler(result, parseError);
				}];
            }];
        }else{
            handler(result, error);
        }
    }];
}

//================ CASE 2: USE GET ATP BRANCH =====================================
- (void)getATP_PK:(CompletionHandler)handler{
    NSString *atpServer = _detailBroker.ATPServer;
    NSString *cmdStr = kTRADE_PK;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
        cmdStr = kTRADE_PK2A;
    }
    NSString *urlStr = [NSString stringWithFormat:@"http://%@/%@?+=1|%@", atpServer,cmdStr, [[ATPAuthenticate singleton] generateTimeStamp]];
    if (_detailBroker.HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?+=1|%@", atpServer,cmdStr, [[ATPAuthenticate singleton] generateTimeStampURLEncoded]];
    }
    [CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
        NSString *content = (NSString *)result;
        if (content.length == 0) {
            content = kFAILED_KEY;
        }
        handler(content, error);
    }];
}


/**
 Logout function
 @param handler <#handler description#>
 */
- (void)doLogout:(CompletionHandler)handler{
    NSString *urlStr;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradeLogout",_atpServer, _currentSessionKey];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradeLogout",_atpServer, _currentSessionKey];
    }
    [CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
        NSString *content = (NSString *)result;
        if (content.length == 0) {
            content = @"Success";
        }
        handler(content, error);
    }];
}

#pragma mark - Wrap to start login
- (void)autoDectFollowAuthentication:(void (^)(BOOL isFollowBalance, id result, NSError *error))hander{
	__block NSString *contentResult;
	if (_detailBroker.ATPLoadBalance) {
		DLog(@"1111: getATP_Balance");
		[self getATP_Balance:^(id result, NSError *error) {
			if (error) {
				DLog(@"+++ Get getATP_Balance Error");
			}
			[[GCDQueue globalQueue] queueBlock:^{
				contentResult = (NSString *)result;
				if (!error && contentResult.length > 0) {
					[self parseATP_Balance:contentResult];
					[[GCDQueue mainQueue] queueBlock:^{
						[UserPrefConstants singleton].bhCode = self->_detailBroker.BrokerID;
						self->_detailBroker.BHCode = self->_detailBroker.BrokerID;
						hander(YES, result, error);
					}];
				}else{
					[[GCDQueue mainQueue] queueBlock:^{
						hander(YES, result, error);
					}];
				}
			}];
		}];
	} else {
		DLog(@"1111: getATP_PK");
		[self getATP_PK:^(id result, NSError *error) {
			if (error) {
				DLog(@"+++ Get getATP_PK Error");
			}
			[[GCDQueue globalQueue] queueBlock:^{
				contentResult = (NSString *)result;
				if (!error && contentResult.length > 0) {
					[self parseATP_Balance:contentResult];
					[[GCDQueue mainQueue] queueBlock:^{
						hander(NO, result, error);
					}];
				}else{
					[[GCDQueue mainQueue] queueBlock:^{
						hander(NO, result, error);
					}];
				}
			}];
		}];
	}
}

#pragma mark - MAIN
//======== Main : Call this to start performing login ===========
- (void)startLoginFrom:(NSString *)userID password:(NSString *)password progress:(AUProgressHandler)progressHandler completion:(CompletionHandler)completionHandler{
	//Clear cache first
	[self clearData];
	//Initial
	_userName = userID;
	_password = password;
	_detailBroker = [BrokerManager shareInstance].detailBroker;
	//For old follow
	[UserPrefConstants singleton].userName = userID;
	[UserPrefConstants singleton].userPassword = password;
	//Set progress
	if (progressHandler) {
		progressHandler(LOGIN_INIT);
	}
	//__weak typeof(self) weakSelf = self;
	//1: Call auto detect follow
	[self autoDectFollowAuthentication:^(BOOL isFollowBalance, id result, NSError *error) {
		if (error || [result isEqualToString:kFAILED_KEY]) {
			completionHandler(result, error);
			DLog(@"+++ autoDectFollowAuthentication Error");
			return ;
		}
		//2: Call get_ATP_Key2
		NSString *moduleKey = [self->_moduleKey copy];
		NSString *exponentKey = [self->_exponentKey copy];
		NSString *publicKey = [self->_publicKey copy];
		if (isFollowBalance) {
			if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
				//Nothing change
			}else{
				moduleKey = publicKey;
				exponentKey = nil;
			}
		}else{
			if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
				//Keep nothing change
			}else{
				moduleKey = result;
				exponentKey = nil;
			}
		}
		//Set progress
		if (progressHandler) {
			progressHandler(LOGIN_PK);
		}
		__weak typeof(self) weakSelf = self;
		DLog(@"2222: getATP_Key2_From - moduleKey: %@ - exponentKey: %@", moduleKey, exponentKey);
		[self getATP_Key2_From:moduleKey exponent:exponentKey completion:^(id result, NSError *error) {
			if (error || [result isEqualToString:kFAILED_KEY]) {
				completionHandler(result, error);
				if (error) {
					DLog(@"+++ getATP_Key2_From");
					completionHandler(result, error);
				}
				return ;
			}
			NSString *dataSTR = (NSString *)result;
			//Handler successfully here: Call api getSession
			if ([weakSelf parseToCompareATPKey2Result:dataSTR]) {
				//Call api getSession2
				//Set progress
				if (progressHandler) {
					progressHandler(LOGIN_SESSION2);
				}
				DLog(@"3333: getATP_Sesion2 - _rsaEncryptedKey: %@", self->_rsaEncryptedKey);
				[weakSelf getATP_Sesion2:self->_rsaEncryptedKey completion:^(id result, NSError *error) {
					if (error || [result isEqualToString:kFAILED_KEY]) {
						completionHandler(result, error);
						if (error) {
							DLog(@"+++ getATP_Sesion2");
							completionHandler(result, error);
						}
						return ;
					}
					//3:Handle successfully: Call e2ee or doLogin
					NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:result currentUserInfoEncryptionKey:self->_userInfoEncryptionKey];
					[ConfigureServerSession shareInstance].currentSessionKey = content;
					self->_currentSessionKey = content;
					//Call login service or atp_e2ee Params
					if (self->isE2EE_Encryption){
						//Set progress
						if (progressHandler) {
							progressHandler(LOGIN_E2EE);
						}
						DLog(@"4444: getATP_E2EEParams");
						[weakSelf getATP_E2EEParams:^(id result, NSError *error) {
							if (error || [result isEqualToString:kFAILED_KEY]) {
								completionHandler(result, error);
								if (error) {
									DLog(@"+++ getATP_E2EEParams");
									completionHandler(result, error);
								}
								return ;
							}
							//Handle successfully here
							[weakSelf parseE2EE_Params:result];
							if ([weakSelf isEncryptedPassword:self->_e2eeInfo]) {
								//Set progress
								if (progressHandler) {
									progressHandler(LOGIN_LOGIN);
								}
								//4:call api login here
								[weakSelf apt_doLoginWithCompletion:^(id result, NSError *error) {
									if (!error) {
										//Step 5: Get Exchange List
										//Set progress
										if (progressHandler) {
											progressHandler(LOGIN_GET_EXCHANGE);
										}
										//5: GET EXCHANGES LIST
										[weakSelf getExchangeList:^(id result, NSError *error) {
											//Do something here....
											//Call Get Client Account here
											if (!error) {
												//6: Final step here: Connect ATP + get account list
												if (progressHandler) {
													progressHandler(LOGIN_GET_CLIENT_ACCOUNT);
												}
												[weakSelf atpGetAccountClientList:^(id result, NSError *error) {
													if (!error) {
														if (progressHandler) {
															progressHandler(LOGIN_SUCCESS);
														}
													}
													//Finish
													completionHandler(result, error);
												}];
											}else{
												completionHandler(result, error);
											}
										}];
									}else{
										completionHandler(result, error);
									}
								}];
							}else{
								completionHandler(kFAILED_KEY, error);
							}
						}];
					}else {
						//Set progress
						if (progressHandler) {
							progressHandler(LOGIN_LOGIN);
						}
						//call api login here
						DLog(@"4444: apt_doLoginWithCompletion");
						[weakSelf apt_doLoginWithCompletion:^(id result, NSError *error) {
							if (!error) {
								//Step 5: Get Exchange List
								//Set progress
								if (progressHandler) {
									progressHandler(LOGIN_GET_EXCHANGE);
								}
								//5: GET EXCHANGES LIST
								[weakSelf getExchangeList:^(id result, NSError *error) {
									//Do something here....
									//Call Get Client Account here
									if (!error) {
										//6: Final step here: Connect ATP + get account list
										if (progressHandler) {
											progressHandler(LOGIN_GET_CLIENT_ACCOUNT);
										}
										[weakSelf atpGetAccountClientList:^(id result, NSError *error) {
											if (!error) {
												if (progressHandler) {
													progressHandler(LOGIN_SUCCESS);
												}
											}
											//Finish
											completionHandler(result, error);
										}];
									}else{
										DLog(@"ERROR: 5: GET EXCHANGES LIST");
										completionHandler(result, error);
									}
								}];
							}else{
								DLog(@"ERROR: 4444: apt_doLoginWithCompletion");
								completionHandler(result, error);
							}
						}];
					}
				}];
			}else{
				//completion
				completionHandler(kFAILED_KEY, error);
			}
		}];
	}];
}


#pragma mark - TRADING API relevant
- (void)getExchangeList:(CompletionHandler)handler{
    //OCT 2016 - All URL get from LMS plist
    NSString *urlStr = [NSString stringWithFormat:@"%@identifier=%@&sponsor=%@&prod=TCiPhone",
                        [UserPrefConstants singleton].LMSServerAddress,
                        _userName,
                        [UserPrefConstants singleton].SponsorID];
    [CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
        if (error) {
            DLog(@"++++getExchangeList ERROR: %@", urlStr);
            handler(result, error);
        }else{
            [[GCDQueue globalQueue] queueBlock:^{
                [self parseExchangeListData:result];
                [[GCDQueue mainQueue] queueBlock:^{
					//Connect VertX - At first step now
					//[[VertxConnectionManager singleton] connect];
                    handler(result, error);
                }];
            }];
        }
    }];
}

- (void)atpGetAccountClientList:(CompletionHandler)handler{
	NSString *postStr = [NSString stringWithFormat:@"'=%@", [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"]];
	NSString *urlStr;
	if (_detailBroker.HTTPSEnabled) {
		urlStr = [NSString stringWithFormat:@"https://%@/[0]TradeClient?10|%c%@|%@%cE",
				  _atpServer, START_OF_TEXT,
				  [[ATPAuthenticate singleton] base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
				  [[ATPAuthenticate singleton] AESEncryptAndBase64Encode:postStr withKey:_userInfoEncryptionKey],
				  END_OF_TEXT];
		
	} else {
		urlStr = [NSString stringWithFormat:@"http://%@/[0]TradeClient?10|%c%@|%@%cE",
				  _atpServer, START_OF_TEXT,
				  [[ATPAuthenticate singleton] base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
				  [[ATPAuthenticate singleton] AESEncryptAndBase64Encode:postStr withKey:_userInfoEncryptionKey],
				  END_OF_TEXT];
	}
	
	if ([UserPrefConstants singleton].userAccountlist.count > 0) {
		[UserPrefConstants singleton].userAccountlist = @[].mutableCopy;
	}
	
	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
		if (error) {
			DLog(@"++++atpGetAccountClientList ERROR: %@", urlStr);
			handler(result, error);
		}else{
			[[GCDQueue globalQueue] queueBlock:^{
				__block NSError *parseError = [[ParserUtil shareInstance] parseAccountClientList:result];
				[[GCDQueue mainQueue] queueBlock:^{
					handler(result, parseError);
				}];
			}];
		}
	}];
}

#pragma mark - Check Single LogOn
- (void)checkSingleLogOn{
	NSString *postStr = [NSString stringWithFormat:@"+=%@", _userName];
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]CheckSingleLogon?%@", _atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
			  postStr];
	//Call request
	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
		if (error) {
			DLog(@"++++atpGetAccountClientList ERROR: %@", urlStr);
			//Do nothing
		}else{
			NSString *_message;
			NSString *_status_code;
			
			//Parse data
			NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:(NSString *)result];
			NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
			//NSLog(@"doCheckSingleLogon %@", content);
			if ([arr count] < 2) {
				if (([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1116"].location != NSNotFound) ||
					([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1102"].location != NSNotFound) ||
					([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1201"].location != NSNotFound) ||
					([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1100"].location != NSNotFound)) {
					
					_message = content;
					_status_code = content;
				}
			}
			else {
				GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
				GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
				
				int message = -1;   //,
				int status = -1;    //-
				NSString *s;
				for (s in arr) {
					if ([metaregex matchesString:s]) {
						s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
						NSArray *tokens = [s componentsSeparatedByString:@"|"];
						if ([tokens count] >= 4 && [tokens count] < 60) {
							int pos = 0;
							for (NSString *token in tokens) {
								if ([token isEqualToString:@","]) {
									message = pos;
								}
								else if ([token isEqualToString:@"-"]) {
									status = pos;
								}
								pos++;
							}
						}
					}
					else if ([dataregex matchesString:s]) {
						s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
						NSArray *tokens = [s componentsSeparatedByString:@"|"];
						if ([tokens count] >= 4 && [tokens count] < 60) {
							if (message != -1) {
								_message = [tokens objectAtIndex:message];
							}
							if (status != -1) {
								_status_code = [tokens objectAtIndex:status];
							}
						}
					}
				}
			}
			if (![_status_code isEqualToString:@"0"]) {
				NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
				[notificationData setObject:_status_code forKey:@"statusCode"];
				[notificationData setObject:_message forKey:@"message"];
				[[NSNotificationCenter defaultCenter] postNotificationName:@"doKickout" object:self userInfo:notificationData];
			}
		}
	}];
}

#pragma mark - Utils VC
//Parse ATP_Balance
- (void)parseATP_Balance:(NSString *)strData{
    GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
    GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
    
    NSLog(@"atpGetKey Returned : %@",strData);
    int rsa_public_key = -1;
    int atp_public_ip = -1;
    int atp_private_ip = -1;
    int secondary_port = -1;
    int modulus = -1;
    int exponent = -1;
    
    NSString *public_key = @"";
    NSString *modulus_key = @"";
    NSString *exponent_key = @"";
    
    NSArray *arr = [strData componentsSeparatedByString:@"\r\n"];
    
    for (NSString *s in arr) {
        if ([metaregex matchesString:s]) {
            NSString *s2 = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
            NSArray *tokens = [s2 componentsSeparatedByString:@"|"];
            int pos = 0;
            for (NSString *token in tokens) {
                if ([token isEqualToString:@","]) {
                    rsa_public_key = pos;
                }
                else if ([token isEqualToString:@"-"]) {
                    atp_public_ip = pos;
                }
                else if ([token isEqualToString:@"."]) {
                    atp_private_ip = pos;
                }
                else if ([token isEqualToString:@"0"]) {
                    secondary_port = pos;
                }else if([token isEqualToString:@"1"]){
                    modulus = pos;
                }else if([token isEqualToString:@"2"]){
                    exponent = pos;
                }
                pos++;
            }
        }
        else if ([dataregex matchesString:s]) {
            NSString *s2 = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
            NSArray *tokens = [s2 componentsSeparatedByString:@"|"];
            
            if (rsa_public_key != -1 && [tokens count] > rsa_public_key) {
                public_key = [tokens objectAtIndex:rsa_public_key];
            }
            if (atp_public_ip != -1 && [tokens count] > atp_public_ip) {
                NSData *public_ip_data = [[ATPAuthenticate singleton] base64Decode:[tokens objectAtIndex:atp_public_ip]];
                NSString *public_ip = [[NSString alloc] initWithData:public_ip_data encoding:NSASCIIStringEncoding];
                [ConfigureServerSession shareInstance].atpPublicIP = public_ip;
                //Addmore
                [UserPrefConstants singleton].atpPublicIP = public_ip;
            }
            if (atp_private_ip != -1 && [tokens count] > atp_private_ip) {
                NSData *private_ip_data = [[ATPAuthenticate singleton] base64Decode:[tokens objectAtIndex:atp_private_ip]];
                NSString *private_ip = [[NSString alloc] initWithData:private_ip_data encoding:NSASCIIStringEncoding];
                [ConfigureServerSession shareInstance].atpPrivateIP = private_ip;
                //AddMore
                [UserPrefConstants singleton].atpPrivateIP = private_ip;
            }
            if (secondary_port != -1 && [tokens count] > secondary_port) {
                NSData *secondary_port_data = [[ATPAuthenticate singleton] base64Decode:[tokens objectAtIndex:secondary_port]];
                NSString *secondary_port = [[NSString alloc] initWithData:secondary_port_data encoding:NSASCIIStringEncoding];
                [ConfigureServerSession shareInstance].atpPort = secondary_port;
                //AddMore
                [UserPrefConstants singleton].atpPort = secondary_port;
            }
            if (modulus != -1 && [tokens count] > modulus) {
                modulus_key = [tokens objectAtIndex:modulus];
                
            }
            if (exponent != -1 && [tokens count] > exponent) {
                exponent_key = [tokens objectAtIndex:exponent];
            }
            
            //ggtt ssi
            //Check to assign new atp
            if ([UserPrefConstants singleton].atpPublicIP != nil) {
				if ([UserPrefConstants singleton].atpPublicIP.length > 0) {
					_atpServer = [UserPrefConstants singleton].atpPublicIP;
					if ([UserPrefConstants singleton].atpPort != nil) {
						_atpServer = [NSString stringWithFormat:@"%@:%@", _atpServer, [UserPrefConstants singleton].atpPort];
					}
				}
            } else {
                _atpServer = [UserPrefConstants singleton].userATPServerIP;
            }
            
            //Params for api get atpKey2
            _moduleKey = modulus_key;
            _exponentKey = exponent_key;
            _publicKey = public_key;
        }
    }
}

//Parse ATP_KEY2
- (BOOL)parseToCompareATPKey2Result:(NSString *)dataSTR{
    NSData *decodedContent = [[ATPAuthenticate singleton] base64Decode:dataSTR];
    NSData *doubleHashKey = [[ATPAuthenticate singleton] getDoubleHashEncryptionKey:_userInfoEncryptionKey];
    return [decodedContent isEqualToData:doubleHashKey];
}

//Check error E2EE Params
- (BOOL)isE2EE_ErrorContent:(NSString *)content{
    GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
    GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
    if ([errorReg1 matchesString:content] || [errorReg2 matchesString:content] || [content isEqualToString:@""]) {
        return YES;
    }
    if ([content rangeOfString:@"(="].location == NSNotFound) {
        return YES;
    }
    return NO;
}

- (void)parseE2EE_Params:(NSString *)content{
    {
        NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
        
        int pubKey = -1;
        int sessionID = -1;
        int randomNumber = -1;
        int modulus = -1;
        int exponent = -1;
        
        if (_e2eeInfo != nil) {
            _e2eeInfo = nil;
        }
        _e2eeInfo = [[E2EEInfo alloc] init];
        for (NSString *str in arr) {
            if ([str hasPrefix:@"(="]) {
                NSString *s = [str stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                int pos = 0;
                for (NSString *token in tokens) {
                    if ([token isEqualToString:@"+"]) {
                        pubKey = pos;
                    }
                    else if ([token isEqualToString:@","]) {
                        sessionID = pos;
                    }
                    else if ([token isEqualToString:@"-"]) {
                        randomNumber = pos;
                    }
                    else if ([token isEqualToString:@"."]) {
                        modulus = pos;
                    }
                    else if ([token isEqualToString:@"/"]) {
                        exponent = pos;
                    }
                    pos++;
                }
            }
            else if ([str hasPrefix:@")="]) {
                NSString *s = [str stringByReplacingOccurrencesOfString:@")=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                if (pubKey != -1) {
                    _e2eeInfo.pubKey = [tokens objectAtIndex:pubKey];
                }
                if (sessionID != -1) {
                    _e2eeInfo.sessionID = [tokens objectAtIndex:sessionID];
                }
                if (randomNumber != -1) {
                    _e2eeInfo.randomKey = [tokens objectAtIndex:randomNumber];
                }
                if (modulus != -1) {
                    _e2eeInfo.modulus = [tokens objectAtIndex:modulus];
                }
                if (exponent != -1) {
                    _e2eeInfo.exponent = [tokens objectAtIndex:exponent];
                }
            }
        }
        
        //Generate E2EE Encrypted Password
        _e2eeInfo.e2ee_EncryptedPassword =
        [E2EECrypt e2eePassword:_e2eeInfo.pubKey randomKey:_e2eeInfo.randomKey session:_e2eeInfo.sessionID password:_password];
    }
}

//======== Parse

#pragma mark - UTILS
//Check did encrypt password
- (BOOL)isEncryptedPassword:(E2EEInfo *)e2eeInfo{
    if (e2eeInfo.e2ee_EncryptedPassword != nil && ![e2eeInfo.e2ee_EncryptedPassword isEqualToString:@""]) {
        return YES;
    }
    return NO;
}

//===== Prepare Post url for login api ====
- (NSString *)preparePostDataStringFromUsername:(NSString *)userName password:(NSString *)password{
    // clear the 2FA for new user login
    // clear the SMS OTP for new user login
    [UserPrefConstants singleton].verified2FA = NO;
    // [UserPrefConstants singleton].deviceListFor2FA = [[NSMutableArray alloc] init];
    [[UserPrefConstants singleton].deviceListFor2FA removeAllObjects];
    [[UserPrefConstants singleton].deviceListForSMSOTP removeAllObjects];
    [UserPrefConstants singleton].byPass2FA = @"";
    [UserPrefConstants singleton].is2FARequired =  NO;
    [UserPrefConstants singleton].dvData2FA = nil;
    [UserPrefConstants singleton].clientLimitOptionDict = [[NSMutableDictionary alloc] init];
    
    NSString *postStr = @"";
    NSString *systemVersion =@"6";
    NSString *deviceModel =@"1";
    NSString *networkType =@"Wifi";
    NSString *carrierName =@"MAXIS";
    NSString *bundleVersion =@"0.1.2";
    NSString *screenHeightWidth =@"960x640";
    NSString *isATPCommpression =@"2"; //([AppConfigManager isATPCompression]?@"2":@"0")
    //Add more UUID for SSI version
	if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
		bundleVersion = [bundleVersion stringByAppendingString:[NSString stringWithFormat:@" [UDID]%@", [TAKUUIDStorage sharedInstance].findOrCreate]];
	}
    if (isE2EE_Encryption) {
        postStr = [NSString stringWithFormat:@"Request=[@RSAEnabled@]UserName=%@|Password=%@|RSAPassword=%@|E2EERandomNumber=%@|AppName=M|AppCode=MD|OSName=iOS|Version=%@|Model=%@|NetType=%@|Provider=%@|Content=%@|ScreenSize=%@|PullMode=1|PullCompress=%@|Encryption=0|EncryptedUP=0|ClientIP=%@",
                   userName,
                   _e2eeInfo.e2ee_EncryptedPassword,
                   [[ATPAuthenticate singleton] rsaEncryptData:password withModulus:_e2eeInfo.modulus andExponent:_e2eeInfo.exponent andTag:@"e2ee_publicKey"],
                   _e2eeInfo.randomKey,
                   systemVersion,
                   deviceModel,
                   networkType,
                   carrierName,
                   bundleVersion,
                   screenHeightWidth,
                   isATPCommpression,
                   [[ATPAuthenticate singleton] getIPAddress]];
        [[ATPAuthenticate singleton] removeKeyRefWithTag:@"e2ee_publicKey"];
    }
    else {
        postStr = [NSString stringWithFormat:@"Request=UserName=%@|Password=%@|AppName=M|AppCode=MD|OSName=iOS|Version=%@|Model=%@|NetType=%@|Provider=%@|Content=%@|ScreenSize=%@|PullMode=1|PullCompress=%@|Encryption=0|EncryptedUP=1|ClientIP=%@",
                   [[ATPAuthenticate singleton] encryptString:userName],
                   [[ATPAuthenticate singleton] encryptString:password],
                   systemVersion,
                   deviceModel,
                   networkType,
                   carrierName,
                   bundleVersion,
                   screenHeightWidth ,
                   isATPCommpression,
                   [[ATPAuthenticate singleton] getIPAddress]];
    }
    if (_detailBroker.BHCode != nil && ![_detailBroker.BHCode isEqualToString:@""]) {
        postStr = [NSString stringWithFormat:@"%@|BHCode=%@", postStr, _detailBroker.BHCode];
    }
    postStr = [NSString stringWithFormat:@"%@&Userparams=%@", postStr, _currentSessionKey];
    postStr = [self urlEscapeString:postStr];
    
    // if encryption (Sure it is)
    postStr = [NSString stringWithFormat:@"10|%c%@|%@%cE",
               START_OF_TEXT,
               [[ATPAuthenticate singleton] base64Encode:_currentSessionKey],
               [[ATPAuthenticate singleton] AESEncryptAndBase64Encode:postStr withKey:_userInfoEncryptionKey],
               END_OF_TEXT];
    return postStr;
}


/**
 
 
 @param data data from Result to get userExchagelist: [UserPrefConstants singleton].userExchagelist
 */
- (void)parseExchangeListData:(NSString *)data{
    {
        //Prepare available first
        NSMutableArray *userExchangelist = @[].mutableCopy;
        //===========
        NSString *content = data;
        NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
        NSArray *dataTokens = [[NSArray alloc] init];
        NSArray *MDToken =[[NSArray alloc]init];
        int data = 0;
        for (NSString *line in arr) {
            if (data != 1) {
                if ([line isEqualToString:@"--_BeginData_"]) {
                    data = 1;
                }
            }
            else {
                if ([line isEqualToString:@"--_EndData_"]) {
                    data = 2;
                }
                else {
                    dataTokens = [line componentsSeparatedByString:@"|"];
                }
            }
            
            if (data == 2) {
                break;
            }
        }
        if ([dataTokens count] >= 2) {
            if([[dataTokens objectAtIndex:0] isEqualToString:@"0"]) { // SUCCESS?
                //Begin All Service
                int service = 0;
                for (NSString *line in arr) {
                    
                    if (service != 1) {
                        if ([line isEqualToString:@"--_BeginAllService_"]) {
                            service = 1;
                        }
                    }
                    else {
                        if ([line isEqualToString:@"--_EndAllService_"]) {
                            service = 2;
                        }
                        else {
                            NSArray *tokens = [line componentsSeparatedByString:@"|"];
                            if ([tokens count] >= 18) {
                                ExchangeData *ed = [[ExchangeData alloc] init];
                                ed.equityOrDer = [[tokens objectAtIndex:2] intValue];
                                ed.exchange_name = [tokens objectAtIndex:3];
                                ed.exchange_description = [tokens objectAtIndex:4];
                                ed.exchange_type = [tokens objectAtIndex:5];
                                ed.feed_exchg_code = [tokens objectAtIndex:6];
                                ed.currency = [tokens objectAtIndex:7];
                                
                                // if null, then set mktDepth = 1;
                                if (([[tokens objectAtIndex:9] isEqual:[NSNull null]]) ||
                                    ([[tokens objectAtIndex:9] isEqualToString:@"null"]))
                                    ed.mktDepthLevel = @"1";
                                else
                                    ed.mktDepthLevel = [tokens objectAtIndex:9];
                                
                                NSLog(@"Resolved: %@",ed.mktDepthLevel);
                                
                                ed.MDLfeature = 0;
                                NSString *feedString = [tokens objectAtIndex:16];
                                if (feedString.length>0){
                                    
                                    ed.exchange_qc_server = [[[tokens objectAtIndex:16] componentsSeparatedByString:@":"] objectAtIndex:1];
                                    [UserPrefConstants singleton].qcServer = ed.exchange_qc_server;
                                    
                                }
                                ed.trade_exchange_code = [tokens objectAtIndex:17];
                                ed.subsCPIQCompSynp = (BOOL)[[ATPAuthenticate singleton] isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:0];
                                ed.subsCPIQCompInfo = (BOOL)[[ATPAuthenticate singleton] isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:1];
                                ed.subsCPIQAnnounce = (BOOL)[[ATPAuthenticate singleton] isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:2];
                                ed.subsCPIQCompKeyPer = (BOOL)[[ATPAuthenticate singleton] isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:3];
                                ed.subsCPIQShrHoldSum = (BOOL)[[ATPAuthenticate singleton] isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:4];
                                ed.subsCPIQFinancialRep = (BOOL)[[ATPAuthenticate singleton] isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:5];
                                [userExchangelist addObject:ed];
                            }
                        }
                    }
                    if (service == 2) {
                        break;
                    }
                }
            }
        }
        data =0;
        for (NSString *line in arr) {
            if (data != 1) {
                if ([line isEqualToString:@"--_BeginFeatures_"]) {
                    data = 1;
                }
            } else {
                if ([line isEqualToString:@"--_EndFeatures_"]) {
                    data = 2;
                } else {
                    dataTokens = [line componentsSeparatedByString:@"|"];
                    MDToken = [[dataTokens objectAtIndex:0]componentsSeparatedByString:@"-"];
                    
                    if (([MDToken count] >= 2)&&([[MDToken objectAtIndex:0] isEqualToString:@"MD"])) {
                        // update override MD level for listed exchange
                        for (ExchangeData *ed in userExchangelist) {
                            if ([ed.feed_exchg_code.uppercaseString isEqual:[[MDToken objectAtIndex:1] uppercaseString]]) {
                                ed.mktDepthLevel =[NSString stringWithFormat:@"%d",[[MDToken objectAtIndex:2] intValue]];
                                ed.MDLfeature = 1;
                            }
                        }
                    }
                }
            }
            if (data == 2) { // END
                break;
            }
        }
		//Using this to Fake Exchange for SSI
		//* === Will remove later
		if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
			[userExchangelist addObjectsFromArray:[self createDemoSSIExchange]];
		}
        [UserPrefConstants singleton].userExchagelist = [userExchangelist mutableCopy];
        
        //            // Override exchange names
        //            // [self overrideExchangeNames]; // 16 June 2017 info: LMS will update on their side
        //            //+++Should Get Exchange Info here
        //            [[VertxConnectionManager singleton] vertxGetExchangeInfo];
        //            [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveExchangeList" object:nil]; // pnExchangeListDone
        //            [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveExchangeListMain" object:nil]; // pnExchangeListDone
        //            [self serverLogIn];
    }
}
#pragma mark - Escape String URL
- (NSString*)urlEscapeString:(NSString *)url{
    //NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
    //NSString * encodedString = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
    //NSString *encodedString  = [url stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];
    return [url stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];;
}

- (NSArray *)createDemoSSIExchange{
	NSMutableArray *ssiEx = @[].mutableCopy;
	ExchangeData *hsx = [ExchangeData new];
	hsx.exchange_name = @"HSX";
	hsx.feed_exchg_code = @"HSX";
	hsx.trade_exchange_code = @"HSX";
	hsx.MDLfeature = YES;
	hsx.mktDepthLevel = @"3";
	[ssiEx addObject:hsx];
	
	ExchangeData *hnx = [ExchangeData new];
	hnx.exchange_name = @"HNX";
	hnx.feed_exchg_code = @"HNX";
	hnx.trade_exchange_code = @"HNX";
	hnx.MDLfeature = YES;
	hnx.mktDepthLevel = @"10";
	[ssiEx addObject:hnx];
	
	ExchangeData *dvt = [ExchangeData new];
	dvt.exchange_name = @"DVT";
	dvt.feed_exchg_code = @"DVT";
	dvt.trade_exchange_code = @"DVT";
	dvt.MDLfeature = YES;
	dvt.mktDepthLevel = @"10";
	[ssiEx addObject:dvt];
	
	ExchangeData *upc = [ExchangeData new];
	upc.exchange_name = @"UPC";
	upc.feed_exchg_code = @"UPC";
	upc.trade_exchange_code = @"UPC";
	upc.MDLfeature = YES;
	upc.mktDepthLevel = @"10";
	[ssiEx addObject:upc];
	return [ssiEx copy];
}

//- HSX : 3 levels
//- HNX : 10 levels
//- DVT : 10 levels
//- UPC : 10levels
@end

