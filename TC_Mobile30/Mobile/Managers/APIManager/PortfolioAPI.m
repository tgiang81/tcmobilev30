//
//  PortfolioAPI.m
//  TC_Mobile30
//
//  Created by Kaka on 10/24/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PortfolioAPI.h"
#import "UserPrefConstants.h"
#import "UserAccountClientData.h"
#import "BrokerManager.h"
#import "DetailBrokerModel.h"
#import "ParserUtil.h"
#import "AllCommon.h"
#import "PortfolioData.h"
#import "Utils.h"
#import "VertxConnectionManager.h"
#import "PrtfSubDtlRptData.h"
#import "ASIHttpGeneric.h"
#import "HashUtils.h"
#define kURLRequestTimeout      30 // seconds
#define START_OF_TEXT 2
#define END_OF_TEXT 3
@implementation PortfolioAPI
//================== Singleton ==============
+ (PortfolioAPI *)shareInstance{
	static PortfolioAPI *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[PortfolioAPI alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
	}
	return  self;
}

#pragma mark - Main
#pragma mark - Unrealised/Realised
//Get Porfolios: Unrealised or Realised (type: 0-Realised, 1-Unrealisez)
//Completion: NSArray of PortfolioData
- (void)getEquityPortfolioFrom:(UserAccountClientData *)uacd forType:(NSInteger)type completion:(CompletionHandler)handler{
	//Prepare Params
	NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|b=%@|Z=%ld",
						 [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
						 uacd.client_account_number,                //.	bhclicode
						 uacd.broker_code,							//3	broker code
						 uacd.broker_branch_code,                   //M	broker branch code
						 uacd.client_acc_exchange,                  //b client acc exchange code,
						 (long)type
						 ];
	NSString *userParamStr = [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"];
    NSString *paramPortfolio=@"þ=8401";
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradePortfolio?%@|%@", atpServer, userParamStr, postStr,paramPortfolio];
	if ([UserPrefConstants singleton].HTTPSEnabled) {
		urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradePortfolio?%@",
				  atpServer, userParamStr, postStr];
	}
	//urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLHostAllowedCharacterSet];
	[CPService requestDataUsingOldVersionFromUrl:urlStr completion:^(id result, NSError *error) {
		if (error) {
			//Failure
			handler(result, error);
			return ;
		}
		NSString *contentData = (NSString *)result;
		if (!contentData || contentData.length == 0) {
			handler(contentData, error);
			return ;
		}
		//Parse data to model
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			NSArray *responseArr = [[ParserUtil shareInstance] parse_REALISED_PortfoliosDataResponse:contentData];
			dispatch_async(dispatch_get_main_queue(), ^{
				//Get detail of list stock code
				NSArray *stockCodes = [responseArr valueForKeyPath:@"@distinctUnionOfObjects.stockcode"];
				[[VertxConnectionManager singleton]vertxGetStockDetailWithStockArr:stockCodes FIDArr:[[UserPrefConstants singleton]userQuoteScreenColumn] tag:2];
				//Completion
				handler(responseArr, error);
			});
		});
	}];
	
//	NSURLSession *session = [NSURLSession sharedSession];
//	NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlStr] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//		if (!data || error) {
//			//Failure
//			dispatch_async(dispatch_get_main_queue(), ^{
//				handler(data, error);
//			});
//			return ;
//		}
//		//Success
//		NSString* contentData = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//		if (!contentData || contentData.length == 0 || error) {
//			handler(contentData, error);
//			return ;
//		}
//		//Parse data to model
//		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//			NSArray *responseArr = [[ParserUtil shareInstance] parsePortfoliosDataResponse:contentData];
//			dispatch_async(dispatch_get_main_queue(), ^{
//				handler(responseArr, error);
//			});
//		});
//	}];
//	[dataTask resume];
	
	
//	//Call API
//	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
//		NSString *contentData = (NSString *)result;
//		if (!contentData || contentData.length == 0 || error) {
//			handler(result, error);
//			return ;
//		}
//		//Parse data to model
//		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//			NSArray *responseArr = [[ParserUtil shareInstance] parsePortfoliosDataResponse:contentData];
//			dispatch_async(dispatch_get_main_queue(), ^{
//				handler(responseArr, error);
//			});
//		});
//	}];
}
#pragma mark - PF Detail

- (void)getDetailPrtfDtlRpt:(UserAccountClientData *)uacd isDerivative:(BOOL)isDerivative ofSubDtlRpt:(PrtfSubDtlRptData *)psdrd detail_type:(int)detail_type completion:(CompletionHandler)handler{
    ASIHttpGeneric *_asiGeneric = [[ASIHttpGeneric alloc] init];
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|c=%@|e=%@|+=%@|b=%@|g=%d",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.    bhclicode
                         uacd.broker_code,                            //3    broker code
                         uacd.broker_branch_code,                   //M    broker branch code
                         uacd.client_acc_exchange,                  //c client acc exchange code,
                         isDerivative?@"D":@"E",                    //e Market Type.‘E’=Equity or ‘D’=Derivative
                         [Utils stripSymbolFromStockCode:psdrd.stock_code],    //+ stock code
                         [Utils stripExchgCodeFromStockCode:psdrd.stock_code], //b stock exchange
                         detail_type == 0 ? 2 : 1  //g Mode 1-Overnight, 2-Day
                         ];
    
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
    NSString *userParamStr = [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"];
    

    NSString *urlStr = [NSString stringWithFormat:@"http://%@/[0]TradePrtfDtlRpt?10|%c%@|%@%cE",
                        atpServer,
                        START_OF_TEXT,
                        [_asiGeneric base64Encode:userParamStr],
                        [_asiGeneric AESEncryptAndBase64Encode:postStr withKey:[HashUtils generateAESKey]],
                        END_OF_TEXT
                        ];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]TradePrtfDtlRpt?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [_asiGeneric base64Encode:userParamStr],
                  [_asiGeneric AESEncryptAndBase64Encode:postStr withKey:[HashUtils generateAESKey]],
                  END_OF_TEXT
                  ];
    }
    
    //isEncryption = NO
     
//    NSString *urlStr = [UserPrefConstants singleton].HTTPSEnabled?[NSString stringWithFormat:@"https://%@/[%@]TradePrtfDtlRpt?%@",
//                                              atpServer,
//                                              userParamStr,
//                                              postStr
//                                              ]:[NSString stringWithFormat:@"http://%@/[%@]TradePrtfDtlRpt?%@",
//                                                 atpServer,
//                                                 userParamStr,
//                                                 postStr
//                                                 ];
    [CPService requestDataUsingOldVersionFromUrl:urlStr completion:^(id result, NSError *error) {
        if (error) {
            //Failure
            handler(result, error);
            return ;
        }
        NSString *contentData = (NSString *)result;
        if (!contentData || contentData.length == 0) {
            handler(contentData, error);
            return ;
        }
        //Parse data to model
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *responseArr = [[ParserUtil shareInstance] parseDetailPortfolioFrom:contentData];
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(responseArr, error);
            });
        });
    }];
}
#pragma mark - PF Virtual
//Using tradePortfolioSubDetailRpt
//Response: Array of PrtfSubDtlRptData
- (void)getVirtualPortfolioFrom:(UserAccountClientData *)uacd isDerivative:(BOOL)isDerivative completion:(CompletionHandler)handler
{
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|c=%@|e=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.    bhclicode
                         uacd.broker_code,                            //3    broker code
                         uacd.broker_branch_code,                   //M    broker branch code
                         uacd.client_acc_exchange,                  //c client acc exchange code,
                         isDerivative?@"D":@"E"                     //e Market Type.‘E’=Equity or ‘D’=Derivative
                         ];
    
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
    NSString *userParamStr = [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradePrtfSummRpt?%@", atpServer, userParamStr, postStr];
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradePrtfSummRpt?%@",
                  atpServer, userParamStr, postStr];
    }
    [CPService requestDataUsingOldVersionFromUrl:urlStr completion:^(id result, NSError *error) {
        if (error) {
            //Failure
            handler(result, error);
            return ;
        }
        NSString *contentData = (NSString *)result;
        if (!contentData || contentData.length == 0) {
            handler(contentData, error);
            return ;
        }
        //Parse data to model
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *responseArr = [[ParserUtil shareInstance] parseSummaryPortfolioFrom:contentData];
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(responseArr, error);
            });
        });
    }];
}
//+++ Summary PF
//Response: Array of PrtfSummRptData
- (void)getSummaryPortfolioFrom:(UserAccountClientData *)uacd isDerivative:(BOOL)isDerivative completion:(CompletionHandler)handler{
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|c=%@|e=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.    bhclicode
                         uacd.broker_code,                            //3    broker code
                         uacd.broker_branch_code,                   //M    broker branch code
                         uacd.client_acc_exchange,                  //c client acc exchange code,
                         isDerivative?@"D":@"E"                     //e Market Type.‘E’=Equity or ‘D’=Derivative
                         ];
    
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
    NSString *userParamStr = [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradePrtfSubDtlRpt?%@", atpServer, userParamStr, postStr];
    
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradePrtfSubDtlRpt?%@", atpServer, userParamStr, postStr];;
        
    }
    [CPService requestDataUsingOldVersionFromUrl:urlStr completion:^(id result, NSError *error) {
        if (error) {
            //Failure
            handler(result, error);
            return ;
        }
        NSString *contentData = (NSString *)result;
        if (!contentData || contentData.length == 0) {
            handler(contentData, error);
            return ;
        }
        //Parse data to model
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray *responseArr = [[ParserUtil shareInstance] parseVirtualPortfolioFrom:contentData];
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(responseArr, error);
            });
        });
    }];
}
@end
