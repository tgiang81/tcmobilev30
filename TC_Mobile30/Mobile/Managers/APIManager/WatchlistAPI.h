//
//  WatchlistAPI.h
//  TC_Mobile30
//
//  Created by Kaka on 4/2/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"

@interface WatchlistAPI : NSObject{
	
}

//================== Singleton ==============
+ (WatchlistAPI *)shared;

#pragma mark - Main
- (void)getWatchlist:(CompletionHandler)handler;
- (void)deleteWatchlist:(NSInteger)favID completion:(CompletionHandler)handler;
- (void)addOrUpdateWatchlist:(NSInteger)favID name:(NSString *)favName completion:(CompletionHandler)handler;

//Wathlist items - Returns: List of stockcodes
- (void)getWatchlistItemFrom:(NSInteger)favID completion:(CompletionHandler)handler;

//Add stock to Watchlist
- (void)addStock:(NSString *)stockCode inWatchlist:(NSInteger)favID completion:(CompletionHandler)handler;

@end
