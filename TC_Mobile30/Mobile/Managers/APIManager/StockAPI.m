//
//  StockAPI.m
//  TCiPad
//
//  Created by Kaka on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockAPI.h"
#import "AllCommon.h"

@implementation StockAPI
//================== Singleton ==============
+ (StockAPI *)shareInstance{
	static StockAPI *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[StockAPI alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
	}
	return  self;
}

#pragma mark - Main
//======== get list watchlist by faID
- (void) getWatchListItemsFrom:(int)favID completion:(CompletionHandler)handler{
	NSString *postStr = [NSString stringWithFormat:@"*=GetFavListStkCodeCache|+=%@|-=%d", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@",
						   atpServer,
						   [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
						   postStr];
	if ([UserPrefConstants singleton].HTTPSEnabled) {
		urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@",
				  atpServer,
				  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
				  postStr];
	}
}

@end
