//
//  APIManager.h
//  TCiPad
//
//  Created by Kaka on 3/21/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"

@class BrokerModel;
@interface BrokerAPI : NSObject{
	
}
//Singleton
+ (BrokerAPI *)shareInstance;

#pragma mark - Main
- (void)getInitialPlistFile:(CompletionHandler)handler;
//===== Sublist Broker=======
- (void)openSubListBrokersFrom:(BrokerModel *)broker completion:(CompletionHandler)handler;
//===== Get list detail Brokers =======
- (void)getDetailBroker:(BrokerModel *)broker completion:(CompletionHandler)handler;

@end
