//
//  ResearchAPI.h
//  TCiPad
//
//  Created by Kaka on 6/26/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPService.h"

@interface ResearchAPI : NSObject{
	
}
//Singleton
+ (ResearchAPI *)shareInstance;

#pragma mark - Main
- (void)doAllElasticNews:(NSDate *)fromDate toDate:(NSDate *)toDate andSource:(NSString *)source andCategory:(NSString *)category andKeyword:(NSString *)keyword andTargetID:(NSString *)targetID completion:(CompletionHandler)handler;

//Using this to load Research
- (void)requestResearchFrom:(NSDate *)fromDate toDate:(NSDate *)toDate source:(NSString *)source category:(NSString *)category keyword:(NSString *)keyword target:(NSString *)target completion:(CompletionHandler)handler;

//Load Research report for stock
- (void)getResearReportForStock:(NSString *)stockCode fromDate:(NSDate *)fromDate toDate:(NSDate *)toDate source:(NSString *)source category:(NSString *)category keyword:(NSString *)keyword target:(NSString *)target completion:(CompletionHandler)handler;

@end
