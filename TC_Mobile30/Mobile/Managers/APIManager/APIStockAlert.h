//
//  APIStockAlert.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/21/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFBaseRequest.h"
@interface APIStockAlert : AFBaseRequest

//iPad
+ (void) storePushNotificationInfo:(NSString *)accountName
                           andUDID:(NSString *)deviceUDID
                        andExhange:(NSString *)exchange
                    andSponsorCode:(NSString *)sponsorCode
                         andBHCode:(NSString *)bhCode
                          andAppID:(NSString *)appID
                       andBundleId:(NSString *)bundleID
                     andClientCode:(NSString *)clientCode
                    andDeviceToken:(NSString *)deviceToken
                      andIPAddress:(NSString *)ipAddress
                 andActivationCode:(NSString *)activationCode
                   completeHandler:(CompleteHandler)completeHandler;

+ (void) getPublicKeyWithAppURL:(NSString *)url appId:(NSString *)appId bhCode:(NSString *)bhCode completeHandler:(CompleteHandler)completeHandler;

+(NSString *)generateJWTToken:(NSString *)algSring
                   andEString:(NSString *)eString
                 andKidString:(NSString *)kidString
                 andKtyString:(NSString *)ktyString
                   andNString:(NSString *)nString
                     andAppId:(NSString *)appId;

+ (void)getListAlerts:(NSString *)jwtToken bhCode:(NSString *)bhCode appID:(NSString *)appID status:(NSString *)status completeHandler:(CompleteHandler)completeHandler;

+ (void)addAlert:(NSString *)jwtToken bhCode:(NSString *)bhCode appID:(NSString *)appID cond:(NSString *)cond compt:(NSString *)compt alrtt:(NSString *)alrtt mdn:(NSString *)mdn ex:(NSString *)ex rmk:(NSString *)rmk lmt:(NSString *)lmt stkCd:(NSString *)stkCd stkNm:(NSString *)stkNm  completeHandler:(CompleteHandler)completeHandler;

+ (void)updateAlert:(NSString *)jwtToken bhCode:(NSString *)bhCode appID:(NSString *)appID s:(NSString *)s cond:(NSString *)cond compt:(NSString *)compt alrtt:(NSString *)alrtt mdn:(NSString *)mdn rmk:(NSString *)rmk lmt:(NSString *)lmt aid:(NSString *)aid completeHandler:(CompleteHandler)completeHandler;

+ (void)getUid:(NSString *)jwtToken bhCode:(NSString *)bhCode appID:(NSString *)appID completeHandler:(CompleteHandler)completeHandler;

+ (void)getSettingList:(NSString *)jwtToken appId:(NSString *)appId bhCode:(NSString *)bhCode s:(NSString *)s completeHandler:(CompleteHandler)completeHandler;
+ (void)updateSetting:(NSString *)jwtToken appId:(NSString *)appId bhCode:(NSString *)bhCode type:(NSString *)type val:(NSString *)val completeHandler:(CompleteHandler)completeHandler;
@end
