//
//  CPService.h
//  SidebarDemo
//
//  Created by Kaka on 11/8/17.
//  Copyright © 2017 N2N Connect Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSInteger{
	MethodService_Get = 0,
	MethodService_Post,
	MethodService_Put,
	MethodService_Delete
}MethodService;

typedef void (^CompletionHandler)(id result, NSError *error);
typedef void (^ProgressHandler)(NSProgress *progress);

#define BASE_URL    @"http://n2ncardpro.com/" //Production
//#define BASE_URL @"http://superstickbadminton.com/cardpro/" //Dev

@interface CPService : NSObject
+ (NSString*)urlEscapeString:(NSString *)url;

//Main
+ (void)sendRequestWithMethod:(MethodService)method url:(NSString *)url params:(NSDictionary *)params completionHandler:(CompletionHandler)handler;
//LoadData Request return String data
+ (void)requestDataFromURL:(NSString *)url completion:(CompletionHandler)handler;

//======= Post data String  ===========
+ (void)requestPostData:(NSString *)dataSTR url:(NSString *)url completion:(CompletionHandler)handler;

#pragma mark - Request data Using Old Version
+ (void)requestDataUsingOldVersionFromUrl:(NSString *)url completion:(CompletionHandler)handler;
@end
