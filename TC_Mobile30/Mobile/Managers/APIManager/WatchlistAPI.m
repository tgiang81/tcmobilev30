//
//  WatchlistAPI.m
//  TC_Mobile30
//
//  Created by Kaka on 4/2/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "WatchlistAPI.h"
#import "AllCommon.h"
#import "ATPAuthenticate.h"
#import "GCDQueue.h"
#import "ConfigureServerSession.h"
#import "AuthenticationAPI.h"
#import "GTMRegex.h"


@implementation WatchlistAPI
//================== Singleton ==============
+ (WatchlistAPI *)shared{
	static WatchlistAPI *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[WatchlistAPI alloc] init];
	});
	return _instance;
}

#pragma mark - Main
- (void)getWatchlist:(CompletionHandler)handler{
	NSString *postStr = [NSString stringWithFormat:@"*=GetFavListInfo|+=%@|,=1", [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"]];
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
	if ([UserPrefConstants singleton].HTTPSEnabled) {
		urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
	}
	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
		if (!error) {
			[[GCDQueue globalQueue] queueBlock:^{
				NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:result];
				NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
				NSMutableArray *watchlistArr = @[].mutableCopy;
				if(contentArr){
					NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
					[dateFormatter setDateFormat:@"yyyyMMddHHmmss.SSS"];
					for (int i=1; i<contentArr.count; i++)
					{
						NSArray *userWatchListHeaderItem = [contentArr[i] componentsSeparatedByString:@"|"];
						if(userWatchListHeaderItem)
						{
							NSMutableDictionary *userWatchListDict= [[NSMutableDictionary alloc] init];
							[userWatchListDict setObject:userWatchListHeaderItem[1] forKey:@"FavID"];
							[userWatchListDict setObject:userWatchListHeaderItem[2] forKey:@"Name"];
							
							NSDate *timeStampDate = [dateFormatter dateFromString:userWatchListHeaderItem[7]];
							[userWatchListDict setObject:timeStampDate forKey:@"TimeStamp"];
							//[[UserPrefConstants singleton].userWatchListArray addObject:userWatchListDict];
							[watchlistArr addObject:userWatchListDict];
						}
					}
				}
				
				if ([watchlistArr count] > 0) {
					// sort it based on time created
					NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"TimeStamp" ascending:YES];
					NSArray *sortedArr = [watchlistArr sortedArrayUsingDescriptors:@[descriptor]];
					//[UserPrefConstants singleton].userWatchListArray = [sortedArr mutableCopy];
					watchlistArr = [sortedArr mutableCopy];
				}
				[[GCDQueue mainQueue] queueBlock:^{
					handler(watchlistArr.copy, error);
				}];
			}];
		}else{
			handler(result, error);
		}
	}];
}

//Delete
- (void)deleteWatchlist:(NSInteger)favID completion:(CompletionHandler)handler{
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
	NSString *postStr = [NSString stringWithFormat:@"*=DelFavList|+=%@|-=%ld|4=1", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
		handler(result, error);
	}];
}

//Add Or Update
- (void)addOrUpdateWatchlist:(NSInteger)favID name:(NSString *)favName completion:(CompletionHandler)handler{
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"]];
	NSString *postStr = [NSString stringWithFormat:
						 @"Request=*=UpdFavListName|+=%@|-=%ld|.=%@|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101",
						 [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],
						 favID,
						 favName];
	CFStringRef stringRef = CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)postStr, NULL, (CFStringRef)@"!*'\"();:@&=+$,/?%#[]-.", kCFStringEncodingUTF8);
	postStr = (__bridge_transfer NSString *)stringRef;
	
	[CPService requestPostData:postStr url:urlStr completion:^(id result, NSError *error) {
		if (!error) {
			NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:result];
			if (content.length > 0) {
				//Error
				NSDictionary *userInfo = @{
										   NSLocalizedDescriptionKey: [LanguageManager stringForKey:content],
										   NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The response data error.", nil),
										   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Please configuration plist", nil)
										   };
				NSError *aError = [NSError errorWithDomain:@"TradeLogin" code:RESP_CODE_EXCEPTION_ERROR_SERVER userInfo:userInfo];
				handler(content, aError);
			}else{
				//Success
				handler(content, error);
			}
		}
	}];
}

//Items watchlist
- (void)getWatchlistItemFrom:(NSInteger)favID completion:(CompletionHandler)handler{
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
	NSString *postStr = [NSString stringWithFormat:@"*=GetFavListStkCodeCache|+=%@|-=%ld", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@",
			  atpServer,
			  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
			  postStr];
	if ([UserPrefConstants singleton].HTTPSEnabled) {
		urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@",
				  atpServer,
				  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
				  postStr];
	}
	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
		if (!error) {
			[[GCDQueue globalQueue] queueBarrierBlock:^{
				NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:result];
				NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
				NSMutableArray *stockListArr = @[].mutableCopy;
				if(contentArr)
				{
					for (int i=1; i<contentArr.count; i++)
					{
						NSString *cleanStr = [[[contentArr[i] description]
											   stringByReplacingOccurrencesOfString: @")" withString: @""]
											  stringByReplacingOccurrencesOfString: @"=" withString: @""];
						NSArray *userWatchListItem = [cleanStr componentsSeparatedByString:@"|"];
						NSString *stkCodeOnly = userWatchListItem[0];
						NSString *exchangeOnly = userWatchListItem[1];
						
						if ([exchangeOnly length]<=0) exchangeOnly = [UserPrefConstants singleton].userCurrentExchange;
						
						if ([exchangeOnly isEqualToString:@"SI"]) exchangeOnly = @"SG";
						
						NSString *stkCodeFull = [[stkCodeOnly stringByAppendingString:@"."]stringByAppendingString:exchangeOnly];
						if (![stockListArr containsObject:stkCodeFull]) [stockListArr addObject:stkCodeFull];
					}
				}
				[[GCDQueue mainQueue] queueBlock:^{
					handler(stockListArr, error);
				}];
			}];
		}else{
			handler(result, error);
		}
	}];
}

//Add Stock to Watchlist
//Result success is a Dictionary
- (void)addStock:(NSString *)stockCode inWatchlist:(NSInteger)favID completion:(CompletionHandler)handler{
//	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
//	NSString *postStr = [NSString stringWithFormat:@"*=GetFavListStkCodeCache|+=%@|-=%ld", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
// 	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@",
//			  atpServer,
//			  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
//			  postStr];
//	if ([UserPrefConstants singleton].HTTPSEnabled) {
//
//		urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@",
//				  atpServer,
//				  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
//				  postStr];
//	}
//	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
//		if (!error) {
//			NSMutableArray *replyArr = [[NSMutableArray alloc] init];
//			NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:result];
//			NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
//			// NSLog(@"addWatchlistItem Returned atpDecrypted : %@",contentArr);
//
//			/*
//			 1=stkcode
//			 7=return val
//			 8=msg
//			 "(=1|7|8|",
//			 ")=01288|0||"
//
//			 Funny thing about this reply is if same stock added to the list,
//			 it also returns same reply: stkcode|0||.
//
//			 */
//
//			if ([contentArr count]>1) {
//				GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
//				GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
//
//				/// NSLog(@"TradeStatus Reply --------- \n\n %@ \n\n", content);
//
//				int stk_code = -1;             //1
//				int return_val = -1;			//7
//				int return_msg = -1;			//8
//
//				NSString *s;
//				for (s in contentArr) {
//					if ([metaregex matchesString:s]) {
//
//						s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
//						NSArray *tokens = [s componentsSeparatedByString:@"|"];
//						if ([tokens count] > 2) {
//							int pos = 0;
//							for (NSString *token in tokens) {
//								if ([token isEqualToString:@"1"]) {
//									stk_code = pos;
//								}
//								if ([token isEqualToString:@"7"]) {
//									return_val = pos;
//								}
//								if ([token isEqualToString:@"8"]) {
//									return_msg = pos;
//								}
//								pos++;
//							}
//						}
//					} else if ([dataregex matchesString:s]) {
//						s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
//						NSArray *tokens = [s componentsSeparatedByString:@"|"];
//						if ([tokens count] > 2) {
//							NSMutableDictionary *WLReplyDict = [[NSMutableDictionary alloc] init];
//							if (stk_code != -1) {
//								[WLReplyDict setObject:[tokens objectAtIndex:stk_code] forKey:@"1"];
//							}
//							if (return_val != -1) {
//								[WLReplyDict setObject:[tokens objectAtIndex:return_val] forKey:@"7"];
//							}
//							if (return_msg != -1) {
//								[WLReplyDict setObject:[tokens objectAtIndex:return_msg] forKey:@"8"];
//							}
//							[replyArr addObject:WLReplyDict];
//						}
//					}
//				}
//			}
//			NSDictionary *replyDict = [NSDictionary dictionaryWithObject:replyArr forKey:@"data"];
//			handler(replyDict, error);
//		}else{
//			handler(result, error);
//		}
//	}];
	
	NSString *atpServer = [UserPrefConstants singleton].userATPServerIP;
	NSString *postStr = [NSString stringWithFormat:@"*=GetFavListStkCodeCache|+=%@|-=%ld", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], (long)favID];
	NSString *urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
						postStr];
	if ([UserPrefConstants singleton].HTTPSEnabled) {
		urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@",
				  atpServer,
				  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
				  postStr];
	}
	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
		if (!error) {
			NSString *content = [[ATPAuthenticate singleton] atpDecryptionProcess:result];
			NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
			if(contentArr){
				__block NSMutableArray *listArr = [[NSMutableArray alloc] init];
				for (int i=1; i<contentArr.count; i++){
					NSString *cleanStr = [[[contentArr[i] description]
										   stringByReplacingOccurrencesOfString: @")" withString: @""]
										  stringByReplacingOccurrencesOfString: @"=" withString: @""];
					NSArray *userWatchListItem = [cleanStr componentsSeparatedByString:@"|"];
					NSString *stkCodeOnly = userWatchListItem[0];
					NSString *exchangeOnly = userWatchListItem[1];
					
					if ([exchangeOnly length]<=0) exchangeOnly = [[UserPrefConstants singleton].userCurrentExchange copy];
					
					if ([exchangeOnly isEqualToString:@"SI"]) exchangeOnly = @"SG";
					
					NSString *stkCodeFull = [[stkCodeOnly stringByAppendingString:@"."]stringByAppendingString:exchangeOnly];
					[listArr addObject:stkCodeFull];
				}
				
				NSMutableArray *addingStocks = [[stockCode componentsSeparatedByString:@","] mutableCopy];
				NSMutableArray *duplicateStocks = [[NSMutableArray alloc] init];
				for (int i=0; i<[addingStocks count]; i++) {
					NSString *stk = [addingStocks objectAtIndex:i];
					if ([listArr containsObject:stk]) {
						[addingStocks removeObject:stk];
						[duplicateStocks addObject:stk];
					}
				}
				NSDictionary *replyDict = @{@"data": duplicateStocks};
				dispatch_async(dispatch_get_main_queue(), ^{
					handler(replyDict, error);
				});
				return ;
			}
			handler(result, error);
		}
	}];
}
@end
