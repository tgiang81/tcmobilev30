//
//  UserSession.h
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AllCommon.h"
@class SectorInfo;
@interface UserSession : NSObject{
	
}
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *lastUserId;
@property (assign, nonatomic) LanguageType languageType;
@property (assign, nonatomic) BOOL isAcceptDislaimerNote;
@property (assign, nonatomic) BOOL isRememberMe;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *password;

@property(assign, nonatomic) BOOL didLogin;

//Singleton
+ (UserSession *)shareInstance;

#pragma mark - Main
- (void)save;
- (void)clearSessionData;
- (void)restoreSessionIfNeeded;

@end
