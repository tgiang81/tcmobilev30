//
//  ConfigureServerSession.m
//  TCiPad
//
//  Created by Kaka on 3/24/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ConfigureServerSession.h"
#import "BrokerManager.h"
#import "DetailBrokerModel.h"

@implementation ConfigureServerSession
#pragma mark - Singleton
//================== Singleton ==============
+ (ConfigureServerSession *)shareInstance{
	static ConfigureServerSession *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[ConfigureServerSession alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
		_atpPublicIP = nil;
		_atpPrivateIP = nil;
		_atpPort = nil;
		_atpServer = nil;
		_currentSessionKey = nil;
	}
	return  self;
}

#pragma mark - Main
//======== use this for calling api ============
- (NSString *)getATPServerIP{
	NSString *fullATPServer = @"";
	if (!_atpPublicIP) {
		fullATPServer = _atpPublicIP;
		if (!_atpPort) {
			fullATPServer = [NSString stringWithFormat:@"%@:%@", fullATPServer, _atpPort];
		}
	}else{
		fullATPServer = [BrokerManager shareInstance].detailBroker.ATPServer;
	}
	return fullATPServer;
}
@end
