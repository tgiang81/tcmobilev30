//
//  ConfigureServerSession.h
//  TCiPad
//
//  Created by Kaka on 3/24/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigureServerSession : NSObject{
	
}
@property (strong, nonatomic) NSString *atpPublicIP;
@property (strong, nonatomic) NSString *atpPrivateIP;
@property (strong, nonatomic) NSString *atpPort;
@property (strong, nonatomic) NSString *atpServer;

//Session key
@property (strong, nonatomic) NSString *currentSessionKey;

//Use this to get atpKey2
@property (strong, nonatomic) NSString *exponent_key;

//Singleton
+ (ConfigureServerSession *)shareInstance;

#pragma mark - Main
- (NSString *)getATPServerIP;

@end
