//
//  UserSession.m
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "UserSession.h"
#import "NSObject+Extension.h"
#import "UserPrefConstants.h"
#import "NetworkManager.h"

//======= Key for Session ======================
#define kSessionKey				@"kSessionKey"
#define kSessionVersion			@"kSessionVersionKey"


#define kTIME_APP_RESIGN_KEY    @"kTIME_APP_RESIGN_KEY"

@interface UserSession(){
	NSDate *timeAppGotoBackground;
}
@end
@implementation UserSession
//================== Singleton ==============
+ (UserSession *)shareInstance{
	static UserSession *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[UserSession alloc] init];
	});
	return _instance;
}

- (instancetype)init{
	self = [super init];
	if (self) {
		//Custom data here
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appGotoBackground:) name:UIApplicationWillResignActiveNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doKickOutNotification:) name:@"doKickout" object:nil];
		[self restoreSessionIfNeeded];
	}
	return  self;
}
//Save
- (void)save{
	NSDictionary *allDataTmp = [self propertiesDictionary];
	NSMutableDictionary *allData = [[NSMutableDictionary alloc] initWithDictionary:allDataTmp];
	
	for (id key in[allDataTmp allKeys]) {
		id value = [allDataTmp objectForKey:key];
		if (!([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]])) {
			[allData removeObjectForKey:key];
		}
	}
	
	NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSessionKey, kSessionVersion];
	[[NSUserDefaults standardUserDefaults] setObject:allData forKey:sessionKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

//Restore
- (void)restoreSessionIfNeeded {
	NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSessionKey, kSessionVersion];
	NSDictionary *allData = [[NSUserDefaults standardUserDefaults] objectForKey:sessionKey];
	
	if (allData && ![allData isEqual:[NSNull null]]) {
		//[self initDefaultData];
		NSArray *keyArray =  [allData allKeys];
		NSUInteger count = [keyArray count];
		for (int i = 0; i < count; i++) {
			NSString *key = [keyArray objectAtIndex:i];
			id obj = [allData objectForKey:key];
			if ([self respondsToSelector:NSSelectorFromString([keyArray objectAtIndex:i])]) {
				[self setValue:obj forKey:[keyArray objectAtIndex:i]];
			}
		}
	}
	else {
		[self initDefaultData];
	}
	[self save];
}

//Default data
- (void)initDefaultData{
	_token = nil;
	_lastUserId = nil;
	_languageType = LanguageType_EN;
	_isAcceptDislaimerNote = NO;
	_isRememberMe = NO;
	_userName = nil;
	_password = nil;
	_isRememberMe = NO;
	//Check user login or not
	_didLogin = NO;
}

//Clear session
- (void)clearSessionData {
	[self initDefaultData];
	NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSessionKey, kSessionVersion];
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:sessionKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[self save];
}

//Print data
- (void)printDescription {
	NSString *sessionKey = [NSString stringWithFormat:@"%@_%@", kSessionKey, kSessionVersion];
	NSDictionary *allData = [[NSUserDefaults standardUserDefaults] objectForKey:sessionKey];
	DLog(@"%@", allData);
}

#pragma mark - APP Notification
- (void)appGotoBackground:(NSNotification *)noti{
	timeAppGotoBackground = [NSDate date];
	//No need to cache this value
	//[USER_DEFAULT setObject:timeAppGotoBackground forKey:kTIME_APP_RESIGN_KEY];
	//[USER_DEFAULT synchronize];
}

- (void)appEnterForeground:(NSNotification *)noti{
	//Detect user was logged in or not
	if (_didLogin == NO) {
		return;
	}
	int distanceTimeFromBackgroundAppToNow = [[NSDate date] timeIntervalSinceDate:timeAppGotoBackground];
	NSInteger _timeOutApp = [UserPrefConstants singleton].sessionTimeoutSeconds;
	if (distanceTimeFromBackgroundAppToNow > _timeOutApp) { //
		[[NetworkManager shared] stopCheckingSigleSignOn];
		[AppDelegateAccessor showContinueLogin];
	}
}

#pragma mark - Handle Notification
- (void)doKickOutNotification:(NSNotification *)noti{
	if ([UserSession shareInstance].didLogin == NO) {
		return;
	}
	dispatch_async(dispatch_get_main_queue(), ^{
		DLog(@"DoKICKOUT: %@", noti);
		NSString *message = noti.userInfo[@"message"];
		[[NetworkManager shared] stopCheckingSigleSignOn];
		if (!message || message.length == 0) {
			message = [LanguageManager stringForKey:@"System detect you just login to another device and this session will be stopped."];
		}
		[AppDelegateAccessor doKickOut:message];
	});
}
@end
