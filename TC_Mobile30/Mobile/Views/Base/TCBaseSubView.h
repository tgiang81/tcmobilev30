//
//  TCBaseSubView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppMacro.h"
@interface TCBaseSubView : UIView
- (void)setupView;
@end
