//
//  TCBaseView.m
//  TCiPad
//
//  Created by Kaka on 5/3/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseView.h"

@implementation TCBaseView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib{
	[super awakeFromNib];
	self.layer.masksToBounds = YES;
}
@end
