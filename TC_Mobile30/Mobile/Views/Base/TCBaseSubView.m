//
//  TCBaseSubView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

@implementation TCBaseSubView
{
    UIView *contentView;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        [self customInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        [self customInit];
    }
    return self;
}

- (void)customInit{
    contentView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil][0];
    
    [self addSubview:contentView];
    contentView.frame = self.bounds;
    contentView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                    UIViewAutoresizingFlexibleHeight);
    contentView.translatesAutoresizingMaskIntoConstraints = YES;
    [self setupView];
}
- (void)setupView{
    
}
@end
