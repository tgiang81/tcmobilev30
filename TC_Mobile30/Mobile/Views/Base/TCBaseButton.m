//
//  TCBaseButton.m
//  TCiPad
//
//  Created by Kaka on 5/3/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseButton.h"

@implementation TCBaseButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib{
	[super awakeFromNib];
	self.layer.masksToBounds = YES;
}

- (void)layoutSubviews{
	[super layoutSubviews];
	//[self setShowsTouchWhenHighlighted:YES];
}

#pragma mark - Custom Override
- (void)setEnabled:(BOOL)enabled{
	self.alpha = enabled? 1.0 : 0.3;
	[self setUserInteractionEnabled:enabled];
}

@end
