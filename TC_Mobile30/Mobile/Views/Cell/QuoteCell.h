//
//  QuoteCell.h
//  TCiPad
//
//  Created by Kaka on 3/29/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class QuoteCell;
@class StockModel;
@class PADesignableButton;
@class TCChartView;

//============ State Cell =================
typedef NS_ENUM (NSInteger, CellState) {
	CellState_Expanded,
	CellState_Collapsed
};

@protocol QuoteCellDelegate<NSObject>
@optional
- (void)doubleTap:(QuoteCell *)cell;
- (void)checkExpandCell:(QuoteCell *)cell isExpanded:(BOOL)isExpanded;
@end
@interface QuoteCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UIButton *btnArrow;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (assign, nonatomic) CellState state;

//Properties Label
@property (weak, nonatomic) IBOutlet UILabel *lblStName;
@property (weak, nonatomic) IBOutlet UILabel *lblStTradeValue;
@property (weak, nonatomic) IBOutlet UILabel *lblChange;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice; //Using ref price
@property (weak, nonatomic) IBOutlet UIView *topContent;

//For Chart
@property (weak, nonatomic) IBOutlet TCChartView *chartView;


@property (weak, nonatomic) id <QuoteCellDelegate> delegate;

//====== Animate =================
- (void)setAnimateUpArrow:(BOOL)isUp;
//======= Using temp stock to update
//- (void)setupData:(StockModel *)newStock oldStock:(StockModel *)oldStock indexPath:(NSIndexPath *)indexPath expand:(BOOL)isExpanded;

//======= Using change value to update stock =======================
- (void)setupUIFromModel:(StockModel *)model atIndexPath:(NSIndexPath *)indexPath expand:(BOOL)expand;
//========== Just control data by Model =========
- (void)updateData:(StockModel *)model useBlinkIfNeed:(BOOL)isUsedBlink;
@end
