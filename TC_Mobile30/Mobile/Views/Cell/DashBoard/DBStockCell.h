//
//  DBStockCell.h
//  TC_Mobile30
//
//  Created by Michael Seven on 10/30/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface DBStockCell : BaseTableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblStockContent;
@property (weak, nonatomic) IBOutlet UILabel *lblValueContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTradesContent;
@property (weak, nonatomic) IBOutlet UILabel *lblChangeContent;
@property (weak, nonatomic) IBOutlet UILabel *lblCloseContent;

//Passing Data
@property (strong, nonatomic) StockModel *stock;

@end
