//
//  DBStockCell.m
//  TC_Mobile30
//
//  Created by Michael Seven on 10/30/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "DBStockCell.h"
#import "StockModel.h"

@implementation DBStockCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setupDataFrom:(StockModel *)model {
    if(model) {
        _stock = model;
        _lblStockContent.text = model.stockName;
        _lblValueContent.text = [model.volume stringValue];
        _lblTradesContent.text = [model.trade stringValue];
        _lblChangeContent.text = [model.changePer stringValue];
        _lblCloseContent.text = [model.dClose stringValue];
    }
}

@end
