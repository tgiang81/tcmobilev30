//
//  AlertCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@class AlertCell;
@class AlertModel;
@protocol AlertCellDelegate <NSObject>
- (void)didSelectDelete:(AlertCell *)cell;
- (void)didSelectActive:(AlertCell *)cell;
- (void)didSelectEdit:(AlertCell *)cell;
@end
@interface AlertCell : BaseTableViewCell
@property(weak, nonatomic) id<AlertCellDelegate>delegate;
- (void)setupData:(AlertModel *)model;
@end
