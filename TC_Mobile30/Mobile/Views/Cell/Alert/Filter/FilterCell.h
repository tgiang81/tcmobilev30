//
//  FilterCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
@class FilterCell;
@protocol FilterCellDelegate <NSObject>
- (void)didSelect:(FilterCell *)cell;
@end
@interface FilterCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn_action;
@property (weak, nonatomic) IBOutlet UILabel *lbl_content;
@property (weak, nonatomic) IBOutlet UIImageView *img_state;
@property (weak, nonatomic) id<FilterCellDelegate> delegate;
- (void)changeState:(BOOL)isSelected;
- (void)setContent:(NSString *)content;
@end
