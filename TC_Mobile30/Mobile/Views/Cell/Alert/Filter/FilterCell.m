//
//  FilterCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setContent:(NSString *)content{
    self.lbl_content.text = content;
}
- (void)changeState:(BOOL)isSelected{
    if(isSelected){
        self.lbl_content.textColor = COLOR_FROM_HEX(0x00BAD8);
    }else{
        self.lbl_content.textColor = COLOR_FROM_HEX(0x658497);
    }
}
- (IBAction)action:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelect:)]){
        [self.delegate didSelect:self];
    }
}



@end
