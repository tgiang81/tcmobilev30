//
//  SettingAlertCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "SettingAlertModel.h"
NS_ASSUME_NONNULL_BEGIN
@class SettingAlertCell;
@protocol SettingAlertCellDelegate<NSObject>
- (void)didChangeState:(BOOL)value onCell:(SettingAlertCell *)cell;
@end
@interface SettingAlertCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_SubTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Bottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Leading;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content;
@property (weak, nonatomic) IBOutlet UISwitch *sw_State;
@property (weak, nonatomic) id<SettingAlertCellDelegate> delegate;
- (void)setupModelForCell:(SettingAlertModel *)model;
- (void)setupTitle:(NSString *)title andIsOn:(BOOL)isOn;
- (void)setupTitle:(NSString *)title subTitle:(NSString *)subTitle andIsOn:(BOOL)isOn;
- (void)setHightLightCell:(BOOL)isHightLight;
@end

NS_ASSUME_NONNULL_END
