//
//  SettingAlertCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SettingAlertCell.h"

@implementation SettingAlertCell
{
    CGFloat _leftMargin;
    CGFloat _levelMargin;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    _leftMargin = 0;
    _levelMargin = 16;
    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_bgCell);
    self.lbl_SubTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_textColor);
    self.lbl_Content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_textColor);
    self.sw_State.onTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_switchTintColor);
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setHightLightCell:(BOOL)isHightLight{
    if(isHightLight){
        self.lbl_SubTitle.alpha = 1;
        self.lbl_Content.alpha = 1;
        self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_bgHightLightCell);
    }else{
        self.lbl_SubTitle.alpha = 0.6;
        self.lbl_Content.alpha = 0.6;
        self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_bgCell);
    }
}
- (void)setupTitle:(NSString *)title andIsOn:(BOOL)isOn{
    NSString *subTitle = @"I want to receive Push Notifications on this device";
    if(!isOn){
        subTitle = @"I don’t want to receive Push Notifications on this device";
    }
    self.cst_Bottom.constant = 8;
    [self setupTitle:title subTitle:subTitle andIsOn:isOn];
}
- (void)setupTitle:(NSString *)title subTitle:(NSString *)subTitle andIsOn:(BOOL)isOn{
    self.lbl_SubTitle.text = subTitle;
    self.lbl_Content.text = title;
    [self.sw_State setOn:isOn];
}
- (void)setupModelForCell:(SettingAlertModel *)model{
    self.cst_Bottom.constant = 0;
    self.lbl_Content.text = [model getName];
    [self.sw_State setOn:[model checkEnable]];
    self.cst_Leading.constant = (model.level * _levelMargin) + _leftMargin;
}
- (IBAction)changeValue:(UISwitch *)sender {
    if([self.delegate respondsToSelector:@selector(didChangeState:onCell:)]){
        [self.delegate didChangeState:sender.isOn onCell:self];
    }
}

@end
