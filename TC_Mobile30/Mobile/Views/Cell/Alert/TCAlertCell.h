//
//  TCAlertCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MGSwipeTableCell.h"
@class AlertModel;
@class TCAlertCell;
@protocol TCAlertCellDelegate<NSObject>
- (void)didSelectChangeValue:(BOOL)on at:(TCAlertCell *)cell;
@end
@interface TCAlertCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Value;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AlertType;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AlertTypeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ConditionTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbl_condition;
@property (weak, nonatomic) IBOutlet UISwitch *s_ChangeStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ValueTitle;



@property (weak, nonatomic) id<TCAlertCellDelegate>delegateSetting;
- (void)setupView:(AlertModel *)model;
@end
