//
//  TCAlertCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCAlertCell.h"
#import "AlertModel.h"
#import "UITableViewCell+Utils.h"
#import "Utils.h"
#import "NSString+Util.h"
@implementation TCAlertCell
{
    CGFloat activedAlpha;
    CGFloat deactivedAlpha;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    activedAlpha = 1.0;
    deactivedAlpha = 0.5;
    [self showFullSeperator];
    // Initialization code
    //setColor
    self.lbl_AlertTypeTitle.text = [LanguageManager stringForKey:@"Alert Type:"];
    self.lbl_ConditionTitle.text = [LanguageManager stringForKey:@"Alert when value is:"];
    self.lbl_ValueTitle.text = [LanguageManager stringForKey:@"Value:"];
    
    self.lbl_Name.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellTextColor);
    self.lbl_condition.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellTextColor);
    self.lbl_Value.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellTextColor);
    self.lbl_ValueTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellTextColor);
    self.lbl_ConditionTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellTextColor);
    self.lbl_AlertTypeTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellTextColor);
    self.lbl_AlertType.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellTextColor);
    
    self.s_ChangeStatus.onTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellSwitchTint);
    [self.contentView setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_cellBg)];
    
}
- (void)setupTextColor:(BOOL)isOn{
    if(isOn){
        self.lbl_Name.alpha = activedAlpha;
        self.lbl_condition.alpha = activedAlpha;
        self.lbl_Value.alpha = activedAlpha;
        self.lbl_ValueTitle.alpha = activedAlpha;
        self.lbl_ConditionTitle.alpha = activedAlpha;
        self.lbl_AlertTypeTitle.alpha = activedAlpha;
        self.lbl_AlertType.alpha = activedAlpha;
    }else{
        self.lbl_Name.alpha = deactivedAlpha;
        self.lbl_condition.alpha = deactivedAlpha;
        self.lbl_Value.alpha = deactivedAlpha;
        self.lbl_ValueTitle.alpha = deactivedAlpha;
        self.lbl_ConditionTitle.alpha = deactivedAlpha;
        self.lbl_AlertTypeTitle.alpha = deactivedAlpha;
        self.lbl_AlertType.alpha = deactivedAlpha;
    }
    
}
- (void)setupView:(AlertModel *)model{
    self.lbl_Name.text = [NSString stringWithFormat:@"%@ (%@)", model.stkNm, [Utils stripSymbolFromStockCode:model.stkCd]];
    self.lbl_condition.text = [model getConditionString];
    self.lbl_Value.text = [NSString stringWithFormat:@"%@%@", @"",model.lmt];
    self.lbl_AlertType.text = [AlertModel getAlrtName:model.alrtt];
    [self.s_ChangeStatus setOn:[model.s isEqualToString:@"A"]];
    [self setupTextColor:[model.s isEqualToString:@"A"]];
    
}
- (IBAction)changeStatus:(UISwitch *)sender {
    if([self.delegateSetting respondsToSelector:@selector(didSelectChangeValue:at:)]){
        [self.delegateSetting didSelectChangeValue:sender.isOn at:self];
    }
}

@end
