//
//  AlertStockCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface AlertStockCell : BaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *searchResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *searchResultLabel2;
@end

NS_ASSUME_NONNULL_END
