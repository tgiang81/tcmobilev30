//
//  AlertCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AlertCell.h"
#import "AlertModel.h"
#import "UITableViewCell+Utils.h"
@implementation AlertCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_viewBg);
    [self showFullSeperator];
    // Initialization code
}
- (IBAction)delete:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectDelete:)]){
        [self.delegate didSelectDelete:self];
    }
}
- (IBAction)active:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectActive:)]){
        [self.delegate didSelectActive:self];
    }
}
- (IBAction)edit:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectEdit:)]){
        [self.delegate didSelectEdit:self];
    }
}

- (void)setupData:(AlertModel *)model{
    
}
@end
