//
//  CommonLeftMenuCell.m
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "CommonLeftMenuCell.h"
#import "Utils.h"

#define kNormalColor  		RGB(68, 91, 110)
#define kHighlightColor  	AppColor_TintColor

@implementation CommonLeftMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	_lblContent.font = AppFont_MainFontMediumWithSize(12);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)loadContent:(NSString *)content{
	_lblContent.text = content;
}

- (void)loadContent:(NSString *)content atCellType:(MenuContentType)cellType withHighlight:(BOOL)isHighlight{
	_lblContent.text = [Utils commonMenuTitleBy:cellType];
	_imgIcon.image = [UIImage imageNamed:[self iconImageNamedAtType:cellType highlighted:isHighlight]];
	if (isHighlight) {
		_highlightView.backgroundColor = [UIColor whiteColor];
		_lblContent.textColor = kHighlightColor;
		_imgArrow.image = [UIImage imageNamed:@"arrow_highlight_icon"];
	}else{
		_highlightView.backgroundColor = kNormalColor;
		_lblContent.textColor = kNormalColor;
		_imgArrow.image = [UIImage imageNamed:@"arrow_blue_icon"];
	}
}

#pragma mark - UTILS
- (NSString *)iconImageNamedAtType:(MenuContentType)cellType highlighted:(BOOL)highlight{
	NSString *iconName = @"";
	switch (cellType) {
		case MenuContentType_Login:{
			//Hard code: Login icon
			iconName = highlight? @"announcement_highlight_icon" : @"announcement_icon";
		}
			break;
		case MenuContentType_AboutUs:{
			iconName = highlight? @"about_highlight_icon" : @"about_icon";
		}
			break;
		case MenuContentType_TermAndCondition:{
			iconName = highlight? @"term_highlight_icon" : @"term_icon";
		}
			break;
		case MenuContentType_Announcement:{
			iconName = highlight? @"announcement_highlight_icon" : @"announcement_icon";
		}
			break;
		case MenuContentType_ForgotPassword:{
			iconName = highlight? @"key_highlight_icon" : @"key_icon";
		}
			break;
			
		case MenuContentType_Setting:{
			iconName = highlight? @"setting_highlight_icon" : @"setting_icon";
		}
			break;
		case MenuContentType_Tutorial:{
			iconName = highlight? @"tutorial_highlight_icon" : @"tutorial_icon";
		}
			break;
			
		default:
			iconName = highlight? @"key_highlight_icon" : @"key_icon";
			break;
	}
	return iconName;
}
@end
