//
//  NewsCell.m
//  TCiPad
//
//  Created by Kaka on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "NewsCell.h"
#import "NewsModel.h"
@implementation NewsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setupUIByModel:(NewsModel *)model{
	if(model){
		_lblTitle.text = model.title;
		_lblExchange.text = model.exchange;
		_lblTimeStamp.text = model.timeStamp;
	}
}

#pragma mark - Setup
- (void)setup{
	self.backgroundColor = AppColor_MainBackgroundCellColor;
	//Setup font and color below
}
@end
