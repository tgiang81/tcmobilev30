//
//  WatchlistCell.h
//  TCiPad
//
//  Created by Kaka on 5/31/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MGSwipeTableCell.h"
#import "AllCommon.h"
@class WatchlistModel;

@interface WatchlistCell : MGSwipeTableCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

//Optional: Passing data
@property (assign, nonatomic) ScreenType screenType;

//Static Class Function
+ (NSString *)reuseIdentifier;
+ (NSString *)nibName;
//Main
- (void)setupDataFrom:(WatchlistModel *)model withType:(ScreenType)type;

@end
