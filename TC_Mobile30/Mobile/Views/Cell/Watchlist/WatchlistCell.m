//
//  WatchlistCell.m
//  TCiPad
//
//  Created by Kaka on 5/31/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "WatchlistCell.h"
#import "WatchlistModel.h"

@implementation WatchlistCell

//Singleton
+ (NSString *)reuseIdentifier {
	return NSStringFromClass([self class]);
}

+ (NSString *)nibName{
	return NSStringFromClass([self class]);
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - Main
- (void)setup{
	self.backgroundColor = [UIColor clearColor];
	self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)updateTheme{
	self.backgroundColor = [UIColor clearColor];//TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	_lblTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_LabelTextColor);
}

//Passing data
- (void)setupDataFrom:(WatchlistModel *)model withType:(ScreenType)type{
	[self updateTheme];
	//Parse data
	_lblTitle.text = model.Name;
	self.screenType = type;
}

#pragma mark - UTILS
- (void)setScreenType:(ScreenType)screenType{
	[self iconImageFromType:screenType];
	_screenType = screenType;
}
- (void)iconImageFromType:(ScreenType)screenType{
	switch (screenType) {
		case ScreenType_Normal:{
			_imgIcon.hidden = YES;
		}
			break;
		case ScreenType_Rename:{
			_imgIcon.hidden = NO;
			_imgIcon.image = [UIImage imageNamed:@"more_rename_icon"];
		}
			break;
		case ScreenType_Delete:{
			_imgIcon.hidden = NO;
			_imgIcon.image = [UIImage imageNamed:@"trash_icon"];
		}
			break;
		default:
			break;
	}
}
@end
