//
//  NewsCell.h
//  TCiPad
//
//  Created by Kaka on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class NewsModel;

@interface NewsCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblExchange;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeStamp;

#pragma mark - Main
- (void)setupUIByModel:(NewsModel *)model;
@end
