//
//  SkipContainerCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/31/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
@class AroundShadowView;
NS_ASSUME_NONNULL_BEGIN

@interface SkipContainerCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Height;
@property (weak, nonatomic) IBOutlet UIView *v_Content;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@property (nonatomic, strong) NSMutableArray *detailArray;
+ (NSInteger)numberOfLines:(NSArray *)detailArray;
@end

NS_ASSUME_NONNULL_END
