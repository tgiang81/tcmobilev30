//
//  SkipContainerCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/31/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "SkipContainerCell.h"
#import "AroundShadowView.h"
#import "ConfirmTableViewIPhoneCell.h"
#import "MarketInfoCell.h"
#import "UILabel+TCUtil.h"
@interface SkipContainerCell() <UITableViewDelegate, UITableViewDataSource>

@end
@implementation SkipContainerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.tbl_Content registerNib:[UINib nibWithNibName:[MarketInfoCell nibName] bundle:nil] forCellReuseIdentifier:[MarketInfoCell reuseIdentifier]];
    [self.tbl_Content registerNib:[UINib nibWithNibName:@"ConfirmTableViewIPhoneCell" bundle:nil] forCellReuseIdentifier:@"ConfirmTableViewIPhoneCell"];
    
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    self.tbl_Content.estimatedRowHeight = 30;
    self.v_Content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellBg);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        if(indexPath.row%2 == 0){
            cell.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_bgCell1);
        }else{
            cell.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_bgCell2);
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [SkipContainerCell numberOfLines:self.detailArray];
}
+ (NSInteger)numberOfLines:(NSArray *)detailArray{
    return [detailArray count];
//    if([detailArray count]%2==0){
//        return [detailArray count]/2;
//    }else{
//        return ([detailArray count]/2) + 1;
//    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ConfirmTableViewIPhoneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConfirmTableViewIPhoneCell" forIndexPath:indexPath];
    
    NSDictionary *dict = [self.detailArray objectAtIndex:indexPath.row];
//    NSDictionary *dict2;
//    if(indexPath.row*2 < self.detailArray.count-1){
//        dict2 = [self.detailArray objectAtIndex:(indexPath.row*2)+1];
//    }
    NSString *title = [[dict allKeys] objectAtIndex:0];
    NSString *contentStr1 = [dict objectForKey:title];
    if ([contentStr1 length]<=0) {
        contentStr1 = @"-";
    }
    cell.dataTitle.text = title;
    cell.dataContent.text = contentStr1;
//    [cell.dataTitle setAttributeForLabel:title content:contentStr1];
//    if(dict2){
//        NSString *title2 = [[dict2 allKeys] objectAtIndex:0];
//        NSString *contentStr2 = [dict2 objectForKey:title2];
//        if ([contentStr2 length]<=0) {
//            contentStr2 = @"-";
//        }
//        [cell.dataTitle2 setAttributeForLabel:title2 content:contentStr2];
//        cell.dataTitle2.text = [NSString stringWithFormat:@"%@ %@", title2, contentStr2];
//        cell.dataTitle2.text = title2;
//        cell.DataContent2.text = contentStr2;
//    }else{
//        [cell.dataTitle2 setAttributeForLabel:@"" content:@""];
//        cell.DataContent2.text = @"";
//    }
    return cell;
    
}
@end
