//
//  ConfirmTableViewIPhoneCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/31/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "ConfirmTableViewIPhoneCell.h"
#import "ThemeManager.h"
#import "AppMacro.h"
@implementation ConfirmTableViewIPhoneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.dataTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_hightLightTextColor);
    self.dataContent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_textColor);
    self.dataTitle2.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_hightLightTextColor);
    self.DataContent2.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_textColor);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
