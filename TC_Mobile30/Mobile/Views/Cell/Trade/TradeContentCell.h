//
//  TradeContentCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/18/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface TradeContentCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIView *v_Content;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ContentHeight;

@end

NS_ASSUME_NONNULL_END
