//
//  CommonLeftMenuCell.h
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface CommonLeftMenuCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIView *highlightView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

//MAIN
- (void)loadContent:(NSString *)content;
- (void)loadContent:(NSString *)content atCellType:(MenuContentType)cellType withHighlight:(BOOL)isHighlight;

@end
