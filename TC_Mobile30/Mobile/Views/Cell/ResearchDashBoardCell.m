//
//  ResearchDashBoardCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "ResearchDashBoardCell.h"
#import "NewsModel.h"
#import "NewsMetadata.h"
#import "NSDate+Utilities.h"
#import "DashBoardCornerView.h"
#import "Utils.h"
@implementation ResearchDashBoardCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
    _lblTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
    _lblContent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
    _lblExchange.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
    _lblDate.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
    
    [self setup];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setup{
    [self setupColor];
}
- (void)setupColor{
    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_NewsBgCell);
    self.v_Main.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_NewsBgCell);
}
//Main
- (void)setupDataFrom:(id)model{
    [self setupColor];
    [self getSubTitle];
    if ([model isKindOfClass:[NewsModel class]]) {
        //For NewsModel
        NewsModel *newsModel = (NewsModel *)model;
        _lblTitle.text = [self getTitle:newsModel.exchange];
        _lblContent.text = newsModel.title;
        _lblExchange.text = [self getSubTitle];
        NSDate *date = [NSDate dateFromString:newsModel.timeStamp format:kSERVER_NEWS_FORMAT_DATE];
        _lblDate.text = [NSString stringWithFormat:@"%@%@ %@", [self getOnlyDayFromServerDate:date],[date daySuffixForDate], [self getDateString:date]];
        
        
    }
    else if([model isKindOfClass:[NewsMetadata class]]){
        NewsMetadata *newsModel = (NewsMetadata *)model;
        _lblTitle.text = [self getTitle:newsModel.exchange];
        _lblContent.text = newsModel.newsTitle;
        _lblExchange.text = [self getSubTitle];
        
        //Date & time
        NSDate *date = [NSDate dateFromString:newsModel.date1 format:kSERVER_Research_FORMAT_DATE];
        _lblDate.text = [NSString stringWithFormat:@"%@%@ %@", [self getOnlyDayFromServerDate:date],[date daySuffixForDate], [self getDateString:date]];
    }
    else{
        //For ResearchModel - Demo
        _lblTitle.text = @"Research TITLE";
        _lblExchange.text = @"Exchange TEST";
    }
}
- (NSString *)getTitle:(NSString *)content{
    if([content isEqualToString:[LanguageManager stringForKey:@"Announcement"]] || [content isEqualToString:@""]){
        return @"General News";
    }
    return [content capitalizedString];
}
- (NSString *)getSubTitle{
    NSString *code = ([UserPrefConstants singleton].userCurrentExchange.length>2)?([[UserPrefConstants singleton].userCurrentExchange substringToIndex:2]):([UserPrefConstants singleton].userCurrentExchange);
    return [Utils getShortExChangeName:[Utils getExchange:code]];
}
#pragma mark - UTILS
- (NSString *)getDateString:(NSDate *)svDate{
    return [svDate stringWithFormat:@"MMMM yyyy HH:mm:ss"];
}
- (NSString *)getOnlyDayFromServerDate:(NSDate *)svDate{
    return [svDate stringWithFormat:@"dd"];
}

@end
