//
//  StockStreamerCell.m
//  TCiPad
//
//  Created by n2nconnect on 03/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockStreamerCell.h"
#import "MStockTrackerModel.h"
#import "NSNumber+Formatter.h"
#import "ThemeManager.h"
#import "SettingManager.h"
#import "UITableViewCell+Utils.h"
@implementation StockStreamerCell


@synthesize lblStkName;
@synthesize lblCondition;
@synthesize lblPrice;
@synthesize lblQuantity;
@synthesize lblValue;
@synthesize lblTime;
@synthesize lblChange;
@synthesize lblChangePercentage;
@synthesize animationView;
- (void)setupView:(MStockTrackerModel *)model isAnimation:(BOOL)isAnimation{
    NSString *price = [[NSNumber numberWithFloat:model.price] toCurrencyNumber];
    NSString *changeValueString =  [NSNumber numberFormat2D:model.changeValue];
    [self updateContent:[model getName] andPI:model.status andPrice:price andQuantity:model.quantity andValue:model.value andTime:[model getDateString] andChange:changeValueString andChangePercentage:model.changeValuePercentageString andChangeValue:model.changeValue isAnimation:[self needAnimation:model]];
}
- (void) updateContent:(NSString *)stockName
                 andPI:(NSString *)pi
              andPrice:(NSString *)price
           andQuantity:(NSString *)quantity
              andValue:(NSString *)value
               andTime:(NSString *)time
             andChange:(NSString *)change
   andChangePercentage:(NSString *)changePercentage
        andChangeValue:(float)changeValue
           isAnimation:(BOOL)isAnimation
{
    
    if(isAnimation){
        [UIView animateWithDuration:0.5f animations:^{
            if(changeValue>0){
                [self.animationView setBackgroundColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
            }else if(changeValue<0){
                [self.animationView setBackgroundColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
            }else{
                [self.animationView setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgContentCellColor)];
            }
            
            [self.animationView setAlpha:0.5f];
            
        } completion:^(BOOL finished) {
            
            //fade out
            [UIView animateWithDuration:0.5f animations:^{
                [self.animationView setBackgroundColor:[UIColor clearColor]];
                [self.animationView setAlpha:0.0f];
                
            } completion:nil];
            
        }];
    }
    
    
    
    if(changeValue>0){
        [lblCondition setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
//        [lblTime setTextColor:[UIColor greenColor]];
        [lblPrice  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
        [lblChange  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
        [lblChangePercentage  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
    }
    else if (changeValue<0){
        [lblCondition setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
//        [lblTime setTextColor:[UIColor redColor]];
        [lblPrice  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
        [lblChange  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
        [lblChangePercentage  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
    }
    else{
        [lblCondition setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
//        [lblTime setTextColor:[UIColor orangeColor]];
        [lblPrice  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
        [lblChange  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
        [lblChangePercentage  setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
    }
    
    
    [lblStkName setText:stockName];
    [lblCondition setText:pi];
    [lblPrice setText:price];
    [lblQuantity setText:quantity];
    [lblValue setText:value];
    [lblTime setText:time];
    [lblChange setText:change];
    [lblChangePercentage setText:changePercentage];

    // Add blink effect to this
}




- (void)awakeFromNib {
    [super awakeFromNib];
    //self.contentView.backgroundColor = UIColorFromRGB(0x2E3E49);
    [self createUI];
    [self showFullSeperator];
    // Initialization code
}

- (void)createUI{
//    self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgCellColor);
    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgContentCellColor);
    lblStkName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textCellColor);
    lblPrice.textColor = lblStkName.textColor;
    lblCondition.textColor = lblStkName.textColor;
    lblQuantity.textColor = lblStkName.textColor;
    lblChangePercentage.textColor = lblStkName.textColor;
    lblTime.textColor = lblStkName.textColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)needAnimation:(MStockTrackerModel *)model{
    if(![lblStkName.text isEqualToString:[model getName]]){
        return YES;
    }else{
        return ![lblChangePercentage.text isEqualToString:model.changeValuePercentageString];
    }
}
@end
