//
//  StockStreamerCell.h
//  TCiPad
//
//  Created by n2nconnect on 03/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MStockTrackerModel;
@interface StockStreamerCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *lblStkName;
@property (weak, nonatomic) IBOutlet UILabel *lblCondition;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblChange;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePercentage;
@property (weak, nonatomic) IBOutlet UIView *animationView;
@property (weak, nonatomic) IBOutlet UIView *contentViewCell;

- (void) updateContent:(NSString *)stockName
                 andPI:(NSString *)pi
              andPrice:(NSString *)price
           andQuantity:(NSString *)quantity
              andValue:(NSString *)value
               andTime:(NSString *)time
             andChange:(NSString *)change
   andChangePercentage:(NSString *)changePercentage
        andChangeValue:(float)changeValue
           isAnimation:(BOOL)isAnimation;
- (void)setupView:(MStockTrackerModel *)model isAnimation:(BOOL)isAnimation;
@end
