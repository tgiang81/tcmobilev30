//
//  BaseTableViewCell.m
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "UIView+Animation.h"

@implementation BaseTableViewCell
//Singleton
+ (NSString *)reuseIdentifier {
	return NSStringFromClass([self class]);
}

+ (NSString *)nibName{
	return NSStringFromClass([self class]);
}
//=======================************================================
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
//Should override this function to update UI with data
- (void)setupDataFrom:(id)model{
	//do nothing here
}

#pragma mark - Animation
- (void)makeAnimateBy:(StockModel *)oldModel newStock:(StockModel *)newStock completion:(void(^)(void))completion{
	[self.contentView makeAnimateBy:oldModel newStock:newStock completion:^{
		if (completion) {
			completion();
		}
	}];
}
@end
