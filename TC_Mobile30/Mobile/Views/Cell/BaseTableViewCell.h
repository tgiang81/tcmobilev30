//
//  BaseTableViewCell.h
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"

@interface BaseTableViewCell : UITableViewCell{
	
}

//Reuse IDCEll
+ (NSString *)reuseIdentifier;
+ (NSString *)nibName;

#pragma mark - Main to Override
- (void)setupDataFrom:(id)model;
#pragma mark - Animation
- (void)makeAnimateBy:(StockModel *)oldModel newStock:(StockModel *)newStock completion:(void(^)(void))completion;
@end
