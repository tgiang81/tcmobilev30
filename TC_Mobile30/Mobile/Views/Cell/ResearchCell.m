//
//  ResearchCell.m
//  TCiPad
//
//  Created by Kaka on 6/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ResearchCell.h"
#import "NSDate+Utilities.h"
#import "NewsModel.h"
#import "NewsMetadata.h"
#import "Utils.h"
#import "TCBaseView.h"
#import "UIView+ShadowBorder.h"

#define kDATE_CELL_FORMAT	@"dd'th' MMM yyyy, HH:mm:ss"
@implementation ResearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - MAIN
- (void)setup{
	self.viewContent.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	_lblTitle.font = AppFont_MainFontRegularWithSize(12);
	_lblExchange.font = AppFont_MainFontBoldWithSize(12);
	_lblDateTime.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblExchange.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	[self.viewContent makeBorderShadow];
}

//Main
- (void)setupDataFrom:(id)model{
	self.viewContent.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	[self.viewContent makeBorderShadow];
	if ([model isKindOfClass:[NewsModel class]]) {
		//For NewsModel
		NewsModel *newsModel = (NewsModel *)model;
		_lblTitle.text = newsModel.title;
		_lblExchange.text = newsModel.exchange;
		
		//Date time
		NSDate *date = [NSDate dateFromString:newsModel.timeStamp format:kSERVER_NEWS_FORMAT_DATE];
		_lblDateTime.text = [date stringWithFormat:kDATE_CELL_FORMAT];
		//Date & time
		/*
		_lblDay.text = [self getOnlyDayFromServerDate:newsModel.timeStamp format:kSERVER_NEWS_FORMAT_DATE];
		_lblMonth.text = [self getOnlyMonthFromServerDate:newsModel.timeStamp format:kSERVER_NEWS_FORMAT_DATE];
		_lblYear.text = [self getOnlyYearFromServerDate:newsModel.timeStamp format:kSERVER_NEWS_FORMAT_DATE];
		*/
    }else if([model isKindOfClass:[NewsMetadata class]]){
        NewsMetadata *newsModel = (NewsMetadata *)model;
        
        _lblTitle.text = newsModel.newsTitle;
        _lblExchange.text = [Utils stripExchgCodeFromStockCode:newsModel.stockcode];
		_lblDateTime.text = newsModel.fullDate;
		
        //Date & time
		NSDate *date = [NSDate dateFromString:newsModel.date1 format:kSERVER_Research_FORMAT_DATE];
		_lblDateTime.text = [date stringWithFormat:kDATE_CELL_FORMAT];
		/*
        _lblDay.text = [self getOnlyDayFromServerDate:newsModel.date1 format:kSERVER_Research_FORMAT_DATE];
        _lblMonth.text = [self getOnlyMonthFromServerDate:newsModel.date1 format:kSERVER_Research_FORMAT_DATE];
        _lblYear.text = [self getOnlyYearFromServerDate:newsModel.date1 format:kSERVER_Research_FORMAT_DATE];
		*/
    }else{
		//For ResearchModel - Demo
		_lblTitle.text = @"Research TITLE";
		_lblExchange.text = @"Exchange TEST";
	}
}

#pragma mark - UTILS
- (NSString *)getOnlyYearFromServerDate:(NSString *)svDate format:(NSString *)format{
	NSDate *date = [NSDate dateFromString:svDate format:format];
	return [date stringWithFormat:@"yyyy"];
}
- (NSString *)getOnlyMonthFromServerDate:(NSString *)svDate format:(NSString *)format{
	NSDate *date = [NSDate dateFromString:svDate format:format];
	return [date stringWithFormat:@"MMM"];
}
- (NSString *)getOnlyDayFromServerDate:(NSString *)svDate format:(NSString *)format{
	NSDate *date = [NSDate dateFromString:svDate format:format];
	return [date stringWithFormat:@"dd"];
}
@end
