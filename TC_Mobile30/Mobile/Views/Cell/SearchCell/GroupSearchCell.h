//
//  GroupSearchCell.h
//  TC_Mobile30
//
//  Created by Kaka on 11/15/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface GroupSearchCell : BaseTableViewCell{
	
}

//MAIN
- (void)updateUIFrom:(StockModel *)model;

@end
