//
//  GroupSearchCell.m
//  TC_Mobile30
//
//  Created by Kaka on 11/15/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "GroupSearchCell.h"
@interface GroupSearchCell(){
	
}
//OUTLET
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCode;

@end

@implementation GroupSearchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - MAIN
- (void)updateUIFrom:(StockModel *)model{
	[self updateTheme];
	if (model) {
		if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
			_lblName.text = model.compName;
			_lblCode.text = model.stockName;//[model.stockCode stringByDeletingPathExtension];
		}
		_lblName.text = model.stockName;
		_lblCode.text = model.stockCode;//[model.stockCode stringByDeletingPathExtension];
	}
}

- (void)updateTheme{
	self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	_lblName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
}
@end
