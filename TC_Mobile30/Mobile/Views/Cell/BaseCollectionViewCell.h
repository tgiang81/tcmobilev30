//
//  BaseCollectionViewCell.h
//  TCiPad
//
//  Created by Kaka on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"
#import "UILabel+FormattedText.h"
#import "StockModel.h"
#import "NSNumber+Formatter.h"
#import "UIView+Animation.h"
#import "MarqueeLabel.h"

@interface BaseCollectionViewCell : UICollectionViewCell{
	
}
//@property (weak, nonatomic) IBOutlet MarqueeLabel *lblMarketName;
//@property (weak, nonatomic) IBOutlet MarqueeLabel *lblNameOrCode;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
//@property (weak, nonatomic) IBOutlet UILabel *lblUpdownPercent;
//
//@property (weak, nonatomic) IBOutlet MarqueeLabel *lblVolume;
//
//
//@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblName;
//@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//@property (weak, nonatomic) IBOutlet UILabel *lblPercentUpDownPrice;
//@property (weak, nonatomic) IBOutlet UILabel *lblUpDownPrice;

//Passing data
@property (assign, nonatomic) BOOL isExpandedCell;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) StockModel *stock;

//Reuse IDCEll
+ (NSString *)reuseIdentifier;
+ (NSString *)nibName;

#pragma mark - Animation
- (void)makeAnimateBy:(StockModel *)oldModel newStock:(StockModel *)newStock completion:(void(^)(void))completion;

//make white all label into view
- (void)setAllWhiteColorTextLabelFrom:(UIView *)contentView;

@end
