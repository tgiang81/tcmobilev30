//
//  MStockCollectionViewCell.h
//  TCiPad
//
//  Created by ngo phi long on 4/27/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MStockCollectionViewCell : UICollectionViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *value1;
@property (weak, nonatomic) IBOutlet UILabel *value2;
@property (weak, nonatomic) IBOutlet UILabel *value3;

@end
