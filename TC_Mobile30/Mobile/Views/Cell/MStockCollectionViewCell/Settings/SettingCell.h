//
//  SettingCell.h
//  TCiPad
//
//  Created by Kaka on 6/15/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface SettingCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

//Main
- (void)setupDataFrom:(SettingType)type;
- (void)setDataShouldHighlight:(BOOL)isHighlight;

@end
