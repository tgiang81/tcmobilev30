//
//  SettingCell.m
//  TCiPad
//
//  Created by Kaka on 6/15/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "SettingCell.h"

@implementation SettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setup{
	_lblTitle.font = AppFont_MainFontMediumWithSize(12);
}

//Update Data
- (void)setupDataFrom:(SettingType)type{
	_lblTitle.text = [self titleFrom:type];
	if (type == SettingType_Unknown) {
		_lblTitle.hidden = YES;
		_imgArrow.hidden = YES;
		self.backgroundColor = COLOR_FROM_HEX(0xE6E7E8);
	}else{
		_lblTitle.hidden = NO;
		_imgArrow.hidden = NO;
		self.backgroundColor = COLOR_FROM_HEX(0xFFFFFF);
	}
	[self setDataShouldHighlight:NO];
}

- (void)setDataShouldHighlight:(BOOL)isHighlight{
	_lblTitle.textColor = isHighlight ? AppColor_HighlightTitleColor : AppColor_NormalTitleColor;
	_imgArrow.highlighted = isHighlight;
}

#pragma mark - UTILS
- (NSString *)titleFrom:(SettingType )type{
	NSString *_title = @"";
	switch (type) {
		case SettingType_ChangePassword:
			_title = [LanguageManager stringForKey:@"Change Password"];
			break;
		case SettingType_ChangePin:
			_title = [LanguageManager stringForKey:@"Change Pin"];
			break;
		case SettingType_ForgotPin:
			_title = [LanguageManager stringForKey:@"Forgot Pin"];
			break;
		case SettingType_ActiveTouchID:
			_title = [LanguageManager stringForKey:@"Active TouchID"];
			break;
		case SettingType_PushNotification:
			_title = [LanguageManager stringForKey:@"Push Notification"];
			break;
		case SettingType_RefreshRate:
			_title = [LanguageManager stringForKey:@"Refresh Rate"];
			break;
		case SettingType_ViewPreference:
			_title = [LanguageManager stringForKey:@"View Preferences"];
			break;
		case SettingType_AboutUs:
			_title = [LanguageManager stringForKey:@"About Us"];
			break;
		case SettingType_ContactUs:
			_title = [LanguageManager stringForKey:@"Contact Us"];
			break;
		case SettingType_RateUs:
			_title = [LanguageManager stringForKey:@"Rate Us"];
			break;
		case SettingType_TermOfService:
			_title = [LanguageManager stringForKey:@"Term Of Service"];
			break;
		case SettingType_PrivacyPolicy:
			_title = [LanguageManager stringForKey:@"Privacy Policy"];
			break;
		case SettingType_DisclaimerOfWarranty:
			_title = [LanguageManager stringForKey:@"Disclaimer Of Warranty"];
			break;
		case SettingType_Notification:
			_title = [LanguageManager stringForKey:@"Notification"];
			break;
		default:
			break;
	}
	return _title;
}
@end
