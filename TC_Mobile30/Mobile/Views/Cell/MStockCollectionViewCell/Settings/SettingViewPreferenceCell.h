//
//  SettingViewPreferenceCell.h
//  TCiPad
//
//  Created by Kaka on 6/18/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface SettingViewPreferenceCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (void)setupDataFrom:(TabbarItemView)itemView highlight:(BOOL)isHighlight;

@end
