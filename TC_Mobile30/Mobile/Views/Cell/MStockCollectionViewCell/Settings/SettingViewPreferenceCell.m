//
//  SettingViewPreferenceCell.m
//  TCiPad
//
//  Created by Kaka on 6/18/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "SettingViewPreferenceCell.h"
#import "LanguageKey.h"

@implementation SettingViewPreferenceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - Main
//Update Data
- (void)setupDataFrom:(TabbarItemView)itemView highlight:(BOOL)isHighlight{
	_lblTitle.text = [self titleFrom:itemView];
	[self setDataShouldHighlight:isHighlight];
}

- (void)setDataShouldHighlight:(BOOL)isHighlight{
	_lblTitle.textColor = isHighlight ? AppColor_HighlightTitleColor : AppColor_NormalTitleColor;
}

#pragma mark - UTILS
- (NSString *)titleFrom:(TabbarItemView)itemView{
	NSString *title = @"";
	switch (itemView) {
		case TabbarItemView_OrderBook:
			title = [LanguageManager stringForKey:@"Order Book"];
			break;
		case TabbarItemView_Quote:
			title = [LanguageManager stringForKey:@"Quotes"];
			break;
		case TabbarItemView_Home:
			title = [LanguageManager stringForKey:@"Home"];
			break;
		default:
			break;
	}
	return title;
}
@end
