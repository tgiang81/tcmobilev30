//
//  SetupButtonCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "SetupButtonCell.h"

@implementation SetupButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)layoutSubviews{
	[super layoutSubviews];
	[self setupButton:self.btn_Action];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)actionCell:(id)sender {
	self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
    if([self.delegate respondsToSelector:@selector(didSelectSetupButton:)]){
        [self.delegate didSelectSetupButton:self];
    }
}
- (void)setupButton:(TCBaseButton *)button{
	NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
    button.borderColor = TC_COLOR_FROM_HEX(mainTintColor);
    [button setTitleColor:TC_COLOR_FROM_HEX(mainTintColor) forState:UIControlStateNormal];
}
@end
