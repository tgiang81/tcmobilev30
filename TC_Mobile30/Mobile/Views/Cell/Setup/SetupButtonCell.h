//
//  SetupButtonCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "TCBaseButton.h"
NS_ASSUME_NONNULL_BEGIN
@protocol SetupButtonCellDelegate<NSObject>
- (void)didSelectSetupButton:(UITableViewCell *)cell;
@end
@interface SetupButtonCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet TCBaseButton *btn_Action;
@property (weak, nonatomic) id<SetupButtonCellDelegate> delegate;

- (void)setupButton:(TCBaseButton *)button;

@end

NS_ASSUME_NONNULL_END
