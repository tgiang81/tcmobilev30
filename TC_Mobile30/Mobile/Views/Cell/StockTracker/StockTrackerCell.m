//
//  StockTrackerCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "StockTrackerCell.h"
#import "MStockTrackerModel.h"
#import "NSNumber+Formatter.h"
#import "MarqueeLabel.h"
#import "ThemeManager.h"
#import "SettingManager.h"
@implementation StockTrackerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.contentView setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgCellColor)];
    self.contentView.layer.borderColor = [TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_borderCellColor) CGColor];
    [_lbl_Name setTextColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textCellColor)];
    [_lbl_Volume setTextColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textCellColor)];
    [_lbl_sbh setTextColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textCellColor)];
    [_lbl_bbh setTextColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textCellColor)];
    [_lbl_Time setTextColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textCellColor)];
    
    
    self.contentView.layer.borderWidth = 1;
    self.contentView.layer.cornerRadius = 4;
    self.isShowBBHAndSBH = NO;
    // Initialization code
}
- (void)setIsShowBBHAndSBH:(BOOL)isShowBBHAndSBH{
    _isShowBBHAndSBH = isShowBBHAndSBH;
    if(isShowBBHAndSBH == NO){
        _lbl_sbh.text = @"";
        _lbl_bbh.text = @"";
    }
}

- (void)setupView:(MStockTrackerModel *)model isAnimation:(BOOL)isAnimation{
    NSString *status = model.status;
    if([self needAnimation:model]){
        [UIView animateWithDuration:0.5f animations:^{
            
            if(model.changeValue>0){
                [self.animationView setBackgroundColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
            }else if(model.changeValue<0){
                [self.animationView setBackgroundColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
            }else{
                [self.animationView setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgCellColor)];
            }
            
            
//            if([status isEqualToString:@"b"]){
//                [self.animationView setBackgroundColor:[UIColor greenColor]];
//            }else if([status isEqualToString:@"s"]){
//                [self.animationView setBackgroundColor:[UIColor redColor]];
//            }else{
//                [self.animationView setBackgroundColor:[UIColor orangeColor]];
//            }
            [self.animationView setAlpha:0.5f];
            
        } completion:^(BOOL finished) {
            
            //fade out
            [UIView animateWithDuration:0.5f animations:^{
                [self.animationView setBackgroundColor:[UIColor clearColor]];
                [self.animationView setAlpha:0.0f];
                
            } completion:nil];
            
        }];
    }
    
    
    if(model.changeValue>0){
        [_lbl_Price setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
    }
    else if (model.changeValue<0){
        [_lbl_Price setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
    }
    else{
        [_lbl_Price setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
    }
    
    self.lbl_Name.text = [model getName];
    if(self.isShowBBHAndSBH){
        self.lbl_bbh.text = model.bbh;
        self.lbl_sbh.text = model.sbh;
    }
    
    self.lbl_Price.text = [[NSNumber numberWithFloat:model.price] toCurrencyNumber];
    self.lbl_Time.text = [model getDateString];
    self.lbl_Volume.text = model.quantity;
    self.changeValue = model.changeValuePercentageString;
}

- (BOOL)needAnimation:(MStockTrackerModel *)model{
    if(![self.lbl_Name.text isEqualToString:[model getName]]){
        return YES;
    }else{
        return ![self.changeValue isEqualToString:model.changeValuePercentageString];
    }
}
@end
