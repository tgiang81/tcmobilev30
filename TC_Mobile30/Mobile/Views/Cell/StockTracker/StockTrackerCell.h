//
//  StockTrackerCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MStockTrackerModel;
@class MarqueeLabel;
@interface StockTrackerCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet MarqueeLabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_bbh;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sbh;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Volume;
@property (weak, nonatomic) IBOutlet UIView *animationView;
@property (strong, nonatomic) NSString *changeValue;
@property (assign, nonatomic) BOOL isShowBBHAndSBH;
- (void)setupView:(MStockTrackerModel *)model isAnimation:(BOOL)isAnimation;

@end
