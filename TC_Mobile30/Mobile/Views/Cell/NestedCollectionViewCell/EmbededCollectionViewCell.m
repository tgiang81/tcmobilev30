//
//  EmbededCollectionViewCell.m
//  TCiPad
//
//  Created by Kaka on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "EmbededCollectionViewCell.h"
#import "BaseCollectionView.h"
#import "QuoteCollectionViewCell.h"

//For New Type Cell
#import "StockTypeTwoCell.h"
#import "StockTypeThreeCell.h"

#import "StockIndicesCell.h"


@implementation EmbededCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)layoutSubviews{
	[super layoutSubviews];
	self.collectionView.frame = self.contentView.bounds;
	self.collectionView.backgroundColor = [UIColor clearColor];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withScrollDirection: (UICollectionViewScrollDirection)direction{
	if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
	
//	UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//	//layout.sectionInset = UIEdgeInsetsMake(1, 1, 1, 1);
//	//layout.itemSize = CGSizeMake(44, 44);
//	layout.scrollDirection = direction;
//	self.collectionView = [[BaseCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
//	//Register Cell here
//	[self.collectionView registerNib:[UINib nibWithNibName:[QuoteCollectionViewCell nibName] bundle:nil] forCellWithReuseIdentifier:[QuoteCollectionViewCell reuseIdentifier]];
//	[self.collectionView registerNib:[UINib nibWithNibName:[StockIndicesCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockIndicesCell reuseIdentifier]];
//	//For New Type
//	[self.collectionView registerNib:[UINib nibWithNibName:[StockTypeOneCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockTypeOneCell reuseIdentifier]];
//	[self.collectionView registerNib:[UINib nibWithNibName:[StockTypeTwoCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockTypeTwoCell reuseIdentifier]];
//	[self.collectionView registerNib:[UINib nibWithNibName:[StockTypeThreeCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockTypeThreeCell reuseIdentifier]];
//	self.collectionView.showsHorizontalScrollIndicator = NO;
//	//Color
//	self.collectionView.backgroundColor = [UIColor clearColor];
//	self.backgroundColor = [UIColor clearColor];
//	//self.contentView.backgroundColor = [UIColor clearColor];
//	//self.backgroundColor = [UIColor clearColor];
//	if (direction == UICollectionViewScrollDirectionVertical) {
//		self.collectionView.scrollEnabled = NO;
//	}else{
//		self.collectionView.scrollEnabled = YES;
//	}
	[self initCollectionViewWithDirection:direction];
	return self;
}

#pragma mark - init with ScrollView
- (void)initCollectionViewWithDirection:(UICollectionViewScrollDirection)direction{
	UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
	self.collectionView = [[BaseCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
	//Register Cell here
	[self.collectionView registerNib:[UINib nibWithNibName:[QuoteCollectionViewCell nibName] bundle:nil] forCellWithReuseIdentifier:[QuoteCollectionViewCell reuseIdentifier]];
	[self.collectionView registerNib:[UINib nibWithNibName:[StockIndicesCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockIndicesCell reuseIdentifier]];
	//For New Type
	[self.collectionView registerNib:[UINib nibWithNibName:[StockTypeTwoCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockTypeTwoCell reuseIdentifier]];
	[self.collectionView registerNib:[UINib nibWithNibName:[StockTypeThreeCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockTypeThreeCell reuseIdentifier]];
	self.collectionView.showsHorizontalScrollIndicator = NO;
	if (direction == UICollectionViewScrollDirectionVertical) {
		self.collectionView.scrollEnabled = NO;
	}else{
		self.collectionView.scrollEnabled = YES;
	}
	[self.contentView addSubview:self.collectionView];
}

#pragma mark - Main Delegate
- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath{
	self.collectionView.dataSource = dataSourceDelegate;
	self.collectionView.delegate = dataSourceDelegate;
	self.collectionView.indexPath = indexPath;
	[self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
	[self.collectionView reloadData];
}

@end
