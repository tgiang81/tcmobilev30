//
//  EmbededCollectionViewCell.h
//  TCiPad
//
//  Created by Kaka on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class BaseCollectionView;
@interface EmbededCollectionViewCell : BaseTableViewCell{
	
}
@property (nonatomic, strong) BaseCollectionView *collectionView;

//==== Main initial =======
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withScrollDirection: (UICollectionViewScrollDirection)direction;

- (void)initCollectionViewWithDirection:(UICollectionViewScrollDirection)direction;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
