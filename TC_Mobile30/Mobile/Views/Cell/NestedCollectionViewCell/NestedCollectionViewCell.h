//
//  NestedCollectionViewCell.h
//  TCiPad
//
//  Created by Kaka on 8/2/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "BaseCollectionView.h"

@interface NestedCollectionViewCell : BaseTableViewCell{
	
}
@property (nonatomic, strong) BaseCollectionView *collectionView;

#pragma mark - Main
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withScrollDirection: (UICollectionViewScrollDirection)direction;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
