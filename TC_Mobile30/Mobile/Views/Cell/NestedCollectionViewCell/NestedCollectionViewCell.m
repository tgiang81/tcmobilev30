//
//  NestedCollectionViewCell.m
//  TCiPad
//
//  Created by Kaka on 8/2/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "NestedCollectionViewCell.h"

//For Cell
#import "StockIndicesCell.h"
#import "StockTypeTwoCell.h"
#import "StockTypeThreeCell.h"
#import "StockHeatmapCell.h"

@implementation NestedCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)layoutSubviews{
	[super layoutSubviews];
	self.collectionView.frame = self.bounds;
	[self.collectionView layoutIfNeeded];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withScrollDirection: (UICollectionViewScrollDirection)direction{
	if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
	
	UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
	//layout.sectionInset = UIEdgeInsetsMake(1, 1, 1, 1);
	//layout.itemSize = CGSizeMake(44, 44);
	layout.scrollDirection = direction;
	[layout invalidateLayout];
	self.collectionView = [[BaseCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
	[self addSubview:self.collectionView];
	//[self implementConstrain];
	
	//Register Cell
	[self.collectionView registerNib:[UINib nibWithNibName:[StockIndicesCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockIndicesCell reuseIdentifier]];
	//For New Type
	[self.collectionView registerNib:[UINib nibWithNibName:[StockTypeTwoCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockTypeTwoCell reuseIdentifier]];
	[self.collectionView registerNib:[UINib nibWithNibName:[StockTypeThreeCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockTypeThreeCell reuseIdentifier]];
	[self.collectionView registerNib:[UINib nibWithNibName:[StockHeatmapCell nibName] bundle:nil] forCellWithReuseIdentifier:[StockHeatmapCell reuseIdentifier]];
	self.collectionView.showsHorizontalScrollIndicator = NO;
	//Color
	self.collectionView.backgroundColor = [UIColor clearColor];
	self.backgroundColor = [UIColor clearColor];
	if (direction == UICollectionViewScrollDirectionVertical) {
		self.collectionView.scrollEnabled = NO;
	}else{
		self.collectionView.scrollEnabled = YES;
	}
	return self;
}

#pragma mark - UTILS
- (void)implementConstrain{
	//Add Constrain
	NSMutableArray *_constrains = @[].mutableCopy;
	[self.collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
	NSLayoutConstraint *topAnchor = [self.collectionView.topAnchor constraintEqualToAnchor:self.topAnchor];
	[_constrains addObject:topAnchor];
	NSLayoutConstraint *bottomAnchor = [self.collectionView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:0];
	[_constrains addObject:bottomAnchor];
	NSLayoutConstraint *leadintAnchor = [self.collectionView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:0];
	[_constrains addObject:leadintAnchor];
	NSLayoutConstraint *trailingAnchor = [self.collectionView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:0];
	[_constrains addObject:trailingAnchor];
	[NSLayoutConstraint activateConstraints:_constrains];
}

#pragma mark - Main Delegate
- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath{
	self.collectionView.dataSource = dataSourceDelegate;
	self.collectionView.delegate = dataSourceDelegate;
	self.collectionView.indexPath = indexPath;
	[self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
	[self.collectionView reloadData];
}
@end
