//
//  DetailBrokerCell.m
//  TCiPad
//
//  Created by Kaka on 8/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DetailBrokerCell.h"
#import "BrokerModel.h"
#import "TCBaseImageView.h"

@implementation DetailBrokerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setup{
	_lblBrokerName.textColor = RGB(66, 90, 108);
	_lblBrokerName.font = AppFont_MainFontMediumWithSize(12);
	_imgSelection.layer.borderColor = RGB(66, 90, 108).CGColor;
	_imgSelection.backgroundColor = RGB(66, 90, 108);
}
//#1: Can use this to set data by model
- (void)setupDataFrom:(BrokerModel *)model shoudlSelected:(BOOL)selected{
	if (model) {
		_imgBrokerIcon.image = [UIImage imageNamed:[model.iconName stringByDeletingPathExtension]];
		_lblBrokerName.text =  model.appName;
		[self shouldSelectedCell:selected];
	}
}

- (void)shouldSelectedCell:(BOOL)selected{
	if (selected) {
		_lblBrokerName.textColor = AppColor_TintColor;
		_imgSelection.backgroundColor = AppColor_TintColor;
	}else{
		_lblBrokerName.textColor = RGB(66, 90, 108);
		_imgSelection.backgroundColor = RGB(66, 90, 108);
	}
}
@end
