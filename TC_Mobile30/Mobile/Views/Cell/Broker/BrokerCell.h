//
//  BrokerCell.h
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class BrokerModel;
@class TCBaseImageView;
@interface BrokerCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UIImageView *imgBrokerIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblBrokerName;
@property (weak, nonatomic) IBOutlet TCBaseImageView *imgArrow;

//setup
- (void)setupDataFrom:(BrokerModel *)model;
- (void)shouldHighlight:(BOOL)highlight;

@end
