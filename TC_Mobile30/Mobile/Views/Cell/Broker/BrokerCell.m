//
//  BrokerCell.m
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BrokerCell.h"
#import "BrokerModel.h"
#import "TCBaseImageView.h"
#import "Utils.h"

@implementation BrokerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setup{
	_lblBrokerName.textColor = RGB(66, 90, 108);
	_lblBrokerName.font = AppFont_MainFontMediumWithSize(12);
}
//#1: Can use this to set data by model
- (void)setupDataFrom:(BrokerModel *)model{
	if (model) {
		_imgBrokerIcon.image = [UIImage imageNamed:[model.iconName stringByDeletingPathExtension]];
		_lblBrokerName.text =  model.appName;
		_imgArrow.hidden = ![Utils isMultiInBroker:model];
	}
}

//#2: Can use this to set data by properties
- (void)setupDataFrom:(NSString *)brokerIcon brokerName:(NSString *)name{
	//update data here
}

- (void)shouldHighlight:(BOOL)highlight{
	if (highlight) {
		_lblBrokerName.textColor = AppColor_TintColor;
		_imgArrow.highlighted = YES;
	}else{
		_lblBrokerName.textColor = RGB(66, 90, 108);
		_imgArrow.highlighted = NO;
	}
}

#pragma mark - Actions

- (IBAction)goBack:(id)sender {
}
@end
