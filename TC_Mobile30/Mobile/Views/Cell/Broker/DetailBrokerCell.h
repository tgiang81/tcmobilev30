//
//  DetailBrokerCell.h
//  TCiPad
//
//  Created by Kaka on 8/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class TCBaseImageView;
@class BrokerModel;
@interface DetailBrokerCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UIImageView *imgBrokerIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblBrokerName;
@property (weak, nonatomic) IBOutlet TCBaseImageView *imgSelection;

//MAIN
- (void)setupDataFrom:(BrokerModel *)model shoudlSelected:(BOOL)selected;
- (void)shouldSelectedCell:(BOOL)selected;

@end
