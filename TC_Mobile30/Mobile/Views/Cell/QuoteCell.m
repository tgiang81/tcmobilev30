//
//  TCChartView.h
//  TCiPad
//
//  Created by Kaka on 04/04/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "QuoteCell.h"
#import "StockModel.h"
#import "TCChartView.h"
@interface QuoteCell()<TCChartViewDelegate>{
    
}
@end
@implementation QuoteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	_lblPrice.layer.backgroundColor = [UIColor clearColor].CGColor;
	_lblChange.layer.backgroundColor = [UIColor clearColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
/* +++ no need use temp array to check whatever stock changes
- (void)setupData:(StockModel *)newStock oldStock:(StockModel *)oldStock indexPath:(NSIndexPath *)indexPath expand:(BOOL)isExpanded{
	_indexPath = indexPath;
	self.state = isExpanded ? CellState_Expanded : CellState_Collapsed;
	if (newStock) {
		_lblStName.text = newStock.stockName;
		_lblChange.text = newStock.fChange;
		_lblPrice.text = [newStock.dRefPrice stringValue];
		_chartView.stCode = newStock.stockCode;
		//Check UI
		if (oldStock) {
			//Check UI for animate blink
			UIColor *targetColor = [UIColor clearColor];
			if (newStock.fChange.doubleValue > 0) {
				targetColor = kGrnFlash;
			}else if (newStock.fChange.longLongValue < 0){
				targetColor = kRedFlash;
			}else{
				targetColor = [UIColor lightGrayColor];
			}
			[self animateBlinkView:_lblPrice toColor:targetColor];
		}
	}
}
*/
//======== Using fChange value to update stock =====================
- (void)setupUIFromModel:(StockModel *)model atIndexPath:(NSIndexPath *)indexPath expand:(BOOL)expand{
	_indexPath = indexPath;
	//self.state = expand ? CellState_Expanded : CellState_Collapsed;
	[self updateData:model useBlinkIfNeed:NO];
	//For Chart
	_chartView.stCode = model.stockCode;
	[_chartView startLoadDefaultChart];
	self.state = expand ? CellState_Expanded : CellState_Collapsed;
}
//========== Just control data by Model =========
- (void)updateData:(StockModel *)model useBlinkIfNeed:(BOOL)isUsedBlink{
	if (model) {
		_lblStName.text = model.stockName;
		_lblChange.text = [model.fChange stringValue];
		_lblPrice.text = [model.lastDonePrice stringValue];
		
		//Check UI for animate blink
		_lblPrice.textColor = [self textColorByValueChanged:model.fChange];
		_lblChange.textColor = [self textColorByValueChanged:model.fChange];
		if (isUsedBlink) {
			UIColor *blinkColor = [self colorForBlinkViewByValueChanged:model.fChange];
			[self animateBlinkView:_lblPrice toColor:blinkColor];
		}
		
		//For Chart - maybe: No need reload chart ???
		//_chartView.stCode = model.stockCode;
		//[_chartView startLoadDefaultChart];
	}
}
- (UIColor *)textColorByValueChanged:(NSNumber *)valueChanged{
	if (valueChanged.doubleValue > 0) {
		return kGrnFlash;
	}
	if (valueChanged.doubleValue < 0) {
		return kRedFlash;
	}
	return kYellowColor;
}

- (UIColor *)colorForBlinkViewByValueChanged:(NSNumber *)valueChanged{
	if (valueChanged.doubleValue > 0) {
		return kGrnFlash;
	}
	if (valueChanged.doubleValue < 0) {
		return kRedFlash;
	}
	return [UIColor clearColor ];
}
//======== Animate View =============
- (void)setAnimateUpArrow:(BOOL)isUp{
	CGFloat rotateAngel = 0;
	if (isUp) {
		rotateAngel = -M_PI + 0.00;
	}
	//Reload arrow button on cell
	[UIView animateWithDuration:.5 delay:0.3 options:UIViewAnimationOptionCurveLinear animations:^{
		_btnArrow.transform = CGAffineTransformMakeRotation(rotateAngel);
	} completion:^(BOOL finished) {
		
	}];
}


- (void)animateBlinkView:(UIView *)view{
	[UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
		view.layer.backgroundColor = [UIColor clearColor].CGColor;
	} completion:^(BOOL finished) {
		
	}];
}
- (void)animateBlinkView:(UIView *)view toColor:(UIColor *)color{
	view.layer.backgroundColor = color.CGColor;
	[UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
		view.layer.backgroundColor = [UIColor clearColor].CGColor;
	} completion:^(BOOL finished) {
		view.layer.backgroundColor = [UIColor clearColor].CGColor;
	}];
}

- (void)setState:(CellState)state{
	if (state == CellState_Expanded) {
		[_btnArrow setImage: [UIImage imageNamed:@"up-arrow_white"] forState:UIControlStateNormal];
	}else{
		[_btnArrow setImage: [UIImage imageNamed:@"down-arrow-white"] forState:UIControlStateNormal];
	}
	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void) {
		if (state == CellState_Expanded) {
			_chartView.alpha = 1.0;
		}else{
			_chartView.alpha = 0.0;
		}
	} completion:^(BOOL finished) {
		
	}];
	_state = state;
}
#pragma mark - Main Action
- (IBAction)onArrowAction:(UIButton *)sender {
	if (_delegate) {
		[_delegate checkExpandCell:self isExpanded:sender.selected];
	}
}

#pragma mark - Override Delegate
- (void)setDelegate:(id<QuoteCellDelegate, TCChartViewDelegate>)delegate{
	_delegate = delegate;
	_chartView.delegate = self;
}
#pragma mark TCChartViewDelegate
- (void)doubleTapChartView:(TCChartView *)chartView{
    if([self.delegate respondsToSelector:@selector(doubleTap:)]){
        [self.delegate doubleTap:self];
    }
}
@end
