//
//  BaseCollectionViewCell.m
//  TCiPad
//
//  Created by Kaka on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "UIView+Animation.h"
@implementation BaseCollectionViewCell
//Singleton
+ (NSString *)reuseIdentifier {
	return NSStringFromClass([self class]);
}

+ (NSString *)nibName{
	return NSStringFromClass([self class]);
}
//=======================*********

#pragma mark - Main
#pragma mark - Animation
//Check animate
//- (void)setAnimateWithPriceChange:(NSNumber *)fChange completion:(void(^)())completion{
//	[self.contentView animateBlinkForPriceChanged:(fChange.doubleValue > 0) completion:^{
//		completion();
//	}];
//}

#pragma mark - Animation
- (void)makeAnimateBy:(StockModel *)oldModel newStock:(StockModel *)newStock completion:(void(^)(void))completion{
	[self.contentView makeAnimateBy:oldModel newStock:newStock completion:^{
		if (completion) {
			completion();
		}
	}];
}

#pragma mark - Call recrusion to make white label
//+++ do contentView to make all white text label
- (void)setAllWhiteColorTextLabelFrom:(UIView *)contentView{
	if (contentView.subviews.count == 0) {
		return;
	}
	for (id aView in contentView.subviews) {
		if ([aView isKindOfClass:[UILabel class]]) {
			UILabel *label = (UILabel *)aView;
			label.textColor = [UIColor whiteColor];
		}else if ([aView isKindOfClass:[UIView class]]){
			[self setAllWhiteColorTextLabelFrom:aView];
		}else{
			continue;
		}
	}
}
@end
