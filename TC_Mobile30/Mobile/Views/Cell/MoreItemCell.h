//
//  MoreItemCell.h
//  TCiPad
//
//  Created by Kaka on 6/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MoreItemCell : BaseTableViewCell{
	
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

//Passing data
@property (assign, nonatomic) MoreItemType itemType;

//Main
- (void)setupDataFrom:(MoreItemType)type;
//Highlight cell
- (void)setDataShouldHighlight:(BOOL)isHighlight;

@end
