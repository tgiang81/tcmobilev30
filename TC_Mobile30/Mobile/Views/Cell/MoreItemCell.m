//
//  MoreItemCell.m
//  TCiPad
//
//  Created by Kaka on 6/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MoreItemCell.h"
#define kNormal_Title_Color 		RGB(66, 90, 108)
#define kHighlight_Title_Color 		AppColor_TintColor
@implementation MoreItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - MAIN
- (void)setup{
	self.backgroundColor = [UIColor clearColor];
	_lblTitle.font = AppFont_MainFontMediumWithSize(12);
}
//Update Data
- (void)setupDataFrom:(MoreItemType)type{
	_itemType = type;
	_imgIcon.image = [UIImage imageNamed:[self iconNameFrom:type isHighlight:NO]];
	_imgIcon.highlightedImage = [UIImage imageNamed:[self iconNameFrom:type isHighlight:YES]];
	_lblTitle.text = [self titleFrom:type];
	[self setDataShouldHighlight:NO];
}

- (void)setDataShouldHighlight:(BOOL)isHighlight{
	_imgIcon.highlighted = isHighlight;
	_lblTitle.textColor = isHighlight ? kHighlight_Title_Color : kNormal_Title_Color;
}

#pragma mark - UTILS
- (NSString *)iconNameFrom:(MoreItemType)moreType isHighlight:(BOOL)highlight{
	NSString *iconName = @"";
	switch (moreType) {
		case MoreItemType_AddNew:
			iconName = highlight ? @"more_plus_highlight_icon" : @"more_plus_icon";
			break;
		case MoreItemType_Rename:
			iconName = highlight ? @"more_rename_highlight_icon" : @"more_rename_icon";
			break;
		case MoreItemType_Delete:
			iconName = highlight ? @"more_trash_highlight_icon" : @"more_trash_icon";
			break;
		case MoreItemType_Close:
			iconName = highlight ? @"more_close_highlight_icon" : @"more_close_icon";
			break;
			
		default:
			break;
	}
	return iconName;
}

- (NSString *)titleFrom:(MoreItemType)moreType{
	NSString *title = @"";
	switch (moreType) {
		case MoreItemType_AddNew:
			title = [LanguageManager stringForKey:@"Add"];
			break;
		case MoreItemType_Rename:
			title = [LanguageManager stringForKey:@"Rename"];
			break;
		case MoreItemType_Delete:
			title = [LanguageManager stringForKey:@"Delete"];
			break;
		case MoreItemType_Close:
			title = [LanguageManager stringForKey:@"Close"];
			break;
		default:
			break;
	}
	return title;
}
@end
