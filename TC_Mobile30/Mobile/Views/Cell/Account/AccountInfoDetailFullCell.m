//
//  AccountInfoDetailFullCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/13/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AccountInfoDetailFullCell.h"
#import "UITableViewCell+Utils.h"
@implementation AccountInfoDetailFullCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self showFullSeperator];
    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_bg);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].accDetail_textCell);
    self.lbl_Content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].accDetail_textCell);
    self.lbl_Content.alpha = 0.6;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
