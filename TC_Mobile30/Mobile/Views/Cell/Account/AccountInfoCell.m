//
//  AccountInfoCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AccountInfoCell.h"
#import "NumberHandler.h"
#import "UITableViewCell+Utils.h"
@interface AccountInfoCell()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Top;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Left;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Bottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Right;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ButtonWidth;


@end
@implementation AccountInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_bg);
//    self.v_Content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_bgCell);
//    self.v_Content.layer.cornerRadius = 8;
    // Initialization code
    [self showFullSeperator];
    [self.btn_Next setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell)];
    self.lbl_Content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell);
    self.lbl_Content2.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell);
}

- (void)setupDataForCell:(UserAccountClientData *)model{
    if ([UserPrefConstants singleton].CreditLimitOrdBook) {
        self.lbl_Content.text = model.display_name;
    }
    else {
        self.lbl_Content.text = [NSString stringWithFormat:@"%@-%@", model.broker_branch_code, model.client_account_number];
    }    
}
- (void)setFlatMode:(BOOL)flatMode{
    _flatMode = flatMode;
    if(flatMode){
        [self.btn_Next setHidden:YES];
        self.cst_ButtonWidth.constant = 8;
    }
}

- (IBAction)didSelectNext:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectButton:)]){
        [self.delegate didSelectButton:self];
    }
}
@end
