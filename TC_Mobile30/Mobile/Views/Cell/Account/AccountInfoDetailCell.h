//
//  AccountInfoDetailCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountInfoDetailCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Title2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content2;

@end

NS_ASSUME_NONNULL_END
