//
//  AccountInfoDetailCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AccountInfoDetailCell.h"
#import "UITableViewCell+Utils.h"
@implementation AccountInfoDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self showFullSeperator];
    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_bg);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].accDetail_textCell);
    self.lbl_Content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].accDetail_textCell);
    self.lbl_Content.alpha = 0.6;
    
    self.lbl_Title2.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].accDetail_textCell);
    self.lbl_Content2.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].accDetail_textCell);
    self.lbl_Content2.alpha = 0.6;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
