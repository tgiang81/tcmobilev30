//
//  AccountInfoDetailFullCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/13/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountInfoDetailFullCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content;
@end

NS_ASSUME_NONNULL_END
