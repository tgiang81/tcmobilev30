//
//  AccountInfoCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import  "AccountInfoView.h"
@class UserAccountClientData;
NS_ASSUME_NONNULL_BEGIN

@interface AccountInfoCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet AccountInfoView *v_AccountInfo;
@property (weak, nonatomic) IBOutlet UIView *v_Content;
@property (weak, nonatomic) id<AccountInfoViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content2;
@property (weak, nonatomic) IBOutlet UIButton *btn_Next;

@property (assign, nonatomic) BOOL flatMode;
- (void)setupDataForCell:(UserAccountClientData *)model;
@end

NS_ASSUME_NONNULL_END
