//
//  ResearchCell.h
//  TCiPad
//
//  Created by Kaka on 6/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class NewsModel;
@class TCBaseView;
@interface ResearchCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet TCBaseView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblExchange;

//Main
- (void)setupDataFrom:(id)model;

@end
