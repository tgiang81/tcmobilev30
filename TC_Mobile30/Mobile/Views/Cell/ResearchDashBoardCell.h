//
//  ResearchDashBoardCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@class DashBoardCornerView;
NS_ASSUME_NONNULL_BEGIN

@interface ResearchDashBoardCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblExchange;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet DashBoardCornerView *v_Main;

- (void)setupDataFrom:(id)model;
@end

NS_ASSUME_NONNULL_END
