//
//  PortfolioVirtualCell.m
//  TC_Mobile30
//
//  Created by Kaka on 9/26/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PortfolioVirtualCell.h"
#import "PrtfSummRptData.h"
#import "NSNumber+Formatter.h"
@implementation PortfolioVirtualCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModelForCell:(PrtfSummRptData *)model{
    _lblBFCashBalance.text = [NSNumber numberFormat2D:[model.bf_cash_balance doubleValue]];
    _lblDeposit.text = [NSNumber numberFormat2D:[model.deposit doubleValue]];
    _lblWithDrawal.text = [NSNumber numberFormat2D:[model.withdrawal doubleValue]];
    _lblRealisedGL.text = [NSNumber numberFormat2D:[model.realized_pl doubleValue]];
    _lblCurrentBalance.text = [NSNumber numberFormat2D:model.current_balance];
    
    //
    _lblUnrealisedGL.text = [NSNumber numberFormat2D:model.total_unrealized_pl];
    double equity = model.current_balance + model.total_unrealized_pl;
    _lblEquity.text = [NSNumber numberFormat2D:equity];
    //
    
    _lblBuyOptionMktValue.text = [NSNumber numberFormat2D:[model.open_long doubleValue]];
    _lblSellOptionMktValue.text = [NSNumber numberFormat2D:[model.open_short doubleValue]];
    
    //
    double net_liquidation = equity + [model.open_long doubleValue] + [model.open_short doubleValue];
    _lblNetLiquidation.text = [NSNumber numberFormat2D:net_liquidation];
    //
    
    _lblEligibleCollateral.text = [NSNumber numberFormat2D:[model.eligible_collateral doubleValue]];
    _lblInitialMargin.text = [NSNumber numberFormat2D:[model.initial_margin doubleValue]];
    _lblMaintenanceMargin.text = [NSNumber numberFormat2D:[model.maintenance_margin doubleValue]];
    
    //
    double excess_shortfall = net_liquidation + [model.eligible_collateral doubleValue] - [model.initial_margin doubleValue];
    _lblExcessOrShortfall.text = [NSNumber numberFormat2D:excess_shortfall];
    
    double eligibility = 0;
    if ([model.initial_margin doubleValue] > 0) {
        eligibility = ((net_liquidation + [model.eligible_collateral doubleValue]) / [model.initial_margin doubleValue]) * 100;
    }
    else {
        eligibility = 0;
    }
    _lblEligibilityPercent.text = [NSNumber numberFormat2D:eligibility];
    //
    
    _lblMarginCall.text = [NSNumber numberFormat2D:[model.margin_call doubleValue]];
    
    
    
}
#pragma mark - Main
#pragma mark - MAIN
- (void)setup{
	self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
//	_lblStockName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
//	_lblAVG_Bprice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
//	_lblAVG_Sprice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
//	_lblGL.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
//	_lblGLPercent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
}
@end
