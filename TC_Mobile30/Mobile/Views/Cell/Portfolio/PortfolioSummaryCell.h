//
//  PortfolioSummaryCell.h
//  TC_Mobile30
//
//  Created by Kaka on 11/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
@class PrtfSubDtlRptData;
@interface PortfolioSummaryCell : BaseTableViewCell{
	
}
//OUTLETS
//Ins

@property (weak, nonatomic) IBOutlet UILabel *lblIns_ViewDetails;

@property (weak, nonatomic) IBOutlet UILabel *lblIns_UnrealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Currency;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_GrBuy;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_GrSell;

@property (weak, nonatomic) IBOutlet UILabel *lblIns_RealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_CurrencyRate;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_BFBuy;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_BFSell;

//Value
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblNettPos;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePer;

@property (weak, nonatomic) IBOutlet UILabel *lblUnrealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblGrBuy;
@property (weak, nonatomic) IBOutlet UILabel *lblGrSell;

@property (weak, nonatomic) IBOutlet UILabel *lblRealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyRate;
@property (weak, nonatomic) IBOutlet UILabel *lblBFBuy;
@property (weak, nonatomic) IBOutlet UILabel *lblBFRate;

- (void)setModelForCell:(PrtfSubDtlRptData *)psdrd;
@end
