//
//  PortfolioVirtualCell.h
//  TC_Mobile30
//
//  Created by Kaka on 9/26/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
@class PrtfSummRptData;
@interface PortfolioVirtualCell : BaseTableViewCell{
	
}
//OUTLETS
//Ins
@property (weak, nonatomic) IBOutlet UILabel *lblIns_BFCashBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Deposit;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Withdrawal;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_RealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_CurrentBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_UnrealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Equity;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_BuyOptionEquityValue;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_SellOptionEquityValue;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_NetLiquidation;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_EligibleCollateral;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_InitialMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_MainternanceMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_ExcessOrShortfall;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_MarginCall;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_EligibilityPercent;
//Value
@property (weak, nonatomic) IBOutlet UILabel *lblBFCashBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblDeposit;
@property (weak, nonatomic) IBOutlet UILabel *lblWithDrawal;
@property (weak, nonatomic) IBOutlet UILabel *lblRealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblUnrealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblEquity;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyOptionMktValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSellOptionMktValue;
@property (weak, nonatomic) IBOutlet UILabel *lblNetLiquidation;
@property (weak, nonatomic) IBOutlet UILabel *lblEligibleCollateral;
@property (weak, nonatomic) IBOutlet UILabel *lblInitialMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblMaintenanceMargin;
@property (weak, nonatomic) IBOutlet UILabel *lblExcessOrShortfall;
@property (weak, nonatomic) IBOutlet UILabel *lblMarginCall;
@property (weak, nonatomic) IBOutlet UILabel *lblEligibilityPercent;

- (void)setModelForCell:(PrtfSummRptData *)model;
@end
