//
//  MPortfolioEquityCell.m
//  TC_Mobile30
//
//  Created by Kaka on 12/20/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MPortfolioEquityCell.h"
#import "TCBaseLabel.h"
#import "MarqueeLabel.h"
#import "NSNumber+Formatter.h"
#import "QCData.h"
//Model
#import "PortfolioData.h"

@interface MPortfolioEquityCell(){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *mainInfoView;
@property (weak, nonatomic) IBOutlet UIView *subInfoView;

@property (weak, nonatomic) IBOutlet TCBaseLabel *lblGL;
@property (weak, nonatomic) IBOutlet TCBaseLabel *lblGLValue;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UILabel *lblStockName;
@property (weak, nonatomic) IBOutlet UILabel *lblStockCode;
//SubInfo
@property (weak, nonatomic) IBOutlet UILabel *lblQtyOnHand;
@property (weak, nonatomic) IBOutlet UILabel *lblAverageBP;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblLeftOne;
@property (weak, nonatomic) IBOutlet UILabel *lblLeftTwo;
@property (weak, nonatomic) IBOutlet UILabel *lblRigntOne;
- (IBAction)btnDetailValue:(id)sender;


@end
@implementation MPortfolioEquityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _countClick=0;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Actions

- (IBAction)onExpandAction:(id)sender {
	if (_delegate) {
		[_delegate didTapExpandCell:self];
	}
}
- (IBAction)onTapStockNameAction:(id)sender {
    if (_delegate) {
        [_delegate didTapStockNameCell:self];
    }
}

#pragma mark - UTILS
- (void)setIsExpandedCell:(BOOL)isExpandableCell{
	_isExpandedCell = isExpandableCell;
	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void) {
		if (!isExpandableCell) {
			self.subInfoView.alpha = 0.0;
		}else{
			self.subInfoView.alpha = 1.0;
		}
	} completion:^(BOOL finished) {
		//Do nothing
		self.subInfoView.hidden = !isExpandableCell;
	}];
}

#pragma mark - Main
- (void)setupDataFrom:(PortfolioData *)model forPfEquityType:(MPfEquityType)type{
    _equityType=type;
	if (model) {
		_lblStockName.text = model.stockname;
		_lblStockCode.text = [model.stockcode stringByDeletingPathExtension];
		
		NSDictionary *stkData = [[[QCData singleton] qcFeedDataDict] objectForKey:model.stockcode];
		double price_current = [[stkData objectForKey:FID_98_D_LAST_DONE_PRICE] doubleValue];
		double price_ref = [[stkData objectForKey:FID_51_D_REF_PRICE] doubleValue];
		if (type == MPfEquityType_Unrealised) {
			//Unrealised GL
			double Amount = 0.0;
			double BuyTrxFee;
			double Commission;
			double SCCPFee;
			double SECFee;
			double SalesTax;
			double SellTrxFee;
			double VAT;
			for(NSString *formula in [UserPrefConstants singleton].trxFeeFormulaDict){
				NSString *keyFormula = [[formula componentsSeparatedByString:@"="] objectAtIndex:0];
				if ([keyFormula isEqualToString:@"Amount"]) {
					Amount = (double)(price_current * [model.quantity_on_hand longLongValue]);
					break;
				}
			}
			
			SCCPFee = (double)Amount*0.0001;
			SECFee = (double)Amount*0.00005;
			SalesTax = (double)Amount * 0.006;
			Commission = (double)Amount*0.0025;
			VAT = (double)Commission*0.12;
			BuyTrxFee = (double)Commission+SCCPFee+SECFee+VAT;
			SellTrxFee = (double)BuyTrxFee+(double)SalesTax;
			double unrealized_gain_loss_amount = (double)(((price_current != 0 ? price_current : price_ref) - [model.price_avg_purchase doubleValue]) * [model.quantity_on_hand longLongValue]);
			double quantityOnHand = [model.quantity_on_hand intValue];
			if ([UserPrefConstants singleton].isEnabledTRXFee) {
				if (quantityOnHand<0) {
					unrealized_gain_loss_amount = ABS(unrealized_gain_loss_amount) - SellTrxFee;
				}else{
					unrealized_gain_loss_amount = unrealized_gain_loss_amount - BuyTrxFee;
				}
			}
			if ((price_current == 0 && price_ref == 0) || [model.price_avg_purchase doubleValue] == 0 || [model.quantity_on_hand longLongValue] == 0) {
				unrealized_gain_loss_amount = 0;
			}
            _lblGLValue.text = [[NSNumber numberWithDouble:unrealized_gain_loss_amount] toDecimaWithPointer:2];
			if (unrealized_gain_loss_amount >= 0) {
				_lblGL.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
			}else{
				_lblGL.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
			}
            // Unrealised Percent
            double unrealized_gain_loss_percent=((unrealized_gain_loss_amount)/(model.price_avg_purchase.doubleValue * model.quantity_on_hand.doubleValue))*100;
            model.unrealized_gain_loss_amount =[NSString stringWithFormat:@"%f",unrealized_gain_loss_amount];
            model.unrealized_gain_loss_percentage=[NSString stringWithFormat:@"%f",unrealized_gain_loss_percent];
			//Sub Info
			double avgBPuchase = model.price_avg_purchase.doubleValue; //How to calculate
			_lblAverageBP.text = [[NSNumber numberWithDouble:avgBPuchase] toCurrencyNumber];
			_lblQtyOnHand.text = [[NSNumber numberWithDouble:model.quantity_on_hand.doubleValue] abbreviateNumber];
		}else{
			//Realised GL
			_lblGLValue.text =[[NSNumber numberWithDouble:model.realized_gain_loss_amount.doubleValue] toDecimaWithPointer:2] ;
			if (model.realized_gain_loss_amount.doubleValue >= 0) {
				_lblGL.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
			}else{
				_lblGL.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
			}
			//Sub Info
			//AVG B Price
			double avgBPuchase = model.aggregated_buy_price.doubleValue; //How to calculate
			_lblAverageBP.text = [[NSNumber numberWithDouble:avgBPuchase] toCurrencyNumber];
			_lblQtyOnHand.text = [[NSNumber numberWithDouble:model.total_quantity_P.doubleValue] abbreviateNumber];
		}
		_lblQty.text = [[NSNumber numberWithDouble:model.quantity_available.doubleValue] abbreviateNumber];
		
		//Sub Info common
		double lastPrice = (model.price_last_done.doubleValue == 0) ? model.price_ref.doubleValue : model.price_last_done.doubleValue;
		_lblLastPrice.text = [[NSNumber numberWithDouble:lastPrice] toCurrencyNumber];
	}
    _pData=model;
}
//Cuongnx (16/04/2019) Create UI for Realised Portfolio Screen
- (void) createUIExpand:(MPfEquityType)type{
    if (type==MPfEquityType_Realised) {
        _lblLeftOne.text=@"Avg.Bought Price:";
        NSLayoutConstraint *constraintLeft =[NSLayoutConstraint
                                           constraintWithItem:_lblLeftOne
                                           attribute:NSLayoutAttributeWidth
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:_lblQtyOnHand
                                           attribute:NSLayoutAttributeWidth
                                           multiplier:5
                                           constant:0];
        NSLayoutConstraint *constraintRight =[NSLayoutConstraint
                                             constraintWithItem:_lblRigntOne
                                             attribute:NSLayoutAttributeWidth
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:_lblLastPrice
                                             attribute:NSLayoutAttributeWidth
                                             multiplier:2.9
                                             constant:0];
        [ self addConstraint:constraintLeft];
        [ self addConstraint:constraintRight];
        _lblRigntOne.text=@"Avg.Sold Price:";
//         _subInfoView.frame= CGRectMake(_subInfoView.frame.origin.x, _subInfoView.frame.origin.y, _subInfoView.frame.size.width, _subInfoView.frame.size.height-16);
    }
}
- (IBAction)btnDetailValue:(id)sender {
    NSString *data;
    if (_countClick==0) {
        if (_equityType==MPfEquityType_Unrealised) {
            data=[NSString stringWithFormat:@"%s","00"];
        }
        else{
            data=[NSString stringWithFormat:@"%s","01"];
        }
        _lblGLValue.text=[NSString stringWithFormat:@"%.2f",_pData.unrealized_gain_loss_amount.doubleValue];        _countClick=1;
    }
    else if (_countClick==1){
        if (_equityType==MPfEquityType_Unrealised) {
            data=[NSString stringWithFormat:@"%s","10"];
        }
        else{
            data=[NSString stringWithFormat:@"%s","11"];
        }
      _lblGLValue.text=[NSString stringWithFormat:@"%.2f%%",_pData.unrealized_gain_loss_percentage.doubleValue];
        _countClick=0;
    }
     [[NSNotificationCenter defaultCenter] postNotificationName:@"didChangeValuePercent" object:data];
}

@end
