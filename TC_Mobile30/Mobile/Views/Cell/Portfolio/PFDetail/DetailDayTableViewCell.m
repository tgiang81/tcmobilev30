//
//  DetailDayTableViewCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/4/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "DetailDayTableViewCell.h"
#import "NSNumber+Formatter.h"
@implementation DetailDayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) loadDataWithPortfolioData:(PrtfDtlRptData *)pdrd {
    if ([[pdrd.side uppercaseString] isEqualToString:@"B"]) {
        _label_buy_qty.text = pdrd.matched_qty;
        _label_sell_qty.text = @"0";
    }
    else if ([[pdrd.side uppercaseString] isEqualToString:@"S"]) {
        _label_buy_qty.text = @"0";
        _label_sell_qty.text = pdrd.matched_qty;
    }
    
    _label_price.text = [[NSNumber numberWithDouble:[pdrd.matched_price doubleValue]] toCurrencyNumber];
}
@end
