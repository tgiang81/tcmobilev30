//
//  DetailOvernightTableViewCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/4/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "PrtfDtlRptData.h"
NS_ASSUME_NONNULL_BEGIN

@interface DetailOvernightTableViewCell : BaseTableViewCell
@property (nonatomic, retain) NSString *trade_exchg_code;
@property (weak, nonatomic) IBOutlet UILabel *label_buy_qty;
@property (weak, nonatomic) IBOutlet UILabel *label_sell_qty;
@property (weak, nonatomic) IBOutlet UILabel *label_price;
@property (weak, nonatomic) IBOutlet UILabel *label_unrealized_pl;

- (void) loadDataWithPortfolioData:(PrtfDtlRptData *)pdrd;
@end

NS_ASSUME_NONNULL_END
