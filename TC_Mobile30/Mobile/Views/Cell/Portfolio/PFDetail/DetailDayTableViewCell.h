//
//  DetailDayTableViewCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/4/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "PrtfDtlRptData.h"
NS_ASSUME_NONNULL_BEGIN

@interface DetailDayTableViewCell : BaseTableViewCell
@property (nonatomic, retain) NSString *trade_exchg_code;

@property (weak, nonatomic) IBOutlet UILabel *label_buy_qty;
@property (weak, nonatomic) IBOutlet UILabel *label_sell_qty;
@property (weak, nonatomic) IBOutlet UILabel *label_price;

- (void) loadDataWithPortfolioData:(PrtfDtlRptData *)pdrd;
@end

NS_ASSUME_NONNULL_END
