//
//  PortfoliosUnrealisedCell.m
//  TC_Mobile30
//
//  Created by Kaka on 10/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PortfoliosUnrealisedCell.h"
#import "PortfolioData.h"
#import "NSNumber+Formatter.h"
#import "MarqueeLabel.h"
@interface PortfoliosUnrealisedCell(){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockName;
@property (weak, nonatomic) IBOutlet UILabel *lblAVG_Bprice;
@property (weak, nonatomic) IBOutlet UILabel *lblQTY;
@property (weak, nonatomic) IBOutlet UILabel *lblMktValue;
@property (weak, nonatomic) IBOutlet UILabel *lblGL;


@end
@implementation PortfoliosUnrealisedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - MAIN
- (void)setup{
	self.backgroundColor = [UIColor clearColor];
	self.mainContentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	_lblStockName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblAVG_Bprice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblQTY.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblGL.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblMktValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
}

- (void)setupDataFrom:(PortfolioData *)model{
	if (model) {
		_lblStockName.text = model.stockname;
		_lblQTY.text = [[NSNumber numberWithDouble:model.quantity_on_hand.longLongValue] abbreviateNumber];
		double price_current = (model.price_last_done.doubleValue == 0) ? model.price_ref.doubleValue : model.price_last_done.doubleValue;
		double mktValue = price_current * model.quantity_on_hand.longLongValue;
		_lblMktValue.text = [[NSNumber numberWithDouble:mktValue] toCurrencyNumber];
		
		//AVG B Price
		double avgBPrice = 0.0; //How to calculate
		avgBPrice = model.total_price_B.doubleValue / model.quantity_on_hand.doubleValue;
		if (model.quantity_on_hand.doubleValue == 0) {
			avgBPrice = 0.0;
		}
		_lblAVG_Bprice.text = [[NSNumber numberWithDouble:avgBPrice] toCurrencyNumber];
		
		
		//gain loss (GL)
		_lblGL.text = [[NSNumber numberWithDouble:model.unrealized_gain_loss_amount.doubleValue] abbreviateNumber];
	}
}
@end
