//
//  PortfolioCell.m
//  TC_Mobile30
//
//  Created by Kaka on 9/26/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PortfolioRelizedCell.h"
#import "PortfolioData.h"
#import "NSNumber+Formatter.h"
#import "MarqueeLabel.h"

@interface PortfolioRelizedCell(){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockName;
@property (weak, nonatomic) IBOutlet UILabel *lblAVG_Bprice;
@property (weak, nonatomic) IBOutlet UILabel *lblAVG_Sprice;
@property (weak, nonatomic) IBOutlet UILabel *lblGL;
@property (weak, nonatomic) IBOutlet UILabel *lblGLPercent;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

@end
@implementation PortfolioRelizedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - MAIN
- (void)setup{
	self.backgroundColor = [UIColor clearColor];
	self.mainContentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	_lblStockName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblAVG_Bprice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblAVG_Sprice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblGL.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblGLPercent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
}

- (void)setupDataFrom:(PortfolioData *)model{
	if (model) {
		_lblStockName.text = model.stockname;
		//AVG B Price
		double avgSellPrice = 0.0;
		avgSellPrice = model.total_price_S.doubleValue / model.quantity_on_hand.doubleValue;
		if (model.quantity_on_hand.doubleValue == 0) {
			avgSellPrice = 0.0;
		}
		double avgBPrice = 0.0; //How to calculate
		avgBPrice = model.total_price_B.doubleValue / model.quantity_on_hand.doubleValue;
		if (model.quantity_on_hand.doubleValue == 0) {
			avgBPrice = 0.0;
		}
		_lblAVG_Bprice.text = [[NSNumber numberWithDouble:avgBPrice] toCurrencyNumber];
		_lblAVG_Sprice.text = [[NSNumber numberWithDouble:avgSellPrice] toCurrencyNumber];
		
		//gain loss (GL)
		_lblGL.text = [[NSNumber numberWithDouble:model.realized_gain_loss_amount.doubleValue] abbreviateNumber];
		
		_lblGLPercent.text = [[NSNumber numberWithDouble:model.realized_gain_loss_percentage.doubleValue] toPercentWithSign];
	}
}
@end
