//
//  PortfolioSummaryCell.m
//  TC_Mobile30
//
//  Created by Kaka on 11/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PortfolioSummaryCell.h"
#import "PrtfSubDtlRptData.h"
#import "NSNumber+Formatter.h"
#import "Utils.h"
@implementation PortfolioSummaryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModelForCell:(PrtfSubDtlRptData *)psdrd{
    StockModel *sd = [Utils stockQCFeedDataFromCode:psdrd.stock_code];
    _lblName.text = psdrd.stock_name;
    
    _lblDescription.text = @"";
    
    _lblNettPos.text = [NSString stringWithFormat:@"%d", psdrd.nett_position];
    
    //0: Last Done
    //1: Average Purchase Price
    int change_type_value = 0;
    if (change_type_value == 0) {
        double price = 0;
        if (sd != nil) {
            price = [sd.lastDonePrice doubleValue];
        }
        else {
            price = psdrd.price_current;
        }
        _lblLastPrice.text = [[NSNumber numberWithDouble:price] toCurrencyNumber];
    }
    else {
        _lblLastPrice.text = [[NSNumber numberWithDouble:[psdrd.average_purchase_price doubleValue]] toCurrencyNumber];
    }
    _lblChangePer.text = [NSNumber numberFormat2D:psdrd.contractPerVal];
    
    _lblUnrealisedGL.text = [NSNumber numberFormat2D:psdrd.unrealized_pl];
    _lblCurrency.text = psdrd.stock_currency;
    _lblGrBuy.text = psdrd.gross_buy;
    _lblGrSell.text = psdrd.gross_sell;
    
    _lblRealisedGL.text = [NSNumber numberFormat2D:[psdrd.realized_pl doubleValue]];
    _lblCurrencyRate.text = psdrd.exchange_rate;
    _lblBFBuy.text = psdrd.bf_buy;
    _lblBFRate.text = psdrd.bf_sell;
}
#pragma mark - Main
- (void)setup{
	self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	_lblIns_ViewDetails.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	
	_lblIns_UnrealisedGL.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblIns_Currency.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblIns_GrBuy.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblIns_GrSell.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	
//	_lblIns_ViewDetails;
//
//	_lblIns_UnrealisedGL;
//	_lblIns_Currency;
//	_lblIns_GrBuy;
//	_lblIns_GrSell;
//
//	_lblIns_RealisedGL;
//	_lblIns_CurrencyRate;
//	_lblIns_BFBuy;
//	_lblIns_BFSell;
//
//	//Value
//	_lblName;
//	_lblDescription;
//	_lblNettPos;
//	_lblLastPrice;
//	_lblChangePer;
//
//	_lblUnrealisedGL;
//	_lblCurrency;
//	_lblGrBuy;
//	_lblGrSell;
//
//	_lblRealisedGL;
//	_lblCurrencyRate;
//	_lblBFBuy;
//	_lblBFRate;
}

@end
