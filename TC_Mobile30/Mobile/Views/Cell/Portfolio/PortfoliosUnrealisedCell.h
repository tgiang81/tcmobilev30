//
//  PortfoliosUnrealisedCell.h
//  TC_Mobile30
//
//  Created by Kaka on 10/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
@class PortfolioData;

@interface PortfoliosUnrealisedCell : BaseTableViewCell{
	
}
//Main
- (void)setupDataFrom:(PortfolioData *)model;

@end
