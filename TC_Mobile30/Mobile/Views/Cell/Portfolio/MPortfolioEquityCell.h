//
//  MPortfolioEquityCell.h
//  TC_Mobile30
//
//  Created by Kaka on 12/20/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

#define kMPNORMAL_CELL_HEIGHT		60
#define kMPEXPANDED_CELL_HEIGHT		110


@class PortfolioData;
@class MPortfolioEquityCell;

//Configs
typedef enum: NSInteger {
	MPfEquityType_Unrealised = 0,
	MPfEquityType_Realised
} MPfEquityType;

@protocol MPortfolioEquityCellDelegate <NSObject>
- (void)didTapExpandCell:(MPortfolioEquityCell *)cell;
-(void)didTapStockNameCell:(MPortfolioEquityCell *)cell;
@end

@interface MPortfolioEquityCell : BaseTableViewCell{
	
}
@property (assign,nonatomic) NSInteger countClick;
@property (assign,nonatomic) PortfolioData *pData ;
//Passing data
@property (assign, nonatomic) BOOL isExpandedCell;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (assign, nonatomic) MPfEquityType equityType;
@property (weak, nonatomic) id <MPortfolioEquityCellDelegate> delegate;

//MAIN
- (void)setupDataFrom:(PortfolioData *)model forPfEquityType:(MPfEquityType)type;
- (void) createUIExpand:(MPfEquityType)type;

@end
