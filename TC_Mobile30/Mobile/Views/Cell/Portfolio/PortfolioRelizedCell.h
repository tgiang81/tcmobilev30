//
//  PortfolioCell.h
//  TC_Mobile30
//
//  Created by Kaka on 9/26/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

@class PortfolioData;
@interface PortfolioRelizedCell : BaseTableViewCell{
}
//Main
- (void)setupDataFrom:(PortfolioData *)model;

@end
