//
//  SectorCell.h
//  TC_Mobile30
//
//  Created by Kaka on 12/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseCollectionViewCell.h"
@class TCBaseView;
@interface SectorCell : BaseCollectionViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet TCBaseView *viewContent;

@end
