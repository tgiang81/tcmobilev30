//
//  SectorCell.m
//  TC_Mobile30
//
//  Created by Kaka on 12/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SectorCell.h"
#import "TCBaseView.h"

@implementation SectorCell
- (void)layoutSubviews{
	[super layoutSubviews];
}

#pragma mark - Configs
- (void)setSelected:(BOOL)selected{
	if (selected) {
		_lblContent.textColor = TC_COLOR_FROM_HEX(@"ffffff");
		self.viewContent.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
		self.viewContent.borderWidth = 0.0;
	}else{
		_lblContent.textColor = TC_COLOR_FROM_HEX(@"616161");
		self.viewContent.backgroundColor = TC_COLOR_FROM_HEX(@"ffffff");
		self.viewContent.borderWidth = 1.0;
	}
}
@end
