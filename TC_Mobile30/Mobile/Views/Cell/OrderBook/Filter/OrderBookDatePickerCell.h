//
//  OrderBookDatePickerCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/18/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "TTRangeSlider.h"
@protocol OrderBookDatePickerCellDelegate<NSObject>
- (void)changeRange:(NSTimeInterval)startAt endAt:(NSTimeInterval)endAt;
@end
@interface OrderBookDatePickerCell : BaseCollectionViewCell
@property (nonatomic, assign) NSTimeInterval startAt;
@property (nonatomic, assign) NSTimeInterval endAt;
@property (nonatomic, assign) CGFloat step;
@property (weak, nonatomic) IBOutlet TTRangeSlider *slider;
@property (weak, nonatomic) id<OrderBookDatePickerCellDelegate> delegate;
- (void)setupUI;
@end
