//
//  OrderBookFilterCell.swift
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

import UIKit

class OrderBookFilterCell: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var cst_clvHeight: NSLayoutConstraint!
    @IBOutlet weak var layout_content: AlignedCollectionViewFlowLayout!
    @IBOutlet weak var clv_content: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        layout_content?.horizontalAlignment = .left
        
        // Enable automatic cell-sizing with Auto Layout:
        layout_content?.estimatedItemSize = .init(width: 100, height: 30)
    
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
