//
//  OrderBookFilterItemCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionViewCell.h"
@class OrderBookFilterItemCell;
@protocol OrderBookFilterItemCellDelegate <NSObject>
@required
- (void)didSelect:(NSIndexPath *)index;
@end
@interface OrderBookFilterItemCell : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *view_content;
@property (weak, nonatomic) IBOutlet UIButton *btn_content;
@property (weak, nonatomic) id<OrderBookFilterItemCellDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *index;
@property (assign) BOOL isSelectedFilter;
- (void)updateView;
- (void)changeColor;
@end
