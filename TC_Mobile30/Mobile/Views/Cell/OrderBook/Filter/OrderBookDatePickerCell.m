//
//  OrderBookDatePickerCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/18/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "OrderBookDatePickerCell.h"
@interface OrderBookDatePickerCell() <TTRangeSliderDelegate>
@end
@implementation OrderBookDatePickerCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setupUI{
    self.slider.delegate = self;
    self.slider.minDistance = self.step;
    self.slider.minValue = self.startAt;
    self.slider.maxValue = self.endAt;
    self.slider.selectedMinimum = self.startAt;
    self.slider.selectedMaximum = self.endAt;
    self.slider.step = self.step;
    self.slider.enableStep = YES;
    self.slider.tintColor = [UIColor clearColor];
    self.slider.tintColorBetweenHandles = [UIColor blackColor];
    self.slider.handleBorderColor = [UIColor blackColor];
    self.slider.minLabelColour = [UIColor blackColor];
    self.slider.minHandleColor = [UIColor whiteColor];
    self.slider.maxLabelColour = [UIColor blackColor];
    self.slider.maxHandleColor = [UIColor whiteColor];
    self.slider.handleBorderWidth = 1;
    self.slider.handleDiameterHeight = 30;
    self.slider.handleDiameter = 80;
}

- (void)setStartAt:(NSTimeInterval)startAt{
    _startAt = startAt;
    self.slider.minValue = startAt;
}
- (void)setEndAt:(NSTimeInterval)endAt{
    _endAt = endAt;
    self.slider.maxValue = endAt;
}

#pragma mark TTRangeSliderViewDelegate
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    if([self.delegate respondsToSelector:@selector(changeRange:endAt:)]){
        [self.delegate changeRange:selectedMinimum endAt:selectedMaximum];
    }
}
@end
