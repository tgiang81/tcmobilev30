//
//  OrderBookFilterItemCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "OrderBookFilterItemCell.h"

@implementation OrderBookFilterItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.view_content.layer.borderWidth = 1;
    self.view_content.layer.cornerRadius = 8;
    
    
    // Initialization code
}
- (void)updateView{
    self.view_content.layer.borderColor = [TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterBorderCell) CGColor];
}
- (IBAction)select:(UIButton *)sender {
    self.isSelectedFilter = !self.isSelectedFilter;
    [self changeColor];
    if([self.delegate respondsToSelector:@selector(didSelect:)]){
        [self.delegate didSelect:self.index];
    }
}
- (void)changeColor{
    if(self.isSelectedFilter == YES){
        [self.btn_content setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterSelectedTextColorCell) forState:UIControlStateNormal];
        [self.view_content setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterSelectedCellBg)];
    }else{
        [self.btn_content setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterTextColorCell) forState:UIControlStateNormal];
        [self.view_content setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterUnSelectedCellBg)];
    }
}
@end
