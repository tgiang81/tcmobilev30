//
//  OrderBookV3HeaderViewCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"
@class OrderBookV3HeaderView;
NS_ASSUME_NONNULL_BEGIN

@interface OrderBookV3HeaderViewCell : TCBaseSubView
@property (weak, nonatomic) IBOutlet OrderBookV3HeaderView *view_header;
@end

NS_ASSUME_NONNULL_END
