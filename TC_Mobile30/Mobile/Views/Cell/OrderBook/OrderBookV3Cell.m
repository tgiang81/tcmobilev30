//
//  OrderBookV3Cell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "OrderBookV3Cell.h"
#import "TradeStatus.h"
#import "NSNumber+Formatter.h"
#import "NSString+Util.h"
#import "TC_Calculation.h"
@implementation OrderBookV3Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.lbl_code.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor);
    self.lbl_price.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor);
    self.lbl_qty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor);
    self.lbl_lastUpdated.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor);
    self.lbl_total.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor);
    self.lbl_status.layer.cornerRadius = 8;
    
    
    self.lbl_matchP.textColor = _lbl_code.textColor;
    self.lbl_matchPTitle.textColor = _lbl_code.textColor;
    self.lbl_matchQ.textColor = _lbl_code.textColor;
    self.lbl_matchQTitle.textColor = _lbl_code.textColor;
    
    self.lbl_action.textColor = _lbl_code.textColor;
    self.lbl_actionTitle.textColor = _lbl_code.textColor;
    self.lbl_status.textColor = _lbl_code.textColor;
    self.lbl_statusTitle.textColor = _lbl_code.textColor;
    
    [self setAlphaForSubtitle:_lbl_lastUpdated];
    
    [self setAlphaForSubtitle:_lbl_matchPTitle];
    
    [self setAlphaForSubtitle:_lbl_matchQTitle];

    
    [self setAlphaForSubtitle:_lbl_actionTitle];

    [self setAlphaForSubtitle:_lbl_statusTitle];
    
    // Initialization code
}
- (void)setAlphaForSubtitle:(UILabel *)label{
    label.alpha = 0.7;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)labelCodeTapped{
    if([self.delegate respondsToSelector:@selector(didSelectCodeLabel:)]){
        [self.delegate didSelectCodeLabel:self];
    }
}
-(void)setup:(TradeStatus *)data{
    [self.lbl_status setBackgroundColor:TC_COLOR_FROM_HEX([data getQtyBg])];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *price = [data.order_price toNumber];
    NSNumber *qty = [data.order_quantity toNumber];
//    NSNumber *total = [NSNumber numberWithDouble:[price doubleValue] * [qty doubleValue]];
    if([SettingManager shareInstance].isVisibleNameStock == YES){
        self.lbl_code.text = data.stock_name;
    }else{
        self.lbl_code.text = data.stock_code;
    }
    self.lbl_price.text = [price toCurrencyNumber];
    [self.lbl_status setText:data.statusMode];
    self.lbl_action.text = data.action;
    self.lbl_lastUpdated.text = [data getDate];
    self.lbl_matchQ.text = data.mt_quantity;
    self.lbl_matchP.text = [[NSNumber numberWithDouble:[data.mt_price doubleValue]] toCurrencyNumber];
    self.lbl_qty.text = data.order_quantity;
    data.total = [TC_Calculation getPriceWithStockLotSize:-1 settcurrency:nil stkPrice:[price doubleValue] stockCurrency:@"" quantity:[qty longValue]];
    self.lbl_total.text = [data.total toCurrencyNumber];

    
}
- (IBAction)tapCodeLabel:(id)sender {
    [self labelCodeTapped];
}
- (IBAction)changeMode:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectChangeQtyMode)]){
        [self.delegate didSelectChangeQtyMode];
    }
}

@end
