//
//  OrderBookItemCellHeader.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "OrderBookItemCellHeader.h"
#import "UIButton+RightImage.h"
@implementation OrderBookItemCellHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    [_btn_Action setupRightImage];
    [self setColorForTitle:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor)];
    [self setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_HeaderBg)];
}

- (void)setColorForTitle:(UIColor *)color{
    [_btn_Action setTintColor:color];
    [_btn_Action setTitleColor:color forState:UIControlStateNormal];
}
- (IBAction)didSelect:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(didSelectHeader:)]){
        [self.delegate didSelectHeader:self];
    }
}

@end
