//
//  OrderBookNewHeaderView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/28/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "OrderBookNewHeaderView.h"
#import "ThemeManager.h"
#import "UIButton+RightImage.h"
@implementation OrderBookNewHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.btn_Name setTitle:@"Name" forState:UIControlStateNormal];
    [self.btn_Price setTitle:@"Price" forState:UIControlStateNormal];
    [self.btn_Status setTitle:@"Status" forState:UIControlStateNormal];
    
    [_btn_Name setupRightImage];
    [_btn_Price setupRightImage];
    [_btn_Status setupRightImage];
    [self setColorForTitle:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor) button:_btn_Name];
    [self setColorForTitle:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor) button:_btn_Price];
    [self setColorForTitle:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor) button:_btn_Status];
    [self setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_HeaderBg)];
}

- (void)setColorForTitle:(UIColor *)color button:(UIButton *)button{
    [button setTintColor:color];
    [button setTitleColor:color forState:UIControlStateNormal];
}

- (IBAction)didSelectName:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectName)]){
        [self.delegate didSelectName];
    }
}
- (IBAction)didSelectPrice:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectPrice)]){
        [self.delegate didSelectPrice];
    }
}

- (IBAction)didSelectStatus:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectStatus)]){
        [self.delegate didSelectStatus];
    }
}

@end
