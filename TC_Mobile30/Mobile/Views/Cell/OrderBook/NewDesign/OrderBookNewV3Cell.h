//
//  OrderBookNewV3Cell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class TradeStatus;
@protocol OrderBookNewV3CellDelegate<NSObject>
- (void)expandCellAt:(UITableViewCell *)cell;
@end
@interface OrderBookNewV3Cell : BaseTableViewCell
@property (weak, nonatomic) id<OrderBookNewV3CellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Code;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Status;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Qty;
@property (weak, nonatomic) IBOutlet UILabel *lbl_QtyValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_MPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbl_MPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_MQty;
@property (weak, nonatomic) IBOutlet UILabel *lbl_MQtyValue;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ViewDetail;
@property (weak, nonatomic) IBOutlet UIView *v_Detail;

-(void)setup:(TradeStatus *)data;
@end

NS_ASSUME_NONNULL_END
