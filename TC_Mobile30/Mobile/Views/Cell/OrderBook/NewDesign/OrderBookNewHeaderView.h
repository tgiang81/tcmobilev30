//
//  OrderBookNewHeaderView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/28/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

NS_ASSUME_NONNULL_BEGIN
@protocol OrderBookNewHeaderViewDelegate<NSObject>
- (void)didSelectName;
- (void)didSelectPrice;
- (void)didSelectStatus;
@end
@interface OrderBookNewHeaderView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UIButton *btn_Name;
@property (weak, nonatomic) IBOutlet UIButton *btn_Price;
@property (weak, nonatomic) IBOutlet UIButton *btn_Status;

@property (weak, nonatomic) id<OrderBookNewHeaderViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
