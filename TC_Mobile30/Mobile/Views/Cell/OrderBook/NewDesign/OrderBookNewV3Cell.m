//
//  OrderBookNewV3Cell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "OrderBookNewV3Cell.h"
#import "TradeStatus.h"
#import "NSNumber+Formatter.h"
#import "NSString+Util.h"
#import "TC_Calculation.h"
#import "UITableViewCell+Utils.h"
@implementation OrderBookNewV3Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self showFullSeperator];
    _lbl_Name.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor);
    _lbl_Price.textColor = _lbl_Name.textColor;
    _lbl_Status.textColor = [UIColor whiteColor];
    
    
    _lbl_Qty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_unSelectTextColor);
    _lbl_Code.textColor = _lbl_Qty.textColor;
    _lbl_QtyValue.textColor = _lbl_Qty.textColor;
    _lbl_MPrice.textColor = _lbl_Qty.textColor;
    _lbl_MPriceValue.textColor = _lbl_Qty.textColor;
    _lbl_MQty.textColor = _lbl_Qty.textColor;
    _lbl_MQtyValue.textColor = _lbl_Qty.textColor;
    
    self.lbl_Status.layer.cornerRadius = 8;
    // Initialization code
}

- (void)setup:(TradeStatus *)data{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *price = [data.order_price toNumber];
    if([BrokerManager shareInstance].broker.tag == BrokerTag_SSI){
        self.lbl_Name.text = data.stock_name;
        self.lbl_Code.text = data.stock_code;
    }else{
        self.lbl_Code.text = @"";
        if([SettingManager shareInstance].isVisibleNameStock == YES){
            self.lbl_Name.text = data.stock_name;
        }else{
            self.lbl_Name.text = data.stock_code;
        }
    }
    
    self.lbl_Price.text = [price toCurrencyNumber];
    [self.lbl_Status setBackgroundColor:TC_COLOR_FROM_HEX([data getQtyBg])];
    [self.lbl_Status setText:data.statusMode];
    self.lbl_MQtyValue.text = data.mt_quantity;
    self.lbl_MPriceValue.text = [[NSNumber numberWithDouble:[data.mt_price doubleValue]] toCurrencyNumber];
    self.lbl_QtyValue.text = data.order_quantity;
    [self expandRow:data.isExpanded];
}
- (void)expandRow:(BOOL)isExpanded{
    [self.v_Detail setHidden:!isExpanded];
    if(isExpanded){
        self.cst_ViewDetail.constant = 49;
    }else{
        self.cst_ViewDetail.constant = 0;
    }
}
- (IBAction)expandCell:(id)sender {
    if([self.delegate respondsToSelector:@selector(expandCellAt:)]){
        [self.delegate expandCellAt:self];
    }
}

@end
