//
//  OrderBookV3HeaderView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCBaseSubView.h"
@protocol OrderBookV3HeaderViewDelegate<NSObject>
@optional
-(void)didSelectCode:(NSInteger)section;
-(void)didSelectStatus:(NSInteger)section;
-(void)didSelectAction:(NSInteger)section;
-(void)didSelectPrice:(NSInteger)section;
-(void)didSelectMPrice:(NSInteger)section;
-(void)didSelectMQty:(NSInteger)section;
-(void)didSelectQty:(NSInteger)section;
-(void)didSelectTotal:(NSInteger)section;
-(void)didSelectLastUpdate:(NSInteger)section;
@end
@interface OrderBookV3HeaderView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UIButton *lbl_code;
@property (weak, nonatomic) IBOutlet UIButton *lbl_lastUpdated;

@property (weak, nonatomic) IBOutlet UIButton *lbl_status;
@property (weak, nonatomic) IBOutlet UIButton *lbl_mQty;
@property (weak, nonatomic) IBOutlet UIButton *lbl_mPrice;

@property (weak, nonatomic) IBOutlet UIButton *lbl_action;
@property (weak, nonatomic) IBOutlet UIButton *lbl_price;
@property (weak, nonatomic) IBOutlet UIButton *lbl_qty;
@property (weak, nonatomic) IBOutlet UIButton *lbl_total;
@property (weak, nonatomic) id<OrderBookV3HeaderViewDelegate> delegate;
@property (assign) NSInteger section;
- (void)setCustomTintColor:(UIColor *)color;
@end
