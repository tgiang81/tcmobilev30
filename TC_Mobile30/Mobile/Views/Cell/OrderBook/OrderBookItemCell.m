//
//  OrderBookItemCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "OrderBookItemCell.h"

@implementation OrderBookItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lbl_Content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_CellTextColor);
    self.lbl_Content.layer.cornerRadius = 8;
    self.lbl_Content.clipsToBounds = YES;
    self.canEdit = NO;
}
- (IBAction)tapCell:(id)sender {
    if(_canEdit == YES && [self.delegate respondsToSelector:@selector(didCheck:)]){
        BOOL isCheck = [self.delegate didCheck:self];
        if(isCheck){
            self.img_Check.image = [UIImage imageNamed:@"Check"];
        }else{
            self.img_Check.image = [UIImage imageNamed:@"Uncheck"];
        }
    }
}
- (void)setCanEdit:(BOOL)canEdit{
    _canEdit = canEdit;
    [self.btn_action setHidden:!canEdit];
    if(canEdit){
        self.cst_Leading.constant = 8;
        self.cst_ButtonWidth.constant = 18;
    }else{
        self.cst_Leading.constant = 0;
        self.cst_ButtonWidth.constant = 0;
    }
}

@end
