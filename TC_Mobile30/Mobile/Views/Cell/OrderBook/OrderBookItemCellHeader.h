//
//  OrderBookItemCellHeader.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@protocol OrderBookItemCellHeaderDelegate<NSObject>
- (void)didSelectHeader:(BaseCollectionViewCell *)cell;
@end
@interface OrderBookItemCellHeader : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn_Action;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Leading;

@property (weak, nonatomic) id<OrderBookItemCellHeaderDelegate>delegate;
- (void)setColorForTitle:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
