//
//  OrderBookV3Cell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MarqueeLabel.h"
@class TradeStatus;
@protocol OrderBookV3CellDelegate<NSObject>
- (void)didSelectChangeQtyMode;
- (void)didSelectCodeLabel:(UITableViewCell *)cell;
@end
@interface OrderBookV3Cell : BaseTableViewCell
@property (weak, nonatomic) id<OrderBookV3CellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_code;
@property (weak, nonatomic) IBOutlet UILabel *lbl_action;
@property (weak, nonatomic) IBOutlet UILabel *lbl_actionTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_statusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_matchPTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_matchP;
@property (weak, nonatomic) IBOutlet UILabel *lbl_matchQTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_matchQ;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lastUpdated;

@property (weak, nonatomic) IBOutlet UILabel *lbl_price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_status;

@property (weak, nonatomic) IBOutlet UILabel *lbl_qty;
@property (weak, nonatomic) IBOutlet UILabel *lbl_total;
-(void)setup:(TradeStatus *)data;
@end
