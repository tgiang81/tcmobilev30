//
//  OrderBookV3DateHeaderView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderBookV3HeaderViewCell.h"
@class OrderBookV3HeaderView;
@interface OrderBookV3DateHeaderView : OrderBookV3HeaderViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;

@end
