//
//  OrderBookItemCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@protocol OrderBookItemCellDelegate<NSObject>
- (BOOL)didCheck:(UICollectionViewCell *)cell;
@end
@interface OrderBookItemCell : BaseCollectionViewCell
@property (weak, nonatomic) id<OrderBookItemCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content;
@property (weak, nonatomic) IBOutlet UIButton *btn_action;

@property (weak, nonatomic) IBOutlet UIView *v_seperator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Leading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ButtonWidth;

@property (weak, nonatomic) IBOutlet UIImageView *img_Check;
@property (assign, nonatomic) BOOL canEdit;
@end

NS_ASSUME_NONNULL_END
