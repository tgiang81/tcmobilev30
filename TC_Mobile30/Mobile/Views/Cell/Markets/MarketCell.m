//
//  MarketCell.m
//  TCiPad
//
//  Created by Kaka on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MarketCell.h"
#import "MarketSummaryView.h"
#import "MarketScoreBoardView.h"

@implementation MarketCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self createUI];
}

- (void)layoutSubviews{
	[super layoutSubviews];
	[self updateUI];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - CreateUI
- (void)createUI{
	//1: Prepare Content
	/* +++ Create UI Manually
	if (!_scoreBoardView) {
		_scoreBoardView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH_PORTRAIT, self.scrollView.frame.size.height)];
		_scoreBoardView.backgroundColor = [UIColor blueColor];
		[_scrollView addSubview:_scoreBoardView];
	}
	if (!_marketSummaryView) {
		_marketSummaryView = [[MarketSummaryView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH_PORTRAIT, 0, SCREEN_WIDTH_PORTRAIT, self.scrollView.frame.size.height)];
		[_scrollView addSubview:_marketSummaryView];
	}
	[_scrollView setContentSize:CGSizeMake(2 * SCREEN_WIDTH_PORTRAIT, self.scrollView.frame.size.height)];
	[_scrollView setBounces:YES];
	_scrollView.delegate = self;
	[self layoutIfNeeded];
	 */
	//2: Make default pageControl
	_summaryView.allowDoubleTapToViewFull = YES;
	_pageControl.currentPage = 0;
	[_scrollView setBounces:YES];
	_scrollView.delegate = self;
	//LoadChart
	//[self loadMarketChart];
}

- (void)updateUI{
	[_scrollView setBounces:YES];
	[self layoutIfNeeded];
}
#pragma mark - LoadChart
- (void)reloadSummaryMarket{
	if (_summaryView) {
		[_summaryView updateMarketSummaryData];
	}
}
- (void)reloadScoreBoardMarket{
	if (_scoreBoardView) {
		[_scoreBoardView loadDataForMarket];
	}
}

#pragma mark - Main
- (void)populateData{
	[self reloadSummaryMarket];
	[self reloadScoreBoardMarket];
}

#pragma mark - Main Action

- (IBAction)onPageControlValueChanged:(UIPageControl *)sender {
	NSInteger _currentPage = self.pageControl.currentPage;
	[self scrollToPageAtIndex:_currentPage animate:YES];
}

- (void)scrollToPageAtIndex:(NSInteger)index animate:(BOOL)animate{
	CGPoint offset = CGPointMake(index * self.scrollView.frame.size.width, 0);
	[self.scrollView setContentOffset:offset animated:animate];
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	static NSInteger previousPage = 0;
	CGFloat pageWidth = scrollView.frame.size.width;
	float fractionalPage = scrollView.contentOffset.x / pageWidth;
	NSInteger page = lround(fractionalPage);
	if (previousPage != page) {
		// Page has changed, do your thing!
		// ...
		// Finally, update previous page
		previousPage = page;
	}
	_pageControl.currentPage = page;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {

	CGFloat xOffset = scrollView.contentOffset.x;
	int currentPage = floor(xOffset / scrollView.bounds.size.width);
	DLog(@"XOffset: %@", @(xOffset));
	DLog(@"Current Page: %@", @(currentPage));
	if (xOffset >= currentPage + SCREEN_WIDTH_PORTRAIT) {
		// User scrolled to right.
		//[self scrollEffectInDirection:kScrollDirectionRight];
		[self scrollToPageAtIndex:currentPage + 1 animate:YES];
		_pageControl.currentPage = 1;
	}else if (xOffset <= currentPage - SCREEN_WIDTH_PORTRAIT) {
		// User scrolled to left.
		//[self scrollEffectInDirection:kScrollDirectionLeft];
		[self scrollToPageAtIndex:currentPage - 1 animate:YES];
		_pageControl.currentPage = 0;
	}else{
		[self scrollToPageAtIndex:_pageControl.currentPage animate:YES];
	}
}
@end
