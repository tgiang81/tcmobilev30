//
//  MarketTableViewCell.m
//  TC_Mobile30
//
//  Created by Kaka on 2/12/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MarketTableViewCell.h"
#import "TCBaseButton.h"
#import "IndicesModel.h"
#import "NSNumber+Formatter.h"
#import "TCLineChartView.h"
#import "DataManager.h"
#import "VertxConnectionManager.h"
#import "GCDQueue.h"

@interface MarketTableViewCell(){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet TCLineChartView *lineChart;

//Data
@property (strong, nonatomic) IndicesModel *indices;
@property (assign, nonatomic) BOOL didLoadChart;

@end

@implementation MarketTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	self.lineChart.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - Main
- (void)setupDataFrom:(IndicesModel *)model forMarket:(NSString *)marketName{
	if (model) {
		self.indices = model;
		_lblName.text = model.name;
		_lblExchange.text = marketName? marketName : @"--";
		_lblLastPrice.text = [model.curPrice toCurrencyNumber];
		float close = model.closePrice.floatValue;
		float change  = model.curPrice.floatValue - close;
		float changeP;
		if (close <= 0) {
			changeP = 0;
		}else{
			changeP = change / close * 100;
		}
		NSString *valueChange = [SettingManager shareInstance].isVisibleChangePer ? [[NSNumber numberWithFloat:changeP] toPercentWithSign] : [[NSNumber numberWithFloat:change] toChangedStringWithSign];
		[_btnValueChange setTitle:valueChange forState:UIControlStateNormal];
		//Color
		UIColor *colorChanged = [[NSNumber numberWithFloat:changeP] colorByCompareToPrice:0];
		[_btnValueChange setBackgroundColor:colorChanged];
		//Load Chart
		[self getDataChart];
	}
}

#pragma mark - Action

- (IBAction)onSwitchValueAction:(id)sender {
	//Post Notification to switch value
	[SettingManager shareInstance].isVisibleChangePer = ![SettingManager shareInstance].isVisibleChangePer;
	[[SettingManager shareInstance] save];
	[[NSNotificationCenter defaultCenter] postNotificationName:kSwitchValueChangedNotification object:nil];
}

#pragma mark - CHART
//API
- (void)getDataChart{
	//Config chart
	float close = self.indices.closePrice.floatValue;
	float change  = self.indices.curPrice.floatValue - close;
	float changeP;
	if (close <= 0) {
		changeP = 0;
	}else{
		changeP = change / close * 100;
	}
	UIColor *lineChartColor = (changeP >= 0) ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	
	NSString *exchange = [UserPrefConstants singleton].currentExchangeData.feed_exchg_code;
	[self.lineChart loadChart:TCLineChartType_Day forCode:self.indices.stockCode inExchange:exchange includeLastValue:self.indices.curPrice chartColor:lineChartColor];
}

//- (void)callApiGetChartFrom:(NSString *)code{
//	//Random delay
//	int NUMBER = ((float)rand() / RAND_MAX) * 3 + 1;
//	float randomNum = ((float)rand() / RAND_MAX) * NUMBER;
//	//Create custom Queue
//	//dispatch_queue_t chartQueue = dispatch_queue_create("com.indiceschart.queue", DISPATCH_QUEUE_CONCURRENT); //
//	[[GCDQueue globalQueue] queueBlock:^{
//		NSString *exchange = [UserPrefConstants singleton].currentExchangeData.feed_exchg_code;
//		[[VertxConnectionManager singleton] getDefaultWeekChartDataFrom:code inExchange:exchange completion:^(id result, NSError *error) {
//			if (result) {
//				NSArray *dataArr = (NSArray *)result;
//				if (dataArr.count == 0) {
//					return ;
//				}
//				NSDictionary *info = dataArr.firstObject;
//				//Check whether data is correct
//				if (![info[@"0"] isEqualToString:code]) {
//					return;
//				}
//				if (self.didLoadChart == YES) {
//					return ;
//				}
//				self.didLoadChart = YES;
//				NSArray *chartArr = [dataArr subarrayWithRange:NSMakeRange(1, dataArr.count - 1)]; //From index 1, because the first item is stock info item
//				dispatch_async(dispatch_get_main_queue(), ^{
//					[self drawChartFromData:chartArr];
//				});
//			}
//		}];
//	} afterDelay:randomNum];
//	[self performSelector:@selector(reloadChart) withObject:nil afterDelay:(randomNum + 1)];
//}
//
//- (void)reloadChart{
////	if (![UserSession shareInstance].didLogin) {
////		return;
////	}
//	if (!self.didLoadChart) {
//		[self getDataChart];
//	}
//}
//
//- (void)justUpdateChartFrom:(NSString *)code{
//	self.didLoadChart = NO;
//	//Call api to reload chart
//	[self callApiGetChartFrom:code];
//}
//
//#pragma mark - CHART UTIL
////Draw chart
//- (void)drawChartFromData:(NSArray *)dataChart{
//	if (!dataChart) {
//		return;
//	}
//	NSMutableArray *xElements = @[].mutableCopy;
//	NSMutableArray *yElements = @[].mutableCopy;
//	for (int i = 0; i < dataChart.count; i++) {
//		NSDictionary *dictData = dataChart[i];
//		[xElements addObject:dictData[@"0"]];
//		[yElements addObject:[NSNumber numberWithDouble:[dictData[@"4"] doubleValue]]];
//	}
//	if (yElements.count == 0) {
//		return;
//	}
//	//Store data
//	[[DataManager shared] storeDataLineCharts:dataChart forKey:self.indices.exStCode];
//	//Add last object
//	[xElements addObject:@"now"];
//	[yElements addObject:self.indices.curPrice];
//	
//	self.lineChart.datasources = [yElements mutableCopy];
//	self.lineChart.xValues = [xElements mutableCopy];
//	
//	//Config chart
//	float close = self.indices.closePrice.floatValue;
//	float change  = self.indices.curPrice.floatValue - close;
//	float changeP;
//	if (close <= 0) {
//		changeP = 0;
//	}else{
//		changeP = change / close * 100;
//	}
//	UIColor *lineChartColor = (changeP >= 0) ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
//	self.lineChart.lineColor = lineChartColor;
//	self.didLoadChart = YES;
//	[self.lineChart reloadChart];
//}

@end
