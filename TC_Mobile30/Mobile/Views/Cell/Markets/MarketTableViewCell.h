//
//  MarketTableViewCell.h
//  TC_Mobile30
//
//  Created by Kaka on 2/12/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MarqueeLabel.h"

@class TCBaseButton;
@class IndicesModel;
@interface MarketTableViewCell : BaseTableViewCell{
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblExchange;

@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnValueChange;

//MAIN
- (void)setupDataFrom:(IndicesModel *)model forMarket:(NSString *)marketName;
@end
