//
//  MarketCell.h
//  TCiPad
//
//  Created by Kaka on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class MarketSummaryView;
@class MarketScoreBoardView;
@interface MarketCell : BaseTableViewCell<UIScrollViewDelegate>{
	
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet MarketScoreBoardView *scoreBoardView;
@property (weak, nonatomic) IBOutlet MarketSummaryView *summaryView;


- (void)populateData;
//Main on Each Market
- (void)reloadSummaryMarket;
- (void)reloadScoreBoardMarket;


@end
