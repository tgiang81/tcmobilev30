//
//  MoreInfoStockCell.h
//  TCiPad
//
//  Created by Kaka on 5/15/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MoreInfoStockCell : BaseTableViewCell{
	
}

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

//Passing data
@property (assign, nonatomic) StockMoreInfoType infoType;

//Main Function
- (void)setDataShouldHighlight:(BOOL)isHighlight;
- (void)updateData:(StockMoreInfoType)infoType;

@end
