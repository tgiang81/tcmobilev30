//
//  StockTypeTwoCell.m
//  TCiPad
//
//  Created by Kaka on 7/30/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockTypeTwoCell.h"

@implementation StockTypeTwoCell


- (void)awakeFromNib {
	[super awakeFromNib];
	// Initialization code
	[self setup];
}

#pragma mark - Main
- (void)setup{
	_chartView.delegate = self;
	self.layer.cornerRadius = 5.0;
	self.layer.borderWidth = 1.0;
	self.layer.borderColor = AppColor_TintColor.CGColor;
	self.layer.masksToBounds = YES;
	[self setupFontAndColor];
}
- (void)setupFontAndColor{
	self.backgroundColor = RGB(69, 98, 114);
}

//======== Using fChange value to update stock =====================
- (void)setupUIFromModel:(StockModel *)model withAnimate:(BOOL)animate {
    //_indexPath = indexPath;

    [self updateData:model useBlinkIfNeed:animate];
    //For Chart
    
    [_chartView setChartType:ChartType_Day];
    _chartView.stCode = model.stockCode;
    _chartView.isOnlyLoadChartByImage = TRUE;

    [_chartView startLoadDefaultChart];
}
//========== Just control data by Model =========
- (void)updateData:(StockModel *)model useBlinkIfNeed:(BOOL)isUsedBlink{
    if (model) {
        
//        self.lblNameOrCode.text = [SettingManager shareInstance].isVisibleNameStock ? model.stockName : [model.stockCode stringByDeletingPathExtension];
//
//        self.lblLastPrice.text = [model.dRefPrice toCurrencyNumber];
//
//        self.lblUpDownPrice.text = [model.fChange toCurrencyNumber];
//
//        [self colorUIFromPriceChange: model.fChange];
		if (isUsedBlink) {
//			[self setAllWhiteColorTextLabelFrom:self];
//			__weak typeof(self) weakSelf = self;
//			[self setAnimateWithPriceChange:model.fChange completion:^{
//				[weakSelf colorUIFromPriceChange:model.fChange];
//			}];
		}
    }
}

#pragma mark - UI By Fchange Value
- (void)colorUIFromPriceChange:(NSNumber *)fChange{
    if (fChange.doubleValue > 0) {
//        self.imgUpDown.image = [UIImage imageNamed:AppIcon_Up_Price];
//        self.lblUpdownPercent.textColor = AppColor_PriceUp_TextColor;
//        self.lblLastPrice.textColor = AppColor_PriceUp_TextColor;
//		 self.lblUpDownPrice.textColor = AppColor_PriceUp_TextColor;
//    }
//    if (fChange.doubleValue < 0) {
//        self.imgUpDown.image = [UIImage imageNamed:AppIcon_Down_Price];
//        self.lblUpdownPercent.textColor = AppColor_PriceDown_TextColor;
//        self.lblLastPrice.textColor = AppColor_PriceDown_TextColor;
//		self.lblUpDownPrice.textColor = AppColor_PriceDown_TextColor;
//    }
//    if (fChange.doubleValue == 0) {
//        self.imgUpDown.image =  [UIImage imageNamed:AppIcon_UnChange_Price];;
//        self.lblUpdownPercent.textColor = AppColor_PriceUnChanged_TextColor;
//        self.lblLastPrice.textColor = AppColor_PriceUnChanged_TextColor;
//		self.lblUpDownPrice.textColor = AppColor_PriceUnChanged_TextColor;
    }
}

#pragma mark - TCChartViewDelegate
- (void)doubleTapChartView:(TCChartView *)chartView{
	if (_delegate) {
		[_delegate doubleTap:self];
	}
}
@end
