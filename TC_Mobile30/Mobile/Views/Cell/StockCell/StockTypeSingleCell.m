//
//  StockTypeSingleCell.m
//  TCiPad
//
//  Created by Kaka on 8/16/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockTypeSingleCell.h"
#import "UILabel+FormattedText.h"
#import "StockModel.h"
#import "NSNumber+Formatter.h"
#import "UIView+Animation.h"
#import "MarqueeLabel.h"
#import "AllCommon.h"
#import "UIImageView+TCUtil.h"
#import "TCBaseButton.h"
#import "VertxConnectionManager.h"
#import "TCLineChartView.h"
#import "ImageCacheManager.h"
#import "DataManager.h"
#import "LanguageKey.h"
#import "GCDQueue.h"
#define kWIDTH_INDEX_VIEW	30
@interface StockTypeSingleCell()<TCLineChartViewDelegate>{
	
}
//Data
@property (assign, nonatomic) BOOL didLoadChart;
//Line Chart
@property (weak, nonatomic) IBOutlet TCLineChartView *lineChart;

//Info View
@property (weak, nonatomic) IBOutlet UILabel *lblInsVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblInsValue;
@property (weak, nonatomic) IBOutlet UILabel *lblInsHigh;
@property (weak, nonatomic) IBOutlet UILabel *lblInsOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblInsLow;
@property (weak, nonatomic) IBOutlet UILabel *lblInsTrade;
//=== Value ====
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblHigh;
@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblLow;
@property (weak, nonatomic) IBOutlet UILabel *lblTrade;

@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UILabel *lblIndex;
//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indexViewConstrainHeight;
@end
@implementation StockTypeSingleCell

//Singleton
+ (NSString *)reuseIdentifier {
	return NSStringFromClass([self class]);
}

+ (NSString *)nibName{
	return NSStringFromClass([self class]);
}

//MAIN
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews{
	[super layoutSubviews];
	//[self setup];
}
#pragma mark - Main
- (void)setup{
	[self setupLanguage];
	_lineChart.backgroundColor = [UIColor clearColor];
}

- (void)updateTheme{
//	if ([[ThemeManager shareInstance] getCurrentTheme] == ThemeType_Light) {
//		return;
//	}
//	self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
//	self.contentView.backgroundColor = [UIColor clearColor];
//	
//	_lblLastPrice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
//	_lblName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
//	_lblCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
//	
//	_lblVolume.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblHigh.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblOpen.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblLow.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblTrade.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	
//	_lblInsVolume.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblInsValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblInsHigh.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblInsOpen.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblInsLow.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
//	_lblInsTrade.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
}

- (void)setupLanguage{
	_lblInsVolume.text = [LanguageManager stringForKey:Vol];
	_lblInsValue.text = [LanguageManager stringForKey:Val];
	_lblInsHigh.text = [LanguageManager stringForKey:Hi];
	_lblInsOpen.text = [LanguageManager stringForKey:Open];
	_lblInsLow.text = [LanguageManager stringForKey:Lo];
	_lblInsTrade.text = [LanguageManager stringForKey:Trd];
//    [_btnViewStockInfo setTitle:[LanguageManager stringForKey:View_Stock_Info] forState:UIControlStateNormal];
}

#pragma mark - CONFIGS
- (void)setIsExpandedCell:(BOOL)isExpandableCell{
	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void) {
		if (!isExpandableCell) {
			self.subInfoView.alpha = 0.0;
		}else{
			self.subInfoView.alpha = 1.0;
		}
	} completion:^(BOOL finished) {
		//Do nothing
	}];
	_isExpandedCell = isExpandableCell;
}

- (void)setShowIndex:(BOOL)showIndex{
	_showIndex = showIndex;
	_indexViewConstrainHeight.constant = self.showIndex? kWIDTH_INDEX_VIEW : 0;
	[self layoutIfNeeded];
}

#pragma mark - Action

- (IBAction)onExtendAction:(id)sender {
	if (_mainDelegate) {
		[_mainDelegate didTapExpandCell:self];
	}
}

- (IBAction)gotoDetailAction:(id)sender {
	if (_mainDelegate) {
		[_mainDelegate shouldGotoDetailFromCell:self];
	}
}
- (IBAction)onSwitchValueAction:(id)sender {
	//Post Notification to switch value
	[SettingManager shareInstance].isVisibleChangePer = ![SettingManager shareInstance].isVisibleChangePer;
	[[SettingManager shareInstance] save];
	[[NSNotificationCenter defaultCenter] postNotificationName:kSwitchValueChangedNotification object:nil];
}

#pragma mark - SetupData
//parse data from model here....
- (void)updateData:(StockModel *)model{
	if (self.indexPath) {
		self.lblIndex.text = [NSString stringWithFormat:@"%@.", @(self.indexPath.row + 1)];
	}
	[self updateTheme];
	if (model) {
		self.stock = model;
		//Update Info
		if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
			_lblCode.text = model.stockName;
			_lblName.text = model.compName;
		}else{
			_lblCode.text = [model.stockCode stringByDeletingPathExtension];
			_lblName.text = model.stockName;
		}
		
		_lblLastPrice.text = [model.lastDonePrice toCurrencyNumber];
		
		NSString *valueChange = [SettingManager shareInstance].isVisibleChangePer ? [model.changePer toPercentWithSign] : [model.fChange toChangedStringWithSign];
		[_btnValueChange setTitle:valueChange forState:UIControlStateNormal];
		
		_lblVolume.text = [model.volume abbreviateNumber];
		_lblValue.text = [model.dValue abbreviateNumber];
		_lblHigh.text = [model.highPrice toCurrencyNumber];
		_lblOpen.text = [model.openPrice toCurrencyNumber];
		_lblLow.text = [model.lowPrice toCurrencyNumber];
		_lblTrade.text = [model.iTrd abbreviateNumber];
		
		//Color UI and Animation
		[self colorUIFromPriceChange:model.changePer];
		
		//Get Data Chart
		[self getDataChart];
	}
}

//+++ do contentView to make all white text label
- (void)setAllWhiteColorTextLabelFrom:(UIView *)contentView{
	if (contentView.subviews.count == 0) {
		return;
	}
	for (id aView in contentView.subviews) {
		if ([aView isKindOfClass:[UILabel class]]) {
			UILabel *label = (UILabel *)aView;
			label.textColor = [UIColor whiteColor];
		}else if ([aView isKindOfClass:[UIView class]]){
			[self setAllWhiteColorTextLabelFrom:aView];
		}else{
			continue;
		}
	}
}

#pragma mark - UI By Fchange Value
- (void)setPriceChangeValue:(NSNumber *)priceChange{
	//+++ Should have sign char
	[_btnValueChange setTitle:[priceChange toPercentWithSign] forState:UIControlStateNormal];
}
- (void)colorUIFromPriceChange:(NSNumber *)changePer{
	if (changePer.doubleValue >= 0) {
		[_btnValueChange setBackgroundColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
	}else{
		[_btnValueChange setBackgroundColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
	}
	/*
	else{
		_imgUpDown.image = [UIImage imageNamed:AppIcon_UnChange_Price];
		_lblLastPrice.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor);
		[_imgUpDown toColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
	}
	*/
}

#pragma mark - CHART
//API
- (void)getDataChart{
	//Config chart
	UIColor *lineChartColor = (self.stock.changePer.doubleValue >= 0) ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	NSString *exchange = [UserPrefConstants singleton].currentExchangeData.feed_exchg_code;

	[self.lineChart loadChart:TCLineChartType_Day forCode:self.stock.stockCode inExchange:exchange includeLastValue:self.stock.lastDonePrice chartColor:lineChartColor];
}

//- (void)callApiGetChartFrom:(StockModel *)model{
//	//Random delay
//	int NUMBER = ((float)rand() / RAND_MAX) * 3 + 1;
//	float randomNum = ((float)rand() / RAND_MAX) * NUMBER;
//	[[GCDQueue globalQueue] queueBlock:^{
//		NSString *exchange = [UserPrefConstants singleton].currentExchangeData.feed_exchg_code;
//		[[VertxConnectionManager singleton] getDefaultWeekChartDataFrom:model.stockCode inExchange:exchange completion:^(id result, NSError *error) {
//			if (result) {
//				NSArray *dataArr = (NSArray *)result;
//				if (dataArr.count == 0) {
//					return ;
//				}
//				NSDictionary *info = dataArr.firstObject;
//				//Check whether data is correct
//				if (![info[@"0"] isEqualToString:self.stock.stockCode]) {
//					return;
//				}
//				if (self.didLoadChart == YES) {
//					return ;
//				}
//				self.didLoadChart = YES;
//				NSArray *chartArr = [dataArr subarrayWithRange:NSMakeRange(1, dataArr.count - 1)]; //From index 1, because the first item is stock info item
//				[[GCDQueue mainQueue] queueBlock:^{
//					[self drawChartFromData:chartArr];
//				}];
//			}
//		}];
//	} afterDelay:randomNum];
//	[self performSelector:@selector(reloadChart) withObject:nil afterDelay:(randomNum + 1)];
//}
//
//- (void)reloadChart{
//	if (![UserSession shareInstance].didLogin) {
//		return;
//	}
//	if (!self.didLoadChart) {
//		[self getDataChart];
//	}
//}
//
//- (void)justUpdateChartFrom:(StockModel *)model{
//	self.didLoadChart = NO;
//	//Call api to reload chart
//	[self callApiGetChartFrom:model];
//}
//
//#pragma mark - CHART UTIL
////Draw chart
//- (void)drawChartFromData:(NSArray *)dataChart{
//	if (!dataChart) {
//		return;
//	}
//	NSMutableArray *xElements = @[].mutableCopy;
//	NSMutableArray *yElements = @[].mutableCopy;
//	for (int i = 0; i < dataChart.count; i++) {
//		NSDictionary *dictData = dataChart[i];
//		[xElements addObject:dictData[@"0"]];
//		[yElements addObject:[NSNumber numberWithDouble:[dictData[@"4"] doubleValue]]];
//	}
//	if (yElements.count == 0) {
//		//self.imgLineChart.image = [self.lineChart snapshotImage];
//		//self.imgLineChart.hidden = NO;
//		//self.lineChart.hidden = YES;
//		return;
//	}
//	//Store data
//	NSString *cache_key = [NSString stringWithFormat:@"%@_%ld", self.stock.stockCode, TCLineChartType_Day];
//	[[DataManager shared] storeDataLineCharts:dataChart forKey:cache_key];
//	//Add last object
//	[xElements addObject:@"now"];
//	[yElements addObject:self.stock.lastDonePrice];
//	
//	//self.lineChart.delegate = self; //+++ No need use this now
//	self.lineChart.hidden = NO;
//	self.lineChart.datasources = [yElements mutableCopy];
//	self.lineChart.xValues = [xElements mutableCopy];
//	
//	//Config chart
//	UIColor *lineChartColor = (self.stock.changePer.doubleValue >= 0) ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
//	self.lineChart.lineColor = lineChartColor;
//	self.didLoadChart = YES;
//	[self.lineChart reloadChart];
//}
//
////Store and show Image Chart
//- (void)storeAndShowImageChart{
//	__block UIImage *imgChart = [self.lineChart snapshotImage];
//	if (imgChart) {
//		[[ImageCacheManager shareInstance] storeImage:imgChart forKey:self.stock.stockCode completion:^{
//			dispatch_async(dispatch_get_main_queue(), ^{
//				self.lineChart.hidden = YES;
//			});
//		}];
//	}
//}
//
//#pragma mark - TCLineChartViewDelegate
//- (void)didFinishLoadChart{
//	if (self.didLoadChart) {
//		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.11 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//			[self storeAndShowImageChart];
//		});
//	}
//}
@end
