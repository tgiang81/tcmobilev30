//
//  StockTypeSingleCell.h
//  TCiPad
//
//  Created by Kaka on 8/16/18.
//  Copyright © 2018 N2N. All rights reserved.
//

//#import "BaseTableViewCell.h"
#import "MGSwipeTableCell.h"

@class TCBaseButton;
@class MarqueeLabel;
@class StockModel;
@class StockTypeSingleCell;

#define kNORMAL_CELL_HEIGHT		64.5
#define kEXPANDED_CELL_HEIGHT	120

@protocol StockTypeSingleCellDelegate <NSObject>
- (void)didTapExpandCell:(StockTypeSingleCell *)cell;
- (void)shouldGotoDetailFromCell:(StockTypeSingleCell *)cell;
@end
@interface StockTypeSingleCell : MGSwipeTableCell{
	
}
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *subInfoView;

@property (weak, nonatomic) IBOutlet UILabel *lblCode;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnValueChange;
@property (weak, nonatomic) IBOutlet UIView *lastView;
//Chart

//Passing data
@property (assign, nonatomic) BOOL isExpandedCell;
@property (assign, nonatomic) BOOL showIndex;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) id <StockTypeSingleCellDelegate> mainDelegate;
@property (strong, nonatomic) StockModel *stock;

//Static Class Function
+ (NSString *)reuseIdentifier;
+ (NSString *)nibName;
//MAIN
- (void)updateData:(StockModel *)model;
////For Chart
//- (void)justUpdateChartFrom:(StockModel *)model;

@end
