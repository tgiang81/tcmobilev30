//
//  MTimeSaleCell.h
//  TCiPad
//
//  Created by Kaka on 7/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"


@class TimeSaleModel;
@interface MTimeSaleCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPI;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblVol;

//Main
- (void)setupDataFrom:(TimeSaleModel *)model comparePrice:(float)refPrice;

@end
