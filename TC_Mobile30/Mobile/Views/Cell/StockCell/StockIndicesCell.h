//
//  StockCollectionViewCell.h
//  TCiPad
//
//  Created by Kaka on 5/9/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@class StockModel;
@interface StockIndicesCell : BaseCollectionViewCell{
	
}
//@property (weak, nonatomic) IBOutlet UILabel *lblName;
//@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//@property (weak, nonatomic) IBOutlet UILabel *lblPercentUpDownPrice;
//@property (weak, nonatomic) IBOutlet UILabel *lblUpDownPrice;

//@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;

//Main
- (void)updateData:(StockModel *)model shouldCheckAnimation:(BOOL)shouldCheck;

@end
