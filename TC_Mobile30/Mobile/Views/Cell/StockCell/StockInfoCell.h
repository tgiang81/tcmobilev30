//
//  StockInfoCell.h
//  TCiPad
//
//  Created by Kaka on 5/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class TCBaseButton;
@class StockInfoCell;
@class StockModel;
@class MarqueeLabel;

@protocol StockInfoCellDelegate <NSObject>
@optional
- (void)didOpenMoreInfo:(StockInfoCell *)cell;
@end

// HEIGHT CELL
#define kSHORT_CELL_HEIGHT	50
#define kFULL_CELL_HEIGHT	90

@interface StockInfoCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UIView *shortView;
@property (weak, nonatomic) IBOutlet UIView *fullView;

//For Full Info View
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPercentUpDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpDownPrice;
//SubInfo
@property (weak, nonatomic) IBOutlet UILabel *lblVol;
@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblVal;
@property (weak, nonatomic) IBOutlet UILabel *lblLo;
@property (weak, nonatomic) IBOutlet UILabel *lblHi;
@property (weak, nonatomic) IBOutlet UILabel *lblTrd;

@property (weak, nonatomic) IBOutlet TCBaseButton *btnInfo;
//======== End Full Info View ===============

//=========== For Short View ================
@property (weak, nonatomic) IBOutlet UILabel *lblNameShort;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceShort;
@property (weak, nonatomic) IBOutlet UILabel *lblPercentUpDownShort;


//Passing data
@property (assign, nonatomic) BOOL isExpandableCell;
@property (weak, nonatomic) id <StockInfoCellDelegate> delegate;
@property (strong, nonatomic) StockModel *stock;

//Main
- (void)updateData:(StockModel *)model shouldCheckAnimation:(BOOL)shouldCheck;

@end
