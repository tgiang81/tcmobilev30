//
//  StockTypeTwoCell.h
//  TCiPad
//
//  Created by Kaka on 7/30/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "TCChartView.h"
#include "StockModel.h"
@class StockTypeTwoCell;
@protocol StockTypeTwoCellDelegate<NSObject>
@optional
- (void)doubleTap:(StockTypeTwoCell *)cell;
@end

@interface StockTypeTwoCell : BaseCollectionViewCell<TCChartViewDelegate>{
	
}

@property (weak, nonatomic) IBOutlet TCChartView *chartView;
@property (weak, nonatomic) id <StockTypeTwoCellDelegate> delegate;

- (void)setupUIFromModel:(StockModel *)model withAnimate:(BOOL)animate;

@end
