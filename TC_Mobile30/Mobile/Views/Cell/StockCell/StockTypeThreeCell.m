//
//  StockTypeThreeCell.m
//  TCiPad
//
//  Created by Kaka on 7/30/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockTypeThreeCell.h"

@implementation StockTypeThreeCell

- (void)awakeFromNib {
	[super awakeFromNib];
	// Initialization code
	[self setup];
}

#pragma mark - Main
- (void)setup{
	self.layer.cornerRadius = 5.0;
	self.layer.borderWidth = 1.0;
	self.layer.borderColor = AppColor_TintColor.CGColor;
	self.layer.masksToBounds = YES;
	//Content
	[self setupFontAndColor];
}
- (void)setupFontAndColor{
	//self.backgroundColor = RGB(69, 98, 114);
	self.backgroundColor = [UIColor clearColor];
	//text color highlight: 133 - 169 - 192
	_lblHighPrice.textColor = [UIColor blackColor];
	_lblLowPrice.textColor = [UIColor blackColor];
	_lblLastPrice.textColor = [UIColor blackColor];
	_lblVolume.textColor = [UIColor blackColor];
	_lblNameOrCode.textColor = [UIColor blackColor];
	
	_lblUpDownPrice.textColor = [UIColor blackColor];
	_lblUpdownPercent.textColor = [UIColor blackColor];
}

//======== Using fChange value to update stock =====================
- (void)setupUIFromModel:(StockModel *)model withAnimate:(BOOL)animate {
    [self updateData:model useBlinkIfNeed:animate];
}
//========== Just control data by Model =========
- (void)updateData:(StockModel *)model useBlinkIfNeed:(BOOL)isUsedBlink{
    if (model) {
        
        self.lblNameOrCode.text = [SettingManager shareInstance].isVisibleNameStock ? model.stockName : [model.stockCode stringByDeletingPathExtension];

        self.lblLastPrice.text = [model.lastDonePrice toCurrencyNumber];
        
        self.lblUpDownPrice.text = [model.fChange toStringWithSign];
        self.lblUpdownPercent.text = [model.changePer toPercentWithSign];
        
		self.lblHighPrice.text =  [NSString stringWithFormat:@"H %@", [model.highPrice toCurrencyNumber]];
        self.lblLowPrice.text =  [NSString stringWithFormat:@"L %@", [model.lowPrice toCurrencyNumber]];
        
        self.lblVolume.text =  [model.volume abbreviateNumber];
        
        [self colorUIFromPriceChange: model.fChange];
		/* +++ not use now
		if (isUsedBlink) {
			[self setAllWhiteColorTextLabelFrom:self];
			__weak typeof(self) weakSelf = self;
			[self setAnimateWithPriceChange:model.fChange completion:^{
				[weakSelf colorUIFromPriceChange:model.fChange];
			}];
		}
		 */
		[self setupFontAndColor];
		[self updateHeatMapFrom:model];
    }
}

#pragma mark - UI By Fchange Value
- (void)colorUIFromPriceChange:(NSNumber *)fChange{
    if (fChange.doubleValue > 0) {
        self.imgUpDown.image = [UIImage imageNamed:AppIcon_Up_Price];
        self.lblUpdownPercent.textColor = AppColor_PriceUp_TextColor;
        self.lblLastPrice.textColor = AppColor_PriceUp_TextColor;
		
		self.lblHighPrice.textColor = AppColor_PriceUp_TextColor;
		self.lblLowPrice.textColor = AppColor_PriceUp_TextColor;
		self.lblUpDownPrice.textColor = AppColor_PriceUp_TextColor;
    }
    if (fChange.doubleValue < 0) {
        self.imgUpDown.image = [UIImage imageNamed:AppIcon_Down_Price];
        self.lblUpdownPercent.textColor = AppColor_PriceDown_TextColor;
        self.lblLastPrice.textColor = AppColor_PriceDown_TextColor;
		
		self.lblHighPrice.textColor = AppColor_PriceDown_TextColor;
		self.lblLowPrice.textColor = AppColor_PriceDown_TextColor;
		self.lblUpDownPrice.textColor = AppColor_PriceDown_TextColor;
    }
    if (fChange.doubleValue == 0) {
        self.imgUpDown.image =  [UIImage imageNamed:AppIcon_UnChange_Price];;
        self.lblUpdownPercent.textColor = AppColor_PriceUnChanged_TextColor;
        self.lblLastPrice.textColor = AppColor_PriceUnChanged_TextColor;
		
		self.lblHighPrice.textColor = AppColor_PriceUnChanged_TextColor;
		self.lblLowPrice.textColor = AppColor_PriceUnChanged_TextColor;
		self.lblUpDownPrice.textColor = AppColor_PriceUnChanged_TextColor;
    }
}


#pragma mark - HeatMap
- (void)updateHeatMapFrom:(StockModel *)model{
	float changePer = [model.changePer floatValue];
	float alpha = fabsf(model.changePer.floatValue / 10);
	if (changePer == 0) {
		self.contentView.backgroundColor = RGB(199, 199, 199);
	}else if (changePer > 0){
		self.contentView.backgroundColor = RGBA(34, 115, 23, alpha);
		//self.contentView.layer.opacity = alpha;
	}else{
		self.contentView.backgroundColor = RGBA(217, 0, 0, alpha);
		//self.contentView.layer.opacity = alpha;
	}
}
@end
