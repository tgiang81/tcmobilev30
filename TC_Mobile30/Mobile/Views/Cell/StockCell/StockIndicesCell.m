//
//  StockCollectionViewCell.m
//  TCiPad
//
//  Created by Kaka on 5/9/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockIndicesCell.h"
#import "UIView+Animation.h"
#import "NSNumber+Formatter.h"
#import "StockModel.h"

static NSString *kICON_UP_PRICE_NAME	= @"up_price_icon";
static NSString *kICON_DOWN_PRICE_NAME	= @"down_price_icon";

@implementation StockIndicesCell

- (void)awakeFromNib {
    [super awakeFromNib];
	//Make default Cell style
	self.layer.borderWidth = 1.0;
	self.layer.borderColor = AppColor_TintColor.CGColor;
	self.layer.cornerRadius = 5.0;
    // Initialization code
	[self setup];
}
#pragma mark - Main
- (void)updateData:(StockModel *)model shouldCheckAnimation:(BOOL)shouldCheck{
//	if (model) {
//		self.lblName.text = model.stockName;
//		self.lblPrice.text = [model.dRefPrice toCurrencyNumber];
//		[self setPercentChangePrice:model.changePer];
//		if (shouldCheck) {
//			[self setAnimateWithPriceChange:model.fChange];
//		}
//	}
}

//Initial setup
- (void)setup{
	//setup
//	self.lblName.font = AppFont_MainFontMediumWithSize(15);
//	self.lblPrice.font = AppFont_MainFontBoldWithSize(15);
//	self.lblPercentUpDownPrice.font = AppFont_MainFontMediumWithSize(10);
//
//	self.lblName.textColor = RGB(230, 231, 232);
//	self.lblPrice.textColor = RGB(230, 231, 232);
//	self.lblPercentUpDownPrice.textColor = COLOR_FROM_HEX(0x87a7c1);
//	self.lblUpDownPrice.textColor = COLOR_FROM_HEX(0x87a7c1);
}

#pragma mark - UTILS
- (void)setPercentChangePrice:(NSNumber *)fChangePer{
//	if (fChangePer.doubleValue > 0) {
//		self.imgUpDown.image = [UIImage imageNamed:AppIcon_Up_Price];
//	}
//	if (fChangePer.doubleValue < 0) {
//		self.imgUpDown.image = [UIImage imageNamed:AppIcon_Down_Price];
//	}
//	if (fChangePer.doubleValue == 0) {
//		self.imgUpDown.image = nil;
//	}
//	self.lblPercentUpDownPrice.text = [NSString stringWithFormat:@"%@ %@", [fChangePer toStringByPointer:[UserPrefConstants singleton].pointerDecimal], @"%"];
}

//Check animate
- (void)setAnimateWithPriceChange:(NSNumber *)changedPrice{
	if (changedPrice.doubleValue > 0) {
		[self.contentView animateBlinkForPriceChanged:YES];
	}
	if (changedPrice.doubleValue < 0) {
		[self.contentView animateBlinkForPriceChanged:NO];
	}
}
@end
