//
//  MStockTypeInfoCell.h
//  TC_Mobile30
//
//  Created by Kaka on 12/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "BTLabel.h"

@class TTTAttributedLabel;
@class StockInfoModel;
@interface MStockInfoTableViewCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *leftInsLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *leftValueLabel;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *rightInsLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *rightValueLabel;

//MAIN
- (void)updateDataFrom:(StockInfoModel *)model byDict:(NSDictionary *)dict;

@end
