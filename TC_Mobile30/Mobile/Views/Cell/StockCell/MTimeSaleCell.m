//
//  MTimeSaleCell.m
//  TCiPad
//
//  Created by Kaka on 7/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MTimeSaleCell.h"
#import "TimeSaleModel.h"
#import "NSNumber+Formatter.h"
#import "NSDate+Utilities.h"

@implementation MTimeSaleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - MAIN
- (void)setup{
	//self.backgroundColor = AppColor_MainBackgroundCellColor;
	_lblTime.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblVol.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	
	_lblPI.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblPrice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
}

//Parsing data
- (void)setupDataFrom:(TimeSaleModel *)model comparePrice:(float)refPrice{
	if (model) {
		//Time
		NSDate *date = [NSDate dateFromString:model.time format:kSERVER_FORMAT_TIME];
		_lblTime.text = date? [date stringWithFormat:kUPDATE_FORMAT_TIME] : @"#";
		if (!date) {
			date = [NSDate dateFromString:model.time format:@"yyyyMMdd"];
			_lblTime.text = date? [date stringWithFormat:@"yyyy-mm-dd"] : @"#";
		}
		
		_lblPI.text = model.PI;
		_lblVol.text = [model.vol abbreviateNumber];
		_lblPrice.text = [model.incomingPrice toCurrencyNumber];
		
		//Check Color - No check now
		_lblPrice.textColor = [model.incomingPrice colorByCompareToPrice:refPrice];
		_lblPI.textColor = [self colorByCheckPIvalue:model.PI];
	}
}

#pragma mark - UTILS
- (UIColor *)colorByCheckPIvalue:(NSString *)piValue{
	if ([piValue isEqualToString:@"b"]) {
		return TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
	}
	if ([piValue isEqualToString:@"s"]) {
		return TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);;
	}
	return TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);;
}
@end
