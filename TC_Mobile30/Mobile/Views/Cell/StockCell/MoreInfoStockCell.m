//
//  MoreInfoStockCell.m
//  TCiPad
//
//  Created by Kaka on 5/15/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MoreInfoStockCell.h"

#define kNormal_Title_Color 		RGB(66, 90, 108)
#define kHighlight_Title_Color 		AppColor_TintColor
@implementation MoreInfoStockCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - MAIN
- (void)setup{
	self.backgroundColor = [UIColor clearColor];
	_lblTitle.font = AppFont_MainFontMediumWithSize(12);
}

//Update Data
- (void)updateData:(StockMoreInfoType)infoType{
	_infoType = infoType;
	[self setDataShouldHighlight:NO];
}

- (void)setDataShouldHighlight:(BOOL)isHighlight{
	_imgIcon.image = [UIImage imageNamed:[self iconNameFrom:_infoType isHighlight:isHighlight]];
	_lblTitle.text = [self titleFrom:_infoType];
	_lblTitle.textColor = isHighlight ? kHighlight_Title_Color : kNormal_Title_Color;
}

#pragma mark - UTILS
- (NSString *)iconNameFrom:(StockMoreInfoType)infoType isHighlight:(BOOL)highlight{
	NSString *iconName = @"";
	switch (infoType) {
		case StockMoreInfoType_Fundamentals:
			iconName = highlight ? @"popup_fundamentals_highlight_icon" : @"popup_fundamentals_icon";
			break;
		case StockMoreInfoType_News:
			iconName = highlight ? @"popup_news_highlight_icon" : @"popup_news_icon";
			break;
		case StockMoreInfoType_Chat:
			iconName = highlight ? @"popup_chat_highlight_icon" : @"popup_chat_icon";
			break;
		case StockMoreInfoType_TimeSale:
			iconName = highlight ? @"popup_timesale_highlight_icon" : @"popup_timesale_icon";
			break;
		case StockMoreInfoType_Close:
			iconName = highlight ? @"close_highlight_icon" : @"close_icon";
			break;
			
		default:
			break;
	}
	return iconName;
}

- (NSString *)titleFrom:(StockMoreInfoType)infoType{
	NSString *title = @"";
	switch (infoType) {
		case StockMoreInfoType_Fundamentals:
			title = [LanguageManager stringForKey:@"Fundamentals"];
			break;
		case StockMoreInfoType_News:
			title = [LanguageManager stringForKey:@"News"];
			break;
		case StockMoreInfoType_Chat:
			title = [LanguageManager stringForKey:@"Chat"];
			break;
		case StockMoreInfoType_TimeSale:
			title = [LanguageManager stringForKey:@"Time&Sale"];
			break;
		case StockMoreInfoType_Close:
			title = [LanguageManager stringForKey:@"Close"];
			break;
		default:
			break;
	}
	return title;
}
@end
