//
//  StockTypeThreeCell.h
//  TCiPad
//
//  Created by Kaka on 7/30/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface StockTypeThreeCell : BaseCollectionViewCell{
	
}

@property (weak, nonatomic) IBOutlet MarqueeLabel *lblNameOrCode;

@property (weak, nonatomic) IBOutlet MarqueeLabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblUpdownPercent;

@property (weak, nonatomic) IBOutlet MarqueeLabel *lblVolume;


@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;

//@property (weak, nonatomic) IBOutlet MarqueeLabel *lblName;
//@property (weak, nonatomic) IBOutlet MarqueeLabel *lblPrice;
//@property (weak, nonatomic) IBOutlet UILabel *lblPercentUpDownPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblUpDownPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblHighPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblLowPrice;

//Main
- (void)setupUIFromModel:(StockModel *)model withAnimate:(BOOL)animate;

@end
