//
//  QuoteDetailCellCollectionViewCell.h
//  TCiPad
//
//  Created by conveo cutoan on 3/29/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "N2PercentLabel.h"

@interface StockDetaiCell : UICollectionViewCell
{
    
}

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet N2PercentLabel *bdBuyPercent;
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;

@end
