//
//  StockHeatmapCell.m
//  TC_Mobile30
//
//  Created by Kaka on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "StockHeatmapCell.h"
#import "MarqueeLabel.h"
#import "UIView+ShadowBorder.h"
#import "NSString+SizeOfString.h"
#import "Utils.h"

#define kMAX_STOCK_NAME_HEIGHT	30
@interface StockHeatmapCell(){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UILabel *lblNameOrCode;

@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblChangedValue;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelConstrainHeight;

@end

@implementation StockHeatmapCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}
- (void)setup{
	//self.layer.borderWidth = 1.0;
	//self.layer.borderColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor).CGColor;
	//self.layer.masksToBounds = YES;
	[self makeBorderShadow];
	self.layer.cornerRadius = 5.0;
	//Content
	//[self setupFontAndColor];
}
#pragma mark - Main
- (void)setupDataFrom:(StockModel *)model{
	self.layer.borderColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor).CGColor;
	if (model) {
		//self.lblNameOrCode.text = [SettingManager shareInstance].isVisibleNameStock ? model.stockName : [model.stockCode stringByDeletingPathExtension];
		self.lblNameOrCode.text = [Utils stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:model.stockName];
		self.lblLastPrice.text = [model.lastDonePrice toCurrencyNumber];
		//Changed Value
		NSString *priceChanged = [model.fChange toStringWithSign];
		NSString *percentChanged = [model.changePer toPercentWithSign];
		_lblChangedValue.text = [NSString stringWithFormat:@"%@ (%@)", priceChanged, percentChanged];
		[self updateHeightOfNameLabel];
		//Heatmap
		[self setHeatmapColorFrom:model];
	}
}

- (void)updateHeightOfNameLabel{
	float height = [_lblNameOrCode.text heightForWidth:_lblNameOrCode.frame.size.width usingFont:_lblNameOrCode.font];
	if (height > kMAX_STOCK_NAME_HEIGHT) {
		height = kMAX_STOCK_NAME_HEIGHT;
	}
	if (height < 15) {
		height = 15;
	}
	_nameLabelConstrainHeight.constant = height;
	[self.contentView layoutIfNeeded];
}

#pragma mark - HeatMap
- (void)setHeatmapColorFrom:(StockModel *)model{
	float changePer = [model.changePer floatValue];
	float alpha = fabsf(changePer / 100);
	alpha = (alpha > 1) ? 1.0 : alpha;
	UIColor *heatColor;
	CGFloat  h, s, b, a2 = 1.0;
	if (changePer == 0) {
		[[UIColor whiteColor] getHue:&h saturation:&s brightness:&b alpha:&a2];
		heatColor = RGB(255, 255, 255);
	}else if (changePer > 0){
		UIColor *upColor = ([[SettingManager shareInstance].upPriceColor isEqualToString:kUP_COLOR_PRICE])? [UIColor greenColor] : [UIColor redColor];
		[upColor getHue:&h saturation:&s brightness:&b alpha:&a2];
		heatColor = [UIColor colorWithHue:h saturation:alpha brightness:b alpha:1.0];//H: 120
	}else{
		UIColor *downColor = ([[SettingManager shareInstance].downPriceColor isEqualToString:kDOWN_COLOR_PRICE])? [UIColor redColor] : [UIColor greenColor];
		[downColor getHue:&h saturation:&s brightness:&b alpha:&a2];
		heatColor = [UIColor colorWithHue:h saturation:alpha brightness:b alpha:1.0]; //H = 359
	}
	self.contentView.layer.backgroundColor = heatColor.CGColor;
}
@end
