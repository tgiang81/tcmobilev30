//
//  StockHeatmapCell.h
//  TC_Mobile30
//
//  Created by Kaka on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface StockHeatmapCell : BaseCollectionViewCell{
	
}
//MAIN
- (void)setupDataFrom:(StockModel *)model;

@end
