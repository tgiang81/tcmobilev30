//
//  MMarketDepthCell.m
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MMarketDepthCell.h"
#import "MarketDepthModel.h"

#import "NSNumber+Formatter.h"

@implementation MMarketDepthCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setup{
	_lblNo.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblAsk.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblBid.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblAskQty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblBuySplit1.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblBidQty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
}
//======== Parse Data to UI =========
- (void)setupDataFrom:(MarketDepthModel *)model comparePrice:(float)refPrice atIndexPath:(NSIndexPath *)indexPath{
	if (model) {
		_lblNo.text = [NSString stringWithFormat:@"%@", @(indexPath.row + 1)];
		_lblBuySplit1.text = model.buySplit1;
		_lblBidQty.text = [model.buyQty abbreviateNumber];
		
		_lblSellSplit1.text = model.sellSplit1;
		_lblAskQty.text = [model.sellQty abbreviateNumber];
		
		//Just get decimal number
		_lblBid.text = [model.buyPrice toDecimaWithPointer:3];
		_lblAsk.text = [model.sellPrice toDecimaWithPointer:3];
		
		//Check Color for Price label
		_lblBid.textColor = [model.buyPrice colorByCompareToPrice:refPrice]; //[self colorForTextFrom:model.buyPrice.floatValue refPrice:refPrice];
		_lblAsk.textColor = [model.sellPrice colorByCompareToPrice:refPrice];;
	}
}
@end
