//
//  StockInfoCell.m
//  TCiPad
//
//  Created by Kaka on 5/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockInfoCell.h"
#import "TCBaseButton.h"
#import "UILabel+FormattedText.h"
#import "StockModel.h"
#import "NSNumber+Formatter.h"
#import "UIView+Animation.h"
#import "MarqueeLabel.h"

@implementation StockInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
#pragma mark - MAIN
- (void)setup{
	_shortView.alpha = 0.0;
	_fullView.alpha = 0.0;
	
	_shortView.backgroundColor = [UIColor clearColor];
	_fullView.backgroundColor = [UIColor clearColor];
	[_btnInfo setShowsTouchWhenHighlighted:NO];
	//Color
	self.backgroundColor = AppColor_MainBackgroundCellColor;
	_lblName.textColor = AppColor_MainTextColor;
	_lblPrice.textColor = AppColor_MainTextColor;
	_lblVol.textColor = AppColor_MainTextColor;
	_lblVal.textColor = AppColor_MainTextColor;
	_lblOpen.textColor = AppColor_MainTextColor;
	_lblHi.textColor = AppColor_MainTextColor;
	_lblLo.textColor = AppColor_MainTextColor;
	_lblTrd.textColor = AppColor_MainTextColor;
	//On Short View
	_lblNameShort.textColor = AppColor_MainTextColor;
	_lblPriceShort.textColor = RGB(237, 101, 48);
	_lblPercentUpDownShort.textColor = RGB(237, 101, 48);
	
	//Font
	_lblName.font = AppFont_MainFontMediumWithSize(15);
	_lblPrice.font = AppFont_MainFontBoldWithSize(15);
	_lblPercentUpDown.font = AppFont_MainFontMediumWithSize(12);
	_lblVol.font = AppFont_MainFontBoldWithSize(10);
	_lblVal.font = AppFont_MainFontBoldWithSize(10);
	_lblOpen.font = AppFont_MainFontBoldWithSize(10);
	_lblHi.font = AppFont_MainFontBoldWithSize(10);
	_lblLo.font = AppFont_MainFontBoldWithSize(10);
	_lblTrd.font = AppFont_MainFontBoldWithSize(10);
	//On Short View
	_lblNameShort.font = AppFont_MainFontMediumWithSize(12);
	_lblPriceShort.font = AppFont_MainFontRegularWithSize(15);
	_lblPercentUpDownShort.font = AppFont_MainFontRegularWithSize(15);
}

//parse data from model here....
- (void)updateData:(StockModel *)model shouldCheckAnimation:(BOOL)shouldCheck{
	if (model) {
		_stock = model;
		//Parse value
		_lblName.text = model.stockName;
		_lblNameShort.text = model.stockName;
		
		_lblPrice.text = [model.lastDonePrice toCurrencyNumber];
		_lblPriceShort.text = _lblPrice.text;
		[self setPercentChangePrice:model.changePer];
		
		//Sortcut
		[self setVol:[model.volume abbreviateNumber]];
		[self setVal:[model.dValue abbreviateNumber]];
		[self setTrd:[model.iTrd abbreviateNumber]];
		//Group
		[self setHi:[model.highPrice toCurrencyNumber]];
		[self setLo:[model.lowPrice toCurrencyNumber]];
		[self setOpen:[model.openPrice toCurrencyNumber]];
		
		//Check Animate
		if (shouldCheck) {
			[self setAnimateWithPriceChange:model.fChange];
		}
	}
}

#pragma mark - UTILS
- (void)setIsExpandableCell:(BOOL)isExpandableCell{
	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void) {
		if (!isExpandableCell) {
			_shortView.alpha = 1.0;
			_fullView.alpha = 0.0;
		}else{
			_fullView.alpha = 1.0;
			_shortView.alpha = 0.0;
		}
	} completion:^(BOOL finished) {
		//Do nothing
	}];
	_isExpandableCell = isExpandableCell;
}

//====== Set Data ===========
- (void)setPercentUpDownValue:(NSString *)value isUp:(BOOL)isUp{
	_lblPercentUpDown.text = value;
	if (isUp) {
		_lblPercentUpDown.textColor = AppColor_PriceUp_TextColor;
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Up_Price];
	}else{
		_lblPercentUpDown.textColor = AppColor_PriceDown_TextColor;
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Down_Price];
	}
}
- (void)setPercentChangePrice:(NSNumber *)fChangePer{
	if (fChangePer.doubleValue > 0) {
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Up_Price];
		_lblPercentUpDown.textColor = AppColor_PriceUp_TextColor;
		
		//For Short
		_lblPriceShort.textColor = AppColor_PriceUp_TextColor;
		_lblPercentUpDownShort.textColor = AppColor_PriceUp_TextColor;
	}
	if (fChangePer.doubleValue < 0) {
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Down_Price];
		_lblPercentUpDown.textColor = AppColor_PriceDown_TextColor;
		
		//For Short
		_lblPriceShort.textColor = AppColor_PriceDown_TextColor;
		_lblPercentUpDownShort.textColor = AppColor_PriceDown_TextColor;
	}
	if (fChangePer.doubleValue == 0) {
		_imgUpDownPrice.image = nil;
		_lblPercentUpDown.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor);
		
		//For Short
		_lblPriceShort.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor);
		_lblPercentUpDownShort.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor);
	}
	_lblPercentUpDown.text = [NSString stringWithFormat:@"%@ %@", [fChangePer toStringByPointer:[UserPrefConstants singleton].pointerDecimal], @"%"];
	_lblPercentUpDownShort.text = _lblPercentUpDown.text;
}

- (void)setVol:(NSString *)vol{
	_lblVol.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"vol:"], vol];
	[_lblVol colorSubString:[LanguageManager stringForKey:@"vol:"] withColor:AppColor_HighlightTextColor];
}

- (void)setVal:(NSString *)val{
	_lblVal.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"val:"], val];
	[_lblVal colorSubString:[LanguageManager stringForKey:@"val:"] withColor:AppColor_HighlightTextColor];
}

- (void)setOpen:(NSString *)open{
	_lblOpen.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Open:"], open];
	[_lblOpen colorSubString:[LanguageManager stringForKey:@"Open:"] withColor:AppColor_HighlightTextColor];
}

- (void)setHi:(NSString *)hi{
	_lblHi.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Hi:"], hi];
	[_lblHi colorSubString:[LanguageManager stringForKey:@"Hi:"] withColor:AppColor_HighlightTextColor];
}

- (void)setLo:(NSString *)lo{
	_lblLo.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Lo:"], lo];
	[_lblLo colorSubString:[LanguageManager stringForKey:@"Lo:"] withColor:AppColor_HighlightTextColor];
}

- (void)setTrd:(NSString *)trd{
	_lblTrd.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"trd:"], trd];
	[_lblTrd colorSubString:[LanguageManager stringForKey:@"trd:"] withColor:AppColor_HighlightTextColor];
}

//Check animate
- (void)setAnimateWithPriceChange:(NSNumber *)changedPrice{
	if (changedPrice.doubleValue > 0) {
		[self.contentView animateBlinkForPriceChanged:YES];
	}
	if (changedPrice.doubleValue < 0) {
		[self.contentView animateBlinkForPriceChanged:NO];
	}
}

#pragma mark - Action
- (IBAction)onMoreInfoAction:(id)sender {
	if (_delegate) {
		[_delegate didOpenMoreInfo:self];
	}
}
@end
