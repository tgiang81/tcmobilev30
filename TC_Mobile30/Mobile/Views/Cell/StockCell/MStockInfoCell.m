//
//  FoundationCell.m
//  TCiPad
//
//  Created by Kaka on 6/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MStockInfoCell.h"
#import "StockInfoModel.h"
#import "NSNumber+Formatter.h"
#import "UILabel+TCUtil.h"

@implementation MStockInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setup{
	self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);//COLOR_FROM_HEX(0x425C6B);
	_lblStatus.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	_lblMktAvgPrice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
//	_lblInsFullName.text = [LanguageManager stringForKey:@"Full Name"];
//	_lblInsIndices.text = [LanguageManager stringForKey:@"Indices"];
//	_lblInsRemark.text = [LanguageManager stringForKey:@"Remark"];
//
//	_lblInsRemark.text = [LanguageManager stringForKey:@"Remark"];
//	_lblInsHight.text = [LanguageManager stringForKey:@"Hight"];
//	_lblInsLow.text = [LanguageManager stringForKey:@"Low"];
//	_lblInsCelling.text = [LanguageManager stringForKey:@"Celling"];
//	_lblInsFloor.text = [LanguageManager stringForKey:@"Floor"];
//	_lblInsLACP.text = [LanguageManager stringForKey:@"LACP"];
//	_lblInsVolume.text = [LanguageManager stringForKey:@"Volume"];
//	_lblInsValue.text = [LanguageManager stringForKey:@"Value"];
//	_lblInsTotBuyVol.text = [LanguageManager stringForKey:@"Tot Buy Vol"];
//	_lblInsTotSellVol.text = [LanguageManager stringForKey:@"Tot Sell Vol"];
//
//	_lblInsLotSize.text = [LanguageManager stringForKey:@"Lot Size"];
//	_lblInsShareIssued.text = [LanguageManager stringForKey:@"Share Issued"];
//	_lblIns_Mkt_Cap.text = [LanguageManager stringForKey:@"Mkt Cap"];
}

- (void)setupDataFrom:(StockInfoModel *)model{
	if (model) {
		NSString *sector = [[model.sectorName componentsSeparatedByString:@"|"] objectAtIndex:4];
		[_lblSector setValueText:sector];
		[_lblIndices setValueText:model.indices];
		[_lblISIN setValueText:model.ISINString];
		
		NSString *tradingBoard = nil;
		if ([[model.tradingBoard componentsSeparatedByString:@"|"] count] >3){
			tradingBoard = [[model.tradingBoard componentsSeparatedByString:@"|"] objectAtIndex:3];
		}
		[_lblTradingBoard setValueText:tradingBoard];
		[_lblLotSize setValueText:model.lotSize];
		[_lblCurrenCy setValueText:model.currency];
		
		_lblVolume.text = [model.volume abbreviateNumber];
		_lblValue.text = [model.value abbreviateNumber];
		_lblTrades.text = [model.trade toDecimaWithPointer:0];
		
		_lblOpen.text = [model.open toCurrencyNumber];
		_lblHigh.text = [model.highPrice toCurrencyNumber];
		_lblLow.text = [model.lowPrice toCurrencyNumber];
		
		[_lblSharedIssued setValueText:[model.totalShareIssued abbreviateNumber]];
		[_lblMktCap setValueText:[model.totalMktCap abbreviateNumber]];
		[_lblParValue setValueText:[model.parValue abbreviateNumber]];
		[_lblCeiling setValueText:[model.ceil toCurrencyNumber]];
		[_lblFloor setValueText:[model.floor toCurrencyNumber]];
		[_lblEPS setValueText:[model.eps toCurrencyNumber]];
		[_lblPE_Ratio setValueText:[model.peRatio toCurrencyNumber]]; //?Key
		[_lblNTA setValueText:[model.nta toCurrencyNumber]]; //%//?Key
		[_lblDividendYield setValueText:[model.dividendYield toCurrencyNumber]];
		[_lblCPF setValueText:model.cpf];
		[_lblDeliveryBasis setValueText:model.deliveryBasis];
		[_lblForeignLimit setValueText:[model.foreignLimit toCurrencyNumber]];
		[_lblForeign setValueText:[model.foreignLimit toCurrencyNumber]];
		[_lblOwnership setValueText:[model.ownerLimit toCurrencyNumber]];

		//RIGHT
		if (model.status.length > 1) {
			unichar uc = [model.status characterAtIndex:1];
			NSString *status = [[UserPrefConstants singleton] getBoardStatus:[NSString stringWithCharacters:&uc length:1]];
			[_lblStatus setValueText:status];
		}else{
			_lblStatus.text = @"-";
		}
		
		[_lblBuyRate setValueText:model.buyRate? [[model.buyRate toStringPecent] stringByAppendingString:@"%"] : @""];
		
		[_lblTot_B_Vol setValueText:model.totalBuyVol? [model.totalBuyVol abbreviateNumber] : @""];
		[_lblTot_S_Vol setValueText:model.totalSellVol? [model.totalSellVol abbreviateNumber] : @""];
		[_lblTot_S_Sell_Vol setValueText:model.totalShortSellVol? [model.totalShortSellVol abbreviateNumber] : @""];
		[_lblTot_B_Trans setValueText:model.totalBuyTran? [model.totalBuyTran abbreviateNumber] : @""];
		[_lblTot_S_Trans setValueText:model.totalSellTran? [model.totalSellTran abbreviateNumber] : @""];
		[_lblDaySpread setValueText:model.daySpread];//?Key
		[_lblMktAvgPrice setValueText:model.mktAVGPrice? [model.mktAVGPrice toCurrencyNumber] : @""];//?Key
		[_lblDynamicHigh setValueText:model.highPrice? [model.highPrice toCurrencyNumber] : @""];
		[_lblDynamicLow setValueText:model.lowPrice? [model.lowPrice toCurrencyNumber] : @""];
		[_lbl52Wks_High setValueText:model.wks_52_High? [model.wks_52_High toCurrencyNumber] : @""];//?Key
		[_lbl52Wks_Low setValueText:model.wks_52_Low? [model.wks_52_Low toCurrencyNumber] : @""];//?Key
		[_lbl52Wks_Spread setValueText:model.wks_52_Spread? [model.wks_52_Spread toCurrencyNumber] : @""]; //?Key
		[_lblRemarks setValueText:model.remark];
		[_lblType setValueText:model.type];

		[_lblIDSS_Tot_Vol setValueText:model.idssTotVol? [model.idssTotVol abbreviateNumber] : @""];
		[_lblIDSS_Tot_Val setValueText:model.idssTotVal? [model.idssTotVal abbreviateNumber] : @""];
	}
}
@end
