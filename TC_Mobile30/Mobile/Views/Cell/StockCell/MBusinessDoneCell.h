//
//  MBusinessDoneCell.h
//  TCiPad
//
//  Created by Kaka on 7/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"
@class TCProgressBar;
@class BusinessDoneModel;
@interface MBusinessDoneCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBVol;
@property (weak, nonatomic) IBOutlet UILabel *lblSVol;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyPercent;
@property (weak, nonatomic) IBOutlet UILabel *lblTVol;
@property (weak, nonatomic) IBOutlet UILabel *lblTVal;

@property (weak, nonatomic) IBOutlet TCProgressBar *buyPercentProgress;

//MAIN
- (void)setupDataFrom:(BusinessDoneModel *)model checkPrice:(float)refPrice;

@end
