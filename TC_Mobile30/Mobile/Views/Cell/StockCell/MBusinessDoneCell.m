//
//  MBusinessDoneCell.m
//  TCiPad
//
//  Created by Kaka on 7/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MBusinessDoneCell.h"
#import "BusinessDoneModel.h"
#import "TCProgressBar.h"

#import "NSNumber+Formatter.h"
#import "ThemeManager.h"
@implementation MBusinessDoneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setup{
	_buyPercentProgress.backgroundColor = [UIColor redColor];//TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);//RGB(191, 61, 141);
	_buyPercentProgress.progressColor = RGB(68, 184, 145);//TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
	_lblPrice.textColor = [UIColor whiteColor];
}

//Parsing data to UI
- (void)setupDataFrom:(BusinessDoneModel *)model checkPrice:(float)refPrice{
	if (model) {
		[self setup];
		_lblPrice.text = [model.price toCurrencyNumber];
		_lblBVol.text = [model.bVol abbreviateNumber];
		_lblSVol.text = [model.sVol abbreviateNumber];
		
		//Not use now
		//_lblBuyPercent.text  = [NSString stringWithFormat:@"%.2f%%", (model.buyPercent*100.0)];
		//_buyPercentProgress.value = model.buyPercent;
		
		//_lblTVol.text = [model.tVol abbreviateNumber];
		//END Not use
		_lblTVal.text = [model.tVal abbreviateNumber];
		
		//Color by check to ref price
		_lblPrice.textColor = [model.price colorByCompareToPrice:refPrice];
	}
}
@end
