//
//  FoundationCell.h
//  TCiPad
//
//  Created by Kaka on 6/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@class StockInfoModel;
@interface MStockInfoCell: BaseTableViewCell{
	
}
//Values
@property (weak, nonatomic) IBOutlet UILabel *lblSector;
@property (weak, nonatomic) IBOutlet UILabel *lblIndices;

@property (weak, nonatomic) IBOutlet UILabel *lblISIN;

@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblHigh;
@property (weak, nonatomic) IBOutlet UILabel *lblLow;
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTrades;

@property (weak, nonatomic) IBOutlet UILabel *lblTradingBoard;
@property (weak, nonatomic) IBOutlet UILabel *lblLotSize;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrenCy;
@property (weak, nonatomic) IBOutlet UILabel *lblSharedIssued;
@property (weak, nonatomic) IBOutlet UILabel *lblMktCap;
@property (weak, nonatomic) IBOutlet UILabel *lblParValue;
@property (weak, nonatomic) IBOutlet UILabel *lblCeiling;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@property (weak, nonatomic) IBOutlet UILabel *lblEPS;
@property (weak, nonatomic) IBOutlet UILabel *lblPE_Ratio;
@property (weak, nonatomic) IBOutlet UILabel *lblNTA;
@property (weak, nonatomic) IBOutlet UILabel *lblDividendYield;
@property (weak, nonatomic) IBOutlet UILabel *lblCPF;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryBasis;
@property (weak, nonatomic) IBOutlet UILabel *lblForeignLimit;
@property (weak, nonatomic) IBOutlet UILabel *lblForeign;
@property (weak, nonatomic) IBOutlet UILabel *lblOwnership;

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyRate;
@property (weak, nonatomic) IBOutlet UILabel *lblTot_B_Vol;
@property (weak, nonatomic) IBOutlet UILabel *lblTot_S_Vol;
@property (weak, nonatomic) IBOutlet UILabel *lblTot_S_Sell_Vol;
@property (weak, nonatomic) IBOutlet UILabel *lblTot_B_Trans;
@property (weak, nonatomic) IBOutlet UILabel *lblTot_S_Trans;
@property (weak, nonatomic) IBOutlet UILabel *lblDaySpread;
@property (weak, nonatomic) IBOutlet UILabel *lblMktAvgPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamicHigh;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamicLow;
@property (weak, nonatomic) IBOutlet UILabel *lbl52Wks_High;
@property (weak, nonatomic) IBOutlet UILabel *lbl52Wks_Low;
@property (weak, nonatomic) IBOutlet UILabel *lbl52Wks_Spread;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarks;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UILabel *lblIDSS_Tot_Vol;
@property (weak, nonatomic) IBOutlet UILabel *lblIDSS_Tot_Val;


////Instruction
//@property (weak, nonatomic) IBOutlet UILabel *lblInsFullName;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsIndices;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblInsHight;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsLow;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsCelling;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsFloor;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsRemark;
//
//
//@property (weak, nonatomic) IBOutlet UILabel *lblInsLACP;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsVolume;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsValue;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsTotBuyVol;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsTotSellVol;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsLotSize;
//@property (weak, nonatomic) IBOutlet UILabel *lblInsShareIssued;
//@property (weak, nonatomic) IBOutlet UILabel *lblIns_Mkt_Cap;
//
//
////Value
//@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
//@property (weak, nonatomic) IBOutlet UILabel *lblIndices;
//@property (weak, nonatomic) IBOutlet UILabel *lblHight;
//@property (weak, nonatomic) IBOutlet UILabel *lblLow;
//@property (weak, nonatomic) IBOutlet UILabel *lblCelling;
//@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
//@property (weak, nonatomic) IBOutlet UILabel *lblRemark;
//
//
//@property (weak, nonatomic) IBOutlet UILabel *lblLACP;
//@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
//@property (weak, nonatomic) IBOutlet UILabel *lblValue;
//
//@property (weak, nonatomic) IBOutlet UILabel *lblTotBuyVol;
//@property (weak, nonatomic) IBOutlet UILabel *lblTotSellVol;
//@property (weak, nonatomic) IBOutlet UILabel *lblLotSize;
//@property (weak, nonatomic) IBOutlet UILabel *lblShareIssued;
//@property (weak, nonatomic) IBOutlet UILabel *lbl_Mkt_Cap;

//MAIN
- (void)setupDataFrom:(StockInfoModel *)model;

@end
