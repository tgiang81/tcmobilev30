//
//  MStockTypeInfoCell.m
//  TC_Mobile30
//
//  Created by Kaka on 12/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MStockInfoTableViewCell.h"
#import "StockInfoModel.h"
#import "UILabel+TCUtil.h"
#import "LanguageKey.h"
#import "NSNumber+Formatter.h"
#import "TTTAttributedLabel.h"

@interface MStockInfoTableViewCell(){
	
}
@property (strong, nonatomic) StockInfoModel *stockInfo;
@end
@implementation MStockInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

#pragma mark - Main
- (void)updateDataFrom:(StockInfoModel *)model byDict:(NSDictionary *)dict{
	if (model) {
		self.stockInfo = model;
		//Left
		MInfoContentType leftType = [dict[@"left"] integerValue];
		[self updateUIFrom:leftType forInsLabel:_leftInsLabel forInsValue:_leftValueLabel];
		//Right
		MInfoContentType rightType = [dict[@"right"] integerValue];
		[self updateUIFrom:rightType forInsLabel:_rightInsLabel forInsValue:_rightValueLabel];
		
		//Alignment
		_leftInsLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
		_leftValueLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
		_rightInsLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
		_rightValueLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
	}
}

#pragma mark - Util
- (void)updateUIFrom:(MInfoContentType)type forInsLabel:(UILabel *)insLabel forInsValue:(UILabel *)valueLabel{
	switch (type) {
		case MInfoContentType_Unknown:{
			insLabel.text = @"";
			valueLabel.text = @"";
		}
			break;
		case MInfoContentType_Sector:{
			insLabel.text = [LanguageManager stringForKey:Sector];
			//Value
			[valueLabel setValueText:_stockInfo.sectorName];
		}
			break;
		case MInfoContentType_Indices:{
			insLabel.text = [LanguageManager stringForKey:_Indices];
			//Value
			[valueLabel setValueText:_stockInfo.indices];
		}
			break;
		case MInfoContentType_ISIN:{
			insLabel.text = [LanguageManager stringForKey:ISIN];
			//Value
			[valueLabel setValueText:_stockInfo.ISINString];
		}
			break;
		case MInfoContentType_Status:{
			insLabel.text = [LanguageManager stringForKey:Status];
			//Value
			[valueLabel setValueText:_stockInfo.status];
		}
			break;
		case MInfoContentType_Open:{
			insLabel.text = [LanguageManager stringForKey:Open];
			//Value
			valueLabel.text = [_stockInfo.open toCurrencyNumber];
		}
			break;
		case MInfoContentType_Volume:{
			insLabel.text = [LanguageManager stringForKey:Volume];
			//Value
			
			[valueLabel setValueText:_stockInfo.volume? [_stockInfo.volume abbreviateNumber] : @""];
		}
			break;
		case MInfoContentType_HighPrice:{
			insLabel.text = [LanguageManager stringForKey:High];
			//Value
			[valueLabel setValueText:[_stockInfo.highPrice toCurrencyNumber]];
		}
			break;
		case MInfoContentType_Value:{
			insLabel.text = [LanguageManager stringForKey:Value];
			//Value
			[valueLabel setValueText:_stockInfo.value? [_stockInfo.value abbreviateNumber] : @""];
		}
			break;
		case MInfoContentType_LowPrice:{
			insLabel.text = [LanguageManager stringForKey:Low];
			//Value
			[valueLabel setValueText:[_stockInfo.lowPrice toCurrencyNumber]];
		}
			break;
		case MInfoContentType_Trades:{
			insLabel.text = [LanguageManager stringForKey:Trades];
			//Value
			valueLabel.text = [_stockInfo.trade toDecimaWithPointer:0];
		}
			break;
		case MInfoContentType_TradingBoard:{
			insLabel.text = [LanguageManager stringForKey:Trading_Board];
			//Value
			[valueLabel setValueText:_stockInfo.tradingBoard];
		}
			break;
		case MInfoContentType_BuyRate:{
			insLabel.text = [LanguageManager stringForKey:Buy_Rate];
			//Value
			[valueLabel setValueText:_stockInfo.buyRate? [[_stockInfo.buyRate toStringPecent] stringByAppendingString:@"%"] : @""];
		}
			break;
		case MInfoContentType_ForeignBuy:{
			insLabel.text = [LanguageManager stringForKey:Foreign_Buy];
			//Value
			[valueLabel setValueText:_stockInfo.foreignBuy? [_stockInfo.foreignBuy toCurrencyNumber] : @""];
		}
			break;
		case MInfoContentType_LotSize:{
			insLabel.text = [LanguageManager stringForKey:Lot_Size];
			//Value
			[valueLabel setValueText:_stockInfo.lotSize];
		}
			break;
		case MInfoContentType_TotBVol:{
			insLabel.text = [LanguageManager stringForKey:TOT_B___VOL__];
			//Value
			[valueLabel setValueText:[_stockInfo.totalBuyVol abbreviateNumber]];
		}
			break;
		case MInfoContentType_ForeignSell:{
			insLabel.text = [LanguageManager stringForKey:Foreign_Sell];
			//Value
			[valueLabel setValueText:_stockInfo.foreignSell? [_stockInfo.foreignSell toCurrencyNumber] : @""];
		}
			break;
		case MInfoContentType_Currency:{
			insLabel.text = [LanguageManager stringForKey:Currency];
			//Value
			[valueLabel setValueText:_stockInfo.currency];
		}
			break;
		case MInfoContentType_TotSVol:{
			insLabel.text = [LanguageManager stringForKey:TOT_S___VOL__];
			//Value
			[valueLabel setValueText:_stockInfo.totalSellVol? [_stockInfo.totalSellVol abbreviateNumber] : @""];
		}
			break;
		case MInfoContentType_SharedIssue:{
			insLabel.text = [LanguageManager stringForKey:Share_Issued];
			//Value
			[valueLabel setValueText:[_stockInfo.totalShareIssued abbreviateNumber]];
		}
			break;
		case MInfoContentType_Tot_Short_SVol:{
			insLabel.text = [LanguageManager stringForKey:Tot_S__Sell_Vol];
			//Value
			[valueLabel setValueText:_stockInfo.totalShortSellVol? [_stockInfo.totalShortSellVol abbreviateNumber] : @""];
		}
			break;
		case MInfoContentType_MktCap:{
			insLabel.text = [LanguageManager stringForKey:Mkt_Cap];
			//Value
			[valueLabel setValueText:[_stockInfo.totalMktCap abbreviateNumber]];
		}
			break;
		case MInfoContentType_Tot_B_Trans:{
			insLabel.text = [LanguageManager stringForKey:Tot_B__Trans];
			//Value
			[valueLabel setValueText:_stockInfo.totalBuyTran? [_stockInfo.totalBuyTran abbreviateNumber] : @""];
		}
			break;
		case MInfoContentType_ParValue:{
			insLabel.text = [LanguageManager stringForKey:Par_Value];
			//Value
			[valueLabel setValueText:[_stockInfo.parValue abbreviateNumber]];
		}
			break;
		case MInfoContentType_Tot_S_Trans:{
			insLabel.text = [LanguageManager stringForKey:Tot_S__Trans];
			//Value
			[valueLabel setValueText:_stockInfo.totalSellTran? [_stockInfo.totalSellTran abbreviateNumber] : @""];
		}
			break;
		case MInfoContentType_Ceiling:{
			insLabel.text = [LanguageManager stringForKey:Ceiling];
			//Ceiling
			[valueLabel setValueText:[_stockInfo.ceil toCurrencyNumber]];
		}
			break;
		case MInfoContentType_DaySpread:{
			insLabel.text = [LanguageManager stringForKey:Day_Spread];
			//Value
			[valueLabel setValueText:_stockInfo.daySpread];//?Key
		}
			break;
		case MInfoContentType_Floor:{
			insLabel.text = [LanguageManager stringForKey:Floor];
			//Value
			[valueLabel setValueText:[_stockInfo.floor toCurrencyNumber]];
		}
			break;
		case MInfoContentType_Mkt_Avg_Price:{
			insLabel.text = [LanguageManager stringForKey:Mkt_Avg_Price];
			//Value
			[valueLabel setValueText:_stockInfo.mktAVGPrice? [_stockInfo.mktAVGPrice toCurrencyNumber] : @""];//?Key
		}
			break;
		case MInfoContentType_EPS:{
			NSString *eps = [LanguageManager stringForKey:EPS];
			if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
				eps = [eps stringByAppendingString:@" (VND)"];
			}
			insLabel.text = eps;
			//Value
			[valueLabel setValueText:[_stockInfo.eps toCurrencyNumber]];
		}
			break;
		case MInfoContentType_DynamicHigh:{
			insLabel.text = [LanguageManager stringForKey:Dynamic_High];
			//Value
			[valueLabel setValueText:_stockInfo.highPrice? [_stockInfo.highPrice toCurrencyNumber] : @""];
		}
			break;
		case MInfoContentType_PeRatio:{
			insLabel.text = [LanguageManager stringForKey:P_E_RATIO];
			//Value
			[valueLabel setValueText:[_stockInfo.peRatio toCurrencyNumber]]; //?Key
		}
			break;
		case MInfoContentType_DynamicLow:{
			insLabel.text = [LanguageManager stringForKey:Dynamic_Low];
			//Value
			[valueLabel setValueText:_stockInfo.lowPrice? [_stockInfo.lowPrice toCurrencyNumber] : @""];
		}
			break;
		case MInfoContentType_NTA:{
			insLabel.text = [LanguageManager stringForKey:NTA];
			//Value
			[valueLabel setValueText:[_stockInfo.nta toCurrencyNumber]]; //%//?Key
		}
			break;
		case MInfoContentType_52Wks_High:{
			insLabel.text = [LanguageManager stringForKey:_52Wks_High];
			//Value
			[valueLabel setValueText:_stockInfo.wks_52_High? [_stockInfo.wks_52_High toCurrencyNumber] : @""];//?Key
		}
			break;
		case MInfoContentType_DevidendYield:{
			insLabel.text = [LanguageManager stringForKey:Devidend_Yield];
			//Value - Update UI
			[valueLabel setValueText:[_stockInfo.dividendYield toCurrencyNumber]];
		}
			break;
		case MInfoContentType_52Wks_Low:{
			insLabel.text = [LanguageManager stringForKey:_52Wks_Low];
			//Value
			[valueLabel setValueText:_stockInfo.wks_52_Low? [_stockInfo.wks_52_Low toCurrencyNumber] : @""];//?Key
		}
			break;
		case MInfoContentType_CPF:{
			insLabel.text = [LanguageManager stringForKey:CPF];
			//Value
			[valueLabel setValueText:_stockInfo.cpf];
		}
			break;
		case MInfoContentType_52Wks_Spread:{
			insLabel.text = [LanguageManager stringForKey:_52Wks_Spread];
			//Value
			[valueLabel setValueText:_stockInfo.wks_52_Spread? [_stockInfo.wks_52_Spread toCurrencyNumber] : @""]; //?Key
		}
			break;
		case MInfoContentType_DeliveryBasis:{
			insLabel.text = [LanguageManager stringForKey:Delivery_Basis];
			//Value
			[valueLabel setValueText:_stockInfo.deliveryBasis];
		}
			break;
		case MInfoContentType_Remark:{
			insLabel.text = [LanguageManager stringForKey:Remarks];
			//Value
			[valueLabel setValueText:_stockInfo.remark];
		}
			break;
		case MInfoContentType_ForeignLimit:{
			insLabel.text = [LanguageManager stringForKey:Foreign_Limit];
			//Value
			[valueLabel setValueText:[_stockInfo.foreignLimit toCurrencyNumber]];
		}
			break;
		case MInfoContentType_TypeIDSS:{
			insLabel.text = [LanguageManager stringForKey:Type];
			//Value
			[valueLabel setValueText:_stockInfo.idssTypeString];
		}
			break;
		case MInfoContentType_ForeignShare:{
			insLabel.text = [LanguageManager stringForKey:Foreign];
			//Value
			[valueLabel setValueText:[_stockInfo.foreignShare toCurrencyNumber]];
		}
			break;
		case MInfoContentType_IDSS_Tot_Vol:{
			insLabel.text = [LanguageManager stringForKey:IDSS_Tot_Vol];
			//Value
			[valueLabel setValueText:_stockInfo.idssTotVol? [_stockInfo.idssTotVol abbreviateNumber] : @""];
		}
			break;
		case MInfoContentType_ForeignOwnerShipLimit:{
			insLabel.text = [LanguageManager stringForKey:Ownership];
			[valueLabel setValueText:[_stockInfo.ownerLimit toCurrencyNumber]];
		}
			break;
		case MInfoContentType_IDSS_Tot_Val:{
			insLabel.text = [LanguageManager stringForKey:IDSS_Tot_Val];
			//Value
			[valueLabel setValueText:_stockInfo.idssTotVal? [_stockInfo.idssTotVal abbreviateNumber] : @""];
		}
			break;
			
		case MInfoContentType_FullName:{
			insLabel.text = [LanguageManager stringForKey:Full_Name];
			//Value
			[valueLabel setValueText:_stockInfo.fullName];
		}
			break;
			
		case MInfoContentType_Intraday_MktAvgPrice:{
			insLabel.text = [LanguageManager stringForKey:Intraday_Mkt_Avg___Price];
			//Value
			[valueLabel setValueText:_stockInfo.intradayMktAvgPrice? [_stockInfo.intradayMktAvgPrice abbreviateNumber] : @""];
		}
			break;
			
		case MInfoContentType_Exchange:{
			insLabel.text = [LanguageManager stringForKey:Exchange];
			//Value
			[valueLabel setValueText:_stockInfo.exchange];
		}
			break;
			
		case MInfoContentType_AvgVolumeOfTenDay:{
			insLabel.text = [LanguageManager stringForKey:Avg_Volume_of_10_days];
			//Value
			[valueLabel setValueText:_stockInfo.avgVolume_10days? [_stockInfo.intradayMktAvgPrice abbreviateNumber] : @""];
		}
			break;
			
		case MInfoContentType_RemainForeign:{
			insLabel.text = [LanguageManager stringForKey:Remain_Foreign];
			//Value
			[valueLabel setValueText:_stockInfo.remainForeign? [_stockInfo.remainForeign abbreviateNumber] : @""];
		}
			break;
			
		case MInfoContentType_Beta:{
			insLabel.text = [LanguageManager stringForKey:Beta];
			//Value
			[valueLabel setValueText:_stockInfo.BETA? [_stockInfo.BETA toCurrencyNumber] : @""];
		}
			break;
			
		case MInfoContentType_ROA:{
			insLabel.text = [LanguageManager stringForKey:ROA(%)];
			//Value
			[valueLabel setValueText:_stockInfo.ROA? [_stockInfo.ROA toCurrencyNumber] : @""];
		}
			break;
			
		case MInfoContentType_ROE:{
			insLabel.text = [LanguageManager stringForKey:ROE(%)];
			//Value
			[valueLabel setValueText:_stockInfo.ROA? [_stockInfo.ROE toCurrencyNumber] : @""];
		}
			break;
			
		case MInfoContentType_P_B:{
			insLabel.text = [LanguageManager stringForKey:P_B];
			//Value
			[valueLabel setValueText:_stockInfo.p_b? [_stockInfo.p_b toCurrencyNumber] : @""];
		}
			break;
			
		case MInfoContentType_Underlying:{
			insLabel.text = [LanguageManager stringForKey:Underlying];
			//Value
			[valueLabel setValueText:_stockInfo.ROA? [_stockInfo.underlying abbreviateNumber] : @""];
		}
			break;
			
		case MInfoContentType_OI:{
			insLabel.text = [LanguageManager stringForKey:_OI];
			//Value
			[valueLabel setValueText:_stockInfo.OI? [_stockInfo.OI abbreviateNumber] : @""];
		}
			break;
			
		case MInfoContentType_Basis:{
			insLabel.text = [LanguageManager stringForKey:Basis];
			//Value
			[valueLabel setValueText:_stockInfo.OI? [_stockInfo.OI abbreviateNumber] : @""];
		}
			break;
			
		case MInfoContentType_ExpiryDate:{
			insLabel.text = [LanguageManager stringForKey:Expiry_Date];
			//Value
			[valueLabel setValueText:_stockInfo.expiryDate];
		}
			break;
			
		case MInfoContentType_UnderlyingPrice:{
			insLabel.text = [LanguageManager stringForKey:Underlying_price];
			//Value
			[valueLabel setValueText:_stockInfo.underlyingPrice? [_stockInfo.OI toCurrencyNumber] : @""];
		}
			break;
		default:{
			valueLabel.text = @"";
			insLabel.text = @"";
		}
			break;
	}
}
@end
