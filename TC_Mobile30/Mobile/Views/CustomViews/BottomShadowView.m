//
//  BottomShadowView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/15/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BottomShadowView.h"
#import "UIView+ShadowBorder.h"
@implementation BottomShadowView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        [self dropShadow];
    }
    return self;
}
@end
