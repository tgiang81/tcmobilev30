//
//  TCDrawCustomImageView.m
//  TC_Mobile30
//
//  Created by Kaka on 12/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCDrawCustomImageView.h"

@implementation TCDrawCustomImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)drawRect:(CGRect)rect {
//	UIBezierPath *path = [UIBezierPath bezierPath];
//	[path moveToPoint:CGPointMake(10.0, 10.0)];
//	[path addLineToPoint:CGPointMake(100.0, 100.0)];
//	path.lineWidth = 3;
//	[[UIColor blueColor] setStroke];
//	[path stroke];
	
	UIBezierPath *path = [UIBezierPath bezierPath];
	CAShapeLayer *shapeLayer = [CAShapeLayer layer];
	shapeLayer.strokeColor = [[UIColor redColor] CGColor];
	shapeLayer.lineWidth = 0.5;
	
	CGPoint start = CGPointMake(CGRectGetMinX(self.frame), CGRectGetMidY(self.frame)+CGRectGetHeight(self.frame)*.25);
	[path moveToPoint:start];
	[path addLineToPoint:CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMidY(self.frame))];
	[path addLineToPoint:CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
	[path addLineToPoint:CGPointMake(CGRectGetMinX(self.frame), CGRectGetMaxY(self.frame))];	
	shapeLayer.path = [path CGPath];
	//    shapeLayer.fillRule = kCAFillRuleEvenOdd;
	shapeLayer.fillColor = [[UIColor blackColor] CGColor];
	
	[self.layer addSublayer:shapeLayer];
}
@end
