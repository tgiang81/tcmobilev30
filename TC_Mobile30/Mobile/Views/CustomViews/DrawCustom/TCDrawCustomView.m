//
//  TCDrawCustomView.m
//  TC_Mobile30
//
//  Created by Kaka on 12/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCDrawCustomView.h"

@implementation TCDrawCustomView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect {
	UIBezierPath *path = [UIBezierPath bezierPath];
	[path moveToPoint:CGPointMake(0.0, 0.0)];
	[path addLineToPoint:CGPointMake(0.0, rect.size.height)];
	[path addLineToPoint:CGPointMake(rect.size.width, rect.size.height * 3 / 4)];
	[path addLineToPoint:CGPointMake(rect.size.width, 0.0)];
	//[path addLineToPoint:CGPointMake(0.0, 0.0)];
	[path closePath];
	path.lineWidth = 1;
	[[UIColor blackColor] setFill];
	[path fill];
	
//	UIBezierPath *path = [UIBezierPath bezierPath];
//	CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//	shapeLayer.strokeColor = [[UIColor redColor] CGColor];
//	shapeLayer.lineWidth = 0.5;
//
//	CGPoint start = CGPointMake(CGRectGetMinX(self.frame), CGRectGetMidY(self.frame)+CGRectGetHeight(self.frame)*.25);
//	[path moveToPoint:start];
//	[path addLineToPoint:CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMidY(self.frame))];
//	[path addLineToPoint:CGPointMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
//	[path addLineToPoint:CGPointMake(CGRectGetMinX(self.frame), CGRectGetMaxY(self.frame))];
//	shapeLayer.path = [path CGPath];
//	//    shapeLayer.fillRule = kCAFillRuleEvenOdd;
//	shapeLayer.fillColor = [[UIColor blackColor] CGColor];
//
//	[self.layer addSublayer:shapeLayer];
}
@end
