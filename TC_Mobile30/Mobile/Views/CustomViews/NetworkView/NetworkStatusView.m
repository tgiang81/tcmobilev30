//
//  NetworkStatusView.m
//  TC_Mobile30
//
//  Created by Kaka on 12/25/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "NetworkStatusView.h"
#import "AllCommon.h"
#import "LanguageKey.h"

@implementation NetworkStatusView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
//Init
- (instancetype)initWithMessage:message{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	if (self) {
		_lblMessage.text = message;
		_isShowing = NO;
	}
	return self;
}

- (instancetype)init{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	if (self) {
		_lblMessage.text = [LanguageManager stringForKey:No_Connection];
	}
	return self;
}
#pragma mark - Main
- (void)showErrorNetwork:(NSString *)message fromView:(UIView *)view{
	if(message){
		self.lblMessage.text = message;
	}else{
		self.lblMessage.text = [LanguageManager stringForKey:No_Connection];
	}
	CGRect tmp = self.frame;
	tmp.origin = CGPointZero;
	self.frame = tmp;
	[view addSubview:self];
	self.backgroundColor = [UIColor redColor];
	self.alpha = 0.3;
	[UIView animateWithDuration:1.0 animations:^{
		self.alpha = 1.0;
	} completion:^(BOOL finished) {
		self.isShowing = YES;
		[view bringSubviewToFront:self];
	}];
}

- (void)hide{
	[UIView animateWithDuration:0.6 animations:^{
		self.alpha = 0.3;
	} completion:^(BOOL finished) {
		self.isShowing = NO;
		[self removeFromSuperview];
	}];
}

- (void)hideWhenNetworkConnected{
	self.lblMessage.text = [LanguageManager stringForKey:@"Connected"];
	self.backgroundColor = [UIColor greenColor];
	self.opaque = 0.7;
	[self hide];
}
- (void)hide:(void(^)(void))completion{
	self.lblMessage.text = [LanguageManager stringForKey:@"Connected"];
	self.backgroundColor = [UIColor greenColor];
	self.opaque = 0.7;
	self.alpha = 1.0;
	[UIView animateWithDuration:1.0 delay:0.3 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
		self.alpha = 0.3;
	} completion:^(BOOL finished) {
		self.isShowing = NO;
		[self removeFromSuperview];
		completion();
	}];
}
#pragma mark - Handle Notification
- (void)handleNetworkNotification:(NSNotification *)noti{
	if ([NetworkManager shared].networkStatus != NotReachable) {
		//Auto hide
		[self hideWhenNetworkConnected];
	}
}
@end
