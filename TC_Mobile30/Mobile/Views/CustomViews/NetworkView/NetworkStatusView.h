//
//  NetworkStatusView.h
//  TC_Mobile30
//
//  Created by Kaka on 12/25/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseView.h"

@interface NetworkStatusView : TCBaseView{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (assign, nonatomic) BOOL isShowing;
//Main
- (void)showErrorNetwork:(NSString *)message fromView:(UIView *)view;
- (void)hide;
- (void)hideWhenNetworkConnected;
- (void)hide:(void(^)(void))completion;
@end
