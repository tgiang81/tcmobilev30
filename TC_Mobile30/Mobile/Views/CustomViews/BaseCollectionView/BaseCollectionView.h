//
//  BaseCollectionView.h
//  TCiPad
//
//  Created by Kaka on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionView : UICollectionView{
	
}
@property (strong, nonatomic) NSIndexPath *indexPath;

@end
