//
//  SectorHeaderReusableView.h
//  TC_Mobile30
//
//  Created by Kaka on 12/28/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectorHeaderReusableView : UICollectionReusableView{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
