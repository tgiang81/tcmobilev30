//
//  TradePopupView.m
//  TCiPad
//
//  Created by Kaka on 8/29/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TradePopupView.h"
#import "TCBaseButton.h"
#import "AllCommon.h"
#import "LanguageKey.h"
@interface TradePopupView (){
	
}
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;

@property (weak, nonatomic) IBOutlet TCBaseButton *btnBuy;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnSell;

@end
@implementation TradePopupView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
        [self setupLanguage];
	}
	return self;
}

- (void)layoutSubviews{
	[super layoutSubviews];
	_btnBuy.borderColor = TC_COLOR_FROM_HEX(@"F0F9FE");
	_btnSell.borderColor = TC_COLOR_FROM_HEX(@"F0F9FE");
    _btnSell.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].sellColor);
	_btnBuy.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].buyColor);
}
-(void)setupLanguage{
    [_btnBuy setTitle:[LanguageManager stringForKey:Buy] forState:UIControlStateNormal];
    [_btnSell setTitle:[LanguageManager stringForKey:Sell] forState:UIControlStateNormal];
}
- (void)awakeFromNib{
	[super awakeFromNib];
}
#pragma mark - Common Init
- (void)commonInit
{
	NSString *className = NSStringFromClass([self class]);
	_customConstraints = [[NSMutableArray alloc] init];
	UIView *view = nil;
	NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	for (id object in objects) {
		if ([object isKindOfClass:[UIView class]]) {
			view = object;
			break;
		}
	}
	
	if (view != nil) {
		_containerView = view;
		view.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:view];
		[self setNeedsUpdateConstraints];
	}
	_containerView.backgroundColor = [UIColor clearColor];
	[self updateConstraints];
}

- (void)updateConstraints
{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);
		
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}

#pragma mark - Main
#pragma mark - Action

- (IBAction)onSellAction:(id)sender {
	if (_delegate) {
		[_delegate didTapTradeType:TradeType_Sell];
	}
}

- (IBAction)onBuyAction:(id)sender {
	if (_delegate) {
		[_delegate didTapTradeType:TradeType_Buy];
	}
}
@end
