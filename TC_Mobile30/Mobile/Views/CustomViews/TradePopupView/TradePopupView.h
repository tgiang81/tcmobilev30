//
//  TradePopupView.h
//  TCiPad
//
//  Created by Kaka on 8/29/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum : NSInteger{
	TradeType_Buy,
	TradeType_Sell,
    TradeType_Cancel,
    TradeType_Revise
}TradeType;
@protocol TradePopupViewDelegate<NSObject>
@optional
- (void)didTapTradeType:(TradeType)type;
@end

@interface TradePopupView : UIView{
	
}
@property (weak, nonatomic) id <TradePopupViewDelegate> delegate;

@end
