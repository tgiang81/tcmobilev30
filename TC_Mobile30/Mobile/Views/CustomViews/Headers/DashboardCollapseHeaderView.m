//
//  DashboardCollapseHeaderView.m
//  TCiPad
//
//  Created by Kaka on 5/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DashboardCollapseHeaderView.h"
#import "TCBaseButton.h"
#import "AllCommon.h"

static NSString *kTOOGLE_OPEN_ICON_NAME	= @"arrow_highlight_icon";
static NSString *kTOOGLE_CLOSE_ICON_NAME = @"arrow_white_right_icon";

@implementation DashboardCollapseHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithTitle:(NSString *)title state:(HeaderState)state atSextion:(NSInteger)section{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	if (self) {
		self.lblTitle.text = title;
		self.state = state;
		self.section = section;
	}
	return self;
}

- (void)awakeFromNib{
	[super awakeFromNib];
	[self setup];
}
- (void)layoutSubviews{
	[super layoutSubviews];
	
}

#pragma mark - Main
- (void)setup{
	_btnToogle.showsTouchWhenHighlighted = NO;
	[_btnToogle setImage:[UIImage imageNamed:kTOOGLE_OPEN_ICON_NAME] forState:UIControlStateSelected];
	[_btnToogle setImage:[UIImage imageNamed:kTOOGLE_CLOSE_ICON_NAME] forState:UIControlStateNormal];
	self.backgroundColor = [UIColor clearColor];
	_viewContent.backgroundColor = RGB(72, 96, 116);
	_lblTitle.font = AppFont_MainFontMediumWithSize(12);
	_lblTitle.textColor = [UIColor whiteColor];
	[_btnDetail setTitleColor:RGB(242, 101, 34) forState:UIControlStateNormal];
}

//============== Handle View ===============
- (void)setState:(HeaderState)state{
	_btnToogle.selected = (state == HeaderState_Open) ? YES : NO;
	float angle = 0;
	if (state == HeaderState_Open) {
		angle = M_PI_2;
	}
	// Setup the animation
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.2];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
	[UIView setAnimationBeginsFromCurrentState:YES];
	
	// The transform matrix
	CGAffineTransform tRotate45 = CGAffineTransformMakeRotation(angle);
	_btnToogle.transform = tRotate45;
	// Commit the changes
	[UIView commitAnimations];
	_state = state;
}

#pragma mark - ACTIONS

- (IBAction)onToogleAction:(id)sender {
	if (_delegate) {
		[_delegate didToogleHeaderView:self];
	}
}
@end
