//
//  WatchlistSectionView.m
//  TCiPad
//
//  Created by Kaka on 8/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "WatchlistSectionView.h"
@interface WatchlistSectionView (){

}
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;

//Main
@property (assign, nonatomic) StockCellType typeCell;

@end
@implementation WatchlistSectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithSectionType:(StockCellType)type{
	self = [super init];
	if (self) {
		[self commonInit];
		self.typeCell = type;
	}
	return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
}
#pragma mark - Common Init
- (void)commonInit
{
	//++++ Using Auto Layout ++++
	NSString *className = NSStringFromClass([self class]);
	_customConstraints = [[NSMutableArray alloc] init];
	UIView *view = nil;
	NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	for (id object in objects) {
		if ([object isKindOfClass:[UIView class]]) {
			view = object;
			break;
		}
	}
	
	if (view != nil) {
		_containerView = view;
		view.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:view];
		[self setNeedsUpdateConstraints];
	}
	[self updateConstraints];
}

- (void)updateConstraints
{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);
		
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}

#pragma mark - Main
- (void)highlightTypeCellAt:(StockCellType)type{
	_btnTypeOne.selected = NO;
	_btnTypeTwo.selected = NO;
	_btnTypeThree.selected = NO;
	switch (type) {
		case StockCellType_One:{
			_btnTypeOne.selected = YES;
		}
			break;
		case StockCellType_Two:{
			_btnTypeTwo.selected = YES;
		}
			break;
		case StockCellType_Three:{
			_btnTypeThree.selected = YES;
		}
			break;
		default:
			break;
	}
}

- (void)setTypeCell:(StockCellType)typeCell{
	_typeCell = typeCell;
	[self highlightTypeCellAt:typeCell];
}
#pragma mark - Action

- (IBAction)onSelectTypeCellAction:(UIButton *)sender {
	self.typeCell = sender.tag;
	if (_delegate) {
		[_delegate didSelectTypeCell:_typeCell];
	}
}
@end
