//
//  WatchlistSectionView.h
//  TCiPad
//
//  Created by Kaka on 8/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"
@protocol WatchlistSectionViewDelegate <NSObject>
@optional
- (void)didSelectTypeCell:(StockCellType)typeCell;
@end
@interface WatchlistSectionView : UIView{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblNameWatchlist;
//Type Cell
@property (weak, nonatomic) IBOutlet UIButton *btnTypeOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTypeTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnTypeThree;

@property (assign, nonatomic) id <WatchlistSectionViewDelegate> delegate;

//Main:
- (instancetype)initWithSectionType:(StockCellType)type;

@end
