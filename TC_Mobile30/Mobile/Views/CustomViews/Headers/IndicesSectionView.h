//
//  IndiceSectionView.h
//  TCiPad
//
//  Created by Kaka on 8/2/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCPageMenuControl.h"

@interface IndicesSectionView : UIView{
	
}
@property (weak, nonatomic) IBOutlet TCPageMenuControl *controlView;

#pragma mark - Main
- (instancetype)initWithControlTitles:(NSArray *)titles;
- (void)setControlViewDelegate:(id <TCControlViewDelegate>) delegate;
- (void)setupControlTitles:(NSArray *)titles;

@end
