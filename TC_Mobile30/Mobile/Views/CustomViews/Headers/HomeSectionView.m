//
//  HomeSectionView.m
//  TCiPad
//
//  Created by Kaka on 7/16/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "HomeSectionView.h"


@implementation HomeSectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib{
	[super awakeFromNib];
	[self setup];
}
#pragma mark - Main
//Init
- (instancetype)initWithSection:(HomeSectionType)section{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	if (self) {
		_lblTitle.text = [self titleBySection:section];
	}
	return self;
}
- (void)setup{
	self.backgroundColor = AppColor_MainBackgroundColor;
}

#pragma mark - UTILS
- (NSString *)titleBySection:(HomeSectionType)section{
	NSString *title = @"Unknow";
	switch (section) {
		case HomeSectionType_Market:
			title = [LanguageManager stringForKey:@"Market"];
			break;
		case HomeSectionType_Indices:
			title = [LanguageManager stringForKey:@"Indices"];
			break;
		case HomeSectionType_Research:
			title = [LanguageManager stringForKey:@"Research"];
			break;
		case HomeSectionType_Alert:
			title = [LanguageManager stringForKey:@"Alert"];
			break;
		case HomeSectionType_Portfolios:
			title = [LanguageManager stringForKey:@"Portfilio"];
			break;
		case HomeSectionType_Advertisement:
			title = [LanguageManager stringForKey:@"Advertisement"];
			break;
		default:
			break;
	}
	return title;
}
@end
