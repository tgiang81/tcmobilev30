//
//  MarketHeaderView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MarketHeaderView.h"

@implementation MarketHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)action:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectExpand)]){
        [self.delegate didSelectExpand];
    }
}

@end
