//
//  PortfolioDetailHeaderView.m
//  TC_Mobile30
//
//  Created by Kaka on 10/31/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PortfolioDetailHeaderView.h"
#import "AllCommon.h"
@interface PortfolioDetailHeaderView(){
	
}
//OUTLETS

@end

@implementation PortfolioDetailHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
#pragma mark - INIT

- (void)awakeFromNib {
	[super awakeFromNib];
}

- (instancetype)initWithHeader:(PortfolioHeaderType)headerType{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	if (self) {
		self.headerType = headerType;
	}
	return self;
}

#pragma mark - MAIN
- (void)setup{
	
}

#pragma mark - Config
- (void)setHeaderType:(PortfolioHeaderType)headerType{
	switch (headerType) {
		case PortfolioHeaderType_Buy:
			_lblHeaderType.text = [LanguageManager stringForKey:@"BUY"];
			break;
			
		case PortfolioHeaderType_Sell:
			_lblHeaderType.text = [LanguageManager stringForKey:@"SELL"];
			break;
		default:
			break;
	}
	_headerType = headerType;
}
@end
