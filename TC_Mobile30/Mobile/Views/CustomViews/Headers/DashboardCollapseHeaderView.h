//
//  DashboardCollapseHeaderView.h
//  TCiPad
//
//  Created by Kaka on 5/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger, HeaderState) {
	HeaderState_Close,
	HeaderState_Open
};
@class TCBaseButton;
@class DashboardCollapseHeaderView;

@protocol DashboardCollapseHeaderViewDelegate <NSObject>
@optional
- (void)didToogleHeaderView:(DashboardCollapseHeaderView *)headerView;
@end

@interface DashboardCollapseHeaderView : UIView{
	
}
@property (weak, nonatomic) IBOutlet TCBaseButton *btnToogle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnDetail;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

//Passing data
@property (assign, nonatomic) NSInteger section;
@property (assign, nonatomic) HeaderState state;
@property (weak, nonatomic) id <DashboardCollapseHeaderViewDelegate> delegate;

- (instancetype)initWithTitle:(NSString *)title state:(HeaderState)state atSextion:(NSInteger)section;

@end
