//
//  HomeSectionView.h
//  TCiPad
//
//  Created by Kaka on 7/16/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"
@interface HomeSectionView : UIView{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

//Main
- (instancetype)initWithSection:(HomeSectionType)section;

@end
