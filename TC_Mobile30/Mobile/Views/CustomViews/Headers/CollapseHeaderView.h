//
//  ContactHeaderView.h
//  CxEzyhaul
//
//  Created by CUONGTA on 8/1/16.
//  Copyright © 2016 CMC Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"

//=================== Header Type =================================
typedef enum: NSInteger {
	HeaderType_Unknown = -1,
	HeaderType_Market = 0,
	HeaderType_WatchList = 1,
	HeaderType_News = 2,
	HeaderType_TopRank = 3,
	HeaderType_Portfolio = 4
} HeaderType;

@class CollapseHeaderView;
@class PADesignableLabel;
@protocol CollapseHeaderViewDelegate <NSObject>

@optional
- (void)didOpenHistoryHeaderView:(CollapseHeaderView *)headerView;
- (void)didTapFilterAction;
@end

@interface CollapseHeaderView : UIView{
    BOOL _contactShipment;
}
@property (weak, nonatomic) IBOutlet UIButton *btnArrow;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UIView *contentView;

//Passing data
@property (assign, nonatomic) HeaderType currentHeader;
@property (weak, nonatomic) id <CollapseHeaderViewDelegate> delegate;
- (instancetype)initWithHeader:(HeaderType)headerType isSelect:(BOOL)selected;

@end
