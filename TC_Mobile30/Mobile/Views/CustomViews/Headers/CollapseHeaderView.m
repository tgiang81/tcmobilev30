//
//  ContactHeaderView.m
//  CxEzyhaul
//
//  Created by CUONGTA on 8/1/16.
//  Copyright © 2016 CMC Mobile. All rights reserved.
//

#import "CollapseHeaderView.h"
#import "PADesignableLabel.h"
@implementation CollapseHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - INIT

-(void)awakeFromNib {
	[super awakeFromNib];
    [_btnArrow setImage:[UIImage imageNamed:@"ico_right"] forState:UIControlStateNormal];
    [_btnArrow setImage:[UIImage imageNamed:@"ico_down"] forState:UIControlStateSelected];
}

- (instancetype)initWithHeader:(HeaderType)headerType isSelect:(BOOL)selected{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	if (self) {
		_currentHeader = headerType;
		_btnArrow.selected = selected;
		//Setup UI
		[self updateUIWithStatus:selected];
		[self updateUIBy:headerType];
	}
	return self;
}

#pragma mark - Util View
//Call this to update UI
- (void)updateUIWithStatus:(BOOL)selected{
	/*
	for (UIView *v in self.contentView.subviews) {
		v.alpha = selected ? 1.0 : .5;
	}
	 */
	_btnFilter.alpha = selected ? 1.0 : .5;
	self.lblTitle.textColor = [self titleColorBySelected:selected];
}

- (void)updateUIBy:(HeaderType)type{
	self.lblTitle.text = [self titleHeaderBy:type];
	if (type == HeaderType_TopRank) {
		_btnFilter.hidden = NO;
	}else{
		_btnFilter.hidden = YES;
	}
}

- (UIColor *)titleColorBySelected:(BOOL)selected{
	if (!selected) {
		return [UIColor lightGrayColor];
	}
	return [UIColor whiteColor];
	
}

- (NSString *)titleHeaderBy:(HeaderType)type{
	NSString *title = @"";
	switch (type) {
		case HeaderType_Market:
			title = [LanguageManager stringForKey:@"Market"];
			break;
		case HeaderType_WatchList:
			title = [LanguageManager stringForKey:@"Watchlist"];
			break;
		case HeaderType_News:
			title = [LanguageManager stringForKey:@"News"];
			break;
		case HeaderType_TopRank:
			title = [LanguageManager stringForKey:@"Top Rank"];
			break;
		case HeaderType_Portfolio:
			title = [LanguageManager stringForKey:@"Portfilio"];
			break;
		default:
			break;
	}
	return title;
}


#pragma mark - Action
- (IBAction)openHeaderView:(id)sender {
    DLog(@"+++Did Tap Open HistoryHeaderView");
    //_btnArrow.selected = !_btnArrow.selected;
    if (_delegate) {
        [_delegate didOpenHistoryHeaderView:self];
    }
}

- (IBAction)onFilterAction:(id)sender {
	DLog(@"Filter Action");
	if (_delegate) {
		[_delegate didTapFilterAction];
	}
}

@end
