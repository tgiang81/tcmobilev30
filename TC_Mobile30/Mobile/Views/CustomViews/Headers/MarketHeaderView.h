//
//  MarketHeaderView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"
#import "AroundShadowView.h"
@class AroundShadowView;
NS_ASSUME_NONNULL_BEGIN
@protocol MarketHeaderViewDelegate<NSObject>
- (void)didSelectExpand;
@end
@interface MarketHeaderView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Icon;
@property (weak, nonatomic) IBOutlet UIButton *btn_Action;
@property (weak, nonatomic) id<MarketHeaderViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Bottom;

@end

NS_ASSUME_NONNULL_END
