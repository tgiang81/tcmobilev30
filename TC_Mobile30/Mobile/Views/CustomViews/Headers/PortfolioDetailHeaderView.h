//
//  PortfolioDetailHeaderView.h
//  TC_Mobile30
//
//  Created by Kaka on 10/31/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

//Page
typedef enum : NSInteger{
	PortfolioHeaderType_Buy = 0,
	PortfolioHeaderType_Sell
}PortfolioHeaderType;

@interface PortfolioDetailHeaderView : UIView{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderType;

//Passing data
@property (assign, nonatomic) PortfolioHeaderType headerType;

//Main
- (instancetype)initWithHeader:(PortfolioHeaderType)headerType;

@end
