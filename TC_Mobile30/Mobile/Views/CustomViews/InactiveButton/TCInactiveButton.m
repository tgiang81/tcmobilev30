//
//  TCInactiveButton.m
//  TC_Mobile30
//
//  Created by Kaka on 11/9/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCInactiveButton.h"
#import "AllCommon.h"

@implementation TCInactiveButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews{
	[super layoutSubviews];
	[self setUserInteractionEnabled:NO];
	[self setContentMode:UIViewContentModeScaleAspectFit];
}
@end
