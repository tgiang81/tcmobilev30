//
//  TCSwitch.h
//  TC_Mobile30
//
//  Created by Kaka on 1/16/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCSwitch : UISwitch{
	
}
@property (strong, nonatomic) UIColor *offTintColor;
@end
