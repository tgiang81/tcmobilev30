//
//  TCSwitch.m
//  TC_Mobile30
//
//  Created by Kaka on 1/16/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCSwitch.h"
#import "UISwitch+TCUtils.h"
#import "AllCommon.h"
@implementation TCSwitch

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews{
	[super layoutSubviews];
	self.onTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].switchOnColor);
	self.offTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].switchOnColor);
}

#pragma mark - Config
- (void)setOffTintColor:(UIColor *)offTintColor{
	[self setOffColor:offTintColor];
	_offTintColor = offTintColor;
}
@end
