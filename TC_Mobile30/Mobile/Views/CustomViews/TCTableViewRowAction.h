//
//  TCTableViewRowAction.h
//  TCiPad
//
//  Created by Kaka on 8/7/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseButton.h"

@interface TCTableViewRowAction : UITableViewRowAction{
	
}
@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) UIImage *icon;

+ (instancetype)rowActionWithStyle:(UITableViewRowActionStyle)style title:(NSString *)title icon:(UIImage*)icon handler:(void (^)(UITableViewRowAction *action, NSIndexPath *indexPath))handler;

@end
