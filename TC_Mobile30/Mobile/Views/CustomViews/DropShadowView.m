//
//  DropShadowView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/15/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "DropShadowView.h"
#import "UIView+ShadowBorder.h"
@implementation DropShadowView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        [self dropShadow];
    }
    return self;
}

@end
