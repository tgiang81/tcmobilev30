//
//  PADesignableView.h
//  PlastApp
//
//  Created by Kaka on 9/22/17.
//  Copyright © 2017 ITSOL. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface PADesignableView : UIView{
	
}
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

@property (assign, nonatomic) IBInspectable BOOL isRounded;
@end
