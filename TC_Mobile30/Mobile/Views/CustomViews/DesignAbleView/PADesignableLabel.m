//
//  PADesignableLabel.m
//  PlastApp
//
//  Created by Kaka on 9/17/17.
//  Copyright © 2017 ITSOL. All rights reserved.
//

#import "PADesignableLabel.h"

@implementation PADesignableLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//Overide Init
- (id)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	if (self)
	{
		self.edgeInsets = UIEdgeInsetsMake(self.topEdge, self.leftEdge, self.bottomEdge, self.rightEdge);
		[self updateUI];
	}
	return self;
}

- (void)drawTextInRect:(CGRect)rect{
	self.edgeInsets = UIEdgeInsetsMake(self.topEdge, self.leftEdge, self.bottomEdge, self.rightEdge);
	
	[super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

- (CGSize)intrinsicContentSize{
	CGSize size = [super intrinsicContentSize];
	size.width  += self.edgeInsets.left + self.edgeInsets.right;
	size.height += self.edgeInsets.top + self.edgeInsets.bottom;
	return size;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self updateUI];
	}
	return self;
}

- (void)prepareForInterfaceBuilder {
	[self updateUI];
}

- (void)updateUI {
	self.layer.borderWidth = self.borderWidth;
	self.layer.borderColor = self.borderColor.CGColor;
	self.layer.cornerRadius = self.cornerRadius;
	self.layer.cornerRadius = self.isRounded? self.frame.size.height / 2 : self.cornerRadius;
}

//Overide Init
- (void)awakeFromNib{
	[super awakeFromNib];
	self.layer.masksToBounds = YES;
}
- (void)layoutSubviews{
	[super layoutSubviews];
	[self updateUI];
}

// set border Color
- (void)setBorderColor:(UIColor *)borderColor
{
	_borderColor = borderColor;
	[self updateUI];
}

//BorderWidth
- (void)setBorderWidth:(CGFloat)borderWidth
{
	_borderWidth = borderWidth;
	[self updateUI];
}

//Corner Radius
- (void)setCornerRadius:(CGFloat)cornerRadius
{
	_cornerRadius = cornerRadius;
	[self updateUI];
}

//Rounded View
- (void)setIsRounded:(BOOL)isRounded{
	_isRounded = isRounded;
	[self updateUI];
}
@end
