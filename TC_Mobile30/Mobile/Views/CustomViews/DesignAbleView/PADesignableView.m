//
//  PADesignableView.m
//  PlastApp
//
//  Created by Kaka on 9/22/17.
//  Copyright © 2017 ITSOL. All rights reserved.
//

#import "PADesignableView.h"

@implementation PADesignableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self updateUI];
	}
	return self;
}

- (void)prepareForInterfaceBuilder {
	[self updateUI];
}

- (void)updateUI {
	self.layer.borderWidth = _borderWidth;
	self.layer.borderColor = _borderColor.CGColor;
	self.layer.cornerRadius = _isRounded? self.frame.size.height / 2 : _cornerRadius;
}

//Overide Init
- (instancetype)initWithFrame:(CGRect)frame
{
	if (!(self = [super initWithFrame:frame])) return self;
	[self updateUI];
	return self;
}
- (void)awakeFromNib{
	[super awakeFromNib];
	self.layer.masksToBounds = YES;
}
- (void)layoutSubviews{
	[super layoutSubviews];
	[self updateUI];
}

// set border Color
- (void)setBorderColor:(UIColor *)borderColor
{
	_borderColor = borderColor;
	[self updateUI];
}

//BorderWidth
- (void)setBorderWidth:(CGFloat)borderWidth
{
	_borderWidth = borderWidth;
	[self updateUI];
}

//Corner Radius
- (void)setCornerRadius:(CGFloat)cornerRadius
{
	_cornerRadius = cornerRadius;
	[self updateUI];
}

//Rounded View
- (void)setIsRounded:(BOOL)isRounded{
	_isRounded = isRounded;
	[self updateUI];
}
@end
