//
//  PADesignableImageView.h
//  TCiPad
//
//  Created by Kaka on 8/3/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PADesignableImageView : UIImageView{
	
}
@property (assign, nonatomic) IBInspectable CGFloat borderWidth;
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;
@property (strong, nonatomic) IBInspectable UIColor *borderColor;

@property (assign, nonatomic) IBInspectable BOOL isRounded;
@end
