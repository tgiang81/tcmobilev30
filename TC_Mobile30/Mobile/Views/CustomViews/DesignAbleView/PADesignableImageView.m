//
//  PADesignableImageView.m
//  TCiPad
//
//  Created by Kaka on 8/3/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PADesignableImageView.h"

@implementation PADesignableImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self updateUI];
	}
	return self;
}

- (void)prepareForInterfaceBuilder {
	[self updateUI];
}

- (void)updateUI {
	self.layer.borderWidth = self.borderWidth;
	self.layer.borderColor = self.borderColor.CGColor;
	self.layer.cornerRadius = self.isRounded? self.frame.size.height / 2 : self.cornerRadius;
}

//Overide Init
- (instancetype)initWithFrame:(CGRect)frame
{
	if (!(self = [super initWithFrame:frame])) return self;
	[self updateUI];
	return self;
}
- (void)awakeFromNib{
	[super awakeFromNib];
	self.layer.masksToBounds = YES;
}
- (void)layoutSubviews{
	[super layoutSubviews];
	[self updateUI];
}

// set border Color
- (void)setBorderColor:(UIColor *)borderColor
{
	_borderColor = borderColor;
	[self updateUI];
}

//BorderWidth
- (void)setBorderWidth:(CGFloat)borderWidth
{
	_borderWidth = borderWidth;
	[self updateUI];
}

//Corner Radius
- (void)setCornerRadius:(CGFloat)cornerRadius
{
	_cornerRadius = cornerRadius;
	[self updateUI];
}

//Rounded View
- (void)setIsRounded:(BOOL)isRounded{
	_isRounded = isRounded;
	[self updateUI];
}
@end
