//
//  TCShadowDesignableView.m
//  TC_Mobile30
//
//  Created by Kaka on 1/16/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCShadowDesignableView.h"

@implementation TCShadowDesignableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self updateUI];
	}
	return self;
}

- (void)prepareForInterfaceBuilder {
	[self updateUI];
}

//Overide Init
- (instancetype)initWithFrame:(CGRect)frame
{
	if (!(self = [super initWithFrame:frame])) return self;
	[self updateUI];
	return self;
}
- (void)awakeFromNib{
	[super awakeFromNib];
	self.layer.masksToBounds = YES;
}
- (void)layoutSubviews{
	[super layoutSubviews];
	[self updateUI];
}
#pragma mark - Main
- (void)updateUI{
	self.layer.shadowColor = [UIColor blackColor].CGColor; //TC_COLOR_FROM_HEX([ThemeManager shareInstance].shadowColor).CGColor
	[self.layer setShadowOpacity:self.shadowOpacity];
	[self.layer setShadowRadius:self.shadowRadius];
	[self.layer setShadowOffset:CGSizeMake(self.widthOffset, self.heightOffset)];
	self.layer.shouldRasterize = NO;
	self.layer.masksToBounds = NO;
}

#pragma mark - Configs
- (void)setWidthOffset:(CGFloat)widthOffset{
	_widthOffset = widthOffset;
	[self updateUI];
}
- (void)setHeightOffset:(CGFloat)heightOffset{
	_heightOffset = heightOffset;
	[self updateUI];
}
- (void)setShadowRadius:(CGFloat)shadowRadius{
	_shadowRadius = shadowRadius;
	[self updateUI];
}
- (void)setShadowOpacity:(CGFloat)shadowOpacity{
	_shadowOpacity = shadowOpacity;
	[self updateUI];
}
@end
