//
//  TCShadowDesignableView.h
//  TC_Mobile30
//
//  Created by Kaka on 1/16/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface TCShadowDesignableView : UIView{
	
}
@property (assign, nonatomic) IBInspectable CGFloat widthOffset;
@property (assign, nonatomic) IBInspectable CGFloat heightOffset;
@property (assign, nonatomic) IBInspectable CGFloat shadowRadius;
@property (assign, nonatomic) IBInspectable CGFloat shadowOpacity;
@end
