//
//  MarketView.m
//  TCiPad
//
//  Created by Kaka on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MarketView.h"
#import "AllCommon.h"
//Constant congfiguration
#define kTopHeighButtonControl	40
#define kTopWidthButtonControl	80


typedef enum : NSInteger{
	IndiceLocationType_US = 0,
	IndiceLocationType_Canada,
	IndiceLocationType_Asia,
	IndiceLocationType_Europe,
	IndiceLocationType_Germany,
	IndiceLocationType_Australia
}IndiceLocationType;

typedef enum : NSInteger{
	TimeChartType_1Day = 0,
	TimeChartType_5Day,
	TimeChartType_1M,
	TimeChartType_3M,
	TimeChartType_6M,
	TimeChartType_1Y,
	TimeChartType_2Y,
	TimeChartType_5Y,
	TimeChartType_Max
}TimeChartType;

@interface MarketView ()<UIWebViewDelegate, UIGestureRecognizerDelegate>{
	NSMutableArray *_arrLocationControls;
	NSMutableArray *_arrTimeControls;
}
@end

@implementation MarketView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
}

- (void)layoutSubviews{
	[super layoutSubviews];
}

#pragma mark - Common Init
- (void)commonInit
{
	//=========== Using Autoresize class ====================
	//* This sample to use custom view
	NSString *className = NSStringFromClass([self class]);
	UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
	rootView.frame = self.bounds;
	rootView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
	[self addSubview:rootView];
	[self setNeedsUpdateConstraints];
	//======== init valuw ===========
	_arrLocationControls = @[].mutableCopy;
	_arrTimeControls = @[].mutableCopy;
	[self setupControl];
}

#pragma mark - Setup Control
- (void)setupControl{
	_mainContent.layer.backgroundColor = [UIColor purpleColor].CGColor;
	//Prepare Time Chart
	for (id aView in _stackTimeChartControl.subviews) {
		if ([aView isKindOfClass:[UIButton class]]) {
			UIButton *btnControl = (UIButton *)aView;
			[btnControl setTitle:[self timeTitleFrom:btnControl.tag] forState:UIControlStateNormal];
			[_arrTimeControls addObject:aView];
		}
	}
	//Prepare Indice Locations
	NSArray *indiceLocations = @[@(IndiceLocationType_US), @(IndiceLocationType_Canada), @(IndiceLocationType_Asia), @(IndiceLocationType_Europe), @(IndiceLocationType_Germany), @(IndiceLocationType_Australia)].copy;
	for (int i = 0; i < indiceLocations.count; i ++) {
		UIButton *btnIndiceControl = [[UIButton alloc] initWithFrame:CGRectMake(i * kTopWidthButtonControl, 0, kTopWidthButtonControl, kTopHeighButtonControl)];
		btnIndiceControl.showsTouchWhenHighlighted = YES;
		btnIndiceControl.titleLabel.text = [NSString stringWithFormat:@"%d", i];
		btnIndiceControl.tag = [indiceLocations[i] integerValue];
		[btnIndiceControl setTitle:[NSString stringWithFormat:@"%d", i] forState:UIControlStateNormal];
		if (i % 2 == 0) {
			btnIndiceControl.backgroundColor = [UIColor grayColor];
		}
		[btnIndiceControl setTitleColor:RGB(0, 122, 255) forState:UIControlStateNormal];
		[btnIndiceControl addTarget:self action:@selector(onSelectIndiceLocation:) forControlEvents:UIControlEventTouchUpInside];
		[btnIndiceControl setTitle:[self indiceTitleFrom:btnIndiceControl.tag] forState:UIControlStateNormal];
		[_topScrollView addSubview:btnIndiceControl];
		[_arrLocationControls addObject:btnIndiceControl];
	}
	[_topScrollView setContentSize:CGSizeMake(indiceLocations.count * kTopWidthButtonControl, kTopHeighButtonControl)];
	[_topScrollView setBounces:YES];
	[self setNeedsUpdateConstraints];
	
	//Make Selected Defaul
	[self selectedIndiceLocationAt:IndiceLocationType_US];
	[self selectedTimeChartAt:TimeChartType_1Day];
}
#pragma mark - Util View
- (NSString *)indiceTitleFrom:(IndiceLocationType)indiceType{
	NSString *_title = @"";
	switch (indiceType) {
		case IndiceLocationType_US:
			_title = [LanguageManager stringForKey:@"US"];
			break;
		case IndiceLocationType_Canada:
			_title = [LanguageManager stringForKey:@"Canada"];
			break;
		case IndiceLocationType_Asia:
			_title = [LanguageManager stringForKey:@"Asia"];
			break;
		case IndiceLocationType_Germany:
			_title = [LanguageManager stringForKey:@"Germany"];
			break;
		case IndiceLocationType_Europe:
			_title = [LanguageManager stringForKey:@"Europe"];
			break;
		case IndiceLocationType_Australia:
			_title = [LanguageManager stringForKey:@"Australia"];
			break;
		default:
			break;
	}
	return _title;
}

- (NSString *)timeTitleFrom:(TimeChartType)chartType{
	NSString *_title = @"";
	switch (chartType) {
		case TimeChartType_1Day:
			_title = [LanguageManager stringForKey:@"1D"];
			break;
		case TimeChartType_5Day:
			_title = [LanguageManager stringForKey:@"5D"];
			break;
		case TimeChartType_1M:
			_title = [LanguageManager stringForKey:@"1M"];
			break;
		case TimeChartType_3M:
			_title = [LanguageManager stringForKey:@"3M"];
			break;
		case TimeChartType_6M:
			_title = [LanguageManager stringForKey:@"6M"];
			break;
		case TimeChartType_1Y:
			_title = [LanguageManager stringForKey:@"1Y"];
			break;
		case TimeChartType_2Y:
			_title = [LanguageManager stringForKey:@"2Y"];
			break;
		case TimeChartType_5Y:
			_title = [LanguageManager stringForKey:@"5Y"];
			break;
		case TimeChartType_Max:
			_title = [LanguageManager stringForKey:@"Max"];
			break;
		default:
			break;
	}
	return _title;
}

//Selected
- (void)selectedIndiceLocationAt:(IndiceLocationType)type{
	for (UIButton *btn in _arrLocationControls) {
		if (btn.tag == type) {
			[btn setBackgroundColor:[UIColor blueColor]];
		}else{
			[btn setBackgroundColor:[UIColor whiteColor]];
		}
	}
}

- (void)selectedTimeChartAt:(TimeChartType)type{
	for (UIButton *btn in _arrTimeControls) {
		if (btn.tag == type) {
			[btn setBackgroundColor:[UIColor blueColor]];
		}else{
			[btn setBackgroundColor:[UIColor whiteColor]];
		}
	}
}
#pragma mark - Main Actions
- (void)onSelectIndiceLocation:(UIButton *)sender{
	[self selectedIndiceLocationAt:sender.tag];
}

- (IBAction)onChangeChartTypeAction:(UIButton *)sender {
	[self selectedTimeChartAt:sender.tag];
}

@end
