//
//  MarketSummaryView.m
//  TCiPad
//
//  Created by Kaka on 4/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MarketSummaryView.h"
#import "UILabel+FormattedText.h"
#import "NSNumber+Formatter.h"
#import "QCData.h"

#import "TCChartView.h"
#import "DetailMarketChartVC.h"

@implementation MarketSummaryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
}

- (void)layoutSubviews{
	[super layoutSubviews];
	[self layoutIfNeeded];
}

#pragma mark - Common Init
- (void)commonInit
{
	//=========== Using Autoresize class ====================
	//* This sample to use custom view
	NSString *className = NSStringFromClass([self class]);
	UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
	rootView.frame = self.bounds;
	rootView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[self addSubview:rootView];
	[self setNeedsUpdateConstraints];
	//======== init valuw ===========
	_allowDoubleTapToViewFull = NO;
	[self setupLanguage];
	[_chartView hiddenActionControls];
}

- (void)setupLanguage{
	_lblTitle.text = [LanguageManager stringForKey:@"Market Summary"];
	_lblInsVolume.text = [LanguageManager stringForKey:@"Volume"];
	_lblInsValue.text = [LanguageManager stringForKey:@"Value"];
	_lblInsUp.text = [LanguageManager stringForKey:@"Up"];
	_lblInsDown.text = [LanguageManager stringForKey:@"Down"];
	_lblInsUnChanged.text = [LanguageManager stringForKey:@"Trade"];
	_lblInsUnTrade.text = [LanguageManager stringForKey:@"UnTrade"];
	[self setTradeCountInfoValue:@"#"];
}

#pragma mark - UTILS
- (void)setTradeCountInfoValue:(NSString *)value{
	_lblTradeCountInfo.text = [NSString stringWithFormat:@"%@: %@", [LanguageManager stringForKey:@"Trade Count"], value];
	//Make Color for subString
	[_lblTradeCountInfo colorSubString: [NSString stringWithFormat:@"%@:", [LanguageManager stringForKey:@"Trade Count"]] withColor:kGrnFlash];
}

#pragma mark - Main
- (void)updateMarketSummaryData{
	long long totalVolume = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_110_I_SCORE_VOL]longLongValue];
	long long totalVal = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_111_D_SCORE_VAL]longLongValue];
	long long totalScore = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_133_D_SCORE_OF_TRD]longLongValue];
	
	_lblVolume.text  = [[NSNumber numberWithLongLong:totalVolume] groupNumber];
	_lblValue.text  = [[NSNumber numberWithLongLong:totalVal] groupNumber];
	
	[self setTradeCountInfoValue:[[NSNumber numberWithLongLong:totalScore] groupNumber]];
	
	_lblUp.text  = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_105_I_SCORE_UP]stringValue];
	_lblDown.text           = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_106_I_SCORE_DW]stringValue];
	_lblUnChanged.text   = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_107_I_SCORE_UNCHG]stringValue];
	_lblUnTrade.text  = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_108_I_SCORE_NOTRD]stringValue];
	//Load Chart
	[_chartView loadMarketChart];
}


#pragma mark - Configuration
- (void)setAllowDoubleTapToViewFull:(BOOL)allowDoubleTapToViewFull{
	if (allowDoubleTapToViewFull) {
		if (!_tap) {
			_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlerDoubleTap:)];
			_tap.delegate = self;
			_tap.numberOfTapsRequired = 2;
		}
		[self addGestureRecognizer:_tap];
	}else{
		if (_tap) {
			[self removeGestureRecognizer:_tap];
			_tap = nil;
		}
	}
	_allowDoubleTapToViewFull = allowDoubleTapToViewFull;
}

#pragma mark - Main


- (void)handlerDoubleTap:(UITapGestureRecognizer *)tap{
	DetailMarketChartVC *_detailMarket = NEW_VC_FROM_NIB([DetailMarketChartVC class], [DetailMarketChartVC nibName]);
	//_detailMarket.providesPresentationContextTransitionStyle = YES;
	[AppDelegateAccessor.container presentViewController:_detailMarket animated:NO completion:^{
		_detailMarket.allowDoubleTapToViewFull = YES;
	}];
}

#pragma mark - UITapGestureReconized
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
	return YES;
}
@end


