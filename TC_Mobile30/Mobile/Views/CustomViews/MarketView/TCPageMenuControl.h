//
//  TCControlView.h
//  TCiPad
//
//  Created by Kaka on 5/9/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseView.h"
@class TCPageMenuControl;

//SCREEN TYPE
typedef enum: NSInteger {
	TCPageMenuControlType_Segment,
	TCPageMenuControlType_Bubble
} TCPageMenuControlType;

typedef void (^TCPageMenuCompletion)(NSString *item, NSInteger index);

@protocol TCControlViewDelegate <NSObject>
@optional
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index;
@end

@interface TCPageMenuControl : TCBaseView{
	
}
@property (strong, nonatomic) NSArray *dataSources;
@property (weak, nonatomic) id <TCControlViewDelegate> delegate;

//Configuration
@property (assign, nonatomic) NSInteger currentSelectedItem;

//Normal case
@property (strong, nonatomic) UIColor *unselectedColor;
@property (strong, nonatomic) UIColor *selectedColor;
@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) UIColor *bgColor;
//Bubble Case
@property (strong, nonatomic) UIColor *itemBgColor;
@property (strong, nonatomic) UIColor *selectedItemBgColor;
//For Segment case
@property (assign, nonatomic) float indicatorHeight;
@property (assign, nonatomic) float itemWidth;
@property (strong, nonatomic) UIColor *indicatorColor;
@property (assign, nonatomic) TCPageMenuControlType menuType;

//Main:
//- (void)setupControlWith:(NSArray *)titleControls;
//- (instancetype)initWithTitles:(NSArray *)titles;
//- (instancetype)initWithFrame:(CGRect)frame withTitle:(NSArray *)titles;
- (void)scrollToItemAtIndex:(NSInteger)index animate:(BOOL)animate;
- (void)startLoadingPageMenu;

#pragma mark - Action
- (void)didSelectItem:(TCPageMenuCompletion)completion;
@end
