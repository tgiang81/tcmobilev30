//
//  MarketScoreBoardView.m
//  TCiPad
//
//  Created by Kaka on 4/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MarketScoreBoardView.h"
#import "PADesignableView.h"
#import "JBBarChartView.h"
#import "QCData.h"

// Numerics
CGFloat const kJBBarChartViewControllerChartHeight = 250.0f;
CGFloat const kJBBarChartViewControllerChartPadding = 10.0f;
CGFloat const kJBBarChartViewControllerChartHeaderHeight = 80.0f;
CGFloat const kJBBarChartViewControllerChartHeaderPadding = 20.0f;
CGFloat const kJBBarChartViewControllerChartFooterHeight = 25.0f;
CGFloat const kJBBarChartViewControllerChartFooterPadding = 5.0f;
CGFloat const kJBBarChartViewControllerBarPadding = 5.0;
NSInteger const kJBBarChartViewControllerNumBars = 12;
NSInteger const kJBBarChartViewControllerMaxBarHeight = 10;
NSInteger const kJBBarChartViewControllerMinBarHeight = 5;

NSString const *kTotalUp_Key 		= @"TotalUp";
NSString const *kTotalDown_Key 		= @"TotalDown";
NSString const *kTotalUnChanged_Key	= @"TotalUnChanged";
NSString const *kTotalUnTrade_Key 	= @"TotalUnTrade";
NSString const *kTotalVolume_Key 	= @"TotalVolume";
NSString const *kTotalValue_Key 	= @"TotalValue";
NSString const *kTotalAll_Key 		= @"TotalAll";

typedef enum: NSInteger {
	BarChartValueType_Up = 0,
	BarChartValueType_Down,
	BarChartValueType_UnChanged,
	BarChartValueType_UnTraded
} BarChartValueType;

@interface MarketScoreBoardView()<JBBarChartViewDelegate, JBBarChartViewDataSource>{
	NSDictionary *_mainMKDict;
	NSDictionary *_warrantMKDict;
	NSDictionary *_aceMKDict;
}
@end
@implementation MarketScoreBoardView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
	[self setupUI];
}

- (void)layoutSubviews{
	[super layoutSubviews];
}

#pragma mark - Common Init
- (void)commonInit
{
	//=========== Using Autoresize class ====================
	//* This sample to use custom view
	NSString *className = NSStringFromClass([self class]);
	UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
	rootView.frame = self.bounds;
	rootView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
	[self addSubview:rootView];
	[self setNeedsUpdateConstraints];
	//======== init valuw ===========
}

#pragma mark - Main
- (void)setupUI{
	//mainMKDict = @{}.copy;
	//_warrantMKDict = @{}.copy;
	//_aceMKDict = @{}.copy;
	//Language
	_lblInsMainMarket.text = [LanguageManager stringForKey:@"Main Market"];
	_lblInsWarrantMarket.text = [LanguageManager stringForKey:@"Warrant Market"];
	_lblInsACEMarket.text = [LanguageManager stringForKey:@"ACE Market"];
	
	_lblInsUp.text = [LanguageManager stringForKey:@"UP"];
	_lblInsDown.text = [LanguageManager stringForKey:@"DOWN"];
	_lblInsUnChanged.text = [LanguageManager stringForKey:@"UNCHANGED"];
	_lblUnTraded.text = [LanguageManager stringForKey:@"UNTRADED"];
	
	//Color
	_greenNoteView.backgroundColor = kGreenColor ;
	_redNoteView.backgroundColor = kRedColor;
	_grayNoteView.backgroundColor = kGrayColor;
	_whiteNoteView.backgroundColor = kWhiteColor;
	
	//Chart
	//Delegate
	_mainMKChart.delegate = self;
	_mainMKChart.dataSource = self;
	
	_warrantMKChart.delegate = self;
	_warrantMKChart.dataSource = self;
	
	_aceMKChart.delegate = self;
	_aceMKChart.dataSource = self;
	
	_mainMKChart.minimumValue = 0.0f;
	_mainMKChart.inverted = NO;
	
	_warrantMKChart.minimumValue = 0.0f;
	_warrantMKChart.inverted = NO;
	
	_aceMKChart.minimumValue = 0.0f;
	_aceMKChart.inverted = NO;
}

#pragma mark - MAIN
//Main Call reload data
- (void)loadDataForMarket{
	NSDictionary *mainMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2201]];
	NSDictionary *warrantMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2203]];
	NSDictionary *aceMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2204]];
	[self updateData:mainMarketDict warrantDict:warrantMarketDict aceDict:aceMarketDict];
}

- (void)reloadChart{
	[_mainMKChart reloadDataAnimated:YES];
	[_warrantMKChart reloadDataAnimated:YES];
	[_aceMKChart reloadDataAnimated:YES];
}
//Private Function
- (void)updateData:(NSDictionary *)mainDict warrantDict:(NSDictionary *)warrantDict aceDict:(NSDictionary *)aceDict{
	if (mainDict) {
		_mainMKDict = [self parseCustomFieldFromMarket:mainDict];
	}
	if (warrantDict) {
		_warrantMKDict = [self parseCustomFieldFromMarket:warrantDict];
	}
	if (aceDict) {
		_aceMKDict = [self parseCustomFieldFromMarket:aceDict];
	}
	[self reloadChart];
}

#pragma mark - UTIL
- (NSDictionary *)parseCustomFieldFromMarket:(NSDictionary *)marketDict
{
	NSMutableDictionary *totalMarketDict = @{}.mutableCopy;
	NSArray *headerFIDs = @[@"37",@"110",@"105",@"106",@"107",@"108",@"109",@"111"];
	int totalUp = 0;
	int totalDown = 0;
	int totalUnchg = 0;
	int totalUntrd = 0;
	int totalTotal = 0;
	int totalVol = 0;
	int totalValue = 0;
	
	for (NSDictionary *dict in [marketDict allValues])//Mining, REITS, IPC, Finance...
	{
		// NSLog(@"marketDict %@", dict);
		for(NSString *fid in headerFIDs)//UP, Down, Unchanged..
		{
			if([fid isEqualToString:@"105"])
			{
				totalUp += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"106"])
			{
				totalDown += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"107"])
			{
				totalUnchg += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"108"])
			{
				totalUntrd += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"109"])
			{
				totalTotal += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"110"])
			{
				totalVol += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"111"])
			{
				totalValue += [[dict objectForKey:fid]integerValue];
			}
		}
	}
	
	[totalMarketDict setObject:[NSNumber numberWithInt:totalUp]     forKey:kTotalUp_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalDown]   forKey:kTotalDown_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalUnchg]  forKey:kTotalUnChanged_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalUntrd]  forKey:kTotalUnTrade_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalTotal]  forKey:kTotalAll_Key];
	[totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:kTotalVolume_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalValue]  forKey:kTotalValue_Key];
	return [totalMarketDict copy];
}
- (float)valueChartFrom:(NSDictionary *)chartDataDict atValueType:(BarChartValueType)type{
	float value = 0.0;
	switch (type) {
		case BarChartValueType_Up:
			value = [chartDataDict[kTotalUp_Key] floatValue];
			break;
		case BarChartValueType_Down:
			value = [chartDataDict[kTotalDown_Key] floatValue];
			break;
		case BarChartValueType_UnChanged:
			value = [chartDataDict[kTotalUnChanged_Key] floatValue];
			break;
		case BarChartValueType_UnTraded:
			value = [chartDataDict[kTotalUnTrade_Key] floatValue];
			break;
		default:
			break;
	}
	return value;
}
#pragma mark - JBBarChartViewDataSource

- (NSUInteger)numberOfBarsInBarChartView:(JBBarChartView *)barChartView
{
	if (barChartView == _mainMKChart) {
		if (_mainMKDict) {
			return 4;
		}
	}
	if (barChartView == _warrantMKChart) {
		if (_warrantMKDict) {
			return 4;
		}
	}
	if (barChartView == _aceMKChart) {
		if (_aceMKDict) {
			return 4;
		}
	}
	return 0;
}

- (void)barChartView:(JBBarChartView *)barChartView didSelectBarAtIndex:(NSUInteger)index touchPoint:(CGPoint)touchPoint
{
//	NSNumber *valueNumber = [self.chartData objectAtIndex:index];
//	[self.informationView setValueText:[NSString stringWithFormat:kJBStringLabelDegreesFahrenheit, [valueNumber intValue], kJBStringLabelDegreeSymbol] unitText:nil];
//	[self.informationView setTitleText:kJBStringLabelWorldwideAverage];
//	[self.informationView setHidden:NO animated:YES];
//	[self setTooltipVisible:YES animated:YES atTouchPoint:touchPoint];
//	[self.tooltipView setText:[[self.monthlySymbols objectAtIndex:index] uppercaseString]];
}

- (void)didDeselectBarChartView:(JBBarChartView *)barChartView
{
//	[self.informationView setHidden:YES animated:YES];
//	[self setTooltipVisible:NO animated:YES];
}

#pragma mark - JBBarChartViewDelegate

- (CGFloat)barChartView:(JBBarChartView *)barChartView heightForBarViewAtIndex:(NSUInteger)index{
	if (barChartView == _mainMKChart) {
		return [self valueChartFrom:_mainMKDict atValueType:(BarChartValueType)index];
	}
	if (barChartView == _warrantMKChart) {
		return [self valueChartFrom:_warrantMKDict atValueType:(BarChartValueType)index];
	}
	if (barChartView == _aceMKChart) {
		return [self valueChartFrom:_aceMKDict atValueType:(BarChartValueType)index];
	}
	return 0.0;
}

- (UIColor *)barChartView:(JBBarChartView *)barChartView colorForBarViewAtIndex:(NSUInteger)index
{
	BarChartValueType chartType = (BarChartValueType)index;
	UIColor *color;
	switch (chartType) {
		case BarChartValueType_Up:
			color = kGreenColor;
			break;
		case BarChartValueType_Down:
			color = kRedColor;
			break;
		case BarChartValueType_UnChanged:
			color = kGrayColor;
			break;
		case BarChartValueType_UnTraded:
			color = kWhiteColor;
			break;
		default:
			break;
	}
	return color;
}

- (UIColor *)barSelectionColorForBarChartView:(JBBarChartView *)barChartView
{
	return [UIColor purpleColor];
}

- (CGFloat)barPaddingForBarChartView:(JBBarChartView *)barChartView
{
	return kJBBarChartViewControllerBarPadding;
}
@end
