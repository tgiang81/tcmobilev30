//
//  MarketScoreBoardView.h
//  TCiPad
//
//  Created by Kaka on 4/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"

@class JBBarChartView;
@class PADesignableView;
@interface MarketScoreBoardView : UIView{
	
}
//Note
@property (weak, nonatomic) IBOutlet PADesignableView *greenNoteView;
@property (weak, nonatomic) IBOutlet PADesignableView *redNoteView;
@property (weak, nonatomic) IBOutlet PADesignableView *grayNoteView;
@property (weak, nonatomic) IBOutlet PADesignableView *whiteNoteView;

@property (weak, nonatomic) IBOutlet UILabel *lblInsUp;
@property (weak, nonatomic) IBOutlet UILabel *lblInsDown;
@property (weak, nonatomic) IBOutlet UILabel *lblInsUnChanged;
@property (weak, nonatomic) IBOutlet UILabel *lblUnTraded;

//Market
@property (weak, nonatomic) IBOutlet UILabel *lblInsMainMarket;
@property (weak, nonatomic) IBOutlet UILabel *lblInsWarrantMarket;
@property (weak, nonatomic) IBOutlet UILabel *lblInsACEMarket;

//ChartView
@property (weak, nonatomic) IBOutlet JBBarChartView *mainMKChart;
@property (weak, nonatomic) IBOutlet JBBarChartView *warrantMKChart;
@property (weak, nonatomic) IBOutlet JBBarChartView *aceMKChart;


//Main Function
- (void)loadDataForMarket;
- (void)reloadChart;
@end
