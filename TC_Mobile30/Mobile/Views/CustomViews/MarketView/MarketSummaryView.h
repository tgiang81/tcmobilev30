//
//  MarketSummaryView.h
//  TCiPad
//
//  Created by Kaka on 4/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"

@class TCChartView;
@interface MarketSummaryView : UIView<UIGestureRecognizerDelegate>{
	UITapGestureRecognizer *_tap;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblInsVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblInsValue;
@property (weak, nonatomic) IBOutlet UILabel *lblInsUp;
@property (weak, nonatomic) IBOutlet UILabel *lblInsDown;
@property (weak, nonatomic) IBOutlet UILabel *lblInsUnChanged;
@property (weak, nonatomic) IBOutlet UILabel *lblInsUnTrade;
@property (weak, nonatomic) IBOutlet UILabel *lblTradeCountInfo;

//Value
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblUp;
@property (weak, nonatomic) IBOutlet UILabel *lblUnChanged;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDown;
@property (weak, nonatomic) IBOutlet UILabel *lblUnTrade;

@property (weak, nonatomic) IBOutlet TCChartView *chartView;

//Configuration
@property (assign, nonatomic) BOOL allowDoubleTapToViewFull;


#pragma mark - Main
- (void)updateMarketSummaryData;
@end
