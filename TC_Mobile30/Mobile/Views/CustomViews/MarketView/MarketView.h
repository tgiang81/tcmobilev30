//
//  MarketView.h
//  TCiPad
//
//  Created by Kaka on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketView : UIView{
	
}
@property (weak, nonatomic) IBOutlet UIView *topControlView;
@property (weak, nonatomic) IBOutlet UIScrollView *topScrollView;

@property (weak, nonatomic) IBOutlet UIView *bottomControlView;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;
@property (weak, nonatomic) IBOutlet UIStackView *stackTimeChartControl;
@property (weak, nonatomic) IBOutlet UIView *mainContent;


@end
