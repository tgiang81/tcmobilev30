//
//  TCControlView.m
//  TCiPad
//
//  Created by Kaka on 5/9/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCPageMenuControl.h"
#import "TCBaseView.h"
#import "TCBaseButton.h"
#import "NSString+SizeOfString.h"
#import "UIView+ShadowBorder.h"

#import "AllCommon.h"

#define kDEFAULT_PAGE_ITEM_WIDTH				75
#define kDEFAULT_BUBBLE_ITEM_WIDTH				75
#define KDEFAULT_INDICATOR_ITEM_HEIGHT 			2

@interface TCPageMenuControl(){
	NSMutableArray *_buttonControls;
	NSMutableArray *_indicatorControls;
	float _widthItem;
}
@property (strong, nonatomic) UIScrollView *scrollContent;
@property (strong, nonatomic) TCPageMenuCompletion completion;
@end
@implementation TCPageMenuControl

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init{
	self = [super init];
	if (self) {
		//init default
		[self iniDefaultValue];
	}
	return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	if (self) {
		//init default value
		[self iniDefaultValue];
	}
	return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
	self = [super initWithCoder:aDecoder];
	if (self) {
		//init default value
		[self iniDefaultValue];
	}
	return self;
}

- (void)awakeFromNib{
	[super awakeFromNib];
	//Do something...
	self.layer.masksToBounds = YES;
}

- (void)layoutSubviews{
	[super layoutSubviews];
	[self updateConstraintsIfNeeded];
}
#pragma mark - Setup
- (void)iniDefaultValue{
	self.itemWidth = kDEFAULT_PAGE_ITEM_WIDTH;
	self.indicatorHeight = KDEFAULT_INDICATOR_ITEM_HEIGHT;
	self.currentSelectedItem = 0;
	self.font = AppFont_MainFontMediumWithSize(14);
	self.menuType = TCPageMenuControlType_Segment;
	
	self.itemBgColor = TC_COLOR_FROM_HEX(@"ffffff");
	self.selectedItemBgColor = TC_COLOR_FROM_HEX([BrokerManager shareInstance].detailBroker.color.mainTint);
	
	//Prepare control
	_buttonControls = @[].mutableCopy;
	_indicatorControls = @[].mutableCopy;
	
	//+++ Add Notification for Update Theme
	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTheme:) name:kDidChangeThemeNotification object:nil];
	[self updateTheme:nil];
}

- (void)updateTheme:(NSNotification *)noti{
	self.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	self.indicatorColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].indicatorColor);
	self.unselectedColor = [UIColor blackColor];//TC_COLOR_FROM_HEX([ThemeManager shareInstance].unselectedTextColor);
	self.selectedColor = [UIColor grayColor];//TC_COLOR_FROM_HEX([ThemeManager shareInstance].selectedTextColor);
}

#pragma mark - UpdateUI
- (void)updateUI{
	//Remove old
	[_buttonControls removeAllObjects];
	[_indicatorControls removeAllObjects];
	if (self.scrollContent) {
		for (UIView *v in _scrollContent.subviews) {
			for (id subView in v.subviews) {
				[subView removeFromSuperview];
			}
			[v removeFromSuperview];
		}
		//Remove all old controls
		[_scrollContent removeFromSuperview];
		_scrollContent = nil;
	}
	if (!_dataSources || _dataSources.count == 0) {
		return;
	}
	//Start Add Items
	//*******************
	[self populatePageMenu:self.menuType];
	//+++Select item
	[self setSelectItemAt:_currentSelectedItem];
	[self scrollToItemAtIndex:_currentSelectedItem animate:NO];
}


#pragma mark - Main
- (void)startLoadingPageMenu{
	[self updateUI];
}

#pragma mark - Main Action
- (void)onItemAction:(TCBaseButton *)sender{
	self.currentSelectedItem = sender.tag;
	if (_delegate) {
		[_delegate didSelectControlView:self atIndex:_currentSelectedItem];
	}
	NSString *items = self.dataSources[self.currentSelectedItem];
	if (self.completion) {
		self.completion(items, _currentSelectedItem);
	}
}

#pragma mark - Config
#pragma mark - Configuration
- (void)setMenuType:(TCPageMenuControlType)menuType{
	_menuType = menuType;
	self.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	if (menuType == TCPageMenuControlType_Segment) {
		self.indicatorColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].indicatorColor);
		self.unselectedColor = [UIColor blackColor];//TC_COLOR_FROM_HEX([ThemeManager shareInstance].unselectedTextColor);
		self.selectedColor = [UIColor whiteColor];//TC_COLOR_FROM_HEX([ThemeManager shareInstance].selectedTextColor);
	}else{
		self.unselectedColor = [UIColor blackColor];//TC_COLOR_FROM_HEX(@"616161");
		self.selectedColor =  [UIColor whiteColor];//TC_COLOR_FROM_HEX(@"ffffff");
	}
}
//1: Override set Title Array Control
- (void)setDataSources:(NSArray *)titleControls{
	_dataSources = titleControls;
	//[self updateUI];
}

- (void)setCurrentSelectedItem:(NSInteger)currentSelectedItem{
	_currentSelectedItem = currentSelectedItem;
	[self setSelectItemAt:currentSelectedItem];
}
- (void)setupControlWith:(NSArray *)titleControls{
	_dataSources = titleControls;
	//[self updateUI];
}

- (void)setFont:(UIFont *)font{
	_font = font;
	/*
    for (TCBaseButton *button in _buttonControls) {
        button.titleLabel.font = font;
    }
	*/
}
- (void)setBgColor:(UIColor *)bgColor{
	self.backgroundColor = bgColor;
	_bgColor = bgColor;
}
- (void)setIndicatorColor:(UIColor *)tintColor{
	_indicatorColor = tintColor;
	//[self updateUI];
	//[self setSelectItemAt:_currentSelectedItem];
}
- (void)setUnselectedColor:(UIColor *)titleColor{
	_unselectedColor = titleColor;
	//[self updateUI];
	/*
	for (TCBaseButton *button in _buttonControls) {
		[button setTitleColor:titleColor forState:UIControlStateNormal];
	}
	[self setSelectItemAt:_currentSelectedItem];
	*/
}

- (void)setSelectedColor:(UIColor *)selectedTitleColor{
	//[self updateUI];
	_selectedColor = selectedTitleColor;
	for (TCBaseButton *button in _buttonControls) {
		[button setTitleColor:selectedTitleColor forState:UIControlStateSelected];
	}
	[self setSelectItemAt:_currentSelectedItem];
}
- (void)setItemWidth:(float)itemWidth{
	_itemWidth = itemWidth;
	//[self updateUI];
}

#pragma mark - UTIL
- (void)setSelectItemAt:(NSInteger)index{
	if (self.menuType == TCPageMenuControlType_Segment) {
		for (TCBaseButton *buttonItem in _buttonControls) {
			if (buttonItem.tag == index) {
				buttonItem.selected = YES;
			}else{
				buttonItem.selected = NO;
			}
		}
		for (UIImageView *indicatorItem in _indicatorControls) {
			if (indicatorItem.tag == index) {
				indicatorItem.backgroundColor = self.indicatorColor;
			}else{
				indicatorItem.backgroundColor = [UIColor clearColor];
			}
		}
	}else{
		for (TCBaseButton *buttonItem in _buttonControls) {
			if (buttonItem.tag == index) {
				buttonItem.selected = YES;
				buttonItem.backgroundColor = self.selectedItemBgColor;
			}else{
				buttonItem.selected = NO;
				buttonItem.backgroundColor = self.itemBgColor;
			}
		}
	}
}

- (void)populatePageMenu:(TCPageMenuControlType)type{
	//Default
	_scrollContent = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	_scrollContent.backgroundColor = [UIColor clearColor];
	[self addSubview:_scrollContent];
	self.layer.masksToBounds = NO;
	self.scrollContent.layer.masksToBounds = NO;
	switch (type) {
		case TCPageMenuControlType_Segment:{
			float mediumWidthItem = (self.bounds.size.width / _dataSources.count);
			if (mediumWidthItem > _itemWidth) {
				if (mediumWidthItem > kDEFAULT_PAGE_ITEM_WIDTH) {
					_widthItem = kDEFAULT_PAGE_ITEM_WIDTH;
				}else{
					_widthItem = mediumWidthItem;
				}
			}else{
				_widthItem = _itemWidth;
			}
			
			for (int i = 0; i < _dataSources.count; i ++) {
				//View Item
				TCBaseView *viewItem = [[TCBaseView alloc] initWithFrame:CGRectMake(i * _widthItem, 0, _widthItem, self.bounds.size.height)];
				viewItem.backgroundColor = [UIColor clearColor];
				viewItem.tag = i;
				//Button Item
				TCBaseButton *buttonItem = [TCBaseButton buttonWithType:UIButtonTypeCustom];
				buttonItem.frame = CGRectMake(0, 0, _widthItem, self.bounds.size.height - _indicatorHeight);
				[buttonItem setTitle:_dataSources[i] forState:UIControlStateNormal];
				
				[buttonItem setTitleColor:_unselectedColor forState:UIControlStateNormal];
				[buttonItem setTitleColor:_selectedColor forState:UIControlStateSelected];
				
				[buttonItem addTarget:self action:@selector(onItemAction:) forControlEvents:UIControlEventTouchUpInside];
				buttonItem.titleLabel.font = self.font;
				[buttonItem setTitleColor:self.unselectedColor forState:UIControlStateNormal];
				[buttonItem setTitleColor:self.selectedColor forState:UIControlStateSelected];
				buttonItem.tag = i;
				buttonItem.showsTouchWhenHighlighted = YES;
				//Indicator
				UIImageView *indicatorItem = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - _indicatorHeight, _widthItem, self.indicatorHeight)];
				indicatorItem.backgroundColor = [UIColor clearColor];
				indicatorItem.tag = i;
				[_indicatorControls addObject:indicatorItem];
				//Add Control
				[viewItem addSubview:indicatorItem];
				[viewItem addSubview:buttonItem];
				viewItem.layer.masksToBounds = YES;
				[_scrollContent addSubview:viewItem];
				//Cache item
				[_buttonControls addObject:buttonItem];
			}
			[_scrollContent setContentSize:CGSizeMake(_dataSources.count * _widthItem, self.bounds.size.height)];
			[_scrollContent setBounces:YES];
			//_scrollContent.delaysContentTouches = NO;
		}
			break;
		case TCPageMenuControlType_Bubble:{
			CGFloat widtContent = 0;
			CGFloat spacingItem = 5.0;
			for (int i = 0; i < _dataSources.count; i ++) {
				NSString *itemTitle = _dataSources[i];
				if (itemTitle.length == 0) {
					itemTitle = @"###";
				}
				CGFloat widthItem = [itemTitle widthOfString:self.font] + 10;//10 pixel for Margin
				if (widthItem < kDEFAULT_BUBBLE_ITEM_WIDTH) {
					widthItem = kDEFAULT_BUBBLE_ITEM_WIDTH;
				}
				float xPoint = widtContent;
				if (i == self.dataSources.count - 1) {
					widtContent += widthItem;
				}else{
					widtContent += (widthItem + spacingItem);
				}
				
				TCBaseView *viewItem = [[TCBaseView alloc] initWithFrame:CGRectMake(xPoint, 0, widthItem, self.bounds.size.height)];
				viewItem.backgroundColor = [UIColor clearColor];
				viewItem.layer.masksToBounds = NO;
				//Button Item
				TCBaseButton *buttonItem = [TCBaseButton buttonWithType:UIButtonTypeCustom];
				buttonItem.frame = CGRectMake(0, 0, widthItem, self.bounds.size.height);
				[viewItem addSubview:buttonItem];
				[buttonItem setTitle:itemTitle forState:UIControlStateNormal];
				
				[buttonItem setTitleColor:_unselectedColor forState:UIControlStateNormal];
				[buttonItem setTitleColor:_selectedColor forState:UIControlStateSelected];
				
				[buttonItem addTarget:self action:@selector(onItemAction:) forControlEvents:UIControlEventTouchUpInside];
				buttonItem.titleLabel.font = self.font;
				[buttonItem setTitleColor:self.unselectedColor forState:UIControlStateNormal];
				[buttonItem setTitleColor:self.selectedColor forState:UIControlStateSelected];
				buttonItem.tag = i;
				buttonItem.showsTouchWhenHighlighted = YES;
				[buttonItem makeBorderShadow];
				buttonItem.cornerRadius = 12.0;
				[_scrollContent addSubview:viewItem];
				//Cache item
				[_buttonControls addObject:buttonItem];
			}
			float widthContentSize = widtContent;//((_dataSources.count - 1) * spacingItem) + widtContent;
			[_scrollContent setContentSize:CGSizeMake(widthContentSize, self.bounds.size.height)];
			[_scrollContent setBounces:YES];
		}
			break;
			
		default:
			break;
	}
	_scrollContent.scrollEnabled = YES;
	[_scrollContent setShowsHorizontalScrollIndicator:NO];
}

#pragma mark - SELECT ITEM
- (void)didSelectItem:(TCPageMenuCompletion)completion{
	if (completion) {
		self.completion = completion;
		completion = nil;
	}
}
#pragma mark - Scroll To Item
- (void)scrollToItemAtIndex:(NSInteger)index animate:(BOOL)animate{
	//Return if all of item was visible
	if (self.menuType == TCPageMenuControlType_Segment) {
		if (_dataSources.count * _widthItem <= self.frame.size.width) {
			return;
		}
		float xPoint = index * _widthItem;
		if (index != 0) {
			xPoint = (index - 1) * _widthItem;
		}
		//Just scroll to visible item. Not increase more space
		if (index == _dataSources.count - 1 && ((index + 1) * _widthItem == _scrollContent.contentSize.width)) {
			xPoint = (index - 2 ) * _widthItem;
		}
		CGPoint offset = CGPointMake(xPoint, 0);
		[_scrollContent setContentOffset:offset animated:animate];
	}
	if (self.menuType == TCPageMenuControlType_Bubble) {
		if (self.scrollContent.contentSize.width <= self.frame.size.width) {
			return;
		}
		TCBaseButton *item = _buttonControls[index];
		TCBaseView *viewItem = (TCBaseView *)item.superview;
		[_scrollContent scrollRectToVisible:viewItem.frame animated:NO];
	}
}
@end
