//
//  TCListContentCell.h
//  TCiPad
//
//  Created by Kaka on 8/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface TCListContentCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@end
