//
//  TCListContentView.m
//  TCiPad
//
//  Created by Kaka on 8/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCListContentView.h"
#import "TCListContentCell.h"

@interface TCListContentView ()<UITableViewDelegate, UITableViewDataSource>{
	NSArray *_dataSources;
}
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;
//OUTLETS
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@end
@implementation TCListContentView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithControlTitles:(NSArray *)titles{
	self = [super init];
	if (self) {
		[self commonInit];
		//self.controlView.titleControls = titles;
	}
	return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame contents:(NSArray *)contents
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
		self.contents = contents;
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
}
#pragma mark - Common Init
- (void)commonInit
{
	//++++ Using Auto Layout ++++
	NSString *className = NSStringFromClass([self class]);
	_customConstraints = [[NSMutableArray alloc] init];
	UIView *view = nil;
	NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	for (id object in objects) {
		if ([object isKindOfClass:[UIView class]]) {
			view = object;
			break;
		}
	}
	
	if (view != nil) {
		_containerView = view;
		view.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:view];
		[self setNeedsUpdateConstraints];
	}
	[self updateConstraints];
	//Initdata
	_dataSources = @[].copy;
	_curSelectedIndex = -1;
	//Set delegate
	[_tblContent registerNib:[UINib nibWithNibName:[TCListContentCell nibName] bundle:nil] forCellReuseIdentifier:[TCListContentCell reuseIdentifier]];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
}

- (void)updateConstraints
{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);
		
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}

#pragma mark - Main
- (void)setContents:(NSArray *)contents{
	_dataSources = contents;
	[_tblContent reloadData];
}
#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_dataSources count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	TCListContentCell *cell = [tableView dequeueReusableCellWithIdentifier:[TCListContentCell reuseIdentifier]];
	if (!cell) {
		cell = [[TCListContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[TCListContentCell reuseIdentifier]];
	}
	if (_curSelectedIndex == indexPath.row) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}else{
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	cell.tintColor = AppColor_TintColor;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.lblContent.text = _dataSources[indexPath.row];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	if (_delegate) {
		[_delegate didSelectItemAt:indexPath.row inView:self];
	}
}

#pragma mark - Main
- (void)reloadData{
	[_tblContent reloadData];
	[self setNeedsDisplay];
}
@end
