//
//  TCListContentView.h
//  TCiPad
//
//  Created by Kaka on 8/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TCListContentView;
@protocol TCListContentViewDelegate <NSObject>
@optional
- (void)didSelectItemAt:(NSInteger)index inView:(TCListContentView *)listView;
@end

@interface TCListContentView : UIView{
	
}
@property (strong, nonatomic) NSArray *contents;
@property (assign, nonatomic) NSInteger curSelectedIndex;
@property (assign, nonatomic) id <TCListContentViewDelegate> delegate;

//Main
//Can use this to show content or passing to contents array directyly
- (instancetype)initWithFrame:(CGRect)frame contents:(NSArray *)contents;

- (void)reloadData;
@end
