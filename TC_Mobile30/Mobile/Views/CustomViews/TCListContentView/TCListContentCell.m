//
//  TCListContentCell.m
//  TCiPad
//
//  Created by Kaka on 8/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCListContentCell.h"

@implementation TCListContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setup
- (void)setup{
	_lblContent.textColor = AppColor_HighlightTextColor;
	self.backgroundColor = [UIColor whiteColor];
}
@end
