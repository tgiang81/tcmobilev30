//
//  StatusInfoView.h
//  TC_Mobile30
//
//  Created by Kaka on 9/21/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseView.h"
typedef void (^TCStatusAction)(void);
@interface TCStatusInfoView : TCBaseView{
	
}
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) TCStatusAction retryAction;

#pragma mark - MAIN
- (void)showStatus:(NSString *)statusMessage inView:(UIView *)containerView retryAction:(TCStatusAction)action;
- (void)justShowStatus:(NSString *)statusMessage inView:(UIView *)containerView;

- (void)hideStatus:(void(^)(void))completion;

@end
