//
//  StatusInfoView.m
//  TC_Mobile30
//
//  Created by Kaka on 9/21/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCStatusInfoView.h"
#import "TCBaseButton.h"
#import "NSString+SizeOfString.h"
#import "AllCommon.h"
#import "UIImageView+TCUtil.h"
#import "LanguageKey.h"

#define kDEFAULT_HEIGHT_MESSAGE	30

@interface TCStatusInfoView (){
}
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;
//OUTLETS
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnRetry;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageHeightConstrain;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

@implementation TCStatusInfoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
	[self setup];
}
#pragma mark - Common Init
- (void)commonInit{
	//++++ Using Auto Layout ++++
	NSString *className = NSStringFromClass([self class]);
	_customConstraints = [[NSMutableArray alloc] init];
	UIView *view = nil;
	NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	for (id object in objects) {
		if ([object isKindOfClass:[UIView class]]) {
			view = object;
			break;
		}
	}
	
	if (view != nil) {
		_containerView = view;
		view.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:view];
		[self setNeedsUpdateConstraints];
	}
	[self updateConstraints];
	
	//Common init
	[self setup];
}

- (void)updateConstraints
{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}

- (void)setup{
	[_btnRetry setTitle:[LanguageManager stringForKey:@"Tap to retry"] forState:UIControlStateNormal];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTheme:) name:kDidChangeThemeNotification object:nil];;
	[self updateTheme:nil];
}

- (void)updateTheme:(NSNotification *)noti{
	//Font and color below
	self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	self.lblMessage.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].unselectedTextColor);
	[self.imgIcon toColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].unselectedTextColor)];
	self.btnRetry.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].unselectedTextColor);
}

#pragma mark - MAIN
- (void)setMessage:(NSString *)message{
	//Calculate height of message
	float heightOfMessage = [message heightForWidth:_lblMessage.frame.size.width usingFont:AppFont_MainFontMediumWithSize(14)];
	if (heightOfMessage < kDEFAULT_HEIGHT_MESSAGE) {
		heightOfMessage = kDEFAULT_HEIGHT_MESSAGE;
	}
	_messageHeightConstrain.constant = heightOfMessage;
	_lblMessage.text = message? message : [LanguageManager stringForKey:Something_wrongs];
	_message = message;
	[self layoutIfNeeded];
}

- (void)justShowStatus:(NSString *)statusMessage inView:(UIView *)containerView{
	[self addToView:containerView];
	self.message = statusMessage;
	_btnRetry.hidden = YES;
}

- (void)showStatus:(NSString *)statusMessage inView:(UIView *)containerView retryAction:(TCStatusAction)action{
	_btnRetry.hidden = NO;
	[self addToView:containerView];
	self.message = statusMessage;
	self.retryAction = action;
	action = nil;
}

- (void)hideStatus:(void(^)(void))completion{
	//Remove self with animation
	self.alpha = 1.0;
	[UIView animateWithDuration:0.35 animations:^{
		self.alpha = 0.0;
	} completion:^(BOOL finished) {
		[self removeConstraints:self.constraints];
		[self removeFromSuperview];
		if (completion) {
			completion();
		}
	}];
}
#pragma mark - UTILS
- (void)addToView:(UIView *)view{
	[view addSubview:self];
	[view bringSubviewToFront:self];
	//Add Constrain
	[self setTranslatesAutoresizingMaskIntoConstraints:NO];
	//Add Constrain
	NSMutableArray *_constrains = @[].mutableCopy;
	NSLayoutConstraint *topAnchor = [self.topAnchor constraintEqualToAnchor:view.topAnchor];
	[_constrains addObject:topAnchor];
	NSLayoutConstraint *bottomAnchor = [self.bottomAnchor constraintEqualToAnchor:view.bottomAnchor constant:0];
	[_constrains addObject:bottomAnchor];
	NSLayoutConstraint *leadintAnchor = [self.leadingAnchor constraintEqualToAnchor:view.leadingAnchor constant:0];
	[_constrains addObject:leadintAnchor];
	NSLayoutConstraint *trailingAnchor = [self.trailingAnchor constraintEqualToAnchor:view.trailingAnchor constant:0];
	[_constrains addObject:trailingAnchor];
	[NSLayoutConstraint activateConstraints:_constrains];
	//Make animation
	self.alpha = 0.0;
	[UIView animateWithDuration:0.35 animations:^{
		self.alpha = 1.0;
	}];
}
#pragma mark - ACTION
- (IBAction)onRetryAction:(id)sender {
	[self hideStatus:^{
		if (self.retryAction) {
			self.retryAction();
		}
	}];
}
@end
