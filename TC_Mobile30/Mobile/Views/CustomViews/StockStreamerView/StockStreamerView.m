//
//  StockStreamerView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "StockStreamerView.h"
#import "ThemeManager.h"
#import "LanguageManager.h"
@implementation StockStreamerView
- (void)awakeFromNib{
    [super awakeFromNib];
    //self.v_header.backgroundColor = UIColorFromRGB(0x425A6C);
    self.v_header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_headerBgColor);
    [self.leftSteamerView registerNib: [UINib nibWithNibName: @"StockStreamerCell" bundle:nil] forCellReuseIdentifier: @"StockStreamerCell"];
    self.leftSteamerView.alwaysBounceVertical = NO;
    
    [self setupColor];
    
    _lbl_Name.text = [LanguageManager stringForKey:@"NAME"];
    _lbl_Price.text = [LanguageManager stringForKey:@"L.PRICE"];
    _lbl_Pi.text = [LanguageManager stringForKey:@"PI"];
    _lbl_LQTy.text = [LanguageManager stringForKey:@"L.QTY"];
    _lbl_Change.text = [LanguageManager stringForKey:@"CHG%"];
    _lbl_Time.text = [LanguageManager stringForKey:@"TIME"];
}
- (void)setupColor{
    _lbl_Name.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textCellColor);
    _lbl_Price.textColor = _lbl_Name.textColor;
    _lbl_Pi.textColor = _lbl_Name.textColor;
    _lbl_LQTy.textColor = _lbl_Name.textColor;
    _lbl_Change.textColor = _lbl_Name.textColor;
    _lbl_Time.textColor = _lbl_Name.textColor;
}
@end
