//
//  StockStreamerCardMode.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "StockStreamerCardModeView.h"
#import "StockStreamerCell.h"
@implementation StockStreamerCardModeView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self.clv_content registerNib:[UINib nibWithNibName: @"StockTrackerCell" bundle:nil] forCellWithReuseIdentifier:@"StockTrackerCell"];
}

@end
