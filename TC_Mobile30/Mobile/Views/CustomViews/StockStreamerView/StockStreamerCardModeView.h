//
//  StockStreamerCardMode.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCBaseSubView.h"
#import "BaseCollectionView.h"
@interface StockStreamerCardModeView : TCBaseSubView
@property (weak, nonatomic) IBOutlet BaseCollectionView *clv_content;

@end
