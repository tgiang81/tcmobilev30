//
//  StockStreamerView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCBaseSubView.h"
@interface StockStreamerView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UITableView *leftSteamerView;
@property (weak, nonatomic) IBOutlet UIView *v_header;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Pi;
@property (weak, nonatomic) IBOutlet UILabel *lbl_LQTy;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Change;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Time;

@end
