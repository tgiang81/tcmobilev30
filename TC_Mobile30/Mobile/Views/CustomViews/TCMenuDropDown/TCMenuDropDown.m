//
//  TCMenuDropDown.m
//  TCiPad
//
//  Created by Kaka on 8/24/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCMenuDropDown.h"
#import "TCListContentView.h"
#import "TCListContentCell.h"
#import "MKDropdownMenu.h"
#import "TCDropDown.h"
#define kHEIGHT_OF_ITEM	44
@interface TCMenuDropDown ()<UITableViewDelegate, UITableViewDataSource, MKDropdownMenuDelegate, MKDropdownMenuDataSource>{
	BOOL _isExpanded;
	float _fullHeight;
    TCDropDown *_sectorDropdown;
}

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;

//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgIndicator;
@end


@implementation TCMenuDropDown

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (void)layoutSubviews{
	[super layoutSubviews];
}
- (void)awakeFromNib{
	[super awakeFromNib];
	self.clipsToBounds = YES;

}
- (void)addCustomView:(UIView *)customView{
    [_sectorDropdown addCustomView:customView];
}
- (void)openComponent:(NSInteger)component animated:(BOOL)animated{
    if(_useMkDropDown){
        if(!_isExpanded){
            _isExpanded = YES;
            [self.v_dropdown openComponent:component animated:animated];
        }
        [self.v_dropdown reloadComponent:component];
    }else{
        [_sectorDropdown show];
    }
}
- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didCloseComponent:(NSInteger)component{
    _isExpanded = NO;
}
- (void)closeAll{
    if(_useMkDropDown){
        [self.v_dropdown closeAllComponentsAnimated:YES];
    }else{
        [_sectorDropdown hide];
    }
}
- (void)setSourceView:(UIView *)sourceView{
    _sourceView = sourceView;
    _sectorDropdown.sourceView = sourceView;
}
- (void)setupDropdown{
    _sectorDropdown = [[TCDropDown alloc] init];
    _sectorDropdown.sourceView = self;
    _sectorDropdown.isSearchable = NO;
    //_sectorDropdown.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
    //_sectorDropdown.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
    [_sectorDropdown didSelectItem:^(NSString *item, NSInteger index) {
        self.lblTitleMenu.text = item;
        if([self.delegate respondsToSelector:@selector(didSelectRowAt:inView:)]){
            [self.delegate didSelectRowAt:index inView:self];
        }
        if(self.popCellBlock){
            self.popCellBlock(self, index, 0);
        }
        
    }];
}
#pragma mark - Common Init
- (void)commonInit
{
	//* This sample to use custom view
//	NSString *className = NSStringFromClass([self class]);
//	UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
//	rootView.frame = self.bounds;
//	rootView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//	[self addSubview:rootView];
//	[self setNeedsUpdateConstraints];
	//++++ Using Auto Layout ++++
	NSString *className = NSStringFromClass([self class]);
	_customConstraints = [[NSMutableArray alloc] init];
	UIView *view = nil;
	NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	for (id object in objects) {
		if ([object isKindOfClass:[UIView class]]) {
			view = object;
			break;
		}
	}

	if (view != nil) {
		_containerView = view;
		view.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:view];
		[self setNeedsUpdateConstraints];
	}
	[self updateConstraints];
	//Initdata
	[self initData];
    [self setupDropdown];
    self.v_dropdown.disclosureIndicatorImage = self.imgIndicator.image;
    self.topSpace = 8;
}

- (void)updateConstraints
{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);
		
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}
#pragma mark - Main
- (void)initData{
	_lblTitleMenu.text = @"";
	_menuTitles = @[].copy;
	_titleAlign = NSTextAlignmentCenter;
	self.bgColor = [UIColor whiteColor];
	self.selectedViewColor =  RGB(46, 67, 75);
	_contentView.backgroundColor = [UIColor clearColor];
	[_tblContent registerNib:[UINib nibWithNibName:[TCListContentCell nibName] bundle:nil] forCellReuseIdentifier:[TCListContentCell reuseIdentifier]];
	_contentView.hidden = YES;
	[_tblContent setBounces:YES];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_isExpanded = NO;
    
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_tblContent];
}
#pragma mark - ACTIONS

- (IBAction)actionShowDropDown:(id)sender {
    if(_sectorDropdown.didShowDropdown){
        [_sectorDropdown hide];
    }else{
        [_sectorDropdown show];
    }
}
- (IBAction)onExpandAction:(id)sender {
	[self.superview bringSubviewToFront:self];
	_isExpanded = !_isExpanded;
	_contentView.hidden = NO;
	float newHeight  = _isExpanded ? _fullHeight : (self.height == 0 ? kHEIGHT_OF_ITEM:self.height);
	NSLayoutConstraint *_heightConstrains = [self getHeightConstrainOfSelf];
	//+++ Not use frame
	CGRect tmpFrame = self.frame;
	tmpFrame.size.height = newHeight;
	if (!_heightConstrains) {
		//+++ Use frame to show/hide
		[self setTranslatesAutoresizingMaskIntoConstraints:YES];
	}else{
		_heightConstrains.constant = newHeight;
	}
	/* +++ No need transform icon now
	[UIView animateWithDuration:0.2 animations:^{
		if (self->_isExpanded) {
			self.imgIndicator.transform = CGAffineTransformMakeRotation(M_PI);
		}else{
			self.imgIndicator.transform = CGAffineTransformIdentity;
		}
	} completion:^(BOOL finished) {
	}];
	 */
	[UIView animateWithDuration:0.35 delay:0.01 options:UIViewAnimationOptionCurveLinear animations:^{
		if (!_heightConstrains) {
			//+++ Use frame to show/hide
			self.frame = tmpFrame;
		}else{
			[self.superview layoutIfNeeded];
		}
	} completion:^(BOOL finished) {
		if (!self->_isExpanded) {
			self->_contentView.hidden = YES;
		}
	}];
}
- (void)setUseMkDropDown:(BOOL)useMkDropDown{
    _useMkDropDown = useMkDropDown;
    if(useMkDropDown){
        [self.v_dropdown setHidden:NO];
        [self.titleView setHidden:YES];
        self.v_dropdown.dataSource = self;
        self.v_dropdown.delegate = self;
    }else{
        [self.v_dropdown setHidden:YES];
        [self.titleView setHidden:NO];
    }
}
- (void)setDismissAutoResize:(BOOL)dismissAutoResize{
    _dismissAutoResize = dismissAutoResize;
    if(dismissAutoResize){
        [self removeConstraint:self.cst_Left];
        
        self.cst_Left = [NSLayoutConstraint constraintWithItem:self.btn_Indicator
                                                     attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                     attribute: NSLayoutAttributeRight
                                                    multiplier:1
                                                      constant:-8];
        [self addConstraint:self.cst_Left];
    }
}
#pragma mark - Override function
- (void)setTopSpace:(CGFloat)topSpace{
    _topSpace = topSpace;
    self.v_dropdown.topSpace = topSpace;
}
- (void)setRadius:(CGFloat)radius{
    _radius = radius;
    self.layer.cornerRadius = radius;
    self.v_dropdown.dropdownCornerRadius = self.radius;
}
- (void)setBgColor:(UIColor *)bgColor{
	_bgColor = bgColor;
	[self updateUI];
}
- (void)setTintIcon:(UIColor *)tintIcon{
    _tintIcon = tintIcon;
    [self updateUI];
}
- (void)setSelectedViewColor:(UIColor *)selectedViewColor{
	_selectedViewColor = selectedViewColor;
    [_titleView setBackgroundColor:selectedViewColor];
}
- (void)setTitleMenu:(NSString *)titleMenu{
	//_titleView.hidden = (titleMenu.length == 0) ? YES : NO;
	_titleMenu = titleMenu;
    [self updateUI];
}
- (void)setNumberItemVisible:(NSInteger)numberItemVisible{
//    if (numberItemVisible > _menuTitles.count) {
//        numberItemVisible = _menuTitles.count;
//    }
	_numberItemVisible = numberItemVisible;
	[self updateUI];
    [self reloadDropDown];
}

- (void)setMenuTitles:(NSArray *)menuTitles{
	_menuTitles = menuTitles;
	//Default selected First
	if (!_titleMenu) {
		_lblTitleMenu.text = menuTitles.count > 0 ? menuTitles[0] : @"";
	}
	[self updateUI];
}

- (void)setTitleAlign:(NSTextAlignment)titleAlign{
	_titleAlign = titleAlign;
    _lblTitleMenu.textAlignment = titleAlign;
    [_v_dropdown setComponentTextAlignment:titleAlign];
}
- (void)setSelectedLabelColor:(UIColor *)selectedLabelColor{
    _selectedLabelColor = selectedLabelColor;
    _lblTitleMenu.textColor = selectedLabelColor;
}
- (void)setIndicatorImage:(UIImage *)indicatorImage{
    _indicatorImage = indicatorImage;
    self.imgIndicator.image = indicatorImage;
    self.v_dropdown.disclosureIndicatorImage = self.imgIndicator.image;
}
- (void)setSelectedLabelFont:(UIFont *)selectedLabelFont{
    _lblTitleMenu.font = selectedLabelFont;
    _selectedLabelFont = selectedLabelFont;
}
#pragma mark - Main
- (void)updateUI{
	self.titleView.backgroundColor = _selectedViewColor;
    self.v_dropdown.backgroundColor = _selectedViewColor;
    _sectorDropdown.dataSources = _menuTitles;
    [self.btn_Indicator setTintColor:_tintIcon];
    self.v_dropdown.componentTintColor = _tintIcon;
	self.backgroundColor = _bgColor;
    _lblTitleMenu.text = _titleMenu;
    _fullHeight = (self.height == 0 ? kHEIGHT_OF_ITEM:self.height) * (_numberItemVisible + 1);
	_lblTitleMenu.textAlignment = _titleAlign;
}
- (NSLayoutConstraint *)getHeightConstrainOfSelf{
	NSLayoutConstraint *heightConstraint;
	for (NSLayoutConstraint *constraint in self.constraints) {
		if (constraint.firstAttribute == NSLayoutAttributeHeight) {
			heightConstraint = constraint;
			break;
		}
	}
	return heightConstraint;
}
- (void)reloadDropDown{
	[self updateUI];
    [self.tblContent reloadData];
    [self.v_dropdown reloadAllComponents];
}
#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_menuTitles count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	TCListContentCell *cell = [tableView dequeueReusableCellWithIdentifier:[TCListContentCell reuseIdentifier]];
	if (!cell) {
		cell = [[TCListContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[TCListContentCell reuseIdentifier]];
	}
    if(_menuTitles.count == 0 || indexPath.row > _menuTitles.count - 1){
        return cell;
    }
	/*
	if (_curSelectedIndex == indexPath.row) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}else{
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	 */
	cell.tintColor = AppColor_TintColor;
	cell.lblContent.textAlignment = _titleAlign;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.lblContent.text = _menuTitles[indexPath.row];
    if(self.itemColor){
        cell.lblContent.textColor = self.itemColor;
    }
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	//Update UI
	self.titleMenu = _menuTitles[indexPath.row];
	[self onExpandAction:nil];
	//Passing data
	if (_delegate) {
		[_delegate didSelectRowAt:indexPath.row inView:self];
	}
}


#pragma mark
- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu maximumNumberOfRowsInComponent:(NSInteger)component{
    return _numberItemVisible;
}
- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu;
{
    return 1;
}
/// Return the number of rows in each component.
- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component{
    return self.menuTitles.count;
}

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    return 35;
}
- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    return YES;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.alignment                = _titleAlign;
    
    NSAttributedString *attributedString   =
    [NSAttributedString.alloc initWithString:self.titleMenu == nil ? @"" : self.titleMenu
                                  attributes:
     @{NSParagraphStyleAttributeName:paragraphStyle,NSForegroundColorAttributeName: self.selectedLabelColor == nil ? [UIColor whiteColor] : self.selectedLabelColor, NSFontAttributeName: _selectedLabelFont == nil ? self.lblTitleMenu.font : _selectedLabelFont}];
    
    return attributedString;
}
- (UIView *)dropdownMenu:(MKDropdownMenu *)dropdownMenu viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    TCListContentCell *cell = [_tblContent dequeueReusableCellWithIdentifier:[TCListContentCell reuseIdentifier]];
    if (!cell) {
        cell = [[TCListContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[TCListContentCell reuseIdentifier]];
    }
    if(_menuTitles.count == 0 || row > _menuTitles.count - 1){
        return cell;
    }
    cell.tintColor = AppColor_TintColor;
    cell.lblContent.textAlignment = _titleAlign;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblContent.text = _menuTitles[row];
    if(self.itemColor){
        cell.lblContent.textColor = self.itemColor;
    }
    return cell;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.titleMenu = self.menuTitles[row];
    if (_delegate) {
        [_delegate didSelectRowAt:row inView:self];
    }
    if(component != NSNotFound){
        [dropdownMenu reloadComponent:component];
    }else{
        [dropdownMenu reloadComponent:0];
    }
    delay(0.15, ^{
        [dropdownMenu closeAllComponentsAnimated:YES];
    });
}
@end
