//
//  TCMenuDropDown.h
//  TCiPad
//
//  Created by Kaka on 8/24/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKDropdownMenu.h"
@class TCListContentView;
@class TCMenuDropDown;
static inline void delay(NSTimeInterval delay, dispatch_block_t block) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
}
@protocol TCMenuDropDownDelegate <NSObject>
@optional
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView;
@end
@interface TCMenuDropDown : UIView{
	
}
@property (strong, nonatomic) UIView *sourceView;
//Passing data
@property (strong, nonatomic) NSString *titleMenu;
@property (strong, nonatomic) NSArray *menuTitles;
@property (nonatomic, assign) float height;
//Config
@property (strong, nonatomic) UIColor *bgColor;
@property (strong, nonatomic) UIColor *selectedViewColor;
@property (strong, nonatomic) UIColor *selectedLabelColor;
@property (strong, nonatomic) UIFont *selectedLabelFont;
@property (strong, nonatomic) UIColor *itemColor;
@property (strong, nonatomic) UIColor *tintIcon;
@property (assign, nonatomic) NSInteger numberItemVisible;
@property (assign, nonatomic) CGFloat radius;
@property (assign, nonatomic) CGFloat topSpace;
@property (assign, nonatomic) BOOL dismissAutoResize;
@property (assign, nonatomic) BOOL useMkDropDown;

@property (strong, nonatomic) UIImage *indicatorImage;
@property (assign, nonatomic) NSTextAlignment titleAlign;
@property (weak, nonatomic) id <TCMenuDropDownDelegate> delegate;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *v_dropdown;
@property (weak, nonatomic) IBOutlet UIButton *btn_Indicator;
@property (weak, nonatomic) IBOutlet UIButton *btn_Dropdown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Left;
@property (nonatomic, copy) void (^popCellBlock)(TCMenuDropDown * popupVC, NSInteger row, NSInteger section);
//Main
- (void)reloadDropDown;
- (void)openComponent:(NSInteger)component animated:(BOOL)animated;
- (void)closeAll;
- (void)addCustomView:(UIView *)customView;
@end
