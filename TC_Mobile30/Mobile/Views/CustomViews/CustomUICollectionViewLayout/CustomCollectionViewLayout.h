//
//  CustomCollectionViewLayout.h
//  Brightec
//
//  Created by JOSE MARTINEZ on 03/09/2014.
//  Copyright (c) 2014 Brightec. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    StockInformation = 0,
    StockMarketDepth ,
    StockTimeSales,
    StockBusiness,
    StockNews ,
    IndicesScore,
    IndicesTimeSales,
    IndicesMainMarket ,
    IndicesStructureWarrant,
    IndicesAceMarket,
    Other
} CollectionViewLayoutType;

// NOTE: This class is not used in this project - actually it is removed from the target. I added it just in case you need to compare the code between Objective-C and Swift

@interface CustomCollectionViewLayout : UICollectionViewLayout


@property (nonatomic, assign) CollectionViewLayoutType collectionViewLayoutType;
@property (nonatomic, assign) NSInteger numberOfCols;
@property (nonatomic, strong) NSArray *widthForCols;
@property (nonatomic, assign) CGFloat headerHeight;
@property (nonatomic, assign) CGFloat normalHeight;
@property (nonatomic, assign) BOOL dismissKeepingHeader;
@end
