//
//  TCBubbleMessageView.h
//  TC_Mobile30
//
//  Created by Kaka on 1/24/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum: NSInteger {
	BubbelMessageType_Message = 0,
	BubbelMessageType_Success,
	BubbelMessageType_Error
} BubbelMessageType;

typedef enum: NSInteger {
	BubbleAnimationType_Fade,
	BubbleAnimationType_Transition
} BubbleAnimationType;

@interface TCBubbleMessageView : UIView{
	
}
@property (strong, nonatomic) NSString *message;
@property (assign, nonatomic) BubbelMessageType bubbleType;
//+ (TCBubbleMessageView *)shared;

//Main
- (void)hide;
- (void)hide:(void(^)(void))completion;
- (void)showBubbleMessage:(NSString *)message inView:(UIView *)containerView autoHiden:(BOOL)autoHidden;
- (void)showBubbleMessage:(NSString *)message inView:(UIView *)containerView withType:(BubbelMessageType )bubbleType autoHidden:(BOOL)autoHidden;

//Class Methods
+ (void)hide;
+ (void)hide:(void(^)(void))completion;
+ (void)showBubbleMessage:(NSString *)message inView:(UIView *)containerView withType:(BubbelMessageType)bubbleTye autoHidden:(BOOL)autoHidden;

//Special case on window
+ (void)showWindowBubbleMessage:(NSString *)message;
+ (void)showSuccessWindowBubbleMessage:(NSString *)message;
+ (void)showErrorWindowBubbleMessage:(NSString *)message;
@end
