//
//  TCBubbleMessageView.m
//  TC_Mobile30
//
//  Created by Kaka on 1/24/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBubbleMessageView.h"
#import "TCBaseImageView.h"
#import "NSString+SizeOfString.h"
#import "AllCommon.h"
#import "UIView+ShadowBorder.h"

#define kMARGIN_BOTTOM	60
#define kMARGIN_WINDOW_BOTTOM	90
#define kMARGIN_LEFT_RIGHT		50
#define kMARGIN_CONTENT_LEFT_RIGHT	8
#define kDEFAULT_BUBBLE_HEIGHT	40

@interface TCBubbleMessageView(){
	
}
//OUTLET
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

//Configs internal
@property (assign, nonatomic) BOOL isShowing;
@property (assign, nonatomic) NSInteger marginBottom;
@property (assign, nonatomic) BubbleAnimationType animationType;
@property (strong, nonatomic) UIColor *bgColor;

@end
@implementation TCBubbleMessageView

+ (TCBubbleMessageView *)shared{
	static TCBubbleMessageView *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[TCBubbleMessageView alloc] init];
	});
	return _instance;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init{
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	if (self) {
		//Do something
		//Default values
		self.lblMessage.font = AppFont_MainFontRegularWithSize(14);
		self.marginBottom = kMARGIN_BOTTOM;
		self.isShowing = NO;
		self.message = @"Show something...";
		self.bubbleType = BubbelMessageType_Message;
		self.animationType = BubbleAnimationType_Fade;
		self.backgroundColor = [UIColor clearColor];
		self.bgImageView.layer.cornerRadius = (kDEFAULT_BUBBLE_HEIGHT / 2);
		self.bgImageView.backgroundColor = [UIColor blackColor];
		self.bgImageView.alpha = 0.7;
	}
	return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
	self.frame = frame;
	if (self) {
		//Do something
		self.frame = frame;
	}
	return self;
}

#pragma mark - Main
#pragma mark - Configs
- (void)setBubbleType:(BubbelMessageType)bubbleType{
	_bubbleType = bubbleType;
	switch (bubbleType) {
		case BubbelMessageType_Message:
			self.bgColor = [UIColor blackColor];
			break;
		case BubbelMessageType_Error:
			self.bgColor = [UIColor colorWithRed:(226.0f/255.0f) green:(85.0f/255.0f) blue:(69.0f/255.0f) alpha:1.0];
			break;
		case BubbelMessageType_Success:
			self.bgColor = [UIColor blueColor];
			break;
		default:
			break;
	}
	self.bgImageView.alpha = 0.7;
}
- (void)setMessage:(NSString *)message{
	_message = message;
	self.lblMessage.text = message;
}

- (void)setBgColor:(UIColor *)bgColor{
	_bgColor = bgColor;
	self.bgImageView.backgroundColor = bgColor;
}
#pragma mark - Action
- (void)hide{
	[self hide:nil];
}
- (void)hide:(void(^)(void))completion{
	if (!self.isShowing) {
		return;
	}
	if (self.animationType == BubbleAnimationType_Fade) {
		[UIView animateWithDuration:0.5 animations:^{
			self.bgImageView.alpha = 0.0;
		} completion:^(BOOL finished) {
			[self removeFromSuperview];
			self.isShowing = NO;
			if (completion) {
				completion();
			}
		}];
	}else{
		CGRect animatedFrame = self.frame;
		animatedFrame.origin.y += 200;
		[UIView animateWithDuration:0.5 animations:^{
			self.frame = animatedFrame;
		} completion:^(BOOL finished) {
			[self removeFromSuperview];
			self.isShowing = NO;
			if (completion) {
				completion();
			}
		}];
	}
}
- (void)showBubbleMessage:(NSString *)message inView:(UIView *)containerView autoHiden:(BOOL)autoHidden{
	if (self.isShowing) {
		return;
	}
	//Passing data
	if (message) {
		self.message = message;
	}
	//Prepare Info
	CGRect standardFrame = containerView.frame;
	CGFloat width = standardFrame.size.width - (2 * kMARGIN_LEFT_RIGHT);
	CGFloat textHeight = [self.message heightForWidth:(width - (2 * kMARGIN_CONTENT_LEFT_RIGHT)) usingFont:self.lblMessage.font];
	CGFloat height = textHeight + 16 + 4;//16 for Margin Top-Bottom, 4 for nice text
	if (height < kDEFAULT_BUBBLE_HEIGHT) {
		height = kDEFAULT_BUBBLE_HEIGHT;
	}
	standardFrame.size.width = width;
	standardFrame.size.height = height;
	standardFrame.origin.x = kMARGIN_LEFT_RIGHT;
	standardFrame.origin.y = containerView.frame.size.height - self.marginBottom - height;
	
	//Just 2 case of animation type: use if condition
	if (self.animationType == BubbleAnimationType_Fade) {
		self.frame = standardFrame;
		self.bgImageView.alpha = 0.0;
		[containerView addSubview:self];
		[containerView bringSubviewToFront:self];
		[UIView animateWithDuration:0.5 animations:^{
			self.bgImageView.alpha = 0.7;
		} completion:^(BOOL finished) {
			[self makeBorderShadow];
			//Do something
			self.isShowing = YES;
			if (autoHidden) {
				[self performSelector:@selector(hide) withObject:nil afterDelay:2.5];
			}
		}];
	}else{
		CGRect animatedFame = standardFrame;
		animatedFame.origin.y = standardFrame.origin.y + 200;
		self.frame = animatedFame;
		[containerView addSubview:self];
		[containerView bringSubviewToFront:self];
		[UIView animateWithDuration:0.5 animations:^{
			self.frame = standardFrame;
		} completion:^(BOOL finished) {
			[self makeBorderShadow];
			//Do something
			self.isShowing = YES;
			if (autoHidden) {
				[self performSelector:@selector(hide) withObject:nil afterDelay:2.5];
			}
		}];
	}
}

- (void)showBubbleMessage:(NSString *)message inView:(UIView *)containerView withType:(BubbelMessageType )bubbleType autoHidden:(BOOL)autoHidden{
	self.bubbleType = bubbleType;
	[self showBubbleMessage:message inView:containerView autoHiden:autoHidden];
}
#pragma mark - Class Methods
+ (void)hide{
	[[TCBubbleMessageView shared] hide];
}
+ (void)hide:(void(^)(void))completion{
	[[TCBubbleMessageView shared] hide:completion];
}
+ (void)showBubbleMessage:(NSString *)message inView:(UIView *)containerView withType:(BubbelMessageType)bubbleTye autoHidden:(BOOL)autoHidden{
	[[TCBubbleMessageView shared] showBubbleMessage:message inView:containerView withType:bubbleTye autoHidden:autoHidden];
}

+ (void)showWindowBubbleMessage:(NSString *)message{
	UIWindow *aWindow = [UIApplication sharedApplication].keyWindow;
	[TCBubbleMessageView shared].marginBottom = kMARGIN_WINDOW_BOTTOM;
	[[TCBubbleMessageView shared] showBubbleMessage:message inView:aWindow withType:BubbelMessageType_Message autoHidden:YES];
}

+ (void)showSuccessWindowBubbleMessage:(NSString *)message{
	UIWindow *aWindow = [UIApplication sharedApplication].keyWindow;
	[TCBubbleMessageView shared].marginBottom = kMARGIN_WINDOW_BOTTOM;
	[[TCBubbleMessageView shared] showBubbleMessage:message inView:aWindow withType:BubbelMessageType_Success autoHidden:YES];
}

+ (void)showErrorWindowBubbleMessage:(NSString *)message{
	UIWindow *aWindow = [UIApplication sharedApplication].keyWindow;
	[TCBubbleMessageView shared].marginBottom = kMARGIN_WINDOW_BOTTOM;
	[[TCBubbleMessageView shared] showBubbleMessage:message inView:aWindow withType:BubbelMessageType_Error autoHidden:YES];
}
@end
