//
//  MMarketPieChartView.h
//  TCiPad
//
//  Created by Kaka on 7/18/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TCPieChartView;
@class PADesignableView;
@class PADesignableButton;
@interface MMarketPieChartView : UIView{
	
}
@property (weak, nonatomic) IBOutlet TCPieChartView *pieView;

@property (weak, nonatomic) IBOutlet PADesignableView *noteUntradeView;
@property (weak, nonatomic) IBOutlet PADesignableButton *noteUntradeButton;

@property (weak, nonatomic) IBOutlet PADesignableView *noteUnchangeView;
@property (weak, nonatomic) IBOutlet PADesignableButton *noteUnchangeButton;

@property (weak, nonatomic) IBOutlet PADesignableView *noteUpView;
@property (weak, nonatomic) IBOutlet PADesignableButton *noteUpButton;

@property (weak, nonatomic) IBOutlet PADesignableView *noteDownView;
@property (weak, nonatomic) IBOutlet PADesignableButton *noteDownButton;

@property (weak, nonatomic) IBOutlet UILabel *lblNoteUntrade;
@property (weak, nonatomic) IBOutlet UILabel *lblUntrade;

@property (weak, nonatomic) IBOutlet UILabel *lblNoteUnchange;
@property (weak, nonatomic) IBOutlet UILabel *lblUnChange;

@property (weak, nonatomic) IBOutlet UILabel *lblNoteUp;
@property (weak, nonatomic) IBOutlet UILabel *lblUp;

@property (weak, nonatomic) IBOutlet UILabel *lblNoteDown;
@property (weak, nonatomic) IBOutlet UILabel *lblDown;


@property (weak, nonatomic) IBOutlet UIView *noteView;
@property (weak, nonatomic) IBOutlet UIView *contentChartView;

#pragma mark - Main
//For Scoreboard data value
- (void)populateChartFromDict:(NSDictionary *)dataDict;
//For: Availabel value chart
- (void)populateChartFrom:(float)upValue down:(float)downValue untrade:(float)untradeValue unchanged:(float)unchangedValue;
@end
