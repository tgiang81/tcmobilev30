//
//  TCChartView.h
//  TCiPad
//
//  Created by Kaka on 04/04/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCChartView.h"
#import "AllCommon.h"
#import "UIView+HUD.h"
#import "Utils.h"
#import "ImgChartManager.h"
#import "UISynchedWebView.h"
#import "ZoomRotatePanImageView.h"
#import "UIImageView+WebCache.h"
#import "CPService.h"
#import "LanguageKey.h"
#import "TCPageMenuControl.h"
//Load Type Chart
static NSString *kTradingView_Chart     = @"TradingView";
static NSString *kImage_Chart 		= @"Image";
static NSString *kModulus_Chart 	= @"Modulus";
static NSString *kTeleTrader_Chart 	= @"TeleTrader";


//=================== LEFT COMMON MENU TYPE =============
typedef enum : NSInteger{
	StatusChart_Success = 1,
	StatusChart_NoURLError,
	StatusChart_DefaultError,
	StatusChart_Loading,
	StatusChart_Init,
	StatusChart_Fail
}StatusChart;

#define kRatio_Zoom_Chart	6

@interface TCChartView ()<UIWebViewDelegate, UIGestureRecognizerDelegate>{
	
	UITapGestureRecognizer *_tap;
	UserPrefConstants *_userPrefs;
	DGActivityIndicatorView *_activityHUD;
	
	NSArray *_listChartType;
}
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;

//Properties
@property (weak, nonatomic) IBOutlet TCPageMenuControl *menuControls;

@end


@implementation TCChartView
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
}

- (void)layoutSubviews{
	[super layoutSubviews];
	_containerView.frame = self.bounds;
	[self layoutIfNeeded];
}


#pragma mark - Common Init
- (void)commonInit
{
	//++++ Using Auto Layout ++++
	/*
	NSString *className = NSStringFromClass([self class]);
	_customConstraints = [[NSMutableArray alloc] init];
	UIView *view = nil;
	NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	for (id object in objects) {
		if ([object isKindOfClass:[UIView class]]) {
			view = object;
			break;
		}
	}

	if (view != nil) {
		_containerView = view;
		view.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:view];
		[self setNeedsUpdateConstraints];
	}
	[self updateConstraints];
	*/
	//=========== Using Autoresize class ====================
	//* This sample to use custom view
	NSString *className = NSStringFromClass([self class]);
	UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
	rootView.frame = self.bounds;
	rootView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[self addSubview:rootView];
	[self setNeedsUpdateConstraints];

	
	//Default init:
	[self initDefaultValue];
	//Start load default chard
	//[self loadChartForDays:[Utils rawValueFrom:_chartType] stockCode:_stCode];
}

- (void)updateConstraints
{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);

		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}

#pragma mark - Configure
- (void)initDefaultValue{
	_activityHUD = [[Utils shareInstance] createPrivateActivity];
	_userPrefs = [UserPrefConstants singleton];
	_webViewChart.layer.backgroundColor = [UIColor clearColor].CGColor;
	_imageChart.layer.backgroundColor = [UIColor clearColor].CGColor;
	_webViewChart.backgroundColor = [UIColor clearColor];
	self.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	_webViewChart.hidden = YES;
	_imageChart.hidden = YES;
	_chartType = ChartType_3Month;
	_webViewChart.delegate = self;
	_isOnlyLoadChartByImage = NO;
	self.allowDoubleTapToViewFull = YES;
	[self setChartStatus:StatusChart_Init];
	[self setLockMode:NO];
	
	//MenuControls
	[self setupMenuControls];
}

//Prevent zoom, interactive to chart or not
- (void)setLockMode:(BOOL)lockMode{
	_webViewChart.scrollView.scrollEnabled = !lockMode;
	_webViewChart.scrollView.bounces = !lockMode;
	_imageChart.userInteractionEnabled = !lockMode;
	_lockMode = lockMode;
}

- (void)setupMenuControls{
	_listChartType = @[@(ChartType_Day), @(ChartType_Week), @(ChartType_1Month), @(ChartType_3Month), @(ChartType_6Month), @(ChartType_1Year), @(ChartType_2Year)];
	_menuControls.menuType = TCPageMenuControlType_Bubble;
	_menuControls.dataSources = [self nameOfListTypeChart:_listChartType];
	_menuControls.currentSelectedItem = [_listChartType indexOfObject:@(self.chartType)];
	[_menuControls startLoadingPageMenu];
	__weak TCChartView *weakSelf = self;
	[_menuControls didSelectItem:^(NSString *item, NSInteger index) {
		//Do something
		weakSelf.chartType = [self->_listChartType[index] integerValue];
		if (weakSelf.delegate) {
			[weakSelf.delegate didLoadChartAtType:self->_chartType];
		}
		//==== Start Loading Chart =====
		[weakSelf loadChartForDays:[Utils rawValueFrom:weakSelf.chartType] stockCode:self->_stCode];
	}];
}
#pragma mark - Action

- (IBAction)onChangeTypeChartAction:(UIButton *)sender {
	DLog(@"Change type chart at index: %@", @(sender.tag));
	_chartType = (ChartType)sender.tag;
	if (_delegate) {
		[_delegate didLoadChartAtType:_chartType];
	}
	//==== Start Loading Chart =====
	[self loadChartForDays:[Utils rawValueFrom:_chartType] stockCode:_stCode];
}


#pragma mark - LoadChart
- (void)startLoadDefaultChart{
	[self loadChartForDays:[Utils rawValueFrom:_chartType] stockCode:_stCode];
}
- (void)loadChartForDays:(NSInteger)days stockCode:(NSString *)stockCode{
	[self selectedButtonControlAt:_chartType];
	_imageChart.hidden = YES;
	_webViewChart.hidden = YES;
	NSString *chartTypeToLoad = [UserPrefConstants singleton].chartType;

	/* +++ No need use this now
	if (days==K_DAY) {
		chartTypeToLoad = kImage_Chart;
	}
	*/
	NSString *resolvedChartURL = [UserPrefConstants singleton].interactiveChartURL;
	
	if (_isOnlyLoadChartByImage) {
		NSString *imageChartURL = @"";
		NSString *chartURL = [UserPrefConstants singleton].historicalChartURL;
		if (days == 1) {
			chartURL = [UserPrefConstants singleton].intradayChartURL;
		} else {
			chartURL = [UserPrefConstants singleton].historicalChartURL;
		}
		
		//+++Make size big for image chart
		float width = _imageChart.frame.size.width * kRatio_Zoom_Chart;
		float height = _imageChart.frame.size.height * kRatio_Zoom_Chart;
		NSString *urlString = [NSString stringWithFormat:@"%@&w=%.0f&h=%.0f&k=%@&c=b&d=%ld",chartURL,
							   width, height, stockCode, (long)days];
		imageChartURL = urlString;
		DLog(@"Chart type: %@ - URL: %@", @(_chartType), imageChartURL);
		//+++++ Load chart by image: old way
		/*
		 [self showActivity];
		ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
		[imgChartManager getImageChartWithURL:imageChartURL
							completedWithData:^(NSData *data) {
								dispatch_async(dispatch_get_main_queue(), ^{
									[self hideActivity];
									UIImage *chartImg = [UIImage imageWithData:data];
									_imageChart.image = chartImg;
									_imageChart.hidden = NO;
								});
							}
									  failure:^(NSString *errorMsg) {
										  dispatch_async(dispatch_get_main_queue(), ^{
											  [self showStatusWithMessage:[LanguageManager stringForKey:@"Load Chart Fail. Try again later."]];
										  });
									  }];
		 */
		//Load chart by image: #way 2
		[self showActivity];
		[ImgChartManager loadImageFromURL:imageChartURL completion:^(UIImage *image, NSError *error) {
			[self hideActivity];
			if (error) {
				 [self showStatusWithMessage:[LanguageManager stringForKey:error.localizedDescription]];
			}else{
				self->_imageChart.image = image;
				self->_imageChart.hidden = NO;
			}
		}];
		
	}else{
        if ([chartTypeToLoad isEqualToString:kTradingView_Chart]) {
			NSString *tvChart = [UserPrefConstants singleton].tvChart? [UserPrefConstants singleton].tvChart : resolvedChartURL;
            NSArray *stkCodeAndExchangeArr= [stockCode componentsSeparatedByString:@"."];
            NSString *url = [NSString stringWithFormat:@"%@code=%@&exchg=%@&color=w&view=m&mode=h&amount=%ld",
                             tvChart,
                             [stkCodeAndExchangeArr objectAtIndex:0], [stkCodeAndExchangeArr objectAtIndex:1], (long)days];
            
            //NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
            
            [_webViewChart loadRequest:request];
        }
        
		//============= Modulus Chart =========================
		else if ([chartTypeToLoad isEqualToString:kModulus_Chart]) {
			NSArray *stkCodeAndExchangeArr= [stockCode componentsSeparatedByString:@"."];
			NSString *url = [NSString stringWithFormat:@"%@exchg=%@&code=%@&color=w&view=f&amount=%ld",
							 resolvedChartURL,
							 [stkCodeAndExchangeArr objectAtIndex:1], [stkCodeAndExchangeArr objectAtIndex:0], (long)days];
			
			//NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
			NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
			
			[_webViewChart loadRequest:request];
		}
		
		//============== IMAGE CHART ===================
		else if ([chartTypeToLoad isEqualToString:kImage_Chart]) {
			NSString *chartURL = [UserPrefConstants singleton].historicalChartURL;
			if (days == 1) {
				chartURL = [UserPrefConstants singleton].intradayChartURL;
			} else {
				chartURL = [UserPrefConstants singleton].historicalChartURL;
			}
			//+++Make size big for image chart
			float width = _imageChart.frame.size.width * kRatio_Zoom_Chart;
			float height = _imageChart.frame.size.height * kRatio_Zoom_Chart;
			NSString *urlString = [NSString stringWithFormat:@"%@&w=%.0f&h=%.0f&k=%@&c=b&d=%ld",chartURL,
								   width, height, stockCode, (long)days];
			NSLog(@"%@",urlString);
			ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
			[self showActivity];
			[imgChartManager getImageChartWithURL:urlString
								completedWithData:^(NSData *data) {
									dispatch_async(dispatch_get_main_queue(), ^{
										[self hideActivity];
										UIImage *chartImg = [UIImage imageWithData:data];
										_imageChart.image = chartImg;
										_imageChart.hidden = NO;
									});
								}
										  failure:^(NSString *errorMsg) {
											  dispatch_async(dispatch_get_main_queue(), ^{
												  [self showStatusWithMessage:errorMsg]; //[LanguageManager stringForKey:@"Load Chart Fail. Try again later."]
											  });
										  }];
		}
		
		// ========================================
		//                TELETRADER
		// ========================================
		
		else if ([chartTypeToLoad isEqualToString:kTeleTrader_Chart]) {
			NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
			NSString *url = [NSString stringWithFormat:@"%@code=%@&Name=%@&exchg=%@&mode=d&color=b&lang=en&key=%@&amount=%ld",
							 resolvedChartURL,
							 [splitStkCode objectAtIndex:0],
							 [splitStkCode objectAtIndex:1],
							 [UserPrefConstants singleton].userCurrentExchange,
							 [[UserPrefConstants singleton] encryptTime],
							 (long)days];
			
			NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
			
			//Load Request
			[_webViewChart loadRequest:request];
		}
	}
}


- (void)loadMarketChart{
	//invisible action controls
	[self hiddenActionControls];
	NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex]
							  valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
	NSString *chartTypeToLoad = [UserPrefConstants singleton].chartTypeToLoad;
	//For Modulus
    
    
    if ([chartTypeToLoad isEqualToString:kTradingView_Chart]) {
        NSString *url = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m&isstock=N",
                         [UserPrefConstants singleton].interactiveChartURL,
                         [splitStkCode objectAtIndex:0],
                         [[UserPrefConstants singleton] stripDelayedSymbol:_userPrefs.userCurrentExchange]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
        //Load Request
        [_webViewChart loadRequest:request];
    }
    
	else if ([chartTypeToLoad isEqualToString:kModulus_Chart]) {
		NSString *url = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m&isstock=N",
						 [UserPrefConstants singleton].interactiveChartURL,
						 [splitStkCode objectAtIndex:0],
						 [[UserPrefConstants singleton] stripDelayedSymbol:_userPrefs.userCurrentExchange]];
		NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
		//Load Request
		[_webViewChart loadRequest:request];
	}
    
	//For Image Chart
	else if ([chartTypeToLoad isEqualToString:kImage_Chart]) {
		NSString *urlString = [NSString stringWithFormat:@"%@t=1&w=%.0f&h=%.0f&k=%@.%@&c=b",
							   [UserPrefConstants singleton].intradayChartURL,
							   _imageChart.frame.size.width,
							   _imageChart.frame.size.height,
							   [splitStkCode objectAtIndex:0],
							   [_userPrefs stripDelayedSymbol:_userPrefs.userCurrentExchange]];
		
		ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
		[self showActivity];
		[imgChartManager getImageChartWithURL:urlString
							completedWithData:^(NSData *data) {
								dispatch_async(dispatch_get_main_queue(), ^{
									[self hideActivity];
									UIImage *chartImg = [UIImage imageWithData:data];
									_imageChart.image = chartImg;
									_imageChart.hidden = NO;
								});
							}
									  failure:^(NSString *errorMsg) {
										  dispatch_async(dispatch_get_main_queue(), ^{
											  [self showStatusWithMessage:[LanguageManager stringForKey:@"Load Chart Fail. Try again later."]];
										  });
									  }];
		
	}
	//For TeleTrader
	else if ([chartTypeToLoad isEqualToString:kTeleTrader_Chart]) {
		NSString *urlString = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m&lang=en&key=%@",
							   [UserPrefConstants singleton].interactiveChartURL,
							   [splitStkCode objectAtIndex:0],
							   [_userPrefs stripDelayedSymbol:_userPrefs.userCurrentExchange],
							   [[UserPrefConstants singleton] encryptTime]
							   ];
		NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0];
		//Load Request
		[_webViewChart loadRequest:request];
	}
}

#pragma mark - Utils VIEW
//======= ACTIVITY =========
- (void)showActivity{
	_viewStatus.hidden = NO;
	_lblStatusMsg.hidden = YES;
	[[Utils shareInstance] showActivity:_activityHUD inView:_chartContentView];
}
- (void)hideActivity{
	[[Utils shareInstance] dismissActivity:_activityHUD];
	_viewStatus.hidden = YES;
}
//=========== MESSAGE =============
- (void)showStatusWithMessage:(NSString *)msg{
	_viewStatus.hidden = NO;
	[[Utils shareInstance] dismissActivity:_activityHUD];
	_lblStatusMsg.hidden = NO;
	_lblStatusMsg.text = msg;
}
- (void)hideStatus{
	_viewStatus.hidden = YES;
}
-(void)setChartStatus:(StatusChart)status {
	NSString *errormsg = [LanguageManager stringForKey:Chart_Not_Available];
	switch (status) {
		case StatusChart_Success:{
			
		}
			break;
			
		case StatusChart_Fail:{
			errormsg = [LanguageManager stringForKey:Load_Chart_Fail___Try_again_later__];
		}
			break;
		case StatusChart_Init:{
			_viewStatus.hidden = YES;
			_lblStatusMsg.text = @"";
		}
			break;
		case StatusChart_NoURLError:{
			
		}
			break;
		case StatusChart_Loading:{
			
		}
			break;
		case StatusChart_DefaultError:{
			
		}
			break;
			
		default:
			break;
	}
	_lblStatusMsg.text = errormsg;
}

- (void)hiddenActionControls{
	_stackViewWidthConstrain.constant = 0.0;
	[self layoutIfNeeded];
}

#pragma mark - ChartType UTIL
- (NSString *)nameOfTypeChart:(ChartType)type{
	switch (type) {
		case ChartType_Day:
			return [LanguageManager stringForKey:@"1D"];
			break;
		case ChartType_Week:
			return [LanguageManager stringForKey:@"1W"];
			break;
		case ChartType_1Month:
			return [LanguageManager stringForKey:@"1M"];
			break;
		case ChartType_3Month:
			return [LanguageManager stringForKey:@"3M"];
			break;
		case ChartType_6Month:
			return [LanguageManager stringForKey:@"6M"];
			break;
		case ChartType_1Year:
			return [LanguageManager stringForKey:@"1Y"];
			break;
		case ChartType_2Year:
			return [LanguageManager stringForKey:@"2Y"];
			break;
		default:
			return @"Unknown";
			break;
	}
}

- (NSArray *)nameOfListTypeChart:(NSArray *)listTypeChart{
	NSMutableArray *names = @[].mutableCopy;
	[listTypeChart enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		ChartType type = [obj integerValue];
		[names addObject:[self nameOfTypeChart:type]];
	}];
	return names.copy;
}

#pragma mark - Overide Control
//========= Selected Chart at Position Button ======
- (void)selectedButtonControlAt:(ChartType)type{
	for (id aView in _stackControl.subviews) {
		if ([aView isKindOfClass:[UIButton class]]) {
			UIButton *btnControl = (UIButton *)aView;
			if (btnControl.tag == type) {
				[btnControl setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor) forState:UIControlStateNormal];
			}else{
				[btnControl setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
			}
		}
	}
}

- (void)setAllowDoubleTapToViewFull:(BOOL)allowDoubleTapToViewFull{
	if (allowDoubleTapToViewFull) {
		if (!_tap) {
			_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlerDoubleTap:)];
			_tap.delegate = self;
			_tap.numberOfTapsRequired = 2;
		}
		[self addGestureRecognizer:_tap];
	}else{
		if (_tap) {
			[self removeGestureRecognizer:_tap];
			_tap = nil;
		}
	}
	_allowDoubleTapToViewFull = allowDoubleTapToViewFull;
}

- (void)setChartType:(ChartType)chartType{
	[self selectedButtonControlAt:chartType];
	_chartType = chartType;
}

- (void)setBgColor:(UIColor *)bgColor{
	self.backgroundColor = bgColor;
	_bgColor = bgColor;
}

////========== Defezee WebView =======
//-(void) stopRunLoop{
//	CFRunLoopRef runLoop = [[NSRunLoop currentRunLoop] getCFRunLoop];
//	CFRunLoopStop(runLoop);
//
//}
- (void)setIsOnlyLoadChartByImage:(BOOL)isOnlyLoadChartByImage{
	if (isOnlyLoadChartByImage) {
		[self hiddenActionControls];
	}
    _isOnlyLoadChartByImage = isOnlyLoadChartByImage;
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView{
	[self showActivity];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
	_webViewChart.hidden = NO;
	[self hideActivity];

}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
	DLog(@"+++ Load Chart Error: %@", error.localizedDescription);
	[self showStatusWithMessage:error.localizedDescription];
}

#pragma mark - Using TapGester
- (void)handlerDoubleTap:(UITapGestureRecognizer *)tap{
	/*
	if (_delegate) {
		[_delegate doubleTapChartView:self];
	}
	 */
}

#pragma mark - UITapGestureRecognizedDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
	return YES;
}
@end
