//
//  MMarketPieChartView.m
//  TCiPad
//
//  Created by Kaka on 7/18/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MMarketPieChartView.h"
#import "TCPieChartView.h"
#import "PieLayer.h"
#import "PieElement.h"

#import "PADesignableView.h"
#import "PADesignableButton.h"

#import "AllCommon.h"
#import "NSNumber+Formatter.h"
#import "LanguageKey.h"


static NSString  *kTotalUp_Key 			= @"TotalUp";
static NSString  *kTotalDown_Key 		= @"TotalDown";
static NSString  *kTotalUnChanged_Key	= @"TotalUnChanged";
static NSString  *kTotalUnTrade_Key 	= @"TotalUnTrade";
static NSString  *kTotalVolume_Key 		= @"TotalVolume";
static NSString  *kTotalValue_Key 		= @"TotalValue";
static NSString  *kTotalAll_Key 		= @"TotalAll";

typedef enum: NSInteger {
	ChartValueType_Up = 0,
	ChartValueType_Down,
	ChartValueType_UnChanged,
	ChartValueType_UnTraded
} ChartValueType;


@interface MMarketPieChartView (){
	PieElement *_untradeElement;
	PieElement *_unchangedElement;
	PieElement *_upElement;
	PieElement *_downElement;
}
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;
@end

@implementation MMarketPieChartView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}
- (void)awakeFromNib{
	[super awakeFromNib];
	[self setup];
}
#pragma mark - Common Init
- (void)commonInit
{
	//++++ Using Auto Layout ++++
	 NSString *className = NSStringFromClass([self class]);
	 _customConstraints = [[NSMutableArray alloc] init];
	 UIView *view = nil;
	 NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	 for (id object in objects) {
		 if ([object isKindOfClass:[UIView class]]) {
			 view = object;
			 break;
		 }
	 }
	 
	 if (view != nil) {
	 _containerView = view;
	 view.translatesAutoresizingMaskIntoConstraints = NO;
	 [self addSubview:view];
	 [self setNeedsUpdateConstraints];
	 }
	 [self updateConstraints];
	
	//Default init:
	[self setup];
	[self initChart];
}

- (void)updateConstraints
{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);
		
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}

#pragma mark - UTILS
#pragma mark - UTIL
- (NSDictionary *)parseCustomFieldFromMarket:(NSDictionary *)marketDict
{
	NSMutableDictionary *totalMarketDict = @{}.mutableCopy;
	NSArray *headerFIDs = @[@"37",@"110",@"105",@"106",@"107",@"108",@"109",@"111"];
	int totalUp = 0;
	int totalDown = 0;
	int totalUnchg = 0;
	int totalUntrd = 0;
	int totalTotal = 0;
	int totalVol = 0;
	int totalValue = 0;
	
	for (NSDictionary *dict in [marketDict allValues])//Mining, REITS, IPC, Finance...
	{
		// NSLog(@"marketDict %@", dict);
		for(NSString *fid in headerFIDs)//UP, Down, Unchanged..
		{
			if([fid isEqualToString:@"105"])
			{
				totalUp += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"106"])
			{
				totalDown += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"107"])
			{
				totalUnchg += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"108"])
			{
				totalUntrd += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"109"])
			{
				totalTotal += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"110"])
			{
				totalVol += [[dict objectForKey:fid]integerValue];
			}
			else if([fid isEqualToString:@"111"])
			{
				totalValue += [[dict objectForKey:fid]integerValue];
			}
		}
	}
	
	[totalMarketDict setObject:[NSNumber numberWithInt:totalUp]     forKey:kTotalUp_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalDown]   forKey:kTotalDown_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalUnchg]  forKey:kTotalUnChanged_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalUntrd]  forKey:kTotalUnTrade_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalTotal]  forKey:kTotalAll_Key];
	[totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:kTotalVolume_Key];
	[totalMarketDict setObject:[NSNumber numberWithInt:totalValue]  forKey:kTotalValue_Key];
	return [totalMarketDict copy];
}
- (float)valueChartFrom:(NSDictionary *)chartDataDict atValueType:(ChartValueType)type{
	float value = 0.0;
	switch (type) {
		case ChartValueType_Up:
			value = [chartDataDict[kTotalUp_Key] floatValue];
			break;
		case ChartValueType_Down:
			value = [chartDataDict[kTotalDown_Key] floatValue];
			break;
		case ChartValueType_UnChanged:
			value = [chartDataDict[kTotalUnChanged_Key] floatValue];
			break;
		case ChartValueType_UnTraded:
			value = [chartDataDict[kTotalUnTrade_Key] floatValue];
			break;
		default:
			break;
	}
	return value;
}

#pragma mark - Main
//Pre setup view
- (void)setup{
	[self setupLanguage];
	[self setupFontAndColor];
	_noteUntradeView.isRounded = YES;
	_noteUnchangeView.isRounded = YES;
	_noteUpView.isRounded = YES;
	_noteUpView.isRounded = YES;
	
	_noteUntradeButton.isRounded = YES;
	_noteUnchangeButton.isRounded = YES;
	_noteUpButton.isRounded = YES;
	_noteDownButton.isRounded = YES;
}

- (void)setupFontAndColor{
	//View
	_noteUntradeView.backgroundColor = RGBA(170, 170, 170, 0.2);
	_noteUnchangeView.backgroundColor = RGBA(170, 170, 170, 0.2);
	_noteUpView.backgroundColor = RGBA(170, 170, 170, 0.2);
	_noteDownView.backgroundColor = RGBA(170, 170, 170, 0.2);
	//Button
	_noteUntradeButton.backgroundColor = AppColor_Untrade_PieChart_Color;
	_noteUnchangeButton.backgroundColor = AppColor_Unchange_PieChart_Color;
	_noteUpButton.backgroundColor = AppColor_Up_PieChart_Color;
	_noteDownButton.backgroundColor = AppColor_Down_PieChart_Color;
	
	//Text instruction
	_lblNoteUntrade.textColor = AppColor_HighlightTextColor;
	_lblNoteUnchange.textColor = AppColor_HighlightTextColor;
	_lblNoteUp.textColor = AppColor_HighlightTextColor;
	_lblNoteDown.textColor = AppColor_HighlightTextColor;
	
	//Container view
	_containerView.backgroundColor = [UIColor clearColor];
	_pieView.backgroundColor = [UIColor clearColor];
	_noteView.backgroundColor = [UIColor clearColor];
	self.backgroundColor = [UIColor clearColor];;
}

- (void)setupLanguage{
	_lblNoteUntrade.text = [LanguageManager stringForKey:Untraded];
	_lblNoteUnchange.text = [LanguageManager stringForKey:Unchanged];
	_lblNoteUp.text = [LanguageManager stringForKey:Up];
	_lblNoteDown.text = [LanguageManager stringForKey:Down];
}

#pragma mark - Polulate Chart
- (void)initChart{
	_untradeElement = [PieElement pieElementWithValue:0 color:AppColor_Untrade_PieChart_Color];
	[_pieView.layer addValues:@[_untradeElement] animated:NO];
	
	_unchangedElement = [PieElement pieElementWithValue:0 color:AppColor_Unchange_PieChart_Color];
	[_pieView.layer addValues:@[_unchangedElement] animated:NO];
	
	_upElement = [PieElement pieElementWithValue:0 color:AppColor_Up_PieChart_Color];
	[_pieView.layer addValues:@[_upElement] animated:NO];
	
	_downElement = [PieElement pieElementWithValue:0 color:AppColor_Down_PieChart_Color];
	[_pieView.layer addValues:@[_downElement] animated:NO];
	
	//Show title chart
	_pieView.layer.transformTitleBlock = ^(PieElement* elem, float percent){
		int intPercent = (int)percent;
		return [NSString stringWithFormat:@"%d %%", intPercent];//[(PieElement*)elem title];
	};
	_pieView.layer.showTitles = ShowTitlesAlways;
	_pieView.clipsToBounds = YES;
	
	_lblUp.text = @"#";
	_lblDown.text = @"#";
	_lblUntrade.text = @"#";
	_lblUnChange.text = @"#";
}
//For Summary ScoreBoard data
- (void)populateChartFromDict:(NSDictionary *)dataDict{
	//Parse data
	NSDictionary *chartDict = [self parseCustomFieldFromMarket:dataDict];
	float untradeValue = [self valueChartFrom:chartDict atValueType:ChartValueType_UnTraded];
	float unchangedValue =  [self valueChartFrom:chartDict atValueType:ChartValueType_UnChanged];
	float upValue =  [self valueChartFrom:chartDict atValueType:ChartValueType_Up];
	float downValue =  [self valueChartFrom:chartDict atValueType:ChartValueType_Down];
	
	[self populateChartFrom:upValue down:downValue untrade:untradeValue unchanged:unchangedValue];
}

//For: Availabel value chart
- (void)populateChartFrom:(float)upValue down:(float)downValue untrade:(float)untradeValue unchanged:(float)unchangedValue{
	[PieElement animateChanges:^{
		_lblUp.text = [[NSNumber numberWithFloat:upValue] groupNumber];
		_upElement.val = upValue;
		
		_lblDown.text = [[NSNumber numberWithFloat:downValue] groupNumber];
		_downElement.val = upValue;
		
		_lblUp.text = [[NSNumber numberWithFloat:upValue] groupNumber];
		_upElement.val = downValue;
		
		_lblUntrade.text = [[NSNumber numberWithFloat:untradeValue] groupNumber];
		_untradeElement.val = untradeValue;
		
		_lblUnChange.text = [[NSNumber numberWithFloat:unchangedValue] groupNumber];
		_unchangedElement.val = unchangedValue;
	}];
}
@end
