//
//  PAMainControlView.h
//  PlastApp
//
//  Created by Kaka on 8/17/17.
//  Copyright © 2017 ITSOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"
@class TCChartView;
@class UISynchedWebView;
@class ZoomRotatePanImageView;
@class TCChartView;

#pragma mark - Protocol
@protocol TCChartViewDelegate <NSObject>
@optional
- (void)doubleTapChartView:(TCChartView *)chartView;
@optional
- (void)didLoadChartAtType:(ChartType)type;
@end


@interface TCChartView : UIView{
	
}
@property (weak, nonatomic) IBOutlet UIStackView *stackControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackViewWidthConstrain;


@property (weak, nonatomic) IBOutlet UIView *chartContentView;
@property (weak, nonatomic) IBOutlet ZoomRotatePanImageView *imageChart;
@property (weak, nonatomic) IBOutlet UISynchedWebView *webViewChart;

//For Status View
@property (weak, nonatomic) IBOutlet UIView *viewStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusMsg;

//Configure visible chart view
@property (assign, nonatomic) BOOL lockMode;
@property (assign, nonatomic) BOOL allowDoubleTapToViewFull;
@property (assign, nonatomic) BOOL isOnlyLoadChartByImage;
@property (assign, nonatomic) ChartType chartType;
@property (strong, nonatomic) UIColor *bgColor;

//Passing data
@property (strong, nonatomic) NSString *stCode;
@property (weak, nonatomic) id <TCChartViewDelegate> delegate;

#pragma mark - Main Function
//Load chart for stock
- (void)startLoadDefaultChart;
//Load chart for Market
- (void)loadMarketChart;

//Utils
- (void)hiddenActionControls;

@end
