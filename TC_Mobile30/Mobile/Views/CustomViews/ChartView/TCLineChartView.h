//
//  TCLineChartView.h
//  TC_Mobile30
//
//  Created by Kaka on 2/13/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum: NSInteger {
	TCLineChartType_Day = 7,
	TCLineChartType_1Month = 30,
	TCLineChartType_3Months = 90,
	TCLineChartType_6Months = 180,
	TCLineChartType_12Months = 360
} TCLineChartType;


//Block
typedef void (^TCLineChartViewBlock)(void);
@protocol TCLineChartViewDelegate<NSObject>
@optional
- (void)didFinishLoadChart;
@end
@interface TCLineChartView : UIView{
	
}

//datasource
@property (assign, nonatomic) TCLineChartType chartType;
@property (strong, nonatomic) NSMutableArray *datasources;
@property (strong, nonatomic) NSMutableArray *xValues;
@property (strong, nonatomic) TCLineChartViewBlock didLoadChartBlock;
@property (weak, nonatomic) id <TCLineChartViewDelegate> delegate;
//Configs
@property (strong, nonatomic) UIColor *lineColor;
//Main
- (void)reloadChart;
//UTIL MAIN
- (UIImage *)snapshotImage;
- (UIImage *)snapshotImageInBackground:(BOOL)isInBackground;
- (void)didFinishLoadChart:(TCLineChartViewBlock)completion;

#pragma mark - MAIN - LOAD CHART
//Can use to loading chart
- (void)loadChart:(TCLineChartType)type forCode:(NSString *)code inExchange:(NSString *)exchange includeLastValue:(NSNumber *)lastValue chartColor:(UIColor *)color;


//Make Full info
- (void)showFullInfoChart;
- (void)showShortInfoChart;

@end
