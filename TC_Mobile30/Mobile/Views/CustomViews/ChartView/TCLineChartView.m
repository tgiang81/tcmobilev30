//
//  TCLineChartView.m
//  TC_Mobile30
//
//  Created by Kaka on 2/13/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCLineChartView.h"
#import "BEMSimpleLineGraphView.h"
#import "AllCommon.h"
#import "VertxConnectionManager.h"
#import "DataManager.h"
#import "GCDQueue.h"

@interface TCLineChartView()<BEMSimpleLineGraphDelegate, BEMSimpleLineGraphDataSource>{
	
}
//Initial

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;

@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
//OUTLETS
@property (weak, nonatomic) IBOutlet BEMSimpleLineGraphView *myGraph;

//Properties
@property (strong, nonatomic) NSString *exchange;
@property (strong, nonatomic) NSString *code;
@property (assign, nonatomic) BOOL didLoadChart;
@property (strong, nonatomic) NSNumber *lastValue;
@end
@implementation TCLineChartView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithControlTitles:(NSArray *)titles{
	self = [super init];
	if (self) {
		[self commonInit];
	}
	return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}


- (void)drawRect:(CGRect)rect{
	//	NSArray *colors = @[(id)[UIColor clearColor].CGColor, (id)RGB(92, 52, 76).CGColor, (id)RGB(183, 61, 31).CGColor];
	//	[self drawGradientFromCGColors:colors];
	
}

//Layout view
- (void)awakeFromNib{
	[super awakeFromNib];
}
- (void)layoutSubviews{
	[super layoutSubviews];
	
}
#pragma mark - Common Init
- (void)commonInit{
	//++++ Using Auto Layout ++++
	NSString *className = NSStringFromClass([self class]);
	_customConstraints = [[NSMutableArray alloc] init];
	UIView *view = nil;
	NSArray *objects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
	for (id object in objects) {
		if ([object isKindOfClass:[UIView class]]) {
			view = object;
			break;
		}
	}
	
	if (view != nil) {
		_containerView = view;
		view.translatesAutoresizingMaskIntoConstraints = NO;
		[self addSubview:view];
		[self setNeedsUpdateConstraints];
	}
	[self updateConstraints];
	//Initial First
	[self initialFirst];
}

- (void)updateConstraints{
	[self removeConstraints:self.customConstraints];
	[self.customConstraints removeAllObjects];
	
	if (self.containerView != nil) {
		UIView *view = self.containerView;
		NSDictionary *views = NSDictionaryOfVariableBindings(view);
		
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"H:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self.customConstraints addObjectsFromArray:
		 [NSLayoutConstraint constraintsWithVisualFormat:
		  @"V:|-0-[view]-0-|" options:0 metrics:nil views:views]];
		[self addConstraints:self.customConstraints];
	}
	[super updateConstraints];
}

#pragma mark - Initial
- (void)initialFirst{
	self.xValues = @[].mutableCopy;
	self.datasources = @[].mutableCopy;
	self.lineColor = [UIColor blueColor];
	self.backgroundColor = [UIColor clearColor];
	
	[self.indicator stopAnimating];
	self.indicator.hidden = YES;
	self.noDataLabel.hidden = YES;
	
	//Properties
	self.exchange = nil;
	self.code = nil;
	self.didLoadChart = NO;
	
	//Initial chart
	[self configChart];
}
- (void)configChart{
	_myGraph.displayDotsOnly = NO;
	_myGraph.enableTouchReport = NO;
	_myGraph.enablePopUpReport = NO;
	_myGraph.enableYAxisLabel = NO;
	_myGraph.enableXAxisLabel = NO;
	_myGraph.autoScaleYAxis = YES;
	_myGraph.alwaysDisplayDots = NO;
	_myGraph.enableReferenceXAxisLines = NO;
	_myGraph.enableReferenceYAxisLines = NO;
	_myGraph.enableReferenceAxisFrame = NO;
	
	// Set the graph's animation style to draw, fade, or none
	_myGraph.animationGraphStyle = BEMLineAnimationNone;
	_myGraph.animationGraphEntranceTime = 0.0;
	// Setup initial curve selection segment
	_myGraph.enableBezierCurve = NO;
	_myGraph.displayDotsWhileAnimating = NO;
	_myGraph.averageLine.enableAverageLine = NO;
	_myGraph.averageLine.color = [UIColor purpleColor];
	_myGraph.colorLine = self.lineColor;
	_myGraph.colorTop = [UIColor clearColor];
	_myGraph.colorBottom = [UIColor clearColor];
	_myGraph.backgroundColor = [UIColor clearColor];
	_myGraph.colorBackgroundXaxis = [UIColor clearColor];
	_myGraph.colorBackgroundYaxis = [UIColor clearColor];
	_myGraph.widthLine = 2.0;
	_myGraph.noDataLabelFont = AppFont_MainFontRegularWithSize(12);
	_myGraph.noDataLabelColor = [UIColor blackColor];
	_myGraph.enableNoDataLabel = NO;
	self.chartType = TCLineChartType_Day;
	// Dash the y reference lines
	self.myGraph.lineDashPatternForReferenceYAxisLines = @[@(2),@(2)];
	
	self.myGraph.delegate = self;
	self.myGraph.dataSource = self;
	[self.myGraph reloadGraph];
}
#pragma mark - Configs
- (void)setDatasources:(NSMutableArray *)datasources{
	_datasources = datasources;
	if (!datasources || datasources.count == 0) {
		self.noDataLabel.hidden = NO;
		self.myGraph.hidden = YES;
	}else{
		self.noDataLabel.hidden = YES;
		self.myGraph.hidden = NO;
	}
}
- (void)setLineColor:(UIColor *)lineColor{
	_lineColor = lineColor;
	self.myGraph.colorLine = lineColor;
}

#pragma mark - UTIL
- (NSString *)formatStringValueBy:(NSInteger)pointer{
	switch (pointer) {
		case 2:
			return @"%.2f";
		case 3:
			return @"%.3f";
		case 4:
			return @"%.4f";
		case 5:
			return @"%.5f";
		case 6:
			return @"%.6f";
		default:
			return @"%.0f";
	}
}
#pragma mark - Main
- (void)showFullInfoChart{
	self.myGraph.enableXAxisLabel = YES;
	self.myGraph.enableYAxisLabel = YES;
	self.myGraph.enableReferenceAxisFrame = YES;
	self.myGraph.enableReferenceYAxisLines = YES;
	self.myGraph.enableReferenceXAxisLines = YES;
	
	// Show the y axis values with this format string
	//[self toStringByPointer:[UserPrefConstants singleton].pointerDecimal];
	self.myGraph.formatStringForValues = [self formatStringValueBy:[UserPrefConstants singleton].pointerDecimal];
	[self.myGraph reloadGraph];
}

- (void)showShortInfoChart{
	self.myGraph.enableXAxisLabel = NO;
	self.myGraph.enableYAxisLabel = NO;
	self.myGraph.enableReferenceYAxisLines = NO;
	self.myGraph.enableReferenceAxisFrame = NO;
	self.myGraph.enableReferenceXAxisLines = NO;

	//self.myGraph.enableReferenceXAxisLines = YES;
	[self.myGraph reloadGraph];
}
- (UIImage *)snapshotImage{
	return [self.myGraph graphSnapshotImage];
}
- (UIImage *)snapshotImageInBackground:(BOOL)isInBackground{
	return [self.myGraph graphSnapshotImageRenderedWhileInBackground:isInBackground];
}
- (void)didFinishLoadChart:(TCLineChartViewBlock)completion{
	if (completion) {
		self.didLoadChartBlock = completion;
		completion = nil;
	}
}
- (void)reloadChart{
	[self.myGraph reloadGraph];
}

#pragma mark - Load Chart
- (void)loadChart:(TCLineChartType)type forCode:(NSString *)code inExchange:(NSString *)exchange includeLastValue:(NSNumber *)lastValue chartColor:(UIColor *)color{
	self.chartType = type;
	self.exchange = exchange;
	self.code = code;
	self.lineColor = color;
	self.lastValue = lastValue;
	
	//Check nil value by server dummy 
	if (!self.lastValue) {
		[self.indicator stopAnimating];
		self.indicator.hidden = YES;
		self.myGraph.hidden = YES;
		self.didLoadChart = YES;
		return;
	}
	
	NSString *cache_key = [NSString stringWithFormat:@"%@_%ld", code, TCLineChartType_Day];
	NSArray *dataChartArr = [[DataManager shared] getDataLineChartForKey:cache_key];
	if (dataChartArr) {
		[self.indicator stopAnimating];
		self.indicator.hidden = YES;
		self.myGraph.hidden = NO;
		self.didLoadChart = YES;
		[self drawChart:dataChartArr withLastValue:lastValue color:color];
	}else{
		//Call API to load chart
		self.didLoadChart = NO;
		self.indicator.hidden = NO;
		[self.indicator startAnimating];
		self.myGraph.hidden = YES;
		[self getChartFromAPI];
	}
}

//Call API
- (void)getChartFromAPI{
	if (![UserSession shareInstance].didLogin) {
		return;
	}
	if (self.didLoadChart) {
		[self.indicator stopAnimating];
		self.indicator.hidden = YES;
		self.myGraph.hidden = NO;
		return;
	}
	[[GCDQueue globalQueue] queueBlock:^{
		[[VertxConnectionManager singleton] getDefaultWeekChartDataFrom:self.code inExchange:self.exchange completion:^(id result, NSError *error) {
			[[GCDQueue mainQueue] queueBlock:^{
				if (self.didLoadChart) {
					return ;
				}
				if (error) {
					DLog(@"TCLineChart: Load Chart Error for Stock: %@", self.code);
					return ;
				}
				if (result) {
					NSArray *dataArr = (NSArray *)result;
					if (dataArr.count == 0) {
						//Show no data here
						DLog(@"TCLineChart: No data for stock: %@", self.code)
						return ;
					}
					NSDictionary *info = dataArr.firstObject;
					//Check whether data is correct
					if (![info[@"0"] isEqualToString:self.code]) {
						//Do nothing
						return;
					}
					//Start parse data and handle UI
					self.didLoadChart = YES;
					[self.indicator stopAnimating];
					self.indicator.hidden = YES;
					NSArray *chartArr = [dataArr subarrayWithRange:NSMakeRange(1, dataArr.count - 1)];//From index 1, because the first item is stock info item
					NSString *cache_key = [NSString stringWithFormat:@"%@_%ld", self.code, self.chartType];
					if (chartArr && chartArr.count > 0) {
						//Cache and load chart here
						[[DataManager shared] storeDataLineCharts:chartArr forKey:cache_key];
						[self drawChart:chartArr withLastValue:self.lastValue color:self.lineColor];
					}else{
						[[DataManager shared] storeDataLineCharts:@[] forKey:cache_key];
						DLog(@"TCLineChart: No data for stock: %@", self.code)
					}
				}
			}];
		}];
	}];
	
	//Random delay
	int NUMBER = ((float)rand() / RAND_MAX) * 3 + 1;
	float randomNum = ((float)rand() / RAND_MAX) * NUMBER;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, randomNum * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		if (!self.didLoadChart) {
			[self getChartFromAPI];
		}
	});
}

- (void)drawChart:(NSArray *)dataChart withLastValue:(NSNumber *)lastValue color:(UIColor *)color{
	if (!dataChart || dataChart.count == 0) {
		//No data available
		return;
	}
	//Hanle UI + Update chart
	NSMutableArray *xElements = @[].mutableCopy;
	NSMutableArray *yElements = @[].mutableCopy;
	for (int i = 0; i < dataChart.count; i++) {
		NSDictionary *dictData = dataChart[i];
		if ([dictData.allKeys containsObject:@"0"] && [dictData.allKeys containsObject:@"4"]) {
			[xElements addObject:dictData[@"0"]];
			[yElements addObject:[NSNumber numberWithDouble:[dictData[@"4"] doubleValue]]];
		}
	}
	
	//Add last object
	if (!lastValue) {
		//stop all
		self.myGraph.hidden = NO;
		self.indicator.hidden = YES;
		return;
	}
	[xElements addObject:@"now"];
	[yElements addObject:lastValue];
	self.myGraph.hidden = NO;
	self.indicator.hidden = YES;
	self.datasources = [yElements mutableCopy];
	self.xValues = [xElements mutableCopy];
	self.lineColor = color;
	[self reloadChart];
}

#pragma mark - SimpleLineGraph Data Source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph {
	return [self.datasources count];
}

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index {
	return [[self.datasources objectAtIndex:index] doubleValue];
}

#pragma mark - SimpleLineGraph Delegate

- (NSInteger)numberOfGapsBetweenLabelsOnLineGraph:(BEMSimpleLineGraphView *)graph {
	switch (self.chartType) {
		case TCLineChartType_Day:{
			return 1;
		}
			break;
		case TCLineChartType_1Month:{
			return 6;
		}
			break;
		case TCLineChartType_3Months:{
			return 18;
		}
			break;
		case TCLineChartType_6Months:{
			return 36;
		}
			break;
		case TCLineChartType_12Months:{
			return 72;
		}
			break;
			
		default:
			break;
	}
	return 1;
}

- (NSString *)lineGraph:(BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index {
	return _xValues[index];
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didTouchGraphWithClosestIndex:(NSInteger)index {
	//Do something here
}

- (void)lineGraph:(BEMSimpleLineGraphView *)graph didReleaseTouchFromGraphWithClosestIndex:(CGFloat)index {
	//Do something here
}

- (void)lineGraphDidFinishLoading:(BEMSimpleLineGraphView *)graph {
	//Do someting here
	if (self.didLoadChartBlock) {
		self.didLoadChartBlock();
	}
	if ([self.delegate respondsToSelector:@selector(didFinishLoadChart)]) {
		[self.delegate didFinishLoadChart];
	}
}

/* - (void)lineGraphDidFinishDrawing:(BEMSimpleLineGraphView *)graph {
 // Use this method for tasks after the graph has finished drawing
 } */

- (NSString *)popUpSuffixForlineGraph:(BEMSimpleLineGraphView *)graph {
	return @"###";
}
@end
