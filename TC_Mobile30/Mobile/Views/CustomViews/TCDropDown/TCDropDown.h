//
//  TCDropDown.h
//  TC_Mobile30
//
//  Created by Kaka on 10/16/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseView.h"

//Direction
typedef enum : NSInteger{
	TCDirection_Down,
	TCDirection_Up,
	TCDirection_Any
}TCDirection;

//Block
typedef void (^TCDropdownCompletion)(NSString *item, NSInteger index);

@interface TCDropDown : TCBaseView{
	
}
//Config
@property (assign, nonatomic) BOOL isSearchable;
@property (assign, nonatomic) TCDirection direction;
@property (assign, nonatomic) NSInteger numberVisibleRow;
@property (assign, nonatomic) float offset; //Distance from sourceview or sourcepoint to dropdown

//Use sourceView or source point to show
@property (strong, nonatomic) UIView *sourceView;
//Optional
@property (assign, nonatomic) float width;
@property (assign, nonatomic) CGPoint sourcePoint;
@property (assign, nonatomic) float rowHeight;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIColor *bgColor;
@property (assign, nonatomic) NSInteger defaltSelectedItem;
@property (assign, nonatomic) NSTextAlignment contentAlign;
@property (assign, nonatomic) BOOL didShowDropdown;
@property (assign, nonatomic) BOOL showSeparatedRow;
@property (strong, nonatomic) UIFont *font;
//Data
@property (strong, nonatomic) NSArray *dataSources;
- (void)addCustomView:(UIView *)customView;
//MAIN
- (void)show;
- (void)hide;

//Selected Dropdown Item
- (void)didSelectItem:(TCDropdownCompletion)selectionHandler;
@end
