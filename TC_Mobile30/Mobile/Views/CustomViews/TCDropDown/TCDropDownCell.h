//
//  TCDropDownCell.h
//  TC_Mobile30
//
//  Created by Kaka on 10/16/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface TCDropDownCell : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
//Passing data
@property (strong, nonatomic) NSString *content;

//Config
@property (strong, nonatomic) UIColor *textColor;
@end
