//
//  CustomTextField.m
//  TC_Mobile30
//
//  Created by Kaka on 10/16/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
	return CGRectInset(bounds, 8, 0);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
	return CGRectInset(bounds, 8, 0);
}
@end
