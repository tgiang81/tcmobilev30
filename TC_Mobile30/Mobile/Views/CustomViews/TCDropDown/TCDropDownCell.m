//
//  TCDropDownCell.m
//  TC_Mobile30
//
//  Created by Kaka on 10/16/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCDropDownCell.h"

@interface TCDropDownCell(){
	
}
@end
@implementation TCDropDownCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setup{
	
}

#pragma mark - Configs
- (void)setTextColor:(UIColor *)textColor{
	_lblContent.textColor = textColor;
	_textColor = textColor;
}
#pragma mark - Data
- (void)setContent:(NSString *)content{
    self.lblContent.text = [LanguageManager stringForKey:content];
	_content = content;
}
@end
