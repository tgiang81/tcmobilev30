//
//  TCDropDown.m
//  TC_Mobile30
//
//  Created by Kaka on 10/16/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCDropDown.h"
#import "TCBaseView.h"
#import "TCDropDownCell.h"
#import "CustomTextField.h"
#import "NSString+Util.h"
#import "HexColors.h"
#import "UIView+ShadowBorder.h"

#define kALIGN_TOP_BOTTOM	0
#define kALIGN_LEFT_RIGHT	2

#define kHEIGHT_SERCHVIEW	30
#define kSPACING_BETWEEN_SEACH_CONTENT_VIEW		3

#define kDEFAULT_OFFSET			3
#define kDEFAULT_VISIBLE_ROW	6
#define kDEFAULT_ROW_HEIGHT		35
#define kDEFAULT_CONNER_RADIUS	5.0


@interface TCDropDown()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
	BOOL _isFromSourceView;
	BOOL _didSetWidth;
	NSMutableArray *_filterArr;
	NSMutableArray *_constrains;
	UIWindow *_topWindow;
    UIView *_customView;
	
}
@property (strong, nonatomic) TCBaseView *containerView;
@property (strong, nonatomic) UITableView *tblContent;
@property (strong, nonatomic) CustomTextField *txtSearch;

//Can re-pass value
@property (strong, nonatomic) NSLayoutConstraint *heightSelfAnchor;
@property (strong, nonatomic) TCDropdownCompletion selectionHandler;

@end

@implementation TCDropDown

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init{
	self = [super init];
	if (self) {
		//init default
		[self iniDefaultValue];
	}
	return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
	self = [super initWithFrame:frame];
	if (self) {
		//init default value
		[self iniDefaultValue];
	}
	return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
	self = [super initWithCoder:aDecoder];
	if (self) {
		//init default value
		[self iniDefaultValue];
	}
	return self;
}

- (void)awakeFromNib{
	[super awakeFromNib];
	//Do something...
	self.layer.masksToBounds = YES;
}
#pragma mark - Main
- (void)iniDefaultValue{
	self.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	self.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].textTintColor);
	self.showSeparatedRow = NO;
	self.isSearchable = NO;
	_isFromSourceView = NO;
	_didShowDropdown = NO;
	_didSetWidth = NO;
	_defaltSelectedItem = -1;
	_contentAlign = NSTextAlignmentLeft;
	self.direction = TCDirection_Any;
	self.dataSources = @[].copy;
	_constrains = @[].mutableCopy;
	self.rowHeight = kDEFAULT_ROW_HEIGHT;
	self.numberVisibleRow = kDEFAULT_VISIBLE_ROW;
	self.offset = kDEFAULT_OFFSET;
	self.cornerRadius = kDEFAULT_CONNER_RADIUS;
	self.font =  AppFont_MainFontMediumWithSize(14);
	_heightSelfAnchor.constant = 0;
	_width = 0.4 * SCREEN_WIDTH_PORTRAIT;
	_filterArr = @[].mutableCopy;
	_topWindow = [[UIApplication sharedApplication] keyWindow];
	
	//View
	_containerView = [[TCBaseView alloc] initWithFrame:_topWindow.bounds];
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapConternainView:)];
	tap.numberOfTapsRequired = 1;
	tap.cancelsTouchesInView = YES;
	[_containerView addGestureRecognizer:tap];
	
	_tblContent = [[UITableView alloc] initWithFrame:CGRectZero];
	[_tblContent registerNib:[UINib nibWithNibName:[TCDropDownCell nibName] bundle:nil] forCellReuseIdentifier:[TCDropDownCell reuseIdentifier]];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_tblContent.estimatedRowHeight = kDEFAULT_ROW_HEIGHT;
	_tblContent.rowHeight = UITableViewAutomaticDimension;
	_tblContent.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	[_tblContent setSeparatorInset:UIEdgeInsetsZero];
	_tblContent.separatorStyle = self.showSeparatedRow? UITableViewCellSeparatorStyleSingleLine : UITableViewCellSeparatorStyleNone;
	self.tblContent.layer.cornerRadius = kDEFAULT_CONNER_RADIUS;

	_txtSearch = [[CustomTextField alloc] initWithFrame:CGRectZero];
	[_txtSearch setBorderStyle:UITextBorderStyleNone];
	_txtSearch.placeholder = [LanguageManager stringForKey:@"Search"];
	_txtSearch.backgroundColor = RGB(196, 224, 243);
	_txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
	_txtSearch.textColor = COLOR_FROM_HEX(0x0099F7);
	_txtSearch.font = _font;
	_txtSearch.delegate = self;
	[_txtSearch addTarget:self
				  action:@selector(textFieldDidChange:)
		forControlEvents:UIControlEventEditingChanged];
	
	[self makeBorderShadow];
	//+++ Add Notification for Update Theme
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTheme:) name:kDidChangeThemeNotification object:nil];
}

- (void)updateTheme:(NSNotification *)noti{
	self.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subBgColor);
	self.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].textTintColor);
}
#pragma mark - Override Properties
- (void)setShowSeparatedRow:(BOOL)showSeparatedRow{
	_tblContent.separatorStyle = showSeparatedRow? UITableViewCellSeparatorStyleSingleLine : UITableViewCellSeparatorStyleNone;
	_showSeparatedRow = showSeparatedRow;
}
- (void)setSourceView:(UIView *)sourceView{
	_isFromSourceView = YES;
	_sourceView = sourceView;
}

- (void)setSourcePoint:(CGPoint)sourcePoint{
	_sourcePoint = sourcePoint;
	_sourceView = [[UIView alloc] initWithFrame:CGRectMake(sourcePoint.x, sourcePoint.y, _width, 0)];
	_isFromSourceView = YES;
}
- (void)setWidth:(float)width{
	_width = width;
	_didSetWidth = YES;
}

- (void)setTextColor:(UIColor *)textColor{
	_textColor = textColor;
	[self.tblContent reloadData];
}

- (void)setBgColor:(UIColor *)bgColor{
	_bgColor = bgColor;
	self.backgroundColor = bgColor;
}
- (void)setDataSources:(NSArray *)dataSources{
	_filterArr = [dataSources mutableCopy];
	_dataSources = dataSources;
	[self.tblContent reloadData];
}

- (void)setDefaltSelectedItem:(NSInteger)defaltSelectedItem{
	_defaltSelectedItem = defaltSelectedItem;
	[self.tblContent reloadData];
}

- (void)setContentAlign:(NSTextAlignment)contentAlign{
	_contentAlign = contentAlign;
	[self.tblContent reloadData];
}
#pragma mark - UTILS
- (void)resetConstrains{
	//Remove Constrain
	[_topWindow removeConstraints:self->_constrains];
	[self removeConstraints:self->_constrains];
	[self->_tblContent removeConstraints:self->_tblContent.constraints];
	[self->_txtSearch removeConstraints:self->_txtSearch.constraints];
	[self->_constrains removeAllObjects];
}
- (BOOL)isUpFrom:(TCDirection)direction{
	if (direction == TCDirection_Down) {
		return NO;
	}
	if (direction == TCDirection_Up) {
		return YES;
	}
	if (direction == TCDirection_Any) {
		if (_isFromSourceView) {
			CGRect tmpRect = [_sourceView convertRect:_sourceView.bounds toView:_topWindow];
			float heightUp = tmpRect.origin.y - _offset;
			float heightDown = SCREEN_HEIGHT_PORTRAIT - (tmpRect.origin.y + tmpRect.size.height + _offset);
			return heightUp > heightDown;
		}else{
			//Using from SourcePoint
			CGPoint tmpPoint = [self convertPoint:_sourcePoint toView:_topWindow]; //Mayber not need
			float heightUp = tmpPoint.y - _offset;
			float heightDown = SCREEN_HEIGHT_PORTRAIT - (tmpPoint.y + _offset);
			return heightUp > heightDown;
		}
	}
	return NO;
}
- (CGRect)rectFrom:(BOOL)isSourceView{
	CGRect mainRect;
	if (isSourceView) {
		CGRect tmpRect = [_sourceView convertRect:_sourceView.bounds toView:_topWindow];
		tmpRect.size.height = self.isSearchable ? ((2 * kALIGN_TOP_BOTTOM) + (_numberVisibleRow * _rowHeight) + kSPACING_BETWEEN_SEACH_CONTENT_VIEW + kHEIGHT_SERCHVIEW) : ((2 * kALIGN_TOP_BOTTOM) + ((_dataSources.count > _numberVisibleRow)? (_numberVisibleRow * _rowHeight) : (_dataSources.count * _rowHeight)));
		BOOL isUpDirection = [self isUpFrom:_direction];
		if (isUpDirection == YES) {
			tmpRect.origin.y -= (_offset + tmpRect.size.height);
		}else{
			tmpRect.origin.y += _sourceView.frame.size.height + _offset;
		}
		float mainWidth;
		if (_didSetWidth) {
			mainWidth = _width;
		}else{
			if (tmpRect.size.width <= (4 * kALIGN_LEFT_RIGHT)) {
				mainWidth = _width;
			}else{
				mainWidth = tmpRect.size.width;
			}
		}
		
		//Check should show left or right
		if (tmpRect.origin.x > (SCREEN_WIDTH_PORTRAIT - tmpRect.origin.x)) {
			tmpRect.origin.x =  tmpRect.origin.x - mainWidth + tmpRect.size.width;
		}
		
		tmpRect.size.width = mainWidth;
		mainRect = tmpRect;
	}else{
		//Rect from Point
		CGPoint tmpPoint = [self convertPoint:_sourcePoint toView:_topWindow]; //Mayber not need
		BOOL isUpDirection = [self isUpFrom:_direction];
		if (isUpDirection) {
			tmpPoint.y -= _offset;
		}else{
			tmpPoint.y += _offset;
		}
		
		//Check should show left or right
		float xPoint;
		if (tmpPoint.x > (SCREEN_WIDTH_PORTRAIT - tmpPoint.x)) {
			xPoint = tmpPoint.x - _width;
		}else{
			xPoint = tmpPoint.x;
		}
		float heightContent = _isSearchable ? ((2 * kALIGN_TOP_BOTTOM) + (_numberVisibleRow * _rowHeight) + kSPACING_BETWEEN_SEACH_CONTENT_VIEW + kHEIGHT_SERCHVIEW) : ((2 * kALIGN_TOP_BOTTOM) + ((_dataSources.count > _numberVisibleRow)? (_numberVisibleRow * _rowHeight) : (_dataSources.count * _rowHeight)));
		CGSize tmpSize = CGSizeMake(_width, heightContent);
		mainRect = CGRectMake(xPoint, tmpPoint.y, tmpSize.width, tmpSize.height);
	}
	return mainRect;
}

- (void)addConstrailFromRect:(CGRect)mainRect isUpDirection:(BOOL)isUpDirection{
	[self setTranslatesAutoresizingMaskIntoConstraints:NO];
	[_txtSearch setTranslatesAutoresizingMaskIntoConstraints:NO];
	[_tblContent setTranslatesAutoresizingMaskIntoConstraints:NO];
	if (isUpDirection) {
		//Constrain for Self
		NSLayoutConstraint *bottomAnchor = [self.topAnchor constraintEqualToAnchor:_topWindow.topAnchor constant:mainRect.origin.y]; //(SCREEN_HEIGHT_PORTRAIT - mainRect.origin.y)
		[_constrains addObject:bottomAnchor];
		
		NSLayoutConstraint *leadingAnchor = [self.leadingAnchor constraintEqualToAnchor:_topWindow.leadingAnchor constant:mainRect.origin.x];
		[_constrains addObject:leadingAnchor];
		
		NSLayoutConstraint *widthAnchor = [self.widthAnchor constraintEqualToConstant:mainRect.size.width];
		[_constrains addObject:widthAnchor];
		
		_heightSelfAnchor = [self.heightAnchor constraintEqualToConstant:0];
		[_constrains addObject:_heightSelfAnchor];
	}else{
		//Constrain for Self
		NSLayoutConstraint *topAnchor = [self.topAnchor constraintEqualToAnchor:_topWindow.topAnchor constant:mainRect.origin.y];
		[_constrains addObject:topAnchor];
		
		NSLayoutConstraint *leadingAnchor = [self.leadingAnchor constraintEqualToAnchor:_topWindow.leadingAnchor constant:mainRect.origin.x];
		[_constrains addObject:leadingAnchor];
		
		NSLayoutConstraint *widthAnchor = [self.widthAnchor constraintEqualToConstant:mainRect.size.width];
		[_constrains addObject:widthAnchor];
		
		_heightSelfAnchor = [self.heightAnchor constraintEqualToConstant:0];
		[_constrains addObject:_heightSelfAnchor];
	}
	//Constrain for Search Textfield
	NSLayoutConstraint *leadingSearchViewAnchor = [_txtSearch.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:kALIGN_LEFT_RIGHT];
	[_constrains addObject:leadingSearchViewAnchor];
	NSLayoutConstraint *trailingSearchViewAnchor = [_txtSearch.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-kALIGN_LEFT_RIGHT];
	[_constrains addObject:trailingSearchViewAnchor];
	NSLayoutConstraint *topSearchViewAnchor = [_txtSearch.topAnchor constraintEqualToAnchor:self.topAnchor constant:kALIGN_LEFT_RIGHT];
	[_constrains addObject:topSearchViewAnchor];

	float heightSearchView = _isSearchable ? kHEIGHT_SERCHVIEW : 0;
	NSLayoutConstraint *heightSearchViewAnchor = [_txtSearch.heightAnchor constraintEqualToConstant:heightSearchView];
	[_constrains addObject:heightSearchViewAnchor];

	//Constrain for TableView
	NSLayoutConstraint *leadingTBVAnchor = [_tblContent.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:kALIGN_LEFT_RIGHT];
	[_constrains addObject:leadingTBVAnchor];
	NSLayoutConstraint *trailingTBVAnchor = [_tblContent.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-kALIGN_LEFT_RIGHT];
	[_constrains addObject:trailingTBVAnchor];
	
	float kSpaing = _isSearchable ? kSPACING_ITEM : 0;
	NSLayoutConstraint *topTBVAnchor = [_tblContent.topAnchor constraintEqualToAnchor:_txtSearch.bottomAnchor constant:kSpaing];
	[_constrains addObject:topTBVAnchor];
	
	NSLayoutConstraint *bottomTBVAnchor = [_tblContent.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-kALIGN_TOP_BOTTOM];
	[_constrains addObject:bottomTBVAnchor];
    
    if(_customView){
        NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_customView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_tblContent attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:_customView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_tblContent attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:_customView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_tblContent attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
        NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:_customView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_tblContent attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
        [self addConstraints:@[top,bottom,leading,trailing]];
    }
    
    
	
	//Active Constrail
	[NSLayoutConstraint activateConstraints:_constrains];
}

- (void)addFrameFromRect:(CGRect)mainRect isUpDirection:(BOOL)isUpDirection{
	self.frame = mainRect;
	[_topWindow addSubview:self];
	//Add SearchView
	_txtSearch.frame = CGRectMake(kALIGN_LEFT_RIGHT, kALIGN_TOP_BOTTOM, mainRect.size.width - 2*kALIGN_LEFT_RIGHT, _isSearchable ? kHEIGHT_SERCHVIEW : 0);
	[self addSubview:_txtSearch];
	//Add Contentview
	float orY = _isSearchable ? (kHEIGHT_SERCHVIEW + kSPACING_ITEM + kALIGN_TOP_BOTTOM) : (kALIGN_TOP_BOTTOM);
	float heigtContent = _isSearchable ? (mainRect.size.height - kSPACING_ITEM - kHEIGHT_SERCHVIEW - 2*kALIGN_TOP_BOTTOM) : (mainRect.size.height - 2*kALIGN_TOP_BOTTOM);
	_tblContent.frame = CGRectMake(kALIGN_LEFT_RIGHT, orY, mainRect.size.width - 2*kALIGN_LEFT_RIGHT, heigtContent);
	[self addSubview:_tblContent];
}

- (void)addCustomView:(UIView *)customView{
    _customView = customView;
}

- (void)removeSubviews:(NSArray *)subviews{
	for (UIView *aView in subviews) {
		[aView removeFromSuperview];
	}
}

- (void)startSearching:(NSString *)searchText{
	if ([searchText removeAllWhitespace].length == 0) {
		_filterArr = [_dataSources copy];
	}else{
		NSString *searchTerm = [searchText trimmedLeadingAndTrailingWhiteSpace];
		//Start searching
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", searchTerm];
		_filterArr = [[_dataSources filteredArrayUsingPredicate:predicate] copy];
	}
	//Update UI
	[self updateSizeBySearchedContent:_filterArr];
}
//+++Update UI to fit with number of content: Maybe not use if needed
- (void)updateSizeBySearchedContent:(NSArray *)content{
	CGRect originalSize = [self rectFrom:_isSearchable];
	if (content.count >= _numberVisibleRow) {
		_heightSelfAnchor.constant = originalSize.size.height;
	}else if (content.count == 0){ //Maybe not use this case
		_heightSelfAnchor.constant = originalSize.size.height - (_numberVisibleRow * _rowHeight) - kSPACING_BETWEEN_SEACH_CONTENT_VIEW;
	}else{
		//For < case
		_heightSelfAnchor.constant = originalSize.size.height - ((_numberVisibleRow - content.count) * _rowHeight);
	}
	//Recalculate size
	[UIView animateWithDuration:0.35 animations:^{
		[self->_topWindow setNeedsLayout];
	}completion:^(BOOL finished) {
		[self->_tblContent reloadData];
	}];
}
#pragma mark - ACTION
- (void)show{
	if (_didShowDropdown) {
		[self hide];
		return;
	}
	//Remove if needed
	[_containerView removeFromSuperview];
	[self removeFromSuperview];
	if (_dataSources.count < _numberVisibleRow) {
		_numberVisibleRow = _dataSources.count;
	}
	CGRect mainRect = [self rectFrom:_isFromSourceView];
	[_topWindow addSubview:_containerView];
	[_topWindow addSubview:self];
	[_topWindow bringSubviewToFront:self];
	[self addSubview:_txtSearch];
	[self addSubview:_tblContent];
    if(_customView){
        [self addSubview:_customView];
    }
    
	_txtSearch.textAlignment = self.contentAlign;

	//Add constrain
	BOOL isUpDirection = [self isUpFrom:_direction];
	//+++ Use Constrain below
	[self addConstrailFromRect:mainRect isUpDirection:isUpDirection];
	
	_heightSelfAnchor.constant = mainRect.size.height;
	[UIView animateWithDuration:0.2 delay:0.1 usingSpringWithDamping:0.9 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveLinear animations:^{
		[self->_topWindow setNeedsDisplay];
	} completion:^(BOOL finished) {
		self.txtSearch.hidden = !self.isSearchable;
		self->_didShowDropdown = YES;
	}];
}
- (void)hide{
	if (!_didShowDropdown) {
		return;
	}
	[self endEditing:YES];
	_heightSelfAnchor.constant = 0;
	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
		[self->_topWindow setNeedsDisplay];
	} completion:^(BOOL finished) {
		//Reset config
		self.numberVisibleRow = kDEFAULT_VISIBLE_ROW;
		//Remove Constrain
		[self resetConstrains];
		//Remove subview
		[self->_containerView removeFromSuperview];
		[self removeSubviews:self.subviews];
		[self removeFromSuperview];
		//Update data
		self->_filterArr = [self->_dataSources copy];
		[self.tblContent reloadData];
		self->_didShowDropdown = NO;
	}];
}

- (void)tapConternainView:(UITapGestureRecognizer *)tap{
	if (_didShowDropdown) {
		[self hide];
	}
}

#pragma mark - DidSelectItem
- (void)didSelectItem:(TCDropdownCompletion)selectionHandler{
	if (selectionHandler) {
		self.selectionHandler = selectionHandler;
		selectionHandler = nil;
	}
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (_isSearchable) {
		return [_filterArr count];
	}
	return [_dataSources count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	TCDropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:[TCDropDownCell reuseIdentifier]];
	if (!cell) {
		cell = [[TCDropDownCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[TCDropDownCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	if (indexPath.row == _defaltSelectedItem) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}else{
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	//Align
	cell.lblContent.textAlignment = self.contentAlign;
	cell.textColor = _textColor;
	cell.content = _isSearchable ? _filterArr[indexPath.row] : _dataSources[indexPath.row];
	cell.lblContent.font = self.font;
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	NSInteger index = indexPath.row;
	NSString *item = _dataSources[indexPath.row];
	if (_isSearchable) {
		item = _filterArr[indexPath.row];
		index = [_dataSources indexOfObject:item];
	}
	self.selectionHandler(item, index);
	[self hide];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldClear:(UITextField *)textField{
	return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
	
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	return YES;
}

//For search
- (void)textFieldDidChange:(UITextField *)textField {
	[self startSearching:textField.text];
}
@end
