//
//  TCTopAlertOptions.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCTopAlertOptions.h"
#import "TCMenuDropDown.h"
#import "AlertModel.h"
#import "AppFont.h"
#import "ThemeManager.h"
@interface TCTopAlertOptions() <TCMenuDropDownDelegate>
{
    BOOL isSetup;
}
@end
@implementation TCTopAlertOptions
- (void)awakeFromNib{
    [super awakeFromNib];
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);
    [self.sld_Value setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor)];
    [self.sw_Option setOnTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor)];
    self.lbl_currentcy.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);
    self.inputView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingDropDownBg);
    self.tf_content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);
    self.inputView.layer.cornerRadius = 8;
    
    [self.tf_content addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
}
- (void)setConditionType:(NSInteger)comditionType{
    _conditionType = comditionType;
    [self setupDropDown];
}
- (void)setStringValue:(NSString *)stringValue{
    _stringValue = stringValue;
    self.tf_content.text = stringValue;
    [self.sld_Value setMaximumValue:[stringValue doubleValue] == 0 ? 1 : [stringValue doubleValue]*2];
    [self.sld_Value setValue:[stringValue doubleValue]];
    
}
- (void)setupDropDown{
    if(isSetup == NO){
        isSetup = YES;
        _v_dropDownOptions.useMkDropDown = YES;
        _v_dropDownOptions.dismissAutoResize = YES;
        _v_dropDownOptions.selectedLabelFont = AppFont_MainFontRegularWithSize(12);
        _v_dropDownOptions.titleAlign = NSTextAlignmentCenter;
        _v_dropDownOptions.height = 28;
        _v_dropDownOptions.radius = 12;
        _v_dropDownOptions.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintDropDownButtonColor);
        _v_dropDownOptions.indicatorImage = [UIImage imageNamed:@"ic_dropDownDarkBlue"];
        _v_dropDownOptions.tintIcon = TC_COLOR_FROM_HEX([[ThemeManager shareInstance] stt_tintDropDownButtonColor]);
        NSMutableArray *items = [AlertModel getListCondition];
        [items removeObjectAtIndex:0];
        _v_dropDownOptions.titleMenu = items[_conditionType-1];;
        _v_dropDownOptions.menuTitles = items;
        _v_dropDownOptions.numberItemVisible = 6;
        _v_dropDownOptions.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingDropDownBg);
        _v_dropDownOptions.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
        _v_dropDownOptions.delegate = self;
        [_v_dropDownOptions reloadDropDown];
    }
    
}
- (void)setMaximumValue:(CGFloat)value{
    [self.sld_Value setMaximumValue:value];
}
- (IBAction)changeValue:(id)sender {
    [self.sld_Value setValue:((int)((self.sld_Value.value + 0.25) / 0.5) * 0.5) animated:NO];
    self.tf_content.text = [NSString stringWithFormat:@"%f", self.sld_Value.value];
}
- (void)textFieldDidChange:(UITextField *)sender {
    CGFloat maxValue = [sender.text doubleValue];
    if(maxValue <= self.sld_Value.maximumValue){
        [self.sld_Value setValue:maxValue];
    }else{
        [self.sld_Value setValue:self.sld_Value.maximumValue];
    }
    
}

#pragma mark TCMenuDropDownDelegate
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
    self.conditionType = index + 1;
}
@end
