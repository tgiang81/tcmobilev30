//
//  TCBottomAlertOptions2.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/11/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

@interface TCBottomAlertOptions2 : TCBaseSubView
@property (weak, nonatomic) IBOutlet UISwitch *sw_active;
@property (weak, nonatomic) IBOutlet UITextView *tv_input;
@property (assign, nonatomic) BOOL isOn;
@property (strong, nonatomic) NSString *remark;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Active;
@property (weak, nonatomic) IBOutlet UILabel *lbl_rmk;
@property (assign, nonatomic) BOOL isCreate;

@end
