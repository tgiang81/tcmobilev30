//
//  TCBottomAlertOptions2.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/11/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBottomAlertOptions2.h"
#import "ThemeManager.h"
@implementation TCBottomAlertOptions2

- (void)awakeFromNib{
    [super awakeFromNib];
    self.lbl_rmk.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTintColor);
    self.lbl_Active.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTintColor);
    [self.sw_active setOnTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingSwitchTintColor)];
    self.tv_input.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTintColor);
    self.tv_input.layer.cornerRadius = 8;
    self.tv_input.layer.borderWidth = 1;
    self.tv_input.layer.borderColor = [[UIColor darkGrayColor] CGColor];
}
- (void)setIsCreate:(BOOL)isCreate{
    _isCreate = isCreate;
    [self.sw_active setHidden:isCreate];
}
- (void)setIsOn:(BOOL)isOn{
    _isOn = isOn;
    [self.sw_active setOn:isOn];
}
- (void)setRemark:(NSString *)remark{
    _remark = remark;
    self.tv_input.text = remark;
}
@end
