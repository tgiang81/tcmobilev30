//
//  TCTopAlertOptions.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"
#import "TCMenuDropDown.h"
@interface TCTopAlertOptions : TCBaseSubView
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_dropDownOptions;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UITextField *tf_content;
@property (weak, nonatomic) IBOutlet UILabel *lbl_currentcy;

@property (assign, nonatomic) NSInteger conditionType;
@property (strong, nonatomic) NSString *stringValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UISwitch *sw_Option;
@property (weak, nonatomic) IBOutlet UISlider *sld_Value;
- (void)setMaximumValue:(CGFloat)value;
@end
