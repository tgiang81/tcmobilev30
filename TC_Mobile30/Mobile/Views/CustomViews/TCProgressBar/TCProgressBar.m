//
//  TCProgressBar.m
//  DemoProgressBar
//
//  Created by Kaka on 4/24/18.
//  Copyright © 2018 MMS. All rights reserved.
//

#import "TCProgressBar.h"
@interface TCProgressBar (){
	NSMutableArray *_progressConstraints;
}
@property (weak, nonatomic) UIView *progress;
@end

@implementation TCProgressBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (void)layoutSubviews{
	[super layoutSubviews];
	[self updateUI];
}

#pragma mark - Inspectable Properties
- (void)setRoundedCorners:(BOOL)roundedCorners{
	_roundedCorners = roundedCorners;
	[self updateUI];
}

- (void)setSpacing:(CGFloat)spacing{
	_spacing = fmax(0, spacing);
	[self updateUI];
}

- (void)setOutlineWidth:(CGFloat)outlineWidth{
	_outlineWidth = fmax(0, outlineWidth);
	[self updateUI];
}

- (void)setOutlineColor:(UIColor *)outlineColor{
	_outlineColor = outlineColor;
	[self updateUI];
}

- (void)setProgressColor:(UIColor *)progressColor{
	_progressColor = progressColor;
	[self updateUI];
}
/**
 Sets the value of the progress bar in a range of 0.0 to 1.0.
 */
- (void)setValue:(CGFloat)value{
	_value = fmax(0, fmin(value, 1));
	[self updateUI];
}

#pragma mark - INITIALIZERS
- (void)commonInit{
	_progressConstraints = @[].mutableCopy;
	[self addProgress];
	[self updateUI];
}
- (void)addProgress{
	UIView *progressView = [UIView new];
	progressView.translatesAutoresizingMaskIntoConstraints = NO;
	progressView.backgroundColor = self.progressColor;
	[self addSubview:progressView];
	self.progress = progressView;
	self.layer.masksToBounds = YES;
}

- (void)updateUI{
	[self removeConstraints:_progressConstraints];
	NSLayoutConstraint *leadingConstrain = [self.progress.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:(self.outlineWidth + self.spacing)];
	NSLayoutConstraint *widthConstrain = [self.progress.widthAnchor constraintGreaterThanOrEqualToAnchor:self.widthAnchor multiplier:self.value constant:(self.outlineWidth * 2 + self.spacing * 2)];
	NSLayoutConstraint *topConstrain = [self.progress.topAnchor constraintEqualToAnchor:self.topAnchor constant:(self.outlineWidth + self.spacing)];
	NSLayoutConstraint *bottomConstrain = [self.progress.bottomAnchor constraintEqualToAnchor:self.bottomAnchor constant:-(self.outlineWidth + self.spacing)];
	_progressConstraints = @[leadingConstrain, widthConstrain, topConstrain, bottomConstrain].mutableCopy;
	
	[UIView animateWithDuration:0.3 animations:^{
		[NSLayoutConstraint activateConstraints:self->_progressConstraints.copy];
	}];
	
	self.layer.cornerRadius = self.roundedCorners ? self.frame.size.height / 2 : 0 ;
	//No need using coner radius for content
	//self.progress.layer.cornerRadius = self.roundedCorners ? self.progress.frame.size.height / 2 : 0 ;
	self.progress.backgroundColor = self.progressColor;
	self.layer.borderWidth = self.outlineWidth;
	self.layer.borderColor = self.outlineColor.CGColor;
}

@end
