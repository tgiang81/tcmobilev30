//
//  TCProgressBar.h
//  DemoProgressBar
//
//  Created by Kaka on 4/24/18.
//  Copyright © 2018 MMS. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface TCProgressBar : UIView{
	
}
@property (assign, nonatomic) IBInspectable BOOL roundedCorners;
@property (assign, nonatomic) IBInspectable CGFloat spacing;
@property (assign, nonatomic) IBInspectable CGFloat outlineWidth;
@property (strong, nonatomic) IBInspectable UIColor *outlineColor;
@property (strong, nonatomic) IBInspectable UIColor *progressColor;

@property (assign, nonatomic) IBInspectable CGFloat value;

@end


