//
//  AccountInfoView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCBaseSubView.h"
@class UserAccountClientData;
NS_ASSUME_NONNULL_BEGIN
@protocol AccountInfoViewDelegate<NSObject>
-(void)didSelectButton:(UITableViewCell *)cell;
@end
@interface AccountInfoView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UIView *v_Content;
@property (weak, nonatomic) IBOutlet UIView *v_image;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AccountType;
@property (weak, nonatomic) IBOutlet UILabel *lbl_InvestedValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AccountNo;
@property (weak, nonatomic) IBOutlet UIButton *btn_Action;

@property (weak, nonatomic) id<AccountInfoViewDelegate> delegate;
- (void)setupDataForCell:(UserAccountClientData *)model;
@end

NS_ASSUME_NONNULL_END
