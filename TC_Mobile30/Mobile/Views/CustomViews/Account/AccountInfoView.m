//
//  AccountInfoView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AccountInfoView.h"
#import "UserAccountClientData.h"
#import "ThemeManager.h"
#import "NSNumber+Formatter.h"
#import "BrokerManager.h"
#import "DetailBrokerModel.h"
#import "UserPrefConstants.h"
@implementation AccountInfoView
- (void)awakeFromNib{
    [super awakeFromNib];
    self.v_image.layer.cornerRadius = 8;
    self.v_image.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_bgImageCell);
    self.v_Content.backgroundColor =  TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_bgCell);
    
    self.lbl_Name.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell);
    self.lbl_InvestedValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell);
    self.lbl_AccountNo.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell);

    self.lbl_AccountType.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell);
    [self.btn_Action setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell)];

}
- (void)setDelegate:(id<AccountInfoViewDelegate>)delegate{
    _delegate = delegate;
    self.v_Content.layer.cornerRadius = 8;
    [self.btn_Action setHidden:NO];
}
- (void)setupDataForCell:(UserAccountClientData *)model{
    self.lbl_Name.text = model.client_name;
    self.lbl_InvestedValue.text = [NSString stringWithFormat:@"%@%@", model.client_acc_exchange, [[NSNumber numberWithDouble:model.credit_limit] toCurrencyNumber]];
    self.lbl_AccountNo.text = [NSString stringWithFormat:@"%@ %@",@"Account No:", model.client_account_number];
    NSString *name = [BrokerManager shareInstance].detailBroker.AppName;
    if(model.isMarginAccount == 1){
        self.lbl_AccountType.text = [NSString stringWithFormat:@"%@, %@", name, @"Margin Account"];
    }else{
        self.lbl_AccountType.text = name;
    }
    
}

#pragma mark
- (IBAction)showDetail:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(didSelectButton:)]){
        [self.delegate didSelectButton: [UITableViewCell new]];
    }
}


@end
