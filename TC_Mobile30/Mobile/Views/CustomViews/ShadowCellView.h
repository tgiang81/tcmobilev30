//
//  ShadowCellView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/16/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShadowCellView : UIView

@end

NS_ASSUME_NONNULL_END
