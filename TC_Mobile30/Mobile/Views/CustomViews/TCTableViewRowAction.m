//
//  TCTableViewRowAction.m
//  TCiPad
//
//  Created by Kaka on 8/7/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCTableViewRowAction.h"

@implementation TCTableViewRowAction

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (instancetype)rowActionWithStyle:(UITableViewRowActionStyle)style title:(NSString *)title icon:(UIImage*)icon handler:(void (^)(UITableViewRowAction *action, NSIndexPath *indexPath))handler
{
	if (title.length) title = [@"\n" stringByAppendingString:title]; // move title under centerline; icon will go above
	TCTableViewRowAction *action = [super rowActionWithStyle:style title:title handler:handler];
	action.icon = icon;
	return action;
}

- (void)_setButton:(UIButton*)button
{
	if (self.font) button.titleLabel.font = self.font;
	if (self.icon) {
		[button setImage:[self.icon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
		button.tintColor = button.titleLabel.textColor;
		CGSize titleSize = [button.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:button.titleLabel.font}];
		button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height/2 + 5), 0, 0, -titleSize.width); // +5px gap under icon
	}
}
@end
