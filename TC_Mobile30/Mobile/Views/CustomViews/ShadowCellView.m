//
//  ShadowCellView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/16/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "ShadowCellView.h"
#import "UIView+ShadowBorder.h"
@implementation ShadowCellView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        [self setupDeauftCellBorderAndShadow];
    }
    return self;
}

@end
