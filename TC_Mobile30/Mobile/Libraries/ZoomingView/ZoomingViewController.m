//
//  ZoomingViewController.m
//  TapZoomRotate
//
//  Created by Matt Gallagher on 2010/09/27.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import "ZoomingViewController.h"
#import "AllCommon.h"

@implementation ZoomingViewController

@synthesize proxyView;
@synthesize view;

//- (instancetype)init{
//	self = [super init];
//	if (self) {
//
//	}
//	return self;
//}

#pragma mark - UTIL
- (void)setDismissDoubleTapToViewFull:(BOOL)dismissDoubleTapToViewFull{
    if (dismissDoubleTapToViewFull) {
        if (singleTapGestureRecognizer) {
            [self.view removeGestureRecognizer:singleTapGestureRecognizer];
            singleTapGestureRecognizer = nil;
        }
    }else{
        
    }
    _dismissDoubleTapToViewFull = dismissDoubleTapToViewFull;
}

- (BOOL)isLandsCapeDevice{
	UIInterfaceOrientation iOrientation = [UIApplication sharedApplication].statusBarOrientation;
	UIDeviceOrientation dOrientation = [UIDevice currentDevice].orientation;
	bool landscape;
	
	if (dOrientation == UIDeviceOrientationUnknown || dOrientation == UIDeviceOrientationFaceUp || dOrientation == UIDeviceOrientationFaceDown) {
		// If the device is laying down, use the UIInterfaceOrientation based on the status bar.
		landscape = UIInterfaceOrientationIsLandscape(iOrientation);
	} else {
		// If the device is not laying down, use UIDeviceOrientation.
		landscape = UIDeviceOrientationIsLandscape(dOrientation);
	}
	return landscape;
}
- (CGAffineTransform)orientationTransformFromSourceBounds:(CGRect)sourceBounds
{
	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	if (orientation == UIDeviceOrientationFaceUp ||
		orientation == UIDeviceOrientationFaceDown)
	{
		orientation = [UIApplication sharedApplication].statusBarOrientation;
	}
	
	if (orientation == UIDeviceOrientationPortraitUpsideDown)
	{
		return CGAffineTransformMakeRotation(M_PI);
	}
	else if (orientation == UIDeviceOrientationLandscapeLeft)
	{
		CGRect windowBounds = self.view.window.bounds;
		CGAffineTransform result = CGAffineTransformMakeRotation(0.5 * M_PI);
		result = CGAffineTransformTranslate(result,
			0.5 * (windowBounds.size.height - sourceBounds.size.width),
			0.5 * (windowBounds.size.height - sourceBounds.size.width));
		return result;
	}
	else if (orientation == UIDeviceOrientationLandscapeRight)
	{
		CGRect windowBounds = self.view.window.bounds;
		CGAffineTransform result = CGAffineTransformMakeRotation(-0.5 * M_PI);
		result = CGAffineTransformTranslate(result,
			0.5 * (windowBounds.size.width - sourceBounds.size.height),
			0.5 * (windowBounds.size.width - sourceBounds.size.height));
		return result;
	}

	return CGAffineTransformIdentity;
}

- (CGRect)rotatedWindowBounds
{
	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	if (orientation == UIDeviceOrientationFaceUp ||
		orientation == UIDeviceOrientationFaceDown)
	{
		orientation = [UIApplication sharedApplication].statusBarOrientation;
	}
	
	if (orientation == UIDeviceOrientationLandscapeLeft ||
		orientation == UIDeviceOrientationLandscapeRight)
	{
		CGRect windowBounds = self.view.window.bounds;
		return CGRectMake(0, 0, windowBounds.size.height, windowBounds.size.width);
	}

	return self.view.window.bounds;
}

- (void)deviceRotated:(NSNotification *)aNotification
{
	if (proxyView)
	{
		if (aNotification)
		{
			CGRect windowBounds = self.view.window.bounds;
			UIView *blankingView =
				[[[UIView alloc] initWithFrame:
					CGRectMake(-0.5 * (windowBounds.size.height - windowBounds.size.width),
						0, windowBounds.size.height, windowBounds.size.height)] autorelease];
			blankingView.backgroundColor = [UIColor blackColor];
			[self.view.superview insertSubview:blankingView belowSubview:self.view];
			
			[UIView animateWithDuration:0.35 animations:^{
				self.view.bounds = [self rotatedWindowBounds];
				self.view.transform = [self orientationTransformFromSourceBounds:self.view.bounds];
			} completion:^(BOOL complete){
				[blankingView removeFromSuperview];
			}];
		}
		else
		{
			self.view.bounds = [self rotatedWindowBounds];
			self.view.transform = [self orientationTransformFromSourceBounds:self.view.bounds];
		}
	}
	else
	{
		self.view.transform = CGAffineTransformIdentity;
		//self.view.transform = _originalTransfrom;
	}
}

- (void)toggleZoom:(UITapGestureRecognizer *)gestureRecognizer
{
	
	if (proxyView)
	{
		
		CGRect frame =
			[proxyView.superview
				convertRect:self.view.frame
				fromView:self.view.window];
		self.view.frame = frame;
		
		CGRect proxyViewFrame = proxyView.frame;

		[proxyView.superview addSubview:self.view];
		[proxyView removeFromSuperview];
		[proxyView autorelease];
		proxyView = nil;
		
		[AppDelegateAccessor.container setRootViewStatusBarHidden:NO];
		AppDelegateAccessor.container.rootViewStatusBarUpdateAnimation = UIStatusBarAnimationFade;
		[UIApplication sharedApplication].statusBarHidden = NO;;
		UIView *statusBarView = [[UIApplication sharedApplication] valueForKey:@"statusBar"];
		statusBarView.hidden = NO;
		//+++ Force Portrait
		if ([self isLandsCapeDevice]) {
			[UIView animateWithDuration:0.5 animations:^{
				[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
				[UINavigationController attemptRotationToDeviceOrientation];
			} completion:^(BOOL finished) {
				//Animate
				[UIView
				 animateWithDuration:0.35
				 animations:^{
					 self.view.frame = proxyViewFrame;
				 }];
			}];
		}else{
			[UIView
			 animateWithDuration:0.35
			 animations:^{
				 self.view.frame = proxyViewFrame;
			 }];
		}
		
		//[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
		//[AppDelegateAccessor.container setRootViewStatusBarHidden:NO];
		
		[[NSNotificationCenter defaultCenter]
			removeObserver:self
			name:UIDeviceOrientationDidChangeNotification
			object:[UIDevice currentDevice]];
	}
	else
	{
		//+++Force landscape
		/*
		[UIView animateWithDuration:0.5 animations:^{
			[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeLeft) forKey:@"orientation"];
			[UINavigationController attemptRotationToDeviceOrientation];
		}];
		 */
		[self.view setTranslatesAutoresizingMaskIntoConstraints:YES];
		proxyView = [[UIView alloc] initWithFrame:self.view.frame];
		proxyView.hidden = YES;
		proxyView.autoresizingMask = self.view.autoresizingMask;
		[self.view.superview addSubview:proxyView];
		
		CGRect frame =
			[self.view.window
				convertRect:self.view.frame
				fromView:proxyView.superview];
		[self.view.window addSubview:self.view];
		self.view.frame = frame;
		
		[AppDelegateAccessor.container setRootViewStatusBarHidden:YES];
		AppDelegateAccessor.container.rootViewStatusBarUpdateAnimation = UIStatusBarAnimationFade;
		[UIApplication sharedApplication].statusBarHidden = YES;;

		//statusBar.isHidden = true
		UIView *statusBarView = [[UIApplication sharedApplication] valueForKey:@"statusBar"];
		statusBarView.hidden = YES;
		[UIView
			animateWithDuration:0.35
			animations:^{
				self.view.frame = self.view.window.bounds;
			}];
		//[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
		
		[[NSNotificationCenter defaultCenter]
			addObserver:self
			selector:@selector(deviceRotated:)
			name:UIDeviceOrientationDidChangeNotification
			object:[UIDevice currentDevice]];
	}
	
	[self deviceRotated:nil];
}

- (void)dismissFullscreenView
{
	if (proxyView)
	{
		[self toggleZoom:nil];
	}
}

- (void)setView:(UIView *)newView
{
	if (view)
	{
		[self toggleZoom:nil];
		[view removeGestureRecognizer:singleTapGestureRecognizer];
		[singleTapGestureRecognizer release];
		singleTapGestureRecognizer = nil;
	}
	
	[view autorelease];
	view = [newView retain];
	
	singleTapGestureRecognizer =
		[[UITapGestureRecognizer alloc]
			initWithTarget:self action:@selector(toggleZoom:)];
	singleTapGestureRecognizer.numberOfTapsRequired = 2;
	singleTapGestureRecognizer.delegate = self;
	[self.view addGestureRecognizer:singleTapGestureRecognizer];
}

- (void)dealloc
{
	[proxyView removeFromSuperview];
	[proxyView release];
	proxyView = nil;
	
	[singleTapGestureRecognizer release];
	singleTapGestureRecognizer = nil;

	[view release];
	view = nil;

	[proxyView release];
	proxyView = nil;

	[super dealloc];
}
#pragma mark - UITapGestureRecognizedDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
	return YES;
}
@end

