//
//  EditWatchlistVC.h
//  TCiPad
//
//  Created by Kaka on 8/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@class TCBaseButton;
@class RSSwitch;
@class WatchlistModel;
@class TCBaseView;

@protocol EditWatchlistVCDelegate <NSObject>
@optional
- (void)didCancelCreatingWatchlist;
- (void)didFinishEditting:(BOOL)isEditting watchlist:(WatchlistModel *)model;
@end
@interface EditWatchlistVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *lblInsWatchlistName;
@property (weak, nonatomic) IBOutlet UILabel *lblSetAppDefault;
@property (weak, nonatomic) IBOutlet UILabel *lblCountNumberChars;

@property (weak, nonatomic) IBOutlet UITextField *tfName;

@property (weak, nonatomic) IBOutlet UISwitch *switchControl;
@property (weak, nonatomic) IBOutlet TCBaseView *inputView;

@property (weak, nonatomic) IBOutlet TCBaseButton *btnClose;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnCreate;

//Passing data
@property (assign, nonatomic) BOOL isEdditing;
@property (strong, nonatomic) WatchlistModel *curWatchlist;

@property (strong, nonatomic) NSArray *watchlistArr;
//@property (strong, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) id <EditWatchlistVCDelegate> delegate;
@end
