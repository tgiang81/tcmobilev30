//
//  EditWatchlistVC.m
//  TCiPad
//
//  Created by Kaka on 8/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "EditWatchlistVC.h"
#import "TCBaseButton.h"
#import "RSSwitch.h"
#import "WatchlistModel.h"
#import "NSString+Watchlist.h"
#import "ATPAuthenticate.h"
#import "UIImageView+TCUtil.h"
#import "UISwitch+TCUtils.h"
#import "TCBaseView.h"
#import "MZFormSheetController.h"
#import "LanguageKey.h"
#import "TCBubbleMessageView.h"
#import "WatchlistAPI.h"
#import "UIView+ShadowBorder.h"

#define MAXLENGTHNAME 25
@interface EditWatchlistVC ()<UITextFieldDelegate>{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation EditWatchlistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self createUI];
	[self loadData];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	//[_tfName becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI

- (void)createUI{
	self.view.backgroundColor = [UIColor clearColor];
	self.enableTapDismissKeyboard = YES;
	//self.enableTapDismissKeyboard = YES;
	[self setupFontAnColor];
	[self setupLanguage];
	
	[_switchControl setOn:NO];
	[_switchControl addTarget:self action:@selector(onSwitchValueChanged:) forControlEvents:UIControlEventTouchUpInside];
	//[_inputView makeBorderShadow];
	//Textfield
	_tfName.delegate = self;
	[self.tfName addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
	[self updateTheme:nil];
}

- (void)setupFontAnColor{
	//COLOR_FROM_HEX(0x364D59);
	
}
- (void)setupLanguage{
	_lblInsWatchlistName.text = [LanguageManager stringForKey:@"Name"];
	_lblSetAppDefault.text = [LanguageManager stringForKey:@"Set as Default Watchlist"];
	[_btnClose setTitle:[LanguageManager stringForKey:Cancel] forState:UIControlStateNormal];
	NSString *createTite = _isEdditing ? [LanguageManager stringForKey:Confirm] : [LanguageManager stringForKey:Confirm];
	[_btnCreate setTitle:createTite forState:UIControlStateNormal];
    _lblTitle.text=[LanguageManager stringForKey:Add_Watchlist];
    _tfName.placeholder =[LanguageManager stringForKey:Enter_here];
}

- (void)updateTheme:(NSNotification *)noti{
	NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	[_btnClose setTitleColor:TC_COLOR_FROM_HEX(mainTintColor) forState:UIControlStateNormal];
	_btnClose.borderColor = TC_COLOR_FROM_HEX(mainTintColor);
	_btnCreate.backgroundColor = TC_COLOR_FROM_HEX(mainTintColor);
	_lblCountNumberChars.textColor = TC_COLOR_FROM_HEX(mainTintColor);
	_switchControl.onTintColor = TC_COLOR_FROM_HEX(mainTintColor);
	/* +++ No need set now
	//Return
	_switchControl.onTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].switchOnColor);
	//[_switchControl setOffColor:[UIColor blackColor]];

	_contentView.backgroundColor = [UIColor clearColor];
	_lblInsWatchlistName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblSetAppDefault.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);	
	_lblCountNumberChars.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_tfName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_inputView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	//Button
	_btnClose.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].deleteColor);
	_btnCreate.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	 */
}
#pragma mark - LoadData
- (void)loadData{
	_lblTitle.text = [LanguageManager stringForKey:Add_Watchlist];
	if (_isEdditing) {
		_lblTitle.text = [LanguageManager stringForKey:Edit_Watchlist];
		_tfName.text = _curWatchlist.Name;
		[_switchControl setOn:[_curWatchlist.Name isDefaultWatchlist]];
	}
}

#pragma mark - ACTION

- (IBAction)onCloseAction:(id)sender {
	[self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
		//Do something here
		if (self->_delegate) {
			[self->_delegate didCancelCreatingWatchlist];
		}
	}];
	
	/*
	[self dismissViewControllerAnimated:YES completion:^{
		//Do something here
		if (self->_delegate) {
			[self->_delegate didCancelCreatingWatchlist];
		}
	}];
	*/
}

- (IBAction)onCreateAction:(id)sender {
	NSString *name = [Utils stringTrimmedForLeadingAndTrailingWhiteSpacesFromString:_tfName.text];
	//Check length of name
	if (name.length < 2) {
		[TCBubbleMessageView showErrorWindowBubbleMessage:Minimum_2_characters_for_Watchlist];
		return ;
	}
	if (!_isEdditing) {
		//Create new
		//Check existed name or not
		if ([self checkNewWatchlistName:name existedIn:_watchlistArr]) {
			[TCBubbleMessageView showErrorWindowBubbleMessage:The_name_has_already_been_used___Please_choose_a_different_name];
			return ;
		}
		if (_watchlistArr.count >= 15) {
			[TCBubbleMessageView showErrorWindowBubbleMessage:The_name_has_already_been_used___Please_choose_a_different_name];
			return ;
		}
		//Call API add new watchlist
		NSInteger newWatchlistID = (_watchlistArr.count == 0) ? 1 : ([(WatchlistModel *)_watchlistArr.lastObject FavID] + 1);
		[[Utils shareInstance] showWindowHudWithTitle:[LanguageManager stringForKey:@"Creating..."]];
		[[WatchlistAPI shared] addOrUpdateWatchlist:newWatchlistID name:name completion:^(id result, NSError *error) {
			if (!error) {
				WatchlistModel *createModel = [[WatchlistModel alloc] init];
				createModel.Name = name;
				createModel.FavID = newWatchlistID;
				//Handle success here
				if (self.switchControl.isOn == YES) {
					[name saveAsDefaultWatchlist];
				}
				
				[self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
					if (self.delegate) {
						[self.delegate didFinishEditting:self->_isEdditing watchlist:createModel];
					}
				}];
			}else{
				[self showCustomAlertWithMessage:error.localizedDescription];
			}
		}];
	}else{
		//Check existed name or not
		if ([self checkNewWatchlistName:name existedIn:_watchlistArr] && [name isDefaultWatchlist] == _switchControl.isOn) {
			[TCBubbleMessageView showErrorWindowBubbleMessage:Nothing_changed__Unable_to_update_this_watchlist];
			return ;
		}
	
		//Call api
		[[Utils shareInstance] showWindowHudWithTitle:[LanguageManager stringForKey:@"Updating..."]];
		[[WatchlistAPI shared] addOrUpdateWatchlist:_curWatchlist.FavID name:name completion:^(id result, NSError *error) {
			if (!error) {
				if (self->_switchControl.isOn == YES) {
					[name saveAsDefaultWatchlist];
				}
				//Call back here
				[self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
					if (self->_delegate) {
						self->_curWatchlist.Name = name;
						[self->_delegate didFinishEditting:self->_isEdditing watchlist:self->_curWatchlist];
					}
				}];
			}else{
				[TCBubbleMessageView showErrorWindowBubbleMessage:error.localizedDescription];
			}
		}];
	}
}

- (IBAction)onSwitchValueChanged:(UISwitch *)sender {
	//Do nothings now
}

#pragma mark - UTILS
//===== Check isExisted Name from Watchlist array ====
- (BOOL)checkNewWatchlistName:(NSString *)name existedIn:(NSArray <WatchlistModel *>*)watchlistArr{
	BOOL isExisted = NO;
	for (int i = 0; i < watchlistArr.count; i ++) {
		WatchlistModel *model = watchlistArr[i];
		if ([model.Name isEqualToString:name]) {
			isExisted = YES;
			break;
		}
	}
	return isExisted;
}

#pragma mark - TextfieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	if (textField.text.length >= MAXLENGTHNAME && range.length == 0) {
		return NO; // Change not allowed
	}
	return YES;
}
- (void)textFieldValueChanged:(UITextField *)textfield{
	_lblCountNumberChars.text = [NSString stringWithFormat:@"%@/%d", @(textfield.text.length), MAXLENGTHNAME];
}
@end
