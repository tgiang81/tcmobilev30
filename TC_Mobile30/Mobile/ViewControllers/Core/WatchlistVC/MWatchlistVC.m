//
//  MWatchlistVC.m
//  TCiPad
//
//  Created by Kaka on 8/9/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MWatchlistVC.h"
#import "WatchlistCell.h"
#import "EditWatchlistVC.h"

#import "WatchlistModel.h"
//Control
#import "ATPAuthenticate.h"
#import "DetailWatchlistVC.h"
#import "NSString+Watchlist.h"
#import "SVPullToRefresh.h"
#import "LanguageKey.h"
#import "TCStatusInfoView.h"

#import "WatchlistAPI.h"
#import "TCBubbleMessageView.h"

static CGFloat kHEIGHT_WATCHLIST_CELL = 50;
@interface MWatchlistVC ()<UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate, EditWatchlistVCDelegate>{
	//Data
	NSMutableArray *_watchlistArr;
    UIView *statusInfoView;
}
@property (assign, nonatomic) BOOL isFirstShow;
@end

@implementation MWatchlistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_watchlistArr = @[].mutableCopy;
	_isFirstShow = YES;
	[self createUI];
	[self loadData];
}

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	//NavigationBar
	self.title = [LanguageManager stringForKey:_Watchlist];
	[self createLeftMenu];
	
	[self createCustomBar:BarItemType_Right icon:@"main_plus_icon" selector:@selector(addWatchlistAction:)];
	//TableView
	[_tblContent registerNib:[UINib nibWithNibName:[WatchlistCell nibName] bundle:nil] forCellReuseIdentifier:[WatchlistCell reuseIdentifier]];
	//None separated line
	//[_tblContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];

	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	[_tblContent setAllowsMultipleSelection:NO];
	
	//AddPullToRefresh
	//+++ Demo using pull refresh
	/*
	__weak MWatchlistVC *weakself = self;
	[_tblContent addPullToRefreshWithActionHandler:^{
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
			//Post Notification here
			[weakself.tblContent stopAnimating];
		});
	}];
	UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblContent.frame.size.width, 44)];
	UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	indicatorView.center = customView.center;
	indicatorView.hidesWhenStopped = NO;
	[customView addSubview:indicatorView];
	indicatorView.color = [UIColor redColor];
	customView.backgroundColor = [UIColor clearColor];
	
	//[[self.tblContent pullToRefreshView] setCustomView:customView forState:SVPullToRefreshStateTriggered];
	[indicatorView startAnimating];
	[[self.tblContent pullToRefreshView] setCustomView:customView forState:SVPullToRefreshStateAll];
	*/
}
- (void)removeWatchList:(WatchlistModel *)model{
    for(int i = 0; i < _watchlistArr.count; i++){
        WatchlistModel *watchList = _watchlistArr[i];
        if(watchList.FavID == model.FavID){
            [_watchlistArr removeObjectAtIndex:i];
            return;
        }
    }
    NSLog(@"");
}
- (void)addWatchList:(WatchlistModel *)model{
    [_watchlistArr addObject:model];
}
- (void)editWatchList:(WatchlistModel *)model{
    for(int i = 0; i < _watchlistArr.count; i++){
        WatchlistModel *watchList = _watchlistArr[i];
        if(watchList.FavID == model.FavID){
            [_watchlistArr replaceObjectAtIndex:i withObject:model];
            return;
        }
    }
}
- (void)reloadTableView{
    [_tblContent reloadData];
}
- (NSMutableArray *)getListWatchList{
    return _watchlistArr;
}
#pragma mark - LoadData
- (void)loadData{
	[self.view showPrivateHUD];
	[self.view removeStatusViewIfNeeded];
	[[WatchlistAPI shared] getWatchlist:^(id result, NSError *error) {
		[self.view dismissPrivateHUD];
		if(!error) {
			self->_watchlistArr = [WatchlistModel arrayFromDicts:result];
			[self->_tblContent reloadData];
			//Check show status Data
			[self showStatusData:(self->_watchlistArr.count == 0) message:nil];
            [self didLoadWatchlist:self->_watchlistArr];
			
			//For First time: Show detail of DEFAULT WL
			/*+++ TMP No use now
            if (self.isFirstShow && self.isDashBoard == NO && self->_watchlistArr.count > 0) {
				DetailWatchlistVC *_detailVC = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [DetailWatchlistVC storyboardID]);
				NSString *defaultWL = [NSString getDefaultWatchlist];
				NSInteger index = [self indexOfDefaultWatchlist:defaultWL];
				_detailVC.watchlist = self->_watchlistArr[index]; //Remove later
				_detailVC.curWatchlistIndex = index;
				_detailVC.watchlistArr = self->_watchlistArr;
				[self nextTo:_detailVC animate:NO];
			}
			*/
		}else{
			//Show error and restry action
			__weak MWatchlistVC *_weakSelf = self;
			[self.view showStatusViewWithMessage:error.localizedDescription retryAction:^{
				[_weakSelf loadData];
			}];
		}
	}];
}
- (void)didLoadWatchlist:(NSMutableArray *)watchList{
    NSLog(@"didLoadWatchlist");
}
- (WatchlistModel *)getWatchListAt:(NSInteger)index{
    return _watchlistArr[index];
}
#pragma mark - UTILS
- (NSInteger)indexOfDefaultWatchlist:(NSString *)defaultWL{
	if (!defaultWL) {
		return 0;
	}
	__block NSInteger index = 0;
	[_watchlistArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		if ([defaultWL isEqualToString:[(WatchlistModel *)obj Name]]){
			index = idx;
			*stop = YES;
		}
	}];
	return index;
}
- (void)showStatusData:(BOOL)shouldShow message:(NSString *)message{
	_tblContent.hidden = shouldShow;
    if (_tblContent) {// only show statusInfoView in self Watchlist screen, not in Dashboard:Watchlist
        [statusInfoView removeFromSuperview];
        CGRect tempRect = CGRectMake((self.view.frame.size.width/2 - 300/2), (self.view.frame.size.height/2 - 300/2), 300, 300);
        statusInfoView = [[UIView alloc] initWithFrame:tempRect];
        TCStatusInfoView *tcStatusInfoView = [[TCStatusInfoView alloc] init];
        [self.view addSubview:statusInfoView];
        [tcStatusInfoView justShowStatus:No_Data inView:statusInfoView];
        statusInfoView.hidden = !shouldShow;
    }
}
#pragma mark - ACTIONS
- (void)addWatchlistAction:(id)sender{
	DLog(@"Add or Edit watchlist");
	EditWatchlistVC *_editVC = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [EditWatchlistVC storyboardID]);
	[_editVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
	_editVC.definesPresentationContext = YES;
	_editVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	//Passing data
	_editVC.delegate = self;
	_editVC.isEdditing = NO;
	_editVC.watchlistArr = [_watchlistArr copy];

	[self showPopupWithContent:_editVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 250) showInCenter:YES topInsect:80 completion:^(MZFormSheetController *formSheetController) {
		//Do something here
	}];
	//290 for width
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_watchlistArr count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return kHEIGHT_WATCHLIST_CELL;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	WatchlistCell *cell = [tableView dequeueReusableCellWithIdentifier:[WatchlistCell reuseIdentifier]];
	if (!cell) {
		cell = [[WatchlistCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[WatchlistCell reuseIdentifier]];
	}
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	//Alway normal style for new design
	cell.delegate = self;
	cell.screenType = ScreenType_Normal;
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[cell setupDataFrom:_watchlistArr[indexPath.row] withType:ScreenType_Normal];
	return cell;
}

#pragma mark - MGSWipeTableViewCellDelegate
/**
 * Delegate method to enable/disable swipe gestures
 * @return YES if swipe is allowed
 **/
- (BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction{
	if (direction == MGSwipeDirectionRightToLeft) {
		return YES;
	}
	return NO;
}

/**
 * Delegate method to setup the swipe buttons and swipe/expansion settings
 * Buttons can be any kind of UIView but it's recommended to use the convenience MGSwipeButton class
 * Setting up buttons with this delegate instead of using cell properties improves memory usage because buttons are only created in demand
 * @param swipeTableCell the UITableVieCel to configure. You can get the indexPath using [tableView indexPathForCell:cell]
 * @param direction The swipe direction (left to right or right to left)
 * @param swipeSettings instance to configure the swipe transition and setting (optional)
 * @param expansionSettings instance to configure button expansions (optional)
 * @return Buttons array
 **/
- (NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
			  swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings{
	static NSString *emptyTitle = @"";
	if (direction == MGSwipeDirectionRightToLeft) {
		swipeSettings.transition = MGSwipeTransitionStatic;
		swipeSettings.bottomMargin = 0;
		swipeSettings.topMargin = 0;
		expansionSettings.fillOnTrigger = NO;
		//Create Button
		MGSwipeButton *delete = [MGSwipeButton buttonWithTitle:emptyTitle icon:[UIImage imageNamed:@"delete_cell_icon"] backgroundColor:RGB(239, 116, 116) callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
			//Do something here
			DLog(@"+++Delete here");
			[self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:Are_you_sure_you_want_to_delete_this_watchlist] withOKAction:^{
				NSIndexPath *indexPath = [self->_tblContent indexPathForCell:cell];
				WatchlistModel *model = self->_watchlistArr[indexPath.row];
				__weak MWatchlistVC *weakSelf = self;
				[[WatchlistAPI shared] deleteWatchlist:model.FavID completion:^(id result, NSError *error) {
					if (error) {
						[self showCustomAlertWithMessage:error.localizedDescription];
						return;
					}
					//Succes + handle UI
                    [self removeWatchList:model];
                    [self postNotification:@"didDeleteWatchList" object:model];
					[self->_tblContent beginUpdates];
					[self->_tblContent deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
					[self->_tblContent endUpdates];
				}];
			} cancelAction:^{
				//Do nothing
			}];
			return YES;
		}];
		delete.buttonWidth = kHEIGHT_WATCHLIST_CELL;
		MGSwipeButton *edit = [MGSwipeButton buttonWithTitle:emptyTitle icon:[UIImage imageNamed:@"edit_cell_icon"] backgroundColor:RGB(111, 188, 252) callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
			//Do something here
			DLog(@"+++Edit here");
			NSIndexPath *indexPath = [self->_tblContent indexPathForCell:cell];
			WatchlistModel *model = self->_watchlistArr[indexPath.row];
			EditWatchlistVC *_editVC = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [EditWatchlistVC storyboardID]);
			[_editVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
			_editVC.definesPresentationContext = YES;
			_editVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
			//Passing data
			_editVC.delegate = self;
			_editVC.isEdditing = YES;
			_editVC.curWatchlist = model;
			_editVC.watchlistArr = self->_watchlistArr;
			[self showPopupWithContent:_editVC inSize:CGSizeMake(290, 250) showInCenter:NO topInsect:80 completion:^(MZFormSheetController *formSheetController) {
				//Do something here
			}];
			return YES;
		}];
		edit.buttonWidth = kHEIGHT_WATCHLIST_CELL;
		return @[delete, edit];
	}
	return nil;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DetailWatchlistVC *_detailVC = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [DetailWatchlistVC storyboardID]);
	_detailVC.watchlist = _watchlistArr[indexPath.row]; //Remove later
	_detailVC.curWatchlistIndex = indexPath.row;
	_detailVC.watchlistArr = _watchlistArr;
	[self nextTo:_detailVC animate:YES];
}

#pragma mark - EditWatchlistVCDelegate
- (void)didCancelCreatingWatchlist{
	//Do nothing now
	//_tblContent.hidden = NO;
}

- (void)didFinishEditting:(BOOL)isEditting watchlist:(WatchlistModel *)model{
	_tblContent.hidden = NO;
	if (isEditting) {
        [self editWatchList:model];
        [self postNotification:@"didEditWatchList" object:model];
		[_tblContent reloadData]; //Or use reload row at indexPath...
	}else{
        [self addWatchList:model];
        [self postNotification:@"didAddWatchList" object:model];
		NSIndexPath *insertIndexPath = [NSIndexPath indexPathForRow:(_watchlistArr.count - 1) inSection:0];
		[_tblContent beginUpdates];
		[_tblContent insertRowsAtIndexPaths:@[insertIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		[_tblContent endUpdates];
	}
    [self showStatusData:(self->_watchlistArr.count == 0) message:nil];
    [self didLoadWatchlist:_watchlistArr];
}

- (void)updateTheme:(NSNotification *)noti{
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    _tblContent.backgroundColor = [UIColor clearColor];
	[_tblContent reloadData];
}
@end
