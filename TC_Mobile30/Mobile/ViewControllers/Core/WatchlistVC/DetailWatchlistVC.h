//
//  DetailWatchlistVC.h
//  TCiPad
//
//  Created by Kaka on 6/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@class WatchlistModel;
@interface DetailWatchlistVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
//Passing data
@property (strong, nonatomic) WatchlistModel *watchlist;//Should remove later

@property (assign, nonatomic) NSInteger curWatchlistIndex;
@property (strong, nonatomic) NSArray *watchlistArr;
@property (assign, nonatomic) BOOL isDashBoard;
- (void)loadData;
@end
