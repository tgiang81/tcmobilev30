//
//  MWatchlistVC.h
//  TCiPad
//
//  Created by Kaka on 8/9/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "WatchlistModel.h"
@interface MWatchlistVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (assign, nonatomic) BOOL isDashBoard;
- (void)didLoadWatchlist:(NSMutableArray *)watchList;
- (WatchlistModel *)getWatchListAt:(NSInteger)index;
- (void)addWatchlistAction:(id)sender;
- (void)loadData;
- (void)removeWatchList:(WatchlistModel *)model;
- (void)addWatchList:(WatchlistModel *)model;
- (void)editWatchList:(WatchlistModel *)model;
- (void)reloadTableView;
- (NSMutableArray *)getListWatchList;
@end
