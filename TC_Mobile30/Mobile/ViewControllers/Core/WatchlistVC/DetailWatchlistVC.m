//
//  DetailWatchlistVC.m
//  TCiPad
//
//  Created by Kaka on 6/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DetailWatchlistVC.h"
#import "StockTypeSingleCell.h"
#import "ATPAuthenticate.h"
#import "QCData.h"

#import "StockModel.h"
#import "WatchlistModel.h"
#import "StockInfoVC.h"

#import "SearchStockVC.h"
#import "ATPAuthenticate.h"
#import "MSearchVC.h"

#import "UIView+Animation.h"
#import "UIButton+TCUtils.h"
#import "NSString+SizeOfString.h"
#import "UIImageView+TCUtil.h"
#import "SVPullToRefresh.h"
#import "LanguageKey.h"
#import "GHContextMenuView.h"
#import "MTradeViewController.h"
#import "BaseNavigationController.h"
#import "MStockInfoVC.h"
#import "WatchlistAPI.h"
#import "TCBubbleMessageView.h"

#define kFULL_CELL_HEIGHT	90
@interface DetailWatchlistVC ()<UITableViewDataSource, UITableViewDelegate, StockTypeSingleCellDelegate, MGSwipeTableCellDelegate, MSearchVCDelegate, GHContextOverlayViewDataSource, GHContextOverlayViewDelegate>{
	//Cell
	NSMutableSet *_expandedCells;
    UIRefreshControl *refreshControl;
}


//OUTLETS
@property (strong, atomic) NSMutableArray *items;
@property (weak, nonatomic) IBOutlet UIView *topControlView;
@property (weak, nonatomic) IBOutlet UIView *instructionView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *lblWatchlistName;
@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UILabel *lblIns_Name;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Last;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Change;

//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLabelIns_Change_Constrain;

//Control
@property (strong, nonatomic) MSearchVC *mySearchVC;
@end

@implementation DetailWatchlistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_items = @[].mutableCopy;
	_expandedCells = @[].mutableCopy;

	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	//BarItem
	[self setBackButtonDefault];
	self.title = _watchlist.Name;
	[self setupLanguage];
    [self createCustomBar:BarItemType_Right icon:@"plus_icon" selector:@selector(addWatchlistItemAction:)];

	[_tblContent registerNib:[UINib nibWithNibName:[StockTypeSingleCell nibName] bundle:nil] forCellReuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	[_tblContent setAllowsMultipleSelection:NO];
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //add pull to refresh data at tableView
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:[LanguageManager stringForKey:Refreshing_data______] attributes:attrsDictionary];
    refreshControl.attributedTitle = attributedTitle;
    if (@available(iOS 10.0, *)) {
        self.tblContent.refreshControl = refreshControl;
    } else {
        // Fallback on earlier versions
        [self.tblContent addSubview:refreshControl];
    }
    
	//UI First
	[self showStatusData:NO message:nil];
	[self setupLanguage];
	[self setupFontAnColor];
	[self updateTheme:nil];
	[self setupContextMenu];
}

- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:_Watchlist];
    	self.lblIns_Name.text=[LanguageManager stringForKey:_Name];
    	self.lblIns_Last.text=[LanguageManager stringForKey:Last];
	_lblIns_Change.text = [LanguageManager stringForKey:@"STATS"];
}

- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	
	_instructionView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subHeaderBg);
	_lblIns_Name.textColor =  TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subTintIconColor);
	_lblIns_Last.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subTintIconColor);
	_lblIns_Change.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subTintIconColor);
	
	_btnNext.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_btnPrevious.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblWatchlistName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	[self.tblContent reloadData];
}
- (void)setupContextMenu{
	//Add Context Menu
	GHContextMenuView* overlay = [[GHContextMenuView alloc] init];
	overlay.dataSource = self;
	overlay.delegate = self;
	
	UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];
	[self.tblContent addGestureRecognizer:_longPressRecognizer];
}
/*
- (void)setupSearchView{
	if (!_searchVC) {
		_searchVC = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [SearchStockVC storyboardID]);
	}
	_searchVC.view.frame = self.view.bounds;
	[self addChildViewController:_searchVC];
	[self.view addSubview:_searchVC.view];
	[_searchVC didMoveToParentViewController:self];
}

- (void)shouldShowSearchStock:(BOOL)isShow{
	if (isShow) {
		if (!_searchVC) {
			_searchVC = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [SearchStockVC storyboardID]);
			_searchVC.view.frame = self.view.bounds;
			[self addChildViewController:_searchVC];
			[self.view addSubview:_searchVC.view];
			[_searchVC didMoveToParentViewController:self];
		}
		__weak DetailWatchlistVC *weakSelf = self;
		_searchVC.myBlock = ^(NSDictionary * _Nullable dic) {
			DLog(@"+++ Model: %@", dic);
			if (dic) {
				id result = dic[@"key"];
				StockModel *stock = nil;
				if (![result isKindOfClass:[StockModel class]]) {
					[weakSelf shouldShowSearchStock:NO];
					return ;
				}
				stock = result;
				if (stock) {
					//Call api to add watchlist here
					[[ATPAuthenticate singleton] addStock:stock.stockCode toWatchlist:(int)weakSelf.watchlist.FavID completion:^(BOOL success, id result, NSError *error) {
						if (success) {
							NSString *someDuplicate = [result objectForKey:@"DuplicateStocks"];
							if ([someDuplicate length]>0) {
								[[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"SomeStockAlreadyAddedError" withPlaceholders:@{@"%stkCode%":someDuplicate}]
															 inController:weakSelf withTitle:[LanguageManager stringForKey:@"Done"]];
								[weakSelf showCustomAlertWithMessage:[LanguageManager stringForKey:@"SomeStockAlreadyAddedError" withPlaceholders:@{@"%stkCode%":someDuplicate}]];
							}else{
								//Success
								[weakSelf postNotification:kDidFinishAddStockToWatchlistNotification object:nil];
								[weakSelf showCustomAlertWithMessage:[LanguageManager stringForKey:@"Stock has been successfully added!"]];
								//--- reload data ---
								[_items insertObject:stock atIndex:0];
								NSIndexPath *insertIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
								[weakSelf.tblContent beginUpdates];
								[weakSelf.tblContent insertRowsAtIndexPaths:@[insertIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
								[weakSelf.tblContent endUpdates];
								//[weakSelf loadData];
								//Hide search view
								[weakSelf shouldShowSearchStock:NO];
							}
						}else{
							if (error) {
								[weakSelf showErrorCustomAlertWithMessage:error.localizedDescription];
							}
						}
					}];
				}
			}
		};
	}else{
		if (_searchVC) {
			[_searchVC removeFromParentViewController];
			[_searchVC.view removeFromSuperview];
			_searchVC = nil;
		}
	}
}
*/
#pragma mark - ACTIONS
- (void)addWatchlistItemAction:(id)sender{
	[self createCustomBar:BarItemType_Right title:[LanguageManager stringForKey:Cancel] selector:@selector(onCancelAdding:)];
	if (!_mySearchVC) {
		_mySearchVC = NEW_VC_FROM_STORYBOARD(kMSearchStoryboardName, [MSearchVC storyboardID]);
	}
	self.title = [LanguageManager stringForKey:@"Add Stock"];
	_mySearchVC.delegate = self;
	_mySearchVC.searchType = MSearchType_Selected;
	_mySearchVC.view.frame = self.view.bounds;
	_mySearchVC.view.alpha = 0.3;
	[self.view addSubview:self.mySearchVC.view];
	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
		self.mySearchVC.view.alpha = 1.0;
	} completion:^(BOOL finished) {
		[self addChildViewController:self.mySearchVC];
		[self didMoveToParentViewController:self.mySearchVC];
	}];
}
- (void)onCancelAdding:(id)sender{
	self.title = [LanguageManager stringForKey:WatchList];
	[self createCustomBar:BarItemType_Right icon:@"plus_icon" selector:@selector(addWatchlistItemAction:)];
	[UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
		self.mySearchVC.view.alpha = 0.3;
	} completion:^(BOOL finished) {
		[self.mySearchVC.view removeFromSuperview];
		[self.mySearchVC removeFromParentViewController];
		self.mySearchVC = nil;
	}];
}

- (IBAction)onPreviousAction:(id)sender {
	-- _curWatchlistIndex;
	[self loadData];
}

- (IBAction)onNextAction:(id)sender {
	++ _curWatchlistIndex;
	[self loadData];
}

- (IBAction)onSwitchChangeValueAction:(id)sender {
	[SettingManager shareInstance].isVisibleChangePer = ![SettingManager shareInstance].isVisibleChangePer;
	[self setVisibleChangePer:[SettingManager shareInstance].isVisibleChangePer];
	[[SettingManager shareInstance] save];
	[self.tblContent reloadData];
}

#pragma mark - LoadData
- (void)loadData{
	[self updateDataFromWatchlistIndex:_curWatchlistIndex];
	[self startLoadingContentWatchlistAt:_curWatchlistIndex];
}

- (void)refreshData{
    [self loadData];
    [self.view dismissPrivateHUD];
    [self.contentView dismissPrivateHUD];
}
- (void)startLoadingContentWatchlistAt:(NSInteger)index{
	//Clear data
	[_expandedCells removeAllObjects];
	[_items removeAllObjects];
	[self.tblContent reloadData];
	WatchlistModel *model = _watchlistArr[index];
    _watchlist = model;
	[self.contentView showPrivateHUD];
	[self.contentView removeStatusViewIfNeeded];
	int favID = (int) model.FavID;
	//+++For cache: Maybe not use later
	[UserPrefConstants singleton].prevWatchlistFavID = favID;
	[UserPrefConstants singleton].watchListName = _watchlist.Name;
	[UserPrefConstants singleton].quoteScreenFavID = [NSNumber numberWithInt:favID];
	//+++ end Cache
	[self.contentView removeStatusViewIfNeeded];
	//Use new class
	//[[ATPAuthenticate singleton] getWatchListItems:favID];
	[[WatchlistAPI shared] getWatchlistItemFrom:model.FavID completion:^(id result, NSError *error) {
		if (!error) {
			//API continue call detail of info item from VERTX
			//Call Vertx to get Watchlist
			[UserPrefConstants singleton].userWatchListStockCodeArr = [result mutableCopy];
			 [[VertxConnectionManager singleton]vertxGetStockDetailWithStockArr:result FIDArr:[[UserPrefConstants singleton]userQuoteScreenColumn] tag:0];
		}else{
			[self showCustomAlertWithMessage:error.localizedDescription];
		}
	}];
}

#pragma mark - Notification
//Add Notification
- (void)registerNotification{
	//Update new data
	[self addNotification:kVertxNo_didReceivedWatchListItems selector:@selector(didReciveNotification:)];
	[self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
	[self addNotification:@"pnWebSocketDisconnected" selector:@selector(didReceiveErrorSocketNotification:)];
	[self addNotification:kDidErrorLoadingContentOfWatchlist selector:@selector(didErrorLoadingWatchlist:)];
	//Swap value
	[self addNotification:kSwitchValueChangedNotification selector:@selector(shouldSwitchValueChangedNotification:)];
}
#pragma mark - UTILS
- (void)showStatusData:(BOOL)shouldShow message:(NSString *)message{
	if (!message) {
		message = [LanguageManager stringForKey:There_is_no_data];
	}
	/*
	_tblContent.hidden = shouldShow;
	_lblStatus.hidden = !shouldShow;
	_lblStatus.text = message;
	*/
	if (shouldShow) {
		[self.contentView showStatusMessage:message];
	}else{
		[self.contentView removeStatusViewIfNeeded];
	}
}

//For Ins Change Label
- (void)setVisibleChangePer:(BOOL)isShow{
	/*
	if (isShow) {
		_lblIns_Change.text = [LanguageManager stringForKey:@"Change%"];
	}else{
		_lblIns_Change.text = [LanguageManager stringForKey:@"Change"];
	}
	 */
	//float updatedWidth = [_lblIns_Change.text widthOfString:_lblIns_Change.font];
	//_widthLabelIns_Change_Constrain.constant = updatedWidth + 2; //4 for Alignment
	//[self.view layoutIfNeeded];
	_lblIns_Change.text = [LanguageManager stringForKey:@"STATS"];
}

- (void)updateDataFromWatchlistIndex:(NSInteger)index{
	[_btnPrevious shouldEnable:(index != 0)];
	[_btnNext shouldEnable:(index != (_watchlistArr.count - 1))];
	WatchlistModel *model = _watchlistArr[index];
	_lblWatchlistName.text = model.Name;
}

//API: Adding stock to watchlist
- (void)addingStock:(StockModel *)stock toWatchlist:(WatchlistModel *)watchlist{
	//Call api to add watchlist here
	[self.contentView showPrivateHUD];
	[self.contentView removeStatusViewIfNeeded];
//	[[WatchlistAPI shared] addStock:stock.stockCode inWatchlist:watchlist.FavID completion:^(id result, NSError *error) {
//		if (error) {
//			[self.contentView showStatusViewWithMessage:error.localizedDescription retryAction:^{
//				//Do nothing...
//			}];
//		}else{
//			NSArray *dubplicatedArr = result[@"data"];
//			if (dubplicatedArr.count > 0) {
//				[self showCustomAlertWithMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlist.Name}]];
//			}else{
//				DLog(@"+++ Add Stock To Watchlist Result: %@", result);
//				//Just add stock to array
//				[self.items addObject:stock];
//				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.items.count - 1) inSection:0];
//				[self.tblContent beginUpdates];
//				[self.tblContent
//				 insertRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationTop];
//				[self.tblContent endUpdates];
//				[self postNotification:@"reloadWatchListAt" object:[NSNumber numberWithInteger:self->_curWatchlistIndex]];
//			}
//		}
//	}];
	[[ATPAuthenticate singleton] addStock:stock.stockCode toWatchlist:(int)watchlist.FavID completion:^(BOOL success, id result, NSError *error) {
		[self.contentView dismissPrivateHUD];
		if (success) {
			NSString *someDuplicate = [result objectForKey:@"DuplicateStocks"];
			if ([someDuplicate length]>0) {
				[self showCustomAlertWithMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlist.Name}]];
			}else{
				//Success
				//Just add stock to array
				[self.items addObject:stock];
				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.items.count - 1) inSection:0];
				[self.tblContent beginUpdates];
				[self.tblContent
				 insertRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationTop];
				[self.tblContent endUpdates];
			}
			[self postNotification:@"reloadWatchListAt" object:[NSNumber numberWithInteger:self->_curWatchlistIndex]];
		}else{
			if (error) {
				__weak DetailWatchlistVC *weakSelf = self;
				[self.contentView showStatusViewWithMessage:error.localizedDescription retryAction:^{
					[weakSelf addingStock:stock toWatchlist:watchlist];
				}];
			}
		}
	}];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_items count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return [_expandedCells containsObject:indexPath] ? kEXPANDED_CELL_HEIGHT : kNORMAL_CELL_HEIGHT;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	StockTypeSingleCell *cell = [tableView dequeueReusableCellWithIdentifier:[StockTypeSingleCell reuseIdentifier]];
	if (!cell) {
		cell = [[StockTypeSingleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	[tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
	cell.delegate = self;
	cell.mainDelegate = self;
	cell.indexPath = indexPath;
	cell.isExpandedCell = [_expandedCells containsObject:indexPath];
	cell.showIndex = NO;
	//Parse data to UI
	[cell updateData:_items[indexPath.row]];
	return cell;
}

#pragma mark - MGSWipeTableViewCellDelegate
/**
 * Delegate method to enable/disable swipe gestures
 * @return YES if swipe is allowed
 **/
- (BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction{
	if (direction == MGSwipeDirectionRightToLeft) {
		return YES;
	}
	return NO;
}

/**
 * Delegate method to setup the swipe buttons and swipe/expansion settings
 * Buttons can be any kind of UIView but it's recommended to use the convenience MGSwipeButton class
 * Setting up buttons with this delegate instead of using cell properties improves memory usage because buttons are only created in demand
 * @param swipeTableCell the UITableVieCel to configure. You can get the indexPath using [tableView indexPathForCell:cell]
 * @param direction The swipe direction (left to right or right to left)
 * @param swipeSettings instance to configure the swipe transition and setting (optional)
 * @param expansionSettings instance to configure button expansions (optional)
 * @return Buttons array
 **/
- (NSArray*)swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
			  swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings{
	static NSString *emptyTitle = @"";
	if (direction == MGSwipeDirectionRightToLeft) {
		swipeSettings.transition = MGSwipeTransitionDrag;
		swipeSettings.bottomMargin = 0;
		swipeSettings.topMargin = 0;
		expansionSettings.fillOnTrigger = YES;
		//Create Button
		__block NSIndexPath *indexPath = [self.tblContent indexPathForCell:cell];
		MGSwipeButton *delete = [MGSwipeButton buttonWithTitle:emptyTitle icon:[UIImage imageNamed:@"delete_cell_icon"] backgroundColor:RGB(239, 116, 116) callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
			//Do something here
			__weak DetailWatchlistVC *weakSelf = self;
			[self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:Are_you_sure_you_want_to_remove_this_stock] withOKAction:^{
				StockModel *stock = self.items[indexPath.row];
				[[ATPAuthenticate singleton] removeStock:stock.stockCode fromWatchlist:self->_watchlist.FavID completion:^(BOOL success, id result, NSError *error) {
					if (!success) {
						[weakSelf showErrorCustomAlertWithMessage:error.localizedDescription];
						return;
					}
					if ([self->_expandedCells containsObject:indexPath]) {
						[self->_expandedCells removeObject:indexPath];
					}
                    [self postNotification:@"reloadWatchListAt" object:[NSNumber numberWithInteger:self->_curWatchlistIndex]];
					[self.items removeObject:stock];
					[weakSelf.tblContent reloadData];
				}];
			} cancelAction:^{
				//Do nothing
			}];
			return YES;
		}];
		delete.buttonWidth = [_expandedCells containsObject:indexPath] ? kEXPANDED_CELL_HEIGHT : kNORMAL_CELL_HEIGHT;
		return @[delete];
	}
	return nil;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	// No use now
	/*
	DetailStockVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [DetailStockVC storyboardID]);
	_detailVC.stock = _items[indexPath.row];
	[self nextTo:_detailVC animate:YES];
	*/
}


#pragma mark - StockTypeSingleCellDelegate
- (void)didTapExpandCell:(StockTypeSingleCell *)cell{
	NSIndexPath *curIndexPath = cell.indexPath;
	//Save current Stock
	[UserPrefConstants singleton].userSelectingThisStock  =  _items[curIndexPath.row] ;
	
	BOOL shouldClose = [_expandedCells containsObject:curIndexPath];
	
//	if (shouldClose) {
//		[_expandedCells removeObject:curIndexPath];
//	}else{
//		[_expandedCells addObject:curIndexPath];
//	}
//	[_tblContent beginUpdates];
//	cell.isExpandedCell = !shouldClose;
//	[_tblContent endUpdates];
	
	 //+++OLD
	 if (shouldClose) {
	 [_expandedCells removeObject:curIndexPath];
	 }else{
	 [_expandedCells addObject:curIndexPath];
	 }
	 [_tblContent beginUpdates];
	 cell.isExpandedCell = !shouldClose;
	 [self.tblContent reloadRowsAtIndexPaths:@[curIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
	 [_tblContent endUpdates];
}

- (void)shouldGotoDetailFromCell:(StockTypeSingleCell *)cell{
	MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
	_detailVC.stock = _items[cell.indexPath.row];
	[self nextTo:_detailVC animate:YES];
}

- (void)shoudSwitchValue:(StockTypeSingleCell *)cell{
	[self onSwitchChangeValueAction:nil];
}

#pragma mark - MSearchVCDelegate
- (void)didSelectStock:(StockModel *)model{
	[self showCustomAlert:TC_Pro_Mobile  message: [LanguageManager stringForKey:Add_To_Watchlist] withOKAction:^{
		[self onCancelAdding:nil];
		//Call API for adding stock to watchlist
		WatchlistModel *watchlistMO = self.watchlistArr[self->_curWatchlistIndex];
		[self addingStock:model toWatchlist:watchlistMO];
	} cancelAction:^{
		//do nothing
	}];
}

#pragma mark - GHContextMenuDataSource
- (BOOL)shouldShowMenuAtPoint:(CGPoint)point{
	NSIndexPath* indexPath = [self.tblContent indexPathForRowAtPoint:point];
	StockTypeSingleCell *cell = [self.tblContent cellForRowAtIndexPath:indexPath];
	return cell != nil;
}
- (NSInteger) numberOfMenuItems{
	return [[AppConstants contextWatchlistMenu] count];
}
- (UIImage*)imageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextWatchlistMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}

- (UIImage*) highlightedImageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextWatchlistMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_hl_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_hl_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_hl_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_hl_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_hl_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}
#pragma mark - GHContextMenuDelegate
- (void)didSelectItemAtIndex:(NSInteger) selectedIndex forMenuAtPoint:(CGPoint)point{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[selectedIndex] integerValue];
	
	//Select Stock Model
	NSInteger index;
	NSIndexPath *indexPath = [self.tblContent indexPathForRowAtPoint:point];
	index = indexPath.row;
	StockModel *selectedStock = _items[index];
	switch (menuType) {
		case MContextMenu_Trading:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Buy:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Sell:{
			//Sell here
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Sell;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Info:{
			//Detail here
			MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
			_detailVC.stock = selectedStock;
			[self nextTo:_detailVC animate:YES];
		}
			break;
		case MContextMenu_Favorite:{
			//Do nothing here
		}
			break;
		default:
			break;
	}
}
#pragma mark - Notification
- (void)didErrorLoadingWatchlist:(NSNotification *)noti{
    if (self->refreshControl) {
        [self->refreshControl endRefreshing];
    }
	NSError *error = noti.object;
    NSString *msgError = [LanguageManager stringForKey:Watchlist_content_is_not_available_now___Please_try_again_later___] ;
	if (error) {
		msgError = error.localizedDescription;
	}
	[self.contentView showStatusMessage:msgError];
}
- (void)didReciveNotification:(NSNotification *)noti{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSString *_nameNotification = noti.name;
		if ([_nameNotification isEqualToString:kVertxNo_didReceivedWatchListItems]) {
			NSArray *wlistStkCodeArr = [[NSArray alloc]initWithArray:[UserPrefConstants singleton].userWatchListStockCodeArr copyItems:YES];
			[self.items removeAllObjects];
			DLog(@"+++ Watchlist Result: %@", wlistStkCodeArr);
			for (NSString *stkCode in wlistStkCodeArr)
			{
				if (([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
                    NSDictionary *tmpDict = [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode];
					StockModel *stock = [[StockModel alloc] initWithDictionary:tmpDict error:nil];
					if (stock) {
						[self.items addObject:stock];
					}
				}
			}
			//Reload data here
			dispatch_async(dispatch_get_main_queue(), ^{
				[self.contentView removeStatusViewIfNeeded];
				[self->_tblContent reloadData];
                if (self->refreshControl) {
                    [self->refreshControl endRefreshing];
                }
				[self showStatusData:(self.items.count == 0) message:nil];
				[Utils dismissApplicationHUD];
				[self.contentView dismissPrivateHUD];
			});
		}
	});
}

//Did Update newest stock
- (void)didUpdate:(NSNotification *)noti{
    [_tblContent stopAnimating];
	NSString *stockCode = noti.object;
	StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
	if (!changedStock) {
		return; //do nothing
	}
	
	//Update for TopRank
	NSInteger indexOfItems = [Utils indexOfStockByCode:stockCode fromArray:self.items];
	if (self.items.count > 0 && indexOfItems < self.items.count) {
		if (indexOfItems != -1) {
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfItems inSection:0];
			StockModel *oldStock = [self.items[indexOfItems] copy];
			StockTypeSingleCell *cell = [self->_tblContent cellForRowAtIndexPath:indexPath];
			if (cell) {
				[cell.lastView makeAnimateBy:oldStock newStock:changedStock completion:^{
					[cell updateData:changedStock];
				}];
			}
			[self.items replaceObjectAtIndex:indexOfItems withObject:changedStock];
		}
	}
}

//Error Notification
//Error Socket
- (void)didReceiveErrorSocketNotification:(NSNotification *)noti{
    if (self->refreshControl) {
        [self->refreshControl endRefreshing];
    }
	[[Utils shareInstance] dismissWindowHUD];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView dismissPrivateHUD];
}

//Switch Value Change or ChangeP
- (void)shouldSwitchValueChangedNotification:(NSNotification *)noti{
	[self.tblContent reloadData];
}
@end
