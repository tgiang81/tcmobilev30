//
//  SectorVC.h
//  TC_Mobile30
//
//  Created by Kaka on 12/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"



@protocol MSectorVCDelegate<NSObject>
@optional
- (void)didApplyFilter;
@end
@interface MSectorVC : BaseVC{
	
}
@property (weak, nonatomic) id <MSectorVCDelegate> delegate;
@end
