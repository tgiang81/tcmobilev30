//
//  SectorVC.m
//  TC_Mobile30
//
//  Created by Kaka on 12/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MSectorVC.h"
#import "SectorCell.h"
#import "SectorInfo.h"
#import "NSString+SizeOfString.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "SectorHeaderReusableView.h"
#import "LinqToObjectiveC.h"
#import "NSString+Util.h"
#import "LanguageKey.h"
#import "TCBaseButton.h"
#import "Utils.h"

#define kDEFULT_WIDTH_SECTOR_CELL	60
#define kDEFAULT_HEIGHT_SECTOR_CELL	30
#define kSECTOR_SPACING_ITEM		5
#define kSECTOR_SPACING_LINE		5
#define kHEIGHT_HEADER				44

//Sample KL exchange
typedef enum: NSInteger {
	MarketLotKey_All = 10,
	MarketLotKey_OddLot = 15,
	MarketLotKey_BuyIn	= 17,
	MarketLotKey_OddLotBuyIn = 18,
	MarketLotKey_OFFmarket = 19,
	MarketLotKey_Indices = 2000
} MarketLotKey;

typedef enum: NSInteger {
	HeaderType_Sector = 0,
	HeaderType_SubSector,
	HeaderType_Lot
} HeaderType;

@interface MSectorVC ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>{
	NSMutableArray *_headers;
	//Content
	NSMutableArray *_sectors;
	NSMutableArray *_subsector;
	NSMutableArray *_lots;
}

//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *vRemoveSector;
@property (weak, nonatomic) IBOutlet UILabel *lblRemoveWarrant;
@property (weak, nonatomic) IBOutlet UISwitch *warrantSwitch;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnReset;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnApply;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *colContent;

//Passing data
@property (strong, nonatomic) SectorInfo *selectedSector;
@property (strong, nonatomic) SectorInfo *selectedSubSector;
@property (strong, nonatomic) NSString *selectedLot;//Market Info
@property (assign, nonatomic) BOOL enableWarrant;
@end

@implementation MSectorVC

- (void)viewDidLoad {
    [super viewDidLoad];
	//Init
	_headers = @[].mutableCopy;
	_sectors = @[].mutableCopy;
	_subsector = @[].mutableCopy;
	_lots = @[].mutableCopy;
	[self createUI];
    [self setupLanguage];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	self.title = [LanguageManager stringForKey:Filter_By];
	[self setBackButtonDefault];
	//Col
	_colContent.delegate = self;
	_colContent.dataSource = self;
	UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
	_colContent.collectionViewLayout = layout;
	[_colContent registerNib:[UINib nibWithNibName:@"SectorHeaderReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SectorHeaderReusableView"];
	_colContent.allowsSelection = YES;
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	NSString *mainTint = [ThemeManager shareInstance].tintColor;
	[self.btnApply setBackgroundColor:TC_COLOR_FROM_HEX(mainTint)];
	[self.btnApply setTitleColor:TC_COLOR_FROM_HEX(@"FFFFFF") forState:UIControlStateNormal];
	[self.btnReset setTitleColor:TC_COLOR_FROM_HEX(mainTint) forState:UIControlStateNormal];
	self.btnReset.borderColor = TC_COLOR_FROM_HEX(mainTint);
	self.btnReset.backgroundColor = [UIColor clearColor];
	self.warrantSwitch.onTintColor = TC_COLOR_FROM_HEX(mainTint);
}
- (void)setupLanguage{
    _lblRemoveWarrant.text=[LanguageManager stringForKey:Remove_Warrant];
    [_btnReset setTitle:[LanguageManager stringForKey:Reset] forState:UIControlStateNormal];
    [_btnApply setTitle:[LanguageManager stringForKey:Apply] forState:UIControlStateNormal];
}
#pragma mark - LoadData
- (void)loadData{
	//Check selected Sector
	//Sector
	[self loadBeginingOfSelectionSector];
	if (_sectors.count > 0) {
		[_headers addObject:@(HeaderType_Sector)];
	}
	if (_subsector.count > 0) {
		[_headers addObject:@(HeaderType_SubSector)];
	}
	//Lot
	[self loadBeginingOfSelectingLot];
	if (_lots.count > 0) {
		[_headers addObject:@(HeaderType_Lot)];
	}
	[_colContent reloadData];
	//Warrant
    //There are 2 kinds: Equities | Futures. Futures -> there's no Warrant
    
    NSString *code = ([UserPrefConstants singleton].userCurrentExchange.length>2)?([[UserPrefConstants singleton].userCurrentExchange substringToIndex:2]):([UserPrefConstants singleton].userCurrentExchange);
    ExchangeData *ex = [Utils getExchange:code];

    if ([ex.exchange_description containsString:@"Derivative"]) {
        _vRemoveSector.hidden = YES;

    }else{
        _vRemoveSector.hidden = NO;
    }

    _enableWarrant = [UserPrefConstants singleton].enableWarrant;
	_warrantSwitch.on = _enableWarrant;
}
- (void)loadBeginingOfSelectionSector{
	_sectors = [[Utils loadInfoSectors] mutableCopy];
	if (_sectors.count == 0) {
		return;
	}
	_selectedSector = [UserPrefConstants singleton].selectedSector;
	_selectedSubSector = [UserPrefConstants singleton].selectedSubsector;
	if (!_selectedSector) {
		//Should selected sector All
		_selectedSector = _sectors[0];
		_subsector = @[].mutableCopy;
		return;
	}
	//For available Main Sector
	if (_selectedSector) {
		//Check subsector
		_subsector = _selectedSector.subItemsArray;
	}
}

- (void)loadBeginingOfSelectingLot{
	NSDictionary *marketInfos = [[UserPrefConstants singleton].currentExchangeInfo.marketInfos copy];
	if (marketInfos.count > 0) {
		NSMutableArray *listValueLots = @[].mutableCopy;
		NSArray *allKeys = [marketInfos.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
			return obj1.integerValue > obj2.integerValue;
		}];
		for (int i = 0; i < allKeys.count; i++) {
			NSString *key = allKeys[i];
			NSString *value = marketInfos[key];
			[listValueLots addObject:value];
		}
		_lots = listValueLots;
		//Re-sort
		//NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
		//_lots = [[_lots sortedArrayUsingDescriptors:@[sd]] mutableCopy];
		
		//[_lots sortedArrayUsingSelector:@selector(compare:)];
		/* +++ Demo
		NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
		data = [data sortedArrayUsingDescriptors:@[sd]];
		NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
		NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:NO];
		data = [data sortedArrayUsingDescriptors:@[sd, sd2]];
		*/
		//Check selected
		if ([UserPrefConstants singleton].selectedMarket) {
			_selectedLot = marketInfos[[UserPrefConstants singleton].selectedMarket];
		}else{
			[UserPrefConstants singleton].selectedMarket = marketInfos.allKeys.firstObject;
			_selectedLot = marketInfos[[UserPrefConstants singleton].selectedMarket];
		}
	}
}
#pragma mark - UTILS
- (CGSize)sizeOfCellFromFilter:(NSString *)filter{
	float width = kDEFULT_WIDTH_SECTOR_CELL;
	float height = kDEFAULT_HEIGHT_SECTOR_CELL;
	width = [filter widthOfString:AppFont_MainFontRegularWithSize(14)] + 10 + 16; //10 for padding, 16 for align left + right
	if (width < kDEFULT_WIDTH_SECTOR_CELL) {
		width = kDEFULT_WIDTH_SECTOR_CELL;
	}
	if (width > self.colContent.frame.size.width) {
		width = self.colContent.frame.size.width;
	}
	return CGSizeMake(width, height);
}

- (NSString *)titleHeaderAt:(HeaderType)type{
	NSString *title = @"";
	switch (type) {
		case HeaderType_Sector:
			title = [LanguageManager stringForKey:Sector];
			break;
		case HeaderType_SubSector:
			title = [LanguageManager stringForKey:_Subsector];
			break;
		case HeaderType_Lot:
			title = [LanguageManager stringForKey:Lot];
			break;
			
		default:
			break;
	}
	return title;
}

- (BOOL)compareSector:(SectorInfo *)info1 toSector:(SectorInfo *)info2{
	if ([info1.sectorCode isEqualToString:info2.sectorCode] || [info1.sectorName isEqualToString:info2.sectorName]) {
		return YES;
	}
	return NO;
}
#pragma mark - UICollectionView
#pragma mark - UICollectionViewCell DataSource
//For Header
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
	return [_headers count];
}

//For header
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
	return CGSizeMake(collectionView.frame.size.width, kHEIGHT_HEADER);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	SectorHeaderReusableView *header = nil;
	if (kind == UICollectionElementKindSectionHeader) {
		header = [collectionView dequeueReusableSupplementaryViewOfKind:kind
													withReuseIdentifier:@"SectorHeaderReusableView"
														   forIndexPath:indexPath];
		HeaderType type = [_headers[indexPath.section] integerValue];
		header.lblTitle.text = [self titleHeaderAt:type];
	}
	return header;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	HeaderType type = [_headers[section] integerValue];
	NSInteger countItem = 0;
	switch (type) {
		case HeaderType_Sector:
			countItem = [_sectors count];
			break;
		case HeaderType_SubSector:
			countItem = [_subsector count];
			break;
		case HeaderType_Lot:
			countItem = [_lots count];
			break;
		default:
			break;
	}
	return countItem;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	SectorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([SectorCell class]) forIndexPath:indexPath];
	
	HeaderType type = [_headers[indexPath.section] integerValue];
	//Selected Sector
	switch (type) {
		case HeaderType_Sector:{
			SectorInfo *sector = _sectors[indexPath.item];
			cell.selected = ([self compareSector:sector toSector:_selectedSector])? YES : NO;
			cell.lblContent.text = sector.sectorName;
		}
			break;
		case HeaderType_SubSector:{
			SectorInfo *sector = _subsector[indexPath.item];
			cell.selected = ([self compareSector:sector toSector:_selectedSubSector])? YES : NO;
			cell.lblContent.text = sector.sectorName;
		}
			break;
		case HeaderType_Lot:{
			NSString *filter = _lots[indexPath.item];
			cell.selected = ([filter containsString:_selectedLot])? YES : NO;
			cell.lblContent.text = [filter removeSpecialChars:@"[]"];;
		}
			break;
			
		default:
			break;
	}
	return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	HeaderType type = [_headers[indexPath.section] integerValue];
	CGSize sizeOfCell = CGSizeMake(kDEFULT_WIDTH_SECTOR_CELL, kDEFAULT_HEIGHT_SECTOR_CELL);
	switch (type) {
		case HeaderType_Sector:{
			SectorInfo *sector = _sectors[indexPath.item];
			sizeOfCell = [self sizeOfCellFromFilter:sector.sectorName];
		}
			break;
		case HeaderType_SubSector:{
			SectorInfo *sector = _subsector[indexPath.item];
			sizeOfCell = [self sizeOfCellFromFilter:sector.sectorName];
		}
			break;
		case HeaderType_Lot:{
			NSString *filter = _lots[indexPath.item];
			sizeOfCell = [self sizeOfCellFromFilter:filter];
		}
			
		default:
			break;
	}
	return sizeOfCell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
	return kSECTOR_SPACING_ITEM;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
	return kSECTOR_SPACING_LINE;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	HeaderType type = [_headers[indexPath.section] integerValue];
	switch (type) {
		case HeaderType_Sector:{
			SectorInfo *sector = _sectors[indexPath.item];
			_selectedSector = sector;
			_selectedSubSector = nil;
			if (sector.subItemsArray) {
				_subsector = sector.subItemsArray;
				if (_subsector.count > 0) {
					if (![_headers containsObject:@(HeaderType_SubSector)]) {
						[_headers insertObject:@(HeaderType_SubSector) atIndex:HeaderType_SubSector];
					}
				}else{
					if ([_headers containsObject:@(HeaderType_SubSector)]) {
						[_headers removeObject:@(HeaderType_SubSector)];
					}
				}
			}
		}
			break;
		case HeaderType_SubSector:{
			SectorInfo *sector = _subsector[indexPath.item];
			_selectedSubSector = sector;
		}
			break;
		case HeaderType_Lot:{
			_selectedLot = _lots[indexPath.item];
		}
			break;
			
		default:
			break;
	}
	[_colContent reloadData];
}
#pragma mark - Action
- (IBAction)onResetAction:(id)sender {
	//reset-Sector
	[self loadBeginingOfSelectionSector];
	if (_subsector.count > 0) {
		if (![_headers containsObject:@(HeaderType_SubSector)]) {
			[_headers insertObject:@(HeaderType_SubSector) atIndex:HeaderType_SubSector];
		}
	}
	//reset-Lot
	[self loadBeginingOfSelectingLot];
	[_colContent reloadData];
	//reset-Warrant
	_enableWarrant = [UserPrefConstants singleton].enableWarrant;
	_warrantSwitch.on = _enableWarrant;
}

- (IBAction)onApplyAction:(id)sender {
	[UserPrefConstants singleton].selectedSector = _selectedSector;
	[UserPrefConstants singleton].selectedSubsector = _selectedSubSector;
	[UserPrefConstants singleton].enableWarrant = _enableWarrant;
	//Selected Lot
	NSDictionary *marketInfos = [UserPrefConstants singleton].currentExchangeInfo.marketInfos;
	for (NSString *key in marketInfos.allKeys) {
		NSString *value = marketInfos[key];
		if ([value isEqualToString:_selectedLot]) {
			[UserPrefConstants singleton].selectedMarket = key;
			break;
		}
	}
	if (_delegate) {
		[_delegate didApplyFilter];
	}
	[self popView:YES];
}
- (IBAction)onWarrantValueChanged:(UISwitch *)sender {
	_enableWarrant = sender.on;
}
@end
