//
//  AlertVC.h
//  TCiPad
//
//  Created by tunv on 5/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "UIViewController+AlertUtil.h"
@class TCPageMenuControl;
@interface AlertVC : BaseVC
@property (weak, nonatomic) IBOutlet UITableView *tbl_content;
@property (weak, nonatomic) IBOutlet TCPageMenuControl *view_control;
@property (weak, nonatomic) IBOutlet UIButton *btn_filter;
@property (weak, nonatomic) IBOutlet UIView *view_filter;
@property (weak, nonatomic) IBOutlet UIView *view_top;
@property (weak, nonatomic) IBOutlet UIView *v_content;

@end
