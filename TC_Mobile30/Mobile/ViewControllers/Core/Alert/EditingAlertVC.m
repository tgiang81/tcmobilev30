//
//  EditingAlertVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "EditingAlertVC.h"
#import "TCTopAlertOptions.h"
#import "TCBottomAlertOptions2.h"
#import "TCBaseButton.h"
#import "UIViewController+SetupButton.h"
@interface EditingAlertVC ()
{
    CGFloat defaultCornerRadius;
}
@end

@implementation EditingAlertVC

- (void)viewDidLoad {
    defaultCornerRadius = 4;
    [super viewDidLoad];
    self.lbl_Title.text = [LanguageManager stringForKey:@"Alert Me When"];
    [self.btn_Cancel setTitle:[LanguageManager stringForKey:@"Cancel"] forState:UIControlStateNormal];
    [self.btn_Confirm setTitle:[LanguageManager stringForKey:@"Confirm"] forState:UIControlStateNormal];
    self.enableTapDismissKeyboard = YES;
    _bottomView.isCreate = self.isNew;
    if([self.alertModel.cdt isEqualToString:@""]){
        _topView.conditionType = 1;
    }else{
        _topView.conditionType = [self.alertModel.cdt integerValue];
    }
    _topView.stringValue = self.alertModel.lmt;
    
    _bottomView.isOn = [self.alertModel.s isEqualToString:@"A"];
    _bottomView.remark = self.alertModel.rmk;
}


- (IBAction)cancel:(TCBaseButton *)sender {
    if([self.delegate respondsToSelector:@selector(didSelectClose:)]){
        [self.delegate didSelectClose:self.index];
    }
}
- (IBAction)close:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(didSelectClose:)]){
        [self.delegate didSelectClose:self.index];
    }
}
- (IBAction)apply:(TCBaseButton *)sender {
    if([self.delegate respondsToSelector:@selector(didSelectApply:alert:isNew:)]){
        self.alertModel.cdt = [NSString stringWithFormat:@"%ld", _topView.conditionType];
        self.alertModel.lmt = _topView.tf_content.text;
//        self.alertModel.rmk = _bottomView.tv_input.text;
        if(_bottomView.sw_active.isOn){
            self.alertModel.s = @"A";
        }else{
            self.alertModel.s = @"X";
        }
        [self.delegate didSelectApply:self.index alert:self.alertModel isNew:self.isNew];
    }
}

- (void)updateTheme:(NSNotification *)noti{
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);
    [self.btn_Close setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTintColor)];
    
    [self setupButton:self.btn_Cancel corner:defaultCornerRadius borderColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor) bgColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_cancelColor) textColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor)];
    [self setupButton:self.btn_Confirm corner:defaultCornerRadius borderColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor) bgColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor) textColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor)];
}
@end
