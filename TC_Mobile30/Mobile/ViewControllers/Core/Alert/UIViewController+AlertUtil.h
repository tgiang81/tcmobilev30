//
//  UIViewController+Util.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/21/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, AlertVCSortType) {
    AlertVCSortType_None = 0,
    AlertVCSortType_Stock,
    AlertVCSortType_AlertType,
    AlertVCSortType_CompareType,
    AlertVCSortType_Media,
    AlertVCSortType_LastEdit
};

@protocol AlertSortDelegate<NSObject>
- (void)sortAlertMedia;
- (void)sortAlertAlertType;
- (void)sortComType;
- (void)sortLastUpdate;
- (void)filterWithState:(BOOL)isOn;
@end

@interface UIViewController (AlertUtil)
- (NSMutableArray *)sortType:(AlertVCSortType)type input:(NSArray *)input ascending:(BOOL)ascending;
@end

NS_ASSUME_NONNULL_END
