//
//  EditingAlertVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "AlertModel.h"
@class TCTopAlertOptions;
@class TCBottomAlertOptions2;
@protocol EditingAlertVCDelegate<NSObject>
- (void)didSelectClose:(NSIndexPath *)index;
- (void)didSelectApply:(NSIndexPath *)index alert:(AlertModel *)alert isNew:(BOOL)isNew;
@end
@interface EditingAlertVC : BaseVC
@property(weak, nonatomic) id<EditingAlertVCDelegate> delegate;
@property(strong, nonatomic) AlertModel *alertModel;
@property(strong, nonatomic) NSIndexPath *index;
@property (weak, nonatomic) IBOutlet TCTopAlertOptions *topView;
@property (weak, nonatomic) IBOutlet TCBottomAlertOptions2 *bottomView;
@property (assign, nonatomic) BOOL isNew;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Close;
@property (weak, nonatomic) IBOutlet UIButton *btn_Cancel;
@property (weak, nonatomic) IBOutlet UIButton *btn_Confirm;

@end
