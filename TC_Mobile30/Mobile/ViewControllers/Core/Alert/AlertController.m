//
//  AlertController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AlertController.h"
#import "AlertModel.h"
#import "APIStockAlert.h"
#import "UserPrefConstants.h"
#import "AlertMediaType.h"
#import "SettingAlertModel.h"
#import "SettingAlertWrapModel.h"
@implementation AlertController

+ (void) getAlerts:(void (^)(NSArray *alerts, NSError *error))completeHandler{
    NSString *bhCode = [UserPrefConstants singleton].brokerCode;
    NSString *appID = [UserPrefConstants singleton].appID;
    NSString *url = [UserPrefConstants singleton].stockAlertGetPublicURL;
    [APIStockAlert getPublicKeyWithAppURL:url appId:appID bhCode:bhCode completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            completeHandler(nil, error);
        }else{
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                 options:kNilOptions
                                                                   error:&error];
            id key = [[json objectForKey:@"keys"] objectAtIndex:0];
            NSString *algString = [key objectForKey:@"alg"];
            NSString *eString = [key objectForKey:@"e"];
            NSString *kidString = [key objectForKey:@"kid"];
            NSString *ktyString = [key objectForKey:@"kty"];
            NSString *nString = [key objectForKey:@"n"];
            
            [self requestAlerts:algString andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppIdStrin:appID completeHandler:completeHandler];
        }
    }];
}
+ (void) updateAlert:(AlertModel *)alert completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler{
    NSString *appID = [UserPrefConstants singleton].appID;
    [self updateAlert:appID alert:alert completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error){
            [UserPrefConstants singleton].uid = @"";
            [UserPrefConstants singleton].token = @"";
        }
        completeHandler(success, responseObject, error);
    }];
}


+ (void) addAlert:(AlertModel *)alert completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler{
    NSString *appID = [UserPrefConstants singleton].appID;
    NSString *bhCode = [UserPrefConstants singleton].brokerCode;
    NSString *url = [UserPrefConstants singleton].stockAlertGetPublicURL;
    NSString *uid = [UserPrefConstants singleton].uid;
    if([UserPrefConstants singleton].pushNotificationChanelId != nil && [UserPrefConstants singleton].token != nil && ![[UserPrefConstants singleton].token isEqualToString:@""] && uid != nil && ![uid isEqualToString:@""]){
        
        [APIStockAlert addAlert:[UserPrefConstants singleton].token bhCode:bhCode appID:appID cond:alert.cdt compt:alert.compt alrtt:alert.alrtt mdn:alert.mdn ex:alert.ex rmk:alert.rmk lmt:alert.lmt stkCd:alert.stkCd stkNm:alert.stkNm completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(error){
                [UserPrefConstants singleton].uid = @"";
                [UserPrefConstants singleton].token = @"";
            }
            completeHandler(success, responseObject, error);
        }];
    }else{
        [APIStockAlert getPublicKeyWithAppURL:url appId:appID bhCode:bhCode completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(error != nil){
                completeHandler(NO, nil, error);
            }else{
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                     options:kNilOptions
                                                                       error:&error];
                id key = [[json objectForKey:@"keys"] objectAtIndex:0];
                NSString *algString = [key objectForKey:@"alg"];
                NSString *eString = [key objectForKey:@"e"];
                NSString *kidString = [key objectForKey:@"kid"];
                NSString *ktyString = [key objectForKey:@"kty"];
                NSString *nString = [key objectForKey:@"n"];
                
                [self addAlert:algString andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appID alert:alert completeHandler:^(BOOL success, id responseObject, NSError *error) {
                    if(error){
                        [UserPrefConstants singleton].uid = @"";
                        [UserPrefConstants singleton].token = @"";
                    }
                    completeHandler(success, responseObject, error);
                }];
            }
        }];
    }
    
}
+ (void) requestAlerts:(NSString *)algSring
            andEString:(NSString *)eString
          andKidString:(NSString *)kidString
          andKtyString:(NSString *)ktyString
            andNString:(NSString *)nString
         andAppIdStrin:(NSString *)appId
       completeHandler:(void (^)(NSArray *alerts, NSError *error))completeHandler{
    NSString *bhCode = [UserPrefConstants singleton].bhCode;
    
    NSString *jwtToken = [APIStockAlert generateJWTToken:algSring andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appId];
    NSString *uid = [UserPrefConstants singleton].uid;
    
    NSString *urlString = [NSString stringWithFormat:@"%@?jwt=%@&bh=%@&appId=%@",[UserPrefConstants singleton].stockAlertURL,jwtToken,bhCode,appId];
    NSLog(@"web alert ULR:%@", urlString);
    
    if(uid == nil || [uid isEqualToString:@""]){
        [APIStockAlert getUid:jwtToken bhCode:bhCode appID:appId completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(success == YES){
                NSString *tmpJwtToken = [APIStockAlert generateJWTToken:algSring andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appId];
                [APIStockAlert getListAlerts:tmpJwtToken bhCode:bhCode appID:appId status:nil completeHandler:^(BOOL success, id responseObject, NSError *error) {
                    if(error){
                        [UserPrefConstants singleton].uid = @"";
                        [UserPrefConstants singleton].token = @"";
                    }
                    [self calculateAlerts:responseObject completeHandler:completeHandler];
                }];
            }
            
        }];
    }else{
        [APIStockAlert getListAlerts:jwtToken bhCode:bhCode appID:appId status:nil completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(error){
                [UserPrefConstants singleton].uid = @"";
                [UserPrefConstants singleton].token = @"";
            }
            [self calculateAlerts:responseObject completeHandler:completeHandler];
        }];
    }
}
+ (void) updateAlert:(NSString *)appId
               alert:(AlertModel *)alert
     completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler{
    
    NSString *bhCode = [UserPrefConstants singleton].bhCode;
    
    [APIStockAlert updateAlert:[UserPrefConstants singleton].token bhCode:bhCode appID:appId s:alert.s cond:alert.cdt compt:alert.compt alrtt:alert.alrtt mdn:alert.mdn rmk:alert.rmk lmt:alert.lmt aid:alert.id completeHandler:completeHandler];
}
+ (void) addAlert:(NSString *)algSring
       andEString:(NSString *)eString
     andKidString:(NSString *)kidString
     andKtyString:(NSString *)ktyString
       andNString:(NSString *)nString
         andAppId:(NSString *)appId
            alert:(AlertModel *)alert
  completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler{
    NSString *bhCode = [UserPrefConstants singleton].bhCode;
    
    NSString *jwtToken = [APIStockAlert generateJWTToken:algSring andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appId];
    NSString *uid = [UserPrefConstants singleton].uid;
    
    NSString *urlString = [NSString stringWithFormat:@"%@?jwt=%@&bh=%@&appId=%@",[UserPrefConstants singleton].stockAlertURL,jwtToken,bhCode,appId];
    NSLog(@"web alert ULR:%@", urlString);
    
    if(uid == nil || [uid isEqualToString:@""]){
        [APIStockAlert getUid:jwtToken bhCode:bhCode appID:appId completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(success == YES){
                NSString *tmpJwtToken = [APIStockAlert generateJWTToken:algSring andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appId];
                [APIStockAlert addAlert:tmpJwtToken bhCode:bhCode appID:appId cond:alert.cdt compt:alert.compt alrtt:alert.alrtt mdn:alert.mdn ex:alert.ex rmk:alert.rmk lmt:alert.lmt stkCd:alert.stkCd stkNm:alert.stkNm completeHandler:completeHandler];
            }
        }
         ];
    }else{
        [APIStockAlert addAlert:jwtToken bhCode:bhCode appID:appId cond:alert.cdt compt:alert.compt alrtt:alert.alrtt mdn:alert.mdn ex:alert.ex rmk:alert.rmk lmt:alert.lmt stkCd:alert.stkCd stkNm:alert.stkNm completeHandler:completeHandler];
    }
    
    
    
}
+ (NSDictionary *)getSettingsFile{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"settings" ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    if([json isKindOfClass:[NSDictionary class]]){
        return json;
    }
    return nil;
}


+ (SettingAlertModel *)getModelToShow:(NSIndexPath *)indexPath sections:(NSMutableArray *)sections{
    SettingAlertModel *model = sections[indexPath.section];
    SettingAlertModel *modelToShow;
    if([model isOrderAlert]){
//        if(indexPath.row == 0){
//            modelToShow = model;
//        }else{
            modelToShow = model.subAlerts[indexPath.row];
//            modelToShow.level = 1;
//        }
    }else{
        modelToShow = model;
    }
    return modelToShow;
}
- (BOOL)isEnableNotification{
    for(SettingAlertModel *settingModel in self.sections){
        if([settingModel checkEnable]){
            if(settingModel.subAlerts.count > 0){
                for(SettingAlertModel *subSetting in settingModel.subAlerts){
                    if([subSetting checkEnable]){
                        return YES;
                    }
                }
            }else{
                return YES;
            }
        }
    }
    return NO;
}
- (void) getSettingList:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler{
    [[AppState sharedInstance] addNetworkIndicator];
    NSString *appID = [UserPrefConstants singleton].appID;
    NSString *bhCode = [UserPrefConstants singleton].brokerCode;
    NSString *url = [UserPrefConstants singleton].stockAlertGetPublicURL;
    NSString *uid = [UserPrefConstants singleton].uid;
    if([UserPrefConstants singleton].token != nil && ![[UserPrefConstants singleton].token isEqualToString:@""] && uid != nil && ![uid isEqualToString:@""]){
        [APIStockAlert getSettingList:[UserPrefConstants singleton].token appId:appID bhCode:bhCode s:@"0" completeHandler:^(BOOL success, id responseObject, NSError *error) {
            [[AppState sharedInstance] removeNetworkIndicator];
            self.isLoaded = YES;
            if(error){
                [UserPrefConstants singleton].uid = @"";
                [UserPrefConstants singleton].token = @"";
            }
            [self calculateSettingAlerts:responseObject completeHandler:^(id model) {
                [self calculateSections:model];
                completeHandler(success, model, error);
            }];
            
        }];
    }else{
        [APIStockAlert getPublicKeyWithAppURL:url appId:appID bhCode:bhCode completeHandler:^(BOOL success, id responseObject, NSError *error) {
            self.isLoaded = YES;
            if(error != nil){
                NSLog(@"Error: %@", error.localizedDescription);
                completeHandler(NO, nil, error);
            }else{
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                     options:kNilOptions
                                                                       error:&error];
                id key = [[json objectForKey:@"keys"] objectAtIndex:0];
                NSString *algString = [key objectForKey:@"alg"];
                NSString *eString = [key objectForKey:@"e"];
                NSString *kidString = [key objectForKey:@"kid"];
                NSString *ktyString = [key objectForKey:@"kty"];
                NSString *nString = [key objectForKey:@"n"];
                
                [self getSettingList:algString andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appID s:@"0" completeHandler:^(BOOL success, id responseObject, NSError *error) {
                    [[AppState sharedInstance] removeNetworkIndicator];
                    if(error){
                        [UserPrefConstants singleton].uid = @"";
                        [UserPrefConstants singleton].token = @"";
                        completeHandler(NO, nil, error);
                    }
                    [self calculateSettingAlerts:responseObject completeHandler:^(id model) {
                        [self calculateSections:model];
                        completeHandler(success, model, error);
                    }];
                    
                }];
            }
        }];
    }
}

- (void) getSettingList:(NSString *)algSring
             andEString:(NSString *)eString
           andKidString:(NSString *)kidString
           andKtyString:(NSString *)ktyString
             andNString:(NSString *)nString
               andAppId:(NSString *)appId
                      s:(NSString *)s
        completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler{
    NSString *bhCode = [UserPrefConstants singleton].bhCode;
    
    NSString *jwtToken = [APIStockAlert generateJWTToken:algSring andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appId];
    NSString *uid = [UserPrefConstants singleton].uid;
    
    NSString *urlString = [NSString stringWithFormat:@"%@?jwt=%@&bh=%@&appId=%@",[UserPrefConstants singleton].stockAlertURL,jwtToken,bhCode,appId];
    NSLog(@"web alert ULR:%@", urlString);
    
    if(uid == nil || [uid isEqualToString:@""]){
        [APIStockAlert getUid:jwtToken bhCode:bhCode appID:appId completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(success == YES){
                NSString *tmpJwtToken = [APIStockAlert generateJWTToken:algSring andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:appId];
                [APIStockAlert getSettingList:tmpJwtToken appId:appId bhCode:bhCode s:s completeHandler:completeHandler];
            }
        }
         ];
    }else{
        [APIStockAlert getSettingList:jwtToken appId:appId bhCode:bhCode s:s completeHandler:completeHandler];
    }
    
}
+ (void) updateSetting:(SettingAlertModel *)model completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler{
    NSString *bhCode = [UserPrefConstants singleton].bhCode;
    NSString *appID = [UserPrefConstants singleton].appID;
    [[AppState sharedInstance] addNetworkIndicator];
    [APIStockAlert updateSetting:[UserPrefConstants singleton].token appId:appID bhCode:bhCode type:model.type val:model.value completeHandler:^(BOOL success, id responseObject, NSError *error) {
        [[AppState sharedInstance] removeNetworkIndicator];
        completeHandler(success, responseObject, error);
    }];
}


+ (void)calculateAlerts:(NSDictionary *)response
        completeHandler:(void (^)(NSArray *alerts, NSError *error))completeHandler{
    NSError *error;
    if([response objectForKey:@"alrtLs"]){
        NSArray *alertList = [AlertModel arrayOfModelsFromDictionaries:[response objectForKey:@"alrtLs"] error:&error];
        completeHandler(alertList, nil);
    }else{
        completeHandler(nil, error);
    }
}
- (void)calculateSettingAlerts:(NSDictionary *)response
               completeHandler:(void (^)(id model))completeHandler{
    NSError *error;
    SettingAlertWrapModel *model = [SettingAlertWrapModel new];
    model.alertSettings = [response valueForKey:@"alertSettings"];
    model.defOrderPush = [response valueForKey:@"defOrderPush"];
    model.defPricePush = [response valueForKey:@"defPricePush"];
    model.defMarketPush = [response valueForKey:@"defMarketPush"];
    model.ltUpdDt = [response valueForKey:@"ltUpdDt"];
    if([response valueForKey:@"orderItems"]){
        model.orderItems = [[response valueForKey:@"orderItems"] componentsSeparatedByString:@","];
    }
    model.setLs = [SettingAlertModel arrayOfModelsFromDictionaries:[response objectForKey:@"setLs"] error:&error];
    completeHandler(model);
}
+ (NSMutableArray *) getAlertTypes:(NSString *)bhCode{
    NSArray *media = [[self getSettingsFile] objectForKey:[NSString stringWithFormat:@"media_%@", bhCode]];
    if(media.count > 0){
        NSMutableArray *alerts = [AlertMediaType arrayOfModelsFromDictionaries:media error:nil];
        for(AlertMediaType *alert in alerts){
            if(![alert.key isEqualToString:@"S"] && ![alert.key isEqualToString:@"E"]){
                alert.id = [UserPrefConstants singleton].pushNotificationChanelId;
            }
        }
        return alerts;
    }
    return nil;
    
}
+ (NSMutableArray *) getAlertTypesName:(NSMutableArray *)mediaTypes{
    NSMutableArray *names = [NSMutableArray new];
    for(AlertMediaType *media in mediaTypes){
        [names addObject:media.value];
    }
    return names;
}

- (void)calculateSections:(SettingAlertWrapModel *)settingAlert{
    SettingAlertModel *opdAlert;
    self.sections = [NSMutableArray new];
    NSArray *subSettings = @[@"FM", @"PM", @"C", @"R"];
    if(settingAlert.orderItems.count > 0){
        subSettings = settingAlert.orderItems;
    }
    NSMutableArray *subSettingsModel = [NSMutableArray new];
    for(SettingAlertModel *setting in settingAlert.setLs){
        if(![subSettings containsObject:setting.type]){
            [_sections addObject:setting];
        }else{
            if(opdAlert){
                setting.parentType = opdAlert.type;
            }
            
            [subSettingsModel addObject:setting];
        }
        if([setting isOrderAlert]){
            opdAlert = setting;
        }
    }
    opdAlert.subAlerts = subSettingsModel;
}

- (SettingAlertModel *)orderWithType:(NSString *)type{
    for(SettingAlertModel *model in self.sections){
        if([model.type isEqualToString:type]){
            return model;
        }
    }
    return nil;
}

@end
