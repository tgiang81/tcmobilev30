//
//  AlertVC.m
//  TCiPad
//
//  Created by tunv on 5/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "AlertVC.h"
#import "TCPageMenuControl.h"
#import "AlertCell.h"
#import "TCAlertCell.h"
#import "FilterVC.h"
#import "AlertController.h"
#import "AlertModel.h"
#import "EditingAlertVC.h"
#import "StatusAlertVC.h"
#import "SearchStockVC.h"
#import "AddingAlertVC.h"
#import "SVPullToRefresh.h"
#import "TCStatusInfoView.h"
#import "LanguageKey.h"
#import "SettingManager.h"
#define kWIDTH_CELL 50
#define maxAlerts 30
@interface AlertVC () <MGSwipeTableCellDelegate, TCControlViewDelegate, UITableViewDelegate, UITableViewDataSource, TCAlertCellDelegate, EditingAlertVCDelegate, AlertSortDelegate>
{
    NSMutableArray *exchangeList;
    NSMutableArray *alerts;
    NSMutableArray *filteredAlerts;
    NSMutableArray *alertsMediaType;
    NSInteger lastSelected;
    TCStatusInfoView *tmpstatusView;
    AlertVCSortType currentSortType;
    AlertVCSortType previousSortType;
}
@end

@implementation AlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.customBarTitle = [LanguageManager stringForKey:Stock_Alert];
    [self createLeftMenu];
    //    [self setBackButtonDefault];
    [self setupTableView];
    [self setupTopControl];
    [self addAlertItem];
    [self setupLoadData];
    
}
- (void)setupLoadData{
    [self getAlerts:YES];
    alertsMediaType = [AlertController getAlertTypes:[UserPrefConstants singleton].brokerCode];
    exchangeList = [Utils getExchangeList];
    [self.tbl_content addPullToRefreshWithActionHandlerAndRemoveArrow:^{
        [self getAlerts:NO];
    }];
}
- (void)initValues{
    alerts = [NSMutableArray new];
    filteredAlerts = [NSMutableArray new];
}

- (void)getAlerts:(BOOL)showLoading{
    [[AppState sharedInstance] forceRemoveLoadingView];
    if(showLoading == YES){
        [[AppState sharedInstance] addLoadingView:self.view];
    }
    [AlertController getAlerts:^(NSArray *alerts, NSError *error) {
        [self.tbl_content stopAnimating];
        if(showLoading == YES){
            [[AppState sharedInstance] removeLoadingView:self.view];
        }
        if(error){
            [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[error localizedDescription]];
        }
        self->alerts = [NSMutableArray arrayWithArray:alerts];
        self->filteredAlerts = [self->alerts mutableCopy];
        [self setupTopControl];
        [self reloadTable];
        [self checkStatus];
        
        
    }];
}
- (void)reloadTable{
    [self.tbl_content reloadData];
}
- (void)checkStatus{
    if(self->filteredAlerts.count == 0){
        dispatch_async(dispatch_get_main_queue(), ^{
            self->tmpstatusView = [TCStatusInfoView new];
            [self->tmpstatusView justShowStatus:[LanguageManager stringForKey:No_Data] inView:self.v_content];
        });
        
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->tmpstatusView removeFromSuperview];
        });
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[AppState sharedInstance] forceRemoveLoadingView];
}

- (void)addAlertItem{
    UIImage *alertImage = [UIImage imageNamed:@"more_plus_icon"];
    CGRect alertFrame = CGRectMake(0, 0, alertImage.size.width, alertImage.size.height);
    UIButton *alertButton = [UIButton buttonWithType:UIButtonTypeSystem];
    alertButton.frame = alertFrame;
    [alertButton setImage:alertImage forState:UIControlStateNormal];
    [alertButton addTarget:self action:@selector(onAlertAction:)
          forControlEvents:UIControlEventTouchUpInside];
    alertButton.imageView.tintColor = AppColor_TintColor;
    UIBarButtonItem *alertItem = [[UIBarButtonItem alloc] initWithCustomView:alertButton];
    alertItem.tintColor = AppColor_TintColor;
    
    self.navigationItem.rightBarButtonItems = @[alertItem];
}
- (void)onAlertAction:(id)sender{
    [self showAddAlertVC];
}

- (void)setupTableView{
    self.tbl_content.allowsMultipleSelectionDuringEditing = NO;
    self.tbl_content.tableFooterView = [UIView new];
    self.tbl_content.delegate = self;
    self.tbl_content.dataSource = self;
    [self.tbl_content registerNib:[UINib nibWithNibName:@"TCAlertCell" bundle:nil] forCellReuseIdentifier:@"TCAlertCell"];
}

- (void)setupTopControl{
    //setColor
    _view_control.itemWidth = 130;
    _view_control.dataSources = [self getTopTitles];
    _view_control.delegate = self;
    _view_control.font = AppFont_MainFontMediumWithSize(15);
    _view_control.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_topBg);
    _view_control.indicatorColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_textColor);
    _view_control.unselectedColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_unSelectedTop);
    _view_control.selectedColor =TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_textColor);
    [_view_control startLoadingPageMenu];
}

- (void)showAddAlertVC{
    if(alerts.count <= maxAlerts){
        AddingAlertVC *newAlertVC = NEW_VC_FROM_NIB([AddingAlertVC class], @"AddingAlertVC");
        newAlertVC.exchangeList = exchangeList;
        newAlertVC.mediaType = alertsMediaType;
        newAlertVC.delegate = self;
        newAlertVC.isNew = YES;
        newAlertVC.enableSearch = YES;
        AlertModel *newModel = [[AlertModel alloc] init];
        newModel.s = @"A";
        newAlertVC.alertModel = newModel;
        CGSize contentSize = CGSizeMake(0.9 * SCREEN_WIDTH_PORTRAIT, 0.8 * SCREEN_HEIGHT_PORTRAIT);
        //    [self nextTo:newAlertVC animate:YES];
        [self showPopupWithContent:newAlertVC inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
        }];
    }else{
        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:@"You have hit the maximum number of alerts which can be set"];
    }
    
}

- (NSMutableArray *)calculateFilterdAlerts:(NSArray *)types{
    NSMutableArray *tmp = [NSMutableArray new];
    for(AlertModel *alert in alerts){
        for(NSString *type in types){
            if([alert.alrtt isEqualToString:type]){
                [tmp addObject:alert];
            }
        }
    }
    return tmp;
}
- (NSInteger)getIndexWithAlert:(AlertModel *)alert{
    for(int index = 0; index < alerts.count; index++){
        AlertModel *currentAlert = alerts[index];
        if([alert.id isEqualToString:currentAlert.id]){
            return index;
        }
    }
    return -1;
}
- (NSArray *)getTopTitles{
    NSMutableArray *title = [NSMutableArray new];
    [title addObject:[NSString stringWithFormat:@"%@ (%ld)", @"All", alerts.count]];
    for(NSDictionary *type in [AlertModel getalrtTs]){
        [title addObject:[NSString stringWithFormat:@"%@ (%ld)", [type allValues].firstObject, [self calculateFilterdAlerts:[type allKeys]].count]];
    }
    return title;
    
    //    @[[NSString stringWithFormat:@"%@ %ld", @"All", alerts.count], [NSString stringWithFormat:@"%@ %ld", @"Active", [self calculateFilterdAlerts:@[@"A"]].count], [NSString stringWithFormat:@"%@ %ld", @"Deactived", [self calculateFilterdAlerts:@[@"C", @"X"]].count]];
}

#pragma mark Action
- (void)showEditingVC:(NSIndexPath *)index{
    AddingAlertVC *addingAlertVC = NEW_VC_FROM_NIB([AddingAlertVC class], @"AddingAlertVC");
    addingAlertVC.delegate = self;
    addingAlertVC.index = index;
    addingAlertVC.exchangeList = exchangeList;
    addingAlertVC.mediaType = alertsMediaType;
    addingAlertVC.alertModel = [filteredAlerts[index.row] copy];
    CGSize contentSize = CGSizeMake(0.9 * SCREEN_WIDTH_PORTRAIT, 0.8 * SCREEN_HEIGHT_PORTRAIT);
    
    [self showPopupWithContent:addingAlertVC inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
    }];
}
- (void)showStatus:(NSString *)content{
    [self checkStatus];
//    StatusAlertVC *status = NEW_VC_FROM_NIB([StatusAlertVC class], @"StatusAlertVC");
//    status.message = content;
//    CGSize contentSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 350);
//    [self showPopupWithContent:status inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
//    }];
}
- (IBAction)filter:(id)sender {
    //    FilterVC *filterVC = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [FilterVC storyboardID]);
    //    CGSize contentSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.6 * SCREEN_HEIGHT_PORTRAIT);
    //    filterVC.delegate = self;
    //    [self showPopupWithContent:filterVC inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
    //    }];
    
}
#pragma mark EditingAlertVCDelegate
- (void)didSelectClose:(NSIndexPath *)index{
    [self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
        
    }];
}
- (void)didSelectApply:(NSIndexPath *)index alert:(AlertModel *)alert isNew:(BOOL)isNew{
    if(isNew){
        [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
            [AlertController addAlert:alert completeHandler:^(BOOL success, id responseObject, NSError *error) {
                if(!error){
                    [self->alerts insertObject:alert atIndex:0];
                    [self updateUI];
                    [self showStatus: [LanguageManager stringForKey:Alert_for_this_stock_has_been_made___You_can_manage_your_alerts_at_the_settings_page]];
                    [self getAlerts:NO];
                }else{
                    [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedDescription];
                }
                
            }];
        }];
    }else{
        [self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
            [self updateAlertWithAlert:alert status:@""];
        }];
    }
    
}
#pragma mark - TCControlViewDelegate
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
    if(index == 0){
        filteredAlerts = [alerts mutableCopy];
    }else{
        filteredAlerts = [self calculateFilterdAlerts:[[AlertModel getalrtTs][index-1] allKeys]];
    }
    //    else if(index == 1){
    //        filteredAlerts = [self calculateFilterdAlerts:@[@"A"]];
    //    }else{
    //        filteredAlerts = [self calculateFilterdAlerts:@[@"C", @"X"]];
    //    }
    [self reloadTable];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 106;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self showEditingVC: indexPath];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return filteredAlerts.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TCAlertCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TCAlertCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.delegateSetting = self;
    [cell setupView:filteredAlerts[indexPath.row]];
    return cell;
}
#pragma mark TCAlertCellDelegate
- (void)didSelectChangeValue:(BOOL)on at:(TCAlertCell *)cell{
    NSIndexPath *index = [self.tbl_content indexPathForCell:cell];
    if(on){
        [self updateAlertWith:index status:@"A"];
    }else{
        
        [self updateAlertWith:index status:@"X"];
    }
}
- (void)updateAlertWith:(NSIndexPath *)index status:(NSString *)status{
    AlertModel *alert = filteredAlerts[index.row];
    alert.s = status;
    if(alert.id == nil){
        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:Please_refresh_data]];
    }else{
        [self updateAlertWithAlert:alert status:status];
    }
    
}

- (void)updateAlertWithAlert:(AlertModel *)alert status:(NSString *)status{
    [[AppState sharedInstance] addLoadingView:self.view];
    [AlertController updateAlert:alert completeHandler:^(BOOL success, id responseObject, NSError *error) {
        [[AppState sharedInstance] removeLoadingView:self.view];
        if(success == YES){
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self getIndexWithAlert:alert] inSection:0];
            NSString *content = [LanguageManager stringForKey:Alert_for_this_stock_has_been_edited];
            if([status isEqualToString:@"L"]){
                content = [LanguageManager stringForKey:Alert_for_this_stock_has_been_deleted];
                [self->alerts removeObject:alert];
                [self->filteredAlerts removeObject:alert];
            }else{
                [self->alerts replaceObjectAtIndex:indexPath.row withObject:alert];
                
            }
            [self updateUI];
            [self showStatus: [LanguageManager stringForKey:content]];
        }else{
            [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedDescription];
        }
    }];
}
- (void)updateUI{
    lastSelected = _view_control.currentSelectedItem;
    [self didSelectControlView:_view_control atIndex:lastSelected];
    [self setupTopControl];
    [_view_control setCurrentSelectedItem:lastSelected];
}

- (UIImage*)deleteImageForHeight:(CGFloat)height image:(UIImage *)image backgroundColor:(UIColor *)backgroundColor{
    
    CGRect frame = CGRectMake(0, 0, 62, height);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(62, height), NO, [UIScreen mainScreen].scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, backgroundColor.CGColor);
    CGContextFillRect(context, frame);
    
    [image drawInRect:CGRectMake(frame.size.width/2.0, frame.size.height/2.0 - 10, 18, 20)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)updateTheme:(NSNotification *)noti{
    self.view_top.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_topBg);
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_viewBg);
    self.tbl_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_viewBg);
}


#pragma mark - MGSWipeTableViewCellDelegate
/**
 * Delegate method to enable/disable swipe gestures
 * @return YES if swipe is allowed
 **/
- (BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction{
    if (direction == MGSwipeDirectionRightToLeft) {
        return YES;
    }
    return NO;
}

/**
 * Delegate method to setup the swipe buttons and swipe/expansion settings
 * Buttons can be any kind of UIView but it's recommended to use the convenience MGSwipeButton class
 * Setting up buttons with this delegate instead of using cell properties improves memory usage because buttons are only created in demand
 * @param cell the UITableVieCel to configure. You can get the indexPath using [tableView indexPathForCell:cell]
 * @param direction The swipe direction (left to right or right to left)
 * @param swipeSettings instance to configure the swipe transition and setting (optional)
 * @param expansionSettings instance to configure button expansions (optional)
 * @return Buttons array
 **/
- (NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
              swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings{
    static NSString *emptyTitle = @"";
    if (direction == MGSwipeDirectionRightToLeft) {
        swipeSettings.transition = MGSwipeTransitionStatic;
        swipeSettings.bottomMargin = 2;
        swipeSettings.topMargin = 0;
        expansionSettings.fillOnTrigger = NO;
        //Create Button
        MGSwipeButton *delete = [MGSwipeButton buttonWithTitle:emptyTitle icon:[UIImage imageNamed:@"delete_cell_icon"] backgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_deleteBg) callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
            //Do something here
            DLog(@"+++Delete here");
            [self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:Do_you_want_to_delete_this_alert] withOKAction:^{
                NSIndexPath *indexPath = [self.tbl_content indexPathForCell:cell];
                [self updateAlertWith:indexPath status:@"L"];
            } cancelAction:^{
                //Do nothing
            }];
            
            return YES;
        }];
        delete.buttonWidth = kWIDTH_CELL;
        return @[delete];
    }
    return nil;
}
- (void)sortAlerts:(AlertVCSortType)type{
    currentSortType = type;
    filteredAlerts = [self sortType:type input:filteredAlerts ascending:(currentSortType == AlertVCSortType_None) || (currentSortType != previousSortType)];
    [self reloadTable];
    if(previousSortType == AlertVCSortType_None){
        previousSortType = type;
    }else{
        previousSortType = AlertVCSortType_None;
    }
    
}
- (void)sortAlertStock{
    [self sortAlerts:AlertVCSortType_Stock];
}
- (void)sortAlertMedia{
    [self sortAlerts:AlertVCSortType_Media];
}
- (void)sortAlertAlertType{
    [self sortAlerts:AlertVCSortType_AlertType];
}
- (void)sortComType{
    [self sortAlerts:AlertVCSortType_CompareType];
}
- (void)sortLastUpdate{
    [self sortAlerts:AlertVCSortType_LastEdit];
}
- (void)filterWithState:(BOOL)isOn{
    filteredAlerts = [self filterWithState:isOn input:alerts];
    [self sortAlerts:currentSortType];
    [self reloadTable];
}
- (NSMutableArray *)filterWithState:(BOOL)isOn input:(NSMutableArray *)input{
    NSString *filterState = @"s == 'A'";
    if(!isOn){
        filterState = @"s != 'A'";
    }
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filterState];
    NSArray *filteredArr  = [input filteredArrayUsingPredicate:predicate1];
    return [[NSMutableArray alloc] initWithArray:filteredArr];
}

- (IBAction)StockAction:(id)sender {
    [self sortAlertStock];
}
- (IBAction)AlertType:(id)sender {
    [self sortAlertAlertType];
}
- (IBAction)Compare:(id)sender {
    [self sortComType];
}
- (IBAction)Media:(id)sender {
    [self sortAlertMedia];
}
- (IBAction)StatusAction:(id)sender {
    [self sortLastUpdate];
}
- (IBAction)OnOffFilter:(id)sender {
    [self filterWithState:YES];
}

@end
