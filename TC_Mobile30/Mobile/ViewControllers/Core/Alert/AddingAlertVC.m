//
//  AddingAlertVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AddingAlertVC.h"
#import "AlertModel.h"
#import "AlertController.h"
#import "SearchStockController.h"
#import "AlertStockCell.h"
#import "UIViewController+SetupButton.h"
#import "LanguageKey.h"
#import "NSString+Util.h"
#import "AlertMediaType.h"
#import "ExchangeData.h"
@interface AddingAlertVC () <TCMenuDropDownDelegate, UITextFieldDelegate>
{
    AlertModel *originalModel;
}
@end

@implementation AddingAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.enableTapDismissKeyboard = YES;
    [self setupDropDown];
    self.tf_StockCode.delegate = self;
    [self.tf_StockCode setEnabled:self.enableSearch];
    [self.tf_StockCode addTarget:self
                        action:@selector(textFieldDidChange:)
              forControlEvents:UIControlEventEditingChanged];
    NSString *mainTintColor = [ThemeManager shareInstance].tintColor;
    [self setupButton:self.btn_Confirm corner:self.defaultRadius borderColor:TC_COLOR_FROM_HEX(mainTintColor) bgColor:TC_COLOR_FROM_HEX(mainTintColor) textColor:TC_COLOR_FROM_HEX(@"FFFFFF")];
    
    [self setupButton:self.btn_Cancel corner:self.defaultRadius borderColor:TC_COLOR_FROM_HEX(mainTintColor) bgColor:[UIColor clearColor] textColor:TC_COLOR_FROM_HEX(mainTintColor)];
    
    self.tv_Remarks.layer.cornerRadius = self.defaultRadius;
    [self.tv_Remarks setTextContainerInset:UIEdgeInsetsMake(8, 2, 8, 2)];
    [self setupTextField:self.tf_Value];
    [self setupTextField:self.tf_StockCode];
    
    
    self.lbl_Title.text = [LanguageManager stringForKey:Set_Alert];
    self.lbl_StockCode.text = [LanguageManager stringForKey:Stock_Code_Name];
    self.lbl_AlertType.text = [LanguageManager stringForKey:Alert_Type];
    self.lbl_AlertMe.text = [LanguageManager stringForKey:Alert_me_when_value_is_];
    self.lbl_ValueType.text = [LanguageManager stringForKey:Value_Type];
    self.lbl_Value.text = [LanguageManager stringForKey:Value];
    self.lbl_Remarks.text = [LanguageManager stringForKey:Remarks];
//    if(!self.isNew){
//        [self.btn_Cancel setTitle:[LanguageManager stringForKey:Cancel] forState:UIControlStateNormal];
//        [self.btn_Confirm setTitle:[LanguageManager stringForKey:Confirm] forState:UIControlStateNormal];
//    }else{
        [self.btn_Cancel setTitle:[LanguageManager stringForKey:Clear] forState:UIControlStateNormal];
        [self.btn_Confirm setTitle:[LanguageManager stringForKey:Save] forState:UIControlStateNormal];
//    }
    originalModel = [AlertModel copyModel:self.alertModel];
    [self setupContent];
    // Do any additional setup after loading the view from its nib.
}


- (void)setupTextField:(UITextField *)textField{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingDropDownBg);
    textField.layer.cornerRadius = self.defaultRadius;
}



- (void)setupContent{
    self.tf_StockCode.text = self.alertModel.stkNm;
    self.tf_StockCode.text = self.alertModel.stkNm;
    self.tf_Value.text = self.alertModel.lmt;
    self.tv_Remarks.text = self.alertModel.rmk;
    [self reloadSearchData:NO];
}
- (void)resetData{
    _alertModel = [AlertModel copyModel:originalModel];
    [self setupDropDown];
    [self setupContent];
}
- (void)setupDropDown{
    NSMutableArray *items0 = [AlertModel getalrtTNames];
    
    NSMutableArray *items1 = [AlertModel getComTNames];
    
    NSMutableArray *items2 = [AlertModel getListCondition];
    [items2 removeObjectAtIndex:0];
    NSArray *alertTypeNames = [AlertController getAlertTypesName:self.mediaType];
    if(self.alertModel.mdn && [self.alertModel.mdn isValidString]){
        [self setupDropDown:_dv_MediaType title:[self.alertModel getMdnName] contents:alertTypeNames];
    }else{
        self.alertModel.mdn = @"PN";
        [self setupDropDown:_dv_MediaType title:alertTypeNames.firstObject contents:alertTypeNames];
    }
    if(self.alertModel.exStk && [self.alertModel.exStk isValidString]){
        [_dv_Exchange setUserInteractionEnabled:NO];
        [self setupDropDown:_dv_Exchange title:[Utils getShortExChangeName:[Utils getExchange:self.alertModel.exStk]] contents:[Utils getNamesOfExchange:self.exchangeList]];
    }else{
        self.alertModel.ex = [UserPrefConstants singleton].userCurrentExchange;
        [self setupDropDown:_dv_Exchange title:[Utils getShortExChangeName:[Utils getExchange:[UserPrefConstants singleton].userCurrentExchange]] contents:[Utils getNamesOfExchange:self.exchangeList]];
    }
    
    [self setupDropDown:self.dv_searchResult title:nil contents:[self getStockNames]];
    [self setupDropDown:_dv_AlertType title:[AlertModel getAlrtName:self.alertModel.alrtt] contents:items0];
    [self setupDropDown:_dv_ValueType title:[AlertModel getComTName:self.alertModel.compt] contents:items1];
    [self setupDropDown:_dv_AlertMe title:[self.alertModel getConditionString] contents:items2];
}
- (IBAction)closeAction:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectClose:)]){
        [self.delegate didSelectClose:nil];
    }
}

- (IBAction)didSelectCancel:(id)sender {
    [self resetData];
}
- (IBAction)didSelectConfirm:(id)sender {
    if(self.alertModel.stkNm){
        self.alertModel.lmt = self.tf_Value.text;
        self.alertModel.rmk = self.tv_Remarks.text;
        if(![self.alertModel isValidAlert]){
            [self showAlert: [LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:Please_enter_fields]];
        }else{
            if([self.delegate respondsToSelector:@selector(didSelectApply:alert:isNew:)]){
                [self.delegate didSelectApply:self.index alert:self.alertModel isNew:self.isNew];
            }
        }
        
    }else{
        [self showAlert: [LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:Please_select_an_alert]];
    }
    
}


#pragma mark TCMenuDropDownDelegate
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
    if(_dv_MediaType == menuView){
        self.alertModel.mdn = ((AlertMediaType *)self.mediaType[index]).id;
    }else if(_dv_Exchange == menuView){
        self.alertModel.ex = ((ExchangeData *)self.exchangeList[index]).feed_exchg_code;
    }
    else if(_dv_AlertType == menuView){
        [self.alertModel setAlertTIndex:index];
    }else if(_dv_ValueType == menuView){
        [self.alertModel setComTIndex:index];
    }else if (_dv_AlertMe == menuView){
        self.alertModel.cdt = [NSString stringWithFormat:@"%ld", index+1];
    }else{
        [self didSelectStock:[self.tableDataArr objectAtIndex:index]];
    }
}
#pragma mark SearchingStock
- (void)didSelectStock:(StockModel *)stock{
    NSString *stkCode = stock.stockCode;
    NSString *stkName = stock.stockName;
    self.alertModel.stkCd = stkCode;
    self.alertModel.stkNm = stkName;
    [self setupContent];
    [self.btn_Confirm setEnabled:YES];
    [self tapOnView:nil];
}

#pragma mark UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.tableDataArr count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlertStockCell *cell = [tableView dequeueReusableCellWithIdentifier:[AlertStockCell reuseIdentifier] forIndexPath:indexPath];
    cell.searchResultLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellTextColor);
    cell.searchResultLabel2.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellTextColor);
    NSDictionary *eachDict = [self.tableDataArr objectAtIndex:indexPath.row];
    cell.searchResultLabel.text = [eachDict objectForKey:FID_33_S_STOCK_CODE];
    cell.searchResultLabel2.text = [eachDict objectForKey:FID_39_S_COMPANY];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark UITableViewDataSource
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 ==0) {
        cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellBg1);
        
    } else {
        cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellBg2);
    }
    
}


#pragma mark UITextFieldDelegate
- (void)textFieldDidChange:(UITextField *)sender {
    [self searchStock:sender.text];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(self.tableDataArr.count > 0){
        [self.dv_searchResult openComponent:0 animated:YES];
    }
}
    
- (void)updateTheme:(NSNotification *)noti{
    [super updateTheme:noti];
    [self.btn_Close setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor)];
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);
    self.lbl_StockCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingLabelColor);
    self.lbl_AlertType.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingLabelColor);
    self.lbl_AlertMe.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingLabelColor);
    self.lbl_ValueType.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingLabelColor);
    self.lbl_Value.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingLabelColor);
    self.lbl_Remarks.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingLabelColor);
    
    self.tf_Value.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);
    self.tf_StockCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);
    self.tv_Remarks.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingTitleColor);

}
@end
