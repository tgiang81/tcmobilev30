//
//  SettingAlertVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
@class AlertController;
NS_ASSUME_NONNULL_BEGIN
@protocol SettingAlertVCDelegate<NSObject>
- (void)didReloadSettingNofi;
@end
@interface SettingAlertVC : BaseVC
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@property (weak, nonatomic) id<SettingAlertVCDelegate> delegate;
@property (strong, nonatomic) AlertController *controller;
- (NSInteger)numberOfItems;
@end
#define kNotification_DidLoadNotificationSetting           @"DidLoadNotificationSetting"
NS_ASSUME_NONNULL_END
