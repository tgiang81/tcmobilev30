//
//  StatusAlertVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "StatusAlertVC.h"
#import "TCBaseButton.h"
#import "LanguageKey.h"
@interface StatusAlertVC ()
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet TCBaseButton *btn_Done;
@property (weak, nonatomic) IBOutlet UIButton *btn_icon;

@end

@implementation StatusAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lbl_Message.text = self.message;
    self.lbl_Title.text = [LanguageManager stringForKey:Set_Alert];
    self.lbl_Message.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].as_messColor);
    [self.btn_Done setTitle:[LanguageManager stringForKey:Done] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    [self dismissPopup:YES completion:nil];
}

- (void)updateTheme:(NSNotification *)noti{
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].as_bg);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].as_TitleColor);
    self.lbl_Message.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].as_messColor);
    self.btn_Done.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].as_tintColor);
    [self.btn_icon setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].as_tintColor)];
}
@end
