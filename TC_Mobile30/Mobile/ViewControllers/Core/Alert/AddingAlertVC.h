//
//  AddingAlertVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "EditingAlertVC.h"
#import "SearchingDropDownVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface AddingAlertVC : SearchingDropDownVC
@property(weak, nonatomic) id<EditingAlertVCDelegate> delegate;
    @property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Close;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Media;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Exchange;

@property (weak, nonatomic) IBOutlet UILabel *lbl_StockCode;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AlertType;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AlertMe;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ValueType;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Value;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Remarks;
@property (weak, nonatomic) IBOutlet UITextField *tf_StockCode;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *dv_MediaType;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *dv_Exchange;

@property (weak, nonatomic) IBOutlet TCMenuDropDown *dv_AlertType;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *dv_ValueType;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *dv_AlertMe;
@property (weak, nonatomic) IBOutlet UITextField *tf_Value;
@property (weak, nonatomic) IBOutlet UITextView *tv_Remarks;
@property (weak, nonatomic) IBOutlet UIButton *btn_Cancel;
@property (weak, nonatomic) IBOutlet UIButton *btn_Confirm;

@property(strong, nonatomic) NSMutableArray *exchangeList;
@property(strong, nonatomic) NSMutableArray *mediaType;
@property(strong, nonatomic) NSIndexPath *index;
@property(strong, nonatomic) AlertModel *alertModel;
@property(assign, nonatomic) BOOL enableSearch;
@property (assign, nonatomic) BOOL isNew;
@end

NS_ASSUME_NONNULL_END
