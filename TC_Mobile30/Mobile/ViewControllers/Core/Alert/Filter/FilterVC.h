//
//  FilterVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//
@class TCBaseButton;
#import "BaseVC.h"
@protocol FilterVCDelegate <NSObject>
- (void)didSelectApply:(long)filterType;
- (void)didSelectClose;
@end
@interface FilterVC : BaseVC
@property (weak, nonatomic) IBOutlet UIView *view_top;
@property (weak, nonatomic) IBOutlet UIButton *btn_back;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UITableView *tbl_content;
@property (weak, nonatomic) IBOutlet UIView *view_bottom;
@property (weak, nonatomic) IBOutlet TCBaseButton *btn_close;
@property (weak, nonatomic) IBOutlet TCBaseButton *btn_apply;
@property (nonatomic) NSArray *filterTypes;
@property (weak, nonatomic) id<FilterVCDelegate> delegate;
@end
