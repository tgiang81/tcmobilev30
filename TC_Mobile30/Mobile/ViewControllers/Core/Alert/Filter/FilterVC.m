//
//  FilterVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "FilterVC.h"
#import "FilterCell.h"
@interface FilterVC () <UITableViewDelegate, UITableViewDataSource, FilterCellDelegate>
@property (assign) long selectedFilter;

@end

@implementation FilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initValues];
    [self setupTableView];
}
- (void)setupTableView{
    self.tbl_content.delegate = self;
    self.tbl_content.dataSource = self;
    self.tbl_content.rowHeight = 50;
    [self.tbl_content registerNib:[UINib nibWithNibName:[FilterCell nibName] bundle:nil] forCellReuseIdentifier:[FilterCell reuseIdentifier]];
}
- (void)initValues{
    self.filterTypes = @[@"Short by Name", @"Short by Stock", @"Short by News", @"Short by List", @"Short by Like", @"Short by Favourite", @"Short by Volume"];
}
#pragma mark action

- (IBAction)back:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectClose)]){
        [self.delegate didSelectClose];
    }
}
- (IBAction)close:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectClose)]){
        [self.delegate didSelectClose];
    }
}
- (IBAction)apply:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectApply:)]){
        [self.delegate didSelectApply:self.selectedFilter];
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.filterTypes.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FilterCell *cell = [tableView dequeueReusableCellWithIdentifier:[FilterCell reuseIdentifier] forIndexPath:indexPath];
    [cell setContent:self.filterTypes[indexPath.row]];
    [cell changeState:indexPath.row == self.selectedFilter];
    cell.delegate = self;
    return cell;
}

#pragma mark FilterCellDelegate
- (void)didSelect:(FilterCell *)cell{
    NSIndexPath *selectedIndex = [self.tbl_content indexPathForCell:cell];
    if(selectedIndex != nil){
        self.selectedFilter = selectedIndex.row;
        [self.tbl_content reloadData];
    }
}



@end
