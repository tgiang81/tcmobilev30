//
//  UIViewController+Util.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/21/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "UIViewController+AlertUtil.h"

@implementation UIViewController (AlertUtil)
- (NSMutableArray *)sortType:(AlertVCSortType)type input:(NSArray *)input ascending:(BOOL)ascending{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:[self getSortedPropertyName:type]
                                                 ascending:ascending];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[input sortedArrayUsingDescriptors:@[sortDescriptor]]];
    return sortedArray;
}

- (NSString *)getSortedPropertyName:(AlertVCSortType)type{
    
    switch (type) {
        case AlertVCSortType_Media:
            return @"mdn";
        case AlertVCSortType_Stock:
            return @"stkCd";
        case AlertVCSortType_AlertType:
            return @"alrtt";
        case AlertVCSortType_CompareType:
            return @"compt";
        case AlertVCSortType_LastEdit:
            return @"lstupdDouble.doubleValue";
        default:
            return @"";
    }
}
@end
