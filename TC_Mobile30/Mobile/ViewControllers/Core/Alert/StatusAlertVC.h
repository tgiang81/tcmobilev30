//
//  StatusAlertVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

@interface StatusAlertVC : BaseVC
@property (weak, nonatomic) IBOutlet UILabel *lbl_Message;
@property (strong, nonatomic) NSString *message;
@end
