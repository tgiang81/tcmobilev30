//
//  AlertController.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFBaseRequest.h"
#import "AppState.h"
@class AlertModel;
@class SettingAlertModel;
@class SettingAlertWrapModel;
@interface AlertController : NSObject
@property(strong, nonatomic)NSMutableArray *sections;
+ (void) getAlerts:(void (^)(NSArray *alerts, NSError *error))completeHandler;
+ (NSMutableArray *) getAlertTypes:(NSString *)bhCode;
+ (NSMutableArray *) getAlertTypesName:(NSMutableArray *)mediaTypes;
+ (void) updateAlert:(AlertModel *)alert completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler;
+ (void) addAlert:(AlertModel *)alert completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler;

//Notification
- (void) getSettingList:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler;
+ (SettingAlertModel *)getModelToShow:(NSIndexPath *)indexPath sections:(NSMutableArray *)sections;
+ (void) updateSetting:(SettingAlertModel *)model completeHandler:(void (^)(BOOL success, id responseObject, NSError *error))completeHandler;
- (BOOL)isEnableNotification;
@property(assign, nonatomic) BOOL isLoaded;
- (SettingAlertModel *)orderWithType:(NSString *)type;
@end
