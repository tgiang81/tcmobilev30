//
//  SettingAlertVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SettingAlertVC.h"
#import "SettingAlertCell.h"
#import "AlertController.h"
#import "SettingAlertWrapModel.h"
#import "SettingAlertModel.h"
#import "TCBubbleMessageView.h"
#import "LanguageKey.h"
#define kTURN_OFF_ALERT_MSG    @"Turn Off Notification successfully"
#define kTURN_ON_ALERT_MSG    @"Turn On Notification successfully"
@interface SettingAlertVC () <UITableViewDelegate, UITableViewDataSource, SettingAlertCellDelegate>
@end

@implementation SettingAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.customBarTitle = [LanguageManager stringForKey:@"Settings"];
    [self.tbl_Content registerNib:[UINib nibWithNibName:[SettingAlertCell nibName] bundle:nil] forCellReuseIdentifier:[SettingAlertCell reuseIdentifier]];
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    self.tbl_Content.estimatedRowHeight = 50;
    [self addNotification:kNotification_DidLoadNotificationSetting selector:@selector(didLoadSettings:)]; 
}

- (void)didLoadSettings:(NSNotification *)noti{
    [self reloadTableView];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeNotification:kNotification_DidLoadNotificationSetting];
}
- (void)loadData{
//    if(!_controller.isLoaded){
//        [_controller getSettingList:^(BOOL success, id responseObject, NSError *error) {
//            if(error){
//                [self showAlert:@"Error" message:error.localizedDescription];
//            }else{
//                [self reloadTableView];
//            }
//        }];
//    }
}

- (void)updateTheme:(NSNotification *)noti{
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_bgView);
    self.tbl_Content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].sta_bgView);
}
- (NSInteger)numberOfItems{
    NSInteger total = 0;
    for(int index = 0; index < _controller.sections.count; index++){
        total += [self numberOfItemsInSection:index];
    }
    return total;
}
- (NSInteger)numberOfItemsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }
    SettingAlertModel *model = _controller.sections[section - 1];
    if([model checkEnable]){
        if(model.subAlerts.count > 0){
            return model.subAlerts.count;
        }else{
            return 1;
        }
    }else{
        return 1;
    }
}
#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return UITableViewAutomaticDimension;
    }
    return 50;
}

#pragma mark UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SettingAlertCell *cell = [tableView dequeueReusableCellWithIdentifier:[SettingAlertCell reuseIdentifier] forIndexPath:indexPath];
    cell.delegate = self;
    [cell setHightLightCell:indexPath.section == 0];
    if(indexPath.section != 0){
        NSIndexPath *correctIndex = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section - 1];
        SettingAlertModel *model = [AlertController getModelToShow:correctIndex sections:_controller.sections];
        [cell setupModelForCell:model];
    }else{
        [cell setupTitle:[LanguageManager stringForKey:@"Enable Push Notifications"] andIsOn:[_controller isEnableNotification]];
    }
    
    return  cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self numberOfItemsInSection:section];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([_controller isEnableNotification]){
        return _controller.sections.count + 1;
    }else{
        return 1;
    }
}
#pragma mark SettingAlertCellDelegate
- (void)didChangeState:(BOOL)value onCell:(SettingAlertCell *)cell{
    NSIndexPath *indexPath = [self.tbl_Content indexPathForCell:cell];
    if(indexPath.section != 0){
        NSIndexPath *correctIndex = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section - 1];
        SettingAlertModel *settingModel = [AlertController getModelToShow:correctIndex sections:_controller.sections];
        [self setupAlert:settingModel value:value isShowBubble:NO];
        
        SettingAlertModel *parentModel = [_controller orderWithType:settingModel.type];
        if([parentModel checkChildrenEnable] == value && parentModel.isEnable != value){
            [self setupAlert:parentModel value:value isShowBubble:NO];
        }
    }else{
        for(SettingAlertModel *settingModel in _controller.sections){
            if(settingModel.isEnable != value){
                [self setupAlert:settingModel value:value isShowBubble:YES];
            }
        }
        [self reloadTableView];
    }
}
- (void)setupAlert:(SettingAlertModel *)settingModel value:(BOOL)value isShowBubble:(BOOL)isShowBubble{
    __block NSString *messageChanged;
    settingModel.isEnable = value;
    [AlertController updateSetting:settingModel completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error){
            [self showAlert:TC_Pro_Mobile message:error.localizedDescription];
        }else{
            if(isShowBubble){
                if (value) {
                    messageChanged = kTURN_ON_ALERT_MSG;
                }else{
                    messageChanged = kTURN_OFF_ALERT_MSG;
                }
                [TCBubbleMessageView showWindowBubbleMessage:messageChanged];
            }
            [self reloadTableView];
            [SettingManager shareInstance].isOnNotification = [self->_controller isEnableNotification];
            [[SettingManager shareInstance] save];
        }
    }];
}
- (void)reloadTableView{
    [self.tbl_Content reloadData];
    if([self.delegate respondsToSelector:@selector(didReloadSettingNofi)]){
        [self.delegate didReloadSettingNofi];
    }
}
@end
