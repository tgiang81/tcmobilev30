//
//  MSettingVC.m
//  TC_Mobile30
//
//  Created by Kaka on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MSettingVC.h"
#import "AllCommon.h"
#import "UIViewController+TCUtil.h"
#import "UIViewController+Alert.h"
#import "TCBaseButton.h"
#import "TCBaseView.h"
#import "TCDropDown.h"
#import "Utils.h"
#import "DeviceUtils.h"
#import "SettingAlertVC.h"
#import "AlertController.h"
#import "SettingAlertModel.h"
#import "ExchangeSelectionVC.h"
#import "UIViewController+Auth.h"
#import "TCBubbleMessageView.h"
#import "JNKeychain.h"
#import "ChangePasswordViewController.h"
#import "TouchManager.h"
#import "LanguageKey.h"
#import "TermAndConditionVC.h"
#import "MZFormSheetController.h"
//Constant type - Maybe store in localized string
#define kTURN_OFF_TOUCH_ID_MSG	@"Turn Off Touch ID successfully"
#define kTURN_ON_TOUCH_ID_MSG	@"Turn On Touch ID successfully"
#define kTURN_OFF_FACE_ID_MSG	@"Turn Off Face ID successfully"
#define kTURN_ON_FACE_ID_MSG	@"Turn On Face ID successfully"


//SECTIONS
typedef enum: NSInteger {
    MSettingSection_Unknown = -1,
    MSettingSection_Application,
    MSettingSection_ViewPreference,
    MSettingSection_TradingPreference,
    MSettingSection_Language,
    MSettingSection_Account,
    MSettingSection_Policy
} MSettingSection;

@interface MSettingVC () <SettingAlertVCDelegate, ExchangeSelectionVCDelegate, UITextFieldDelegate>{
    TCDropDown *_homepageDropdown;
    CGFloat _defaultCellHeight;
    NSArray *_listPages;
}
//OUTLETS
//Theme
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_RightThemeView;

@property (weak, nonatomic) IBOutlet UIView *themeView;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHomepageDropdown;

@property (weak, nonatomic) IBOutlet UILabel *lblDefaultPageName;
//TOUCHID
@property (weak, nonatomic) IBOutlet UILabel *lblTouchID;
@property (weak, nonatomic) IBOutlet UISwitch *switchTouchID;
@property (weak, nonatomic) IBOutlet UISwitch *switchNotification;
@property (weak, nonatomic) IBOutlet UISwitch *switchQuantity;
@property (weak, nonatomic) IBOutlet UISwitch *switchPrice;
@property (weak, nonatomic) IBOutlet UISwitch *switchOrderTypeUI;
@property (weak, nonatomic) IBOutlet UISwitch *switchTradeButton;

@property (weak, nonatomic) IBOutlet UIButton *btnUpPriceColor;
@property (weak, nonatomic) IBOutlet UIButton *btnDownPriceColor;

@property (weak, nonatomic) IBOutlet UIButton *btnBuyColor;
@property (weak, nonatomic) IBOutlet UIButton *btnSellColor;

@property (weak, nonatomic) IBOutlet UIView *v_Notifications;
@property (weak, nonatomic) IBOutlet UIButton *btn_QuantityValue;
//Exchange
@property (weak, nonatomic) IBOutlet UILabel *lblExchange;
@property (weak,nonatomic) id<TransferDataDelegate> delegate;

@end

@implementation MSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _listPages = @[@(DefaultPage_Dashboard), @(DefaultPage_Watchlists), @(DefaultPage_News), @(DefaultPage_Markets)];
    [self addNotification:kDidChangeThemeNotification selector:@selector(updateTheme:)];
    [self createUI];
    [self loadData];
    [self configView];
    
    [_txtcustomQuantity addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
//    [self.view addGestureRecognizer:[[TouchManager alloc] initWithTarget:self action:@selector(dismissKeyboardTexField:)]];
    
}
- (void)configView{
    if([BrokerManager shareInstance].broker.tag == BrokerTag_SSI){
        NSInteger numberOfSSITheme = 1;
        NSInteger numberOfOtherThemes = [self.themeView subviews].count;
        CGFloat rightPosition = ((numberOfOtherThemes - numberOfSSITheme - 1) * 20) + ((numberOfOtherThemes - numberOfSSITheme) * 18) + 16;
        for(int index = 0; index < [self.themeView subviews].count; index++){
            //hide other themes
            UIView *subView = [self.themeView subviews][index];
            [subView setHidden:index > numberOfSSITheme - 1];
        }
        self.cst_RightThemeView.constant = -rightPosition;

    }
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _switchNotification.on = [SettingManager shareInstance].isOnNotification;
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CreateUI
- (void)createUI{
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(toogleLeftMenu:)];
    [leftBtn setImageInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    self.navigationItem.leftBarButtonItem = leftBtn;
    [self setupLanguage];
    [self updateTheme:nil];
    //Dropdown
    [self setupDropDown];
    [self changeSettingTouchId:[SettingManager shareInstance].isActivateTouchID];
    _defaultCellHeight = 44;
    
    [self.btn_QuantityValue setTitle:[SettingManager getQuantityString:[[SettingManager shareInstance] currentQuantityType]] forState:UIControlStateNormal];
    [self.btn_QuantityValue setEnabled:[UserPrefConstants singleton].quantityModifierMode];
//    [self.switchQuantity setEnabled:([SettingManager shareInstance].currentQuantityType == 1)];
}
-(void)changeSettingTouchId:(BOOL)actived{
    //TouchID
    NSString *insTouchID = [LanguageManager stringForKey:@"Touch ID"];
    if ([DeviceUtils isFaceIdSupported]) {
        insTouchID = @"Face ID";
    }
    if (actived) {
        _lblTouchID.text=[NSString stringWithFormat:@"Deactivate %@",insTouchID];
    }
    else{
        _lblTouchID.text=[NSString stringWithFormat:@"Activate %@",insTouchID];
    }
}
- (void)setupLanguage{
    self.title = [LanguageManager stringForKey:@"Settings"];
}

- (void)updateTheme:(NSNotification *)noti{
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    [self.tableView reloadData];
}

- (void)setupDropDown{
    _homepageDropdown = [TCDropDown new];
    _homepageDropdown.dataSources = [self getTitlePageList];
    _homepageDropdown.direction = TCDirection_Down;
	_homepageDropdown.isSearchable = NO;
    [_homepageDropdown didSelectItem:^(NSString *item, NSInteger index) {
        self.lblDefaultPageName.text = item;
		[SettingManager shareInstance].defaultPage = [self->_listPages[index] integerValue];
        [[SettingManager shareInstance] save];
        
        [AppDelegateAccessor setHomeVC];
    }];
}
#pragma mark - LoadData
- (void)loadData{
    //Theme
    NSInteger index = [[ThemeManager shareInstance] getCurrentTheme];
    [self selectThemeAt:index];
    //Default Page
    _lblDefaultPageName.text = [Utils textTitleFromPage:[SettingManager shareInstance].defaultPage];
    //Touch ID
    _switchTouchID.on = [SettingManager shareInstance].isActivateTouchID;
    //Notification
    _switchNotification.on = [SettingManager shareInstance].isOnNotification;
    
    _switchPrice.on = [SettingManager shareInstance].isOnPrice;
    _switchQuantity.on = [SettingManager shareInstance].isOnQuantity;
    NSString *quantity = [SettingManager shareInstance].customQuantity;
    if([quantity isEqualToString:@"nil"]){
        quantity = @"";
    }
    _txtcustomQuantity.delegate = self;
    _txtcustomQuantity.text = quantity;
    // Set custom quantity
    if (![SettingManager shareInstance].isOnQuantity) {
        _txtcustomQuantity.enabled=NO;
    }
    else {
        _txtcustomQuantity.enabled=YES;
    }
    
    _switchOrderTypeUI.on = [SettingManager shareInstance].isOnOrderType2;
    
    //Up-down price Color
    _btnUpPriceColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
    _btnDownPriceColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
    //Buy Sell
    _btnBuyColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].buyColor);
    _btnSellColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].sellColor);
    
    [self addNotification:kNotification_DidLoadNotificationSetting selector:@selector(didLoadSettings:)];
    [self changeNotificationStatus];
    
    //Exchange
    ExchangeData *curExData = [Utils getExchange:[SettingManager shareInstance].defaultExchangeCode];
    _lblExchange.text = [Utils getShortExChangeName:curExData];
	
	//Switch Fload Trading Button
	_switchTradeButton.on = [SettingManager shareInstance].enableFloatTradingButton;
}

- (void)didLoadSettings:(NSNotification *)noti{
    [self changeNotificationStatus];
}
- (void)changeNotificationStatus{
    self->_switchNotification.on = [self->_notificationControler isEnableNotification];
}
#pragma MARK - SettingAlertVCDelegate
- (void)didReloadSettingNofi{
    [self.tableView reloadData];
}
#pragma mark - UTILS
- (void)selectThemeAt:(NSInteger)index{
    for (TCBaseView *aV in self.themeView.subviews) {
        for (id contentV in aV.subviews) {
            if ([contentV isKindOfClass:[TCBaseButton class]]) {
                TCBaseButton *btnTheme = (TCBaseButton *)contentV;
                if (btnTheme.tag == index) {
                    btnTheme.borderWidth = 2.0;
                    btnTheme.borderColor = [UIColor whiteColor];
                }else{
                    btnTheme.borderWidth = 0.0;
                    btnTheme.borderColor = [UIColor clearColor];
                    btnTheme.backgroundColor = [UIColor clearColor];
                }
                if (btnTheme.tag == ThemeType_Light) {
                    aV.borderColor = RGB(112, 112, 112);
                    aV.borderWidth = 2.0;
                    btnTheme.borderColor = TC_COLOR_FROM_HEX(@"F5F5F5");
                }
            }
        }
    }
}

- (NSArray *)getTitlePageList{
    NSMutableArray *listTitle = @[].mutableCopy;
    
    for (id page in _listPages) {
        DefaultPage dfPage = [page integerValue];
        NSString *titlePage = [Utils textTitleFromPage:dfPage];
        [listTitle addObject:titlePage];
    }
    return [listTitle copy];
}

- (NSInteger)indexOfSelectedPage:(DefaultPage)page{
    return [_listPages indexOfObject:@(page)];
}

- (BOOL)detectEnableCellAt:(NSIndexPath *)indexPath{
    BOOL isEnable = NO;
    switch (indexPath.section) {
        case MSettingSection_Application:
            isEnable = YES;
            break;
        case MSettingSection_ViewPreference:{
            if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 4) {
                isEnable = YES;
            }else{
                isEnable = NO;
            }
        }
            break;
        case MSettingSection_TradingPreference:{
            if (indexPath.row == 0) {
                isEnable = NO;
            }else{
                isEnable = YES;
            }
        }
            break;
        case MSettingSection_Language:
            isEnable = NO;
            break;
        case MSettingSection_Account:
            isEnable = YES;
            break;
        case MSettingSection_Policy:
            isEnable = NO;
            break;
        default:
            break;
    }
    return isEnable;
}

//Show Popup Exchange Selectione
- (void)showPopupSelectionExchange{
    NSArray *exChangeList = [UserPrefConstants singleton].supportedExchanges.copy;
    if (exChangeList.count == 0) {
        //Can show popup here
        return;
    }
    ExchangeSelectionVC *exVC = NEW_VC_FROM_NIB([ExchangeSelectionVC class], [ExchangeSelectionVC nibName]);
	exVC.type = ExchangeSelection_Default;
    exVC.delegate = self;
    CGSize contentSize = CGSizeMake(0.8 * self.view.frame.size.width, 300);
    if (exChangeList.count < 6) {
        contentSize = CGSizeMake(0.8 * self.view.frame.size.width, kEXCHANGE_CELL_HEIGHT * exChangeList.count + 60);
    }
    [self showPopupWithContent:exVC inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
        //Do something here
    }];
}

//Change Password
- (void) goToChangePassword{
    ChangePasswdViewController *changePasswordVC = [[ChangePasswdViewController alloc] initWithNibName:@"ChangePasswordView" bundle:nil];
    [changePasswordVC setAction_type:CHANGE_PASSWORD_CODE];
    [self.navigationController pushViewController:changePasswordVC animated:NO];
}
//Change Pin
- (void) gotoChangePin{
    ChangePasswdViewController *changePasswordVC = [[ChangePasswdViewController alloc] initWithNibName:@"ChangePasswordView" bundle:nil];
    [changePasswordVC setAction_type:CHANGE_PIN_CODE];
    [self.navigationController pushViewController:changePasswordVC animated:NO];
}
// Check Show Terms And Conditions
-(void)checkShowTermAndCondition{
    if (![SettingManager shareInstance].isAgreeWithTermAndCondition) {
        TermAndConditionVC *_setupTermCondition = NEW_VC_FROM_STORYBOARD(@"Setup", @"TermAndCondition");
        _setupTermCondition.delegate=self;
        [self showPopupWithContent:_setupTermCondition inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.8* SCREEN_HEIGHT_PORTRAIT) showInCenter:YES topInsect:80 completion:^(MZFormSheetController *formSheetController) {
            //Do something here
        }];
    }
}
// After Agree or Decline
- (void)showedTermAndCondition:(BOOL)success{
     [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
		 [SettingManager shareInstance].isAgreeWithTermAndCondition = success;
		 [[SettingManager shareInstance] save];
		 [self enableQuickAccess:success];
     }];
}

#pragma mark - ACTIONS

- (IBAction)onSelectUIType:(id)sender {
    [SettingManager shareInstance].isOnOrderType2 = ![SettingManager shareInstance].isOnOrderType2;
    
    [[SettingManager shareInstance] save];
}
- (IBAction)onSelectQuantity:(id)sender {
    [SettingManager shareInstance].isOnQuantity = ![SettingManager shareInstance].isOnQuantity;
    if (![SettingManager shareInstance].isOnQuantity) {
        _txtcustomQuantity.enabled = NO;
    }
    else{
         _txtcustomQuantity.enabled = YES;
    }
    [SettingManager shareInstance].customQuantity = _txtcustomQuantity.text;
    [[SettingManager shareInstance] save];
}
- (IBAction)onSelectPrice:(id)sender {
    [SettingManager shareInstance].isOnPrice = ![SettingManager shareInstance].isOnPrice;
    [[SettingManager shareInstance] save];
}

- (IBAction)onSelectThemeAction:(TCBaseButton *)sender {
    [[ThemeManager shareInstance] updateNewTheme:sender.tag];
    [self selectThemeAt:sender.tag];
}
- (IBAction)onSelectHomepageAction:(UIButton *)sender {
    _homepageDropdown.sourceView = sender;
    _homepageDropdown.width = 150;
    _homepageDropdown.direction = TCDirection_Any;
    _homepageDropdown.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgDropdownColor);
    _homepageDropdown.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].textTintColor);
    _homepageDropdown.defaltSelectedItem = [self indexOfSelectedPage:[[SettingManager shareInstance] defaultPage]];
    [_homepageDropdown show];
}
- (IBAction)onSwitchTradeButton:(UISwitch *)sender {
	[SettingManager shareInstance].enableFloatTradingButton = ![SettingManager shareInstance].enableFloatTradingButton;
	[[SettingManager shareInstance] save];
	[[NSNotificationCenter defaultCenter] postNotificationName:kDidUpdateSettingFloatTradingButtonNotification object:nil];
}

//TouchID
- (IBAction)onSwitchTouchIdValueChange:(UISwitch *)sender {
    BOOL isActivateTouchID = sender.on;
    // Check is agree terms and condition? If it agreeed allow switch Touch ID
    if (isActivateTouchID) {
		  if (![SettingManager shareInstance].isAgreeWithTermAndCondition) {
			  [self checkShowTermAndCondition];
			  return;
		  }
    }
	if (isActivateTouchID) {
		//Make Active QuickAccess
		[self enableQuickAccess:YES];
	}else{
		//Deactive QuickAccess
		NSString *mWarning = [DeviceUtils isFaceIdSupported] ? @"Are you sure to deactivate FACE ID?" : @"Are you sure to deactivate TOUCH ID?";
		[self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:mWarning] okTitle:[LanguageManager stringForKey:Confirm] withOKAction:^{
			[self enableQuickAccess:NO];
			// Re-setting if Deactive Touch ID
			[SettingManager shareInstance].isAgreeWithTermAndCondition=NO;
			[SettingManager shareInstance].isShowTermAndCondition=NO;
		} cancelTitle:[LanguageManager stringForKey:Cancel] cancelAction:^{
			//Do nothing
			sender.on = YES;
		}];
	}
}

- (void)enableQuickAccess:(BOOL )enable{
	BOOL isActivateTouchID = enable;
	NSString *message = [DeviceUtils isFaceIdSupported] ? @"Setup FaceID was updated successfully" : @"Setup TouchID was updated successfully";
	if (isActivateTouchID) {
		//Do nothings...
		[SettingManager shareInstance].isActivateTouchID = isActivateTouchID;
		[[SettingManager shareInstance] save];
		//Update info QuickAccess
		[[SettingManager shareInstance] saveQuickAccess:[UserSession shareInstance].userName pass:[UserSession shareInstance].password];
		[TCBubbleMessageView showWindowBubbleMessage:message];
	}else{
		//Update info QuickAccess
		[[SettingManager shareInstance] deleteCurrentQuickAccessBroker];
	}
	self.switchTouchID.on = isActivateTouchID;
	[self changeSettingTouchId:isActivateTouchID];
	
}

//Notification
- (IBAction)onSwitchNotification:(UISwitch *)sender {
   
}


//Setting Up-Down Price Color
- (IBAction)onUpDownPriceColorAction:(id)sender {
    [self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:@"Are you sure you want to change?"] okTitle:[LanguageManager stringForKey:@"Confirm"] withOKAction:^{
        NSString *tmpColor = [[SettingManager shareInstance].upPriceColor copy];
        [SettingManager shareInstance].upPriceColor = [SettingManager shareInstance].downPriceColor;
        [SettingManager shareInstance].downPriceColor = tmpColor;
        [[SettingManager shareInstance] save];
        //Update UI
        self->_btnUpPriceColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
        self->_btnDownPriceColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeThemeNotification object:nil];
    } cancelTitle:[LanguageManager stringForKey:@"Cancel"] cancelAction:^{
        
    }];
}

- (IBAction)onBuySellColorAction:(id)sender {
    [self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:@"Are you sure you want to change?"] okTitle:[LanguageManager stringForKey:@"Confirm"] withOKAction:^{
        NSString *tmpColor = [[SettingManager shareInstance].buyColor copy];
        [SettingManager shareInstance].buyColor = [SettingManager shareInstance].sellColor;
        [SettingManager shareInstance].sellColor = tmpColor;
        [[SettingManager shareInstance] save];
        //Update UI
        self->_btnBuyColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].buyColor);
        self->_btnSellColor.backgroundColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].sellColor);
        [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeThemeNotification object:nil];
    } cancelTitle:[LanguageManager stringForKey:@"Cancel"] cancelAction:^{
        
    }];
}


#pragma mark - Actions
- (void)toogleLeftMenu:(id)sender {
    [AppDelegateAccessor toogleLeftMenu:nil];
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //Hide ChangePassword/Pin in Setting base on config flag for each broker
    if (![UserPrefConstants singleton].isEnabledPassword && ![UserPrefConstants singleton].isEnabledPin) {
        if (section == 5) {
            return 0;
        }
    }
    return _defaultCellHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section > 3){
        return _defaultCellHeight;
    }
    //Auto height from Application Section -> Languages Section
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 3;
    }else if(section == 1){
        return 5;
    }else if(section == 2){
        return 6;
    }else if(section == 3){
        return 1;
    }else if(section == 4){
        //Hide ChangePassword/Pin in Setting base on config flag for each broker
        if (![UserPrefConstants singleton].isEnabledPassword && ![UserPrefConstants singleton].isEnabledPin) {
            return 0;
        }else{
            return 3;
        }
        
    }else{
        return 4;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //[cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
    UIView *contentV = [cell.contentView viewWithTag:199];
    for(int i = 0; i < contentV.subviews.count; i++){
        if(i == 0){
            UILabel *label = (UILabel *)contentV.subviews[0];
            label.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
            label.font = AppFont_MainFontBoldWithSize(14);
        }else{
            UILabel *subLabel = (UILabel *)contentV.subviews[1];
            subLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_headerSubtextColor);
            subLabel.font = AppFont_MainFontRegularWithSize(12);
        }
    }
    for (id aView in cell.contentView.subviews) {
        if ([aView isKindOfClass:[UISwitch class]]) {
            UISwitch *sw = (UISwitch *)aView;
            sw.onTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
        }
        if(contentV){
            if ([aView isKindOfClass:[UILabel class]]) {
                UILabel *lblTitle = (UILabel *)aView;
                lblTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
            }
        }else{
            if ([aView isKindOfClass:[UILabel class]]) {
                UILabel *lblTitle = (UILabel *)aView;
                lblTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
            }
        }
        
        if ([aView isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)aView;
            button.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
        }
    }
    
    
    //Check Enable or Not
    if ([self detectEnableCellAt:indexPath]) {
        cell.contentView.alpha = 1.0;
    }else{
        cell.contentView.alpha = 0.3;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    view.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].headerCellColor);
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
    header.textLabel.font = AppFont_MainFontBoldWithSize(14);
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentLeft;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selectionStyle == UITableViewCellSelectionStyleNone) {
        return;
    }
    //Check Enable or Not
    if (![self detectEnableCellAt:indexPath]) {
        return;
    }
    switch (indexPath.section) {
        case MSettingSection_ViewPreference:
            //For Exchange
            if (indexPath.row == 4) {
                [self showPopupSelectionExchange];
            }
            break;
        case MSettingSection_Application:
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            if (indexPath.row == 3) { //Notification
                [self showNotificationVC];
            }
            break;
        case MSettingSection_Account:
            if (indexPath.row == 0) {
                [self goToChangePassword];
            }
            else if (indexPath.row == 1) {
                [self gotoChangePin];
            }
            break;
        default:
            break;
    }
    
    
}
- (void)showNotificationVC{
    SettingAlertVC *vc = NEW_VC_FROM_NIB([SettingAlertVC class], @"SettingAlertVC");
    vc.controller = _notificationControler;
    [self nextTo:vc animate:YES];
}
#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)changeQuantityValue:(id)sender {
    if([SettingManager shareInstance].currentQuantityType == 0){
        [SettingManager shareInstance].currentQuantityType = 1;
    }else{
        [SettingManager shareInstance].currentQuantityType = 0;
    }
    [self.btn_QuantityValue setTitle:[SettingManager getQuantityString:[SettingManager shareInstance].currentQuantityType] forState:UIControlStateNormal];
//    [self.switchQuantity setEnabled:([SettingManager shareInstance].currentQuantityType == 1)];
    [[SettingManager shareInstance] save];
}

#pragma mark - ExchangeSelectionVCDelegate
- (void)didSelectExchange:(ExchangeData *)exData{
    _lblExchange.text = [Utils getShortExChangeName:exData];
    [SettingManager shareInstance].defaultExchangeCode = exData.feed_exchg_code;
    [[SettingManager shareInstance] save];
}
- (IBAction)onInputCustomQuantity:(id)sender {
    [SettingManager shareInstance].customQuantity=_txtcustomQuantity.text;
    [[SettingManager shareInstance] save];
}
-(void)dismissKeyboardTexField:(TouchManager *)touchEvent
{
    if (_txtcustomQuantity.isFirstResponder) {
        [_txtcustomQuantity resignFirstResponder ];
    }
}

#pragma mark UITextFieldDelegate
- (void)textFieldDidChange:(UITextField *)textField {
    [SettingManager shareInstance].customQuantity=_txtcustomQuantity.text;
    [[SettingManager shareInstance] save];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [SettingManager shareInstance].customQuantity = _txtcustomQuantity.text;
    [[SettingManager shareInstance] save];
    return YES;
}
@end
