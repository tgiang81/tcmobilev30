//
//  MyUninteractiveSettingButton.m
//  TC_Mobile30
//
//  Created by Kaka on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MyUninteractiveSettingButton.h"
#import "AllCommon.h"

@implementation MyUninteractiveSettingButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)layoutSubviews{
	[super layoutSubviews];
	self.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTintColor);
	[self setUserInteractionEnabled:NO];
	[self setContentMode:UIViewContentModeScaleAspectFit];
}
@end
