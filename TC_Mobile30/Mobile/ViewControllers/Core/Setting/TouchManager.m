//
//  TouchManager.m
//  TC_Mobile30
//
//  Created by Admin on 3/25/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TouchManager.h"
#import <UIkit/UIGestureRecognizerSubclass.h>

@implementation TouchManager:UIGestureRecognizer
-(id)initWithTarget:(id)target action:(SEL)action
{
    if ((self=[super initWithTarget:target action:action])) {
        
    }
    return  self;
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    self.state = UIGestureRecognizerStateRecognized;
    [self reset];
    NSLog(@"Touch End");
}
- (void)reset
{
    [super reset];
}
@end
