//
//  SettingVC.m
//  TCiPad
//
//  Created by Kaka on 6/15/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "SettingVC.h"
#import "SettingCell.h"


static CGFloat kHEIGHT_UNKNOWN_CELL = 30;
static CGFloat kHEIGHT_NORMAL_CELL	= 44;

@interface SettingVC ()<UITableViewDataSource, UITableViewDelegate>{
	NSMutableArray *_settingItems;
}
@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//Initial data
	_settingItems = @[].mutableCopy;
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self createLeftMenu];
	[_tblContent registerNib:[UINib nibWithNibName:[SettingCell nibName] bundle:nil] forCellReuseIdentifier:[SettingCell reuseIdentifier]];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_tblContent.backgroundColor = [UIColor clearColor];
}
- (void)setupLanguage{
	self.customBarTitle = [LanguageManager stringForKey:@"Settings"];
}

#pragma mark - LoadData
- (void)loadData{
	_settingItems = @[@(SettingType_ChangePassword), @(SettingType_ChangePin), @(SettingType_ForgotPin), @(SettingType_ActiveTouchID), @(SettingType_PushNotification), @(SettingType_Unknown), @(SettingType_RefreshRate), @(SettingType_Unknown), @(SettingType_ViewPreference), @(SettingType_Unknown), @(SettingType_AboutUs), @(SettingType_ContactUs), @(SettingType_RateUs), @(SettingType_TermOfService), @(SettingType_PrivacyPolicy), @(SettingType_DisclaimerOfWarranty), @(SettingType_Notification)].mutableCopy;
	[_tblContent reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_settingItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	SettingType type = [_settingItems[indexPath.row] integerValue];
	if (type == SettingType_Unknown) {
		return kHEIGHT_UNKNOWN_CELL;
	}
	return kHEIGHT_NORMAL_CELL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:[SettingCell reuseIdentifier]];
	if (!cell) {
		cell = [[SettingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[SettingCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	SettingType infoType = [_settingItems[indexPath.row] integerValue];
	[cell setupDataFrom:infoType];
	return cell;
}

#pragma mark - UITableViewDelegate
- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"Will Selected At Index Path : %@", @(indexPath.row));
	return indexPath;
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	return YES;
}
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did highlight");
	SettingCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell setDataShouldHighlight:YES];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did unhighlight");
	SettingCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell setDataShouldHighlight:NO];
}

//DidSelect Cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}
@end
