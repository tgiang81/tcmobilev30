//
//  MSettingVC.h
//  TC_Mobile30
//
//  Created by Kaka on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TermAndConditionVC.h"
@class AlertController;
@interface MSettingVC : UITableViewController<TransferDataDelegate>
@property(strong, nonatomic) AlertController *notificationControler;
- (void)changeNotificationStatus;
@property (weak, nonatomic) IBOutlet UITableViewCell *customQuantity;
@property (weak, nonatomic) IBOutlet UITextField *txtcustomQuantity;
@end
