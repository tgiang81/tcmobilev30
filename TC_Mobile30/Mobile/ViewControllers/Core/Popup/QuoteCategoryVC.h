//
//  QuoteCategoryVC.h
//  TCiPad
//
//  Created by Kaka on 4/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@protocol QuoteCategoryVCDelegate<NSObject>
@optional
- (void)didSelectRankName:(NSString *)rankName;
@end

@interface QuoteCategoryVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (strong, nonatomic) NSString *selectedRankName;
@property (weak, nonatomic) id <QuoteCategoryVCDelegate> delegate;

@end
