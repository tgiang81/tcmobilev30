//
//  SelectWatchlistPopupVC.h
//  TC_Mobile30
//
//  Created by Kaka on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
@class TCBaseButton;
@class StockModel;
@class WatchlistModel;
@protocol SelectWatchlistPopupVCDelegate <NSObject>
@optional
//Use this bellow
- (void)didSelectStock:(StockModel *)stock willAddToWatchlist:(WatchlistModel *)watchlist;
@end
@interface SelectWatchlistPopupVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet TCBaseButton *btnClose;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnApply;

@property (weak, nonatomic) IBOutlet UITableView *tblContent;
//Passing data
@property (strong, nonatomic) StockModel *stock;
@property (weak, nonatomic) id <SelectWatchlistPopupVCDelegate>delegate;
@end
