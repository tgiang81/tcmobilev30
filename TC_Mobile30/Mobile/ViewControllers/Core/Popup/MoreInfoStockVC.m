//
//  MoreInfoStockVC.m
//  TCiPad
//
//  Created by Kaka on 5/15/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MoreInfoStockVC.h"
#import "MoreInfoStockCell.h"
#import "ChartVC.h"
#import "StockModel.h"

@interface MoreInfoStockVC ()<UITableViewDelegate, UITableViewDataSource>{
	NSMutableArray *_items;
}

@end

@implementation MoreInfoStockVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	//Initial first
	_items = @[].mutableCopy;
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self.tblContent registerNib:[UINib nibWithNibName:[MoreInfoStockCell nibName] bundle:nil] forCellReuseIdentifier:[MoreInfoStockCell reuseIdentifier]];
	//Remove header and footer line
	//self.tblContent.tableHeaderView = nil;
	//self.tblContent.tableFooterView = nil;
	[_tblContent setBounces:NO];
}
#pragma mark - LoadData
- (void)loadData{
	_items = @[@(StockMoreInfoType_Fundamentals), @(StockMoreInfoType_News), @(StockMoreInfoType_Chat), @(StockMoreInfoType_TimeSale), @(StockMoreInfoType_Close)].mutableCopy;
	[_tblContent reloadData];
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MoreInfoStockCell *cell = [tableView dequeueReusableCellWithIdentifier:[MoreInfoStockCell reuseIdentifier]];
	if (!cell) {
		cell = [[MoreInfoStockCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MoreInfoStockCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	StockMoreInfoType infoType = [_items[indexPath.row] integerValue];
	[cell updateData:infoType];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"did Selected At index %@", @(indexPath.row));
	StockMoreInfoType infoType = [_items[indexPath.row] integerValue];
	/*
	switch (infoType) {
		case StockMoreInfoType_Fundamentals:{
			ChartVC *_chartVC = NEW_VC_FROM_NIB([ChartVC class], [ChartVC nibName]);
			_chartVC.stockCode = _stock.stockCode;
			_chartVC.isOnlyImageChart = NO;
			_chartVC.chartType = ChartType_3Month;
			[self dismissPopup:NO completion:^(MZFormSheetController *formSheetController) {
				[AppDelegateAccessor.container presentViewController:_chartVC animated:YES completion:^{
					//Do nothing
				}];
			}];
		}
			break;
		case StockMoreInfoType_Close:
			//Close popup and do nothing
			[self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
				DLog(@"+++ dismissed popup");
			}];
			break;
			
		default:
			break;
	}
    */
	BOOL animated = NO;
	if (infoType == StockMoreInfoType_Close) {
		animated = YES;
	}
	[self dismissPopup:animated completion:^(MZFormSheetController *formSheetController) {
		if (_delegate) {
			[_delegate didTapMoreInfo:self atType:infoType];
		}
	}];
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"Will Selected At Index Path : %@", @(indexPath.row));
	return indexPath;
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	return YES;
}
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did highlight");
	MoreInfoStockCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell setDataShouldHighlight:YES];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did unhighlight");
	MoreInfoStockCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell setDataShouldHighlight:NO];
}
@end
