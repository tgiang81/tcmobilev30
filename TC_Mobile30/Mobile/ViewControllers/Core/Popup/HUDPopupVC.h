//
//  HUDPopupVC.h
//  TCiPad
//
//  Created by Kaka on 5/5/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class DGActivityIndicatorView;
@interface HUDPopupVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet DGActivityIndicatorView *activityView;

@property (weak, nonatomic) IBOutlet UIVisualEffectView *visualEffectView;

@end
