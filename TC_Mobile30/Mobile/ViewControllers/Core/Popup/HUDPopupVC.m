//
//  HUDPopupVC.m
//  TCiPad
//
//  Created by Kaka on 5/5/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "HUDPopupVC.h"
#import "DGActivityIndicatorView.h"
@interface HUDPopupVC ()

@end

@implementation HUDPopupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self setup];
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
	[_activityView stopAnimating];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Setup
- (void)setup{
	self.view.backgroundColor = [UIColor clearColor];
	_visualEffectView.backgroundColor = RGBA(31, 33, 36,1.0);
	_activityView.backgroundColor = [UIColor clearColor];
	_activityView.tintColor = AppColor_TintColor;
	_activityView.type = DGActivityIndicatorAnimationTypeNineDots;
	[_activityView startAnimating];
}
@end
