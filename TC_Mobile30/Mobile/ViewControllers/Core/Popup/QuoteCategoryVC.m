//
//  QuoteCategoryVC.m
//  TCiPad
//
//  Created by Kaka on 4/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "QuoteCategoryVC.h"

@interface QuoteCategoryVC ()<UITableViewDelegate, UITableViewDataSource>{
	NSMutableArray *_rankNameArr;
}

@end

@implementation QuoteCategoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_rankNameArr = [Utils rankedByNameArr];
	[_tblContent reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_rankNameArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellID = @"CELL";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellID];
	}
	if (_selectedRankName && [_selectedRankName isEqualToString:_rankNameArr[indexPath.row]]) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}else{
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	cell.textLabel.text = _rankNameArr[indexPath.row];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	_selectedRankName = _rankNameArr[indexPath.row];
	[self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
		if (_delegate) {
			[_delegate didSelectRankName:_selectedRankName];
		}
	}];
}
@end
