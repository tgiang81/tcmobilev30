//
//  MoreWatchlistVC.h
//  TCiPad
//
//  Created by Kaka on 6/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class MoreWatchlistVC;

@protocol MoreWatchlistVCDelegate <NSObject>
@optional
- (void)didTapMoreWatchlist:(MoreWatchlistVC *)moreWatchlistVC atType:(MoreItemType)type;
@end
@interface MoreWatchlistVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

//Passing
@property (weak, nonatomic) id <MoreWatchlistVCDelegate> delegate;
@end
