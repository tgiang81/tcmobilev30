//
//  SelectWatchlistPopupVC.m
//  TC_Mobile30
//
//  Created by Kaka on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SelectWatchlistPopupVC.h"
#import "ATPAuthenticate.h"
#import "WatchlistModel.h"
#import "TCBaseButton.h"
#import "LanguageKey.h"

@interface SelectWatchlistPopupVC ()<UITableViewDelegate, UITableViewDataSource>{
	NSMutableArray *_watchlistArr;
	DGActivityIndicatorView *_activity;
	NSIndexPath *_selectedIndexPath;
	
	//UI
	UILabel *_lblStatus;
}
@end

@implementation SelectWatchlistPopupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_watchlistArr = @[].mutableCopy;
    [self setupLanguage];
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	self.tblContent.delegate = self;
	self.tblContent.dataSource = self;
}
-(void)setupLanguage{
    [_btnApply setTitle:[LanguageManager stringForKey:Apply] forState:UIControlStateNormal];
    [_btnClose setTitle:[LanguageManager stringForKey:Close] forState:UIControlStateNormal];
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	_btnClose.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].deleteColor);
	_btnApply.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
}

#pragma mark - LoadData
- (void)loadData{
	[self showActivity];
	[[ATPAuthenticate singleton] getWatchlistWithCompletion:^(BOOL success, id result, NSError *error) {
		[self hideActivity];
		if (success) {
			self->_watchlistArr = [WatchlistModel arrayFromDicts:result];
			[self->_tblContent reloadData];
			//Check show status Data
			[self showStatusData:(self->_watchlistArr.count == 0) message:nil];
		}else{
			[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedDescription];
		}
	}];
}

- (void)showStatusData:(BOOL)shouldShow message:(NSString *)message{
	_tblContent.hidden = shouldShow;
	if (!_lblStatus) {
		_lblStatus = [[UILabel alloc] initWithFrame:self.tblContent.frame];
		_lblStatus.textAlignment = NSTextAlignmentCenter;
		_lblStatus.font = AppFont_MainFontMediumWithSize(20);
		_lblStatus.textColor = [UIColor purpleColor];
		[self.view addSubview:_lblStatus];
	}
	_lblStatus.hidden = !shouldShow;
	if (!message) {
		message = [LanguageManager stringForKey:No_WatchList];
	}
	_lblStatus.text = message;
}

#pragma mark - UTILS
//+++ Show/Hide Activity
- (void)showActivity{
	if(!_activity){
		_activity = [[Utils shareInstance]createPrivateActivity];
	}
	[[Utils shareInstance] showActivity:_activity inView:self.tblContent];
}

- (void)hideActivity{
	[[Utils shareInstance] dismissActivity:_activity];
}

#pragma mark - ACTIONS

- (IBAction)onCloseAction:(id)sender {
	[self dismissPopup:YES completion:nil];
}

- (IBAction)onApplyAction:(id)sender {
	if (!_selectedIndexPath) {
		[self showCustomAlertWithMessage:[LanguageManager stringForKey:Opp__No_Watchlist_selected__]];
		return;
	}
	[self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
		//Passing data here
		WatchlistModel *model = self->_watchlistArr[self->_selectedIndexPath.row];
		if (self->_delegate) {
			if (self.stock) {
                if ([self->_delegate respondsToSelector:@selector(didSelectStock:willAddToWatchlist:)]) {
                    [self->_delegate didSelectStock:self->_stock willAddToWatchlist:model];
				}
			}
		}
	}];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_watchlistArr count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CELLID = @"SELECT_WATCHLIST_CELL";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELLID];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELLID];
	}
	cell.tintColor = AppColor_TintColor;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.font = AppFont_MainFontMediumWithSize(12);
	cell.textLabel.textAlignment = NSTextAlignmentLeft;
	cell.textLabel.textColor = (indexPath == _selectedIndexPath) ? AppColor_TintColor : RGB(66, 90, 108);
	//Passing data
	cell.accessoryType = (indexPath == _selectedIndexPath) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
	WatchlistModel *model = _watchlistArr[indexPath.row];
	cell.textLabel.text = model.Name;
	return cell;
}
//Cuongnx (28/03/2019) Add Title for List of Watchlist
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 32)];
    headerView.backgroundColor=[UIColor colorWithWhite:1.0f alpha:1.0f];
    headerView.layer.borderColor=[UIColor colorWithWhite:1.0f alpha:1.0f].CGColor;
    headerView.layer.borderWidth=0.0;
    UILabel *headerLabel=[[UILabel alloc] init];
    headerLabel.frame=CGRectMake(14, 8, tableView.frame.size.width, 24);
    headerLabel.backgroundColor=[UIColor whiteColor];
    headerLabel.textColor=[UIColor blackColor];
    headerLabel.font=[UIFont boldSystemFontOfSize:18];
    headerLabel.text=Add_to_Watchlist;
    [headerView addSubview:headerLabel];
    return headerView;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	_selectedIndexPath = indexPath;
	[self.tblContent reloadData];
}
@end
