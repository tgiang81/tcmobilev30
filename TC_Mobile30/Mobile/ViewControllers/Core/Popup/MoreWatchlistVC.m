//
//  MoreWatchlistVC.m
//  TCiPad
//
//  Created by Kaka on 6/1/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MoreWatchlistVC.h"
#import "MoreItemCell.h"

@interface MoreWatchlistVC ()<UITableViewDelegate, UITableViewDataSource>{
	NSMutableArray *_items;
}

@end

@implementation MoreWatchlistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_items = @[].mutableCopy;
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self.tblContent registerNib:[UINib nibWithNibName:[MoreItemCell nibName] bundle:nil] forCellReuseIdentifier:[MoreItemCell reuseIdentifier]];
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	//Remove header and footer line
	//self.tblContent.tableHeaderView = nil;
	//self.tblContent.tableFooterView = nil;
	[_tblContent setBounces:NO];
}

#pragma mark - LoadData
- (void)loadData{
	_items = @[@(MoreItemType_AddNew), @(MoreItemType_Rename), @(MoreItemType_Delete), @(MoreItemType_Close)].mutableCopy;
	[_tblContent reloadData];
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MoreItemCell *cell = [tableView dequeueReusableCellWithIdentifier:[MoreItemCell reuseIdentifier]];
	if (!cell) {
		cell = [[MoreItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MoreItemCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	MoreItemType infoType = [_items[indexPath.row] integerValue];
	[cell setupDataFrom:infoType];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"did Selected At index %@", @(indexPath.row));
	MoreItemType infoType = [_items[indexPath.row] integerValue];
	BOOL animated = NO;
	if (infoType != MoreItemType_AddNew) {
		animated = YES;
	}
	__weak MoreWatchlistVC *weakSelf = self;
	[self dismissPopup:animated completion:^(MZFormSheetController *formSheetController) {
		if (weakSelf.delegate) {
			[weakSelf.delegate didTapMoreWatchlist:weakSelf atType:infoType];
		}
	}];
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"Will Selected At Index Path : %@", @(indexPath.row));
	return indexPath;
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	return YES;
}
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did highlight");
	MoreItemCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell setDataShouldHighlight:YES];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did unhighlight");
	MoreItemCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell setDataShouldHighlight:NO];
}
@end
