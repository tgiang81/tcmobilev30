//
//  MoreInfoStockVC.h
//  TCiPad
//
//  Created by Kaka on 5/15/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class StockModel;
@class MoreInfoStockVC;

@protocol MoreInfoStockVCDelegate <NSObject>
@optional
- (void)didTapMoreInfo:(MoreInfoStockVC *)moreInfoVC atType:(StockMoreInfoType)type;
@end
@interface MoreInfoStockVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

//Passing data
@property (strong, nonatomic) StockModel *stock;
@property (weak, nonatomic) id <MoreInfoStockVCDelegate> delegate;
@end
