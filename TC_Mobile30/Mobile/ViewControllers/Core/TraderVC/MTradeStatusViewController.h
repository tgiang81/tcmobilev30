//
//  MTradeStatusViewController.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/14/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TradeStatusViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTradeStatusViewController : TradeStatusViewController
@property (strong, nonatomic) NSArray *bizDoneTableDataArr;
@property (strong, nonatomic) NSArray *arrMarketDepthItems;
@property (strong, nonatomic) NSArray *timeSaleArr;
@property (strong, nonatomic) NSString *stockCode;
@end

NS_ASSUME_NONNULL_END
