//
//  TraderViewController.h
//  TCiPad
//
//  Created by ngo phi long on 5/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "OrderDetails.h"
#import "UserPrefConstants.h"
#import "UIImageUtility.h"
#import "OrderControl.h"
#import "AdditionalPaymentData.h"
#import "QCData.h"
#import "QCConstants.h"
#import "ATPAuthenticate.h"

#import "MSkipConfirmationViewController.h"
#import "MTradeStatusViewController.h"
#import "MValidatePINViewController.h"
#import "QuantityPadViewController.h"
#import "PricePadViewController.h"

#import "MarqueeLabel.h"
#import "DGActivityIndicatorView.h"
#import "UserAccountClientData.h"
#import "NewsModalViewController.h"
#import "TransitionDelegate.h"
#import "ExchangeData.h"
#import "LanguageManager.h"
#import "CurrencyRate.h"
#import "N2DatePicker.h"
#import "GTMRegex.h"
#import "BaseVC.h"
#import "BUYSELL_Cell.h"
#import "SearchingDropDownVC.h"
#import "CheckBoxView.h"
typedef enum : NSInteger{
    TradeUIType1 = 0,
    TradeUIType2
}TradeUIType;

typedef enum : NSInteger{
    TradeContentModeTrade = 0,
    TradeContentModeConfirm,
    TradeContentModeStatus
}TradeContentModeType;
@class StockModel;
@interface MTradeViewController : SearchingDropDownVC
{
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Bottom;
@property (weak, nonatomic) IBOutlet UIView *v_InputKeyboard;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_InputKeyboardBottom;

@property (weak, nonatomic) IBOutlet UIView *v_Bottom;
@property (strong, nonatomic) StockModel *stock;
@property (strong, nonatomic) NSArray *tradeObject;
@property (strong, nonatomic) NSDictionary *actionTypeDict;
@property (weak, nonatomic) IBOutlet TCBaseButton *btn_submit;
@property (weak, nonatomic) IBOutlet TCBaseButton *btn_buy;
@property (weak, nonatomic) IBOutlet TCBaseButton *btn_sell;

@property (weak, nonatomic) IBOutlet UIStackView *st_bottomUI2;
@property (strong, nonatomic) NSArray *bizDoneTableDataArr;
@property (assign) TradeType tradeType;
@property (assign, nonatomic) BOOL isRevise;
@property (assign, nonatomic) BOOL isOrderBook;
@property (assign, nonatomic) BOOL willCancelOrder;
@property (assign, nonatomic) BOOL isNotFromTradeButton;
@property (assign, nonatomic) TradeUIType uiType;
@property (assign, nonatomic) TradeContentModeType contentMode;

//SSI
@property (weak, nonatomic) IBOutlet UIView *v_ActionSSI;
@property (weak, nonatomic) IBOutlet CheckBoxView *v_ShortSellSSI;
@property (weak, nonatomic) IBOutlet CheckBoxView *v_SkipSSI;

@end
