//
//  OverlayVC.m
//  TCiPad
//
//  Created by giangtu on 7/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "OverlayVC.h"

@interface OverlayVC ()
{
    OverlayVCCallback callback;
}
@end

@implementation OverlayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.alpha = 0;

}

-(void)doBlock:(OverlayVCCallback ) cb
{
    callback = [cb copy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) fnDone {
    if (callback) {
        callback(0);
    }
    
    [self toggleOverlay];
}
- (IBAction) toggleOverlay {

    [UIView animateWithDuration:0.2f animations:^(void) {
        
        if (self.view.alpha == 0) {
            self.view.alpha = 1;
            _mPickerView.hidden = NO;
        }
        else {
            self.view.alpha = 0;
            _mPickerView.hidden = TRUE;
        }
    }];
}

@end
