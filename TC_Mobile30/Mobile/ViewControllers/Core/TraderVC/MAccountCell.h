//
//  MAccountCell.h
//  TC_Mobile30
//
//  Created by n2nconnect on 13/09/2018.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserAccountClientData.h"
#import "UserPrefConstants.h"

@interface MAccountCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *accountNumber;
@property (strong, nonatomic) IBOutlet UIView *selectedView;


-(void)populateItemsAtIndexPath:(NSIndexPath*)indexPath withArray:(NSMutableArray*)array;

@end
