//
//  MarketDepthHeader.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

@interface MarketDepthHeader : TCBaseSubView
@property (weak, nonatomic) IBOutlet UILabel *lbl_ask;
@property (weak, nonatomic) IBOutlet UILabel *lbl_bid;
@property (weak, nonatomic) IBOutlet UILabel *lbl_qty;
@property (weak, nonatomic) IBOutlet UILabel *lbl_bQty;
@property (weak, nonatomic) IBOutlet UILabel *lbl_No;
@property (weak, nonatomic) IBOutlet UILabel *lbl_left;
@property (weak, nonatomic) IBOutlet UILabel *lbl_right;

- (void)setupTextColor;
@end
