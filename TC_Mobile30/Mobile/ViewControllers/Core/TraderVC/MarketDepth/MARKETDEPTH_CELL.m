//
//  MARKETDEPTH_CELL.m
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MARKETDEPTH_CELL.h"

@implementation MARKETDEPTH_CELL

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellBg);
    _mdIndex.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellBg);
    _buySplit.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    _buyQty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    
    _buyBid.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    
    _sellAsk.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    
    _sellQty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    _sellSplit.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    
    _lbl_No.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    _lbl_left.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    _lbl_right.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//======== Parse Data to UI =========
- (void)setupDataFrom:(MarketDepthModel *)model comparePrice:(float)refPrice atIndexPath:(NSIndexPath *)indexPath{
    
    if (model) {
        _lbl_No.text = [NSString stringWithFormat:@"%@", @(indexPath.row)]; //indexPath.row + 1
        
        _buyQty.text   = [model.buyQty abbreviateNumber] ;
        
        _buyBid.text = [model.buyPrice toCurrencyNumber];
        
        _sellAsk.text = [model.sellPrice toCurrencyNumber];
        
        _sellQty.text = [model.sellQty abbreviateNumber];
        
        _lbl_left.text = model.buySplit1;
        _lbl_right.text = model.sellSplit1;
        
        //Check Color for Price label
//        _buyBid.textColor = [model.buyPrice colorByCompareToPriceExtend:refPrice];
//        _sellAsk.textColor = [model.sellPrice colorByCompareToPriceExtend:refPrice];;
    }
}

@end
