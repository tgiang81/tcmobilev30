//
//  MarketDepthNewCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 4/2/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class AroundShadowView;
@interface MarketDepthNewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet AroundShadowView *v_Border;

@property (weak, nonatomic) IBOutlet AroundShadowView *v_MarketDepth;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_TimeSale;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_BDone;
@property (weak, nonatomic) IBOutlet UIPageControl *page_Control;
@property (weak, nonatomic) IBOutlet UIScrollView *scv_Content;
- (void)showNoData;
- (void)removeNoData;
@end

NS_ASSUME_NONNULL_END
