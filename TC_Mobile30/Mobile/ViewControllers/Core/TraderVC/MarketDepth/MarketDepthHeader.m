//
//  MarketDepthHeader.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MarketDepthHeader.h"
#import "ThemeManager.h"
#import "LanguageManager.h"
#import "LanguageKey.h"
@implementation MarketDepthHeader
- (void)setupTextColor{
    self.lbl_ask.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_bid.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_qty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_bQty.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    
    self.lbl_No.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_left.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_right.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    
}
- (void)awakeFromNib{
    [super awakeFromNib];
    
    
    self.lbl_ask.text = [LanguageManager stringForKey:Ask];
    self.lbl_bid.text = [LanguageManager stringForKey:Bid];
    self.lbl_qty.text = [LanguageManager stringForKey:A__Qty];
    self.lbl_bQty.text = [LanguageManager stringForKey:B__Qty];
    
    self.lbl_No.text = [LanguageManager stringForKey:No__];
    self.lbl_left.text = [LanguageManager stringForKey:@"#"];
    self.lbl_right.text = [LanguageManager stringForKey:@"#"];
}

@end
