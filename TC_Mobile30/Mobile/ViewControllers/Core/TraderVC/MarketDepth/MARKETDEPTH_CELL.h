//
//  MARKETDEPTH_CELL.h
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarketDepthModel.h"
#import "NSNumber+Formatter.h"
#import "BaseTableViewCell.h"
@interface MARKETDEPTH_CELL : BaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *mdIndex;

@property (strong, nonatomic) IBOutlet UILabel *buySplit;
@property (strong, nonatomic) IBOutlet UILabel *buyQty;
@property (strong, nonatomic) IBOutlet UILabel *buyBid;

@property (strong, nonatomic) IBOutlet UILabel *sellAsk;
@property (strong, nonatomic) IBOutlet UILabel *sellQty;
@property (strong, nonatomic) IBOutlet UILabel *sellSplit;

@property (strong, nonatomic) IBOutlet UIView *mdBuyBgView;
@property (strong, nonatomic) IBOutlet UIView *mdSellBgView;
@property (weak, nonatomic) IBOutlet UILabel *lbl_No;
@property (weak, nonatomic) IBOutlet UILabel *lbl_left;
@property (weak, nonatomic) IBOutlet UILabel *lbl_right;



- (void)setupDataFrom:(MarketDepthModel *)model comparePrice:(float)refPrice atIndexPath:(NSIndexPath *)indexPath;

@end
