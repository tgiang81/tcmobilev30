//
//  MarketInfoCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MarketInfoCell.h"
#import "MARKETDEPTH_CELL.h"
#import "MarketDepthHeader.h"
#import "TimeSaleCell.h"
#import "TimeSaleHeader.h"
#import "TCStatusInfoView.h"
#import "AroundShadowView.h"
#import "MBusinessDoneCell.h"
#import "BidDoneHeader.h"
#import "QCData.h"
#import "LanguageKey.h"
#import "MarketDepthModel.h"
#import "NSNumber+Formatter.h"
typedef enum MarketInfoType : NSUInteger {
    MarketInfoType_MarketDepth,
    MarketInfoType_TimeSell
} MarketInfoType;
@interface MarketInfoCell() <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSArray *numberOfMarketDeep;
@property (strong, nonatomic) NSArray *numberOfTimeSale;
@end
@implementation MarketInfoCell
{
    UITableView *tblbDone;
    UITableView *tblMarketDeep;
    UITableView *tblTimeSale;
    CGFloat cellHeight;
    CGFloat headerCellHeight;
    CGFloat maxHeight;
    NSInteger maxLines;
    NSInteger previousPage;
    CGFloat viewHeight;
    BOOL isExpand;
    TCStatusInfoView *_statusView;
    UIView *tmpViewMD;
    UIView *tmpViewTL;
    UIView *tmpViewBDone;
    CGFloat _lacpValue;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellBg);
    self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.pageControl.currentPageIndicatorTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    self.lbl_TotalAsk.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_TotalBid.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_TotalAskValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_TotalBidValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    
    cellHeight = 30;
    headerCellHeight = 35;
    maxHeight = cellHeight*3 + headerCellHeight;
    _statusView = [TCStatusInfoView new];
    [_statusView justShowStatus:[LanguageManager stringForKey:Loading______] inView:self.contentView];
    
    // Initialization code
}

- (void)setMarketData:(NSArray *)marketDeep timeSale:(NSArray *)timeSale bDone:(NSArray *)bDone stockCode:(NSString*)stockCode width:(CGFloat)width{
    [_statusView removeFromSuperview];
    _lacpValue = [[[[[QCData singleton] qcFeedDataDict]objectForKey:stockCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
    [self.pageControl setHidden:NO];
    [self.scv_content setHidden:NO];
    self.numberOfMarketDeep = marketDeep;
    self.numberOfTimeSale = timeSale;
    self.bizDoneTableDataArr = bDone;
    maxLines = self.numberOfMarketDeep.count > self.numberOfTimeSale.count ? self.numberOfMarketDeep.count : self.numberOfTimeSale.count;
    [self setupScrollView:width];
    [tmpViewMD removeFromSuperview];
    [tmpViewTL removeFromSuperview];
    [tmpViewBDone removeFromSuperview];
    if(marketDeep.count == 0){
        self.lbl_TotalBidValue.text = @"";
        self.lbl_TotalAskValue.text = @"";
        [tblMarketDeep setHidden:YES];
        CGRect tmpRect = CGRectMake(tblMarketDeep.frame.origin.x, headerCellHeight + 16, tblMarketDeep.bounds.size.width, tblMarketDeep.bounds.size.height);
        tmpViewMD = [[UIView alloc] initWithFrame:tmpRect];
        TCStatusInfoView *tmpstatusView = [TCStatusInfoView new];
        [_scv_content addSubview:tmpViewMD];
        [tmpstatusView justShowStatus:[LanguageManager stringForKey:No_Data] inView:tmpViewMD];
    }else{
        NSArray *values = [self numberOfBuyAndSell];
        self.lbl_TotalBidValue.text = values[0];
        self.lbl_TotalAskValue.text = values[1];
        [tblMarketDeep setHidden:NO];
    }
    if(timeSale.count == 0){
        [tblTimeSale setHidden:YES];
        CGRect tmpRect = CGRectMake(tblTimeSale.frame.origin.x, headerCellHeight + 16, tblTimeSale.bounds.size.width, tblMarketDeep.bounds.size.height);
        tmpViewTL = [[UIView alloc] initWithFrame:tmpRect];
        TCStatusInfoView *tmpstatusView = [TCStatusInfoView new];
        [_scv_content addSubview:tmpViewTL];
        [tmpstatusView justShowStatus:[LanguageManager stringForKey:No_Data] inView:tmpViewTL];
    }else{
        [tblTimeSale setHidden:NO];
    }
    
    if(bDone.count == 0){
        [tblbDone setHidden:YES];
        CGRect tmpRect = CGRectMake(tblbDone.frame.origin.x, headerCellHeight + 16, tblbDone.bounds.size.width, tblMarketDeep.bounds.size.height);
        tmpViewBDone = [[UIView alloc] initWithFrame:tmpRect];
        TCStatusInfoView *tmpstatusView = [TCStatusInfoView new];
        [_scv_content addSubview:tmpViewBDone];
        [tmpstatusView justShowStatus:[LanguageManager stringForKey:No_Data] inView:tmpViewBDone];
    }else{
        [tblbDone setHidden:NO];
    }
}
- (void)setupScrollView:(CGFloat)width{
    [_scv_content layoutIfNeeded];
//    viewHeight = cellHeight * maxLines;
//    if(viewHeight > maxHeight){
//        viewHeight = maxHeight;
//    }
    self.cst_Height.constant = maxHeight;
    if(tblMarketDeep == nil){
        _scv_content.delegate = self;
        tblMarketDeep = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                      0,
                                                                      width,
                                                                      maxHeight)];
        [tblMarketDeep setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        tblMarketDeep.allowsSelection = true;
        tblMarketDeep.delegate = self;
        tblMarketDeep.dataSource = self;
        tblMarketDeep.backgroundColor = [UIColor clearColor];
        [tblMarketDeep registerNib:[UINib nibWithNibName:@"MARKETDEPTHCell" bundle:nil] forCellReuseIdentifier:@"MARKETDEPTH_CELL"];
        [_scv_content addSubview:tblMarketDeep];
        
        tblTimeSale = [[UITableView alloc] initWithFrame:CGRectMake(width,
                                                                    0,
                                                                    width,
                                                                    maxHeight)];
        [tblTimeSale setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [tblTimeSale registerNib:[UINib nibWithNibName:@"TimeSaleCell" bundle:nil] forCellReuseIdentifier:@"TimeSaleCell"];
        tblTimeSale.backgroundColor = [UIColor clearColor];
        tblTimeSale.delegate = self;
        tblTimeSale.dataSource = self;
        [_scv_content addSubview:tblTimeSale];
        
        tblbDone = [[UITableView alloc] initWithFrame:CGRectMake(2*width,
                                                                    0,
                                                                    width,
                                                                    maxHeight)];
        [tblbDone setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [tblbDone registerNib:[UINib nibWithNibName:[MBusinessDoneCell nibName] bundle:nil] forCellReuseIdentifier:[MBusinessDoneCell reuseIdentifier]];
        tblbDone.backgroundColor = [UIColor clearColor];
        tblbDone.delegate = self;
        tblbDone.dataSource = self;
        [_scv_content addSubview:tblbDone];
        
        _scv_content.delaysContentTouches = NO;
        [_scv_content setContentSize:CGSizeMake(width*3, 0)];
        [self moveToPage:0 animated:NO];
    }else{
        [tblMarketDeep reloadData];
        [tblTimeSale reloadData];
        [tblbDone reloadData];
    }
    
}
- (NSString *)getTitleWith:(MarketInfoType)type{
    if(type == MarketInfoType_TimeSell){
        return [LanguageManager stringForKey:Time___Sales];
    }else{
        return [LanguageManager stringForKey:Market_Depth];
    }
}

- (void)moveToPage:(NSInteger)page animated:(BOOL)animated{
    CGRect frame = _scv_content.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [_scv_content setContentOffset:frame.origin animated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView.contentOffset.x == 0){
        return;
    }
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if (previousPage != page) {
        previousPage = page;
        self.pageControl.currentPage = page;
        
        if([self.delegate respondsToSelector:@selector(expandRow:)]){
            [self.delegate expandRow:[self getTitleWith:page]];
        }
    }
    [self.v_totalAsk setHidden:previousPage != 0];
    [self.v_totalBid setHidden:previousPage != 0];
}
- (IBAction)changeValue:(UIPageControl *)sender {
    [self moveToPage:sender.currentPage animated:YES];
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return headerCellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(tableView == tblMarketDeep){
        MarketDepthHeader *header = [[MarketDepthHeader alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, cellHeight)];
        header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderBg);
        [header setupTextColor];
        return header;
    }else if (tableView == tblTimeSale){
        TimeSaleHeader *header = [[TimeSaleHeader alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, cellHeight)];
        [header setupTextColor];
        header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderBg);
        return header;
    }
    BidDoneHeader *header = [[BidDoneHeader alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, cellHeight)];
    [header setupTextColor];
    header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderBg);
    return header;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.pageControl.currentPage == 0){
        if([self.delegate respondsToSelector:@selector(didSelectMarketDepth:)]){
            [self.delegate didSelectMarketDepth:self.numberOfMarketDeep[indexPath.row]];
        }
    }
}
#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == tblTimeSale){
        return self.numberOfTimeSale.count;
    }
    else if(tableView == tblMarketDeep){
        return self.numberOfMarketDeep.count;
    }
    return self.bizDoneTableDataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == tblMarketDeep){
        MARKETDEPTH_CELL *cell = [tableView dequeueReusableCellWithIdentifier: @"MARKETDEPTH_CELL" forIndexPath:indexPath];
        [cell setupDataFrom: self.numberOfMarketDeep[indexPath.row] comparePrice:0 atIndexPath:indexPath];
        return cell;
    }else if(tableView == tblTimeSale){
        TimeSaleCell *cell = [tableView dequeueReusableCellWithIdentifier: @"TimeSaleCell" forIndexPath:indexPath];
        [cell setupDataFrom: _numberOfTimeSale[indexPath.row]];
        return cell;
    }else{
        MBusinessDoneCell *cell = [tableView dequeueReusableCellWithIdentifier:[MBusinessDoneCell reuseIdentifier]];
        if (!cell) {
            cell = [[MBusinessDoneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MBusinessDoneCell reuseIdentifier]];
        }
        [cell setupDataFrom:self.bizDoneTableDataArr[indexPath.row] checkPrice:_lacpValue];
        return cell;
    }
    
    return [UITableViewCell new];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 != 0) {
        cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellBg1);
        
    } else {
        cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellBg2);
    }
    
}
- (NSArray *)numberOfBuyAndSell{
    double numberOfBuy = 0;
    double numberOfSell = 0;
    for(MarketDepthModel *model in _numberOfMarketDeep){
        numberOfBuy += [model.buyQty doubleValue];
        numberOfSell += [model.sellQty doubleValue];
    }
    
    return @[[[NSNumber numberWithDouble:numberOfBuy] abbreviateNumber], [[NSNumber numberWithDouble:numberOfSell] abbreviateNumber]];
}

@end
