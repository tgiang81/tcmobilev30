//
//  MarketInfoCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
@class MarketDepthModel;
@class AroundShadowView;
@protocol MarketInfoCellDelegate<NSObject>
- (void)expandRow:(NSString *)title;
- (void)didSelectMarketDepth:(MarketDepthModel *)model;
@end
@interface MarketInfoCell : BaseTableViewCell
@property (weak, nonatomic) id<MarketInfoCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;
@property (strong, nonatomic) NSArray *bizDoneTableDataArr;
@property (weak, nonatomic) IBOutlet UIScrollView *scv_content;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Height;
- (void)setMarketData:(NSArray *)marketDeep timeSale:(NSArray *)timeSale bDone:(NSArray *)bDone stockCode:(NSString *)stockCode width:(CGFloat)width;
- (void)moveToPage:(NSInteger)page animated:(BOOL)animated;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TotalBidValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TotalBid;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TotalAskValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TotalAsk;
@property (weak, nonatomic) IBOutlet UIView *v_totalBid;
@property (weak, nonatomic) IBOutlet UIView *v_totalAsk;

@end
