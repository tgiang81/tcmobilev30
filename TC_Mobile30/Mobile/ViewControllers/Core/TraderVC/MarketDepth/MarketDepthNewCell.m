//
//  MarketDepthNewCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 4/2/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MarketDepthNewCell.h"
#import "TCStatusInfoView.h"
#import "LanguageKey.h"
#import "AroundShadowView.h"
@interface MarketDepthNewCell() <UIScrollViewDelegate>
@end
@implementation MarketDepthNewCell
{
    NSInteger previousPage;
    AroundShadowView *tmpViewMD;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.scv_Content.delegate = self;
    self.page_Control.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.page_Control.currentPageIndicatorTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    self.v_Border.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    
    // Initialization code
}
- (void)showNoData{
    [self removeNoData];
    TCStatusInfoView *tmpstatusView = [TCStatusInfoView new];
    CGRect tmpRect = self.v_Border.frame;
    tmpViewMD = [[AroundShadowView alloc] initWithFrame:tmpRect];
    [self.contentView addSubview:tmpViewMD];
    [tmpstatusView justShowStatus:[LanguageManager stringForKey:No_Data] inView:tmpViewMD];
}

- (void)removeNoData{
    [tmpViewMD removeFromSuperview];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView.contentOffset.x == 0){
        return;
    }
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if (previousPage != page) {
        previousPage = page;
        self.page_Control.currentPage = page;
    }
}
- (IBAction)changeValue:(UIPageControl *)sender {
    [self moveToPage:sender.currentPage animated:YES];
}

- (void)moveToPage:(NSInteger)page animated:(BOOL)animated{
    CGRect frame = _scv_Content.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [_scv_Content setContentOffset:frame.origin animated:YES];
}
@end
