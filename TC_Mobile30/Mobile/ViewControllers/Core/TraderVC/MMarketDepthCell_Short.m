//
//  MMarketDepthCell.m
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MMarketDepthCell_Short.h"
#import "MarketDepthModel.h"

#import "NSNumber+Formatter.h"

@implementation MMarketDepthCell_Short

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[self setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Main
- (void)setup{
	
}

//======== Parse Data to UI =========
- (void)setupDataFrom:(MarketDepthModel *)model comparePrice:(float)refPrice atIndexPath:(NSIndexPath *)indexPath{
	if (model) {
		_lblNo.text = [NSString stringWithFormat:@"%@", @(indexPath.row + 1)];
		_lblBuySplit1.text = model.buySplit1;
		_lblBid.text = [model.buyPrice toCurrencyNumber];
		
		_lblSellSplit1.text = model.sellSplit1;
		_lblAsk.text = [model.sellPrice toCurrencyNumber];
		
		//Check Color for Price label
		_lblBid.textColor = [model.buyPrice colorByCompareToPriceExtend:refPrice];
		_lblAsk.textColor = [model.sellPrice colorByCompareToPriceExtend:refPrice];;
	}
}
@end
