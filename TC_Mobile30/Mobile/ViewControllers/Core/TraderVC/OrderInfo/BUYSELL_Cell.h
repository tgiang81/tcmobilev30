//
//  BUYSELL_Cell.h
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TradePopupView.h"
#import "BaseTableViewCell.h"
@class AroundShadowView;
@protocol BUYSELL_CellDelegate <NSObject>
@optional
- (void)changeBuySellType:(TradeType)type;
@end
@interface BUYSELL_Cell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ActionsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ActionTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_AccountInfoTop;

@property (weak, nonatomic) IBOutlet UILabel *lb_accName;
@property (weak, nonatomic) IBOutlet UILabel *lb_accInfor;
@property (weak, nonatomic) IBOutlet UIButton *btnAccount;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sg_control;
@property (weak, nonatomic) id<BUYSELL_CellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *view_control;
@property (weak, nonatomic) IBOutlet UILabel *lbBuyLimit;
@property (weak, nonatomic) IBOutlet UILabel *lbSellLimit;
@property (weak, nonatomic) IBOutlet UILabel *lbBuyLimitLabel;
@property (weak, nonatomic) IBOutlet UILabel *lbSellLimitLabel;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIView *v_AccountInfo;
@property (weak, nonatomic) IBOutlet UIButton *btn_NextIcon;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;

- (void)changeColor:(TradeType)type;
- (void)disableSegment;
@end
