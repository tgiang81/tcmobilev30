//
//  BUYSELL_Cell.m
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BUYSELL_Cell.h"
#import "UISegmentedControl+Util.h"
#import "AppColor.h"
#import "AppMacro.h"
#import "UIButton+RightImage.h"
#import "AroundShadowView.h"
@implementation BUYSELL_Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lbl_Title.text = [LanguageManager stringForKey:@"Order Info"];
    [self.sg_control setTitle:[LanguageManager stringForKey:@"BUY"] forSegmentAtIndex:0];
    [self.sg_control setTitle:[LanguageManager stringForKey:@"SELL"] forSegmentAtIndex:1];
    
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoBg);
    self.v_AccountInfo.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_AccountBg);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_OrderInfoText);
    
    self.lb_accName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_AccountText);
    self.lb_accInfor.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_AccountText);
    self.lbBuyLimit.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_AccountText);
    self.lbSellLimit.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_AccountText);
    self.lbBuyLimitLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_AccountText);
    self.lbSellLimitLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_AccountText);
    self.btn_NextIcon.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_SearchButtonTint);
    self.view_control.layer.cornerRadius = 4;
    
    [self.sg_control changeTextColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_BuyText) font:[UIFont boldSystemFontOfSize:15]];
    [self.sg_control setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_UnSelectBg)];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)change:(UISegmentedControl *)sender {
    if([self.delegate respondsToSelector:@selector(changeBuySellType:)]){
        [self.delegate changeBuySellType:sender.selectedSegmentIndex];
    }
    [self changeColor:sender.selectedSegmentIndex];
    
}
- (void)disableSegment{
    self.cst_ActionsHeight.constant = 0;
    self.cst_ActionTop.constant = 0;
    self.cst_AccountInfoTop.constant = 0;
}
- (void)changeColor:(TradeType)type{
    self.cst_AccountInfoTop.constant = 10;
    self.cst_ActionTop.constant = 8;
    self.cst_ActionsHeight.constant = 34;
    if(type == TradeType_Buy){
        [self.sg_control setTintColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].buyColor)];
    }else if(type == TradeType_Sell){
        [self.sg_control setTintColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].sellColor)];
    }else{
        [self disableSegment];
    }
}

@end
