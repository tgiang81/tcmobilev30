//
//  INPUTDATA_Cell.h
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "BaseCollectionView.h"
@class AroundShadowView;
@interface INPUTDATA_Cell : BaseTableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet BaseCollectionView *collectionView;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;

#pragma mark - Main

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
