//
//  CollectionCell_TriggerPrice.m
//  TCiPad
//
//  Created by n2nconnect on 27/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "CollectionCell_TriggerPrice.h"

@implementation CollectionCell_TriggerPrice

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btn_DropDown.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_SearchButtonTint);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoCellText);
    self.tf_Input.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoInputTextCellColor);
    self.lbl_subTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoCellText);
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoInputTextCellBg);
    // Initialization code
}

- (IBAction)dropDown:(id)sender {
    if([self.delegate respondsToSelector:@selector(showDropDown:)]){
        [self.delegate showDropDown:self];
    }
}
- (void)setDisableDropDown:(BOOL)disableDropDown{
    _disableDropDown = disableDropDown;
    if(_disableDropDown){
        self.cst_dropdownWidth.constant = 0;
        [self.btn_DropDown setHidden:YES];
    }
}

@end
