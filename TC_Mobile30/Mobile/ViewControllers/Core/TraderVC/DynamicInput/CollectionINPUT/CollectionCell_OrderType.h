//
//  CollectionCell_OrderType.h
//  TCiPad
//
//  Created by n2nconnect on 24/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseCollectionTradeInput_Cell.h"

@interface CollectionCell_OrderType : BaseCollectionTradeInput_Cell
@property (weak, nonatomic) IBOutlet UIView *v_DropDown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_DropDownWidth;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_dropDownIcon;
@property (weak, nonatomic) IBOutlet UIView *v_content;
@property (assign, nonatomic) BOOL isHightLight;
@end
