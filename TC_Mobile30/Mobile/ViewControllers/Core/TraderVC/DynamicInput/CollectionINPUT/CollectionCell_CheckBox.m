//
//  CollectionCell_CheckBox.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "CollectionCell_CheckBox.h"

@implementation CollectionCell_CheckBox

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoCellText);
    // Initialization code
}

- (IBAction)click:(UIButton *)sender {
    self.isSelectedCell = !self.isSelectedCell;
    if (self.CallBackValue) {
        if(self.isSelectedCell == YES)
        {
            [sender setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        }
        else
        {
            [sender setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
        self.CallBackValue(self.index ,@{@"value": @( self.isSelectedCell ) });
    }
}

@end
