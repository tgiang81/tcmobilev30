//
//  BaseCollectionTradeInput_Cell.m
//  TCiPad
//
//  Created by n2nconnect on 27/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseCollectionTradeInput_Cell.h"
#import "NSNumber+Formatter.h"
@interface BaseCollectionTradeInput_Cell()<UITextFieldDelegate>

@end
@implementation BaseCollectionTradeInput_Cell

- (void)awakeFromNib{
    [super awakeFromNib];
}
- (void)setIsInputNumber:(BOOL)isInputNumber{
    _isInputNumber = isInputNumber;
    self.tf_Input.keyboardType = UIKeyboardTypeDecimalPad;
}
- (void)setMaxDecimalNumber:(int)maxDecimalNumber{
    _maxDecimalNumber = maxDecimalNumber;
    self.tf_Input.delegate = self;
}
-(IBAction)changeValue:(id)sender
{
//    UITextField *tf = (UITextField*)sender;
//
//    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
//    [numberFormatter setGroupingSeparator:@","];
//    [numberFormatter setGroupingSize:3];
//    [numberFormatter setDecimalSeparator:@"."];
//    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    [numberFormatter setMaximumFractionDigits:3];
//    [numberFormatter setMinimumFractionDigits:3];
//
//    NSNumber *numResult =  [numberFormatter numberFromString: tf.text];
//
//
//    NSLog(@"the Formatted String is  %@",[numResult toCurrencyNumber]);
//
//    tf.text = [numResult toCurrencyNumber];
    if (_CallBackValue) {
        _CallBackValue(self.index,@{@"value":self.tf_Input.text});
    }
}

-(IBAction)fnBtnClick:(id)sender
{
    if (_CallBackValue) {
        _CallBackValue(self.index,@{@"value": @1});
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
    if([arrayOfString count] < 1){
        arrayOfString = [newString componentsSeparatedByString:@","];
    }
    if(self.maxDecimalNumber > 0){
        if(arrayOfString.count == 2){
            if ([arrayOfString[1] length] > self.maxDecimalNumber)
                return NO;
        }
        
    }
    return YES;
}

@end
