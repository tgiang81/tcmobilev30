//
//  BaseCollectionTradeInput_Cell.h
//  TCiPad
//
//  Created by n2nconnect on 27/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//
#import "BaseCollectionViewCell.h"
@protocol BaseCollectionTradeInput_CellDelegate <NSObject>
- (void)showDropDown:(BaseCollectionViewCell *)cell;
@end
@interface BaseCollectionTradeInput_Cell : BaseCollectionViewCell
{
    
}
@property (assign, nonatomic) NSInteger index;

@property (nonatomic, copy) callBackValue CallBackValue;
@property (nonatomic, strong) IBOutlet UILabel* lb_content;
@property (nonatomic, strong) IBOutlet UIButton* btn_Input;
@property (nonatomic, strong) IBOutlet UITextField* tf_Input;
@property (assign, nonatomic) BOOL isInputNumber;
@property (assign, nonatomic) int maxDecimalNumber;
@property (weak, nonatomic) id<BaseCollectionTradeInput_CellDelegate> delegate;
@end
