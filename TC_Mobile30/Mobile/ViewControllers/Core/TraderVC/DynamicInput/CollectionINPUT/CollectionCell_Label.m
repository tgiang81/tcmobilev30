//
//  CollectionCell_Label.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "CollectionCell_Label.h"

@implementation CollectionCell_Label

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoCellText);
    self.lbl_Value.textColor = self.lbl_Title.textColor;
    // Initialization code
}



@end
