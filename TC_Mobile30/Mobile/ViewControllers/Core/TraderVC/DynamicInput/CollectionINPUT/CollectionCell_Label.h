//
//  CollectionCell_Label.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionTradeInput_Cell.h"
@interface CollectionCell_Label : BaseCollectionTradeInput_Cell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Value;
@end
