//
//  CollectionCell_CheckBox.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionTradeInput_Cell.h"
@interface CollectionCell_CheckBox : BaseCollectionTradeInput_Cell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (assign) BOOL isSelectedCell;
@end
