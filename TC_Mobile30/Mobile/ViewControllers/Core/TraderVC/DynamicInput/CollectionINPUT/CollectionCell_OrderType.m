//
//  CollectionCell_OrderType.m
//  TCiPad
//
//  Created by n2nconnect on 24/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "CollectionCell_OrderType.h"
#import "LanguageKey.h"

@implementation CollectionCell_OrderType

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btn_dropDownIcon.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_SearchButtonTint);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoCellText);
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoDropdownCellBg);
    self.lb_content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoDropdownCellText);
    //Cuongnx (21/02/2019) set multi language
    self.lbl_Title.text=[LanguageManager stringForKey:Order_Type];
    // Initialization code
}

- (void)setIsHightLight:(BOOL)isHightLight{
    _isHightLight = isHightLight;
    if(isHightLight ){
        self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
    }else{
        self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoCellText);
    }
    
}
- (IBAction)dropDown:(id)sender {
    if([self.delegate respondsToSelector:@selector(showDropDown:)]){
        [self.delegate showDropDown:self];
    }
}

@end
