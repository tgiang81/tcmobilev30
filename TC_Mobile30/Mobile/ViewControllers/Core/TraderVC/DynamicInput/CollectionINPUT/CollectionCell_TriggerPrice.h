//
//  CollectionCell_TriggerPrice.h
//  TCiPad
//
//  Created by n2nconnect on 27/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseCollectionTradeInput_Cell.h"

@interface CollectionCell_TriggerPrice : BaseCollectionTradeInput_Cell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subTitle;
@property (weak, nonatomic) IBOutlet UIView *v_content;
@property (weak, nonatomic) IBOutlet UIButton *btn_DropDown;
@property (assign, nonatomic) BOOL disableDropDown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_dropdownWidth;
@end
