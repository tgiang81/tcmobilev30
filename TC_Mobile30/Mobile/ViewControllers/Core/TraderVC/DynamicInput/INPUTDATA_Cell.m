//
//  INPUTDATA_Cell.m
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "INPUTDATA_Cell.h"
#import "CustomCollectionViewLayout.h"

#import "CollectionCell_OrderType.h"
#import "CollectionCell_Trigger.h"
#import "CollectionCell_TriggerPrice.h"
#import "CollectionCell_CheckBox.h"
#import "CollectionCell_Label.h"
#import "AroundShadowView.h"
@implementation INPUTDATA_Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.collectionView registerNib:[UINib nibWithNibName:[CollectionCell_CheckBox nibName] bundle:nil] forCellWithReuseIdentifier:[CollectionCell_CheckBox reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:[CollectionCell_Label nibName] bundle:nil] forCellWithReuseIdentifier:[CollectionCell_Label reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:[CollectionCell_OrderType nibName] bundle:nil] forCellWithReuseIdentifier:[CollectionCell_OrderType reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:[CollectionCell_Trigger nibName] bundle:nil] forCellWithReuseIdentifier:[CollectionCell_Trigger reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:[CollectionCell_TriggerPrice nibName] bundle:nil] forCellWithReuseIdentifier:[CollectionCell_TriggerPrice reuseIdentifier]];
    
    self.collectionView.showsHorizontalScrollIndicator = NO;
    //Color
    self.collectionView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);//[UIColor clearColor];
    
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoBg);
}
- (void)layoutSubviews{
    [super layoutSubviews];
    [self layoutIfNeeded];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withScrollDirection: (UICollectionViewScrollDirection)direction{
    if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
    if (direction == UICollectionViewScrollDirectionVertical) {
        self.collectionView.scrollEnabled = NO;
    }else{
        self.collectionView.scrollEnabled = YES;
    }
    return self;
}

#pragma mark - UTILS
- (void)implementConstrain{
    //Add Constrain
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.v_content attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.v_content attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.v_content attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.v_content attribute:NSLayoutAttributeTrailing multiplier:1 constant:0];
    [self addConstraints:@[top,bottom,leading,trailing]];
}

#pragma mark - Main Delegate
- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.indexPath = indexPath;
//    [self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
    [self.collectionView reloadData];
}
@end
