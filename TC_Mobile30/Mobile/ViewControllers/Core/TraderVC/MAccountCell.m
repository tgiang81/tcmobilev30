//
//  MAccountCell.m
//  TC_Mobile30
//
//  Created by n2nconnect on 13/09/2018.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MAccountCell.h"

@implementation MAccountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



-(void)populateItemsAtIndexPath:(NSIndexPath*)indexPath withArray:(NSMutableArray*)array {
    
    UserAccountClientData *uacd = [array objectAtIndex:indexPath.row];
    self.userName.text = uacd.client_name;
    self.accountNumber.text = [NSString stringWithFormat:@"%@-%@",uacd.client_account_number, uacd.broker_branch_code];
    
    [self.selectedView.layer setBorderWidth:1];
    [self.selectedView.layer setCornerRadius:self.selectedView.frame.size.width/2.0];
    [self.selectedView.layer setMasksToBounds:NO];
    
    
//    UserAccountClientData *selectedAcc = [UserPrefConstants singleton].userSelectedAccount;
//    if ([uacd.client_account_number isEqualToString:selectedAcc.client_account_number]) {
//        [self.selectedView.layer setBackgroundColor:[UIColor cyanColor].CGColor];
//    } else{
//        [self.selectedView.layer setBackgroundColor:[UIColor clearColor].CGColor];
//    }
}

@end
