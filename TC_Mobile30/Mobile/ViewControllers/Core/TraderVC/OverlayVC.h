//
//  OverlayVC.h
//  TCiPad
//
//  Created by giangtu on 7/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^OverlayVCCallback)(NSInteger index);

@interface OverlayVC : UIViewController

@property (weak, nonatomic) IBOutlet UIPickerView *mPickerView;
@property (weak, nonatomic) IBOutlet UIView *vContainer;

- (void) toggleOverlay;
- (void) doBlock:(OverlayVCCallback ) cb;
@end
