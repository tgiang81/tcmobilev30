//
//  BidDoneHeader.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/31/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BidDoneHeader : TCBaseSubView
@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_BVol;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SVol;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Buy;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TVol;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TVal;

- (void)setupTextColor;
@end

NS_ASSUME_NONNULL_END
