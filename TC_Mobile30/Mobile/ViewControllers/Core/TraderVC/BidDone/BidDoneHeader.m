//
//  BidDoneHeader.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/31/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BidDoneHeader.h"
#import "ThemeManager.h"
#import "LanguageManager.h"
@implementation BidDoneHeader

- (void)awakeFromNib{
    [super awakeFromNib];
    self.lbl_Price.text = [LanguageManager stringForKey:@"Price"];
    self.lbl_BVol.text = [LanguageManager stringForKey:@"BVol"];
    self.lbl_SVol.text = [LanguageManager stringForKey:@"SVol"];
    self.lbl_Buy.text = [LanguageManager stringForKey:@"Buy%"];
    self.lbl_TVol.text = [LanguageManager stringForKey:@"T.Vol"];
    self.lbl_TVal.text = [LanguageManager stringForKey:@"T.Val"];

}

- (void)setupTextColor{
    self.lbl_Price.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_BVol.textColor = self.lbl_Price.textColor;
    self.lbl_SVol.textColor = self.lbl_Price.textColor;
    self.lbl_Buy.textColor = self.lbl_Price.textColor;
    self.lbl_TVol.textColor = self.lbl_Price.textColor;
    self.lbl_TVal.textColor = self.lbl_Price.textColor;
}

@end
