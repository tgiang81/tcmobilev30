//
//  MSkipConfirmationViewController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/14/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MSkipConfirmationViewController.h"
#import "ThemeManager.h"
#import "AroundShadowView.h"
#import "MarketInfoCell.h"
#import "SkipContainerCell.h"
@interface MSkipConfirmationViewController ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation MSkipConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBackButtonDefault];
    self.customBarTitle = [LanguageManager stringForKey:@"Order Pad"];
    self.confirmTitle.text = [LanguageManager stringForKey:@"Order Details"];
    
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    self.v_Content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    self.confirmTable.delegate = self;
    self.confirmTable.dataSource = self;
    self.confirmTable.alwaysBounceVertical = NO;
    self.confirmTable.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    [self.confirmTable registerNib:[UINib nibWithNibName:[MarketInfoCell nibName] bundle:nil] forCellReuseIdentifier:[MarketInfoCell reuseIdentifier]];
    [self.confirmTable registerNib:[UINib nibWithNibName:[SkipContainerCell nibName] bundle:nil] forCellReuseIdentifier:[SkipContainerCell reuseIdentifier]];

    self.confirmTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_titleColor);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_hightLightTextColor);
    [self.btn_Back setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_hightLightTextColor)];
    self.StkName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_hightLightTextColor);
    self.StkCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_textColor);
    
    
    self.cancelBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_cancelColor);
    [self.cancelBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor) forState:UIControlStateNormal];
    
    self.submitBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor);
    [self.submitBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor) forState:UIControlStateNormal];
    
    
}
- (NSArray *)recalculateDetailArray{
    
    return @[@"Account No.", @"Indiactive Exch. Rate", @"Buy Limit/Sell Limit", @"Action", @"Order Type", @"Validity", @"Settlement Currentcy", @"Payment", @"Order Price", @"Order Qty (Unit)", @"Order Value"];
}
- (void)backButtonTapped:(id)sender{
    if([self.skipDelegate respondsToSelector:@selector(backFromSkip)]){
        [self.skipDelegate backFromSkip];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backAction:(id)sender {
    if([self.skipDelegate respondsToSelector:@selector(subConfirmViewBack)]){
        [self.skipDelegate subConfirmViewBack];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.section == 0){
//        MarketInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[MarketInfoCell reuseIdentifier] forIndexPath:indexPath];
//        [cell setMarketData:self.arrMarketDepthItems timeSale:self.timeSaleArr bDone:self.bizDoneTableDataArr stockCode:self.stockCode width:SCREEN_WIDTH_PORTRAIT - 16];
//        return cell;
//    }else{
        SkipContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:[SkipContainerCell reuseIdentifier] forIndexPath:indexPath];
        cell.detailArray = self.detailArray;
        cell.cst_Height.constant = tableView.frame.size.height;
        [cell.tbl_Content reloadData];
        return cell;
//    }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1; 
}
@end
