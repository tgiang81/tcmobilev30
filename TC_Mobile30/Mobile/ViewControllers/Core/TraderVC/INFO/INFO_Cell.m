//
//  INFO_Cell.m
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "INFO_Cell.h"
#import "AppMacro.h"
#import "MarqueeLabel.h"
#import "AroundShadowView.h"
#import "SettingManager.h"
#import "ThemeManager.h"
static NSString *kICON_UP_PRICE_NAME    = @"up_price_icon";
static NSString *kICON_DOWN_PRICE_NAME    = @"down_price_icon";
@interface INFO_Cell() <UITextFieldDelegate>
    
@end

@implementation INFO_Cell

- (void)awakeFromNib {
    [super awakeFromNib];
//    db_bgDropDown
    self.lbStockName.delegate = self;
    self.lbStockName.enabled = [SettingManager shareInstance].isOnOrderType2;
    if([SettingManager shareInstance].isOnOrderType2 == YES){
        self.lbStockName.clearButtonMode = UITextFieldViewModeWhileEditing;
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 20)];
        self.lbStockName.clearButtonMode = UITextFieldViewModeAlways;
        self.lbStockName.leftView = paddingView;
        self.lbStockName.leftViewMode = UITextFieldViewModeAlways;
        self.lbStockName.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingDropDownBg);
        self.lbStockName.layer.cornerRadius = 4;

    }
    
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoBg);
    self.lbStockName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.lbCompany.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockCompanyText);
    self.changeValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.changePercent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.btn_Search.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_SearchButtonTint);
    // Initialization code
}
- (void)clearTextView:(id)sender{
    self.lbStockName.text = @"";
    [self.delegate didChangeKeyword:self.lbStockName.text];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)search:(id)sender {
    if([self.delegate respondsToSelector:@selector(actionSearch:)]){
        [self.delegate actionSearch:sender];
    }
}
- (void)setChangeValueContent:(CGFloat)changeValueContent{
    if (changeValueContent>0) {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
    } else if (changeValueContent<0) {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
    } else {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
    }
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.delegate didBeginEditing:textField.text];
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField{
    [self.delegate didChangeKeyword:@""];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self.delegate didChangeKeyword:searchStr];
    return YES;
}

@end
