//
//  INFO_Cell.h
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@class TCMenuDropDown;
@class AroundShadowView;
@class INFO_Cell, MarqueeLabel;
@protocol INFO_CellDelegate <NSObject>
@optional
- (void)didChangeKeyword:(NSString *)keyword;
- (void)didBeginEditing:(NSString *)keyword;
@required
- (void)actionSearch:(INFO_Cell *)cell;
@end
@interface INFO_Cell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UITextField *lbStockName;
@property (weak, nonatomic) IBOutlet UILabel *lbCompany;
@property (weak, nonatomic) IBOutlet UIImageView *imgvArrow;
@property (weak, nonatomic) IBOutlet UILabel *changeValue;
@property (weak, nonatomic) IBOutlet UILabel *changePercent;
@property (weak, nonatomic) IBOutlet UIButton *btn_Search;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDownStocks;

@property (weak, nonatomic) id<INFO_CellDelegate> delegate;

@property (assign, nonatomic) CGFloat changeValueContent;
@end
