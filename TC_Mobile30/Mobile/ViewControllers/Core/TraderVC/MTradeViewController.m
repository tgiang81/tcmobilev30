
//
//  TraderViewController.m
//  TCiPad
//
//  Created by GIANG
//  Copyright © 2018 N2N. All rights reserved.
//

//#import "AppControl.h"
#import "MTradeViewController.h"
#import "TradeAccountController.h"
#import "AppControl.h"
#import "TradingRules.h"
#import "StockInfoModel.h"
#import "NumberHandler.h"
#import "VertxConnectionManager.h"
#import "OverlayVC.h"
#import "MMarketDepthCell_Short.h"
#import "MarketDepthModel.h"
#import "MStockPreviewController.h"
#import "StockModel.h"
#import "BaseNavigationController.h"
#import "UIView+ShadowBorder.h"
#import "CustomCollectionViewLayout.h"
//
#import "INFO_Cell.h"
#import "MARKETDEPTH_CELL.h"
#import "BUYSELL_Cell.h"
#import "INPUTDATA_Cell.h"
#import "PINPASSWORD_Cell.h"

#import "CollectionCell_OrderType.h"
#import "CollectionCell_Trigger.h"
#import "CollectionCell_TriggerPrice.h"
#import "CollectionCell_CheckBox.h"
#import "CollectionCell_Label.h"

#import "AppMacro.h"
#import "AppControl.h"

#import "CAPopUpViewController.h"

#import "DefineChooseSwift.h"

#import "TCBaseButton.h"
#import "MSearchVC.h"
#import "MTradeAccountVC.h"
#import "UIViewController+Util.h"
#import "TC_Calculation.h"
#import "IQKeyboardManager.h"
#import "TimeSaleModel.h"
#import "NSDate+Utilities.h"
#import "MarketInfoCell.h"
#import "AccountsVC.h"
#import "MarketHeaderView.h"
#import "SettingManager.h"
#import "AppState.h"
#import "UIViewController+TCUtil.h"
#import "TradeUtils.h"
#import "TCMenuDropDown.h"
#import "UIViewController+TCUtil.h"
#import "TradeContentCell.h"
#import "MarketDepthNewCell.h"
#import "MMarketDepthContentVC.h"
#import "MBusinessDoneContentVC.h"
#import "MTimeSaleContentVC.h"
#import "BrokerManager.h"
#import "BrokerModel.h"
#define kTradeTag 100
#define kDEFAULT_CONNER_RADIUS    5.0
#import "LanguageKey.h"
static  NSString *identifier_INFO_cell = @"identifier_INFO_cell";
static  NSString *identifier_MARKETDEPTH_CELL = @"MARKETDEPTH_CELL";
static  NSString *identifier_MARKETDEPTHCell_Labels = @"identifier_MARKETDEPTHCell_Labels";

static  NSString *identifier_BUYSELL_Cell = @"identifier_BUYSELL_Cell";
static  NSString *identifier_INPUTDATA_Cell = @"identifier_INPUTDATA_Cell";
static  NSString *identifier_PINPASSWORD_Cell = @"identifier_PINPASSWORD_Cell";

static  NSString *identifier_MarketDepthHeader_Cell = @"identifier_MarketDepthHeader_Cell";


typedef enum
{
    FIELD_INFO,
    FIELD_MARKETDEPTH,
    FIELD_BUYSELL,
    FIELD_INPUT_DATA,
    FIELD_PIN_PASSWORD
}SECTION_FIELD_TYPE;




@interface MTradeViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, N2DatePickerDelegate , ValidatePINDelegate ,  SkipDialogDelegate ,  TradeStatusMsgDelegate, TradeAccountDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIPopoverPresentationControllerDelegate, WWCalendarTimeSelectorProtocol,
BUYSELL_CellDelegate, INFO_CellDelegate, MarketInfoCellDelegate, MarketHeaderViewDelegate, MSearchVCDelegate, BaseCollectionTradeInput_CellDelegate, UIPopoverPresentationControllerDelegate, PricePadDelegate, QuantityPadDelegate, ATPDelegate> {
    UIPopoverPresentationController *popoverController;
    MSearchVC *searchVC;
    __weak IBOutlet UITableView *tableControl;
    BaseCollectionView *myCollectionView;
    
    NSMutableArray *_timeSaleArr;
    NSMutableArray *arrSections;
    NSMutableArray *arrCollectViewControls;
    
    NSMutableArray *arrMarketDepthItems;
    
    NSNumber *defAPrice;
    NSNumber *defAQty;
    NSNumber *defBQty;
    NSNumber *defBPrice;
    CGFloat positionOfThirdCard;
    CGFloat minDropDownWidth;
    BOOL isExpandMarket;
    NSString *_messString;
    MarketHeaderView *marketInfoHeader;
    
    __weak IBOutlet UIView *vContainerSearchStock;
    
    __weak IBOutlet UIView *mAccInforView;
    __weak IBOutlet UISegmentedControl *mFirstSecond;
    //
    
    //Other Viewcontroller
    MSkipConfirmationViewController *skipVC;
    MTradeStatusViewController *tsVC;
    MValidatePINViewController *validVC;
    
    BaseCollectionTradeInput_Cell *selectedCell;
    //Payment
    OrderDetails *myOrderDetails;
    TradeStatus *myTradeStatus;
    
    BOOL isFirst;
    BOOL isSkipConfirmation;
    
    //Account Number Outlets
    //    IBOutlet UILabel *accountName;
    NSString * m_accountName;
    
    //    IBOutlet UILabel *tradeLimitTitle;
    
    NSString *tradeLimitTitle;
    
    
    //header
    IBOutlet UILabel *lbTicker;
    IBOutlet UILabel *lbCompanyName;
    IBOutlet UILabel *labelPrice;
    IBOutlet UILabel *labelPriceChange;
    IBOutlet UILabel *labelPriceChangeP;
    IBOutlet UIImageView *chgArrow;
    
    
    IBOutlet UITableView *tblContent;
    
    NSMutableArray  *_MDSellArr, *_MDBuyArr;
    float _lacpValue;
    int _maxPriceDecimalNumber;
    
    QCData *_qcData;
    
    TradingRules *trdRules;
    ExchangeData *currentED;
    UserAccountClientData *uacd;
    ATPAuthenticate *atp;
    NSArray *acclist;
    
    NSNumberFormatter *priceFormatter;
    
    NSString * stkExchange;
    NSString * stockCurrency;
    NSString * exchangeCurrency;
    
    //
    MDInfo *mdInfo;
    
    //Save last textfield
    UITextField *activeTextField;
    
    NSNumber *PriceNo1;
    NSNumber *PriceNo2;
    NSNumber *PriceNo3;
    
    NSNumber *QtyNo1;
    NSNumber *QtyNo2;
    NSNumber *QtyNo3;
    
    UIToolbar* numberToolbar;
    
    OverlayVC * overlayVC;
    
    IBOutlet UILabel *totalValue;
    IBOutlet UILabel *unitsnprice;
    //Increment Decrement View
    IBOutlet UIView *numberButtonIncrementView;
    IBOutlet UILabel *numberButtonLabel1;
    IBOutlet UILabel *numberButtonLabel2;
    IBOutlet UILabel *numberButtonLabel3;
    IBOutlet UILabel *numberButtonLabel4;
    IBOutlet UILabel *numberButtonLabel5;
    IBOutlet UILabel *numberButtonLabel6;
    IBOutlet UIButton *numberButton1;
    IBOutlet UIButton *numberButton2;
    IBOutlet UIButton *numberButton3;
    IBOutlet UIButton *numberButton4;
    IBOutlet UIButton *numberButton5;
    IBOutlet UIButton *numberButton6;
    
    
    //List for 4 pickers
    NSMutableArray *currencyList;
    NSMutableArray *paymentList;
    NSMutableArray *orderValidityList;
    NSMutableArray *orderTypeList;
    IBOutlet UIPickerView *selectorPicker;
    
    //picker 3-4
    NSMutableArray *pickerList;
    UIPickerView *myPickerView;
    
    //Date picker n2n custom ...
    N2DatePicker *datePicker;
    NSDate *dateRow;
    
    
    int lotSizeStringInt;
    int autoFillValue;
    //
    long currencyPlusIndex;
    
    //
    long validityRow,
    priceRow,
    ordTypeRow,
    triggerRow,
    directionRow,
    currencyRow,
    paymentRow,
    whichQty,
    whichPrice,
    triggerPriceRow;
    
    
    double resolvedLimit;
    BOOL keyUP;
    BOOL isShowing;
    BOOL isShowingPopupPriceOrQty;
    IBOutlet NSLayoutConstraint *constraintQuickButtonsInput;
    
    BOOL isShowingSearchVC;
    
    ValType currentValType;
    NSString *password;
    
    CGFloat inputItemheight;
    
    
    
    MMarketDepthContentVC *_depthVC;
    MBusinessDoneContentVC *_bDoneVC;
    MTimeSaleContentVC *_saleVC;
}

@end

@implementation MTradeViewController



- (void)viewDidLoad {
    [super viewDidLoad];
//    if([SettingManager shareInstance].isOnOrderType2){
        self.uiType = TradeUIType2;
//    }else{
//        self.uiType = TradeUIType1;
//    }
//    [SettingManager shareInstance].isOnOrderType2 = [BrokerManager shareInstance].broker.tag == BrokerTag_SSI;
    [self setupForSSI];
    [SettingManager shareInstance].isOnOrderType2 = YES;
    autoFillValue = [[SettingManager shareInstance].customQuantity intValue];
    minDropDownWidth = 123;
    [self.v_InputKeyboard setAlpha:0];
    [self registerNotifications];
    [self addNotiKeyboard];
    isExpandMarket = YES;
    isFirst = YES;
    lotSizeStringInt = 1;
    _maxPriceDecimalNumber = [UserPrefConstants singleton].pointerDecimal;
    inputItemheight = 58;
    password = @"";
    _qcData = [QCData singleton];
    
    //global selected Stock code.
    if(_stock == nil && self.isNotFromTradeButton == NO){
        _stock = [UserPrefConstants singleton].userSelectingThisStock;
    }
    self.enableCheckButtonTapDismissKeyboard = !self.isNotFromTradeButton;
    self.enableTapDismissKeyboard = !self.isNotFromTradeButton;
    vContainerSearchStock.hidden = TRUE;
    //If there's no selected Stock -> show search view
    
    
    /**/
    arrSections = [NSMutableArray new];
    
    [arrSections addObject:@{
                             @"TYPE": @(FIELD_INFO),
                             @"CONTENT" : @{}
                             }];
    
    [arrSections addObject:@{
                             @"TYPE": @(FIELD_MARKETDEPTH),
                             @"CONTENT" : @{}
                             }];
    
    [arrSections addObject:@{
                             @"TYPE": @(FIELD_BUYSELL),
                             @"CONTENT" : @{}
                             }];
    
    [arrSections addObject:@{
                             @"TYPE": @(FIELD_INPUT_DATA),
                             @"CONTENT" : @{}
                             }];
    
    //    if([UserPrefConstants singleton].skipPin == NO){
    //        [arrSections addObject:@{
    //                                 @"TYPE": @(FIELD_PIN_PASSWORD),
    //                                 @"CONTENT" : @{}
    //                                 }];
    //    }
    
    
    //
    
    arrCollectViewControls =  [NSMutableArray new];
    [self setupDataForArrayControls];
    //The order of array controls are very important. For Cell CollectionView embeded
    
    [tableControl registerNib:[UINib nibWithNibName:@"TradeContentCell" bundle:nil] forCellReuseIdentifier:@"TradeContentCell"];
    [tableControl registerNib:[UINib nibWithNibName:@"INPUTDATACell" bundle:nil] forCellReuseIdentifier:identifier_INPUTDATA_Cell];
    [tableControl registerNib:[UINib nibWithNibName:@"INFOCell" bundle:nil] forCellReuseIdentifier:identifier_INFO_cell];
    [tableControl registerNib:[UINib nibWithNibName:[MarketInfoCell nibName] bundle:nil] forCellReuseIdentifier:[MarketInfoCell reuseIdentifier]];
    [tableControl registerNib:[UINib nibWithNibName:@"BUYSELLCell" bundle:nil] forCellReuseIdentifier:identifier_BUYSELL_Cell];
    [tableControl registerNib:[UINib nibWithNibName:[INPUTDATA_Cell nibName] bundle:nil] forCellReuseIdentifier:[INPUTDATA_Cell reuseIdentifier]];
    [tableControl registerNib:[UINib nibWithNibName:@"PINPASSWORDCell" bundle:nil] forCellReuseIdentifier:identifier_PINPASSWORD_Cell];
    [tableControl registerNib:[UINib nibWithNibName:@"MarketDepthHeader_Cell" bundle:nil] forCellReuseIdentifier:identifier_MarketDepthHeader_Cell];
    [tableControl registerNib:[UINib nibWithNibName:@"MarketDepthNewCell" bundle:nil] forCellReuseIdentifier:[MarketDepthNewCell reuseIdentifier]];
    
    [tableControl registerNib:[UINib nibWithNibName:@"MARKETDEPTHCell_Labels" bundle:nil] forCellReuseIdentifier:identifier_MARKETDEPTHCell_Labels];
    
    
    //Set Navigation buttons/title.
    [self setBackButtonDefault];
    //    [self createCustomBar:BarItemType_Left icon:@"back_white_icon" selector:@selector(fnDismiss:)];
    self.customBarTitle = [LanguageManager stringForKey:@"Order Pad"];
    
    
    // OLD SOURCE
    
    //    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"]; //Main
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneVertxGetScoreBoardWithExchange:) name:@"doneVertxGetScoreBoardWithExchange" object:nil];
    
    
    
    keyUP = NO;
    
    myOrderDetails = [OrderDetails new];
    myTradeStatus = [TradeStatus new];
    
    arrMarketDepthItems = @[].mutableCopy;
    //Init arrai
    orderTypeList = [NSMutableArray new];
    //=>
    currencyList = [NSMutableArray new];
    paymentList = [NSMutableArray new];
    orderValidityList = [NSMutableArray new];
    
    
    //Delegate table
    [tblContent registerNib:[UINib nibWithNibName:[MMarketDepthCell_Short nibName] bundle:nil] forCellReuseIdentifier: [MMarketDepthCell_Short reuseIdentifier]];
    tblContent.dataSource = self;
    tblContent.delegate = self;
    
    
    //User account Client Data
    uacd = [UserPrefConstants singleton].userSelectedAccount;
    
    //Set value for mdInfor
    for (MDInfo *inf in [UserPrefConstants singleton].currentExchangeInfo.qcParams.exchgInfo) {
        if ([inf.exchange isEqualToString:[UserPrefConstants singleton].userCurrentExchange]) {
            mdInfo = inf;
        }
    }
    
    //Get Market Depth
    [self addNotification:kDidFinishMarketDepthNotification selector:@selector(didFinishMarketDepth:)];
    //Get Time Sale
    [self addNotification:kDidFinishedTimeAndSalesNotification selector:@selector(didReceiveTimeSaleNotification:)];
    
    m_accountName = [NSString stringWithFormat:@"%@-%@-%@", uacd.broker_branch_code, uacd.client_account_number, [uacd.client_name capitalizedString]] ;
    
    //For dismissing Num Pad
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    
    
    //2 pickers
    //    myPickerView =[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 320, 200)];
    
    //Common list temp for all pickers.
    pickerList =[NSMutableArray array];
    
    
    //set image buttons
    [numberButton1 setBackgroundImage:[UIImage imageNamed:@"ButtonMinus.png"] forState:UIControlStateNormal ];
    [numberButton2 setBackgroundImage:[UIImage imageNamed:@"ButtonMinus.png"] forState:UIControlStateNormal ];
    [numberButton3 setBackgroundImage:[UIImage imageNamed:@"ButtonMinus.png"] forState:UIControlStateNormal ];
    
    [numberButton4 setBackgroundImage:[UIImage imageNamed:@"Button_Plus.png"] forState:UIControlStateNormal ];
    [numberButton5 setBackgroundImage:[UIImage imageNamed:@"Button_Plus.png"] forState:UIControlStateNormal ];
    [numberButton6 setBackgroundImage:[UIImage imageNamed:@"Button_Plus.png"] forState:UIControlStateNormal ];
    
    //Set Trading Rules
    //    if (_actionTypeDict.allKeys.count > 3) { //magic number
    //        trdRules = [_actionTypeDict objectForKey:@"TradeRules"];
    //    }
    
    // ----- Get increment/decrement buttons value -----
    [self getNumberButtonsValue];
    
    
    atp = [ATPAuthenticate singleton];
    atp.delegate = self;
    
    validityRow=priceRow=ordTypeRow=
    triggerRow=directionRow=currencyRow=
    paymentRow=whichQty=triggerPriceRow = 0;
    
    acclist = [[[UserPrefConstants singleton] userAccountlist] copy];
    
    //Format Price
    priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGeneratesDecimalNumbers:YES];
    
    
    stockCurrency = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stock.stockCode] objectForKey:FID_134_S_CURRENCY];
    if ([stockCurrency length] <=0 ) {
        stockCurrency = [UserPrefConstants singleton].defaultCurrency;
    }
    
    [selectorPicker setUserInteractionEnabled:YES];
    
    // others
    [self findAndReplacePlusCurrency];
    
    [selectorPicker reloadAllComponents];
    [self calculatePrice];
    
    [self refreshTrdLmtBtnPressed:nil];
    
    
    
    [self setupBottomActionsWith:self.uiType tradeType:self.tradeType];
    [self fnUpdateDataViaNewStock];
    // Apply trading rules - fill data
    [self updateUIwithStock];
    if(![SettingManager shareInstance].isOnOrderType2){
        [self addSearchButton];
    }
    
    [self setupValidVC];
    
    if(_willCancelOrder){
        [self fnSubmit:nil];
    }
    
    _depthVC = NEW_VC_FROM_NIB([MMarketDepthContentVC class], [MMarketDepthContentVC nibName]);
    
    _bDoneVC = NEW_VC_FROM_NIB([MBusinessDoneContentVC class], [MBusinessDoneContentVC nibName]);
    
    _saleVC = NEW_VC_FROM_NIB([MTimeSaleContentVC class], [MTimeSaleContentVC nibName]);
    
    
}
- (void)setStock:(StockModel *)stock{
    _stock = stock;
    if(!self.enableTapDismissKeyboard){
        self.enableCheckButtonTapDismissKeyboard = stock;
        self.enableTapDismissKeyboard = stock;
    }
    
}
- (void)setupForSSI{
    if(([BrokerManager shareInstance].broker.tag == BrokerTag_SSI)){
        [self.v_ActionSSI setHidden:NO];
        [self.v_SkipSSI setHidden:YES];
        [self.v_ShortSellSSI setHidden:YES];
        self.v_SkipSSI.index = -1;
        self.v_ShortSellSSI.index = -1;
        self.v_SkipSSI.lbl_Title.text = [self getTitleWith:CONTROL_SKIPCONFIRMATION];
        [self.v_SkipSSI setCallBackValue:^(NSInteger index, NSDictionary *value)
         {
             if(index != -1){
                 [self actionWhenCheckingCell:index value:value];
             }
         }];
        
        self.v_ShortSellSSI.lbl_Title.text = [self getTitleWith:CONTROL_SHORTSELL];
        [self.v_ShortSellSSI setCallBackValue:^(NSInteger index, NSDictionary *value) {
            if(index != -1){
                [self actionWhenCheckingCell:index value:value];
            }
        }];
    }else{
        [self.v_ActionSSI setHidden:YES];
    }
    
}
- (void)actionWhenCheckingCell:(NSInteger)index value:(NSDictionary *)value{
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary: self->arrCollectViewControls[index]];
    
    [dicMul removeObjectForKey:@"value"];
    [dicMul setObject:value[@"value"] forKey:@"value"];
    
    self->isSkipConfirmation = [[value valueForKey:@"value"] boolValue];
    
    [self->arrCollectViewControls replaceObjectAtIndex:index withObject:dicMul];
}
- (void)setContentMode:(TradeContentModeType)contentMode{
    _contentMode = contentMode;
    [self enableActionButtons:YES];
    if(contentMode != TradeContentModeTrade){
        [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 10;
        self.cst_Bottom.constant = -(self.v_Bottom.frame.size.height);
        [self.v_Bottom setHidden:YES];
    }else{
        [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = self.v_InputKeyboard.frame.size.height;
        self.cst_Bottom.constant = 8;
        [self.v_Bottom setHidden:NO];
    }
    tableControl.scrollEnabled = _contentMode == TradeContentModeTrade;
    [tableControl reloadData];
    
    if(([BrokerManager shareInstance].broker.tag == BrokerTag_SSI)){
        [self.v_ActionSSI setHidden:contentMode != TradeContentModeTrade];
    }
}
- (void)addNotiKeyboard
{
    // This could be in an init method.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didShowKeyBoard:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willHideKeyBoard:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)willHideKeyBoard:(NSNotification*)notification
{
    isShowingPopupPriceOrQty = NO;
    [UIView animateWithDuration:0.1 animations:^{
        [self.v_InputKeyboard setAlpha:0];
    }];
}

- (void)didShowKeyBoard:(NSNotification*)notification
{
    if(self.contentMode == TradeContentModeTrade && isShowingPopupPriceOrQty){
        [UIView animateWithDuration:0.1 animations:^{
            [self.v_InputKeyboard setAlpha:1];
        }];
        if(self.cst_InputKeyboardBottom.constant == 0){
            NSDictionary* keyboardInfo = [notification userInfo];
            NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
            CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
            CGFloat safeAreaBottomInset = 0;
            if (@available(iOS 11.0, *)) {
                safeAreaBottomInset = self.view.safeAreaInsets.bottom;
            }
            self.cst_InputKeyboardBottom.constant = keyboardFrameBeginRect.size.height - safeAreaBottomInset - self.v_Bottom.frame.size.height - 8;
        }
    }
}

- (void)setupDataForArrayControls{
    if(self.tradeObject == nil){
        [arrCollectViewControls addObjectsFromArray:[Utils getDefaultTradeObject]];
    }else{
        [arrCollectViewControls addObjectsFromArray:self.tradeObject];
    }
}

- (void)addSearchButton{
    UIBarButtonItem *searchBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_search_white"] style:UIBarButtonItemStylePlain target:self action:@selector(presentSearchVC)];
    [searchBtn setImageInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    self.navigationItem.rightBarButtonItem = searchBtn;
}
-(void) fnUpdateDataViaNewStock
{
    //reset lists
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [orderTypeList removeAllObjects];
    [currencyList removeAllObjects];
    [orderValidityList removeAllObjects];
    [paymentList removeAllObjects];
    
    NSArray *stkCodeAndExchangeArr= [_stock.stockCode componentsSeparatedByString:@"."];
    trdRules = [[[UserPrefConstants singleton]tradingRules] objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
    
    
    //Stock Price
    if (_actionTypeDict.allKeys.count > 3) { //magic number
        trdRules = [_actionTypeDict objectForKey:@"TradeRules"];
    }
    
    
    //set value for 4 Lists
    if (trdRules.orderTypeList) {
        //1
        [orderTypeList addObjectsFromArray: trdRules.orderTypeList];
    }
    
    if (trdRules.validityList) {
        //2
        [orderValidityList addObjectsFromArray:trdRules.validityList];
    }
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    defAPrice = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stock.stockCode] objectForKey:FID_88_D_SELL_PRICE_1];
    
    if ([defAPrice floatValue]<=0) {
        defAPrice = _stock.lastDonePrice;
        //  NSLog(@"zero %f",defPrice);
    }
    if([defAPrice isKindOfClass:[NSString class]]){
        defAPrice = [NSNumber numberWithDouble:[defAPrice doubleValue]];
    }
    // Get Price for Buy
    defBPrice = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stock.stockCode] objectForKey:FID_68_D_BUY_PRICE_1];
    
    if ([defBPrice floatValue]<=0) {
        defBPrice = _stock.lastDonePrice;
        //  NSLog(@"zero %f",defPrice);
    }
    if([defBPrice isKindOfClass:[NSString class]]){
        defBPrice = [NSNumber numberWithDouble:[defAPrice doubleValue]];
    }
//    if ([SettingManager shareInstance].isOnQuantity && self.isOrderBook) {
//        // Get lotSize to division if setting is Lot
//        double lotSize;
//        if ([SettingManager shareInstance].currentQuantityType==1) {
//            // Lot
//            lotSize = [[[[_qcData qcFeedDataDict] objectForKey:_stock.stockCode] objectForKey:FID_40_I_SHARE_PER_LOT] doubleValue];
//        }
//        else {
//            //Unit
//            lotSize=1;
//        }
//        //Get Quantity for Sell
//        defAQty = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stock.stockCode] objectForKey:FID_78_I_SELL_QTY_1];
//        defAQty = @([defAQty doubleValue] / lotSize);
//        if([defAQty isKindOfClass:[NSString class]]){
//            defAQty = [NSNumber numberWithDouble:[defAQty doubleValue]];
//        }
//        // Get Quantity for Buy
//        defBQty = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stock.stockCode] objectForKey:FID_58_I_BUY_QTY_1];
//        defBQty = @([defBQty doubleValue] / lotSize);
//        if([defBQty isKindOfClass:[NSString class]]){
//            defBQty = [NSNumber numberWithDouble:[defBQty doubleValue]];
//        }
//    }
//    else if (![SettingManager shareInstance].isOnQuantity && self.isOrderBook)
//    {
//        NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
//        format.numberStyle = NSNumberFormatterDecimalStyle;
//        defAQty = [format numberFromString:[SettingManager shareInstance].customQuantity];
//        defBQty=[format numberFromString:[SettingManager shareInstance].customQuantity];
//    }
    NSString *actionString = @"BUY";
    if(self.tradeType == TradeType_Sell){
        actionString = @"SELL";
    }
    
    dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
            actionString,@"Action",
            _stock.stockCode,@"StockCode",
            _stock.stockName,@"StockName",
            [defAPrice toCurrencyNumber],@"StockPrice",
            [defAPrice toCurrencyNumber],@"DefaultPrice",
            [NSString stringWithFormat:@"%@ / %@",
             [stkCodeAndExchangeArr objectAtIndex:0],
             _stock.stockName],@"stkNname",
            trdRules, @"TradeRules",
            nil];
    
    self.actionTypeDict = dict;
    
    //Set Stock Exchange
    stkExchange = [Utils stripExchgCodeFromStockCode:_stock.stockCode];
    
    //have stock code
    _lacpValue = [[[[[QCData singleton] qcFeedDataDict] objectForKey: [UserPrefConstants singleton].userSelectingStockCode] objectForKey:FID_51_D_REF_PRICE] floatValue];
    
    [self getMarketDepth];
    [self loadTimeSale];
    [self getBDone];
    
    //Set Current Exchange Data
    for (ExchangeData *ed in [[UserPrefConstants singleton] userExchagelist]) {
        if ([ed.feed_exchg_code isEqual:[[UserPrefConstants singleton] userCurrentExchange]]) {
            currentED = ed;
            exchangeCurrency = ed.currency;
            
            if ([exchangeCurrency isEqualToString:@"RM"])
                exchangeCurrency = @"MYR";
        }
    }
    
    
    NSRange range = [_stock.stockCode rangeOfString:@"."];
    NSString *stockCountry = [[_stock.stockCode substringFromIndex:NSMaxRange(range)]
                              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    // override only if data "PaymentCfg_Payment" available and feed exchange is SG
    if ( ([trdRules.paymentCfgList count]>0) && ([stkExchange isEqualToString:@"SG"]) )
    {
        long index = -1;
        
        for (int i=0; i < [trdRules.paymentCfgList count]; i++)
        {
            PaymentCUT *cut = [trdRules.paymentCfgList objectAtIndex:i];
            
            if(([stockCountry isEqualToString:cut.paymentCountry]) && ([uacd.account_type  isEqualToString:cut.AccountType]))
            {
                index=i;
                [paymentList addObjectsFromArray: [cut paymentOptions] ];
                
                [currencyList addObject: [cut paymentSettings]];
                
                break;
            }
        }
        
        // if not exist for account types above, then add default Payment
        if (index == -1) {
            for(int i=0 ; i < [trdRules.paymentCfgList count] ; i++)
            {
                PaymentCUT *cut = [trdRules.paymentCfgList objectAtIndex:i];
                
                if(([stockCountry isEqualToString:[cut paymentCountry]]) && ([@"Def" isEqual:[cut AccountType]]) )
                {
                    index = i;
                    PaymentCUT *cut2 = [trdRules.paymentCfgList objectAtIndex:index];
                    
                    [paymentList addObjectsFromArray: [cut2 paymentOptions] ];
                    [currencyList addObjectsFromArray:trdRules.currencyList ];
                    break;
                }
            }
        }
    }
    else
    {
        [paymentList addObjectsFromArray: trdRules.paymentList];
        [currencyList addObjectsFromArray: trdRules.currencyList];
    }
    
    if ([currencyList count] == 0 && stockCurrency != nil) {
        [currencyList addObject: stockCurrency];
    }
    
    //Update Price
    if([dict valueForKey:@"StockPrice"]){
        [self updateArrayControl:CONTROL_PRICE value:[dict valueForKey:@"StockPrice"]];
    }
    // Update Qty
    if ([dict valueForKey:@"Qty"]) {
        [self updateArrayControl:CONTROL_QUANTITY value:[dict valueForKey:@"Qty"]];
    }
    if(orderTypeList.count > 0){
        if(!self.isOrderBook){
            [self updateArrayControl:CONTROL_ORDERTYPE value:orderTypeList[0]];
        }
        ordTypeRow = 0;
        [self updateOrdTypeAndValidity];
        [self calculatePrice];
    }
    if(currencyList.count > 0){
        if(!self.isOrderBook){
            [self updateArrayControl:CONTROL_STATEMENTCURRENCY value:currencyList[0]];
        }
        
    }
    if(paymentList.count > 0){
        if(!self.isOrderBook){
            [self updateArrayControl:CONTROL_PAYMENTTYPE value:paymentList[0]];
        }
        
    }
    if(orderValidityList.count > 0){
        if(!self.isOrderBook){
            [self updateArrayControl:CONTROL_VALIDITY value:orderValidityList[0]];
        }
        
    }
    //Reload table
    [self doOrderCtrl:self.isRevise];
}

-(void) updateUIwithStock
{
    NSString * _titleQty;
    lotSizeStringInt = 1;
    if ([SettingManager shareInstance].currentQuantityType == 1 && !self.isOrderBook) {
        lotSizeStringInt = [[[[_qcData qcFeedDataDict] objectForKey:_stock.stockCode] objectForKey:FID_40_I_SHARE_PER_LOT] intValue];
        if(lotSizeStringInt < 1){
            lotSizeStringInt = 1;
        }
        _titleQty =[NSString stringWithFormat:@" x%d Units",lotSizeStringInt];
    } else {
        _titleQty = @" x1 Unit";
    }
    
    //update arr controls
    for (int i = 0; i < arrCollectViewControls.count; i ++) {
        NSMutableDictionary * tmpDic = [NSMutableDictionary dictionaryWithDictionary:arrCollectViewControls[i]];
        if ([tmpDic[@"control_type"] intValue] == CONTROL_QUANTITY) {
            if([SettingManager shareInstance].isOnQuantity){
                tmpDic[@"value"] = [SettingManager shareInstance].customQuantity;
            }
            [arrCollectViewControls replaceObjectAtIndex:i withObject: @{@"control_type" : @(CONTROL_QUANTITY), @"value" : tmpDic[@"value"] , @"lotsize": _titleQty } ];
            break;
        }
    }
    [self calculatePrice];
    [self reloadInputData];
}

- (void)reloadInputData{
    if (tableControl.numberOfSections > 0) {
        NSRange range = NSMakeRange(FIELD_INPUT_DATA, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [tableControl reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
    }
}
/*
 Update value when switch BUY <-> SELL
 */
-(void)updateWhenSwitchBuySell
{
    if (self.tradeType == TradeType_Sell) {
        [self updateArrayControl:CONTROL_QUANTITY value:(NSString*)defBQty];
        [self updateArrayControl:CONTROL_PRICE value:(NSString*)defBPrice];
    }
    if (self.tradeType == TradeType_Buy) {
        [self updateArrayControl:CONTROL_QUANTITY value:(NSString*)defAQty];
        [self updateArrayControl:CONTROL_PRICE value:(NSString*)defAPrice];
    }
}
//MARK: - TABLE HEADER
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    NSDictionary *dicData = arrSections[section];
//
//    if ([dicData[@"TYPE"] intValue] == FIELD_MARKETDEPTH) {
//        return 44 + (isExpandMarket == YES ? 4 : 8);
//
//    }else{
//        return 0.0;
//    }
//}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//
//    NSDictionary *dicData = arrSections[section];
//
//    if ([dicData[@"TYPE"] intValue] == FIELD_MARKETDEPTH) {
//
//        CGRect screenRect = [[UIScreen mainScreen] bounds];
//
//        if(marketInfoHeader == nil){
//            marketInfoHeader=[[MarketHeaderView alloc]initWithFrame:CGRectMake(0, 0, screenRect.size.width, 44)];
//            marketInfoHeader.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketBg);
//            marketInfoHeader.delegate = self;
//            marketInfoHeader.lbl_Title.font = AppFont_MainFontBoldWithSize(15);
//            [marketInfoHeader.lbl_Title setTextAlignment:NSTextAlignmentCenter];
//            [marketInfoHeader.lbl_Title setTextColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketText)];
//            [marketInfoHeader.btn_Icon setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketText)];
//            marketInfoHeader.lbl_Title.numberOfLines=1;
//            [marketInfoHeader setBackgroundColor:[UIColor clearColor]];
//            marketInfoHeader.lbl_Title.text= [LanguageManager stringForKey:@"Market Depth"];
//        }
//
//        //38 68 94
//
//        if (isExpandMarket) {
//            [marketInfoHeader.btn_Icon setImage:[UIImage imageNamed:@"ic_arrow_down"] forState:UIControlStateNormal];
//            marketInfoHeader.cst_Bottom.constant = 0;
//        }
//        else
//        {
//            [marketInfoHeader.btn_Icon setImage:[UIImage imageNamed:@"ic_arrow_up"] forState:UIControlStateNormal];
//            marketInfoHeader.cst_Bottom.constant = -4;
//        }
//
//
//        return  marketInfoHeader;
//    }
//    return nil;
//}
- (void)setupViewsForCell:(MarketDepthNewCell *)cell{
    if(self.stock){
        [cell removeNoData];
        _depthVC.stock = self.stock;
        _depthVC.fullMode = YES;
        _saleVC.stock = self.stock;
        _saleVC.fullMode = YES;
        _bDoneVC.stock = self.stock;
        _bDoneVC.fullMode = YES;
        [_depthVC hideViewMode:YES];
        [_bDoneVC hideViewMode:YES];
        [_saleVC hideViewMode:YES];
        [_depthVC.view removeFromSuperview];
        [_saleVC.view removeFromSuperview];
        [_bDoneVC.view removeFromSuperview];
        if(_depthVC){
            [_depthVC clearColor];
            [_saleVC clearColor];
            [_bDoneVC clearColor];
            
            [self addVC:_depthVC toView:cell.v_MarketDepth];
            [self addVC:_saleVC toView:cell.v_TimeSale];
            [self addVC:_bDoneVC toView:cell.v_BDone];
        }
    }else{
        [cell showNoData];
    }
    
}

#pragma mark - INFO_CellDelegate
- (void)didChangeKeyword:(NSString *)keyword{
    [self searchStock:keyword];
    if([keyword isEqualToString:@""]){
        //
    }
}
- (void)didBeginEditing:(NSString *)keyword{
    if(self.tableDataArr.count > 0){
        [self.dv_searchResult openComponent:0 animated:YES];
    }
}
- (void)actionSearch:(INFO_Cell *)cell{
    NSLog(@"action search");
    [self presentSearchVC];
    
    //
    //    SearchStockVC * searchVC = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [SearchStockVC storyboardID]);
    //    searchVC.myBlock = ^(NSDictionary * _Nullable dic) {
    //        NSLog(@"%@", dic);
    //        _stock = dic[@"key"];
    //        [self fnUpdateDataViaNewStock];
    //    };
    //
    //    [self.navigationController pushViewController:searchVC animated:YES];
    //
    
}

#pragma mark - BUYSELL_CellDelegate
- (void)changeBuySellType:(TradeType)type{
    //0 buy / 1 sell
    
    self.tradeType = type;
    
    [self setupBottomActionsWith:self.uiType tradeType:type];
    [self doOrderCtrl:self.isRevise];
    [self updateWhenSwitchBuySell];
    DLog(@"Updating quantity buy or sell");
}
#pragma mark - TABLEVIEW


/*
 FIELD_INFO,
 FIELD_MARKETDEPTH,
 FIELD_BUYSELL,
 FIELD_ACCOUNT,
 FIELD_INPUT_DATA,
 FIELD_PIN_PASSWORD,
 */

//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.contentMode == TradeContentModeTrade){
        return arrSections.count;
    }else{
        if(arrSections.count > 1){
            return arrSections.count - 1;
        }
    }
    return 0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSDictionary *dicData = arrSections[section];
    
    //market depth
    if ([dicData[@"TYPE"] intValue] == FIELD_MARKETDEPTH ) {
        //
        if (isExpandMarket)
        {
            return 1;
            //return number of item in marketdepth
            //            return arrMarketDepthItems.count + 1; // add first cell for labels
            
        }else{
            //only show header MarketDepth
            return 0;
        }
    }
    
    //only 1 cell static.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = arrSections[indexPath.section];
    if ([dicData[@"TYPE"] intValue] == FIELD_INPUT_DATA) {
        
        int iLines = 0;
        if ( arrCollectViewControls.count % 2 == 0) {
            iLines = (int)arrCollectViewControls.count/2;
        }else{
            iLines = (int)arrCollectViewControls.count /2 + 1;
        }
        //height of input CollectionView Cell
        CGFloat maxHeight = (inputItemheight * iLines) + iLines*kSPACING_ITEM + 16;
        CGFloat tmpHeight = SCREEN_HEIGHT_PORTRAIT - positionOfThirdCard - [self getNavHeight] - self.v_Bottom.frame.size.height;
        if(maxHeight < tmpHeight){
            maxHeight = tmpHeight;
        }
        return maxHeight;
    }else if ([dicData[@"TYPE"] intValue] == FIELD_BUYSELL)  {
        if(self.contentMode != TradeContentModeTrade){
            return SCREEN_HEIGHT_PORTRAIT - positionOfThirdCard - [self getNavHeight];
        }
    }
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //Section
    NSDictionary *dicData = arrSections[indexPath.section];
    
    UITableViewCell * cell = nil;
    SECTION_FIELD_TYPE type = [dicData[@"TYPE"] intValue];
    if(type == FIELD_INFO){
        INFO_Cell * cellInfo = (INFO_Cell *)[tableControl dequeueReusableCellWithIdentifier: identifier_INFO_cell forIndexPath:indexPath];
        cellInfo.delegate = self;
        self.dv_searchResult = cellInfo.v_DropDownStocks;
        [self setupDropDown:self.dv_searchResult title:nil contents:[self getStockNames]];
        if(_stock)
        {
            if([BrokerManager shareInstance].broker.tag == BrokerTag_SSI){
                cellInfo.lbCompany.text = _stock.compName;
                cellInfo.lbStockName.text = _stock.stockCode;
            }else{
                cellInfo.lbStockName.text = _stock.stockName;
                cellInfo.lbCompany.text = [_stock.stockCode stringByDeletingPathExtension];
            }
            
            cellInfo.changeValue.text = [_stock.lastDonePrice toCurrencyNumber];
            cellInfo.changePercent.text = [_stock changePercentValue];
            cellInfo.changeValueContent = [_stock.fChange doubleValue];
        }else{
            if(positionOfThirdCard != 0){
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [cellInfo.lbStockName becomeFirstResponder];
                });
                
            }
            
        }
        
        
        return cellInfo;
    }else if (type == FIELD_MARKETDEPTH){
        
        cell = (MarketDepthNewCell *)[tableControl dequeueReusableCellWithIdentifier: [MarketDepthNewCell reuseIdentifier] forIndexPath:indexPath];
        [self setupViewsForCell:cell];
        
    }else{
        if(self.contentMode == TradeContentModeTrade){
            if(type == FIELD_BUYSELL){
                
                BUYSELL_Cell * cellBuySell = [tableControl dequeueReusableCellWithIdentifier: identifier_BUYSELL_Cell forIndexPath:indexPath];
                cellBuySell.delegate = self;
                cellBuySell.lb_accName.text = m_accountName;
                cellBuySell.lb_accInfor.text = @"";
                NSMutableArray *headers = [self listHeaders];
                if(headers == nil){
                    cellBuySell.lbBuyLimitLabel.text = @"";
                    cellBuySell.lbSellLimitLabel.text = @"";
                    cellBuySell.lbBuyLimit.text  = @"";
                    cellBuySell.lbSellLimit.text = @"";
                }else{
                    for(int index = 0; index < headers.count; index++){
                        if (index == 0) {
                            NSArray *header = [TradeUtils getHeaderAndValueWith:[headers[0] intValue] uacd:self->uacd];
                            cellBuySell.lbBuyLimitLabel.text = header.firstObject;
                            cellBuySell.lbBuyLimit.text  = header.lastObject;
                        }else if(index == 1){
                            NSArray *header = [TradeUtils getHeaderAndValueWith:[headers[1] intValue] uacd:self->uacd];
                            cellBuySell.lbSellLimitLabel.text = header.firstObject;
                            cellBuySell.lbSellLimit.text = header.lastObject;
                        }
                    }
                }
                if(self.uiType == TradeUIType1){
                    [cellBuySell.sg_control setSelectedSegmentIndex:self.tradeType];
                    [cellBuySell changeColor:self.tradeType];
                }else{
                    [cellBuySell disableSegment];
                }
                
                [cellBuySell.btnAccount addTarget:self action:@selector(accountButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                return cellBuySell;
            }else if (type == FIELD_INPUT_DATA){
                //=> Grid
                INPUTDATA_Cell *cell = [tableView dequeueReusableCellWithIdentifier:identifier_INPUTDATA_Cell forIndexPath:indexPath];
                myCollectionView = cell.collectionView;
                [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
                
                //Remove separate line
                [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
                [cell layoutIfNeeded];
                return cell;
            }else if (type == FIELD_PIN_PASSWORD){
                cell = [tableControl dequeueReusableCellWithIdentifier: identifier_PINPASSWORD_Cell forIndexPath:indexPath];
                //            ((PINPASSWORD_Cell *)cell).delegate = self;
                [((PINPASSWORD_Cell *)cell) setCallBackValue:^(NSInteger index, NSDictionary *value) {
                    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                    if([def objectForKey:@"TradingPin"]==nil)
                    {
                        [def setObject:self->password forKey:@"TradingPin"];
                    }
                    else
                    {
                        [def removeObjectForKey:@"TradingPin"];
                    }
                }];
                ((PINPASSWORD_Cell *)cell).validatingType = VAL_PIN;
            }
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"TradeContentCell" forIndexPath:indexPath];
//            ((TradeContentCell *)cell).cst_ContentHeight.constant = SCREEN_HEIGHT_PORTRAIT - positionOfThirdCard - [self getNavHeight];
            if(self.contentMode == TradeContentModeConfirm){
                [skipVC.view removeFromSuperview];
                [self addVC:skipVC toView:((TradeContentCell *)cell).v_Content];
            }else{
                [tsVC.view removeFromSuperview];
                [self addVC:tsVC toView:((TradeContentCell *)cell).v_Content];
            }
        }
        
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath section] == FIELD_BUYSELL && positionOfThirdCard == 0){
        CGRect rectOfCellInTableView = [tableView rectForRowAtIndexPath: indexPath];
        CGRect rectOfCellInSuperview = [tableView convertRect: rectOfCellInTableView toView: tableView.superview];
        positionOfThirdCard = rectOfCellInSuperview.origin.y;
        [tableView reloadData];
    }
}
-(IBAction)expandAction:(id)sender
{
    isExpandMarket = !isExpandMarket;
    
    [tableControl reloadData];
}

#pragma mark - UICollectionView DataSource
- (NSString *)getTitleWith:(CONTROL_INPUTFIELD_TYPE)type{
    switch (type) {
        case CONTROL_ORDERTYPE:
            return [LanguageManager stringForKey:@"Order Type"];
        case CONTROL_TRIGGER:
            return [LanguageManager stringForKey:@"Trigger"];
        case CONTROL_TRIGGERPRICE:
            return [LanguageManager stringForKey:@"Trigger Price"];
        case CONTROL_DIRECTION:
            return [LanguageManager stringForKey:@"Direction"];
        case CONTROL_DISCLOSED:
            return [LanguageManager stringForKey:@"Disclosed"];
        case CONTROL_QUANTITY:
            return [LanguageManager stringForKey:@"Qty"];
        case CONTROL_MINQUANTITY:
            return [LanguageManager stringForKey:@"Qty.Min Units"];
        case CONTROL_PRICE:
            return [LanguageManager stringForKey:@"Price"];
        case CONTROL_STATEMENTCURRENCY:
            return [LanguageManager stringForKey:@"Sett.Curr"];
        case CONTROL_VALIDITY:
            return [LanguageManager stringForKey:@"Validity"];
        case CONTROL_PAYMENTTYPE:
            return [LanguageManager stringForKey:@"Payment"];
        case CONTROL_SHORTSELL:
            return [LanguageManager stringForKey:@"Short Sell"];
        case CONTROL_SKIPCONFIRMATION:
            return [LanguageManager stringForKey:@"Skip confirmation"];
        case CONTROL_GROSS:
            return @"";
        default:
            return @"";
    }
}
- (NSString *)getStringValues:(NSDictionary *)dic{
    id value = [dic valueForKey:@"value"];
    if([value isKindOfClass:[NSString class]]){
        return value;
    }else if([value isKindOfClass:[NSNumber class]]){
        return [value stringValue];
    }
    return @"";
}
- (CollectionCell_OrderType *)setupDropDownCell:(UICollectionView *)collectionView type:(CONTROL_INPUTFIELD_TYPE)type cellForItemAtIndexPath:(NSIndexPath *)indexPath dicControl:(NSDictionary *)dicControl isHightLight:(BOOL)isHightLight{
    CollectionCell_OrderType *cell = (CollectionCell_OrderType*)[collectionView dequeueReusableCellWithReuseIdentifier:[CollectionCell_OrderType reuseIdentifier] forIndexPath:indexPath];
    cell.index = indexPath.row;
    cell.delegate = self;
    cell.lbl_Title.text = [self getTitleWith:type];
    cell.lb_content.text = dicControl[@"value"] ;
    cell.isHightLight = NO;
    return cell;
}

- (void)updateDataForInputTextCell:(CollectionCell_TriggerPrice *)cell andValue:(NSString *)value{
    cell.tf_Input.text = value;
}
- (CollectionCell_TriggerPrice *)setupInputTextCell:(UICollectionView *)collectionView type:(CONTROL_INPUTFIELD_TYPE)type cellForItemAtIndexPath:(NSIndexPath *)indexPath dicControl:(NSDictionary *)dicControl maxDecimalNumber:(int)maxDecimalNumber{
    CollectionCell_TriggerPrice *cell = (CollectionCell_TriggerPrice*)[collectionView dequeueReusableCellWithReuseIdentifier:[CollectionCell_TriggerPrice reuseIdentifier] forIndexPath:indexPath];
    cell.delegate = self;
    cell.maxDecimalNumber = maxDecimalNumber;
    cell.isInputNumber = YES;
    cell.lbl_Title.text = [self getTitleWith:type];
    NSString *value = [self getStringValues:dicControl];
    if(value && [value doubleValue] != 0){
        if(maxDecimalNumber > 0){
            cell.tf_Input.text = [[NSNumber numberWithDouble:[value doubleValue]] toCurrencyNumber];
        }else{
            cell.tf_Input.text = value;
        }
    }else{
        cell.tf_Input.text = @"";
    }
    cell.lbl_subTitle.text = @"";
    cell.index= indexPath.row;
    [cell setCallBackValue:^(NSInteger index, NSDictionary *value)
     {
         NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary: self->arrCollectViewControls[index]];
         
         [dicMul removeObjectForKey:@"value"];
         [dicMul setObject:value[@"value"] forKey:@"value"];
         
         //update
         //         for the quantity, can we auto put in the thousand seperator?
         //         also for the contract value. it should auto calculate the value base on QTY X Price
         
         [self->arrCollectViewControls replaceObjectAtIndex:index withObject:dicMul];
         
         [self calculatePrice];
         
     }];
    
    return cell;
}
- (CollectionCell_CheckBox *)setupCheckBoxCell:(UICollectionView *)collectionView type:(CONTROL_INPUTFIELD_TYPE)type cellForItemAtIndexPath:(NSIndexPath *)indexPath dicControl:(NSDictionary *)dicControl{
    CollectionCell_CheckBox *cell = (CollectionCell_CheckBox*)[collectionView dequeueReusableCellWithReuseIdentifier:[CollectionCell_CheckBox reuseIdentifier] forIndexPath:indexPath];
    cell.lbl_Title.text = [self getTitleWith:type];
    cell.index = indexPath.row;
    [cell setCallBackValue:^(NSInteger index, NSDictionary *value)
     {
         [self actionWhenCheckingCell:index value:value];
     }];
    
    return cell;
}
- (CollectionCell_Label *)setupLabelCell:(UICollectionView *)collectionView type:(CONTROL_INPUTFIELD_TYPE)type cellForItemAtIndexPath:(NSIndexPath *)indexPath dicControl:(NSDictionary *)dicControl{
    CollectionCell_Label *cell = (CollectionCell_Label*)[collectionView dequeueReusableCellWithReuseIdentifier:[CollectionCell_Label reuseIdentifier] forIndexPath:indexPath];
    [self setDataForCell:cell type:type dicControl:dicControl];
    return cell;
}

- (void)setDataForCell:(CollectionCell_Label *)cell type:(CONTROL_INPUTFIELD_TYPE)type dicControl:(NSDictionary *)dicControl {
    cell.lbl_Title.text = [self getTitleWith:type];
    cell.lbl_Value.text = [dicControl valueForKey:@"value"];
    if(type == CONTROL_GROSS){
        cell.lbl_Value.font = [UIFont boldSystemFontOfSize:15];
    }else{
        cell.lbl_Value.font = [UIFont boldSystemFontOfSize:12];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(([BrokerManager shareInstance].broker.tag == BrokerTag_SSI)){
        NSInteger total = arrCollectViewControls.count;
        if(total > 0){
            total--;//Remove gross value;
            if(self.v_SkipSSI.index != -1){
                total--;
            }
            if(self.v_ShortSellSSI.index != -1){
                total--;
            }
        }
        
        return total;
    }else{
        return arrCollectViewControls.count;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary* dicControl = arrCollectViewControls[indexPath.row];
    
    switch ([dicControl[@"control_type"] intValue]) {
        case CONTROL_ORDERTYPE:
        {
            return [self setupDropDownCell:collectionView type:CONTROL_ORDERTYPE cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:YES];
        }
            
        case CONTROL_TRIGGER:
        {
            return [self setupDropDownCell:collectionView type:CONTROL_TRIGGER cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:NO];
        }
        case CONTROL_TRIGGERPRICE:
        {
            return [self setupInputTextCell:collectionView type:CONTROL_TRIGGERPRICE cellForItemAtIndexPath:indexPath dicControl:dicControl maxDecimalNumber:_maxPriceDecimalNumber];
            //TextField
        }
        case CONTROL_DIRECTION:
        {
            return [self setupDropDownCell:collectionView type:CONTROL_DIRECTION cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:NO];
        }
            
        case CONTROL_DISCLOSED:
        {
            return [self setupDropDownCell:collectionView type:CONTROL_DISCLOSED cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:NO];
        }
            
        case CONTROL_QUANTITY:
        {
            CollectionCell_TriggerPrice *cell = [self setupInputTextCell:collectionView type:CONTROL_QUANTITY cellForItemAtIndexPath:indexPath dicControl:dicControl maxDecimalNumber:0];
            cell.lbl_subTitle.text = dicControl[@"lotsize"];
            return cell;
            
        }
        case CONTROL_MINQUANTITY:
        {
            return [self setupInputTextCell:collectionView type:CONTROL_MINQUANTITY cellForItemAtIndexPath:indexPath dicControl:dicControl maxDecimalNumber:0];
            //TextField
        }
        case CONTROL_PRICE:
        {
            NSMutableDictionary *tmpDic = [NSMutableDictionary dictionaryWithDictionary:dicControl];
            if(![SettingManager shareInstance].isOnPrice && !self.isOrderBook && [tmpDic valueForKey:@"value"]){
                [tmpDic setObject:@"" forKey:@"value"];
                
                //update
                //         for the quantity, can we auto put in the thousand seperator?
                //         also for the contract value. it should auto calculate the value base on QTY X Price
                
                [self->arrCollectViewControls replaceObjectAtIndex:indexPath.row withObject:tmpDic];
            }
            return [self setupInputTextCell:collectionView type:CONTROL_PRICE cellForItemAtIndexPath:indexPath dicControl:tmpDic maxDecimalNumber:_maxPriceDecimalNumber];
            //TextField
        }
            
            //egL SGD / MYR
        case CONTROL_STATEMENTCURRENCY:
        {
            return [self setupDropDownCell:collectionView type:CONTROL_STATEMENTCURRENCY cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:NO];
        }
            
            //DAY / GTD
        case CONTROL_VALIDITY:
        {
            return [self setupDropDownCell:collectionView type:CONTROL_VALIDITY cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:NO];
        }
            
            //CFD
        case CONTROL_PAYMENTTYPE:
        {
            return [self setupDropDownCell:collectionView type:CONTROL_PAYMENTTYPE cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:NO];
        }
        case CONTROL_SHORTSELL:
        {
            return [self setupCheckBoxCell:collectionView type:CONTROL_SHORTSELL cellForItemAtIndexPath:indexPath dicControl:dicControl];
        }
        case CONTROL_SKIPCONFIRMATION:
        {
            return [self setupCheckBoxCell:collectionView type:CONTROL_SKIPCONFIRMATION cellForItemAtIndexPath:indexPath dicControl:dicControl];
        }
        case CONTROL_GROSS:
        {
            return [self setupLabelCell:collectionView type:CONTROL_GROSS cellForItemAtIndexPath:indexPath dicControl:dicControl];
        }
            
        default:
            return [self setupDropDownCell:collectionView type:CONTROL_PAYMENTTYPE cellForItemAtIndexPath:indexPath dicControl:dicControl isHightLight:NO];
    }
    
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary* dicControl = arrCollectViewControls[indexPath.row];
    int type = [dicControl[@"control_type"] intValue];
    if(type == CONTROL_GROSS){
        if (arrCollectViewControls.count%3==2){
            return CGSizeMake(SCREEN_WIDTH_PORTRAIT/2 - 20, inputItemheight);
        }
        return CGSizeMake(SCREEN_WIDTH_PORTRAIT - 20, inputItemheight);
    }else if (type == CONTROL_SKIPCONFIRMATION){
        if (arrCollectViewControls.count%3==0){
            return CGSizeMake(SCREEN_WIDTH_PORTRAIT/2 - 20, inputItemheight);
        }
        return CGSizeMake(SCREEN_WIDTH_PORTRAIT - 20, inputItemheight);
    }
    return CGSizeMake(SCREEN_WIDTH_PORTRAIT/3 - 15, inputItemheight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return kSPACING_ITEM;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return kSPACING_ITEM;;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);  // top, left, bottom, right
}

- (NSArray *)getListContent:(NSInteger)iType{
    //Popup with content list
    NSArray *listContent = nil;
    NSDictionary* dicControl = arrCollectViewControls[iType];
    switch ([dicControl[@"control_type"] intValue]) {
        case CONTROL_ORDERTYPE:
        {
            listContent = [orderTypeList copy];
        }
            break;
        case CONTROL_TRIGGER:
        {
            listContent = trdRules.triggerPriceTypeList;
            
        }
            break;
        case CONTROL_DIRECTION:
        {
            listContent = trdRules.triggerPriceDirectionList;
        }
            break;
        case CONTROL_STATEMENTCURRENCY:
        {
            listContent = [currencyList copy];
        }
            break;
        case CONTROL_VALIDITY:
        {
            listContent = [orderValidityList copy];
        }
            break;
        case CONTROL_PAYMENTTYPE:
        {
            listContent = [paymentList copy];
        }
            break;
        default:
            break;
    }
    return listContent;
}
- (void)showDictOptions:(NSIndexPath *)indexPath{
    NSInteger iType = indexPath.item;
    NSArray *listContent = [self getListContent:iType];
    NSDictionary* dicControl = arrCollectViewControls[iType];
    UICollectionViewLayoutAttributes *attributes = [myCollectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellFrameInSuperview = [myCollectionView convertRect:attributes.frame toView:nil];
    if (listContent.count == 0)
        return;
    CGFloat dropdownWidth = cellFrameInSuperview.size.width;
    if(dropdownWidth < minDropDownWidth){
        dropdownWidth = minDropDownWidth;
    }
    TCMenuDropDown *popup = [[TCMenuDropDown alloc] initWithFrame:CGRectMake(cellFrameInSuperview.origin.x, cellFrameInSuperview.origin.y, dropdownWidth, cellFrameInSuperview.size.height)];
    
    ((CollectionCell_OrderType *)[myCollectionView cellForItemAtIndexPath:indexPath]).cst_DropDownWidth.constant = dropdownWidth - 8;
    popup.sourceView = ((CollectionCell_OrderType *)[myCollectionView cellForItemAtIndexPath:indexPath]).v_DropDown;
    popup.titleAlign =  NSTextAlignmentLeft;
    popup.radius = 12;
    popup.height = 28;
    popup.titleMenu = [LanguageManager stringForKey:@""];
    popup.numberItemVisible = 6;
    popup.tintIcon = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintExchangeDropDownButtonColor);
    popup.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgExChangeColor);
    popup.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
    popup.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgExChangeColor);
    popup.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
    popup.menuTitles = listContent;
    [popup reloadDropDown];
    __weak MTradeViewController *weakSelf = self;
    [popup setPopCellBlock:^(TCMenuDropDown *popupVC, NSInteger row, NSInteger section) {
        MTradeViewController *strongSelf = weakSelf;
        //update value for selected item
        NSMutableDictionary* mutDic = [strongSelf->arrCollectViewControls[iType] mutableCopy] ;
        mutDic[@"value"] = listContent[row];
        //Update selected value
        [strongSelf->arrCollectViewControls replaceObjectAtIndex:(int)iType withObject:mutDic];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Refresh the list
            
            switch ([dicControl[@"control_type"] intValue]) {
                case CONTROL_ORDERTYPE:
                {
                    self->ordTypeRow =row;
                    [strongSelf updateOrdTypeAndValidity];
                    [strongSelf calculatePrice];
                }
                    break;
                case CONTROL_STATEMENTCURRENCY:
                {
                    self->currencyRow =row;
                    [strongSelf calculatePrice];
                }
                    break;
                case CONTROL_VALIDITY:
                {
                    self->validityRow = row;
                    //If GTD -> SHOW Time selection
                    NSString *currentValidity = strongSelf->orderValidityList[strongSelf->validityRow];
                    if ([currentValidity isEqualToString:@"GTD"]) {
                        [strongSelf showDatePicker];
                    }else{
                        [strongSelf updateOrdTypeAndValidity];
                    }
                    
                }
                    break;
                case CONTROL_PAYMENTTYPE:
                {
                    strongSelf->paymentRow =row;
                    if ([strongSelf->paymentList count]>0) {
                        // need to call API again because depends on isCUT or not
                        [strongSelf refreshTrdLmtBtnPressed:nil];
                        
                    }
                }
                    break;
                default:
                    break;
            }
            
            //Reload data
            [strongSelf->myCollectionView reloadData];
        });
    }];
    
    [popup openComponent:0 animated:YES];
}


//Mark:- Delegate Calendar
//
- (void)showDatePicker{
    CGSize defaultSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.7 * SCREEN_HEIGHT_PORTRAIT);
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:[NSBundle mainBundle]];
    WWCalendarTimeSelector *datePicker = [sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    datePicker.delegate = self;
    //setColor
    
    datePicker.dateFormat = kCALENDARPICKER_FORMAT_DATE;
    datePicker.optionTintColor = UIColorFromHex(0x2E3E49);
    datePicker.optionCalendarFontColorToday = UIColorFromHex(0x1493FC);
    datePicker.optionEnableCallDoneWhenSelectDate = FALSE;
    datePicker.optionButtonShowCancel = TRUE;
    datePicker.optionStyles.showDateMonth = TRUE;
    datePicker.optionStyles.showTime = FALSE;
    datePicker.optionStyles.showMonth = FALSE;
    
    datePicker.optionButtonShowBottomActions = TRUE;
    datePicker.optionSelectorPanelOffsetHighlightDate = 0;
    [self showPopupWithContent:datePicker inSize:defaultSize completion:^(MZFormSheetController *formSheetController) {
        
    }];
}

#pragma mark DatePickerDelegate
-(void) WWCalendarTimeSelectorDone:(WWCalendarTimeSelector *)selector date:(NSDate *)date
{
    [self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
        //Convert date local -> date Paris
        NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
        [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
        [formatter_local setDateFormat:@"dd/MM/yyyy"];
        
        NSString *date_Local = [formatter_local stringFromDate:date];
        [self updateArrayControl:CONTROL_VALIDITY value:date_Local];
        NSRange range = NSMakeRange(FIELD_INPUT_DATA, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [tableControl reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
    }];
    
}

- (void)WWCalendarTimeSelectorCancel:(WWCalendarTimeSelector *)selector date:(NSDate *)date{
    [selector dismissPopupWithCompletion:nil];
}


-(NSDate *)convertNSStringDateToNSDate:(NSString *)string
{
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSDate *convertedDate = [dateFormatter dateFromString:dateString];
    //[dateFormatter release];
    return convertedDate;
}

// TRADER VC USED FOR ANY BUY,SELL,REVISE,CANCEL an order
//
#pragma mark - View LifeCycles delegate



-(void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    //Testing
    // remove width constaint
    
    //    for (NSLayoutConstraint *constraint in mOrderPad1.constraints) {
    //        if (constraint.firstAttribute == NSLayoutAttributeTop || constraint.firstAttribute == NSLayoutAttributeBottom) {
    //            [mOrderPad1 removeConstraint: constraint];
    //            break;
    //        }
    //    }
    //
    
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)backButtonTapped:(id)sender{
    if(isShowingSearchVC){
        [self presentSearchVC];
    }else{
//        [self enableActionButtons:YES];
//        if(self.contentMode != TradeContentModeConfirm){
            [super backButtonTapped:sender];
            [self removeNotifications];
//        }
    }
}

- (void)registerNotifications{
    [self removeNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedBusinessDone:) name:@"didFinishedBusinessDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(validateTrade2FAPinForDeviceDone:) name:@"validateTrade2FAPinForDeviceDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(atpValidate1FADone:) name:@"atpValidate1FADone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelTrade:) name:@"dismissAllModalViews" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeLimitReceived) name:@"tradeLimitReceived" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TradeSubmitStatus:) name:@"TradeSubmitStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeReviseNotification:) name:@"tradeReviseNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeCancelNotification:) name:@"tradeCancelNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mdPriceTappedNotification:) name:@"mdPriceTappedNotification" object:nil];
}

- (void)removeNotifications{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didFinishedBusinessDone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"validateTrade2FAPinForDeviceDone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"atpValidate1FADone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissAllModalViews" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"tradeLimitReceived" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"TradeSubmitStatus" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"tradeReviseNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"tradeCancelNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"mdPriceTappedNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kDidFinishMarketDepthNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kDidFinishedTimeAndSalesNotification object:nil];
    
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (keyUP) {
        [self shiftView:2 forTextField:activeTextField];
        keyUP = NO;
        [activeTextField resignFirstResponder];
    }
    atp.delegate = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = self.v_InputKeyboard.frame.size.height;
    [[IQKeyboardManager sharedManager] setEnable:_stock];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:_stock];
    
    if (_stock.stockCode == nil) {
        if(isFirst){
            isFirst = NO;
            if(![SettingManager shareInstance].isOnOrderType2){
                [self presentSearchVC];
            }
            
        }else{
            [self backVC:nil];
        }
    }
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [IQKeyboardManager sharedManager].keyboardDistanceFromTextField = 10;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) doneWithNumberPad
{
    [activeTextField resignFirstResponder];
    //hiden quick buttons input
    if (keyUP) {
        [self shiftView:2 forTextField:activeTextField];
        keyUP = NO;
    }
    
}

-(void)updateOrdTypeAndValidity {
    [self findAndReplacePlusCurrency];
    //    NSString *ordType = orderTypeList[ordTypeRow];
    //
    //    NSString *coloredLblStr;
    [self doOrderCtrl:self.isRevise];
    
    //    coloredLblStr = [NSString stringWithFormat:@"%@ (%@) \n%@",[LanguageManager stringForKey:action],ordType    [_actionTypeDict objectForKey:@"stkNname"]];
    //
    //    [stkNname setText:coloredLblStr];
    
    //    [self calculatePrice];
}

- (void)showPopupVC:(UIViewController *)vc{
    [[AppState sharedInstance] forceRemoveLoadingView:self.view];
    [self enableActionButtons:YES];
    if(isShowing){
        return;
    }
    CGSize contentSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.6 * SCREEN_HEIGHT_PORTRAIT);
    if(0.6 * SCREEN_HEIGHT_PORTRAIT < 550){
        contentSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 550);
    }
    [self showPopupWithContent:vc inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
        self->isShowing = YES;
    }];
}
- (void)didDismissPopup{
    isShowing = NO;
}
//MARK:- Button pressed event

- (IBAction)btnSkipConfirmationPressed:(UIButton *)sender {
    //    [atp timeoutActivity];
    //    [btnSkipConfirmation setSelected:!sender.selected];
}

- (IBAction) accountButtonPressed:(id)sender {
    if(_stock == nil){
        return;
    }
    if (keyUP) {
        [self shiftView:2 forTextField:activeTextField];
        keyUP = NO;
    }
    [self.view endEditing: YES];
    [atp timeoutActivity];
    // only allow change account on Buy and Sell action only
    
    AccountsVC *_accountVC = NEW_VC_FROM_NIB([AccountsVC class], [AccountsVC nibName]);
    
    //    MTradeAccountVC *_previewVC = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, @"MTradeAccountVC");
    _accountVC.CallBackValue = ^(UserAccountClientData *account) {
        [self updateTradingAccount:account];
    };
    
    
    [self nextTo:_accountVC animate:YES];
    
}

- (IBAction)refreshTrdLmtBtnPressed:(id)sender {
    
    [atp timeoutActivity];
    
    NSLog(@"isCut %d",[self checkIsSelectedTrdCut]);
    
}
- (BOOL)checkIsSelectedTrdCut{
    NSString *paytype;
    if ([paymentList count]>=paymentRow+1)
        paytype = paymentList[paymentRow];
    
    BOOL isCut = ([paytype isEqualToString:@"CUT"])?YES:NO;
    return isCut;
}
- (NSString *)selectedCurrency{
    NSString *selectedCurr;
    if ([currencyList count]>=currencyRow+1)
        selectedCurr = currencyList[currencyRow];
    return selectedCurr;
}

- (IBAction)dateBtnPressed:(UIButton *)sender {
    
    if (keyUP) {
        [self shiftView:2 forTextField:activeTextField];
        keyUP = NO;
        [activeTextField resignFirstResponder];
    }
    
    [atp timeoutActivity];
    
    //Adding picker...
    //Overlay vc for Picker view
    [self addOverLayVC];
    [datePicker removeFromSuperview];
    
    //show picker
    
    [overlayVC toggleOverlay];
    
    if (dateRow == nil)
        datePicker = [[N2DatePicker alloc] initWithDate: [NSDate date]];
    else
    {
        [datePicker setPickerDate:dateRow animated:TRUE];
    }
    
    datePicker.myDelegate = self;
    overlayVC.mPickerView.delegate = nil;
    
    overlayVC.mPickerView.hidden = TRUE;
    
    UIView *subview = datePicker;
    
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    
    [overlayVC.vContainer addSubview:subview];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(subview);
    
    [overlayVC.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[subview]-0-|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:views]];
    [overlayVC.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[subview]-0-|"
                                                                                 options:0
                                                                                 metrics:nil
                                          
                                                                                   views:views]];
    [overlayVC.vContainer setNeedsDisplay];
}

-(CGFloat)getPriceValue:(long)tag {
    
    CGFloat result = 0;
    switch (tag) {
        case -1:
            result = -1.0;
            break;
        case -2:
            result = -0.1;
            break;
        case -3:
            result = -0.01;
            break;
        case -4:
            result = -2.0;
            break;
        case -5:
            result = -5.0;
            break;
        case -6:
            result = -0.001;
            break;
        case 1:
            result = 1.0;
            break;
        case 2:
            result = 0.1;
            break;
        case 3:
            result = 0.01;
            break;
        case 4:
            result = 2.0;
            break;
        case 5:
            result = 5.0;
            break;
        case 6:
            result = 0.001;
            break;
        default:
            break;
    }
    
    return result;
}


- (IBAction)optionBtnPressed:(UIButton *)sender {
    
    //trigger tipe ~ textTriggerType + textTriggerDirection
    if (keyUP) {
        [self shiftView:2 forTextField:activeTextField];
        keyUP = NO;
        [activeTextField resignFirstResponder];
    }
    
    
    [atp timeoutActivity];
    //reset picker
    [pickerList removeAllObjects];
    
    //Adding picker...
    //Overlay vc for Picker view
    [self addOverLayVC];
    //    [datePicker removeFromSuperview];
    
    overlayVC.mPickerView.hidden = FALSE;
    
    [overlayVC.vContainer addSubview:overlayVC.mPickerView];
    
    UIView *subview2 = overlayVC.mPickerView;
    
    subview2.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(subview2);
    
    [overlayVC.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[subview2]-0-|"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:views]];
    [overlayVC.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[subview2]-0-|"
                                                                                 options:0
                                                                                 metrics:nil
                                          
                                                                                   views:views]];
    [overlayVC.vContainer setNeedsDisplay];
    
    //show picker
    myPickerView = overlayVC.mPickerView;
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    
    [overlayVC toggleOverlay];
    
    switch (sender.tag) {
            
        case 3: { // Trigger Type
            
            for (int i =0; i<=trdRules.triggerPriceTypeList.count-1;i++) {
                if (![pickerList containsObject:[trdRules.triggerPriceTypeList objectAtIndex:i]])
                    [pickerList addObject:[trdRules.triggerPriceTypeList objectAtIndex:i]];
                
            }
            myPickerView.tag = 3;
            if ([pickerList count]>0)[myPickerView selectRow:triggerRow inComponent:0 animated:YES];
        }break;
        case 4: { // Direction
            
            for (int i =0; i<=trdRules.triggerPriceDirectionList.count-1;i++) {
                if (![pickerList containsObject:[trdRules.triggerPriceDirectionList objectAtIndex:i]])
                    [pickerList addObject:[trdRules.triggerPriceDirectionList objectAtIndex:i]];
                
            }
            myPickerView.tag = 4;
            if ([pickerList count]>0)[myPickerView selectRow:directionRow inComponent:0 animated:YES];
        }break;
        default:
            break;
    }
    [myPickerView reloadAllComponents];
}

-(void) addOverLayVC
{
    //Overlay VC
    if (overlayVC == nil) {
        overlayVC = [[OverlayVC alloc] initWithNibName:@"OverlayVC" bundle:nil];
    }
    
    [overlayVC.view removeFromSuperview];
    
    UIView *subview = overlayVC.view;
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    [AppDelegateAccessor.window.rootViewController.view addSubview:subview];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(subview);
    
    [AppDelegateAccessor.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[subview]-0-|"
                                                                                                               options:0
                                                                                                               metrics:nil
                                                                                                                 views:views]];
    [AppDelegateAccessor.window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[subview]-0-|"
                                                                                                               options:0
                                                                                                               metrics:nil
                                                                        
                                                                                                                 views:views]];
    [overlayVC.view setNeedsDisplay];
}


//MARK:- Userful functions
- (void)getBDone{
    NSString *stkCode = _stock.stockCode;
    
    NSArray* arrayWithTwoStrings = [stkCode componentsSeparatedByString:@"."];
    
    if (stkCode !=nil) {
        
        [[VertxConnectionManager singleton] vertxGetBusinessDoneByStockCode:stkCode exchg:[UserPrefConstants singleton].userCurrentExchange];
        
    }
}
- (void)getMarketDepth
{
    
    NSString *stkCode = _stock.stockCode;
    
    NSArray* arrayWithTwoStrings = [stkCode componentsSeparatedByString:@"."];
    
    if (stkCode !=nil) {
        
        [[VertxConnectionManager singleton] vertxGetMarketDepth:stkCode andCollarr:[arrayWithTwoStrings objectAtIndex:1]];
        
    }
    
}
- (void)loadTimeSale{
    NSString *stkCode = _stock.stockCode;
    if (stkCode !=nil) {
        NSString *tranNo = [[[[QCData singleton]qcFeedDataDict]objectForKey:stkCode ]objectForKey:FID_103_I_TRNS_NO];
        [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:self.stock.stockCode begin:@"0"
                                                                        end:tranNo exchange:[UserPrefConstants singleton].userCurrentExchange];
    }
    
}
-(BOOL)ShortSellEnable {
    
    BOOL result = NO;
    NSString *tradeExchgCode = currentED.trade_exchange_code;
    result = [[[UserPrefConstants singleton].TrdShortSellIndicator objectForKey:tradeExchgCode] boolValue];
    return result;
}

-(NSDictionary*) getObjControl: (CONTROL_INPUTFIELD_TYPE) iField
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"control_type  = %d", iField];
    NSArray *filteredArray = [arrCollectViewControls filteredArrayUsingPredicate:predicate];
    
    if ([filteredArray count] == 0)
    {
        //not exist
        return  @{@"control_type" : @(iField), @"value" : @"" };
    }
    
    return filteredArray[0];
}

- (void)doOrderCtrl:(BOOL)forRevise {
    //    NSMutableArray *a = [[UserPrefConstants singleton] userExchagelist];
    if (orderTypeList.count == 0) {
        return;
    }
    [self setupDataForArrayControls];
    //    [self updateArrayControl:CONTROL_PRICE value:[defPrice toCurrencyNumber]];
    
    //Get OrderControl or ReviseControl object from dictionary by current selected order type
    OrderControl *oc = nil;
    
    // NSLog(@"2 self.orderTypeList %@",self.orderTypeList);
    
    NSString *ordType = [orderTypeList objectAtIndex:ordTypeRow];
    
    NSLog(@"ordType %@", ordType);
    
    if (!forRevise) {
        oc = [trdRules.ordCtrlConditions objectForKey:ordType];
    }
    else {
        oc = [trdRules.reviseCtrlConditions objectForKey:ordType];
    }
    
    if (oc) {
        if (!forRevise) {
            //Validity List that are allowed to select
            if ([orderValidityList count] > 0) {
                [orderValidityList removeAllObjects];
            }
            [orderValidityList addObjectsFromArray:oc.validityList];
            [selectorPicker reloadComponent:1];
        }
        
        //Enable Text Fields that are allowed to key in
        NSMutableArray * fieldsWillbeEnabled = [NSMutableArray new];
        
        [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_ORDERTYPE] ];
        
        if ([oc.enable_fields containsObject:@"TriggerPriceType"]) {
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_TRIGGER] ];
        }
        
        if ([oc.enable_fields containsObject:@"StopLimit"]) {
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_TRIGGERPRICE] ];
        }
        
        
        
        if ([oc.enable_fields containsObject:@"TriggerPriceDirection"]) {
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_DIRECTION] ];
        }
        
        // CONTROL_DISCLOSED  is for Iceberg order type => we don't have this order type in demo system
        NSString *currentValidity = orderValidityList[validityRow];
        
        if ([oc.enable_fields containsObject:@"Disclosed"]) {
            if ([oc.disclosed_validityList count] > 0) {
                if ([oc.disclosed_validityList containsObject:currentValidity]) {
                    [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_DISCLOSED] ];
                }
            }
            else {
                [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_DISCLOSED] ];
            }
        }
        
        if ([oc.enable_fields containsObject:@"Qty"]) {
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_QUANTITY] ];
        }
        
        
        if ([oc.enable_fields containsObject:@"Min"]) {
            if ([oc.min_validityList count] > 0) {
                if ([oc.min_validityList containsObject:currentValidity]) {
                    [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_MINQUANTITY] ];
                }
            }
            else {
                [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_MINQUANTITY] ];
            }
        }
        
        if ([oc.enable_fields containsObject:@"Price"]) {
            
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_PRICE] ];
        }
        
        
        //Default included:
        if(currencyList.count > 0){
            if([BrokerManager shareInstance].broker.tag != BrokerTag_SSI){
                [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_STATEMENTCURRENCY]];
            }
            
        }
        if(paymentList.count > 0){
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_PAYMENTTYPE] ];
        }
        if(orderValidityList.count > 0){
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_VALIDITY] ];
        }
        
        //dump data
        //Short
        if ([self ShortSellEnable] && self.tradeType == TradeType_Sell)
        {
            [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_SHORTSELL]];
            if(([BrokerManager shareInstance].broker.tag == BrokerTag_SSI)){
                self.v_ShortSellSSI.index = fieldsWillbeEnabled.count - 1;
                [self.v_ShortSellSSI setHidden: NO];
            }
        }

        //Btn Skip Confirmation
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        
        if ( [UserPrefConstants singleton].skipPin == NO )
        {
            if (([def objectForKey:@"TradingPin"] == NO)||([def objectForKey:@"TradingPin"]==nil)) {
                
                //Hide Skip confirmation
                
                //No skip PIN -> Request PIN
                //                [_rememberPin setHidden:NO];
                
                
                //                [self showTradingPinAlert];
                
            }
            else
            {
                //hide PIN...because TradingPin
                [UserPrefConstants singleton].orderDetails.pin = [def objectForKey:@"TradingPin"];
                
                //Show btn Skip Confirmation
                [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_SKIPCONFIRMATION]];
                if(([BrokerManager shareInstance].broker.tag == BrokerTag_SSI)){
                    self.v_SkipSSI.index = fieldsWillbeEnabled.count - 1;
                    [self.v_SkipSSI setHidden: NO];
                }
            }
        }
        [fieldsWillbeEnabled addObject: [self getObjControl: CONTROL_GROSS]];
        
        
        
        //reset list
        [arrCollectViewControls removeAllObjects];
        [arrCollectViewControls addObjectsFromArray:fieldsWillbeEnabled];
        
        //if (!forRevise) {
        //        if ([currentValidity isEqualToString:@"GTD"]) {
        //            [self setFieldEnable:YES  forView:dateView];
        //            [dateView layoutIfNeeded];
        //        }
        //}
    }
    
    //    NSRange range = NSMakeRange(FIELD_INPUT_DATA, 1);
    //    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
    //    [tableControl reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
    
    
    
    [tableControl reloadData];
}
- (NSArray *)updateArrayControl:(CONTROL_INPUTFIELD_TYPE)key value:(NSString *)value{
    for (int i = 0; i < arrCollectViewControls.count; i ++) {
        NSMutableDictionary * tmpDic = [[NSMutableDictionary alloc] initWithDictionary:arrCollectViewControls[i]];
        if ([tmpDic[@"control_type"] intValue] == key) {
            if(value == nil){
                value = @"";
            }
            [tmpDic setValue:value forKey:@"value"];
            [arrCollectViewControls replaceObjectAtIndex:i withObject: tmpDic];
            return @[[NSNumber numberWithInt:i], arrCollectViewControls[i]];
        }
    }
    return @[[NSNumber numberWithInt:-1], [NSDictionary new]];
}

- (void) findAndReplacePlusCurrency {
    
    if ([currencyList containsObject:@"+"]) {
        
        currencyPlusIndex = -1;
        
        for (int i = 0; i < [currencyList count]; i++) {
            NSString *token = [currencyList objectAtIndex:i];
            if ([token isEqualToString:@"+"]) {
                currencyPlusIndex = i;
                
            }
        }
        if (![currencyList containsObject: stockCurrency] && (stockCurrency!=nil)) {
            [ currencyList replaceObjectAtIndex:currencyPlusIndex withObject: stockCurrency];
            
        }
        else {
            [currencyList removeObjectAtIndex:currencyPlusIndex];
        }
    }
    [selectorPicker reloadComponent:2];
    
    //Check Payment List for Current Selected Currency
    if ([selectorPicker selectedRowInComponent:2] == currencyPlusIndex) {
        for (AdditionalPaymentData *payment in trdRules.paymentPlusList) {
            [paymentList addObject:payment.payment_name];
        }
    }
    else {
        for (AdditionalPaymentData *payment in trdRules.paymentPlusList) {
            [paymentList removeObjectIdenticalTo:payment.payment_name];
        }
    }
    
    if (([stkExchange isEqual:@"HKD"] || [stkExchange isEqual:@"HK"]) && [currencyList containsObject:trdRules.currencyNotSupported])
    {
        [ currencyList removeObjectAtIndex: [currencyList indexOfObject:trdRules.currencyNotSupported ]];
    }
    
}

-(NSDictionary *)fnGetControlObject:(CONTROL_INPUTFIELD_TYPE) type{
    
    //Check Price: Price must be more than zero.
    
    for (NSDictionary * tmpDic in arrCollectViewControls) {
        
        NSLog(@"%@" , tmpDic[@"value"] );
        
        
        if ([tmpDic[@"control_type"] intValue] == type ) {
            return tmpDic;
            break;
        }
    }
    return nil;
}

-(BOOL)checkAccountAndPayment {
    if(lotSizeStringInt < 1){
        lotSizeStringInt = 1;
    }
    BOOL result = YES;
    
    // check payment type and account type matrix
    if ([[UserPrefConstants singleton].brokerCode isEqualToString:@"066"]) {
        
        if ([uacd.account_type isEqualToString:@"B"]) {
            if ([[paymentList objectAtIndex:paymentRow] isEqualToString:@"CUT"]) {
                
            } else {
                
            }
        } else {
            if ([[paymentList objectAtIndex:paymentRow] isEqualToString:@"CUT"]) {
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"InformationPayCUTAccNonCUT"]
                                             inController:self withTitle:@""];
                //                [self clearLoadingView];
                result = NO;
            } else {
                
            }
        }
    }
    
    long qtyFactor = 1;
    
    if ([SettingManager shareInstance].currentQuantityType == 1 && !self.isOrderBook) {
        qtyFactor = [self.stock.lotSize doubleValue];
    } else {
        qtyFactor = 1;
    }
    
    
    NSDictionary*dic = [self fnGetControlObject:CONTROL_PRICE];
    
    double price = [dic[@"value"] doubleValue];
    dic = [self fnGetControlObject:CONTROL_QUANTITY];
    double qty = [dic[@"value"]  longLongValue ] * qtyFactor;
    
    
    // resolved limit is either buy limit or credit limit
    // in default currency
    
    double limitCheck = resolvedLimit;
    double priceCheck = price;
    
    
    
    // get currency rate for settlement currency
    CurrencyRate *cr = [[UserPrefConstants singleton].currencyRateDict objectForKey:myOrderDetails.currency];
    
    // userpref defaultcurrency = broker currency
    // stockCurrency = currency of stock data (price)
    // exchangeCurrency = currency of selected exchange
    // orderDetails.currency = settlement currency (user selected)
    
    //convert limit to sett curr.
    if (![[UserPrefConstants singleton].defaultCurrency isEqualToString: myOrderDetails.currency]) {
        limitCheck = resolvedLimit / cr.buy_rate;
    }
    
    // convert price to sett curr
    if (![stockCurrency isEqualToString:myOrderDetails.currency]) {
        priceCheck =  price / cr.buy_rate;
    }
    
    // NSLog(@"CHECK: %f, %f",priceCheck*qty, limitCheck);
    
    BOOL notEnough = NO;
    
    if (priceCheck*qty>limitCheck) notEnough = YES;
    
    if (resolvedLimit<=0) notEnough = YES;
    
    // only for CIMB SG.
    if (notEnough&&[[UserPrefConstants singleton].brokerCode isEqualToString:@"066"]) {
        
        if ([[paymentList objectAtIndex:paymentRow] isEqualToString:@"CUT"]) {
            // for CUT account
            if ([uacd.account_type isEqualToString:@"B"]) {
                
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"InsufficientCashCUT"]
                                             inController:self withTitle:@""];
                //                [self clearLoadingView];
                result = NO;
                
                
                // non cut account
            } else {
                
                //                [self alertWithMsg:[LanguageManager stringForKey:@"InformationCUT"]
                //                    andButtonTitle:[LanguageManager stringForKey:@"F.A.Q."] andLink:@"https://www.itradecimb.com.sg/app/help.client.services.z?cat=01&subcat=01_03&subsubcat=8a3e938c4761fbe0014766fbe86b017d"];
                //                [self clearLoadingView];
                result = NO;
                
            }
        }
    }
    
    return result;
}

#pragma mark - TExtField delegate
/*
 - (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
 return YES;
 }
 
 - (void)textFieldDidBeginEditing:(UITextField *)textField {
 [atp timeoutActivity];
 
 activeTextField = textField;
 
 //For dismissing KBoard.
 activeTextField.inputAccessoryView = numberToolbar;
 
 if (keyUP == NO) {
 [self shiftView:1 forTextField:textField];
 keyUP = YES;
 }
 UIView * parentV = textField.superview;
 
 constraintQuickButtonsInput.constant = CGRectGetMaxY(parentV.frame) + CGRectGetHeight(parentV.frame);
 
 if (textField == stkPrice || textField == triggerPrice) {
 numberButtonLabel1.text = [NSString stringWithFormat:@"-%@", PriceNo3];
 numberButtonLabel2.text = [NSString stringWithFormat:@"-%@", PriceNo2];
 numberButtonLabel3.text = [NSString stringWithFormat:@"-%@", PriceNo1];
 numberButtonLabel4.text = [NSString stringWithFormat:@"+%@", PriceNo1];
 numberButtonLabel5.text = [NSString stringWithFormat:@"+%@", PriceNo2];
 numberButtonLabel6.text = [NSString stringWithFormat:@"+%@", PriceNo3];
 }
 else if (textField == quantity || textField == minquantity || textField == discloseQty) {
 numberButtonLabel1.text = [NSString stringWithFormat:@"-%@", QtyNo3];
 numberButtonLabel2.text = [NSString stringWithFormat:@"-%@", QtyNo2];
 numberButtonLabel3.text = [NSString stringWithFormat:@"-%@", QtyNo1];
 numberButtonLabel4.text = [NSString stringWithFormat:@"+%@", QtyNo1];
 numberButtonLabel5.text = [NSString stringWithFormat:@"+%@", QtyNo2];
 numberButtonLabel6.text = [NSString stringWithFormat:@"+%@", QtyNo3];
 }
 }
 
 - (BOOL)textFieldShouldReturn:(UITextField *)textField
 {
 [atp timeoutActivity];
 
 if (keyUP) {
 [self shiftView:2 forTextField:textField];
 keyUP = NO;
 }
 
 [textField resignFirstResponder];
 return YES;
 }
 
 
 */
//- (IBAction)priceEditingChanged:(id)sender {
//    [atp timeoutActivity];
//    [self calculatePrice];
//}
//
//- (IBAction)qtyEditingChanged:(id)sender {
//    [atp timeoutActivity];
//    [self calculatePrice];
//}

- (IBAction)minQtyEditingChanged:(id)sender {
}

- (IBAction)discQtyEditingChanged:(id)sender {
}

- (IBAction)triggerPriceEditingChanged:(id)sender {
}

- (IBAction)toggleShortSell:(id)sender {
    [atp timeoutActivity];
    
    UIButton *btn = (UIButton*)sender;
    //    isShortSell = !isShortSell;
    
    //    if (isShortSell) {
    //        [btn setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
    //    } else {
    //        [btn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    //    }
}

//Increment buttons
- (IBAction) numberButtonPressed:(UIButton *) sender {
    /*
     [atp timeoutActivity];
     
     double inc = 0;
     if (activeTextField == stkPrice || activeTextField == triggerPrice) {
     if (sender == numberButton1) {
     inc = [PriceNo3 doubleValue]*-1;
     }
     else if (sender == numberButton2) {
     inc = [PriceNo2 doubleValue]*-1;
     }
     else if (sender == numberButton3) {
     inc = [PriceNo1 doubleValue]*-1;
     }
     else if (sender == numberButton4) {
     inc = [PriceNo1 doubleValue];
     }
     else if (sender == numberButton5) {
     inc = [PriceNo2 doubleValue];
     }
     else if (sender == numberButton6) {
     inc = [PriceNo3 doubleValue];
     }
     }
     else if (activeTextField == quantity || activeTextField == minquantity || activeTextField == discloseQty) {
     if (sender == numberButton1) {
     inc = [QtyNo3 doubleValue]*-1;
     }
     else if (sender == numberButton2) {
     inc = [QtyNo2 doubleValue]*-1;
     }
     else if (sender == numberButton3) {
     inc = [QtyNo1 doubleValue]*-1;
     }
     else if (sender == numberButton4) {
     inc = [QtyNo1 doubleValue];
     }
     else if (sender == numberButton5) {
     inc = [QtyNo2 doubleValue];
     }
     else if (sender == numberButton6) {
     inc = [QtyNo3 doubleValue];
     }
     }
     
     double number = [activeTextField.text doubleValue];
     double number_new = number + inc;
     
     if (number_new >= 0) {
     number = number_new;
     }
     else {
     //        if (allowNegativePrice) {
     //            number = number_new;
     //        }
     }
     
     if (activeTextField == stkPrice || activeTextField == triggerPrice) {
     
     activeTextField.text = [[NumberHandler numberFormatStockPrice:number isPrice:YES isPriceChange:NO forExchange: currentED.trade_exchange_code] stringByReplacingOccurrencesOfString:@"," withString:@""];
     }
     else if (activeTextField == quantity || activeTextField == minquantity || activeTextField == discloseQty) {
     activeTextField.text = [NSString stringWithFormat:@"%lld", (long long)number];
     }
     */
}

- (void) shiftView:(int) direction forTextField:(UITextField *) textField {
    
    [UIView animateWithDuration:0.3 animations:^(void) {
        
        switch (direction) {
            case 1:
                numberButtonIncrementView.hidden = NO;
                numberButtonIncrementView.alpha = 1;
                break;
            case 2:
                numberButtonIncrementView.hidden = YES;
                numberButtonIncrementView.alpha = 0;
                break;
        }
    }];
    
    [self.view setNeedsDisplay];
}

#pragma mark -
#pragma mark Methods to get configuration for change of price and change of stock quantity


//Get number button value from ChgOfPriceNQtyConfig.plist
- (void) getNumberButtonsValue {
    
    //Get ChgOfPriceNQtyConfig.plist dictionary
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *finalPath = [path stringByAppendingPathComponent:@"ChgOfPriceNQtyConfig.plist"];
    NSDictionary *plistDictionary = [NSDictionary dictionaryWithContentsOfFile:finalPath];
    
    //Get Change of Price Numbers Value
    NSDictionary *d = [plistDictionary objectForKey:@"ChgOfPrice"];
    
    NSArray *arr = nil;
    id obj = [d objectForKey: currentED.trade_exchange_code];
    if (obj == nil) {
        obj = [d objectForKey:@"Default"];
    }
    
    if ([obj isKindOfClass:[NSArray class]]) {
        arr = obj;
    }
    
    if ([obj isKindOfClass:[NSDictionary class]]) {
        //        arr = [(NSDictionary *)obj objectForKey:stockData.sector_code];
        //        if (arr == nil) {
        //            arr = [(NSDictionary *)obj objectForKey:@"Default"];
        //        }
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES];
    NSArray *sortedArr = [[NSArray alloc] initWithArray:[arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]]];
    
    PriceNo1 = [sortedArr objectAtIndex:0];
    PriceNo2 = [sortedArr objectAtIndex:1];
    PriceNo3 = [sortedArr objectAtIndex:2];
    
    //Get Change of Quantity Numbers Value
    NSDictionary *d1 = [plistDictionary objectForKey:@"ChgOfQty"];
    NSArray *arr1 = [d1 objectForKey: currentED.trade_exchange_code];
    if (arr1 == nil) {
        arr1 = [d1 objectForKey:@"Default"];
    }
    NSArray *sortedArr1 = [[NSArray alloc] initWithArray:[arr1 sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]]];
    
    QtyNo1 = [sortedArr1 objectAtIndex:0];
    QtyNo2 = [sortedArr1 objectAtIndex:1];
    QtyNo3 = [sortedArr1 objectAtIndex:2];
}


#pragma mark - UTILS VC
//Parse data to model
- (NSArray *)dataModelsFrom:(NSArray *)marketBuys sells:(NSArray *)marketSells{
    if (!marketBuys || !marketSells) {
        return @[];
    }
    NSMutableArray *models = @[].mutableCopy;
    NSInteger count = MAX(marketBuys.count, marketSells.count);
    for (int i = 0; i < count; i ++) {
        MarketDepthModel *model = [MarketDepthModel new];
        NSDictionary *buyDict;
        NSDictionary *sellDict;
        if(i < marketSells.count){
            sellDict = marketSells[i];
        }
        if(i < marketBuys.count){
            buyDict = marketBuys[i];
        }
        //BUY VALUE
        if ([buyDict.allKeys containsObject:FID_170_I_BUY_SPLIT_1]) {
            model.buySplit1 = [buyDict[FID_170_I_BUY_SPLIT_1] stringValue];
        }
        if ([buyDict.allKeys containsObject:FID_58_I_BUY_QTY_1]) {
            model.buyQty = [NSNumber numberWithLongLong:[buyDict[FID_58_I_BUY_QTY_1] longLongValue]];
        }
        if ([buyDict.allKeys containsObject:FID_68_D_BUY_PRICE_1]) {
            model.buyPrice = [NSNumber numberWithFloat:[buyDict[FID_68_D_BUY_PRICE_1] floatValue]];
        }
        //SELL VALUE
        if ([sellDict.allKeys containsObject:FID_170_I_BUY_SPLIT_1]) {
            model.sellSplit1 = [sellDict[FID_170_I_BUY_SPLIT_1] stringValue];
        }
        if ([sellDict.allKeys containsObject:FID_58_I_BUY_QTY_1]) {
            model.sellQty = [NSNumber numberWithLongLong:[sellDict[FID_58_I_BUY_QTY_1] longLongValue]];
        }
        if ([sellDict.allKeys containsObject:FID_68_D_BUY_PRICE_1]) {
            model.sellPrice = [NSNumber numberWithFloat:[sellDict[FID_68_D_BUY_PRICE_1] floatValue]];
        }
        //Store
        [models addObject:model];
    }
    return [models copy];
}

//MARK:- NOTIFICATION
-(void)didFinishedBusinessDone:(NSNotification*)notification {
    
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    
    
    // NSLog(@"BIZDONE: %@", data);
    
    if ([data count]>4)
    {
        _bizDoneTableDataArr = [Utils dataBDoneModelsFrom:[NSArray arrayWithArray:[data subarrayWithRange:NSMakeRange(0, [data count]-4)]]];
        [tblContent reloadData];
    }
}
-(void)didFinishMarketDepth:(NSNotification *)notification
{
    //Remove notification alert
    //    [self removeNotification: kDidFinishMarketDepthNotification];
    
    
    NSDictionary * response = [notification.userInfo copy];
    NSArray *_mktDepthArr = [response objectForKey:@"marketdepthresults"];
    
    NSLog(@"mktDepthArr %@",_mktDepthArr);
    
    NSMutableArray *tmpBuyArr = [[NSMutableArray alloc] init];
    NSMutableArray *tmpSellArr = [[NSMutableArray alloc] init];
    
    
    int MDLfeature = ([mdInfo.isMDL isEqualToString:@"Y"])?1:0;
    
    
    // Non Feature md data is all in single dictionary, so we convert it to same
    // format as featured MD.
    if (MDLfeature==0) {
        
        // if server give any data
        if ([_mktDepthArr count]>0) {
            
            
            // non MDL - all mkt depth data is in one object (max 10 level only)
            NSDictionary *stkDataWithMDDict = [_mktDepthArr objectAtIndex:0];
            
            
            int fid_buy_split_start = 170;
            int fid_sell_split_start = 180;
            int fid_buy_qty_start = 58;
            int fid_sell_qty_start = 78;
            int fid_buy_price_start = 68;
            int fid_sell_price_start = 88;
            
            
            // populate tmpBuyArr (loop 10 and if found, create it and add)
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *buyMD = [[NSMutableDictionary alloc] init];
                
                NSString *buySplitKey = [NSString stringWithFormat:@"%d",fid_buy_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buySplitKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buySplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *buyPriceKey = [NSString stringWithFormat:@"%d",fid_buy_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyPriceKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *buyQtyKey = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyQtyKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[buyMD allKeys] count]>0) {
                    // set FID 107
                    [buyMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [buyMD setObject:[NSNumber numberWithInt:1] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    
                    [tmpBuyArr addObject:buyMD];
                }
            }
            
            // NSLog(@"tmpBuyArr %@", tmpBuyArr);
            
            // populate tmpSellArr
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *sellMD = [[NSMutableDictionary alloc] init];
                
                NSString *sellSplitKey = [NSString stringWithFormat:@"%d",fid_sell_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellSplitKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellSplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *sellPriceKey = [NSString stringWithFormat:@"%d",fid_sell_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellPriceKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *sellQtyKey = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellQtyKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[sellMD allKeys] count]>0) {
                    // set FID 107
                    [sellMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [sellMD setObject:[NSNumber numberWithInt:2] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    [tmpSellArr addObject:sellMD];
                }
            }
            
            
        } else {
            ; // empty data = do nothing (will be handled below)
        }
        
        
    } else if (MDLfeature==1) {
        // MDL type - mkt depth data in separate objects
        
        // if server give any data
        if ([_mktDepthArr count]>0) {
            
            for (int i=0; i<[_mktDepthArr count]; i++) {
                
                NSDictionary *eachMDdata = [_mktDepthArr objectAtIndex:i];
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==1) {
                    // BID
                    [tmpBuyArr addObject:eachMDdata];
                }
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==2) {
                    // SELL
                    [tmpSellArr addObject:eachMDdata];
                }
            }
            
        } else {
            ; // empty data = do nothing  (will be handled below)
        }
        
    }
    
    //NSLog(@"tmpBuycount %d",[tmpBuyArr count]);
    
    // if tmpBuyArr empty, prepare zeroes
    if ([tmpBuyArr count]<=0) {
        int mdLevel = [mdInfo.mdLevel intValue];
        
        // NSLog(@"mdLevel %d",mdLevel);
        
        int alternate = 1;
        int pos = 0;
        for (long i=0; i<mdLevel*2; i++) {
            NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:2],@"105",
                                      [NSNumber numberWithInt:alternate],@"106",
                                      [NSNumber numberWithInt:pos],@"107",
                                      [NSNumber numberWithInt:0],@"118",
                                      @"KL",@"131",
                                      [NSNumber numberWithInt:0],@"170",
                                      @"5517.KL",@"33",
                                      @"0",@"45",
                                      [NSNumber numberWithInt:0],@"58",
                                      [NSNumber numberWithInt:0],@"68",
                                      nil];
            
            if (alternate==1) {
                [tmpBuyArr addObject:zeroDict];
                alternate = 2;
                pos++;
            } else {
                [tmpSellArr addObject:zeroDict];
                alternate = 1;
            }
        }
        
    }
    
    
    // sort data based on FID 107
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_107_I_MARKET_DEPTH_POSITION ascending:YES];
    NSArray *limitedArrayB = [tmpBuyArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSArray *limitedArrayS = [tmpSellArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [tmpBuyArr removeAllObjects];
    [tmpSellArr removeAllObjects];
    
    [arrMarketDepthItems removeAllObjects];
    
    if ([limitedArrayB count]>0) {
        //
        _MDBuyArr = [NSMutableArray arrayWithArray:[limitedArrayB mutableCopy]];
        _MDSellArr = [NSMutableArray arrayWithArray:[limitedArrayS mutableCopy]];
        
        [arrMarketDepthItems addObjectsFromArray: [self dataModelsFrom:_MDBuyArr sells:_MDSellArr] ];
        
        [tblContent reloadData];
    }
}

-(void)updateTradingAccount:(UserAccountClientData*)account {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UserPrefConstants singleton].userSelectedAccount = account;
        self->uacd = account;
        
        
        //        [self updateOrderPad];
        
        self->m_accountName = [NSString stringWithFormat:@"%@ - %@ - %@",
                               self->uacd.client_account_number,
                               self->uacd.client_name,
                               self->uacd.broker_branch_code];
        
        //Update Section account
        NSRange range = NSMakeRange(FIELD_BUYSELL, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self->tableControl reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
        
        
        
        if ([self->uacd.account_type isEqualToString:@"X"]) {
            // autoselect cash if it exist
            if ([self->paymentList containsObject:@"Cash"]) {
                NSUInteger idx = [self->paymentList indexOfObject:@"Cash"];
                NSLog(@"selectRow");
                [self->selectorPicker selectRow:idx inComponent:3 animated:YES];
            }
            
        } else if ([self->uacd.account_type isEqualToString:@"B"]) {
            // autoselect CUT if it exist
            if ([self->paymentList containsObject:@"CUT"]) {
                NSUInteger idx = [self->paymentList indexOfObject:@"CUT"];
                [self->selectorPicker selectRow:idx inComponent:3 animated:YES];
            }
            
        }
        
        [self refreshTrdLmtBtnPressed:nil];
        
    });
    
}

-(void)tradeLimitReceived {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //Reload Section INFO
        
        NSRange range = NSMakeRange(FIELD_INFO, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self->tableControl reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
        
    });
    
}

-(void)validateTrade2FAPinForDeviceDone:(NSNotification*)notification {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (![self checkAccountAndPayment]) return;
        
        NSDictionary *replyDict = notification.userInfo;
        NSString *statusCode = [replyDict objectForKey:@"statusCode"];
        NSString *statusMsg = [replyDict objectForKey:@"statusMsg"];
        NSString *errorMsg = [replyDict objectForKey:@"errorMsg"];
        
        // statuscode 0 is success for CIMB SG 2FA. WTF
        if ([statusCode isEqualToString:@"200"]||[statusCode isEqualToString:@"0"]) {
            
            // success!
            [UserPrefConstants singleton].verified2FA = YES;
            [UserPrefConstants singleton].orderDetails.otpPin = [UserPrefConstants singleton].dvData2FA.otpPin;
            [UserPrefConstants singleton].orderDetails.deviceID = [UserPrefConstants singleton].dvData2FA.deviceID;
            
            //            if (self->isSkipConfirmation) {
            [self doTrade];
            //            } else {
            //                [self showConfirmation];
            //            }
            
        } else {
            
            //            [_traderWorking stopAnimating];
            //            [_okBtn setHidden:NO];
            //            if ([errorMsg length]<=0) errorMsg = statusMsg;
            //            _errorMsgLabel.text = [NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg];
            
        }
        
    });
}

-(void)atpValidate1FADone:(NSNotification*)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[AppState sharedInstance] removeLoadingView:self.view];
        if (![self checkAccountAndPayment]) return;
        
        NSDictionary *replyDict = notification.userInfo;
        NSString *statusCode = [replyDict objectForKey:@"statusCode"];
        NSString *statusMsg = [replyDict objectForKey:@"statusMsg"];
        NSString *errorMsg = [replyDict objectForKey:@"errorMsg"];
        
        if (![statusCode isEqualToString:@"200"]) {
            
            //            [_traderWorking stopAnimating];
            //            [_okBtn setHidden:NO];
            if ([errorMsg length]<=0) errorMsg = statusMsg;
            //            _errorMsgLabel.text = [NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg];
            [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg]];
            
        } else {
            
            //            if (self->isSkipConfirmation) {
            [self doTrade];
            //            } else {
            //                [self showConfirmation];
            //            }
            
        }
        
    });
    
}

- (IBAction)cancelTrade:(id)sender {
    [atp timeoutActivity];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        //        [_padView setCenter:CGPointMake(self.view.frame.size.width*2, _padView.center.y)];
        
    } completion:^(BOOL finished) {
        //        [self.delegate traderDidCancelled];
        
    }];
}
- (void)setupStatusVC{
    if(!tsVC){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TradeStoryboard" bundle:nil];
        tsVC = [storyboard instantiateViewControllerWithIdentifier:@"MTradeStatusViewController"];
        tsVC.delegate =self;
    }
}
- (void)setupValidVC{
    if(!validVC){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TradeStoryboard" bundle:nil];
        validVC = [storyboard instantiateViewControllerWithIdentifier:@"MValidatePINViewController"];
        validVC.modalPresentationStyle= UIModalPresentationFormSheet;
        validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
        validVC.preferredContentSize =CGSizeMake(320, 330);
        validVC.delegate = self;
        validVC.validatingType = VAL_PIN;
    }
}
-(void)TradeSubmitStatus:(NSNotification *)notification
{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    
    
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSDictionary * response = [notification.userInfo copy];
    
    //            [_boxView setHidden:YES];
    // show window trade done and option to go orderbook.
    [self setupStatusVC];
    
    if ([[response objectForKey:@"tradesubmitstatus"] isEqual:@"Failed"]) {
        NSString *errorMsg =  [response objectForKey:@"error"];
        
        //NSLog(@"errorMsg %@",errorMsg);
        tsVC.messageStr = errorMsg;
        tsVC.statusStr = [LanguageManager stringForKey:@"Order Rejected"];;
        
        BOOL errorPin = !([[errorMsg lowercaseString] rangeOfString:@"pin"].location == NSNotFound);
        
        if (errorPin) {
            // clear pin
            //NSLog(@"Pin cleared");
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"TradingPin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
    } else {
        tsVC.statusStr = [LanguageManager stringForKey:@"Order Sent"];
        response = [response objectForKey:@"data"];
        NSDate *date = [NSDate date];
        [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
        
        
        
        tsVC.messageStr =   [LanguageManager stringForKey:@"TradeTicketReceipt"
                                         withPlaceholders:@{@"%1%":[response objectForKey:@"+"],
                                                            @"%2%":[response objectForKey:@"5"],
                                                            @"%3%":[response objectForKey:@"4"],
                                                            @"%4%":[response objectForKey:@";"],
                                                            @"%5%":[response objectForKey:@"6"],
                                                            @"%6%":[response objectForKey:@"<"],
                                                            @"%7%":[response objectForKey:@"9"],
                                                            @"%8%":[dateFormatter stringFromDate:date]}];
        
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [[AppState sharedInstance] forceRemoveLoadingView:self.view];
        [self enableActionButtons:YES];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"didSubmitTicket"
         object:self userInfo:nil];
        self->tsVC.arrMarketDepthItems = self->arrMarketDepthItems;
        self->tsVC.timeSaleArr = self->_timeSaleArr;
        self->tsVC.bizDoneTableDataArr = self->_bizDoneTableDataArr;
        self->tsVC.stockCode = self.stock.stockCode;
        self->tsVC.stock = self.stock;
        self.contentMode = TradeContentModeStatus;
        //        [self nextTo:self->tsVC animate:YES];
        
    });
    
}

-(void)tradeReviseNotification:(NSNotification *)notification
{
    // show window trade done and option to go orderbook.
    [self setupStatusVC];
    
    NSDictionary *responseDict = notification.userInfo;
    tsVC.messageStr = [responseDict objectForKey:@"msg"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showPopupVC:self->tsVC];
    });
    
}


-(void)tradeCancelNotification:(NSNotification *)notification
{
    // show window trade done and option to go orderbook.
    [self setupStatusVC];
    
    NSDictionary *responseDict = notification.userInfo;
    tsVC.messageStr = [responseDict objectForKey:@"msg"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showPopupVC:self->tsVC];
    });
    
}

//??
#pragma mark - MD Price Clicked Notification

-(void)mdPriceTappedNotification:(NSNotification*)notification {
    
    //    NSDictionary *dict = notification.userInfo;
    //    NSString *priceStr = [dict objectForKey:@"Price"];
    //
    //    if ([priceStr floatValue]<=0) {
    //        return;
    //    }
    //
    //    CGFloat priceFloat = [priceStr floatValue];
    //    if ([UserPrefConstants singleton].pointerDecimal==3) {
    //        stkPrice.text = [NSString stringWithFormat:@"%.3f",priceFloat];
    //    }else{
    //        stkPrice.text = [NSString stringWithFormat:@"%.4f",priceFloat];
    //    }
    
}


- (NSArray *)getSubmitColor:(TradeType)type{
    switch (type) {
        case TradeType_Cancel:
            return @[TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_BuyText), TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_SpecialButtonBg)];
        case TradeType_Revise:
            return @[TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_BuyText), TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonBg)];
        case TradeType_Buy:
            return @[TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_BuyText), TC_COLOR_FROM_HEX([SettingManager shareInstance].buyColor)];
        default:
            return @[TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_SellText), TC_COLOR_FROM_HEX([SettingManager shareInstance].sellColor)];
    }
}

- (NSArray *)getCurrentSubmitColor{
    return [self getSubmitColor:self.tradeType];
}

- (NSString *)getSubmitTitle:(TradeType)type{
    if(type == TradeType_Buy){
        return @"Buy";
    }
    return @"Sell";
}
- (NSArray *)getSubmitTitle:(TradeUIType)uiType tradeType:(TradeType)type{
    if (uiType == TradeUIType1) {
        return @[@"Next"];
        //        switch (type) {
        //            case TradeType_Cancel:
        //                return @[@"CANCEL"];
        //            case TradeType_Revise:
        //                return @[@"REVISE"];
        //            case TradeType_Buy:
        //                return @[@"Buy"];
        //            default:
        //                return @[@"Sell"];
        //        }
    }else{
        return @[@"Buy", @"Sell"];
    }
}
- (void)setupBottomActionsWith:(TradeUIType)uiType tradeType:(TradeType)type{
    if (uiType == TradeUIType1) {
        [self.st_bottomUI2 setHidden:YES];
        [self.btn_submit setHidden:NO];
        [self changeSubmitButtonColor:uiType tradeType:type];
    }else{
        [self.st_bottomUI2 setHidden:NO];
        [self.btn_submit setHidden:YES];
        [self changeSubmitButton2:uiType tradeType:type];
    }
}
- (void)changeSubmitButton2:(TradeUIType)uiType tradeType:(TradeType)type{
    NSArray *titles = [self getSubmitTitle:uiType tradeType:type];
    [self.btn_buy setTitle:titles[0] forState:UIControlStateNormal];
    [self.btn_sell setTitle:titles[1] forState:UIControlStateNormal];
    if(type == TradeType_Revise){
        [self.st_bottomUI2 setHidden:YES];
        [self.btn_submit setHidden:NO];
        NSArray *reviseTitle = [self getSubmitTitle:uiType tradeType:type];
        NSArray *currentColors = [self getSubmitColor:type];
        [self.btn_submit setTitleColor:reviseTitle[0] forState:UIControlStateNormal];
        [self.btn_buy setTitleColor:currentColors[0] forState:UIControlStateNormal];
        [self.btn_buy setBackgroundColor:currentColors[1]];
    }else{
        NSArray *currentColors = [self getSubmitColor:TradeType_Buy];
        [self.btn_buy setTitleColor:currentColors[0] forState:UIControlStateNormal];
        [self.btn_buy setBackgroundColor:currentColors[1]];
        
        currentColors = [self getSubmitColor:TradeType_Sell];
        [self.btn_sell setTitleColor:currentColors[0] forState:UIControlStateNormal];
        [self.btn_sell setBackgroundColor:currentColors[1]];
    }
    
    
    
}
- (void)changeSubmitButtonColor:(TradeUIType)uiType tradeType:(TradeType)type{
    NSArray *currentColors = [self getCurrentSubmitColor];
    NSArray *titles = [self getSubmitTitle:uiType tradeType:type];
    [self.btn_submit setTitle:titles[0] forState:UIControlStateNormal];
    [self.btn_submit setTitleColor:currentColors[0] forState:UIControlStateNormal];
    [self.btn_submit setBackgroundColor:currentColors[1]];
}
#pragma mark - SUBMIT TRADING

- (IBAction)buyAction:(id)sender {
    self.tradeType = TradeType_Buy;
//    [self changeBuySellType:TradeType_Buy];
    [self fnSubmit:sender];
}
- (IBAction)sellAction:(id)sender {
    self.tradeType = TradeType_Sell;
//    [self changeBuySellType:TradeType_Sell];
    [self fnSubmit:sender];
}
- (void)enableActionButtons:(BOOL)isEnable{
    [self.btn_submit setEnabled:isEnable];
    [self.btn_buy setEnabled:isEnable];
    [self.btn_sell setEnabled:isEnable];
}
- (IBAction)fnSubmit:(UIButton*)sender {
    [atp timeoutActivity];
    if(lotSizeStringInt < 1){
        lotSizeStringInt = 1;
    }
    //    [self.loadingView setHidden:NO];
    //    [_traderWorking startAnimating];
    //    [_errorMsgLabel setText:[LanguageManager stringForKey:@"Working..."]];
    //    [_okBtn setHidden:YES];
    
    NSString *currExch = [[UserPrefConstants singleton] stripDelayedSymbol:[UserPrefConstants singleton].userCurrentExchange];
    
    // check if current exchange is supported to trade
    NSArray *supportedExch = [NSArray arrayWithArray:[UserPrefConstants singleton].userSelectedAccount.supported_exchanges];
    
    if ([supportedExch count]>0) {
        __block BOOL canTrade = NO;
        __block NSString *suppExchgStr = @"";
        [supportedExch enumerateObjectsUsingBlock:^(id x, NSUInteger idx, BOOL *stop) {
            NSString *supported = (NSString*)x; //NSLog(@"each Supported: %@",supported);
            suppExchgStr = [NSString stringWithFormat:@"%@ %@",suppExchgStr,supported];
            if ([supported isEqualToString:currExch]) canTrade=YES;
            
            //=================== ??
            //NOTE: temporary assume SG equals to SI
            if (([supported isEqualToString:@"SG"]&&[currExch isEqualToString:@"SI"])||
                ([supported isEqualToString:@"SI"]&&[currExch isEqualToString:@"SG"])) canTrade = YES;
        }];
        
        if (!canTrade) {
            
            [self showErrorNotification: [LanguageManager stringForKey:@"This account can only trade in Exchanges %@" withPlaceholders:@{@"%exchList%":suppExchgStr}] inViewController:self];
            
            return;
        }
        
    } else {
        [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"No supported exchanges available for this account to trade."]
                                     inController:self withTitle:@""];
        return;
    }
    
    [myOrderDetails reset] ;
    [myTradeStatus reset] ;
    
    if (self.tradeType == TradeType_Buy) {
        myOrderDetails.order_action = @"Buy";
        myTradeStatus.action = @"Buy";
    }else if (self.tradeType == TradeType_Sell){
        myOrderDetails.order_action = @"Sell";
        myTradeStatus.action = @"Sell";
    }
    else if (self.tradeType == TradeType_Cancel){
        myOrderDetails.order_action = @"Cancel";
        myTradeStatus.action = @"Cancel";
    }
    else if (self.tradeType == TradeType_Revise){
        myOrderDetails.order_action = @"Revise";
        myTradeStatus.action = @"Revise";
    }
    
    
    myOrderDetails.ticker = _stock.stockName;
    myOrderDetails.stock_code = _stock.stockCode;
    myOrderDetails.company_name = _stock.compName;
    
    
    long qtyFactor = 0;
    
    if ([SettingManager shareInstance].currentQuantityType == 1 && !self.isOrderBook) {
        qtyFactor = [self.stock.lotSize doubleValue];
    }
    
    if(qtyFactor <= 0){
        qtyFactor = 1;
    }
    //Check Price: Price must be more than zero.
    
    for (NSDictionary * tmpDic in arrCollectViewControls) {
        
        NSLog(@"%@" , tmpDic[@"value"] );
        
        
        switch ([tmpDic[@"control_type"] intValue]) {
                
            case CONTROL_MINQUANTITY:
            {
                if ([tmpDic[@"value"] intValue] <= 0 )
                {
                    // if not entered, then minQty no need to add
                }
                else
                    myOrderDetails.minQuantity = [NSString stringWithFormat:@"%lld", [tmpDic[@"value"]  longLongValue] * qtyFactor];
            }
                break;
                
            case CONTROL_PAYMENTTYPE:
            {
                if (tmpDic[@"value"] )
                    myOrderDetails.payment_type =tmpDic[@"value"];
                else
                    myOrderDetails.payment_type =@"";
            }
                break;
                
            case CONTROL_ORDERTYPE:
            {
                myOrderDetails.order_type = tmpDic[@"value"];
            }
                break;
                
            case CONTROL_PRICE:
            {
                if ( [tmpDic[@"value"] doubleValue] <= 0 ) {
                    [self showErrorNotification: [LanguageManager stringForKey:@"Price must be more than zero."] inViewController:self];
                    return;
                }
                
                if ([UserPrefConstants singleton].pointerDecimal==3) {
                    myOrderDetails.price = [NSString stringWithFormat:@"%.3f", [tmpDic[@"value"] doubleValue]];
                }else{
                    myOrderDetails.price = [NSString stringWithFormat:@"%.4f", [tmpDic[@"value"] doubleValue]];
                }
                
            }
                break;
                
            case CONTROL_VALIDITY:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                
                if ( [strTmp containsString:@"/" ])
                {
                    myOrderDetails.validity =  @"GTD" ;
                    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
                    [ndf setDateFormat:@"dd/MM/yyyy"];
                    
                    NSDate *date = [ndf dateFromString: strTmp];
                    [ndf setDateFormat:@"yyyyMMdd"];
                    
                    NSString *todayStr = [ndf stringFromDate:[NSDate date]];
                    NSDate *todayDate = [ndf dateFromString:todayStr];
                    
                    // NSLog(@"DATES: %@ %@", date, todayDate);
                    
                    // validate date if lesser than today
                    if( [todayDate timeIntervalSinceDate:date] > 0 ) {
                        [self showErrorNotification: [LanguageManager stringForKey:@"Date must be today or later."] inViewController:self];
                        return;
                    }
                    
                    myOrderDetails.expiry = [ndf stringFromDate:date];
                    
                }else{
                    myOrderDetails.validity = strTmp;
                }
                
                
                
            }
                break;
                
                
            case CONTROL_QUANTITY:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                
                if ( [strTmp doubleValue] * qtyFactor <= 0 ) {
                    
                    [self showErrorNotification: [LanguageManager stringForKey:@"Quantity must be more than zero."] inViewController:self];
                    return;
                }
                myOrderDetails.quantity = [NSString stringWithFormat:@"%.0f", [strTmp doubleValue] * qtyFactor];
            }
                break;
                
            case CONTROL_STATEMENTCURRENCY:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                
                if ( strTmp) {
                    myOrderDetails.currency = strTmp;
                    
                }else{
                    myOrderDetails.currency = exchangeCurrency;
                }
            }
                break;
                
            case CONTROL_TRIGGERPRICE:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                
                if ( strTmp.length <= 0) {
                    // if not entered, then  no need to add
                    
                }else{
                    if ([UserPrefConstants singleton].pointerDecimal==3) {
                        myOrderDetails.trigger_price = [NSString stringWithFormat:@"%.3f", [strTmp doubleValue]];
                    }else{
                        myOrderDetails.trigger_price = [NSString stringWithFormat:@"%.4f", [strTmp doubleValue]];
                    }
                }
                
                
                if (myOrderDetails.trigger_price.floatValue <= 0) {
                    [self showErrorNotification: [LanguageManager stringForKey:@"Trigger Price cannot be less than or equal to 0."] inViewController:self];
                    return;
                }
            }
                break;
                
                
            case CONTROL_TRIGGER:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                myOrderDetails.triggerType = strTmp;
                
            }
                break;
                
            case CONTROL_DIRECTION:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                myOrderDetails.triggerDirection = strTmp;
                
            }
                break;
                
            case CONTROL_DISCLOSED:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                myOrderDetails.disclosedQuantity = [NSString stringWithFormat:@"%lld", [strTmp longLongValue] * qtyFactor];
            }
                break;
                
            case CONTROL_SHORTSELL:
            {
                NSString *strTmp = tmpDic[@"value"] ;
                myOrderDetails.shortSellState = [strTmp boolValue];
            }
                break;
                
            default:
                break;
        }
    }
    
    [UserPrefConstants singleton].userTradeConfirmData =[UserPrefConstants singleton].userSelectedAccount;
    [UserPrefConstants singleton].orderDetails = myOrderDetails;
    
    [self do1FA2FARequest];
    [self enableActionButtons:NO];
}


- (void) do1FA2FARequest {
    
    if ([[UserPrefConstants singleton].byPass2FA rangeOfString:@"ALL" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        //1FA
        [self show1FAAlert:YES];
    }
    else {
        if ([UserPrefConstants singleton].is2FARequired)
        {
            if (![UserPrefConstants singleton].verified2FA)
            {
                NSLog(@"deviceListForSMSOTP %lu",(unsigned long)[[UserPrefConstants singleton].deviceListForSMSOTP count]);
                
                if ([[UserPrefConstants singleton].deviceListFor2FA count] != 0 && [[UserPrefConstants singleton].deviceListForSMSOTP count] !=0)
                {
                    //2FA
                    [self showBothOTPAlert];
                }
                
                else if ([[UserPrefConstants singleton].deviceListFor2FA count] != 0) {
                    //2FA
                    [self show2FAAlert];
                }
                else if([[UserPrefConstants singleton].deviceListForSMSOTP count] !=0) {
                    
                    [self showSMSOTPAlert];
                }
                else
                {
                    if ([[UserPrefConstants singleton].byPass2FA rangeOfString:@"Y" options:NSCaseInsensitiveSearch].location != NSNotFound)
                    {
                        //1FA
                        
                        [self show1FAAlert:YES];
                        
                    }
                    else {
                        //Error Message
                        [self showErrorWithAlert:[LanguageManager stringForKey:@"CIMBOneTokenMessage"]];
                        return;
                    }
                }
            }
            else
            {
                
                [UserPrefConstants singleton].orderDetails.otpPin = [UserPrefConstants singleton].dvData2FA.otpPin;
                [UserPrefConstants singleton].orderDetails.deviceID = [UserPrefConstants singleton].dvData2FA.deviceID;
                
                //                if (isSkipConfirmation) {
                [self doTrade];
                //                } else {
                //                    [self showConfirmation];
                //                }
            }
        }
        else {
            [self doTradingPinRequest];
        }
    }
    
}



-(void)showConfirmation {
    
    //    [_boxView setHidden:YES];
    if(!skipVC){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TradeStoryboard" bundle:nil];
        skipVC = [storyboard instantiateViewControllerWithIdentifier:@"MSkipConfirmationViewController"];
        skipVC.skipDelegate =self;
    }
    skipVC.bizDoneTableDataArr = self.bizDoneTableDataArr;
    skipVC.timeSaleArr = _timeSaleArr;
    skipVC.arrMarketDepthItems = arrMarketDepthItems;
    skipVC.stockCode = _stock.stockCode;
    [skipVC.confirmTable reloadData];
    [skipVC.submitBtn setBackgroundColor:self.btn_submit.backgroundColor];
    //    [self presentViewController:skipVC animated:YES completion:nil];
    //    [self showPopupVC:skipVC];
    self.contentMode = TradeContentModeConfirm;
}


- (void)show1FAAlert:(BOOL)promptNotice {
    
    //    [_boxView setHidden:YES];
    
    if (promptNotice) {
        //        [self showCustomAlert:[LanguageManager stringForKey:@"Important Notice"] message:[LanguageManager stringForKey:@"RegisterOneKeyFor2FAMsg"] okTitle:[LanguageManager stringForKey:@"Register Now"] withOKAction:^{
        //
        //        } cancelTitle:[LanguageManager stringForKey:@"Later"] cancelAction:^{
        //
        //        }];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Important Notice"]
                                                                       message:[LanguageManager stringForKey:@"RegisterOneKeyFor2FAMsg"] preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *btnRegister = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Register Now"]
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *action) {
                                                                //                                                                [_boxView setHidden:NO];
                                                                
                                                                if ([[UserPrefConstants singleton].Register2FAPgURL length]<=0) {
                                                                    // URL not setup in PLIST
                                                                } else {
                                                                    //                                                                    [self clearLoadingView];
                                                                    NSURL *url = [NSURL URLWithString:[[UserPrefConstants singleton].Register2FAPgURL stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
                                                                    [[UIApplication sharedApplication] openURL:url];
                                                                }
                                                            }];
        
        UIAlertAction *btnLater = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Later"]
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             [self show1FAAlert:NO];
                                                         }];
        
        
        [alert addAction:btnRegister]; [alert addAction:btnLater];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    [self setupValidVC];
    validVC.validatingType = VAL_1FA;
    //    [self presentViewController:validVC animated:YES completion:nil];
    [self showConfirmation];
    
}

#pragma mark - Validate1FA2FAandPIN prompt delegate
- (void)isDisableRequestSMSTOPBtn:(BOOL)isDisable{
    CGFloat maxSize = 260;
    skipVC.cst_PinHeight.constant = maxSize - [validVC redundentHeight];
    [skipVC.confirmTable reloadData];
}
-(void)validatePINcancelClicked {
    //    [_boxView setHidden:NO];
    //    [self clearLoadingView];
}


-(void)validatePINokBtnClickedWithPassword:(NSString*)passwd  andType:(ValType)type {
    password = passwd;
    currentValType = type;
    [self checkOPT:passwd andType:type];
}
- (void)checkOPT:(NSString*)passwd  andType:(ValType)type{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //        [_boxView setHidden:NO];
        
        switch (type) {
            case VAL_1FA:{
                [self->atp doValidate1FAPassword:passwd];
                self->_messString = [LanguageManager stringForKey:@"Validating Password..."];
                [[AppState sharedInstance] addLoadingView:self.view];
                //                [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating Password..."]];
                //                [_loadingView setHidden:NO];
                //                [_traderWorking startAnimating];
                //                [_okBtn setHidden:YES];
            } break;
            case VAL_2FA:{
                self->_messString = [LanguageManager stringForKey:@"Validating OTP Pin..."];
                [UserPrefConstants singleton].dvData2FA.otpPin = passwd;
                [[AppState sharedInstance] addLoadingView:self.view];
                [self->atp validate2FAPinForDevice:[UserPrefConstants singleton].dvData2FA forTrade:YES];
                //                [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                //                [_loadingView setHidden:NO];
                //                [_traderWorking startAnimating];
                //                [_okBtn setHidden:YES];
            } break;
            case VAL_PIN:{
                [UserPrefConstants singleton].orderDetails.pin = passwd;
                //                if (self->isSkipConfirmation) {
                [self doTrade];
                //                } else {
                //                    [self showConfirmation];
                //                }
                
            } break;
            case VAL_SMS:
            {
                [UserPrefConstants singleton].orderDetails.pin = passwd;
                [self->atp doValidateSMSOTP:passwd andTrade:YES];
                [[AppState sharedInstance] addLoadingView:self.view];
                self->_messString = [LanguageManager stringForKey:@"Validating OTP Pin..."];
                //                [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                //                [_loadingView setHidden:NO];
                //                [_traderWorking startAnimating];
                //                [_okBtn setHidden:YES];
            }
                break;
                
            case  VAL_SMS_PIN:
                
                if ([UserPrefConstants singleton].currentOTPView==0) {
                    [UserPrefConstants singleton].dvData2FA.otpPin = passwd;
                    [[AppState sharedInstance] addLoadingView:self.view];
                    [self->atp validate2FAPinForDevice:[UserPrefConstants singleton].dvData2FA forTrade:YES];
                    self->_messString = [LanguageManager stringForKey:@"Validating OTP Pin..."];
                    //                    [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                    //                    [_loadingView setHidden:NO];
                    //                    [_traderWorking startAnimating];
                    //                    [_okBtn setHidden:YES];
                }else{
                    [UserPrefConstants singleton].orderDetails.pin = passwd;
                    [self->atp doValidateSMSOTP:passwd andTrade:YES];
                    [[AppState sharedInstance] addLoadingView:self.view];
                    self->_messString = [LanguageManager stringForKey:@"Validating OTP Pin..."];
                    //                    [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                    //                    [_loadingView setHidden:NO];
                    //                    [_traderWorking startAnimating];
                    //                    [_okBtn setHidden:YES];
                }
                
                break;
            default:
                break;
        }
        
    });
}
- (void) show2FAAlert {
    
    //    [_boxView setHidden:YES];
    
    [self setupValidVC];
    validVC.validatingType = VAL_2FA;
    //    [self presentViewController:validVC animated:YES completion:nil];
    [self showConfirmation];
}

- (void) showSMSOTPAlert {
    [self setupValidVC];
    validVC.validatingType = VAL_SMS;
    [self showConfirmation];
}

- (void) showBothOTPAlert {
    [self setupValidVC];
    validVC.validatingType = VAL_SMS_PIN;
    [self showConfirmation];
}

- (void) doTradingPinRequest {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    
    if (![UserPrefConstants singleton].skipPin) {
        if (([def objectForKey:@"TradingPin"] == NO)||([def objectForKey:@"TradingPin"]==nil)) {
            //            if(password == nil || [password isEqualToString:@""]){
            //                [self showErrorNotification: [LanguageManager stringForKey:@"Please enter your pincode"] inViewController:self];
            //            }else{
            //                [self checkOPT:password andType:currentValType];
            //            }
            
            [self showTradingPinAlert];
        }
        else {
            [UserPrefConstants singleton].orderDetails.pin = [def objectForKey:@"TradingPin"];
            if (isSkipConfirmation) {
                [self doTrade];
            } else {
                [self showConfirmation];
            }
        }
    }
    else {
        if (isSkipConfirmation) {
            [self doTrade];
        } else {
            [self showConfirmation];
        }
    }
}

- (void) showTradingPinAlert {
    if(!validVC){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TradeStoryboard" bundle:nil];
        validVC = [storyboard instantiateViewControllerWithIdentifier:@"MValidatePINViewController"];
        validVC.modalPresentationStyle= UIModalPresentationFormSheet;
        validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
        validVC.preferredContentSize =CGSizeMake(320, 330);
        validVC.delegate = self;
        validVC.validatingType = VAL_PIN;
    }
    [self showConfirmation];
    //    [self showPopupVC:validVC];
    //        [self presentViewController:validVC animated:YES completion:nil];
    
}


#pragma mark - TradeStatusVC Delegates
-(void)closeBtnDidClicked {
    self.contentMode = TradeContentModeTrade;
    //    [_boxView setHidden:NO];
    //
    //    [_traderWorking stopAnimating];
    //    [_loadingView setHidden:YES];
    
    //*At Stock Detail page, when user click trade (success or failed) should open back those Orderpad.
    //    [self.delegate traderDidCancelled];
}

-(void)closeAndOpenOrdBookDidClicked {
    [self backVCToRoot:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotiOrderBook object:nil];
    }];
}
-(void)closeAndOpenPortfolioDidClicked{
    [self backVCToRoot:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotiPortFolio object:nil];
    }];
}
-(void)closeAndOpenQuoteDidClicked{
    [self backVCToRoot:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotiQuote object:nil];
    }];
}

-(void)doTrade
{
    [[AppState sharedInstance] addLoadingView:self.view];
    [atp timeoutActivity];
    
    //    [_boxView setHidden:NO];
    //    _errorMsgLabel.text =  [LanguageManager stringForKey:@"Submitting Order..."];
    //    [_loadingView setHidden:NO];
    //    [_traderWorking startAnimating];
    //    [_okBtn setHidden:YES];
    
    
    // NSLog(@"DO TRADE CALLED");
    
    [atp prepareTradeSubmit:NO forAccount:[UserPrefConstants singleton].userTradeConfirmData
                   forOrder:[UserPrefConstants singleton].orderDetails];
}

#pragma mark - Skip Dialog Delegates
- (void)subConfirmViewBack{
    self.contentMode = TradeContentModeTrade;
}
- (void)backFromSkip{
    [self enableActionButtons:YES];
}
-(void)skipDialogDidSubmit:(NSString *)currentAction {
    [[AppState sharedInstance] addLoadingView:self.view];
    [self->validVC actionAfterClosing];
    //    [self doTrade];
    
}
- (void)needToReloadSkipVCData{
    NSArray *currentColors = [self getCurrentSubmitColor];
    [skipVC.submitBtn setTitleColor:currentColors[0] forState:UIControlStateNormal];
    [skipVC.submitBtn setBackgroundColor:currentColors[1]];
    if(self.uiType == TradeUIType2){
        [skipVC.submitBtn setTitle:[self getSubmitTitle:self.tradeType] forState:UIControlStateNormal];
    }
    self->skipVC.changeValue.text = [self->_stock.lastDonePrice toCurrencyNumber];
    self->skipVC.changePercent.text = [self->_stock changePercentValue];
    self->skipVC.changeValueContent = [self->_stock.fChange doubleValue];
    [self->skipVC loadData:[UserPrefConstants singleton].userTradeConfirmData.client_account_number
              orderdetails: self->myOrderDetails tradeStat: self->myTradeStatus fullMode: NO
         withStockCurrency:self->stockCurrency];
    [validVC checkRememberPinState];
    [skipVC addVC:validVC toView:skipVC.pinView];
}
-(void)clearLoadingView {
    
    //    [_loadingView setHidden:YES];
    //    [_traderWorking stopAnimating];
    //    [_cancelBtn setHidden:YES];
    //    [_okBtn setHidden:YES];
}

-(void)skipDialogDidCancel {
    [self clearLoadingView];
    //    [_boxView setHidden:NO];
}
- (void)presentSearchVC{
    isShowingSearchVC = !isShowingSearchVC;
    if(isShowingSearchVC){
        if(searchVC == nil){
            searchVC = NEW_VC_FROM_STORYBOARD(kMSearchStoryboardName, [MSearchVC storyboardID]);
            searchVC.searchType = MSearchType_Selected;
            searchVC.delegate = self;
        }
        [self addVC:searchVC toView:self.view animation:YES];
    }else{
        [searchVC.view removeFromSuperview];
    }
    
}
#pragma mark - MSearchVCDelegate
- (void)didSelectStock:(StockModel *)model{
    self.stock = model;
    [self fnUpdateDataViaNewStock];
    [searchVC.view removeFromSuperview];
    [tableControl reloadData];
}
#pragma mark -  Segue Account - Search Stock
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    
    if ([segueName isEqualToString:@"searchStockSegueID"] && _stock == nil) {
        
    }
}



- (NSMutableArray *)listHeaders{
    NSMutableArray *headers = [NSMutableArray new];
    if (![stkExchange isEqual:@"MY"]) {
        if ([self checkIsSelectedTrdCut]) {
            [headers addObject:[NSNumber numberWithInt:AccountSubInfo_InvPowerLimit]];
        }
        else {
            if ([[[UserPrefConstants singleton].clientLimitOptionDict objectForKey:stkExchange] intValue] == 3) {
                if (uacd.isMarginAccount == 0) {
                    if(self.tradeType == TradeType_Sell){
                        [headers addObject:[NSNumber numberWithInt:AccountSubInfo_SellLimit]];
                    }else{
                        [headers addObject:[NSNumber numberWithInt:AccountSubInfo_BuyLimit]];
                    }
                }
                else {
                    [headers addObject:[NSNumber numberWithInt:AccountSubInfo_TradingLimit]];
                }
            }
        }
        
        if ([uacd.client_acc_exchange isEqual:@"MY"] || [uacd.account_type isEqual:@"D"]) {
            return nil;
        }
    }
    if(headers.count == 0){
        [headers addObject:[NSNumber numberWithInt:AccountSubInfo_TradingLimit]];
    }
    return headers;
}
-(void)calculatePrice
{
    // * Get value from StkPrice input.
    NSDictionary *dicObj = [self getObjControl: CONTROL_STATEMENTCURRENCY] ;
    if(lotSizeStringInt < 1){
        lotSizeStringInt = 1;
    }
    NSString *settcurrency;
    
    if (dicObj[@"value"] ) {
        settcurrency = dicObj[@"value"];
    }
    
    dicObj = [self getObjControl: CONTROL_PRICE];
    
    
    double stkPrice = [dicObj[@"value"] doubleValue];
    double bidPrice = [[[[[QCData singleton] qcFeedDataDict] objectForKey:self.stock.stockCode] objectForKey:FID_68_D_BUY_PRICE_1] doubleValue];
    double askPrice = [[[[[QCData singleton] qcFeedDataDict] objectForKey:self.stock.stockCode] objectForKey:FID_88_D_SELL_PRICE_1] doubleValue];
    
    if (stkPrice<=0) {
        if (self.tradeType == TradeType_Buy) {
            stkPrice = bidPrice;// bid or ask price
        } else {
            stkPrice = askPrice;// bid or ask price
        }
    }
    
    //get quantity value
    dicObj = [self getObjControl: CONTROL_QUANTITY] ;
    
    double quantityNum = [dicObj[@"value"] doubleValue] ;
    
    
    NSNumber *value = [TC_Calculation getPriceWithStockLotSize:lotSizeStringInt settcurrency:settcurrency stkPrice:stkPrice stockCurrency:stockCurrency quantity:quantityNum];
    
    NSDictionary *grossDict;
    NSInteger grossIndex = CONTROL_GROSS;
    NSArray *result = [self updateArrayControl:CONTROL_GROSS value:[NSString stringWithFormat:@"%@ %@", [self getStockCurrentcy], [value toCurrencyNumber]]];
    if([[result firstObject] intValue] != -1){
        grossIndex = [result[0] intValue];
        grossDict = result[1];
        
    }
    
    if (tableControl.numberOfSections > 0) {
        NSIndexPath *index = [NSIndexPath indexPathForRow:grossIndex inSection:0];
        
        CollectionCell_Label *cell = [myCollectionView cellForItemAtIndexPath:index];
        if(cell != nil && [cell isKindOfClass:[CollectionCell_Label class]]){
            [self setDataForCell:cell type:CONTROL_GROSS dicControl:grossDict];
        }
    }
}

- (NSString *)getStockCurrentcy{
    NSString *tmpStockCurrentcy = stockCurrency;
    if ([tmpStockCurrentcy length] <=0 ) {
        tmpStockCurrentcy = [UserPrefConstants singleton].defaultCurrency;
    }
    return tmpStockCurrentcy;
}
#pragma mark SearchStockVCDelegate
- (void)didSelectBackOnSearchVC{
    
}
#pragma mark Time Sale

- (void)didReceiveTimeSaleNotification:(NSNotification *)noti{
    NSDictionary * response = [noti.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Parse data array to model array
        self->_timeSaleArr = [Utils getTimeSalesFrom:data];
        //For Last updated
        [self->tableControl reloadData];
    });
}

#pragma mark MarketInfoCellDelegate
- (void)expandRow:(NSString *)title{
    marketInfoHeader.lbl_Title.text = title;
}
- (void)didSelectMarketDepth:(MarketDepthModel *)model{
    if (self.tradeType == TradeType_Buy) {
        [self updateArrayControl:CONTROL_PRICE value:[model.buyPrice toCurrencyNumber]];
    }else if (self.tradeType == TradeType_Sell){
        [self updateArrayControl:CONTROL_PRICE value:[model.sellPrice toCurrencyNumber]];
    }
    [self reloadInputData];
}
- (void)didSelectExpand{
    [self expandAction:nil];
}
//User Grid view to handle list of all controls.


#pragma mark BaseCollectionTradeInput_CellDelegate
- (void)showDropDown:(BaseCollectionViewCell *)cell{
    NSIndexPath *index = [myCollectionView indexPathForCell:cell];
    if([cell isKindOfClass:[CollectionCell_OrderType class]]){
        [self showDictOptions:index];
    }else{
        [self showInputView:cell];
    }
    
}
- (void)showPopupPrice:(BaseCollectionViewCell *)sourceView{
    isShowingPopupPriceOrQty = YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TradeStoryboard" bundle:nil];
    PricePadViewController *pricePad = [storyboard instantiateViewControllerWithIdentifier:@"PricePadViewController"];
    pricePad.view.backgroundColor = [UIColor clearColor];
    [pricePad setDelegate:self];
    if(self.v_InputKeyboard.subviews.count > 1){
        [self.v_InputKeyboard.subviews[1] removeFromSuperview];
    }
    [self addVC:pricePad toView:self.v_InputKeyboard];
    BaseCollectionTradeInput_Cell *cell = ((BaseCollectionTradeInput_Cell *)sourceView);
    selectedCell = cell;
    [cell.tf_Input becomeFirstResponder];
}
- (void)showPopupQuantity:(BaseCollectionViewCell *)sourceView{
    isShowingPopupPriceOrQty = YES;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TradeStoryboard" bundle:nil];
    PricePadViewController *quantityPad = [storyboard instantiateViewControllerWithIdentifier:@"QuantityPadViewController"];
    quantityPad.view.backgroundColor = [UIColor clearColor];
    quantityPad.v_Content.layer.cornerRadius = kDEFAULT_CONNER_RADIUS;
    [quantityPad setDelegate:self];
    if(self.v_InputKeyboard.subviews.count > 1){
        [self.v_InputKeyboard.subviews[1] removeFromSuperview];
    }
    [self addVC:quantityPad toView:self.v_InputKeyboard];
    BaseCollectionTradeInput_Cell *cell = ((BaseCollectionTradeInput_Cell *)sourceView);
    selectedCell = cell;
    [cell.tf_Input becomeFirstResponder];
}


- (void)showInputView:(BaseCollectionViewCell *)sourceView{
    NSIndexPath *indexPath = [myCollectionView indexPathForCell:sourceView];
    NSDictionary* dicControl = arrCollectViewControls[indexPath.row];
    
    switch ([dicControl[@"control_type"] intValue]) {
        case CONTROL_TRIGGERPRICE:
        {
            whichPrice = 1;
            [self showPopupPrice:sourceView];
            break;
            //TextField
        }
        case CONTROL_PRICE:
        {
            whichPrice = 0;
            [self showPopupPrice:sourceView];
            break;
        }
        case CONTROL_QUANTITY:
        {
            whichQty = 0;
            [self showPopupQuantity:sourceView];
            break;
        }
        case CONTROL_MINQUANTITY:
        {
            whichQty = 1;
            [self showPopupQuantity:sourceView];
            break;
        }
            
            
        default:
            break;
    }
}

#pragma mark UIPopoverPresentationControllerDelegate
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    
    return UIModalPresentationNone;
}

#pragma mark PricePadDelegate
- (void)priceBtnPressed:(UIButton *)btn{
    NSString *value;
    // 0 = trade price
    if (whichPrice==0) {
        NSDictionary *dict = [self getObjControl: CONTROL_PRICE];
        CGFloat priceToBe = [[dict valueForKey:@"value"] floatValue] + [self getPriceValue:btn.tag];
        value = [[NSNumber numberWithDouble:priceToBe] toCurrencyNumber];
        if (priceToBe<=0) {
            [self showErrorNotification: [LanguageManager stringForKey:@"Price must be more than zero."] inViewController:self];
            return;
        }
        [self updateArrayControl:CONTROL_PRICE value:value];
    } else {
        // 1 = trigger price
        NSDictionary *dict = [self getObjControl: CONTROL_TRIGGERPRICE];
        CGFloat priceToBe = [[dict valueForKey:@"value"] floatValue] + [self getPriceValue:btn.tag];
        if (priceToBe<=0) {
            [self showErrorNotification: [LanguageManager stringForKey:@"Trigger Price must be more than zero."] inViewController:self];
            return;
        }
        value = [[NSNumber numberWithDouble:priceToBe] toCurrencyNumber];
        [self updateArrayControl:CONTROL_TRIGGERPRICE value:value];
    }
    selectedCell.tf_Input.text = [[NSNumber numberWithDouble:[value doubleValue]] toCurrencyNumber];
    [self calculatePrice];
}
#pragma mark QuantityPadDelegate
- (void)quantityBtnPressed:(UIButton *)btn{
    long quantityCount = 0;
    if (whichQty==0) {
        NSDictionary *dict = [self getObjControl: CONTROL_QUANTITY];
        quantityCount = [[dict valueForKey:@"value"] intValue] + (int)btn.tag;
        if (quantityCount<=0) {
            [self showErrorNotification: [LanguageManager stringForKey:@"Quantity must be more than zero."] inViewController:self];
            return;
        }
        [self updateArrayControl:CONTROL_QUANTITY value:[NSString stringWithFormat:@"%ld", quantityCount]];
    } else if (whichQty==1) {
        NSDictionary *dict = [self getObjControl: CONTROL_MINQUANTITY];
        quantityCount = [[dict valueForKey:@"value"] intValue] + btn.tag;
        if (quantityCount<=0) {
            [self showErrorNotification: [LanguageManager stringForKey:@"Min Quantity must be more than zero."] inViewController:self];
            return;
        }
        [self updateArrayControl:CONTROL_MINQUANTITY value:[NSString stringWithFormat:@"%ld", quantityCount]];
    }
    selectedCell.tf_Input.text = [NSString stringWithFormat:@"%ld", quantityCount];
    [self calculatePrice];
}

#pragma mark ATPDelegate
- (void)showShortSellConfirm:(BOOL)isBuyOrSell msg:(NSString *)msg{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:msg okTitle:[LanguageManager stringForKey:@"Ok"] withOKAction:^{
            [self->atp actionShortSell:isBuyOrSell];
        } cancelTitle:[LanguageManager stringForKey:@"Cancel"] cancelAction:^{
            [[AppState sharedInstance] forceRemoveLoadingView:self.view];
            self.contentMode = TradeContentModeTrade;
        }];
    });
    
}


@end

