//
//  MTradeAccountVC.h
//  TC_Mobile30
//
//  Created by n2nconnect on 13/09/2018.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

#import <UIKit/UIKit.h>
#import "UserAccountClientData.h"
#import "UserPrefConstants.h"
#import "MAccountCell.h"
#import "ATPAuthenticate.h"
#import "LanguageManager.h"


typedef void (^callBackAccountSelected) (UserAccountClientData *account);

@interface MTradeAccountVC : BaseVC <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,
UIGestureRecognizerDelegate>

@property (nonatomic, copy) callBackAccountSelected CallBackValue;

@property (weak, nonatomic) IBOutlet UITableView *accTable;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchOptions;

- (IBAction)cancelAccount:(id)sender;
- (IBAction)searchByField:(id)sender;

@end
