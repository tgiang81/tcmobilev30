//
//  SearchStockController.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/4/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchStockController : NSObject
typedef void (^Completion)(NSMutableArray *stocks);
- (void)searchAtFirst:(NSString *)keywords completion:(Completion)completion;
- (void)searchStock:(NSString *)keywords completion:(Completion)completion;
- (void)registerNotifications;
- (void)removeNotifications;
@end
