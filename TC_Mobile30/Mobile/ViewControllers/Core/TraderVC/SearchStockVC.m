//
//  SearchStockVC
//  TCiPad
//
//  Created by n2n on 23/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "SearchStockVC.h"
#import "SearchTableViewCell.h"
#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"
#import "QCConstants.h"
#import "UserPrefConstants.h"
#import "APIStockSearch.h"
#import "SearchStockController.h"

@interface SearchStockVC () {
    //HUD
    DGActivityIndicatorView *_hudActivity;
    SearchStockController *_controller;
}
@end

@implementation SearchStockVC

#define K_AUTOSEARCH_TIME 2.0

#pragma mark - view lifecycle deleegates



- (void)viewDidLoad {
    [super viewDidLoad];
    if(!_hudActivity){
        _hudActivity = [[Utils shareInstance]createPrivateActivity];
    }
    _controller = [SearchStockController new];
    [_controller registerNotifications];

    // Do any additional setup after loading the view.
    [self setBackButtonDefault];
    self.customBarTitle = [LanguageManager stringForKey:@"Order Pad"];

    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    _resultLabel.text = @"";
    
    self.stocksArray = [[NSMutableArray alloc] init];
    

    [_searchTable setBackgroundView:_pullMoreView];
    
    _searchBar.placeholder = [LanguageManager stringForKey:@"Search Stock"];

    _tableDataArr = [[NSMutableArray alloc] init];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelSearch:) name:@"dismissAllModalViews" object:nil];
    
//    [_searchBar becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_controller removeNotifications];
}


- (void)willMoveToParentViewController:(UIViewController *)parent
{
    [super willMoveToParentViewController:parent];
    if (!parent) {
        [_controller removeNotifications];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark actions

- (IBAction)cancelSearch:(id)sender {
    
    //[[ATPAuthenticate singleton] timeoutActivity];
    if (self.myBlock){
        self.myBlock(@{@"key": @"Cancel"});
    }
}


- (IBAction)goBack:(id)sender {
    [[Utils shareInstance] dismissActivity:_hudActivity];

    [[ATPAuthenticate singleton] timeoutActivity];
    
    [UserPrefConstants singleton].searchPage -= 1;
    [self executeSearch];
}

#pragma mark get stocks
//list stock searched
- (void)searchStocksReceived:(NSMutableArray *)stocks
{
    dispatch_async(dispatch_get_main_queue(), ^{

        [[Utils shareInstance] dismissActivity:self->_hudActivity];

        self->_searchTable.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
        self->_searchTable.contentOffset = CGPointMake(0, 0);
        
        [self->_pullMoreLabel setHidden:NO];
        
        [self->_pullImg1 setHidden:NO];[self->_pullImg2 setHidden:NO];
        [self->_pullActivity stopAnimating];
        
        if ([UserPrefConstants singleton].searchPage==0) self->_pullMoreContainer.hidden = YES;
        else self->_pullMoreContainer.hidden = NO;
        
        [self->_searchBar resignFirstResponder];

        self->_tableDataArr = stocks;
        
        
        if ([self->_tableDataArr count]>0) {
            
            long tot = [self->_tableDataArr count]-1;
            
            long recNum = 1+([UserPrefConstants singleton].searchPage)*20;
            NSString *fromRecStr = [NSString stringWithFormat:@"%ld",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%ld",recNum+tot];
            self->_resultLabel.text = [LanguageManager stringForKey:@"Showing Results %@ - %@"
                                             withPlaceholders:@{@"%fromRes%":fromRecStr, @"%toRes%":toRecStr}];
        } else {
            
            if ([UserPrefConstants singleton].searchPage>0)
            {
                
                self->_resultLabel.text = [LanguageManager stringForKey:@"No more items"];
                
            } else {
                
                self->_resultLabel.text = @"";
            }
            
        }
        
        [self->_searchTable reloadData];
    });
    
}

#pragma mark - Search Delegates

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [[Utils shareInstance] showActivity:_hudActivity inView:self.view];
    [_tableDataArr removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_searchTable reloadData];
    });
    
    [_controller searchAtFirst:searchBar.text completion:^(NSMutableArray *stocks) {
        [self searchStocksReceived:stocks];
    }];
    
}

// to load other page
-(void)executeSearch {
    [_controller searchStock:_searchBar.text completion:^(NSMutableArray *stocks) {
        [self searchStocksReceived:stocks];
    }];
}


#pragma mark - ScrollView Delegate (of tableview)

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    
    if (_canLoadData!=kDataLoadCannot) {
        // do loading here
        // set offset, hide label, show activity
        
        
        if (_canLoadData==kDataLoadNext) {
            
            _searchTable.contentInset = UIEdgeInsetsMake(0, 0, 60.0, 0);
            [_pullMoreLabel setHidden:YES];
            [_pullImg1 setHidden:YES];[_pullImg2 setHidden:YES];
            [_pullActivity startAnimating];
            [UserPrefConstants singleton].searchPage += 1;
            
            [self performSelector:@selector(executeSearch) withObject:nil afterDelay:1.0];
        }
        if (_canLoadData==kDataLoadPrev) {
            
            _searchTable.contentInset = UIEdgeInsetsMake(60, 0, 0.0, 0);
            [_pullMoreLabel setHidden:YES];
            [_pullImg1 setHidden:YES];[_pullImg2 setHidden:YES];
            [_pullActivity startAnimating];
            [UserPrefConstants singleton].searchPage -= 1;
            
            [self performSelector:@selector(executeSearch) withObject:nil afterDelay:1.0];
        }
        _canLoadData = kDataLoadCannot;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat scrollViewHeight;
    CGFloat scrollContentSizeHeight;
    CGFloat bottomInset;
    CGFloat scrollViewBottomOffset;
    CGFloat currentYOffset;
    CGFloat pullContainerXPos;
    
    
    scrollViewHeight = _searchTable.bounds.size.height;
    scrollContentSizeHeight = _searchTable.contentSize.height;
    bottomInset = _searchTable.contentInset.bottom;
    scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
    currentYOffset = _searchTable.contentOffset.y;
    pullContainerXPos = _searchTable.frame.size.width/2.0;
    
    
    [_pullMoreContainer setAlpha:0.0];
    
    // AT THE TOP
    if(currentYOffset < 0) {
        
        _pullImg1.image = _pullImg2.image = [UIImage imageNamed:@"pulldn"];
        _pullMoreContainer.center = CGPointMake(pullContainerXPos, _pullMoreContainer.frame.size.height/2.0);
        
        CGFloat draggedDist = - currentYOffset;
        if (draggedDist>=kPaginationTrigger) draggedDist = kPaginationTrigger;
        
        if (draggedDist>=kPaginationTrigger) {
            [_pullMoreContainer setAlpha:1.0];
            _pullImg1.transform = CGAffineTransformMakeRotation(M_PI);
            _pullImg2.transform = CGAffineTransformMakeRotation(-M_PI);
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Release to Load"];
            if ([UserPrefConstants singleton].searchPage>=1) _canLoadData = kDataLoadPrev;
        } else {
            if ([_pullActivity isAnimating]) {
                [_pullMoreContainer setAlpha:1.0];
            } else {
                [_pullMoreContainer setAlpha:draggedDist/kPaginationTrigger];
            }
            _pullImg1.transform = CGAffineTransformIdentity;
            _pullImg2.transform = CGAffineTransformIdentity;
            long recNum = 1+([UserPrefConstants singleton].searchPage-1)*20;
            
            NSString *fromRecStr = [NSString stringWithFormat:@"%ld",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%ld",recNum+19];
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Pull Down For Results %d-%d" withPlaceholders:@{@"%fromRecord%":fromRecStr,@"%toRecord%":toRecStr}];
            _canLoadData = kDataLoadCannot;
            if ([UserPrefConstants singleton].searchPage>=1) {
                [_pullMoreContainer setHidden:NO];
            } else {
                [_pullMoreContainer setHidden:YES];
            }
        }
        
    }
    
    
    
    // AT THE BOTTOM
    if(currentYOffset > scrollViewBottomOffset) {
        
        _pullImg1.image = _pullImg2.image = [UIImage imageNamed:@"pull"];
        _pullMoreContainer.center = CGPointMake(pullContainerXPos,
                                                _searchTable.frame.size.height-_pullMoreContainer.frame.size.height/2.0);
        
        CGFloat draggedDist = currentYOffset - scrollViewBottomOffset;
        if (draggedDist>=kPaginationTrigger) draggedDist = kPaginationTrigger;
        
        
        
        if (draggedDist>=kPaginationTrigger) {
            [_pullMoreContainer setAlpha:1.0];
            _pullImg1.transform = CGAffineTransformMakeRotation(M_PI);
            _pullImg2.transform = CGAffineTransformMakeRotation(-M_PI);
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Release to Load"];
            if ([_tableDataArr count]>=20) {
                _canLoadData = kDataLoadNext;
                _pullMoreContainer.hidden = NO;
            } else {
                _pullMoreContainer.hidden = YES;
            }
        } else {
            if ([_pullActivity isAnimating]) {
                [_pullMoreContainer setAlpha:1.0];
            } else {
                [_pullMoreContainer setAlpha:draggedDist/kPaginationTrigger];
            }
            _pullImg1.transform = CGAffineTransformIdentity;
            _pullImg2.transform = CGAffineTransformIdentity;
            long recNum = 1+([UserPrefConstants singleton].searchPage+1)*20;
            NSString *fromRecStr = [NSString stringWithFormat:@"%ld",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%ld",recNum+19];
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Pull Up For Results %d-%d" withPlaceholders:@{@"%fromRecord%":fromRecStr,@"%toRecord%":toRecStr}];
            _canLoadData = kDataLoadCannot;
            if ([_tableDataArr count]<20) {
                [_pullMoreContainer setHidden:YES];
            } else {
                [_pullMoreContainer setHidden:NO];
            }
        }
        
    }
}


#pragma mark - TableView Delegates


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 63.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *eachDict = [_tableDataArr objectAtIndex:indexPath.row];
    NSString *_stkCode = [eachDict objectForKey:FID_33_S_STOCK_CODE];
    NSString *stkName = [eachDict objectForKey:FID_38_S_STOCK_NAME];
    
    // subscribe selected stock only.
    [[[QCData singleton] qcFeedDataDict] setObject:eachDict forKey:_stkCode];
    // NSLog(@"didSelectRowAtIndexPath %@",[[QCData singleton] qcFeedDataDict]);
    
    //Get Stock information from StockCode.
    
//    [[VertxConnectionManager singleton] vertxSubscribeAllInQcFeed];
    if(self.tmpSelection == NO){
        [UserPrefConstants singleton].userSelectingStockCode = _stkCode;
    }
    

//    [[VertxConnectionManager singleton] vertxSearchStkCodeDetail:_stkCode];
    
    NSDictionary *stockDetail = [[NSDictionary alloc]initWithDictionary:[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode]];

    
    if (self.myBlock)
    {
        //Call vert or api to get Stock detail
        
        //        [[VertxConnectionManager singleton] vertxGetStockDetailWithStockArr:@[@"0108.KL"]
        //                                                                     FIDArr:[[AppConstants SortingList] allKeys] tag:0];
        
        self.myBlock(@{@"key": [[StockModel alloc] initWithDictionary:stockDetail error:nil]});
//        [self.navigationController popViewControllerAnimated:YES];

    }

    
    //comeback and refresh with selected Stock.
    //get stock structure by stockcode.
//    StockModel
//    self.blockSelectStock(<#NSDictionary *#>)

    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([_tableDataArr count]<=0) {
        
        if ([UserPrefConstants singleton].searchPage>0) {
            _searchTable.backgroundView = _pullMoreView;
            _searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
            
        } else {
            
            // Display a message when the table is empty
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            
            messageLabel.text = [LanguageManager stringForKey:@"No Match."];
            messageLabel.textColor = [UIColor whiteColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            [messageLabel sizeToFit];
            
            _searchTable.backgroundView = messageLabel;
            _searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        }
    } else {
        _searchTable.backgroundView = _pullMoreView;
        _searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [_tableDataArr count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"searchCellID";
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell = [[SearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    cell.searchResultLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellTextColor);
    cell.searchResultLabel2.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellTextColor);
    NSDictionary *eachDict = [_tableDataArr objectAtIndex:indexPath.row];
    
    cell.searchResultLabel.text = [eachDict objectForKey:FID_33_S_STOCK_CODE];
    cell.searchResultLabel2.text = [eachDict objectForKey:FID_39_S_COMPANY];
    // load data
    [cell.btnCell addTarget:self action:@selector(fnCell:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnCell.tag = indexPath.row + 100;
    return cell;
}

- (IBAction) fnCell:(UIButton*)sender {
    [[ATPAuthenticate singleton] timeoutActivity];
    
    NSDictionary *eachDict = [_tableDataArr objectAtIndex:sender.tag-100];
    
    NSString *_stkCode = [eachDict objectForKey:FID_33_S_STOCK_CODE];
    NSString *stkName = [eachDict objectForKey:FID_38_S_STOCK_NAME];
    
    // subscribe selected stock only.
    [[[QCData singleton] qcFeedDataDict] setObject:eachDict forKey:_stkCode];
    // NSLog(@"didSelectRowAtIndexPath %@",[[QCData singleton] qcFeedDataDict]);
    
    //Get Stock information from StockCode.
    
    //    [[VertxConnectionManager singleton] vertxSubscribeAllInQcFeed];
    
    [UserPrefConstants singleton].userSelectingStockCode = _stkCode;
    
    //    [[VertxConnectionManager singleton] vertxSearchStkCodeDetail:_stkCode];
    
    NSDictionary *stockDetail = [[NSDictionary alloc]initWithDictionary:[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode]];
    
    
    if (self.myBlock)
    {
        //Call vert or api to get Stock detail
        
        //        [[VertxConnectionManager singleton] vertxGetStockDetailWithStockArr:@[@"0108.KL"]
        //                                                                     FIDArr:[[AppConstants SortingList] allKeys] tag:0];
        
        self.myBlock(@{@"key":   [[StockModel alloc] initWithDictionary:stockDetail error:nil]   });
       
    }
    [self backVC:nil];

}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 ==0) {
        cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellBg1);

    } else {
        cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellBg2);
    }
    
}

- (void)updateTheme:(NSNotification *)noti{
    [super updateTheme:noti];
    self.searchBar.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].st_cellBg2);
}
@end
