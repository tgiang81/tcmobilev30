//
//  MTradeStatusViewController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/14/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MTradeStatusViewController.h"
#import "ThemeManager.h"
#import "AroundShadowView.h"
#import "MarketInfoCell.h"
@interface MTradeStatusViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet AroundShadowView *v_Content;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;

@end

@implementation MTradeStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    self.tbl_Content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    [self.tbl_Content registerNib:[UINib nibWithNibName:[MarketInfoCell nibName] bundle:nil] forCellReuseIdentifier:[MarketInfoCell reuseIdentifier]];
    
    self.v_Content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    self.tsTitle.text = [LanguageManager stringForKey:@"Trade Status"];
    [self.openOrdBkBtn setTitle:[LanguageManager stringForKey:@"Order Book"] forState:UIControlStateNormal];
    [self.openPortfolioBtn setTitle:[LanguageManager stringForKey:@"Portfolio"] forState:UIControlStateNormal];
    [self.openQuoteBtn setTitle:[LanguageManager stringForKey:@"Quote Screen"] forState:UIControlStateNormal];
    [self.closeBtn setTitle:[LanguageManager stringForKey:@"Close"] forState:UIControlStateNormal];
    
    self.tsTitle.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_topBg);
    self.tsTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_topTextColor);
    
    self.lbl_status.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_LabelTextColor);
    self.tsMessage.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_TextColor);
    self.openOrdBkBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_openBg);
    self.openPortfolioBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_openBg);
    self.openQuoteBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_openBg);
    self.closeBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_SpecialButtonBg);
    
    [self.openOrdBkBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_openTextColor) forState:UIControlStateNormal];
    [self.openPortfolioBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_openTextColor) forState:UIControlStateNormal];
    [self.openQuoteBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_openTextColor) forState:UIControlStateNormal];
    [self.closeBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_SpecialButtonTextColor) forState:UIControlStateNormal];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tbl_Content reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MarketInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[MarketInfoCell reuseIdentifier] forIndexPath:indexPath];
    [cell setMarketData:self.arrMarketDepthItems timeSale:self.timeSaleArr bDone:self.bizDoneTableDataArr stockCode:self.stockCode width:SCREEN_WIDTH_PORTRAIT - 16];
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
@end
