//
//  MSkipConfirmationViewController.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/14/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SkipConfirmationViewController.h"
@class AroundShadowView;
NS_ASSUME_NONNULL_BEGIN

@interface MSkipConfirmationViewController : SkipConfirmationViewController
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Back;

@property (weak, nonatomic) IBOutlet UIView *pinView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_PinHeight;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_Content;
@property (strong, nonatomic) NSArray *bizDoneTableDataArr;
@property (strong, nonatomic) NSArray *arrMarketDepthItems;
@property (strong, nonatomic) NSArray *timeSaleArr;
@property (strong, nonatomic) NSString *stockCode;
@end

NS_ASSUME_NONNULL_END
