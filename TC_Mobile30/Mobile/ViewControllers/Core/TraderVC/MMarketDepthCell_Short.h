//
//  MMarketDepthCell.h
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTableViewCell.h"

@class MarketDepthModel;
@interface MMarketDepthCell_Short : BaseTableViewCell{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblBuySplit1;
@property (weak, nonatomic) IBOutlet UILabel *lblBidQty;
@property (weak, nonatomic) IBOutlet UILabel *lblBid;
@property (weak, nonatomic) IBOutlet UILabel *lblAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblAskQty;
@property (weak, nonatomic) IBOutlet UILabel *lblSellSplit1;

//Main
- (void)setupDataFrom:(MarketDepthModel *)model comparePrice:(float)refPrice atIndexPath:(NSIndexPath *)indexPath;

@end
