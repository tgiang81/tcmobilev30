//
//  SearchStockController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/4/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SearchStockController.h"
#import "UserPrefConstants.h"
#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"
@implementation SearchStockController
{
    Completion _completion;
}
- (void)registerNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchDataDictReceived:)  name:@"doneVertxSearch" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchStockDetai:)  name:@"doneVertxSearchStock" object:nil];
}
- (void)removeNotifications{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doneVertxSearch" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doneVertxSearchStock" object:nil];
}
- (void)searchAtFirst:(NSString *)keywords completion:(Completion)completion{
    [UserPrefConstants singleton].searchPage = 0;
    [self searchStock:keywords completion:completion];
}
- (void)searchStock:(NSString *)keywords completion:(Completion)completion{
    _completion = completion;
    
    [[VertxConnectionManager singleton] vertxSearchV3:keywords];
    
    [[ATPAuthenticate singleton] timeoutActivity];
}


- (void)searchStockDetai:(NSNotification *)notification
{
    NSLog(@"%@", notification);
    
}


//list stock searched
- (void)searchDataDictReceived:(NSNotification *)notification
{
    NSDictionary * searchResultDict = [notification.userInfo copy];
    NSMutableArray *tableDataArr = [NSMutableArray arrayWithArray:[searchResultDict objectForKey:FID_33_S_STOCK_CODE]];
    if(_completion){
        _completion(tableDataArr);
    }
    
}
@end
