//
//  MTradeAccountVC.m
//  TC_Mobile30
//
//  Created by n2nconnect on 13/09/2018.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MTradeAccountVC.h"

@interface MTradeAccountVC () {
    
    CGSize scrSize;
    NSInteger searchBy;
    NSMutableArray *tableDataArr;
}

@end

@implementation MTradeAccountVC

#pragma mark - LifeCycles View Delegates

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.customBarTitle = [LanguageManager stringForKey:@"Accounts"];

    
    tableDataArr = [NSMutableArray new];
    
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    
    searchBy = 0;
    
    _searchBar.placeholder = [LanguageManager stringForKey:@"Enter Search Keywords"];
    
    [_searchOptions setTitle:[LanguageManager stringForKey:@"Name"] forSegmentAtIndex:0];
    [_searchOptions setTitle:[LanguageManager stringForKey:@"Account No."] forSegmentAtIndex:1];
    
    [tableDataArr addObjectsFromArray: [UserPrefConstants singleton].userAccountlist ];

    scrSize = [[UIScreen mainScreen] bounds].size;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backButtonTapped:) name:@"dismissAllModalViews" object:nil];
    
    [self setBackButtonDefault];
}
- (void)backButtonTapped:(id)sender{
    [[ATPAuthenticate singleton] timeoutActivity];
    [super backButtonTapped:sender];
}


#pragma mark - SearchBar Delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if ([searchText length]<=0) {
        tableDataArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userAccountlist];
        [_accTable reloadData];
        
        // then scroll to current selection
        long idx = [tableDataArr indexOfObject:[UserPrefConstants singleton].userSelectedAccount];
        
        if (idx>0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
            [_accTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    } else{
        
        NSString *filter;
        if (searchBy==1) {
            filter = @"client_account_number BEGINSWITH[cd] %@";
        } else {
            filter = @"client_name BEGINSWITH[cd] %@";
        }
        NSPredicate* predicate = [NSPredicate predicateWithFormat:filter, searchText];
        NSArray *filteredArr  = [[UserPrefConstants singleton].userAccountlist filteredArrayUsingPredicate:predicate];
        tableDataArr = [[NSMutableArray alloc] initWithArray:filteredArr];
        [_accTable reloadData];
    }
    
}

#pragma mark - TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [UserPrefConstants singleton].userSelectedAccount = [tableDataArr objectAtIndex:indexPath.row];
    
    if (_CallBackValue) {
        _CallBackValue(  [UserPrefConstants singleton].userSelectedAccount );
    }
    
    [self cancelAccount:nil];
}
- (IBAction)cancelAccount:(id)sender {
    [[ATPAuthenticate singleton] timeoutActivity];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    _accTable.backgroundView = nil;
    _accTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableDataArr count];
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   MAccountCell *cell = (MAccountCell*)[tableView dequeueReusableCellWithIdentifier:@"MAccountCellID"];

    [cell populateItemsAtIndexPath:indexPath withArray:tableDataArr];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 ==0) {
        cell.backgroundColor = RGB(4, 25, 42);
        
    } else {
        cell.backgroundColor = RGB(11, 35, 52);
    }
    
}

@end
