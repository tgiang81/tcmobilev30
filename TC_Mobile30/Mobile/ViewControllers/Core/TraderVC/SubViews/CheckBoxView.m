//
//  CheckBoxView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "CheckBoxView.h"

@implementation CheckBoxView
-(void)setupView{
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_InfoCellText);
}
- (IBAction)didTapCheckBox:(id)sender {
    self.isSelectedCell = !self.isSelectedCell;
    if (self.CallBackValue) {
        if(self.isSelectedCell == YES)
        {
            [sender setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        }
        else
        {
            [sender setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
        self.CallBackValue(self.index ,@{@"value": @( self.isSelectedCell ) });
    }
}

@end
