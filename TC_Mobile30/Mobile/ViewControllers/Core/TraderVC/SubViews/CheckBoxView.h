//
//  CheckBoxView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"
#import "AllCommon.h"
NS_ASSUME_NONNULL_BEGIN

@interface CheckBoxView : TCBaseSubView
@property (assign, nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (nonatomic, copy) callBackValue CallBackValue;
@property (assign) BOOL isSelectedCell;
@end

NS_ASSUME_NONNULL_END
