//
//  TimeSaleHeader.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TimeSaleHeader.h"
#import "ThemeManager.h"
#import "LanguageManager.h"
@implementation TimeSaleHeader


- (void)awakeFromNib{
    [super awakeFromNib];
    
    self.lbl_time.text = [LanguageManager stringForKey:@"Time"];
    self.lbl_orderType.text = [LanguageManager stringForKey:@"PI"];
    self.lbl_Price.text = [LanguageManager stringForKey:@"Price"];
    self.lbl_Volumne.text = [LanguageManager stringForKey:@"Vol"];
}
- (void)setupTextColor{
    self.lbl_time.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketHeaderText);
    self.lbl_orderType.textColor = self.lbl_time.textColor;
    self.lbl_Price.textColor = self.lbl_time.textColor;
    self.lbl_Volumne.textColor = self.lbl_time.textColor;
}
@end
