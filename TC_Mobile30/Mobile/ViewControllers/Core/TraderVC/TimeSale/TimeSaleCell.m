//
//  TimeSaleCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TimeSaleCell.h"
#import "TimeSaleModel.h"
#import "NSNumber+Formatter.h"
#import "NSDate+Utilities.h"
@implementation TimeSaleCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellBg);
    _lbl_time.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    _lbl_orderType.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    _lbl_Volume.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    _lbl_Price.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
    // Initialization code
}

-(void)setupDataFrom:(TimeSaleModel *)timeSale{
    NSDate *date = [NSDate dateFromString:timeSale.time format:kSERVER_FORMAT_TIME];
    _lbl_time.text = date? [date stringWithFormat:kUPDATE_FORMAT_TIME] : @"#";
    _lbl_orderType.text = timeSale.PI;
    _lbl_Volume.text = [timeSale.vol abbreviateNumber];
    _lbl_Price.text = [timeSale.incomingPrice toCurrencyNumber];

}
@end
