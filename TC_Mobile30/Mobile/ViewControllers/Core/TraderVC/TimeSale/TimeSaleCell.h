//
//  TimeSaleCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseTableViewCell.h"
@class TimeSaleModel;
@interface TimeSaleCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_orderType;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Volume;
-(void)setupDataFrom:(TimeSaleModel *)timeSale;
@end
