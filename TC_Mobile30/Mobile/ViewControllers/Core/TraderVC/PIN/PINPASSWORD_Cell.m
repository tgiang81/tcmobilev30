//
//  PINPASSWORD_Cell.m
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PINPASSWORD_Cell.h"
#import "UISegmentedControl+Util.h"
#import "AppMacro.h"

@implementation PINPASSWORD_Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = COLOR_FROM_HEX(0x374d5a);
    self.sg_pinPass.layer.cornerRadius = 4;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)fnBtnClick:(UIButton *)sender
{
    self.isSelectedCell = !self.isSelectedCell;
    if (_CallBackValue) {
        if(self.isSelectedCell == YES)
        {
            [sender setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        }
        else
        {
            [sender setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
        _CallBackValue(0 ,@{@"value": @( self.isSelectedCell ) });
    }
}
- (IBAction)changeValue:(UITextField *)sender {
    [self.delegate validatePINokBtnClickedWithPassword:sender.text andType:_validatingType];
}



@end
