//
//  PINPASSWORD_Cell.h
//  TCiPad
//
//  Created by n2nconnect on 23/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"
#import "ValidatePINViewController.h"
@interface PINPASSWORD_Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *sg_pinPass;
@property (nonatomic, copy) callBackValue CallBackValue;
@property (weak, nonatomic) id<ValidatePINDelegate> delegate;
@property (nonatomic, assign) ValType validatingType;
@property (assign) BOOL isSelectedCell;
@end
