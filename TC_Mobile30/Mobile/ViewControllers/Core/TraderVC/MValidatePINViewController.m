//
//  MValidatePINViewController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/14/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MValidatePINViewController.h"
#import "ThemeManager.h"
@interface MValidatePINViewController ()

@end

@implementation MValidatePINViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    
    self.myTitle.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_topBg);
    self.myTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_titleColor);
    self.rememberPin.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_rememberTitle);
    
    self.dvDataView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_okBg);
    
    self.cancelBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_cancelColor);
    [self.cancelBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor) forState:UIControlStateNormal];
    
    self.okBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor);
    [self.okBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor) forState:UIControlStateNormal];
    
    [self.cancelBtn setTitle:[LanguageManager stringForKey:@"Cancel"] forState:UIControlStateNormal];
    [self.okBtn setTitle:[LanguageManager stringForKey:@"Ok"] forState:UIControlStateNormal];
//    self.okBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_okBg);
//    [self.okBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_okTitle) forState:UIControlStateNormal];
//    self.cancelBtn.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_cancelBg);
//    [self.cancelBtn setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].pin_cancelTitle) forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
