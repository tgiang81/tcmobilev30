//
//  SearchStockVC.h
//  TC_Mobile30
//
//  Created by n2nconnect on 07/09/2018.
//  Copyright © 2018 n2nconnect. All rights reserved.
//
//SearchViewController

#import "BaseVC.h"

#import <Speech/Speech.h>
#import "LanguageManager.h"
#import "QCData.h"
#import "VertxConnectionManager.h"
#import "StocksCollectionViewCell.h"
#import "WatchListStk.h"
#import "UIViewController+Util.h"
typedef void (^blockSelectStock)(NSDictionary* _Nullable dic);


@interface SearchStockVC : BaseVC <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, StockCellDelegate>

@property (nonatomic, copy, nullable) blockSelectStock myBlock;


@property (weak, nonatomic) IBOutlet UITableView *searchTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *tableDataArr;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

// add to watchlist
@property (nonatomic, strong) NSMutableArray *stocksArray;
@property (weak, nonatomic) IBOutlet UICollectionView *stockCollectionView;


// pullmore
@property (strong, nonatomic) IBOutlet UIView *pullMoreView;
@property (weak, nonatomic) IBOutlet UIView *pullMoreContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *pullActivity;
@property (weak, nonatomic) IBOutlet UILabel *pullMoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pullImg1;
@property (weak, nonatomic) IBOutlet UIImageView *pullImg2;
@property (nonatomic, assign) int canLoadData;
@property (nonatomic, assign) BOOL tmpSelection;
- (IBAction)cancelSearch:(id)sender;
- (IBAction)goBack:(id)sender;


@end

