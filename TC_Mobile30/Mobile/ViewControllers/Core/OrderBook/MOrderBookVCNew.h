//
//  MOrderBookVCNew.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "MOrderBookDefine.h"
#import "MOrderBookView.h"
@class MOrderBookView;
NS_ASSUME_NONNULL_BEGIN

@interface MOrderBookVCNew : BaseVC
@property (weak, nonatomic) IBOutlet MOrderBookView *v_Content;

@end

NS_ASSUME_NONNULL_END
