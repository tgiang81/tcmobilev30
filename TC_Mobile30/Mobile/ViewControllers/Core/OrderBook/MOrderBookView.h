//
//  MOrderBookView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"
#import "MOrderBookSelectAccountView.h"
#import "MOrderBookDateView.h"
#import "MOrderBookDefine.h"
#import "OrderBookNewHeaderView.h"
@class TCPageMenuControl;
@class TradeStatus;
@class UserAccountClientData;
NS_ASSUME_NONNULL_BEGIN
@protocol MOrderBookViewDelegate<MOrderBookSelectAccountViewDelegate, MOrderBookDateViewDelegate, OrderBookNewHeaderViewDelegate>
- (TradeStatus *)getTradeStatus:(NSInteger)index;
- (void)didSelectStatusAt:(NSInteger)index;
- (void)didChangeOrderBookType:(MOrderBookVCType)type;
- (NSInteger)numberOfItems;
- (void)refreshData;
@end
@interface MOrderBookView : TCBaseSubView
@property (weak, nonatomic) IBOutlet OrderBookNewHeaderView *v_HeaderTable;

@property (weak, nonatomic) IBOutlet MOrderBookSelectAccountView *v_SelectingAccount;

@property (weak, nonatomic) IBOutlet TCPageMenuControl *v_Page;
@property (weak, nonatomic) IBOutlet MOrderBookDateView *v_DateView;
@property (weak, nonatomic) IBOutlet UIView *v_Header;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_DatePickerHeight;

@property (weak, nonatomic) id<MOrderBookViewDelegate> delegate;
- (void)setAccountInfo:(UserAccountClientData *)accountInfo;
- (void)setStartDate:(NSDate *)startDate;
- (void)setEndDate:(NSDate *)endDate;
- (void)reloadData;
- (void)showDatePicker:(BOOL)isShow;
- (void)stopRefreshAnimation;
@end

NS_ASSUME_NONNULL_END
