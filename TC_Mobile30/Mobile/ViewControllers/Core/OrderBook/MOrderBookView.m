//
//  MOrderBookView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MOrderBookView.h"
#import "TCPageMenuControl.h"
#import "ThemeManager.h"
#import "AppFont.h"
#import "NSDate+Utilities.h"
#import "OrderBookNewV3Cell.h"
#import "TradeStatus.h"
#import "SVPullToRefresh.h"
@interface MOrderBookView() <TCControlViewDelegate, UITableViewDelegate, UITableViewDataSource, MOrderBookDateViewDelegate, MOrderBookSelectAccountViewDelegate, OrderBookNewV3CellDelegate, OrderBookNewHeaderViewDelegate>

@end
@implementation MOrderBookView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    self.tbl_Content.tableFooterView = [UIView new];
    self.v_DateView.delegate = self;
    self.v_HeaderTable.delegate = self;
    self.v_SelectingAccount.delegate = self;
    [self.tbl_Content registerNib:[UINib nibWithNibName:[OrderBookNewV3Cell nibName] bundle:nil] forCellReuseIdentifier:[OrderBookNewV3Cell reuseIdentifier]];
    
    [self.tbl_Content addPullToRefreshWithActionHandlerAndRemoveArrow:^{
        if([self.delegate respondsToSelector:@selector(refreshData)]){
            [self.delegate refreshData];
        }
    }];
    
    [self setupTopControl];
    [self showDatePicker:NO];
}
- (void)stopRefreshAnimation{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tbl_Content stopAnimating];
    });
    
}
- (void)reloadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tbl_Content reloadData];
    });
    
}
- (void)setupTopControl{
    _v_Page.itemWidth = SCREEN_WIDTH_PORTRAIT / 2;
    _v_Page.dataSources = [self getTopTitles];
    _v_Page.delegate = self;
    _v_Page.font = AppFont_MainFontMediumWithSize(15);
    _v_Page.indicatorColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    _v_Page.unselectedColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_unSelectTextColor);
    _v_Page.selectedColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    [_v_Page startLoadingPageMenu];
}
- (void)showDatePicker:(BOOL)isShow{
    [self.v_DateView setHidden:!isShow];
    if(isShow == YES){
        self.cst_DatePickerHeight.constant = 67;
    }else{
        self.cst_DatePickerHeight.constant = 0;
    }
}
- (NSArray *)getTopTitles{
    return @[@"Today", @"History"];
}

- (void)setAccountInfo:(UserAccountClientData *)accountInfo{
    [_v_SelectingAccount setDisplayAccount: accountInfo];
}
- (void)setStartDate:(NSDate *)startDate{
    [self.v_DateView.btn_StartDate setTitle:[startDate stringWithFormat:kDisplayFormatDate] forState:UIControlStateNormal];
}
- (void)setEndDate:(NSDate *)endDate{
    [self.v_DateView.btn_EndDate setTitle:[endDate stringWithFormat:kDisplayFormatDate] forState:UIControlStateNormal];
}
#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.delegate respondsToSelector:@selector(getTradeStatus:)]){
        TradeStatus *status = [self.delegate getTradeStatus:indexPath.row];
        status.isExpanded = !status.isExpanded;
        [self.tbl_Content reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}
#pragma mark TCControlViewDelegate
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
    [self showDatePicker:index != MOrderBookVCToday];
    if([self.delegate respondsToSelector:@selector(didChangeOrderBookType:)]){
        [self.delegate didChangeOrderBookType:index];
    }
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([self.delegate respondsToSelector:@selector(numberOfItems)]){
        return [self.delegate numberOfItems];
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderBookNewV3Cell *cell = [tableView dequeueReusableCellWithIdentifier:[OrderBookNewV3Cell reuseIdentifier] forIndexPath:indexPath];
    cell.delegate = self;
    if([self.delegate respondsToSelector:@selector(getTradeStatus:)]){
        [cell setup:[self.delegate getTradeStatus:indexPath.row]];
    }
    return cell;
}

#pragma mark MOrderBookDateViewDelegate
- (void)didSelectStartDate{
    if([self.delegate respondsToSelector:@selector(didSelectStartDate)]){
        [self.delegate didSelectStartDate];
    }
}
- (void)didSelectEndDate{
    if([self.delegate respondsToSelector:@selector(didSelectEndDate)]){
        [self.delegate didSelectEndDate];
    }
}
#pragma mark MOrderBookSelectAccountViewDelegate
- (void)didSelectAccountView{
    if([self.delegate respondsToSelector:@selector(didSelectAccountView)]){
        [self.delegate didSelectAccountView];
    }
}
#pragma mark OrderBookNewV3CellDelegate
- (void)expandCellAt:(UITableViewCell *)cell{
    NSIndexPath *indexPath = [self.tbl_Content indexPathForCell:cell];
    if([self.delegate respondsToSelector:@selector(didSelectStatusAt:)]){
        [self.delegate didSelectStatusAt:indexPath.row];
    }
}

#pragma mark OrderBookNewHeaderViewDelegate
- (void)didSelectName{
    if([self.delegate respondsToSelector:@selector(didSelectName)]){
        [self.delegate didSelectName];
    }
}
- (void)didSelectPrice{
    if([self.delegate respondsToSelector:@selector(didSelectPrice)]){
        [self.delegate didSelectPrice];
    }
}
- (void)didSelectStatus{
    if([self.delegate respondsToSelector:@selector(didSelectStatus)]){
        [self.delegate didSelectStatus];
    }
}
@end
