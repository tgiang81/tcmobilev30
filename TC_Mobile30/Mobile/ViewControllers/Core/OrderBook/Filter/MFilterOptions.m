//
//  MFilterOptions.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MFilterOptions.h"
#import "ExchangeData.h"
#import "Utils.h"
@implementation MFilterOptions
+ (NSString *)getContentString:(id)object getKey:(BOOL)getKey{
    if([object isKindOfClass:[NSString class]]){
        return object;
    }else if([object isKindOfClass:[ExchangeData class]]){
        if(getKey){
            return ((ExchangeData*)object).trade_exchange_code;
        }else{
            return [Utils getShortExChangeName:((ExchangeData*)object)];
        }
        
    }else if([object isKindOfClass:[NSArray class]]){
        NSString *content = @"";
        for(id tmpObject in object){
            content = [NSString stringWithFormat:@"%@%@", content, [self getContentString:tmpObject getKey:getKey]];
        }
        return content;
    }else if([object isKindOfClass:[NSDictionary class]]){
        if(getKey){
            return [object allKeys].firstObject;
        }else{
            return [object allValues].firstObject;
        }
        
    }
    return @"";
}
@end
