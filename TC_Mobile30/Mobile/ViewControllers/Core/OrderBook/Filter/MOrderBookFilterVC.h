//
//  MOrderBookFilterVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@protocol MOrderBookFilterVCDelegate<NSObject>
- (void)didSelectApply;
@end
@interface MOrderBookFilterVC : BaseVC
@property (weak, nonatomic) IBOutlet UIView *view_top;
@property (weak, nonatomic) IBOutlet UITableView *tbl_content;
@property (weak, nonatomic) IBOutlet UIView *view_bottom;
@property (weak, nonatomic) IBOutlet UIButton *btn_reset;
@property (weak, nonatomic) IBOutlet UIButton *btn_apply;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Close;
@property (weak, nonatomic) id<MOrderBookFilterVCDelegate> delegate;


- (void)initData;
@end
