//
//  MFilterOptions.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/18/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//
#define kSectionTitleOrderStatus            @"Order Status"


#define kSectionTitleExchange               @"Exchange"
//#define kSectionValueExchange               @[@"KL", @"SGX", @"SET", @"ASX", @"TYO", @"TSX"]

#define kSectionTitleValidity               @"Validity"
//#define kSectionValueValidity               @[@"Day", @"GTD", @"FAK", @"FOK", @"GTM"]

#define kSectionTitleOrderType              @"Order Type"
//#define kSectionValueOrderType              @[@"Limit", @"MKT", @"MarketToLimit", @"Stop", @"StopLimit", @"LimitIfTouched"]

#define kSectionTitleStockTitle             @"Stock Title"
#define kSectionValueStockTitle             @[@"Name", @"Code"]

#define kSectionTitlePaymentTitle           @"Payment Type"
#define kSectionValuePaymentValue           @[@"Cash", @"CPF", @"CUT", @"SRS"]

#define kSectionTitleDateRange              @"Date Range"
#define kSectionValueDateRange              @[@"Date"]
#define kStartAt                            @"kStartAt"
#define kEndAt                              @"kEndAt"



#define kFilterNotification                 @"kFilterNotification"
#define kFilterChangeValue                  @"kIsChange"

@interface MFilterOptions:NSObject
+ (NSString *)getContentString:(id)object getKey:(BOOL)getKey;
@end
    

