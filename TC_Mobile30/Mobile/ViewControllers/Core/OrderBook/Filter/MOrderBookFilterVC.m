//
//  MOrderBookFilterVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MOrderBookFilterVC.h"
#import "DefineChooseSwift.h"
#import "OrderBookFilterItemCell.h"
#import "OrderBookDatePickerCell.h"
#import "OrderBookFilterItemCell.h"
#import "MFilterOptions.h"
#import "Utils.h"
#import "ExchangeData.h"
#import "MFilterOptions.h"
#import "UICollectionView+Util.h"
#import "UITableView+Util.h"
#import "UserPrefConstants.h"
#import "LanguageKey.h"
#import "MOrderBookController.h"
@interface MOrderBookFilterVC () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, OrderBookFilterItemCellDelegate, OrderBookDatePickerCellDelegate>
{
    NSMutableArray *sections;
    
    NSMutableDictionary *selectedOptions;
    
    NSTimeInterval _startAt;
    NSTimeInterval _endAt;
}
@end

@implementation MOrderBookFilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBackButtonDefault];
    [self setCustomBarTitle:[LanguageManager stringForKey:Filter_By]];
    [self setupUI];
    [self initData];
    [self setupLanguage];
    // Do any additional setup after loading the view from its nib.
}

- (void)initData{
    [self initFromDateToDate];
    selectedOptions = [NSMutableDictionary new];
    NSMutableArray *tmpKeys = [NSMutableArray new];
    NSMutableArray *tmpValues = [NSMutableArray new];
    sections = [NSMutableArray new];
    if([Utils getExchangeList].count > 0){
        [tmpKeys addObject:kSectionTitleExchange];
        [tmpValues addObject:[Utils getExchangeList]];
    }
    NSArray *OrderStatus = [MOrderBookController getFilterStatusOptions];
    if(OrderStatus.count > 0){
        [tmpKeys addObject:kSectionTitleOrderStatus];
        [tmpValues addObject:OrderStatus];
    }
    if([Utils getOrderTypeList].count > 0){
        [tmpKeys addObject:kSectionTitleOrderType];
        [tmpValues addObject:[Utils getOrderTypeList]];
    }
    if([Utils getValidityList].count > 0){
        [tmpKeys addObject:kSectionTitleValidity];
        [tmpValues addObject:[Utils getValidityList]];
    }
    
    NSMutableArray *payment = [Utils getPaymentList];
    if(payment.count > 0){
        [tmpKeys addObject:kSectionTitlePaymentTitle];
        [tmpValues addObject:payment];
    }
    DLog(@"");
    //    if(kSectionValueStockTitle.count > 0){
    //        [tmpKeys addObject:kSectionTitleStockTitle];
    //        [tmpValues addObject:kSectionValueStockTitle];
    //    }
    //    if(kSectionValueDateRange.count > 0){
    //        [tmpKeys addObject:kSectionTitleDateRange];
    //        [tmpValues addObject:kSectionValueDateRange];
    //    }
    
    selectedOptions = [[NSMutableDictionary alloc] initWithDictionary:[UserPrefConstants singleton].filterOrderBook];
    
    for(int index = 0; index < tmpValues.count; index++){
        [sections addObject:@{tmpKeys[index]:tmpValues[index]}];
    }
    [self.tbl_content reloadWithAutoSizingCell];
}
- (void)initFromDateToDate{
    _startAt = 1442640248;
    _endAt = 1538265600;
}
- (void)setupUI{
    self.btn_reset.layer.borderWidth = 2;
    self.btn_apply.layer.borderWidth = 2;
    self.btn_reset.layer.cornerRadius = 6;
    self.btn_apply.layer.cornerRadius = 6;
    
    [self.tbl_content registerNib:[UINib nibWithNibName:@"OrderBookFilterCell" bundle:nil] forCellReuseIdentifier:@"OrderBookFilterCell"];
    self.tbl_content.delegate = self;
    self.tbl_content.dataSource = self;
    self.tbl_content.estimatedRowHeight = 100;
    
}
-(void)setupLanguage{
    [self.btn_reset setTitle:[LanguageManager stringForKey:Reset] forState:UIControlStateNormal];
    [self.btn_apply setTitle:[LanguageManager stringForKey:Apply] forState:UIControlStateNormal];
}
- (BOOL)isSelected:(NSString *)key value:(NSString *)value{
    if([selectedOptions objectForKey:key]){
        return [[selectedOptions objectForKey:key] containsObject:value];
    }
    return NO;
    
}
- (NSDictionary *)getStartEndDate{
    return @{kStartAt:[NSNumber numberWithDouble:_startAt], kEndAt:[NSNumber numberWithDouble:_endAt]};
}
- (NSDictionary *)calculateSectionsToApply{
    //    [selectedOptions setValue:[self getStartEndDate] forKey:kSectionTitleDateRange];
    return [[NSDictionary alloc] initWithDictionary:selectedOptions];
}
- (void)calculateSelectOptions:(NSIndexPath *)indexPath{
    NSString *key = [self getTitleAtSection:indexPath.section];
    NSString *value = [self getValueAtSection:indexPath.section atRow:indexPath.row getKey:YES];
    NSMutableArray *values = [NSMutableArray arrayWithArray:[selectedOptions objectForKey:key]];
    if(values == nil){
        values = [NSMutableArray new];
    }
    if(![values containsObject:value]){
        [values addObject:value];
    }else{
        [values removeObject:value];
    }
    [selectedOptions setValue:values forKey:key];
}
- (void)reloadFilter{
    [selectedOptions removeAllObjects];
    [self.tbl_content reloadWithAutoSizingCell];
}
- (NSMutableDictionary *)getUserDict:(BOOL)isChange{
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:[NSNumber numberWithBool:isChange] forKey:kFilterChangeValue];
    return notificationData;
}
- (IBAction)close:(id)sender {
    [AppDelegateAccessor toogleRightMenu:^{
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kFilterNotification
         object:self userInfo:[self getUserDict:NO]];
    }];
}
- (IBAction)reset:(UIButton *)sender {
    [self initFromDateToDate];
    //    [UserPrefConstants singleton].filterOrderBook = @{kSectionTitleDateRange:[self getStartEndDate]};
    [UserPrefConstants singleton].filterOrderBook = @{};
    [self reloadFilter];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kFilterNotification
     object:self userInfo:[self getUserDict:YES]];
    [self popView:YES];
//    [AppDelegateAccessor toogleRightMenu:^{
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:kFilterNotification
//         object:self userInfo:[self getUserDict:YES]];
//    }];
}
- (IBAction)apply:(UIButton *)sender {
    
    [UserPrefConstants singleton].filterOrderBook = [self calculateSectionsToApply];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kFilterNotification
     object:self userInfo:[self getUserDict:YES]];
    
    if([self.delegate respondsToSelector:@selector(didSelectApply)]){
        [self.delegate didSelectApply];
    }
    [self popView:YES];
//    [AppDelegateAccessor toogleRightMenu:^{
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:kFilterNotification
//         object:self userInfo:[self getUserDict:YES]];
//    }];
    
}
- (void)changeButtonState:(UIButton *)button isSelected:(BOOL)isSelected{
    if(isSelected == YES){
        [button setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterSelectedTextColorButton) forState:UIControlStateNormal];
        [button setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterSelectedButtonBg)];
    }else{
        [button setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterUnSelectedTextColorButton) forState:UIControlStateNormal];
        [button setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterUnSelectedButtonBg)];
    }
}
- (NSString *)getTitleAtSection:(NSInteger)section{
    id object = [sections[section] allKeys][0];
    return object;
}
- (NSString *)getValueAtSection:(NSInteger)section atRow:(NSInteger)row getKey:(BOOL)getKey{
    NSArray *items = [sections[section] allValues].firstObject;
    id object = items[row];
    return [MFilterOptions getContentString:object getKey:getKey];
    
}

- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}
#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return sections.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderBookFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderBookFilterCell" forIndexPath:indexPath];
    cell.clv_content.tag = 100 + indexPath.section;
    cell.lbl_title.text = [self getTitleAtSection:indexPath.section];
    cell.lbl_title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterTextHeader);;
    if(cell.clv_content.delegate == nil){
        [cell.clv_content registerNib:[UINib nibWithNibName:[OrderBookFilterItemCell nibName] bundle:nil] forCellWithReuseIdentifier:[OrderBookFilterItemCell reuseIdentifier]];
        [cell.clv_content registerNib:[UINib nibWithNibName:[OrderBookDatePickerCell nibName] bundle:nil] forCellWithReuseIdentifier:[OrderBookDatePickerCell reuseIdentifier]];
        
        cell.clv_content.delegate = self;
        cell.clv_content.dataSource = self;
    }else{
        [cell.clv_content reloadWithAutoSizingCell];
    }
    cell.cst_clvHeight.constant = cell.clv_content.collectionViewLayout.collectionViewContentSize.height;
    return cell;
    
}
#pragma mark UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSArray *items = [sections[collectionView.tag - 100] allValues].firstObject;
    return [items count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = collectionView.tag - 100;
    OrderBookFilterItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[OrderBookFilterItemCell reuseIdentifier] forIndexPath:indexPath];
    [cell updateView];
    [cell.btn_content setTitle:[self getValueAtSection:section atRow:indexPath.row getKey:NO] forState:UIControlStateNormal];
    cell.index = [NSIndexPath indexPathForRow:indexPath.row inSection:section];
    cell.isSelectedFilter = [self isSelected:[self getTitleAtSection:section] value:[self getValueAtSection:section atRow:indexPath.row getKey:YES]];
    cell.delegate = self;
    [cell changeColor];
    return cell;
    
}

#pragma mark OrderBookFilterItemCellDelegate
- (void)didSelect:(NSIndexPath *)index{
    [self calculateSelectOptions:index];
}

#pragma mark OrderBookDatePickerCellDelegate
- (void)changeRange:(NSTimeInterval)startAt endAt:(NSTimeInterval)endAt{
    _startAt = startAt;
    _endAt = endAt;
}

- (void)updateTheme:(NSNotification *)noti{
    self.btn_reset.layer.borderColor = [TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterBorderButton) CGColor];
    self.btn_apply.layer.borderColor = [TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterBorderButton) CGColor];
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterTextHeader);
    self.btn_Close.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_FilterTextColorCell);
    [self changeButtonState:self.btn_apply isSelected:YES];
    [self changeButtonState:self.btn_reset isSelected:NO];
    
    [self.tbl_content reloadWithAutoSizingCell];
}
@end
