//
//  MOrderBookDefine.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/27/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#ifndef MOrderBookDefine_h
#define MOrderBookDefine_h


#endif /* MOrderBookDefine_h */
typedef NS_ENUM(NSUInteger, MOrderBookVCType) {
    MOrderBookVCToday = 0,
    MOrderBookVCHistory
};
typedef NS_ENUM(NSUInteger, MOrderBookDateType) {
    MOrderBookDateTypeStart = 0,
    MOrderBookDateTypeEnd
};

typedef NS_ENUM(NSUInteger, MOrderBookVCFilterOrderType) {
    MOrderBookVCFilterOrderTypeAll = -1,
    MOrderBookVCFilterOrderTypeOpen = 3,
    MOrderBookVCFilterOrderTypeFilled = 1,
    MOrderBookVCFilterOrderTypeActive = 2,
    MOrderBookVCFilterOrderTypeInactive = 4
};

typedef NS_ENUM(NSUInteger, MOrderBookVCSortType) {
    MOrderBookVCSortType_None = 0,
    MOrderBookVCSortType_Code,
    MOrderBookVCSortType_Name,
    MOrderBookVCSortType_Status,
    MOrderBookVCSortType_Action,
    MOrderBookVCSortType_Price,
    MOrderBookVCSortType_Qty,
    MOrderBookVCSortType_Total,
    MOrderBookVCSortType_LastUpdate,
    MOrderBookVCSortType_MQty,
    MOrderBookVCSortType_MPrice
};

typedef NS_ENUM(NSUInteger, MOrderBookVCHeaderType) {
    MOrderBookVCHeaderType_Name = 0,
    MOrderBookVCHeaderType_Price,
    MOrderBookVCHeaderType_Qty,
    MOrderBookVCHeaderType_Status,
    MOrderBookVCHeaderType_MQty,
    MOrderBookVCHeaderType_MPrice
};
#define kDisplayFormatDate      @"dd/MM/yy"
