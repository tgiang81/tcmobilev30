//
//  MOrderBookDetailVC.h
//  TCiPad
//
//  Created by giangtu on 7/25/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "TradeStatus.h"
#import "StockModel.h"
@class AroundShadowView;
@interface MOrderBookDetailVC : BaseVC
@property (nonatomic, strong) StockModel *stockModel;
@property (nonatomic, strong) NSDictionary *dicDetail;
@property (nonatomic, strong) TradeStatus *tradeStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_VolumeTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_reviseButtonTop;
@property (weak, nonatomic) IBOutlet UILabel *lbl_yourOrderValue;

@property (weak, nonatomic) IBOutlet UILabel *lbl_VolumeValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ValueTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbl_ValueValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_HightTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbl_HightValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_OpenTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbl_OpenValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_LowTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbl_LowValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TradeTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbl_TradeValue;
@property (weak, nonatomic) IBOutlet UIImageView *img_status;
@property (weak, nonatomic) IBOutlet UILabel *lbl_yourOrderTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbl_rmkTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_rmkValue;


@property (nonatomic, strong) NSString *remarkString;

@property (weak, nonatomic) IBOutlet AroundShadowView *v_Header;
@property (weak, nonatomic) IBOutlet UIView *v_YourOrder;
@property (assign, nonatomic) BOOL isHistory;
@end
