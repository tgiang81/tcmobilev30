//
//  MOrderBookConfirm.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/14/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN
@protocol MOrderBookConfirmDelegate<NSObject>
@optional
- (void)didSelectCancel;
- (void)didSelectConfirm;
@end
@interface MOrderBookConfirm : BaseVC
@property (weak, nonatomic) id<MOrderBookConfirmDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Content;
@property (weak, nonatomic) IBOutlet UIButton *btn_Cancel;
@property (weak, nonatomic) IBOutlet UIButton *btn_Confirm;

@end

NS_ASSUME_NONNULL_END
