//
//  MOrderBookController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MOrderBookController.h"
#import "NSDate+Utilities.h"
#import "ATPAuthenticate.h"
#import "MFilterOptions.h"
#import "BrokerManager.h"

@interface MOrderBookController()
{
    NSDate *_loadedFromDate, *_loadedToDate;
    
    NSMutableArray *_todayResultList;
    NSMutableArray *_historyResultList;
    NSMutableArray *_filteredResult;
    
    
    ATPAuthenticate *_atp;
    MOrderBookVCSortType _sortType;
    MOrderBookVCSortType _preType;
    
    
    NSArray *filterStatus;
    NSArray *filterExchanges;
    NSArray *filterType;
    NSArray *filterValidity;
    NSArray *filterPayment;
    NSArray *filterStock;
    
    NSSortDescriptor *sortDescriptor;
    
    BOOL isLoadedToday;
    BOOL isLoadedHistory;
}
@end
@implementation MOrderBookController
- (instancetype)init{
    self = [super init];
    [self setupController];
    return self;
}
- (void)setupController{
    _toDate = [NSDate date];
    _fromDate = [[NSDate date] dateByAddingDays:-1];
    
    _historyResultList = [NSMutableArray new];
    _todayResultList = [NSMutableArray new];
    _filteredResult = [NSMutableArray new];
    
    _atp = [ATPAuthenticate singleton];
    
    if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
        [_atp getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount
                        forPaymentCUT:NO andCurrency:@""];
}

- (void)removeLoadData{
    [_atp timeoutActivity];
}
- (BOOL)checkLoadToday{
    return isLoadedToday;
}
- (BOOL)checkLoadHistory{
    if(_loadedFromDate == self.fromDate && self.toDate == _loadedToDate){
        return YES;
    }
    return NO;
}
- (void)loadTodayData{
    [_atp timeoutActivity];
    [_atp getTradeStatus:0 completion:^(NSArray *orderBooks) {
        self->isLoadedToday = YES;
        self->_todayResultList = [NSMutableArray arrayWithArray:orderBooks];
        self->_filteredResult = [NSMutableArray arrayWithArray:orderBooks];
        if([self.delegate respondsToSelector:@selector(didLoadOrderBooks:orderBooks:)]){
            [self.delegate didLoadOrderBooks:MOrderBookVCToday orderBooks:orderBooks];
        }
    }];
}
- (void)loadHistoryData{
    [self loadHistoryData:self.fromDate toDate:self.toDate];
}
- (void)loadHistoryData:(NSDate *)fromDate toDate:(NSDate *)toDate{
    _loadedToDate = toDate;
    _loadedFromDate = fromDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd"];
    NSString *stringFromDate = [formatter stringFromDate:fromDate];
    NSString *stringToDate = [formatter stringFromDate:toDate];
    [_atp timeoutActivity];
    [_atp getTradeHistoryFromDate:stringFromDate toDate:stringToDate completion:^(NSArray *orderBooks) {
        self->isLoadedHistory = YES;
        self->_historyResultList = [NSMutableArray arrayWithArray:orderBooks];
        self->_filteredResult = [NSMutableArray arrayWithArray:orderBooks];
        if([self.delegate respondsToSelector:@selector(didLoadOrderBooks:orderBooks:)]){
            [self.delegate didLoadOrderBooks:MOrderBookVCHistory orderBooks:orderBooks];
        }
    }];
}

- (TradeStatus *)getStatusAt:(NSInteger)index type:(MOrderBookVCType)type{
    return _filteredResult[index];
    
}
- (NSInteger)numberOfItems{
    return _filteredResult.count;
}
- (void)calculateSortType:(MOrderBookVCSortType)type{
    if(_preType == _sortType){
        _preType = MOrderBookVCSortType_None;
    }else{
        _preType = _sortType;
    }
    _sortType = type;
}
- (void)reloadDataAfterSwitchingVCType:(MOrderBookVCType)orderVCType{
    [self sortData:_sortType orderVCType:orderVCType];
}
- (void)actionSort:(MOrderBookVCSortType)type orderVCType:(MOrderBookVCType)orderVCType{
    [self calculateSortType:type];
    [self sortData:type orderVCType:orderVCType];
}
- (void)sortData:(MOrderBookVCSortType)type orderVCType:(MOrderBookVCType)orderVCType{
    if(orderVCType == MOrderBookVCToday){
        _filteredResult = [NSMutableArray arrayWithArray:_todayResultList];
    }else{
        _filteredResult = [NSMutableArray arrayWithArray:_historyResultList];
    }
    if(type == MOrderBookVCSortType_None){
        return;
    }
    _filteredResult = [self sortType:type input:_filteredResult ascending:_preType != _sortType];
}

- (NSString *)getSortedPropertyName:(MOrderBookVCSortType)type{
    
    switch (type) {
        case MOrderBookVCSortType_Code:
            return @"stock_code";
        case MOrderBookVCSortType_Price:
            return @"order_price.doubleValue";
        case MOrderBookVCSortType_Action:
            return @"action";
        case MOrderBookVCSortType_Status:
            return @"statusMode";
        case MOrderBookVCSortType_Name:
            return @"stock_name";
        case MOrderBookVCSortType_Total:
            return @"total.doubleValue";
        case MOrderBookVCSortType_Qty:
            return @"order_quantity.doubleValue";
        case MOrderBookVCSortType_LastUpdate:
            return @"last_update";
        case MOrderBookVCSortType_MQty:
            return @"mt_quantity.doubleValue";
        case MOrderBookVCSortType_MPrice:
            return @"mt_price.doubleValue";
        default:
            return @"";
    }
}
- (NSMutableArray *)sortType:(MOrderBookVCSortType)type input:(NSArray *)input ascending:(BOOL)ascending{
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:[self getSortedPropertyName:type]
                                                 ascending:ascending];
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:[input sortedArrayUsingDescriptors:@[sortDescriptor]]];
    return sortedArray;
}


#pragma mark filter
- (NSMutableArray *)filterOrdersWithExChanges:(NSArray *)exchanges
                                       status:(NSArray *)status
                                        types:(NSArray *)types
                                   validities:(NSArray *)validities
                                 paymentTypes:(NSArray *)paymentTypes
                                 arrayToCheck:(NSMutableArray *)arrayToCheck{
    if(arrayToCheck.count == 0){
        return [NSMutableArray new];
    }
    NSMutableArray *tmpFilterOrders = [NSMutableArray new];
    NSMutableArray *tmpResult = arrayToCheck;
    NSInteger numberOfFields = 5;
    if(numberOfFields > 0){
        for(TradeStatus *ts in tmpResult){
            NSInteger matchedFields = 0;
            
            if([self checkValidExchange:ts exchanges:exchanges]){
                matchedFields++;
            }
            
            if([self checkValidOrderStatus:ts status:status]){
                matchedFields++;
            }
            
            if([self checkValidOrderType:ts type:types]){
                matchedFields++;
            }
            
            if([self checkValidOrderValidities:ts validities:validities]){
                matchedFields++;
            }
            
            if([self checkValidOrderPayment:ts paymentTypes:paymentTypes]){
                matchedFields++;
            }
            
            if(numberOfFields == matchedFields){
                [tmpFilterOrders addObject:ts];
            }
            
            
        }
    }else{
        tmpFilterOrders = arrayToCheck;
    }
    if(sortDescriptor){
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *arrTmp = [tmpFilterOrders sortedArrayUsingDescriptors:sortDescriptors];
        tmpFilterOrders = [[NSMutableArray alloc] initWithArray:arrTmp];
    }
    return tmpFilterOrders;
}
- (BOOL)checkValidExchange:(TradeStatus *)ts exchanges:(NSArray *)exchanges{
    if(exchanges.count == 0){
        return YES;
    }
    for(id name in exchanges){
        if([[[MFilterOptions getContentString:name getKey:YES] lowercaseString] isEqualToString:[ts.exchg_code lowercaseString]]){
            return YES;
        }
    }
    return NO;
}

- (BOOL)checkValidOrderStatusSSI:(TradeStatus *)ts status:(NSArray *)status
{
    if(status.count == 0){
        return YES;
    }
    for(NSString *type in status){
        NSString *sts = ts.status;
        if([sts isEqualToString:type]){
            return YES;
        }
    }
    return NO;
}


- (BOOL)checkValidOrderStatus:(TradeStatus *)ts status:(NSArray *)status
{
    if(status.count == 0){
        return YES;
    }
    for(NSNumber *type in status){
        NSString *sts = ts.status;
        switch([type integerValue]){
            case MOrderBookVCFilterOrderTypeAll:
                return YES;
            case MOrderBookVCFilterOrderTypeOpen:
                if([sts isEqualToString:@"7"] || [sts isEqualToString:@"A"] ||
                   [sts isEqualToString:@"1"] || [sts isEqualToString:@"0"] ||
                   [sts isEqualToString:@"5"] || [sts isEqualToString:@"6"] ||
                   [sts isEqualToString:@"E"]
                   )
                {
                    return YES;
                }
            case MOrderBookVCFilterOrderTypeFilled:
                if([sts isEqualToString:@"1"] || [sts isEqualToString:@"2"] || [sts isEqualToString:@"3"] ||[sts isEqualToString:@"C"] || [sts isEqualToString:@"4"])
                {
                    if ([ts.mt_quantity intValue]>0) {
                        return YES;
                    }
                    
                }
            case MOrderBookVCFilterOrderTypeActive:
                if([sts isEqualToString:@"1"] || [sts isEqualToString:@"0"] ||
                   [sts isEqualToString:@"5"] || [sts isEqualToString:@"6"] ||
                   [sts isEqualToString:@"E"])
                {
                    return YES;
                }
            case MOrderBookVCFilterOrderTypeInactive:
                if([sts isEqualToString:@"3"] || [sts isEqualToString:@"4"] ||
                   [sts isEqualToString:@"8"] || [sts isEqualToString:@"9"] ||
                   [sts isEqualToString:@"C"])
                {
                    return YES;
                }
        }
    }
    return NO;
}
- (BOOL)checkValidOrderType:(TradeStatus *)ts type:(NSArray *)types{
    if(types.count == 0){
        return YES;
    }
    for(NSString *name in types){
        if([[name lowercaseString] isEqualToString:[ts.order_type lowercaseString]]){
            return YES;
        }
    }
    return NO;
}
- (BOOL)checkValidOrderValidities:(TradeStatus *)ts validities:(NSArray *)validities{
    if(validities.count == 0){
        return YES;
    }
    for(NSString *name in validities){
        if([[name lowercaseString] isEqualToString:[ts.validity lowercaseString]]){
            return YES;
        }
    }
    return NO;
}
- (BOOL)checkValidOrderPayment:(TradeStatus *)ts paymentTypes:(NSArray *)paymentTypes{
    if(paymentTypes.count == 0){
        return YES;
    }
    for(NSString *name in paymentTypes){
        if([[name lowercaseString] isEqualToString:[ts.payment_type lowercaseString]]){
            return YES;
        }
    }
    return NO;
}

- (void)filterWithOptions:(MOrderBookVCType)orderVCType{
    if(orderVCType == MOrderBookVCToday){
        _filteredResult = [NSMutableArray arrayWithArray:_todayResultList];
    }else{
        _filteredResult = [NSMutableArray arrayWithArray:_historyResultList];
    }
    [self getFilterData];
    _filteredResult = [self filterOrdersWithExChanges:filterExchanges status:filterStatus types:filterType validities:filterValidity paymentTypes:filterPayment arrayToCheck:_filteredResult];

}
- (void)getFilterData{
    NSDictionary *filterOptions = [UserPrefConstants singleton].filterOrderBook;
    filterStatus = [filterOptions objectForKey:kSectionTitleOrderStatus];
    
    filterExchanges = [filterOptions objectForKey:kSectionTitleExchange];
    
    filterValidity = [filterOptions objectForKey:kSectionTitleValidity];
    
    filterType = [filterOptions objectForKey:kSectionTitleOrderType];
    
    filterStock = [filterOptions objectForKey:kSectionTitleStockTitle];
    
    filterPayment = [filterOptions objectForKey:kSectionTitlePaymentTitle];
}

+ (NSArray *)getFilterStatusOptions{
    //@[@{@"0":@"Queued"}, @{@"1":@"Partially filled"}, @{@"2":@"Filled"}, @{@"3":@"Done for day"}, @{@"4":@"Cancelled"}, @{@"5":@"Replaced"}, @{@"6":@"Pending Cancel"}, @{@"7":@"Pending Release"}, @{@"8":@"Rejected"}, @{@"9":@"Suspended"}, @{@"A":@"Pending Queue"}, @{@"B":@"Calculated"}, @{@"C":@"Expired"}, @{@"D":@"Accepted for bidding"}, @{@"E":@"Pending Replace"}]
    if([BrokerManager shareInstance].broker.tag == BrokerTag_SSI){
        return @[@{@"0":@"Queued"}, @{@"1":@"Partially filled"}, @{@"2":@"Filled"}, @{@"4":@"Cancelled"}, @{@"8":@"Rejected"}];
    }else{
        return @[@{@"1" : @"Filled"}, @{@"2" : @"Active"}, @{@"3" : @"Open"}, @{@"4" : @"Inactive"}];
    }
    
    
    
}
@end
