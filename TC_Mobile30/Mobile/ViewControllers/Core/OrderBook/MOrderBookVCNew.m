//
//  MOrderBookVCNew.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MOrderBookVCNew.h"
#import "UIButton+TCUtils.h"
#import "MOrderBookView.h"
#import "DefineChooseSwift.h"
#import "MOrderBookController.h"
#import "AccountsVC.h"
#import "MOrderBookFilterVC.h"
#import "BaseVC+OrderBook.h"
#import "AppState.h"
@interface MOrderBookVCNew () <WWCalendarTimeSelectorProtocol, MOrderBookViewDelegate, MOrderBookControllerDelegate, MOrderBookFilterVCDelegate>
{
    UserAccountClientData *uacd;
    MOrderBookVCType currentType;
    MOrderBookVCSortType sortType;
    MOrderBookVCSortType preType;
    MOrderBookDateType currentDateType;
    MOrderBookController *_controller;
    BOOL isDerivative;
    BOOL isEdit;
    
    BOOL needRefreshTodayData;
    BOOL needRefreshHistoryData;
}
@end

@implementation MOrderBookVCNew

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createLeftMenu];
    [self addRightNavItems];
    [self setupLanguage];
    _controller = [MOrderBookController new];
    _controller.delegate = self;
    [_v_Content setStartDate:_controller.fromDate];
    [_v_Content setEndDate:_controller.toDate];
    _v_Content.delegate = self;
    [self updateAccountInfo];
    [self loadData:MOrderBookVCToday showLoadingView:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeLimitReceived) name:@"tradeLimitReceived" object:nil];
}
-(void)tradeLimitReceived {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self accountSelected:self->uacd];
        
    });
    
}
- (void)setupLanguage{
    //update language title for all item here
    self.customBarTitle = [LanguageManager stringForKey:@"Order Book"];
}
- (void)addRightNavItems{
    UIButton *filterButton = [UIButton setupNavItem:@"filter_icon"];
    [filterButton addTarget:self action:@selector(filter:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *filterItem = [[UIBarButtonItem alloc] initWithCustomView:filterButton];
    self.navigationItem.rightBarButtonItems = @[filterItem];
}
- (void)filter:(UIButton *)sender {
    MOrderBookFilterVC *_filter = NEW_VC_FROM_NIB([MOrderBookFilterVC class], [MOrderBookFilterVC nibName]);
    _filter.delegate = self;
    [self nextTo:_filter animate:YES];
    //    [AppDelegateAccessor toogleRightMenu:nil];
}
- (void)loadData:(MOrderBookVCType)type showLoadingView:(BOOL)showLoadingView{
    if(type == MOrderBookVCToday){
        if(![_controller checkLoadToday] || needRefreshTodayData){
            needRefreshTodayData = NO;
            if(showLoadingView){
                [[AppState sharedInstance] addLoadingView:self.view];
            }
            [_controller loadTodayData];
        }
        
    }else{
        if(![_controller checkLoadHistory] || needRefreshHistoryData){
            needRefreshHistoryData = NO;
            if(showLoadingView){
                [[AppState sharedInstance] addLoadingView:self.view];
            }
            [_controller loadHistoryData];
        }
        
    }
    
}
- (void)reloadData{
    [[AppState sharedInstance] removeLoadingView:self.view];
    [_v_Content stopRefreshAnimation];
    [_controller removeLoadData];
    [_controller reloadDataAfterSwitchingVCType:currentType];
    [_controller filterWithOptions:currentType];
    [_v_Content reloadData];
}
#pragma mark - TradeAccount Delegate

-(void)accountSelected:(UserAccountClientData *)account {
    // update account details in trader
    [self updateTradingAccount:account];
    
}
-(void)updateTradingAccount:(UserAccountClientData*)account {
    [UserPrefConstants singleton].userSelectedAccount = account;
    [self updateAccountInfo];
}
- (void)updateAccountInfo{
    dispatch_async(dispatch_get_main_queue(), ^{
        self->uacd = [UserPrefConstants singleton].userSelectedAccount;
        [self.v_Content setAccountInfo:self->uacd];
    });
}


- (void)showDatePicker{
    CGSize defaultSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.7 * SCREEN_HEIGHT_PORTRAIT);
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:nil];
    WWCalendarTimeSelector *datePicker = (WWCalendarTimeSelector *)[sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    datePicker.delegate = self;
    //setColor
    if(currentDateType == MOrderBookDateTypeStart){
        datePicker.optionTopPanelTitle = @"Start Date";
        datePicker.optionCurrentDate = _controller.fromDate;
    }else{
        datePicker.optionTopPanelTitle = @"End Date";
        datePicker.optionCurrentDate = _controller.toDate;
    }
    
    datePicker.dateFormat = kCALENDARPICKER_FORMAT_DATE;
    datePicker.optionTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].dp_tintColor);
    datePicker.optionCalendarFontColorToday = TC_COLOR_FROM_HEX([ThemeManager shareInstance].dp_today);
    datePicker.optionEnableCallDoneWhenSelectDate = FALSE;
    datePicker.optionButtonShowCancel = TRUE;
    datePicker.optionStyles.showDateMonth = TRUE;
    datePicker.optionStyles.showTime = FALSE;
    datePicker.optionStyles.showMonth = FALSE;
    
    datePicker.optionButtonShowBottomActions = TRUE;
    datePicker.optionSelectorPanelOffsetHighlightDate = 0;
    [self showPopupWithContent:datePicker inSize:defaultSize completion:^(MZFormSheetController *formSheetController) {
        
    }];
}
- (void)showAccountVC{
    AccountsVC *accountVC = NEW_VC_FROM_NIB([AccountsVC class], [AccountsVC nibName]);
    accountVC.CallBackValue = ^(UserAccountClientData *account) {
        self->needRefreshTodayData = YES;
        self->needRefreshHistoryData = YES;
        [self loadData:self->currentType showLoadingView:YES];
        [self updateTradingAccount:account];
    };
    
    [self nextTo:accountVC animate:YES];
}
#pragma mark DatePickerDelegate
-(void) WWCalendarTimeSelectorDone:(WWCalendarTimeSelector *)selector date:(NSDate *)date
{
    [self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
        if(self->currentDateType == MOrderBookDateTypeStart){
            [self.v_Content setStartDate:date];
            self->_controller.fromDate = date;
        }else{
            [self.v_Content setEndDate:date];
            self->_controller.toDate = date;
        }
        [self loadData:MOrderBookVCHistory showLoadingView:YES];
    }];
    
}

- (void)WWCalendarTimeSelectorCancel:(WWCalendarTimeSelector *)selector date:(NSDate *)date{
    [selector dismissPopupWithCompletion:nil];
}


#pragma mark MOrderBookViewDelegate
- (TradeStatus *)getTradeStatus:(NSInteger)index{
    return [_controller getStatusAt:index type:currentType];
}
- (NSInteger)numberOfItems{
    return [_controller numberOfItems];
}

- (void)didSelectStatusAt:(NSInteger)index{
    TradeStatus *tradeStatus = [_controller getStatusAt:index type:currentType];
    [self showOrderBookDetail:tradeStatus type:currentType];
}

- (void)didSelectStartDate{
    currentDateType = MOrderBookDateTypeStart;
    [self showDatePicker];
}
- (void)didSelectEndDate{
    currentDateType = MOrderBookDateTypeEnd;
    [self showDatePicker];
}
- (void)didSelectAccountView{
    [self showAccountVC];
}
- (void)refreshData{
    [self loadData:currentType showLoadingView:NO];
}
- (void)didChangeOrderBookType:(MOrderBookVCType)type{
    currentType = type;
    [self reloadData];
    [self loadData:type showLoadingView:YES];
}
- (void)didSelectName{
    [_controller actionSort:MOrderBookVCSortType_Name orderVCType:currentType];
    [self reloadData];
}
- (void)didSelectPrice{
    [_controller actionSort:MOrderBookVCSortType_Price orderVCType:currentType];
    [self reloadData];
}
- (void)didSelectStatus{
    [_controller actionSort:MOrderBookVCSortType_Status orderVCType:currentType];
    [self reloadData];
}
#pragma mark MOrderBookControllerDelegate
- (void)didLoadOrderBooks:(MOrderBookVCType)type orderBooks:(NSArray *)orderBooks{
    [self reloadData];
}

#pragma mark MOrderBookFilterVCDelegate
- (void)didSelectApply{
    [self reloadData];
}
@end
