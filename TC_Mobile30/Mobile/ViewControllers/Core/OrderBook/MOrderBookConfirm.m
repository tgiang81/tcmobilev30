//
//  TCCheckAuthVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/14/19.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MOrderBookConfirm.h"
#import "UIViewController+SetupButton.h"
@interface MOrderBookConfirm ()
{
    CGFloat defaultCornerRadius;
}
@end

@implementation MOrderBookConfirm

- (void)viewDidLoad {
    [super viewDidLoad];
    defaultCornerRadius = 4;
    self.enableTapDismissKeyboard = YES;
    
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_textColor);
    self.lbl_Content.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_textColor);
    
    
    [self setupButton:self.btn_Cancel corner:defaultCornerRadius borderColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor) bgColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_cancelColor) textColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor)];
    [self setupButton:self.btn_Confirm corner:defaultCornerRadius borderColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor) bgColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_tintColor) textColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor)];
}

- (IBAction)cancel:(id)sender {
    [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
        if([self.delegate respondsToSelector:@selector(didSelectCancel)]){
            [self.delegate didSelectCancel];
        }
    }];
}
- (IBAction)confirm:(id)sender {
    [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
        if([self.delegate respondsToSelector:@selector(didSelectConfirm)]){
            [self.delegate didSelectConfirm];
        }
    }];
    
}


@end
