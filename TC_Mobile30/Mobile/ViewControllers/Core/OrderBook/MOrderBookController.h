//
//  MOrderBookController.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOrderBookDefine.h"
@class TradeStatus;
NS_ASSUME_NONNULL_BEGIN
@protocol MOrderBookControllerDelegate<NSObject>
- (void)didLoadOrderBooks:(MOrderBookVCType)type orderBooks:(NSArray *)orderBooks;
@end
@interface MOrderBookController : NSObject
@property(weak, nonatomic) id<MOrderBookControllerDelegate> delegate;
@property(strong, nonatomic) NSDate *fromDate;
@property(strong, nonatomic) NSDate *toDate;

- (void)loadHistoryData:(NSDate *)fromDate toDate:(NSDate *)toDate;
- (void)loadTodayData;
- (void)loadHistoryData;
- (TradeStatus *)getStatusAt:(NSInteger)index type:(MOrderBookVCType)type;
- (NSInteger)numberOfItems;
- (void)calculateSortType:(MOrderBookVCSortType)type;
- (void)actionSort:(MOrderBookVCSortType)type orderVCType:(MOrderBookVCType)orderVCType;
- (void)reloadDataAfterSwitchingVCType:(MOrderBookVCType)orderVCType;
- (void)removeLoadData;
- (void)filterWithOptions:(MOrderBookVCType)orderVCType;

- (BOOL)checkLoadToday;
- (BOOL)checkLoadHistory;

+ (NSArray *)getFilterStatusOptions;
@end

NS_ASSUME_NONNULL_END
