//
//  MOrderBookDateView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

NS_ASSUME_NONNULL_BEGIN
@protocol MOrderBookDateViewDelegate<NSObject>
- (void)didSelectStartDate;
- (void)didSelectEndDate;
@end
@interface MOrderBookDateView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UIButton *btn_StartDate;
@property (weak, nonatomic) IBOutlet UIButton *btn_EndDate;
@property (weak, nonatomic) IBOutlet UIView *v_Seperator;
@property (weak, nonatomic) id<MOrderBookDateViewDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
