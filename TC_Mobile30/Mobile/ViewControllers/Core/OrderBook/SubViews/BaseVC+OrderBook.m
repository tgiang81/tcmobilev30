//
//  BaseVC+OrderBook.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/28/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC+OrderBook.h"
#import "TradeStatus.h"
#import "MOrderBookDetailVC.h"
#import "QCData.h"
#import "NSNumber+Formatter.h"
#import "UIViewController+OrderDetailVC.h"

@implementation BaseVC (OrderBook)

-(void)showOrderBookDetail:(TradeStatus*)ts type:(MOrderBookVCType)type {
    // header is stock data
    NSMutableDictionary *mutStockData = [NSMutableDictionary new];
    int qtyPoint = 3;
    int changePoint = 3;
    NSDictionary *stkDict = [[NSDictionary alloc] initWithDictionary:[[[QCData singleton] qcFeedDataDict] objectForKey:ts.stock_code] copyItems:YES];
    if([BrokerManager shareInstance].broker.tag == BrokerTag_SSI){
        mutStockData[@"stkName"] = ts.stock_code;
        StockModel *tmpStock = [Utils stockQCFeedDataFromCode:ts.stock_code];
        if(tmpStock){
            mutStockData[@"stkCode"] = tmpStock.compName;
        }else{
            mutStockData[@"stkCode"] = ts.stock_name;
        }
        
    }else{
        mutStockData[@"stkName"] = ts.stock_name;
        mutStockData[@"stkCode"] = ts.stock_code;
    }
    
    
    mutStockData[@"lastDone"] = [[NSNumber numberWithFloat:[[stkDict objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue]] toCurrencyNumber];
    
    CGFloat chg = [[stkDict objectForKey:FID_CUSTOM_F_CHANGE] floatValue];
    CGFloat chgPer = [[stkDict objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue];
    
    
    
    mutStockData[@"chg"] = [NSNumber numberWithFloat:chg];
    if (chg>0) {
        mutStockData[@"change"] = [NSString stringWithFormat:@"+%@ (+%.2f%%)",[[NSNumber numberWithFloat:chg] toDecimaWithPointer:changePoint],chgPer];
    } else{
        mutStockData[@"change"] = [NSString stringWithFormat:@"%@ (%.2f%%)",[[NSNumber numberWithFloat:chg] toDecimaWithPointer:changePoint],chgPer];
    }
    
    // get all ts data and put in _detailsArray and reload detailtable
    NSMutableArray *_detailsArray = [NSMutableArray new];
    
    // Format data
    NSString *ordPrice = [[NSNumber numberWithFloat: ts.order_price.floatValue] toDecimaWithPointer:qtyPoint];
    // this is special rule
    if ([ts.order_type isEqualToString:@"Market"]||[ts.order_type isEqualToString:@"MarketToLimit"]) {
        if (ts.order_price.floatValue<=0) {
            ordPrice = @"-";
        }
    }

    NSString *validityStr = [LanguageManager stringForKey:ts.validity];
    if ([ts.validity isEqualToString:@"GTD"]&&([ts.expiry length]>=8)) {
        //NSLog(@"expiry %@",ts.expiry); eg: 20170620170000.000
        
        NSString *dateStr = [ts.expiry substringToIndex:8];
        // NSLog(@"dateStr %@", dateStr);
        
        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
        [ndf setDateFormat:@"yyyyMMdd"];
        NSDate *date = [ndf dateFromString:dateStr];
        [ndf setDateFormat:@"dd/MM/yyyy"];
        
        validityStr = [NSString stringWithFormat:@"%@ (%@)", validityStr,[ndf stringFromDate:date]];
        
    }
    _detailsArray = [self getDetailData:ts];
    
    MOrderBookDetailVC * orderBookDetailVC = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [MOrderBookDetailVC storyboardID]);
    StockModel *model = [Utils stockQCFeedDataFromCode:ts.stock_code];
    if(model == nil){
        model = [[StockModel alloc] initStockFrom:ts];
    }
    orderBookDetailVC.stockModel = model;
    orderBookDetailVC.tradeStatus = ts;
    orderBookDetailVC.dicDetail = @{
                                    @"stkHeader" : mutStockData,
                                    @"arrDetail": _detailsArray
                                    };
    orderBookDetailVC.remarkString = ts.message;
    orderBookDetailVC.isHistory = type == MOrderBookVCHistory;
    
    [self presentWithNav:orderBookDetailVC animate:YES];
    
}
- (UIColor *)getColor:(OrderStatusType)type{
    switch (type) {
        case OrderStatus_Reject:
            return UIColor.redColor;
        case OrderStatus_Cancel:
            return UIColor.redColor;
        case OrderStatus_Queue:
            return UIColor.yellowColor;
        case OrderStatus_PartiallyMatched:
            return UIColor.yellowColor;
        case OrderStatus_Matched:
            return UIColor.greenColor;
        case OrderStatus_PartiallyMatchedAndCanceled:
            return UIColor.greenColor;
        case OrderStatus_Expired:
            return UIColor.grayColor;
        default:
            return UIColor.blackColor;
    }
}
@end
