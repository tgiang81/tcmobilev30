//
//  MOrderBookSelectAccountView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"
@class UserAccountClientData;
NS_ASSUME_NONNULL_BEGIN
@protocol MOrderBookSelectAccountViewDelegate <NSObject>
- (void)didSelectAccountView;
@end
@interface MOrderBookSelectAccountView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UIButton *btn_Tint;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AccountDisplay;
@property (weak, nonatomic) id<MOrderBookSelectAccountViewDelegate> delegate;
- (void)setDisplayAccount:(UserAccountClientData *)uacd;
@end

NS_ASSUME_NONNULL_END
