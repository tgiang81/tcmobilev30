//
//  MOrderBookDateView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MOrderBookDateView.h"
#import "ThemeManager.h"
@implementation MOrderBookDateView
- (void)awakeFromNib{
    [super awakeFromNib];
    self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_bgTop3);
    self.v_Seperator.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_seperatorButtons);
    self.btn_StartDate.layer.cornerRadius = 16;
    self.btn_StartDate.layer.borderWidth = 1;
    self.btn_EndDate.layer.cornerRadius = 16;
    self.btn_EndDate.layer.borderWidth = 1;
    
    self.btn_StartDate.layer.borderColor = [TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_borderColor) CGColor];
    self.btn_EndDate.layer.borderColor = [TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_borderColor) CGColor];
    
    self.btn_StartDate.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    self.btn_EndDate.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);

    [self.btn_StartDate setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_DateTextColor) forState:UIControlStateNormal];
    [self.btn_EndDate setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_DateTextColor) forState:UIControlStateNormal];
}
- (IBAction)onTapStartDate:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectStartDate)]){
        [self.delegate didSelectStartDate];
    }
}

- (IBAction)onTapEndDate:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectEndDate)]){
        [self.delegate didSelectEndDate];
    }
}

@end
