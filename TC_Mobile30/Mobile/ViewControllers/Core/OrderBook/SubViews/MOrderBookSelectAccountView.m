//
//  MOrderBookSelectAccountView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MOrderBookSelectAccountView.h"
#import "ThemeManager.h"
#import "UserPrefConstants.h"
@implementation MOrderBookSelectAccountView
- (void)awakeFromNib{
    [super awakeFromNib];
    self.lbl_AccountDisplay.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    self.btn_Tint.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    self.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_bgTop1);
}
- (IBAction)action:(id)sender {
    if([self.delegate respondsToSelector:@selector(didSelectAccountView)]){
        [self.delegate didSelectAccountView];
    }
}
- (void)setDisplayAccount:(UserAccountClientData *)uacd{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UserPrefConstants singleton].CreditLimitOrdBook) {
            self.lbl_AccountDisplay.text  = uacd.display_name;
        }
        else {
            NSString *accountName = [NSString stringWithFormat:@"%@-%@", uacd.broker_branch_code, uacd.client_account_number];
            self.lbl_AccountDisplay.text = accountName;
        }
    });
}

@end
