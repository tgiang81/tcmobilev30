//
//  BaseVC+OrderBook.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 3/28/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "MOrderBookDefine.h"
typedef enum {
    OrderStatus_Reject,
    OrderStatus_Cancel,
    OrderStatus_Queue,
    OrderStatus_PartiallyMatched,
    OrderStatus_Matched,
    OrderStatus_PartiallyMatchedAndCanceled,
    OrderStatus_Expired,
    OrderStatus_Other
} OrderStatusType;
@class TradeStatus;
NS_ASSUME_NONNULL_BEGIN

@interface BaseVC (OrderBook)
-(void)showOrderBookDetail:(TradeStatus*)ts type:(MOrderBookVCType)type;
- (NSString *)getSortedPropertyName:(MOrderBookVCSortType)type;
- (NSMutableArray *)sortType:(MOrderBookVCSortType)type input:(NSArray *)input ascending:(BOOL)ascending;
- (UIColor *)getColor:(OrderStatusType)type;
@end

NS_ASSUME_NONNULL_END
