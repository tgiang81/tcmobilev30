//
//  MOrderBookDetailVC.m
//  TCiPad
//
//  Created by giangtu on 7/25/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MOrderBookDetailVC.h"
#import "DetailsTableViewCell.h"
#import "MTradeViewController.h"
#import "TradingRules.h"
#import "UserPrefConstants.h"
#import "StockModel.h"
#import "StockInfoVC.h"
#import "NSNumber+Formatter.h"
#import "UIViewController+OrderDetailVC.h"
#import "AroundShadowView.h"
#import "LanguageKey.h"
#import "MStockInfoVC.h"


static NSString *kICON_UP_PRICE_NAME    = @"up_price_icon";
static NSString *kICON_DOWN_PRICE_NAME    = @"down_price_icon";
@interface MOrderBookDetailVC () <UITableViewDelegate>
{
    // Details
    
    NSArray *detailsArray;
//    IBOutlet UILabel *lbl_RemarkContent;
    IBOutlet UILabel *detailStkCode;
    IBOutlet UILabel *detailStkName;
    IBOutlet UILabel *detailLastDone;
    IBOutlet UILabel *detailChange;
    
    IBOutlet UITableView *detailsTableview;

    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *reviseButton;
    IBOutlet UIButton *buyButton;
    IBOutlet UIButton *sellButton;
    IBOutlet UIButton *copyButton;
    
}
@end

@implementation MOrderBookDetailVC
- (void)checkShowButtons{
    BOOL hidebutton = NO;
    
    if ([_tradeStatus.status isEqualToString:@"9"] ||
        [_tradeStatus.status isEqualToString:@"4"] ||
        [_tradeStatus.status isEqualToString:@"8"] ||
        [_tradeStatus.status isEqualToString:@"2"] ||
        [_tradeStatus.status isEqualToString:@"C"]) {
        hidebutton = YES;
        self.cst_reviseButtonTop.constant = -(8 + cancelButton.frame.size.height + reviseButton.frame.size.height);
    }
    cancelButton.hidden = hidebutton;
    reviseButton.hidden = hidebutton;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
//    if(self.isHistory){
//        [self hideActionButtons];
//    }
    [self checkShowButtons];
    
    
    self.lbl_rmkTitle.text = [LanguageManager stringForKey:Remark];
    [buyButton setTitle:[LanguageManager stringForKey:Copy_Order] forState:UIControlStateNormal];
    [reviseButton setTitle:[LanguageManager stringForKey:REVISE] forState:UIControlStateNormal];
    [cancelButton setTitle:[LanguageManager stringForKey:CANCEL] forState:UIControlStateNormal];
    self.customBarTitle = [LanguageManager stringForKey:Order_Details];

    detailStkCode.text = [_dicDetail[@"stkHeader"][@"stkCode"] stringByDeletingPathExtension];
    detailStkName.text = _dicDetail[@"stkHeader"][@"stkName"];
    detailChange.text = _dicDetail[@"stkHeader"][@"change"];
    detailLastDone.text = _dicDetail[@"stkHeader"][@"lastDone"];
    
    CGFloat chg =  [_dicDetail[@"stkHeader"][@"chg"] floatValue];
    
    if (chg>0) {
//        self.img_status.image = [UIImage imageNamed:kICON_UP_PRICE_NAME];
        [detailChange setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
        [detailLastDone setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
    } else if (chg<0) {
//        self.img_status.image = [UIImage imageNamed:kICON_DOWN_PRICE_NAME];
        [detailChange setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
        [detailLastDone setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
    } else {
//        self.img_status.image = [UIImage imageNamed:@""];
        [detailChange setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
        [detailLastDone setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
    }
    
    detailsArray = _dicDetail[@"arrDetail"];
    
    
    detailsTableview.estimatedRowHeight =250.0;
    detailsTableview.rowHeight = UITableViewAutomaticDimension;
    
    [detailsTableview reloadData];
    [self showStockInfo];
}
- (void)showStockInfo{
    self.lbl_VolumeValue.text = [self.stockModel.volume toCurrencyNumber];
    self.lbl_ValueValue.text = [self.stockModel.dValue toCurrencyNumber];
    self.lbl_HightValue.text = [self.stockModel.highPrice toCurrencyNumber];
    self.lbl_OpenValue.text = [self.stockModel.openPrice toCurrencyNumber];
    self.lbl_LowValue.text = [self.stockModel.lowPrice toCurrencyNumber];
    self.lbl_TradeValue.text = [self.stockModel.trade toCurrencyNumber];
}
- (void)createUI{
    cancelButton.layer.cornerRadius = 4;
    reviseButton.layer.cornerRadius = 4;
    buyButton.layer.cornerRadius = 4;
    sellButton.layer.cornerRadius = 4;
    copyButton.layer.cornerRadius = 4;
    _lbl_rmkValue.text = self.remarkString;
    self.lbl_yourOrderValue.text = self.tradeStatus.order_number;
    [self setBackButtonDefault];
    [self setupLanguage];
}
- (void)setupLanguage{
    [super setupLanguage:nil];
    
    //setlanguageV3
    [cancelButton setTitle:[LanguageManager stringForKey:Cancel] forState:UIControlStateNormal];
    [reviseButton setTitle:[LanguageManager stringForKey:Revise] forState:UIControlStateNormal];
    [buyButton setTitle:[LanguageManager stringForKey:Buy] forState:UIControlStateNormal];
    [sellButton setTitle:[LanguageManager stringForKey:Sell] forState:UIControlStateNormal];
    [copyButton setTitle:[LanguageManager stringForKey:Copy_Order]forState:UIControlStateNormal];
    
    //setColor
//    [self.view setBackgroundColor:COLOR_FROM_HEX(0x374D5D)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)hideActionButtons{
    [cancelButton setHidden:YES];
    [reviseButton setHidden:YES];
    [buyButton setHidden:YES];
    [sellButton setHidden:YES];
}
-(void)setDetailNoData {
    [self hideActionButtons];
    
    
    detailLastDone.text = @"";
    detailStkCode.text = @"";
    detailStkName.text = @"";
    detailChange.text = @"";
}

#pragma mark - Table View Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  UITableViewAutomaticDimension;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == detailsTableview) {
        
        if ([detailsArray count]>0) {
            tableView.backgroundView = nil;
//            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
            return 1;
        } else {
            
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            
            messageLabel.text = [LanguageManager stringForKey:No_Results__];
            messageLabel.textColor = [UIColor whiteColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            [messageLabel sizeToFit];
            tableView.backgroundView = messageLabel;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            
            [self setDetailNoData];
            
            
            return 0;
        }
        
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [detailsArray count];
    
}


// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"DetailCellID";
    DetailsTableViewCell *cell = (DetailsTableViewCell *) [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil)
    {
        cell = [[DetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    cell = [self setupDateToCell:cell detailArray:[NSMutableArray arrayWithArray:detailsArray] indexPath:indexPath];
    return cell;
}



//Mark:- button clicked
- (StockModel *)getStockModel:(NSString *)stockCode{
    return [Utils stockQCFeedDataFromCode:stockCode];
}
- (void)showStockDetail{
	/*
    StockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [StockInfoVC storyboardID]);
    _detailVC.stock = [self getStockModel:_tradeStatus.stock_code];
    [self presentWithNav:_detailVC animate:YES];
	*/
	MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
	_detailVC.stock = [self getStockModel:_tradeStatus.stock_code];
	[self nextTo:_detailVC animate:YES];
	
}
- (IBAction)showStockDetail:(UIButton *)sender {
    [self showStockDetail];
}


- (IBAction)buyorderBtnPressed:(UIButton *)sender
{
    [self executeTrade:10];
}


- (IBAction)sellorderBtnPressed:(UIButton *)sender
{
    [self executeTrade:11];
}

-(IBAction)reviseOrderBtnPressed:(UIButton *)sender
{
    [self executeTrade:12];
}


-(IBAction)cancelOrderBtnPressed:(UIButton *)sender{
    [self executeTrade:13];
}
- (IBAction)copyOrder:(id)sender {
    [self executeTrade:99];
}


-(void)executeTrade:(int)option {
    if(option == 12 || option == 13){
        NSString *type = @"cancel";
        if(option == 12){
            type = @"revise";
        }
        [self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:[NSString stringWithFormat:@"Do you want to %@ this order?", type]] withOKAction:^{
            [self actionWith:option];
        } cancelAction:^{
            //Do nothing
        }];
    }else{
        [self actionWith:option];
    }
}
- (void)actionWith:(int)option{
    if(option == 99 ){ //copy order
        if([[self getValueFromDictWith:@"Action"] isEqualToString:@"Buy"]){
            option = 10;
        }else{
            option = 11;
        }
    }
    
    NSString * _stkCode = _dicDetail[@"stkHeader"][@"stkCode"];
    NSString * _stkName = _dicDetail[@"stkHeader"][@"stkName"];
    
    NSArray *stkCodeAndExchangeArr= [_stkCode componentsSeparatedByString:@"."];
    TradingRules *trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
    
    
    
    NSDictionary *dict = [NSDictionary new];
    
    CGFloat defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_88_D_SELL_PRICE_1] floatValue];
    
    //NSLog(@"b4 %f",defPrice);
    if (defPrice<=0) {
        defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
        //  NSLog(@"zero %f",defPrice);
    }
    
    switch (option) {
        case 10: // BUY
        {
            if ([UserPrefConstants singleton].pointerDecimal==3) {
                dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                        @"Buy",@"Action",
                        
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
                        [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName] ,@"stkNname",
                        trdRules, @"TradeRules",
                        nil];
            }else{
                dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                        @"Buy",@"Action",
                        
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
                        [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName],@"stkNname",
                        trdRules, @"TradeRules",
                        nil];
            }
            
            
        }
            break;
        case 11: //SELL
        {
            if ([UserPrefConstants singleton].pointerDecimal==3) {
                dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                        @"Sell",@"Action",
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
                        [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName],@"stkNname",
                        trdRules, @"TradeRules",
                        nil];
            }else{
                dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                        @"Sell",@"Action",
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
                        [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName],@"stkNname",
                        trdRules, @"TradeRules",
                        nil];
            }
            
            
        }
            break;
            
        case 12://REVISE
        {
            
            [UserPrefConstants singleton].callBuySellReviseCancel = 3;
            
            NSNumber *defPrice = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"];
            if (defPrice==nil) defPrice = [NSNumber numberWithFloat:0.0];
            
            if ([UserPrefConstants singleton].pointerDecimal==3) {
                dict = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"Revise",@"Action",
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                        [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                        defPrice,@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName],@"stkNname",
                        trdRules, @"TradeRules",
                        self.tradeStatus, @"TradeStatus",
                        nil];
            }else{
                dict = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"Revise",@"Action",
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                        [NSString stringWithFormat:@"%.4f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                        defPrice,@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName],@"stkNname",
                        trdRules, @"TradeRules",
                        self.tradeStatus, @"TradeStatus",
                        nil];
            }
            
        }
            break;
        case 13: //CANCEL
        {
            [UserPrefConstants singleton].callBuySellReviseCancel = 3;
            
            trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
            
            NSNumber *defPrice = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"];
            if (defPrice==nil) defPrice = [NSNumber numberWithFloat:0.0];
            
            if ([UserPrefConstants singleton].pointerDecimal==3) {
                
                dict = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"Cancel",@"Action",
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                        [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                        defPrice,@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName],@"stkNname",
                        trdRules, @"TradeRules",
                        self.tradeStatus, @"TradeStatus",
                        nil];
            }else{
                dict = [NSDictionary dictionaryWithObjectsAndKeys:
                        @"Cancel",@"Action",
                        _stkCode,@"StockCode",
                        _stkName,@"StockName",
                        [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                        [NSString stringWithFormat:@"%.4f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                        defPrice,@"DefaultPrice",
                        [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stkName],@"stkNname",
                        trdRules, @"TradeRules",
                        self.tradeStatus, @"TradeStatus",
                        nil];
            }
            
        }
            break;
            
        default:
            break;
    }
    MTradeViewController * tradeVC = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
    tradeVC.isOrderBook = YES;
    tradeVC.stock = self.stockModel;
    tradeVC.tradeObject = [Utils getTradeObjectOrderType:[self getValueFromDictWithControlType:CONTROL_ORDERTYPE] trigger:[self getValueFromDictWithControlType:CONTROL_TRIGGER] triggerPrice:[self getValueFromDictWithControlType:CONTROL_TRIGGERPRICE] direction:[self getValueFromDictWithControlType:CONTROL_DIRECTION] disclosed:[self getValueFromDictWithControlType:CONTROL_DISCLOSED] quantity:[self getValueFromDictWithControlType:CONTROL_QUANTITY] minquantity:[self getValueFromDictWithControlType:CONTROL_MINQUANTITY] price:[self getValueFromDictWithControlType:CONTROL_PRICE] statementCurrency:[self getValueFromDictWithControlType:CONTROL_STATEMENTCURRENCY] validity:[self getValueFromDictWithControlType:CONTROL_VALIDITY] paymentType:[self getValueFromDictWithControlType:CONTROL_PAYMENTTYPE] shortSell:@"" skipConfirmation:@"" gross:[self getValueFromDictWith:@"Order Value"]];
    if(option == 10){
        tradeVC.tradeType = TradeType_Buy;
    }
    else if(option == 11){
        tradeVC.tradeType = TradeType_Sell;
    }
    else if(option == 12){
        tradeVC.tradeType = TradeType_Revise;
    }
    else if(option == 13){
        tradeVC.willCancelOrder = YES;
        tradeVC.tradeType = TradeType_Cancel;
    }
    
    tradeVC.actionTypeDict = dict;
    [self.navigationController pushViewController:tradeVC animated:YES];
}
- (NSString *)getValueFromDictWithControlType:(CONTROL_INPUTFIELD_TYPE)controlType{
    for(NSDictionary *obj in detailsArray){
        if([[obj valueForKey:@"control_type"] integerValue] == controlType){
            return [obj valueForKey:@"Content"];
        }
    }
    return @"";
}
- (NSString *)getValueFromDictWith:(NSString *)key{
    for(NSDictionary *obj in detailsArray){
        if([[obj valueForKey:@"Title"] isEqualToString:key]){
            return [obj valueForKey:@"Content"];
        }
    }
    return @"";
}

- (void)updateTheme:(NSNotification *)noti{
    [super updateTheme:noti];
    self.v_Header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topBg);
    detailStkCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockCompanyText  );
    detailStkName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    detailLastDone.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    _lbl_yourOrderValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    detailChange.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    
    _lbl_VolumeValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topTextColor);
    _lbl_ValueValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topTextColor);
    _lbl_HightValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topTextColor);
    _lbl_OpenValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topTextColor);
    _lbl_LowValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topTextColor);
    _lbl_TradeValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topTextColor);
    
    
    
    _lbl_VolumeTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    _lbl_ValueTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    _lbl_HightTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    _lbl_OpenTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    _lbl_LowTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    _lbl_TradeTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_topHightLightTextColor);
    _lbl_yourOrderTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_HightLightTextColor);
    _lbl_rmkTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_HightLightTextColor);
    _lbl_rmkValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_TextColor);
    
    
    copyButton.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonBg);
    cancelButton.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_SpecialButtonBg);
    reviseButton.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonBg);
    buyButton.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonBg);
    sellButton.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonBg);
    
    [cancelButton setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_SpecialButtonTextColor) forState:UIControlStateNormal];
    [copyButton setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonTextColor) forState:UIControlStateNormal];
    [reviseButton setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonTextColor) forState:UIControlStateNormal];
    [buyButton setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonTextColor) forState:UIControlStateNormal];
    [sellButton setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonTextColor) forState:UIControlStateNormal];
    

    
}
@end
