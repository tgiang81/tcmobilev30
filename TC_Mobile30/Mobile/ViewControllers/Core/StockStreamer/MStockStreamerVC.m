//
//  MStockStreamerVC.m
//  TCiPad
//
//  Created by n2nconnect on 03/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MStockStreamerVC.h"
#import "VertxConnectionManager.h"
#import "StockTicker.h"
#import "ATPAuthenticate.h"
#import "StockStreamer.h"
#import "StockStreamerCell.h"
#import "StockStreamerView.h"
#import "LanguageKey.h"
//#define MAX_LINES 13
@interface MStockStreamerVC () <UITableViewDelegate, UITableViewDataSource>
{
    VertxConnectionManager *vertxConnectionManager;
    ATPAuthenticate *atp;
    int leftCount;
    NSMutableArray *watchListArr;
    NSArray *filterByArray;
    int valueCount;
    int quantityCount;
    int numberOfLine;
    int increaseMultiple;
    BOOL leftDirection;
    NSLayoutConstraint *constraintWidthTable1;
    NSMutableArray *leftStreamerArray;
    
    UITableViewController *popupTableViewController;
    UITableViewController *popupFilterTableViewController;
    
    UIPopoverController *columnPickerPopover;
    int favID;
    int MAX_LINES;
    CGFloat rowHeight;
    //    IBOutlet UITableView *rightStreamerView;
    
    
}
@property (weak, nonatomic) IBOutlet StockStreamerView *stockView;
@property (weak, nonatomic) IBOutlet StockStreamerView *stockView2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_stockViewWidth;

@property (nonatomic, weak) IBOutlet UILabel *watchListLabel;
@property (nonatomic, weak) IBOutlet UILabel *filterByLabel;

@end

@implementation MStockStreamerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotated:) name:UIDeviceOrientationDidChangeNotification object:nil];
    [self restrictRotation:NO];
    [self createLeftMenu];
    self.customBarTitle = [LanguageManager stringForKey:@"Tracker Record"];
    
    
    
    
    
    //    popupTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    //    popupTableViewController.tableView.delegate=self;
    //    popupTableViewController.tableView.dataSource=self;
    //    [popupTableViewController.tableView setTag:1];
    //
    //    popupFilterTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    //    popupFilterTableViewController.tableView.delegate=self;
    //    popupFilterTableViewController.tableView.dataSource=self;
    //    [popupFilterTableViewController.tableView setTag:2];
    //
    leftStreamerArray = [[NSMutableArray alloc] init];
    
    vertxConnectionManager = [VertxConnectionManager singleton];
    
    //    [vertxConnectionManager vertxGetMarketStreamer:nil andValue:0 andQuantity:0 andCondition:0];
    
    [vertxConnectionManager vertxGetMarketStreamer:nil andValue:100000 andQuantity:0 andCondition:0];
    
    
    
    atp = [ATPAuthenticate singleton];
    
    filterByArray = [[NSArray alloc] initWithObjects:@"Value >= 100K",@"Vol >= 30K",@"Vol >= 30K && Value >= 100K",@"Vol >= 30K && Value >= 30K",nil];
    
    
    [NSTimer scheduledTimerWithTimeInterval: 1.0
                                     target: self
                                   selector: @selector(handleTimer)
                                   userInfo: nil
                                    repeats: YES];
    
    
    //    float height = 0;
    //    for (int x=0;x<13;x++)
    //    {
    //        StockStreamer *ticker = [[StockStreamer alloc] initWithFrame:CGRectMake(0, 0, 510*sizeIncreaseWidth, 42*sizeIncreaseHeight)];
    //        height = ticker.frame.size.height;
    //        ticker.frame = CGRectMake(0,(10*sizeIncreaseHeight)+x*((height+5)*sizeIncreaseHeight),510*sizeIncreaseWidth,42*sizeIncreaseHeight);
    //        [ticker setTag:x+1];
    //
    //        ticker.contentMode = UIViewContentModeScaleAspectFill;
    //
    //
    //        ticker.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    //
    //        [self.leftSteamerView addSubview:ticker];
    //        ticker.alpha = 0;
    //    }
    
    
    
    //    for (int x=0;x<13;x++)
    //    {
    //        StockStreamer *ticker = [[StockStreamer alloc] initWithFrame:CGRectMake(0, 0, 510*sizeIncreaseWidth, 42*sizeIncreaseHeight)];
    //        height = ticker.frame.size.height;
    //        ticker.frame = CGRectMake(0,(10*sizeIncreaseHeight)+x*((height+5)*sizeIncreaseHeight),510*sizeIncreaseWidth,42*sizeIncreaseHeight);
    //        [ticker setTag:x+14];
    //
    //        ticker.contentMode = UIViewContentModeScaleAspectFill;
    //
    //        ticker.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    //
    //        [self.rightStreamerView addSubview:ticker];
    //        ticker.alpha = 0;
    //    }
    
    watchListArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
    [watchListArr insertObject:@"None" atIndex:0];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    leftCount = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestWatchListContentReceived) name:@"getWatchlistContentSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(marketStreamerReceived:)  name:@"marketStreamerReceived" object:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    [[NSNotificationCenter defaultCenter] removeObserver:@"getWatchlistContentSuccess"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"marketStreamerReceived"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self rotated:nil];
}
- (void)rotated:(NSNotification *)notification {
    leftCount = 0;
    if(UIDevice.currentDevice.orientation == UIDeviceOrientationLandscapeLeft || UIDevice.currentDevice.orientation == UIDeviceOrientationLandscapeRight){
        self.cst_stockViewWidth.constant = SCREEN_WIDTH_PORTRAIT/2;
        increaseMultiple = 2;
        [self calculateMaxLine];
    }else{
        self.cst_stockViewWidth.constant = SCREEN_WIDTH_PORTRAIT;
        increaseMultiple = 1;
        [self calculateMaxLine];
    }
    [self.stockView.leftSteamerView reloadData];
    [self.stockView2.leftSteamerView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)calculateMaxLine{
    
    
    rowHeight = 40;
    CGFloat maxLine = 0;
    CGFloat h = CGRectGetHeight(self.stockView.leftSteamerView.frame);
    maxLine = h / rowHeight;
    if(maxLine == floorf(maxLine)){
        MAX_LINES = maxLine;
    }else{
        rowHeight = (CGFloat)h / maxLine;
        MAX_LINES = h / rowHeight ;
    }
    
    
    self.stockView.leftSteamerView.delegate = self;
    self.stockView.leftSteamerView.dataSource = self;
    self.stockView.leftSteamerView.rowHeight = 40;
    
    self.stockView2.leftSteamerView.delegate = self;
    self.stockView2.leftSteamerView.dataSource = self;
    self.stockView2.leftSteamerView.rowHeight = 40;
    
}
- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"hh:mm:ss"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
- (void) latestWatchListContentReceived{
    // NSLog(@"latestWatchListContentReceived %@",[UserPrefConstants singleton].userWatchListStockCodeArr);
    [vertxConnectionManager vertxGetMarketStreamer:[UserPrefConstants singleton].userWatchListStockCodeArr andValue:valueCount andQuantity:quantityCount andCondition:0];
    
}

- (void) handleTimer{
    [self testStreamer];
}

- (void) testStreamer{
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"TestStreamer" ofType:@"plist"];
    NSDictionary *response = [NSDictionary dictionaryWithContentsOfFile:file];
    
    if(leftStreamerArray.count<increaseMultiple*MAX_LINES){
        [leftStreamerArray addObject:response];
        if(leftStreamerArray.count <= MAX_LINES){
            [self.stockView.leftSteamerView reloadData];
        }else{
            [self.stockView2.leftSteamerView reloadData];
        }
        
    }else{
        
        if (leftCount>=increaseMultiple*MAX_LINES) {
            leftCount=0;
        }
        [leftStreamerArray replaceObjectAtIndex:leftCount withObject:response];
        
        
        //reload
        int tmpCount = leftCount;
        if(leftCount >= MAX_LINES){
            tmpCount = leftCount - MAX_LINES;
        }
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:tmpCount inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        if(leftCount < MAX_LINES){
            [self.stockView.leftSteamerView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
        }else{
            [self.stockView2.leftSteamerView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
        }
        
    }
    
    
    // populate the data to screen
    leftCount++;
    
}


#pragma mark - Receive notification from Vertx
- (void)marketStreamerReceived:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary * response = [notification.userInfo copy];
        
        if(leftStreamerArray.count<increaseMultiple*MAX_LINES){
            [leftStreamerArray addObject:response];
            if(leftStreamerArray.count<MAX_LINES){
                [self.stockView.leftSteamerView reloadData];
            }else{
                [self.stockView2.leftSteamerView reloadData];
            }
            
        }else{
            
            if (leftCount>=increaseMultiple*MAX_LINES) {
                leftCount=0;
            }
            
            [leftStreamerArray replaceObjectAtIndex:leftCount withObject:response];
            
            //reload
            int tmpCount = leftCount;
            if(leftCount > MAX_LINES){
                tmpCount = leftCount - MAX_LINES;
            }
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:tmpCount inSection:0];
            NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            if(leftCount < MAX_LINES){
                [self.stockView.leftSteamerView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
            }else{
                [self.stockView2.leftSteamerView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
            }
        }
        
        // populate the data to screen
        leftCount++;
    });
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.stockView.leftSteamerView) {
        if(leftStreamerArray.count > MAX_LINES){
            return MAX_LINES;
        }
        return leftStreamerArray.count;
    }
    else if(tableView == self.stockView2.leftSteamerView) {
        if(leftStreamerArray.count > increaseMultiple*MAX_LINES){
            return MAX_LINES;
        }
        return leftStreamerArray.count - MAX_LINES;
    }
    else if (tableView.tag==1) {
        return watchListArr.count;
    }else{
        return 4;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.stockView.leftSteamerView || tableView == self.stockView2.leftSteamerView) {
        
        int tag =  (int)indexPath.row;
        if(tableView == self.stockView2.leftSteamerView){
             tag = tag + MAX_LINES;
        }
        StockStreamerCell *ticker = (StockStreamerCell*)[tableView dequeueReusableCellWithIdentifier:@"StockStreamerCell" forIndexPath:indexPath];
        
        
        //        if (tag+1>13) {
        //            ticker = (StockStreamer *)[self.rightStreamerView viewWithTag:tag+1];
        //        }else{
        //            ticker = (StockStreamer *)[self.leftSteamerView viewWithTag:tag+1];
        //        }
        
        //        ticker.alpha = 1;
        NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;
        
        if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
            
            //change = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"1001"]stringValue];
            //changeP = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"1003"]stringValue];
        }
        
        int timeValue = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"54"] intValue];
        NSDate *dateTraded = [NSDate dateWithTimeIntervalSince1970:timeValue];
        
        float lacp =  [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"51"] floatValue];
        float price = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"98"] floatValue];
        
        NSString *stockCode = [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"33"]];
        NSString *pi =  [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"104"]];
        NSString *priceString =  [NSString stringWithFormat:@"%.3f",price];
        NSString *quantity =  [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"99"]];
        NSString *value =  [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"100"]];
        
        float changeValue = price - lacp;
        float changeValuePercentage = changeValue / lacp  * 100;
        
        NSString *changeValueString =  [NSString stringWithFormat:@"%.4f",changeValue];
        NSString *changeValuePercentageString =  [NSString stringWithFormat:@"%.3f%%",changeValuePercentage];
        
        //NSLog(@"Stock Code %@ and Tag %d",stockCode,tag);
        [ticker updateContent:stockCode
                        andPI:pi
                     andPrice:priceString
                  andQuantity:quantity
                     andValue:value
                      andTime:[self formatDate:dateTraded]
                    andChange:changeValueString
          andChangePercentage:changeValuePercentageString
               andChangeValue:changeValue
                  isAnimation:YES];
        
        return ticker;
        
    }
    
    
    static NSString *SimpleCellIdentifier = @"Cell";
    UITableViewCell *simpleCell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:SimpleCellIdentifier];
    if (simpleCell == nil) {
        simpleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleCellIdentifier];
    }
    simpleCell.textLabel.backgroundColor = [UIColor clearColor];
    simpleCell.textLabel.textColor = [UIColor darkTextColor];
    simpleCell.textLabel.font =  [UIFont systemFontOfSize:16];
    simpleCell.textLabel.numberOfLines = 2;
    simpleCell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    //NSLog(@"left Count = %d",leftCount);
    if (tableView.tag==1) {
        if (indexPath.row==0) {
            simpleCell.textLabel.text = @"None";
        }else{
            simpleCell.textLabel.text = [[watchListArr objectAtIndex:indexPath.row] objectForKey:@"Name"];
        }
    }else{
        simpleCell.textLabel.text = [filterByArray objectAtIndex:indexPath.row];
    }
    return simpleCell;
}
//5

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.stockView.leftSteamerView || tableView == self.stockView2.leftSteamerView) {
        if(tableView == self.stockView.leftSteamerView){
            [self.stockView2.leftSteamerView reloadData];
        }else{
            [self.stockView.leftSteamerView reloadData];
        }
        return;
    }
    
    
    if (tableView.tag==1) {
        
        if (indexPath.row==0) {
            _watchListLabel.text = @"None";
            favID = 0;
        }else{
            _watchListLabel.text = [[watchListArr objectAtIndex:indexPath.row] objectForKey:@"Name"];
            favID = [[[watchListArr objectAtIndex:indexPath.row] objectForKey:@"FavID"] intValue];
        }
        
        
    }else{
        _filterByLabel.text = [filterByArray objectAtIndex:indexPath.row];
        switch (indexPath.row) {
            case 0:
                valueCount = 100000;
                quantityCount = 0;
                break;
            case 1:
                valueCount = 0;
                quantityCount = 30000;
                break;
            case 2:
                valueCount = 100000;
                quantityCount = 30000;
                break;
            case 3:
                valueCount = 30000;
                quantityCount = 30000;
                break;
            default:
                break;
        }
    }
    [columnPickerPopover dismissPopoverAnimated:TRUE];
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return 40;
//}

//MARK:- Button events

- (IBAction)clickApplyButton:(UIButton *)sender{
    //    leftCount = 0;
    //    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    //    for(int x=0;x<26;x++){
    //        StockStreamer *ticker;
    //        if (x+1>13) {
    //            ticker = (StockStreamer *)[self.rightStreamerView viewWithTag:x+1];
    //        }else{
    //            ticker = (StockStreamer *)[self.leftSteamerView viewWithTag:x+1];
    //        }
    //        ticker.alpha = 0;
    //    }
    //    [leftStreamerArray removeAllObjects];
    //
    //    if (favID!=0) {
    //        [atp getWatchListItemsForStreamer:favID];
    //    }else{
    //        [vertxConnectionManager vertxGetMarketStreamer:nil andValue:valueCount andQuantity:quantityCount andCondition:0];
    //    }
    
}

- (IBAction)clickWatchList:(UIButton *)sender{
    [popupTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupTableViewController];
    navController.navigationBar.topItem.title= [LanguageManager stringForKey:_WatchList];
    
    popupTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    NSInteger totalToDisplay = 5;
    if (totalToDisplay>=10) totalToDisplay = 10;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*60);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)clickFilterBy:(UIButton *)sender{
    [popupFilterTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupFilterTableViewController];
    navController.navigationBar.topItem.title= @"Filter By:";
    
    popupFilterTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    navController.preferredContentSize=CGSizeMake(325, 150);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

@end
