//
//  MStockStreamerModeVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
@class StockStreamerView;
@class StockStreamerCardModeView;
@interface MStockStreamerModeVC : BaseVC
@property (weak, nonatomic) IBOutlet StockStreamerView *v_mornalMode;
@property (weak, nonatomic) IBOutlet StockStreamerCardModeView *v_cardMode;

@end
