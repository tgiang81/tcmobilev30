//
//  HomeVC.h
//  TCiPad
//
//  Created by Kaka on 3/26/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

//=============== WatchlistViewType ============
typedef NS_ENUM (NSInteger, WatchlistViewType) {
	WatchlistViewType_List, //For TableView
	WatchlistViewType_Grid	//For CollectionView
};


@interface HomeVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@end
