//
//  MHomeVC.m
//  TCiPad
//
//  Created by Kaka on 7/16/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MHomeVC.h"
#import "HomeSectionView.h"
#import "MMarketVC.h"
#import "ResearchVC.h"

#define kSectionHeight	44
@interface MHomeVC ()<UITableViewDelegate, UITableViewDataSource>{
	NSMutableArray *_sections;
	//Height Cell at Section
	float _heightCellOfMarketSection;
	float _heightCellOfResearchSection;
	//ContentVC
	MMarketVC *_marketVC;
	ResearchVC *_researchVC;
	
}
@end

@implementation MHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//init value
	_heightCellOfMarketSection = 500;
	_heightCellOfResearchSection = 0;
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self createLeftMenu];
	[self setupLanguage];
	[self setupFontAnColor];
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
}
- (void)setupLanguage{
	self.customBarTitle = [LanguageManager stringForKey:@"Dashboard"];
}
- (void)setupFontAnColor{
	_tblContent.backgroundColor = [UIColor clearColor];
	self.view.backgroundColor = COLOR_FROM_HEX(0x364D59);
}
#pragma mark - LoadData
- (void)loadData{
	_sections = @[@(HomeSectionType_Market), @(HomeSectionType_Indices), @(HomeSectionType_Portfolios), @(HomeSectionType_Research), @(HomeSectionType_Alert), @(HomeSectionType_Advertisement)].mutableCopy;
	if (!_marketVC) {
		_marketVC = NEW_VC_FROM_NIB([MMarketVC class], [MMarketVC nibName]);
	}
	if (!_researchVC) {
		_researchVC = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [ResearchVC storyboardID]);
		//_researchVC.navigationController = self.navigationController;
	}
	[_tblContent reloadData];
}

#pragma mark - Register Notification
- (void)registerNotification{
	[self addNotification:kDidFinishLoadDataNotification selector:@selector(didReceiveDoneLoadingData:)];
}
#pragma mark - UTILS VC
- (void)addChildVC:(BaseVC *)childVC onCell:(UITableViewCell *)cell{
	[cell addSubview:childVC.view];
	[self addChildViewController:_marketVC];
	[childVC didMoveToParentViewController:self];
	//Add Constrain
	NSMutableArray *_constrains = @[].mutableCopy;
	[childVC.view setTranslatesAutoresizingMaskIntoConstraints:NO];
	NSLayoutConstraint *topAnchor = [childVC.view.topAnchor constraintEqualToAnchor:cell.topAnchor];
	[_constrains addObject:topAnchor];
	NSLayoutConstraint *bottomAnchor = [childVC.view.bottomAnchor constraintEqualToAnchor:cell.bottomAnchor constant:0];
	[_constrains addObject:bottomAnchor];
	NSLayoutConstraint *leadintAnchor = [childVC.view.leadingAnchor constraintEqualToAnchor:cell.leadingAnchor constant:0];
	[_constrains addObject:leadintAnchor];
	NSLayoutConstraint *trailingAnchor = [childVC.view.trailingAnchor constraintEqualToAnchor:cell.trailingAnchor constant:0];
	[_constrains addObject:trailingAnchor];
	[NSLayoutConstraint activateConstraints:_constrains];
}
//Configure Cell
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
	HomeSectionType section = [_sections[indexPath.section] integerValue];
	for (UIView *v in cell.subviews) {
		[v removeFromSuperview];
	}
	cell.clipsToBounds = YES;
	BaseVC *childVC;
	switch (section) {
		case HomeSectionType_Market:{
			childVC = _marketVC;
		}
			break;
		case HomeSectionType_Research:{
			childVC = _researchVC;
		}
			break;
		default:
			break;
	}
	if (childVC) {
		if ([cell.subviews containsObject:childVC.view]) {
			childVC.indexPath = indexPath;
			return;
		}
		[childVC removeFromParentViewController];
		[childVC.view removeFromSuperview];
		//Add
		childVC.indexPath = indexPath;
		[self addChildVC:childVC onCell:cell];
	}
}

- (void)setHeightCell:(float)height inSection:(NSInteger)section{
	HomeSectionType sectionType = [_sections[section] integerValue];
	switch (sectionType) {
		case HomeSectionType_Market:
			_heightCellOfMarketSection = height;
			break;
		case HomeSectionType_Research:
			_heightCellOfResearchSection = height;
			break;
		default:
			break;
	}
}
#pragma mark - UITableViewDataSource / UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return [_sections count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return kSectionHeight;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
	return kSectionHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	HomeSectionType type = [_sections[section] integerValue];
	HomeSectionView *header = [[HomeSectionView alloc] initWithSection:type];
	return header;
}

//For Cell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	HomeSectionType type = [_sections[indexPath.section] integerValue];
	float  heightCell = 100;
	switch (type) {
		case HomeSectionType_Market:
			heightCell = _heightCellOfMarketSection;
			break;
		case HomeSectionType_Research:
			heightCell = _heightCellOfResearchSection;
			break;
		default:
			break;
	}
	return heightCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *IDCELL = @"MHomeCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDCELL];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDCELL];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
	[self configureCell:cell atIndexPath:indexPath];
}

#pragma mark - Handle Receive Notification
- (void)didReceiveDoneLoadingData:(NSNotification *)noti{
	ControllerModel *model = noti.object;
	if (model.indexPath) {
		dispatch_async(dispatch_get_main_queue(), ^{
			NSInteger sectionIndex = model.indexPath.section;
			[self setHeightCell:model.heightContent inSection:sectionIndex];
			//NSRange range = NSMakeRange(model.indexPath.section, 1);
			//NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
			//NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:sectionIndex];
//			[self.tblContent beginUpdates];
//			[self.tblContent reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
//			[self.tblContent endUpdates];
			[self.tblContent reloadData];
		});
	}
}
@end
