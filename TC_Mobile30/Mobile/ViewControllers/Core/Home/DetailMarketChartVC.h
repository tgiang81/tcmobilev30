//
//  DetailMarketChartVC.h
//  TCiPad
//
//  Created by Kaka on 4/27/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@class MarketSummaryView;
@interface DetailMarketChartVC : BaseVC{
	
}

@property (weak, nonatomic) IBOutlet MarketSummaryView *marketSummaryView;

//Configuration
@property (assign, nonatomic) BOOL allowDoubleTapToViewFull;

@end
