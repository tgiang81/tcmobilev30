//
//  DetailMarketChartVC.m
//  TCiPad
//
//  Created by Kaka on 4/27/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DetailMarketChartVC.h"
#import "MarketSummaryView.h"

@interface DetailMarketChartVC ()<UIGestureRecognizerDelegate>{
	UITapGestureRecognizer *_tap;
}

@end

@implementation DetailMarketChartVC
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		//Set this for show like popup
		 //self.providesPresentationContextTransitionStyle = YES;
		 //[self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
		 //self.definesPresentationContext = YES;
		 self.view.backgroundColor = RGBA(0, 0, 0, 0.3);		 
		[self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
	}
	return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_marketSummaryView.alpha = 0.0;
}


- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self loadChart];
	//Force Lanscapde mode
	[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeLeft) forKey:@"orientation"];
	[UINavigationController attemptRotationToDeviceOrientation];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	[UIView transitionWithView:self.view duration:1.0 options:(UIViewAnimationOptionTransitionFlipFromTop) animations:^{
		_marketSummaryView.alpha = 1.0;
	} completion:^(BOOL finished) {
		
	}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Status Device

- (BOOL)prefersStatusBarHidden {
	return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
	return UIInterfaceOrientationLandscapeLeft;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
	return UIInterfaceOrientationMaskLandscape;
}

-(BOOL)shouldAutorotate {
	return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Configuration
- (void)setAllowDoubleTapToViewFull:(BOOL)allowDoubleTapToViewFull{
	if (allowDoubleTapToViewFull) {
		if (!_tap) {
			_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlerDoubleTap:)];
			_tap.delegate = self;
			_tap.numberOfTapsRequired = 2;
		}
		[self.view addGestureRecognizer:_tap];
	}else{
		if (_tap) {
			[self.view removeGestureRecognizer:_tap];
			_tap = nil;
		}
	}
	_allowDoubleTapToViewFull = allowDoubleTapToViewFull;
}

#pragma mark - Main
- (void)loadChart{
	[self.marketSummaryView updateMarketSummaryData];
}

- (void)handlerDoubleTap:(UITapGestureRecognizer *)tap{
	[UIView transitionWithView:self.marketSummaryView duration:1.0 options:(UIViewAnimationOptionTransitionFlipFromBottom) animations:^{
		_marketSummaryView.alpha = 0.0;
	} completion:^(BOOL finished) {
		[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
		[UINavigationController attemptRotationToDeviceOrientation];
		[self dismissViewControllerAnimated:NO completion:nil];
	}];

}

#pragma mark - UITapGestureReconized
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
	return YES;
}
@end
