//
//  HomeVC.m
//  TCiPad
//
//  Created by Kaka on 3/26/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "HomeVC.h"
#import "CollapseHeaderView.h"
#import "MarketView.h"
#import "MarketCell.h"
#import "NewsCell.h"
#import "QuoteCell.h"
#import "EmbededCollectionViewCell.h"
#import "QuoteCollectionViewCell.h"

#import "AllCommon.h"
#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"
#import "QCData.h"
#import "StockModel.h"
#import "QuoteCategoryVC.h"

#import "DetailNewsVC.h"
#import "TCChartView.h"
#import "TCAnimation.h"
#import "LanguageKey.h"
//Constant Configuration
#define kHeightMarketCell  			300
#define kHeightNewsHomeCell				70
#define kHeightExpandQuoteCell		280
#define kHeightCollapseQuoteCell	60

//For CollectionViewCell
#define kSPACING_BETTWEN_ITEMS		2//010
#define kMAGIN_LEFT_RIGHT_ITEM		2//5
#define kMINIMUM_LINE_SPACING		2
#define kMINIMUM_INTERITEM_SPACING	2
#define kRATIO_WIDTH_HEIGHT_ITEM 	1.6
#define kITEM_PER_ROW				2


//For icon Menu
#define kGrid_Menu_ICON		@"grid_menu_icon"
#define kList_Menu_ICON		@"list_menu_icon"

@interface HomeVC ()<UITableViewDelegate, UITableViewDataSource, CollapseHeaderViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, QuoteCategoryVCDelegate, QuoteCellDelegate>{
	NSMutableArray *_boolSectionArr; //include bool value
	HeaderType _oldSectionChoosed;
	WatchlistViewType _viewType;
	
	NSMutableArray *_rankedByNameArr;
	NSMutableArray *_rankedByKeyArr;
	NSString *_rankNameKey;
	//Store Data
	NSMutableArray *_watchlistArr;
	NSMutableArray *_topRankArr;
	NSMutableArray *_newsArr;
	
	NewsType _newType;
	
	//Control
	VertxConnectionManager *_vcm;
	
	NSInteger scoreBoardUpdateCnt;
    
    TCAnimation *animation;
	//Self Calculate fields
	int vertxMarketQueryCount;
	
	int data2201;
	int data2203;
	int data2204;
}
@property (nonatomic, strong) NSTimer *scoreBoardTimer;

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//init properties 	//init data
    animation = [[TCAnimation alloc] init];
	_boolSectionArr = @[].mutableCopy;
	_oldSectionChoosed = HeaderType_Unknown;
	_viewType = WatchlistViewType_List;
	_watchlistArr = @[].mutableCopy;
	_topRankArr = @[].mutableCopy;
	_newsArr = @[].mutableCopy;
	
	data2201 = 0;
	data2203 = 0;
	data2204 = 0;
	
	_rankedByNameArr = [Utils rankedByNameArr];
	_rankedByKeyArr = [Utils rankedByKeyArr];
	_rankNameKey = _rankedByNameArr.firstObject;
	
	_vcm = [VertxConnectionManager singleton];
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	//[self createCustomRightMenuWithImage:kList_Menu_ICON selector:@selector(onChangeVisibleQuoteTypeAction:)];
	[self createLeftMenu];
	[self setupLanguage];
	//Register MarketCell
	[self.tblContent registerNib:[UINib nibWithNibName:[MarketCell nibName] bundle:nil] forCellReuseIdentifier:[MarketCell reuseIdentifier]];
	//Register NewsCell
	[self.tblContent registerNib:[UINib nibWithNibName:[NewsCell nibName] bundle:nil] forCellReuseIdentifier:[NewsCell reuseIdentifier]];
	//Register QuoteCell
	[self.tblContent registerNib:[UINib nibWithNibName:[QuoteCell nibName] bundle:nil] forCellReuseIdentifier:[QuoteCell reuseIdentifier]];
	//Register EmbededCell
	[self.tblContent registerNib:[UINib nibWithNibName:[EmbededCollectionViewCell nibName] bundle:nil] forCellReuseIdentifier:[EmbededCollectionViewCell reuseIdentifier]];
}
- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:Home];
}

- (CGSize)sizeForCell{
	float width = (SCREEN_WIDTH_PORTRAIT - kSPACING_BETTWEN_ITEMS * (kITEM_PER_ROW) - (2 * kMINIMUM_INTERITEM_SPACING)) / 2;
	//float width = self.tblContent.frame.size.width;
	float height = width / kRATIO_WIDTH_HEIGHT_ITEM;
	return CGSizeMake(width, height);
}

#pragma mark - LoadData
- (void)loadData{
	//Load vertx first
	//KLCIs need to load it
	[_vcm vertxGetKLCI];
	[self loadScoreBoard];
	
	//#1: Prepare Section content
	[self prepareDataSection];
	
	//Load section data
	//1: Watchlist
	[[ATPAuthenticate singleton] getWatchlistWithCompletion:^(BOOL success, id result, NSError *error) {
		if (success) {
			[self loadWatchlist];
		}else{
			DLog(@"+++ Failed load watchlist: %@", error);
		}
	}];
	
	//Load TopRank
	[self loadTopRank];
	
	//Load News
	[self loadNews];
}
- (void)prepareDataSection{
	NSArray *sections = @[@(HeaderType_Market), @(HeaderType_WatchList), @(HeaderType_News), @(HeaderType_TopRank), @(HeaderType_Portfolio)].copy;
	for (int i = 0; i < sections.count; i ++) {
		[_boolSectionArr addObject:@NO];
	}
}

//Load ScroreBoard
- (void)loadScoreBoard{
	vertxMarketQueryCount =0;
	scoreBoardUpdateCnt=1;
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
	// update timer
	if (_scoreBoardTimer!=nil) {
		[_scoreBoardTimer invalidate];
		_scoreBoardTimer = nil;
	}
	_scoreBoardTimer = [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(updateScoreBoard) userInfo:nil
													   repeats:YES];
}



//===== Watchlist =======
- (void)loadWatchlist{
	
	NSMutableArray *userWatchlistArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
	if ([userWatchlistArr count]>0) {
		NSDictionary *firstWL = [userWatchlistArr objectAtIndex:0];
		int favID = [[firstWL objectForKey:@"FavID"] intValue];
		[UserPrefConstants singleton].prevWatchlistFavID = favID;
		[UserPrefConstants singleton].watchListName = [firstWL objectForKey:@"Name"];
		[[ATPAuthenticate singleton] getWatchListItems:favID];
		[UserPrefConstants singleton].quoteScreenFavID = [NSNumber numberWithInt:favID];
		
	}else {
		//No watchlist data
	}
}

//======== Load Top Rank =================
- (void)loadTopRank{
	NSString *property = [self propertyFromRankNameKey:_rankNameKey];
	NSString *direction = [self directionFromRankNameKey:_rankNameKey];
	__weak HomeVC *_weakSelf = self;
	[_vcm vertxSortByLeft:property Direction:direction exchg:[UserPrefConstants singleton].userCurrentExchange completion:^(id result, NSError *error) {
		NSDictionary *rsDict = (NSDictionary *)result;
		if (!rsDict || rsDict.count == 0) {
			DLog(@"+++ can not get quote from socket...");
			//Make load again
			[_weakSelf loadTopRank];
			return ;
		}
		//Parse data
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
			[_topRankArr removeAllObjects];
			//Parse data here
			for (NSString *key in rsDict.allKeys) {
				if ([key isEqualToString:@"sortedresults"]) {
					NSArray *stockCodes = rsDict[key];
					for (NSString *stkCode in stockCodes) {
						StockModel *model = [Utils stockQCFeedDataFromCode:stkCode];
						if (model) {
							[_topRankArr addObject:model];
						}
					}
					break;
				}
			}
			dispatch_async(dispatch_get_main_queue(), ^{
				[_tblContent reloadData];
			});
		});
	}];
}
//===== For News
- (void)loadNews{
	if ([UserPrefConstants singleton].isElasticNews) {
		[[ATPAuthenticate singleton] getElasticNewsForStocks:@"Home" forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
		_newType = NewsType_Elastic;
	}else if ([UserPrefConstants singleton].isArchiveNews) {
		// NSLog(@"Archive");
		_newType = NewsType_Archive;
		[[ATPAuthenticate singleton] getArchiveNewsForStock:@"Home" forDays:30];
	} else {
		//isJarNews = YES;
		_newType = NewsType_Jar;
		[[ATPAuthenticate singleton] getJarNews];
	}
}
#pragma mark - Util VC
- (NSString *)propertyFromRankNameKey:(NSString *)rankNameKey{
	if (!_rankedByNameArr || !_rankedByKeyArr) {
		return nil;
	}
	NSInteger indexRank = [_rankedByNameArr indexOfObject:rankNameKey];
	NSArray *propertyDivideDirection = [[_rankedByKeyArr objectAtIndex:indexRank] componentsSeparatedByString:@"|"];
	return  [propertyDivideDirection objectAtIndex:0];
}

- (NSString *)directionFromRankNameKey:(NSString *)rankNameKey{
	if (!_rankedByNameArr || !_rankedByKeyArr) {
		return nil;
	}
	NSInteger indexRank = [_rankedByNameArr indexOfObject:rankNameKey];
	NSArray *propertyDivideDirection = [[_rankedByKeyArr objectAtIndex:indexRank] componentsSeparatedByString:@"|"];
	return [propertyDivideDirection objectAtIndex:1];
}
- (void)registerNotification{
	//Update new data
	[self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
	//Notification item
	[self addNotification:kVertxNo_didReceivedWatchListItems selector:@selector(didReiveNotification:)];
	
	//Get KLCI
	[self addNotification:kVertxNo_DoneGetKLCI selector:@selector(didReceiveKLCINotification:)];
	
	//ScoreBoard
	[self addNotification:kVertxNo_doneVertxGetScoreBoardWithExchange selector:@selector(doneScoreBoard:)];
	
	//Get News
	[self addNotification:kVertxNo_didFinishedGetAllStockNews selector:@selector(didReiveNotification:)];
	[self addNotification:kVertxNo_didFinishGetArchiveNews selector:@selector(didReiveNotification:)];
	[self addNotification:kVertxNo_didFinishGetArchiveNews selector:@selector(didReiveNotification:)];
	[self addNotification:kVertxNo_didReceiveJarNews selector:@selector(didReiveNotification:)];
}

//======== StockModel from code =========================
- (StockModel *)stockModelFromCode:(NSString *)stockCode{
	NSDictionary *stockDetail = [[NSDictionary alloc]initWithDictionary:[[[QCData singleton]qcFeedDataDict]objectForKey:stockCode]];
	return  [[StockModel alloc] initWithDictionary:stockDetail error:nil];
}
#pragma mark - Main Action
- (void)onChangeVisibleQuoteTypeAction:(UIButton *)sender{
	if (_viewType == WatchlistViewType_Grid) {
		_viewType = WatchlistViewType_List;
		//[sender setImage:[UIImage imageNamed:kGrid_Menu_ICON]];
	}else{
		_viewType = WatchlistViewType_Grid;
		//[sender setImage:[UIImage imageNamed:kList_Menu_ICON]];
	}
	//Reload section
	NSRange range   = NSMakeRange(HeaderType_WatchList, 1);
	NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
	[_tblContent reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
}

-(void)updateScoreBoard {
	scoreBoardUpdateCnt++;
	if (scoreBoardUpdateCnt>=100) scoreBoardUpdateCnt = 10;
	
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
}
#pragma mark - UITableViewDataSource
//For Header
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return [_boolSectionArr count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 44;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
	return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	BOOL isSelectedHeader = [[_boolSectionArr objectAtIndex:section] boolValue];
	CollapseHeaderView *headerView = [[CollapseHeaderView alloc] initWithHeader:(HeaderType)section isSelect:isSelectedHeader];
	if (section == HeaderType_TopRank) {
		headerView.lblTitle.text = [NSString stringWithFormat:@"%@: %@", [LanguageManager stringForKey:@"Top Rank"], _rankNameKey];
	}
	headerView.delegate = self;
	return headerView;
}

//For Cell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if ([[_boolSectionArr objectAtIndex:section] boolValue]) {
		if (section == HeaderType_Market) {
			return 1;
		}
		if (section == HeaderType_WatchList) {
			if (_viewType == WatchlistViewType_Grid) {
				return 1;
			}
			return [_watchlistArr count];
		}
		if (section == HeaderType_TopRank) {
			return [_topRankArr count];
		}
		if (section == HeaderType_News) {
			return [_newsArr count];
		}
		return 3;
	}
	return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	BOOL isOpenHeader = [_boolSectionArr[indexPath.section] boolValue];
	if (!isOpenHeader) {
		return 0;
	}
	if (indexPath.section == HeaderType_Market) {
		return kHeightMarketCell;
	}
	if (indexPath.section == HeaderType_News) {
		return kHeightNewsHomeCell;
	}
	
	if (indexPath.section == HeaderType_TopRank) {
		return kHeightExpandQuoteCell;
	}
	
	//Watchlist
	if (indexPath.section == HeaderType_WatchList) {
		if (_viewType == WatchlistViewType_Grid) {
			NSInteger countItem = _watchlistArr.count;
			float rowItem = (countItem / 2) + (countItem % 2);
			CGSize sizeCell = [self sizeForCell];
			return (rowItem * sizeCell.height + kMINIMUM_INTERITEM_SPACING * (rowItem + 1));
		}
		//For type List
		return kHeightExpandQuoteCell;
	}
	return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	//Market
	if (indexPath.section == HeaderType_Market) {
		MarketCell *cell = [tableView dequeueReusableCellWithIdentifier:[MarketCell reuseIdentifier]];
		if (!cell) {
			cell = [[MarketCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MarketCell reuseIdentifier]];
		}
		
		[cell populateData];
		return cell;
	}
	
	//News
	if (indexPath.section == HeaderType_News) {
		NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:[NewsCell reuseIdentifier]];
		if (!cell) {
			cell = [[NewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NewsCell reuseIdentifier]];
		}
		[cell setupUIByModel:_newsArr[indexPath.row]];
		return cell;
	}
	
	//Watchlist
	if (indexPath.section == HeaderType_WatchList) {
		if (_viewType == WatchlistViewType_List) {
			QuoteCell *cell = [tableView dequeueReusableCellWithIdentifier:[QuoteCell reuseIdentifier]];
			if (!cell) {
				cell = [[QuoteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[QuoteCell reuseIdentifier]];
			}
			
			StockModel *model = _watchlistArr[indexPath.row];
            cell.delegate = self;
			[cell setupUIFromModel:model atIndexPath:indexPath expand:YES];
			return cell;
		}else{
			EmbededCollectionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[EmbededCollectionViewCell reuseIdentifier]];
			UICollectionViewScrollDirection direction = UICollectionViewScrollDirectionVertical;
			if (!cell) {
				cell = [[EmbededCollectionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[EmbededCollectionViewCell reuseIdentifier] withScrollDirection:direction];
			}
			[cell initCollectionViewWithDirection:direction];
			[cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
			return cell;
		}
	}
	
	//Top Rank
	if (indexPath.section == HeaderType_TopRank) {
		QuoteCell *cell = [tableView dequeueReusableCellWithIdentifier:[QuoteCell reuseIdentifier]];
		if (!cell) {
			cell = [[QuoteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[QuoteCell reuseIdentifier]];
		}
		
		StockModel *model = _topRankArr[indexPath.row];
		[cell setupUIFromModel:model atIndexPath:indexPath expand:YES];
		return cell;
	}
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
	};
	cell.backgroundColor = [UIColor purpleColor];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	//For News
	if (indexPath.section == HeaderType_News) {
		NewsModel *model = _newsArr[indexPath.row];
		DetailNewsVC *_newsVC = NEW_VC_FROM_NIB([DetailNewsVC class], [DetailNewsVC nibName]);
		_newsVC.newsModel = model;
		[self.navigationController pushViewController:_newsVC animated:YES];
	}
}

#pragma mark - For EmbededTableView
#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return 15;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	QuoteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[QuoteCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
	return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	return [self sizeForCell];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
	return kMINIMUM_LINE_SPACING;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
	return kMINIMUM_INTERITEM_SPACING;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(2, 2, 2, 2);  // top, left, bottom, right
}
#pragma mark - CollapseHeaderViewDelegate
#pragma mark -  ContactHeaderView Delegate
- (void)didOpenHistoryHeaderView:(CollapseHeaderView *)headerView{
	NSInteger _currentSection = headerView.currentHeader;
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:_currentSection];
	if (_oldSectionChoosed != -1 && _oldSectionChoosed != _currentSection) {
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:_oldSectionChoosed];
		BOOL collapsed  = [[_boolSectionArr objectAtIndex:indexPath.section] boolValue];
		if (collapsed == YES) {
			//Reload old section
			collapsed  = !collapsed;
			[_boolSectionArr replaceObjectAtIndex:_oldSectionChoosed withObject:[NSNumber numberWithBool:collapsed]];
			NSRange range   = NSMakeRange(indexPath.section, 1);
			NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
			[_tblContent reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
		}
	}
	//Reload new section
	BOOL collapsed  = [[_boolSectionArr objectAtIndex:indexPath.section] boolValue];
	collapsed  = !collapsed;
	UITableViewRowAnimation animationRowType = UITableViewRowAnimationBottom;
	if (!collapsed) {
		animationRowType = UITableViewRowAnimationTop;
	}
	[_boolSectionArr replaceObjectAtIndex:_currentSection withObject:[NSNumber numberWithBool:collapsed]];
	NSRange range = NSMakeRange(indexPath.section, 1);
	NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
	[_tblContent reloadSections:sectionToReload withRowAnimation:animationRowType];
	_oldSectionChoosed = _currentSection;
}
- (void)didTapFilterAction{
	QuoteCategoryVC *_contentPopup = NEW_VC_FROM_NIB([QuoteCategoryVC class], [QuoteCategoryVC nibName]);
	_contentPopup.selectedRankName = _rankNameKey;
	_contentPopup.delegate = self;
	[self showPopupWithContent:_contentPopup withCompletion:^(MZFormSheetController *formSheetController) {
		
	}];
}

#pragma mark - QuoteCategoryVCDelegate - PopupDelegate
- (void)didSelectRankName:(NSString *)rankName{
	_rankNameKey = rankName;
	NSRange range = NSMakeRange(HeaderType_TopRank, 1);
	NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
	[_tblContent reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
	//Reload Toprank again
	[self loadTopRank];
}
#pragma mark - DidReiveNotification
- (void)didReiveNotification:(NSNotification *)noti{
	NSString *_nameNotification = noti.name;
	if ([_nameNotification isEqualToString:kVertxNo_didReceivedWatchListItems]) {
		NSArray *wlistStkCodeArr = [[NSArray alloc]initWithArray:[UserPrefConstants singleton].userWatchListStockCodeArr copyItems:YES];
		DLog(@"+++ Watchlist Result: %@", wlistStkCodeArr);
		for (NSString *stkCode in wlistStkCodeArr)
		{
			if (([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
				NSDictionary *dictToAdd = [[NSDictionary alloc] initWithDictionary:
										   [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];
				//[dictToSort setObject:dictToAdd forKey:stkCode];
				StockModel *stock = [[StockModel alloc] initWithDictionary:dictToAdd error:nil];
				if (stock) {
					[_watchlistArr addObject:stock];
				}
			}
		}
		//Reload watchlist section here
		DLog(@"+++ Watchlist Array: %@", _watchlistArr);
		return;
	}
	//========= For watchlist - Maybe not use this case <not register notification on mobilde function> ======
	if ([_nameNotification isEqualToString:kVertxNo_getWatchlistSuccess]) {
		[self loadWatchlist];
		return;
	}
	
	//For News Notificaiton
	if ([_nameNotification isEqualToString:kVertxNo_didFinishedGetAllStockNews] || [_nameNotification isEqualToString:kVertxNo_didFinishGetArchiveNews] || [_nameNotification isEqualToString:kVertxNo_didFinishGetArchiveNews] || [_nameNotification isEqualToString:kVertxNo_didReceiveJarNews]) {
		DLog(@"+++ News Noti: %@ Content\n:%@", _nameNotification, noti.userInfo);
		if ([_nameNotification isEqualToString:kVertxNo_didFinishGetArchiveNews]) {
			//Handle Achive News
			_newType = NewsType_Archive;
			NSArray *newsArray = [noti.userInfo objectForKey:@"News"];
			NSLog(@"archiveNews %@", newsArray);
			if ([newsArray count]>0) {
				NSArray *_sortedNewsArr = [NSArray arrayWithArray:newsArray];
				NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"dt" ascending: NO];
				_sortedNewsArr = [_sortedNewsArr sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
				//Parse to model here
				_newsArr = [[Utils parseNewsArrFrom:_sortedNewsArr byType:NewsType_Archive] mutableCopy];
			}
		}
		if ([_nameNotification isEqualToString:kVertxNo_didReceiveJarNews]) {
			//Handle JarNews
			_newType = NewsType_Jar;
			NSArray *rsDict = [noti.userInfo objectForKey:@"data"];
			if ([rsDict count] <= 0) {
				//isJarNews = NO;
				[[VertxConnectionManager singleton] vertxCompanyNews:@""];
			} else {
				//Parse Data here
				_newsArr = [[Utils parseNewsArrFrom:rsDict byType:NewsType_Jar] mutableCopy];
			}
		}
		
		//Handle AllStock New
		if ([_nameNotification isEqualToString:kVertxNo_didFinishedGetAllStockNews
			 ]) {
			_newType = NewsType_AllStock;
			NSArray *unSortedArray = [noti.userInfo objectForKey:@"data"];
			if (unSortedArray.count > 0) {
				NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"4" ascending:NO];
				NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
				NSArray *sortedArr = [unSortedArray sortedArrayUsingDescriptors:descriptors];
				//Check to get right Object
				NSMutableArray *finalNewsArr = @[].mutableCopy;
				for (NSDictionary *dict in sortedArr) {
					if (![[dict objectForKey:@"1"] isEqualToString:@"FMETF."]) {
						[finalNewsArr addObject:dict];
					}
				}
				//Parse to model here
				_newsArr = [[Utils parseNewsArrFrom:finalNewsArr byType:NewsType_AllStock] mutableCopy];
			}
			//======= Reload News Section ===========
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:HeaderType_News];
			NSRange range = NSMakeRange(indexPath.section, 1);
			NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
			[_tblContent reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
		}
	}
}
//KLCI Notification
- (void)didReceiveKLCINotification:(NSNotification *)noti{
	DLog(@"+++ Did receive KLCI data");
	//======= Reload News Section ===========
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:HeaderType_Market];
	MarketCell *cell = [_tblContent cellForRowAtIndexPath:indexPath];
	[cell reloadSummaryMarket];
	/* No need reload section
	NSRange range = NSMakeRange(indexPath.section, 1);
	NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
	[_tblContent reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
	 */
}

//ScoreBoard
- (void)doneScoreBoard:(NSNotification *)noti{
	if ([[[QCData singleton]qcScoreBoardDict] count] <= 0) {
		return;
	}
	// CIMB SG sends 2201 twice and 2203. 2204 data missing.
	NSDictionary *mainMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2201]];
	NSDictionary *warrantMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2203]];
	NSDictionary *aceMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2204]];
	DLog(@"ScoreBoard: M - %@, W - %@, ACE - %@", mainMarketDict, warrantMarketDict, aceMarketDict);
	
	if ([mainMarketDict count]>0) data2201 = 1; else data2201 = 0;
	if ([warrantMarketDict count]>0) data2203 = 1; else data2203 = 0;
	if ([aceMarketDict count]>0) data2204 = 1; else data2204 = 0;
	
	vertxMarketQueryCount  = data2201+data2203+data2204;
	if(vertxMarketQueryCount == 3) // 3 = Main, Warrant and Ace.
	{
		vertxMarketQueryCount =0; // reset
	}
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:HeaderType_Market];
	MarketCell *cell = [_tblContent cellForRowAtIndexPath:indexPath];
	[cell reloadScoreBoardMarket];
}

//DidUpdate
- (void)didUpdate:(NSNotification *)noti{
	/*
	DLog(@"+++ Did update: %@", noti.object);
	NSString *stockCode = noti.object;
	//1: Check in watchlist array
	NSInteger indexOfStockWatchlist = [Utils indexOfStockByCode:stockCode fromArray:_watchlistArr];
	StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
	//Update for watchlist
	if (changedStock && indexOfStockWatchlist != -1) {
		//Replace to new object and beginning update data
		[_watchlistArr replaceObjectAtIndex:indexOfStockWatchlist withObject:changedStock];
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfStockWatchlist inSection:HeaderType_WatchList];
		if (_viewType == WatchlistViewType_List) {
			QuoteCell *cell = [_tblContent cellForRowAtIndexPath:indexPath];
			[cell updateData:changedStock useBlinkIfNeed:YES];
		}
		return;
	}
	
	//Update for TopRank
	NSInteger indexOfTopRank = [Utils indexOfStockByCode:stockCode fromArray:_topRankArr];
	if (changedStock && indexOfTopRank != -1) {
		//Replace to new object and beginning update data
		[_topRankArr replaceObjectAtIndex:indexOfTopRank withObject:changedStock];
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfTopRank inSection:HeaderType_TopRank];
		QuoteCell *cell = [_tblContent cellForRowAtIndexPath:indexPath];
		[cell updateData:changedStock useBlinkIfNeed:YES];
		//Update UI here
		//[_tblContent beginUpdates];
		//[_tblContent endUpdates];
		return;
	}
    */
}

#pragma mark QuoteCellDelegate
- (void)checkExpandCell:(QuoteCell *)cell isExpanded:(BOOL)isExpanded{
    NSLog(@"cc");
}
- (void)doubleTap:(QuoteCell *)cell{
    [animation showChartDetail:cell.contentView isCell:YES];
}
@end
