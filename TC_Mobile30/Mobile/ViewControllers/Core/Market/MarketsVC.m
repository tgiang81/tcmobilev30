//
//  MarketsVC.m
//  TC_Mobile30
//
//  Created by Kaka on 2/12/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MarketsVC.h"
#import "TCPageMenuControl.h"
#import "MarketTableViewCell.h"
#import "VertxConnectionManager.h"
#import "UIView+TCUtil.h"
#import "TCBubbleMessageView.h"
#import "QCData.h"
#import "IndicesModel.h"
#import "MarketInfoVC.h"

@interface MarketsVC ()<UITableViewDelegate, UITableViewDataSource>{
	//Bubble Message View
	TCBubbleMessageView *_bubbleMsgView;
	//Data
	NSMutableArray *_listMarkets;
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet TCPageMenuControl *menuExchanges;
@property (weak, nonatomic) IBOutlet UIView *instructionView;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

//Config
@property (assign, nonatomic) BOOL startLoadingIndices;
@property (strong, nonatomic) NSString *marketName;
@end

@implementation MarketsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_bubbleMsgView = [TCBubbleMessageView new];
	_listMarkets = @[].mutableCopy;
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)viewDidLayoutSubviews{
	[super viewDidLayoutSubviews];
	if (![VertxConnectionManager singleton].connected) {
		[_bubbleMsgView showBubbleMessage:@"No Connection" inView:self.view withType:BubbelMessageType_Error autoHidden:NO];
		return;
	}
}

//- (void)viewWillDisappear:(BOOL)animated{
//	[super viewWillDisappear:animated];
//	[self removeNotification:kVertxNo_pnWebSocketConnected];
//	[self removeNotification:@"pnWebSocketDisconnected"];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	self.title = [LanguageManager stringForKey:@"Indices"];
	[self createLeftMenu];
	[_tblContent registerNib:[UINib nibWithNibName:[MarketTableViewCell nibName] bundle:nil] forCellReuseIdentifier:[MarketTableViewCell reuseIdentifier]];
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	[self setupMenuExchange];
}

- (void)setupMenuExchange{
	if ([UserPrefConstants singleton].supportedExchanges.count == 0) {
		return;
	}
	//ExchangeView
	_menuExchanges.menuType = TCPageMenuControlType_Bubble;
	_menuExchanges.dataSources = [Utils getListExchangeName];
	
	_menuExchanges.currentSelectedItem = [Utils getIndexOfCurrentExchange];
	[_menuExchanges startLoadingPageMenu];
	self.marketName = _menuExchanges.dataSources[_menuExchanges.currentSelectedItem];
	[_menuExchanges didSelectItem:^(NSString *item, NSInteger index) {
		self.marketName = item;
		ExchangeData *newEx = [[UserPrefConstants singleton].supportedExchanges objectAtIndex:index];
		[Utils selectedNewExchange:newEx];
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
			//Post Notification here
			[[NSNotificationCenter defaultCenter] postNotificationName:kDidSelectNewExchangeNotification object:newEx];
		});
	}];
}
#pragma mark - LoadData
- (void)loadData{
	[self getIndices];
}

- (void)reloadData{
	_startLoadingIndices = YES;
	[_listMarkets removeAllObjects];
	[self.tblContent reloadData];
	[self getIndices];
}

//Get Indices List
- (void)getIndices{
	//For Network
	if (![VertxConnectionManager singleton].connected) {
		//Do nothing...
		return;
	}
	if ([[UserPrefConstants singleton].userExchagelist count] == 0) {
		[self.contentView showStatusMessage:@"Loading Exchanges..."];
		return;
	}
	
	//Prepare data
	[self.contentView showPrivateHUD];
	[self.contentView removeStatusViewIfNeeded];
	_startLoadingIndices = YES;
	ExchangeData *curSelectedEx = [UserPrefConstants singleton].currentExchangeData;
	//Call api
	[[VertxConnectionManager singleton] getIndicesFromExchange:curSelectedEx.feed_exchg_code completion:^(id result, NSError *error) {
		[self.contentView dismissPrivateHUD];
		if (error) {
			[self.contentView showStatusViewWithMessage:error.localizedDescription retryAction:^{
				//Reload indices here
				[self getIndices];
			}];
			return ;
		}
		//Parse data here
		// SORT IT
		if ([[QCData singleton].qcIndicesDict count] == 0) {
			[self.contentView showStatusViewWithMessage:[LanguageManager stringForKey:@"The Market is not available. Please try again later!"] retryAction:^{
				//Reload indices here
				[self getIndices];
			}];
			return;
		}
		NSMutableArray *independentArray = [[NSMutableArray alloc] init];
		for(id key in [[QCData singleton] qcIndicesDict]) {
			NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:key]];
			[independentArray addObject:eachDict];
		}
		NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
		NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
		NSMutableArray *indicesArr = [IndicesModel arrayOfModelsFromDictionaries:sortedArray error:nil];
		
		if (!self.startLoadingIndices) {
			return;
		}
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.contentView removeStatusViewIfNeeded];
			if (indicesArr && indicesArr.count > 0) {
				self->_startLoadingIndices = NO;
				self->_listMarkets = [indicesArr mutableCopy];
				[self.tblContent reloadData];
			}
		});
	}];
}
#pragma mark - Register Notification If Needed
- (void)registerNotification{
	[self addNotification:@"didReceiveExchangeList" selector:@selector(didReceiveExchangeListNotification:)];
	[self addNotification:@"pnWebSocketDisconnected" selector:@selector(didReceiveErrorSocketNotification:)];
	[self addNotification:kVertxNo_pnWebSocketConnected selector:@selector(socketConnectedNotification:)];
	[self addNotification:kDidSelectNewExchangeNotification selector:@selector(didSelectNewExchangeNotification:)];
	[self addNotification:kSwitchValueChangedNotification selector:@selector(shouldSwitchValueChangedNotification:)];
}

#pragma mark - UTILS VC
//Do something here
//****************
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_listMarkets count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MarketTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[MarketTableViewCell reuseIdentifier]];
	if (!cell) {
		cell = [[MarketTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MarketTableViewCell reuseIdentifier]];
	}
	[cell setupDataFrom:_listMarkets[indexPath.row] forMarket:self.marketName];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	MarketInfoVC *infoVC = NEW_VC_FROM_STORYBOARD(kMMarketStoryboardName, [MarketInfoVC storyboardID]);
	infoVC.market = _listMarkets[indexPath.row];
	[self nextTo:infoVC animate:YES];
}
#pragma mark - Notification
//For exchange List
- (void)didReceiveExchangeListNotification:(NSNotification *)note{
	[self setupMenuExchange];
	if (_listMarkets.count == 0) {
		[self loadData];
	}
}
//Error Socket
- (void)didReceiveErrorSocketNotification:(NSNotification *)noti{
	[[Utils shareInstance] dismissWindowHUD];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView dismissPrivateHUD];
	//Show message
	[_bubbleMsgView showBubbleMessage:@"No Connection" inView:self.view withType:BubbelMessageType_Error autoHidden:NO];
}
- (void)socketConnectedNotification:(NSNotification *)noti{
	//Update Bubble Message
	_bubbleMsgView.message = @"Connected";
	_bubbleMsgView.bubbleType = BubbelMessageType_Success;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		[self->_bubbleMsgView hide];
	});
	
	//Dimiss other indicator if needed
	[[Utils shareInstance] dismissWindowHUD];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView dismissPrivateHUD];
	if (_listMarkets.count == 0) {
		[self loadData];
	}
}

//DidSelectNewExchange
- (void)didSelectNewExchangeNotification:(NSNotification *)noti{
	//Do something here
	if (_menuExchanges.dataSources.count > 0) {
		if (_menuExchanges.currentSelectedItem != [Utils getIndexOfCurrentExchange]) {
			[self setupMenuExchange];
		}
	}
	[self reloadData];
}

//Switch Value Change or ChangeP
- (void)shouldSwitchValueChangedNotification:(NSNotification *)noti{
	[self.tblContent reloadData];
}
@end
