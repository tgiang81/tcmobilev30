//
//  MarketInfoVC.h
//  TC_Mobile30
//
//  Created by Kaka on 2/12/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

@class IndicesModel;
@interface MarketInfoVC : BaseVC{
	
}
//Passing data
@property (strong, nonatomic) IndicesModel *market;
@end
