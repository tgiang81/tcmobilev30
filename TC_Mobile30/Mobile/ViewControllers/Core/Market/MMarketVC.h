//
//  MMarketVC.h
//  TCiPad
//
//  Created by Kaka on 7/16/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class MarqueeLabel;
@interface MMarketVC : BaseVC{
	
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollChartView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIButton *btnPrevious;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UILabel *lblNameChart;

@property (weak, nonatomic) IBOutlet UIView *topInfoView;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblMarketName;
@property (weak, nonatomic) IBOutlet UILabel *lblMarketPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpDownPercent;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceUpDown;
@property (weak, nonatomic) IBOutlet UILabel *lblPercent;

@property (weak, nonatomic) IBOutlet UILabel *lblInsTotalVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblInsTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblInsTotalTrade;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalTrade;


@end
