//
//  MarketInfoVC.m
//  TC_Mobile30
//
//  Created by Kaka on 2/12/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MarketInfoVC.h"
#import "UIView+ShadowBorder.h"
#import "StockTypeSingleCell.h"
#import "StockInfoVC.h"
#import "IndicesModel.h"
#import "NSNumber+Formatter.h"
#import "SectorInfo.h"
#import "VertxConnectionManager.h"
#import "MarqueeLabel.h"
#import "UIView+Animation.h"
#import "GHContextMenuView.h"
#import "MTradeViewController.h"
#import "BaseNavigationController.h"
#import "SelectWatchlistPopupVC.h"
#import "TCBubbleMessageView.h"
#import "ImageCacheManager.h"
#import "TCLineChartView.h"
#import "DataManager.h"
#import "MStockInfoVC.h"
#import "LanguageKey.h"
#import "WatchlistModel.h"

@interface MarketInfoVC ()<UITableViewDelegate, UITableViewDataSource, StockTypeSingleCellDelegate, GHContextOverlayViewDelegate, GHContextOverlayViewDataSource, SelectWatchlistPopupVCDelegate>{
	//Configs
	NSMutableArray *_expandedCells;
	//Data
	NSMutableArray *_listStocks;
	BOOL _didLoadChart;
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *topInfoView;
@property (weak, nonatomic) IBOutlet UIView *contentChartView;
@property (weak, nonatomic) IBOutlet UIView *stockContentView;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
//Chart
@property (weak, nonatomic) IBOutlet TCLineChartView *lineChart;

//INFO
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblName;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblExchange;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblValueChanged;
//INS
@property (weak, nonatomic) IBOutlet UILabel *lblInsStock;
@end

@implementation MarketInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_expandedCells = @[].mutableCopy;
	_listStocks = @[].mutableCopy;
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setBackButtonDefault];
	self.title = [LanguageManager stringForKey:@"Indices"];
	//Draw shadow
	[_topInfoView makeBorderShadow];
	[_contentChartView makeBorderShadow];
	[_stockContentView makeBorderShadow];
	
	//TableView
	[_tblContent registerNib:[UINib nibWithNibName:[StockTypeSingleCell nibName] bundle:nil] forCellReuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	[self addContextMenu];
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	self.view.backgroundColor = TC_COLOR_FROM_HEX(@"F4F4F4");
	//self.lblInsStock.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	self.lineChart.backgroundColor = [UIColor clearColor];
}

- (void)addContextMenu{
	//Add Context Menu
	GHContextMenuView* overlay = [[GHContextMenuView alloc] init];
	overlay.dataSource = self;
	overlay.delegate = self;
	
	UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];
	[self.tblContent addGestureRecognizer:_longPressRecognizer];
}

#pragma mark - Register Notification
- (void)registerNotification{
	//Notification: didUpdate
	[self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
	[self addNotification:kSwitchValueChangedNotification selector:@selector(shouldSwitchValueChangedNotification:)];
}

#pragma mark - LoadData
- (void)loadData{
	[self loadTopMarketInfo:self.market];
	[self getStocksFromMarket:self.market];
	[self loadChart:self.market];
}

#pragma mark - Load Sub Module Data
- (void)loadChart:(IndicesModel *)model{
	float close = model.closePrice.floatValue;
	float change  = model.curPrice.floatValue - close;
	float changeP;
	if (close <= 0) {
		changeP = 0;
	}else{
		changeP = change / close * 100;
	}
	UIColor *lineChartColor = (changeP >= 0) ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	NSString *exchange = [UserPrefConstants singleton].currentExchangeData.feed_exchg_code;
	[self.lineChart loadChart:TCLineChartType_Day forCode:model.stockCode inExchange:exchange includeLastValue:model.curPrice chartColor:lineChartColor];
}
- (void)loadTopMarketInfo:(IndicesModel *)model{
	_lblName.text = model.name;
	ExchangeData *curEx = [UserPrefConstants singleton].currentExchangeData;
	_lblExchange.text = [Utils getShortExChangeName:curEx];
	//Price Info
	_lblLastPrice.text = [model.curPrice toCurrencyNumber];
	float close = model.closePrice.floatValue;
	float change  = model.curPrice.floatValue - close;
	float changeP;
	if (close <= 0) {
		changeP = 0;
	}else{
		changeP = change / close * 100;
	}
	//Parse to UI
	_lblValueChanged.text = [NSString stringWithFormat:@"%@ (%@)", [[NSNumber numberWithFloat:change] toChangedStringWithSign], [[NSNumber numberWithFloat:changeP] toPercentWithSign]];
	//Color
	_lblValueChanged.textColor = [[NSNumber numberWithFloat:change] colorByCompareToPrice:0];
	_lblLastPrice.textColor = _lblValueChanged.textColor;
}

- (void)getStocksFromMarket:(IndicesModel *)model{
	//1: Parse market to sector first
	SectorInfo *sector = [Utils getSectorInfoObjFor:model.name];
	//2: Call API
	__weak MarketInfoVC *_weakSelf = self;
	[UserPrefConstants singleton].quoteScrPage = 0;
	[self startLoadingQuotesBySector:sector completion:^(NSArray<StockModel *> *quotes, NSError *error) {
		if (error) {
			[self->_listStocks removeAllObjects];
			[_weakSelf.tblContent reloadData];
			//Show error here
			[_weakSelf.stockContentView showStatusViewWithMessage:error.localizedDescription retryAction:^{
				//+++ Refresh here
				[_weakSelf getStocksFromMarket:model];
			}];
			return;
		}
		[self.stockContentView removeStatusViewIfNeeded];
		self->_listStocks = [quotes mutableCopy];
		[self.tblContent reloadData];
	}];
}

#pragma mark - UTILS VC
- (void)startLoadingQuotesBySector:(SectorInfo *)sector completion:(void (^)(NSArray <StockModel *> *quotes, NSError *error))completion{
	//Set Sector
	if (sector && [sector.sectorName isEqualToString:[LanguageManager stringForKey:@"All Stocks"]]) {
		[UserPrefConstants singleton].sectorToFilter = nil;
	}else{
		[UserPrefConstants singleton].sectorToFilter = sector;
	}
	//2: Set rank
	NSString *property = @"101";
	NSString *direction = @"DESC";
	
	ExchangeData *curEx = [UserPrefConstants singleton].currentExchangeData;
	[[VertxConnectionManager singleton] vertxSortByServer:property Direction:direction exchg:curEx.feed_exchg_code completion:^(id result, NSError *error) {
		NSArray *quotes = [StockModel arrayOfModelsFromDictionaries:result error:nil];
		//Response is a NSArray now
		//NSDictionary *rsDict = (NSDictionary *)result;
		//NSArray *quotes = [self parseDataFromDict:rsDict];
		dispatch_async(dispatch_get_main_queue(), ^{
			completion(quotes, error);
		});
	}];
}

//Parse Data
- (NSArray<StockModel *> *)parseDataFromDict:(NSDictionary *)dict{
	if (!dict || dict.count == 0) {
		return @[];
	}
	NSMutableArray *quotes = @[].mutableCopy;
	//Parse data here
	for (NSString *key in dict.allKeys) {
		NSError *error;
		NSDictionary *stockDict = dict[key];
		StockModel *stock = [[StockModel alloc] initWithDictionary:stockDict error:&error];
		if (error) {
			DLog(@"+++ Error: Can not parse data to StockModel - %@", error);
			continue;
		}
		[quotes addObject:stock];
	}
	return [quotes copy];
}

//#pragma mark - CHART UTIL
//- (void)callApiGetChartFrom:(NSString *)code{
//	NSString *exchange = [UserPrefConstants singleton].currentExchangeData.feed_exchg_code;
//	[[VertxConnectionManager singleton] getDefaultWeekChartDataFrom:code inExchange:exchange completion:^(id result, NSError *error) {
//		if (result) {
//			NSArray *dataArr = (NSArray *)result;
//			if (dataArr.count == 0) {
//				return ;
//			}
//			NSDictionary *info = dataArr.firstObject;
//			//Check whether data is correct
//			if (![info[@"0"] isEqualToString:code]) {
//				return;
//			}
//			if (self->_didLoadChart) {
//				return;
//			}
//			self->_didLoadChart = YES;
//			NSArray *chartArr = [dataArr subarrayWithRange:NSMakeRange(1, dataArr.count - 1)]; //From index 1, because the first item is stock info item
//			dispatch_async(dispatch_get_main_queue(), ^{
//				[self drawChartFromData:chartArr];
//			});
//		}
//	}];
//}
//
////Draw chart
//- (void)drawChartFromData:(NSArray *)dataChart{
//	if (!dataChart) {
//		return;
//	}
//	NSMutableArray *xElements = @[].mutableCopy;
//	NSMutableArray *yElements = @[].mutableCopy;
//	for (int i = 0; i < dataChart.count; i++) {
//		NSDictionary *dictData = dataChart[i];
//		[xElements addObject:dictData[@"0"]];
//		[yElements addObject:[NSNumber numberWithDouble:[dictData[@"4"] doubleValue]]];
//	}
//	if (yElements.count == 0) {
//		return;
//	}
//	//Store data
//	[[DataManager shared] storeDataLineCharts:dataChart forKey:self.market.exStCode];
//	//Add last object
//	[xElements addObject:@"now"];
//	[yElements addObject:self.market.curPrice];
//
//	self.lineChart.datasources = [yElements mutableCopy];
//	self.lineChart.xValues = [xElements mutableCopy];
//
//	//Config chart
//	float close = self.market.closePrice.floatValue;
//	float change  = self.market.curPrice.floatValue - close;
//	float changeP;
//	if (close <= 0) {
//		changeP = 0;
//	}else{
//		changeP = change / close * 100;
//	}
//	UIColor *lineChartColor = (changeP >= 0) ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
//	self.lineChart.lineColor = lineChartColor;
//	_didLoadChart = YES;
//	self.lineChart.backgroundColor = [UIColor clearColor];
//	[self.lineChart reloadChart];
//}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_listStocks count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return [_expandedCells containsObject:indexPath] ? kEXPANDED_CELL_HEIGHT : kNORMAL_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	StockTypeSingleCell *cell = [tableView dequeueReusableCellWithIdentifier:[StockTypeSingleCell reuseIdentifier]];
	if (!cell) {
		cell = [[StockTypeSingleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.mainDelegate = self;
	cell.indexPath = indexPath;
	cell.isExpandedCell = [_expandedCells containsObject:indexPath];
	cell.showIndex = YES;
	
	//Parse data to UI
	[cell updateData:_listStocks[indexPath.row]];
	[tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
	return cell;
}

#pragma mark - StockTypeSingleCellDelegate
- (void)didTapExpandCell:(StockTypeSingleCell *)cell{
	NSIndexPath *curIndexPath = cell.indexPath;
	BOOL shouldClose = [_expandedCells containsObject:curIndexPath];
	if (shouldClose) {
		[_expandedCells removeObject:curIndexPath];
	}else{
		[_expandedCells addObject:curIndexPath];
	}
	[_tblContent beginUpdates];
	cell.isExpandedCell = !shouldClose;
	[_tblContent endUpdates];
}

- (void)shouldGotoDetailFromCell:(StockTypeSingleCell *)cell{
	[self.view endEditing:YES];
	
	/*
	StockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [StockInfoVC storyboardID]);
	_detailVC.stock = _listStocks[cell.indexPath.row];
	[self nextTo:_detailVC animate:YES];
	*/
	MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
	_detailVC.stock = _listStocks[cell.indexPath.row];
	[self nextTo:_detailVC animate:YES];
}

#pragma mark - GHContextMenuDataSource
- (BOOL)shouldShowMenuAtPoint:(CGPoint)point{
	NSIndexPath* indexPath = [self.tblContent indexPathForRowAtPoint:point];
	StockTypeSingleCell *cell = [self.tblContent cellForRowAtIndexPath:indexPath];
	return cell != nil;
}
- (NSInteger) numberOfMenuItems{
	return [[AppConstants contextMenu] count];
}
- (UIImage*)imageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}

- (UIImage*) highlightedImageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_hl_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_hl_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_hl_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_hl_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_hl_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}
#pragma mark - GHContextMenuDelegate
- (void)didSelectItemAtIndex:(NSInteger) selectedIndex forMenuAtPoint:(CGPoint)point{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[selectedIndex] integerValue];
	
	//Select Stock Model
	NSInteger index;
	NSIndexPath *indexPath = [self.tblContent indexPathForRowAtPoint:point];
	index = indexPath.row;
	StockModel *selectedStock = _listStocks[index];
	switch (menuType) {
		case MContextMenu_Trading:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Buy:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Sell:{
			//Sell here
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Sell;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Info:{
			//Detail here
			MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
			_detailVC.stock = selectedStock;
			[self nextTo:_detailVC animate:YES];
		}
			break;
		case MContextMenu_Favorite:{
			//Add favorite here
			SelectWatchlistPopupVC *_selectVC = NEW_VC_FROM_NIB([SelectWatchlistPopupVC class], [SelectWatchlistPopupVC nibName]);
			_selectVC.stock = selectedStock;
			_selectVC.delegate = self;
			
			[self showPopupWithContent:_selectVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.4 * SCREEN_HEIGHT_PORTRAIT) showInCenter:NO topInsect:80 completion:^(MZFormSheetController *formSheetController) {
				//Do something here
			}];
			/*
			[self showPopupWithContent:_selectVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.4 * SCREEN_HEIGHT_PORTRAIT) completion:^(MZFormSheetController *formSheetController) {
				
			}];
			*/
		}
			break;
		default:
			break;
	}
	DLog(@"Did select Menu Item: %@", @(selectedIndex));
}

#pragma mark - SelectWatchlistPopupVCDelegate
- (void)didSelectStock:(StockModel *)stock willAddToWatchlist:(WatchlistModel*)watchlist{
    NSInteger watchlistID=watchlist.FavID;
    NSString* watchlistName=watchlist.Name;
	//Call api to add watchlist now
	[[ATPAuthenticate singleton] addStock:stock.stockCode toWatchlist:(int)watchlistID completion:^(BOOL success, id result, NSError *error) {
		if (success) {
			NSString *someDuplicate = [result objectForKey:@"DuplicateStocks"];
			if ([someDuplicate length]>0) {
				[[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlistName}]
											 inController:self withTitle:[LanguageManager stringForKey:Duplicate_Stock]];
				[self showCustomAlertWithMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlistName}]];
			}else{
				//Success
				[self postNotification:kDidFinishAddStockToWatchlistNotification object:nil];
				[TCBubbleMessageView showWindowBubbleMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]];
			}
		}else{
			if (error) {
				[TCBubbleMessageView showErrorWindowBubbleMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]];
			}
		}
	}];
}


#pragma mark - Notification
- (void)didUpdate:(NSNotification *)noti{
	dispatch_async(dispatch_get_main_queue(), ^{
		NSString *stockCode = noti.object;
		StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
		if (!changedStock || changedStock == nil) {
			return; //do nothing
		}
		//For Quotes
		NSInteger indexOfQuoteStock = [Utils indexOfStockByCode:stockCode fromArray:self->_listStocks];
		if (self->_listStocks.count > 0 && indexOfQuoteStock < self->_listStocks.count) {
			if (indexOfQuoteStock != -1) {
				NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfQuoteStock inSection:0];
				StockModel *oldStock = [self->_listStocks[indexOfQuoteStock] copy];
				[self->_listStocks replaceObjectAtIndex:indexOfQuoteStock withObject:changedStock];
				StockTypeSingleCell *cell = [self->_tblContent cellForRowAtIndexPath:indexPath];
				if (cell) {
					[cell.contentView makeAnimateBy:oldStock newStock:changedStock completion:^{
						[cell updateData:changedStock];
					}];
				}
			}
		}
	});
}
//Switch Value Change or ChangeP
- (void)shouldSwitchValueChangedNotification:(NSNotification *)noti{
	[self.tblContent reloadData];
}

@end
