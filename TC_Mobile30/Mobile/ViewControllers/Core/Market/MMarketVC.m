//
//  MMarketVC.m
//  TCiPad
//
//  Created by Kaka on 7/16/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MMarketVC.h"
#import "MMarketPieChartView.h"
#import "TCChartView.h"
#import "NSNumber+Formatter.h"
#import "QCData.h"
#import "MarqueeLabel.h"

#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"

@interface MMarketVC ()<UIScrollViewDelegate>{
	TCChartView *_klicChart;//Sm: Summary; Interactive Chart
	MMarketPieChartView *_mainSmPieChart;
	MMarketPieChartView *_mainSbPieChare; //Sb: Scoreboard
	MMarketPieChartView *_aceSbPieChart;
	MMarketPieChartView *_warrantSbPieChart;
	
	//Vertx
	VertxConnectionManager *_vcm;
	
	//ScoreBoard info
	NSInteger scoreBoardUpdateCnt;
	//Self Calculate fields
	int vertxMarketQueryCount;
	
	int data2201;
	int data2203;
	int data2204;
	
	//Data
	NSMutableArray *_listChartViews;
}
@property (nonatomic, strong) NSTimer *scoreBoardTimer;
@end

@implementation MMarketVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	//Init data
    if(!self.isHome){
        [self createLeftMenu];
    }
	_vcm = [VertxConnectionManager singleton];
	[self registerNotification];
	[self createUI];
	//Load Data
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self setupFontAnColor];
	//PageControl
	_scrollChartView.delegate = self;
	_pageControl.currentPage = 0;
	_pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
	_pageControl.pageIndicatorTintColor = RGB(101, 132, 151);
	//Add Chart
	[self prepareChartViews];
	//Update Slice UI
	[self updateSliceControlUIAt:0];
}

- (void)setupLanguage{
	
}
- (void)setupFontAnColor{
	_scrollChartView.backgroundColor = [UIColor clearColor];
	self.view.backgroundColor = COLOR_FROM_HEX(0x364D59);
	_topInfoView.backgroundColor = COLOR_FROM_HEX(0x374d5a);
	
	//Make Shadow view
	_topInfoView.layer.shadowColor = [UIColor blackColor].CGColor;
	_topInfoView.layer.shadowOffset = CGSizeMake(-2, 10);
	_topInfoView.layer.shadowOpacity = 0.3;
	
	//Label
	_lblInsTotalVolume.textColor = AppColor_HighlightTextColor;
	_lblInsTotalTrade.textColor = AppColor_HighlightTextColor;
	_lblInsTotalValue.textColor = AppColor_HighlightTextColor;
}
- (void)updateSliceControlUIAt:(NSInteger)index{
	[self hiddenControlAt:index];
	_lblNameChart.text = [self nameChartAt:index];
}
#pragma mark - UTILS VC
- (void)prepareChartViews{
	_listChartViews = @[].mutableCopy;
	if (!_klicChart) {
		_klicChart = [[TCChartView alloc] initWithFrame:CGRectZero];
		[_listChartViews addObject:_klicChart];
	}
	if (!_mainSmPieChart) {
		_mainSmPieChart = [[MMarketPieChartView alloc]initWithFrame:CGRectZero];
		[_listChartViews addObject:_mainSmPieChart];
	}
	if (!_mainSbPieChare) {
		_mainSbPieChare = [[MMarketPieChartView alloc]initWithFrame:CGRectZero];
		[_listChartViews addObject:_mainSbPieChare];
	}
	if (!_warrantSbPieChart) {
		_warrantSbPieChart = [[MMarketPieChartView alloc]initWithFrame:CGRectZero];
		[_listChartViews addObject:_warrantSbPieChart];
	}
	if (!_aceSbPieChart) {
		_aceSbPieChart = [[MMarketPieChartView alloc]initWithFrame:CGRectZero];
		[_listChartViews addObject:_aceSbPieChart];
	}
	//Add chart into scrollChartView
	[self.view setNeedsUpdateConstraints];
	float HEIGHT_CHART = _scrollChartView.frame.size.height;
	float WIDTH_CHART = SCREEN_WIDTH_PORTRAIT;
	for (int i = 0; i < _listChartViews.count; i ++) {
		UIView *aChart = _listChartViews[i];
		aChart.frame = CGRectMake(i * WIDTH_CHART, 0, WIDTH_CHART, HEIGHT_CHART);
		[_scrollChartView addSubview:aChart];
	}
	
	[_scrollChartView setContentSize:CGSizeMake(_listChartViews.count * WIDTH_CHART, HEIGHT_CHART)];
	[_scrollChartView setBounces:YES];
	_scrollChartView.scrollEnabled = YES;
	[_scrollChartView setShowsHorizontalScrollIndicator:NO];
}

- (void)scrollToPageAtIndex:(NSInteger)index animate:(BOOL)animate{
	CGPoint offset = CGPointMake(index * self.scrollChartView.frame.size.width, 0);
	[self.scrollChartView setContentOffset:offset animated:animate];
}

- (void)hiddenControlAt:(NSInteger)index{
	if (index == 0) {
		_btnPrevious.hidden = YES;
		_btnNext.hidden = NO;
	} else if (index == _listChartViews.count - 1){
		_btnPrevious.hidden = NO;
		_btnNext.hidden = YES;
	}else{
		_btnPrevious.hidden = NO;
		_btnNext.hidden = NO;
	}
}

- (NSString *)nameChartAt:(NSInteger)index{
	NSString *name = @"";
	switch (index) {
		case 0:
			name = [LanguageManager stringForKey:@"KLIC Chart"];
			break;
		case 1:
			name = [LanguageManager stringForKey:@"Summary Chart"];
			break;
		case 2:
			name = [LanguageManager stringForKey:@"Main Chart"];
			break;
		case 3:
			name = [LanguageManager stringForKey:@"Warranty Chart"];
			break;
		case 4:
			name = [LanguageManager stringForKey:@"ACE Chart"];
			break;
		default:
			break;
	}
	return name;
}

//Percent Value with changes
- (void)setPriceChanged:(float)priceChange percentChange:(float)percentChange{
	NSString *aSignPriceChange = @"";
	if (priceChange > 0) {
		aSignPriceChange = @"+";
		_imgUpDownPercent.image = [UIImage imageNamed:AppIcon_Up_Price];
		_lblPercent.textColor = AppColor_PriceUp_TextColor;
		_lblPriceUpDown.textColor = AppColor_PriceUp_TextColor;
	}
	if (priceChange < 0) {
		aSignPriceChange = @"-";
		_imgUpDownPercent.image = [UIImage imageNamed:AppIcon_Down_Price];
		_lblPercent.textColor = AppColor_PriceDown_TextColor;
		_lblPriceUpDown.textColor = AppColor_PriceDown_TextColor;
	}
	if (priceChange == 0) {
		_imgUpDownPercent.image = nil;
		_lblPercent.textColor = AppColor_PriceUnChanged_TextColor;
		_lblPriceUpDown.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor);
	}
	//Show Value
	priceChange = fabsf(priceChange);
	percentChange = fabsf(percentChange * 100);
	_lblPriceUpDown.text = [NSString stringWithFormat:@"%@%@", aSignPriceChange, [[NSNumber numberWithFloat: priceChange] toStringUsingSystemPointer]];
	_lblPercent.text = [NSString stringWithFormat:@"%@%%", [[NSNumber numberWithFloat:percentChange] toStringUsingSystemPointer]];
}

//Parse KLICC data
//Get KLCI Index
- (void)generateKlciIndex
{
	if ([[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]] count]<=0) {
		_lblMarketName.text=@"";
		_lblMarketPrice.text =@"";
		
		return;
	}
	
	if ([[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_39_S_COMPANY] length]<1) {
		_lblMarketName.text= [NSString stringWithFormat:@"%@:",[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]] objectForKey:FID_33_S_STOCK_CODE]];
	} else {
		_lblMarketName.text=[NSString stringWithFormat:@"%@",[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_39_S_COMPANY]];
	}
	float close     = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_50_D_CLOSE]floatValue];
	float lastDone  = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
	
	float chg = lastDone - close;
	float chgPer = chg/close * 100;
	if (close<=0) chgPer = 0;
	
	_lblMarketPrice.text = [[NSNumber numberWithFloat:lastDone] toCurrencyNumber];
	[self setPriceChanged:chg percentChange:chgPer];
	
	//Get Total
	long long totalVolume = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_110_I_SCORE_VOL]longLongValue];
	long long totalVal = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_111_D_SCORE_VAL]longLongValue];
	long long totalScore = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_133_D_SCORE_OF_TRD]longLongValue];
	_lblTotalVolume.text = [[NSNumber numberWithLongLong:totalVolume] toShortcutNumber];
	_lblTotalValue.text = [[NSNumber numberWithLongLong:totalVal] toShortcutNumber];
	_lblTotalTrade.text = [[NSNumber numberWithLongLong:totalScore] toShortcutNumber];
	
	//Data for pie chart
	float upValue  = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_105_I_SCORE_UP] floatValue];
	float downValue = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_106_I_SCORE_DW] floatValue];
	float unChangedValue  = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_107_I_SCORE_UNCHG] floatValue];
	float untradeValue  = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_108_I_SCORE_NOTRD] floatValue];
	
	//populate Chart
	[_mainSmPieChart populateChartFrom:upValue down:downValue untrade:untradeValue unchanged:unChangedValue];
	//KLICC Chart
	_klicChart.isOnlyLoadChartByImage = YES;
	[_klicChart loadMarketChart];
}
#pragma mark - Regiter Notification
- (void)registerNotification{
	[self addNotification:@"DoneGetKLCI" selector:@selector(didFinishedDoneGetKLCI:)];
	[self addNotification:@"subscribeKLCI" selector:@selector(didReceiveFromSubscribeKLCI:)];
	
	//ScoreBoard
	[self addNotification:kVertxNo_doneVertxGetScoreBoardWithExchange selector:@selector(didReceiveScoreBoardNotification:)];
}
#pragma mark - LoadData
- (void)loadData{
	//1: get KLCI
	[_vcm vertxGetKLCI];
	//2: Load ScoreBoard Chart
	[self loadScoreBoard];
}

- (void)loadChart{
	
}

//Load ScroreBoard
- (void)loadScoreBoard{
	vertxMarketQueryCount =0;
	scoreBoardUpdateCnt=1;
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
	// update timer
	if (_scoreBoardTimer!=nil) {
		[_scoreBoardTimer invalidate];
		_scoreBoardTimer = nil;
	}
	_scoreBoardTimer = [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(updateScoreBoard) userInfo:nil
													   repeats:YES];
}



#pragma mark - ACTIONS

- (IBAction)onPreviousAction:(id)sender {
	NSInteger curPage = _pageControl.currentPage;
	--curPage;
	[self scrollToPageAtIndex:curPage animate:YES];
	_pageControl.currentPage = curPage;
	//Update Slice UI: no need again because of using at ScollViewDidScroll delegate
	//[self updateSliceControlUIAt:curPage];
}
- (IBAction)onNextAction:(id)sender {
	NSInteger curPage = _pageControl.currentPage;
	++curPage;
	[self scrollToPageAtIndex:curPage animate:YES];
	_pageControl.currentPage = curPage;
	//Update Slice UI: no need again because of using at ScollViewDidScroll delegate
	//[self updateSliceControlUIAt:curPage];
}
- (IBAction)onPageControlValueChanged:(id)sender {
	[self scrollToPageAtIndex:_pageControl.currentPage animate:YES];
	//Update Slice UI
	//[self updateSliceControlUIAt:_pageControl.currentPage];
}

-(void)updateScoreBoard {
	scoreBoardUpdateCnt++;
	if (scoreBoardUpdateCnt>=100) scoreBoardUpdateCnt = 10;
	
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"];
	[_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	static NSInteger previousPage = 0;
	CGFloat pageWidth = scrollView.frame.size.width;
	float fractionalPage = scrollView.contentOffset.x / pageWidth;
	NSInteger page = lround(fractionalPage);
	if (previousPage != page) {
		// Page has changed, do your thing!
		// ...
		// Finally, update previous page
		previousPage = page;
	}
	_pageControl.currentPage = page;
	//Update Slice UI
	[self updateSliceControlUIAt:page];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	CGFloat xOffset = scrollView.contentOffset.x;
	int currentPage = floor(xOffset / scrollView.bounds.size.width);
	DLog(@"XOffset: %@", @(xOffset));
	DLog(@"Current Page: %@", @(currentPage));
	if (xOffset >= currentPage + SCREEN_WIDTH_PORTRAIT) {
		// User scrolled to right.
		//[self scrollEffectInDirection:kScrollDirectionRight];
		[self scrollToPageAtIndex:currentPage + 1 animate:YES];
		_pageControl.currentPage = 1;
	}else if (xOffset <= currentPage - SCREEN_WIDTH_PORTRAIT) {
		// User scrolled to left.
		//[self scrollEffectInDirection:kScrollDirectionLeft];
		[self scrollToPageAtIndex:currentPage - 1 animate:YES];
		_pageControl.currentPage = 0;
	}else{
		[self scrollToPageAtIndex:_pageControl.currentPage animate:YES];
	}
}

#pragma mark - Notification
- (void)didFinishedDoneGetKLCI:(NSNotification *)notification{
	[self generateKlciIndex];
}

- (void)didReceiveFromSubscribeKLCI:(NSNotification *)notification{
	[self generateKlciIndex];
}

//ScoreBoard
- (void)didReceiveScoreBoardNotification:(NSNotification *)noti{
	if ([[[QCData singleton]qcScoreBoardDict] count] <= 0) {
		return;
	}
	// CIMB SG sends 2201 twice and 2203. 2204 data missing.
	NSDictionary *mainMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2201]];
	NSDictionary *warrantMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2203]];
	NSDictionary *aceMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2204]];
	DLog(@"ScoreBoard: M - %@, W - %@, ACE - %@", mainMarketDict, warrantMarketDict, aceMarketDict);
	
	if ([mainMarketDict count]>0) data2201 = 1; else data2201 = 0;
	if ([warrantMarketDict count]>0) data2203 = 1; else data2203 = 0;
	if ([aceMarketDict count]>0) data2204 = 1; else data2204 = 0;
	
	vertxMarketQueryCount  = data2201+data2203+data2204;
	if(vertxMarketQueryCount == 3) // 3 = Main, Warrant and Ace.
	{
		vertxMarketQueryCount =0; // reset
	}
	
	//Populate Chart
	[_mainSbPieChare populateChartFromDict:mainMarketDict];
	[_warrantSbPieChart populateChartFromDict:warrantMarketDict];
	[_aceSbPieChart populateChartFromDict:aceMarketDict];
}
@end
