//
//  MDaseBoardVCV3.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MDashBoardVCV3.h"
#import "MIndicesVC.h"
#import "MPortfolioDashBoardViewController.h"
#import "MWatchListDashBoard.h"
#import "NewsVCDashBoard.h"

#import "IndicesView.h"
#import "PortfolioView.h"
#import "WatchListView.h"
#import "AllNewsView.h"
#import "EditingAlertVC.h"
#import "AlertController.h"
#import "StatusAlertVC.h"
#import "SearchStockVC.h"

#import "UIViewController+TCUtil.h"
#import "IndicesModel.h"
#import "Utils.h"
#import "StockModel.h"
#import "DashBoardCornerView.h"
#import "AlertVC.h"
#import "SettingAlertVC.h"
#import "UIView+ShadowBorder.h"
#import "LanguageKey.h"
#import "HeaderDBView.h"
#import "TCBubbleMessageView.h"
#import "AssetManagementAPI.h"
#define kHeaderHeight 120
#define kIndiciesHeight 404
#define kPortfolioHeight 270
#define kWatchlistHeight 380
#define kAllNewsHeight 370
#define kSpacing 6
@interface MDashBoardVCV3 () <EditingAlertVCDelegate, MPortfolioViewControllerDelegate, PortfolioViewDelegate, MIndicesVCDelegate>
{
    MPortfolioDashBoardViewController *_portfolioVC;
    MIndicesVC *_indicesVC;
    BOOL isFistChangePortfolioLayout;
    TCBubbleMessageView *_bubbleMsgView;
}
@property (weak, nonatomic) IBOutlet UIStackView *sv_Container;

@property (strong, nonatomic) HeaderDBView *v_header;

@property (strong, nonatomic) DashBoardCornerView *v_ContainerIndices;
@property (strong, nonatomic) IndicesView *v_Indices;


@property (strong, nonatomic) DashBoardCornerView *v_ContainerPortfolio;
@property (strong, nonatomic) PortfolioView *v_Portfolio;


@property (strong, nonatomic) DashBoardCornerView *v_ContainerWatchList;
@property (strong, nonatomic) WatchListView *v_WatchList;

@property (strong, nonatomic) DashBoardCornerView *v_ContainerNews;
@property (strong, nonatomic) AllNewsView *v_AllNews;




@property (strong, nonatomic) NSLayoutConstraint *cst_PortfolioHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ViewHeight;

@end

@implementation MDashBoardVCV3

- (void)viewDidLoad {
    [super viewDidLoad];
    _bubbleMsgView = [TCBubbleMessageView new];
    [self initViews];
    self.title = [LanguageManager stringForKey:_Dashboard];
    [self createLeftMenu];
    
    [self configSubViews];
}
- (void)initViews{
    self.sv_Container.spacing = kSpacing;
    NSArray *dashBoardComponents = [UserPrefConstants singleton].dashBoard;
    if(dashBoardComponents.count > 0){
        CGFloat heightTotal = 0;
        for(int index = 0; index < dashBoardComponents.count; index++){
            NSString *key = dashBoardComponents[index];
            if([key isEqualToString:@"Header"]){
                heightTotal += kHeaderHeight;
                [self addHeader:index == dashBoardComponents.count - 1];
            }else if([key isEqualToString:@"Indicies"]){
                heightTotal += kIndiciesHeight;
                [self addIndicies:index == dashBoardComponents.count - 1];
            }else if([key isEqualToString:@"Portfolio"]){
                heightTotal += kPortfolioHeight;
                [self addPortfolio:index == dashBoardComponents.count - 1];
            }else if([key isEqualToString:@"WatchList"]){
                heightTotal += kWatchlistHeight;
                [self addWatchList:index == dashBoardComponents.count - 1];
            }else if([key isEqualToString:@"News"]){
                heightTotal += kAllNewsHeight;
                [self addNews:index == dashBoardComponents.count - 1];
            }
        }
        self.cst_ViewHeight.constant = heightTotal + (kSpacing*dashBoardComponents.count);
    }else{
        self.cst_ViewHeight.constant = kHeaderHeight + kIndiciesHeight + kPortfolioHeight + kWatchlistHeight + kAllNewsHeight + kSpacing*5;
        [self addDefaultComponents];
    }
    [self updateTheme:nil];
}
- (void)addDefaultComponents{
    [self addHeader:NO];
    [self addIndicies:NO];
    [self addPortfolio:NO];
    [self addWatchList:NO];
    [self addNews:YES];
}
- (NSLayoutConstraint *)addContainerViews:(UIView *)subView constraintHeight:(CGFloat)height isLastComponent:(BOOL)isLastComponent{
    [self.sv_Container addArrangedSubview:subView];
    
    if(!isLastComponent){
        subView.translatesAutoresizingMaskIntoConstraints = NO;
        NSLayoutConstraint *heightConstrain = [NSLayoutConstraint constraintWithItem:subView
                                                                           attribute:NSLayoutAttributeHeight
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:nil
                                                                           attribute: NSLayoutAttributeNotAnAttribute
                                                                          multiplier:1
                                                                            constant:height];
        [self.sv_Container addArrangedSubview:subView];
        [self.sv_Container addConstraint:heightConstrain];
        return heightConstrain;
    }
    return nil;
}
- (void)addSubView:(UIView *)subView parent:(UIView *)parent isLastComponent:(BOOL)isLastComponent needPadding:(BOOL)needPadding{
    [parent addSubview:subView];
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:subView
                                                                      attribute:NSLayoutAttributeTop
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:parent
                                                                      attribute: NSLayoutAttributeTop
                                                                     multiplier:1
                                                            constant:needPadding == YES ? kSpacing/4 : 0];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:subView
                                                                       attribute:NSLayoutAttributeLeft
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:parent
                                                                       attribute: NSLayoutAttributeLeft
                                                                      multiplier:1
                                                             constant:needPadding == YES ? kSpacing : 0];
    NSLayoutConstraint *bottom =  [NSLayoutConstraint constraintWithItem:subView
                                                                attribute:NSLayoutAttributeBottom
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:parent
                                                                attribute:NSLayoutAttributeBottom
                                                              multiplier:1.f constant:needPadding == YES ? -kSpacing/4 : 0];
    NSLayoutConstraint *right =  [NSLayoutConstraint constraintWithItem:subView
                                                                attribute:NSLayoutAttributeRight
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:parent
                                                                attribute:NSLayoutAttributeRight
                                                               multiplier:1.f constant: needPadding == YES ? -kSpacing : 0];
    [parent addConstraints:@[top, left, bottom, right]];
}
- (void)addHeader:(BOOL)isLastComponent{
    self.v_header = [[HeaderDBView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addContainerViews:self.v_header constraintHeight:kHeaderHeight isLastComponent:isLastComponent];
}
- (void)addIndicies:(BOOL)isLastComponent{
    self.v_ContainerIndices = [[DashBoardCornerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    self.v_Indices = [[IndicesView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    
    UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addContainerViews:tmpView constraintHeight:kIndiciesHeight isLastComponent:isLastComponent];
    [self addSubView:self.v_ContainerIndices parent:tmpView isLastComponent:isLastComponent needPadding:YES];
    [self addSubView:self.v_Indices parent:self.v_ContainerIndices isLastComponent:isLastComponent needPadding:NO];
}
- (void)addPortfolio:(BOOL)isLastComponent{
    self.v_ContainerPortfolio = [[DashBoardCornerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    self.v_Portfolio = [[PortfolioView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    
    self.cst_PortfolioHeight = [self addContainerViews:tmpView constraintHeight:kPortfolioHeight isLastComponent:isLastComponent];
    [self addSubView:self.v_ContainerPortfolio parent:tmpView isLastComponent:isLastComponent needPadding:YES];
    [self addSubView:self.v_Portfolio parent:self.v_ContainerPortfolio isLastComponent:isLastComponent needPadding:NO];
}
- (void)addWatchList:(BOOL)isLastComponent{
    self.v_ContainerWatchList = [[DashBoardCornerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addContainerViews:tmpView constraintHeight:kWatchlistHeight isLastComponent:isLastComponent];
    [self addSubView:self.v_ContainerWatchList parent:tmpView isLastComponent:isLastComponent needPadding:YES];
    self.v_WatchList = [[WatchListView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addSubView:self.v_WatchList parent:self.v_ContainerWatchList isLastComponent:isLastComponent needPadding:NO];
}
- (void)addNews:(BOOL)isLastComponent{
    self.v_ContainerNews = [[DashBoardCornerView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addContainerViews:tmpView constraintHeight:kAllNewsHeight isLastComponent:isLastComponent];
    [self addSubView:self.v_ContainerNews parent:tmpView isLastComponent:isLastComponent needPadding:YES];
    
    self.v_AllNews = [[AllNewsView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self addSubView:self.v_AllNews parent:self.v_ContainerNews isLastComponent:isLastComponent needPadding:NO];
}
- (void)configSubViews{
    _indicesVC = NEW_VC_FROM_STORYBOARD(kMIndicesStoryboardName, @"MIndicesVCDashBoard");
    _indicesVC.delegate = self;
    _indicesVC.isDashBoardMode = YES;
    [self addVC:_indicesVC toView:self.v_Indices.v_Content];
    
    
    _portfolioVC = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, @"MPortfolioVCDashBoardNew");
    _portfolioVC.isDashBoardMode = YES;
    _portfolioVC.delegate = self;
    self.v_Portfolio.delegate = self;
    [self addVC:_portfolioVC toView:self.v_Portfolio.v_Content];
    
    
    MWatchListDashBoard *_watchlistVC = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [MWatchListDashBoard storyboardID]);
    _watchlistVC.isDashBoard = YES;
    [self addVC:_watchlistVC toView:self.v_WatchList.v_Content];
    
    
    NewsVCDashBoard *_newsVC = NEW_VC_FROM_NIB([NewsVCDashBoard class], [NewsVCDashBoard nibName]);
    [self addVC:_newsVC toView:self.v_AllNews.v_Content];
}




#pragma mark EditingAlertVCDelegate
- (void)didSelectClose:(NSIndexPath *)index{
    [self dismissPopup:YES completion:nil];
}
- (void)didSelectApply:(NSIndexPath *)index alert:(AlertModel *)alert isNew:(BOOL)isNew{
    [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
        [AlertController addAlert:alert completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(!error){
                [self showStatus: [LanguageManager stringForKey:@"Alert for this stock has been made. You can manage your alerts at the settings page"]];
            }else{
                [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedDescription];
            }
        }];
    }];
}
- (void)showStatus:(NSString *)content{
//    StatusAlertVC *status = NEW_VC_FROM_NIB([StatusAlertVC class], @"StatusAlertVC");
//    status.message = content;
//    CGSize contentSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 350);
//    [self showPopupWithContent:status inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
//    }];
}

- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
    //Some default value
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_bg);
    self.v_header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    self.v_header.lbl_title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_headerTextColor);
    self.v_header.lbl_subtitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_headerSubtextColor);
    self.v_ContainerNews.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    self.v_ContainerIndices.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    self.v_ContainerPortfolio.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    self.v_ContainerWatchList.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
	
//    [self.v_header makeBorderShadow];
}

#pragma mark MPortfolioViewControllerDelegate
- (void)didScrollTo:(NSInteger)page{
    self.v_Portfolio.page_Portfolio.currentPage = page;
}
- (void)didChangeLayout:(CGFloat)value{
    if(value != 0 && !isFistChangePortfolioLayout){
        isFistChangePortfolioLayout = YES;
        self.cst_PortfolioHeight.constant = self.cst_PortfolioHeight.constant - value;
        self.cst_ViewHeight.constant = self.cst_ViewHeight.constant - value;
    }
    
}

#pragma mark PortfolioViewDelegate
- (void)didChangePortfolioPage:(NSInteger)currentPage{
    [_portfolioVC scrollToPage:currentPage];
}

#pragma mark MIndicesVCDelegate
- (void)didShowError:(NSString *)content autoHide:(BOOL)autoHide type:(NSInteger)type{
    [_bubbleMsgView removeFromSuperview];
    _bubbleMsgView = [TCBubbleMessageView new];
    [_bubbleMsgView showBubbleMessage:content inView:self.view withType:type autoHidden:autoHide];
}
- (void)hideError{
    [_bubbleMsgView hide];
}
@end
