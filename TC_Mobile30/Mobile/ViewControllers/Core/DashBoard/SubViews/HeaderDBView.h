//
//  HeaderDBView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HeaderDBView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subtitle;
@end

NS_ASSUME_NONNULL_END
