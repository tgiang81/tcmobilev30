//
//  PortfolioView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PortfolioView.h"
#import "ThemeManager.h"
@implementation PortfolioView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib{
    [super awakeFromNib];
}
- (void)setupView{
    [super setupView];
    self.page_Portfolio.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.page_Portfolio.currentPageIndicatorTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_MarketCellText);
}
- (IBAction)changePortfolioPage:(UIPageControl *)sender {
    if ([self.delegate respondsToSelector:@selector(didChangePortfolioPage:)]) {
        [self.delegate didChangePortfolioPage:sender.currentPage];
    }
}
@end
