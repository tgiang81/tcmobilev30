//
//  MWatchListDashBoard.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "MWatchlistVC.h"
NS_ASSUME_NONNULL_BEGIN
@class TCMenuDropDown;
@interface MWatchListDashBoard : MWatchlistVC
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDown;
@property (weak, nonatomic) IBOutlet UIView *v_Detail;
@property (weak, nonatomic) IBOutlet UIButton *btn_Add;
@property (weak, nonatomic) IBOutlet UILabel *lbl_WatchListTitle;

@end

NS_ASSUME_NONNULL_END
