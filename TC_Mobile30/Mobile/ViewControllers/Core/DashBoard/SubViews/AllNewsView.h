//
//  AllNewsView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCBaseSubView.h"
NS_ASSUME_NONNULL_BEGIN

@interface AllNewsView : TCBaseSubView
@property (weak, nonatomic) IBOutlet UIView *v_Content;
@end

NS_ASSUME_NONNULL_END
