//
//  PortfolioView.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCBaseSubView.h"
NS_ASSUME_NONNULL_BEGIN
@protocol PortfolioViewDelegate<NSObject>
- (void)didChangePortfolioPage:(NSInteger)currentPage;
@end
@interface PortfolioView : TCBaseSubView
@property (weak, nonatomic) id<PortfolioViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *v_Content;
@property (weak, nonatomic) IBOutlet UIPageControl *page_Portfolio;
@end

NS_ASSUME_NONNULL_END
