//
//  NewsVCDashBoard.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "NewsVCDashBoard.h"
#import "TCMenuDropDown.h"
#import "ResearchDashBoardCell.h"
#define kHeightNewsV3Cell                115

@interface NewsVCDashBoard () <TCMenuDropDownDelegate, UITableViewDataSource, UITabBarDelegate>
{
    NSInteger _maxItems;
    NSInteger _currentPage;
    NSInteger _maxPage;
}
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDownType;

@end

@implementation NewsVCDashBoard

- (void)viewDidLoad {
    [super viewDidLoad];
    _maxItems = 4;
    _currentPage = 0;
    [self setupDropDown];
    self.lbl_Title.text = [LanguageManager stringForKey:@"All News"];
    self.tblContent.estimatedRowHeight = kHeightNewsV3Cell;
    [self.tblContent registerNib:[UINib nibWithNibName:[ResearchDashBoardCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[ResearchDashBoardCell reuseIdentifier]];
    self.tblContent.estimatedRowHeight = kHeightNewsV3Cell;
    self.tblContent.alwaysBounceVertical = YES;
}

- (void)setupDropDown{
    //    self.v_DropDownType.radius = 12;
    self.v_DropDownType.selectedLabelFont = AppFont_MainFontBoldWithSize(20);
    self.v_DropDownType.height = 60;
    self.v_DropDownType.titleAlign = NSTextAlignmentLeft;
    self.v_DropDownType.titleMenu = @"All News";
    self.v_DropDownType.menuTitles = @[@"All News", @"All Rechearches"];
    self.v_DropDownType.numberItemVisible = 6;
    self.v_DropDownType.radius  = 12;
    self.v_DropDownType.delegate = self;
    [self.v_DropDownType reloadDropDown];
    
}

- (void)reloadContent{
    [self.tblContent reloadData];
}

#pragma mark TCMenuDropDown
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
    [self reloadDataWith:index];
}

#pragma mark Actions
- (void)checkEnableAction{
    if(_maxPage == 0){
        self.btn_next.enabled = NO;
        self.btn_back.enabled = NO;
    }else{
        self.btn_next.enabled = (_currentPage < _maxPage - 1);
        self.btn_back.enabled = _currentPage  > 0;
    }
    
}
- (IBAction)refresh:(id)sender {
    [self reloadDataWith:0];
}

- (IBAction)next:(UIButton *)sender {
    if(_currentPage < _maxPage - 1){
        _currentPage++;
        [self reloadContent];
    }
    [self checkEnableAction];
}
- (IBAction)back:(UIButton *)sender {
    if(_currentPage > 0){
        _currentPage--;
        [self reloadContent];
    }
    [self checkEnableAction];
}

- (void)didReloadData{
    NSInteger numberOfItems = [self.items count];
    if(numberOfItems%_maxItems == 0){
        _maxPage = numberOfItems/_maxItems;
    }else{
        _maxPage = (numberOfItems/_maxItems) + 1;
    }
    [self checkEnableAction];
}
#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(_currentPage == (_maxPage - 1) && self.items.count > 0){
        
        return  [self.items count] - _currentPage*_maxItems;
    }
    if(self.items.count > _maxItems - 1){
        return _maxItems;
    }
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger currentIndex = _currentPage*_maxItems + indexPath.row;
    ResearchDashBoardCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResearchDashBoardCell reuseIdentifier] forIndexPath:indexPath];
    [cell setupDataFrom:self.items[currentIndex]];
    return cell;
}

- (void)updateTheme:(NSNotification *)noti{
    self.v_BottomView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
//    self.tblContent.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_NewsBgCell);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
    [self.tblContent reloadData];
    self.btn_next.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_tintColorIconDropDown);
    self.btn_back.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_tintColorIconDropDown);
    self.btn_refresh.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_tintColorIconDropDown);
    _v_DropDownType.tintIcon = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_tintColorIconDropDown);
    _v_DropDownType.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_bgDropDown);
    _v_DropDownType.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColorDropDown);
    _v_DropDownType.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_bgDropDown);
    _v_DropDownType.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColorItemDropDown );
}
@end
