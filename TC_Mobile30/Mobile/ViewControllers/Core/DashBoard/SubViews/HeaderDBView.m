//
//  HeaderDBView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "HeaderDBView.h"
#import "UserSession.h"
@implementation HeaderDBView

- (void)awakeFromNib{
    [super awakeFromNib];
    
}
- (void)setupView{
    [super setupView];
    self.lbl_title.text = [NSString stringWithFormat:@"%@,\n%@", [LanguageManager stringForKey:@"Hello"], [UserSession shareInstance].userName];
    self.lbl_subtitle.text = [LanguageManager stringForKey:@"Here's what's happening today."];
}
@end
