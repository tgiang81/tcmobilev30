//
//  NewsVCDashBoard.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "ResearchVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface NewsVCDashBoard : ResearchVC
@property (weak, nonatomic) IBOutlet UIView *v_BottomView;
@property (weak, nonatomic) IBOutlet UIButton *btn_next;
@property (weak, nonatomic) IBOutlet UIButton *btn_back;
@property (weak, nonatomic) IBOutlet UIButton *btn_refresh;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;

@end

NS_ASSUME_NONNULL_END
