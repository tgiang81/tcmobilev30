//
//  MWatchListDashBoard.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MWatchListDashBoard.h"
#import "UIViewController+TCUtil.h"
#import "DetailWatchlistVC.h"
#import "TCMenuDropDown.h"
#import "NSString+Watchlist.h"
@interface MWatchListDashBoard () <TCMenuDropDownDelegate>
{
    DetailWatchlistVC *_detailVC;
    WatchlistModel *selectedWatchList;
    BOOL _isSetup;
    NSInteger selectedWatchListIndex;
}
@end

@implementation MWatchListDashBoard

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setEnableTapDismissKeyboard:NO];
    self.lbl_WatchListTitle.text = [LanguageManager stringForKey:@"Watchlist"];
    _detailVC = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, @"DetailWatchlistVCDashBoard");
    _detailVC.isDashBoard = YES;
    [self addVC:_detailVC toView:self.v_Detail];
    [self addNotifications];
    // Do any additional setup after loading the view.
}
- (void)addNotifications{
    
    [self addNotification:@"reloadWatchListAt" selector:@selector(reloadWatchListAtAPI:)];
    [self addNotification:@"didEditWatchList" selector:@selector(didEditWatchListAPI:)];
    [self addNotification:@"didAddWatchList" selector:@selector(didAddWatchListAPI:)];
    [self addNotification:@"didDeleteWatchList" selector:@selector(didDeleteWatchListAPI:)];
}
- (void)reloadWatchListAtAPI:(NSNotification *)noti{
    NSNumber *index = noti.object;
    [self setupWatchListDetail:[index integerValue]];
}
- (void)didEditWatchListAPI:(NSNotification *)noti{
    WatchlistModel *model = noti.object;
    [self editWatchList:model];
    [self reloadTableView];
}
- (void)didAddWatchListAPI:(NSNotification *)noti{
    [self didLoadWatchlist:[self getListWatchList]];
}
- (void)didDeleteWatchListAPI:(NSNotification *)noti{
    WatchlistModel *model = noti.object;
    [self removeWatchList:model];
    [self reloadTableView];
    [self didLoadWatchlist:[self getListWatchList]];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)didLoadWatchlist:(NSMutableArray *)watchList{
    [self setupDropDown:watchList];
    
}
- (void)setupDropDown:(NSMutableArray *)watchList{
    if(_isSetup == NO){
        _isSetup = YES;
        self.v_DropDown.titleAlign = NSTextAlignmentLeft;
        self.v_DropDown.height = 60;
        self.v_DropDown.delegate = self;
        self.v_DropDown.radius  = 12;
    }
    _detailVC.watchlistArr = watchList;
    self.v_DropDown.menuTitles = [self getWatchListName:watchList];
    self.v_DropDown.numberItemVisible = 6;
    
    NSString *watchListDefault = selectedWatchList.Name;
    selectedWatchListIndex = [self getIndexOfWatchListName:watchListDefault watchList:watchList];
    if(!watchListDefault || selectedWatchListIndex == -1){
        watchListDefault = [NSString getDefaultWatchlist];
    }
    if(selectedWatchListIndex == -1){
        selectedWatchListIndex = [self getIndexOfWatchListName:watchListDefault watchList:watchList];
    }
    
    if(watchListDefault){
        self.v_DropDown.titleMenu = watchListDefault;
    }else{
        if(watchList.count > 0){
            selectedWatchListIndex = 0;
            WatchlistModel *model = watchList[0];
            self.v_DropDown.titleMenu = model.Name;
        }else{
            self.v_DropDown.titleMenu = @"Watch List";
        }
    }
    [self.v_DropDown reloadDropDown];
    [self setupWatchListDetail:selectedWatchListIndex];
    
}
- (NSInteger)getIndexOfWatchListName:(NSString *)name watchList:(NSMutableArray *)watchList{
    for(int index = 0 ; index < watchList.count; index ++){
        WatchlistModel *model = watchList[index];
        if([name isEqualToString:model.Name]){
            return index;
        }
    }
    return -1;
}
- (NSMutableArray *)getWatchListName:(NSMutableArray *)watchList{
    NSMutableArray *names = [NSMutableArray new];
    for(WatchlistModel *model in watchList){
        [names addObject:model.Name];
    }
    return names;
}
#pragma mark TCMenuDropDown
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
    [self setupWatchListDetail:index];
}
- (void)setupWatchListDetail:(NSInteger)index{
    if(selectedWatchListIndex != -1){
        selectedWatchList = [self getWatchListAt:index];
        selectedWatchListIndex = index;
        _detailVC.curWatchlistIndex = index;
        [_detailVC loadData];
    }
    
}

- (IBAction)addNewWatchList:(UIButton *)sender {
    [self addWatchlistAction:sender];
}

- (void)updateTheme:(NSNotification *)noti{
    self.lbl_WatchListTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
    [self.btn_Add setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_tintColorIconDropDown)];
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    _detailVC.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    _detailVC.tblContent.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    
    _v_DropDown.tintIcon = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_subItemTextColor);
    _v_DropDown.selectedLabelFont = AppFont_MainFontBoldWithSize(16);
    _v_DropDown.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_bgDropDown);
    _v_DropDown.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_subItemTextColor);
    _v_DropDown.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_bgDropDown);
    _v_DropDown.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_subItemTextColor);
}

@end
