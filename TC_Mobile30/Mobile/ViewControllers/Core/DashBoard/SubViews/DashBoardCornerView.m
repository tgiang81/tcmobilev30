//
//  DashBoardCornerView.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "DashBoardCornerView.h"
#import "UIView+ShadowBorder.h"
@implementation DashBoardCornerView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        [self setupDashBoardShashow];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupDashBoardShashow];
    }
    return self;
}
@end
