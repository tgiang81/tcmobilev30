//
//  MDBWatchlist.h
//  TC_Mobile30
//
//  Created by Michael Seven on 10/30/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "TCMenuDropDown.h"

@class WatchlistModel;
@interface MDBWatchlist : BaseVC

@property (weak, nonatomic) IBOutlet UILabel *lblWatchlistName;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

//Parsing data
@property (strong, nonatomic) WatchlistModel *watchlist;

@end
