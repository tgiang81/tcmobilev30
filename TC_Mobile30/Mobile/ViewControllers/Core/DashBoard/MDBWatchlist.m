//
//  MDBWatchlist.m
//  TC_Mobile30
//
//  Created by Michael Seven on 10/30/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MDBWatchlist.h"
#import "NSIndexSet+Array.h"
#import "NSArray+SKTaskLoop.h"
#import "ATPAuthenticate.h"
#import "WatchlistModel.h"
#import "NSString+Watchlist.h"
#import "QCData.h"
#import "DBStockCell.h"
#import "AppState.h"
#import "LanguageKey.h"
#define kFULL_CELL_HEIGHT 30

@interface MDBWatchlist () <UITableViewDataSource, UITableViewDelegate, TCMenuDropDownDelegate> {

    NSMutableArray *_watchlistArr; //Watchlist info array
    NSMutableArray *_items; //Watchist item
}

@end

@implementation MDBWatchlist

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _items =@[].mutableCopy;
    
    //Register DBStockCell
    [self.tblContent registerNib:[UINib nibWithNibName:[DBStockCell nibName] bundle:nil] forCellReuseIdentifier:[DBStockCell reuseIdentifier]];
    
    //set DataSource and Delegate for tableView
    _tblContent.delegate = self;
    _tblContent.dataSource = self;
    [_tblContent setAllowsSelection:NO];
    
    //Register notification
    [self registerNotification];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadWatchlistArr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LoadWatchlist
//Load Watchlist array
- (void)loadWatchlistArr{
    [[AppState sharedInstance] addLoadingView:self.view];
    [[ATPAuthenticate singleton] getWatchlistWithCompletion:^(BOOL success, id result, NSError *error) {
        [[AppState sharedInstance] removeLoadingView:self.view];
        if (success) {
            self->_watchlistArr = [WatchlistModel arrayFromDicts:result];
            [self loadWatchlistDef];
        }else{
            [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedDescription];
        }
    }];
}

//Load Watchlist default item
- (void)loadWatchlistDef{
    //Get defaultWatchlist from Store
    NSString *defaultWatchlist = [NSString getDefaultWatchlist];
    
    //Set defaultWatchlist is the first Watchlist array if the default watchlist is not set.
    if (!defaultWatchlist) {
        _watchlist = [_watchlistArr objectAtIndex:0];
    }
    
    //Set watchlist is read from Store to watchlist default
    for (int index = 0; index < _watchlistArr.count; index++) {
        if ([[_watchlistArr[index] Name] isEqualToString:defaultWatchlist]) {
            _watchlist = [_watchlistArr objectAtIndex:index];
            break;
        }
    }
    
    //Set Wachlist label
    _lblWatchlistName.text = _watchlist.Name;
    
    //[Utils showApplicationHUD];
    int favID = (int) _watchlist.FavID;
    [UserPrefConstants singleton].prevWatchlistFavID = favID;
    [UserPrefConstants singleton].watchListName = _watchlist.Name;
    [UserPrefConstants singleton].quoteScreenFavID = [NSNumber numberWithInt:favID];
    [[ATPAuthenticate singleton] getWatchListItems:favID];
}



#pragma mark - UTILS
//Add Notification
- (void)registerNotification{
    //Update new data
    [self addNotification:kVertxNo_didReceivedWatchListItems selector:@selector(didReciveNotification:)];
    [self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
}
#pragma mark - Notification
- (void)didReciveNotification:(NSNotification *)noti{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *_nameNotification = noti.name;
        if ([_nameNotification isEqualToString:kVertxNo_didReceivedWatchListItems]) {
            NSArray *wlistStkCodeArr = [[NSArray alloc]initWithArray:[UserPrefConstants singleton].userWatchListStockCodeArr copyItems:YES];
            [self->_items removeAllObjects];
            DLog(@"+++ Watchlist Result: %@", wlistStkCodeArr);
            for (NSString *stkCode in wlistStkCodeArr)
            {
                if (([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
                    NSDictionary *dictToAdd = [[NSDictionary alloc] initWithDictionary:
                                               [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];
                    //[dictToSort setObject:dictToAdd forKey:stkCode];
                    StockModel *stock = [[StockModel alloc] initWithDictionary:dictToAdd error:nil];
                    if (stock) {
                        [self->_items addObject:stock];
                    }
                }
            }
            //Reload data here
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_tblContent reloadData];
                //[self showStatusData:(self->_items.count == 0) message:nil];
                //[Utils dismissApplicationHUD];
            });
        }
    });
}

////Did Update newest stock
- (void)didUpdate:(NSNotification *)noti{
    //Push data to background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *stockCode = noti.object;
        StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
        if (!changedStock) {
            return; //do nothing
        }
        //Update for TopRank
        NSInteger indexOfItems = [Utils indexOfStockByCode:stockCode fromArray:self->_items];
        if (indexOfItems != -1) {
            //Replace to new object and beginning update data
            [self->_items replaceObjectAtIndex:indexOfItems withObject:changedStock];
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:indexOfItems inSection:0];
            //Update UI here
            dispatch_async(dispatch_get_main_queue(), ^{
                DBStockCell *cell = (DBStockCell *)[self->_tblContent cellForRowAtIndexPath:indexPath];
                if(cell) {
                    [cell setupDataFrom:changedStock];
                }
            });
        }
    });
}

#pragma - UITableViewDataSource

- (CGFloat)tableView: (UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return kFULL_CELL_HEIGHT;
}
- (UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath {
    DBStockCell *cell = [tableView dequeueReusableCellWithIdentifier:[DBStockCell reuseIdentifier]];
    if(!cell) {
        cell = [[DBStockCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[DBStockCell reuseIdentifier]];
    }
    [cell setupDataFrom:_items[indexPath.row]];
    //***Check if cell is null -> avoid Crash App
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_items count];
}

@end
