//
//  ChartVC.m
//  TCiPad
//
//  Created by Kaka on 4/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ChartVC.h"
#import "TCChartView.h"
#import <UIKit/UIKit.h>

@interface ChartVC ()<TCChartViewDelegate, UIGestureRecognizerDelegate>{
	//TCChartView *_chart;
}

@end

@implementation ChartVC
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		//Set this for show like popup
		/*
		self.providesPresentationContextTransitionStyle = YES;
		[self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
		self.definesPresentationContext = YES;
		
		self.view.backgroundColor = RGBA(0, 0, 0, 0.3);
		_viewMainContent.backgroundColor = RGBA(0, 0, 0, 0.3);
		 */
		//self.providesPresentationContextTransitionStyle = YES;
		//[self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
	}
	return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self createUI];
	
	/*
	UITapGestureRecognizer	*_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
		_tap.numberOfTapsRequired = 2;
	_tap.delegate = self;
	[self.view addGestureRecognizer:_tap];
	 */
	//_viewMainContent.alpha = 0.0;
	[self showChart];
}
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
//	//Force Lanscapde mode
//	[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeLeft) forKey:@"orientation"];
//	[UINavigationController attemptRotationToDeviceOrientation];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	//Animate show Chart
	//[self showChart];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Main
- (void)showChart{
	if (!self.chartType) {
		self.chartType = ChartType_3Month; //Make default
	}
	//Configure
	_tcChart.chartType = _chartType;
	_tcChart.isOnlyLoadChartByImage = _isOnlyImageChart;
	_tcChart.lockMode = NO;
	_tcChart.stCode = _stockCode;
	[_tcChart startLoadDefaultChart];
	//Animate show chart
	/*
	_viewMainContent.alpha = 0.0;
	[UIView transitionWithView:_viewMainContent duration:1.0 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:^{
		_viewMainContent.alpha = 1.0;
	} completion:^(BOOL finished) {

	}];
	 */
}

- (void)hideChart{
	[self dismissViewControllerAnimated:YES completion:^{
		[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
		[UINavigationController attemptRotationToDeviceOrientation];
	}];
//	[UIView transitionWithView:_viewMainContent duration:1.0 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:^{
//		_viewMainContent.alpha = 0.3;
//	} completion:^(BOOL finished) {
//		[[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
//		[UINavigationController attemptRotationToDeviceOrientation];
//		[self dismissViewControllerAnimated:NO completion:nil];
//	}];
}
#pragma mark - UTILS
- (void)setChartType:(ChartType)chartType{
	_tcChart.chartType = chartType;
	_chartType = chartType;
}
#pragma mark - Status Device
- (BOOL)prefersStatusBarHidden {
	return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
	return UIInterfaceOrientationPortrait;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
	return UIInterfaceOrientationMaskAll;//UIInterfaceOrientationMaskLandscape;
}

-(BOOL)shouldAutorotate {
	return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	_tcChart.delegate = self;
	_viewMainContent.backgroundColor =[UIColor clearColor];
	self.view.backgroundColor = COLOR_FROM_HEX(0x364D59);
}

#pragma mark - TCChartViewDelegate
- (void)doubleTapChartView:(TCChartView *)chartView{
	[self hideChart];
}

- (void)didLoadChartAtType:(ChartType)type{
	if (_delegate) {
		[_delegate didLoadChartAtType:_tcChart.chartType];
	}
}
#define degreesToRadians(x) (M_PI * (x) / 180.0)
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[self.view layoutIfNeeded];
}

#pragma mark - UITapGestureDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
	return YES;
}

@end
