//
//  ChartVC.h
//  TCiPad
//
//  Created by Kaka on 4/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class TCChartView;

#pragma mark - Protocol
@protocol ChartVCDelegate <NSObject>
@optional
- (void)didLoadChartAtType:(ChartType)type;
@end

@interface ChartVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UIView *viewMainContent;
@property (weak, nonatomic) IBOutlet TCChartView *tcChart;
//Configuration
@property (assign, nonatomic) ChartType chartType;
@property (strong, nonatomic) NSString *stockCode;
@property (assign, nonatomic) BOOL isOnlyImageChart;

//check did show
@property (assign, nonatomic) BOOL isPresented;

@property (weak, nonatomic) id <ChartVCDelegate> delegate;
@end
