//
//  AssetManagementVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AssetManagementVC.h"
#import "AccountsVC.h"
#import "AssetManagementAPI.h"
#import "AssetManagementCell.h"
#import "LanguageManager.h"
#import "AccountSummaryVC.h"
#import "AppState.h"
@interface AssetManagementVC () <MOrderBookSelectAccountViewDelegate, UITableViewDelegate, UITableViewDataSource, AccountSummaryVCDelegate>
{
    NSArray *titles;
    AssetManagementModel *model;
    AccountSummaryVC *controller;
}
@end

@implementation AssetManagementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.customBarTitle = [LanguageManager stringForKey:@"Asset Management"];
    [self createLeftMenu];
    [self.tbl_Content registerNib:[UINib nibWithNibName:@"AssetManagementCell" bundle:nil] forCellReuseIdentifier:@"AssetManagementCell"];
    self.tbl_Content.estimatedRowHeight = 80;
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    [self.v_SelectingAccount setDisplayAccount:[UserPrefConstants singleton].userSelectedAccount];
    self.v_SelectingAccount.delegate = self;
    [self loadData:nil];
}
- (void)loadData:(void (^)(void))compliton{

    //    SSIG2 = 8401
    //    SSITTL = 8402
    [AppState sharedInstance].state = AppStateType_Loading;
    [AssetManagementAPI getAssetManagement:[UserPrefConstants singleton].userSelectedAccount ordRouteSource:@"8401" completion:^(id result, NSError *error) {
        [AppState sharedInstance].state = AppStateType_Done;
        self->model = [result firstObject];
        if(self->model){
            self->titles = @[@"Account Summary"];
        }else{
            self->titles = @[];
        }
        [self.tbl_Content reloadData];
        if(compliton){
            compliton();
        }
        
    }];
}
#pragma mark AccountSummaryVCDelegate
- (void)loadDataForSummary{
    [self loadData:^{
        self->controller.asset = self->model;
        [self->controller.tbl_Content reloadData];
    }];
}
#pragma mark MOrderBookSelectAccountViewDelegate
- (void)didSelectAccountView{
    [self showAccountVC];
}


- (void)showAccountVC{
    AccountsVC *accountVC = NEW_VC_FROM_NIB([AccountsVC class], [AccountsVC nibName]);
    accountVC.CallBackValue = ^(UserAccountClientData *account) {
        [self.v_SelectingAccount setDisplayAccount:account];
        [self loadData:nil];
    };
    
    [self nextTo:accountVC animate:YES];
}


#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!controller){
        controller = NEW_VC_FROM_STORYBOARD(kAssetStoryboardName, [AccountSummaryVC storyboardID]);
        controller.delegate = self;
    }
    controller.asset = model;
    [self nextTo:controller animate:YES];
}
#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return titles.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AssetManagementCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AssetManagementCell" forIndexPath:indexPath];
    cell.lbl_Tittle.text = titles[indexPath.row];
    return cell;
}
@end
