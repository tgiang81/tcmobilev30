//
//  AccountSummaryViewCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/8/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseSubView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountSummaryViewCell : TCBaseSubView
@property (weak, nonatomic) IBOutlet UILabel *lbl_TitleLeft;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ValueLeft;

@property (weak, nonatomic) IBOutlet UILabel *lbl_TitleRight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ValueRight;
@property (weak, nonatomic) IBOutlet UIView *v_SeperatorHz;

@end

NS_ASSUME_NONNULL_END
