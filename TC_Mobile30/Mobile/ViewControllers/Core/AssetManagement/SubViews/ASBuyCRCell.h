//
//  ASBuyCRCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface ASBuyCRCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_5P;
@property (weak, nonatomic) IBOutlet UILabel *lbl_5PValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_6P;
@property (weak, nonatomic) IBOutlet UILabel *lbl_6PValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_7P;
@property (weak, nonatomic) IBOutlet UILabel *lbl_7PValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_8P;
@property (weak, nonatomic) IBOutlet UILabel *lbl_8PValue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_9P;
@property (weak, nonatomic) IBOutlet UILabel *lbl_9PValue;
@end

NS_ASSUME_NONNULL_END
