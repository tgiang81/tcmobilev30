//
//  AssetManagementCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface AssetManagementCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_Tittle;

@end

NS_ASSUME_NONNULL_END
