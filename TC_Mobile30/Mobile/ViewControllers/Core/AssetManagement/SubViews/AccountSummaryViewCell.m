//
//  AccountSummaryViewCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/8/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AccountSummaryViewCell.h"
#import "ThemeManager.h"
@implementation AccountSummaryViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    
}
- (void)setupView{
    self.lbl_TitleLeft.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    self.lbl_ValueLeft.textColor = self.lbl_TitleLeft.textColor;
    self.lbl_TitleRight.textColor = self.lbl_TitleLeft.textColor;
    self.lbl_ValueRight.textColor = self.lbl_TitleLeft.textColor;
}
@end
