//
//  AssetManagementCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AssetManagementCell.h"
#import "UITableViewCell+Utils.h"
@implementation AssetManagementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self showFullSeperator];
    self.lbl_Tittle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
