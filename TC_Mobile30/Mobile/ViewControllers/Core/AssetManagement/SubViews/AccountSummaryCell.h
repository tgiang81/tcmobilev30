//
//  AccountSummaryCell.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@class AssetManagementModel;
@interface AccountSummaryCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIStackView *st_Content;
- (void)setupData:(AssetManagementModel *)model;

@end

NS_ASSUME_NONNULL_END
