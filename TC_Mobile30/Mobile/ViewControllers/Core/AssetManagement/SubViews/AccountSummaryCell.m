//
//  AccountSummaryCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AccountSummaryCell.h"
#import "AssetManagementModel.h"
#import "AssetManagementMapper.h"
#import "AccountSummaryViewCell.h"
@implementation AccountSummaryCell

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setupData:(AssetManagementModel *)model{
    for(int i = 1; i < 19; i++){
        AccountSummaryViewCell *contentView = [[AccountSummaryViewCell alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 60)];
        [self setupDataForCell:model inputView:contentView index:i];
        [self.st_Content addArrangedSubview:contentView];
    }
}

- (void)setupDataForCell:(AssetManagementModel *)model inputView:(AccountSummaryViewCell *)cell index:(NSInteger)index{
    AssetManagementKey *mapper = [AssetManagementKey new];
    NSArray *titleSetupLeft = @[];
    NSArray *titleSetupRight = @[];
    
    NSString *leftValue = @"-";
    NSString *rightValue = @"-";
    if(index == 1){
        titleSetupLeft = [mapper getTitle:mapper.CollateralAsset];
        titleSetupRight = [mapper getTitle:mapper.Liability];
        
        leftValue = model.CollateralAsset;
        rightValue = model.Liability;
    }else if(index == 2){
        titleSetupLeft = [mapper getTitle:mapper.Equity];
        titleSetupRight = [mapper getTitle:mapper.CashBalance];
        
        leftValue = model.Equity;
        rightValue = model.CashBalance;
    }
    else if(index == 3){
        titleSetupLeft = [mapper getTitle:mapper.LMV];
        titleSetupRight = [mapper getTitle:mapper.Collateral_A];
        
        leftValue = model.LMV;
        rightValue = model.Collateral_A;
    }
    else if(index == 4){
        titleSetupLeft = [mapper getTitle:mapper.Debt];
        titleSetupRight = [mapper getTitle:mapper.Pre_Loan];
        
        leftValue = model.Debt;
        rightValue = model.Pre_Loan;
    }
    else if(index == 5){
        titleSetupLeft = [mapper getTitle:mapper.AP];
        titleSetupRight = [mapper getTitle:mapper.AR];
        
        leftValue = model.AP;
        rightValue = model.AR;
    }
    else if(index == 6){
        titleSetupLeft = [mapper getTitle:mapper.CreditLimit];
        titleSetupRight = [mapper getTitle:mapper.LMV_non_marginable];
        
        leftValue = model.CreditLimit;
        rightValue = model.LMV_non_marginable;
    }
    else if(index == 7){
        titleSetupLeft = [mapper getTitle:mapper.MarginRequirement];
        titleSetupRight = [mapper getTitle:mapper.BuyMR];
        
        leftValue = model.MarginRequirement;
        rightValue = model.BuyMR;
    }
    else if(index == 8){
        titleSetupLeft = [mapper getTitle:mapper.EE];
        titleSetupRight = [mapper getTitle:mapper.PurchasingPower];
        
        leftValue = model.EE;
        rightValue = model.PurchasingPower;
    }
    else if(index == 9){
        titleSetupLeft = [mapper getTitle:mapper.MarginCall];
        titleSetupRight = [mapper getTitle:mapper.Action];
        
        leftValue = model.MarginCall;
        rightValue = model.Action;
    }
    else if(index == 10){
        titleSetupLeft = [mapper getTitle:mapper.AccruedInterest];
        titleSetupRight = [mapper getTitle:mapper.Fees];
        
        leftValue = model.AccruedInterest;
        rightValue = model.Fees;
    }
    else if(index == 11){
        titleSetupLeft = [mapper getTitle:mapper.AP_T1];
        titleSetupRight = [mapper getTitle:mapper.AR_T1];
        
        leftValue = model.AP_T1;
        rightValue = model.AR_T1;
    }
    else if(index == 12){
        titleSetupLeft = [mapper getTitle:mapper.EE_Credit];
        titleSetupRight = [mapper getTitle:mapper.TotalEquity];
        
        leftValue = model.EE_Credit;
        rightValue = model.TotalEquity;
    }
    else if(index == 13){
        titleSetupLeft = [mapper getTitle:mapper.Call_LMW];
        titleSetupRight = [mapper getTitle:mapper.Force_LMV];
        
        leftValue = model.Call_LMW;
        rightValue = model.Force_LMV;
    }
    else if(index == 14){
        titleSetupLeft = [mapper getTitle:mapper.CallMargin];
        titleSetupRight = [mapper getTitle:mapper.CallForcesell];
        
        leftValue = model.CallMargin;
        rightValue = model.CallForcesell;
    }
    else if(index == 15){
        titleSetupLeft = [mapper getTitle:mapper.Withdrawal];
        titleSetupRight = [mapper getTitle:mapper.MarginRatio];
        
        leftValue = model.Withdrawal;
        rightValue = model.MarginRatio;
    }
    else if(index == 16){
        titleSetupLeft = [mapper getTitle:mapper.SellUnmatch];
        titleSetupRight = [mapper getTitle:mapper.fHoldRight];
        
        leftValue = model.CollateralAsset;
        rightValue = model.fHoldRight;
    }
    else if(index == 17){
        titleSetupLeft = [mapper getTitle:mapper.BuyUnmatch];
        titleSetupRight = [mapper getTitle:mapper.CIA];
        
        leftValue = model.BuyUnmatch;
        rightValue = model.CollateralAsset;
    }
    else if(index == 18){
        titleSetupLeft = [mapper getTitle:mapper.PP_Credit];
        titleSetupRight = [mapper getTitle:mapper.TotalAssets];
        
        leftValue = model.PP_Credit;
        rightValue = model.TotalAssets;
        
        [cell.v_SeperatorHz setHidden:YES];
    }
    if([leftValue isEqualToString:@""]){
        leftValue = @"-";
    }
    if([rightValue isEqualToString:@""]){
        rightValue = @"-";
    }
    cell.lbl_TitleLeft.text = titleSetupLeft[0];
    cell.lbl_ValueLeft.textColor = titleSetupLeft[1];
    cell.lbl_ValueLeft.text = leftValue;
    
    cell.lbl_TitleRight.text = titleSetupRight[0];
    cell.lbl_ValueRight.textColor = titleSetupRight[1];
    cell.lbl_ValueRight.text = rightValue;
}
@end
