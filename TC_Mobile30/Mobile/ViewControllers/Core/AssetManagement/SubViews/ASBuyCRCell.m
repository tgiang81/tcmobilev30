//
//  ASBuyCRCell.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "ASBuyCRCell.h"

@implementation ASBuyCRCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    self.lbl_5P.textColor = self.lbl_Title.textColor;
    self.lbl_5PValue.textColor = self.lbl_Title.textColor;
    self.lbl_6P.textColor = self.lbl_Title.textColor;
    self.lbl_6PValue.textColor = self.lbl_Title.textColor;
    self.lbl_7P.textColor = self.lbl_Title.textColor;
    self.lbl_7PValue.textColor = self.lbl_Title.textColor;
    self.lbl_8P.textColor = self.lbl_Title.textColor;
    self.lbl_8PValue.textColor = self.lbl_Title.textColor;
    self.lbl_9P.textColor = self.lbl_Title.textColor;
    self.lbl_9PValue.textColor = self.lbl_Title.textColor;
}



@end
