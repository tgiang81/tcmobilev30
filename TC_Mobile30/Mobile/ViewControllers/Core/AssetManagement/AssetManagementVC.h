//
//  AssetManagementVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "MOrderBookSelectAccountView.h"
NS_ASSUME_NONNULL_BEGIN

@interface AssetManagementVC : BaseVC
@property (weak, nonatomic) IBOutlet MOrderBookSelectAccountView *v_SelectingAccount;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@end

NS_ASSUME_NONNULL_END
