//
//  AccountSummaryVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AccountSummaryVC.h"
#import "AccountsVC.h"
#import "ASBuyCRCell.h"
#import "AccountSummaryCell.h"
#import "AssetManagementMapper.h"
@interface AccountSummaryVC ()<MOrderBookSelectAccountViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    int numberOfRows;
}
@end

@implementation AccountSummaryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.customBarTitle = [LanguageManager stringForKey:@"Account Summary"];
    [self setBackButtonDefault];
    numberOfRows = 2;//the first row is BUY CR
    [self.tbl_Content registerNib:[UINib nibWithNibName:@"AccountSummaryCell" bundle:nil] forCellReuseIdentifier:@"AccountSummaryCell"];
    [self.tbl_Content registerNib:[UINib nibWithNibName:@"ASBuyCRCell" bundle:nil] forCellReuseIdentifier:@"ASBuyCRCell"];
    self.tbl_Content.estimatedRowHeight = 80;
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    [self.v_SelectingAccount setDisplayAccount:[UserPrefConstants singleton].userSelectedAccount];
    self.v_SelectingAccount.delegate = self;
}

#pragma mark MOrderBookSelectAccountViewDelegate
- (void)didSelectAccountView{
    [self showAccountVC];
}


- (void)showAccountVC{
    AccountsVC *accountVC = NEW_VC_FROM_NIB([AccountsVC class], [AccountsVC nibName]);
    accountVC.CallBackValue = ^(UserAccountClientData *account) {
        [self.v_SelectingAccount setDisplayAccount:account];
        [self.delegate loadDataForSummary];
    };
    
    [self nextTo:accountVC animate:YES];
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return numberOfRows;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        ASBuyCRCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ASBuyCRCell" forIndexPath:indexPath];
        [self setupBUYCRCell:cell];
        return cell;
    }else{
        AccountSummaryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccountSummaryCell" forIndexPath:indexPath];
        [cell setupData:self.asset];
        return cell;
    }
}
- (void)setupBUYCRCell:(ASBuyCRCell *)cell{
    cell.lbl_5PValue.text = self.asset.BuyCREE50;
    cell.lbl_6PValue.text = self.asset.BuyCREE60;
    cell.lbl_7PValue.text = self.asset.BuyCREE70;
    cell.lbl_8PValue.text = self.asset.BuyCREE80;
    cell.lbl_9PValue.text = self.asset.BuyCREE90;
}

@end
