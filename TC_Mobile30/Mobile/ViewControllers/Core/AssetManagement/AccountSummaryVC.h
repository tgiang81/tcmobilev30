//
//  AccountSummaryVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/7/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "MOrderBookSelectAccountView.h"
#import "AssetManagementModel.h"
NS_ASSUME_NONNULL_BEGIN
@protocol AccountSummaryVCDelegate<NSObject>
- (void)loadDataForSummary;
@end
@interface AccountSummaryVC : BaseVC
@property (weak, nonatomic) IBOutlet MOrderBookSelectAccountView *v_SelectingAccount;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@property (strong, nonatomic) AssetManagementModel *asset;
@property (weak, nonatomic) id<AccountSummaryVCDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
