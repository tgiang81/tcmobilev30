//
//  ExchangeSelectionVC.m
//  TC_Mobile30
//
//  Created by Kaka on 1/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "ExchangeSelectionVC.h"
#import "ExchangeData.h"
#import "UIView+ShadowBorder.h"
@interface ExchangeSelectionVC ()<UITableViewDelegate, UITableViewDataSource>{
	NSArray *_listExchange;
}
@property (strong, nonatomic) ExchangeData *curSelectedExData;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *topBar;
@end

@implementation ExchangeSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_listExchange = @[].copy;
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	self.tblContent.delegate = self;
	self.tblContent.dataSource = self;
	if (_type == ExchangeSelection_Filter) {
		_lblTitle.text = [LanguageManager stringForKey:@"Filter By"];
	}else{
		_lblTitle.text = [LanguageManager stringForKey:@"Select By"];
	}
	self.topBar.backgroundColor = [UIColor whiteColor];
	[self.topBar makeBorderShadow];
	[self.view bringSubviewToFront:self.topBar];
}

#pragma mark - LoadData
- (void)loadData{
	_listExchange = [[UserPrefConstants singleton].supportedExchanges copy];
	if (_listExchange.count > 0) {
		[self.tblContent reloadData];
		//Scroll to selected Item
		ExchangeData *defaultEx = [Utils getExchange:[SettingManager shareInstance].defaultExchangeCode];
		if (defaultEx) {
			NSInteger indexOfDefaultEx = [_listExchange indexOfObject:defaultEx];
			NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:indexOfDefaultEx inSection:0];
			[self.tblContent scrollToRowAtIndexPath:selectedIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
		}
	}
}

#pragma mark - ACTIONS

- (IBAction)onCloseAction:(id)sender {
	[self dismissPopupWithCompletion:nil];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_listExchange count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return kEXCHANGE_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CELL_ID = @"PopupExchange";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELL_ID];
	}
	//Font
	cell.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	cell.textLabel.font = AppFont_MainFontRegularWithSize(13);
	cell.textLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	//Parse data
	ExchangeData *exData = _listExchange[indexPath.row];
	/*
	ExchangeData *defaultEx = [Utils getExchange:[SettingManager shareInstance].defaultExchangeCode];
	if (exData.feed_exchg_code == defaultEx.feed_exchg_code) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}else{
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	*/
	cell.textLabel.text = [Utils getShortExChangeName:exData];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	_curSelectedExData = _listExchange[indexPath.row];
	[self.tblContent reloadData];
	if ([self respondsToSelector:@selector(dismissPopup:completion:)]) {
		__weak ExchangeSelectionVC *weakSelf = self;
		[self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
			if (weakSelf.delegate) {
				[weakSelf.delegate didSelectExchange:self->_curSelectedExData];
			}
		}];
	}
}
@end
