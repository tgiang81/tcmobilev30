//
//  ExchangeSelectionVC.h
//  TC_Mobile30
//
//  Created by Kaka on 1/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
typedef enum : NSInteger{
	ExchangeSelection_Filter,
	ExchangeSelection_Default
}ExchangeSelectionType;

@class ExchangeData;
@protocol ExchangeSelectionVCDelegate <NSObject>
@optional
- (void)didSelectExchange:(ExchangeData *)exData;
@end
#define kEXCHANGE_CELL_HEIGHT	44
@interface ExchangeSelectionVC : BaseVC{
	
}
//Passing data
@property (assign, nonatomic) ExchangeSelectionType type;
@property (weak, nonatomic) id <ExchangeSelectionVCDelegate> delegate;
@end
