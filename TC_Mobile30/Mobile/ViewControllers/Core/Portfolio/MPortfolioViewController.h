//
//  MPortfolioViewController.h
//  TC_Mobile30
//
//  Created by Kaka on 12/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
//Page
typedef void (^CompletionHandlerPortFolio)(id result, int page, NSError *error);
typedef enum : NSInteger{
    MPortfolioPage_First = 0, 	//Unrealised or virtual
    MPortfolioPage_Last         //Realised or Summary
}MPortfolioPage;
//Configs
typedef enum: NSInteger {
	MPortfolioAccountType_Derivative = 0,
	MPortfolioAccountType_Equity
} MPortfolioAccountType;
@protocol MPortfolioViewControllerDelegate<NSObject>
- (void)didScrollTo:(NSInteger)page;
- (void)didChangeLayout:(CGFloat)value;
@end
@interface MPortfolioViewController : BaseVC
@property(weak, nonatomic) id<MPortfolioViewControllerDelegate>delegate;
- (void)scrollToPage:(NSInteger)page;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topInfoConstrainHeight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_PortfolioTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AccountNoTitle;

@property (assign, nonatomic) BOOL isDashBoardMode;

- (void)didCalculate:(double)percentChange
currentMarketBalance:(double)currentMarketBalance
     investedBalance:(double)investedBalance
                page:(MPortfolioPage)page;

- (void)showUI2:(double)percentChange
currentMarketBalance:(double)currentMarketBalance
investedBalance:(double)investedBalance
  lblTotalValue:(UILabel *)lblTotalValue
        lblCash:(UILabel *)lblCash
lblChangedValue:(UILabel *)lblChangedValue
       cashView:(UIView *)cashView
    balanceView:(UIView *)balanceView
     lblBalance:(UILabel *)lblBalance
        curPage:(MPortfolioPage)curPage;

- (void)showUI:(double)percentChange
currentMarketBalance:(double)currentMarketBalance
investedBalance:(double)investedBalance;

- (void)actionDerivativeAccountViews:(MPortfolioPage)curPage;
- (void)actionDerivativeAccountViews:(UILabel *)lblTotalValue
                     lblChangedValue:(UILabel *)lblChangedValue
                            cashView:(UIView *)cashView
                         balanceView:(UIView *)balanceView;
- (void)startLoadingContentAt:(NSInteger)page;
@end
