//
//  MPortfolioDashBoardViewController.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/11/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MPortfolioViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MPortfolioDashBoardViewController : MPortfolioViewController
@property (weak, nonatomic) IBOutlet UILabel *lbl_TotalUnRealise;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TotalRealise;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TotalValueRealise;
@property (weak, nonatomic) IBOutlet UILabel *lbl_changedValueRealise;
@property (weak, nonatomic) IBOutlet UILabel *lbl_BalanceRealise;
@property (weak, nonatomic) IBOutlet UILabel *lbl_BalanceValueRealise;
@property (weak, nonatomic) IBOutlet UILabel *lbl_CashRealise;
@property (weak, nonatomic) IBOutlet UILabel *lbl_CashValueRealise;
@property (weak, nonatomic) IBOutlet UIView *realisedBalanceView;
@property (weak, nonatomic) IBOutlet UIView *realisedCashView;
@property (weak, nonatomic) IBOutlet UILabel *lblCashBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblEE;

@end

NS_ASSUME_NONNULL_END
