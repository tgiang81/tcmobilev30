//
//  MPortfolioViewController.m
//  TC_Mobile30
//
//  Created by Kaka on 12/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MPortfolioViewController.h"
#import "TCPageMenuControl.h"
#import "UIImageView+TCUtil.h"
#import "UIView+TCUtil.h"
#import "AccountsVC.h"
#import "BasePortfoiloContent.h"
#import "NSNumber+Formatter.h"
#import "NSString+SizeOfString.h"
#import "UIView+ShadowBorder.h"
#import "BrokerModel.h"

//Content
#import "UnrealisedContentVC.h"
#import "RealisedContentVC.h"
#import "VirtualPFContentVC.h"
#import "SummaryPFContentVC.h"

#import "PortfolioAPI.h"
//Model
#import "PortfolioData.h"
#import "PrtfSubDtlRptData.h"
#import "PrtfSummRptData.h"

#import "QCData.h"
#import "MarqueeLabel.h"


#define kPORTFOLIOS_NUMBER_PAGES	2
#define kTOP_INFO_NORMAL_CONSTRAIN_HEIGHT	180
#define kTOP_INFO_SHORTLY_CONSTRAIN_HEIGHT	120

@interface MPortfolioViewController ()<TCControlViewDelegate, UIScrollViewDelegate>{
    NSMutableArray *_unrealisedArr;
    NSMutableArray *_realisedArr;
    NSMutableArray *_virtualArr;
    NSMutableArray *_summaryArr;
    
    NSInteger currentDashBoardPage;
    
    BOOL isConfig;
}
//OUTLETS
@property (weak, nonatomic) IBOutlet TCPageMenuControl *menuControl;
@property (weak, nonatomic) IBOutlet TCBaseView *infoView;
@property (weak, nonatomic) IBOutlet TCBaseView *accountView;

@property (weak, nonatomic) IBOutlet UILabel *lblAcount;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrowDown;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Total;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblChangedValue;

@property (weak, nonatomic) IBOutlet UIView *balanceView;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Balance;
@property (weak, nonatomic) IBOutlet UILabel *lblBalance;

@property (weak, nonatomic) IBOutlet UIView *cashView;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Cash;
@property (weak, nonatomic) IBOutlet UILabel *lblCash;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *instructionView;
@property (weak, nonatomic) IBOutlet UIView *subContentView;

//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelAccountConstrainWidth;


@property (weak, nonatomic) IBOutlet UIScrollView *scv_content;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UILabel *lblUnGL;
@property (weak, nonatomic) IBOutlet UILabel *lblCashBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblInvestedBalance;

//Configuration
@property (assign, nonatomic) MPortfolioAccountType accountType;
@property (assign, nonatomic) MPortfolioPage curPage; //Load Unrealised for Equity, load Virtual for Derivative account;
@property (strong, nonatomic) BasePortfoiloContent *pfContentVC;

@end

@implementation MPortfolioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Init values
    _unrealisedArr = @[].mutableCopy;
    _realisedArr = @[].mutableCopy;
    _virtualArr = @[].mutableCopy;
    _summaryArr = @[].mutableCopy;
    [self registerNotification];
    [self createUI];
    [self loadData];
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if(self.isDashBoardMode && self.view.frame.size.width != 375 && !isConfig){
        isConfig = YES;
        [self populateInfoCurrentSelectedAccount:[UserPrefConstants singleton].userSelectedAccount];
        [self startLoadingContentAt:MPortfolioAccountType_Equity];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - CreateUI
- (void)createUI{
    //Bar
    [self createLeftMenu];
    self.title = [LanguageManager stringForKey:@"Portfolio"];
    self.scv_content.delegate = self;
}
//Cuongnx (16/04/2019) create UI for UnRealised Tap
-(void)createUIUnRealisedForIntro{
    _lblName.text=@"Name";
    _lblQty.text=@"Qty";
    _lblUnGL.text=@"Un.G/L";
}
//Cuongnx (16/04/2019) create UI for Realised Tap
-(void)createUIRealisedForIntro{
    _lblName.text=@"Name";
    _lblQty.text=@"Qty";
    _lblUnGL.text=@"GL";
}
- (void)updateTheme:(NSNotification *)noti{
    [super updateTheme:noti];
    //Do something
    if(_isDashBoardMode){
        self.lbl_PortfolioTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
        self.lbl_PortfolioTitle.text = [LanguageManager stringForKey:@"Portfolio"];
        self.lbl_AccountNoTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
        self.lbl_AccountNoTitle.text = [LanguageManager stringForKey:@"Account No:"];
        self.lblAcount.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_subItemTextColor);
        [_imgArrowDown toColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_subItemTextColor)];
    }else{
        [_imgArrowDown toColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor)];
    }
    
    _contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    self.instructionView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subHeaderBg);
    self.infoView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    [self.infoView makeBorderShadow];
    [self.contentView makeBorderShadow];
}
#pragma mark - Register Notification
- (void)registerNotification{
    [self addNotification:@"didReceiveUserAccountsList" selector:@selector(didReiveAccountListNotification:)];
    //Notification: didUpdate
    [self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
      [self addNotification:@"didChangeValuePercent" selector:@selector( didChangeValuePercent:)];
}
#pragma mark - LoadData
- (void)loadData{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UserPrefConstants singleton].userSelectedAccount == nil) {
            //Show Message
            //+++ Apply Derivative later
			self.lblAcount.text = @"###";
            [self.subContentView showStatusMessage:[LanguageManager stringForKey:@"Your Portfolio is Empty"]];
            self->_lblTotalValue.text = @"###";
            self->_lblChangedValue.text = @"###";
            self->_cashView.hidden = YES;
            self->_balanceView.hidden = YES;
            return;
        }
        [self populateInfoCurrentSelectedAccount:[UserPrefConstants singleton].userSelectedAccount];
    });
}
- (void)startLoadingContentAt:(NSInteger)page{
    [self.subContentView removeStatusViewIfNeeded];
    [self.subContentView showPrivateHUD];
    [self loadContentAt:page completion:^(id result, int loadedPage, NSError *error) {
        [self.subContentView dismissPrivateHUD];
        if (error) {
            __weak MPortfolioViewController *weakSelf = self;
            [self.subContentView showStatusViewWithMessage:error.localizedDescription retryAction:^{
                //Do retry here
                DLog(@"+++ Make Do retry here +++");
                [weakSelf startLoadingContentAt:loadedPage];
            }];
            return;
        }
        //Show UI Success
        [self populateInfoBalanceForPage:loadedPage];
        [self showPFContent];
    }];
    
}
//Step 1:
- (void)populateInfoCurrentSelectedAccount:(UserAccountClientData *)uacd{
    if(uacd && [uacd isKindOfClass:[UserAccountClientData class]]){
        if(_isDashBoardMode){
            _lblAcount.text = uacd.client_account_number;
        }else{
            _lblAcount.text = [NSString stringWithFormat:@"%@-%@ %@",uacd.client_account_number, uacd.client_name,uacd.client_code];
        }
        
        //Check Size of content
        float widthContentAccount = [_lblAcount.text widthOfString:_lblAcount.font];
        if (widthContentAccount > (SCREEN_WIDTH_PORTRAIT - 100)) {
            widthContentAccount = SCREEN_WIDTH_PORTRAIT - 100;
        }
        _labelAccountConstrainWidth.constant = widthContentAccount + 10;//+10 for make it nicer
        [self.view layoutIfNeeded];
        //Check Account Type
        if ([[AppConstants DerivativeExchanges] containsObject:uacd.client_acc_exchange]) {
            //Account is Derivative
            self.accountType = MPortfolioAccountType_Derivative;
        }else{
            //Account is Equity
            self.accountType = MPortfolioAccountType_Equity;
        }
    }
}

//Call this after load data
- (void)populateInfoBalanceForPage:(MPortfolioPage)page{
    if (self.accountType == MPortfolioAccountType_Equity) {
        switch (page) {
            case MPortfolioPage_First:{
                //Calculate for Unrealised account
                _lblIns_Total.text = [LanguageManager stringForKey:@"Total Market Value"];
                [self parseEquityBalancInfoFrom:_unrealisedArr page:page];
            }
                break;
            case MPortfolioPage_Last:{
                _lblIns_Total.text = [LanguageManager stringForKey:@"Total Gain/Loss"];
                //Calculate for Realised account
                [self parseEquityBalancInfoFrom:_realisedArr page:page];
            }
                break;
                
            default:
                break;
        }
    }else{
        //Comming soon
        //Will calculate for Virtual/Summary account
    }
}

//Step 4
//******* Just call this after loading data successfully
- (void)showPFContent{
    //Remove old
    if (self.pfContentVC) {
        [self.pfContentVC.view removeFromSuperview];
        [self.pfContentVC removeFromParentViewController];
        self.pfContentVC = nil;
    }
    //Prepare new
    self.pfContentVC = [self portfolioContentAt:_curPage];
    self.pfContentVC.view.frame = _subContentView.bounds;
    //Show
    [_subContentView addSubview:self.pfContentVC.view];
    [self addChildViewController:self.pfContentVC];
    [self.pfContentVC didMoveToParentViewController:self];
}


#pragma mark - Parse Balance
//For Derivative
- (void)parseDerivativeBalancInfoFrom:(NSArray<PortfolioData *> *)pfArr page:(MPortfolioPage)page{
    double currentMarketBalance = 0.0;
    double investedBalance = 0.0;
    double percentChange = 0.0;
    for (PortfolioData *pData in pfArr) {
        double currentMkPrice = (pData.price_last_done.doubleValue == 0) ? pData.price_ref.doubleValue : pData.price_last_done.doubleValue;
        double totalMarketPrice = currentMkPrice * pData.quantity_on_hand.integerValue;
        double totalBuyPrice = pData.price_avg_purchase.doubleValue * pData.quantity_on_hand.floatValue;
        
        investedBalance += totalBuyPrice;
        currentMarketBalance += totalMarketPrice;
    }
    if (investedBalance == 0) {
        //Do nothing
    }else{
        percentChange = (currentMarketBalance - investedBalance) / investedBalance * 100;
    }
    [self didCalculate:percentChange currentMarketBalance:currentMarketBalance investedBalance:investedBalance page:page];
}
//For Equity
- (void)parseEquityBalancInfoFrom:(NSArray<PortfolioData *> *)pfArr page:(MPortfolioPage)page{
    double currentMarketBalance = 0.0;
    double investedBalance = 0.0;
    double percentChange = 0.0;
    if (page == MPortfolioPage_First) {
        //For Unrealised
        for (PortfolioData *pData in pfArr) {
            double currentMkPrice = (pData.price_last_done.doubleValue == 0) ? pData.price_ref.doubleValue : pData.price_last_done.doubleValue;
            double totalMarketPrice = currentMkPrice * pData.quantity_on_hand.integerValue;
            double totalBuyPrice = pData.price_avg_purchase.doubleValue * pData.quantity_on_hand.floatValue;
            
            investedBalance += totalBuyPrice;
            currentMarketBalance += totalMarketPrice;
        }
        if (investedBalance == 0) {
            //Do nothing
        }else{
            percentChange = (currentMarketBalance - investedBalance) / investedBalance * 100;
        }
    }else{
        //For Realised - use this for new design
        if (pfArr.count > 0) {
            for (PortfolioData *pfData in pfArr) {
                currentMarketBalance += pfData.realized_gain_loss_amount.doubleValue;
                percentChange += pfData.realized_gain_loss_percentage.doubleValue;
            }
            percentChange = percentChange / pfArr.count;
        }
    }
    
    [self didCalculate:percentChange currentMarketBalance:currentMarketBalance investedBalance:investedBalance page:page];
}

- (void)showUI2:(double)percentChange currentMarketBalance:(double)currentMarketBalance
investedBalance:(double)investedBalance
  lblTotalValue:(UILabel *)lblTotalValue
		lblCash:(UILabel *)lblCash
lblChangedValue:(UILabel *)lblChangedValue
	   cashView:(UIView *)cashView
	balanceView:(UIView *)balanceView
	 lblBalance:(UILabel *)lblBalance
		curPage:(MPortfolioPage)curPage{
	//Show UI
    NSString *code = ([UserPrefConstants singleton].userCurrentExchange.length>2)?([[UserPrefConstants singleton].userCurrentExchange substringToIndex:2]):([UserPrefConstants singleton].userCurrentExchange);
    ExchangeData *ex = [Utils getExchange:code];
	lblTotalValue.text =  [NSString stringWithFormat:@"%@ %@", ex.currency, [[NSNumber numberWithDouble:currentMarketBalance] toDecimaUsingMeaningPointer:[UserPrefConstants singleton].pointerDecimal]];
	lblCash.text = [[NSNumber numberWithDouble:investedBalance] toDecimaUsingMeaningPointer:[UserPrefConstants singleton].pointerDecimal]; //Invested Balance
	lblChangedValue.text = [NSString stringWithFormat:@"(%.2f%@)",percentChange, @"%"];
	//Color
	if (percentChange >= 0) {
		lblTotalValue.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
		lblChangedValue.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
	}else{
		lblTotalValue.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
		lblChangedValue.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	}
	
	//Check Page
	if (curPage == MPortfolioPage_First) {
		//For Unrealised
		//Check For Cash View
        // Edit by Cuongnx (11/04/2019). Comment of N2N_ISP, task TCPMIOSV3-84. show cashView is default expect SSI broker.
        BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
        NSUInteger brokerTag = brokerModel.tag;
        if (brokerTag==BrokerTag_SSI) {
            cashView.hidden = NO;
            balanceView.hidden = NO;
            _lblIns_Balance.text=@"Total Equity";
            _lblIns_Cash.text=@"EE";
        }
        else{
            cashView.hidden = NO;
            balanceView.hidden = NO;
        }
        UserAccountClientData *uacd = [UserPrefConstants singleton].userSelectedAccount;
//        if ([uacd.client_acc_exchange isEqualToString:@"IDX"] || [uacd.client_acc_exchange isEqualToString:@"PH"]) {
//            cashView.hidden = NO;
//        }else{
//            cashView.hidden = NO;
//        }
		
		//Check for BalanceView: Current is Philipine
//        if ([uacd.client_acc_exchange isEqualToString:@"PH"] ) {
//            balanceView.hidden = NO;
//        }else{
//            balanceView.hidden = NO;
//        }
		//Need check this later
		//lblCash.text = [[NSNumber numberWithDouble:uacd.net_cash_limit] toDecimaUsingMeaningPointer:[UserPrefConstants singleton].pointerDecimal];
		
		lblBalance.text = [[NSNumber numberWithDouble:uacd.credit_limit] toDecimaUsingMeaningPointer:[UserPrefConstants singleton].pointerDecimal];
	}else{
		balanceView.hidden = YES;
		cashView.hidden = YES;
	}
	
	//Should show normal or shortly Height of TopInfo View
	if (balanceView.hidden && _cashView.hidden) {
		_topInfoConstrainHeight.constant = kTOP_INFO_SHORTLY_CONSTRAIN_HEIGHT;
	}else{
		_topInfoConstrainHeight.constant = kTOP_INFO_NORMAL_CONSTRAIN_HEIGHT;
	}
	[UIView animateWithDuration:0.3 animations:^{
		[self.view layoutIfNeeded];
	}completion:^(BOOL finished) {
		/*+++ Not use now
		 if([self.delegate respondsToSelector:@selector(didChangeLayout:)]){
		 [self.delegate didChangeLayout:kTOP_INFO_NORMAL_CONSTRAIN_HEIGHT - self->_topInfoConstrainHeight.constant];
		 }
		 */
	}];
}
- (void)didCalculate:(double)percentChange
currentMarketBalance:(double)currentMarketBalance
     investedBalance:(double)investedBalance
                page:(MPortfolioPage)page{
    [self showUI:percentChange currentMarketBalance:currentMarketBalance investedBalance:investedBalance];
}

- (void)showUI:(double)percentChange currentMarketBalance:(double)currentMarketBalance investedBalance:(double)investedBalance{
    [self showUI2:percentChange currentMarketBalance:currentMarketBalance investedBalance:investedBalance lblTotalValue:_lblTotalValue lblCash:_lblCash lblChangedValue:_lblChangedValue cashView:_cashView balanceView:_balanceView lblBalance:_lblBalance curPage:_curPage];
}


#pragma mark - Utils
//******************* UTILS ***************
- (void)prepareMenuPage{
    //ExchangeView
    _menuControl.itemWidth = self.view.frame.size.width * 0.5;
    
    _menuControl.delegate = self;
    if (self.accountType == MPortfolioAccountType_Derivative) {
        _menuControl.dataSources = @[[LanguageManager stringForKey:@"Virtual"], [LanguageManager stringForKey:@"Summary"]];
    }else{
        _menuControl.dataSources = @[[LanguageManager stringForKey:@"Unrealised"], [LanguageManager stringForKey:@"Realised"]];
    }
    _menuControl.currentSelectedItem = MPortfolioPage_First;
    [_menuControl startLoadingPageMenu];
    //+++Alway loading first page
    self.curPage = MPortfolioPage_First;
}

- (void)loadContentAt:(NSInteger)page completion:(CompletionHandlerPortFolio)handler{
    UserAccountClientData *uacd = [UserPrefConstants singleton].userSelectedAccount;
    //For Equity Account
    if (self.accountType == MPortfolioAccountType_Equity) {
        if (page == MPortfolioPage_First) {
            //Load Unrealised
            [[PortfolioAPI shareInstance] getEquityPortfolioFrom:uacd forType:1 completion:^(id result, NSError *error) {
                if (error) {
                    //Show Error here...
                }else{
                    if (result) {
                        self->_unrealisedArr = [result copy];
                    }
                }
				if (handler) {
					 handler(result, MPortfolioPage_First, error);
				}
            }];
        }else{
            //Load Realised
            [[PortfolioAPI shareInstance] getEquityPortfolioFrom:uacd forType:0 completion:^(id result, NSError *error) {
                if (error) {
                    
                }else{
                    DLog(@"+++Realised: %@", result);
                    if (result) {
                        self->_realisedArr = [result copy];
                    }
                }
				if (handler) {
					handler(result, MPortfolioPage_Last, error);
				}
            }];
        }
    }
    //For Derivative Account
    if (self.accountType == MPortfolioAccountType_Derivative) {
        //Do it later
        //Demo
		if (handler) {
			handler(@[].copy, (int)page, nil);
		}		
    }
}

#pragma mark - Util Updating
//For Equity Account
- (PortfolioData *)updatedModelFromStockChange:(NSString *)stkChanged from:(NSArray<PortfolioData *> *)dataArr{
    for (PortfolioData *pf in dataArr) {
        if ([pf.stockcode isEqualToString:stkChanged]) {
            return pf;
        }
    }
    return nil;
}
#pragma mark - PFContentVC
- (BasePortfoiloContent *)portfolioContentAt:(NSInteger)index{
    if (index >= kPORTFOLIOS_NUMBER_PAGES || index < 0) {
        return nil;
    }
    BasePortfoiloContent *controller;
    //For Equity Info
    if (self.accountType == MPortfolioAccountType_Equity) {
        switch (index) {
            case MPortfolioPage_First:
                controller = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [UnrealisedContentVC storyboardID]);
                controller.items = [_unrealisedArr mutableCopy];
                [self createUIUnRealisedForIntro];
                break;
            case MPortfolioPage_Last:
                controller = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [RealisedContentVC storyboardID]);
                controller.items = [_realisedArr mutableCopy];
                [self createUIRealisedForIntro];
//                _infoView.frame=CGRectMake(_infoView.frame.origin.x, _infoView.frame.origin.y, _infoView.frame.size.width, _infoView.frame.size.height-80);
                break;
            default:{
                controller = [[BasePortfoiloContent alloc] init];
            }
                break;
        }
    }else{
        //For Derivative Info
        switch (index) {
            case MPortfolioPage_First:
                controller = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [VirtualPFContentVC storyboardID]);
                controller.items = [_virtualArr mutableCopy];
                break;
            case MPortfolioPage_Last:
                controller = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [SummaryPFContentVC storyboardID]);
                controller.items = [_summaryArr mutableCopy];
                break;
            default:{
                controller = [[BasePortfoiloContent alloc] init];
            }
                break;
        }
    }
    controller.pageIndex = index;
    controller.supperVC = self;
    return controller;
}
#pragma mark - Configuration
//Step 2
- (void)setAccountType:(MPortfolioAccountType)accountType{
    _accountType = accountType;
    [self prepareMenuPage];
}

//Step 3
- (void)setCurPage:(MPortfolioPage)curPage{
    _curPage = curPage;
    if (_accountType == MPortfolioAccountType_Derivative) {
        //+++ Apply Derivative later
        [self.subContentView showStatusMessage:[LanguageManager stringForKey:@"Derivative Account Type. Coming soon..."]];
        [self actionDerivativeAccountViews:curPage];
        return;
    }
	//For Equity account
    //Call API to load data and then show UI if success
    [self startLoadingContentAt:curPage];
}
- (void)actionDerivativeAccountViews:(MPortfolioPage)curPage{
    [self actionDerivativeAccountViews:_lblTotalValue
                       lblChangedValue:_lblChangedValue
                              cashView:_cashView
                           balanceView:_balanceView
                                  page:curPage];
}
- (void)actionDerivativeAccountViews:(UILabel *)lblTotalValue
                     lblChangedValue:(UILabel *)lblChangedValue
                            cashView:(UIView *)cashView
                         balanceView:(UIView *)balanceView
                                page:(MPortfolioPage)page{
    lblTotalValue.text = @"###";
    lblChangedValue.text = @"###";
    cashView.hidden = YES;
    balanceView.hidden = YES;
}
#pragma mark - ACTIONS

- (IBAction)onSelectAccountAction:(id)sender {
    AccountsVC *_accountVC = NEW_VC_FROM_NIB([AccountsVC class], [AccountsVC nibName]);
    [self nextTo:_accountVC animate:YES];
    _accountVC.CallBackValue = ^(UserAccountClientData *account) {
        [UserPrefConstants singleton].userSelectedAccount = account;
        [self populateInfoCurrentSelectedAccount:account];
    };
}

#pragma mark - TCPageMenuControlDelegate
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
    self.curPage = index;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if(currentDashBoardPage != page && [self.delegate respondsToSelector:@selector(didScrollTo:)]){
        currentDashBoardPage = page;
        [self.delegate didScrollTo:page];
    }
}
- (void)scrollToPage:(NSInteger)page{
    CGFloat x = page * self.scv_content.frame.size.width;
    [self.scv_content setContentOffset:CGPointMake(x, 0) animated:YES];
}

#pragma mark - Handle Notification
- (void)didReiveAccountListNotification:(NSNotification *)noti{
    [self loadData];
}

- (void)didUpdate:(NSNotification *)noti{
    NSString *stockCode = noti.object;
    StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
    if (!changedStock || changedStock == nil) {
        return; //do nothing
    }
    //Parse to update
    if (self.accountType == MPortfolioAccountType_Equity) {
        NSArray *pfArr = (self.curPage == MPortfolioPage_First) ? _unrealisedArr : _realisedArr;
        //detect data changed
        PortfolioData *pfData = [self updatedModelFromStockChange:stockCode from:pfArr];
        if (pfData) {
            NSDictionary *stkData = [[[QCData singleton] qcFeedDataDict] objectForKey:stockCode];
            NSString *price_current = [stkData objectForKey:FID_98_D_LAST_DONE_PRICE];
            NSString *price_ref = [stkData objectForKey:FID_51_D_REF_PRICE];
            pfData.price_last_done = price_current;
            pfData.price_ref = price_ref;
            //Update UI
            [self parseEquityBalancInfoFrom:pfArr page:_curPage];
            //Post Notification to update content bellow
            [self postNotification:kDidUpdatePortfolioContentNotification object:pfData];
        }
    }
}
-(void)didChangeValuePercent:(NSNotification *)noti{
    NSString *data=noti.object;
    if ([data isEqual:@"00"]) {
        _lblUnGL.text=@"Un.G/L";
    }
    else if([data isEqual:@"10"]){
        _lblUnGL.text=@"Un.G/L%";
    }
    else if ([data isEqual:@"01"]){
        _lblUnGL.text=@"GL";
    }
    else if ([data isEqual:@"11"])
    {
        _lblUnGL.text=@"GL%";
    }
}
@end
