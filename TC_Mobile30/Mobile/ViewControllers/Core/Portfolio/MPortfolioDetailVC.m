//
//  MPortfolioDetailVC.m
//  TCiPad
//
//  Created by n2nconnect on 09/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MPortfolioDetailVC.h"
#import "MarqueeLabel.h"

#import "DetailPortfolioCell.h"
#import "PortfolioDetailHeaderView.h"

#import "StockModel.h"
#import "NSNumber+Formatter.h"
#import "PortfolioData.h"

@interface MPortfolioDetailVC ()<UITableViewDataSource, UITableViewDelegate>{

}
//OUTLETS
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockName;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblCompanyName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblChangedValue;

@property (weak, nonatomic) IBOutlet UIImageView *imgUpDownPrice;
//SubInfo
@property (weak, nonatomic) IBOutlet UILabel *lblVol;
@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblVal;
@property (weak, nonatomic) IBOutlet UILabel *lblLo;
@property (weak, nonatomic) IBOutlet UILabel *lblHi;
@property (weak, nonatomic) IBOutlet UILabel *lblTrd;

//Content
@property (weak, nonatomic) IBOutlet UIView *bottomContent;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@end

@implementation MPortfolioDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CreateUI
- (void)createUI{
	[self setBackButtonDefault];
	[self setupLanguage];
	
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	_tblContent.backgroundColor = [UIColor clearColor];
	//_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	[_tblContent registerNib:[UINib nibWithNibName:[DetailPortfolioCell nibName] bundle:nil] forCellReuseIdentifier:[DetailPortfolioCell reuseIdentifier]];
}
- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:@"PORTFOLIO DETAILS"];
}
#pragma mark - LoadData
- (void)loadData{
	if (_detailType == DetailPortfolioType_Realised || _detailType == DetailPortfolioType_Unrealised) {
		PortfolioData *pfData = (PortfolioData *)_pfModel;
		StockModel *stock = [Utils stockQCFeedDataFromCode:pfData.stockcode];
		if (stock) {
			[self infoStockFrom:stock];
		}
	}
}

- (void)infoStockFrom:(StockModel *)model{
	if (model) {
		//Parse value
		_lblStockName.text = model.stockName;
		_lblCompanyName.text = model.compName;
		_lblPrice.text = [NSString stringWithFormat:@"%@", [model.lastDonePrice toCurrencyNumber]];
		[self setPercentChangePrice:model];
		
		//Sortcut
		[self setVol:[model.volume abbreviateNumber]];
		[self setVal:[model.dValue abbreviateNumber]];
		[self setTrd:[model.iTrd abbreviateNumber]];
		//Group
		[self setHi:[model.highPrice toCurrencyNumber]];
		[self setLo:[model.lowPrice toCurrencyNumber]];
		[self setOpen:[model.openPrice toCurrencyNumber]];
	}
}

#pragma mark - UTIL
#pragma mark - Stock Info Util
//Data info stock
//====== Set Data ===========
- (void)setPercentChangePrice:(StockModel *)model{
	if (model.changePer.doubleValue > 0) {
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Up_Price];
		_lblChangedValue.textColor = AppColor_PriceUp_TextColor;
		_lblPrice.textColor = AppColor_PriceUp_TextColor;
	}
	if (model.changePer < 0) {
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Down_Price];
		_lblChangedValue.textColor = AppColor_PriceDown_TextColor;
		_lblPrice.textColor = AppColor_PriceDown_TextColor;
	}
	if (model.changePer == 0) {
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_UnChange_Price];;
		_lblChangedValue.textColor = AppColor_PriceUnChanged_TextColor;
		_lblPrice.textColor = AppColor_PriceUnChanged_TextColor;
	}
	
	NSString *priceChanged = [model.fChange toStringWithSign];
	NSString *percentChanged = [model.changePer toPercentWithSign];
	_lblChangedValue.text = [NSString stringWithFormat:@"%@  (%@)", priceChanged, percentChanged];
}

- (void)setVol:(NSString *)vol{
	_lblVol.text = vol;
}

- (void)setVal:(NSString *)val{
	_lblVal.text = val;
}

- (void)setOpen:(NSString *)open{
	_lblOpen.text = open;
}

- (void)setHi:(NSString *)hi{
	_lblHi.text = hi;
}

- (void)setLo:(NSString *)lo{
	_lblLo.text = lo;
}

- (void)setTrd:(NSString *)trd{
	_lblTrd.text = trd;
}

#pragma mark - UITableViewDataSource
//For Header
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 50;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
	return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	PortfolioDetailHeaderView *_headerView = [[PortfolioDetailHeaderView alloc] initWithHeader:section];
	return _headerView;
}

//For Cell

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 28;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	DetailPortfolioCell *cell = [tableView dequeueReusableCellWithIdentifier:[DetailPortfolioCell reuseIdentifier]];
	if (!cell) {
		cell = [[DetailPortfolioCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[DetailPortfolioCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	//[cell setupDataFrom:self.items[indexPath.row]];
	return cell;
}

@end
