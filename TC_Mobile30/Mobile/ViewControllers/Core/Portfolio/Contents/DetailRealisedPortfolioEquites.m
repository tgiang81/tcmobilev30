//
//  DetailRealisedPortfolioEquites.m
//  TC_Mobile30
//
//  Created by NguyenCuong on 5/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "DetailRealisedPortfolioEquites.h"
#import "UIView+ShadowBorder.h"

@interface DetailRealisedPortfolioEquites ()

@end

@implementation DetailRealisedPortfolioEquites

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self setupLanguage];
    [self loadData];
}
-(void)createUI{
    [self setBackButtonDefault];
    self.topView.makeBorderShadow;
    self.detailView.makeBorderShadow;
}
-(void)setupLanguage{
    self.title=[LanguageManager stringForKey:@"Realised Portfolio"];
}
-(void)loadData{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
