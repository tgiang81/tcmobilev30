//
//  BasePortfoiloContent.h
//  TC_Mobile30
//
//  Created by Kaka on 10/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
@class StockModel;
@class PortfolioData;
@interface BasePortfoiloContent : BaseVC{
	
}
//Passing data
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) BaseVC *supperVC;
@property (strong, nonatomic) StockModel *stockData;

#pragma mark - Util
- (NSInteger)indexOfStockCodeObject:(NSString *)stockCode fromItems:(NSArray <PortfolioData *>*)items;

@end
