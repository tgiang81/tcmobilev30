//
//  SummaryPFContentVC.m
//  TC_Mobile30
//
//  Created by Kaka on 11/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SummaryPFContentVC.h"
#import "PortfolioSummaryCell.h"
#import "DerSumPFDetailViewController.h"
#import "Utils.h"
@interface SummaryPFContentVC ()<UITableViewDataSource, UITableViewDelegate>{
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblIns_Name;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_NettPos;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Last;

@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@end

@implementation SummaryPFContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self updateTheme:nil];
	_lblTitle.text = [LanguageManager stringForKey:@"Summary Portfolio"];
	_lblIns_Name.text = [LanguageManager stringForKey:@"NAME"];
	_lblIns_NettPos.text = [LanguageManager stringForKey:@"Nett Pos."];
	_lblIns_Last.text = [LanguageManager stringForKey:@"Last"];
	
	//For TableView
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	_tblContent.backgroundColor = [UIColor clearColor];
	//_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	[_tblContent registerNib:[UINib nibWithNibName:[PortfolioSummaryCell nibName] bundle:nil] forCellReuseIdentifier:[PortfolioSummaryCell reuseIdentifier]];
	
}

- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = [UIColor clearColor];
	self.contentView.backgroundColor = [UIColor clearColor];
	_lblTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblIns_Name.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblIns_NettPos.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
	_lblIns_Last.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 160;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	PortfolioSummaryCell *cell = [tableView dequeueReusableCellWithIdentifier:[PortfolioSummaryCell reuseIdentifier] forIndexPath:indexPath];
    [cell setModelForCell:self.items[indexPath.row]];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    DerSumPFDetailViewController *vc = NEW_VC_FROM_NIB([DerSumPFDetailViewController class], @"DerSumPFDetailViewController");
    vc.psdrd = self.items[indexPath.row];
    StockModel *sd = [Utils stockQCFeedDataFromCode:vc.psdrd.stock_code];
    vc.stockData = sd;
    [self nextTo:vc animate:YES];
}
@end
