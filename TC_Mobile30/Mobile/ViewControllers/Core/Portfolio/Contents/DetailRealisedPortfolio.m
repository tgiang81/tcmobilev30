//
//  DetailRealisedPortfolio.m
//  TC_Mobile30
//
//  Created by Admin on 4/4/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "DetailRealisedPortfolio.h"
#import "UIView+ShadowBorder.h"
#import "PortfolioData.h"
#import "NSNumber+Formatter.h"
@interface DetailRealisedPortfolio ()


@end

@implementation DetailRealisedPortfolio

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self setupLanguage];
    [self loadData];
}
-(void)createUI{
    [self setBackButtonDefault];
    self.topView.makeBorderShadow;
    self.detailView.makeBorderShadow;
}
-(void)setupLanguage{
    self.title=[LanguageManager stringForKey:@"Realised Portfolio"];
}
-(void)loadData{
    PortfolioData *pData=self.portfolioData;
    _lblStockName.text=pData.stockname;
    _lblStockCode.text=pData.stockcode;
    BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
    NSUInteger brokerTag = brokerModel.tag;
    if (brokerTag==BrokerTag_SSI) {
//    _lblExchg.text=(pData.exchange_ref_number==nil)? @"-":[[NSNumber numberWithDouble:pData.exchange_ref_number.doubleValue]toCurrencyNumber];
    }
    else
    {
        double last_price=(pData.price_last_done.doubleValue==0)?pData.price_ref.doubleValue:pData.price_last_done.doubleValue;
        _lblLastDoneValue.text=[[NSNumber numberWithDouble:last_price]toCurrencyNumber];
        //Value Change
        NSString *priceChanged =[[NSNumber numberWithDouble:pData.change_percentage.doubleValue]toCurrencyNumber] ;
        NSString *percentChanged =[[NSNumber numberWithDouble:pData.change_amount.doubleValue]toCurrencyNumber];
        _lblLastChangeValue.text = [NSString stringWithFormat:@"%@ (%.2f%%)", priceChanged, [percentChanged doubleValue]];
        UIColor *changedColor = (pData.change_percentage.doubleValue > 0)? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
        _lblLastDoneValue.textColor = changedColor;
        _lblLastChangeValue.textColor = changedColor;
        _lblTotalValue.text=(pData.total_quantity_S==nil)? @"-":[[NSNumber numberWithDouble:pData.total_quantity_S.doubleValue]toCurrencyNumber];
        _lblBoughtPrice.text=(pData.bought==nil)? @"-": [[NSNumber numberWithDouble:pData.bought.doubleValue]toCurrencyNumber];
        _lblSoldPrice.text=(pData.sold==nil)? @"-":[[NSNumber numberWithDouble:pData.aggregated_sell_price.doubleValue] toCurrencyNumber];
        _lblRealixedGLValue.text=(pData.unrealized_gain_loss_amount==nil)?@"-":[[NSNumber numberWithDouble:pData.unrealized_gain_loss_amount.doubleValue]toCurrencyNumber];
        _lblRealizedGLPer.text=(pData.unrealized_gain_loss_percentage==nil)? @"-":[NSString stringWithFormat:@"%@%%",[[NSNumber numberWithDouble:pData.realized_gain_loss_percentage.doubleValue]toCurrencyNumber]];
        _lblCashDividend.text=(pData.cash_dividend==nil)? @"-":[[NSNumber numberWithDouble:pData.cash_dividend.doubleValue] toCurrencyNumber];
    }
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
}
@end
