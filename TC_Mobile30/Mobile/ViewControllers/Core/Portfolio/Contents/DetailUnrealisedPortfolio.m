//
//  DetailUnrealisedPortfolio.m
//  TC_Mobile30
//
//  Created by Admin on 4/3/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "DetailUnrealisedPortfolio.h"
#import "UIView+ShadowBorder.h"
#import "PortfolioData.h"
#import "NSNumber+Formatter.h"
#import "QCData.h"
@interface DetailUnrealisedPortfolio ()

@end

@implementation DetailUnrealisedPortfolio

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self setupLanguage];
    [self loadData];
}
-(void)createUI{
    //Nav Bar
    [self setBackButtonDefault];
    _detailView.makeBorderShadow;
    _topInfoView.makeBorderShadow;
}
-(void)setupLanguage{
    self.title=[LanguageManager stringForKey:@"Unrealised Portfolio"];
}
-(void)loadData{
    PortfolioData *pData=self.portfolioData;
    _lblStockCode.text=pData.stockcode;
    _lblStockName.text=pData.stockname;
    double lastPrice = (pData.price_last_done.doubleValue == 0) ? pData.price_ref.doubleValue : pData.price_last_done.doubleValue;
        _lblLastDonePrice.text=[[NSNumber numberWithDouble:lastPrice]toCurrencyNumber];
    //Value Change
    NSString *priceChanged = [[NSNumber numberWithDouble:pData.change_amount.doubleValue]toCurrencyNumber];
    NSString *percentChanged = [[NSNumber numberWithDouble:pData.change_percentage.doubleValue]toCurrencyNumber];
    _lblLastChangeValue.text = [NSString stringWithFormat:@"%@ (%.2f%%)", priceChanged, [percentChanged doubleValue]];
    UIColor *changedColor = (pData.change_percentage.doubleValue > 0)? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
    _lblLastDonePrice.textColor = changedColor;
    _lblLastChangeValue.textColor = changedColor;
    // Calculate Total: Total= SellablQty+Restricted+MortgageQty+bonusShareQty+DueBoughtQty+UnderDueBoughtT1+IntradayBought
    double total;
    double value;
    double marketValue;




    total=pData.quantity_available.doubleValue +pData.quantity_on_hand.doubleValue +pData.pledge.doubleValue +pData.rights.doubleValue+pData.settled_bought_t2.doubleValue+pData.unsettled_bought_t1.doubleValue+pData.bought.doubleValue;
    value=total*pData.price_AP.doubleValue;
    marketValue=total*pData.price_last_done.doubleValue;
    _lblTotalValue.text=([[NSNumber numberWithDouble:total]toCurrencyNumber]==0)?@"-":[[NSNumber numberWithDouble:total]toCurrencyNumber];//Not map
    _lblSellable.text=([[NSNumber numberWithDouble:pData.quantity_available.doubleValue]toCurrencyNumber]==nil)? @"-" : [[NSNumber numberWithDouble:pData.quantity_available.doubleValue]toCurrencyNumber];//Sellable
    _lblBought.text=(pData.bought==nil)? @"-":[[NSNumber numberWithDouble:pData.bought.doubleValue]toCurrencyNumber];//Not Map
    _lblSold.text=(pData.sold==nil)? @"-":[[NSNumber numberWithDouble:pData.sold.doubleValue]toCurrencyNumber];//Sold -
    _lblUnBoughtT1.text= (pData.unsettled_bought_t1==nil)? @"-": [[NSNumber numberWithDouble:pData.unsettled_bought_t1.doubleValue]toCurrencyNumber];
    _lblUnSoldT1.text=(pData.unsettled_sold_t1==nil)? @"-":[[NSNumber numberWithDouble:pData.unsettled_sold_t1.doubleValue]toCurrencyNumber];
    _lblBoughtT2.text=(pData.settled_bought_t2==nil)? @"-":[[NSNumber numberWithDouble:pData.settled_bought_t2.doubleValue]toCurrencyNumber];
    _lblSoldT2.text=(pData.settled_sold_t2==nil)? @"-":[[NSNumber numberWithDouble:pData.settled_sold_t2.doubleValue]toCurrencyNumber];
    _lblAvgPrice.text=(pData.price_AP=nil)? @"-":[[NSNumber numberWithDouble:pData.price_AP.doubleValue]toCurrencyNumber];// Average Price
    _lblValue.text=([[NSNumber numberWithDouble:value]toCurrencyNumber]==nil)? @"-": [[NSNumber numberWithDouble:value]toCurrencyNumber];// Map with Trade.Val on TCPlus
    _lblMaketPrice.text=([[NSNumber numberWithDouble:marketValue]toCurrencyNumber]==nil)? @"-" :[[NSNumber numberWithDouble:pData.price_last_done.doubleValue]toCurrencyNumber];
    _lblMaketValue.text=(pData.gross_market_value==nil)? @"-": [[NSNumber numberWithDouble:marketValue]toCurrencyNumber];//Map with Mkt.Val on TCplus
    _lblUnrealizedGLPer.text=[NSString stringWithFormat:@"%@%%",[[NSNumber numberWithDouble:pData.unrealized_gain_loss_percentage.doubleValue]toCurrencyNumber]];
    _lblUnRealisedGL.text=[[NSNumber numberWithDouble:pData.unrealized_gain_loss_amount.doubleValue]toCurrencyNumber];
    _lblLastChangeValue.text=[[NSNumber numberWithDouble:pData.unrealized_gain_loss_amount.doubleValue]toCurrencyNumber];
    _lblPledge.text=(pData.pledge==nil)? @"-" :[[NSNumber numberWithDouble:pData.pledge.doubleValue]toCurrencyNumber];
    _lblRights.text=(pData.rights==nil)? @"-": [[NSNumber numberWithDouble:pData.rights.doubleValue]toCurrencyNumber];
    _lblRegistered.text=(pData.registered==nil)? @"-":[[NSNumber numberWithDouble:pData.registered.doubleValue]toCurrencyNumber];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
