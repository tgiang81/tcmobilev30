//
//  VirtualPFContentVC.m
//  TC_Mobile30
//
//  Created by Kaka on 11/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "VirtualPFContentVC.h"
#import "PortfolioVirtualCell.h"

@interface VirtualPFContentVC ()<UITableViewDelegate, UITableViewDataSource>{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@end

@implementation VirtualPFContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self updateTheme:nil];
	_lblTitle.text = [LanguageManager stringForKey:@"Virtual Portfolio"];
	//For TableView
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	_tblContent.backgroundColor = [UIColor clearColor];
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	[_tblContent registerNib:[UINib nibWithNibName:[PortfolioVirtualCell nibName] bundle:nil] forCellReuseIdentifier:[PortfolioVirtualCell reuseIdentifier]];
}

- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = [UIColor clearColor];
	self.viewContent.backgroundColor = [UIColor clearColor];
	_lblTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].portfolioTextColor);
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 480;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	PortfolioVirtualCell *cell = [tableView dequeueReusableCellWithIdentifier:[PortfolioVirtualCell reuseIdentifier]];
	if (!cell) {
		cell = [[PortfolioVirtualCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[PortfolioVirtualCell reuseIdentifier]];
	}
    [cell setModelForCell:self.items[indexPath.row]];
	return cell;
}
@end
