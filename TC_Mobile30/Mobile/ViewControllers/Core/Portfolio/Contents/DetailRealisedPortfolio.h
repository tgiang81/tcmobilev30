//
//  DetailRealisedPortfolio.h
//  TC_Mobile30
//
//  Created by Admin on 4/4/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "TCBaseView.h"
#import "BasePortfoiloContent.h"
#import "MarqueeLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface DetailRealisedPortfolio : BaseVC
@property (weak, nonatomic) IBOutlet TCBaseView *topView;
@property (weak, nonatomic) IBOutlet TCBaseView *detailView;
@property (strong,nonatomic) PortfolioData *portfolioData;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockName;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblBoughtPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblSoldPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblCashDividend;
@property (weak, nonatomic) IBOutlet UILabel *lblRealixedGLValue;
@property (weak, nonatomic) IBOutlet UILabel *lblRealizedGLPer;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDoneValue;
@property (weak, nonatomic) IBOutlet UILabel *lblLastChangeValue;
@property (weak, nonatomic) IBOutlet UILabel *lblExchg;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalBoughtCon;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalQty;
@property (weak, nonatomic) IBOutlet UILabel *lblAvgBoughtPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBoughtFee;
@property (weak, nonatomic) IBOutlet UILabel *lblRealizedGainLossValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalSoldCon;
@property (weak, nonatomic) IBOutlet UILabel *lblCashDividendRealized;
@property (weak, nonatomic) IBOutlet UILabel *lblRealizedGainLossPer;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockCode;
@end

NS_ASSUME_NONNULL_END
