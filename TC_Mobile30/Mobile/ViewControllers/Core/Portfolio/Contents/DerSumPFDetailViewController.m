//
//  DerSumPFDetailViewController.m
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 3/5/14.
//
//

#import "DerSumPFDetailViewController.h"
#import "PrtfDtlRptData.h"
#import "NSNumber+Formatter.h"
#import "Utils.h"
#import "LanguageManager.h"
#import "PortfolioAPI.h"
#import "AppState.h"
@interface DerSumPFDetailViewController () <TCControlViewDelegate>

@end

@implementation DerSumPFDetailViewController
@synthesize stockData, isDerivative, trade_exchg_code, dtlDayTblVController, dayList, dtlOvrnightTblVController, overnightList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        detail_type = 0;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // ----- Set Trade Exchange Code -----
    
    self.trade_exchg_code = [Utils stripExchgCodeFromStockCode:_psdrd.stock_code];
    [self setBackButtonDefault];
    self.customBarTitle = [LanguageManager stringForKey:@"Detail"];
    // ----- Day Table -----
    dtlDayTblVController = [[DetailDayTableViewController alloc] initWithStyle:UITableViewStylePlain];
    dtlDayTblVController.table_view = day_tableView;
    [dtlDayTblVController.table_view registerNib:[UINib nibWithNibName:@"DetailDayTableViewCell" bundle:nil] forCellReuseIdentifier:@"DetailDayTableViewCell"];
    dtlDayTblVController.trade_exchg_code = trade_exchg_code;
    
    day_tableView.delegate = dtlDayTblVController;
    day_tableView.dataSource = dtlDayTblVController;
    day_table_price_header.text = [NSString stringWithFormat:@"Price (%@)", _psdrd.stock_currency];
    
    
    // ----- Overnight Table -----
    dtlOvrnightTblVController = [[DetailOvernightTableViewController alloc] initWithStyle:UITableViewStylePlain];
    dtlOvrnightTblVController.table_view = overnight_tableView;
    [dtlOvrnightTblVController.table_view registerNib:[UINib nibWithNibName:@"DetailOvernightTableViewCell" bundle:nil] forCellReuseIdentifier:@"DetailOvernightTableViewCell"];
    dtlOvrnightTblVController.trade_exchg_code = trade_exchg_code;
    overnight_tableView.delegate = dtlOvrnightTblVController;
    overnight_tableView.dataSource = dtlOvrnightTblVController;
    overnight_table_price_header.text = [NSString stringWithFormat:@"Price (%@)", _psdrd.stock_currency];
    
    activityView.alpha = 0;
    buttonDay.tintColor = [UIColor whiteColor];
    buttonOvernight.tintColor = buttonDay.tintColor;
    v_button.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subBgColor);
    [self setupTopControl];
}
- (void)setupTopControl{
    _view_control.itemWidth = SCREEN_WIDTH_PORTRAIT / 2;
    _view_control.dataSources = @[@"Day", @"Overnight"];
    _view_control.delegate = self;
    _view_control.font = AppFont_MainFontMediumWithSize(15);
    _view_control.indicatorColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    _view_control.unselectedColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_unSelectTextColor);
    _view_control.selectedColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    [_view_control startLoadingPageMenu];
}

#pragma mark - TCControlViewDelegate
- (void)actionAt:(NSInteger)index{
    if (index == 0) {
        [dyOvrnightScroller scrollRectToVisible:dayView.frame animated:YES];
        detail_type = 0;
        if (!dayDetailLoaded) {
            [self requestPortfolioDetails];
        }
    }
    else{
        [dyOvrnightScroller scrollRectToVisible:overnightView.frame animated:YES];
        detail_type = 1;
        
        if (!overnightDetailLoaded) {
            [self requestPortfolioDetails];
        }
    }
}
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
    [self actionAt:index];
    NSLog(@"%ld", index);
    
}
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [dyOvrnightScroller setContentSize:CGSizeMake(2*self.view.frame.size.width, dyOvrnightScroller.frame.size.height)];
    CGRect frame = self.view.frame;
    frame.size.height = dyOvrnightScroller.frame.size.height;
    frame.origin = CGPointMake(0, 0);
    dayView.frame = frame;
    [dyOvrnightScroller addSubview:dayView];
    
    frame = self.view.frame;
    frame.size.height = dyOvrnightScroller.frame.size.height;
    frame.origin = CGPointMake(dayView.frame.size.width, 0);
    overnightView.frame = frame;
    [dyOvrnightScroller addSubview:overnightView];
    [self actionAt:detail_type];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Presses
- (IBAction)dayOvernightButtonPressed:(id)sender {

//    if ((UIButton *)sender == buttonDay) {
//        [buttonDay setSelected:YES];
//        [buttonOvernight setSelected:NO];
//
//        [dyOvrnightScroller scrollRectToVisible:dayView.frame animated:YES];
//        detail_type = 0;
//
//        if (!dayDetailLoaded) {
//            [self requestPortfolioDetails];
//        }
//    }
//    else if ((UIButton *)sender == buttonOvernight) {
//        [buttonOvernight setSelected:YES];
//        [buttonDay setSelected:NO];
//
//        [dyOvrnightScroller scrollRectToVisible:overnightView.frame animated:YES];
//        detail_type = 1;
//
//        if (!overnightDetailLoaded) {
//            [self requestPortfolioDetails];
//        }
//    }
}

- (IBAction) refreshPortfolioButtonPressed:(id)sender {

    [self requestPortfolioDetails];
}

#pragma mark - Request Portfolio Details
- (void) requestPortfolioDetails {
    [AppState sharedInstance].state = AppStateType_Loading;
    [[PortfolioAPI shareInstance] getDetailPrtfDtlRpt:[UserPrefConstants singleton].userSelectedAccount isDerivative:YES ofSubDtlRpt:_psdrd detail_type:detail_type completion:^(id result, NSError *error) {
        [AppState sharedInstance].state = AppStateType_Done;
        if(!error){
            if(self->detail_type == 1){
                self.overnightList = result;
                [self populateDataWithCalculation];
                
                self->dtlOvrnightTblVController.overnightList = self.overnightList;
                [self->overnight_tableView reloadData];
                self->overnightDetailLoaded = YES;
            }else{
                self->dayDetailLoaded = YES;
                self.dayList = result;
                [self populateDataWithCalculation];
                
                self->dtlDayTblVController.dayList = self.dayList;
                [self->day_tableView reloadData];
                self->dayDetailLoaded = YES;
            }
        }else{
            if(self->detail_type == 1){
                self->overnightDetailLoaded = NO;
            }else{
                self->dayDetailLoaded = NO;
            }
        }
        NSLog(@"");
    }];
    
    [activityIndicatorView startAnimating];
    activityView.alpha = 1;
}

#pragma mark - Calculate and Populate Data
- (void) populateDataWithCalculation {
    int total_buy_qty = 0;
    int total_sell_qty = 0;
    double total_buy_value = 0;
    double total_sell_value = 0;
    double avg_buy_price = 0;
    double avg_sell_price = 0;
    double total_unreal_pl = 0;
    
    for (PrtfDtlRptData *pdrd in (detail_type == 0 ? self.dayList : self.overnightList)) {
        if ([[pdrd.side uppercaseString] isEqualToString:@"B"]) {
            total_buy_value += ([pdrd.matched_price doubleValue] * [pdrd.matched_qty intValue]);
            total_buy_qty += [pdrd.matched_qty intValue];
        }
        else if ([[pdrd.side uppercaseString] isEqualToString:@"S"]) {
            total_sell_value += ([pdrd.matched_price doubleValue] * [pdrd.matched_qty intValue]);
            total_sell_qty += [pdrd.matched_qty intValue];
        }
        
        if (stockData != nil) {
            pdrd.unrealized_pl = [self calculateUnrealizedPLWith:pdrd and:stockData];
        }
        total_unreal_pl += pdrd.unrealized_pl;
    }
    
    if (total_buy_qty != 0) {
        avg_buy_price = total_buy_value / total_buy_qty;
    }
    if (total_sell_qty != 0) {
        avg_sell_price = total_sell_value / total_sell_qty;
    }
    
    
    if (detail_type == 0) {
        day_avg_price_buy.text = [[NSNumber numberWithDouble:avg_buy_price] toCurrencyNumber];
        day_avg_price_sell.text = [[NSNumber numberWithDouble:avg_sell_price] toCurrencyNumber];
        day_total_qty_buy.text = [NSString stringWithFormat:@"%d", total_buy_qty];
        day_total_qty_sell.text = [NSString stringWithFormat:@"%d", total_sell_qty];
    }
    else if (detail_type == 1) {
        overnight_avg_price_buy.text = [[NSNumber numberWithDouble:avg_buy_price] toCurrencyNumber];
        overnight_avg_price_sell.text = [[NSNumber numberWithDouble:avg_sell_price] toCurrencyNumber];
        overnight_total_qty_buy.text = [NSString stringWithFormat:@"%d", total_buy_qty];
        overnight_total_qty_sell.text = [NSString stringWithFormat:@"%d", total_sell_qty];
        overnight_total_unreal.text = [NSNumber numberFormat2D:total_unreal_pl];
    }
    
    DLog(@"Total Buy Qty %d", total_buy_qty);
    DLog(@"Total Sell Qty %d", total_sell_qty);
    DLog(@"Average Buy Price %f", avg_buy_price);
    DLog(@"Average Sell Price %f", avg_sell_price);
    DLog(@"Total Unrealized P/L %f", total_unreal_pl);
    
}

- (double) calculateUnrealizedPLWith:(PrtfDtlRptData *)pdrd and:(StockModel *)sd {
    double last_done_price = [sd.lastDonePrice doubleValue];
    double average_purchase_price = [pdrd.matched_price doubleValue];
    int nett_position = [[pdrd.side uppercaseString] isEqualToString:@"S"] ? [pdrd.matched_qty intValue]*-1 :[pdrd.matched_qty intValue];
    double multiplier = pdrd.contractPerVal;
    double result = 0;
    
//    if (sd.derivativesStockType == DerivativesStockTypeOption) {
//        if ([sd setAndGetDerivativesOptionType] == DerivativesOptionTypeCall) {
//            if (nett_position >= 0) {
//                //DLog(@"OptionCallBuy");
//                if (last_done_price - average_purchase_price <= 0) {
//                    result = 0;
//                }
//                else {
//                    result = (last_done_price - average_purchase_price) * nett_position * multiplier;
//                }
//            }
//            else {
//                //DLog(@"OptionCallSell");
//                if (average_purchase_price - last_done_price >= average_purchase_price) {
//                    result = average_purchase_price * abs(nett_position) * multiplier;
//                }
//                else {
//                    result = (average_purchase_price - last_done_price) * abs(nett_position) * multiplier;
//                }
//            }
//        }
//        else if ([sd setAndGetDerivativesOptionType] == DerivativesOptionTypePut) {
//            if (nett_position >= 0) {
//                //DLog(@"OptionPutBuy");
//                if (last_done_price - average_purchase_price >= 0) {
//                    result = 0;
//                }
//                else {
//                    result = (average_purchase_price - last_done_price) * nett_position * multiplier;
//                }
//            }
//            else {
//                //DLog(@"OptionPutSell");
//                if ((last_done_price - average_purchase_price) >= average_purchase_price) {
//                    result = average_purchase_price * abs(nett_position) * multiplier;
//                }
//                else {
//                    result = (last_done_price - average_purchase_price) * abs(nett_position) * multiplier;
//
//                }
//            }
//        }
//    }
//    else {
        //DLog(@"Futures");
        result = (last_done_price - average_purchase_price) * nett_position * multiplier;
//    }
    /*
    DLog(@"Last Done Price %f", last_done_price);
    DLog(@"Average Price %f", average_purchase_price);
    DLog(@"Nett Position %d", nett_position);
    DLog(@"Multiplier %f", multiplier);
    DLog(@"Unrealized PL = %f", result);
    */
    return result;
}
/*
#pragma mark - ATPHttpPortfolio Delegate
- (void) TradePrtfDtlRptCallback:(ASIHttpPortfolio *)request {
    DLog(@"TradePrtfDtlRptCallback");
    
    if (request.detail_type == 0) {
        if (request.connerror == NO) {
            self.dayList = request.summaryDtlDayList;
            [self populateDataWithCalculation];
            
            dtlDayTblVController.dayList = self.dayList;
            [day_tableView reloadData];
        }
        dayDetailLoaded = YES;
    }
    else if (request.detail_type == 1) {
        if (request.connerror == NO) {
            self.overnightList = request.summaryDtlOvrnightList;
            [self populateDataWithCalculation];
            
            dtlOvrnightTblVController.overnightList = self.overnightList;
            [overnight_tableView reloadData];
        }
        overnightDetailLoaded = YES;
    }
    
    activityView.alpha = 0;
    [activityIndicatorView stopAnimating];
}
*/
#pragma mark -
#pragma mark UIScrollView delegates
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    [appData timeoutActivity];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    DLog(@"scrollViewDidEndDecelerating");
    int page = floor((dyOvrnightScroller.contentOffset.x - dyOvrnightScroller.frame.size.width / 2) / dyOvrnightScroller.frame.size.width) + 1;
	//DLog(@"PAGE: %d", page);
    
    for (UIView *subview in dyOvrnightScroller.subviews) {
        if (subview.frame.origin.x == (dyOvrnightScroller.frame.size.width*page)) {
            if ([subview isEqual:dayView]) {
                DLog(@"scrollViewDidEndDecelerating dayView");
                detail_type = 0;
            }
            else if ([subview isEqual:overnightView]) {
                DLog(@"scrollViewDidEndDecelerating overnightView");
                detail_type = 1;
            }
        }
    }
    [_view_control setCurrentSelectedItem:detail_type];
    [self actionAt:detail_type];
}

@end
