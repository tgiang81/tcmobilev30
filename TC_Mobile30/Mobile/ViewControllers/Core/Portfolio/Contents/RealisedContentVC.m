//
//  RealisedContentVC.m
//  TC_Mobile30
//
//  Created by Kaka on 10/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "RealisedContentVC.h"
#import "PortfolioRelizedCell.h"
#import "MPortfolioDetailVC.h"
#import "UIView+Animation.h"
#import "MPortfolioEquityCell.h"
#import "DetailRealisedPortfolio.h"

@interface RealisedContentVC ()<UITableViewDataSource, UITableViewDelegate, MPortfolioEquityCellDelegate>{
	NSMutableArray *_expandedCells;
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@end

@implementation RealisedContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_expandedCells = @[].mutableCopy;
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CREATEUI
- (void)createUI{
	self.view.backgroundColor = COLOR_FROM_HEX(0xF0F9FE);
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	_tblContent.backgroundColor = [UIColor clearColor];
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	[_tblContent registerNib:[UINib nibWithNibName:[MPortfolioEquityCell nibName] bundle:nil] forCellReuseIdentifier:[MPortfolioEquityCell reuseIdentifier]];
}

- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	[self.tblContent reloadData];
}

#pragma mark - Register Notification
- (void)registerNotification{
	[self addNotification:kDidUpdatePortfolioContentNotification selector:@selector(didUpdateContentNotification:)];
}

#pragma mark - LoadData
- (void)loadData{
	if (self.items.count == 0) {
		[self.contentView showStatusMessage:[LanguageManager stringForKey:@"No Results..."]];
		return;
	}
	[self.tblContent reloadData];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [self.items count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return [_expandedCells containsObject:indexPath] ? kMPEXPANDED_CELL_HEIGHT : kMPNORMAL_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MPortfolioEquityCell *cell = [tableView dequeueReusableCellWithIdentifier:[MPortfolioEquityCell reuseIdentifier]];
	if (!cell) {
		cell = [[MPortfolioEquityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MPortfolioEquityCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	//Configs
	cell.delegate = self;
	cell.indexPath = indexPath;
	cell.isExpandedCell = [_expandedCells containsObject:indexPath];
	[cell setupDataFrom:self.items[indexPath.row] forPfEquityType:MPfEquityType_Realised];
    //[cell createUIExpand:MPfEquityType_Realised];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	/*
	MPortfolioDetailVC *detailVC = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [MPortfolioDetailVC storyboardID]);
	detailVC.pfModel = self.items[indexPath.row];
	detailVC.detailType = DetailPortfolioType_Realised;
	[self.supperVC nextTo:detailVC animate:YES];
	*/
	//+++ Do it later
}

#pragma mark - MPortfolioEquityCellDelegate
- (void)didTapExpandCell:(MPortfolioEquityCell *)cell{
	NSIndexPath *curIndexPath = cell.indexPath;
	BOOL shouldClose = [_expandedCells containsObject:curIndexPath];
	if (shouldClose) {
		[_expandedCells removeObject:curIndexPath];
	}else{
		[_expandedCells addObject:curIndexPath];
	}
	[_tblContent beginUpdates];
	cell.isExpandedCell = !shouldClose;
	[_tblContent endUpdates];
}

#pragma mark - Handle Notification
- (void)didUpdateContentNotification:(NSNotification *)noti{
	PortfolioData *newPF = noti.object;
	if (newPF) {
		//Check update here
		NSInteger indexOfPF = [self indexOfStockCodeObject:newPF.stockcode fromItems:self.items];
		if (indexOfPF != -1) {
			//Do something here
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfPF inSection:0];
			PortfolioData *oldPF = [self.items[indexOfPF] copy];
			[self.items replaceObjectAtIndex:indexOfPF withObject:newPF];
			MPortfolioEquityCell *cell = [self.tblContent cellForRowAtIndexPath:indexPath];
			if (cell) {
				[cell.contentView makeAnimateByCheckingValue:oldPF.price_last_done.floatValue newValue:newPF.price_last_done.floatValue completion:^{
					[cell setupDataFrom:newPF forPfEquityType:MPfEquityType_Realised];
				}];
			}
		}
	}
}
-(void)didTapStockNameCell:(MPortfolioEquityCell *)cell{
    BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
    NSUInteger brokerTag = brokerModel.tag;
    NSString *identifier;
    if (brokerTag==BrokerTag_SSI) {
        identifier=@"DetailRealisedEquities";
    }
    else{
        identifier=@"DetailRealised";
    }
  PortfolioData *pData=self.items[cell.indexPath.row];
    DetailRealisedPortfolio *_detailVC = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, identifier);
    _detailVC.portfolioData = pData;
    [self nextTo:_detailVC animate:YES];

}
@end
