//
//  DetailRealisedPortfolioEquites.h
//  TC_Mobile30
//
//  Created by NguyenCuong on 5/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "TCBaseView.h"
#import "BasePortfoiloContent.h"
#import "MarqueeLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface DetailRealisedPortfolioEquites : BaseVC
@property (weak, nonatomic) IBOutlet TCBaseView *topView;
@property (weak, nonatomic) IBOutlet TCBaseView *detailView;
@end

NS_ASSUME_NONNULL_END
