//
//  DerSumPFDetailViewController.h
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 3/5/14.
//
//

#import <UIKit/UIKit.h>
#import "PrtfSubDtlRptData.h"
#import "DetailDayTableViewController.h"
#import "DetailOvernightTableViewController.h"
#import "StockModel.h"
#import "BaseVC.h"
#import "TCPageMenuControl.h"
@interface DerSumPFDetailViewController : BaseVC < UIScrollViewDelegate> {
    BOOL isDerivative;
    NSString *trade_exchg_code;
    
    //0: Day
    //1: Overnight
    int detail_type;
    
    __weak IBOutlet UIView *v_button;
    //Tap Buttons
    IBOutlet UIButton *buttonDay;
    IBOutlet UIButton *buttonOvernight;
    
    //SrollView
    IBOutlet UIScrollView *dyOvrnightScroller;
    __weak IBOutlet TCPageMenuControl *_view_control;
    
    //Day
    IBOutlet UIView *dayView;
    IBOutlet UILabel *day_avg_price_header;
    IBOutlet UILabel *day_avg_price_buy;
    IBOutlet UILabel *day_avg_price_sell;
    IBOutlet UILabel *day_total_qty_header;
    IBOutlet UILabel *day_total_qty_buy;
    IBOutlet UILabel *day_total_qty_sell;
    IBOutlet UIImageView *day_tableheader;
    IBOutlet UILabel *day_table_price_header;
    IBOutlet UITableView *day_tableView;
    DetailDayTableViewController *dtlDayTblVController;
    NSMutableArray *dayList;
    BOOL dayDetailLoaded;
    
    //Overnight
    IBOutlet UIView *overnightView;
    IBOutlet UILabel *overnight_avg_price_header;
    IBOutlet UILabel *overnight_avg_price_buy;
    IBOutlet UILabel *overnight_avg_price_sell;
    IBOutlet UILabel *overnight_total_qty_header;
    IBOutlet UILabel *overnight_total_qty_buy;
    IBOutlet UILabel *overnight_total_qty_sell;
    IBOutlet UILabel *overnight_total_unreal_header;
    IBOutlet UILabel *overnight_total_unreal;
    IBOutlet UIImageView *overnight_tableheader;
    IBOutlet UILabel *overnight_table_price_header;
    IBOutlet UITableView *overnight_tableView;
    DetailOvernightTableViewController *dtlOvrnightTblVController;
    NSMutableArray *overnightList;
    BOOL overnightDetailLoaded;
    
    //Toolbar
    IBOutlet UIToolbar *toolBar;
    
    //Activity View
    IBOutlet UIView *activityView;
    IBOutlet UIActivityIndicatorView *activityIndicatorView;
}
@property (nonatomic, assign) PrtfSubDtlRptData *psdrd;
@property (nonatomic, assign) StockModel *stockData;
@property (nonatomic, assign) BOOL isDerivative;
@property (nonatomic, retain) NSString *trade_exchg_code;
@property (nonatomic, retain) DetailDayTableViewController *dtlDayTblVController;
@property (nonatomic, retain) NSMutableArray *dayList;
@property (nonatomic, retain) DetailOvernightTableViewController *dtlOvrnightTblVController;
@property (nonatomic, retain) NSMutableArray *overnightList;

- (IBAction) dayOvernightButtonPressed:(id)sender;
- (IBAction) refreshPortfolioButtonPressed:(id)sender;
- (void) requestPortfolioDetails;

@end
