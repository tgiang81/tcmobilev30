//
//  DetailOvernightTableViewController.h
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 3/9/14.
//
//

#import <UIKit/UIKit.h>

@interface DetailOvernightTableViewController : UITableViewController {

}
@property (nonatomic, assign) UITableView *table_view;
@property (nonatomic, retain) NSString *trade_exchg_code;
@property (nonatomic, assign) NSMutableArray *overnightList;
@end
