//
//  DetailUnrealisedPortfolio.h
//  TC_Mobile30
//
//  Created by Admin on 4/3/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BasePortfoiloContent.h"
#import "TCBaseView.h"
#import "MarqueeLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DetailUnrealisedPortfolio : BaseVC
//@property (weak, nonatomic) IBOutlet TCBaseView *detailView;
@property (weak, nonatomic) IBOutlet TCBaseView *detailView;

@property (weak, nonatomic) IBOutlet TCBaseView *topInfoView;
@property (strong,nonatomic) PortfolioData *portfolioData;

@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockCode;

@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDonePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblLastChangeValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSellable;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblAvgPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBought;
@property (weak, nonatomic) IBOutlet UILabel *lblMaketPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblUnRealisedGL;
@property (weak, nonatomic) IBOutlet UILabel *lblSold;
@property (weak, nonatomic) IBOutlet UILabel *lblUnBoughtT1;
@property (weak, nonatomic) IBOutlet UILabel *lblUnSoldT1;
@property (weak, nonatomic) IBOutlet UILabel *lblBoughtT2;
@property (weak, nonatomic) IBOutlet UILabel *lblSoldT2;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaketValue;
@property (weak, nonatomic) IBOutlet UILabel *lblUnrealizedGLPer;
@property (weak, nonatomic) IBOutlet UILabel *lblPledge;
@property (weak, nonatomic) IBOutlet UILabel *lblRights;
@property (weak, nonatomic) IBOutlet UILabel *lblRestricted;
@property (weak, nonatomic) IBOutlet UILabel *lblRegistered;

@end

NS_ASSUME_NONNULL_END
