//
//  MPortfolioDashBoardViewController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/11/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MPortfolioDashBoardViewController.h"

@interface MPortfolioDashBoardViewController ()

@end

@implementation MPortfolioDashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lbl_TotalUnRealise.text = [LanguageManager stringForKey:@"Unrealised Portfolio Balance"];
    self.lbl_TotalRealise.text = [LanguageManager stringForKey:@"Realised Portfolio Balance"];
    BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
    NSUInteger brokerTag = brokerModel.tag;
    if (brokerTag==BrokerTag_SSI) {
        _lblCashBalance.text=@"Total Equitiy";
        _lblEE.text=@"EE";
    }
    // Do any additional setup after loading the view.
}
- (void)didCalculate:(double)percentChange currentMarketBalance:(double)currentMarketBalance investedBalance:(double)investedBalance page:(MPortfolioPage)page{
    if(page == MPortfolioAccountType_Derivative){
        [self showUI:percentChange currentMarketBalance:currentMarketBalance investedBalance:investedBalance];
    }else{
        [self showUI2:percentChange currentMarketBalance:currentMarketBalance investedBalance:investedBalance lblTotalValue:_lbl_TotalValueRealise lblCash:_lbl_CashValueRealise lblChangedValue:_lbl_changedValueRealise cashView:_realisedCashView balanceView:_realisedBalanceView lblBalance:_lbl_BalanceValueRealise curPage:page];
    }
    
}
- (void)actionDerivativeAccountViews:(MPortfolioPage)curPage{
    if(curPage == MPortfolioAccountType_Derivative){
        [super actionDerivativeAccountViews:curPage];
    }else{
        [self actionDerivativeAccountViews:_lbl_TotalValueRealise lblChangedValue:_lbl_changedValueRealise cashView:_realisedCashView balanceView:_realisedBalanceView];
    }
    
}

- (void)updateTheme:(NSNotification *)noti{
    [super updateTheme:noti];
    
}
@end
