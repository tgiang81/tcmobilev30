//
//  MPortfolioDetailVC.h
//  TCiPad
//
//  Created by n2nconnect on 09/08/2018.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "PortfolioData.h"


//Page
typedef enum : NSInteger{
	DetailPortfolioType_Unrealised = 0,
	DetailPortfolioType_Realised,
	DetailPortfolioType_Virtual,
	DetailPortfolioType_Summary
}DetailPortfolioType;

@interface MPortfolioDetailVC : BaseVC{

}
@property (strong, nonatomic) id pfModel;
@property (assign, nonatomic) DetailPortfolioType detailType;
@end
