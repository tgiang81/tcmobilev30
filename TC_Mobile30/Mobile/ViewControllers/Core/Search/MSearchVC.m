//
//  MSearchVC.m
//  TC_Mobile30
//
//  Created by Kaka on 11/15/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MSearchVC.h"
#import "GroupSearchCell.h"
#import "VertxConnectionManager.h"
#import "NSString+Util.h"
#import "Utils.h"
#import "LinqToObjectiveC.h"
#import "StockInfoVC.h"
#import "TCInactiveButton.h"
#import "TCBaseView.h"
#import "LanguageKey.h"
#import "GHContextMenuView.h"
#import "MTradeViewController.h"
#import "BaseNavigationController.h"
#import "SelectWatchlistPopupVC.h"
#import "MStockInfoVC.h"
#import "TCBubbleMessageView.h"
#import "WatchlistModel.h"
#import "TCBaseLabel.h"
#import "ExchangeSelectionVC.h"
#import "NSString+SizeOfString.h"
#import "ExchangeData.h"

typedef enum : NSInteger{
	MSearchContentType_Search = 0,
	MSearchContentType_Content
}MSearchContentType;

#define kLimited_Search_Return_Value	300
#define kLimited_Bubble_Constrain_Width	110


@interface MSearchVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, GHContextOverlayViewDelegate, GHContextOverlayViewDataSource, SelectWatchlistPopupVCDelegate, ExchangeSelectionVCDelegate>{
	NSDictionary *_stockGroupDict;
	NSArray *_searchResults;
}

//OUTLETS
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet TCBaseView *searchView;
@property (weak, nonatomic) IBOutlet TCInactiveButton *searchIcon;
@property (weak, nonatomic) IBOutlet TCBaseLabel *bubbleExchangeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bubbleConstrainWidth;
@property (weak, nonatomic) IBOutlet TCBaseView *bubbleView;



//Private
@property (strong, nonatomic) ExchangeData *selectedEx;
@end

@implementation MSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//Config:
	_stockGroupDict = @{}.copy;
	_searchResults = @[].copy;
	self.enableTapDismissKeyboard = YES;
	[self registerNotification];
	[self createUI];
	[self loadData];
}
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	//Bar
	self.title = [LanguageManager stringForKey:@"Search"];
    if(self.isShowMenuIcon){
        [self createLeftMenu];
    }else{
       [self setBackButtonDefault];
    }
	
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	[_tblContent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	
	_txtSearch.delegate = self;
	[_txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	_txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtSearch.placeholder=[LanguageManager stringForKey:Search];
	self.bubbleExchangeLabel.cornerRadius = 5.0;
	[self updateTheme:nil];
	[self setupMenuContext];
}

- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	_searchIcon.tintColor = RGB(111, 111, 111);
	_searchView.backgroundColor = RGB(232, 232, 232);
	_tblContent.backgroundColor = [UIColor clearColor];
	[_tblContent reloadData];
}

- (void)setupMenuContext{
	//Add Context Menu
	GHContextMenuView* overlay = [[GHContextMenuView alloc] init];
	overlay.dataSource = self;
	overlay.delegate = self;
	
	UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];
	[self.tblContent addGestureRecognizer:_longPressRecognizer];
}

#pragma mark - LoadData
- (void)loadData{
	//Default exchange is nil == Selected ALL
	_selectedEx = nil;
	[self populateBubbleExchange:_selectedEx];
}
#pragma mark - Action

- (IBAction)onCloseBubbleAction:(id)sender {
	self.selectedEx = nil;
	[self populateBubbleExchange:_selectedEx];
	//Start seaching if need
	if ([_txtSearch.text removeAllWhitespace].length > 0) {
		[self textFieldDidChange:self.txtSearch];
	}
}
- (IBAction)onFilterAction:(id)sender {
	NSArray *exChangeList = [UserPrefConstants singleton].supportedExchanges.copy;
	if (exChangeList.count == 0) {
		//Can show popup here
		return;
	}
	ExchangeSelectionVC *exVC = NEW_VC_FROM_NIB([ExchangeSelectionVC class], [ExchangeSelectionVC nibName]);
	exVC.type = ExchangeSelection_Filter;
	exVC.delegate = self;
	CGSize contentSize = CGSizeMake(0.8 * self.view.frame.size.width, 300);
	if (exChangeList.count < 6) {
		contentSize = CGSizeMake(0.8 * self.view.frame.size.width, kEXCHANGE_CELL_HEIGHT * exChangeList.count + 60);
	}
	[self showPopupWithContent:exVC inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
		//Do something here
	}];
}
- (IBAction)onCancelAction:(id)sender {
	CGRect tmpFrame = self.view.frame;
	tmpFrame.origin.y = tmpFrame.size.height;
	[UIView animateWithDuration:0.3
						  delay:0.1
						options: UIViewAnimationOptionCurveLinear
					 animations:^{
						 self.view.frame = tmpFrame;
					 }
					 completion:^(BOOL finished){
						 self.delegate = nil;
						 [self removeFromParentViewController];
						 [self.view removeFromSuperview];
					 }];
}

#pragma mark - Search UTIL
- (void)populateBubbleExchange:(ExchangeData *)exchange{
	if (!exchange){
		self.bubbleView.hidden = YES;
		_bubbleConstrainWidth.constant = 0;
		[self.view layoutIfNeeded];
		return;
	}
	self.bubbleView.hidden = NO;
	_bubbleExchangeLabel.text = [Utils getShortExChangeName:exchange];
	float widthFromNewName = [_bubbleExchangeLabel.text widthOfString:_bubbleExchangeLabel.font] + 31 + 5;//5 for margin
	if (widthFromNewName > kLimited_Bubble_Constrain_Width) {
		widthFromNewName = kLimited_Bubble_Constrain_Width;
	}
	if (widthFromNewName < 50) {
		widthFromNewName = 50;
	}
	_bubbleConstrainWidth.constant = widthFromNewName;
	_bubbleExchangeLabel.cornerRadius = 0.0;
	[UIView animateWithDuration:0.3 animations:^{
		[self.view layoutIfNeeded];
	}];
}
- (void)searchStockt:(NSString *)searchText{
	[Utils showApplicationHUD];
	NSArray *exChanges;
	if (_selectedEx) {
		exChanges = @[_selectedEx];
	}else{
		exChanges = [UserPrefConstants singleton].supportedExchanges;
	}
	[[VertxConnectionManager singleton] vertxSearch:searchText fromExchange:exChanges startPage:0 limit:kLimited_Search_Return_Value];
}

- (NSString *)exchangeNameFrom:(NSArray *)exchanges byCode:(NSString *)code{
	if (!exchanges || exchanges.count == 0) {
		return @"";
	}
	for (ExchangeData *exData in exchanges) {
		if ([exData.feed_exchg_code isEqualToString:code]) {
			return exData.exchange_description;
		}
	}
	return @"";
}
#pragma mark - Register Notification
- (void)registerNotification{
	[self addNotification:@"doneVertxSearch" selector:@selector(doneVertxSearchNotification:)];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_searchResults count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	GroupSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:[GroupSearchCell reuseIdentifier]];
	//NSArray *allKey = _stockGroupDict.allKeys;
	//NSArray *stockArr = _stockGroupDict[allKey[indexPath.row]];
	//NSString *codeKey = allKey[indexPath.row];
	//[cell updateUIFrom:[self exchangeNameFrom:[UserPrefConstants singleton].userExchagelist byCode:codeKey] count:stockArr.count];
	//+++ Just show name now
	StockModel *model = _searchResults[indexPath.row];
	[cell updateUIFrom:model];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self.view endEditing:YES];
	/*
	NSArray *allKey = _stockGroupDict.allKeys;
	NSArray *stockArr = _stockGroupDict[allKey[indexPath.row]];
	if (_delegate) {
		[_delegate didSelectResultSearch:stockArr];
	}
	//[self onCancelAction:nil];
	*/
	//+++ Should Selected Stock now
	if (self.searchType == MSearchType_Selected) {
		if (_delegate) {
			[_delegate didSelectStock:_searchResults[indexPath.row]];
		}
		return;
	}
	//Goto detail
	MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
	_detailVC.stock = _searchResults[indexPath.row];
	[self nextTo:_detailVC animate:YES];
}

#pragma mark - GHContextMenuDataSource
- (BOOL)shouldShowMenuAtPoint:(CGPoint)point{
	NSIndexPath* indexPath = [self.tblContent indexPathForRowAtPoint:point];
	GroupSearchCell *cell = [self.tblContent cellForRowAtIndexPath:indexPath];
	return cell != nil;
}
- (NSInteger) numberOfMenuItems{
	return [[AppConstants contextMenu] count];
}
- (UIImage*)imageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Buy:
			imageName = @"buy_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}

- (UIImage*) highlightedImageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Buy:
			imageName = @"buy_hl_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_hl_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_hl_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_hl_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}
#pragma mark - GHContextMenuDelegate
- (void)didSelectItemAtIndex:(NSInteger) selectedIndex forMenuAtPoint:(CGPoint)point{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[selectedIndex] integerValue];
	
	//Select Stock Model
	NSInteger index;
	NSIndexPath *indexPath = [self.tblContent indexPathForRowAtPoint:point];
	index = indexPath.row;
	StockModel *selectedStock = _searchResults[index];
	switch (menuType) {
		case MContextMenu_Buy:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Sell:{
			//Sell here
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Sell;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Info:{
			//Detail here
			MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
			_detailVC.stock = selectedStock;
			[self nextTo:_detailVC animate:YES];
		}
			break;
		case MContextMenu_Favorite:{
			//Add favorite here
			SelectWatchlistPopupVC *_selectVC = NEW_VC_FROM_NIB([SelectWatchlistPopupVC class], [SelectWatchlistPopupVC nibName]);
			_selectVC.stock = selectedStock;
			_selectVC.delegate = self;
			[self showPopupWithContent:_selectVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.4 * SCREEN_HEIGHT_PORTRAIT) completion:^(MZFormSheetController *formSheetController) {
				
			}];
		}
			break;
		default:
			break;
	}
	DLog(@"Did select Menu Item: %@", @(selectedIndex));
}

#pragma mark - SelectWatchlistPopupVCDelegate
- (void)didSelectStock:(StockModel *)stock willAddToWatchlist:(WatchlistModel*)watchlist{
    NSInteger watchlistID=watchlist.FavID;
    NSString* watchlistName=watchlist.Name;
	//Call api to add watchlist now
	[[ATPAuthenticate singleton] addStock:stock.stockCode toWatchlist:(int)watchlistID completion:^(BOOL success, id result, NSError *error) {
		if (success) {
			NSString *someDuplicate = [result objectForKey:@"DuplicateStocks"];
			if ([someDuplicate length]>0) {
				[[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlistName}]
											 inController:self withTitle:[LanguageManager stringForKey:Duplicate_Stock]];
				[self showCustomAlertWithMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlistName}]];
			}else{
				//Success
				[self postNotification:kDidFinishAddStockToWatchlistNotification object:nil];
				[TCBubbleMessageView showWindowBubbleMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]];
			}
		}else{
			if (error) {
				[TCBubbleMessageView showErrorWindowBubbleMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]];
			}
		}
	}];
}
#pragma mark - ACTION
- (void)textFieldDidChange:(UITextField *)textField{
	if ([textField.text removeAllWhitespace].length == 0) {
		_searchResults = @[].copy;
		[self.tblContent reloadData];
		_txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
		return;
	}
	NSString *searchText = [textField.text trimmedLeadingAndTrailingWhiteSpace];
	[Utils showApplicationHUD];
	[self searchStockt:searchText];
	
	//Start search
	/*
	double delayInSeconds = 0.5;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[Utils showApplicationHUD];
		[self searchStockt:searchText];
	});
	*/
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldClear:(UITextField *)textField{
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	NSString *searchText = [textField.text trimmedLeadingAndTrailingWhiteSpace];
	[Utils showApplicationHUD];
	[self searchStockt:searchText];
	[textField resignFirstResponder];
	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
	
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
	
}
#pragma mark - Notification
- (void)doneVertxSearchNotification:(NSNotification *)noti{
	[Utils dismissApplicationHUD];
	if ([_txtSearch.text removeAllWhitespace].length == 0) {
		return;
	}
	//FID_33_S_STOCK_CODE
	NSArray *arraryStock = noti.userInfo[FID_33_S_STOCK_CODE];
	NSMutableArray *results = [StockModel arrayOfModelsFromDictionaries:arraryStock error:nil];
	_searchResults = [results copy];
	if (_searchResults.count > 0) {
		_txtSearch.clearButtonMode = UITextFieldViewModeAlways;
	}else{
		_txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
	}
	[self.tblContent reloadData];
	//+++ No need grouping now
	/*
	if (results.count > 0) {
		//Group data now:
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			self->_stockGroupDict = [results linq_groupBy:^id(id item) {
				return [[item stockCode] pathExtension];
			}];
			dispatch_async(dispatch_get_main_queue(), ^(){
				[self.tblContent reloadData];
			});
		});
	}else{
		_stockGroupDict = @{}.copy;
		[self.tblContent reloadData];
	}
	*/
}

#pragma mark - ExchangeSelectionVCDelegate
- (void)didSelectExchange:(ExchangeData *)exData{
	_selectedEx = exData;
	[self populateBubbleExchange:exData];
	//Start seaching if need
	if ([_txtSearch.text removeAllWhitespace].length > 0) {
		[self textFieldDidChange:self.txtSearch];
	}
}
@end
