//
//  MSearchVC.h
//  TC_Mobile30
//
//  Created by Kaka on 11/15/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
//Menu
typedef enum : NSInteger{
	MSearchType_ViewDetail,
	MSearchType_Selected
}MSearchType;

@protocol MSearchVCDelegate<NSObject>
@optional
- (void)didSelectStock:(StockModel *)model;
@end
@interface MSearchVC : BaseVC{
	
}
//Passing data
@property (weak, nonatomic) id <MSearchVCDelegate>delegate;
@property (assign, nonatomic) MSearchType searchType;
@property (assign, nonatomic) BOOL isShowMenuIcon;
//MAIN
- (void)textFieldDidChange:(UITextField *)textField;
@end
