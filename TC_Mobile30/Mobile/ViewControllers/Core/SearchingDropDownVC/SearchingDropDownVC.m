//
//  SearchingDropDownVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 4/17/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "SearchingDropDownVC.h"
#import "SearchStockController.h"
#import "StockModel.h"

@interface SearchingDropDownVC () <TCMenuDropDownDelegate>
{
    SearchStockController *_controller;
    
}
@end

@implementation SearchingDropDownVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.maxRows = 6;
    self.defaultRadius = 4;
    _controller = [SearchStockController new];
    [_controller registerNotifications];
    // Do any additional setup after loading the view.
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_controller removeNotifications];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}
- (void)searchStock:(NSString *)keyword{
    if(keyword != nil && ![keyword isEqualToString:@""] && ![[keyword stringByTrimmingCharactersInSet:
          [NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]){
        [_controller searchAtFirst:keyword completion:^(NSMutableArray *stocks) {
            self->_tableDataArr = [StockModel arrayOfModelsFromDictionaries:stocks error:nil];
            [self reloadSearchData:NO];
            if(stocks.count > 0){
                [self.dv_searchResult openComponent:0 animated:YES];
            }else{
                [self.dv_searchResult closeAll];
            }
        }];
    }else{
        _tableDataArr = [NSArray new];
        [self.dv_searchResult closeAll];
    }
    
}

- (void)reloadSearchData:(BOOL)isClose{
    self.dv_searchResult.numberItemVisible = self.maxRows;
    self.dv_searchResult.menuTitles = [self getStockNames];
    if(isClose){
        [self.dv_searchResult closeAll];
    }
}
- (NSArray *)getStockNames{
    NSMutableArray *names = [NSMutableArray new];
    for(StockModel *model in _tableDataArr){
        [names addObject:model.stockName];
    }
    return names;
}

- (void)setupDropDown:(TCMenuDropDown *)menu title:(NSString *)title contents:(NSArray *)contents{
    menu.useMkDropDown = YES;
    menu.dismissAutoResize = YES;
    menu.selectedLabelFont = AppFont_MainFontRegularWithSize(14);
    menu.titleAlign = NSTextAlignmentLeft;
    menu.height = 37;
    menu.radius = self.defaultRadius;
    menu.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintDropDownButtonColor);
    menu.tintIcon = TC_COLOR_FROM_HEX([[ThemeManager shareInstance] stt_tintDropDownButtonColor]);
    if(contents.count > 0){
        menu.titleMenu = contents[0];
        menu.menuTitles = contents;
    }
    if(title){
        menu.titleMenu = title;
    }
    menu.numberItemVisible = self.maxRows;
    menu.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].a_settingDropDownBg);
    menu.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
    menu.delegate = self;
    [menu reloadDropDown];
    
}
- (void)didSelectStock:(StockModel *)stock{
    
}
#pragma mark TCMenuDropDownDelegate
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
    [self didSelectStock:[self.tableDataArr objectAtIndex:index]];
}
@end
