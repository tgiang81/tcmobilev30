//
//  SearchingDropDownVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 4/17/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "TCMenuDropDown.h"
NS_ASSUME_NONNULL_BEGIN
@class StockModel;
@interface SearchingDropDownVC : BaseVC
@property (assign, nonatomic) NSInteger maxRows;
@property (assign, nonatomic) CGFloat defaultRadius;
@property (strong, nonatomic) NSArray *tableDataArr;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *dv_searchResult;
- (void)setupDropDown:(TCMenuDropDown *)menu title:(NSString *)title contents:(NSArray *)contents;
- (void)reloadSearchData:(BOOL)isClose;
- (NSArray *)getStockNames;
- (void)searchStock:(NSString *)keyword;
- (void)didSelectStock:(StockModel *)stock;
@end

NS_ASSUME_NONNULL_END
