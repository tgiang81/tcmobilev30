//
//  MIndicesDetailViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MIndicesDetailViewController.h"

@interface MIndicesDetailViewController ()

@end

@implementation MIndicesDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myScrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, self.view.frame.size.width * 5)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat aPageWidth = myScrollView.frame.size.width;
    float aFractionalPage = myScrollView.contentOffset.x / aPageWidth;
    NSInteger aPage = lround(aFractionalPage);
    myPageControl.currentPage = aPage;
}



@end
