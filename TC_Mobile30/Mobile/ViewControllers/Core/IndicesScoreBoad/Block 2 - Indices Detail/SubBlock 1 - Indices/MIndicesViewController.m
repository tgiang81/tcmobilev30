//
//  MConstructn.m
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MIndicesViewController.h"
#import "CustomCollectionViewLayout.h"
#import "StockDetaiCell.h"

@interface MIndicesViewController()
{
    long indexCounter;
    NSString *stockcodeTimeandSales;
    long selectedRow, prevSelectedRow;
    BOOL isupdate;
}

@property (nonatomic, strong) NSMutableArray *indicesValArray;
@property (nonatomic, retain) NSMutableArray *indicesTempArray;

@end

@implementation MIndicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = IndicesScore;
    aLayout.numberOfCols = 5;
    myCollectionView .collectionViewLayout = aLayout;
    [myVertxConnectionManager vertxGetIndices];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedDoneGetIndices:) name:@"DoneGetIndices" object:nil];
    isupdate = NO;
    indexCounter=0;
    self.indicesValArray  = [[NSMutableArray alloc] init];
    self.indicesTempArray = [[NSMutableArray alloc] init];
    myCollectionView.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.indicesValArray.count > 0) {
        return self.indicesValArray.count + 1;
    } else {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];

    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.name.text = @"Name";
        } else  if (indexPath.row == 1) {
            cell.name.text = @"Prev";
        } else  if (indexPath.row == 2) {
            cell.name.text = @"Current";
        } else  if (indexPath.row == 3) {
            cell.name.text = @"Chg";
        } else  if (indexPath.row == 4) {
            cell.name.text = @"Chg%";
        }
        cell.name.textColor = [UIColor whiteColor];
    } else {
        int row = (int)indexPath.section - 1;
        
        if (row==selectedRow) {
            cell.name.textColor = [UIColor cyanColor];
        } else {
            cell.name.textColor = [UIColor whiteColor];
        }
        NSString *stockCode = [[_indicesValArray objectAtIndex:row]objectForKey:FID_38_S_STOCK_NAME];
        NSArray *splitStkCode = [stockCode componentsSeparatedByString:@"."];
        CGFloat close = [[[_indicesValArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue];
        CGFloat lastDone = [[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
        CGFloat chg = lastDone - close ;
        CGFloat chgTemp = chg / close * 100;
        if (close<=0) chg = 0;
        NSString *chgP = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:chgTemp]];
        if (indexPath.row ==0) {
            cell.name.text =  [splitStkCode objectAtIndex:0];
        } else if (indexPath.row == 1) {
            [priceFormatter setRoundingMode: NSNumberFormatterRoundDown];
           cell.name.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_indicesValArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue]]];
        } else if (indexPath.row == 2) {
            [priceFormatter setRoundingMode: NSNumberFormatterRoundDown];
            cell.name.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue]]];
            if (chgTemp < 0) {
                cell.name.textColor = [UIColor redColor];
            } else if (chgTemp > 0) {
                cell.name.textColor = [UIColor greenColor];
            } else {
                cell.name.textColor = [UIColor greenColor];
            }
        } else if (indexPath.row == 3) {
            cell.name.text = [[UserPrefConstants singleton] abbreviatePrice:chg];
            if (chgTemp < 0) {
                cell.name.textColor = [UIColor redColor];
            } else if (chgTemp > 0) {
                cell.name.textColor = [UIColor greenColor];
            } else {
                cell.name.textColor = [UIColor whiteColor];
            }
        } else if (indexPath.row == 4){
            cell.name.text= chgP;
            if (chgTemp < 0) {
                cell.name.textColor = [UIColor redColor];
            } else if (chgTemp > 0) {
                cell.name.textColor = [UIColor greenColor];
            } else {
                cell.name.textColor = [UIColor whiteColor];
            }
        }

        if (isupdate && _indicesTempArray.count > 0) {
            
            CGFloat indicesCurrentValue=[[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
            CGFloat tempCurrentValue=[[[_indicesTempArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
            if (indexPath.row == 2) {
                UIColor *prevClr = cell.name.textColor;
                [UIView animateWithDuration:0.0 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                    if(indicesCurrentValue > tempCurrentValue) {
                        cell.name.layer.backgroundColor=[kGrnFlash CGColor];
                    } else if (indicesCurrentValue < tempCurrentValue) {
                        cell.name.layer.backgroundColor=[kRedFlash CGColor];
                    }
                    
                    cell.name.textColor = [UIColor whiteColor];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                        cell.name.layer.backgroundColor=[UIColor clearColor].CGColor;
                        cell.name.textColor = prevClr;
                    } completion:nil];
                }];
            }

            CGFloat tempclose = [[[_indicesTempArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue];
            CGFloat templastDone = [[[_indicesTempArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
            CGFloat tempchg = templastDone - tempclose ;
            tempchg = tempchg / tempclose * 100;
            NSString *tempchgP = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:tempchg]];
            
            if (indexPath.row == 3) {
                UIColor *prevClr = cell.name.textColor;
                [UIView animateWithDuration:0.0 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                    if (chgTemp > tempchg)
                        cell.name.layer.backgroundColor=[kGrnFlash CGColor];
                    else if(chg < tempchg){
                        cell.name.layer.backgroundColor=[kRedFlash CGColor];
                    }
                    cell.name.textColor = [UIColor whiteColor];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                        cell.name.layer.backgroundColor=[UIColor clearColor].CGColor;
                        cell.name.textColor = prevClr;
                    } completion:nil];
                }];
            }
            if (indexPath.row == 4) {
                UIColor *prevClr = cell.name.textColor;
                [UIView animateWithDuration:0.0 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                    if (chgP.floatValue > tempchgP.floatValue)
                        cell.name.layer.backgroundColor=[kGrnFlash CGColor];
                    else if(chgP.floatValue <tempchgP.floatValue)
                        cell.name.layer.backgroundColor=[kRedFlash CGColor];
                    cell.name.textColor = [UIColor whiteColor];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                        cell.name.layer.backgroundColor=[UIColor clearColor].CGColor;
                        cell.name.textColor = prevClr;
                    } completion:nil];
                }];
            }
        }
        
        isupdate = NO;
    }
    return cell;
}

- (void)animateBlinkView:(UIView *)view{
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        view.layer.backgroundColor = [UIColor clearColor].CGColor;
    } completion:nil];
}

#pragma Server

- (void)didFinishedDoneGetIndices:(NSNotification *)notification
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString * response = [notification object];
        if (indexCounter>100) indexCounter = 1;
        if ([[myQuoteData qcIndicesDict] count]<=0) return;
        // FIRST TIME
        if (indexCounter==0) {
            // SORT IT
            NSMutableArray *independentArray = [[NSMutableArray alloc] init];
            for(id key in [myQuoteData qcIndicesDict]) {
                NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[myQuoteData qcIndicesDict] objectForKey:key]];
                [independentArray addObject:eachDict];
            }
            NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
            NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            _indicesValArray = [NSMutableArray arrayWithArray:sortedArray];
            
            NSString *firstItemStockCode = [[_indicesValArray objectAtIndex:0] objectForKey:FID_33_S_STOCK_CODE];
            NSString *latestTradedNumber = [[[_indicesValArray objectAtIndex:0] objectForKey:FID_103_I_TRNS_NO] stringValue];
            
            stockcodeTimeandSales=firstItemStockCode;
            
            [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:stockcodeTimeandSales begin:@"0"
                                                                            end:latestTradedNumber
                                                                       exchange:[UserPrefConstants singleton].userCurrentExchange ];
            
            
//            _chartTitleLabel.text = [LanguageManager stringForKey:@"Movement & Chart - %@" withPlaceholders:@{@"%moveChart%":[[_indicesValArray objectAtIndex:0] objectForKey:FID_39_S_COMPANY]}];
            selectedRow = 0;
            [myCollectionView reloadData];
//            NSArray *splitStkCode = [firstItemStockCode componentsSeparatedByString:@"."];
//            [self loadChartMethodForIndex:[splitStkCode objectAtIndex:0]];
            // UPDATING
        }  else {
            
            // NSLog(@"------------------------------ \n\n");
            
            if ([_indicesValArray count]==0) return;
            
            __block long updateRow = -1;
            [_indicesValArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary *eachIndex = (NSDictionary*)obj;
                
                if ([[eachIndex objectForKey:FID_33_S_STOCK_CODE] isEqualToString:response]) {
                    updateRow = idx;
                    *stop = YES;
                }
            }];
            
            if (updateRow>=0) {
                
                _indicesTempArray = [NSMutableArray arrayWithArray:_indicesValArray];
                // update indicesValArray from qcIndices dict
                NSDictionary *updatedDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:response]];
                
                [_indicesValArray replaceObjectAtIndex:updateRow withObject:updatedDict];
                
                NSIndexPath* rowToReload1 = [NSIndexPath indexPathForRow:2 inSection:(updateRow + 1)];
                NSIndexPath* rowToReload2 = [NSIndexPath indexPathForRow:3 inSection:(updateRow + 1)];
                NSIndexPath* rowToReload3 = [NSIndexPath indexPathForRow:4 inSection:(updateRow + 1)];
                // check if row is visible and already loaded before reloading
                if ([myCollectionView.indexPathsForVisibleItems containsObject:rowToReload1]) {
                    isupdate = YES;
                    [myCollectionView performBatchUpdates:^{
                        [myCollectionView reloadItemsAtIndexPaths:@[rowToReload1, rowToReload2, rowToReload3]];
                    } completion:nil];
                }
            }
        }
        
        indexCounter++;
    });
    // NSLog(@"qcIndices %@",[[QCData singleton]qcIndicesDict]);
}


@end
