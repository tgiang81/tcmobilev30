//
//  MIndicesDetailViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MIndicesBaseViewController.h"

@interface MIndicesDetailViewController : MIndicesBaseViewController
{
    
    __weak IBOutlet UIScrollView *myScrollView;
    __weak IBOutlet UIPageControl *myPageControl;
}

@end
