//
//  MAceMarketViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MAceMarketViewController.h"
#import "CustomCollectionViewLayout.h"
#import "StockDetaiCell.h"

@interface MAceMarketViewController ()
{
    NSMutableDictionary *aceMarketDict;
    NSMutableDictionary *totalMarketDict;
    long long totalUp;
    long long totalDown;
    long long totalUnchg;
    long long totalUntrd;
    long long totalVol;
    long long totalTotal;
    long long totalValue;
}

@property (nonatomic, retain) NSMutableArray *acesMktArray;

@end


@implementation MAceMarketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = IndicesMainMarket;
    aLayout.numberOfCols = 7;
    myCollectionView.collectionViewLayout = aLayout;
    aceMarketDict          = [NSMutableDictionary dictionary];
    totalMarketDict         = [NSMutableDictionary dictionary];
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"]; //Ace
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneVertxGetScoreBoardWithExchange:) name:@"doneVertxGetScoreBoardWithExchange" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.acesMktArray.count > 0) {
        return self.acesMktArray.count + 1;
    } else {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.name.text = @"Sector";
        } else if (indexPath.row == 1) {
            cell.name.text = @"Vol";
        } else if (indexPath.row == 2) {
            cell.name.hidden = YES;
            cell.imageIcon.hidden = NO;
            [cell.imageIcon setImage:[UIImage imageNamed:@"upFlag.png"]];
        } else if (indexPath.row == 3) {
            cell.name.hidden = YES;
            cell.imageIcon.hidden = NO;
            [cell.imageIcon setImage:[UIImage imageNamed:@"downFlag.png"]];
        } else if (indexPath.row == 4) {
            cell.name.hidden = YES;
            cell.imageIcon.hidden = NO;
            [cell.imageIcon setImage:[UIImage imageNamed:@"unChangeFlag.png"]];
        } else if (indexPath.row == 5) {
            cell.name.hidden = YES;
            cell.imageIcon.hidden = NO;
            [cell.imageIcon setImage:[UIImage imageNamed:@"flatFlag.png"]];
        } else if (indexPath.row == 6) {
            cell.name.text = @"Total";
        }
        cell.name.textColor = [UIColor whiteColor];
    } else {
        int row = (int)indexPath.section - 1;
        
        //        if (row==selectedRow1) {
        //            cellScoreBoardMain.lblName.textColor = [UIColor cyanColor];
        //        } else {
        //            cellScoreBoardMain.lblName.textColor = [UIColor whiteColor];
        //        }
        NSString *stockCode = [[_acesMktArray objectAtIndex:row]objectForKey:FID_37_S_SECTOR_NAME];
        if (indexPath.row == 0) {
            cell.name.text = stockCode;
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 1) {
            NSNumber *vol =[[_acesMktArray objectAtIndex:row]objectForKey:FID_110_I_SCORE_VOL];
            NSString *volume = [[UserPrefConstants singleton] abbreviateNumber:[vol intValue]];
            cell.name.text   = volume;
            cell.name.textColor = [UIColor yellowColor];
        } else if (indexPath.row == 2) {
            cell.name.text    = [[[_acesMktArray objectAtIndex:row] objectForKey:FID_105_I_SCORE_UP]stringValue];
            cell.name.textColor = [UIColor greenColor];
        } else if (indexPath.row == 3) {
            cell.name.text    = [[[_acesMktArray objectAtIndex:row] objectForKey:FID_106_I_SCORE_DW]stringValue];
            cell.name.textColor = [UIColor redColor];
        } else if (indexPath.row == 4) {
            cell.name.text    = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_107_I_SCORE_UNCHG]stringValue];
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 5) {
            cell.name.text    = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_108_I_SCORE_NOTRD]stringValue];
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 6) {
            cell.name.text    = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_109_I_SCORE_TOTAL]stringValue];
            cell.name.textColor = [UIColor yellowColor];
        }
        
        //         NSString *totalMainScore = [[UserPrefConstants singleton] abbreviateNumber:[[totalMarketDict objectForKey:@"mainTotalVol"] longLongValue] ];
        
        
        //        _MainMarketVoltotal.text= totalMainScore;//[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalVol"] ];
        //        _MMupTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalUp"] ];
        //        _MMDownTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalDown"] ];
        //        _MMUnchagTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalUnchg"] ];
        //        _MMUntradTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalUntraded"] ];
        //        _MMTotalTotal.text= [quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalTotal"] ];
        
    }
    
    
    
    return cell;
}

#pragma Server

- (void)doneVertxGetScoreBoardWithExchange:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSDictionary * response = [notification.userInfo copy];
        ////NSLog(@"doneVertxGetScoreBoardWithExchange response : %@",response);
        
        // NSLog(@"doneVertxGetScoreBoardWithExchange: %@",[[QCData singleton]qcScoreBoardDict]);
        NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_37_S_SECTOR_NAME ascending:YES];
        
        aceMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2204]];
        NSArray * sortedArray = [[aceMarketDict allValues] sortedArrayUsingDescriptors:@[sortDescriptor]];
        _acesMktArray = [NSMutableArray arrayWithArray:sortedArray];
        
        
        //Self calculate 20 jan 2015
        [self calculateCustomFields:aceMarketDict];
        [myCollectionView reloadData];
    });
}
- (void)calculateCustomFields:(NSDictionary *)marketDict
{
    NSArray *headerFIDs = @[@"37",@"110",@"105",@"106",@"107",@"108",@"109",@"111"];
    
    for (NSDictionary *dict in [marketDict allValues])//Mining, REITS, IPC, Finance...
    {
        for(NSString *fid in headerFIDs)//UP, Down, Unchanged..
        {
            if([fid isEqualToString:@"105"])
            {
                totalUp += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"106"])
            {
                totalDown += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"107"])
            {
                totalUnchg += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"108"])
            {
                totalUntrd += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"109"])
            {
                totalTotal += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"110"])
            {
                totalVol += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"111"])
            {
                totalValue += [[dict objectForKey:fid]integerValue];
            }
        }
    }
    
    NSString *marketID;
    if (marketDict)
    {
        marketID = [[[[marketDict allValues]objectAtIndex:0]objectForKey:@"35"]stringValue];
    }
    if([marketID isEqualToString:@"2201"])
    {
        ////NSLog(@"v2201: %@",[marketDict objectForKey:@"35"]);
        
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUp]     forKey:@"mainTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalDown]   forKey:@"mainTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUnchg]  forKey:@"mainTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUntrd]  forKey:@"mainTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalTotal]  forKey:@"mainTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"mainTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalValue]  forKey:@"mainTotalValue"];
    }
    else if([marketID isEqualToString:@"2203"])
        
    {
        ////NSLog(@"v2203 %@",[marketDict objectForKey:@"35"]);
        
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUp]     forKey:@"warTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalDown]   forKey:@"warTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUnchg]  forKey:@"warTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUntrd]  forKey:@"warTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalTotal]  forKey:@"warTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"warTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalValue]  forKey:@"warTotalValue"];
        
    }
    else if([marketID isEqualToString:@"2204"])
    {
        ////NSLog(@"v2204: %@",[marketDict objectForKey:@"35"]);
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUp]     forKey:@"aceTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalDown]   forKey:@"aceTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUnchg]  forKey:@"aceTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUntrd]  forKey:@"aceTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalTotal]  forKey:@"aceTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"aceTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalValue]  forKey:@"aceTotalValue"];
    }
    
    totalUp = 0;
    totalDown = 0;
    totalUnchg = 0;
    totalUntrd = 0;
    totalTotal = 0;
    totalVol = 0;
    totalValue = 0;
}


@end
