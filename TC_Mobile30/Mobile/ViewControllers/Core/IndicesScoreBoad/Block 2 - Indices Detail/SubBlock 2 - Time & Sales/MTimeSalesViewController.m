//
//  MTimeSalesViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MTimeSalesViewController.h"
#import "CustomCollectionViewLayout.h"
#import "StockDetaiCell.h"

@interface MTimeSalesViewController()
{
    NSMutableArray *indicesTimeSalesArr;
    NSString *stockcodeTimeandSales;
    NSDate *currentTime;
}
@end

@implementation MTimeSalesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = IndicesTimeSales;
    aLayout.numberOfCols = 2;
    myCollectionView .collectionViewLayout = aLayout;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedVertxGetIndicesTimeAndSales:) name:@"didFinishedTimeAndSales" object:nil];
    [self getTimeAndSale];
    currentTime = [NSDate date];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

#pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (indicesTimeSalesArr.count > 0) {
        return indicesTimeSalesArr.count + 1;
    } else {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    cell.name.textColor = [UIColor whiteColor];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.name.text = @"Time";
        } else if (indexPath.row == 1){
             cell.name.text = @"Index Value";
        }
    } else {
        NSUInteger section = indicesTimeSalesArr.count - 1 - indexPath.section;
        if ([indicesTimeSalesArr count]>1) {
            
            NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
            [dateFormater setDateFormat:@"HHmmss"];
            if ((int)section>=0) {
                NSDate *currentDate = [dateFormater dateFromString:[[indicesTimeSalesArr objectAtIndex:section]objectForKey:@"0"]];
                [dateFormater setDateFormat:@"HH:mm:ss"];
                NSString *convertedDateString = [dateFormater stringFromDate:currentDate];
                if (indexPath.row == 0) {
                    cell.name.text = convertedDateString;
                } else if (indexPath.row == 1) {
                    cell.name.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[indicesTimeSalesArr objectAtIndex:section]objectForKey:@"2"] floatValue] ]];
                }
            }
        }
    }
   
    return cell;
}

#pragma Server

-(void)getTimeAndSale
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([[[QCData singleton] qcIndicesDict] count]<=0) return;
        
        // SORT IT
        NSMutableArray *independentArray = [[NSMutableArray alloc] init];
        for(id key in [[QCData singleton] qcIndicesDict]) {
            NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:key]];
            [independentArray addObject:eachDict];
        }
        NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
        NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
        NSMutableArray*  anIndicesValArray = [NSMutableArray arrayWithArray:sortedArray];
        
        NSString *firstItemStockCode = [[anIndicesValArray objectAtIndex:0] objectForKey:FID_33_S_STOCK_CODE];
        NSString *latestTradedNumber = [[anIndicesValArray objectAtIndex:0] objectForKey:FID_103_I_TRNS_NO] ;
        
        stockcodeTimeandSales=firstItemStockCode;
        
        [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:stockcodeTimeandSales begin:@"0"
                                                                        end:latestTradedNumber
                                                                   exchange:[UserPrefConstants singleton].userCurrentExchange ];
        
    });
}

- (void)didFinishedVertxGetIndicesTimeAndSales:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary * response = [notification.userInfo copy];
        indicesTimeSalesArr = [response objectForKey:@"data"];
        [myCollectionView reloadData];
        currentTime = [NSDate date];
//        _timeSalesTimeStamp.text= [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];

    });
}


@end
