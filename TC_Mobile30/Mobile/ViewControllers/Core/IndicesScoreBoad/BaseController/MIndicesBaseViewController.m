//
//  IndicesBaseViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MIndicesBaseViewController.h"

@interface MIndicesBaseViewController ()

@end

@implementation MIndicesBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    myVertxConnectionManager = [VertxConnectionManager singleton];
    myATPAuthenticate = [ATPAuthenticate singleton];
    myQuoteData = [QCData singleton];
    [self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CreateUI
- (void)createUI{
    [self createLeftMenu];
}

@end
