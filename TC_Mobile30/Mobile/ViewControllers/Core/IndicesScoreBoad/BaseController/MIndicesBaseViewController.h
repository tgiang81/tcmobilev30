//
//  IndicesBaseViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "ATPAuthenticate.h"
#import "VertxConnectionManager.h"
#import "QCData.h"

@interface MIndicesBaseViewController : BaseVC
{
    ATPAuthenticate* myATPAuthenticate;
    VertxConnectionManager* myVertxConnectionManager;
    QCData* myQuoteData;
}

@property (weak, nonatomic) NSString *stockCode;

@end
