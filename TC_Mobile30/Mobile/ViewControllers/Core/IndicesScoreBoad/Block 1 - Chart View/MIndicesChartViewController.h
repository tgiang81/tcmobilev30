//
//  IndicesChartViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MIndicesBaseViewController.h"

@interface MIndicesChartViewController : MIndicesBaseViewController <UIWebViewDelegate>
{
    
    __weak IBOutlet UIWebView *myChartView;
    __weak IBOutlet UIImageView *myImageChart;
    __weak IBOutlet UILabel *myErrorMessage;
    __weak IBOutlet UIActivityIndicatorView *myLoading;
}



@end
