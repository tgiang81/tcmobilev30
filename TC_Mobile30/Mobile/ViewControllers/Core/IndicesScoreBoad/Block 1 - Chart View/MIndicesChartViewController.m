//
//  IndicesChartViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/17/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MIndicesChartViewController.h"
#import "ImgChartManager.h"
#import "LanguageKey.h"

@interface MIndicesChartViewController ()
{
    long indexCounter;
}

@end

@implementation MIndicesChartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    myChartView.delegate = self;
    indexCounter = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedDoneGetIndices:) name:@"DoneGetIndices" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma Servcer

- (void)didFinishedDoneGetIndices:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (indexCounter>100) indexCounter = 1;
        if ([[[QCData singleton] qcIndicesDict] count]<=0) return;
        if (indexCounter==0) {
        
        // SORT IT
            NSMutableArray *independentArray = [[NSMutableArray alloc] init];
            for(id key in [[QCData singleton] qcIndicesDict]) {
                NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:key]];
                [independentArray addObject:eachDict];
            }
            NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
            NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            NSMutableArray*  anIndicesValArray = [NSMutableArray arrayWithArray:sortedArray];
            
            NSString *firstItemStockCode = [[anIndicesValArray objectAtIndex:0] objectForKey:FID_33_S_STOCK_CODE];
            
            NSArray *splitStkCode = [firstItemStockCode componentsSeparatedByString:@"."];
            [self loadChartMethodForIndex:[splitStkCode objectAtIndex:0]];
        }
        indexCounter ++;
        
    });
}

#pragma mark - Charts

-(void)setChartStatus:(long)status {
    
    NSString *errormsg = [LanguageManager stringForKey:Chart_Not_Available];
    
    //success
    if (status==1) {
        [myErrorMessage setHidden:YES];
        [myLoading setHidden:YES];
        [myImageChart setHidden:NO];
    }
    
    //no url
    if (status==2) {
        [myErrorMessage setHidden:NO];
        [myLoading setHidden:YES];
        [myImageChart setHidden:YES];
    }
    
    // error
    if (status==3) {
        [myErrorMessage setHidden:NO];
        [myLoading setHidden:YES];
        [myImageChart setHidden:YES];
    }
    
    // loading
    if (status==4) {
        [myErrorMessage setHidden:YES];
        [myLoading setHidden:NO];
        [myImageChart setHidden:YES];
    }
    
    if (status==5) {
        [myErrorMessage setHidden:YES];
        [myLoading setHidden:YES];
        [myImageChart setHidden:YES];
        
    }
    
    // error
    if (status==6) {
        errormsg = [LanguageManager stringForKey:Load_Chart_Fail___Try_again_later__];
        [myErrorMessage setHidden:NO];
        [myLoading setHidden:YES];
        [myImageChart setHidden:YES];
    }
    
    myErrorMessage.text = errormsg;
}




-(void)loadChartMethodForIndex:(NSString*)index {
    
    
    NSString *chartTypeToLoad = [UserPrefConstants singleton].chartTypeToLoad;
    
    
    // ========================================
    //                MODULUS
    // ========================================
    //chartTypeToLoad = @"Modulus";
    
    
    if ([chartTypeToLoad isEqualToString:@"Modulus"]) {
        
        [self setChartStatus:5];
        
        // &isstock=N
        
        NSString *url = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m&isstock=N",[UserPrefConstants singleton].interactiveChartURL, index,[UserPrefConstants singleton].userCurrentExchange];
        
        //NSLog(@"modulus %@", url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        myChartView.scrollView.scrollEnabled = NO;
        myChartView.scrollView.bounces = NO;
        [myChartView loadRequest:request];
        
        [myImageChart setHidden:YES];
        [myChartView setHidden:NO];
    }
    
    
    //NSString *urlString = [NSString stringWithFormat:@"http://chart.asiaebroker.com/chart/intraday?w=300&h=200&k=%@",swdc.stockCodeForDetail];
    // ========================================
    //                IMAGE
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Image"]) {
        
        [self setChartStatus:4];
        
        NSString *urlString = [NSString stringWithFormat:@"%@t=1&w=%.0f&h=%.0f&k=%@.%@&c=b",
                               [UserPrefConstants singleton].intradayChartURL,
                               myImageChart.frame.size.width,
                               myImageChart.frame.size.height,
                               index,[UserPrefConstants singleton].userCurrentExchange];
        
        ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
        [imgChartManager getImageChartWithURL:urlString
                            completedWithData:^(NSData *data) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIImage *chartImg = [UIImage imageWithData:data];
                                    myImageChart.image = chartImg;
                                    [self setChartStatus:1];
                                });
                            }
                                      failure:^(NSString *errorMsg) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self setChartStatus:3];
                                          });
                                      }];
        
        [myImageChart setHidden:NO];
        [myChartView setHidden:YES];
    }
    
    //https://poc.asiaebroker.com/mchart/index_POC.jsp?code=1023&Name=Annica&exchg=KL&mode=d&color=b&lang=en&key=%01%0E%01%03%00%00
    
    // ========================================
    //                TELETRADER
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"TeleTrader"]) {
        
        [self setChartStatus:5];
        
        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
        
        NSString *url = [NSString stringWithFormat:@"%@code=%@&Name=%@&exchg=%@&mode=d&color=b&lang=en&key=%@",
                         [UserPrefConstants singleton].interactiveChartURL,
                         index,
                         [splitStkCode objectAtIndex:1],
                         [UserPrefConstants singleton].userCurrentExchange,
                         [[UserPrefConstants singleton] encryptTime]];
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        myChartView.scrollView.scrollEnabled = NO;
        myChartView.scrollView.bounces = NO;
        [myChartView loadRequest:request];
        
        [myImageChart setHidden:YES];
        [myChartView setHidden:NO];
    }
    
}

#pragma WebView delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    // Do whatever you want here
    
    [myLoading stopAnimating];
    
    NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) urlResponse.response;
    NSInteger statusCode = httpResponse.statusCode;
    if (statusCode > 399) {
        //NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response":httpResponse}];
        // Forward the error to webView:didFailLoadWithError: or other
        //NSLog(@"%@", error);
        [myErrorMessage setHidden:NO];
        [myImageChart setHidden:YES];
        [myChartView setHidden:YES];
    }
    else {
        // No HTTP error
        [myErrorMessage setHidden:YES];
        [myImageChart setHidden:YES];
        [myChartView setHidden:NO];
    }
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self setChartStatus:6];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
 
}


@end
