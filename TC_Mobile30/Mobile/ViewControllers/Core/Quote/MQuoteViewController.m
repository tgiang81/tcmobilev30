//
//  MQuoteViewController.m
//  TC_Mobile30
//
//  Created by Kaka on 12/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MQuoteViewController.h"
#import "TCPageMenuControl.h"
#import "UIImageView+TCUtil.h"
#import "TCDropDown.h"
#import "SVPullToRefresh.h"
#import "TCStatusInfoView.h"
#import "TCPageMenuControl.h"
#import "NSString+SizeOfString.h"

#import "BaseCollectionView.h"
#import "MSearchVC.h"
#import "NestedCollectionViewCell.h"
#import "StockTypeSingleCell.h"
#import "StockHeatmapCell.h"
#import "GHContextMenuView.h"

#import "StockInfoVC.h"
#import "MStockInfoVC.h"
#import "MSectorVC.h"
#import "TCBubbleMessageView.h"

#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"

#import "SelectWatchlistPopupVC.h"
#import "MTradeViewController.h"
#import "BaseNavigationController.h"
#import "ImageCacheManager.h"
#import "LanguageKey.h"
#import "WatchlistModel.h"

#define kDefault_Height_Instruction_View	24


@interface MQuoteViewController ()<UITableViewDelegate, UITableViewDataSource, StockTypeSingleCellDelegate, UICollectionViewDelegate, UICollectionViewDataSource, TCControlViewDelegate, MSectorVCDelegate, GHContextOverlayViewDelegate, GHContextOverlayViewDataSource, SelectWatchlistPopupVCDelegate, UICollectionViewDelegateFlowLayout>{
	//Data
	NSString *_rankNameKey;
	NSMutableArray *_expandedCells;
	NSMutableArray *_quotes;
	
	TCBubbleMessageView *_bubbleMsgView;
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UIView *filterDropdowView;

@property (weak, nonatomic) IBOutlet TCPageMenuControl *menuExchange;
@property (weak, nonatomic) IBOutlet UIView *instructionView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIImageView *imgArrowDown;

//On Filter
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnSwitchContent;
@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet UILabel *lblFilterDropdown;

//On InstructionView
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Name;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Change;
//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLabelIns_Change_Constrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstrainOfInstructionView;

@property (weak, nonatomic) IBOutlet UITableView *tblContent;

//********************** Control ************************
@property (strong, nonatomic) TCDropDown *topRankDropdown;
//Embeded CollectionView
@property (strong, nonatomic) BaseCollectionView *curCollectionView;

//Pull To Refresh
@property (strong, nonatomic) SVPullToRefreshView* pullToRefreshTop;
@property (strong, nonatomic) SVPullToRefreshView* pullToRefreshBottom;

//********************** Data ***************************
@property (assign, nonatomic) MContentType contentType;
@property (assign, nonatomic) NSInteger curSelectedExchange;
@property (assign) int curPageLoading; //For pagination
@end

@implementation MQuoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//Init data
    if(!self.isHome){
        [self createLeftMenu];
    }
	_expandedCells = @[].mutableCopy;
	_quotes = @[].mutableCopy;
	_curPageLoading = 0;
	[UserPrefConstants singleton].quoteScrPage = 0;
	self.contentType = MContentType_Single;
	_bubbleMsgView = [TCBubbleMessageView new];
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)viewWillLayoutSubviews{
	[super viewWillLayoutSubviews];
}

- (void)viewDidLayoutSubviews{
	[super viewDidLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

//- (void)viewWillDisappear:(BOOL)animated{
//	[super viewWillDisappear:animated];
//	[self removeNotification:kVertxNo_pnWebSocketConnected];
//	[self removeNotification:@"pnWebSocketDisconnected"];
//}
- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self.view layoutIfNeeded];
	[self updateTheme:nil];
    [self setupLanguage:nil];
	//Bar
	[self createLeftMenu];
	
	//TableView
	[_tblContent registerNib:[UINib nibWithNibName:[NestedCollectionViewCell nibName] bundle:nil] forCellReuseIdentifier:[NestedCollectionViewCell reuseIdentifier]];
	[_tblContent registerNib:[UINib nibWithNibName:[StockTypeSingleCell nibName] bundle:nil] forCellReuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	[_tblContent setSeparatorColor:kAppColor_BaseLine_Color];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	
	//Add Context Menu
	GHContextMenuView* overlay = [[GHContextMenuView alloc] init];
	overlay.dataSource = self;
	overlay.delegate = self;
	
	UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];
	[self.tblContent addGestureRecognizer:_longPressRecognizer];
	
	//TopRank
	[self setupTopRankDropdown];
	//Exchange
	[self setupMenuExchange];
	//Pull Refresh
	[self setupPullToRefresh];
}

- (void)setupLanguage: (NSNotificationCenter *) noti{
	self.title = [LanguageManager stringForKey:Quote_Screen];
	//self.navigationItem.prompt = @"Plz show Quote Screen";
}

- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	self.contentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	self.tblContent.backgroundColor = [UIColor clearColor];
	
	_lblIns_Name.textColor =  TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subTintIconColor);
	_lblIns_Change.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subTintIconColor);
	
	_lblFilterDropdown.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_tintIconColor);
	[_imgArrowDown toColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_tintIconColor)];
	_btnFilter.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_tintIconColor);;
	_btnSearch.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_tintIconColor);;
	_btnSwitchContent.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_tintIconColor);;
	
	_instructionView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subHeaderBg);
	
	_menuExchange.bgColor = [UIColor clearColor];
	_filterView.backgroundColor = [UIColor clearColor];
	
	//For tableView
	[self.tblContent reloadData];
}

//For Dropdow - TopRank
- (void)setupTopRankDropdown{
	//Rank
	NSArray *_rankedByNameArr = [Utils rankedByNameArr];
	//Create DropDown
	_topRankDropdown = [[TCDropDown alloc] init];
	_topRankDropdown.dataSources = _rankedByNameArr;
	_topRankDropdown.sourceView = _filterDropdowView;
	_topRankDropdown.isSearchable = NO;
	_topRankDropdown.offset = 0;
	_rankNameKey = _rankedByNameArr.firstObject;
    _lblFilterDropdown.text = [LanguageManager stringForKey:_rankNameKey];
	[_topRankDropdown didSelectItem:^(NSString *item, NSInteger index) {
		//No need back to first page
		//[UserPrefConstants singleton].quoteScrPage = 0;
		self->_rankNameKey = item;
        self->_lblFilterDropdown.text = [LanguageManager stringForKey:item];
		if ([item.uppercaseString isEqualToString:[LanguageManager stringForKey:@"Gainers%"].uppercaseString] || [item.uppercaseString isEqualToString:[LanguageManager stringForKey:@"Losers%"].uppercaseString]) {
			//Do something if need...
			//[SettingManager shareInstance].isVisibleChangePer = YES;
			//[self setVisibleChangePer:[SettingManager shareInstance].isVisibleChangePer];
			//[[SettingManager shareInstance] save];
		}
		if ([item.uppercaseString isEqualToString:[LanguageManager stringForKey:@"Gainers"].uppercaseString] || [item.uppercaseString isEqualToString:[LanguageManager stringForKey:@"Loser"].uppercaseString]) {
			//Do something if need...
			//[SettingManager shareInstance].isVisibleChangePer = NO;
			//[self setVisibleChangePer:[SettingManager shareInstance].isVisibleChangePer];
			//[[SettingManager shareInstance] save];
		}
		//Load data here...
		[self reloadData];
	}];
}

//For Menupage - Exchange
- (void)setupMenuExchange{
	if ([UserPrefConstants singleton].supportedExchanges.count == 0) {
		return;
	}
	//ExchangeView
	_menuExchange.menuType = TCPageMenuControlType_Bubble;
	_menuExchange.dataSources = [Utils getListExchangeName];
	_curSelectedExchange = [Utils getIndexOfCurrentExchange];
	_menuExchange.currentSelectedItem = _curSelectedExchange;
	//[Utils selectedNewExchange:[UserPrefConstants singleton].supportedExchanges[_curSelectedExchange]];
	[_menuExchange startLoadingPageMenu];
	[_menuExchange didSelectItem:^(NSString *item, NSInteger index) {
		ExchangeData *newEx = [[UserPrefConstants singleton].supportedExchanges objectAtIndex:index];
		[Utils selectedNewExchange:newEx];
		//Post Notification here
		[[NSNotificationCenter defaultCenter] postNotificationName:kDidSelectNewExchangeNotification object:newEx];
	}];
}

//PUll to refhresh
- (void)setupPullToRefresh{
	//Previous Page
	if (!self.pullToRefreshTop) {
		[self.tblContent addPullToRefreshWithActionHandler:^{
			DLog(@"+++ load Previous page");
			if (self->_pullToRefreshTop.hidden) {
				[self->_pullToRefreshTop stopAnimating];
				return ;
			}
			--self.curPageLoading;
			[UserPrefConstants singleton].quoteScrPage = self.curPageLoading;
			[self startLoadingQuoteData];
			//+++ no show loading indicator
			[self.contentView dismissPrivateHUD];
		} position:SVPullToRefreshPositionTop];
		
		self.pullToRefreshTop = [self.tblContent pullToRefreshViewAtPosition:SVPullToRefreshPositionTop];
	}
	
	//Next page
	if (!self.pullToRefreshBottom) {
		[self.tblContent addPullToRefreshWithActionHandler:^{
			DLog(@"+++ load next page");
			if (self->_pullToRefreshBottom.hidden) {
				[self->_pullToRefreshBottom stopAnimating];
				return ;
			}
			++self.curPageLoading;
			[UserPrefConstants singleton].quoteScrPage = self.curPageLoading;
			[self startLoadingQuoteData];
			//+++ no show loading indicator
			[self.contentView dismissPrivateHUD];
		} position:SVPullToRefreshPositionBottom];
		//Prepare custom value
		self.pullToRefreshBottom =  [self.tblContent pullToRefreshViewAtPosition:SVPullToRefreshPositionBottom];
	}
	//Check Should show or hide
	[self shouldShowOrHidePullToRefresh];
}
- (void)shouldShowOrHidePullToRefresh{
	_pullToRefreshTop.hidden = (_curPageLoading == 0);
	if ((_quotes.count > 0) && (_quotes.count < kNUMBER_ITEMS_PER_PAGE)) {
		_pullToRefreshBottom.hidden = YES;
	}else if (_quotes.count == 0){
		_pullToRefreshBottom.hidden = YES;
	}else{
		_pullToRefreshBottom.hidden = NO;
	}
	//Custom Title
	[_pullToRefreshTop setTitle:[LanguageManager stringForKey:Pull_down_to_Go_to_Previous_Page]  forState:(SVPullToRefreshStateStopped)];
	NSInteger previosPage = self.curPageLoading - 1;
    NSString *previousPageTitle = [NSString stringWithFormat: [LanguageManager stringForKey:@"Release to Go to Previous Page [%ld - %ld]"], (previosPage * kNUMBER_ITEMS_PER_PAGE + 1), ((previosPage + 1) * kNUMBER_ITEMS_PER_PAGE)];
	
	_pullToRefreshTop.arrowColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	_pullToRefreshTop.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	[_pullToRefreshTop setTitle:previousPageTitle forState:(SVPullToRefreshStateTriggered)];
	[_pullToRefreshTop setTitle:[LanguageManager stringForKey:Loading_Previous_Page______]  forState:(SVPullToRefreshStateLoading)];
	
	//For Next
	NSInteger nextPage = self.curPageLoading + 1;
	[_pullToRefreshBottom setTitle:[LanguageManager stringForKey:Pull_up_to_Go_to_Next_Page]  forState:(SVPullToRefreshStateStopped)];
    NSString *nextPageTitle = [NSString stringWithFormat:[LanguageManager stringForKey:@"Release to Go to Next Page [%ld - %ld]"], (nextPage * kNUMBER_ITEMS_PER_PAGE + 1), ((nextPage + 1) * kNUMBER_ITEMS_PER_PAGE)];
//    NSString *nextPageTitle = [LanguageManager stringForKey:[NSString stringWithFormat:@"Release to Go to Next Page [%ld - %ld]", (nextPage * kNUMBER_ITEMS_PER_PAGE + 1), ((nextPage + 1) * kNUMBER_ITEMS_PER_PAGE)]];
	_pullToRefreshBottom.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	_pullToRefreshBottom.arrowColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	[_pullToRefreshBottom setTitle:nextPageTitle forState:(SVPullToRefreshStateTriggered)];
	[_pullToRefreshBottom setTitle:[LanguageManager stringForKey:Loading_Next_Page______]  forState:(SVPullToRefreshStateLoading)];
}

//For Pull to refresh
- (void)stopPullToRefresh{
	[_pullToRefreshBottom stopAnimating];
	[_pullToRefreshTop stopAnimating];
	[self shouldShowOrHidePullToRefresh];
}

#pragma mark - Register Notification
- (void)registerNotification{
	//Notification: didUpdate
	[self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
	[self addNotification:kVertxNo_pnWebSocketConnected selector:@selector(socketConnectedNotification:)];
	[self addNotification:@"pnWebSocketDisconnected" selector:@selector(didReceiveErrorSocketNotification:)];
	[self addNotification:@"didReceiveExchangeList" selector:@selector(didReceiveExchangeListNotification:)];
	[self addNotification:@"vertxGetExchangeInfoFinished" selector:@selector(didReceiveExchangeInfo:)];
	[self addNotification:kSwitchValueChangedNotification selector:@selector(shouldSwitchValueChangedNotification:)];
	[self addNotification:kDidSelectNewExchangeNotification selector:@selector(didSelectNewExchangeNotification:)];
}

#pragma mark - LoadData
- (void)loadData{
	[self startLoadingQuoteData];
}

- (void)reloadData{
	_curPageLoading = 0;
	[UserPrefConstants singleton].quoteScrPage = 0;
	[_quotes removeAllObjects];
	[_expandedCells removeAllObjects];
	[_tblContent reloadData];
	[self stopPullToRefresh];
	[self loadData];
}

//In Quote
- (void)startLoadingQuoteData{
	//For Network
	if (![VertxConnectionManager singleton].connected) {
		[TCBubbleMessageView showErrorWindowBubbleMessage:[LanguageManager stringForKey:@"No Vertx Connection"]];
		return;
	}
	if ([[UserPrefConstants singleton].userExchagelist count] == 0) {
		[self.contentView showStatusMessage:Loading_Exchanges______];
		return;
	}
	//Filter Sector
	SectorInfo *mainSector = [UserPrefConstants singleton].selectedSector;
	SectorInfo *subSector = [UserPrefConstants singleton].selectedSubsector;
	if (subSector) {
		[UserPrefConstants singleton].sectorToFilter = subSector;
	}else{
		if ([mainSector.sectorName isEqualToString:[LanguageManager stringForKey:@"All Stocks"]]) {
			[UserPrefConstants singleton].sectorToFilter = nil;
		}else{
			[UserPrefConstants singleton].sectorToFilter = mainSector;
		}
	}
	//Filter by Lot: This selected while logining...
	NSDictionary *marketInfos = [UserPrefConstants singleton].currentExchangeInfo.marketInfos;
	if (marketInfos.count > 0) {
		//Check selected
		if (![UserPrefConstants singleton].selectedMarket) {
			[UserPrefConstants singleton].selectedMarket = marketInfos.allKeys.firstObject;
		}
	}
	//Prepare params
	NSString *property = [self propertyFromRankNameKey:_rankNameKey];
	NSString *direction = [self directionFromRankNameKey:_rankNameKey];
	[self.contentView showPrivateHUD];
	if (_bubbleMsgView) {
		[_bubbleMsgView hide];
	}
	[self.contentView removeStatusViewIfNeeded];
	[self stopPullToRefresh];
	__weak MQuoteViewController *_weakSelf = self;
	NSString *keySearch = @"";
	[[VertxConnectionManager singleton] vertxSortByServer:property Direction:direction exchg:[UserPrefConstants singleton].userCurrentExchange keyword:keySearch completion:^(id result, NSError *error) {
		//Response is Array data now
		//Config
		[self.contentView dismissPrivateHUD];
		[self.contentView removeStatusViewIfNeeded];
		//Data
		if (error) {
			TCStatusInfoView *_statusView = [TCStatusInfoView new];
			[_statusView showStatus:error.localizedDescription inView:self.contentView retryAction:^{
				//+++ Refresh here
				[_weakSelf startLoadingQuoteData];
			}];
			return;
		}
		//Reparse
		NSArray *resultArr = (NSArray *)result;
		if (!resultArr || resultArr.count == 0) {
			TCStatusInfoView *_statusView = [TCStatusInfoView new];
			[_statusView showStatus:[LanguageManager stringForKey:@"No data, Please try again!"] inView:self.contentView retryAction:^{
				//+++ Refresh here
				[_weakSelf startLoadingQuoteData];
			}];
			return;
		}
		[self parseDataFrom:resultArr];
		//No need parse from dict now
		//[self parseDataFromDict:(NSDictionary *)result];
		if (self.contentType == MContentType_Single) {
			[self.tblContent reloadData];
		}else{
			[self.tblContent reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
		}
		[self stopPullToRefresh];
	}];
}
#pragma mark - Configurations
- (void)setContentType:(MContentType)contentType{
	switch (contentType) {
		case MContentType_Single:
			//[self createCustomBar:BarItemType_Right icon:@"type_list_one_icon" selector:@selector(onChangeTypeContentAction:)];
			[_btnSwitchContent setImage:[UIImage imageNamed:@"heatmap_icon"] forState:UIControlStateNormal];
			break;
		case MContentType_Double:
			//[self createCustomBar:BarItemType_Right icon:@"type_list_two_icon" selector:@selector(onChangeTypeContentAction:)];
			break;
		case MContentType_Three:
			//[self createCustomBar:BarItemType_Right icon:@"type_list_three_icon" selector:@selector(onChangeTypeContentAction:)];
			break;
		case MContentType_Heatmap:
			//[self createCustomBar:BarItemType_Right icon:@"heatmap_icon" selector:@selector(onChangeTypeContentAction:)];
			[_btnSwitchContent setImage:[UIImage imageNamed:@"type_list_two_icon"] forState:UIControlStateNormal];
			break;
			
		default:
			break;
	}
	[self shouldShowInstructionView:!(contentType == MContentType_Heatmap)];
	_contentType = contentType;
}
#pragma mark - UTILS
- (void)shouldShowInstructionView:(BOOL)isShow{
	float heightConstrain = isShow ? kDefault_Height_Instruction_View : 0;
	_heightConstrainOfInstructionView.constant = heightConstrain;
	[UIView animateWithDuration:0.3 animations:^{
		[self.view layoutIfNeeded];
	}];
}

//TopRank
- (NSString *)propertyFromRankNameKey:(NSString *)rankNameKey{
	NSArray *_rankedByNameArr = [Utils rankedByNameArr];
	NSArray *_rankedByKeyArr = [Utils rankedByKeyArr];
	if (!_rankedByNameArr || !_rankedByKeyArr) {
		return nil;
	}
	NSInteger indexRank = [_rankedByNameArr indexOfObject:rankNameKey];
	return [self propertyRankAt:indexRank];
//	NSArray *propertyDivideDirection = [[_rankedByKeyArr objectAtIndex:indexRank] componentsSeparatedByString:@"|"];
//	return  [propertyDivideDirection objectAtIndex:0];
}

- (NSString *)directionFromRankNameKey:(NSString *)rankNameKey{
	NSArray *_rankedByNameArr = [Utils rankedByNameArr];
	NSArray *_rankedByKeyArr = [Utils rankedByKeyArr];
	if (!_rankedByNameArr || !_rankedByKeyArr) {
		return nil;
	}
	NSInteger indexRank = [_rankedByNameArr indexOfObject:rankNameKey];
	return [self directionRankAt:indexRank];
//	NSArray *propertyDivideDirection = [[_rankedByKeyArr objectAtIndex:indexRank] componentsSeparatedByString:@"|"];
//	return [propertyDivideDirection objectAtIndex:1];
}

- (NSString *)propertyRankAt:(NSInteger)index{
	NSArray *_rankedByKeyArr = [Utils rankedByKeyArr];
	if  (!_rankedByKeyArr) {
		return nil;
	}
	NSArray *propertyDivideDirection = [[_rankedByKeyArr objectAtIndex:index] componentsSeparatedByString:@"|"];
	return  [propertyDivideDirection objectAtIndex:0];
}

- (NSString *)directionRankAt:(NSInteger)index{
	NSArray *_rankedByKeyArr = [Utils rankedByKeyArr];
	if  (!_rankedByKeyArr) {
		return nil;
	}
	NSArray *propertyDivideDirection = [[_rankedByKeyArr objectAtIndex:index] componentsSeparatedByString:@"|"];
	return [propertyDivideDirection objectAtIndex:1];
}
//Parse data
//Parse data from result dict
- (void)parseDataFrom:(NSArray *)array{
	_quotes = [StockModel arrayOfModelsFromDictionaries:array error:nil];
}
- (void)parseDataFromDict:(NSDictionary *)dict{
	[_quotes removeAllObjects];
	if (!dict || dict.count == 0) {
		return;
	}
	//Parse data here
	for (NSString *key in dict.allKeys) {
		NSError *error;
		NSDictionary *stockDict = dict[key];
		StockModel *stock = [[StockModel alloc] initWithDictionary:stockDict error:&error];
		if (error) {
			DLog(@"+++ Error: Can not parse data to StockModel - %@", error);
			continue;
		}
		[_quotes addObject:stock];
	}
	//Resort data:
	//For gain
	//No sort now
	/*
	if ([_rankNameKey.uppercaseString isEqualToString:[LanguageManager stringForKey:@"Gainers"]]) {
		[_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
			return obj2.fChange.doubleValue > obj1.fChange.doubleValue;
		}];
	}
	if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Gainers%"]]) {
		[_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
			return obj2.changePer.doubleValue > obj1.changePer.doubleValue;
		}];
	}
	//For Lose
	if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Losers"]]) {
		[_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
			return obj2.fChange.doubleValue < obj1.fChange.doubleValue;
		}];
	}
	if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Losers%"]]) {
		[_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
			return obj2.changePer.doubleValue < obj1.changePer.doubleValue;
		}];
	}
    //For Volume
    if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Volume"]]) {
        [_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
            return obj2.volume.doubleValue > obj1.volume.doubleValue;
        }];
    }
    //For Name
    if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Name"]]) {
        [_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
            return [obj1.stockName compare:obj2.stockName options:NSNumericSearch];
        }];
    }
    //For Trade
    if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Trade"]]) {
        [_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
            return obj2.trade.doubleValue > obj1.trade.doubleValue;
        }];
    }
    //For Value
    if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Value"]]) {
        [_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
            return obj2.dValue.doubleValue > obj1.dValue.doubleValue;
        }];
    }
    //For Close
    if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Close"]]) {
        [_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
            return obj2.dClose.doubleValue > obj1.dClose.doubleValue;
        }];
    }
    //For LACP
    if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"LACP"]]) {
        [_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
            return obj2.lastDonePrice.doubleValue > obj1.lastDonePrice.doubleValue;
        }];
    }
    //For Biq.
    if ([_rankNameKey isEqualToString:[LanguageManager stringForKey:@"Bid.Qty"]]) {
        [_quotes sortUsingComparator:^NSComparisonResult(StockModel *obj1, StockModel *obj2) {
            return obj2.iBuyQTY1.doubleValue > obj1.iBuyQTY1.doubleValue;
        }];
    }
	*/
}

//For Ins Change Label
- (void)setVisibleChangePer:(BOOL)isShow{
	//Do nothing now
	/*
	if (isShow) {
		_lblIns_Change.text = [LanguageManager stringForKey:@"Change%"];
	}else{
		_lblIns_Change.text = [LanguageManager stringForKey:@"Change"];
	}
	 */
	//float updatedWidth = [_lblIns_Change.text widthOfString:_lblIns_Change.font];
	//_widthLabelIns_Change_Constrain.constant = updatedWidth + 2; //4 for Alignment
	//[self.view layoutIfNeeded];
}
#pragma mark - Calculate Size for UITableViewCell:
//Just for nested CollectionView
- (CGSize)sizeForStockCellBy:(MContentType)type{
	float width = 100, height = 100;
	NSInteger itemPerRow = type + 1;
	//Just show 3 item on row for Heatmap Type
	if (type == MContentType_Heatmap) {
		itemPerRow = 3;
	}
	width = (self.tblContent.frame.size.width - (2 * kMARGIN_CONTENT_LEFT_RIGHT) - ((itemPerRow - 1) * kSPACING_ITEM)) / itemPerRow;
	height =  0.8 * width;
	return CGSizeMake(width, height);
}

- (float)sumHeightOfCellItems:(NSInteger)countItem{
	if (countItem == 0) {
		return 300; //Default value
	}
	CGSize sizeItem = [self sizeForStockCellBy:_contentType];
	NSInteger itemPerRow = _contentType + 1;
	if (_contentType == MContentType_Heatmap) {
		itemPerRow = 3;
	}
	NSInteger rowItem = (countItem / itemPerRow) + ((countItem % itemPerRow) == 0 ? 0 : 1);
	return (rowItem * sizeItem.height + kSPACING_ITEM * (rowItem + 1) + 2 * kMARGIN_CONTENT_TOP_BOTTOM);
}

#pragma mark - Action

- (IBAction)onSearchAction:(id)sender {
	MSearchVC *searchVC = NEW_VC_FROM_STORYBOARD(kMSearchStoryboardName, [MSearchVC storyboardID]);
	[self nextTo:searchVC animate:YES];
}

- (IBAction)onChangeContentTypeAction:(id)sender {
	switch (_contentType) {
		case MContentType_Single:
			//+++ Fake to show only Single and heatmap type
			self.contentType = MContentType_Heatmap;
			break;
		case MContentType_Double:
			self.contentType = MContentType_Three;
			break;
		case MContentType_Three:
			self.contentType = MContentType_Single;
			break;
		case MContentType_Heatmap:
			self.contentType = MContentType_Single;
			break;
			
		default:
			break;
	}

	[self.tblContent reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
	[self.tblContent reloadData];
	[self shouldShowInstructionView:(_contentType == MContentType_Single)];
}
- (IBAction)onFilterAction:(id)sender {
	MSectorVC *_sectorVC = NEW_VC_FROM_STORYBOARD(kSectorStoryboardName, [MSectorVC storyboardID]);
	_sectorVC.delegate = self;
	[self nextTo:_sectorVC animate:YES];
}
- (IBAction)onShowTopRankDropdown:(id)sender {
	[_topRankDropdown show];
}

- (IBAction)onSwitchChangeValueAction:(id)sender {
	[SettingManager shareInstance].isVisibleChangePer = ![SettingManager shareInstance].isVisibleChangePer;
	[self setVisibleChangePer:[SettingManager shareInstance].isVisibleChangePer];
	[[SettingManager shareInstance] save];
	[self.tblContent reloadData];
}

#pragma mark - MSectorVCDelegate
- (void)didApplyFilter{
	//Reload data
	[self reloadData];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return [_quotes count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	switch (_contentType) {
		case MContentType_Double:{
			//do something later
		}
			break;
		case MContentType_Three:{
			//do something later
		}
			break;
		case MContentType_Heatmap:{
			StockHeatmapCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[StockHeatmapCell reuseIdentifier] forIndexPath:indexPath];
			return cell;
		}
			break;
		default:
			break;
	}
	return nil;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
	if (_contentType == MContentType_Heatmap) {
		//do something here
		StockHeatmapCell *heatmapCell = (StockHeatmapCell *)cell;
		[heatmapCell setupDataFrom:_quotes[indexPath.item]];
	}
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	if (_contentType == MContentType_Double) {
		return;
	}
	
	//Goto detail
	MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
	_detailVC.stock = _quotes[indexPath.row];
	[self nextTo:_detailVC animate:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	return [self sizeForStockCellBy:_contentType];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
	return kSPACING_ITEM;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
	return kSPACING_ITEM;;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (_contentType == MContentType_Single) {
		return [_quotes count];;
	}
	return 1;//Just 1 cell contained UICollectionView here
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (_contentType == MContentType_Single) {
		return [_expandedCells containsObject:indexPath] ? kEXPANDED_CELL_HEIGHT : kNORMAL_CELL_HEIGHT;
	}
	return [self sumHeightOfCellItems:_quotes.count];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//	if (_contentType == MContentType_Single) {
//		StockTypeSingleCell *singleCell = (StockTypeSingleCell *)cell;
//		[singleCell updateData:_quotes[indexPath.row]];
//	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (_contentType == MContentType_Single) {
		StockTypeSingleCell *cell = [tableView dequeueReusableCellWithIdentifier:[StockTypeSingleCell reuseIdentifier]];
		if (!cell) {
			cell = [[StockTypeSingleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
		}
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		[tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
		cell.mainDelegate = self;
		cell.indexPath = indexPath;
		cell.isExpandedCell = [_expandedCells containsObject:indexPath];
		cell.showIndex = NO;
		//Parse data to UI
		[cell updateData:_quotes[indexPath.row]];
		return cell;
	}else{
		static NSString *IDCELL = @"NestedQuoteCell";
		NestedCollectionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDCELL];
		if (!cell) {
			UICollectionViewScrollDirection direction =  UICollectionViewScrollDirectionVertical;
			cell = [[NestedCollectionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDCELL withScrollDirection:direction];
		}
		cell.collectionView.contentInset = UIEdgeInsetsMake(kMARGIN_CONTENT_TOP_BOTTOM, kMARGIN_CONTENT_LEFT_RIGHT, kMARGIN_CONTENT_TOP_BOTTOM, kMARGIN_CONTENT_LEFT_RIGHT);
		cell.collectionView.backgroundColor = [UIColor clearColor];
		[cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
		//Remove separate line
		[tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
		_curCollectionView = cell.collectionView;
		return cell;
	}
	return nil;
}

#pragma mark - StockTypeSingleCellDelegate
- (void)didTapExpandCell:(StockTypeSingleCell *)cell{
	NSIndexPath *curIndexPath = cell.indexPath;
	//Save current Stock
	//[UserPrefConstants singleton].userSelectingThisStock  =  _quotes[curIndexPath.row] ;
	BOOL shouldClose = [_expandedCells containsObject:curIndexPath];
	if (shouldClose) {
		[_expandedCells removeObject:curIndexPath];
	}else{
		[_expandedCells addObject:curIndexPath];
	}
	[_tblContent beginUpdates];
	cell.isExpandedCell = !shouldClose;
	[_tblContent endUpdates];
}

- (void)shouldGotoDetailFromCell:(StockTypeSingleCell *)cell{
	[self.view endEditing:YES];
	
	//StockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [StockInfoVC storyboardID]);
	//_detailVC.stock = _quotes[cell.indexPath.row];
	MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
	_detailVC.stock = _quotes[cell.indexPath.row];
	[self nextTo:_detailVC animate:YES];
}

#pragma mark - GHContextMenuDataSource
- (BOOL)shouldShowMenuAtPoint:(CGPoint)point{
	if (_contentType == MContentType_Single) {
		NSIndexPath* indexPath = [self.tblContent indexPathForRowAtPoint:point];
		StockTypeSingleCell *cell = [self.tblContent cellForRowAtIndexPath:indexPath];
		return cell != nil;
	}
	if (_contentType == MContentType_Heatmap){
		NSIndexPath *indexPath = [self.curCollectionView indexPathForItemAtPoint:point];
		StockHeatmapCell *cell = (StockHeatmapCell *)[self.curCollectionView cellForItemAtIndexPath:indexPath];
		return cell != nil;
	}
	return NO;
}
- (NSInteger) numberOfMenuItems{
	return [[AppConstants contextMenu] count];
}
- (UIImage*)imageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}

- (UIImage*) highlightedImageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_hl_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_hl_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_hl_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_hl_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_hl_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}
#pragma mark - GHContextMenuDelegate
- (void)didSelectItemAtIndex:(NSInteger) selectedIndex forMenuAtPoint:(CGPoint)point{
	NSArray *menus = [AppConstants contextMenu];
	MContextMenu menuType = [menus[selectedIndex] integerValue];
	
	//Select Stock Model
	NSInteger index;
	if (_contentType == MContentType_Single) {
		NSIndexPath *indexPath = [self.tblContent indexPathForRowAtPoint:point];
		index = indexPath.row;
	}else{
		//For heatmap
		NSIndexPath *indexPath = [self.curCollectionView indexPathForItemAtPoint:point];
		index = indexPath.item;
	}
	StockModel *selectedStock = _quotes[index];
	switch (menuType) {
		case MContextMenu_Trading:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Buy:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Sell:{
			//Sell here
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Sell;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Info:{
			//Detail here
			MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
			_detailVC.stock = selectedStock;
			[self nextTo:_detailVC animate:YES];
		}
			break;
		case MContextMenu_Favorite:{
			//Add favorite here
			SelectWatchlistPopupVC *_selectVC = NEW_VC_FROM_NIB([SelectWatchlistPopupVC class], [SelectWatchlistPopupVC nibName]);
			_selectVC.stock = selectedStock;
			_selectVC.delegate = self;
			[self showPopupWithContent:_selectVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.4 * SCREEN_HEIGHT_PORTRAIT) completion:^(MZFormSheetController *formSheetController) {
				
			}];
		}
			break;
		default:
			break;
	}
	DLog(@"Did select Menu Item: %@", @(selectedIndex));
}


#pragma mark - SelectWatchlistPopupVCDelegate
- (void)didSelectStock:(StockModel *)stock willAddToWatchlist:(WatchlistModel*)watchlist{
    NSInteger watchlistID=watchlist.FavID;
    NSString* watchlistName=watchlist.Name;
	//Call api to add watchlist now
	[[ATPAuthenticate singleton] addStock:stock.stockCode toWatchlist:(int)watchlistID completion:^(BOOL success, id result, NSError *error) {
		if (success) {
			NSString *someDuplicate = [result objectForKey:@"DuplicateStocks"];
			if ([someDuplicate length]>0) {
				[[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlistName}]
											 inController:self withTitle:[LanguageManager stringForKey:Duplicate_Stock]];
				[self showCustomAlertWithMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":watchlistName}]];
			}else{
				//Success
				[self postNotification:kDidFinishAddStockToWatchlistNotification object:nil];
				[TCBubbleMessageView showWindowBubbleMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]];
			}
		}else{
			if (error) {
				[TCBubbleMessageView showErrorWindowBubbleMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]];
			}
		}
	}];
}


#pragma mark - Notifications
- (void)didUpdate:(NSNotification *)noti{
	dispatch_async(dispatch_get_main_queue(), ^{
		NSString *stockCode = noti.object;
		StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
		if (!changedStock || changedStock == nil) {
			return; //do nothing
		}
		//For Quotes
		NSInteger indexOfQuoteStock = [Utils indexOfStockByCode:stockCode fromArray:self->_quotes];
		if (self->_quotes.count > 0 && indexOfQuoteStock < self->_quotes.count) {
			if (indexOfQuoteStock != -1) {
				//For TypeSingle cell
				if (self->_contentType == MContentType_Single) {
					NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfQuoteStock inSection:0];
					StockModel *oldStock = [self->_quotes[indexOfQuoteStock] copy];
					[self->_quotes replaceObjectAtIndex:indexOfQuoteStock withObject:changedStock];
					StockTypeSingleCell *cell = [self->_tblContent cellForRowAtIndexPath:indexPath];
					if (cell) {
						[cell.contentView makeAnimateBy:oldStock newStock:changedStock completion:^{
							[cell updateData:changedStock];
						}];
					}
				}
				
				//For Heatmap type
				if (self->_contentType == MContentType_Heatmap) {
					NSIndexPath *indexPath = [NSIndexPath indexPathForItem:indexOfQuoteStock inSection:0];
					[self->_quotes replaceObjectAtIndex:indexOfQuoteStock withObject:changedStock];
					if (self->_curCollectionView) {
						//Just reload cell
						StockHeatmapCell *heatCell = (StockHeatmapCell *)[self->_curCollectionView cellForItemAtIndexPath:indexPath];
						if (heatCell) {
							[heatCell setupDataFrom:changedStock];
						}
						//No need to call reload now
						//[self->_curCollectionView reloadItemsAtIndexPaths:@[indexPath]];
					}
				}
				//End
			}
		}
	});
}
//For exchange List
- (void)didReceiveExchangeListNotification:(NSNotification *)note{
	[self setupMenuExchange];
	if (_quotes.count == 0) {
		[self loadData];
	}
}

//Exchange Info
- (void)didReceiveExchangeInfo:(NSNotification *)noti{
	//Reload PageMenu
	[self setupMenuExchange];
	if (_quotes.count == 0) {
		[self loadData];
	}
}

//Error Notification
//Error Socket
- (void)didReceiveErrorSocketNotification:(NSNotification *)noti{
	[[Utils shareInstance] dismissWindowHUD];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView dismissPrivateHUD];
	//Show message
	[_bubbleMsgView showBubbleMessage:[LanguageManager stringForKey:No_Connection] inView:self.view withType:BubbelMessageType_Error autoHidden:NO];
}

- (void)socketConnectedNotification:(NSNotification *)noti{
	//Update Bubble Message
	_bubbleMsgView.message = [LanguageManager stringForKey:Connected];
	_bubbleMsgView.bubbleType = BubbelMessageType_Success;
	//hide if needed
	[TCBubbleMessageView hide];
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
		[self->_bubbleMsgView hide];
	});
	//Remove some indicators if needed
	//[[Utils shareInstance] dismissWindowHUD];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView dismissPrivateHUD];
	if (_quotes.count == 0) {
		[self loadData];
	}
}

//DidSelectNewExchange
- (void)didSelectNewExchangeNotification:(NSNotification *)noti{
	//Do something here
	if (_menuExchange.dataSources.count > 0) {
		if (_menuExchange.currentSelectedItem != [Utils getIndexOfCurrentExchange]) {
			[self setupMenuExchange];
		}
	}
	[self reloadData];
}

//Switch Value Change or ChangeP
- (void)shouldSwitchValueChangedNotification:(NSNotification *)noti{
	[self.tblContent reloadData];
}
@end
