//
//  AccountDetailVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AccountDetailVC.h"
#import "AccountInfoView.h"
#import "AccountInfoDetailCell.h"
#import "UIViewController+Util.h"
#import "AccountInfoCell.h"
#import "NSString+Util.h"
#import "NSDate+Utilities.h"
#import "AccountInfoDetailFullCell.h"
@interface AccountDetailVC () <UITableViewDelegate, UITableViewDataSource>
{
    NSArray *_items;
    NSArray *_itemAccountInfo;
    CGFloat _sectionHeight;
}
@end

@implementation AccountDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    , @{@"Created On":[UserPrefConstants singleton].date
    _items = @[@{@"Full Name": [UserPrefConstants singleton].senderName}];
//    , @{@"Email":[UserPrefConstants singleton].userEmail}, @{@"Phone Number": [UserPrefConstants singleton].phoneNumber}, @{@"Last Login": [self getDateStringFromServer:[UserPrefConstants singleton].lastLoginTime]}
    
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_ItemViewBg);
    self.lbl_AccountName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_textCell);
    self.lbl_AccountName.text = self.userInfo.display_name;
    
    _itemAccountInfo = [self accountDict];
    [_v_AccountInfo setupDataForCell:_userInfo];
    [self setBackButtonDefault];
    self.customBarTitle = [LanguageManager stringForKey:@"Account Details"];
    [self setupViews];
}
- (NSArray *)accountDict{
    return @[@{@"Buying Power": @"-"}, @{@"Open Balance": @"-"}, @{@"Margin Call": @"-"}, @{@"Total P/L": @"-"}, @{@"I. Margin": @"-"}, @{@"M.Margin": @"-"}, @{@"M.Level": @"-"}, @{@"Cash Balance": @"-"}, @{@"Credit Limit": @"-"}, @{@"CTRL Level": @"-"}, @{@"Margin Class": @"-"}];
}
- (NSString *)getDateStringFromServer:(NSString *)svDate{
    NSDate *date = [NSDate dateFromString:svDate format:@"yyyyMMddHHmmss"];
    return [NSString stringWithFormat:@"%@%@ %@", [date stringWithFormat:@"dd"],[date daySuffixForDate], [date stringWithFormat:@"MMMM yyyy"]];
}
- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)setupViews{
    _sectionHeight= 50;
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    self.tbl_Content.tableFooterView = [UIView new];
    [self.tbl_Content registerNib:[UINib nibWithNibName:[AccountInfoDetailCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[AccountInfoDetailCell reuseIdentifier]];
    [self.tbl_Content registerNib:[UINib nibWithNibName:[AccountInfoDetailFullCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[AccountInfoDetailFullCell reuseIdentifier]];
    
    [self.tbl_Content registerNib:[UINib nibWithNibName:[AccountInfoCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[AccountInfoCell reuseIdentifier]];
    
    self.tbl_Content.estimatedRowHeight = 40;
    
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, _sectionHeight)];
    headerView.backgroundColor = [UIColor clearColor];
    UILabel *labelheader = [[UILabel alloc] initWithFrame:CGRectMake(28, 0, tableView.frame.size.width, _sectionHeight)];
    labelheader.backgroundColor = [UIColor clearColor];
    labelheader.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].accDetail_textCell);
    labelheader.font = AppFont_MainFontBoldWithSize(22);
    labelheader.text = [LanguageManager stringForKey:@"Account Details"];
    [headerView addSubview:labelheader];
    return headerView;
}
#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        AccountInfoDetailFullCell *cell = [tableView dequeueReusableCellWithIdentifier:[AccountInfoDetailFullCell reuseIdentifier] forIndexPath:indexPath];
        NSDictionary *content = _items[indexPath.row];
        cell.lbl_Title.text = [content allKeys].firstObject;
        cell.lbl_Content.text = [content allValues].firstObject;
        return cell;
    }else{
        NSDictionary *dict = [_itemAccountInfo objectAtIndex:indexPath.row*2];
        NSDictionary *dict2;
        if(indexPath.row*2 < _itemAccountInfo.count-1){
            dict2 = [_itemAccountInfo objectAtIndex:(indexPath.row*2)+1];
        }
        AccountInfoDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:[AccountInfoDetailCell reuseIdentifier] forIndexPath:indexPath];
        cell.lbl_Title.text = [dict allKeys].firstObject;
        cell.lbl_Content.text = [dict allValues].firstObject;
        
        if(dict2){
            cell.lbl_Title2.text = [dict2 allKeys].firstObject;
            cell.lbl_Content2.text = [dict2 allValues].firstObject;
        }
        
        return cell;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1){
        long numberOfItems = _itemAccountInfo.count;
        if(numberOfItems%2 == 0){
            return numberOfItems/2;
        }else{
            return (numberOfItems/2) + 1;
        }
    }

    return 1;
}
@end
