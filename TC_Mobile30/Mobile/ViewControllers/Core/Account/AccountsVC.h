//
//  AccountsVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "TCBaseView.h"
NS_ASSUME_NONNULL_BEGIN
typedef void (^callBackAccountSelected) (UserAccountClientData *account);

@interface AccountsVC : BaseVC
@property (weak, nonatomic) IBOutlet UIView *v_Search;

@property (weak, nonatomic) IBOutlet UITextField *tf_Search;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@property (weak, nonatomic) IBOutlet UIButton *btn_icon;
@property (nonatomic, copy) callBackAccountSelected CallBackValue;
@property (assign, nonatomic) BOOL isShowMenu;
@end

NS_ASSUME_NONNULL_END
