//
//  AccountsVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AccountsVC.h"
#import "AccountInfoCell.h"
#import "AccountDetailVC.h"
#import "ATPAuthenticate.h"
#import "UserPrefConstants.h"

@interface AccountsVC () <UITableViewDelegate, UITableViewDataSource, AccountInfoViewDelegate>
{
    NSArray *_accounts;
}
@end

@implementation AccountsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.enableTapDismissKeyboard = YES;
    _accounts = [UserPrefConstants singleton].userAccountlist;
    [self setBackButtonDefault];
    self.customBarTitle = [LanguageManager stringForKey:@"Select Account"];
    self.tf_Search.placeholder = [LanguageManager stringForKey:@"Search"];
    [self setupViews];
    
    if(self.isShowMenu){
        [self createLeftMenu];
    }
}

- (void)backButtonTapped:(id)sender{
    [[ATPAuthenticate singleton] timeoutActivity];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupViews{
    self.v_Search.layer.cornerRadius = 20;
    self.v_Search.layer.borderWidth = 1;
    self.tbl_Content.backgroundColor = self.view.backgroundColor;
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    self.tbl_Content.tableFooterView = [UIView new];
    [self.tbl_Content registerNib:[UINib nibWithNibName:[AccountInfoCell reuseIdentifier] bundle:nil] forCellReuseIdentifier:[AccountInfoCell reuseIdentifier]];
    self.tbl_Content.estimatedRowHeight = 160;
    
    [self.tf_Search addTarget:self
                       action:@selector(textFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  UITableViewAutomaticDimension;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [UserPrefConstants singleton].userSelectedAccount = [_accounts objectAtIndex:indexPath.row];
    
    if (_CallBackValue) {
        _CallBackValue([_accounts objectAtIndex:indexPath.row]);
    }
    [self backButtonTapped:nil];
}
#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AccountInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[AccountInfoCell reuseIdentifier] forIndexPath:indexPath];
    cell.delegate = self;
    cell.flatMode = [UserPrefConstants singleton].userSelectedAccount.isDerivative != 1;
    [cell setupDataForCell:_accounts[indexPath.row]];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _accounts.count;
}


#pragma mark UITextFieldDelegate
- (void)textFieldDidChange:(UITextField *)tf{
    if ([tf.text length]<=0) {
        _accounts = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userAccountlist];
        [self.tbl_Content reloadData];
        
        // then scroll to current selection
        long idx = [_accounts indexOfObject:[UserPrefConstants singleton].userSelectedAccount];
        
        if (idx>0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
            [self.tbl_Content scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    } else{
        NSString *filterAccount = @"client_name CONTAINS[cd] %@";
        NSString *filterName = @"display_name CONTAINS[cd] %@";
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filterAccount, tf.text];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:filterName, tf.text];
        NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2]];
        NSArray *filteredArr  = [[UserPrefConstants singleton].userAccountlist filteredArrayUsingPredicate:predicate];
        _accounts = [[NSMutableArray alloc] initWithArray:filteredArr];
        [_tbl_Content reloadData];
    }
}

#pragma mark AccountInfoViewDelegate
- (void)didSelectButton:(UITableViewCell *)cell{
    NSIndexPath *indexPath = [self.tbl_Content indexPathForCell:cell];
    AccountDetailVC *_accountDetailVC = NEW_VC_FROM_NIB([AccountDetailVC class], [AccountDetailVC nibName]);
    _accountDetailVC.userInfo = _accounts[indexPath.row];
    [self nextTo:_accountDetailVC animate:YES];
}

- (void)updateTheme:(NSNotification *)noti{
    [super updateTheme:noti];
    
    [self.btn_icon setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_tint)];
    self.tf_Search.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_tint);
    
    self.v_Search.layer.borderColor = [self.tf_Search.textColor CGColor];
    if ([self.tf_Search respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        self.tf_Search.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.tf_Search.placeholder attributes:@{NSForegroundColorAttributeName: TC_COLOR_FROM_HEX([ThemeManager shareInstance].acc_searchPlaceHolder)}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
}
@end
