//
//  AccountDetailVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
#import "AroundShadowView.h"
@class AccountInfoView;
@class UserAccountClientData;
NS_ASSUME_NONNULL_BEGIN

@interface AccountDetailVC : BaseVC
@property (weak, nonatomic) IBOutlet AccountInfoView *v_AccountInfo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_AccountName;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;

@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@property (strong, nonatomic) UserAccountClientData *userInfo;
@end

NS_ASSUME_NONNULL_END
