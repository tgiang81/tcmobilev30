//
//  MFundamentalsVC.h
//  TC_Mobile30
//
//  Created by Kaka on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
@class StockModel;
@interface MFundamentalsVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

//Passing data
@property (strong, nonatomic) NSString *fundURL;
@property (strong, nonatomic) NSString *fundType;
@end
