//
//  MFundamentalsVC.m
//  TC_Mobile30
//
//  Created by Kaka on 9/6/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MFundamentalsVC.h"
#import "StockModel.h"
#import "LanguageKey.h"

@interface MFundamentalsVC ()<UIWebViewDelegate>{
	DGActivityIndicatorView *_activity;
}
@end

@implementation MFundamentalsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setBackButtonDefault];
	[self setupLanguage];
	
	_webView.delegate = self;
	[self updateTheme:nil];
}
- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:@"FUNDAMENTALS"];
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
}
#pragma mark - LoadData
- (void)loadData{
	self.lblType.text = _fundType;
	NSURL *url = [NSURL URLWithString:[self urlEscapeString:_fundURL]];
	NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url];
	[_webView loadRequest:req];
}

#pragma mark - UTILS
- (NSString*)urlEscapeString:(NSString *)url{
	NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
	NSString * encodedString = [url stringByAddingPercentEncodingWithAllowedCharacters:set];
	//NSString *encodedString  = [url stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];
	return encodedString;
}
//+++ Show/Hide Activity
- (void)showActivity{
	if(!_activity){
		_activity = [[Utils shareInstance]createPrivateActivity];
	}
	[[Utils shareInstance] showActivity:_activity inView:self.view];
}

- (void)hideActivity{
	[[Utils shareInstance] dismissActivity:_activity];
}

#pragma mark -
#pragma mark UIWebView Delegate
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	DLog(@"FundamentalCapitalIQViewController - didFailLoadWithError:%@", error);
	[self hideActivity];
	[self showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:@"Unable to load the page, Please try again later."] withOKAction:^{
		[self popView:YES];
	}];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	if ([[[request URL] absoluteString] isEqualToString: @"about:blank"]) {
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	}
	/*
	 if (navigationType == UIWebViewNavigationTypeLinkClicked) {
	 [[UIApplication sharedApplication] openURL:[request URL]];
	 return NO;
	 }
	 */
	return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[self hideActivity];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	[self showActivity];
}
@end
