//
//  M_IndicesVC.m
//  TC_Mobile30
//
//  Created by Kaka on 5/14/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "M_IndicesVC.h"
#import "TCPageMenuControl.h"
#import "UIView+ShadowBorder.h"
#import "StockTypeSingleCell.h"
#import "MStockInfoVC.h"
#import "VertxConnectionManager.h"
#import "UIView+TCUtil.h"
#import "QCData.h"
#import "IndicesModel.h"
#import "TCDropDown.h"
#import "NSNumber+Formatter.h"
#import "LanguageKey.h"
#import "TCChartView.h"

@interface M_IndicesVC ()<UITableViewDataSource, UITableViewDelegate, StockTypeSingleCellDelegate>{
	NSMutableArray *_listStocks;
	NSMutableArray *_expandedCells;
	
	//Value
	float _df_Card_Stock_Hight;
}
//OUTLETS
@property (weak, nonatomic) IBOutlet TCPageMenuControl *exchangePageMenu;

@property (weak, nonatomic) IBOutlet UIView *mainContent;

//Info View
@property (weak, nonatomic) IBOutlet UIView *cardInfo;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueChangedLabel;
@property (weak, nonatomic) IBOutlet UIButton *sectorDropdownButton;
@property (weak, nonatomic) IBOutlet UILabel *sectorLabel;


//ChartView
@property (weak, nonatomic) IBOutlet UIView *cardChart;
@property (weak, nonatomic) IBOutlet TCChartView *chartView;


//SubInfos
@property (weak, nonatomic) IBOutlet UIView *cardSubInfo;
@property (weak, nonatomic) IBOutlet UILabel *ins_volumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ins_ValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *ins_ScoreOfTrd;
@property (weak, nonatomic) IBOutlet UILabel *ins_Up;
@property (weak, nonatomic) IBOutlet UILabel *ins_UnTrd;
@property (weak, nonatomic) IBOutlet UILabel *ins_Down;
@property (weak, nonatomic) IBOutlet UILabel *ins_InChg;

@property (weak, nonatomic) IBOutlet UILabel *volumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreOfTrdLabel;
@property (weak, nonatomic) IBOutlet UILabel *upLabel;
@property (weak, nonatomic) IBOutlet UILabel *unTrdLabel;
@property (weak, nonatomic) IBOutlet UILabel *downLabel;
@property (weak, nonatomic) IBOutlet UILabel *InChgLabel;

//StockView
@property (weak, nonatomic) IBOutlet UIView *cardStock;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardStock_ConstrainHeight;


//Properties
@property (assign, nonatomic) BOOL startLoadingIndices;
@property (strong, nonatomic) NSArray *listIndices;
@property (strong, nonatomic) NSArray *sectors;
@property (strong, nonatomic) IndicesModel *curSelectedIndices;

//Controls
@property (strong, nonatomic) TCDropDown *sectorDropdown;
@end

@implementation M_IndicesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//Initial values
	_expandedCells = @[].mutableCopy;
	_listStocks = @[].mutableCopy;
	_df_Card_Stock_Hight = 6 * kNORMAL_CELL_HEIGHT;
	[self registerNotifications];
	[self createUI];
	[self loadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	//Bar item
	[self createLeftMenu];
	//Shadow border
	[self.cardInfo makeBorderShadow];
	[self.cardChart makeBorderShadow];
	[self.cardSubInfo makeBorderShadow];
	[self.cardStock makeBorderShadow];
	//TableView
	//TableView
	[self.tblContent registerNib:[UINib nibWithNibName:[StockTypeSingleCell nibName] bundle:nil] forCellReuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	self.tblContent.delegate = self;
	self.tblContent.dataSource = self;
	self.tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:@"Indices"];
}

- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	self.view.backgroundColor = TC_COLOR_FROM_HEX(@"F4F4F4");
}

#pragma mark - LoadData
- (void)loadData{
	[self loadExchangesMenu];
	[self getIndices];
}

//Page menu Exchange
- (void)loadExchangesMenu{
	if ([UserPrefConstants singleton].supportedExchanges.count == 0) {
		return;
	}
	//ExchangeView
	_exchangePageMenu.menuType = TCPageMenuControlType_Bubble;
	_exchangePageMenu.dataSources = [Utils getListExchangeName];
	_exchangePageMenu.bgColor = TC_COLOR_FROM_HEX(@"F4F4F4");
	_exchangePageMenu.currentSelectedItem = [Utils getIndexOfCurrentExchange];
	[_exchangePageMenu startLoadingPageMenu];
	[_exchangePageMenu didSelectItem:^(NSString *item, NSInteger index) {
		ExchangeData *newEx = [[UserPrefConstants singleton].supportedExchanges objectAtIndex:index];
		[Utils selectedNewExchange:newEx];
		//Post Notification here
		[[NSNotificationCenter defaultCenter] postNotificationName:kDidSelectNewExchangeNotification object:newEx];
	}];
}

//======*** Get Indices first ******
- (void)getIndices{
	//For Network
	if (![VertxConnectionManager singleton].connected) {
		//Do nothing...
		return;
	}
	if ([[UserPrefConstants singleton].userExchagelist count] == 0) {
		//[self.contentView showStatusMessage:@"Loading Exchanges..."];
		return;
	}
	
	//Prepare data
	//[self.contentView showPrivateHUD];
	[self.mainContent removeStatusViewIfNeeded];
	_startLoadingIndices = YES;
	ExchangeData *curSelectedEx = [UserPrefConstants singleton].currentExchangeData;
	//Call api
	[[VertxConnectionManager singleton] getIndicesFromExchange:curSelectedEx.feed_exchg_code completion:^(id result, NSError *error) {
		//[self.contentView dismissPrivateHUD];
		if (error) {
			[self.mainContent showStatusViewWithMessage:error.localizedDescription retryAction:^{
				//Reload indices here
				[self getIndices];
			}];
			return ;
		}
		//Parse data here
		// SORT IT
		if ([[QCData singleton].qcIndicesDict count] == 0) {
			[self.mainContent showStatusViewWithMessage:[LanguageManager stringForKey:@"The Market is not available. Please try again later!"] retryAction:^{
				//Reload indices here
				[self getIndices];
			}];
			return;
		}
		NSMutableArray *independentArray = [[NSMutableArray alloc] init];
		for(id key in [[QCData singleton] qcIndicesDict]) {
			NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:key]];
			[independentArray addObject:eachDict];
		}
		NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
		NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
		NSMutableArray *indicesArr = [IndicesModel arrayOfModelsFromDictionaries:sortedArray error:nil];
		if (!self.startLoadingIndices) {
			return;
		}
		dispatch_async(dispatch_get_main_queue(), ^{
			//[self.contentView removeStatusViewIfNeeded];
			if (indicesArr && indicesArr.count > 0) {
				self.startLoadingIndices = NO;
				self.listIndices = [indicesArr copy];
				//Start parsing to get Sector and loading stocks then
				//Get Default Indices
				self.curSelectedIndices = [self getDefaultIndicesFrom:self.listIndices inExchange:[UserPrefConstants singleton].currentExchangeInfo];
				self.sectors = [self listSectorNameFromIndices:self.listIndices];
				[self loadIndicesContent];
			}
		});
	}];
}
//Reload Data
- (void)reloadData{
	[_expandedCells removeAllObjects];
	[_listStocks removeAllObjects];
	_sectors = @[].copy;
	_listIndices = @[];
	[self.tblContent reloadData];
	[self relayoutCardStock];
	[self getIndices];
}

#pragma mark - LoadContent
- (void)loadIndicesContent{
	[self parseInfoIndice:self.curSelectedIndices];
	NSInteger curIndicesIndex = [self.listIndices indexOfObject:self.curSelectedIndices];
	NSString *sectorName = self.sectors[curIndicesIndex];
	_sectorLabel.text = sectorName;
	SectorInfo *sector = [Utils getSectorInfoObjFor:sectorName];
	[self getStocksBySector:sector];
}

- (void)parseInfoIndice:(IndicesModel *)model{
	_priceLabel.text = [model.curPrice toCurrencyNumber];
	float close = model.closePrice.floatValue;
	float change  = model.curPrice.floatValue - close;
	float changeP;
	if (close <= 0) {
		changeP = 0;
	}else{
		changeP = change / close * 100;
	}
	//Parse to UI
	_valueChangedLabel.text = [NSString stringWithFormat:@"%@ (%@)", [[NSNumber numberWithFloat:change] toChangedStringWithSign], [[NSNumber numberWithFloat:changeP] toPercentWithSign]];
	
	_valueChangedLabel.textColor = [[NSNumber numberWithFloat:change] colorByCompareToPrice:0];
	_priceLabel.textColor = _valueChangedLabel.textColor;
	
	//Subcontent
	_volumeLabel.text = [model.vol abbreviateNumber];
	_valueLabel.text = [model.val abbreviateNumber];
	_upLabel.text = [model.up abbreviateNumber];
	_downLabel.text = [model.down abbreviateNumber];
	_scoreOfTrdLabel.text = [model.scoreOfTrd toCurrencyNumber];
	_unTrdLabel.text = [model.unTrd abbreviateNumber];
	_InChgLabel.text = [model.inChg abbreviateNumber];
	
	//Chart
	//_lblLastUpdate.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Last Updated"], [[NSDate date] stringWithFormat:kFormat_Update_Time]];
	_chartView.stCode = model.stockCode;
	[_chartView startLoadDefaultChart];
}

- (void)getStocksBySector:(SectorInfo *)sector{
	//Set Sector
	if (sector && [sector.sectorName isEqualToString:[LanguageManager stringForKey:All_Stocks]]) {
		[UserPrefConstants singleton].sectorToFilter = nil;
	}else{
		[UserPrefConstants singleton].sectorToFilter = sector;
	}
	//2: Set rank
	NSString *property = @"101";
	NSString *direction = @"DESC";
	
	ExchangeData *curSelectedEx = [UserPrefConstants singleton].currentExchangeData;
	[[VertxConnectionManager singleton] vertxSortByServer:property Direction:direction exchg:curSelectedEx.feed_exchg_code completion:^(id result, NSError *error) {
		NSArray *quotes = [StockModel arrayOfModelsFromDictionaries:(NSArray *)result error:nil];
		dispatch_async(dispatch_get_main_queue(), ^{
			self->_listStocks = [quotes mutableCopy];
			[self.tblContent reloadData];
			[self relayoutCardStock];
		});
	}];
}
#pragma mark - Register Notification
- (void)registerNotifications{
	//[self addNotification:@"didReceiveExchangeList" selector:@selector(didReceiveExchangeListNotification:)];
	//[self addNotification:@"pnWebSocketDisconnected" selector:@selector(didReceiveErrorSocketNotification:)];
	//[self addNotification:kVertxNo_pnWebSocketConnected selector:@selector(socketConnectedNotification:)];
	[self addNotification:kDidSelectNewExchangeNotification selector:@selector(didSelectNewExchangeNotification:)];
	[self addNotification:kSwitchValueChangedNotification selector:@selector(shouldSwitchValueChangedNotification:)];
}

#pragma mark - ACTIONS

- (IBAction)onShowSectorDropdownAction:(UIButton *)sender {
	if (!_sectorDropdown) {
		_sectorDropdown = [[TCDropDown alloc] init];
	}
	_sectorDropdown.sourceView = sender;
	_sectorDropdown.isSearchable = NO;
	_sectorDropdown.dataSources = self.sectors;
	__weak M_IndicesVC *weakSelf = self;
	[_sectorDropdown didSelectItem:^(NSString *item, NSInteger index) {
		weakSelf.curSelectedIndices = weakSelf.listIndices[index];
		[self->_expandedCells removeAllObjects];
		[weakSelf relayoutCardStock];
		[self->_listStocks removeAllObjects];
		[weakSelf.tblContent reloadData];
		[weakSelf loadIndicesContent];
	}];
	[self.sectorDropdown show];
}
#pragma mark - UTILS VC
- (void)relayoutCardStock{
	self.cardStock_ConstrainHeight.constant = [self calculateHighOfStocks];
	[self.view layoutIfNeeded];
}
////++++ Index of main indices - at first to show
//- (NSInteger)getDefaultIndexOfIndices:(ExchangeInfo *)exInfo fromIndices:(NSArray *)indices{
//	NSInteger _index = 0;
//	NSString *mainIndicesCode = exInfo.indicesCode;
//	//Check to get index of Main Indices
//	for (int i = 0; i < indices.count; i ++) {
//		IndicesModel *model = indices[i];
//		if ([model.stockCode isEqualToString:mainIndicesCode]) {
//			_index = i;
//			break;
//		}
//	}
//	return _index;
//}
- (float)calculateHighOfStocks{
	if (_listStocks.count == 0) {
		return _df_Card_Stock_Hight + 50;
	}
	float sum_height = _listStocks.count * kNORMAL_CELL_HEIGHT + _expandedCells.count * (kEXPANDED_CELL_HEIGHT - kNORMAL_CELL_HEIGHT) + 50;
	return sum_height;
}
- (IndicesModel *)getDefaultIndicesFrom:(NSArray *)listIndices inExchange:(ExchangeInfo *)exInfo{
	if (!listIndices || listIndices.count == 0) {
		return nil;
	}
	NSString *mainIndicesCode = exInfo.indicesCode;
	//Check to get index of Main Indices
	for (IndicesModel *model in listIndices) {
		if ([model.stockCode isEqualToString:mainIndicesCode]) {
			return model;
		}
	}
	return listIndices[0];
}
- (NSArray *)listSectorNameFromIndices:(NSArray *)indices{
	NSMutableArray *names = @[].mutableCopy;
	for (IndicesModel *model in indices) {
		if (!model.name || model.name.length == 0) {
			model.name = @"-"; //+++Check for null data
		}
		[names addObject:model.name];
	}
	return [names copy];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_listStocks count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return [_expandedCells containsObject:indexPath] ? kEXPANDED_CELL_HEIGHT : kNORMAL_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	StockTypeSingleCell *cell = [tableView dequeueReusableCellWithIdentifier:[StockTypeSingleCell reuseIdentifier]];
	if (!cell) {
		cell = [[StockTypeSingleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.mainDelegate = self;
	cell.indexPath = indexPath;
	cell.isExpandedCell = [_expandedCells containsObject:indexPath];
	cell.showIndex = YES;
	
	//Parse data to UI
	[cell updateData:_listStocks[indexPath.row]];
	[tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
	return cell;
}

#pragma mark - StockTypeSingleCellDelegate
- (void)didTapExpandCell:(StockTypeSingleCell *)cell{
	NSIndexPath *curIndexPath = cell.indexPath;
	BOOL shouldClose = [_expandedCells containsObject:curIndexPath];
	if (shouldClose) {
		[_expandedCells removeObject:curIndexPath];
	}else{
		[_expandedCells addObject:curIndexPath];
	}
	[_tblContent beginUpdates];
	cell.isExpandedCell = !shouldClose;
	[_tblContent endUpdates];
	[self relayoutCardStock];
}

//Goto detail
- (void)shouldGotoDetailFromCell:(StockTypeSingleCell *)cell{
	[self.view endEditing:YES];
	MStockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [MStockInfoVC storyboardID]);
	_detailVC.stock = _listStocks[cell.indexPath.row];
	[self nextTo:_detailVC animate:YES];
}

#pragma mark - Notification
//For exchange List
- (void)didReceiveExchangeListNotification:(NSNotification *)note{
	[self loadExchangesMenu];
//	if (_listMarkets.count == 0) {
//		[self loadData];
//	}
}
////Error Socket
//- (void)didReceiveErrorSocketNotification:(NSNotification *)noti{
//	[[Utils shareInstance] dismissWindowHUD];
//	[self.contentView removeStatusViewIfNeeded];
//	[self.contentView dismissPrivateHUD];
//	//Show message
//	[_bubbleMsgView showBubbleMessage:@"No Connection" inView:self.view withType:BubbelMessageType_Error autoHidden:NO];
//}
//- (void)socketConnectedNotification:(NSNotification *)noti{
//	//Update Bubble Message
//	_bubbleMsgView.message = @"Connected";
//	_bubbleMsgView.bubbleType = BubbelMessageType_Success;
//	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//		[self->_bubbleMsgView hide];
//	});
//
//	//Dimiss other indicator if needed
//	[[Utils shareInstance] dismissWindowHUD];
//	[self.contentView removeStatusViewIfNeeded];
//	[self.contentView dismissPrivateHUD];
//	if (_listMarkets.count == 0) {
//		[self loadData];
//	}
//}

//DidSelectNewExchange
- (void)didSelectNewExchangeNotification:(NSNotification *)noti{
	//Do something here
	if (_exchangePageMenu.dataSources.count > 0) {
		if (_exchangePageMenu.currentSelectedItem != [Utils getIndexOfCurrentExchange]) {
			[self loadExchangesMenu];
		}
	}
	[self reloadData];
}

//Switch Value Change or ChangeP
- (void)shouldSwitchValueChangedNotification:(NSNotification *)noti{
	[self.tblContent reloadData];
}
@end
