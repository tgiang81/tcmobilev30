//
//  MIndicesVC.h
//  TCiPad
//
//  Created by Kaka on 7/31/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class TCPageMenuControl;
@class TCMenuDropDown;
@class TCChartView;
@class StockModel;

@protocol MIndicesVCDelegate<NSObject>
- (void)didShowError:(NSString *)content autoHide:(BOOL)autoHide type:(NSInteger)type;
- (void)hideError;
@end
@interface MIndicesVC : BaseVC{
	
}
@property(assign, nonatomic) BOOL isDashBoardMode;
@property(weak, nonatomic) id<MIndicesVCDelegate> delegate;
- (StockModel *)currentStock;
@end
