//
//  MIndicesVC.m
//  TCiPad
//
//  Created by Kaka on 7/31/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MIndicesVC.h"
#import "TCPageMenuControl.h"
#import "BaseCollectionView.h"
#import "WatchlistSectionView.h"
#import "IndicesSectionView.h"
#import "VertxConnectionManager.h"
#import "TCChartView.h"
#import "TCAnimation.h"
#import "UIView+ShadowBorder.h"
#import "ZoomingViewController.h"

#import "NSDate+Utilities.h"
#import "NSNumber+Formatter.h"
//For New Type Cell
#import "StockTypeSingleCell.h"
#import "QCData.h"
#import "IndicesModel.h"

#import "TCStatusInfoView.h"
#import "NestedCollectionViewCell.h"
#import "TCMenuDropDown.h"
#import "StockInfoVC.h"
#import "TCInactiveButton.h"

#import "TCDropDown.h"
#import "UIView+Animation.h"
#import "UIImageView+TCUtil.h"
#import "NSString+SizeOfString.h"

#import "DetailBrokerModel.h"
#import "TCBubbleMessageView.h"
#import "LanguageKey.h"

#define kWIDTH_INDICES_CELL 		 130
#define kHEIGH_INDICES_CELL			 90
#define kTAG_INDICES_COLLECTIONVIEW		1111
#define kTAG_WATCHLIST_COLLECTIONVIEW	2222


//#define kNORMAL_CELL_HEIGHT		50
//#define kEXPANDED_CELL_HEIGHT	250

#define kFormat_Update_Time		@"MMM d, h:mm a"

@interface MIndicesVC ()<UITableViewDelegate, UITableViewDataSource, TCControlViewDelegate, WatchlistSectionViewDelegate, TCChartViewDelegate, StockTypeSingleCellDelegate>{
	SectorInfo *_currentSelectedSector;
	
	NSMutableArray *_listStocks;
	NSArray *_indices;
	NSArray *_listExchanges;
	//Check load new Indices
	BOOL _isLoadNewIndices;
	
	//Cell
	NSMutableSet *_expandedCells;
	//For Zooming animation
	ZoomingViewController *zoomingViewController;
	TCDropDown *_sectorDropdown;
	
	TCStatusInfoView *_socketStatusView;
    IndicesModel *currentStock;
	
	//Bubble Message View
	TCBubbleMessageView *_bubbleMsgView;
}
//Configs
@property (strong, nonatomic) ExchangeData *indiceEx;

//OUTLETS
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ExchangeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_SectorLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_SectorDropDownLeft;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet TCPageMenuControl *pageMenuExchange;
@property (weak, nonatomic) IBOutlet UIView *viewDropdown;
@property (weak, nonatomic) IBOutlet UILabel *lblSector;

@property (weak, nonatomic) IBOutlet UILabel *lblLastPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblValueChanged;
@property (weak, nonatomic) IBOutlet UILabel *lblLastUpdate;
@property (weak, nonatomic) IBOutlet UILabel *lblStockSectors;

@property (weak, nonatomic) IBOutlet TCChartView *chartView;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UIView *indicesSelectView;
@property (weak, nonatomic) IBOutlet UIView *topContentView;

@property (weak, nonatomic) IBOutlet UIImageView *imgArrowDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgSwitch;

//InstructionView
@property (weak, nonatomic) IBOutlet UILabel *lblInsNameOrCode;


@property (weak, nonatomic) IBOutlet UILabel *lblIns_Last;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Change;

@property (weak, nonatomic) IBOutlet UIView *v_Header;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_TitleStockHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_ViewHeaderHeight;
@property (weak, nonatomic) IBOutlet UILabel *lbl_IndicesTitle;
//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLabelIns_Change_Constrain;

@end

@implementation MIndicesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//init
	_listStocks = @[].mutableCopy;
	_expandedCells = [NSMutableSet new];
	_listExchanges = @[].copy;
	_bubbleMsgView = [TCBubbleMessageView new];
	[self registerNotification];
	[self createUI];
	[self loadData];
}
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	//Instruction +++ Show this because change many where
	[self setVisibleChangePer:[SettingManager shareInstance].isVisibleChangePer];
	[_tblContent reloadData];
}

- (void)viewDidLayoutSubviews{
	[super viewDidLayoutSubviews];
	//For animate chart
    [self setZoomingView];
	if (![VertxConnectionManager singleton].connected) {
        if(self.isDashBoardMode){
            if([self.delegate respondsToSelector:@selector((didShowError:autoHide:type:))]){
                [self.delegate didShowError:[LanguageManager stringForKey:No_Connection] autoHide:NO type:BubbelMessageType_Error];
            }
        }else{
            [_bubbleMsgView showBubbleMessage:[LanguageManager stringForKey:No_Connection] inView:self.view withType:BubbelMessageType_Error autoHidden:NO];
        }
		
		return;
	}
}
- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	//Update Selecting Exchange
	if (self.indiceEx) {
		ExchangeData *curSelectedEx = [UserPrefConstants singleton].currentExchangeData;
		if (![self.indiceEx.feed_exchg_code isEqualToString:curSelectedEx.feed_exchg_code]) {
			[Utils selectedNewExchange:self.indiceEx];
			_isLoadNewIndices = YES;
			[self performSelector:@selector(reloadData) withObject:nil afterDelay:1.0];
		}
	}
}

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	//Navigation Bar
	[self createLeftMenu];
	[self setupLanguage];
	_tblContent.backgroundColor = [UIColor clearColor];
	_tblContent.showsVerticalScrollIndicator = NO;
	[_tblContent registerNib:[UINib nibWithNibName:[StockTypeSingleCell nibName] bundle:nil] forCellReuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	_tblContent.delegate = self;
	_tblContent.dataSource = self;
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	//For Exchange
    if(_isDashBoardMode == YES){
        self.cst_SectorDropDownLeft.constant = -8;
        self.cst_ExchangeHeight.constant = 0;
        self.cst_SectorLeft.constant = 0;
        _lblStockSectors.text = @"";
        _cst_TitleStockHeight.constant = 0;
        _cst_ViewHeaderHeight.constant = 0;
        _tblContent.scrollEnabled = NO;
    }
	[self setupExchangeControl];
	[self prepareSectorDropdown];
	//Chart View
    if(self.isDashBoardMode == NO){
        _chartView.layer.borderColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subBgColor).CGColor;
        _chartView.layer.borderWidth = 1.0;
		[_chartView makeBorderShadow];
	}
	[self updateTheme:nil];
}

- (void)prepareSectorDropdown{
	_sectorDropdown = [[TCDropDown alloc] init];
	_sectorDropdown.sourceView = _viewDropdown;
	_sectorDropdown.isSearchable = NO;
	//_sectorDropdown.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
	//_sectorDropdown.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	[_sectorDropdown didSelectItem:^(NSString *item, NSInteger index) {
		self->_lblSector.text = item;
		self->_currentSelectedSector = [Utils getSectorInfoObjFor:item];
		[self->_expandedCells removeAllObjects];
		//reload data
		[self loadStockBySector:self->_currentSelectedSector];
		//Info indices
		IndicesModel *indice = self->_indices[index];
		[self loadInfoIndices:indice];
	}];
}
- (void)setupExchangeData{
    _listExchanges = [self getIndicesExchange];//[[UserPrefConstants singleton].userExchagelist copy];//[self getListExchangeExcludeDerivative];
    self.indiceEx = _listExchanges.firstObject;
}
- (void)setupExchangeControl{
	if ([UserPrefConstants singleton].userExchagelist.count == 0) {
		return;
	}
	//ExchangeView
    [self setupExchangeData];
	if (_listExchanges.count >= 4) {
		_pageMenuExchange.itemWidth =  SCREEN_WIDTH_PORTRAIT * 0.25;
	}else{
		_pageMenuExchange.itemWidth =  SCREEN_WIDTH_PORTRAIT / _listExchanges.count;
	}
	_pageMenuExchange.delegate = self;
	_pageMenuExchange.dataSources = [Utils getNamesOfExchange:_listExchanges];
	[_pageMenuExchange startLoadingPageMenu];
}
- (void)setZoomingView{
	zoomingViewController = [[ZoomingViewController alloc] init];
	zoomingViewController.view = self.chartView;
    if(self.isDashBoardMode){
        zoomingViewController.dismissDoubleTapToViewFull = YES;
    }
    
	zoomingViewController.view.frame = _chartView.frame;
}
- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:_Indices];
	_lblStockSectors.text = [LanguageManager stringForKey:Stocks];
    _lblInsNameOrCode.text=[LanguageManager stringForKey:_Name];
    _lblIns_Last.text=[LanguageManager stringForKey:Last];
    _lblIns_Change.text=[LanguageManager stringForKey:Change_];
}

- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
    if(self.isDashBoardMode){
        _lblSector.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_subItemTextColor);
        _lblSector.font = AppFont_MainFontBoldWithSize(20);
        [_imgArrowDown toColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_subItemTextColor)];
        self.lbl_IndicesTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].db_textColor);
//        self.lbl_IndicesTitle.text = [LanguageManager stringForKey:@"Markets"];
    }else{
        [_imgArrowDown toColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor)];
    }
    
	_v_Header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subHeaderBg);
	//TextColor
	_lblLastPrice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_LabelTextColor);
	_lblValueChanged.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].os_LabelTextColor);

	_lblStockSectors.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblLastUpdate.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
	_lblInsNameOrCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblIns_Last.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblIns_Change.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	
	[_imgSwitch toColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subTintIconColor)];
	
}
#pragma mark - LoadData
- (void)loadData{
    [self getIndices];
}
- (void)reloadData{
	[_expandedCells removeAllObjects];
	[_listStocks removeAllObjects];
	[self.tblContent reloadData];
	[self loadData];
}

//+++ Load Stocks At Sector
- (void)loadStockBySector:(SectorInfo *)sector{
	__weak MIndicesVC *_weakSelf = self;
	[UserPrefConstants singleton].quoteScrPage = 0;
	[self startLoadingQuotesBySector:sector completion:^(NSArray<StockModel *> *quotes, NSError *error) {
		if (error) {
			[self->_listStocks removeAllObjects];
			[self.tblContent reloadData];
			//Show error here
			[self.contentView showStatusViewWithMessage:error.localizedDescription retryAction:^{
				//+++ Refresh here
				[_weakSelf loadStockBySector:sector];
			}];
			return;
		}
		[self.contentView removeStatusViewIfNeeded];
		self->_listStocks = [quotes mutableCopy];
		[self.tblContent reloadData];
	}];
}
- (void)startLoadingQuotesBySector:(SectorInfo *)sector completion:(void (^)(NSArray <StockModel *> *quotes, NSError *error))completion{
	//Set Sector
	if (sector && [sector.sectorName isEqualToString:[LanguageManager stringForKey:All_Stocks]]) {
		[UserPrefConstants singleton].sectorToFilter = nil;
	}else{
		[UserPrefConstants singleton].sectorToFilter = sector;
	}
	//2: Set rank
	NSString *property = @"101";
	NSString *direction = @"DESC";
	[[VertxConnectionManager singleton] vertxSortByServer:property Direction:direction exchg:self.indiceEx.feed_exchg_code completion:^(id result, NSError *error) {
		NSArray *quotes = [StockModel arrayOfModelsFromDictionaries:(NSArray *)result error:nil];
		dispatch_async(dispatch_get_main_queue(), ^{
			completion(quotes, error);
		});
	}];
}

//Get Indices List
- (void)getIndices{
	//For Network
	if (![VertxConnectionManager singleton].connected) {
		//Do nothing...
		return;
	}
	if ([[UserPrefConstants singleton].userExchagelist count] == 0) {
		[self.contentView showStatusMessage:Loading_Exchanges______];
		return;
	}

    [self.contentView showPrivateHUD];
	[self removeStatusViewIfNeeded];
	_isLoadNewIndices = YES;
	[[VertxConnectionManager singleton] getIndicesFromExchange:self.indiceEx.feed_exchg_code completion:^(id result, NSError *error) {
        [self.contentView dismissPrivateHUD];
		if (error) {
			[self.contentView showStatusViewWithMessage:error.localizedDescription retryAction:^{
				//Reload indices here
				[self getIndices];
			}];
			return ;
		}
		//Parse data here
		// SORT IT
		if ([[QCData singleton].qcIndicesDict count] == 0) {
			[self.contentView showStatusViewWithMessage:[LanguageManager stringForKey:The_Index_is_not_available___Please_try_again_later_] retryAction:^{
				//Reload indices here
				[self getIndices];
			}];
			return;
		}
		NSMutableArray *independentArray = [[NSMutableArray alloc] init];
		for(id key in [[QCData singleton] qcIndicesDict]) {
			NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:key]];
			[independentArray addObject:eachDict];
		}
		NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
		NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
		NSMutableArray *indicesArr = [IndicesModel arrayOfModelsFromDictionaries:sortedArray error:nil];
		
		if (!self->_isLoadNewIndices) {
			return;
		}
		dispatch_async(dispatch_get_main_queue(), ^{
			[self removeStatusViewIfNeeded];
			if (indicesArr && indicesArr.count > 0) {
				self->_indices = [indicesArr copy];
				NSInteger indexOfMainIndices = [self getIndexOfMainIndicesBy:[UserPrefConstants singleton].currentExchangeInfo fromIndices:self->_indices];
				//Reload dropMenu
				NSArray *dropdownDataSource = [self listSectorNameFromIndices:self->_indices];
				NSString *sectorTitle = dropdownDataSource[indexOfMainIndices];
				self->_lblSector.text = sectorTitle;
				self->_currentSelectedSector = [Utils getSectorInfoObjFor:sectorTitle];
				self->_sectorDropdown.dataSources = dropdownDataSource;
				//Start load Stock
				[self loadStockBySector:self->_currentSelectedSector];
				//Load Chart
				IndicesModel *model = self->_indices[indexOfMainIndices];
				[self loadInfoIndices:model];
				self->_isLoadNewIndices = NO;
			}
		});
	}];
}

//+++ Load Indices Chart
- (void)loadInfoIndices:(IndicesModel *)model{
    currentStock = model;
	_lblLastPrice.text = [model.curPrice toCurrencyNumber];
	float close = model.closePrice.floatValue;
	float change  = model.curPrice.floatValue - close;
	float changeP;
	if (close <= 0) {
		changeP = 0;
	}else{
		changeP = change / close * 100;
	}
	//Parse to UI
	_lblValueChanged.text = [NSString stringWithFormat:@"%@ (%@)", [[NSNumber numberWithFloat:change] toChangedStringWithSign], [[NSNumber numberWithFloat:changeP] toPercentWithSign]];
	
	_lblValueChanged.textColor = [[NSNumber numberWithFloat:change] colorByCompareToPrice:0];
	_lblLastPrice.textColor = _lblValueChanged.textColor;
	
    _lblLastUpdate.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Last Updated"], [[NSDate date] stringWithFormat:kFormat_Update_Time]];
	_chartView.stCode = model.stockCode;
	[_chartView startLoadDefaultChart];
}
- (StockModel *)currentStock{
    for(StockModel *model in _listStocks){
        if([model.stockCode isEqualToString:currentStock.stockCode]){
            return model;
        }
    }
    return nil;
}

#pragma mark - Register Notification
- (void)registerNotification{
	//Notification: didUpdate
	[self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
	//[self addNotification:@"DoneGetIndices" selector:@selector(didReceiveNotification:)];
	[self addNotification:@"didReceiveExchangeList" selector:@selector(didReceiveExchangeListNotification:)];
	[self addNotification:@"pnWebSocketDisconnected" selector:@selector(didReceiveErrorSocketNotification:)];
	[self addNotification:kVertxNo_pnWebSocketConnected selector:@selector(socketConnectedNotification:)];
}
#pragma mark - UTILS
- (NSArray *)getIndicesExchange{
	NSArray *exchangeExcludeDerivative = [self getListExchangeExcludeDerivative];
	NSMutableArray *enableExArr = @[].mutableCopy;
	NSArray *_curEnableExCodeArr = [BrokerManager shareInstance].detailBroker.ExchangesEnableIndices;
	//Add: Checked by list enable Indices
	for (ExchangeData *ex in exchangeExcludeDerivative){
		if ([_curEnableExCodeArr containsObject:ex.feed_exchg_code] || [_curEnableExCodeArr containsObject:ex.currency]) {
			[enableExArr addObject:ex];
		}
	}
	return [enableExArr copy];
}
- (NSArray *)getListExchangeExcludeDerivative{
	NSMutableArray *listExchangeName = @[].mutableCopy;
	NSArray *exchangeList = [[UserPrefConstants singleton].userExchagelist copy];
	for (ExchangeData *ex in exchangeList){
		if ([ex.exchange_name.lowercaseString containsString:@"derivative".lowercaseString] || [ex.exchange_name.lowercaseString containsString:[LanguageManager stringForKey:@"Derivative".lowercaseString]] || [ex.exchange_name.lowercaseString containsString:@"Derivatives".lowercaseString]) {
			continue;
		}else{
			[listExchangeName addObject:ex];
		}
	}
	return [listExchangeName copy];
}

- (NSArray *)listExchangeNamesFrom:(NSArray *)exchange{
	NSMutableArray *listExchangeName = @[].mutableCopy;
	for (ExchangeData *ex in _listExchanges){
		[listExchangeName addObject:ex.exchange_name];
	}
	return [listExchangeName copy];
}

//++++ Index of main indices - at first to show
- (NSInteger)getIndexOfMainIndicesBy:(ExchangeInfo *)exInfo fromIndices:(NSArray *)indices{
	NSInteger _index = 0;
	NSString *mainIndicesCode = exInfo.indicesCode;
	//Check to get index of Main Indices
	for (int i = 0; i < indices.count; i ++) {
		IndicesModel *model = indices[i];
		if ([model.stockCode isEqualToString:mainIndicesCode]) {
			_index = i;
			break;
		}
	}
	return _index;
}

- (void)removeStatusViewIfNeeded{
	for (UIView *v in _contentView.subviews) {
		if ([v isKindOfClass:[TCStatusInfoView class]]) {
			[v removeFromSuperview];
			break;
		}
	}
}
//Parse data from result dict
- (NSArray<StockModel *> *)parseDataFromDict:(NSDictionary *)dict{
	if (!dict || dict.count == 0) {
		return @[];
	}
	NSMutableArray *quotes = @[].mutableCopy;
	//Parse data here
	for (NSString *key in dict.allKeys) {
		NSError *error;
		NSDictionary *stockDict = dict[key];
		StockModel *stock = [[StockModel alloc] initWithDictionary:stockDict error:&error];
		if (error) {
			DLog(@"+++ Error: Can not parse data to StockModel - %@", error);
			continue;
		}
		[quotes addObject:stock];
	}
	return [quotes copy];
}

- (NSArray *)listSectorNameFromIndices:(NSArray *)indices{
	NSMutableArray *names = @[].mutableCopy;
	for (IndicesModel *model in indices) {
		if (!model.name || model.name.length == 0) {
			model.name = @"-"; //+++Check for null data
		}
		[names addObject:model.name];
	}
	return [names copy];
}

//Switch - Visible instruction
- (void)setVisibleNamInstruction:(BOOL)isVisible{
	if (isVisible) {
		_lblInsNameOrCode.text = [LanguageManager stringForKey:NAME];
	}else{
		_lblInsNameOrCode.text = [LanguageManager stringForKey:CODE];
	}
}

- (void)setVisibleChangePer:(BOOL)isShow{
	if (isShow) {
		_lblIns_Change.text = [LanguageManager stringForKey:Change_];
	}else{
		_lblIns_Change.text = [LanguageManager stringForKey:Change];
	}
	//float updatedWidth = [_lblIns_Change.text widthOfString:_lblIns_Change.font];
	//_widthLabelIns_Change_Constrain.constant = updatedWidth + 2; //4 for Alignment
	//[self.view layoutIfNeeded];
}
#pragma mark - ACTION
//Switch
- (IBAction)onSwitchVisibleNameOrCodeAction:(id)sender {
	[SettingManager shareInstance].isVisibleNameStock = ![SettingManager shareInstance].isVisibleNameStock;
	[self setVisibleNamInstruction:[SettingManager shareInstance].isVisibleNameStock];
	[[SettingManager shareInstance] save];
	[self.tblContent reloadData];
}

- (IBAction)onSwitchChangeAction:(id)sender {
	[SettingManager shareInstance].isVisibleChangePer = ![SettingManager shareInstance].isVisibleChangePer;
	[self setVisibleChangePer:[SettingManager shareInstance].isVisibleChangePer];
	[[SettingManager shareInstance] save];
	[self.tblContent reloadData];
}

- (IBAction)onShowSectorDropdownAction:(UIButton *)sender {
	_sectorDropdown.sourceView = sender;
	//_sectorDropdown.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subTextColor);
	//_sectorDropdown.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_sectorDropdown.offset = 0;
	[_sectorDropdown show];
}


#pragma mark - UITableViewDataSource

//For Header ++++ No use header now
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 44;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
	return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	//For Indices
	if (section == 0) {
//		IndicesSectionView *_indicesSection = [[IndicesSectionView alloc] initWithControlTitles:@[[LanguageManager stringForKey:@"US"], [LanguageManager stringForKey:@"Canada"], [LanguageManager stringForKey:@"Asia"], [LanguageManager stringForKey:@"Europe"], [LanguageManager stringForKey:@"Germany"], [LanguageManager stringForKey:@"Australia"]]];
		IndicesSectionView *_indicesSection = [[IndicesSectionView alloc] initWithFrame:CGRectMake(0, 0, _tblContent.frame.size.width, 44)];
		[_indicesSection setupControlTitles:@[[LanguageManager stringForKey:@"US"], [LanguageManager stringForKey:@"Canada"], [LanguageManager stringForKey:@"Asia"], [LanguageManager stringForKey:@"Europe"], [LanguageManager stringForKey:@"Germany"], [LanguageManager stringForKey:@"Australia"]]];
		[_indicesSection setControlViewDelegate:self];
		return _indicesSection;
	}
	
	//For Main Watchlist
	if (section == 1) {
		WatchlistSectionView *watchlistSection = [[WatchlistSectionView alloc] initWithSectionType:_typeCell];
		watchlistSection.delegate = self;
		return watchlistSection;
	}
	return [UIView new];
}
*/
//For Cell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_isDashBoardMode){
        return 0;
    }else{
        return [_listStocks count];
    }
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return [_expandedCells containsObject:indexPath] ? kEXPANDED_CELL_HEIGHT : kNORMAL_CELL_HEIGHT;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
	//StockInfoCell *aCell = (StockInfoCell *)cell;
	//aCell.cellType = CellType_Short;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	StockTypeSingleCell *cell = [tableView dequeueReusableCellWithIdentifier:[StockTypeSingleCell reuseIdentifier]];
	if (!cell) {
		cell = [[StockTypeSingleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[StockTypeSingleCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	[tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
	cell.mainDelegate = self;
	cell.indexPath = indexPath;
	cell.isExpandedCell = [_expandedCells containsObject:indexPath];
	//Parse data to UI
	[cell updateData:_listStocks[indexPath.row]];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	/*
	DetailStockVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [DetailStockVC storyboardID]);
	_detailVC.stock = _listStocks[indexPath.row];
	[self nextTo:_detailVC animate:YES];
	*/
}

#pragma mark - StockTypeSingleCellDelegate
- (void)didTapExpandCell:(StockTypeSingleCell *)cell{
	NSIndexPath *curIndexPath = cell.indexPath;
	//Save current Stock
	[UserPrefConstants singleton].userSelectingThisStock  =  _listStocks[curIndexPath.row] ;
	
	BOOL shouldClose = [_expandedCells containsObject:curIndexPath];
	if (shouldClose) {
		[_expandedCells removeObject:curIndexPath];
	}else{
		[_expandedCells addObject:curIndexPath];
	}
	[_tblContent beginUpdates];
	cell.isExpandedCell = !shouldClose;
	[_tblContent endUpdates];
}

- (void)shouldGotoDetailFromCell:(StockTypeSingleCell *)cell{
	StockInfoVC *_detailVC = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [StockInfoVC storyboardID]);
	_detailVC.stock = _listStocks[cell.indexPath.row];
	[self nextTo:_detailVC animate:YES];
}

- (void)shoudSwitchValue:(StockTypeSingleCell *)cell{
	[self onSwitchChangeAction:nil];
}
#pragma mark - TCControlViewDelegate
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
	[_sectorDropdown hide];
	_isLoadNewIndices = YES;
	ExchangeData *newEx = [_listExchanges objectAtIndex:index];
	self.indiceEx = newEx;
	[Utils selectedNewExchange:self.indiceEx];
	//Refresh UI
	[self performSelector:@selector(reloadData) withObject:nil afterDelay:1.0];
}

#pragma mark - TCChartViewDelegate
//- (void)doubleTapChartView:(TCChartView *)chartView{
//	//[self showCustomAlertWithMessage:@"Coming soon!..."];
//	//return;
//	 TCAnimation *tcAnimation = [[TCAnimation alloc] init];
//	[tcAnimation showChartDetail:self.chartView isCell:NO];
//}

#pragma mark - Notification
/*
- (void)didReceiveNotification:(NSNotification *)noti{
	DLog(@"Noti: %@", noti.object);
	// SORT IT
	if ([[[QCData singleton] qcIndicesDict] count] == 0) {
		[self showCustomAlertWithMessage:@"The indices is not availabel!!!"];
		TCStatusInfoView *_statusView = [TCStatusInfoView new];
		[_statusView showStatus:[LanguageManager stringForKey:@"The indices is not availabel!!!"] inView:self.view retryAction:^{
			//+++ Refresh here
		}];
		return;
	}
	NSMutableArray *independentArray = [[NSMutableArray alloc] init];
	for(id key in [[QCData singleton] qcIndicesDict]) {
		NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:key]];
		[independentArray addObject:eachDict];
	}
	NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
	NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
	NSMutableArray *indicesArr = [IndicesModel arrayOfModelsFromDictionaries:sortedArray error:nil];

	if (!_isLoadNewIndices) {
		return;
	}
	if (indicesArr && indicesArr.count > 0) {
		_indices = [indicesArr copy];
		//Reload dropMenu
		[self loadDropMenu];
		_currentSelectedSector = [Utils getSectorInfoObjFor:_menuDropDown.titleMenu];
		//Start load Stock
		[self loadStockBySector:_currentSelectedSector];
		//Load Chart
		IndicesModel *model = _indices[0];
		[self loadInfoIndices:model];
		_isLoadNewIndices = NO;
	}
	
	//_indicesValArray = [NSMutableArray arrayWithArray:sortedArray];
	
	//NSString *firstItemStockCode = [[_indicesValArray objectAtIndex:0] objectForKey:FID_33_S_STOCK_CODE];
	//NSString *latestTradedNumber = [[[_indicesValArray objectAtIndex:0] objectForKey:FID_103_I_TRNS_NO] stringValue];
}
*/
// +++ Did Update
- (void)didUpdate:(NSNotification *)noti{
	NSString *stockCode = noti.object;
	StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
	if (!changedStock) {
		return; //do nothing
	}
	
	//For item
	NSInteger indexOfQuoteStock = [Utils indexOfStockByCode:stockCode fromArray:self->_listStocks];
	if (self->_listStocks.count > 0 && indexOfQuoteStock < self->_listStocks.count) {
		if (indexOfQuoteStock != -1) {
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfQuoteStock inSection:0];
			StockModel *oldStock = [self->_listStocks[indexOfQuoteStock] copy];
			[self->_listStocks replaceObjectAtIndex:indexOfQuoteStock withObject:changedStock];
			StockTypeSingleCell *cell = [self->_tblContent cellForRowAtIndexPath:indexPath];
			if (cell) {
				[cell.lastView makeAnimateBy:oldStock newStock:changedStock completion:^{
					[cell updateData:changedStock];
				}];
			}
		}
	}
}

//For exchange List
- (void)didReceiveExchangeListNotification:(NSNotification *)note{
	[self setupExchangeControl];
	if (_listStocks.count == 0) {
		[self loadData];
	}
}
 //Error Socket
- (void)didReceiveErrorSocketNotification:(NSNotification *)noti{	
	[[Utils shareInstance] dismissWindowHUD];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView dismissPrivateHUD];
	//Show message
	if (!self.isDashBoardMode) {
        [_bubbleMsgView showBubbleMessage:[LanguageManager stringForKey:No_Connection] inView:self.view withType:BubbelMessageType_Error autoHidden:NO];
    }else{
        if([self.delegate respondsToSelector:@selector((didShowError:autoHide:type:))]){
            [self.delegate didShowError:[LanguageManager stringForKey:No_Connection] autoHide:NO type:BubbelMessageType_Error];
        }
    }
}
- (void)socketConnectedNotification:(NSNotification *)noti{
	//Update Bubble Message
	//Update Bubble Message
    if(self.isDashBoardMode){
        if([self.delegate respondsToSelector:@selector((didShowError:autoHide:type:))]){
            [self.delegate didShowError:[LanguageManager stringForKey:Connected] autoHide:YES type:BubbelMessageType_Success];
        }
    }else{
        _bubbleMsgView.message = [LanguageManager stringForKey:Connected];
        _bubbleMsgView.bubbleType = BubbelMessageType_Success;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self->_bubbleMsgView hide];
        });
    }
    
	
	//Dimiss other indicator if needed
	[[Utils shareInstance] dismissWindowHUD];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView dismissPrivateHUD];
	if (_listStocks.count == 0) {
		[self loadData];
	}
}

@end
