//
//  MStockTrackerController.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MStockTrackerController.h"
#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"
#import "Utils.h"
#import "MStockTrackerModel.h"
#import "BrokerModel.h"

#define MAX_NUM_LINES 4
#define MAX_ITEMS_PER_LINE 3
@interface MStockTrackerController()
{
    VertxConnectionManager *vertxConnectionManager;
    ATPAuthenticate *atp;
    int counter;
    int arrayLimit;
    int numberOfLine;
    
    int _favID;
    int _valueCount;
    int _quantityCount;
    NSString *_exch;
}
@end
@implementation MStockTrackerController
@synthesize tickerArray;
@synthesize watchListArr;
@synthesize exChangeArr;
- (void)initValues{
    tickerArray = [NSMutableArray new];
    _isRightToLeft = NO;
    counter =0;
    numberOfLine = 5;
    arrayLimit = numberOfLine*MAX_ITEMS_PER_LINE;
    [self loadWatchList];
    [self loadExChangeList];
    
}
- (void)setIsRightToLeft:(BOOL)isRightToLeft{
    _isRightToLeft = isRightToLeft;
}
- (void)registerListener{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestWatchListContentReceived) name:@"getWatchlistContentSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(marketStreamerReceived:)  name:@"marketStreamerReceived" object:nil];
    vertxConnectionManager = [VertxConnectionManager singleton];
    
    //ggtt
    BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
    NSUInteger brokerTag = brokerModel.tag;
    switch (brokerTag) {
        case BrokerTag_SSI:
            //SSI doesn't request tracking data as default.
            break;
            
        default:
            [vertxConnectionManager vertxGetMarketStreamer:nil andValue:0 andQuantity:0 andCondition:0];
            break;
    }
    
    atp = [ATPAuthenticate singleton];
}
- (void)removeListerner{
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    [[NSNotificationCenter defaultCenter] removeObserver:@"getWatchlistContentSuccess"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"marketStreamerReceived"];
}

- (void)setMaxItems:(int)maxItems{
    arrayLimit = maxItems;
}

- (void)changeDirection{
    self.isRightToLeft = !self.isRightToLeft;
    self.tickerArray = [[NSMutableArray alloc] initWithArray:[[tickerArray reverseObjectEnumerator] allObjects]];
    if([self.delegate respondsToSelector:@selector(reloadContent:)]){
        [self.delegate reloadContent:0];
    }
}
#pragma mark - Receive Stock Item from Vertx
- (void)marketStreamerReceived:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary * response = [notification.userInfo copy];
        
        [self calculateData:response];
    });
}


- (void) latestWatchListContentReceived{
    // NSLog(@"latestWatchListContentReceived %@",[UserPrefConstants singleton].userWatchListStockCodeArr);
    [vertxConnectionManager vertxGetMarketStreamer:[UserPrefConstants singleton].userWatchListStockCodeArr andValue:_valueCount andQuantity:_quantityCount andCondition:0 exch:_exch];
    
}


- (void)applyFilter:(int)favID valueCount:(int)valueCount quantityCount:(int)quantityCount exch:(NSString *)exch{
    _favID = favID;
    _valueCount = valueCount;
    _quantityCount = quantityCount;
    if(exch){
        _exch = exch;
    }else{
        _exch = [UserPrefConstants singleton].userCurrentExchange;
    }
    
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    
    if (favID != -1) {
        //   NSLog(@"Fav ID %d",favID);
        [atp getWatchListItemsForStreamer:favID];
    }else{
        [vertxConnectionManager vertxGetMarketStreamer:nil andValue:valueCount andQuantity:quantityCount andCondition:0 exch:exch];
    }
}
#pragma mark Load Data From Local
- (void)calculateData:(NSDictionary *)response{
    MStockTrackerModel *model = [MStockTrackerModel fromDict:response];
    NSInteger insertAt = 0;
    if(self.isRightToLeft){
        insertAt = tickerArray.count-1;
    }
    
    if(tickerArray.count < arrayLimit){
        if (tickerArray.count==0) {
            [tickerArray addObject:model];
        }else{
            if(self.isRightToLeft){
                [tickerArray addObject:model];
            }else{
                [tickerArray insertObject:model atIndex:0];
            }
        }
    }else{
        if(self.isRightToLeft){
            [tickerArray removeObjectAtIndex:0];
            [tickerArray addObject:model];
        }else{
            [tickerArray insertObject:model atIndex:0];
            [tickerArray removeLastObject];
        }
    }
    if(tickerArray.count > arrayLimit){
        for (int loopIndex = 0; loopIndex < tickerArray.count - arrayLimit; loopIndex++) {
            if(tickerArray.count > 0){
                if(self.isRightToLeft){
                    [tickerArray removeObjectAtIndex:0];
                }else{
                    [tickerArray removeLastObject];
                }
            }
        }
    }
    if([self.delegate respondsToSelector:@selector(reloadContent:)]){
        [self.delegate reloadContent:insertAt];
    }
}
- (void) testStreamer{
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"TestStreamer" ofType:@"plist"];
    NSDictionary *response = [NSDictionary dictionaryWithContentsOfFile:file];
    
    [self calculateData:response];
}
-(void) loadWatchList{
    watchListArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
    //    [watchListArr insertObject:@"None" atIndex:0];
    //NSLog(@"watchListArr %@",watchListArr);
    // if watchlist exist, select the first one
}
- (void)loadExChangeList{
    exChangeArr = [[NSMutableArray alloc] initWithArray:[Utils getListExchangeName] copyItems:YES];
}

- (NSMutableArray *)getWatchListNames{
    NSMutableArray *watchListNames = [NSMutableArray new];
    for(id name in watchListArr){
        if([name isKindOfClass:[NSDictionary class]]){
            [watchListNames addObject:[name objectForKey:@"Name"]];
        }else if([name isKindOfClass:[NSString class]]){
            [watchListNames addObject:name];
        }
    }
    return watchListNames;
}
@end
