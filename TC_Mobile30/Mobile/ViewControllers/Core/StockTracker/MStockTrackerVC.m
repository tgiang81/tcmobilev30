//
//  MStockTrackerVC.m
//  TCiPad
//
//  Created by giangtu on 7/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MStockTrackerVC.h"
#import "StockTracker.h"
#import "VertxConnectionManager.h"
#import "UserPrefConstants.h"
#import "ATPAuthenticate.h"
#import "CAPopUpViewController.h"
#import "UIViewController+Popup.h"
#import "TCMenuDropDown.h"
#import "LanguageKey.h"
#define MAX_NUM_LINES 4
#define MAX_ITEMS_PER_LINE 3

@interface MStockTrackerVC () <TCMenuDropDownDelegate> {
    NSMutableArray *watchListArr;
    NSMutableArray *exChangeArr;
    VertxConnectionManager *vertxConnectionManager;
    ATPAuthenticate *atp;
    
    int counter;
    int valueCount;
    int quantityCount;
    NSArray *filterByArray;
    int numberOfLine;
    BOOL leftDirection;
    NSInteger currentWatchListIndex;
    NSInteger currentExchangeIndex;
    __weak IBOutlet UIStackView *stackView1;
    __weak IBOutlet UIStackView *stackView2;
    __weak IBOutlet UIStackView *stackView3;
    __weak IBOutlet UIStackView *stackView4;
}
@property (nonatomic, strong) NSMutableArray *tickerArray;
@end

@implementation MStockTrackerVC
@synthesize tickerArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createLeftMenu];
    self.customBarTitle = [LanguageManager stringForKey:@"Tracker Record"];
    watchListArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
    [watchListArr insertObject:@"None" atIndex:0];

    //NSLog(@"watchListArr %@",watchListArr);
    // if watchlist exist, select the first one
    
    filterByArray = [[NSArray alloc] initWithObjects:@"None",@"Value >= 2M",@"Vol >= 50K",@"Vol >= 30K & Value >= 1M",@"Vol >= 50K & Value >= 2M",nil];
    vertxConnectionManager = [VertxConnectionManager singleton];
    [vertxConnectionManager vertxGetMarketStreamer:nil andValue:0 andQuantity:0 andCondition:0];
    tickerArray = [[NSMutableArray alloc] init];
    atp = [ATPAuthenticate singleton];
    [self loadWatchList];
    [self loadExChangeList];
    [self setupDropDown];
    leftDirection = TRUE;
    [self buildCells];
    
}
- (void)setupDropDown{
    _v_DropDown_WatchList.layer.zPosition = 1;
    _v_DropDown_WatchList.titleMenu = [LanguageManager stringForKey:_WatchList] ;
    _v_DropDown_WatchList.menuTitles = [[self getExChangeNames] copy];
    _v_DropDown_WatchList.numberItemVisible = 6;
    _v_DropDown_WatchList.bgColor = [UIColor whiteColor];
    _v_DropDown_WatchList.delegate = self;
    [_v_DropDown_WatchList reloadDropDown];
    
    _v_DropDown_ExChangeList.layer.zPosition = 2;
    _v_DropDown_ExChangeList.titleMenu = @"Exchange";
    _v_DropDown_ExChangeList.menuTitles = [exChangeArr copy];
    _v_DropDown_ExChangeList.numberItemVisible = 6;
    _v_DropDown_ExChangeList.bgColor = [UIColor whiteColor];
    _v_DropDown_ExChangeList.delegate = self;
    [_v_DropDown_ExChangeList reloadDropDown];
}
-(void) buildCells
{
    for (UIView *v in stackView1.subviews) {
        [stackView1 removeArrangedSubview:v];
        [v removeFromSuperview];
    }
    for (UIView *v in stackView2.subviews) {
        [stackView2 removeArrangedSubview:v];
        [v removeFromSuperview];
    }
    for (UIView *v in stackView3.subviews) {
        [stackView3 removeArrangedSubview:v];
        [v removeFromSuperview];
    }
    
    for (UIView *v in stackView4.subviews) {
        [stackView4 removeArrangedSubview:v];
        [v removeFromSuperview];
    }
    
    //Left-Right direction
    
    for (int i =0; i < MAX_NUM_LINES; i ++) {
        
        for (int k =0; k < MAX_ITEMS_PER_LINE; k ++) {
            
            StockTracker *ticker = [[StockTracker alloc] init];
            [ticker.heightAnchor constraintEqualToConstant:100].active = true;
            ticker.alpha = 0;
            
            int  iTag;
            if (leftDirection) {
                iTag = ( MAX_ITEMS_PER_LINE* i) + (MAX_ITEMS_PER_LINE - (k+1)) + 200;
            }else{
                iTag = (MAX_ITEMS_PER_LINE * i) + k  + 200;
            }
            ticker.lblStkName.text = [NSString stringWithFormat:@"%d", iTag];
            
            [ticker setTag: iTag];
            switch (i) {
                case 0:
                    [stackView1 addArrangedSubview:ticker];
                    
                    break;
                case 1:
                    [stackView2 addArrangedSubview:ticker];
                    
                    break;
                case 2:
                    [stackView3 addArrangedSubview:ticker];
                    
                    break;
                case 3:
                    [stackView4 addArrangedSubview:ticker];
                    
                    break;
                    
                default:
                    break;
            }
            
        }
    }
    
}

- (void) setTickerContent:(int)tag{
    //   NSLog(@"ticker Tag %d",tag);
    StockTracker *ticker;
    ticker = (StockTracker *)[containerView viewWithTag:tag+200];
    ticker.alpha = 1;
    
    NSString *stockCode = [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag] objectForKey:@"33"]];
    NSString *conditon =  [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag] objectForKey:@"104"]];
    
    float lacp =  [[[tickerArray objectAtIndex:tag] objectForKey:@"51"] floatValue];
    float price = [[[tickerArray objectAtIndex:tag] objectForKey:@"98"] floatValue];
    
    NSString *quantity = @"";
    SET_IF_NOT_NULL(quantity,[[tickerArray objectAtIndex:tag] objectForKey:@"99"]);
    
    NSString *bbh_sbhArray = @"";
    
    //IF PH -> got 120
    SET_IF_NOT_NULL(bbh_sbhArray,[[tickerArray objectAtIndex:tag] objectForKey:@"120"]);
    
    //NSString *quantity =  [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag] objectForKey:@"99"]];
    
    // NSString *bbh_sbhArray =  [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag]  objectForKey:@"120"]];
    //NSString *bbh = [NSString stringWithFormat:@"%d",test];//
    NSString *bbh = @"";
    NSString *sbh = @"";
    
    if (bbh_sbhArray.length!=0) {
        NSArray *arrTmp = [bbh_sbhArray componentsSeparatedByString:@","];
        
        if (arrTmp.count > 3) {
            bbh = arrTmp[1];
            sbh = arrTmp[3];
        }
    }
    
    [ticker updateContent:stockCode
                 andPrice:price
                  andLACP:lacp
              andQuantity:quantity
             andCondition:conditon
                   andBBH:bbh
                   andSBH:sbh];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    counter =0;
    numberOfLine =1;
    arrayLimit = numberOfLine*MAX_ITEMS_PER_LINE;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestWatchListContentReceived) name:@"getWatchlistContentSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(marketStreamerReceived:)  name:@"marketStreamerReceived" object:nil];
}



- (IBAction) clickButtonDirection:(id)sender{
    leftDirection =! leftDirection;
    
    if (leftDirection) {
        [directionButton setImage:[UIImage imageNamed:@"BackwardButton"] forState:UIControlStateNormal];
        
    }else{
        [directionButton setImage:[UIImage imageNamed:@"ForwardButton"] forState:UIControlStateNormal];
    }
    
    [self buildCells];
    
    //update content
    for (int x=0;x<arrayLimit;x++) {
        [self setTickerContent:x];
    }
}

// Line = Floor...Step to floor1...select line 1.
- (IBAction) clickButtonLine:(id)sender{
    numberOfLine++;
    if (numberOfLine> MAX_NUM_LINES) {
        numberOfLine=1;
    }
    arrayLimit = numberOfLine*MAX_ITEMS_PER_LINE;
    [lineButton setTitle:[NSString stringWithFormat:@"%d",numberOfLine] forState:UIControlStateNormal];
    
    //1 line ipad: could display 9 items.
    //Total max || 3 lines
    
    //Hiden other items...
    for(int x=0;x< MAX_ITEMS_PER_LINE * MAX_NUM_LINES ;x++){
        
        if (x>=arrayLimit) {
            StockTracker *ticker = (StockTracker *)[containerView viewWithTag:x+200];
            ticker.alpha = 0;
        }
    }
    
}

#pragma mark - Receive Stock Item from Vertx
- (void)marketStreamerReceived:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary * response = [notification.userInfo copy];
        
        if(tickerArray.count<arrayLimit){
            if (tickerArray.count==0) {
                [tickerArray addObject:response];
            }else{
                [tickerArray insertObject:response atIndex:0];
            }
            //  NSLog(@"marketStreamerReceived %@ = %d",response,counter);
        }else{
            if (counter>=arrayLimit) {
                counter=0;
            }
            [tickerArray insertObject:response atIndex:0];
            
            //[tickerArray replaceObjectAtIndex:counter withObject:response];
            //NSLog(@"replaceObjectAtIndex %@ = %d",response,counter);
        }
        
        //Update content..
        if (counter<arrayLimit) {
            // NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:leftCount inSection:0];
            // NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            // [tableViewLeft reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
            
            // Update Content
            for (int x=0;x<=counter;x++) {
                [self setTickerContent:x];
            }
        }
        
        // populate the data to screen
        counter++;
        
        //remove redundant items in array
        NSMutableArray *discardedItems = [NSMutableArray array];
        if (tickerArray.count>arrayLimit) {
            int x=0;
            for (NSDictionary * response in tickerArray) {
                if (x>arrayLimit) {
                    [discardedItems addObject:response];
                }
                x++;
            }
            [tickerArray removeObjectsInArray:discardedItems];
        }
        
        NSLog(@"Ticker Array Size %lu",(unsigned long)tickerArray.count);
    });
}


- (void) latestWatchListContentReceived{
    // NSLog(@"latestWatchListContentReceived %@",[UserPrefConstants singleton].userWatchListStockCodeArr);
    [vertxConnectionManager vertxGetMarketStreamer:[UserPrefConstants singleton].userWatchListStockCodeArr andValue:valueCount andQuantity:quantityCount andCondition:0];
    
}


//MARK:- TEST
- (void) handleTimer{
    [self testStreamer];
}

- (void) testStreamer{
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"TestStreamer" ofType:@"plist"];
    NSDictionary *response = [NSDictionary dictionaryWithContentsOfFile:file];
    
    if(tickerArray.count<arrayLimit){
        if (tickerArray.count==0) {
            [tickerArray addObject:response];
        }else{
            [tickerArray insertObject:response atIndex:0];
        }
    }else{
        if (counter>=arrayLimit) {
            counter=0;
        }
        [tickerArray insertObject:response atIndex:0];
    }
    
    if (counter<arrayLimit) {
        // Update Content
        for (int x=0;x<=counter;x++) {
            [self setTickerContent:x];
        }
        // populate the data to screen
        counter++;
    }
}
-(void) loadWatchList{
    watchListArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
//    [watchListArr insertObject:@"None" atIndex:0];
    //NSLog(@"watchListArr %@",watchListArr);
    // if watchlist exist, select the first one
}
- (void)loadExChangeList{
    exChangeArr = [[NSMutableArray alloc] initWithArray:[Utils getListExchangeName] copyItems:YES];
}

- (NSMutableArray *)getExChangeNames{
    NSMutableArray *watchListNames = [NSMutableArray new];
    for(id name in watchListArr){
        if([name isKindOfClass:[NSDictionary class]]){
            [watchListNames addObject:[name objectForKey:@"Name"]];
        }else if([name isKindOfClass:[NSString class]]){
            [watchListNames addObject:name];
        }
    }
    return watchListNames;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    [[NSNotificationCenter defaultCenter] removeObserver:@"getWatchlistContentSuccess"];
}



- (void)didSelect:(StockTrackerFilterType)type row:(NSInteger)row{
    if (type==StockTrackerFilterType_WatchList) {
        
        if (row==0) {
            watchListLabel.text = @"None";
            favID = 0;
        }else{
            watchListLabel.text = [[watchListArr objectAtIndex:row] objectForKey:@"Name"];
            favID = [[[watchListArr objectAtIndex:row] objectForKey:@"FavID"] intValue];
        }
        
    }else{
        filterByLabel.text = [filterByArray objectAtIndex:row];
        switch (row) {
            case 0:
                valueCount = 0;
                quantityCount = 0;
                break;
            case 1:
                valueCount = 2000000;
                quantityCount = 0;
                break;
            case 2:
                valueCount = 0;
                quantityCount = 50000;
                break;
            case 3:
                valueCount = 100000;
                quantityCount = 30000;
                break;
            case 4:
                valueCount = 2000000;
                quantityCount = 50000;
                break;
            default:
                break;
        }
    }
}

#pragma mark - Table view data source

- (IBAction)changeMode:(UIButton *)sender {
    
}


- (IBAction)clickApplyButton:(UIButton *)sender{
    
    counter = 0;
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    for(int x=0;x<arrayLimit;x++){
        StockTracker *ticker = (StockTracker *)[containerView viewWithTag:x+2];
        
        ticker.alpha = 0;
    }
    [tickerArray removeAllObjects];
    
    if (favID!=0) {
        //   NSLog(@"Fav ID %d",favID);
        [atp getWatchListItemsForStreamer:favID];
    }else{
        [vertxConnectionManager vertxGetMarketStreamer:nil andValue:valueCount andQuantity:quantityCount andCondition:0];
    }
}

- (IBAction)clickWatchList:(UIButton *)sender{
    
    [self setupPopup:[self getExChangeNames] fromView:self.btn_filter width:150 rowHeight:35 completion:^(CAPopUpViewController *popupVC, UITableViewCell *popCell, NSInteger row, NSInteger section) {
        [self didSelect:StockTrackerFilterType_WatchList row:row];
    }];
}


- (IBAction)clickFilterBy:(UIButton *)sender{
    [self setupPopup:filterByArray fromView:lineButton width:150 rowHeight:35 completion:^(CAPopUpViewController *popupVC, UITableViewCell *popCell, NSInteger row, NSInteger section) {
        [self didSelect:StockTrackerFilterType_Filter row:row];
    }];
}

#pragma mark TCMenuDropDownDelegate
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
    //Do something here
    if(menuView == _v_DropDown_WatchList){
        currentWatchListIndex = index;
    }else{
        currentExchangeIndex = index;
    }
    
}

@end
