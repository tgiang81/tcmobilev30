//
//  MStockTrackerModeVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MStockTrackerModeVC.h"
#import "MStockTrackerController.h"
#import "StockStreamerCardModeView.h"
#import "StockStreamerView.h"
#import "StockTrackerCell.h"
#import "StockStreamerCell.h"
#import "TCMenuDropDown.h"
#import "TCStatusInfoView.h"
#import "LanguageKey.h"
@interface MStockTrackerModeVC () <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, MStockTrackerControllerDelegate, TCMenuDropDownDelegate>
{
    MStockTrackerController *controller;
    MStockTrackerMode mode;
    NSInteger currentWatchListIndex;
    NSInteger currentExchangeIndex;
    CGFloat rowHeight;
    NSInteger lastUpdatedIndex;
    
    NSArray *filterByArray;
    int valueCount;
    int quantityCount;
    
    BOOL isRightToLeft;
    
    TCStatusInfoView *_tmpstatusView;
}
@end

@implementation MStockTrackerModeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    quantityCount = -1;
    self.enableTapDismissKeyboard = YES;
    [self createLeftMenu];
    [self checkIsStream:self.isSteamerMode];
    controller = [MStockTrackerController new];
    controller.delegate = self;
    [controller initValues];
    [controller registerListener];
    
    filterByArray = [[NSArray alloc] initWithObjects:[LanguageManager stringForKey:@"None"],[LanguageManager stringForKey:@"Value >= 2M"], [LanguageManager stringForKey:@"Vol >= 50K"], [LanguageManager stringForKey:@"Vol >= 30K & Value >= 1M"], [LanguageManager stringForKey:@"Vol >= 50K & Value >= 2M"],nil];
    rowHeight = 40;
    currentWatchListIndex = -1;
    currentExchangeIndex = -1;
    
    self.v_CardTracker.clv_content.delegate = self;
    self.v_CardTracker.clv_content.dataSource = self;
    
    self.v_Tracker.leftSteamerView.delegate = self;
    self.v_Tracker.leftSteamerView.dataSource = self;
    self.v_Tracker.leftSteamerView.tableFooterView = [UIView new];
    [self.btn_Direction setHidden:YES];
    [self.btn_Mode setHidden:NO];
    self.btn_Apply.layer.cornerRadius = 4;
    [self setupDropDown];
    
    //self.view.backgroundColor = UIColorFromRGB(0x374D5D);
//    [NSTimer scheduledTimerWithTimeInterval: 1.0
//                                     target: self
//                                   selector: @selector(handleTimer)
//                                   userInfo: nil
//                                    repeats: YES];
    [self createUI];
    [self reloadContent:-1];
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [controller setMaxItems:[self calculateMaxItems]];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [controller removeListerner];
}

- (void)createUI{
    self.topInfoView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgTopColor);
    self.v_Tracker.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgColor);
}

- (void) handleTimer{
    [controller testStreamer];
}
- (int)calculateMaxItems{
    int numberOfRows = (int)(self.v_Tracker.leftSteamerView.frame.size.height/rowHeight);
    if(numberOfRows % 2 != 0){
        numberOfRows++;
    }
    return numberOfRows;
}
- (void)checkIsStream:(BOOL)isSteamerMode{
    if(isSteamerMode){
        self.title = @"Market Streamer";
    }else{
        self.title = @"STOCK TRACKER";
    }
//    [self.btn_Mode setHidden:!isSteamerMode];
//    [self.btn_Direction setHidden:!isSteamerMode];
}
#pragma mark Action

- (IBAction)changeMode:(UIButton *)sender {
    if(mode == MStockTrackerMode_Card){
        mode = MStockTrackerMode_Normal;
        controller.isRightToLeft = YES;
        [self.v_CardTracker setHidden:YES];
        [self.v_Tracker setHidden:NO];
        [self.btn_Direction setHidden:YES];
    }else{
        mode = MStockTrackerMode_Card;
        controller.isRightToLeft = !isRightToLeft;
        [self.v_CardTracker setHidden:NO];
        [self.v_Tracker setHidden:YES];
        if(!self.isSteamerMode){
            [self.btn_Direction setHidden:NO];
        }
    }
    [controller changeDirection];
    [self checkDirectionButton:self.btn_Direction];
}
- (IBAction)changeDirection:(UIButton *)sender {
    if(mode == MStockTrackerMode_Card){
        [controller changeDirection];
        isRightToLeft = controller.isRightToLeft;
        [self checkDirectionButton:sender];
    }
}

- (void)checkDirectionButton:(UIButton *)sender{
    if (controller.isRightToLeft) {
        [sender setImage:[UIImage imageNamed:@"ic_directionBackwardButton"] forState:UIControlStateNormal];
    }else{
        [sender setImage:[UIImage imageNamed:@"ic_directionForwardButton"] forState:UIControlStateNormal];
    }
}


- (IBAction)clickApplyButton:(UIButton *)sender{
    [controller.tickerArray removeAllObjects];
    [self reloadContent];
    int favID = 0;
    ExchangeData *newEx;
    if(currentWatchListIndex > 0){
        favID = [[[controller.watchListArr objectAtIndex:currentWatchListIndex-1] objectForKey:@"FavID"] intValue];
    }
    
    if(currentExchangeIndex > 0){
        newEx = [[UserPrefConstants singleton].userExchagelist objectAtIndex:currentExchangeIndex-1];
    }
    if(favID != 0 || currentWatchListIndex != -1 || currentExchangeIndex != -1 || newEx != nil || quantityCount != -1){
        [self reloadContent:0];
        [controller applyFilter:favID valueCount:valueCount quantityCount:quantityCount exch:newEx.feed_exchg_code];
    }else{
        [self reloadContent:-1];
    }
}



#pragma mark TCMenuDropDownDelegate
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
    //Do something here
    if(menuView == _v_DropDown_WatchList){
        currentWatchListIndex = index;
        [self changeTitleTextDropDown:_v_DropDown_WatchList isPlaceHolder:index!=0];
    }else if (menuView == _v_DropDown_ExChangeList){
        [self changeTitleTextDropDown:_v_DropDown_ExChangeList isPlaceHolder:index!=0];
        currentExchangeIndex = index;
    }else{
        [self changeTitleTextDropDown:_v_DropDown_FilterVolList isPlaceHolder:index!=0];
        switch (index) {
            case 0:
                valueCount = 0;
                quantityCount = 0;
                break;
            case 1:
                valueCount = 2000000;
                quantityCount = 0;
                break;
            case 2:
                valueCount = 0;
                quantityCount = 50000;
                break;
            case 3:
                valueCount = 100000;
                quantityCount = 30000;
                break;
            case 4:
                valueCount = 2000000;
                quantityCount = 50000;
                break;
            default:
                break;
        }
    }
    
}

- (void)setupDropDown{
    _v_DropDown_ExChangeList.dismissAutoResize = YES;
    _v_DropDown_ExChangeList.titleAlign =  NSTextAlignmentLeft;
    _v_DropDown_ExChangeList.radius = 12;
    _v_DropDown_ExChangeList.height = 28;
    _v_DropDown_ExChangeList.titleMenu = [LanguageManager stringForKey:@"Exchange"];
    NSMutableArray *exchanges = controller.exChangeArr;
    [exchanges insertObject:[LanguageManager stringForKey:@"All"] atIndex:0];
    _v_DropDown_ExChangeList.menuTitles = exchanges;
    _v_DropDown_ExChangeList.numberItemVisible = 6;
    
    
    _v_DropDown_ExChangeList.delegate = self;
    [_v_DropDown_ExChangeList reloadDropDown];
    
    _v_DropDown_WatchList.titleAlign =  NSTextAlignmentLeft;
    _v_DropDown_WatchList.dismissAutoResize = YES;
    _v_DropDown_WatchList.radius  = 12;
    _v_DropDown_WatchList.height = 28;
//    _v_DropDown_WatchList.indicatorImage = [UIImage imageNamed:@"ic_dropDownDarkBlue"];
    _v_DropDown_WatchList.titleMenu = [LanguageManager stringForKey:_WatchList];
    NSMutableArray *watchList = [controller getWatchListNames];
    [watchList insertObject:[LanguageManager stringForKey:@"None"] atIndex:0];
    _v_DropDown_WatchList.menuTitles = watchList;
    _v_DropDown_WatchList.numberItemVisible = 6;

    _v_DropDown_WatchList.delegate = self;
    [_v_DropDown_WatchList reloadDropDown];
    
    _v_DropDown_FilterVolList.titleAlign =  NSTextAlignmentLeft;
    _v_DropDown_FilterVolList.dismissAutoResize = YES;
    _v_DropDown_FilterVolList.height = 28;
    _v_DropDown_FilterVolList.radius = 12;
//    _v_DropDown_FilterVolList.indicatorImage = [UIImage imageNamed:@"ic_dropDownDarkBlue"];
    _v_DropDown_FilterVolList.titleMenu = [LanguageManager stringForKey:@"Filter By"];
    _v_DropDown_FilterVolList.menuTitles = [filterByArray copy];
    _v_DropDown_FilterVolList.numberItemVisible = 6;
    
    _v_DropDown_FilterVolList.delegate = self;
    [_v_DropDown_FilterVolList reloadDropDown];
    
    [self setupColorForDropDowns];
    
}

- (void)setupColorForDropDowns{
    _v_DropDown_ExChangeList.tintIcon = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintExchangeDropDownButtonColor);
    _v_DropDown_ExChangeList.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgExChangeColor);
    _v_DropDown_ExChangeList.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
    _v_DropDown_ExChangeList.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgExChangeColor);
    _v_DropDown_ExChangeList.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
    
    _v_DropDown_WatchList.tintIcon = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintDropDownButtonColor);
    _v_DropDown_WatchList.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgWatchListColor);
    _v_DropDown_WatchList.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
    _v_DropDown_WatchList.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgWatchListColor);
    _v_DropDown_WatchList.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
    _v_DropDown_WatchList.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
    
    _v_DropDown_FilterVolList.tintIcon = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintDropDownButtonColor);
    _v_DropDown_FilterVolList.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgWatchListColor);
    _v_DropDown_FilterVolList.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
    _v_DropDown_FilterVolList.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgWatchListColor);
    _v_DropDown_FilterVolList.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
    _v_DropDown_FilterVolList.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
//    _v_DropDown_FilterVolList.tintIcon = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintDropDownButtonColor);
//    _v_DropDown_FilterVolList.selectedViewColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgFilterColor);
//    _v_DropDown_FilterVolList.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
//    _v_DropDown_FilterVolList.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textFilterColor);
//    _v_DropDown_FilterVolList.itemColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
}
- (void)changeTitleTextDropDown:(TCMenuDropDown *)dropDown isPlaceHolder:(BOOL)isPlaceHolder{
    if(!isPlaceHolder){
        dropDown.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textWatchListColor);
    }else{
        if(dropDown == _v_DropDown_ExChangeList){
            dropDown.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textExChangeColor);
        }else{
            dropDown.selectedLabelColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textItemDropDownColor);
        }
        
    }
    
}
#pragma mark MStockTrackerControllerDelegate
- (void)reloadContent{
    if(mode == MStockTrackerMode_Card){
        [self.v_CardTracker.clv_content reloadData];
        
    }else{
        [self.v_Tracker.leftSteamerView reloadData];
    }
}
- (void)reloadContent:(NSInteger)index{
    lastUpdatedIndex = index;
    if(controller.tickerArray.count > 0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->_tmpstatusView removeFromSuperview];
        });
        if(mode == MStockTrackerMode_Card){
            [self.v_CardTracker.clv_content reloadData];
            
        }else{
            [self.v_Tracker.leftSteamerView reloadData];
        }
    }else{
        NSString *content = @"No Data";
        if(index == -1){
            content = @"Please select one of the criteria above";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self->mode == MStockTrackerMode_Card){
                self->_tmpstatusView = [TCStatusInfoView new];
                [self->_tmpstatusView justShowStatus:[LanguageManager stringForKey:content] inView:self.v_CardTracker];
                
            }else{
                self->_tmpstatusView = [TCStatusInfoView new];
                [self->_tmpstatusView justShowStatus:[LanguageManager stringForKey:content] inView:self.v_Tracker];
            }
            
        });
    }
}

#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return controller.tickerArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StockStreamerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StockStreamerCell" forIndexPath:indexPath];
    [cell setupView:controller.tickerArray[indexPath.row] isAnimation:YES];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

#pragma mark CollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return controller.tickerArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    StockTrackerCell
    StockTrackerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"StockTrackerCell" forIndexPath:indexPath];
    cell.isShowBBHAndSBH = !self.isSteamerMode;
    [cell setupView:controller.tickerArray[indexPath.row] isAnimation:YES];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    int numberOfLines = ([self calculateMaxItems]/3);
    CGFloat heightForRow = (collectionView.frame.size.height - (numberOfLines-1)*kSPACING_ITEM)/numberOfLines;
    CGFloat widthForRow = collectionView.frame.size.width/3 - kSPACING_ITEM;
    if(self.isSteamerMode){
        heightForRow -= 70;
        if(heightForRow < widthForRow){
            heightForRow = widthForRow;
        }
    }
    return CGSizeMake(widthForRow, heightForRow);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return kSPACING_ITEM;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return kSPACING_ITEM;;
}

- (void)updateTheme:(NSNotification *)noti{
    [self setupColorForDropDowns];
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgColor);
    self.btn_Apply.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_bgApplyColor);
    [self.btn_Apply setTitleColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_textApplyColor) forState:UIControlStateNormal];
    
    [self.btn_Mode setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintActionButtonColor)];
    [self.btn_Direction setTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].stt_tintActionButtonColor)];
}
@end
