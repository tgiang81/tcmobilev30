//
//  MStockTrackerController.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol MStockTrackerControllerDelegate<NSObject>
- (void)reloadContent:(NSInteger)index;
@end

@interface MStockTrackerController : NSObject
@property(strong, nonatomic) NSMutableArray *tickerArray;
@property(strong, nonatomic) NSMutableArray *watchListArr;
@property(strong, nonatomic) NSMutableArray *exChangeArr;
@property(weak, nonatomic) id<MStockTrackerControllerDelegate> delegate;
@property(assign, nonatomic) BOOL isRightToLeft;
- (void)initValues;
- (void)registerListener;
- (void)removeListerner;
- (void)testStreamer;
- (void)setMaxItems:(int)maxItems;
- (NSMutableArray *)getWatchListNames;

- (void)applyFilter:(int)favID valueCount:(int)valueCount quantityCount:(int)quantityCount exch:(NSString *)exch;

- (void)changeDirection;
@end
