//
//  MStockTrackerModeVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
@class TCMenuDropDown;
@class StockStreamerCardModeView;
@class StockStreamerView;
typedef NS_ENUM(NSInteger, MStockTrackerMode) {
    MStockTrackerMode_Normal = 0,
    MStockTrackerMode_Card
};
@interface MStockTrackerModeVC : BaseVC

@property (weak, nonatomic) IBOutlet UIView *topInfoView;

@property (weak, nonatomic) IBOutlet StockStreamerView *v_Tracker;

@property (weak, nonatomic) IBOutlet StockStreamerCardModeView *v_CardTracker;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDown_WatchList;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDown_FilterVolList;

@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDown_ExChangeList;
@property (weak, nonatomic) IBOutlet UIButton *btn_Filter;
@property (weak, nonatomic) IBOutlet UIButton *btn_Direction;
@property (weak, nonatomic) IBOutlet UIButton *btn_Mode;
@property (weak, nonatomic) IBOutlet UIButton *btn_Apply;

@property (assign, nonatomic) BOOL isSteamerMode;
@end
