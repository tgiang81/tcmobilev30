//
//  MStockTrackerVC.h
//  TCiPad
//
//  Created by giangtu on 7/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class TCMenuDropDown;
typedef NS_ENUM(NSUInteger, StockTrackerFilterType) {
    StockTrackerFilterType_WatchList,
    StockTrackerFilterType_Filter
};
@interface MStockTrackerVC : BaseVC
{
    int favID;
    int arrayLimit;
    int test;
     IBOutlet UIView *containerView;
    IBOutlet UILabel *watchListLabel;
     IBOutlet UILabel *filterByLabel;
     IBOutlet UIButton *directionButton;
     IBOutlet UIButton *lineButton;
    
    
    
}
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDown_WatchList;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDown_ExChangeList;
@property (weak, nonatomic) IBOutlet UIButton *btn_filter;
@property (weak, nonatomic) IBOutlet UIButton *btn_watchList;
@property (weak, nonatomic) IBOutlet UIButton *btn_apply;

@end
