//
//  DetailStockVC.h
//  TCiPad
//
//  Created by Kaka on 6/8/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class TCBaseButton;
@class StockModel;
@class TCBaseView;
@class MarqueeLabel;
@class TCPageMenuControl;

@interface StockInfoVC : BaseVC{
	
}

@property (weak, nonatomic) IBOutlet UIView *topInfoView;

@property (weak, nonatomic) IBOutlet UIView *viewContentInfo;
//Detail
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCode;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblChangedValue;

@property (weak, nonatomic) IBOutlet UIImageView *imgUpDownPrice;
//SubInfo
@property (weak, nonatomic) IBOutlet UILabel *lblVol;
@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblVal;
@property (weak, nonatomic) IBOutlet UILabel *lblLo;
@property (weak, nonatomic) IBOutlet UILabel *lblHi;
@property (weak, nonatomic) IBOutlet UILabel *lblTrd;

//Content
@property (weak, nonatomic) IBOutlet UILabel *lblContentTitle;
@property (weak, nonatomic) IBOutlet TCPageMenuControl *filterContentView;
@property (weak, nonatomic) IBOutlet UIView *pageContentView;


//Passing data
@property (strong, nonatomic) StockModel *stock;
@end
