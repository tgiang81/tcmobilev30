//
//  MStockInfoVC.h
//  TC_Mobile30
//
//  Created by Kaka on 2/20/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"
@class StockModel;
@interface MStockInfoVC : BaseVC{
	
}
//OUTLET
@property (weak, nonatomic) IBOutlet UIView *topInfoView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

//Passing Data
@property (strong, nonatomic) StockModel *stock;

@end
