//
//  MInfoContentVC.h
//  TC_Mobile30
//
//  Created by Kaka on 2/20/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

typedef NS_ENUM (NSInteger, InfoItemType) {
	Analytic					= 771,
	Statistic					= 772,
	MarketDepth 				= 773,
	BusinessDone 				= 774,
	TimeAndSale					= 775
};

@class StockModel;
@interface InfoContentVC : BaseVC{
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

//Passing data
@property (strong, nonatomic) StockModel *stock;
@end
