//
//  MInfoContentVC.m
//  TC_Mobile30
//
//  Created by Kaka on 2/20/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "InfoContentVC.h"
#import "UIView+ShadowBorder.h"
#import "MInfoContentVC.h"
#import "MChartContentVC.h"
#import "MMarketDepthContentVC.h"
#import "MBusinessDoneContentVC.h"
#import "MTimeSaleContentVC.h"

@interface InfoContentVC ()<PageStockContentInfoVCDelegate>

@end

@implementation InfoContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self registerNotification];
	[self createUI];
	DLog(@"+++ InfoDidLoad");
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
	DLog(@"+++ Info Content didDisAppear");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	//Add Item to scroll View
	for (UIView *itemView in self.contentView.subviews) {
		[self populateContentItemToView:itemView];
	}
	[self.view setNeedsLayout];
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	self.view.backgroundColor = [UIColor clearColor];
	for (UIView *aView in self.contentView.subviews) {
		[aView makeBorderShadow];
	}
}

#pragma mark - Main
- (void)populateContentItemToView:(UIView *)containerView{
	InfoItemType itemType = containerView.tag;
	PageStockContentInfoVC *childVC;
	switch (itemType) {
		case Analytic:{
			childVC = NEW_VC_FROM_NIB([MChartContentVC class], [MChartContentVC nibName]);
		}
			break;
		case Statistic:{
			childVC = NEW_VC_FROM_NIB([MInfoContentVC class], [MInfoContentVC nibName]);
		}
			break;
		case MarketDepth:{
			childVC = NEW_VC_FROM_NIB([MMarketDepthContentVC class], [MMarketDepthContentVC nibName]);
		}
			break;
		case BusinessDone:{
			childVC = NEW_VC_FROM_NIB([MBusinessDoneContentVC class], [MBusinessDoneContentVC nibName]);
		}
			break;
		case TimeAndSale:{
			childVC = NEW_VC_FROM_NIB([MTimeSaleContentVC class], [MTimeSaleContentVC nibName]);
		}
			break;
			
		default:
			break;
	}
	if (childVC) {
		childVC.tagObject = containerView;
		childVC.stock = self.stock;
		childVC.fullMode = NO;
		childVC.delegate = self;
		childVC.view.layer.masksToBounds = YES;
		[self addChildVC:childVC toView:containerView];
	}
}

- (void)addChildVC:(UIViewController *)childVC toView:(UIView *)containerView{
	[self addChildViewController:childVC];
	childVC.view.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
	[containerView addSubview:childVC.view];
	[childVC didMoveToParentViewController:self];
}

- (void)removeChildVC:(UIViewController *)childVC{
	[childVC willMoveToParentViewController:nil];
	[childVC removeFromParentViewController];
	[childVC.view removeFromSuperview];
}

#pragma mark - Register Notification
- (void)registerNotification{
	[self addNotification:@"DoLess" selector:@selector(doLessNotification:)];
}
#pragma mark - Action
//Do something here...

#pragma mark - UTILS
- (NSLayoutConstraint *)getConstrainHeightFrom:(UIView *)view{
	NSLayoutConstraint *heightConstraint;
	for (NSLayoutConstraint *constraint in view.constraints) {
		if (constraint.firstAttribute == NSLayoutAttributeHeight) {
			heightConstraint = constraint;
			break;
		}
	}
	return heightConstraint;
}

#pragma mark - PageStockContentInfoVCDelegate
- (void)shouldSwitchViewModeFrom:(BOOL)fullMode forController:(PageStockContentInfoVC *)controller{
	if (!fullMode) {
		//Do full View
		//UIView *itemView = controller.view.superview;
		//NSLayoutConstraint *itemConstrainHeight = [self getConstrainHeightFrom:itemView];
		controller.delegate = nil;
		//[self removeChildVC:controller];
		[self postNotification:@"DoFull" object:controller];
	}
}
#pragma mark - Handle Notification
- (void)doLessNotification:(NSNotification *)noti{
	PageStockContentInfoVC *childVC = (PageStockContentInfoVC *)noti.object;
	UIView *containerView = (UIView *)childVC.tagObject;
	childVC.fullMode = NO;
	childVC.delegate = self;
	[childVC.view removeBorderShadow];
	[self addChildVC:childVC toView:containerView];
}
@end
