//
//  MStockInfoVC.m
//  TC_Mobile30
//
//  Created by Kaka on 2/20/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MStockInfoVC.h"
#import "UIView+ShadowBorder.h"
#import "CAPSPageMenu.h"
#import "InfoContentVC.h"
#import "MarqueeLabel.h"
#import "NSNumber+Formatter.h"
#import "PageStockContentInfoVC.h"
#import "UIView+Animation.h"
#import "MFundamentalsContentVC.h"
#import "MStockNewsContentVC.h"
#import "ElasticNewsData.h"
#import "GHContextMenuView.h"
#import "MTradeViewController.h"
#import "BaseNavigationController.h"
#import "SelectWatchlistPopupVC.h"
#import "LanguageKey.h"
#import "MSearchVC.h"

@interface MStockInfoVC ()<PageStockContentInfoVCDelegate, GHContextOverlayViewDelegate, GHContextOverlayViewDataSource, SelectWatchlistPopupVCDelegate>{
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockName;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblStockCode;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDonePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblChangedValue;

//CONTROLS
@property (strong, nonatomic) CAPSPageMenu *pageMenu;
@end

@implementation MStockInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[self createUI];
	[self loadData];
}


- (void)viewWillLayoutSubviews{
	[super viewWillLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self registerNotification];
}
- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
	[self removeAllNotification];
	//self.pageMenu = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	//Nav Bar
	[self setBackButtonDefault];
	[self createCustomBar:BarItemType_Right icon:@"search_icon" selector:@selector(onSearchAction:)];
	//UI
	[self.topInfoView makeBorderShadow];
	self.view.backgroundColor = TC_COLOR_FROM_HEX(@"F4F4F4");
	[self setupContextMenu];
    [self setupLanguage];
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
}
-(void)setupLanguage{
    self.title=[LanguageManager stringForKey:Stock_Info];
}
- (void)setupContextMenu{
	GHContextMenuView* overlay = [[GHContextMenuView alloc] init];
	overlay.dataSource = self;
	overlay.delegate = self;
	
	UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];
	[self.view addGestureRecognizer:_longPressRecognizer];
}

- (void)setupPageMenuContentInfo{
	// Array to keep track of controllers in page menu
	NSMutableArray *controllerArray = @[].mutableCopy;
	
	// Create variables for all view controllers you want to put in the
	// page menu, initialize them, and add each to the controller array.
	// (Can be any UIViewController subclass)
	InfoContentVC *info = NEW_VC_FROM_STORYBOARD(kStockInfoStoryboardName, [InfoContentVC storyboardID]);
    info.title =[LanguageManager stringForKey:Info];
	info.stock = self.stock;
	[controllerArray addObject:info];
	
	if ([self shouldShowFundamentals] || ![self shouldShowFundamentals]) { //Fake alway show Fundamental
		MFundamentalsContentVC *fundamentals = NEW_VC_FROM_NIB([MFundamentalsContentVC class], [MFundamentalsContentVC nibName]);
		fundamentals.title =[LanguageManager stringForKey:Fundamentals];
		fundamentals.stock = self.stock;
		fundamentals.supperVC = self;
		[controllerArray addObject:fundamentals];
	}
	
	MStockNewsContentVC *news = NEW_VC_FROM_NIB([MStockNewsContentVC class], [MStockNewsContentVC nibName]);
	news.title =[LanguageManager stringForKey:_News];
	news.isLoadingNews = YES;
	news.stock = self.stock;
	news.supperVC = self;
	[controllerArray addObject:news];
	
	if ([self isAvailableResearchReport] || ![self isAvailableResearchReport]) { //Fake to show ||
		MStockNewsContentVC *news = NEW_VC_FROM_NIB([MStockNewsContentVC class], [MStockNewsContentVC nibName]);
        news.title = [LanguageManager stringForKey:Research];
		news.isLoadingNews = NO;
		news.stock = self.stock;
		news.supperVC = self;
		[controllerArray addObject:news];
	}
	// Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
	NSDictionary *parameters = @{
								 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
								 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
								 CAPSPageMenuOptionMenuItemFont: AppFont_MainFontMediumWithSize(16),
								 CAPSPageMenuOptionSelectedMenuItemLabelColor: [UIColor blackColor],
								 CAPSPageMenuOptionUnselectedMenuItemLabelColor: TC_COLOR_FROM_HEX(@"888888"),
								 CAPSPageMenuOptionSelectionIndicatorHeight:@(0.0),
								 CAPSPageMenuOptionViewBackgroundColor: [UIColor clearColor],
								 CAPSPageMenuOptionCenterMenuItems: @(NO),
								 CAPSPageMenuOptionMenuItemWidthBasedOnTitleTextWidth: @(YES),
								 CAPSPageMenuOptionMenuHeight: @(30.0),
								 CAPSPageMenuOptionMenuMargin: @(20),
								 CAPSPageMenuOptionEnableHorizontalBounce: @(YES)
								 };

	// Initialize page menu with controller array, frame, and optional parameters
	_pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height) options:parameters];
	// Lastly add page menu as subview of base view controller view
	// or use pageMenu controller in you view hierachy as desired
	[_pageMenu.menuScrollView makeBorderShadow];
	[self.contentView addSubview:_pageMenu.view];
}

#pragma mark - LoadData
- (void)loadData{
	[self infoStockFrom:self.stock];
	[self setupPageMenuContentInfo];
}
- (void)infoStockFrom:(StockModel *)model{
	if (model) {
		//Parse value
		if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
			_lblStockCode.text = model.stockName;
			_lblStockName.text = model.compName ;
		}else{
			_lblStockCode.text = [model.stockCode stringByDeletingPathExtension];
			_lblStockName.text = model.stockName;
		}
		_lblLastDonePrice.text = [model.lastDonePrice toCurrencyNumber];
		//Value Change
		NSString *priceChanged = [model.fChange toChangedStringWithSign];
		NSString *percentChanged = [model.changePer toPercentWithSign];
		_lblChangedValue.text = [NSString stringWithFormat:@"%@ (%@)", priceChanged, percentChanged];
		UIColor *changedColor = (model.changePer.doubleValue > 0)? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
		_lblLastDonePrice.textColor = changedColor;
		_lblChangedValue.textColor = changedColor;
	}
}

#pragma mark - ACTIONS
- (void)onSearchAction:(id)sender{
	MSearchVC *searchVC = NEW_VC_FROM_STORYBOARD(kMSearchStoryboardName, [MSearchVC storyboardID]);
	searchVC.searchType = MSearchType_ViewDetail;
	[self nextTo:searchVC animate:YES];
}

#pragma mark - UTIL VC
//Check should show/hide Fundamentals
- (BOOL)shouldShowFundamentals{
	ExchangeData *_curED;
	for (ExchangeData *ed in [[UserPrefConstants singleton] userExchagelist]) {
		if ([ed.feed_exchg_code isEqualToString:[[UserPrefConstants singleton] userCurrentExchange]]) {
			_curED = ed;
			break;
		}
	}
	if (!_curED) {
		return NO;
	}
	if (_curED.subsCPIQAnnounce|| _curED.subsCPIQCompInfo|| _curED.subsCPIQCompSynp
		|| _curED.subsCPIQCompKeyPer || _curED.subsCPIQShrHoldSum|| _curED.subsCPIQFinancialRep) {
		return YES;
	}
	return NO;
}

//******** Research Report ************
- (BOOL)isAvailableResearchReport{
	BOOL isAvailable = NO;
	if ([UserPrefConstants singleton].elasticCat.count > 0) {
		NSMutableArray *tempSourceArray = [[NSMutableArray alloc] init];
		NSMutableArray *_researchArr = @[].mutableCopy;
		for (ElasticNewsData *source in [UserPrefConstants singleton].elasticCat) {
			[tempSourceArray addObject:source.sourceString];
			NSString *stringInformation = [NSString stringWithFormat:@"%@|%@|%@|%@",source.sourceString,source.categoryLevel,source.categoryString,source.newsID];
			if ([source.sourceString.uppercaseString isEqualToString:@"CIMB"] || [source.sourceString.uppercaseString isEqualToString:@"HSC"]) {
				[_researchArr addObject:stringInformation];
			}
		}
		//Check if need call api
		if (_researchArr.count > 0) {
			isAvailable = YES;
		}
	}
	return isAvailable;
}
#pragma mark - Register Notification
- (void)registerNotification{
	[self addNotification:@"DoFull" selector:@selector(doFullNotification:)];
	//Update new data
	[self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
}

#pragma mark - GHContextMenuDataSource
- (NSInteger) numberOfMenuItems{
	return [[AppConstants contextInfoMenu] count];
}

- (UIImage*)imageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextInfoMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}

- (UIImage*) highlightedImageForItemAtIndex:(NSInteger)index{
	NSArray *menus = [AppConstants contextInfoMenu];
	MContextMenu menuType = [menus[index] integerValue];
	NSString *imageName = nil;
	switch (menuType) {
		case MContextMenu_Trading:
			imageName = @"trade_hl_menu";
			break;
		case MContextMenu_Buy:
			imageName = @"buy_hl_menu";
			break;
		case MContextMenu_Sell:
			imageName = @"sell_hl_menu";
			break;
		case MContextMenu_Info:
			imageName = @"info_hl_menu";
			break;
		case MContextMenu_Favorite:
			imageName = @"favorite_hl_menu";
			break;
		default:
			break;
	}
	return [UIImage imageNamed:imageName];
}

#pragma mark - GHContextMenuDelegate
- (void)didSelectItemAtIndex:(NSInteger) selectedIndex forMenuAtPoint:(CGPoint)point{
	NSArray *menus = [AppConstants contextInfoMenu];
	MContextMenu menuType = [menus[selectedIndex] integerValue];
	
	//Select Stock Model
	StockModel *selectedStock = self.stock;
	switch (menuType) {
		case MContextMenu_Trading:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Buy:{
			//BUY
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Buy;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Sell:{
			//Sell here
			[UserPrefConstants singleton].userSelectingThisStock = selectedStock;
			MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
			controller.tradeType = TradeType_Sell;
			controller.actionTypeDict = @{};
			controller.title = [LanguageManager stringForKey:@"Home"];
			controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
			controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
			
			BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
			
			[AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
			}];
		}
			break;
		case MContextMenu_Favorite:{
			//Add favorite here
			SelectWatchlistPopupVC *_selectVC = NEW_VC_FROM_NIB([SelectWatchlistPopupVC class], [SelectWatchlistPopupVC nibName]);
			_selectVC.stock = selectedStock;
			_selectVC.delegate = self;
			
			[self showPopupWithContent:_selectVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.4 * SCREEN_HEIGHT_PORTRAIT) showInCenter:NO topInsect:80 completion:^(MZFormSheetController *formSheetController) {
				//Do something here
			}];
			/*
			 [self showPopupWithContent:_selectVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.4 * SCREEN_HEIGHT_PORTRAIT) completion:^(MZFormSheetController *formSheetController) {
			 
			 }];
			 */
		}
			break;
		default:
			break;
	}
	DLog(@"Did select Menu Item: %@", @(selectedIndex));
}


#pragma mark - PageStockContentInfoVCDelegate
- (void)shouldSwitchViewModeFrom:(BOOL)fullMode forController:(PageStockContentInfoVC *)controller{
	if (controller.fullMode) {
		//*******Do minimum ************
		//Show PageMenu first
		[self.pageMenu.view setHidden:NO];
		//Prepare Child Content
		UIView *containerView = (UIView *)controller.tagObject;
		if (!containerView) {
			return;
		}
		CGRect minimumRect = [containerView convertRect:containerView.bounds toView:self.contentView];
		float duration = self.contentView.frame.size.height / containerView.frame.size.height * 0.155 + 0.2;
		
		[UIView animateWithDuration:duration delay:0.0 usingSpringWithDamping:0.65 initialSpringVelocity:0.3 options:0 animations:^{
			controller.view.frame = minimumRect;
		} completion:^(BOOL finished) {
			if (finished) {
				//Add ChildView
				controller.delegate = nil;
				//[controller willMoveToParentViewController:nil];
				//[controller.view removeFromSuperview];
				//[controller removeFromParentViewController];
				[self postNotification:@"DoLess" object:controller];
			}
		}];
	}
}
#pragma mark - Handle Notification
- (void)doFullNotification:(NSNotification *)noti{
	//Get info and do full
	PageStockContentInfoVC *childVC = (PageStockContentInfoVC *)noti.object;
	UIView *itemView = (UIView *)childVC.tagObject;
	if (!itemView) {
		return;
	}
	//Prepare Frame
	CGRect itemRect = [itemView convertRect:itemView.bounds toView:self.contentView];
	itemRect = CGRectMake(itemRect.origin.x + 4, itemRect.origin.y, itemRect.size.width - 8, itemRect.size.height);
	childVC.view.frame = itemRect;
	[self.contentView addSubview:childVC.view];
	[self.contentView bringSubviewToFront:childVC.view];
	CGRect newRect = CGRectMake(8, 0, self.contentView.frame.size.width - 16, self.contentView.frame.size.height - 8);
	float duration = self.contentView.frame.size.height / itemView.frame.size.height * 0.155 + 0.2;
	
	[UIView animateWithDuration:duration delay:0.0 usingSpringWithDamping:0.65 initialSpringVelocity:0.3 options:0 animations:^{
		childVC.view.frame = newRect;
	} completion:^(BOOL finished) {
		if (finished) {
			//Add ChildView
			childVC.delegate = self;
			[self addChildViewController:childVC];
			[childVC didMoveToParentViewController:self];
			//Hide pageMenu first
			[self.pageMenu.view setHidden:YES];
			//Maybe not need
			[childVC.view makeBorderShadow];
			childVC.fullMode = YES;
			[childVC.view layoutIfNeeded];
		}
	}];
}

//UPDATE NEW
- (void)didUpdate:(NSNotification *)noti{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSString *stockCode = noti.object;
		StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
		if (!changedStock) {
			return; //do nothing
		}
		if ([changedStock.stockCode isEqualToString:self.stock.stockCode]) {
			dispatch_async(dispatch_get_main_queue(), ^{
				//Update animate top info
				self.stock = changedStock;
				__weak MStockInfoVC *weakSelf = self;
				[self.topInfoView makeAnimateBy:self.stock newStock:changedStock completion:^{
					[weakSelf infoStockFrom:weakSelf.stock];
					weakSelf.topInfoView.backgroundColor = [UIColor whiteColor];
				}];
				//Post notification to update content info
				[self postNotification:kDidUpdateStockNotification object:changedStock];
			});
		}
	});
}

@end
