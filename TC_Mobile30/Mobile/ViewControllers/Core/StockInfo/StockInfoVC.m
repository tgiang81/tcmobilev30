//
//  DetailStockVC.m
//  TCiPad
//
//  Created by Kaka on 6/8/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockInfoVC.h"
#import "TCBaseView.h"

#import "StockModel.h"

#import "PageStockContentInfoVC.h"
#import "MChartContentVC.h"
#import "MMarketDepthContentVC.h"
#import "MTimeSaleContentVC.h"
#import "MBusinessDoneContentVC.h"
#import "MStockNewsContentVC.h"
#import "MInfoContentVC.h"
#import "MFundamentalsContentVC.h"
#import "MResearchContentVC.h"


#import "UIView+Animation.h"
#import "NSNumber+Formatter.h"
#import "UILabel+FormattedText.h"
#import "MarqueeLabel.h"
#import "UIView+ShadowBorder.h"

#import "MTradeViewController.h"
#import "TradingRules.h"

#import "TCPageMenuControl.h"
#import "MZFormSheetController.h"
#import "SelectWatchlistPopupVC.h"
#import "EditingAlertVC.h"
#import "AddingAlertVC.h"
#import "StatusAlertVC.h"

#import "NSString+SizeOfString.h"
#import "UIImageView+TCUtil.h"
#import "ElasticNewsData.h"
#import "AlertController.h"
#import "VertxConnectionManager.h"
#import "LanguageKey.h"
//PopUp Stock More Info
typedef enum: NSInteger {
	PageContentType_Unknow = -1,
	PageContentType_Info,
	PageContentType_MarketDepth,
	PageContentType_Chart,
	PageContentType_Fundamentals,
	PageContentType_BusinessDone,
	PageContentType_TimeSales,
	PageContentType_News,
	PageContentType_Research
} PageContentType;

#define kHEIGHT_CONSTRAIN_LABEL_MAX	40
#define kHEIGHT_CONSTRAIN_LABEL_MIN	16

@interface StockInfoVC ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate, TCControlViewDelegate, SelectWatchlistPopupVCDelegate, EditingAlertVCDelegate>{
	NSMutableArray *_contents;
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Volume;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Value;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Hight;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Open;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Low;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Trade;

@property (strong, nonatomic) UIPageViewController *pageViewController;

//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstrainNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstrainCodeLabel;

@end

@implementation StockInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [UserPrefConstants singleton].userSelectingThisStock = _stock;
	//+++ init page content
	if ([self shouldShowFundamentals]) {
		_contents = @[@(PageContentType_Info), @(PageContentType_MarketDepth), @(PageContentType_Chart), @(PageContentType_Fundamentals), @(PageContentType_BusinessDone), @(PageContentType_TimeSales), @(PageContentType_News)].mutableCopy;
	}else{
		_contents = @[@(PageContentType_Info), @(PageContentType_MarketDepth), @(PageContentType_Chart), @(PageContentType_BusinessDone), @(PageContentType_TimeSales), @(PageContentType_News)].mutableCopy;
	}
	if ([self isAvailableResearchReport]) {
		[_contents addObject:@(PageContentType_Research)];
	}
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)viewWillLayoutSubviews{
	[super viewWillLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	//1: BarItem Menu
	[self setupBarItems];
	//2: Control View
	[self setupControlView];
	//3: Create Content Info
	[self setupPageViewControlller];
	[self updateTheme:nil];
}
- (void)setupLanguage{
	self.customBarTitle = [LanguageManager stringForKey:Stock_Info];
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	_pageContentView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	self.topInfoView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	[self.topInfoView dropShadow];
	//[self.viewContentInfo makeBorderShadow];
	[self.view bringSubviewToFront:self.topView];
	/*
	self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	_viewTopInfo.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgTopDetailColor);
	_pageContentView.backgroundColor = [UIColor clearColor];//COLOR_FROM_HEX(0x364D59);
	
	_lblCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblPrice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblVol.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblVal.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblOpen.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblHi.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblLo.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblTrd.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblChangedValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);;
	_lblPrice.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	
	//INS
	_lblIns_Volume.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblIns_Value.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblIns_Hight.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblIns_Low.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblIns_Open.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	_lblIns_Trade.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
	 */
}

//+++ setup Item Bar
- (void)setupBarItems{
	[self setBackButtonDefault];
	
	NSMutableArray *_rightBarItems = @[].mutableCopy;
	//UIImage *shareImage = [UIImage imageNamed:@"share_icon"];
	//CGRect shareFrame = CGRectMake(0, 0, shareImage.size.width, shareImage.size.height);
	//UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeSystem];
	//shareButton.frame = shareFrame;
	//[shareButton setImage:shareImage forState:UIControlStateNormal];
	//[shareButton addTarget:self action:@selector(onShareAction:) forControlEvents:UIControlEventTouchUpInside];
	//shareButton.imageView.tintColor = AppColor_TintColor;
	//UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
	//shareItem.tintColor = AppColor_TintColor;
	//[_rightBarItems addObject:shareItem];
	
	//UIBarButtonItem *fixedSpace1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
	//fixedSpace1.width = 15;
	//[_rightBarItems addObject:fixedSpace1];
	
	UIImage *alertImage = [UIImage imageNamed:@"alert_small_icon"];
	CGRect alertFrame = CGRectMake(0, 0, alertImage.size.width, alertImage.size.height);
	UIButton *alertButton = [UIButton buttonWithType:UIButtonTypeSystem];
	alertButton.frame = alertFrame;
	[alertButton setImage:alertImage forState:UIControlStateNormal];
	[alertButton addTarget:self action:@selector(onAlertAction:)
		  forControlEvents:UIControlEventTouchUpInside];
	alertButton.imageView.tintColor = AppColor_TintColor;
	UIBarButtonItem *alertItem = [[UIBarButtonItem alloc] initWithCustomView:alertButton];
	alertItem.tintColor = AppColor_TintColor;
	
	[_rightBarItems addObject:alertItem];
	
	UIBarButtonItem *fixedSpace2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
	fixedSpace2.width = 15;
	[_rightBarItems addObject:fixedSpace2];
	
	UIImage *favoriteImage = [UIImage imageNamed:@"favorite_icon"];
	CGRect favoriteFrame = CGRectMake(0, 0, favoriteImage.size.width, favoriteImage.size.height);
	UIButton *favoriteButton = [UIButton buttonWithType:UIButtonTypeSystem];
	favoriteButton.frame = favoriteFrame;
	[favoriteButton setImage:favoriteImage forState:UIControlStateNormal];
	[favoriteButton addTarget:self action:@selector(onFavoriteAction:)
			 forControlEvents:UIControlEventTouchUpInside];
	favoriteButton.imageView.tintColor = AppColor_TintColor;
	UIBarButtonItem *favoriteItem = [[UIBarButtonItem alloc] initWithCustomView:favoriteButton];
	[_rightBarItems addObject:favoriteItem];
	self.navigationItem.rightBarButtonItems = _rightBarItems;
}
//Create Control Content Filter
- (void)setupControlView{
	_filterContentView.itemWidth = 130;
	_filterContentView.dataSources = [self contentArrayFromTypes:_contents];

	_filterContentView.delegate = self;
	[_filterContentView startLoadingPageMenu];
	//Data
	_lblContentTitle.text = _filterContentView.dataSources[0];
}
- (void)setupPageViewControlller{
	[self.view layoutIfNeeded];
	// Create page view controller
	self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
	self.pageViewController.dataSource = self;
	self.pageViewController.delegate = self;
	PageStockContentInfoVC *startingViewController = [self viewControllerAtIndex:0];
	NSArray *viewControllers = @[startingViewController];
	[self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
	
	// Change the size of page view controller
	self.pageViewController.view.frame = CGRectMake(0, 0, _pageContentView.bounds.size.width, _pageContentView.bounds.size.height);
	[self addChildViewController:_pageViewController];
	[_pageContentView addSubview:_pageViewController.view];
	[self.pageViewController didMoveToParentViewController:self];
	//Disable swipe gesture
	//[self disableScrollInPageContent];
}

#pragma mark - LoadData
- (void)loadData{
	[self infoStockFrom:_stock];
}

- (void)infoStockFrom:(StockModel *)model{
	if (model) {
		//Parse value
		[self setStockName:model.stockName code:model.stockCode];
		
		[self setPercentChangePrice:model.changePer];
		_lblPrice.text = [NSString stringWithFormat:@"%@", [model.lastDonePrice toCurrencyNumber]];
		
		//Sortcut
		[self setVol:[model.volume abbreviateNumber]];
		[self setVal:[model.dValue abbreviateNumber]];
		[self setTrd:[model.iTrd abbreviateNumber]];
		//Group
		[self setHi:[model.highPrice toCurrencyNumber]];
		[self setLo:[model.lowPrice toCurrencyNumber]];
		[self setOpen:[model.openPrice toCurrencyNumber]];
	}
}

#pragma mark - Show POPUP

#pragma mark - Availabel Reports?
- (BOOL)isAvailableResearchReport{
	BOOL isAvailable = NO;
	if ([UserPrefConstants singleton].elasticCat.count > 0) {
		NSMutableArray *tempSourceArray = [[NSMutableArray alloc] init];
		NSMutableArray *_researchArr = @[].mutableCopy;
		for (ElasticNewsData *source in [UserPrefConstants singleton].elasticCat) {
			[tempSourceArray addObject:source.sourceString];
			NSString *stringInformation = [NSString stringWithFormat:@"%@|%@|%@|%@",source.sourceString,source.categoryLevel,source.categoryString,source.newsID];
			if ([source.sourceString.uppercaseString isEqualToString:@"CIMB"] || [source.sourceString.uppercaseString isEqualToString:@"HSC"]) {
				[_researchArr addObject:stringInformation];
			}
		}
		//Check if need call api
		if (_researchArr.count > 0) {
			isAvailable = YES;
		}
	}
	return isAvailable;
}
#pragma mark - UTILS
- (void)setStockName:(NSString *)name code:(NSString *)code{
	_lblName.text = name;
	_lblCode.text = [code stringByDeletingPathExtension];
	
	//Get height for Name
	float nameHeight = [_lblName.text heightForWidth:_lblName.frame.size.width usingFont:_lblName.font];
	if (nameHeight > kHEIGHT_CONSTRAIN_LABEL_MAX) {
		nameHeight = kHEIGHT_CONSTRAIN_LABEL_MAX;
	}
	if (nameHeight < kHEIGHT_CONSTRAIN_LABEL_MIN) {
		nameHeight = kHEIGHT_CONSTRAIN_LABEL_MIN;
	}
	_heightConstrainNameLabel.constant = nameHeight;
	//Get height for Comp
	float compHeight = [_lblCode.text heightForWidth:_lblCode.frame.size.width usingFont:_lblCode.font];
	if (compHeight > kHEIGHT_CONSTRAIN_LABEL_MAX) {
		compHeight = kHEIGHT_CONSTRAIN_LABEL_MAX;
	}
	if (compHeight < kHEIGHT_CONSTRAIN_LABEL_MIN) {
		compHeight = kHEIGHT_CONSTRAIN_LABEL_MIN;
	}
	_heightConstrainCodeLabel.constant = compHeight;
	[self.view layoutIfNeeded];
}
//Check should show/hide Fundamentals
- (BOOL)shouldShowFundamentals{
	ExchangeData *_curED;
	for (ExchangeData *ed in [[UserPrefConstants singleton] userExchagelist]) {
		if ([ed.feed_exchg_code isEqualToString:[[UserPrefConstants singleton] userCurrentExchange]]) {
			_curED = ed;
			break;
		}
	}
	if (!_curED) {
		return NO;
	}
	if (_curED.subsCPIQAnnounce|| _curED.subsCPIQCompInfo|| _curED.subsCPIQCompSynp
		|| _curED.subsCPIQCompKeyPer || _curED.subsCPIQShrHoldSum|| _curED.subsCPIQFinancialRep) {
		return YES;
	}
	return NO;
}
//Logic for content type on PageControl
- (NSString *)stringFromContentType:(PageContentType)type{
	NSString *strContent = @"";
	switch (type) {
		case PageContentType_Info:
			strContent = [LanguageManager stringForKey:Stock_Info];
			break;
		case PageContentType_MarketDepth:
			strContent = [LanguageManager stringForKey:Market_Depth];
			break;
		case PageContentType_Chart:
			strContent = [LanguageManager stringForKey:Chart];
			break;
		case PageContentType_Fundamentals:
			strContent = [LanguageManager stringForKey:Fundamentals];
			break;
		case PageContentType_BusinessDone:
			strContent = [LanguageManager stringForKey:Business_Done];
			break;
		case PageContentType_TimeSales:
			strContent = [LanguageManager stringForKey:Time___Sales];
			break;
		case PageContentType_News:
			strContent = [LanguageManager stringForKey:_News];
			break;
		case PageContentType_Research:
			strContent = [LanguageManager stringForKey:Research];
			break;
			
		default:
			break;
	}
	return strContent;
}

- (NSArray *)contentArrayFromTypes:(NSArray *)contentTypes{
	NSMutableArray *contents = @[].mutableCopy;
	for (int i = 0; i < contentTypes.count; i ++) {
		PageContentType type = [contentTypes[i] integerValue];
		[contents addObject:[self stringFromContentType:type]];
	}
	return [contents copy];
}

//+++ END Logic.....
- (void)disableScrollInPageContent{
	for (UIScrollView *view in self.pageViewController.view.subviews) {
		if ([view isKindOfClass:[UIScrollView class]]) {
			view.scrollEnabled = NO;
		}
	}
}
//Notification
- (void)registerNotification{
	//Update new data
	[self addNotification:kVertxNo_didUpdate selector:@selector(didUpdate:)];
    [self addNotification:kNotiQuote selector:@selector(closeStockInfoVC:)];
}

#pragma mark - ViewController At Index <For UIPageViewController>
//+++ For content Page View Controller
- (PageStockContentInfoVC *)viewControllerAtIndex:(NSUInteger)index{
	if (index >= _contents.count) {
		return nil;
	}
	PageStockContentInfoVC *controller;
	PageContentType typeContent = [_contents[index] integerValue];
	switch (typeContent) {
		case PageContentType_Info:
			controller = NEW_VC_FROM_NIB([MInfoContentVC class], [MInfoContentVC nibName]);
			break;
		case PageContentType_Chart:
			controller = NEW_VC_FROM_NIB([MChartContentVC class], [MChartContentVC nibName]);
			break;
		case PageContentType_Fundamentals:
			controller = NEW_VC_FROM_NIB([MFundamentalsContentVC class], [MFundamentalsContentVC nibName]);
			break;
		case PageContentType_MarketDepth:
			controller = NEW_VC_FROM_NIB([MMarketDepthContentVC class], [MMarketDepthContentVC nibName]);
			break;
		case PageContentType_TimeSales:
			controller = NEW_VC_FROM_NIB([MTimeSaleContentVC class], [MTimeSaleContentVC nibName]);
			break;
		case PageContentType_BusinessDone:
			controller = NEW_VC_FROM_NIB([MBusinessDoneContentVC class], [MBusinessDoneContentVC nibName]);
			break;
		case PageContentType_News:{
			MStockNewsContentVC *newVC = NEW_VC_FROM_NIB([MStockNewsContentVC class], [MStockNewsContentVC nibName]);
			newVC.isLoadingNews = YES;
			newVC.pageIndex = index;
			newVC.stock = _stock;
			newVC.supperVC = self;
			return newVC;
		}
			break;
		case PageContentType_Research:{
			MStockNewsContentVC *newVC = NEW_VC_FROM_NIB([MStockNewsContentVC class], [MStockNewsContentVC nibName]);
			newVC.isLoadingNews = NO;
			newVC.pageIndex = index;
			newVC.stock = _stock;
			newVC.supperVC = self;
			return newVC;
		}
			break;
		default:{
			controller = [[PageStockContentInfoVC alloc] init];
		}
			break;
	}
	controller.pageIndex = index;
	controller.stock = _stock;
	controller.supperVC = self;
	return controller;
}

- (void)slideToPage:(NSInteger)page{
	if (page >= _contents.count) {
		return;
	}
	PageStockContentInfoVC *_contentVC = [self viewControllerAtIndex:page];
	[_pageViewController setViewControllers:@[_contentVC] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

//Data info stock
//====== Set Data ===========
- (void)setPercentChangePrice:(NSNumber *)fChangePer{
	if (fChangePer.doubleValue >= 0) {
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Up_Price];
			[_imgUpDownPrice toColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
		_lblPrice.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
		_lblChangedValue.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
	} else{
		_imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Down_Price];
		[_imgUpDownPrice toColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
		_lblPrice.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
		_lblChangedValue.textColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	}
	
	NSString *priceChanged = [_stock.fChange toChangedStringWithSign];
	NSString *percentChanged = [_stock.changePer toPercentWithSign];
	_lblChangedValue.text = [NSString stringWithFormat:@"%@ (%@)", priceChanged, percentChanged];
	//_lblChangedValue.text = [NSString stringWithFormat:@"(%@)", percentChanged];
}

- (void)setVol:(NSString *)vol{
	_lblVol.text = vol;
}

- (void)setVal:(NSString *)val{
	_lblVal.text = val;
}

- (void)setOpen:(NSString *)open{
	_lblOpen.text = open;
}

- (void)setHi:(NSString *)hi{
	_lblHi.text = hi;
}

- (void)setLo:(NSString *)lo{
	_lblLo.text = lo;
}

- (void)setTrd:(NSString *)trd{
	_lblTrd.text = trd;
}

#pragma mark EditingAlertVCDelegate
- (void)didSelectClose:(NSIndexPath *)index{
    [self dismissPopup:YES completion:nil];
}
- (void)didSelectApply:(NSIndexPath *)index alert:(AlertModel *)alert isNew:(BOOL)isNew{
    [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
        [AlertController addAlert:alert completeHandler:^(BOOL success, id responseObject, NSError *error) {
            if(!error){
                [self showStatus: [LanguageManager stringForKey:@"Alert for this stock has been made. You can manage your alerts at the settings page"]];
            }else{
                [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile]  message:error.localizedDescription];
            }
        }];
    }];
}
- (void)showStatus:(NSString *)content{
//    StatusAlertVC *status = NEW_VC_FROM_NIB([StatusAlertVC class], @"StatusAlertVC");
//    status.message = content;
//    CGSize contentSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 350);
//    [self showPopupWithContent:status inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
//    }];
}
#pragma mark - Actions
- (void)onShareAction:(id)sender{
	[self showCustomAlertWithMessage:@"Coming soon..."];
}

- (void)onAlertAction:(id)sender{
    [self showAddAlertVC];
}
- (void)showAddAlertVC{
    AddingAlertVC *newAlertVC = NEW_VC_FROM_NIB([AddingAlertVC class], @"AddingAlertVC");
    newAlertVC.delegate = self;
    newAlertVC.isNew = YES;
    newAlertVC.enableSearch = NO;
    AlertModel *newModel = [[AlertModel alloc] init];
    newModel.stkCd = self.stock.stockCode;
    newModel.stkNm = self.stock.stockName;
    newModel.cdt = @"1";
    newModel.s = @"A";
    newAlertVC.alertModel = newModel;
    CGSize contentSize = CGSizeMake(0.9 * SCREEN_WIDTH_PORTRAIT, 0.8 * SCREEN_HEIGHT_PORTRAIT);
    
    [self showPopupWithContent:newAlertVC inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
    }];
}
- (void)onFavoriteAction:(id)sender{
	SelectWatchlistPopupVC *_selectVC = NEW_VC_FROM_NIB([SelectWatchlistPopupVC class], [SelectWatchlistPopupVC nibName]);
	_selectVC.delegate = self;
	[self showPopupWithContent:_selectVC inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.4 * SCREEN_HEIGHT_PORTRAIT) completion:^(MZFormSheetController *formSheetController) {
		
	}];
}
//+++ Not use this now
/*
-(void)executeTrade:(id)sender {

    NSArray *stkCodeAndExchangeArr= [_stock.stockCode componentsSeparatedByString:@"."];
    TradingRules *trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
    QCData *_qcData = [QCData singleton];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    CGFloat defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stock.stockCode] objectForKey:FID_88_D_SELL_PRICE_1] floatValue];
    
    //NSLog(@"b4 %f",defPrice);
    if (defPrice<=0) {
        defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stock.stockCode] objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
        //  NSLog(@"zero %f",defPrice);
    }
    
    if ([UserPrefConstants singleton].pointerDecimal==3) {
        dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                @"BUY",@"Action",
                
                _stock.stockCode,@"StockCode",
                _stock.stockName,@"StockName",
                [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
                [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stock.stockName],@"stkNname",
                trdRules, @"TradeRules",
                nil];
    }else{
        dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                @"BUY",@"Action",
                
                _stock.stockCode,@"StockCode",
                _stock.stockName,@"StockName",
                [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
                [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _stock.stockName],@"stkNname",
                trdRules, @"TradeRules",
                nil];
    }
    
    [UserPrefConstants singleton].userSelectingStockCode = _stock.stockCode ;

    MTradeViewController * tradeVC = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
    //        tradeVC.stockLotSize = lotSizeStringInt;
    //        tradeVC.delegate = self;
    //    tradeVC.parentController = self;
    [self.navigationController pushViewController:tradeVC animated:YES];
    //        [self executeTrade:sender];
    
    tradeVC.actionTypeDict = dict;
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES; //
    
    int lotSizeStringInt = [[[[_qcData qcFeedDataDict] objectForKey:_stock.stockCode] objectForKey:FID_40_I_SHARE_PER_LOT] intValue];
    
    //    tradeVC.stockLotSize = lotSizeStringInt;
    //    [tradeVC callTrade];
}

-(IBAction)goTrade:(id)sender {
    
//    [_atp timeoutActivity];
    
    [self executeTrade:sender];
}

 */
#pragma mark - UIPageViewControllerDataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
	NSUInteger index = ((PageStockContentInfoVC*) viewController).pageIndex;
	if ((index == 0) || (index == NSNotFound))
	{
		return nil;
	}
	index--;
	return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
	NSUInteger index = ((PageStockContentInfoVC*) viewController).pageIndex;
	if (index == NSNotFound)
	{
		return nil;
	}
	index++;
	if (index == _contents.count)
	{
		return nil;
	}
	return [self viewControllerAtIndex:index];
}

#pragma mark - UIPageViewControllerDelegate
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
	if (completed) {
		PageStockContentInfoVC *pageContent = [pageViewController.viewControllers objectAtIndex:0];
		NSInteger index = pageContent.pageIndex;
		//Move Control to index:
		_lblContentTitle.text = _filterContentView.dataSources[index];
		[_filterContentView setCurrentSelectedItem:index];
		[_filterContentView scrollToItemAtIndex:index animate:YES];
	}
}

#pragma mark - TCControlViewDelegate
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
	_lblContentTitle.text = controlView.dataSources[index];
	//Move to page at index here
	PageStockContentInfoVC *nextContentVC = [self viewControllerAtIndex:index];
	NSArray *viewControllers = @[nextContentVC];
	[self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

#pragma mark - SelectWatchlistPopupVCDelegate
- (void)didSelectWatchlistID:(NSInteger)faID:(NSString *)favName{
	//Call api to add watchlist now
	[[ATPAuthenticate singleton] addStock:self.stock.stockCode toWatchlist:(int)faID completion:^(BOOL success, id result, NSError *error) {
		if (success) {
			NSString *someDuplicate = [result objectForKey:@"DuplicateStocks"];
			if ([someDuplicate length]>0) {
				[[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%watchlistName%":favName}]
											 inController:self withTitle:[LanguageManager stringForKey:Duplicate_Stock]];
				[self showCustomAlertWithMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%watchlistName%":favName}]];
			}else{
				//Success
				[self postNotification:kDidFinishAddStockToWatchlistNotification object:nil];
				[self showCustomAlertWithMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]];
			}
		}else{
			if (error) {
				[self showErrorCustomAlertWithMessage:error.localizedDescription];
			}
		}
	}];
}

#pragma mark - Notification
//DidUpdate
- (void)closeStockInfoVC:(NSNotification *)noti{
    [self backButtonTapped:nil];
}
- (void)didUpdate:(NSNotification *)noti{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSString *stockCode = noti.object;
		StockModel *changedStock = [Utils stockQCFeedDataFromCode:stockCode];
		if (!changedStock) {
			return; //do nothing
		}
		if ([changedStock.stockCode isEqualToString:self.stock.stockCode]) {
			dispatch_async(dispatch_get_main_queue(), ^{
				//Update animate top info
				self.stock = changedStock;
				__weak StockInfoVC *weakSelf = self;
				[self.topInfoView makeAnimateBy:self.stock newStock:changedStock completion:^{
					[weakSelf infoStockFrom:weakSelf.stock];
				}];
				//Post notification to update content info
				[self postNotification:kDidUpdateStockNotification object:changedStock];
			});
		}
	});
}

@end
