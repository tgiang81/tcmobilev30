//
//  PageStockContentInfoVC.m
//  TCiPad
//
//  Created by Kaka on 6/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PageStockContentInfoVC.h"
#import "StockModel.h"

@interface PageStockContentInfoVC ()

@end

@implementation PageStockContentInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	self.view.backgroundColor = COLOR_FROM_HEX(0x3F5767);
	[self updateModeView];
}
- (void)viewWillAppear:(BOOL)animated{
	[self registerNotification];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Main
- (void)registerNotification{
	[self addNotification:kDidUpdateStockNotification selector:@selector(didUpdateStockNotification:)];
}

//Update
- (void)shouldUpdateData{
	//do something to update
}
- (void)shouldUpdateData:(StockModel *)oldStock new:(StockModel *)newStock{
	
}
#pragma mark - Configs
- (void)setFullMode:(BOOL)fullMode{
	_fullMode = fullMode;
	//Do something...
	[self updateModeView];
}

- (void)updateModeView{
	//Do something here
}
- (void)hideViewMode:(BOOL)isHidden{
    
}
#pragma mark - Notification
- (void)didUpdateStockNotification:(NSNotification *)noti{
	//Should update data here...
	StockModel *model = noti.object;
	if (model && [model.stockCode isEqualToString:_stock.stockCode]) {
		self.stock = [model copy];
		[self shouldUpdateData];
		//Use this if user want to compare value
		[self shouldUpdateData:self.stock new:model];
	}
}
@end
