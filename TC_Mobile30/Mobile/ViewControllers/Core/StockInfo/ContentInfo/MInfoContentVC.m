//
//  FoundationContentVC.m
//  TCiPad
//
//  Created by Kaka on 6/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MInfoContentVC.h"
#import "MStockInfoCell.h"
#import "StockInfoModel.h"
#import "StockModel.h"
#import "TCBaseButton.h"
#import "VertxConnectionManager.h"
#import "RatingModel.h"
#import "Utils.h"
#import <StoreKit/StoreKit.h>
#import "iBillionaireViewController.h"
#import "QCData.h"
#import "UIButton+TCUtils.h"
#import "MStockInfoTableViewCell.h"

#define kNORMAL_HEIGHT_CONSTRAIN_RATINGVIEW    150
#define kHIDDEN_HEIGHT_CONSTRAIN_RATINGVIEW    0

#define kSPACE_BETWEEN_ITEMS  0
@interface MInfoContentVC ()<SKStoreProductViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>{
    StockInfoModel *_info;
    RatingModel *_rating;
	float _ratingViewHeight;
	NSMutableArray *_infoContents;
}

//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstrainOfRatingView;

@end

@implementation MInfoContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	//init content info <Values were alway shown>
	_infoContents = @[].mutableCopy;
	if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
		//Fixed fields for SSI broker
		_infoContents = @[
				   @{@"left": @(MInfoContentType_FullName), @"right": @(MInfoContentType_Intraday_MktAvgPrice)
					 },
				   
				   @{@"left": @(MInfoContentType_Exchange), @"right": @(MInfoContentType_52Wks_High)
					 },
				   
				   @{@"left": @(MInfoContentType_SharedIssue), @"right": @(MInfoContentType_52Wks_Low)
					 },
				   
				   @{@"left": @(MInfoContentType_RemainForeign), @"right": @(MInfoContentType_Beta)
					 },
				   
				   @{@"left": @(MInfoContentType_EPS), @"right": @(MInfoContentType_ROA)
					 },
				   
				   @{@"left": @(MInfoContentType_PeRatio), @"right": @(MInfoContentType_ROE)
					 },
				   
				   @{@"left": @(MInfoContentType_Value), @"right": @(MInfoContentType_P_B)
					 },
				   
				   @{@"left": @(MInfoContentType_BuyRate), @"right": @(MInfoContentType_ForeignBuy)
					 },
				   
				   @{@"left": @(MInfoContentType_TotBVol), @"right": @(MInfoContentType_ForeignSell)
					 },
				   
				   @{@"left": @(MInfoContentType_TotSVol), @"right": @(MInfoContentType_Underlying)
					 },
				   
				   @{@"left": @(MInfoContentType_Tot_B_Trans), @"right": @(MInfoContentType_OI)
					 },
				   
				   @{@"left": @(MInfoContentType_Tot_S_Trans), @"right": @(MInfoContentType_Basis)
					 },
				   
				   @{@"left": @(MInfoContentType_HighPrice), @"right": @(MInfoContentType_ExpiryDate)
					 },
				   
				   @{@"left": @(MInfoContentType_LowPrice), @"right": @(MInfoContentType_UnderlyingPrice)
					 },
				   ].mutableCopy;
	}else{
		_infoContents = @[
						  @{@"left": @(MInfoContentType_Sector), @"right": @(MInfoContentType_Status)
							},
						  
						  @{@"left": @(MInfoContentType_Indices), @"right": @(MInfoContentType_Open)
							},
						  
						  @{@"left": @(MInfoContentType_ISIN), @"right": @(MInfoContentType_DynamicHigh)
							},
						  
						  @{@"left": @(MInfoContentType_TradingBoard), @"right": @(MInfoContentType_DynamicLow)
							},
						  
						  @{@"left": @(MInfoContentType_LotSize), @"right": @(MInfoContentType_Volume)
							},
						  
						  @{@"left": @(MInfoContentType_Currency), @"right": @(MInfoContentType_Value)
							},
						  
						  @{@"left": @(MInfoContentType_SharedIssue), @"right": @(MInfoContentType_Trades)
							},
						  
						  @{@"left": @(MInfoContentType_MktCap), @"right": @(MInfoContentType_BuyRate)
							},
						  
						  @{@"left": @(MInfoContentType_ParValue), @"right": @(MInfoContentType_TotBVol)
							},
						  
						  @{@"left": @(MInfoContentType_Ceiling), @"right": @(MInfoContentType_TotSVol)
							},
						  
						  @{@"left": @(MInfoContentType_Floor), @"right": @(MInfoContentType_Tot_Short_SVol)
							},
						  
						  @{@"left": @(MInfoContentType_EPS), @"right": @(MInfoContentType_Tot_B_Trans)
							},
						  
						  @{@"left": @(MInfoContentType_PeRatio), @"right": @(MInfoContentType_Tot_S_Trans)
							},
						  
						  @{@"left": @(MInfoContentType_NTA), @"right": @(MInfoContentType_DaySpread)
							},
						  
						  @{@"left": @(MInfoContentType_DevidendYield), @"right": @(MInfoContentType_Mkt_Avg_Price)
							},
						  
						  @{@"left": @(MInfoContentType_CPF), @"right": @(MInfoContentType_52Wks_High)
							},
						  
						  @{@"left": @(MInfoContentType_DeliveryBasis), @"right": @(MInfoContentType_52Wks_Low)
							},
						  
						  @{@"left": @(MInfoContentType_ForeignLimit), @"right": @(MInfoContentType_Remark)
							},
						  
						  @{@"left": @(MInfoContentType_ForeignOwnerShipLimit), @"right": @(MInfoContentType_Unknown)
							}
//						  //configurable
//						  @{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_TypeIDSS)
//							},
//						  @{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_IDSS_Tot_Vol)
//							},
//						  @{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_IDSS_Tot_Val)
//							}
						  ].mutableCopy;
	}
    [self createUI];
    [self loadData];	
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self setupFontAnColor];	
	
	_tblContent.estimatedRowHeight = kINFO_CELL_HIGH;
	_tblContent.rowHeight = UITableViewAutomaticDimension;
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	[_tblContent registerNib:[UINib nibWithNibName:[MStockInfoTableViewCell nibName] bundle:nil] forCellReuseIdentifier:[MStockInfoTableViewCell reuseIdentifier]];
}
- (void)setupLanguage{
    //_lblTitle.text = [LanguageManager stringForKey:@"Detail Info"];
    [_btnFollow setTitle:[LanguageManager stringForKey:@"Follow ibillionaire"] forState:UIControlStateNormal];
}
- (void)setupFontAnColor{
	self.view.backgroundColor = [UIColor clearColor];
	self.contentInfoView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	self.btnViewMode.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
}

#pragma mark - UTILS
//- (CGSize)sizeOfInfoCellFrom:(MInfoContentType)type{
//	float height = kINFO_CELL_HIGH;
//	float width  = 0;
//	if (type == MInfoContentType_Sector || type == MInfoContentType_Indices) {
//		width = _collectionView.frame.size.width;
//	}else{
//		width = (_collectionView.frame.size.width - kSPACE_BETWEEN_ITEMS) / 2;
//	}
//	return CGSizeMake(width, height);
//}

- (BOOL)shouldShowIDSS{
	/*
	BOOL a = [UserPrefConstants singleton].enableIDSS;//condition1
	//BOOL b = [UserPrefConstants singleton].idssEnabled;//condition2 + KL
	if (a && [self.stock.stockCode.pathExtension isEqualToString:@"KL"]) {
		if([self.stock.idssTypeCode.uppercaseString isEqualToString:@"I"] || [self.stock.idssTypeCode.uppercaseString isEqualToString:@"1"] || [self.stock.idssTypeCode.uppercaseString isEqualToString:@"2"] || [self.stock.idssTypeCode.uppercaseString isEqualToString:@"3"]){
			self.stock.idssTypeString = [UserPrefConstants getIDSSStatus:self.stock.idssTypeCode];
			return YES;
		}
	}
	return NO;
	*/
	if ([UserPrefConstants singleton].enableIDSS && [self.stock.stockCode.pathExtension isEqualToString:@"KL"]) {
		return YES;
	}
	return NO;
}
#pragma mark - LoadData
- (void)loadData{
    //save selected stock. ggtt
    [UserPrefConstants singleton].userSelectingThisStock = self.stock;
	//Get info from local
	if (([[[QCData singleton] qcFeedDataDict] objectForKey:self.stock.stockCode]) && ([self.stock.stockCode length] > 0)) {
		if ([[[[QCData singleton] qcFeedDataDict] allKeys] containsObject:self.stock.stockCode]) {
			//NSDictionary *stockDetail = [[[QCData singleton] qcFeedDataDict]objectForKey:self.stock];
			//_info =  [[StockInfoModel alloc] initWithDictionary:stockDetail error:nil];
			_info = [[StockInfoModel alloc] initWithStkCode:self.stock.stockCode];
		}
	}
	//Get New Info stock
	[self addNotification:kDidGetStockInfo_Notification selector:@selector(didGetStockInfoNotification:)];
	[[VertxConnectionManager singleton] vertxGetStockDetailWithStockArr:@[self.stock.stockCode] FIDArr:[[AppConstants SortingList] allKeys] tag:1];//Get detail
	//[[VertxConnectionManager singleton] getDetailStock:self.stock.stockCode];
	
	[self.tblContent reloadData];
	//For Rating
	[self reloadRating];
	
	//For IDSS
	[self getIDSS];
}
- (void)reloadRating{
    //For Rating
    [self checkShowBtnFlow];
    [self addNotification:@"vertxGetScreenerRatingFinished" selector:@selector(vertxGetScreenerRatingFinished:)];
    //[[VertxConnectionManager singleton] vertxGetRating:self.stock.stockCode exchange:[Utils stripExchgCodeFromStockCode:self.stock.stockCode]];
	[[VertxConnectionManager singleton] vertxGetRating:self.stock.stockCode exchange:[self.stock.stockCode pathExtension]];
}

//For Rating
- (void)vertxGetScreenerRatingFinished:(NSNotification *)notification {
    if([notification.object count] > 0){
        _rating = [[RatingModel alloc] initWithDic:[(notification.object) firstObject]];
        [self setupRating];
        [[VertxConnectionManager singleton] vertxUnSubcribeGetRating];
		//+++ Can use this to show Wks_high, low
    }else{
        [self.imgChartRating setHidden:YES];
        [self.imgPointRating setHidden:YES];
        [self checkShowRatingView];
    }
	[self removeNotification:@"vertxGetScreenerRatingFinished"];
}
- (void)setupRating{
    [self.imgPointRating setHidden:![_rating isShow]];
    self.imgPointRating.image = [_rating getImage];
    if(_rating.riskLevel.length != 0){
        UIImage *ratingsImage;
        switch ([_rating.riskLevel intValue]) {
            case -1:
                ratingsImage = [UIImage imageNamed:[NSString stringWithFormat:@"high_rating_chart_icon"]];
                break;
            case 0:
                ratingsImage = [UIImage imageNamed:[NSString stringWithFormat:@"medium_rating_chart_icon"]];
                break;
            case 1:
                ratingsImage = [UIImage imageNamed:[NSString stringWithFormat:@"low_rating_chart_icon"]];
                break;
            default:
                ratingsImage = [UIImage imageNamed:[NSString stringWithFormat:@"medium_rating_chart_icon"]];
                break;
        }
        [self.imgChartRating setHidden:NO];
        self.imgChartRating.image = ratingsImage;
    }else{
        [self.imgChartRating setHidden:YES];
    }
    
    [self checkShowRatingView];
}
- (void)checkShowRatingView {
    if(self.imgPointRating.isHidden && self.btnFollow.isHidden && self.imgChartRating.isHidden){
        self.heightConstrainOfRatingView.constant = 0;
    }else if (self.imgPointRating.isHidden && self.btnFollow.isHidden){
        self.heightConstrainOfRatingView.constant = self.heightConstrainOfRatingView.constant - self.imgPointRating.frame.size.height - 8;
    }else if(self.imgChartRating.isHidden){
        self.heightConstrainOfRatingView.constant = self.heightConstrainOfRatingView.constant - self.imgChartRating.frame.size.height;
    }
	_ratingViewHeight = self.heightConstrainOfRatingView.constant;
	//For update Mode
	[self updateModeView];
}

#pragma mark - Get IDSS
- (void)getIDSS{
	//Get IDSS, 52week, Foreign Share
	//Call API
	NSDictionary *idssType = @{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_TypeIDSS)};
	NSDictionary *idssVol = @{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_IDSS_Tot_Vol)};
	NSDictionary *idssVal = @{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_IDSS_Tot_Val)};
	
	if ([self shouldShowIDSS]) {
		if (![_infoContents containsObject:idssType]) {
			[_infoContents addObject:idssType];
		}
		if (![_infoContents containsObject:idssVol]) {
			[_infoContents addObject:idssVol];
		}
		if (![_infoContents containsObject:idssVal]) {
			[_infoContents addObject:idssVal];
		}
		//configurable
		/*
		@{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_TypeIDSS)
		  },
		@{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_IDSS_Tot_Vol)
		  },
		@{@"left": @(MInfoContentType_Unknown), @"right": @(MInfoContentType_IDSS_Tot_Val)
		  }
		*/
		
	}
	//Alway call to show 52wks...
	[self addNotification:kGetISDDInfo selector:@selector(doneVertxISDDNotification:)];
	[[VertxConnectionManager singleton] getIDSSInfo:self.stock.stockCode];
}

#pragma mark - Actions

- (IBAction)switchViewMode:(UIButton *)sender {
	if (self.delegate) {
		[self.delegate shouldSwitchViewModeFrom:self.fullMode forController:self];
	}
}
- (IBAction)onFollowIbillionaireAction:(id)sender {
    if ([UserPrefConstants singleton].enabledNewiBillionaire) {
        
        NSString *strip_stock_code = [Utils stripSymbolFromStockCode:_info.stockCode];
        
        [UserPrefConstants singleton].iBillionareStock = [NSString stringWithFormat:@"%@:%@",strip_stock_code ,[Utils stripExchgCodeFromStockCode:_info.stockCode]];
        [self pushOpeniBillionaire];
    }else{
        [self actionNotEnableIBillion];
    }
}
- (void) pushOpeniBillionaire{
    iBillionaireViewController *qrVC = [[iBillionaireViewController alloc] initWithNibName:@"iBillionaireViewController" bundle:nil];
    [self nextTo:qrVC animate:YES];
}
- (void)checkShowBtnFlow{
    [self.btnFollow setHidden:![[UserPrefConstants singleton].filteriBillionaire containsObject:[Utils stripExchgCodeFromStockCode:_info.stockCode]]];
}

- (void)actionNotEnableIBillion{
    BOOL twInstalled = NO;
    BOOL isCIMBSG = [UserPrefConstants singleton].isCIMBSG;
    if (isCIMBSG) {
        twInstalled = [self schemeAvailable:@"ibillionaireresearchsg://"];
    }else{
        twInstalled = [self schemeAvailable:@"ibillionaireresearchmy://"];
    }    
    //BOOL twInstalled = [self schemeAvailable:@"ibillionaireresearch://"];
    if (twInstalled) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"Disclaimer"] message:@"By clicking ""OK"", you are launching an external application and agree to the Terms and Conditions of iBillionaire Inc." delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Cancel"] otherButtonTitles:[LanguageManager stringForKey:@"OK"],nil];
        [alert setTag:678];
        [alert show];
        
    }else{
        SKStoreProductViewController *productController = [[SKStoreProductViewController alloc] init];
        productController.delegate = (id<SKStoreProductViewControllerDelegate>)self;
        
        NSUInteger appStoreID = [@"1083955373" longLongValue];
        
        //load product details
        NSDictionary *productParameters = @{SKStoreProductParameterITunesItemIdentifier: [@(appStoreID) description]};
        [productController loadProductWithParameters:productParameters completionBlock:NULL];
        //get root view controller
        //present product view controller
        [self presentViewController:productController animated:YES completion:nil];
        
        // NSURL *urlString = [NSURL URLWithString:@"https://itunes.apple.com/app/ibillionaire-research-invest/id1083955373?mt=8"];
        
        // [[UIApplication sharedApplication] openURL:urlString];
        
    }
}
- (BOOL)schemeAvailable:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];
    return [application canOpenURL:URL];
}
#pragma mark SKStoreProductViewControllerDelegate
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - UTILS
- (void)setHiddenRatingView:(BOOL)isHidden{
    _heightConstrainOfRatingView.constant = isHidden ? kHIDDEN_HEIGHT_CONSTRAIN_RATINGVIEW : kNORMAL_HEIGHT_CONSTRAIN_RATINGVIEW;
    [self.view layoutIfNeeded];
}

#pragma mark - Configs
- (void)updateModeView{
	//[super updateModeView];
	UIImage *modeImage;
	if (self.fullMode) {
		modeImage = [UIImage imageNamed:@"minimum_mode_icon"];
		self.heightConstrainOfRatingView.constant = _ratingViewHeight;
		self.tblContent.scrollEnabled = YES;
	}else{
		modeImage = [UIImage imageNamed:@"full_mode_icon"];
		self.heightConstrainOfRatingView.constant = 0;
		self.tblContent.scrollEnabled = NO;
	}
	[self.btnViewMode setImage:modeImage forState:UIControlStateNormal];
	//Reload Content
	[self.tblContent reloadData];
	[self.view layoutIfNeeded];
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_infoContents count];;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (self.fullMode) {
		return UITableViewAutomaticDimension;
	}
	return kINFO_CELL_HIGH;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MStockInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[MStockInfoTableViewCell reuseIdentifier]];
	if (!cell) {
		cell = [[MStockInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MStockInfoTableViewCell reuseIdentifier]];
	}
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[cell updateDataFrom:_info byDict:_infoContents[indexPath.row]];
	return cell;
}

#pragma mark - UICollectionViewDataSource
/*
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	return [_infoContent count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	StockInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[StockInfoCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
	//Do something
	//Parse data here
	MInfoContentType type = [_infoContent[indexPath.item] integerValue];
	StockInfoCollectionViewCell *aCell = (StockInfoCollectionViewCell *)cell;
	[aCell updateDataFrom:type info:_info];
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	//Do nothings
	[collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
	MInfoContentType type = [_infoContent[indexPath.item] integerValue];
	return [self sizeOfInfoCellFrom:type];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
	return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
	return 0;
}
*/
#pragma mark - Should Update Data
- (void)shouldUpdateData{
	//Reload data
	[self loadData];
}

#pragma mark - Notification

//For Stock Info
- (void)didGetStockInfoNotification:(NSNotification *)noti{
	//[self removeNotification:kDidGetStockInfo_Notification];
	if (([[[QCData singleton] qcFeedDataDict] objectForKey:self.stock.stockCode]) && ([self.stock.stockCode length] > 0)) {
		if ([[[[QCData singleton] qcFeedDataDict] allKeys] containsObject:self.stock.stockCode]) {
			//NSError *error;
			//NSDictionary *stockDetail = [[[QCData singleton] qcFeedDataDict]objectForKey:self.stock.stockCode];
			//_info =  [[StockInfoModel alloc] initWithDictionary:stockDetail error:&error];
			//DLog(@"StockInfo: Error: %@", error.localizedDescription);
			_info = [[StockInfoModel alloc] initWithStkCode:self.stock.stockCode];
			
		}
	}
	[self.tblContent reloadData];
	[self getIDSS];
}

//IDSS
- (void)doneVertxISDDNotification:(NSNotification *)noti{
	[self removeNotification:kGetISDDInfo];
	NSDictionary *idssInfo = noti.userInfo;
	if(idssInfo){
		self.stock.idssVa = [idssInfo[IDSS_VA] doubleValue];
		self.stock.idssVol = [idssInfo[IDSS_VOL] doubleValue];
		if(self.stock.idssTypeCode){
			self.stock.idssTypeString = [UserPrefConstants getIDSSStatus:self.stock.idssTypeCode];
		}
		//IDSS
		if (_info) {
			_info.idssTypeString = self.stock.idssTypeString;
			_info.idssTotVol = [NSNumber numberWithDouble:self.stock.idssVol];
			_info.idssTotVal = [NSNumber numberWithDouble:self.stock.idssVa];
		}
		
		//52-week: low, high
		if (_info) {
			_info.wks_52_Low = [NSNumber numberWithDouble:[idssInfo[WEEK_52_LOW] doubleValue]];
			_info.wks_52_High = [NSNumber numberWithDouble:[idssInfo[WEEK_52_HIGH] doubleValue]];
			_info.foreignShare = [NSNumber numberWithDouble:[idssInfo[FOREIGN_SHARE_191] doubleValue]];
		}
		//Reload data
		[self.tblContent reloadData];
	}
}
@end
