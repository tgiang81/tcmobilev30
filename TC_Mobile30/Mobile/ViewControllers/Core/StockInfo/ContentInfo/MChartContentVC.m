//
//  ChartContentVC.m
//  TCiPad
//
//  Created by Kaka on 6/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MChartContentVC.h"
#import "TCChartView.h"
#import "StockModel.h"
#import "ChartVC.h"
#import "TCAnimation.h"
#import "ZoomingViewController.h"
@interface MChartContentVC ()<UIGestureRecognizerDelegate, ChartVCDelegate>
{
    TCAnimation *tcAnimation;
	ZoomingViewController *zoomingViewController;
}

//Properties
@property (weak, nonatomic) IBOutlet UIButton *btnSwitchModeView;

@end

@implementation MChartContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    tcAnimation = [[TCAnimation alloc] init];
    // Do any additional setup after loading the view from its nib.
	/*
	UITapGestureRecognizer	*_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
	_tap.numberOfTapsRequired = 2;
	_tap.delegate = self;
	[self.view addGestureRecognizer:_tap];
	*/
	
	//[self setZoomingView];
	[self loadChart];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	//[self setZoomingView];
}
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Theme
- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = COLOR_FROM_HEX(0x364D59);
	self.btnSwitchModeView.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
}
#pragma mark - LoadChart
- (void)loadChart{
	[self.view layoutIfNeeded];
	_chartView.stCode = self.stock.stockCode;
	[_chartView startLoadDefaultChart];
}


#pragma mark - Configs
- (void)updateModeView{
	UIImage *modeImage;
	if (self.fullMode) {
		modeImage = [UIImage imageNamed:@"minimum_mode_icon"];
	}else{
		modeImage = [UIImage imageNamed:@"full_mode_icon"];
	}
	[UIView animateWithDuration:0.3 animations:^{
		[self.btnSwitchModeView setImage:modeImage forState:UIControlStateNormal];
	} completion:^(BOOL finished) {
		
	}];
}
#pragma mark - Action
#pragma mark - ACTION

- (IBAction)onSwitchViewMode:(id)sender {
	if (self.delegate) {
		[self.delegate shouldSwitchViewModeFrom:self.fullMode forController:self];
	}
}

- (IBAction)onMinimumModeAction:(id)sender {
	//[zoomingViewController toggleZoom:nil];
}
- (IBAction)onFullModeAction:(id)sender {
	//[zoomingViewController toggleZoom:nil];
}
- (void)handleTap:(UITapGestureRecognizer *)tap{
    [tcAnimation showChartDetail:self.view];
//    ChartVC *_chart = NEW_VC_FROM_NIB([ChartVC class], [ChartVC nibName]);
//    _chart.stockCode = self.stock.stockCode;
//    _chart.chartType = _chartView.chartType;
//    _chart.delegate = self;
//    _chart.isOnlyImageChart = NO;
//    _chart.providesPresentationContextTransitionStyle = YES;
//    [_chart setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    [AppDelegateAccessor.container presentViewController:_chart animated:YES completion:^{
//        _chart.isPresented = YES;
//    }];
}

- (void)setZoomingView{
	zoomingViewController = [[ZoomingViewController alloc] init];
	zoomingViewController.view = self.chartView;
	zoomingViewController.view.frame = self.chartView.frame;
	//[zoomingViewController.view setTranslatesAutoresizingMaskIntoConstraints:YES];
	//[_chartView setTranslatesAutoresizingMaskIntoConstraints:YES];
}
#pragma mark - ChartVCDelegate
- (void)didLoadChartAtType:(ChartType)type{
	self.chartView.chartType = type;
	[self.chartView startLoadDefaultChart];
}

#pragma mark - Should Update
- (void)shouldUpdateData:(StockModel *)oldStock new:(StockModel *)newStock{
	if (oldStock.lastDonePrice.doubleValue != newStock.lastDonePrice.doubleValue ) {
		[self loadChart];
	}
}
#pragma mark - UITapGestureDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}
@end
