//
//  MMarketDepthContentVC.m
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MMarketDepthContentVC.h"
#import "MMarketDepthCell.h"
#import "MDInfo.h"
#import "QCData.h"
#import "StockModel.h"
#import "MarketDepthModel.h"
#import "UILabel+FormattedText.h"
#import "VertxConnectionManager.h"
#import "SVPullToRefresh.h"
#import "LanguageKey.h"
#import "UIButton+TCUtils.h"
#import "NSNumber+Formatter.h"

@interface MMarketDepthContentVC ()<UITableViewDataSource, UITableViewDelegate>{
	VertxConnectionManager* _vertxManager;
	NSMutableArray *_mktDepthArr, *_MDSellArr, *_MDBuyArr;
	NSMutableArray *_mkModels;
	MDInfo *mdInfo;
    UIRefreshControl *refreshControl;
	
	//HUD
	DGActivityIndicatorView *_hudActivity;
	float _lacpValue;
}
@property (weak, nonatomic) IBOutlet UIButton *btnViewMode;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewContentConstrainHeight;

//OUTLETS

@end

@implementation MMarketDepthContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	//Initial data
	_vertxManager = [VertxConnectionManager singleton];
	_mktDepthArr = @[].mutableCopy;
	_MDSellArr = [[NSMutableArray alloc] init];
	_MDBuyArr = [[NSMutableArray alloc] init];
	_mkModels = @[].mutableCopy;
	[self createUI];
	[self loadData];
}
- (void)clearColor{
    self.view.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)hideViewMode:(BOOL)isHidden{
    [_btnViewMode setHidden:isHidden];
    self.tblContent.scrollEnabled = isHidden;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self setupFontAnColor];
	[self updateTheme:nil];
	//For tableView
	[self.tblContent registerNib:[UINib nibWithNibName:[MMarketDepthCell nibName] bundle:nil] forCellReuseIdentifier:[MMarketDepthCell reuseIdentifier]];
	//_tblContent.estimatedRowHeight = 30.0;
	//_tblContent.rowHeight = UITableViewAutomaticDimension;
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //add pull to refresh data at tableView
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..." attributes:attrsDictionary];
    refreshControl.attributedTitle = attributedTitle;
    if (@available(iOS 10.0, *)) {
        self.tblContent.refreshControl = refreshControl;
    } else {
        // Fallback on earlier versions
        [self.tblContent addSubview:refreshControl];
    }
}
- (void)setupLanguage{
    _lblNo.text=[LanguageManager stringForKey:No];
    _lblBidQty.text=[LanguageManager stringForKey:Bid__Qty];
    _lblBid.text=[LanguageManager stringForKey:Bid];
    _lblAsk.text=[LanguageManager stringForKey:Ask];
    _lblAskQty.text=[LanguageManager stringForKey:Ask__Qty];
}
- (void)setupFontAnColor{
	
}

- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	_viewInstructionBar.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subHeaderBg);
	self.btnViewMode.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	[_tblContent reloadData];
}

#pragma mark - Configs
- (void)updateModeView{
	UIImage *modeImage;
	if (self.fullMode) {
		modeImage = [UIImage imageNamed:@"minimum_mode_icon"];		self.tblContent.scrollEnabled = YES;
	}else{
		modeImage = [UIImage imageNamed:@"full_mode_icon"];
		self.tblContent.scrollEnabled = NO;
	}
	[self.btnViewMode setImage:modeImage forState:UIControlStateNormal];
	[self.view setNeedsDisplay];
}
#pragma mark - UTILS VC
//Parse data to model
- (NSArray *)dataModelsFrom:(NSArray *)marketBuys sells:(NSArray *)marketSells{
	if (!marketBuys || !marketSells || marketSells.count == 0 || marketBuys.count == 0) {
		return @[];
	}
	NSMutableArray *models = @[].mutableCopy;
	for (int i = 0; i < marketBuys.count; i ++) {
		MarketDepthModel *model = [MarketDepthModel new];
		NSDictionary *buyDict = marketBuys[i];
		NSDictionary *sellDict = marketSells[i];
		
		//BUY VALUE
		for (NSString *key in buyDict.allKeys) {
			if ([key isEqualToString:FID_170_I_BUY_SPLIT_1]) {
				model.buySplit1 = buyDict[key];
			}
			if ([key isEqualToString:FID_58_I_BUY_QTY_1]) {
				model.buyQty = [NSNumber numberWithLongLong:[buyDict[FID_58_I_BUY_QTY_1] longLongValue]];
			}
			if ([key isEqualToString:FID_68_D_BUY_PRICE_1]) {
				model.buyPrice = [NSNumber numberWithFloat:[buyDict[FID_68_D_BUY_PRICE_1] floatValue]];
			}
		}
		//SELL VALUE
		for (NSString *key in sellDict.allKeys) {
			if ([key isEqualToString:FID_170_I_BUY_SPLIT_1]) {
				model.sellSplit1 = sellDict[FID_170_I_BUY_SPLIT_1];
			}
			if ([key isEqualToString:FID_58_I_BUY_QTY_1]) {
				model.sellQty = [NSNumber numberWithLongLong:[sellDict[FID_58_I_BUY_QTY_1] longLongValue]];
			}
			if ([key isEqualToString:FID_68_D_BUY_PRICE_1]) {
				model.sellPrice = [NSNumber numberWithFloat:[sellDict[FID_68_D_BUY_PRICE_1] floatValue]];
			}
		}
		//Store
		[models addObject:model];
	}
	return [models copy];
}
//Show HUD - Activity
- (void)showHudActivity{
	//Demo show activity on StockView
	if(!_hudActivity){
		_hudActivity = [[Utils shareInstance] createPrivateActivity];
	}
	[[Utils shareInstance] showActivity:_hudActivity inView:self.view];
}

- (void)hideHudActivity{
	[[Utils shareInstance] dismissActivity:_hudActivity];
}

//========= Update Cell Height If Need ========
- (void)updateContentConstrainIfNeed{
	if (_mkModels.count > 0) {
		float heightOfTableContent = _mkModels.count * kCONTENT_INFO_CELL_HEIGHT;
		if (heightOfTableContent > self.contentView.frame.size.height - 90) {
			//90 is sum size of TotalView and Instruction view
			heightOfTableContent = self.contentView.frame.size.height - 90;
		}
		_tableViewContentConstrainHeight.constant = heightOfTableContent;
		[self.view layoutIfNeeded];
	}
}
#pragma mark - LoadData
- (void)loadData{
	ExchangeInfo *info = [UserPrefConstants singleton].currentExchangeInfo;
	if (!info) {
		info = [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:[UserPrefConstants singleton].userCurrentExchange];
	}
	for (MDInfo *inf in info.qcParams.exchgInfo) {
		if ([inf.exchange isEqualToString:[UserPrefConstants singleton].userCurrentExchange]) {
			mdInfo = inf;
			break;
		}
	}
	_lacpValue = [[[[[QCData singleton] qcFeedDataDict]objectForKey:self.stock.stockCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
	[self getMarketDepth];
}

- (void)registerNotification{
	[self addNotification:kDidFinishMarketDepthNotification selector:@selector(didFinishMarketDepth:)];
}

- (void)getMarketDepth{
	if (self.stock) {
		[self showHudActivity];
		[self registerNotification];
		[self.view removeStatusViewIfNeeded];
		[_vertxManager vertxGetMarketDepth:self.stock.stockCode andCollarr:self.stock.stockCode.pathExtension];
	}
}

- (void)refreshData{
    [self getMarketDepth];
    [self.view dismissPrivateHUD];
}

- (void)updateCalculatingTotalValue{
	long long totalBuy = 0;
	long long totalSell = 0;
	
	long totBuySplit = 0; long long totBuyQty = 0;  CGFloat aveBuyBid = 0;
	long totSellSplit = 0; long long totSellQty = 0; CGFloat aveSellAsk = 0;
	
	for (long i=0; i<[_MDBuyArr count]; i++)  {
		NSDictionary *buyData = [_MDBuyArr objectAtIndex:i];
		
		long eachBuySplit = [[buyData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
		totBuySplit += eachBuySplit;
		
		long long eachBuyQty = [[buyData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
		totBuyQty += eachBuyQty;
		
		CGFloat eachBuyBid = [[buyData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
		aveBuyBid += eachBuyBid*eachBuyQty;
		
		totalBuy += eachBuyQty*eachBuyBid;
		
	}
	
	for (long i=0; i<[_MDSellArr count]; i++)  {
		
		NSDictionary *sellData = [_MDSellArr objectAtIndex:i];
		
		long eachSellSplit = [[sellData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
		totSellSplit += eachSellSplit;
		
		long long eachSellQty = [[sellData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
		totSellQty += eachSellQty;
		
		CGFloat eachSellAsk = [[sellData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
		aveSellAsk += eachSellAsk*eachSellQty;
		totalSell += eachSellQty*eachSellAsk;
	}
	
	NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
	[priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
	[priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
	[priceFormatter setGroupingSeparator:@","];
	[priceFormatter setGroupingSize:3];
	
	NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
	[quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[quantityFormatter setGroupingSeparator:@","];
	[quantityFormatter setGroupingSize:3];
	[quantityFormatter setGeneratesDecimalNumbers:YES];
	
	if (totBuyQty<=0) aveBuyBid=0; else aveBuyBid = aveBuyBid/totBuyQty;
	if (totSellQty<=0) aveSellAsk=0; else aveSellAsk = aveSellAsk/totSellQty;
	
	
	_lblTotalSplitBid.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totBuySplit]];
	_lblTotalBidQty.text =  [[UserPrefConstants singleton] abbreviateNumber:round(totBuyQty)];
	_lblTotalBid.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveBuyBid/[_MDBuyArr count]]];
	
	_lblTotalSplitAsk.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totSellSplit]];
	_lblTotalAskQty.text =  [[UserPrefConstants singleton] abbreviateNumber:round(totSellQty)];
	_lblTotalAsk.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveSellAsk/[_MDSellArr count]]];
	
	NSString *totBuyStr;
	if (totalBuy > -1000000 && totalBuy < 1000000) {
		totBuyStr = [[NSNumber numberWithFloat:totalBuy] toDecimaWithPointer:2];
	}else{
		totBuyStr = [[NSNumber numberWithFloat:totalBuy] abbreviateNumber];
	}
	
	NSString *totSellStr;
	if (totalSell > -1000000 && totalSell < 1000000) {
		totSellStr = [[NSNumber numberWithFloat:totalSell] toDecimaWithPointer:2];
	}else{
		totSellStr = [[NSNumber numberWithFloat:totalSell] abbreviateNumber];
	}
	
	_lblSummaryBid.text = [LanguageManager stringForKey:@"Total Bid: %@"
									   withPlaceholders:@{@"%totBid%":totBuyStr}];
	_lblSummaryAsk.text = [LanguageManager stringForKey:@"Total Ask: %@"
									   withPlaceholders:@{@"%totAsk%":totSellStr}];
	//_lblSummaryAsk.text = [LanguageManager stringForKey:@"Total Ask: %@" withPlaceholders:@{@"%totAsk%":[quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totalSell]]}];
	//Format text color
	[_lblSummaryBid boldSubstring:totBuyStr];
	[_lblSummaryAsk boldSubstring:totSellStr];
}

#pragma mark - Action
- (IBAction)onChangeTypeOfViewAction:(id)sender {
	if (self.delegate) {
		[self.delegate shouldSwitchViewModeFrom:self.fullMode forController:self];
	}
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 30.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_mkModels count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MMarketDepthCell *cell = [tableView dequeueReusableCellWithIdentifier:[MMarketDepthCell reuseIdentifier]];
	if (!cell) {
		cell = [[MMarketDepthCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MMarketDepthCell reuseIdentifier]];
	}
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
	if (indexPath.row % 2 == 0) {
		cell.backgroundColor = RGB(247, 247, 247);
	}else{
		cell.backgroundColor = RGB(255, 255, 255);//TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellOddColor);
	}
	//Parse data
	[cell setupDataFrom:_mkModels[indexPath.row] comparePrice:_lacpValue atIndexPath:indexPath];
	return cell;
}

#pragma mark - Notification
- (void)didFinishMarketDepth:(NSNotification *)noti{
	[self removeNotification:kDidFinishMarketDepthNotification];
	self->_mktDepthArr = [NSMutableArray arrayWithArray:noti.userInfo[@"marketdepthresults"]];
	if (!self->_mktDepthArr || self->_mktDepthArr.count == 0) {
		[self->_mkModels removeAllObjects];
		[self hideHudActivity];
		[self.tblContent reloadData];
        if (self->refreshControl) {
            [self->refreshControl endRefreshing];
        }
		[self.contentView showStatusMessage:[LanguageManager stringForKey:No_Data]];
		return;
	}
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		NSMutableArray *tmpBuyArr = [[NSMutableArray alloc] init];
		NSMutableArray *tmpSellArr = [[NSMutableArray alloc] init];
		BOOL MDLfeature = ([self->mdInfo.isMDL.uppercaseString isEqualToString:@"Y"]) ? YES : NO;
		// Non Feature md data is all in single dictionary, so we convert it to same
		// format as featured MD.
		if (!MDLfeature) {
			// if server give any data
			if ([self->_mktDepthArr count]>0) {
				// non MDL - all mkt depth data is in one object (max 10 level only)
				NSDictionary *stkDataWithMDDict = [self->_mktDepthArr objectAtIndex:0];
				int fid_buy_split_start = 170;
				int fid_sell_split_start = 180;
				int fid_buy_qty_start = 58;
				int fid_sell_qty_start = 78;
				int fid_buy_price_start = 68;
				int fid_sell_price_start = 88;
				
				
				// populate tmpBuyArr (loop 10 and if found, create it and add)
				for (int i=0; i<self->mdInfo.mdLevel.integerValue; i++)
				{
					NSMutableDictionary *buyMD = [[NSMutableDictionary alloc] init];
					
					NSString *buySplitKey = [NSString stringWithFormat:@"%d",fid_buy_split_start+i];
					if ([[stkDataWithMDDict allKeys] containsObject:buySplitKey]) {
						[buyMD setObject:[stkDataWithMDDict objectForKey:buySplitKey] forKey:FID_170_I_BUY_SPLIT_1];
					}
					
					NSString *buyPriceKey = [NSString stringWithFormat:@"%d",fid_buy_price_start+i];
					if ([[stkDataWithMDDict allKeys] containsObject:buyPriceKey]) {
						[buyMD setObject:[stkDataWithMDDict objectForKey:buyPriceKey] forKey:FID_68_D_BUY_PRICE_1];
					}
					
					NSString *buyQtyKey = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
					if ([[stkDataWithMDDict allKeys] containsObject:buyQtyKey]) {
						[buyMD setObject:[stkDataWithMDDict objectForKey:buyQtyKey] forKey:FID_58_I_BUY_QTY_1];
					}
					
					if ([[buyMD allKeys] count]>0) {
						// set FID 107
						[buyMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
						// set FID 106
						[buyMD setObject:[NSNumber numberWithInt:1] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
						
						[tmpBuyArr addObject:buyMD];
					}
				}
				
				// NSLog(@"tmpBuyArr %@", tmpBuyArr);
				
				// populate tmpSellArr
				for (int i=0; i<self->mdInfo.mdLevel.integerValue; i++)
				{
					NSMutableDictionary *sellMD = [[NSMutableDictionary alloc] init];
					
					NSString *sellSplitKey = [NSString stringWithFormat:@"%d",fid_sell_split_start+i];
					if ([[stkDataWithMDDict allKeys] containsObject:sellSplitKey]) {
						[sellMD setObject:[stkDataWithMDDict objectForKey:sellSplitKey] forKey:FID_170_I_BUY_SPLIT_1];
					}
					
					NSString *sellPriceKey = [NSString stringWithFormat:@"%d",fid_sell_price_start+i];
					if ([[stkDataWithMDDict allKeys] containsObject:sellPriceKey]) {
						[sellMD setObject:[stkDataWithMDDict objectForKey:sellPriceKey] forKey:FID_68_D_BUY_PRICE_1];
					}
					
					NSString *sellQtyKey = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
					if ([[stkDataWithMDDict allKeys] containsObject:sellQtyKey]) {
						[sellMD setObject:[stkDataWithMDDict objectForKey:sellQtyKey] forKey:FID_58_I_BUY_QTY_1];
					}
					
					if ([[sellMD allKeys] count]>0) {
						// set FID 107
						[sellMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
						// set FID 106
						[sellMD setObject:[NSNumber numberWithInt:2] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
						[tmpSellArr addObject:sellMD];
					}
				}
			} else {
				; // empty data = do nothing (will be handled below)
			}
		} else{
			// MDL type - mkt depth data in separate objects
			// if server give any data
			if ([self->_mktDepthArr count]>0) {
				for (int i=0; i<[self->_mktDepthArr count]; i++) {
					NSDictionary *eachMDdata = [self->_mktDepthArr objectAtIndex:i];
					if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==1) {
						// BID
						[tmpBuyArr addObject:eachMDdata];
					}
					if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==2) {
						// SELL
						[tmpSellArr addObject:eachMDdata];
					}
				}
				
			} else {
				; // empty data = do nothing  (will be handled below)
			}
			
		}
		
		// if tmpBuyArr empty, prepare zeroes
		if ([tmpBuyArr count]<=0) {
			int mdLevel = [self->mdInfo.mdLevel intValue];
			int alternate = 1;
			int pos = 0;
			for (long i=0; i<mdLevel*2; i++) {
				NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
										  [NSNumber numberWithInt:2],@"105",
										  [NSNumber numberWithInt:alternate],@"106",
										  [NSNumber numberWithInt:pos],@"107",
										  [NSNumber numberWithInt:0],@"118",
										  @"KL",@"131",
										  [NSNumber numberWithInt:0],@"170",
										  @"5517.KL",@"33",
										  @"0",@"45",
										  [NSNumber numberWithInt:0],@"58",
										  [NSNumber numberWithInt:0],@"68",
										  nil];
				
				if (alternate==1) {
					[tmpBuyArr addObject:zeroDict];
					alternate = 2;
					pos++;
				} else {
					[tmpSellArr addObject:zeroDict];
					alternate = 1;
				}
			}
			
		}
		// sort data based on FID 107
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_107_I_MARKET_DEPTH_POSITION ascending:YES];
		NSArray *limitedArrayB = [tmpBuyArr sortedArrayUsingDescriptors:@[sortDescriptor]];
		NSArray *limitedArrayS = [tmpSellArr sortedArrayUsingDescriptors:@[sortDescriptor]];
		[tmpBuyArr removeAllObjects]; [tmpSellArr removeAllObjects];
		//Update UI
		[self hideHudActivity];
		[self.view removeStatusViewIfNeeded];
		if ([limitedArrayB count] > 0) {
			self->_MDBuyArr = [NSMutableArray arrayWithArray:[limitedArrayB mutableCopy]];
			self->_MDSellArr = [NSMutableArray arrayWithArray:[limitedArrayS mutableCopy]];
			self->_mkModels = [NSMutableArray arrayWithArray:[self dataModelsFrom:self->_MDBuyArr sells:self->_MDSellArr]];
			
			//Check level
			/*
			 ExchangeData *curExData = [Utils curExchangeDataFrom:[UserPrefConstants singleton].userExchagelist byExchange:[UserPrefConstants singleton].userCurrentExchange];
			 if (curExData && curExData.MDLfeature == YES) {
			 if (curExData.mktDepthLevel.integerValue < self->_mkModels.count) {
			 self->_mkModels = [[self->_mkModels subarrayWithRange:NSMakeRange(0, [curExData.mktDepthLevel integerValue])] mutableCopy];
			 self->_MDBuyArr = [[self->_MDBuyArr subarrayWithRange:NSMakeRange(0, [curExData.mktDepthLevel integerValue])] mutableCopy];
			 self->_MDSellArr = [[self->_MDSellArr subarrayWithRange:NSMakeRange(0, [curExData.mktDepthLevel integerValue])] mutableCopy];
			 }
			 }
			 */
			//Reload data here
			[self updateCalculatingTotalValue];
			[self.tblContent reloadData];
			[self updateContentConstrainIfNeed];
            if (self->refreshControl) {
                [self->refreshControl endRefreshing];
            }
			if (self->_mkModels.count == 0) {
				[self.contentView showStatusMessage:[LanguageManager stringForKey:No_Data]];
			}
		}else{
			if (self->_mkModels.count > 0) {
				[self->_mkModels removeAllObjects];
			}
			[self.tblContent reloadData];
            if (self->refreshControl) {
                [self->refreshControl endRefreshing];
            }
			[self.contentView showStatusMessage:[LanguageManager stringForKey:No_Data]];
		}
	}];
}
#pragma mark - Should Update Data
- (void)shouldUpdateData{
	//Reload data
	[self loadData];
}
@end
