//
//  MStockNewsContentVC.m
//  TCiPad
//
//  Created by Kaka on 7/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MStockNewsContentVC.h"
#import "ResearchCell.h"
#import "TCPageMenuControl.h"

#import "ATPAuthenticate.h"
#import "VertxConnectionManager.h"
#import "StockModel.h"

#import "UIView+TCUtil.h"
#import "TCStatusInfoView.h"
#import "ElasticNewsData.h"
#import "NewsMetadata.h"
#import "NewsModel.h"

#import "NSArray+SKTaskLoop.h"
#import "ResearchAPI.h"
#import "NSDate+Utilities.h"
#import "NSData-Zlib.h"
#import "NSString_stripHtml.h"
#import "DetailNewsVC.h"
#import "SVPullToRefresh.h"
#import "LanguageKey.h"

@interface MStockNewsContentVC ()<UITableViewDataSource, UITableViewDelegate, TCControlViewDelegate>{
	NSMutableArray *_items;
    UIRefreshControl *refreshControl;
	
}

@property (assign, nonatomic) NewsType newType;
@property (strong, nonatomic) TCStatusInfoView *socketStatusView;
@end

@implementation MStockNewsContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_items = @[].mutableCopy;
	
	[self createUI];
	[self loadData];
}

- (void)viewWillLayoutSubviews{
	[super viewWillLayoutSubviews];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	_controlView.itemWidth = 0.5 * SCREEN_WIDTH_PORTRAIT;
	_controlView.currentSelectedItem = _isLoadingNews ? 0 : 1;
	_controlView.bgColor = COLOR_FROM_HEX(0x374d5a);
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
	[self removeAllNotification];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self setupFontAnColor];
	//Setting tableView
	[_tblContent registerNib:[UINib nibWithNibName:[ResearchCell nibName] bundle:nil] forCellReuseIdentifier:[ResearchCell reuseIdentifier]];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero]; //No redudant cell
    
    //add pull to refresh data at tableView
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..." attributes:attrsDictionary];
    refreshControl.attributedTitle = attributedTitle;
    if (@available(iOS 10.0, *)) {
        self.tblContent.refreshControl = refreshControl;
    } else {
        // Fallback on earlier versions
        [self.tblContent addSubview:refreshControl];
    }
	
	//Setup Control
	_controlView.itemWidth = 0.5 * SCREEN_WIDTH_PORTRAIT;
	_controlView.dataSources = @[[LanguageManager stringForKey:@"News"], [LanguageManager stringForKey:@"Report"]];
	_controlView.bgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgInstructionBarColor);
	_controlView.delegate = self;
	[_controlView startLoadingPageMenu];
}
- (void)setupFontAnColor{
	
}
- (void)setupLanguage{
	
}

#pragma mark - LoadData
- (void)loadData{
	[self registerNotification];
	if (_isLoadingNews) {
		[self startLoadingResearchNews];
	}else{
		[self startLoadingResearchReport];
	}
}

- (void)refreshData{
    [self loadData];
    [self.view dismissPrivateHUD];
    [self.contentView dismissPrivateHUD];
}

- (void)startLoadingResearchNews{
	[_items removeAllObjects];
	[_tblContent reloadData];
	[self.contentView removeStatusViewIfNeeded];
	[self.contentView showPrivateHUD];
	if ([UserPrefConstants singleton].isElasticNews) {
		[[ATPAuthenticate singleton] getElasticNewsForStocks:self.stock.stockCode forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
		_newType = NewsType_Elastic;
	}else if ([UserPrefConstants singleton].isArchiveNews) {
		// NSLog(@"Archive");
		_newType = NewsType_Archive;
		[[ATPAuthenticate singleton] getArchiveNewsForStock:self.stock.stockCode forDays:30];
	} else {
		//Stock News Company
		_newType = NewsType_StockCompany;
		[[VertxConnectionManager singleton] vertxCompanyNews:self.stock.stockCode];
	}
}

- (void)startLoadingResearchReport{
	[_items removeAllObjects];
	[_tblContent reloadData];
	[self.contentView removeStatusViewIfNeeded];
	if ([UserPrefConstants singleton].elasticCat.count > 0) {
		NSMutableArray *tempSourceArray = [[NSMutableArray alloc] init];
		NSMutableArray *_researchArr = @[].mutableCopy;
		for (ElasticNewsData *source in [UserPrefConstants singleton].elasticCat) {
			[tempSourceArray addObject:source.sourceString];
			NSString *stringInformation = [NSString stringWithFormat:@"%@|%@|%@|%@",source.sourceString,source.categoryLevel,source.categoryString,source.newsID];
			if ([source.sourceString.uppercaseString isEqualToString:@"CIMB"] || [source.sourceString.uppercaseString isEqualToString:@"HSC"]) {
				[_researchArr addObject:stringInformation];
			}
		}
		//Check if need call api
		if (_researchArr.count > 0) {
			//Call API to load market Research
			[self.contentView showPrivateHUD];
			[_researchArr enumerateTaskParallely:^(id obj, NSUInteger idx, BlockTaskCompletion completion) {
				NSString *researchStr = (NSString *)obj;
				NSString *source = [[researchStr componentsSeparatedByString:@"|"] objectAtIndex:0];
				NSString *category = [[researchStr componentsSeparatedByString:@"|"] objectAtIndex:2];
				//Call API
				[[ResearchAPI shareInstance] requestResearchFrom:[[NSDate date] dateBySubtractingDays:30] toDate:[NSDate date] source:source category:category keyword:[self.stock.stockCode stringByDeletingPathExtension] target:@"" completion:^(id result, NSError *error) {
					completion(nil);
					if (!error) {
						NSArray *_news = [self dataModelsFromResearchResult:result];
						[self->_items addObjectsFromArray:_news];
						dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
							NSArray *_news = [self dataModelsFromResearchResult:result];
							dispatch_async(dispatch_get_main_queue(), ^{
								[self->_items addObjectsFromArray:_news];
							});
						});
					}
				}];
			} blockCompleteAllTask:^{
				dispatch_async(dispatch_get_main_queue(), ^{
					//Update UI - Using meta data to show UI
					//NewsMetadata
					[self.contentView dismissPrivateHUD];
					[self->_tblContent reloadData];
					[self handleUIAfterLoadingData];
				});
			}];
		}else{
			[self handleUIAfterLoadingData];
		}
	}else{
		[self handleUIAfterLoadingData];
	}
}

#pragma mark - UTILS
- (void)handleUIAfterLoadingData{
	if (_items.count == 0) {
		NSString *message = _isLoadingNews ? [LanguageManager stringForKey:There_is_no_data] : [LanguageManager stringForKey:There_is_no_data];
		TCStatusInfoView *warningView = [[TCStatusInfoView alloc] initWithFrame:CGRectZero];
		[warningView showStatus:message inView:self.contentView retryAction:^{
			//Reload indices here
			[self performSelector:@selector(loadData) withObject:nil afterDelay:0.5];
		}];
	}else{
		[self.contentView removeStatusViewIfNeeded];
	}
}
//For qcDecompressionProcess
- (NSString *)qcDecompressionProcess:(NSString *)qcResponseStr {
	if ([UserPrefConstants singleton].isQCCompression) {
		NSRange range1 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)13]];
		NSRange range2 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%C", (unichar)129]];
		NSRange range3 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)30]];
		NSRange range4 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)10]];
		
		if (range1.location != NSNotFound && range2.location != NSNotFound && range3.location != NSNotFound && range4.location != NSNotFound) {
			qcResponseStr = [qcResponseStr substringWithRange:NSMakeRange(range3.location+1, [qcResponseStr length]-range3.location-2)];
			NSString *resultStr = [[NSString alloc] initWithData:[[qcResponseStr dataUsingEncoding:NSISOLatin1StringEncoding] zlibInflate] encoding:NSISOLatin1StringEncoding];
			return resultStr;
		}
		else {
			return qcResponseStr;
		}
	}
	else {
		return qcResponseStr;
	}
}
//+++ Parse result
- (NSArray *)dataModelsFromResearchResult:(NSString *)result{
	NSMutableArray *_news = @[].mutableCopy;
	NSString *content = [self qcDecompressionProcess:result];
	NSArray *lineArr = [content componentsSeparatedByString:@"\r\n"];
	//Find the index of [] and [END]
	int header = -1;
	int footer = -1;
	for (int j = 0; j < [lineArr count]; j++) {
		NSString *line = [lineArr objectAtIndex:j];
		if ([line isEqualToString:@"[]"]) {
			header = j;
		}
		if ([line isEqualToString:@"[END]"]) {
			footer = j;
		}
		if([line localizedCaseInsensitiveContainsString:@"[END]"]){
			footer = j;
		}
	}
	
	if (header != -1 && footer != -1) {
		for (int i = header+1; i < footer; i++) {
			NSString *dataRow = [lineArr objectAtIndex:i];
			NSArray *tokens = [dataRow componentsSeparatedByString:@","];
			if ([tokens count] >= 3) {
				NewsMetadata *nmd = [[NewsMetadata alloc] init];
				int i = 0;
				for (NSString *token in tokens) {
					switch (i) {
						case 0: nmd.newsID = token;
							break;
						case 1: nmd.codeID = token;
							break;
						case 2: nmd.date1 = token;
							break;
							
						case 4:
							//  DLog(@"NEWS Stock Token %@",token);
							nmd.stockcode = token;
							break;
						case 5: nmd.newsTitle = [token stripHtml];
							break;
						case 8: nmd.sourceNews = token;
						default:
							break;
					}
					i++;
				}
				
				BOOL found = NO;
				for (NewsMetadata *current_nmd in _news) {
					if ([current_nmd.newsID isEqualToString:nmd.newsID]) {
						found = YES;
					}
				}
				if (!found) {
					[_news addObject:nmd];
				}
			}
		}
		
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date1" ascending:NO];
		[_news sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	}
	return [_news copy];
}
- (NSMutableArray *)parseStockNewsFrom:(NSDictionary *)resultDict withType:(NewsType)type{
	NSArray *newsArray = @[].copy;
	switch (type) {
		case NewsType_Archive:{
			newsArray = [resultDict objectForKey:@"News"];
		}
			break;
		case NewsType_Jar:{
			newsArray = [resultDict objectForKey:@"data"];
		}
			break;
		case NewsType_StockCompany:{
			newsArray = [resultDict objectForKey:@"data"];
		}
			break;
			
		default:
			newsArray = [resultDict objectForKey:@"data"];
			break;
	}
	NSMutableArray *result = [[Utils parseNewsArrFrom:newsArray byType:type] mutableCopy];
	//Re-sort by Date
	[result sortUsingComparator:^NSComparisonResult(NewsModel *obj1, NewsModel *obj2) {
		NSDate *date1 = [NSDate dateFromString:obj1.timeStamp format:kSERVER_NEWS_FORMAT_DATE];
		NSDate *date2 = [NSDate dateFromString:obj2.timeStamp format:kSERVER_NEWS_FORMAT_DATE];
		return [date2 isLaterThan:date1];
	}];
	return [result mutableCopy];
}

#pragma mark - Add Notification
- (void)registerNotification{
	//Get News
	[self addNotification:kVertxNo_didFinishGetArchiveNews selector:@selector(didReceiveNewsNotification:)];
	[self addNotification:kVertxNo_didFinishGetArchiveNews selector:@selector(didReceiveNewsNotification:)];
	[self addNotification:kVertxNo_didFinishedGetStockNews selector:@selector(didReceiveNewsNotification:)];
}

#pragma mark - UITableViewDataSource
//For Header: +++ No need header now
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 44;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
	return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH_PORTRAIT, 44)];
	headerView.backgroundColor = COLOR_FROM_HEX(0x486174);
	UILabel *labelheader = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, SCREEN_WIDTH_PORTRAIT - 58, 44)];
	labelheader.backgroundColor = [UIColor clearColor];
	labelheader.textColor = [UIColor whiteColor];
	labelheader.font = AppFont_MainFontMediumWithSize(12);
	if (section == 0) {
		labelheader.text = [LanguageManager stringForKey:@"RESEARCH NEWS"];
	}else{
		labelheader.text = [LanguageManager stringForKey:@"RESEARCH REPORT"];
	}
	[headerView addSubview:labelheader];
	return headerView;
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return kHeightNewsCell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	ResearchCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResearchCell reuseIdentifier]];
	if (!cell) {
		cell = [[ResearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[ResearchCell reuseIdentifier]];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	[cell setupDataFrom:_items[indexPath.row]];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id model = _items[indexPath.row];
    DetailNewsVC *_newsVC = NEW_VC_FROM_NIB([DetailNewsVC class], [DetailNewsVC nibName]);
    _newsVC.newsModel = model;
	_newsVC.isResearchReports = !_isLoadingNews;
    [self.supperVC nextTo:_newsVC animate:YES];
}
#pragma mark - TCControlViewDelegate
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
	_isLoadingNews = !index; //Just 0 or 1
	[self loadData];
}
#pragma mark - Should Update Data
- (void)shouldUpdateData{
	//Reload data
	[self loadData];
}
#pragma mark - Handle Receive Notification
- (void)didReceiveNewsNotification:(NSNotification *)noti{
    NSDictionary *resultDict = noti.userInfo;
    if (!resultDict) {
        return;
    }
    
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		self->_items = [self parseStockNewsFrom:resultDict withType:self->_newType];
		dispatch_async(dispatch_get_main_queue(), ^{
			//For Last updated
			[self.contentView dismissPrivateHUD];
			[self->_tblContent reloadData];
            if (self->refreshControl) {
                [self->refreshControl endRefreshing];
            }
			//Check data has or not
			[self handleUIAfterLoadingData];
			[self removeAllNotification];
		});
	});
}

//Error Socket
- (void)didReceiveErrorSocketNotification:(NSNotification *)noti{
    if (self->refreshControl) {
        [self->refreshControl endRefreshing];
    }
    [self.contentView dismissPrivateHUD];
	[self.contentView removeStatusViewIfNeeded];
	if (!_socketStatusView) {
		NSError *error = noti.object;
		_socketStatusView = [[TCStatusInfoView alloc] initWithFrame:CGRectZero];
		[_socketStatusView showStatus:error.localizedDescription inView:_contentView retryAction:^{
			//Reload indices here
			[self performSelector:@selector(loadData) withObject:nil afterDelay:0.5];
		}];
	}
}
@end
