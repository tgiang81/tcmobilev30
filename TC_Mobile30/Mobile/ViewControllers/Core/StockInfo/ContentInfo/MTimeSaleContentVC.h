//
//  MTimeSaleContentVC.h
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PageStockContentInfoVC.h"

@interface MTimeSaleContentVC : PageStockContentInfoVC{
	
}
@property (weak, nonatomic) IBOutlet UIView *instructionBarView;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLastUpdated;
@property (weak, nonatomic) IBOutlet UIView *contentView;


@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPI;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblVol;

@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMode;
- (void)clearColor;
@end
