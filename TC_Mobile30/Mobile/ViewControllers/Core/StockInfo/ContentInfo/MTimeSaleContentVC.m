//
//  MTimeSaleContentVC.m
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MTimeSaleContentVC.h"
#import "MTimeSaleCell.h"
#import "QCData.h"
#import "VertxConnectionManager.h"
#import "NSDate+Utilities.h"
#import "UIView+TCUtil.h"
#import "SVPullToRefresh.h"

#import "StockModel.h"
#import "TimeSaleModel.h"
#import "SVPullToRefresh.h"
#import "LanguageKey.h"
#import "UIButton+TCUtils.h"

@interface MTimeSaleContentVC ()<UITableViewDelegate, UITableViewDataSource>{
	//HUD
	DGActivityIndicatorView *_hudActivity;
	NSMutableArray *_timeSaleArr;
	float _closedPrice;
	NSInteger _limit;
    UIRefreshControl *refreshControl;
	BOOL _isRefreshing;
}
@property (strong, nonatomic) SVPullToRefreshView *pullToRefreshBottom;
@end

@implementation MTimeSaleContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_timeSaleArr = @[].mutableCopy;
	_limit = 0;
	_isRefreshing = NO;
	
	[self createUI];
	[self loadData];
}

- (void)clearColor{
    self.view.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}
- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
	[self removeAllNotification];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)hideViewMode:(BOOL)isHidden{
    [_btnViewMode setHidden:isHidden];
    self.tblContent.scrollEnabled = isHidden;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self setupFontAnColor];
	//For tableView
	[self.tblContent registerNib:[UINib nibWithNibName:[MTimeSaleCell nibName] bundle:nil] forCellReuseIdentifier:[MTimeSaleCell reuseIdentifier]];
	//_tblContent.estimatedRowHeight = 30.0;
	//_tblContent.rowHeight = UITableViewAutomaticDimension;
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //add pull to refresh data at tableView
//    refreshControl = [[UIRefreshControl alloc] init];
//    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
//    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
//                                                                forKey:NSForegroundColorAttributeName];
//    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:[LanguageManager stringForKey:Refreshing_data______] attributes:attrsDictionary];
//    refreshControl.attributedTitle = attributedTitle;
//    if (@available(iOS 10.0, *)) {
//        self.tblContent.refreshControl = refreshControl;
//    } else {
//        // Fallback on earlier versions
//        [self.tblContent addSubview:refreshControl];
//    }
	
	[self updateTheme:nil];
	//Setup Pull Refresh
	[self setupPullRequest];
}
- (void)setupLanguage{
	_lblTitle.text = [LanguageManager stringForKey:Time___Sales];
    _lblTime.text=[LanguageManager stringForKey:Time];
    _lblPI.text=[LanguageManager stringForKey:@"Trade At"];
    _lblPrice.text=[LanguageManager stringForKey:Price];
    _lblVol.text=[LanguageManager stringForKey:@"Volume"];
}
- (void)setupFontAnColor{
	
}

- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	_instructionBarView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subHeaderBg);
	self.btnViewMode.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
}

#pragma mark - Pull To Refresh
- (void)setupPullRequest{
	[self createPullToRefresh];
	[self createPullToLoadMore];
}
- (void)createPullToRefresh{
	//AddPullToRefresh
	 __weak MTimeSaleContentVC *weakself = self;
	 [_tblContent addPullToRefreshWithActionHandler:^{
		//Reload Data here
		 self->_isRefreshing = YES;
		 [weakself refreshData];
	 }];
	 UIView *stopView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblContent.frame.size.width, 44)];
	 UIActivityIndicatorView *stopIN = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	 stopIN.center = stopView.center;
	 stopIN.hidesWhenStopped = NO;
	 [stopView addSubview:stopIN];
	 stopIN.color = [UIColor blackColor];
	 stopView.backgroundColor = [UIColor clearColor];
	[stopIN stopAnimating];
	
	UIView *loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblContent.frame.size.width, 44)];
	UIActivityIndicatorView *loadingIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	loadingIndicator.center = loadingView.center;
	loadingIndicator.hidesWhenStopped = NO;
	[loadingView addSubview:loadingIndicator];
	loadingIndicator.color = [UIColor blackColor];
	loadingView.backgroundColor = [UIColor clearColor];
	[loadingIndicator startAnimating];
	
	//[[self.tblContent pullToRefreshView] setCustomView:stopView forState:SVPullToRefreshStateTriggered];
	[[self.tblContent pullToRefreshView] setCustomView:loadingView forState:SVPullToRefreshStateStopped];
	[[self.tblContent pullToRefreshView] setCustomView:loadingView forState:SVPullToRefreshStateLoading];
}
//PUll to refhresh
- (void)createPullToLoadMore{
	//Load More
	MTimeSaleContentVC *weakSelf = self;
	if (!self.pullToRefreshBottom) {
		[self.tblContent addPullToRefreshWithActionHandler:^{
			[weakSelf startLoadingTimeSale];
			[self.view dismissPrivateHUD];
		} position:SVPullToRefreshPositionBottom];
		//Prepare custom value
		self.pullToRefreshBottom =  [self.tblContent pullToRefreshViewAtPosition:SVPullToRefreshPositionBottom];
	}
	//Check Should show or hide
	[self shouldShowOrHidePullToRefresh];
}
- (void)shouldShowOrHidePullToRefresh{
	if (_timeSaleArr.count > _limit || (_timeSaleArr.count >= _limit) || _timeSaleArr.count == 0) {
		_pullToRefreshBottom.hidden = YES;
	}else{
		_pullToRefreshBottom.hidden = NO;
	}
	
	//For Next
	[_pullToRefreshBottom setTitle:[LanguageManager stringForKey:Pull_up_load] forState:(SVPullToRefreshStateStopped)];
    NSString *nextPageTitle = [NSString stringWithFormat:@"%@", [LanguageManager stringForKey:Release_to_load_more_Time_Sale]];
	_pullToRefreshBottom.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	_pullToRefreshBottom.arrowColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	[_pullToRefreshBottom setTitle:nextPageTitle forState:(SVPullToRefreshStateTriggered)];
	[_pullToRefreshBottom setTitle:[LanguageManager stringForKey:Loading_more_Page______] forState:(SVPullToRefreshStateLoading)];
}

//For Pull to refresh
- (void)stopPullToRefresh{
	[self.tblContent stopAnimating];
	[_pullToRefreshBottom stopAnimating];
	[self shouldShowOrHidePullToRefresh];
}

#pragma mark - Action

- (IBAction)onSwitchViewMode:(id)sender {
	if (self.delegate) {
		[self.delegate shouldSwitchViewModeFrom:self.fullMode forController:self];
	}
}

#pragma mark - Configs
- (void)updateModeView{
	UIImage *modeImage;
	if (self.fullMode) {
		modeImage = [UIImage imageNamed:@"minimum_mode_icon"];		self.tblContent.scrollEnabled = YES;
	}else{
		modeImage = [UIImage imageNamed:@"full_mode_icon"];
		self.tblContent.scrollEnabled = NO;
	}
	[self.btnViewMode setImage:modeImage forState:UIControlStateNormal];
	[self.view setNeedsDisplay];
}

#pragma mark - UTILS VC
//Show HUD - Activity
- (void)showHudActivity{
	//Demo show activity on StockView
	if(!_hudActivity){
		_hudActivity = [[Utils shareInstance]createPrivateActivity];
	}
	[[Utils shareInstance] showActivity:_hudActivity inView:self.view];
}

- (void)hideHudActivity{
	[[Utils shareInstance] dismissActivity:_hudActivity];
}

- (NSMutableArray *)dataModelsFrom:(NSArray *)dictArr{
	if (!dictArr || dictArr.count == 0) {
		return @[].mutableCopy;
	}
	NSMutableArray *models = @[].mutableCopy;
	[dictArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		TimeSaleModel *model = [TimeSaleModel new];
		//Parse properties
		NSDictionary *dict = (NSDictionary *)obj;
		//+++ Note this: Can check config to show time
		NSString *time = dict[@"0"];
		if ([time containsString:self.stock.stockName.lastPathComponent]) {
			time = dict[@"9"];
		}
		model.time = time;
		model.PI = dict[@"1"];
		model.incomingPrice = [NSNumber numberWithFloat:[dict[@"2"] floatValue]];
		model.vol = [NSNumber numberWithLongLong:[dict[@"3"] longLongValue]];
		NSDate *date = [NSDate dateFromString:model.time format:kSERVER_FORMAT_TIME];
		if (!date) {
			date = [NSDate dateFromString:model.time format:@"yyyyMMdd"];
		}
		if (date) {
			//if date is validate -> allow add data
			[models addObject:model];
		}
	}];
	//Re-sort by Date
	[models sortUsingComparator:^NSComparisonResult(TimeSaleModel *obj1, TimeSaleModel *obj2) {
		NSDate *date1 = [NSDate dateFromString:obj1.time format:kSERVER_FORMAT_TIME];
		NSDate *date2 = [NSDate dateFromString:obj2.time format:kSERVER_FORMAT_TIME];
		return [date2 isLaterThan:date1];
	}];
	return models;
}

#pragma mark - LoadData
- (void)loadData{
	//Start checking data
	_closedPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:self.stock.stockCode]objectForKey:FID_50_D_CLOSE]floatValue];
	if (_limit == 0) {
		_limit = [[[[[QCData singleton]qcFeedDataDict]objectForKey:self.stock.stockCode ]objectForKey:FID_103_I_TRNS_NO] integerValue];
	}
	[self startLoadingTimeSale];
}

- (void)startLoadingTimeSale{
	NSInteger begin = [_timeSaleArr count];
	NSInteger end = ((_timeSaleArr.count + kNUMBER_ITEMS_PER_PAGE) > _limit) ? _limit : (_timeSaleArr.count + kNUMBER_ITEMS_PER_PAGE);
	[self getTimeSaleFrom:begin end:end];
}

- (void)getTimeSaleFrom:(NSInteger)begin end:(NSInteger)end{
	if (!_isRefreshing) {
		[self.view showPrivateHUD];
	}
	//Register notification
	[self addNotification:kDidFinishedTimeAndSalesNotification selector:@selector(didReceiveTimeSaleNotification:)];
	//handle API
	[self.view removeStatusViewIfNeeded];
	NSString *exchange = self.stock.stockCode.pathExtension;
	[[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:self.stock.stockCode begin:[@(begin) stringValue] end:[@(end) stringValue] exchange:exchange]; //20->tranNo
}

- (void)refreshData{
	[self getTimeSaleFrom:0 end:kNUMBER_ITEMS_PER_PAGE];
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 30.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_timeSaleArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MTimeSaleCell *cell = [tableView dequeueReusableCellWithIdentifier:[MTimeSaleCell reuseIdentifier]];
	if (!cell) {
		cell = [[MTimeSaleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MTimeSaleCell reuseIdentifier]];
	}
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	if (indexPath.row % 2 == 0) {
		cell.backgroundColor = RGB(247, 247, 247);
	}else{
		cell.backgroundColor = RGB(255, 255, 255);//TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellOddColor);
	}
	//Parsing data to UI
	[cell setupDataFrom:_timeSaleArr[indexPath.row] comparePrice:_closedPrice];
	return cell;
}

#pragma mark - Should Update Data
- (void)shouldUpdateData{
	//Reload data
	[_timeSaleArr removeAllObjects];
	[_tblContent reloadData];
	[self loadData];
}
#pragma mark - Handle Notification
- (void)didReceiveTimeSaleNotification:(NSNotification *)noti{
	[self removeNotification:kDidFinishedTimeAndSalesNotification];
	dispatch_async(dispatch_get_main_queue(), ^{
		NSDictionary * response = [noti.userInfo copy];
		NSArray *data =[response objectForKey:@"data"];
		//Parse data array to model array
		if (self->_isRefreshing) {
			[self->_timeSaleArr removeAllObjects];
		}
		//[self->_timeSaleArr addObjectsFromArray:[self dataModelsFrom:data]];
		NSArray *results = [self dataModelsFrom:data];
		[self->_timeSaleArr insertObjects:results atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, results.count)]];
		//For Last updated
		NSString *timeUpdate = [[NSDate date] stringWithFormat:kUPDATE_FORMAT_TIME];
		self->_lblLastUpdated.text = [NSString stringWithFormat:@"Last update: %@", timeUpdate];
		[self.tblContent reloadData];
		if (self->_timeSaleArr.count == 0) {
            [self.contentView showStatusMessage:[LanguageManager stringForKey:No_Data]];
		}
		[self stopPullToRefresh];
		[self.view dismissPrivateHUD];
	});
}
@end
