//
//  MBusinessDoneContentVC.h
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PageStockContentInfoVC.h"

@interface MBusinessDoneContentVC : PageStockContentInfoVC{
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLastUpdated;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBVol;
@property (weak, nonatomic) IBOutlet UILabel *lblSVol;
@property (weak, nonatomic) IBOutlet UILabel *lblBuy;
@property (weak, nonatomic) IBOutlet UILabel *lblTVol;
@property (weak, nonatomic) IBOutlet UILabel *lblTVal;
- (void)clearColor;
@end
