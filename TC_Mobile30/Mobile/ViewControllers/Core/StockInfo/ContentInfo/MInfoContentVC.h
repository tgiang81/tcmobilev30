//
//  FoundationContentVC.h
//  TCiPad
//
//  Created by Kaka on 6/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PageStockContentInfoVC.h"
@class TCBaseButton;
@interface MInfoContentVC : PageStockContentInfoVC{
	
}
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UIView *contentInfoView;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMode;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

//On RatingView
@property (weak, nonatomic) IBOutlet UIImageView *imgChartRating;
@property (weak, nonatomic) IBOutlet UIImageView *imgPointRating;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnFollow;

@end
