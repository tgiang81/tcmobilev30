//
//  MStockNewsContentVC.h
//  TCiPad
//
//  Created by Kaka on 7/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PageStockContentInfoVC.h"

@class TCPageMenuControl;
@interface MStockNewsContentVC : PageStockContentInfoVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet TCPageMenuControl *controlView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

//Passing data
@property (assign, nonatomic) BOOL isLoadingNews;
@end
