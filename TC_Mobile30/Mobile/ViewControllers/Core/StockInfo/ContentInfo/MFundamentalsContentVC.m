//
//  MFundamentalsContentVC.m
//  TC_Mobile30
//
//  Created by Kaka on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MFundamentalsContentVC.h"
#import "MFundamentalsVC.h"
#import "TCBaseButton.h"

@interface MFundamentalsContentVC (){
	
}
@end

@implementation MFundamentalsContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self updateTheme:nil];
}
- (void)setupLanguage{
	//[_btnFinacialReport setTitle:[LanguageManager stringForKey:@"Finacial Report"] forState:UIControlStateNormal];
	//[_btnCompanySynopis setTitle:[LanguageManager stringForKey:@"Company Synopsis"] forState:UIControlStateNormal];
	//[_btnCompanyKeyPerson setTitle:[LanguageManager stringForKey:@"Company Key persons"] forState:UIControlStateNormal];
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
}
#pragma mark - ACTIONS

- (IBAction)onFinancialReportAction:(id)sender {
	NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:4];
	MFundamentalsVC *_fundVC = NEW_VC_FROM_NIB([MFundamentalsVC class], [MFundamentalsVC nibName]);
	_fundVC.fundType = [LanguageManager stringForKey:@"Finacial Report"];
	_fundVC.fundURL = url;
	[self.supperVC nextTo:_fundVC animate:YES];
}

- (IBAction)onCompanySynopsisAction:(id)sender {
	NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:0];
	MFundamentalsVC *_fundVC = NEW_VC_FROM_NIB([MFundamentalsVC class], [MFundamentalsVC nibName]);
	_fundVC.fundType = [LanguageManager stringForKey:@"Synopsis"];
	_fundVC.fundURL = url;
	[self.supperVC nextTo:_fundVC animate:YES];
}

- (IBAction)onCompanyKeyPersonsAction:(id)sender {
	NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:1];
	MFundamentalsVC *_fundVC = NEW_VC_FROM_NIB([MFundamentalsVC class], [MFundamentalsVC nibName]);
	_fundVC.fundType = [LanguageManager stringForKey:@"Key persons"];
	_fundVC.fundURL = url;
	[self.supperVC nextTo:_fundVC animate:YES];
}

#pragma mark - Responsive Fundamentals (added Dec 2016)

//StkCode=7054.KL&SourceCode=FCS&Sponsor=59&bhCode=086&loginID=n2nuser&prodId=TCMOBILE
// &Type=5&key=%01%05%03%00%06%01&PackageID=0&Menu=N

/*Implement 23 Dec 2016
 
 PackageID
 To open specific package.
 √ 0 = Company Info
 √ 1 = Company Key Person
 √ 2 = Shareholding Summary
 √ 3 = Announcement
 √ 4 = Financial Report
 X 5 = Estimates
 X 6 = Supply Chain
 X 7 = Market Activity
 
 
 Menu
 To control hide/show header and menu bar
 Value put as "N" to hide, default will show
 
 */

- (NSString*)getResponsiveFundamentalDataURLWithPackageID:(int)PackageID {
	return [NSString stringWithFormat:@"%@rw.jsp?StkCode=%@&SourceCode=%@&Sponsor=%@&bhCode=%@&loginID=%@&prodId=TCMOBILE&Type=5&key=%@&PackageID=%d&Menu=N",
			[UserPrefConstants singleton].fundamentalNewsServer,
			self.stock.stockCode,
			[UserPrefConstants singleton].fundamentalSourceCode,
			[UserPrefConstants singleton].SponsorID,
			[UserPrefConstants singleton].brokerCode,
			[UserPrefConstants singleton].userName,
			[[UserPrefConstants singleton] encryptTime],
			PackageID];
}
@end
