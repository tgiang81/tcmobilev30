//
//  MFundamentalsContentVC.h
//  TC_Mobile30
//
//  Created by Kaka on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "PageStockContentInfoVC.h"

@interface MFundamentalsContentVC : PageStockContentInfoVC{
	
}
@property (weak, nonatomic) IBOutlet TCBaseButton *btnFinacialReport;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnCompanySynopis;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnCompanyKeyPerson;


@end
