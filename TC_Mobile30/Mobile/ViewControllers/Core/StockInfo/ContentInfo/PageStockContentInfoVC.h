//
//  PageStockContentInfoVC.h
//  TCiPad
//
//  Created by Kaka on 6/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

#define kCONTENT_INFO_CELL_HEIGHT	30
@class StockModel;
@class PageStockContentInfoVC;
@protocol PageStockContentInfoVCDelegate<NSObject>
@optional
- (void)shouldSwitchViewModeFrom:(BOOL)fullMode forController:(PageStockContentInfoVC *)controller;
@end
@interface PageStockContentInfoVC : BaseVC{
	
}
@property (assign, nonatomic) NSInteger pageIndex;
@property (strong, nonatomic) StockModel *stock;
@property (strong, nonatomic) BaseVC *supperVC;
@property (assign, nonatomic) BOOL fullMode;

//Passing data
@property (weak, nonatomic) id <PageStockContentInfoVCDelegate> delegate;
//Main Notification
- (void)shouldUpdateData;
- (void)shouldUpdateData:(StockModel *)oldStock new:(StockModel *)newStock;
//Mode View
- (void)updateModeView;
- (void)hideViewMode:(BOOL)isHidden;
@end
