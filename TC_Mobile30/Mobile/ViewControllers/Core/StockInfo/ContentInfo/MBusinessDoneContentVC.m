//
//  MBusinessDoneContentVC.m
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MBusinessDoneContentVC.h"
#import "MBusinessDoneCell.h"
#import "VertxConnectionManager.h"
#import "QCData.h"
#import "NSDate+Utilities.h"
#import "DGActivityIndicatorView.h"
#import "SVPullToRefresh.h"

#import "BusinessDoneModel.h"
#import "StockModel.h"
#import "LanguageKey.h"
#import "UIButton+TCUtils.h"
@interface MBusinessDoneContentVC ()<UITableViewDelegate, UITableViewDataSource>{
	NSMutableArray *_businessDoneArr;
	float _lacpValue;
	UIRefreshControl *refreshControl;
    
	//HUD
	DGActivityIndicatorView *_hudActivity;
}
@property (weak, nonatomic) IBOutlet UIView *instructionBarView;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMode;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation MBusinessDoneContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_businessDoneArr = @[].mutableCopy;
	[self createUI];
	[self loadData];
}
- (void)clearColor{
    self.view.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}
- (void)hideViewMode:(BOOL)isHidden{
    [_btnViewMode setHidden:isHidden];
    self.tblContent.scrollEnabled = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	[self setupFontAnColor];
	//For tableView
	[self.tblContent registerNib:[UINib nibWithNibName:[MBusinessDoneCell nibName] bundle:nil] forCellReuseIdentifier:[MBusinessDoneCell reuseIdentifier]];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	_tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //add pull to refresh data at tableView
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
                                                                forKey:NSForegroundColorAttributeName];
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..." attributes:attrsDictionary];
    refreshControl.attributedTitle = attributedTitle;
    if (@available(iOS 10.0, *)) {
        self.tblContent.refreshControl = refreshControl;
    } else {
        // Fallback on earlier versions
        [self.tblContent addSubview:refreshControl];
    }
    
	[self updateTheme:nil];
}
- (void)setupLanguage{
	_lblTitle.text = [LanguageManager stringForKey:Business_Done];
    _lblPrice.text=[LanguageManager stringForKey:Price];
    _lblBVol.text=[LanguageManager stringForKey:BVol];
    _lblSVol.text=[LanguageManager stringForKey:SVol];
    _lblBuy.text=[LanguageManager stringForKey:Buy];
    _lblTVol.text=[LanguageManager stringForKey:T__Vol];
    _lblTVal.text=[LanguageManager stringForKey:T__Val];
}
- (void)setupFontAnColor{
	
}

- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	_instructionBarView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].quote_subHeaderBg);
	self.btnViewMode.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
}

#pragma mark - Configs
- (void)updateModeView{
	UIImage *modeImage;
	if (self.fullMode) {
		modeImage = [UIImage imageNamed:@"minimum_mode_icon"];
		self.tblContent.scrollEnabled = YES;
	}else{
		modeImage = [UIImage imageNamed:@"full_mode_icon"];
		//[self.btnViewMode underLineText:@"View Full" color:self.btnViewMode.titleLabel.textColor];
		self.tblContent.scrollEnabled = NO;
	}
	[self.btnViewMode setImage:modeImage forState:UIControlStateNormal];
	[self.view setNeedsDisplay];
}
#pragma mark - UTILS VC
- (NSArray *)dataModelsFrom:(NSArray *)businessDictArr{
    return [Utils dataBDoneModelsFrom:businessDictArr];
}

//Show HUD - Activity
- (void)showHudActivity{
	//Demo show activity on StockView
	if(!_hudActivity){
		_hudActivity = [[Utils shareInstance]createPrivateActivity];
	}
	[[Utils shareInstance] showActivity:_hudActivity inView:self.view];
}

- (void)hideHudActivity{
	[[Utils shareInstance] dismissActivity:_hudActivity];
}
#pragma mark - LoadData
- (void)loadData{
	//Prepare Lacp value
	NSString *myBundleIdentifier = BUNDLEID_HARDCODE_TESTING;
	CGFloat lacpFloat = 0;
	if ([myBundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [myBundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
		lacpFloat = [[[[[QCData singleton] qcFeedDataDict]objectForKey:self.stock.stockCode]objectForKey:FID_153_D_THEORETICAL_PRICE]floatValue];
	}else{
		lacpFloat = [[[[[QCData singleton] qcFeedDataDict]objectForKey:self.stock.stockCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
	}
	_lacpValue = lacpFloat;//[[[[[QCData singleton] qcFeedDataDict]objectForKey:self.stock.stockCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
	
	[self addNotification:kdidFinishedBusinessDoneNotification selector:@selector(didFinishBizDoneNotification:)];
	[self showHudActivity];
	[self.view dismissPrivateHUD];
	[[VertxConnectionManager singleton] vertxGetBusinessDoneByStockCode:self.stock.stockCode exchg:self.stock.stockCode.pathExtension];
}

#pragma mark - Register Notification
- (void)registerNotification{
	//[self addNotification:kdidFinishedBusinessDoneNotification selector:@selector(didFinishBizDoneNotification:)];
}

#pragma mark - ACTION

- (IBAction)onSwitchViewMode:(id)sender {
	if (self.delegate) {
		[self.delegate shouldSwitchViewModeFrom:self.fullMode forController:self];
	}
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 30.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_businessDoneArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	MBusinessDoneCell *cell = [tableView dequeueReusableCellWithIdentifier:[MBusinessDoneCell reuseIdentifier]];
	if (!cell) {
		cell = [[MBusinessDoneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MBusinessDoneCell reuseIdentifier]];
	}
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	if (indexPath.row % 2 == 0) {
		cell.backgroundColor = RGB(247, 247, 247);
	}else{
		cell.backgroundColor = RGB(255, 255, 255);//TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellOddColor);
	}
	//Parse data to UI
	[cell setupDataFrom:_businessDoneArr[indexPath.row] checkPrice:_lacpValue];
	return cell;
}

#pragma mark - Should Update Data
- (void)shouldUpdateData{
	//Reload data
	[self loadData];
}

#pragma mark - Notification
- (void)didFinishBizDoneNotification:(NSNotification *)noti{
	[self removeNotification:kdidFinishedBusinessDoneNotification];
	
	//Reload data first
	[_businessDoneArr removeAllObjects];
	[_tblContent reloadData];
	//Parse data
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSDictionary * response = [noti.userInfo copy];
		NSArray *data =[response objectForKey:@"data"];
		if ([data count] >= 4){
			NSArray *businessDoneArr = [data subarrayWithRange:NSMakeRange(0, [data count] - 4)];
			//Parse data to model and reload content
			self->_businessDoneArr = [NSMutableArray arrayWithArray:[self dataModelsFrom:businessDoneArr]];
		}
		dispatch_async(dispatch_get_main_queue(), ^{
			[self->_tblContent reloadData];
            if (self->refreshControl) {
                [self->refreshControl endRefreshing];
            }
			//For Last updated
			NSString *timeUpdate = [[NSDate date] stringWithFormat:kUPDATE_FORMAT_TIME];
			self->_lblLastUpdated.text = [NSString stringWithFormat:@"Last update: %@", timeUpdate];
			[self hideHudActivity];
			if (self->_businessDoneArr.count == 0) {
				[self.contentView showStatusMessage:[LanguageManager stringForKey:No_Data] ];
			}
		});
	});
}
@end
