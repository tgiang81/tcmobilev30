//
//  ChartContentVC.h
//  TCiPad
//
//  Created by Kaka on 6/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PageStockContentInfoVC.h"
@class TCChartView;

@interface MChartContentVC : PageStockContentInfoVC{
	
}
@property (weak, nonatomic) IBOutlet TCChartView *chartView;

@end
