//
//  MMarketDepthContentVC.h
//  TCiPad
//
//  Created by Kaka on 7/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "PageStockContentInfoVC.h"

@interface MMarketDepthContentVC : PageStockContentInfoVC{
	
}
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *viewTotal;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblInsBidQty;
@property (weak, nonatomic) IBOutlet UILabel *lblInsBid;
@property (weak, nonatomic) IBOutlet UILabel *lblInsAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblInsAskQty;

@property (weak, nonatomic) IBOutlet UILabel *lblInsTotal;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalSplitBid;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalBidQty;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalBid;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAskQty;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalSplitAsk;

@property (weak, nonatomic) IBOutlet UILabel *lblSummaryBid;
@property (weak, nonatomic) IBOutlet UILabel *lblSummaryAsk;
@property (weak, nonatomic) IBOutlet UIView *viewInstructionBar;
@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblBidQty;
@property (weak, nonatomic) IBOutlet UILabel *lblBid;
@property (weak, nonatomic) IBOutlet UILabel *lblAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblAskQty;
- (void)clearColor;
@end
