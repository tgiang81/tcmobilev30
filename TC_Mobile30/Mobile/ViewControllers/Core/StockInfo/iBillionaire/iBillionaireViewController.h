//
//  iBillionaireViewController.h
//  TCiPhone_N2NMultiLogin_UAT_V1.4.5
//
//  Created by Ahmad Ashraf Azman on 30/10/2017.
//

#import <UIKit/UIKit.h>
#import "StockInfoModel.h"
#import "BaseVC.h"
@interface iBillionaireViewController : BaseVC{
    
}

@property (nonatomic, retain) NSString *stockCode;
@property (nonatomic, retain) NSString *exchangeCode;
@property (nonatomic, retain) NSString *trade_exchg_code;
@property (nonatomic, retain) NSString *feed_exchg_code;
@property (nonatomic, retain) NSString *stockExchangesCode;
@property (nonatomic, retain) NSString *stockCodeScreener;
@property (nonatomic, retain) NSString *exchangeCodeScreener;
@property (nonatomic, retain) NSString *mktDepthLevel;
@property (nonatomic, assign) StockInfoModel *stockData;

@end
