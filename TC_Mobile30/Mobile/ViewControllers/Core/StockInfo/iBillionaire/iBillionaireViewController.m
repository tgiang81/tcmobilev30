//
//  iBillionaireViewController.m
//  TCiPhone_N2NMultiLogin_UAT_V1.4.5
//
//  Created by Ahmad Ashraf Azman on 30/10/2017.
//

#import "iBillionaireViewController.h"
#import "NSData-AES.h"
#import "Base64.h"
#import "ExchangeData.h"
#import "UserDefaultData.h"
#import "LanguageManager.h"
#import "Utils.h"
#import "UserPrefConstants.h"
#import "MTradeViewController.h"
#import "LanguageKey.h"
@interface iBillionaireViewController () <UIWebViewDelegate>{
    IBOutlet UIWebView *webView;
    IBOutlet UIView *activityView;
    int currentButtonClicked;
}

@end

@implementation iBillionaireViewController
@synthesize stockCode, exchangeCode, trade_exchg_code;
@synthesize stockData;
@synthesize stockExchangesCode;
@synthesize stockCodeScreener;
@synthesize exchangeCodeScreener;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType{
    
    NSURL *url = [request URL];
    NSString *urlStr = url.absoluteString;
    
    DLog(@"url Str %@",url);
    
    NSString *protocolPrefixTrade = @"itradecimbtradebuttonclicked:";
    
    if([[urlStr lowercaseString] hasPrefix:protocolPrefixTrade]){
        
        DLog(@"url Str %@",urlStr);
        
        urlStr = [urlStr substringFromIndex:protocolPrefixTrade.length];
        
        NSArray *slashArrayCount = [urlStr componentsSeparatedByString:@"/"];
        if (slashArrayCount.count>=4) {
            self.stockCodeScreener = [[[[urlStr componentsSeparatedByString:@"/"] objectAtIndex:2] componentsSeparatedByString:@":"] objectAtIndex:1];
            
            self.exchangeCodeScreener = [[[[urlStr componentsSeparatedByString:@"/"] objectAtIndex:3] componentsSeparatedByString:@":"] objectAtIndex:1];
        }else{
            self.stockCodeScreener = [[[[urlStr componentsSeparatedByString:@"/"] objectAtIndex:1] componentsSeparatedByString:@":"] objectAtIndex:1];
            
            self.exchangeCodeScreener = [[[[urlStr componentsSeparatedByString:@"/"] objectAtIndex:2] componentsSeparatedByString:@":"] objectAtIndex:1];
        }
        
        [self clickTrade];
        // TODO
        
        // Open Stock Detail Page
        
        
        return NO;
    }
    
    return YES;
}

- (void) clickTrade{
    currentButtonClicked =0;
    activityView.alpha = 0;
    
    if ([self loadBuyAndTrade]) {
        MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
        controller.tradeType = TradeType_Buy;
        controller.actionTypeDict = @{};
        [self nextTo:controller animate:YES];
//        self.stockExchangesCode = [NSString stringWithFormat:@"%@.%@",self.stockCodeScreener,self.exchangeCodeScreener];
//
//        //Set Feed Exchange Code and Trade Exchange Code
//        self.feed_exchg_code = [Utils stripExchgCodeFromStockCode:stockExchangesCode];
//
//        NSString *_temp_exchg_code = [Utils appendDtoExchangeCode:self.exchangeCodeScreener];
//
//        DLog(@"temp exchange Code %@",_temp_exchg_code);
//        BOOL noExchanges = YES;
//
//        for (ExchangeData *ed in [UserPrefConstants singleton].userExchagelist) {
//            NSString *loopExchangeCode = ed.trade_exchange_code;
//            if ([loopExchangeCode isEqualToString:_temp_exchg_code]) {
//                self.feed_exchg_code = _temp_exchg_code;
//                noExchanges = NO;
//                break;
//            }
//        }
//
//        if (noExchanges) {
//             if ([UserPrefConstants singleton].isCIMBSG) {
//                 UIAlertView *tradeAlertView = [[UIAlertView alloc] initWithTitle:@"" message:[LanguageManager stringForKey:@"Market Data not found for this instrument. Unable to trade."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
//                 [tradeAlertView show];
//                 activityView.alpha = 0;
//             }else{
//                 UIAlertView *tradeAlertView = [[UIAlertView alloc] initWithTitle:@"" message:[LanguageManager stringForKey:@"Hi, kindly activate your Cross Border Trading account now. Thanks."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
//                 [tradeAlertView show];
//                 activityView.alpha = 0;
//             }
//            return;
//        }
//
//        DLog(@"feed exchange Code %@",self.feed_exchg_code);
//
//        ExchangeData *ed = [Utils getExchange:self.feed_exchg_code];
//
//        self.trade_exchg_code = ed.trade_exchange_code;
//        self.mktDepthLevel = ed.mktDepthLevel;
//
//
//        self.stockCode = [NSString stringWithFormat:@"%@.%@",self.stockCodeScreener,self.feed_exchg_code];
        
        ///need code
        
        
    }else{
        activityView.alpha = 0;
        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:@"You don't have trading account."]];
    }
    
}

// Original Push Stock Info View
- (BOOL) loadBuyAndTrade{
    
    BOOL isTradeActive = YES;
    
    if ([[UserPrefConstants singleton].userAccountlist count] == 0) {
        isTradeActive = NO;
    }
    
    return isTradeActive;
}


- (void)timeout{
    if ([webView isLoading]) {
        [webView stopLoading];//fire in didFailLoadWithError
    }
}


- (void)webViewDidStartLoad:(UIWebView *)webView{
    activityView.alpha = 1;
    //self.timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timeout) userInfo:nil repeats:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    activityView.alpha = 0;
    // [self.timer invalidate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *urlString=@"";
    
    if ([UserPrefConstants singleton].iBillionareStock.length > 0) {
       urlString = [NSString stringWithFormat:@"https://widgetcimb.ibillionaire.me/stock/%@/%@",[UserPrefConstants singleton].iBillionareStock,[LanguageManager getCurrentLanguage]];
    }else{
        urlString = [NSString stringWithFormat:@"https://widgetcimb.ibillionaire.me/%@",[LanguageManager getCurrentLanguage]];
    }
    DLog(@"Url String %@",urlString);
    //    NSString *urlString = [NSString stringWithFormat:@"www.weraku.com",[GlobalVariables sharedVariables].theScreenerLink,appData.userAccountInfo.senderCode,encodedString];
    
    NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [webView loadRequest:request];
    webView.scrollView.bounces = NO;
    
    [self.view addSubview:activityView];
    activityView.alpha = 1;
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    
    //Set Navigation buttons/title.
    [self setBackButtonDefault];
    //    [self createCustomBar:BarItemType_Left icon:@"back_white_icon" selector:@selector(fnDismiss:)];
    self.customBarTitle = [LanguageManager stringForKey:@"iBillionaire Reports"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webView:(UIWebView *)webView
didFailLoadWithError:(NSError *)error{
    activityView.alpha = 0;
    DLog(@" didFailLoadWithError error %@",error.localizedDescription);
    [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:@"Sorry, iBillionaire is not available at the moment. Please try later."] withOKAction:^{
        [self popView:YES];
    }];
    
}

- (void)viewDidLayoutSubviews {
}
    
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [webView stopLoading];
}
- (void)willMoveToParentViewController:(UIViewController *)parent{
    [super willMoveToParentViewController:parent];
    if (webView.delegate == self) {
        webView.delegate = nil;
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[LanguageManager defaultManager] translateStringsForView:activityView];
}

#pragma mark -
#pragma mark ASIHttpMarketDepthDelegate
//- (void) ASIHttpMarketDepthCallback:(ASIHttpMarketDepth *) request {

//    activityView.alpha = 0;
//    TradeViewController *tvc = [[TradeViewController alloc] initWithNibName:@"TradeView" bundle:nil];
//    tvc.title = @"Trade";
//    tvc.appData = appData;
//    tvc.stockData = stockData;
//    tvc.externalSource = @"IB";
//    tvc.marketDepthList = asiHttpMarketDepth.resultList;
//
//    // Value for Price textfield in Order Pad
//    //Auto-Fill Price is ON, set price to market depth first level sell/Ask price
//    if ([UserDefaultData getAutoFillPriceWithExchange:trade_exchg_code]) {
//        //Set default price to current price
//        tvc.default_price = stockData.price_current;
//
//        //If first bid price is available, replace it with bid price
//        if (stockData.firstBidPrice != 0) {
//            if (stockData.firstBidPrice == -999001 || stockData.firstBidPrice == 999001 || stockData.firstBidPrice == -999002 || stockData.firstBidPrice == 999002) {
//                tvc.default_price = 0;
//            }
//            else {
//                tvc.default_price = stockData.firstBidPrice;
//            }
//        }
//        else {
//            if (stockData.firstBidQty != 0) {
//                tvc.default_price = stockData.firstBidPrice;
//            }
//        }
//
//        //If first ask price is available, replace it with ask price
//        if (stockData.firstAskPrice != 0) {
//            if (stockData.firstAskPrice == -999001 || stockData.firstAskPrice == 999001 || stockData.firstAskPrice == -999002 || stockData.firstAskPrice == 999002) {
//                tvc.default_price = 0;
//            }
//            else {
//                tvc.default_price = stockData.firstAskPrice;
//            }
//        }
//        else {
//            if (stockData.firstAskQty != 0) {
//                tvc.default_price = stockData.firstAskPrice;
//            }
//        }
//    }
//    //Auto-Fill Price is OFF, set to 0
//    else {
//        tvc.default_price = 0;
//    }
//
//    // Value for Quantity textfield in Order Pad
//    //Auto-Fill Quantity is ON, set quantity to market depth first level sell/Ask quantity
//    if ([UserDefaultData getAutoFillQtyWithExchange:trade_exchg_code]) {
//        //if market depth first level ask qty is available
//        if (stockData.firstAskQty != 0) {
//            tvc.default_qty = stockData.firstAskQty;
//        }
//        //else get market depth first level bid qty
//        else {
//            tvc.default_qty = stockData.firstBidQty;
//        }
//    }
//    //Auto-Fill Quantity is OFF, set to custom quantity which is filled in by user
//    else {
//        tvc.default_qty = ([UserDefaultData getTradeQuantityUnitWithExchange:trade_exchg_code] == 0) ? (long long)[UserDefaultData getCustomFillQtyWithExchange:trade_exchg_code] : (long long)([UserDefaultData getCustomFillQtyWithExchange:trade_exchg_code] * stockData.lot_size);
//    }
//
//    [self.navigationController pushViewController:tvc animated:YES];
//    [tvc release];
//}

#pragma mark -
#pragma mark ASIHttpDataPullDelegate
//- (void) ASIHttpDataPullCallback:(ASIHttpDataPull *) request {
//
//    if (!request.connerror) {
//        DLog(@"resultList %d",(int)[asiDataPull.resultList count]);
//
//        if (currentButtonClicked==0) {
//            if ([asiDataPull.resultList count] == 1) {
//
//                stockData = [asiDataPull.resultList objectAtIndex:0];
//                asiHttpMarketDepth.appData = appData;
//                DLog(@"stockData %@",stockData.stock_code);
//                if (![asiHttpMarketDepth isRunning]) {
//
//                    if ([AppConfigManager isJavaQC]) {
//                        if ([AppConfigManager requestMarketDepthByLevel]) {
//
//                            [asiHttpMarketDepth doMarketDepthRequestForStockToJavaQC:stockData.stock_code byMktDepthLevel:[ExchangeHandler getMarketDepthLevelOfFeedExchgCode:self.feed_exchg_code]];
//                        }
//                        else {
//                            [asiHttpMarketDepth doMarketDepthRequestForStock:_feed_exchg_code];
//                        }
//                    }
//                    else {
//                        [asiHttpMarketDepth doMarketDepthRequestForStock:_feed_exchg_code];
//                    }
//                }
//
//                // DLog(@"asiHttpMarketDepth.resultList; %@",asiHttpMarketDepth.resultList);
//                [appData timeoutActivity];
//                return;
//            }else{
//                activityView.alpha = 0;
//                if (BUILD_TARGET_SCREENER==0) {
//                    UIAlertView *tradeAlertView = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"Stock Code not found."] message:@"Market Data not found for this instrument. Unable to trade." delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
//                    [tradeAlertView show];
//                    [tradeAlertView release];
//                }else{
//                    UIAlertView *tradeAlertView = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"Stock Code not found."] message:[LanguageManager stringForKey:@"Please go to menu and search by stock name or contact CIMB Online Trading at 03-22610888 for further assistance."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
//                    [tradeAlertView show];
//                    [tradeAlertView release];
//                }
//            }
//
//        }
//    }else{
//        activityView.alpha = 0;
//    }
//    [asiDataPull stop];
//}

@end
