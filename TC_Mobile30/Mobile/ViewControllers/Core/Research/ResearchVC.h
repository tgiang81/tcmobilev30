//
//  NewsVC.h
//  TCiPad
//
//  Created by Kaka on 5/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class TCPageMenuControl;
@class DashBoardCornerView;
@interface ResearchVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@property (weak, nonatomic) IBOutlet DashBoardCornerView *v_content;
@property (strong, nonatomic) NSMutableArray *items;
@property (weak, nonatomic) IBOutlet TCPageMenuControl *controlView;
- (void)reloadDataWith:(NSInteger)index;
- (void)didReloadData;
@end
