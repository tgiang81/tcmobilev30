//
//  NewsVC.m
//  TCiPad
//
//  Created by Kaka on 4/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DetailNewsVC.h"
#import "NewsModel.h"
#import "NewsMetadata.h"
#import "LanguageKey.h"
@interface DetailNewsVC ()<UIWebViewDelegate>{
	
}

@end

@implementation DetailNewsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self createUI];
	[self loadData];
}
- (int)findMissing:(NSArray *)values{
//	int missing = 1;
//	NSMutableArray *tmpArr = @[].mutableCopy;
//	for (int i = 0; i < values.count; i ++) {
//		int numberAtIndex = [values[i] intValue];
//		if (numberAtIndex > 0) {
//			[tmpArr addObject:@(numberAtIndex)];
//		}
//	}
//	while ([tmpArr containsObject:@(missing)]) {
//		missing ++;
//	}
//	return missing;
	NSArray *sortedArray = [values sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
		return [obj1 intValue] < [obj2 intValue];
	}];
	int missing = 1;
	for (int i = 0; i < sortedArray.count; i ++) {
		int aValue = [sortedArray[i] intValue];
		if (aValue > -1 && aValue == missing) {
			missing ++;
		}
	}
	return missing;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	self.title = [LanguageManager stringForKey:@"News"];
	[self setBackButtonDefault];
	_webView.delegate = self;
}

- (void)loadData{
	NSString *js =
	@"var meta = document.createElement('meta'); " \
	"meta.setAttribute( 'name', 'viewport' ); " \
	"meta.setAttribute( 'content', 'width = device-width, initial-scale=1.0, minimum-scale=0.2, maximum-scale=5.0; user-scalable=1;' ); " \
	"document.getElementsByTagName('head')[0].appendChild(meta)";
	[_webView stringByEvaluatingJavaScriptFromString: js];
	//Make URL
    NSString *urlStr;
    if([_newsModel isKindOfClass:[NewsModel class]]){
        self.title = [LanguageManager stringForKey:_News];
        urlStr = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress, ((NewsModel *)_newsModel).id];
    }else{
        self.title = [LanguageManager stringForKey:@"Research"];
        if(_isResearchReports){
            if ([UserPrefConstants singleton].ElasticNewsServerAddress.length<=5) {
                [UserPrefConstants singleton].ElasticNewsServerAddress = @"http://sgnews.cgs-cimb.com/gcNEWS/srvs/displayNews";
            }
            
            urlStr = [NSString stringWithFormat:@"%@?id=%@",[UserPrefConstants singleton].ElasticNewsServerAddress, ((NewsMetadata *)_newsModel).newsID];
        }else{
            urlStr = [NSString stringWithFormat:@"%@?t=7&nid=%@&ns=%@&ex=%@&spc=%@&bh=%@&appId=MI&rdt=P&ts=%@",[UserPrefConstants singleton].ElasticNewsServerAddress,((NewsMetadata *)_newsModel).newsID,[((NewsMetadata *)_newsModel).sourceNews uppercaseString],[UserPrefConstants singleton].userCurrentExchange,[UserPrefConstants singleton].SponsorID,[UserPrefConstants singleton].bhCode,[self getCurrentTimeStamp]];
        }
    }
    
//    ElasticNewsServerAddress
    if([_newsModel isKindOfClass:[NewsModel class]])
    {
        urlStr = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress, ((NewsModel *)_newsModel).id];
    }else if([_newsModel isKindOfClass:[NewsMetadata class]]) {
        urlStr = [NSString stringWithFormat:@"%@?id=%@",[UserPrefConstants singleton].ElasticNewsServerAddress, ((NewsMetadata *)_newsModel).newsID];
        
    }
	NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval: 30.0];
	[_webView loadRequest:request];
	[_webView setScalesPageToFit:NO];
}

#pragma mark - Util VC
#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView{
	[Utils showApplicationHUD];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
	[Utils dismissApplicationHUD];
}

- (NSString *) getCurrentTimeStamp{
    NSDate *todaysDate = [NSDate new];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *strDateTime = [formatter stringFromDate:todaysDate];
    return strDateTime;
}

@end
