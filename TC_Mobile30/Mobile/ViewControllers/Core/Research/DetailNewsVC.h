//
//  NewsVC.h
//  TCiPad
//
//  Created by Kaka on 4/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class NewsModel;
@class NewsMetadata;
@interface DetailNewsVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;

//Passing data
@property (strong, nonatomic) id newsModel;
@property (nonatomic) BOOL isResearchReports;
@end
