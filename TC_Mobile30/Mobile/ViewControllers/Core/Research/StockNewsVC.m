//
//  StockNewsVC.m
//  TCiPad
//
//  Created by Kaka on 5/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockNewsVC.h"
#import "NewsCell.h"
#import "ATPAuthenticate.h"
#import "VertxConnectionManager.h"
#import "DetailNewsVC.h"

//Model
#import "StockModel.h"
#import "NewsModel.h"


@interface StockNewsVC ()<UITableViewDataSource, UITableViewDelegate>{
	NSMutableArray *_listNews;
	NewsType _newType;
}
@end

@implementation StockNewsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//Initial data
	_listNews = @[].mutableCopy;
	//Add Notification
	[self registerNotification];
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	//Register NewsCell
	[self.tblContent registerNib:[UINib nibWithNibName:[NewsCell nibName] bundle:nil] forCellReuseIdentifier:[NewsCell reuseIdentifier]];
	self.tblContent.dataSource = self; self.tblContent.delegate = self;
	[self createCustomBar:BarItemType_Left icon:@"back_white_icon" selector:@selector(back:)];
	self.tblContent.backgroundColor = [UIColor clearColor];
	[self setupLanguage];
	[self setupFontAndColor];
	[self showStatusData:NO message:nil];
}
- (void)setupLanguage{
	self.customBarTitle = [LanguageManager stringForKey:@"Stock News"];
}

- (void)setupFontAndColor{
	_lblStatus.font = AppFont_MainFontMediumWithSize(20);
	_lblStatus.textColor = AppColor_StatusDataColor;
}
#pragma mark - LoadData
- (void)loadData{
	if ([UserPrefConstants singleton].isElasticNews) {
		_newType = NewsType_Elastic;
		[[ATPAuthenticate singleton] getElasticNewsForStocks:_stock.stockCode forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
	}else if ([UserPrefConstants singleton].isArchiveNews) {
		// NSLog(@"Archive");
		_newType = NewsType_Archive;
		[[ATPAuthenticate singleton] getArchiveNewsForStock:_stock.stockCode forDays:30];
	} else {
		//Stock News Company
		_newType = NewsType_StockCompany;
		[[VertxConnectionManager singleton] vertxCompanyNews:_stock.stockCode];
	}
}


#pragma mark - ACTIONS
- (void)back:(id)sender{
	[self popView:YES];
}
#pragma mark - Add Notification
- (void)registerNotification{
	//Get News
	[self addNotification:kVertxNo_didFinishGetArchiveNews selector:@selector(didReceiveNewsNotification:)];
	[self addNotification:kVertxNo_didFinishGetArchiveNews selector:@selector(didReceiveNewsNotification:)];
	[self addNotification:kVertxNo_didFinishedGetStockNews selector:@selector(didReceiveNewsNotification:)];
}

#pragma mark - UTILS
- (NSMutableArray *)parseStockNewsFrom:(NSDictionary *)resultDict withType:(NewsType)type{
	NSArray *newsArray = @[].copy;
	switch (type) {
		case NewsType_Archive:{
			newsArray = [resultDict objectForKey:@"News"];
		}
			break;
		case NewsType_Jar:{
			newsArray = [resultDict objectForKey:@"data"];
		}
			break;
		case NewsType_StockCompany:{
			newsArray = [resultDict objectForKey:@"data"];
		}
			break;
			
		default:
			newsArray = [resultDict objectForKey:@"data"];
			break;
	}
	return [[Utils parseNewsArrFrom:newsArray byType:type] mutableCopy];
}

- (void)showStatusData:(BOOL)shouldShow message:(NSString *)message{
	_tblContent.hidden = shouldShow;
	_lblStatus.hidden = !shouldShow;
	if (!message) {
		message = [LanguageManager stringForKey:@"NO DATA"];
	}
	_lblStatus.text = message;
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_listNews count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return kHeightNewsCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:[NewsCell reuseIdentifier]];
	if (!cell) {
		cell = [[NewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NewsCell reuseIdentifier]];
	}
	[cell setupUIByModel:_listNews[indexPath.row]];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	NewsModel *model = _listNews[indexPath.row];
	DetailNewsVC *_newsVC = NEW_VC_FROM_NIB([DetailNewsVC class], [DetailNewsVC nibName]);
	_newsVC.newsModel = model;
	[self nextTo:_newsVC animate:YES];
}


#pragma mark - Handle Receive Notification
- (void)didReceiveNewsNotification:(NSNotification *)noti{
	NSDictionary *resultDict = noti.userInfo;
	_listNews = [self parseStockNewsFrom:resultDict withType:_newType];
	[_tblContent reloadData];
	//Check data has or not
	[self showStatusData:(_listNews.count == 0) ? YES : NO message:nil];
}
@end
