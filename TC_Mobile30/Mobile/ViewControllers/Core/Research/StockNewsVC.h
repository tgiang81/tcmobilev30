//
//  StockNewsVC.h
//  TCiPad
//
//  Created by Kaka on 5/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@class StockModel;
@interface StockNewsVC : BaseVC{
	
}

@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

//Passing data
@property (strong, nonatomic) StockModel *stock;

@end
