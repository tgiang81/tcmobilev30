//
//  NewsVC.m
//  TCiPad
//
//  Created by Kaka on 5/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ResearchVC.h"
#import "ResearchCell.h"
#import "DetailNewsVC.h"
#import "ATPAuthenticate.h"
#import "ElasticNewsViewController.h"
#import "TCPageMenuControl.h"

#import "ElasticNewsData.h"
#import "NSDate+Utilities.h"
#import "ResearchAPI.h"
#import "NSData-Zlib.h"
#import "NSArray+SKTaskLoop.h"


#import "NewsMetadata.h"
#import "NewsModel.h"
#import "UIView+TCUtil.h"
#import "DashBoardCornerView.h"
#import "LanguageKey.h"
@interface ResearchVC ()<UITableViewDelegate, UITableViewDataSource, TCControlViewDelegate>{
	NewsType _newType;
	BOOL _isLoadingNews;
	
	//Activity
	DGActivityIndicatorView *_activityView;
}

@end

@implementation ResearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_items = @[].mutableCopy;
	_isLoadingNews = YES;

	[self createUI];
	//Load data
	[self loadData];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	//[self performSelectorOnMainThread:@selector(loadData) withObject:nil waitUntilDone:YES];
	//[self performSelector:@selector(loadData) withObject:nil afterDelay:5.0];
	//[self loadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self createLeftMenu];
	//Register NewsCell
	[self.tblContent registerNib:[UINib nibWithNibName:[ResearchCell nibName] bundle:nil] forCellReuseIdentifier:[ResearchCell reuseIdentifier]];
	self.tblContent.dataSource = self; self.tblContent.delegate = self;
	self.tblContent.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self setupLanguage];
	[self showStatusData:NO message:nil];
	
	//Control
    /*
     Edit by cuongnx(04/01/2019)
     Remmove label News and Research
    _controlView.itemWidth = 0.5 * self.view.frame.size.width;
    _controlView.dataSources = @[[LanguageManager stringForKey:@"News"], [LanguageManager stringForKey:@"Research"]];
    */
    // Add heightAnchor equal 0, if rollback code, comment this code or set value =44
    [self.controlView.heightAnchor constraintEqualToConstant:0].active = YES;
	_controlView.delegate = self;
	[_controlView startLoadingPageMenu];
}

- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:@"News"];
}

#pragma mark - LoadData
- (void)loadData{
	if (_isLoadingNews) {
        [self registerNotification];
		[self requestDataNews];
	}else{
		//[self showCustomAlertWithMessage:@"Coming soon..."];
		//Use later...
		[self requestDataResearch];
	}
}

- (void)requestDataNews{
	[Utils showApplicationHUD];
	[self showActivity];
	if ([UserPrefConstants singleton].isElasticNews) {
		[[ATPAuthenticate singleton] getElasticNewsForStocks:@"Home" forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
		_newType = NewsType_Elastic;
	}else if ([UserPrefConstants singleton].isArchiveNews) {
		// NSLog(@"Archive");
		_newType = NewsType_Archive;
		[[ATPAuthenticate singleton] getArchiveNewsForStock:@"Home" forDays:30];
	} else {
		//isJarNews = YES;
		_newType = NewsType_Jar;
		[[ATPAuthenticate singleton] getJarNews];
	}
}

- (void)requestDataResearch{
	[_items removeAllObjects];
	[_tblContent reloadData];
	if ([UserPrefConstants singleton].elasticCat.count > 0) {
		NSMutableArray *tempSourceArray = [[NSMutableArray alloc] init];
		NSMutableArray *_researchArr = @[].mutableCopy;
		for (ElasticNewsData *source in [UserPrefConstants singleton].elasticCat) {
			[tempSourceArray addObject:source.sourceString];
			NSString *stringInformation = [NSString stringWithFormat:@"%@|%@|%@|%@",source.sourceString,source.categoryLevel,source.categoryString,source.newsID];
			if ([source.sourceString.uppercaseString isEqualToString:@"CIMB"] || [source.sourceString.uppercaseString isEqualToString:@"HSC"]) {
				[_researchArr addObject:stringInformation];
			}
		}
		//Check if need call api
		if (_researchArr.count > 0) {
			//Call API to load market Research
			[self showActivity];
			[_researchArr enumerateTaskParallely:^(id obj, NSUInteger idx, BlockTaskCompletion completion) {
				NSString *researchStr = (NSString *)obj;
				NSString *source = [[researchStr componentsSeparatedByString:@"|"] objectAtIndex:0];
				NSString *category = [[researchStr componentsSeparatedByString:@"|"] objectAtIndex:2];
				//Call API
				[[ResearchAPI shareInstance] requestResearchFrom:[[NSDate date] dateBySubtractingDays:30] toDate:[NSDate date] source:source category:category keyword:@"" target:@"" completion:^(id result, NSError *error) {
					completion(nil);
					if (!error) {
						NSArray *_news = [self dataModelsFromResearchResult:result];
						[self->_items addObjectsFromArray:_news];
						dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
							NSArray *_news = [self dataModelsFromResearchResult:result];
							dispatch_async(dispatch_get_main_queue(), ^{
								[self->_items addObjectsFromArray:_news];
							});
						});
					}
				}];
			} blockCompleteAllTask:^{
				dispatch_async(dispatch_get_main_queue(), ^{
					//Update UI - Using meta data to show UI
					//NewsMetadata
					[self hideActivity];
					[self->_tblContent reloadData];
                    [self didReloadData];
					[self handleUIFromResult];
				});
			}];
		}else{
			[self handleUIFromResult];
		}
	}else{
		[self handleUIFromResult];
        [self didReloadData];
	}
}
#pragma mark - UTILS
- (void)handleUIFromResult{
	//Check data has or not
	[self showStatusData:(_items.count == 0) ? YES : NO message:nil];
	//+++ Post notification to notice that was finished loading data
	//Just post when had indexpath
	if (self.indexPath != nil) {
		ControllerModel *infoModel = [[ControllerModel alloc] init];
		infoModel.name = NSStringFromClass([self class]);
		infoModel.indexPath = self.indexPath;
		infoModel.heightContent = (_items.count == 0) ? 300 : ((kHeightNewsCell * _items.count) + 44);
		[self postNotification:kDidFinishLoadDataNotification object:infoModel];
	}
}
- (NSMutableArray *)parseStockNewsFrom:(NSDictionary *)resultDict withType:(NewsType)type{
	NSArray *newsArray = @[].copy;
	switch (type) {
		case NewsType_Archive:{
			newsArray = [resultDict objectForKey:@"News"];
		}
			break;
		case NewsType_Jar:{
			newsArray = [resultDict objectForKey:@"data"];
		}
			break;
		case NewsType_StockCompany:{
			newsArray = [resultDict objectForKey:@"data"];
		}
			break;
			
		default:
			newsArray = [resultDict objectForKey:@"data"];
			break;
	}
	return [[Utils parseNewsArrFrom:newsArray byType:type] mutableCopy];
}
- (void)registerNotification{
	//Get News
	[self addNotification:kVertxNo_didFinishedGetAllStockNews selector:@selector(didReceiveNewsNotification:)];
	[self addNotification:kVertxNo_didFinishGetElasticNews selector:@selector(didReceiveNewsNotification:)];
	[self addNotification:kVertxNo_didFinishGetArchiveNews selector:@selector(didReceiveNewsNotification:)];
	[self addNotification:kVertxNo_didReceiveJarNews selector:@selector(didReceiveNewsNotification:)];
}

- (void)showStatusData:(BOOL)shouldShow message:(NSString *)message{
	_tblContent.hidden = shouldShow;

    if (shouldShow) {
        [self.v_content showStatusMessage:[LanguageManager stringForKey:There_is_no_data]];
    }else{
        [self.v_content removeStatusViewIfNeeded];
    }
}

//+++ For target
- (NSString *)targetForElasticNew{
	NSString *target = @"";
	return target;
}

//For qcDecompressionProcess
- (NSString *)qcDecompressionProcess:(NSString *)qcResponseStr {
	if ([UserPrefConstants singleton].isQCCompression) {
		NSRange range1 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)13]];
		NSRange range2 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%C", (unichar)129]];
		NSRange range3 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)30]];
		NSRange range4 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)10]];
		
		if (range1.location != NSNotFound && range2.location != NSNotFound && range3.location != NSNotFound && range4.location != NSNotFound) {
			qcResponseStr = [qcResponseStr substringWithRange:NSMakeRange(range3.location+1, [qcResponseStr length]-range3.location-2)];
			NSString *resultStr = [[NSString alloc] initWithData:[[qcResponseStr dataUsingEncoding:NSISOLatin1StringEncoding] zlibInflate] encoding:NSISOLatin1StringEncoding];
			return resultStr;
		}
		else {
			return qcResponseStr;
		}
	}
	else {
		return qcResponseStr;
	}
}

//+++ Parse result
- (NSArray *)dataModelsFromResearchResult:(NSString *)result{
	NSMutableArray *_news = @[].mutableCopy;
	NSString *content = [self qcDecompressionProcess:result];
	NSArray *lineArr = [content componentsSeparatedByString:@"\r\n"];
	//Find the index of [] and [END]
	int header = -1;
	int footer = -1;
	for (int j = 0; j < [lineArr count]; j++) {
		NSString *line = [lineArr objectAtIndex:j];
		if ([line isEqualToString:@"[]"]) {
			header = j;
		}
		if ([line isEqualToString:@"[END]"]) {
			footer = j;
		}
		if([line localizedCaseInsensitiveContainsString:@"[END]"]){
			footer = j;
		}
	}
	
	if (header != -1 && footer != -1) {
		for (int i = header+1; i < footer; i++) {
			NSString *dataRow = [lineArr objectAtIndex:i];
			NSArray *tokens = [dataRow componentsSeparatedByString:@","];
			if ([tokens count] >= 3) {
				NewsMetadata *nmd = [[NewsMetadata alloc] init];
				int i = 0;
				for (NSString *token in tokens) {
					switch (i) {
						case 0: nmd.newsID = token;
							break;
						case 1: nmd.codeID = token;
							break;
						case 2: nmd.date1 = token;
							break;
							
						case 4:
							//  DLog(@"NEWS Stock Token %@",token);
							nmd.stockcode = token;
							break;
						case 5: nmd.newsTitle = [token stripHtml];
							break;
						case 8: nmd.sourceNews = token;
						default:
							break;
					}
					i++;
				}
				
				BOOL found = NO;
				for (NewsMetadata *current_nmd in _news) {
					if ([current_nmd.newsID isEqualToString:nmd.newsID]) {
						found = YES;
					}
				}
				if (!found) {
					[_news addObject:nmd];
				}
			}
		}
		
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date1" ascending:NO];
		[_news sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	}
	return [_news copy];
}

#pragma mark - Show/Hide Activity
//Show Activity
- (void)showActivity{
	//Demo show activity on StockView
	if(!_activityView){
		_activityView = [[Utils shareInstance]createPrivateActivity];
	}
	[[Utils shareInstance] showActivity:_activityView inView:self.view];
}

- (void)hideActivity{
	[[Utils shareInstance] dismissActivity:_activityView];
}
#pragma mark - UITableViewDataSource
//For Header
/* +++ No need header now
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return 44;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
	return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH_PORTRAIT, 44)];
	headerView.backgroundColor = COLOR_FROM_HEX(0x486174);
	UILabel *labelheader = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, SCREEN_WIDTH_PORTRAIT - 58, 44)];
	labelheader.backgroundColor = [UIColor clearColor];
	labelheader.textColor = [UIColor whiteColor];
	labelheader.font = AppFont_MainFontMediumWithSize(12);
	if (section == 0) {
		labelheader.text = [LanguageManager stringForKey:@"RESEARCH NEWS"];
	}else{
		labelheader.text = [LanguageManager stringForKey:@"RESEARCH REPORT"];
	}
	[headerView addSubview:labelheader];
	return headerView;
}
 
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return kHeightNewsCell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	ResearchCell *cell = [tableView dequeueReusableCellWithIdentifier:[ResearchCell reuseIdentifier]];
	if (!cell) {
		cell = [[ResearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[ResearchCell reuseIdentifier]];
	}
	[cell setupDataFrom:_items[indexPath.row]];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	NewsModel *model = _items[indexPath.row];
	DetailNewsVC *_newsVC = NEW_VC_FROM_NIB([DetailNewsVC class], [DetailNewsVC nibName]);
//    if ([model.sourceNews containsString:@"cimb"] ) {
        _newsVC.isResearchReports = YES;
//    }else{
//        _newsVC.isResearchReports = NO;
//    }
	_newsVC.newsModel = model;
	[self nextTo:_newsVC animate:YES];
}
#pragma mark - Notification
- (void)didReceiveNewsNotification:(NSNotification *)noti{
	dispatch_async(dispatch_get_main_queue(), ^{
		//Update UI
		[Utils dismissApplicationHUD];
		[self hideActivity];
		NSDictionary *resultDict = noti.userInfo;
        self->_items = [self parseStockNewsFrom:resultDict withType:self->_newType];
		[self->_tblContent reloadData];
        [self didReloadData];
		//Handle UI
		[self handleUIFromResult];
        [self removeAllNotification];
	});
}
- (void)didReloadData{
    
}
#pragma mark - TCControlViewDelegate
- (void)didSelectControlView:(TCPageMenuControl *)controlView atIndex:(NSInteger)index{
    [self reloadDataWith:index];
}
- (void)reloadDataWith:(NSInteger)index{
    _isLoadingNews = !index;
    [self loadData];
}
@end
