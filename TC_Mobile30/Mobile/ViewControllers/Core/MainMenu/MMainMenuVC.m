//
//  MMainMenuVC.m
//  TC_Mobile30
//
//  Created by Kaka on 11/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MMainMenuVC.h"
#import "AllCommon.h"
#import "UIViewController+TCUtil.h"
#import "TCBaseView.h"
#import "UIViewController+Alert.h"
#import "UILabel+FormattedText.h"


#import "MStockTrackerModeVC.h"
#import "MSettingVC.h"
#import "AccountsVC.h"
#import "TCCheckAuthVC.h"
#import "UIViewController+Popup.h"
#import "MPortfolioViewController.h"
#import "BaseTabBarViewController.h"
#import "AlertVC.h"
#import "BaseTableViewCell.h"
#import "QRCodeReaderViewController.h"
#import "MenuSectionModel.h"
#import "LeftMenuItemModel.h"
#import "ResearchVC.h"
#import "HelpVC.h"
#import "MenuSearchingCell.h"
#import "MSearchVC.h"
#import "MWatchlistVC.h"
#import "LanguageKey.h"
#import "DetailBrokerModel.h"
#import "BrokerModel.h"
#import "TCBaseImageView.h"
#import "UIImageView+TCUtil.h"
#import "BrokerManager.h"
#import "MDashBoardVCV3.h"
#import "AlertController.h"
#import "SettingAlertVC.h"
#import "MOrderBookVCNew.h"
#import "MarketsVC.h"
#import "MQuoteViewController.h"
#import "AssetManagementVC.h"
#import "MIndicesVC.h"
#import "M_IndicesVC.h"
#import "ApplicationUtils.h"
//Define list menu items
#define kDashboard @"kDashboard"
#define kQuoteScreen @"kQuoteScreen"
#define kWatchlist @"kWatchlist"
#define kNews @"kNews"
#define kMarketSummary @"kMarketSummary"
#define kIndices @"kIndices"
#define kOrderBook @"kOrderBook"
#define kPortfolio @"kPortfolio"
#define kStockNote @"kStockNote"
#define kStockAlert @"kStockAlert"
#define kMarketStreamer @"kMarketStreamer"
#define kIdeas @"kIdeas"
#define kIScreener @"kIScreener"
#define kIBillionaire @"kIBillionaire"
#define kQRScanner @"kQRScanner"
#define kSettings @"kSettings"
#define kHelp @"kHelp"
#define kLogout @"kLogout"

//For default Broker
#define kDefaultTextColor @"283674"
#define kDefaultPlaceholderColor @"939AB9"
#define kDefaultBgColor @"FFFFFF"

@class TCBaseImageView;

@interface MenuCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet TCBaseImageView *img;

@end

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end



@interface MMainMenuVC ()<UITableViewDelegate, TCCheckAuthVCDelegate>{
    UIViewController *_currentVC;
    NSArray *arrLeftMenuItems;
    UIColor *mainTextColor;
    AlertController *_notificationControler;
}
//OUTLETS

@property (weak, nonatomic) IBOutlet UILabel *lblBrokerName;
@property (weak, nonatomic) IBOutlet UILabel *lblUser;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanySupplier;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet TCBaseView *avatarView;
@property (weak, nonatomic) IBOutlet TCBaseView *brokerView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBrokerLogo;
@property (weak, nonatomic) IBOutlet TCBaseView *lineView;
@property (weak, nonatomic) IBOutlet TCBaseView *blankView;

@property (weak, nonatomic) IBOutlet UILabel *lblDashboard;
@property (weak, nonatomic) IBOutlet UILabel *lblPortfolio;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderbook;
@property (weak, nonatomic) IBOutlet UILabel *lblSetting;
@property (weak, nonatomic) IBOutlet UILabel *lblEsettlement;
@property (weak, nonatomic) IBOutlet UILabel *lblBroker;
@property (weak, nonatomic) IBOutlet UILabel *lblReport;
@property (weak, nonatomic) IBOutlet UILabel *lblShortcut;
@property (weak, nonatomic) IBOutlet UILabel *lblStockStreamer;

@end

@implementation MMainMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _notificationControler = [AlertController new];
    [self addNotification:kDidChangeThemeNotification selector:@selector(updateTheme:)];
    //==== Register Notification: Did select Broker
    [self addNotification:kNotification_DidSelectBroker selector:@selector(didSelectBrokerNotification:)];
    [self registerNotifications];
    [self createUI];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - CreateUI
- (void)createUI{
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView setBounces:NO];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //For Theme
    [self.tableView registerNib:[UINib nibWithNibName:[MenuSearchingCell nibName] bundle:nil] forCellReuseIdentifier:[MenuSearchingCell reuseIdentifier]];
    [self updateTheme:nil];
    [self setupLanguage];
    [self selectCachedBroker];
}
- (void)calculateActives{
    NSMutableArray *tmpArray = [NSMutableArray new];
    for(MenuSectionModel *model in arrLeftMenuItems){
        if(!model.isHidden){
            NSMutableArray *subItems = [NSMutableArray new];
            for(NSDictionary *dic in model.items){
                LeftMenuItemModel *menuItem = [[LeftMenuItemModel alloc] initWithDictionary:dic error:nil];
                if(!menuItem.isHidden){
                    [subItems addObject:menuItem];
                }
            }
            model.items = subItems;
            [tmpArray addObject:model];
        }
    }
    arrLeftMenuItems = tmpArray;
}

- (void)updateTheme:(NSNotification *)noti{
    //    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    //    self.infoView.backgroundColor =  TC_COLOR_FROM_HEX(@"F7F7F7");
    //    self.avatarView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
}

- (void)setupLanguage{
    //	_lblDashboard.text = [LanguageManager stringForKey:@"Dashboard"];
    //	_lblPortfolio.text = [LanguageManager stringForKey:@"Portfolio"];
    //	_lblOrderbook.text = [LanguageManager stringForKey:@"Orderbook"];
    //	_lblSetting.text = [LanguageManager stringForKey:@"Settings"];
    //	_lblEsettlement.text = [LanguageManager stringForKey:@"E-Settlement"];
    //	_lblBroker.text = [LanguageManager stringForKey:@"BROKER"];
    //	_lblReport.text = [LanguageManager stringForKey:@"Reports"];
    //
    //	_lblShortcut.text = [LanguageManager stringForKey:@"SHORTCUT"];
    //	_lblStockStreamer.text = [LanguageManager stringForKey:@"Market Streamer"];
}
#pragma mark Notifications
- (void)registerNotifications{
    NSNotificationCenter *notCenter = [NSNotificationCenter defaultCenter];
    [notCenter removeObserver:self name:kNotiOrderBook object:nil];
    [notCenter addObserver:self selector:@selector(openOrderBook) name:kNotiOrderBook object:nil];
    
    [notCenter removeObserver:self name:kNotiQuote object:nil];
    [notCenter addObserver:self selector:@selector(openQuote) name:kNotiQuote object:nil];
    
    
    [notCenter removeObserver:self name:kNotiPortFolio object:nil];
    [notCenter addObserver:self selector:@selector(openPortfolio) name:kNotiPortFolio object:nil];
}
- (void)openQuote{
    [self openMenuItemType:QuoteScreen];
}
- (void)openOrderBook{
    [self openMenuItemType:OrderBook];
}

- (void)openPortfolio{
    [self openMenuItemType:Portfolio];
}


#pragma mark - LoadData
- (void)loadData{
    _lblUser.text = [NSString stringWithFormat:@"%@", [UserSession shareInstance].userName];
    //Read leftmenu config file....-> push to LMS server also
    NSString *aFilePath;
    
    //if is SSI -> choose menu for SSI
    BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
    NSUInteger brokerTag = brokerModel.tag;
    
    switch (brokerTag) {
        case BrokerTag_SSI:
        {
            aFilePath = [[NSBundle mainBundle] pathForResource:@"left_menu_SSI" ofType:@"json"];
        }
            break;
            
        default:
            aFilePath = [[NSBundle mainBundle] pathForResource:@"left_menu" ofType:@"json"];
            break;
    }
    
    NSString *aJsonContent = [NSString stringWithContentsOfFile:aFilePath encoding:NSUTF8StringEncoding error:nil];
    NSData *aData = [aJsonContent dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];;;
    arrLeftMenuItems = [MenuSectionModel arrayOfModelsFromData:aData error:nil];
    [self calculateActives];
    
    
    [_notificationControler getSettingList:^(BOOL success, id responseObject, NSError *error) {
        [self postNotification:kNotification_DidLoadNotificationSetting object:nil];
        if(error){
            DLog(@"ERROR_Get Alert Setting List____%@", error.localizedDescription);
            //            [self showAlert:TC_Pro_Mobile message:error.localizedDescription];
        }else{
            
            [SettingManager shareInstance].isOnNotification = [self->_notificationControler isEnableNotification];
        }
    }];
    [self.tableView reloadData];
}

#pragma mark - UTILS arrLeftMenuItems
- (NSString *)titleHeaderInSection:(NSInteger)section{
    if(section == MenuSection_Searching){
        return @"";
    }
    MenuSectionModel *menu = arrLeftMenuItems[section-1];
    if(menu){
        return menu.name;
    }
    return @"";
}

//Selected Broker
- (void)setUIFromSelectedBroker{
    BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
    NSUInteger brokerTag = brokerModel.tag;
    mainTextColor = TC_COLOR_FROM_HEX(kDefaultTextColor);
    switch (brokerTag) {
        case BrokerTag_N2N:
            mainTextColor = TC_COLOR_FROM_HEX(@"293874");
            [_imgBrokerLogo setImage:[UIImage imageNamed:@"AppLogoTCPRO.png"]];
            _lblBrokerName.text = @"TCPRO \nMobile";
            [_lblBrokerName colorSubString:@"TCPRO" withColor:TC_COLOR_FROM_HEX(@"404040")];
            [_lblBrokerName colorSubString:@"Mobile" withColor:mainTextColor];
            _lblCompanySupplier.hidden = NO;
            _lblCompanySupplier.text = @"By N2N Connect";
            [_lblBrokerName boldSubstring:@"TCPRO"];
            [_lblCompanySupplier boldSubstring:@"N2N Connect"];
            [_lineView setBackgroundColor:mainTextColor];
            break;
        case BrokerTag_SSI:
            mainTextColor = TC_COLOR_FROM_HEX(@"E02831");
            [_imgBrokerLogo setImage:[UIImage imageNamed:@"AppLogoSSI.png"]];
            _lblBrokerName.text = @"SAIGON \nSECURITY";
            [_lblBrokerName boldSubstring:@"SAIGON"];
            [_lblBrokerName setTextColor:mainTextColor];
            _lblCompanySupplier.hidden = YES;
            [_lineView setBackgroundColor:mainTextColor];
            break;
        case BrokerTag_AffinHwang:
            mainTextColor = TC_COLOR_FROM_HEX(@"26456B");
            [_imgBrokerLogo setImage:[UIImage imageNamed:@"AppLogoAFFINHWANG.png"]];
            _lblBrokerName.text = @"AFFIN HWANG \nCAPITAL";
            [_lblBrokerName colorSubString:@"AFFIN HWANG" withColor:mainTextColor];
            [_lblBrokerName colorSubString:@"CAPITAL" withColor:TC_COLOR_FROM_HEX(@"41A1D6")];
            _lblCompanySupplier.hidden = YES;
            [_lineView setBackgroundColor:mainTextColor];
            break;
        case BrokerTag_CIMB:
            mainTextColor = TC_COLOR_FROM_HEX(@"961E34");
            [_imgBrokerLogo setImage:[UIImage imageNamed:@"AppLogoCIMBBANK.png"]];
            _lblBrokerName.text = @"CIMB BANK";
            [_lblBrokerName setTextColor:mainTextColor];
            [_lblBrokerName boldSubstring:@"CIMB"];
            _lblCompanySupplier.hidden = YES;
            [_lineView setBackgroundColor:mainTextColor];
            break;
            
        default:
            mainTextColor = TC_COLOR_FROM_HEX(@"293874");
            [_imgBrokerLogo setImage:[UIImage imageNamed:@"AppLogoTCPRO.png"]];
            _lblBrokerName.text = @"TCPRO \nMobile";
            [_lblBrokerName colorSubString:@"TCPRO" withColor:TC_COLOR_FROM_HEX(@"404040")];
            [_lblBrokerName colorSubString:@"Mobile" withColor:mainTextColor];
            _lblCompanySupplier.hidden = NO;
            _lblCompanySupplier.text = @"By N2N Connect";
            [_lblBrokerName boldSubstring:@"TCPRO"];
            [_lblCompanySupplier boldSubstring:@"N2N Connect"];
            [_lineView setBackgroundColor:mainTextColor];
            break;
    }
    [_blankView setBackgroundColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor)];
}

- (void)selectCachedBroker{
    [self setUIFromSelectedBroker];
}

#pragma mark - Main Notification
- (void)didSelectBrokerNotification:(NSNotification *)noti{
    [self setUIFromSelectedBroker];
}

#pragma mark - Actions

- (IBAction)onLogoutAction:(id)sender {
    [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:Are_you_sure_want_to_log_out] withOKAction:^{
        [AppDelegateAccessor logout];
    } cancelAction:^{
        //Do nothing
    }];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrLeftMenuItems.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == MenuSection_Searching){
        return 1;
    }
    return ((MenuSectionModel *)arrLeftMenuItems[section-1]).items.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == MenuSection_Searching){
        return 64;
    }
    return 44;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].bgCellColor);
}

- (UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath {
    if(indexPath.section == MenuSection_Searching){
        MenuSearchingCell *cell= [tableView dequeueReusableCellWithIdentifier:[MenuSearchingCell reuseIdentifier] forIndexPath:indexPath];
        return cell;
    }else{
        MenuSectionModel *leftMenu = arrLeftMenuItems[indexPath.section-1];
        LeftMenuItemModel *menuItem = leftMenu.items[indexPath.row];
        MenuCell *cell= [tableView dequeueReusableCellWithIdentifier:@"MenuCell_ID" forIndexPath:indexPath];
        cell.lbTitle.text =[LanguageManager stringForKey:menuItem.name];
        if([menuItem.menuId integerValue] == MarketStreamer){
            NSString *temp=![[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"PH"]? @"Market Streamer" : @"Stock Tracker";
            cell.lbTitle.text =[LanguageManager stringForKey:temp];
        }
        cell.img.image = [UIImage imageNamed:menuItem.image];
        [cell.img toColor:mainTextColor];
        if (menuItem.enable) {
            cell.contentView.alpha = 1.0;
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        }else{
            cell.contentView.alpha = 0.3;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    }
    
}

//FOR HEADER
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == MenuSection_Searching){
        return 1;
    }
    return 44;
}
/*
 - (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
 UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
 headerView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
 return headerView;
 }
 */

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    header.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, header.frame.size.width - 30, header.frame.size.height)];
    lblHeader.text = [self titleHeaderInSection:section];
    lblHeader.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
    lblHeader.font = AppFont_MainFontBoldWithSize(14);
    [header addSubview:lblHeader];
    
    //Add Baseline
    if(section != MenuSection_Searching + 1){
        UIImageView *baseLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, header.frame.size.width, 0.5)];
        baseLine.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].baselineColor);
        [header addSubview:baseLine];
    }
    return header;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == MenuSection_Searching){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self openMenuItem:indexPath];
    }else{
        MenuSectionModel *leftMenu = arrLeftMenuItems[indexPath.section - 1];
        LeftMenuItemModel *menuItem = leftMenu.items[indexPath.row];
        if (menuItem.enable == NO) {
            return;
        }
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self openMenuItem:indexPath];
    }
}
- (void)openMenuItemType:(MenuItemType)type{
    id controller;
    switch (type) {
        case Searching:{
            controller = NEW_VC_FROM_STORYBOARD(kMSearchStoryboardName, [MSearchVC storyboardID]);
            ((MSearchVC *)controller).isShowMenuIcon = YES;
            break;
        }
        case HomeScreen:{
            [AppDelegateAccessor openNewContent:controller atType:type];
            break;
        }
        case Dashboard:{
            controller = NEW_VC_FROM_NIB([MDashBoardVCV3 class], [MDashBoardVCV3 nibName]);
            //BaseTabBarViewController *tabbar = (BaseTabBarViewController *)controller;
            //[tabbar setSelectedIndex:TabbarItemView_Home];
        }
            break;
        case QuoteScreen:{
            controller = NEW_VC_FROM_STORYBOARD(kMQuoteStoryboardName, [MQuoteViewController storyboardID]);
            //            controller = NEW_VC_FROM_STORYBOARD(kMQuoteStoryboardName, [MQuoteViewController storyboardID]);
            //            BaseTabBarViewController *tabbar = (BaseTabBarViewController *)controller;
            //            [tabbar setSelectedIndex:TabbarItemView_Quote];
            
        }
            break;
            
        case Watchlist:{
            controller = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [MWatchlistVC storyboardID]);
        }
            break;
        case News:
            controller = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [ResearchVC storyboardID]);
            break;
        case MarketSummary:
            break;
        case Indices:{
			controller = NEW_VC_FROM_STORYBOARD(kMIndicesStoryboardName, [M_IndicesVC storyboardID]);
            //((BaseVC *)controller).isHome = [BrokerManager shareInstance].broker.tag != BrokerTag_SSI;
        }
            break;
        case OrderBook:
            controller = NEW_VC_FROM_NIB([MOrderBookVCNew class], @"MOrderBookVCNew");
            break;
        case Portfolio:
            controller = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [MPortfolioViewController storyboardID]);
            break;
        case StockNote:
            break;
        case StockAlert:
            //            if([SettingManager shareInstance].isOnNotification){
            controller = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [AlertVC storyboardID]);
            //            }else{
            //                [self showCustomAlert:@"Push Notification Service Disable" message:@"Please activate Push Notification in Settings" okTitle:@"Settings" withOKAction:^{
            //
            //                    id controller = NEW_VC_FROM_STORYBOARD(kSettingStoryboardName, [MSettingVC storyboardID]);
            //                    ((MSettingVC *)controller).notificationControler = self->_notificationControler;
            //                    [AppDelegateAccessor openNewContent:controller atType:Settings];
            //                } cancelTitle:@"Cancel" cancelAction:^{
            //
            //                }];
            //            }
            
            break;
        case MarketStreamer:{
            controller = NEW_VC_FROM_NIB(MStockTrackerModeVC, @"MStockTrackerModeVC");
            ((MStockTrackerModeVC *)controller).isSteamerMode = ![[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"PH"];
        }
            break;
        case IScreener:
            break;
        case IBillionaire:
            break;
        case QRScanner:
        {
            controller = NEW_VC_FROM_NIB(QRCodeReaderViewController, @"QRCodeReaderViewController");
            [self presentViewController:controller animated:YES completion:nil];
        }
            return;
        case Settings:
        {
            controller = NEW_VC_FROM_STORYBOARD(kSettingStoryboardName, [MSettingVC storyboardID]);
            ((MSettingVC *)controller).notificationControler = _notificationControler;
        }
            break;
        case Help:{
            if(([BrokerManager shareInstance].broker.tag = BrokerTag_SSI)){
                [ApplicationUtils open:[UserPrefConstants singleton].termOfUseLink];
            }else{
                controller = NEW_VC_FROM_NIB(HelpVC, @"HelpVC");
                [self presentViewController:controller animated:YES completion:nil];
            }
            
        }
            return;
        case Logout:{
            [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:Are_you_sure_want_to_log_out] withOKAction:^{
                [AppDelegateAccessor logout];
            } cancelAction:^{
                //Do nothing
            }];
            return;
        }
        case AccountInformation:
            [self showCustomAlertWithMessage:[NSString stringWithFormat:@"Hello, %@",_lblUser.text]];
            return;
            break;
        case AssetManagement:{
            controller = NEW_VC_FROM_STORYBOARD(kAssetStoryboardName, [AssetManagementVC storyboardID]);
        }
            break;
        default:
            break;
    }
    _currentVC = controller;
    if([controller isKindOfClass:[MPortfolioViewController class]] || [controller isKindOfClass:[MOrderBookVCNew class]]){
        if([self checkAuth]) {
            [AppDelegateAccessor openNewContent:controller atType:type];
        };
    }else{
        if(controller){
            [AppDelegateAccessor openNewContent:controller atType:type];
        }
    }
}
- (void)openMenuItem:(NSIndexPath *)indexPath{
    if(indexPath.section == MenuSection_Searching){
        [self openMenuItemType:Searching];
    }else{
        MenuSectionModel *menu = arrLeftMenuItems[indexPath.section-1];
        LeftMenuItemModel *item = menu.items[indexPath.row];
        MenuItemType type = [item.menuId integerValue];
        [self openMenuItemType:type];
    }
}

- (IBAction)showAccountViews:(UIButton *)sender {
    AccountsVC *_previewVC = NEW_VC_FROM_NIB([AccountsVC class], [AccountsVC nibName]);
    _previewVC.isShowMenu = YES;
    _previewVC.CallBackValue = ^(UserAccountClientData *account) {
        [UserPrefConstants singleton].userSelectedAccount = account;
    };
    [AppDelegateAccessor replaceCenterContainerToVC:_previewVC];
}


- (BOOL)checkAuth{
    if(![UserPrefConstants singleton].isAuthOrderBook && [UserPrefConstants singleton].isAuthByTouchId){
        TCCheckAuthVC *_checkAuthVC = NEW_VC_FROM_NIB([TCCheckAuthVC class], [TCCheckAuthVC nibName]);
        _checkAuthVC.delegate = self;
        CGSize contentSize = CGSizeMake(300, 200);
        [self showPopupWithContent:_checkAuthVC inSize:contentSize completion:^(MZFormSheetController *formSheetController) {
        }];
        return NO;
    }else{
        return YES;
    }
}
#pragma mark TCCheckAuthVCDelegate
- (void)didSelectCancel{
    
}
- (void)authCompletion{
    [AppDelegateAccessor openNewContent:_currentVC atType:Portfolio];
}
@end
