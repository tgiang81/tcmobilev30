//
//  MMainMenuVC.h
//  TC_Mobile30
//
//  Created by Kaka on 11/3/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum: NSInteger {
    MenuSection_Searching = 0,
    MenuSection_Normal = 1
}
MenuSection;
@interface MMainMenuVC : UITableViewController

@end
