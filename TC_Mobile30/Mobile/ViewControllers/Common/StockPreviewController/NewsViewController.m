//
//  NewsViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "NewsViewController.h"
#import "CustomCollectionViewLayout.h"
#import "QCData.h"
#import "StockModel.h"
#import "StockDetaiCell.h"
//#import "MStockPreviewController.h"

@interface NewsViewController ()
{
    QCData* myQcData;
}

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = StockNews;
    aLayout.numberOfCols = 1;
    myCollectionView .collectionViewLayout = aLayout;
    myQcData = [QCData singleton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
//    [super viewDidAppear:animated];
//    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController.parentViewController;
//    self.stkCode = aStockPreview.stock.stockCode;
//    [self getCompanyNews];
}

# pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
//    if (self.bizDoneTableDataArr.count > 0) {
//        return self.bizDoneTableDataArr.count;
//    } else {
//        return 0;
//    }
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];

    long section = indexPath.section;
    
    // archive news
    if ([UserPrefConstants singleton].isArchiveNews) {
        NSDictionary *item = [_newsArcArr objectAtIndex:section];
        
        if (item) {
            
//            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [[UserPrefConstants singleton]  getLogicalDate:[item objectForKey:@"dt"]]];
//            cell.textLabel.text =  [[UserPrefConstants singleton] stringByStrippingHTML:[item objectForKey:@"t"]];
            
        }
        
    } else {
        // NSLog(@"not archive");
        
//        NSDictionary *item = [_newsArr objectAtIndex:row];
//        if (item) {
////            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[[UserPrefConstants singleton] getLogicalDate:[item objectForKey:@"4"]]];
////            cell.textLabel.text = [item objectForKey:@"3"];
//
//
//        }
        
    }
    
    
    return cell;
}

#pragma Server

- (void)getCompanyNews
{
    if ([UserPrefConstants singleton].isElasticNews) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetElasticNews:) name:@"didFinishGetElasticNews" object:nil];
        [myATPAuthenticate getElasticNewsForStocks:self.stkCode forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
    }else if ([UserPrefConstants singleton].isArchiveNews) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetArchiveNews:) name:@"didFinishGetArchiveNews" object:nil];
        [myATPAuthenticate getArchiveNewsForStock:self.stkCode forDays:30];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedGetStockNews:) name:@"didFinishedGetStockNews" object:nil];
        [myVertxConnectionManager vertxCompanyNews:self.stkCode];
    }
}

#pragma Hander response from Sever

-(void)didFinishGetElasticNews:(NSNotification*)notification {
}

-(void)didFinishGetArchiveNews:(NSNotification*)notification {
    NSArray *newsArray = [notification.userInfo objectForKey:@"News"];
    if ([newsArray count]>0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *newsDat = [NSArray arrayWithArray:newsArray];
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"dt" ascending: NO];
            _newsArcArr =  [NSMutableArray arrayWithArray:[newsDat sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]]];
            [myCollectionView reloadData];
        });
    } else {
        //news not available
        dispatch_async(dispatch_get_main_queue(), ^{
            [myCollectionView reloadData];
        });
    }
}

- (void)didFinishedGetStockNews:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *dataArr = [data copy];
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"4" ascending: NO];
        _newsArr =  [NSMutableArray arrayWithArray:[dataArr sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]]];
        [myCollectionView reloadData];
    });
}


@end
