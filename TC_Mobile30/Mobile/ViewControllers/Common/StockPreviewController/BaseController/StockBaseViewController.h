//
//  StockBaseViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "ATPAuthenticate.h"
#import "VertxConnectionManager.h"
@class StockModel;

@interface StockBaseViewController : BaseVC
{
    ATPAuthenticate* myATPAuthenticate;
    VertxConnectionManager* myVertxConnectionManager;
}

@property (weak, nonatomic) NSString *stkCode;
@property (weak, nonatomic) StockModel *stock;

@end
