//
//  StockBaseViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockBaseViewController.h"

@interface StockBaseViewController ()

@end

@implementation StockBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    myVertxConnectionManager = [VertxConnectionManager singleton];
    myATPAuthenticate = [ATPAuthenticate singleton];
    [self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CreateUI
- (void)createUI{
    [self createLeftMenu];
}


@end
