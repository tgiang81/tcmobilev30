//
//  QuoteDetailViewController.m
//  TCiPad
//
//  Created by conveo cutoan on 3/29/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "QuoteDetailViewController.h"
//#import "QuoteDetailCell.h"
#import "CustomCollectionViewLayout.h"
#import "VertxConnectionManager.h"
#import "UserPrefConstants.h"
#import "QCData.h"
#import "ImgChartManager.h"
#import "AppConstants.h"
#import "MDInfo.h"
#import "ATPAuthenticate.h"

typedef enum : NSInteger{
    CollectionView_Information = 1,
    CollectionView_Market,
    CollectionView_Time,
    CollectionView_Business,
    CollectionView_News
}CollectionView_Type;

@interface QuoteDetailViewController ()
{
    NSArray *stkCodeAndExchangeArr;
    long currentTotDays;
    QCData *_qcData;
    NSString *stkCodeDisplay; // shortened without exchange end
    NSArray* myInformationTitleArray;
    NSString *bundleIdentifier;
    BOOL isUpdate, isFullScreenChart, runOnce, mktDepthZero;
    MDInfo *mdInfo;
    BOOL loadedMD,loadedTS, loadedNews, loadedBD;
    BOOL canRefreshTimeSales, canRefreshBizDone;
    ATPAuthenticate *_atp;
}

@end

@implementation QuoteDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setBackButtonDefault];
    [self.clvInformation registerNib:[UINib nibWithNibName:@"QuoteDetailCell" bundle:nil] forCellWithReuseIdentifier:@"QuoteDetailCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    self.clvInformation .collectionViewLayout = aLayout;
    
    [self.clvMarket registerNib:[UINib nibWithNibName:@"QuoteDetailCell" bundle:nil] forCellWithReuseIdentifier:@"QuoteDetailCell"];
    CustomCollectionViewLayout* aMarketLayout = [[CustomCollectionViewLayout alloc] init];
    self.clvMarket .collectionViewLayout = aMarketLayout;
    
    [self.clvTime registerNib:[UINib nibWithNibName:@"QuoteDetailCell" bundle:nil] forCellWithReuseIdentifier:@"QuoteDetailCell"];
    CustomCollectionViewLayout* aTimeLayout = [[CustomCollectionViewLayout alloc] init];
    self.clvTime.collectionViewLayout = aTimeLayout;
    
    [self.clvBusiness registerNib:[UINib nibWithNibName:@"QuoteDetailCell" bundle:nil] forCellWithReuseIdentifier:@"QuoteDetailCell"];
    CustomCollectionViewLayout* aBusinessLayout = [[CustomCollectionViewLayout alloc] init];
    self.clvBusiness.collectionViewLayout = aBusinessLayout;
    
    [self.clvNews registerNib:[UINib nibWithNibName:@"QuoteDetailCell" bundle:nil] forCellWithReuseIdentifier:@"QuoteDetailCell"];
    CustomCollectionViewLayout* aNewsLayout = [[CustomCollectionViewLayout alloc] init];
    self.clvNews.collectionViewLayout = aNewsLayout;
    
    //webview
    _wbChart.delegate = self;
    NSURL *url = [NSURL URLWithString:@"https://loton.xyz"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_wbChart setScalesPageToFit:YES];
    [_wbChart loadRequest:request];
    
    [self.detailScroll setContentInset:UIEdgeInsetsMake(0, 0, 0, self.view.frame.size.width * 5)];
    _qcData = [QCData singleton];
//    myInformationTitleArray = [[NSMutableArray alloc] initWithObjects:@[@"Last", @"Bid"], @[@"Change", @"Bid.Qty"], @[@"Change%", @"Ask"], @[@"Open", @"Ask.Qty"], @[@"PrevClose", @"Tot Buy Vol"], @[@"High", @"Tot Sell Vol"], @[@"Low", @"Tot Buy Trans"], @[@"Ceiling", @"Tot Sell Trans"], @[@"Floor", @"Trading Board"], @[@"LACP", @"Tot S.Sell Vol"], @[@"Volume", @"ISIN"], @[@"Value", @"Par Value"], @[@"Trades", @"Lot Size"], @[@"Sector", @"Share Issued"], @[@"Status", @"Mkt.Cap"], nil];
    
    bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
//        _lacpLabel.text = @"TOP";
    }
    
    // Information collection view
    
    // Market depth
    _MDSellArr = [[NSMutableArray alloc] init];
    _MDBuyArr = [[NSMutableArray alloc] init];
    _MDBuyPrevArr = [[NSMutableArray alloc] init];
    _MDSellPrevArr = [[NSMutableArray alloc] init];
    _mktDepthArr = [[NSMutableArray alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedSortByServer:) name:@"didFinishedSortByServer" object:nil];
//    [[VertxConnectionManager singleton]
//     :@"101" Direction:@"DESC" exchg:[UserPrefConstants singleton].userCurrentExchange];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    switch (collectionView.tag) {
        case CollectionView_Information:
            return myInformationTitleArray.count;
            break;
        case CollectionView_Market:
            return 20;
            break;
        case CollectionView_Time:
            return 4;
            break;
        case CollectionView_Business:
            return 40;
            break;
        case CollectionView_News:
            return 20;
            break;
        default:
            return 1;
            break;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (collectionView.tag) {
        case CollectionView_Information:
            return 4;
            break;
        case CollectionView_Market:
            return 7;
            break;
        case CollectionView_Time:
            return 4;
            break;
        case CollectionView_Business:
            return 6;
            break;
        case CollectionView_News:
            return 1;
            break;
        default:
            return 1;
            break;
    }
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    QuoteDetailCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"QuoteDetailCell" forIndexPath:indexPath];
//    if (collectionView.tag == CollectionView_Information && myInformationTitleArray.count > 0    ) {
//        if (indexPath.row == 0) {
//            cell.name.text = [[myInformationTitleArray objectAtIndex:indexPath.section] objectAtIndex:0];
//            cell.name.textColor = [UIColor whiteColor];
//        } else if (indexPath.row == 1) {
//            cell.name.text = [[myInformationTitleArray objectAtIndex:indexPath.section] objectAtIndex:1];
////            [self setColorForTextCell:cell withPrice:(lastDoneFloat-lacpFloat)];
//        } else if (indexPath.row == 2) {
//            cell.name.text = [[myInformationTitleArray objectAtIndex:indexPath.section] objectAtIndex:2];
//            cell.name.textColor = [UIColor whiteColor];
//        } else if (indexPath.row == 3) {
//            cell.name.text = [[myInformationTitleArray objectAtIndex:indexPath.section] objectAtIndex:3];
//
//        }
//
//
//    } else if (collectionView.tag == CollectionView_Market) {
//        if (indexPath.section % 2 != 0) {
//            cell.backgroundColor = [UIColor colorWithWhite:242/255.0 alpha:1.0];
//            } else {
//                cell.backgroundColor = [UIColor whiteColor];
//            }
//        if (indexPath.section == 0) {
//            if (indexPath.row == 0) {
//                cell.name.text = @"No";
//            } else {
//                if (indexPath.row == 1) {
//                    cell.name.text = @"#";
//                } else if (indexPath.row == 2){
//                    cell.name.text = @"Bid.Qty";
//                } else if (indexPath.row == 3){
//                    cell.name.text = @"Bid";
//                } else if (indexPath.row == 4){
//                    cell.name.text = @"Ask";
//                } else if (indexPath.row == 5){
//                    cell.name.text = @"Ask.Qty";
//                } else if (indexPath.row == 6){
//                    cell.name.text = @"#";
//                }
//            }
//        } else {
//            NSDictionary *sellData1 = [NSDictionary new];
//            NSDictionary *buyData1 = [NSDictionary new];
//            if ([_MDBuyArr count]>0) {
//                buyData1 = [_MDBuyArr objectAtIndex:indexPath.row];
//
////                cell.buySplit.text = [[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
////                cell.buyQty.text = [[UserPrefConstants singleton] abbreviateNumber:[[buyData1 objectForKey:FID_58_I_BUY_QTY_1] integerValue]];
////                cell.buyBid.text = [priceFormatter stringFromNumber:[buyData1 objectForKey:FID_68_D_BUY_PRICE_1]];
////
////                double buyPrice = [[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue];
////                if (buyPrice>lacp) {
////                    cell.buyBid.textColor = [UIColor greenColor];
////                } else
////                    if (buyPrice<lacp) {
////                        cell.buyBid.textColor = [UIColor redColor];
////                    } else {
////                        cell.buyBid.textColor = [UIColor whiteColor];
////                    }
//            }
//
//        }
//    }  else if (collectionView.tag == CollectionView_Time) {
//        if (indexPath.section % 2 != 0) {
//            cell.backgroundColor = [UIColor colorWithWhite:242/255.0 alpha:1.0];
//        } else {
//            cell.backgroundColor = [UIColor whiteColor];
//        }
//        if (indexPath.section == 0) {
//            if (indexPath.row == 0) {
//                cell.name.text = @"Date";
//            } else {
//                cell.name.text = @"Section";
//            }
//        } else {
//            if (indexPath.row == 0) {
//                cell.name.text = [NSString stringWithFormat:@"%d",indexPath.section];
//            } else {
//                cell.name.text = @"Content";
//            }
//        }
//    }   else if (collectionView.tag == CollectionView_Business) {
//        if (indexPath.section % 2 != 0) {
//            cell.backgroundColor = [UIColor colorWithWhite:242/255.0 alpha:1.0];
//        } else {
//            cell.backgroundColor = [UIColor whiteColor];
//        }
//        if (indexPath.section == 0) {
//            if (indexPath.row == 0) {
//                cell.name.text = @"Date";
//            } else {
//                cell.name.text = @"Section";
//            }
//        } else {
//            if (indexPath.row == 0) {
//                cell.name.text = [NSString stringWithFormat:@"%d",indexPath.section];
//            } else {
//                cell.name.text = @"Content";
//            }
//        }
//    } else if (collectionView.tag == CollectionView_News) {
//        if (indexPath.section % 2 != 0) {
//            cell.backgroundColor = [UIColor colorWithWhite:242/255.0 alpha:1.0];
//        } else {
//            cell.backgroundColor = [UIColor whiteColor];
//        }
//        if (indexPath.section == 0) {
//            if (indexPath.row == 0) {
//                cell.name.text = @"Date";
//            } else {
//                cell.name.text = @"Section";
//            }
//        } else {
//            if (indexPath.row == 0) {
//                cell.name.text = [NSString stringWithFormat:@"%d",indexPath.section];
//            } else {
//                cell.name.text = @"Content";
//            }
//        }
//    }
    return nil;
}

//-(void)setColorForTextCell:(QuoteDetailCell*)theCell withPrice:(CGFloat)value {
//
//    if (value>0.0001) {
//        theCell.name.textColor = [UIColor greenColor];
//    } else if (value<-0.0001) {
//        theCell.name.textColor = [UIColor redColor];
//    }  else {
//        theCell.name.textColor = [UIColor whiteColor];
//    }
//}


#pragma Update data from server
- (void)didFinishedSortByServer:(NSNotification *)notification
{
    //NSLog(@"finishSort");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // load chart view
        [self loadChartMethodForDays:K_1YEAR];
        [self getDataSource];
        
        
//        [self openStockContainer:NO animated:NO];
//
//        [_filteredDict removeAllObjects];
//        [_tempDict removeAllObjects];
//
//        [_bufferUpdateRow removeAllObjects];
//        [_bufferUpdateStocks removeAllObjects];
//
//        // NSLog(@"sortedByFID %d",sortRow);
//        // UPDATE THE PULL TO LOAD OBJECTS
//
//        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
//        _frontTableView.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
//        _cardCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
//
//        _tableView.contentOffset = CGPointMake(0, 0);
//        _frontTableView.contentOffset = CGPointMake(0, 0);
//        _cardCollectionView.contentOffset = CGPointMake(0, 0);
//
//        [_customPullLabel setHidden:NO];
//        [_pullImg1 setHidden:NO];[_pullImg2 setHidden:NO];
//        [_customPullActivity stopAnimating];
//        if ([UserPrefConstants singleton].quoteScrPage==0) _customPullContainer.hidden = YES;
//        else _customPullContainer.hidden = NO;
//
//        // if sort by Loser/Loser%, we must do ascending sort because we need to have
//        // lowest at top (top loser= the most losing at top) AND also by name
//
//        if ((sortRow==3)||(sortRow==4)||(sortRow==5)) {
//            isDecending = NO;
//            // otherwise do normal descending
//        } else {
//            isDecending = YES;
//        }
        
        NSDictionary *response = [notification.userInfo copy];
        NSMutableArray *sortByServerArr = [[NSMutableArray alloc]initWithArray:[response allKeys] copyItems:YES];
        // NSLog(@"sortByServerArr %@", sortByServerArr);
        
        [UserPrefConstants singleton].userSelectingStockCodeArr = sortByServerArr;
        _selectedArr = [[NSArray alloc] initWithArray:sortByServerArr];

        for (NSString *stkCode in _selectedArr) {
            if(([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {

                NSMutableDictionary *dictToAdd = [[NSMutableDictionary alloc] initWithDictionary:
                                                  [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];

                // add field data if not exist (it is used to sort in quote screen
                // price or qty set to 0
                // this is critical because these fields will be used in sorting
                // if it does not exist, sorting will not work properly.

                if ([dictToAdd objectForKey:FID_101_I_VOLUME]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_101_I_VOLUME];
                }

                // gainer/loser
                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE];
                }

                // gainer/loser %
                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE_PER]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE_PER];
                }

                // name
                if ([dictToAdd objectForKey:FID_38_S_STOCK_NAME]==nil) {
                    [dictToAdd setObject:@"-" forKey:FID_38_S_STOCK_NAME];
                }

                // trades
                if ([dictToAdd objectForKey:FID_132_I_TOT_OF_TRD]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_132_I_TOT_OF_TRD];
                }

                //value
                if ([dictToAdd objectForKey:FID_102_D_VALUE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_102_D_VALUE];
                }

                //close
                if ([dictToAdd objectForKey:FID_50_D_CLOSE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_50_D_CLOSE];
                }

                //lacp
                if ([dictToAdd objectForKey:FID_51_D_REF_PRICE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_51_D_REF_PRICE];
                }

                // bid qty
                if ([dictToAdd objectForKey:FID_58_I_BUY_QTY_1]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_58_I_BUY_QTY_1];
                }


//                [updateTimerCallBack setObject:dictToAdd forKey:stkCode];
            }
        }



//        self.tempDict= [[NSMutableDictionary alloc]initWithDictionary:_filteredDict copyItems:YES];
//        // NSLog(@"filteredDict %@", filteredDict);
//
//        // sort MUST be here! (previously sort in cellForRow = fail)
//        [self sortData];
//
//        if ([_filteredDict count]<=0) {
//            [_noResultsMsgBtn setHidden:NO];
//        } else {
//            [_noResultsMsgBtn setHidden:YES];
//        }
//
//        [loadingBtn stopAnimating];
//        [self setQuoteScreenTitle:[_tempDict count]];
//
//        if ([_cardCollectionView isHidden]) {
//            [_tableView reloadData];
//            [_frontTableView reloadData];
//        } else {
//            [[UserPrefConstants singleton].imgChartCache removeAllObjects];
//            [_detailPageDict removeAllObjects];
//            [_cardCollectionView reloadData];
//        }
//
//        if ([_filteredDict count]<=0) {
//            [_sortBtnContainer setHidden:YES];
//        } else {
//            [_sortBtnContainer setHidden:NO];
//        }

        //        if (updateTimer!=nil) {
        //            [updateTimer invalidate];
        //            updateTimer = nil;
        //        }
        //
        //
        //
        //        updateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerCallBack)
        //                                                     userInfo:nil repeats:YES];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"halfSecondTimerTick" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTimerCallBack)
                                                     name:@"halfSecondTimerTick" object:nil];
        
    });
}

-(void)loadChartMethodForDays:(long)days {

    _stkCode = self.stockModel.stockCode;
    stkCodeAndExchangeArr = [_stkCode componentsSeparatedByString:@"."];
    stkCodeDisplay = [stkCodeAndExchangeArr objectAtIndex:0];
    
    for(UIView *v in [self.chartView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            if (btn.tag>50) {
                [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
        }
    }
    
    switch (days) {
        case K_DAY:
            [[_chartView viewWithTag:51] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_WEEK:
            [[_chartView viewWithTag:52] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_1MONTH:
            [[_chartView viewWithTag:53] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_3MONTH:
            [[_chartView viewWithTag:54] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_6MONTH:
            [[_chartView viewWithTag:55] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_1YEAR:
            [[_chartView viewWithTag:56] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_2YEAR:
            [[_chartView viewWithTag:57] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        default:  [[_chartView viewWithTag:56] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
    }
    
    NSString *chartTypeToLoad = [UserPrefConstants singleton].chartTypeToLoad;
    
    if (days==K_DAY) {
        chartTypeToLoad = @"Image";
    }
    
    
    NSString *resolvedChartURL = [UserPrefConstants singleton].interactiveChartURL;
    
    //    // override for HK N2N
    //    if ([[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"HK"]||[[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"HKD"]) {
    //
    //        chartTypeToLoad = @"Modulus";
    //        resolvedChartURL = @"https://poc.asiaebroker.com/mchart/index_POC.jsp?";
    //    }
    
    // chartTypeToLoad = @"Modulus";
    
    // ========================================
    //                MODULUS
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Modulus"]) {
        
        
        [self setChartStatus:5];
        
        NSString *url = [NSString stringWithFormat:@"%@exchg=%@&code=%@&color=b&view=f&amount=%ld",
                         resolvedChartURL,
                         [stkCodeAndExchangeArr objectAtIndex:1],[stkCodeAndExchangeArr objectAtIndex:0],days];
        
        // NSLog(@"%@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        _wbChart.scrollView.scrollEnabled = NO;
        _wbChart.scrollView.bounces = NO;
        [_wbChart loadRequest:request];
        
        [_imgChart setHidden:YES];
        [_wbChart setHidden:NO];
    }
    
    
    // ========================================
    //                IMAGE
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Image"]) {
        
        [self setChartStatus:4];
        
        NSString *chartURL = [UserPrefConstants singleton].historicalChartURL;
        
        if (currentTotDays==1) {
            chartURL = [UserPrefConstants singleton].intradayChartURL;
        } else {
            chartURL = [UserPrefConstants singleton].historicalChartURL;
        }
        
        NSString *urlString = [NSString stringWithFormat:@"%@&w=%.0f&h=%.0f&k=%@&c=b&d=%ld",chartURL,
                               _imgChart.frame.size.width, _imgChart.frame.size.height, _stkCode,days];
        
        // NSLog(@"%@",urlString);
        
        ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
        [imgChartManager getImageChartWithURL:urlString
                            completedWithData:^(NSData *data) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIImage *chartImg = [UIImage imageWithData:data];
                                    _imgChart.image = chartImg;
                                    [self setChartStatus:1];
                                });
                            }
                                      failure:^(NSString *errorMsg) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self setChartStatus:3];
                                          });
                                      }];
        
        
        [_imgChart setHidden:NO];
        [_wbChart setHidden:YES];
    }
    
    
    // ========================================
    //                TELETRADER
    // ========================================
    
    
    if ([chartTypeToLoad isEqualToString:@"TeleTrader"]) {
        
        [self setChartStatus:5];
        
        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
        NSString *url = [NSString stringWithFormat:@"%@code=%@&Name=%@&exchg=%@&mode=d&color=b&lang=en&key=%@&amount=%ld",
                         resolvedChartURL,
                         [splitStkCode objectAtIndex:0],
                         [splitStkCode objectAtIndex:1],
                         [UserPrefConstants singleton].userCurrentExchange,
                         [[UserPrefConstants singleton] encryptTime], days];
        //NSLog(@"%@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        _wbChart.scrollView.scrollEnabled = NO;
        _wbChart.scrollView.bounces = NO;
        [_wbChart loadRequest:request];
        
        [_imgChart setHidden:YES];
        [_wbChart setHidden:NO];
    }
    
}

-(void)setChartStatus:(long)status {
    //success
    if (status==1) {
        [_chartErrorMsg setHidden:YES];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:NO];
    }
    
    //no url
    if (status==2) {
        [_chartErrorMsg setHidden:NO];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // error
    if (status==3) {
        [_chartErrorMsg setHidden:NO];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // loading
    if (status==4) {
        [_chartErrorMsg setHidden:YES];
        [_btnLoading setHidden:NO];
        [_imgChart setHidden:YES];
    }
    
    // init
    if (status==5) {
        [_chartErrorMsg setHidden:YES];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:YES];
        
    }
}

- (void)getDataSource
{
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    NSString *compName = @"";
    //    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
    //        compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_159_S_SYSBOL2_COMP];
    //    } else {
    //        compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_39_S_COMPANY];
    //    }
    
    compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_39_S_COMPANY];
//    _stockCode.text = compName;
    
    long long totalVol = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]longLongValue];
    long long totalBuyVol     =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]longLongValue];
    long long totalSellVol    =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]longLongValue];
    long long totalBuyTran    =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]longLongValue];
    long long totalSellTran   =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]longLongValue];
    long long totalMarCap     =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]longLongValue];
    long long totalShareIssued=[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]longLongValue];
    
    CGFloat lastDoneFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
    NSString* lastDoneString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lastDoneFloat]];
    
    CGFloat floatPercent = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue];
    CGFloat floatChange = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE] floatValue];
    
    NSString* stockChangeString =  @"";
    NSString* stockChangePerString = @"";
    if (floatPercent > 0) {
//        _stockChangePer.textColor = [UIColor greenColor];
//        _stockChange.textColor = [UIColor greenColor];
        stockChangePerString = [NSString stringWithFormat:@"%.3f%%",floatPercent]; // cqa request no + sign
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            stockChangeString = [NSString stringWithFormat:@"%.3f",floatChange]; // cqa request no + sign
        }else{
            stockChangeString = [NSString stringWithFormat:@"%.4f",floatChange]; // cqa request no + sign
            
        }
    } else if (floatPercent<0) {
//        _stockChangePer.textColor = [UIColor redColor];
//        _stockChange.textColor = [UIColor redColor];
        stockChangePerString= [NSString stringWithFormat:@"%.3f%%",floatPercent];
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            stockChangeString = [NSString stringWithFormat:@"%.3f",floatChange];
        }else{
            stockChangeString = [NSString stringWithFormat:@"%.4f",floatChange];
            
        }
    } else {
//        _stockChangePer.textColor = [UIColor whiteColor];
//        _stockChange.textColor = [UIColor whiteColor];
        stockChangePerString = [NSString stringWithFormat:@"%.3f%%",floatPercent];
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            stockChangeString = [NSString stringWithFormat:@"%.3f",floatChange];
        }else{
            stockChangeString = [NSString stringWithFormat:@"%.4f",floatChange];
        }
    }
    
    CGFloat lacpFloat = 0;
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
        lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_153_D_THEORETICAL_PRICE]floatValue];
    }else{
        lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
    }
    
    NSString* stockLacpString = @"";
    stockLacpString  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lacpFloat]];
    
    NSString* stockOpen = @"";
    CGFloat floatOpen = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE] floatValue];
    stockOpen = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatOpen]];
//    [self setColorForLabel:_stockOpen withPrice:(floatOpen - lacpFloat)];
    
    NSString* stockPrevCloseString = @"";
    CGFloat floatClose = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE] floatValue];
    stockPrevCloseString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatClose]];
//    [self setColorForLabel:_stockPrevClose withPrice:(floatClose-lacpFloat)];
    
    NSString* stockHighString = @"";
    CGFloat floatHigh =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE] floatValue];
    stockHighString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatHigh]];
//    [self setColorForLabel:_stockHigh withPrice:(floatHigh-lacpFloat)];
    
    NSString* stockLowString = @"";
    CGFloat floatLow =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE] floatValue];
    stockLowString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatLow]];
//    [self setColorForLabel:_stockLow withPrice:(floatLow-lacpFloat)];
    
    NSString* stockCeilingString = @"";
    CGFloat floatCeil = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue];
    stockCeilingString  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatCeil]];
//    [self setColorForLabel:_stockCeiling withPrice:(floatCeil-lacpFloat)];
    
    NSString* stockFloorString = @"";
    CGFloat floatFloor = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue];
    stockFloorString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatFloor]];
//    [self setColorForLabel:_stockFloor withPrice:(floatFloor-lacpFloat)];
    
    NSString* stockBidString = @"";
    CGFloat floatBid = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
    stockBidString  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatBid]];
//    [self setColorForLabel:_stockBid withPrice:(floatBid-lacpFloat)];
    
    NSString* stockAsk = @"";
    CGFloat floatAsk = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue];
    stockAsk  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatAsk]];
//    [self setColorForLabel:_stockAsk withPrice:(floatAsk-lacpFloat)];
    
    NSString* _stockValue = @"";
    long long valueTradedVal =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]longLongValue];
    _stockValue = [[UserPrefConstants singleton]abbreviateNumber:valueTradedVal];
    
    NSString* _stockTrades = @"";
    long long tradesVal =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]longLongValue];
    _stockTrades = [[UserPrefConstants singleton]abbreviateNumber:tradesVal];
    
    NSString* _stockBidQty = @"";
    long long bidQtyValue  =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
    _stockBidQty = [[UserPrefConstants singleton]abbreviateNumber:bidQtyValue];
    
    NSString* _stockAskQty = @"";
    long long askQtyValue  =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]longLongValue];
    _stockAskQty = [[UserPrefConstants singleton]abbreviateNumber:askQtyValue];
    
    NSString *stockSectorName = @"";
    //    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
    //        stockSectorName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_158_S_SYSBOL2_SECNAME];
    //    } else {
    //        stockSectorName = [[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
    //    }
    
    stockSectorName = [[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
    
    NSString *stockTradingBoard =[[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:3];
    NSString *stockTotalSellVolume =[NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]];
    NSString *ISINString = [NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_157_S_ISIN]];
    float stockParString =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE] floatValue];
    NSString *lotSizeString = [NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]];
    unichar uc = (unichar)[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_48_S_STATUS] characterAtIndex:1]; //Just extend to 16 bits
    
    ////NSLog(@"stockTotalSellVolume %@ = %@",stockTotalSellVolume,[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]);
    
    // NSLog(@"ISINString %@", ISINString);
    
    CGFloat dur = [stockSectorName length]/10.0;
    
//    if (sectorLabel==nil)
//        sectorLabel=[[MarqueeLabel alloc]initWithFrame:_stockSector.frame duration:dur andFadeLength:5.0f];
//    [sectorLabel setScrollDuration:dur];
//    [_detail2View addSubview:sectorLabel];
    
    if ([stockSectorName length]<=0) stockSectorName = @"-";
    NSString* sectorString = stockSectorName;
//    [sectorLabel setFont:[UIFont systemFontOfSize:14]];
//    [_stockSector removeFromSuperview];
//    [sectorLabel setTextColor:[UIColor whiteColor]];
    
    NSString* _stockTradingBoard = stockTradingBoard;
    NSString* _stockLotSize = lotSizeString;
    
    if (([stockTotalSellVolume length]<=0)||[stockTotalSellVolume containsString:@"null"]) stockTotalSellVolume = @"-";
    NSString* _stockSellVolume = stockTotalSellVolume;
    
    
    if (([ISINString length]<=0)||[ISINString containsString:@"null"]) ISINString = @"-";
    NSString* _stockISIN = ISINString;
    
    
    NSString* _stockParVal =  [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:stockParString]];
    
    NSString* stockStatus = [[UserPrefConstants singleton] getBoardStatus:[NSString stringWithCharacters:&uc length:1]];
    
    
    
    /*Status = FID_44_S_STATE
     3. Trading Board = FID_154_S_GROUP
     4. Total S.Sell Volume = "239"
     5. Lot. Size = "40"
     6. ISIN = "157"
     7. Foreign Ownership = "666191"
     8. Par = "123"
     9. Value Traded = ? It is "108" or "133"
     10. Delivery Basis = ?
     */
    
    //9 Apr 2015. Sometimes feed will return negative value...
    //Nov 2016 - feed not return negative! but it was out of range must use long long for "volumes" data
    
    if (totalMarCap<=0) {
        
        totalMarCap = totalShareIssued * lastDoneFloat;
        
        if (lastDoneFloat<=0) {
            
            totalMarCap = totalShareIssued * lacpFloat;
        }
    }
    
    NSString *lblVolumeString = [[UserPrefConstants singleton] abbreviateNumber:totalVol];
    NSString *lblBuyString = [[UserPrefConstants singleton] abbreviateNumber:totalBuyVol];
    NSString *lblSellString = [[UserPrefConstants singleton] abbreviateNumber:totalSellVol];
    NSString* _stockVolume    = lblVolumeString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalVol]];
    NSString* _stockCeiling      = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]]];
    NSString* _stockFloor       = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]]];
    
    // lblPrevClose.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue]]];
    
    NSString* _stockBuyTotal  =lblBuyString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalBuyVol]];
    NSString* _stockSellTotal = lblSellString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalSellVol]];
    NSString* _stockBuyTrans   =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalBuyTran]];
    NSString* _stockSellTrans  =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalSellTran]];
    NSString* _stockShareIssued     =[[UserPrefConstants singleton]abbreviateNumber:totalShareIssued];
    NSString* _stockMktCap       =[[UserPrefConstants singleton]abbreviateNumber:totalMarCap];
    
    
    
    if ([[stkCodeAndExchangeArr objectAtIndex:1] isEqualToString:@"MY"]) {
//        [_stockMktCap setHidden:YES];
//        [_stockShareIssued setHidden:YES];
//        [_stockShareIssuedTitle setHidden:YES];
//        [_stockMktCapTitle setHidden:YES];
    }
    
    myInformationTitleArray = [[NSMutableArray alloc] initWithObjects:@[@"Last", lastDoneString, @"Bid", stockBidString ], @[@"Change",stockChangeString, @"Bid.Qty", _stockBidQty], @[@"Change%", stockChangePerString, @"Ask", stockAsk], @[@"Open", stockOpen, @"Ask.Qty", _stockAskQty], @[@"PrevClose", stockPrevCloseString, @"Tot Buy Vol", _stockBuyTotal], @[@"High", stockHighString, @"Tot Sell Vol", _stockSellVolume], @[@"Low", stockLowString, @"Tot Buy Trans", _stockBuyTrans], @[@"Ceiling", stockCeilingString, @"Tot Sell Trans", _stockSellTrans], @[@"Floor", stockFloorString, @"Trading Board", _stockTrades], @[@"LACP", stockLacpString, @"Tot S.Sell Vol", _stockSellVolume], @[@"Volume", _stockVolume, @"ISIN", _stockISIN], @[@"Value", _stockValue, @"Par Value", _stockParVal], @[@"Trades", _stockTrades, @"Lot Size", _stockLotSize], @[@"Sector",stockSectorName, @"Share Issued", _stockShareIssued], @[@"Status",stockStatus, @"Mkt.Cap", _stockMktCap], nil];
    [_clvInformation reloadData];
    
}

#pragma mark - Market Depth


-(void)updateFooterData {
    
    
    
    long long totalBuy = 0;
    long long totalSell = 0;
    
    long totBuySplit = 0; long long totBuyQty = 0;  CGFloat aveBuyBid = 0;
    long totSellSplit = 0; long long totSellQty = 0; CGFloat aveSellAsk = 0;
    
    
    for (long i=0; i<[_MDBuyArr count]; i++)  {
        NSDictionary *buyData = [_MDBuyArr objectAtIndex:i];
        
        long eachBuySplit = [[buyData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
        totBuySplit += eachBuySplit;
        
        long long eachBuyQty = [[buyData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
        totBuyQty += eachBuyQty;
        
        CGFloat eachBuyBid = [[buyData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
        aveBuyBid += eachBuyBid*eachBuyQty;
        
        totalBuy += eachBuyQty*eachBuyBid;
        
    }
    
    NSLog(@"_MDSellArr %@",_MDSellArr);
    
    for (long i=0; i<[_MDSellArr count]; i++)  {
        
        NSDictionary *sellData = [_MDSellArr objectAtIndex:i];
        
        long eachSellSplit = [[sellData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
        totSellSplit += eachSellSplit;
        
        long long eachSellQty = [[sellData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
        totSellQty += eachSellQty;
        
        CGFloat eachSellAsk = [[sellData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
        aveSellAsk += eachSellAsk*eachSellQty;
        
        
        totalSell += eachSellQty*eachSellAsk;
        
    }
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    if (totBuyQty<=0) aveBuyBid=0; else aveBuyBid = aveBuyBid/totBuyQty;
    if (totSellQty<=0) aveSellAsk=0; else aveSellAsk = aveSellAsk/totSellQty;
    
    
    _totBidSplit.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totBuySplit]];
    _totBidQty.text =  [[UserPrefConstants singleton] abbreviateNumber:round(totBuyQty)];
    _totBid.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveBuyBid/[_MDBuyArr count]]];
    
    _totAskSplit.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totSellSplit]];
    _totAskQty.text =  [[UserPrefConstants singleton] abbreviateNumber:round(totSellQty)];
    _totAsk.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveSellAsk/[_MDSellArr count]]];
    
    _totalBidLabel.text = [LanguageManager stringForKey:@"Total Bid: %@"
                                       withPlaceholders:@{@"%totBid%":[quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totalBuy]]}];
    _totalAskLabel.text = [LanguageManager stringForKey:@"Total Ask: %@"
                                       withPlaceholders:@{@"%totAsk%":[quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totalSell]]}];
    
    
    
}


-(void)updateMktDepthAndFlash {
    
    // compare prevMDArr and marketDepthArr value
    
    // can compare only if both arrays has the same amount
    
    if (mktDepthZero) return;
    
    [self updateFooterData];
    
    if ([_MDBuyArr count]==[_MDBuyPrevArr count]) {
        
        // then flash as needed
        for (long i=0; i<[_MDBuyArr count]; i++) {
            
            NSDictionary *buyData1 = [_MDBuyArr objectAtIndex:i];
            NSDictionary *buyData2 = [_MDBuyPrevArr objectAtIndex:i];
            
            
            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        }
        
    }
    
    if ([_MDSellArr count]==[_MDSellPrevArr count]) {
        NSLog(@"_MDSellArr %@",_MDSellArr);
        // then flash as needed
        for (long i=0; i<[_MDSellArr count]; i++) {
            
            NSDictionary *sellData1 = [_MDSellArr objectAtIndex:i];
            NSDictionary *sellData2 = [_MDSellPrevArr objectAtIndex:i];
            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        }
        
    }
    
    _MDBuyPrevArr = [NSMutableArray arrayWithArray:_MDBuyArr];
    _MDSellPrevArr = [NSMutableArray arrayWithArray:_MDSellArr];
}




-(void)didFinishMarketDepth:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    _mktDepthArr = [response objectForKey:@"marketdepthresults"];
    
    
    mktDepthZero = NO;
    
    NSLog(@"mktDepthArr %@",_mktDepthArr);
    
    NSMutableArray *tmpBuyArr = [[NSMutableArray alloc] init];
    NSMutableArray *tmpSellArr = [[NSMutableArray alloc] init];
    
    
    int MDLfeature = ([mdInfo.isMDL isEqualToString:@"Y"])?1:0;
    
    
    // Non Feature md data is all in single dictionary, so we convert it to same
    // format as featured MD.
    if (MDLfeature==0) {
        
        // if server give any data
        if ([_mktDepthArr count]>0) {
            
            
            // non MDL - all mkt depth data is in one object (max 10 level only)
            NSDictionary *stkDataWithMDDict = [_mktDepthArr objectAtIndex:0];
            
            
            int fid_buy_split_start = 170;
            int fid_sell_split_start = 180;
            int fid_buy_qty_start = 58;
            int fid_sell_qty_start = 78;
            int fid_buy_price_start = 68;
            int fid_sell_price_start = 88;
            
            
            // populate tmpBuyArr (loop 10 and if found, create it and add)
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *buyMD = [[NSMutableDictionary alloc] init];
                
                NSString *buySplitKey = [NSString stringWithFormat:@"%d",fid_buy_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buySplitKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buySplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *buyPriceKey = [NSString stringWithFormat:@"%d",fid_buy_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyPriceKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *buyQtyKey = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyQtyKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[buyMD allKeys] count]>0) {
                    // set FID 107
                    [buyMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [buyMD setObject:[NSNumber numberWithInt:1] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    
                    [tmpBuyArr addObject:buyMD];
                }
            }
            
            // NSLog(@"tmpBuyArr %@", tmpBuyArr);
            
            // populate tmpSellArr
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *sellMD = [[NSMutableDictionary alloc] init];
                
                NSString *sellSplitKey = [NSString stringWithFormat:@"%d",fid_sell_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellSplitKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellSplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *sellPriceKey = [NSString stringWithFormat:@"%d",fid_sell_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellPriceKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *sellQtyKey = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellQtyKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[sellMD allKeys] count]>0) {
                    // set FID 107
                    [sellMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [sellMD setObject:[NSNumber numberWithInt:2] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    [tmpSellArr addObject:sellMD];
                }
            }
            
            
        } else {
            ; // empty data = do nothing (will be handled below)
        }
        
        
    } else if (MDLfeature==1) {
        // MDL type - mkt depth data in separate objects
        
        // if server give any data
        if ([_mktDepthArr count]>0) {
            
            for (int i=0; i<[_mktDepthArr count]; i++) {
                
                NSDictionary *eachMDdata = [_mktDepthArr objectAtIndex:i];
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==1) {
                    // BID
                    [tmpBuyArr addObject:eachMDdata];
                }
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==2) {
                    // SELL
                    [tmpSellArr addObject:eachMDdata];
                }
            }
            
        } else {
            ; // empty data = do nothing  (will be handled below)
        }
        
    }
    
    //NSLog(@"tmpBuycount %d",[tmpBuyArr count]);
    
    // if tmpBuyArr empty, prepare zeroes
    if ([tmpBuyArr count]<=0) {
        int mdLevel = [mdInfo.mdLevel intValue];
        
        // NSLog(@"mdLevel %d",mdLevel);
        
        int alternate = 1;
        int pos = 0;
        for (long i=0; i<mdLevel*2; i++) {
            NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:2],@"105",
                                      [NSNumber numberWithInt:alternate],@"106",
                                      [NSNumber numberWithInt:pos],@"107",
                                      [NSNumber numberWithInt:0],@"118",
                                      @"KL",@"131",
                                      [NSNumber numberWithInt:0],@"170",
                                      @"5517.KL",@"33",
                                      @"0",@"45",
                                      [NSNumber numberWithInt:0],@"58",
                                      [NSNumber numberWithInt:0],@"68",
                                      nil];
            
            if (alternate==1) {
                [tmpBuyArr addObject:zeroDict];
                alternate = 2;
                pos++;
            } else {
                [tmpSellArr addObject:zeroDict];
                alternate = 1;
            }
        }
        
    }
    
    
    // sort data based on FID 107
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_107_I_MARKET_DEPTH_POSITION ascending:YES];
    NSArray *limitedArrayB = [tmpBuyArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSArray *limitedArrayS = [tmpSellArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [tmpBuyArr removeAllObjects]; [tmpSellArr removeAllObjects];
    
    //NSLog(@"limitedArrayB %@",limitedArrayB);
    //NSLog(@"limitedArrayS %@",limitedArrayS);
    
    if ([limitedArrayB count]>0) {
        //
        //        [_mdFooter setHidden:NO];
        _MDBuyArr = [NSMutableArray arrayWithArray:[limitedArrayB mutableCopy]];
        _MDSellArr = [NSMutableArray arrayWithArray:[limitedArrayS mutableCopy]];
        [_mktDepthTable reloadData];
        [self updateFooterData];
        
        // then send subscribe data (subscribe only if there is mktdepth)
        // TODO: subscribe and flashing
        // [[VertxConnectionManager singleton] vertxSubscribeMktDepthLevel:MDLfeature withStockCode:_stkCode];
    }
    
    [self.clvMarket reloadData];
}


- (void)getMarketDepth
{
    NSArray* arrayWithTwoStrings = [_stkCode componentsSeparatedByString:@"."];
    
    if (_stkCode !=nil) {
        
        //NSLog(@"stkCode %@ coll %@",_stkCode, [arrayWithTwoStrings objectAtIndex:1]);
        [[VertxConnectionManager singleton] vertxGetMarketDepth:_stkCode andCollarr:[arrayWithTwoStrings objectAtIndex:1]];
        
    }
    
}

#pragma mark - Scrollview delegates

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    CGFloat pageWidth = _detailScroll.frame.size.width;
    int page = floor((_detailScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pgControl.currentPage = page;
    
    if ((page==1)&&(!loadedMD)) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishMarketDepth:) name:@"didFinishMarketDepth" object:nil];
        [self getMarketDepth];
        loadedMD = YES;
    }
    
    if (page==2) {
//        [_timeSalesTable addSubview:self.refreshControl];
        if (!loadedTS) {
            loadedTS = YES;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedTimeAndSales:) name:@"didFinishedTimeAndSales" object:nil];
//            [self getTimeAndSales];
        }
    }
    
    if (page==3) {
//        [_bizDoneTable addSubview:self.refreshControl];
        if (!loadedBD) {
            loadedBD = YES;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedBusinessDone:) name:@"didFinishedBusinessDone" object:nil];
//            [self getBizDone];
        }
    }
    
    if ((page==4)&&(!loadedNews)) {
        loadedNews = YES;
//        [self getCompanyNews];
    }
    
}

#pragma mark - Time&Sales

-(void)refreshTimeSales {
    
    if (canRefreshTimeSales) {
        canRefreshTimeSales = NO;
        [self performSelector:@selector(getTimeAndSales) withObject:nil afterDelay:1.0];
    }
}


- (void)getTimeAndSales
{
    NSString *tranNo;
    tranNo = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode ]objectForKey:FID_103_I_TRNS_NO];
    [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:_stkCode begin:@"0"
                                                                    end:tranNo exchange:[UserPrefConstants singleton].userCurrentExchange];
}


- (void)didFinishedTimeAndSales:(NSNotification *)notification
{
    
    canRefreshTimeSales = YES;
//    [self.refreshControl endRefreshing];
    
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    _timeSalesArr = [NSMutableArray arrayWithArray:data ];
    
    //   NSLog(@"_timeAndSalesTableDataArr %@", _timeAndSalesTableDataArr);
    
    [_timeSalesTable reloadData];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *currentTime = [NSDate date];
    _timeSalesLastUpdate.text = [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatter stringFromDate: currentTime]}];
    
}

#pragma mark - News
- (void) openNews:(NSString *)url withTitle:(NSString*)title{
    
//    [_atp timeoutActivity];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
//    vc.view.backgroundColor = [UIColor clearColor];
//    vc.newsTitleLabel.text = title;
    
//    if (![title isEqualToString:[LanguageManager stringForKey:@"Stock News"]]) {
//        vc.currentEd = currentEd;
//        vc.companyNameLabel.text = [[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_39_S_COMPANY];
//    } else {
//        vc.currentEd = nil;
//
//    }
    
//    [vc openUrl:url withJavascript:YES];
//    [vc setTransitioningDelegate:transitionController];
//    vc.modalPresentationStyle= UIModalPresentationCustom;
//    [self presentViewController:vc animated:YES completion:nil];
    
}

// ARCHIVE NEWS FROM ATP
-(void)didFinishGetArchiveNews:(NSNotification*)notification {
    
    NSArray *newsArray = [notification.userInfo objectForKey:@"News"];
    
    
    if ([newsArray count]>0) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *newsDat = [NSArray arrayWithArray:newsArray];
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"dt" ascending: NO];
            _newsArcArr =  [NSMutableArray arrayWithArray:[newsDat sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]]];
            
            [_newsTable reloadData];
        });
    } else {
        //news not available
        dispatch_async(dispatch_get_main_queue(), ^{
            [_newsTable reloadData];
        });
    }
    
}

-(void)didFinishGetElasticNews:(NSNotification*)notification {
    
    
    
}

// NORMAL NEWS FROM VERTX
- (void)didFinishedGetStockNews:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    // ////NSLog(@"didFinishedGetStockNews response : %@",response);
    
    NSArray *data =[response objectForKey:@"data"];
    //    NSArray *reversed = [[[data sortedArrayUsingSelector:@selector(compare:)] reverseObjectEnumerator] allObjects];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *dataArr = [data copy];
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"4" ascending: NO];
        
        _newsArr =  [NSMutableArray arrayWithArray:[dataArr sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]]];
        // NSLog(@"newsArr %@",_newsArr);
        [_newsTable reloadData];
    });
}

- (void)getCompanyNews
{
    if ([UserPrefConstants singleton].isElasticNews) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetElasticNews:) name:@"didFinishGetElasticNews" object:nil];
        [_atp getElasticNewsForStocks:_stkCode forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
    }else if ([UserPrefConstants singleton].isArchiveNews) {
        //NSLog(@"archive");
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetArchiveNews:) name:@"didFinishGetArchiveNews" object:nil];
        [_atp getArchiveNewsForStock:_stkCode forDays:30];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedGetStockNews:) name:@"didFinishedGetStockNews" object:nil];
        [[VertxConnectionManager singleton] vertxCompanyNews:_stkCode];
    }
}


@end
