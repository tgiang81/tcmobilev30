//
//  QuoteDetailViewController.h
//  TCiPad
//
//  Created by conveo cutoan on 3/29/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "StockModel.h"

@interface QuoteDetailViewController : BaseVC <UICollectionViewDataSource, UICollectionViewDelegate, UIWebViewDelegate>
{
    
}


@property (weak, nonatomic) IBOutlet UIScrollView *scrFrame;
@property (weak, nonatomic) IBOutlet UICollectionView *clvInformation;
@property (weak, nonatomic) IBOutlet UICollectionView *clvMarket;
@property (weak, nonatomic) IBOutlet UICollectionView *clvTime;
@property (weak, nonatomic) IBOutlet UICollectionView *clvBusiness;
@property (weak, nonatomic) IBOutlet UICollectionView *clvNews;

@property (nonatomic, strong) NSArray *selectedArr;
@property (nonatomic, strong) NSMutableDictionary *filteredDict;

//Chart view
@property (weak, nonatomic) IBOutlet UIWebView *wbChart;
@property (weak, nonatomic) IBOutlet UIImageView *imgChart;
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (weak, nonatomic) IBOutlet UIButton *chartErrorMsg;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *btnLoading;

@property (weak, nonatomic) NSString *stkCode; // used for getting data from qcfeed
@property (weak, nonatomic) StockModel *stockModel;
@property (weak, nonatomic) IBOutlet UIScrollView *detailScroll;
@property (weak, nonatomic) IBOutlet UIPageControl *pgControl;


// MARKET DEPTH
@property (strong, nonatomic) NSMutableArray *mktDepthArr;
@property (strong, nonatomic) NSMutableArray *MDBuyArr;
@property (strong, nonatomic) NSMutableArray *MDBuyPrevArr;
@property (strong, nonatomic) NSMutableArray *MDSellArr;
@property (strong, nonatomic) NSMutableArray *MDSellPrevArr;
@property (weak, nonatomic) IBOutlet UIView *mktDepthView;
@property (weak, nonatomic) IBOutlet UITableView *mktDepthTable;
@property (weak, nonatomic) IBOutlet UILabel *totBidSplit;
@property (weak, nonatomic) IBOutlet UILabel *totBidQty;
@property (weak, nonatomic) IBOutlet UILabel *totBid;
@property (weak, nonatomic) IBOutlet UILabel *totAsk;
@property (weak, nonatomic) IBOutlet UILabel *totAskQty;
@property (weak, nonatomic) IBOutlet UILabel *totAskSplit;
@property (weak, nonatomic) IBOutlet UILabel *totalBidLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAskLabel;
@property (weak, nonatomic) IBOutlet UILabel *lacpLabel;

// TIME & SALES
@property (strong, nonatomic) NSMutableArray *timeSalesArr;
@property (weak, nonatomic) IBOutlet UIView *timeSalesView;
@property (weak, nonatomic) IBOutlet UILabel *timeSalesLastUpdate;
@property (weak, nonatomic) IBOutlet UITableView *timeSalesTable;

// NEWS
@property (strong, nonatomic) NSMutableArray *newsArr;
@property (strong, nonatomic) NSMutableArray *newsArcArr;
@property (weak, nonatomic) IBOutlet UIView *newsView;
@property (weak, nonatomic) IBOutlet UITableView *newsTable;

// BIZ DONE
@property (strong, nonatomic) NSMutableArray *bizDoneTableDataArr;
@property (weak, nonatomic) IBOutlet UIView *bizDoneView;
@property (weak, nonatomic) IBOutlet UITableView *bizDoneTable;
@property (weak, nonatomic) IBOutlet UILabel *bizDoneLastUpdate;


@end
