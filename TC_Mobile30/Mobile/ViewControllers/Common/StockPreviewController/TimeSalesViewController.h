//
//  TimeSalesViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockBaseViewController.h"

@interface TimeSalesViewController : StockBaseViewController
{
    
    __weak IBOutlet UICollectionView *myCollectionView;
}

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSMutableArray *timeSalesArr;

@end
