//
//  BusinessDoneViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BusinessDoneViewController.h"
#import "CustomCollectionViewLayout.h"
#import "StockModel.h"
#import "StockDetaiCell.h"
#import "QCData.h"
#import "N2PercentLabel.h"
#import "MStockPreviewController.h"
#import "LanguageKey.h"
@interface BusinessDoneViewController ()
{
    NSMutableArray* myDataSource;
    BOOL  canRefreshBizDone;
    QCData *myQcData;
}

@end

@implementation BusinessDoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = StockBusiness;
    aLayout.numberOfCols = 6;
    myCollectionView .collectionViewLayout = aLayout;
    canRefreshBizDone = NO;
    myQcData = [QCData singleton];
    [myCollectionView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionOld context:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController.parentViewController;
    self.stkCode = aStockPreview.stock.stockCode;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedBusinessDone:) name:@"didFinishedBusinessDone" object:nil];
    [self getBizDone];
}

# pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.bizDoneTableDataArr.count > 0) {
        return self.bizDoneTableDataArr.count + 1;
    } else {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];

    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.name.text = @"Price";
        } else if (indexPath.row == 1) {
            cell.name.text = @"Bvol";
        } else if (indexPath.row == 2) {
            cell.name.text = @"Svol";
        } else if (indexPath.row == 3) {
            cell.name.text = @"Buy%";
        } else if (indexPath.row == 4) {
            cell.name.text = @"T.Vol";
        } else if (indexPath.row == 5) {
            cell.name.text = @"T.Val";
        }
        cell.name.textColor = [UIColor whiteColor];
    } else {
        CGFloat lacpFloat = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
        
        long long svol = [[[self.bizDoneTableDataArr objectAtIndex:indexPath.section -1] objectForKey:@"1"] longLongValue];
        long long bvol = [[[self.bizDoneTableDataArr objectAtIndex:indexPath.section - 1] objectForKey:@"2"] longLongValue];
        long long tvol = [[[self.bizDoneTableDataArr objectAtIndex:indexPath.section -1] objectForKey:@"3"] longLongValue];
        long long tval = [[[self.bizDoneTableDataArr objectAtIndex:indexPath.section -1] objectForKey:@"4"] longLongValue];
        
        CGFloat priceFloat = [[[self.bizDoneTableDataArr objectAtIndex:indexPath.section - 1] objectForKey:@"0"] floatValue];
        if (indexPath.row == 0) {
            if ([UserPrefConstants singleton].pointerDecimal==3) {
                
                cell.name.text = [NSString stringWithFormat:@"%.3f",priceFloat];
                
            }else{
                cell.name.text = [NSString stringWithFormat:@"%.4f",priceFloat];
            }
            
            if (priceFloat>lacpFloat) {
                cell.name.textColor = [UIColor greenColor];
            } else if (priceFloat<lacpFloat) {
                cell.name.textColor = [UIColor redColor];
            } else {
                cell.name.textColor = [UIColor whiteColor];
            }
        } else if (indexPath.row == 1){
            cell.name.text = [[UserPrefConstants singleton] abbreviateNumber:bvol];
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 2){
            cell.name.text = [[UserPrefConstants singleton] abbreviateNumber:svol];
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 3){
            float ratio =0;
            
            if (bvol+svol>0) { // prevent NaN
                ratio = (float)bvol/((float)bvol+(float)svol);
            }
            cell.bdBuyPercent.text = [NSString stringWithFormat:@"%.2f%%",ratio*100.0];
            [cell.bdBuyPercent setRatio:ratio];
            [cell.bdBuyPercent setColor1:kGreenColor];
            [cell.bdBuyPercent setColor2:kRedColor];
            cell.name.hidden = YES;
            cell.bdBuyPercent.hidden = NO;
        } else if (indexPath.row == 4){
            cell.name.text = [[UserPrefConstants singleton] abbreviateNumber:tvol];
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 5){
            cell.name.text = [[UserPrefConstants singleton] abbreviateNumber:tval];
            cell.name.textColor = [UIColor whiteColor];
        }
    }

    return cell;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary  *)change context:(void *)context
{
    
}

#pragma Server

-(void)getBizDone {
    [myVertxConnectionManager
     vertxGetBusinessDoneByStockCode:self.stkCode exchg:[UserPrefConstants singleton].userCurrentExchange];
}

-(void)didFinishedBusinessDone:(NSNotification*)notification {
//    [self.refreshControl endRefreshing];
    
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    
    if ([data count]>4)
        _bizDoneTableDataArr = [NSMutableArray arrayWithArray:[data subarrayWithRange:NSMakeRange(0, [data count]-4)]];
    
    canRefreshBizDone = YES;
    [myCollectionView reloadData];
    
    NSDateFormatter *dateFormatterscoreboard = [[NSDateFormatter alloc] init];
    [dateFormatterscoreboard setDateFormat:@"HH:mm:ss"];
    NSDate *currentTime = [NSDate date];
    
//    _bizDoneLastUpdate.text = [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];
}


@end
