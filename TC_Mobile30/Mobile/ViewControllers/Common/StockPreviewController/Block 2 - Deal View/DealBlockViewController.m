//
//  DealBlockViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DealBlockViewController.h"
#import "QCData.h"
#import "StockModel.h"
#import "TradingRules.h"
#import "MStockPreviewController.h"


@interface DealBlockViewController ()
{
    NSArray *stkCodeAndExchangeArr;
    TradingRules *trdRules;
    MTradeViewController *tradeVC;
    int lotSizeStringInt;
}

@end

@implementation DealBlockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController;
    self.stkCode = aStockPreview.stock.stockCode;
    
    
    [UserPrefConstants singleton].userSelectingThisStock = aStockPreview.stock;

    [UserPrefConstants singleton].userSelectingStockCode = aStockPreview.stock.stockCode;

    
    
    stkCodeAndExchangeArr = [self.stkCode componentsSeparatedByString:@"."];
    trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidLayoutSubviews
{
    buyButton.layer.cornerRadius = kButtonRadius*buyButton.frame.size.height;
    buyButton.layer.masksToBounds = YES;
    sellButton.layer.cornerRadius = kButtonRadius*sellButton.frame.size.height;
    sellButton.layer.masksToBounds = YES;
    
    watchListButton.layer.cornerRadius = kButtonRadius*watchListButton.frame.size.height;
    watchListButton.layer.masksToBounds = YES;
    stockDetailButton.layer.cornerRadius = kButtonRadius*stockDetailButton.frame.size.height;
    stockDetailButton.layer.masksToBounds = YES;
}

#pragma Trade

-(void)executeTrade:(id)sender {
    
//    lotSizeStringInt = [[[[[QCData singleton] qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_40_I_SHARE_PER_LOT] intValue];
//    
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//    UIButton *btn = (UIButton *)sender;
//    
//    CGFloat defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:self.stkCode] objectForKey:FID_88_D_SELL_PRICE_1] floatValue];
//    
//    //NSLog(@"b4 %f",defPrice);
//    if (defPrice<=0) {
//        defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:self.stkCode] objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
//        //  NSLog(@"zero %f",defPrice);
//    }
//    
//    NSString *stkNameStr = @"";
//    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
//        stkNameStr = [[[[QCData singleton] qcFeedDataDict]objectForKey:self.stkCode] objectForKey:FID_130_S_SYSBOL_2];
//    } else {
//        stkNameStr = [[[[QCData singleton] qcFeedDataDict]objectForKey:self.stkCode] objectForKey:FID_38_S_STOCK_NAME];
//    }
//    
//    if (btn.tag==10) { // BUY
//        
//        if ([UserPrefConstants singleton].pointerDecimal==3) {
//            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                    @"BUY",@"Action",
//                    
//                    self.stkCode,@"StockCode",
//                    stkNameStr,@"StockName",
//                    [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
//                    [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
//                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], stkNameStr],@"stkNname",
//                    trdRules, @"TradeRules",
//                    nil];
//        }else{
//            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                    @"BUY",@"Action",
//                    
//                    self.stkCode,@"StockCode",
//                    stkNameStr,@"StockName",
//                    [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
//                    [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
//                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], stkNameStr],@"stkNname",
//                    trdRules, @"TradeRules",
//                    nil];
//        }
//        
//        
//        
//    } else if (btn.tag==11) { // SELL
//        
//        if ([UserPrefConstants singleton].pointerDecimal==3) {
//            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                    @"SELL",@"Action",
//                    self.stkCode,@"StockCode",
//                    stkNameStr,@"StockName",
//                    [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
//                    [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
//                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], stkNameStr],@"stkNname",
//                    trdRules, @"TradeRules",
//                    nil];
//        }else{
//            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                    @"SELL",@"Action",
//                    self.stkCode,@"StockCode",
//                    stkNameStr,@"StockName",
//                    [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
//                    [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
//                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], stkNameStr],@"stkNname",
//                    trdRules, @"TradeRules",
//                    nil];
//        }
//        
//        
//        
//        
//    } else  if (btn.tag==12) { // REVISE
//        
//        dict = [[UserPrefConstants singleton].reviseCancelDict mutableCopy];
//        
//        
//    } else  if (btn.tag==13) { // CANCEL
//        
//        dict = [[UserPrefConstants singleton].reviseCancelDict mutableCopy];
//        
//    }
//
//    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    //    TraderViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TraderViewController"];
//    tradeVC.actionTypeDict = dict;
//    //[vc setTransitioningDelegate:transitionController];
////    self.providesPresentationContextTransitionStyle = YES;
////    self.definesPresentationContext = YES; //
//    //    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    //    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//    //    [self presentViewController:vc animated:NO completion:nil];
//    
//    tradeVC.stockLotSize = lotSizeStringInt;
//    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//    } completion:^(BOOL finished) {
//        
//        [tradeVC callTrade];
//    }];

}

#pragma Action

- (IBAction)didTapOnBuyButton:(id)sender {
    //ggtt
//    tradeVC = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);;
//    //NSLog(@"Stock Lot Size %d",lotSizeStringInt);
//    tradeVC.stockLotSize = lotSizeStringInt;
//    tradeVC.delegate = self;
//    tradeVC.parentController = self;
////    [self presentViewController:tradeVC animated:YES completion:nil];
//    [self.navigationController pushViewController:tradeVC animated:YES];
//    [self executeTrade:sender];
}
- (IBAction)didTapOnSellButton:(id)sender {
}
- (IBAction)didTapOnWatchListButton:(id)sender {
}
- (IBAction)didTapOnStockDetailButton:(id)sender {
}

-(void)traderDidCancelled {
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        
    } completion:^(BOOL finished) {
    }];
    
    
}

@end
