//
//  DealBlockViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockBaseViewController.h"
#import "MTradeViewController.h"

@interface DealBlockViewController : StockBaseViewController
{
    
    __weak IBOutlet UIButton *buyButton;
    __weak IBOutlet UIButton *sellButton;
    __weak IBOutlet UIButton *watchListButton;
    __weak IBOutlet UIButton *stockDetailButton;
}

@end
