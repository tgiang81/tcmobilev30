//
//  MarketDepthViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MarketDepthViewController.h"
#import "StockModel.h"
#import "MDInfo.h"
#import "CustomCollectionViewLayout.h"
#import "StockDetaiCell.h"
#import "QCData.h"
#import "MStockPreviewController.h"

@interface MarketDepthViewController ()
{
    MDInfo *mdInfo;
    BOOL isUpdate, isFullScreenChart, runOnce, mktDepthZero;
    NSMutableArray* myDataSource;
    QCData *myQcData;
}

@end

@implementation MarketDepthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = StockMarketDepth;
    aLayout.numberOfCols = 7;
    myCollectionView .collectionViewLayout = aLayout;
    myQcData = [QCData singleton];
    
    _MDSellArr = [[NSMutableArray alloc] init];
    _MDBuyArr = [[NSMutableArray alloc] init];
    _MDBuyPrevArr = [[NSMutableArray alloc] init];
    _MDSellPrevArr = [[NSMutableArray alloc] init];
    _mktDepthArr = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    for (MDInfo *inf in [UserPrefConstants singleton].currentExchangeInfo.qcParams.exchgInfo) {
        if ([inf.exchange isEqualToString:[UserPrefConstants singleton].userCurrentExchange]) {
            mdInfo = inf;
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController.parentViewController;
    self.stkCode = aStockPreview.stock.stockCode;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishMarketDepth:) name:@"didFinishMarketDepth" object:nil];
    [self getMarketDepth];
}

#pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (_MDBuyArr.count > 0) {
        return _MDBuyArr.count + 1;
    } else {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    long session = indexPath.section;
    
    double lacp = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
    
    
    NSDictionary *sellData1 = [NSDictionary new];
    NSDictionary *buyData1 = [NSDictionary new];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.name.text = @"No";
        } else if (indexPath.row == 1){
            cell.name.text = @"#";
        } else if (indexPath.row == 2){
            cell.name.text = @"Bid.Qty";
        } else if (indexPath.row == 3){
            cell.name.text = @"Bid";
        } else if (indexPath.row == 4){
            cell.name.text = @"Ask";
        } else if (indexPath.row == 5){
            cell.name.text = @"Ask.Qty";
        } else if (indexPath.row == 6){
            cell.name.text = @"#";
        }
        cell.name.textColor = [UIColor whiteColor];
    } else {
        if ([_MDBuyArr count]>0) {
            buyData1 = [_MDBuyArr objectAtIndex:session - 1];
            
            if (indexPath.row == 1) {
                cell.name.text = [[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
                cell.name.textColor = [UIColor whiteColor];
            } else if (indexPath.row == 2)
            {
                cell.name.text = [[UserPrefConstants singleton] abbreviateNumber:[[buyData1 objectForKey:FID_58_I_BUY_QTY_1] integerValue]];
                cell.name.textColor = [UIColor whiteColor];
            } else if ( indexPath.row == 3)
            {
                cell.name.text = [priceFormatter stringFromNumber:[buyData1 objectForKey:FID_68_D_BUY_PRICE_1]];
                double buyPrice = [[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue];
                if (buyPrice>lacp) {
                    cell.name.textColor = [UIColor greenColor];
                } else
                    if (buyPrice<lacp) {
                        cell.name.textColor = [UIColor redColor];
                    } else {
                        cell.name.textColor = [UIColor whiteColor];
                    }
            }
        }
        if ([_MDSellArr count]>0) {
            
            sellData1 = [_MDSellArr objectAtIndex:session - 1];
            if (indexPath.row == 6) {
                cell.name.text = [[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
                cell.name.textColor = [UIColor whiteColor];
            } else if (indexPath.row == 5)
            {
                cell.name.text = [[UserPrefConstants singleton] abbreviateNumber:[[sellData1 objectForKey:FID_58_I_BUY_QTY_1] integerValue]];
                cell.name.textColor = [UIColor whiteColor];
            } else if ( indexPath.row == 4)
            {
                cell.name.text = [priceFormatter stringFromNumber:[sellData1 objectForKey:FID_68_D_BUY_PRICE_1]];
                double sellPrice = [[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue];
                if (sellPrice>lacp) {
                    cell.name.textColor = [UIColor greenColor];
                } else
                    if (sellPrice<lacp) {
                        cell.name.textColor = [UIColor redColor];
                    } else {
                        cell.name.textColor = [UIColor whiteColor];
                    }
            }
            
        }
        if (indexPath.row == 0) {
            cell.name.text = [NSString stringWithFormat:@"%ld", session+1];
            cell.name.textColor = [UIColor whiteColor];
        }
        
        if (mktDepthZero) {
            //        cell.buyBid.textColor = [UIColor whiteColor];
            //        cell.sellAsk.textColor = [UIColor whiteColor];
        }
    }
    
    

    return cell;
}

#pragma Sever
- (void)getMarketDepth
{
    NSArray* arrayWithTwoStrings = [self.stkCode componentsSeparatedByString:@"."];
    
    if (self.stkCode) {
        [myVertxConnectionManager vertxGetMarketDepth:self.stkCode andCollarr:[arrayWithTwoStrings objectAtIndex:1]];
    }
    
}

-(void)didFinishMarketDepth:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    _mktDepthArr = [response objectForKey:@"marketdepthresults"];
    
    mktDepthZero = NO;
    
    NSMutableArray *tmpBuyArr = [[NSMutableArray alloc] init];
    NSMutableArray *tmpSellArr = [[NSMutableArray alloc] init];
    
    
    int MDLfeature = ([mdInfo.isMDL isEqualToString:@"Y"])?1:0;
    
    
    // Non Feature md data is all in single dictionary, so we convert it to same
    // format as featured MD.
    if (MDLfeature==0) {
        
        // if server give any data
        if ([_mktDepthArr count]>0) {
            
            
            // non MDL - all mkt depth data is in one object (max 10 level only)
            NSDictionary *stkDataWithMDDict = [_mktDepthArr objectAtIndex:0];
            
            
            int fid_buy_split_start = 170;
            int fid_sell_split_start = 180;
            int fid_buy_qty_start = 58;
            int fid_sell_qty_start = 78;
            int fid_buy_price_start = 68;
            int fid_sell_price_start = 88;
            
            
            // populate tmpBuyArr (loop 10 and if found, create it and add)
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *buyMD = [[NSMutableDictionary alloc] init];
                
                NSString *buySplitKey = [NSString stringWithFormat:@"%d",fid_buy_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buySplitKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buySplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *buyPriceKey = [NSString stringWithFormat:@"%d",fid_buy_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyPriceKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *buyQtyKey = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyQtyKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[buyMD allKeys] count]>0) {
                    // set FID 107
                    [buyMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [buyMD setObject:[NSNumber numberWithInt:1] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    
                    [tmpBuyArr addObject:buyMD];
                }
            }
            
            // NSLog(@"tmpBuyArr %@", tmpBuyArr);
            
            // populate tmpSellArr
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *sellMD = [[NSMutableDictionary alloc] init];
                
                NSString *sellSplitKey = [NSString stringWithFormat:@"%d",fid_sell_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellSplitKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellSplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *sellPriceKey = [NSString stringWithFormat:@"%d",fid_sell_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellPriceKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *sellQtyKey = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellQtyKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[sellMD allKeys] count]>0) {
                    // set FID 107
                    [sellMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [sellMD setObject:[NSNumber numberWithInt:2] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    [tmpSellArr addObject:sellMD];
                }
            }
            
            
        } else {
            ; // empty data = do nothing (will be handled below)
        }
        
        
    } else if (MDLfeature==1) {
        // MDL type - mkt depth data in separate objects
        
        // if server give any data
        if ([_mktDepthArr count]>0) {
            
            for (int i=0; i<[_mktDepthArr count]; i++) {
                
                NSDictionary *eachMDdata = [_mktDepthArr objectAtIndex:i];
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==1) {
                    // BID
                    [tmpBuyArr addObject:eachMDdata];
                }
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==2) {
                    // SELL
                    [tmpSellArr addObject:eachMDdata];
                }
            }
            
        } else {
            ; // empty data = do nothing  (will be handled below)
        }
        
    }
    
    //NSLog(@"tmpBuycount %d",[tmpBuyArr count]);
    
    // if tmpBuyArr empty, prepare zeroes
    if ([tmpBuyArr count]<=0) {
        int mdLevel = [mdInfo.mdLevel intValue];
        
        // NSLog(@"mdLevel %d",mdLevel);
        
        int alternate = 1;
        int pos = 0;
        for (long i=0; i<mdLevel*2; i++) {
            NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:2],@"105",
                                      [NSNumber numberWithInt:alternate],@"106",
                                      [NSNumber numberWithInt:pos],@"107",
                                      [NSNumber numberWithInt:0],@"118",
                                      @"KL",@"131",
                                      [NSNumber numberWithInt:0],@"170",
                                      @"5517.KL",@"33",
                                      @"0",@"45",
                                      [NSNumber numberWithInt:0],@"58",
                                      [NSNumber numberWithInt:0],@"68",
                                      nil];
            
            if (alternate==1) {
                [tmpBuyArr addObject:zeroDict];
                alternate = 2;
                pos++;
            } else {
                [tmpSellArr addObject:zeroDict];
                alternate = 1;
            }
        }
        
    }
    
    
    // sort data based on FID 107
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_107_I_MARKET_DEPTH_POSITION ascending:YES];
    NSArray *limitedArrayB = [tmpBuyArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSArray *limitedArrayS = [tmpSellArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [tmpBuyArr removeAllObjects]; [tmpSellArr removeAllObjects];
    
    //NSLog(@"limitedArrayB %@",limitedArrayB);
    //NSLog(@"limitedArrayS %@",limitedArrayS);
    
    if ([limitedArrayB count]>0) {
        //
        //        [_mdFooter setHidden:NO];
        _MDBuyArr = [NSMutableArray arrayWithArray:[limitedArrayB mutableCopy]];
        _MDSellArr = [NSMutableArray arrayWithArray:[limitedArrayS mutableCopy]];
        [myCollectionView reloadData];
//        [self updateFooterData];
        
        // then send subscribe data (subscribe only if there is mktdepth)
        // TODO: subscribe and flashing
        // [[VertxConnectionManager singleton] vertxSubscribeMktDepthLevel:MDLfeature withStockCode:_stkCode];
    }
}

@end
