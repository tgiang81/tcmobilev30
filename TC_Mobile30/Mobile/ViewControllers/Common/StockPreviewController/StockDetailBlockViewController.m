//
//  StockDetailBlockViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockDetailBlockViewController.h"

@interface StockDetailBlockViewController ()

@end

@implementation StockDetailBlockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myScrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, self.view.frame.size.width * 5)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat aPageWidth = myScrollView.frame.size.width;
    float aFractionalPage = myScrollView.contentOffset.x / aPageWidth;
    NSInteger aPage = lround(aFractionalPage);
    myPageControl.currentPage = aPage;
}



@end
