//
//  StockDetailBlockViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockBaseViewController.h"


@interface StockDetailBlockViewController : StockBaseViewController <UIScrollViewDelegate>
{
    __weak IBOutlet UIScrollView *myScrollView;
    
    __weak IBOutlet UIPageControl *myPageControl;
}


@end
