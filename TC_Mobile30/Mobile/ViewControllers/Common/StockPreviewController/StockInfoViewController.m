//
//  StockInfoViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockInfoViewController.h"
#import "CustomCollectionViewLayout.h"
#import "StockDetaiCell.h"
#import "QCData.h"
#import "StockModel.h"
#import "MStockPreviewController.h"

@interface StockInfoViewController ()
{
    NSMutableArray* myDataSource;
    NSArray *mySelectedArr;
    NSMutableDictionary *myFilteredDict;
    QCData *myQcData;
    NSString *myBundleIdentifier;
}

@end

@implementation StockInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = StockInformation;
    aLayout.numberOfCols = 4;
    myCollectionView .collectionViewLayout = aLayout;
    myQcData = [QCData singleton];
    myBundleIdentifier = BUNDLEID_HARDCODE_TESTING;

    
    if ([myBundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [myBundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController.parentViewController;
    self.stkCode = aStockPreview.stock.stockCode;
    [self getDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

# pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return myDataSource.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];
        if (indexPath.row == 0) {
            cell.name.text = [[myDataSource objectAtIndex:indexPath.section] objectAtIndex:0];
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 1) {
            cell.name.text = [[myDataSource objectAtIndex:indexPath.section] objectAtIndex:1];
        } else if (indexPath.row == 2) {
            cell.name.text = [[myDataSource objectAtIndex:indexPath.section] objectAtIndex:2];
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 3) {
            cell.name.text = [[myDataSource objectAtIndex:indexPath.section] objectAtIndex:3];
        }

    return cell;
}

# pragma handle respone from server

//#pragma Update data from server
//- (void)didFinishedSortByServer:(NSNotification *)notification
//{
//    //NSLog(@"finishSort");
//
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//        // load chart view
////        [self getDataSource];
//
//        NSDictionary *response = [notification.userInfo copy];
//        NSMutableArray *sortByServerArr = [[NSMutableArray alloc]initWithArray:[response allKeys] copyItems:YES];
//
//        [UserPrefConstants singleton].userSelectingStockCodeArr = sortByServerArr;
//        mySelectedArr = [[NSArray alloc] initWithArray:sortByServerArr];
//
//        for (NSString *stkCode in mySelectedArr) {
//            if(([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
//
//                NSMutableDictionary *dictToAdd = [[NSMutableDictionary alloc] initWithDictionary:
//                                                  [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];
//
//                // add field data if not exist (it is used to sort in quote screen
//                // price or qty set to 0
//                // this is critical because these fields will be used in sorting
//                // if it does not exist, sorting will not work properly.
//
//                if ([dictToAdd objectForKey:FID_101_I_VOLUME]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_101_I_VOLUME];
//                }
//
//                // gainer/loser
//                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE];
//                }
//
//                // gainer/loser %
//                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE_PER]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE_PER];
//                }
//
//                // name
//                if ([dictToAdd objectForKey:FID_38_S_STOCK_NAME]==nil) {
//                    [dictToAdd setObject:@"-" forKey:FID_38_S_STOCK_NAME];
//                }
//
//                // trades
//                if ([dictToAdd objectForKey:FID_132_I_TOT_OF_TRD]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_132_I_TOT_OF_TRD];
//                }
//
//                //value
//                if ([dictToAdd objectForKey:FID_102_D_VALUE]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_102_D_VALUE];
//                }
//
//                //close
//                if ([dictToAdd objectForKey:FID_50_D_CLOSE]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_50_D_CLOSE];
//                }
//
//                //lacp
//                if ([dictToAdd objectForKey:FID_51_D_REF_PRICE]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_51_D_REF_PRICE];
//                }
//
//                // bid qty
//                if ([dictToAdd objectForKey:FID_58_I_BUY_QTY_1]==nil) {
//                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_58_I_BUY_QTY_1];
//                }
//
//
//                [myFilteredDict setObject:dictToAdd forKey:stkCode];
//            }
//        }
//
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"halfSecondTimerTick" object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTimerCallBack)
//                                                     name:@"halfSecondTimerTick" object:nil];
//
//    });
//}

- (void)getDataSource
{
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    NSString *compName = @"";
    //    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
    //        compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_159_S_SYSBOL2_COMP];
    //    } else {
    //        compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_39_S_COMPANY];
    //    }
    
    compName = [[[myQcData qcFeedDataDict]objectForKey:self.stkCode] objectForKey:FID_39_S_COMPANY];
    //    _stockCode.text = compName;
    
    long long totalVol = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_101_I_VOLUME]longLongValue];
    long long totalBuyVol     =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_238_I_TOTAL_BVOL]longLongValue];
    long long totalSellVol    =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_237_I_TOTAL_SVOL]longLongValue];
    long long totalBuyTran    =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]longLongValue];
    long long totalSellTran   =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_242_I_TOTAL_STRANS]longLongValue];
    long long totalMarCap     =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_100004_F_MKT_CAP]longLongValue];
    long long totalShareIssued=[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_41_D_SHARES_ISSUED]longLongValue];
    
    CGFloat lastDoneFloat = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
    NSString* lastDoneString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lastDoneFloat]];
    
    CGFloat floatPercent = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue];
    CGFloat floatChange = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_CUSTOM_F_CHANGE] floatValue];
    
    NSString* stockChangeString =  @"";
    NSString* stockChangePerString = @"";
    if (floatPercent > 0) {
        //        _stockChangePer.textColor = [UIColor greenColor];
        //        _stockChange.textColor = [UIColor greenColor];
        stockChangePerString = [NSString stringWithFormat:@"%.3f%%",floatPercent]; // cqa request no + sign
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            stockChangeString = [NSString stringWithFormat:@"%.3f",floatChange]; // cqa request no + sign
        }else{
            stockChangeString = [NSString stringWithFormat:@"%.4f",floatChange]; // cqa request no + sign
            
        }
    } else if (floatPercent<0) {
        //        _stockChangePer.textColor = [UIColor redColor];
        //        _stockChange.textColor = [UIColor redColor];
        stockChangePerString= [NSString stringWithFormat:@"%.3f%%",floatPercent];
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            stockChangeString = [NSString stringWithFormat:@"%.3f",floatChange];
        }else{
            stockChangeString = [NSString stringWithFormat:@"%.4f",floatChange];
            
        }
    } else {
        //        _stockChangePer.textColor = [UIColor whiteColor];
        //        _stockChange.textColor = [UIColor whiteColor];
        stockChangePerString = [NSString stringWithFormat:@"%.3f%%",floatPercent];
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            stockChangeString = [NSString stringWithFormat:@"%.3f",floatChange];
        }else{
            stockChangeString = [NSString stringWithFormat:@"%.4f",floatChange];
        }
    }
    
    CGFloat lacpFloat = 0;
    
    if ([myBundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [myBundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
        lacpFloat = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_153_D_THEORETICAL_PRICE]floatValue];
    }else{
        lacpFloat = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
    }
    
    NSString* stockLacpString = @"";
    stockLacpString  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lacpFloat]];
    
    NSString* stockOpen = @"";
    CGFloat floatOpen = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_55_D_OPEN_PRICE] floatValue];
    stockOpen = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatOpen]];
    //    [self setColorForLabel:_stockOpen withPrice:(floatOpen - lacpFloat)];
    
    NSString* stockPrevCloseString = @"";
    CGFloat floatClose = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_50_D_CLOSE] floatValue];
    stockPrevCloseString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatClose]];
    //    [self setColorForLabel:_stockPrevClose withPrice:(floatClose-lacpFloat)];
    
    NSString* stockHighString = @"";
    CGFloat floatHigh =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_56_D_HIGH_PRICE] floatValue];
    stockHighString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatHigh]];
    //    [self setColorForLabel:_stockHigh withPrice:(floatHigh-lacpFloat)];
    
    NSString* stockLowString = @"";
    CGFloat floatLow =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_57_D_LOW_PRICE] floatValue];
    stockLowString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatLow]];
    //    [self setColorForLabel:_stockLow withPrice:(floatLow-lacpFloat)];
    
    NSString* stockCeilingString = @"";
    CGFloat floatCeil = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue];
    stockCeilingString  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatCeil]];
    //    [self setColorForLabel:_stockCeiling withPrice:(floatCeil-lacpFloat)];
    
    NSString* stockFloorString = @"";
    CGFloat floatFloor = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue];
    stockFloorString = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatFloor]];
    //    [self setColorForLabel:_stockFloor withPrice:(floatFloor-lacpFloat)];
    
    NSString* stockBidString = @"";
    CGFloat floatBid = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
    stockBidString  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatBid]];
    //    [self setColorForLabel:_stockBid withPrice:(floatBid-lacpFloat)];
    
    NSString* stockAsk = @"";
    CGFloat floatAsk = [[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue];
    stockAsk  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatAsk]];
    //    [self setColorForLabel:_stockAsk withPrice:(floatAsk-lacpFloat)];
    
    NSString* _stockValue = @"";
    long long valueTradedVal =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_102_D_VALUE]longLongValue];
    _stockValue = [[UserPrefConstants singleton]abbreviateNumber:valueTradedVal];
    
    NSString* _stockTrades = @"";
    long long tradesVal =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_132_I_TOT_OF_TRD]longLongValue];
    _stockTrades = [[UserPrefConstants singleton]abbreviateNumber:tradesVal];
    
    NSString* _stockBidQty = @"";
    long long bidQtyValue  =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
    _stockBidQty = [[UserPrefConstants singleton]abbreviateNumber:bidQtyValue];
    
    NSString* _stockAskQty = @"";
    long long askQtyValue  =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_78_I_SELL_QTY_1]longLongValue];
    _stockAskQty = [[UserPrefConstants singleton]abbreviateNumber:askQtyValue];
    
    NSString *stockSectorName = @"";
    //    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
    //        stockSectorName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_158_S_SYSBOL2_SECNAME];
    //    } else {
    //        stockSectorName = [[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
    //    }
    
    stockSectorName = [[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
    
    NSString *stockTradingBoard =[[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:3];
    NSString *stockTotalSellVolume =[NSString stringWithFormat:@"%@",[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]];
    NSString *ISINString = [NSString stringWithFormat:@"%@",[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_157_S_ISIN]];
    float stockParString =[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_123_D_PAR_VALUE] floatValue];
    NSString *lotSizeString = [NSString stringWithFormat:@"%@",[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]];
    unichar uc = (unichar)[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_48_S_STATUS] characterAtIndex:1]; //Just extend to 16 bits
    
    ////NSLog(@"stockTotalSellVolume %@ = %@",stockTotalSellVolume,[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]);
    
    // NSLog(@"ISINString %@", ISINString);
    
    CGFloat dur = [stockSectorName length]/10.0;
    
    //    if (sectorLabel==nil)
    //        sectorLabel=[[MarqueeLabel alloc]initWithFrame:_stockSector.frame duration:dur andFadeLength:5.0f];
    //    [sectorLabel setScrollDuration:dur];
    //    [_detail2View addSubview:sectorLabel];
    
    if ([stockSectorName length]<=0) stockSectorName = @"-";
    NSString* sectorString = stockSectorName;
    //    [sectorLabel setFont:[UIFont systemFontOfSize:14]];
    //    [_stockSector removeFromSuperview];
    //    [sectorLabel setTextColor:[UIColor whiteColor]];
    
    NSString* _stockTradingBoard = stockTradingBoard;
    NSString* _stockLotSize = lotSizeString;
    
    if (([stockTotalSellVolume length]<=0)||[stockTotalSellVolume containsString:@"null"]) stockTotalSellVolume = @"-";
    NSString* _stockSellVolume = stockTotalSellVolume;
    
    
    if (([ISINString length]<=0)||[ISINString containsString:@"null"]) ISINString = @"-";
    NSString* _stockISIN = ISINString;
    
    
    NSString* _stockParVal =  [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:stockParString]];
    
    NSString* stockStatus = [[UserPrefConstants singleton] getBoardStatus:[NSString stringWithCharacters:&uc length:1]];
    
    
    
    /*Status = FID_44_S_STATE
     3. Trading Board = FID_154_S_GROUP
     4. Total S.Sell Volume = "239"
     5. Lot. Size = "40"
     6. ISIN = "157"
     7. Foreign Ownership = "666191"
     8. Par = "123"
     9. Value Traded = ? It is "108" or "133"
     10. Delivery Basis = ?
     */
    
    //9 Apr 2015. Sometimes feed will return negative value...
    //Nov 2016 - feed not return negative! but it was out of range must use long long for "volumes" data
    
    if (totalMarCap<=0) {
        
        totalMarCap = totalShareIssued * lastDoneFloat;
        
        if (lastDoneFloat<=0) {
            
            totalMarCap = totalShareIssued * lacpFloat;
        }
    }
    
    NSString *lblVolumeString = [[UserPrefConstants singleton] abbreviateNumber:totalVol];
    NSString *lblBuyString = [[UserPrefConstants singleton] abbreviateNumber:totalBuyVol];
    NSString *lblSellString = [[UserPrefConstants singleton] abbreviateNumber:totalSellVol];
    NSString* _stockVolume    = lblVolumeString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalVol]];
    NSString* _stockCeiling      = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]]];
    NSString* _stockFloor       = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]]];
    
    // lblPrevClose.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue]]];
    
    NSString* _stockBuyTotal  =lblBuyString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalBuyVol]];
    NSString* _stockSellTotal = lblSellString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalSellVol]];
    NSString* _stockBuyTrans   =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalBuyTran]];
    NSString* _stockSellTrans  =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalSellTran]];
    NSString* _stockShareIssued     =[[UserPrefConstants singleton]abbreviateNumber:totalShareIssued];
    NSString* _stockMktCap       =[[UserPrefConstants singleton]abbreviateNumber:totalMarCap];
    
    
    myDataSource = [[NSMutableArray alloc] initWithObjects:@[@"Last", lastDoneString, @"Bid", stockBidString ], @[@"Change",stockChangeString, @"Bid.Qty", _stockBidQty], @[@"Change%", stockChangePerString, @"Ask", stockAsk], @[@"Open", stockOpen, @"Ask.Qty", _stockAskQty], @[@"PrevClose", stockPrevCloseString, @"Tot Buy Vol", _stockBuyTotal], @[@"High", stockHighString, @"Tot Sell Vol", _stockSellVolume], @[@"Low", stockLowString, @"Tot Buy Trans", _stockBuyTrans], @[@"Ceiling", stockCeilingString, @"Tot Sell Trans", _stockSellTrans], @[@"Floor", stockFloorString, @"Trading Board", _stockTrades], @[@"LACP", stockLacpString, @"Tot S.Sell Vol", _stockSellVolume], @[@"Volume", _stockVolume, @"ISIN", _stockISIN], @[@"Value", _stockValue, @"Par Value", _stockParVal], @[@"Trades", _stockTrades, @"Lot Size", _stockLotSize], @[@"Sector",stockSectorName, @"Share Issued", _stockShareIssued], @[@"Status",stockStatus, @"Mkt.Cap", _stockMktCap], nil];
    [myCollectionView reloadData];
    
}

@end
