//
//  StockChartViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockBaseViewController.h"
#import "TCChartView.h"

@interface StockChartViewController : StockBaseViewController <TCChartViewDelegate>
{
    
    __weak IBOutlet TCChartView *myChartView;
}

@property (assign, nonatomic) ChartType chartType;
@property (nonatomic) BOOL isPresented;
@property (weak, nonatomic) IBOutlet UIButton *btnDismiss;

@end
