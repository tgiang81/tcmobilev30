//
//  StockChartViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockChartViewController.h"
#import "StockModel.h"
#import "MStockPreviewController.h"

@interface StockChartViewController ()

@end

@implementation StockChartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createUI];
    self.isPresented = YES;

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController;
    self.stkCode = aStockPreview.stock.stockCode;
    myChartView.chartType = _chartType;
    myChartView.isOnlyLoadChartByImage = NO;
    myChartView.lockMode = NO;
    myChartView.stCode = self.stkCode;
    [myChartView startLoadDefaultChart];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - CreateUI
- (void)createUI{
    myChartView.delegate = self;
    [_btnDismiss setBackgroundColor:RGBA(170, 170, 170, 0.3)];
}

#pragma mark - TCChartViewDelegate
- (void)doubleTapChartView:(TCChartView *)chartView{
    self.isPresented = NO;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)onDismissAction:(id)sender {
    self.isPresented = NO;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - UITapGestureDelegate
- (void)handleDoubleTap:(UITapGestureRecognizer *)tap{
    self.isPresented = NO;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
