//
//  MStockHeaderViewController.h
//  TCiPad
//
//  Created by ngo phi long on 6/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@interface MStockHeaderViewController : BaseVC

{
    
}

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPercentUpDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpDownPrice;
//SubInfo
@property (weak, nonatomic) IBOutlet UILabel *lblVol;
@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblVal;
@property (weak, nonatomic) IBOutlet UILabel *lblLo;
@property (weak, nonatomic) IBOutlet UILabel *lblHi;
@property (weak, nonatomic) IBOutlet UILabel *lblTrd;

@end
