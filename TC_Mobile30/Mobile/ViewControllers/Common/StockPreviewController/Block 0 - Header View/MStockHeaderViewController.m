//
//  MStockHeaderViewController.m
//  TCiPad
//
//  Created by ngo phi long on 6/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MStockHeaderViewController.h"
#import "StockModel.h"
#import "UILabel+FormattedText.h"
#import "NSNumber+Formatter.h"
#import "UIView+Animation.h"
#import "MStockPreviewController.h"

@interface MStockHeaderViewController ()

@end

@implementation MStockHeaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController;
    StockModel * aStockModel = aStockPreview.stock;
    [self updateData:aStockModel shouldCheckAnimation:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateData:(StockModel *)model shouldCheckAnimation:(BOOL)shouldCheck{
    if (model) {
//        _stock = model;
        //Parse value
        _lblName.text = model.stockName;
//        _lblNameShort.text = model.stockName;
        
        _lblPrice.text = [NSString stringWithFormat:@"$ %@", [model.lastDonePrice groupNumber]];
//        _lblPriceShort.text = _lblPrice.text;
        [self setPercentChangePrice:model.changePer];
        
        //Sortcut
        [self setVol:[model.volume toShortcutNumber]];
        [self setVal:[model.dValue toShortcutNumber]];
        [self setTrd:[model.iTrd toShortcutNumber]];
        //Group
        [self setHi:[model.highPrice groupNumber]];
        [self setLo:[model.lowPrice groupNumber]];
        [self setOpen:[model.openPrice groupNumber]];
        
        //Check Animate
//        if (shouldCheck) {
//            [self setAnimateWithPriceChange:model.fChange];
//        }
    }
}

//====== Set Data ===========
- (void)setPercentUpDownValue:(NSString *)value isUp:(BOOL)isUp{
    _lblPercentUpDown.text = value;
    if (isUp) {
        _lblPercentUpDown.textColor = AppColor_PriceUp_TextColor;
        _imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Up_Price];
    }else{
        _lblPercentUpDown.textColor = AppColor_PriceDown_TextColor;
        _imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Down_Price];
    }
}
- (void)setPercentChangePrice:(NSNumber *)fChangePer{
    if (fChangePer.doubleValue > 0) {
        _imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Up_Price];
        _lblPercentUpDown.textColor = AppColor_PriceUp_TextColor;
        
        //For Short
//        _lblPriceShort.textColor = AppColor_PriceUp_TextColor;
//        _lblPercentUpDownShort.textColor = AppColor_PriceUp_TextColor;
    }
    if (fChangePer.doubleValue < 0) {
        _imgUpDownPrice.image = [UIImage imageNamed:AppIcon_Down_Price];
        _lblPercentUpDown.textColor = AppColor_PriceDown_TextColor;
        
        //For Short
//        _lblPriceShort.textColor = AppColor_PriceDown_TextColor;
//        _lblPercentUpDownShort.textColor = AppColor_PriceDown_TextColor;
    }
    if (fChangePer.doubleValue == 0) {
        _imgUpDownPrice.image = nil;
        _lblPercentUpDown.textColor = AppColor_PriceUnChanged_TextColor;
        
        //For Short
//        _lblPriceShort.textColor = AppColor_PriceUnChanged_TextColor;
//        _lblPercentUpDownShort.textColor = AppColor_PriceUnChanged_TextColor;
    }
    _lblPercentUpDown.text = [NSString stringWithFormat:@"%@ %@", [fChangePer groupNumber], @"%"];
//    _lblPercentUpDownShort.text = _lblPercentUpDown.text;
}

- (void)setVol:(NSString *)vol{
    _lblVol.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"vol:"], vol];
    [_lblVol colorSubString:[LanguageManager stringForKey:@"vol:"] withColor:AppColor_HighlightTextColor];
}

- (void)setVal:(NSString *)val{
    _lblVal.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"val:"], val];
    [_lblVal colorSubString:[LanguageManager stringForKey:@"val:"] withColor:AppColor_HighlightTextColor];
}

- (void)setOpen:(NSString *)open{
    _lblOpen.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Open:"], open];
    [_lblOpen colorSubString:[LanguageManager stringForKey:@"Open:"] withColor:AppColor_HighlightTextColor];
}

- (void)setHi:(NSString *)hi{
    _lblHi.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Hi:"], hi];
    [_lblHi colorSubString:[LanguageManager stringForKey:@"Hi:"] withColor:AppColor_HighlightTextColor];
}

- (void)setLo:(NSString *)lo{
    _lblLo.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"Lo:"], lo];
    [_lblLo colorSubString:[LanguageManager stringForKey:@"Lo:"] withColor:AppColor_HighlightTextColor];
}

- (void)setTrd:(NSString *)trd{
    _lblTrd.text = [NSString stringWithFormat:@"%@ %@", [LanguageManager stringForKey:@"trd:"], trd];
    [_lblTrd colorSubString:[LanguageManager stringForKey:@"trd:"] withColor:AppColor_HighlightTextColor];
}

@end
