//
//  MarketDepthViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockBaseViewController.h"

@interface MarketDepthViewController : StockBaseViewController <UICollectionViewDataSource>
{
    __weak IBOutlet UICollectionView *myCollectionView;
    
}

@property (strong, nonatomic) NSMutableArray *mktDepthArr;
@property (strong, nonatomic) NSMutableArray *MDBuyArr;
@property (strong, nonatomic) NSMutableArray *MDBuyPrevArr;
@property (strong, nonatomic) NSMutableArray *MDSellArr;
@property (strong, nonatomic) NSMutableArray *MDSellPrevArr;

@end
