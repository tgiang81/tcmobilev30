//
//  BusinessDoneViewController.h
//  TCiPad
//
//  Created by ngo phi long on 4/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockBaseViewController.h"

@interface BusinessDoneViewController : StockBaseViewController
{
    
    __weak IBOutlet UICollectionView *myCollectionView;
}

@property (strong, nonatomic) NSMutableArray *bizDoneTableDataArr;

@end
