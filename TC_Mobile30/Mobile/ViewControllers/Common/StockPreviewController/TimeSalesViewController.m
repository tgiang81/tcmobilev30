//
//  TimeSalesViewController.m
//  TCiPad
//
//  Created by ngo phi long on 4/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TimeSalesViewController.h"
#import "QCData.h"
#import "CustomCollectionViewLayout.h"
#import "StockModel.h"
#import "StockDetaiCell.h"
#import "MStockPreviewController.h"

@interface TimeSalesViewController ()
{
    BOOL canRefreshTimeSales;
    NSMutableArray* myDataSource;
}

@end

@implementation TimeSalesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollectionView registerNib:[UINib nibWithNibName:@"StockDetaiCell" bundle:nil] forCellWithReuseIdentifier:@"StockDetaiCell"];
    CustomCollectionViewLayout* aLayout = [[CustomCollectionViewLayout alloc] init];
    aLayout.collectionViewLayoutType = StockTimeSales;
    aLayout.numberOfCols = 4;
    myCollectionView .collectionViewLayout = aLayout;
    canRefreshTimeSales = NO;
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    MStockPreviewController* aStockPreview = (MStockPreviewController*)self.parentViewController.parentViewController;
    self.stkCode = aStockPreview.stock.stockCode;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedTimeAndSales:) name:@"didFinishedTimeAndSales" object:nil];
    [self getTimeAndSales];
}

# pragma Collection datasource, delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (self.timeSalesArr.count > 0) {
        return self.timeSalesArr.count;
    } else {
        return 0;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StockDetaiCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"StockDetaiCell" forIndexPath:indexPath];
    
    //Inverting Order of table data
    NSUInteger session = _timeSalesArr.count - 1 - indexPath.section;
//    if (session==0)[cell setHidden:YES];
    

    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"HHmmss"];
    NSDate *currentDate = [dateFormater dateFromString:[[_timeSalesArr objectAtIndex:session]objectForKey:@"0"]];
    
    [dateFormater setDateFormat:@"HH:mm:ss"];
    NSString *convertedDateString = [dateFormater stringFromDate:currentDate];
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:NO];
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.name.text = @"Time";
        } else if (indexPath.row == 1){
            cell.name.text = @"PI";
        } else if (indexPath.row == 2){
            cell.name.text = @"Price";
        } else if (indexPath.row == 3){
            cell.name.text = @"Vol";
        }
        cell.name.textColor = [UIColor whiteColor];
    } else {
        if (indexPath.row == 0) {
            cell.name.text       = convertedDateString;
            cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 1){
            cell.name.text    = [[_timeSalesArr objectAtIndex:session]objectForKey:@"1"];
            // follow TCPlus b=red, s=green
            if ([[[_timeSalesArr objectAtIndex:session]objectForKey:@"1"] isEqualToString:@"b"])
            {
                cell.name.textColor = [UIColor greenColor];
            }
            else if ([[[_timeSalesArr objectAtIndex:session]objectForKey:@"1"] isEqualToString:@"s"])
            {
                cell.name.textColor = [UIColor redColor];
            } else
            {
                cell.name.textColor = [UIColor whiteColor];
            }
        } else if (indexPath.row == 2){
            cell.name.text      = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_timeSalesArr objectAtIndex:session]objectForKey:@"2"] floatValue]]];
            //Get Close price and incoming Price
            float closePrice;
            float incomingPrice;
            closePrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:self.stkCode]objectForKey:FID_50_D_CLOSE]floatValue];
            incomingPrice =[[[_timeSalesArr objectAtIndex:session]objectForKey:@"2"]floatValue];
            if (incomingPrice > closePrice)
                cell.name.textColor = [UIColor greenColor];
            else if (incomingPrice < closePrice)
                cell.name.textColor = [UIColor redColor];
            else if (incomingPrice == closePrice)
                cell.name.textColor = [UIColor whiteColor];
        } else if (indexPath.row == 3){
            NSString *tempVol       = [[_timeSalesArr objectAtIndex:session]objectForKey:@"3"];
            cell.name.text = [[UserPrefConstants singleton] abbreviateNumber:[tempVol intValue]];
            cell.name.textColor = [UIColor whiteColor];
        }
    }
    return cell;
}

#pragma Server
- (void)getTimeAndSales
{
    NSString *tranNo;
    tranNo = [[[[QCData singleton]qcFeedDataDict]objectForKey:self.stkCode ]objectForKey:FID_103_I_TRNS_NO];
    [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:self.stkCode begin:@"0"
                                                                    end:tranNo exchange:[UserPrefConstants singleton].userCurrentExchange];
}

-(void)refreshTimeSales {
    if (canRefreshTimeSales) {
        canRefreshTimeSales = NO;
        [self performSelector:@selector(getTimeAndSales) withObject:nil afterDelay:1.0];
    }
}

- (void)didFinishedTimeAndSales:(NSNotification *)notification
{
    canRefreshTimeSales = YES;
//    [self.refreshControl endRefreshing];
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    _timeSalesArr = [NSMutableArray arrayWithArray:data ];
    [myCollectionView reloadData];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *currentTime = [NSDate date];
//    _timeSalesLastUpdate.text = [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatter stringFromDate: currentTime]}];
    
}

@end
