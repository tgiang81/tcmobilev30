//
//  TutorialVC.m
//  TC_Mobile30
//
//  Created by Michael Seven on 1/3/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "HelpVC.h"

@implementation HelpVC

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self buildIntro];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Help guideline
- (void)buildIntro{
	//Create Panel From Nib
	MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage1"];
	MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage2"];
	MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage3"];
	MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage4"];
	MYIntroductionPanel *panel5 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage5"];
	MYIntroductionPanel *panel6 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage6"];
	MYIntroductionPanel *panel7 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage7"];
	
	//Add panels to an array
	NSArray *panels = @[panel1,panel2,panel3,panel4,panel5,panel6,panel7];
	
	//Create the introduction view and set its delegate
	MYBlurIntroductionView *introductionView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	introductionView.delegate = self;
	introductionView.BackgroundImageView.image = [UIImage imageNamed:@"main_background_icon.png"];
	[introductionView setBackgroundColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:0.65]];
	//introductionView.LanguageDirection = MYLanguageDirectionRightToLeft;
	
	//Build the introduction with desired panels
	[introductionView buildIntroductionWithPanels:panels];
	
	//Add the introduction to your view
	[self.view addSubview:introductionView];
}

#pragma mark - MYIntroductionDelegate
-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType{
	[self dismissViewControllerAnimated:YES completion:nil];
}
//-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex{
//}

//-(void)clickSubscribe:(MYBlurIntroductionView *)introductionView{
//}

@end

