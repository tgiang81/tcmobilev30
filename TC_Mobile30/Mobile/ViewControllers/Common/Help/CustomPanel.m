//
//  CustomPanel.m
//  TCiPhone_N2NMultiLogin_UAT_V1.4.5
//
//  Created by Ahmad Ashraf Azman on 17/03/2016.
//
//

#import "CustomPanel.h"
#import "MYBlurIntroductionView.h"

@implementation CustomPanel

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (IBAction)didPressEnable:(id)sender{
    [self.parentIntroductionView pressNextButton];
}

@end
