//
//  TutorialVC.h
//  TC_Mobile30
//
//  Created by Michael Seven on 1/3/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYIntroductionPanel.h"
#import "MYBlurIntroductionView.h"
#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface HelpVC : BaseVC <MYIntroductionDelegate> {
    
}

@end

NS_ASSUME_NONNULL_END
