//
//  SetupQuickAccessVC.m
//  TC_Mobile30
//
//  Created by Kaka on 12/11/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SetupQuickAccessVC.h"
#import "SetupPushNotificationVC.h"
#import "TCBaseButton.h"
#import "UIImageView+TCUtil.h"
#import "DeviceUtils.h"
#import "UIViewController+Auth.h"
#import "TCBubbleMessageView.h"
#import "JNKeychain.h"
#import "LanguageKey.h"
#import "TermAndConditionVC.h"
#import "MZFormSheetBackgroundWindowViewController.h"
#import "MZFormSheetController.h"

#define kFACE_ID_CONTENT [LanguageManager stringForKey:@"Use your face ID to unlock TCPro Mobile instead of typing your Master Password or PIN code."]
#define kTOUCH_ID_CONTENT	[LanguageManager stringForKey:@"Use your finger print to unlock TCPro Mobile instead of typing your Master Password or PIN code."]

#define kFACE_ID_TITLE [LanguageManager stringForKey:@"Unlock TCPro Mobile with Face ID"]
#define kTOUCH_ID_TITLE	[LanguageManager stringForKey:@"Unlock TCPro Mobile with Touch ID"]

#define kFACE_ID_TITLE_BUTTON	@"Use FACE ID"
#define kTOUCH_ID_TITLE_BUTTON	@"Use TOUCH ID"

@interface SetupQuickAccessVC(){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIImageView *imgMainIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblIndex;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Content;

@property (weak, nonatomic) IBOutlet TCBaseButton *btbUseQuickAccess;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnSkip;
@property (weak,nonatomic) id<TransferDataDelegate> delegate;

@end

@implementation SetupQuickAccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self createUI];
    [self setupLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	NSString *icon = @"touch_id_icon";
	if ([DeviceUtils isFaceIdSupported]) {
		icon = @"face_id_icon";
	}
	self.imgMainIcon.image = [UIImage imageNamed:icon];
	self.lblIns_Title.text = [DeviceUtils isFaceIdSupported] ? kFACE_ID_TITLE : kTOUCH_ID_TITLE;
	self.lblIns_Content.text = [DeviceUtils isFaceIdSupported] ? kFACE_ID_CONTENT : kTOUCH_ID_CONTENT;
	[self.btbUseQuickAccess setTitle:[DeviceUtils isFaceIdSupported]? kFACE_ID_TITLE_BUTTON : kTOUCH_ID_TITLE_BUTTON forState:UIControlStateNormal];
	[self updateTheme:nil];
}
- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = [UIColor whiteColor];
	NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	[self.imgMainIcon toColor:TC_COLOR_FROM_HEX(mainTintColor)];
	self.lblIndex.textColor = [UIColor blackColor];
	self.lblIns_Title.textColor = [UIColor blackColor];
	self.lblIns_Content.textColor = [UIColor blackColor];
	self.btnSkip.layer.borderColor = TC_COLOR_FROM_HEX(mainTintColor).CGColor;
	[self.btbUseQuickAccess setBackgroundColor:TC_COLOR_FROM_HEX(mainTintColor)];
	[self.btbUseQuickAccess setTitleColor:TC_COLOR_FROM_HEX(@"FFFFFF") forState:UIControlStateNormal];
	[_btnSkip setTitleColor:TC_COLOR_FROM_HEX(mainTintColor) forState:UIControlStateNormal];
	_btnSkip.borderColor = TC_COLOR_FROM_HEX(mainTintColor);
}
- (void)setupLanguage{
    _lblIns_Title.text=[LanguageManager stringForKey:Unlock_TC_Pro_Mobile_with_Touch_ID];
    _lblIns_Content.text=[LanguageManager stringForKey:Use_your_finger_print_to_unlock_TC_Pro_Mobile_instead_of_typing_your_Master_Password_or_PIN_code__];
    [_btbUseQuickAccess setTitle:[LanguageManager stringForKey:Use_Touch_ID] forState:UIControlStateNormal];
    [_btnSkip setTitle:[LanguageManager stringForKey:Skip] forState:UIControlStateNormal];
    
}
// After Agree or Decline
-(void)showedTermAndCondition:(BOOL)accepted{
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
		if (accepted) {
			[[SettingManager shareInstance] saveQuickAccess:[UserSession shareInstance].userName pass:[UserSession shareInstance].password];
			//Show Bubble Message
			NSString *message = [DeviceUtils isFaceIdSupported] ? @"FaceID was setuped successfully" : @"TouchID was setuped successfully";
			[TCBubbleMessageView showWindowBubbleMessage:message];
			//Goto next page
			SetupPushNotificationVC *_setupPush = NEW_VC_FROM_STORYBOARD(@"Setup", [SetupPushNotificationVC storyboardID]);
			[self nextTo:_setupPush animate:YES];
		}
		else{
			//Just show first - do nothing concern to QuickAcceess
			//Goto next page
			SetupPushNotificationVC *_setupPush = NEW_VC_FROM_STORYBOARD(@"Setup", [SetupPushNotificationVC storyboardID]);
			[self nextTo:_setupPush animate:YES];
		}
    }];
}
#pragma mark - ACTIONS

- (IBAction)onQuickAccessAction:(id)sender {
    [SettingManager shareInstance].isShowTermAndCondition=YES;
    TermAndConditionVC *_setupTermCondition = NEW_VC_FROM_STORYBOARD(@"Setup", @"TermAndCondition");
    _setupTermCondition.delegate = self;
    [self showPopupWithContent:_setupTermCondition inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 0.8* SCREEN_HEIGHT_PORTRAIT) showInCenter:YES topInsect:80 completion:^(MZFormSheetController *formSheetController) {
        //Do something here
    }];
}

- (IBAction)onSkipAction:(id)sender {
	//Goto next page
	SetupPushNotificationVC *_setupPush = NEW_VC_FROM_STORYBOARD(@"Setup", [SetupPushNotificationVC storyboardID]);
	[self nextTo:_setupPush animate:YES];
}

@end
