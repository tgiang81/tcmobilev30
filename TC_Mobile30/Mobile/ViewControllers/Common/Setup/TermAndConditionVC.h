//
//  TermAndConditionVC.h
//  TC_Mobile30
//
//  Created by Admin on 4/17/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN
@protocol TransferDataDelegate <NSObject>
@optional
-(void)showedTermAndCondition:(BOOL)data;

@end
@interface TermAndConditionVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet TCBaseButton *agreeButton;
@property (weak, nonatomic) IBOutlet TCBaseButton *declineButton;

@property (assign, nonatomic) BOOL isAgree;
- (IBAction)onAgree:(id)sender;
- (IBAction)onDecline:(id)sender;
@property (weak,nonatomic) id<TransferDataDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
