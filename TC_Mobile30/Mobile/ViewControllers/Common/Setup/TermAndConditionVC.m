//
//  TermAndConditionVC.m
//  TC_Mobile30
//
//  Created by Admin on 4/17/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TermAndConditionVC.h"
#import "TCBaseButton.h"

@interface TermAndConditionVC ()

@end

@implementation TermAndConditionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBackButtonDefault];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	
}
- (void)updateTheme:(NSNotification *)noti{
	[super updateTheme:noti];
	NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	[self.agreeButton setBackgroundColor:TC_COLOR_FROM_HEX(mainTintColor)];
	[self.agreeButton setTitleColor:TC_COLOR_FROM_HEX(@"FFFFFF") forState:UIControlStateNormal];
	[self.declineButton setTitleColor:TC_COLOR_FROM_HEX(mainTintColor) forState:UIControlStateNormal];
	_declineButton.borderColor = TC_COLOR_FROM_HEX(mainTintColor);
}
- (IBAction)onAgree:(id)sender {
    [SettingManager shareInstance].isAgreeWithTermAndCondition=YES;
    [self.delegate showedTermAndCondition:[SettingManager shareInstance].isAgreeWithTermAndCondition];
   
}

- (IBAction)onDecline:(id)sender {
    [SettingManager shareInstance].isAgreeWithTermAndCondition=NO;
    [self.delegate showedTermAndCondition:[SettingManager shareInstance].isAgreeWithTermAndCondition];
}
@end
