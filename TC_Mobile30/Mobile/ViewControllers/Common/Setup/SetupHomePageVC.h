//
//  SetupHomePageVC.h
//  TC_Mobile30
//
//  Created by Kaka on 12/11/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

@interface SetupHomePageVC : BaseVC
@property (weak, nonatomic) IBOutlet TCBaseButton *btnDashboard;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnPortfolio;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnIndices;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnQuote;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnWatchlist;

@end
