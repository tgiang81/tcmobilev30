//
//  SetupHomePageNewVC.m
//  TC_Mobile30
//
//  Created by Tu on 5/12/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "SetupHomePageNewVC.h"

@interface SetupHomePageNewVC ()
{
    NSArray *_listPages;
}
@end

@implementation SetupHomePageNewVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    _listPages = DEFAULT_PAGE_LIST;
//    if([BrokerManager shareInstance].broker.tag == BrokerTag_SSI){
        _listPages = @[@(DefaultPage_Dashboard), @(DefaultPage_Watchlists), @(DefaultPage_News), @(DefaultPage_Markets)];
//    }
    self.contents = [self getTitlePageList];
    [self.tbl_Content reloadData];
    // Do any additional setup after loading the view.
}

- (NSArray *)getTitlePageList{
    NSMutableArray *listTitle = @[].mutableCopy;
    for (id page in _listPages) {
        DefaultPage dfPage = [page integerValue];
        NSString *titlePage = [Utils textTitleFromPage:dfPage];
        [listTitle addObject:titlePage];
    }
    return [listTitle copy];
}

- (void)didSelectSetupButton:(UITableViewCell *)cell{
    NSIndexPath *index = [self.tbl_Content indexPathForCell:cell];
    [SettingManager shareInstance].defaultPage = [_listPages[index.row] integerValue];
    [[SettingManager shareInstance] save];
    
    SetupInvestingVC *_setupInvestingVC = NEW_VC_FROM_STORYBOARD(@"Setup", [SetupInvestingVC storyboardID]);
    [self nextTo:_setupInvestingVC animate:YES];
}
@end
