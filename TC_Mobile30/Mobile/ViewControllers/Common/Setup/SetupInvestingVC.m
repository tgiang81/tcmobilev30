//
//  SetupInvestingVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "SetupInvestingVC.h"
#import "SetupButtonCell.h"
#import "LanguageKey.h"
#define kSetupInvestingCellHeight 50.f
@interface SetupInvestingVC () <UITableViewDelegate, UITableViewDataSource, SetupButtonCellDelegate>

@end

@implementation SetupInvestingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _contents = @[@"No Experience", @"Basic", @"Intermediate", @"Advanced"];
    self.tbl_Content.delegate = self;
    self.tbl_Content.dataSource = self;
    [self.tbl_Content registerNib:[UINib nibWithNibName:[SetupButtonCell nibName] bundle:nil] forCellReuseIdentifier:[SetupButtonCell reuseIdentifier]];
    [self setupLanguage];
    // Do any additional setup after loading the view.
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
//    [self updateTableViewContentInset];
}
- (void)updateTableViewContentInset {
    CGFloat viewHeight = self.view.frame.size.height;
    CGFloat tableViewContentHeight = self.tbl_Content.contentSize.height;
    CGFloat marginHeight = (viewHeight - tableViewContentHeight - self.tbl_Content.frame.origin.y) / 2.0;
    
    self.tbl_Content.contentInset = UIEdgeInsetsMake(marginHeight, 0, -marginHeight, 0);
}
- (void)updateTheme:(NSNotification *)noti{
    self.view.backgroundColor = [UIColor whiteColor];
    self.lbl_NumberOfPage.textColor = [UIColor blackColor];
    self.lbl_Title.textColor = [UIColor blackColor];
    self.lbl_SubTitle.textColor = self.lbl_Title.textColor;
}
-(void)setupLanguage{
    _lbl_Title.text=[LanguageManager stringForKey:What_is_your_investing_experience];
}
#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kSetupInvestingCellHeight;
}
#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _contents.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SetupButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:[SetupButtonCell reuseIdentifier] forIndexPath:indexPath];
    [cell.btn_Action setTitle:_contents[indexPath.row] forState:UIControlStateNormal];
	[cell setupButton:cell.btn_Action];
    cell.delegate = self;
    return  cell;
}

#pragma mark SetupButtonCellDelegate
- (void)didSelectSetupButton:(UITableViewCell *)cell{
    NSIndexPath *index = [self.tbl_Content indexPathForCell:cell];
    [SettingManager shareInstance].userExperience = _contents[index.row];
    [[SettingManager shareInstance] save];
    AppDelegateAccessor.container.leftViewSwipeGestureEnabled = YES;
    [AppDelegateAccessor startMainApp];
}
@end
