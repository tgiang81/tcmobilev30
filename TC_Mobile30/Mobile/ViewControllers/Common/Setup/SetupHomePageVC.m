//
//  SetupHomePageVC.m
//  TC_Mobile30
//
//  Created by Kaka on 12/11/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SetupHomePageVC.h"
#import "TCBaseButton.h"
#import "LanguageKey.h"
#import "SetupInvestingVC.h"

@interface SetupHomePageVC (){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UILabel *lblIndex;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Content;
@property (weak, nonatomic) IBOutlet UIStackView *listPageItemsStack;

@end

@implementation SetupHomePageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self createUI];
    [self setupLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self updateTheme:nil];
}
-(void)setupLanguage{
    _lblIns_Title.text=[LanguageManager stringForKey:Choose_your_Home_Page];
    _lblIns_Content.text=[LanguageManager stringForKey:Select_from_a_list_of_screen_to_become_a_default_Home_Page__You_may_change_this_later_in_your_Settings_page__];
    [_btnDashboard setTitle:[LanguageManager stringForKey:_Dashboard] forState:UIControlStateNormal];
    [_btnPortfolio setTitle:[LanguageManager stringForKey:_Portfolio] forState:UIControlStateNormal];
    [_btnIndices setTitle:[LanguageManager stringForKey:_Indices] forState:UIControlStateNormal];
    [_btnQuote setTitle:[LanguageManager stringForKey:Quote] forState:UIControlStateNormal];
    [_btnWatchlist setTitle:[LanguageManager stringForKey:_Watchlist] forState:UIControlStateNormal];
}

- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = [UIColor whiteColor];
	NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	self.lblIndex.textColor = [UIColor blackColor];
	self.lblIns_Title.textColor = [UIColor blackColor];
	self.lblIns_Content.textColor = [UIColor blackColor];
	for (id v in self.listPageItemsStack.subviews) {
		if ([v isKindOfClass:[TCBaseButton class]]) {
			TCBaseButton *item = (TCBaseButton *)v;
			item.borderColor = TC_COLOR_FROM_HEX(mainTintColor);
			[item setTitleColor:TC_COLOR_FROM_HEX(mainTintColor) forState:UIControlStateNormal];
		}
	}
}

#pragma mark - Actions

- (IBAction)onSelectPageAction:(TCBaseButton *)sender {
	DefaultPage selectedPage = sender.tag;
//	if (selectedPage == DefaultPage_MarketSummary || selectedPage == DefaultPage_Accounts) {
//		[self showAlert:@"Warning!!!" message:@"Coming soon...\nPlease choose other page to make as default. Thanks"];
//		return;
//	}
	[SettingManager shareInstance].defaultPage = selectedPage;
	[[SettingManager shareInstance] save];
    
    SetupInvestingVC *_setupInvestingVC = NEW_VC_FROM_STORYBOARD(@"Setup", [SetupInvestingVC storyboardID]);
    [self nextTo:_setupInvestingVC animate:YES];
	//Go to main app
	
}
@end
