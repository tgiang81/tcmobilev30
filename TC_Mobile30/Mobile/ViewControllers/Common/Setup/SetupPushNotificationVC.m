//
//  SetupPushNotificationVC.m
//  TC_Mobile30
//
//  Created by Kaka on 12/11/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SetupPushNotificationVC.h"
#import "SetupHomePageNewVC.h"
#import "UIImageView+TCUtil.h"
#import "TCBaseButton.h"
#import "LanguageKey.h"

@interface SetupPushNotificationVC (){
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UIImageView *imgMainIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblIndex;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Title;
@property (weak, nonatomic) IBOutlet UILabel *lblIns_Content;

@property (weak, nonatomic) IBOutlet TCBaseButton *btnEnable;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnSkip;
@end

@implementation SetupPushNotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLanguage];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self updateTheme:nil];
}
- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = [UIColor whiteColor];
	NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
	[self.imgMainIcon toColor:TC_COLOR_FROM_HEX(mainTintColor)];
	self.lblIndex.textColor = [UIColor blackColor];
	self.lblIns_Title.textColor = [UIColor blackColor];
	self.lblIns_Content.textColor = [UIColor blackColor];

	[self.btnEnable setBackgroundColor:TC_COLOR_FROM_HEX(mainTintColor)];
	[self.btnEnable setTitleColor:TC_COLOR_FROM_HEX(@"ffffff") forState:UIControlStateNormal];
	[self.btnSkip setTitleColor:TC_COLOR_FROM_HEX(mainTintColor) forState:UIControlStateNormal];
	self.btnSkip.borderColor = TC_COLOR_FROM_HEX(mainTintColor);
}
- (void)setupLanguage{
    _lblIns_Title.text= [LanguageManager stringForKey:Enable_push_notification];
    _lblIns_Content.text=[LanguageManager stringForKey:Notification_may_include_alerts__sounds_and_icon_badges___These_can_be_configured_in_Settings];
    [_btnEnable setTitle:[LanguageManager stringForKey:Enable] forState:UIControlStateNormal];
    [_btnSkip setTitle:[LanguageManager stringForKey:Skip] forState:UIControlStateNormal];
}
#pragma mark - ACTIONS

- (IBAction)onEnablePushsAction:(id)sender {
	//Do setup here
	//....
	[self onSkipAction:nil];
}

- (IBAction)onSkipAction:(id)sender {
	SetupHomePageNewVC *_setupHomePage = NEW_VC_FROM_STORYBOARD(@"Setup", [SetupHomePageNewVC storyboardID]);
	[self nextTo:_setupHomePage animate:YES];
}
@end
