//
//  SetupInvestingVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 2/22/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetupInvestingVC : BaseVC
@property (weak, nonatomic) IBOutlet UILabel *lbl_NumberOfPage;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SubTitle;

@property (weak, nonatomic) IBOutlet UITableView *tbl_Content;
@property (strong, nonatomic) NSArray *contents;
- (void)didSelectSetupButton:(UITableViewCell *)cell;
@end

NS_ASSUME_NONNULL_END
