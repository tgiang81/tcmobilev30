//
//  ContactUsViewController.m
//  TCiPad
//
//  Created by conveo cutoan on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ContactUsViewController.h"


@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CreateUI
- (void)createUI{
    [self createLeftMenu];
    [self setupLanguage];
}

- (void)setupLanguage{
    //update language title for all item here
}

- (IBAction)didTapOnPhoneNumber:(id)sender {
    UIButton* aPhoneNumberButton = (UIButton*)sender;
    if ([aPhoneNumberButton isKindOfClass:[UIButton class]]) {
        NSString* aPhoneNumberString = [[aPhoneNumberButton.titleLabel.text componentsSeparatedByString:@"+"] lastObject];
        NSString *aPhoneNumber = [@"tel:+" stringByAppendingString:aPhoneNumberString];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:aPhoneNumber]];
    }
}
   
- (IBAction)didTapOnFax:(id)sender {
}

// email
- (IBAction)didTapOnEmail:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients: [NSArray arrayWithObjects:@"n2n_sales@n2nconnect.com", nil]];
        [controller setSubject:@"Support"];
        [controller setMessageBody:@"Hello there." isHTML:NO];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    } else {
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapOnWebsite:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.n2nconnect.com"]];
}


@end
