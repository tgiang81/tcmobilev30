//
//  IntroductionViewController.m
//  TCiPad
//
//  Created by conveo cutoan on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "IntroductionViewController.h"
#import "GHWalkThroughView.h"

static NSString * const desc1 = @"Monitor the stock market and watchlists, while keeping track of your portfolio performance.";

static NSString * const desc2 = @" Stay up to date with real time stock quotes, key statistics, and stock change notifications.";

static NSString * const desc3 = @"Trade And View Multiple Exchanges.";

static NSString * const desc4 = @"Monitor your favourite stocks by adding them into your Watchlist.";

static NSString * const desc5 = @"Add New Price Aleart and Received Push Notifications.";

@interface IntroductionViewController () <GHWalkThroughViewDataSource>

@property (nonatomic, strong) GHWalkThroughView* ghView ;

@property (nonatomic, strong) NSArray* descStrings;
@property (nonatomic, strong) NSArray* imageStrings;
@property (nonatomic, strong) NSArray* titleStrings;

@property (nonatomic, strong) UILabel* welcomeLabel;

@end

@implementation IntroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self setupPageControl];
}

-(void)setupPageControl
{
    _ghView = [[GHWalkThroughView alloc] initWithFrame:self.navigationController.view.bounds];
    [_ghView setDataSource:self];
    [_ghView setWalkThroughDirection:GHWalkThroughViewDirectionVertical];
    UILabel* welcomeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 50)];
    welcomeLabel.text = @"TCMobile SECURITIES";
    welcomeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];
    welcomeLabel.textColor = [UIColor blackColor];
    welcomeLabel.textAlignment = NSTextAlignmentCenter;
    self.welcomeLabel = welcomeLabel;
    
    self.descStrings = [NSArray arrayWithObjects:desc1,desc2, desc3, desc4, desc5, nil];
    self.imageStrings = [NSArray arrayWithObjects:@"Quote",@"StockInfo", @"Exchange", @"Watchlist", @"Subcribe", nil];
    self.titleStrings = [NSArray arrayWithObjects:@"Market Overview",@"Stock Details", @"Mutiple Exchanges", @"Watchlist", @"Stock Alert", nil];
    
    [_ghView setFloatingHeaderView:self.welcomeLabel];
    [self.ghView setWalkThroughDirection:GHWalkThroughViewDirectionHorizontal];
    [self.ghView showInView:self.view animateDuration:0.3];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CreateUI
- (void)createUI{
    [self createLeftMenu];
    [self setupLanguage];
}

- (void)setupLanguage{
    //update language title for all item here
}

#pragma mark - GHDataSource

-(NSInteger) numberOfPages
{
    return 5;
}

- (void) configurePage:(GHWalkThroughPageCell *)cell atIndex:(NSInteger)index
{
    cell.title = [self.titleStrings objectAtIndex:index];
//    cell.titleImage = [UIImage imageNamed:[NSString stringWithFormat:@"title%ld", (long)(index+1)]];
    cell.desc = [self.descStrings objectAtIndex:index];
}

- (UIImage*) bgImageforPage:(NSInteger)index
{
    UIImage* image = nil;
    if (index < self.imageStrings.count) {
        NSString* imageName =[NSString stringWithFormat:@"%@.png", [self.imageStrings objectAtIndex:index]];
        image = [UIImage imageNamed:imageName];
    }
    return image;
}

@end
