//
//  DetailBrokerVC.m
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DetailBrokerVC.h"
#import "DetailBrokerCell.h"
#import "BrokerAPI.h"
#import "BrokerManager.h"
#import "LoginMobileVC.h"
#import "TCBaseButton.h"

#import "BrokerModel.h"
#import "DetailBrokerModel.h"

#import "VertxConnectionManager.h"
#import "MZFormSheetController.h"
#import "LanguageKey.h"

@interface DetailBrokerVC ()<UITableViewDataSource, UITableViewDelegate>{
	NSMutableArray *_listBrokers;
	NSMutableArray *_selectedIndexPaths;
}

@end

@implementation DetailBrokerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	_listBrokers = @[].mutableCopy;
	_selectedIndexPaths = @[].mutableCopy;
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self.navigationController.navigationBar setHidden:NO];
}
- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	[self.navigationController.navigationBar setHidden:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CreateUI
- (void)createUI{
	[self setBackButtonDefault];
	//Remove footer on empty cell
	self.tblContent.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Action

- (IBAction)onBackAction:(id)sender {
	[self popView:YES];
}

- (IBAction)onApplyAction:(id)sender {
	if (_selectedIndexPaths.count == 0) {
		[self showCustomAlertWithMessage:[LanguageManager stringForKey:@"Opp! No Broker selected."]];
		return;
	}
	[Utils showApplicationHUD];
	NSIndexPath *indexPath = _selectedIndexPaths[0];
	BrokerModel *broker = _listBrokers[indexPath.row];
	[BrokerManager shareInstance].broker = broker;
	//Call API
	[[BrokerAPI shareInstance] getDetailBroker:broker completion:^(id result, NSError *error) {
		//[[Utils shareInstance] dismissWindowHUD];
		[Utils dismissApplicationHUD];
		if (error || !result) {
			[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."]];
			return ;
		}
		//Success
		DetailBrokerModel *detailModel = [[DetailBrokerModel alloc] initWithDictionary:result error:nil];
		if (detailModel) {
			[BrokerManager shareInstance].detailBroker = detailModel;
			[[BrokerManager shareInstance] save];
			//Cache in old file
			[Utils selectedBroker:detailModel];
			//Connect socket
			[[VertxConnectionManager singleton] connectingUponLogin];
		}
		//Refresh data
		[self postNotification:kNotification_DidSelectBroker object:nil];
		__weak DetailBrokerVC *_weakSelf = self;
		[self mz_dismissFormSheetControllerAnimated:NO completionHandler:^(MZFormSheetController * _Nonnull formSheetController) {
			//Handle UI below
			if (_weakSelf.delegate) {
				[_weakSelf.delegate didSelectBroker:detailModel];
			}
		}];
	}];
}
#pragma mark - LoadData
- (void)loadData{
	//Get detail broker here
	DLog(@"Get broker detail");
	_lblTitle.text = _selectedSupperBroker.appName;
	[[Utils shareInstance] showWindowHudWithTitle:nil];
	[[BrokerAPI shareInstance] openSubListBrokersFrom:_selectedSupperBroker completion:^(id result, NSError *error) {
		[[Utils shareInstance] dismissWindowHUD];
		if (error || !result) {
			[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."]];
			return ;
		}
		//Success
		DLog(@"+++ Detail list: %@", result);
		[self parseDataFrom:result];
		[self->_tblContent reloadData];
	}];
}

#pragma mark - Utils VC
- (void)parseDataFrom:(NSDictionary *)result{
	NSString *sortKey;
	for (NSString *key in result.allKeys) {
		if ([key isEqualToString:@"SortBrokerListBy"]) {
			sortKey = result[key];
		}
	
		if ([key isEqualToString:@"BrokerList"]) {
			_listBrokers = [[BrokerModel arrayOfModelsFromDictionaries:result[key] error:nil] mutableCopy];
		}
	}
	//resort listBroker
	if (_listBrokers && _listBrokers.count > 0) {
		_listBrokers = [[[BrokerManager shareInstance] sortArray:_listBrokers byKey:sortKey] mutableCopy];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_listBrokers count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	DetailBrokerCell *cell = [tableView dequeueReusableCellWithIdentifier:[DetailBrokerCell reuseIdentifier]];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	BrokerModel *model = _listBrokers[indexPath.row];
	BOOL selected = [_selectedIndexPaths containsObject:indexPath];
	[cell setupDataFrom:model shoudlSelected:selected];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	/*
	if (![_selectedIndexPaths containsObject:indexPath]) {
		[_selectedIndexPaths addObject:indexPath];
		DetailBrokerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
		[cell shouldSelectedCell:YES];
	}
	*/
	[_selectedIndexPaths addObject:indexPath];
	[self onApplyAction:nil];
}

//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//	if ([_selectedIndexPaths containsObject:indexPath]) {
//		[_selectedIndexPaths removeObject:indexPath];
//		DetailBrokerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//		[cell shouldSelectedCell:NO];
//	}
//}
@end
