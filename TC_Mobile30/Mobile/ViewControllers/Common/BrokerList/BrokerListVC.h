//
//  BrokerListVC.h
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class DetailBrokerModel;
@protocol BrokerListVCDelegate<NSObject>
- (void)didSelectBroker:(DetailBrokerModel *)broker;
@end
@interface BrokerListVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
//Data
@property (weak, nonatomic) id <BrokerListVCDelegate>delegate;
@end
