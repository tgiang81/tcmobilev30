//
//  BrokerListVC.m
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BrokerListVC.h"
#import "BrokerCell.h"
#import "BrokerModel.h"
#import "BrokerManager.h"
#import "DetailBrokerVC.h"
#import "BrokerAPI.h"
#import "DetailBrokerModel.h"
#import "VertxConnectionManager.h"
#import "MZFormSheetController.h"

@interface BrokerListVC ()<UITableViewDelegate, UITableViewDataSource, DetailBrokerVCDelegate>{
	BrokerModel *_selectedBroker;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation BrokerListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//initial first;
	[self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

#pragma mark - CreateUI
- (void)createUI{
	//[self setBackButtonDefault];
	//self.customBarTitle = [LanguageManager stringForKey:@"Select Broker"];
	//self.view.backgroundColor = [UIColor clearColor];
	//Create RightBarButton: Make done selection
	/*
	UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSelectionBroker:)];
	self.navigationItem.rightBarButtonItem = rightBtn;
    */
	self.lblTitle.text = [LanguageManager stringForKey:@"Select Broker"];
	[self.navigationController setNavigationBarHidden:YES];
}

- (void)setupLanguage{
	
}

#pragma mark - UTILS
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size completion:(void (^)(MZFormSheetController *formSheetController))completion{
	MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:contentVC];
	formSheet.presentedFormSheetSize = size;
	formSheet.shadowRadius = 8.0;
	formSheet.shadowOpacity = 0.8;
	formSheet.cornerRadius = 8.0;
	formSheet.shouldCenterVertically = NO;
	formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveToTopInset;
	formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
	formSheet.shouldDismissOnBackgroundViewTap = NO;
	formSheet.portraitTopInset = 130;
	[self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
		//passing data here
		completion(formSheet);
	}];
}
#pragma mark - Action

- (IBAction)onDismissAction:(id)sender {
	/*
	[UIView animateWithDuration:0.5 animations:^{
		[self dismissViewControllerAnimated:YES completion:nil];
	}];
	[UIView transitionWithView:self.view duration:2.0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
		[self.view setFrame:CGRectZero];
	} completion:^(BOOL finished) {
		[self dismissViewControllerAnimated:NO completion:nil];
	}];
	
	Way 2
	[UIView animateWithDuration:0.6
						  delay:0.1
						options: UIViewAnimationOptionCurveLinear
					 animations:^{
						 self.view.frame = CGRectMake(0, SCREEN_HEIGHT_PORTRAIT, SCREEN_WIDTH_PORTRAIT, SCREEN_HEIGHT_PORTRAIT);
					 }
					 completion:^(BOOL finished){
						 [self dismissViewControllerAnimated:NO completion:nil];
					 }];
	*/
	[self dismissPopup:YES completion:nil];
}
/*
- (void)doneSelectionBroker:(id)sender{
	//Handle done selection
	if (_currentSelectionIndex == -1) {
		[[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"Please select a broker"]
									 inController:self withTitle:@""];
		return;
	}
	//Handle here
	_selectedBroker = [[BrokerManager shareInstance].listBrokers objectAtIndex:_currentSelectionIndex];
	if (_delegate) {
		[_delegate didSelectBroker:_selectedBroker];
	}
	[self.navigationController popViewControllerAnimated:YES];
}
*/
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [[BrokerManager shareInstance].listBrokers count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	BrokerCell *cell = [tableView dequeueReusableCellWithIdentifier:[BrokerCell reuseIdentifier]];
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	BrokerModel *model = [[BrokerManager shareInstance].listBrokers objectAtIndex:indexPath.row];
	[cell setupDataFrom:model];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	_selectedBroker = [[BrokerManager shareInstance].listBrokers objectAtIndex:indexPath.row];
	if ([Utils isMultiInBroker:_selectedBroker]) {
		DetailBrokerVC *_detailVC = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [DetailBrokerVC storyboardID]);
		_detailVC.selectedSupperBroker = _selectedBroker;
		_detailVC.delegate = self;
		[self nextTo:_detailVC animate:YES];
	}else{
		//Select broker
		DLog(@"+++ Get broker detail");
		[BrokerManager shareInstance].broker = _selectedBroker;
		//* +++ Hardcode get data from local
        NSDictionary * result = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource: _selectedBroker.plistLink ofType:@"plist"]];
        NSError * error;
        DetailBrokerModel *detailModel = [[DetailBrokerModel alloc] initWithDictionary:result error:&error];

        [BrokerManager shareInstance].detailBroker = detailModel;
        [[BrokerManager shareInstance] save];
        //Cache in old file
        [Utils selectedBroker:detailModel];
        DLog(@"+++ Broker Detali: %@: ", result);
        //Handle Delegate
        if (self->_delegate) {
            [_delegate didSelectBroker:detailModel];
        }
        //Handle UI below
        [self onDismissAction:nil];
		
		
		//+++ Call api get broker detail from Server
		/* +++ Get data at cloud
		[[Utils shareInstance] showWindowHudWithTitle:[LanguageManager stringForKey:@"LOADING..."]];
		[[BrokerAPI shareInstance] getDetailBroker:_selectedBroker completion:^(id result, NSError *error) {
			[[Utils shareInstance] dismissWindowHUD];
			if (error || !result) {
				[self showCustomAlert:[LanguageManager stringForKey:@"Error!"] message:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."]];
				return ;
			}
			//Success
			DetailBrokerModel *detailModel = [[DetailBrokerModel alloc] initWithDictionary:(NSDictionary *)result error:&error];
			[BrokerManager shareInstance].detailBroker = detailModel;
			[[BrokerManager shareInstance] save];
			//Cache in old file
			[Utils selectedBroker:detailModel];
			DLog(@"+++ Broker Detali: %@: ", result);
			//Connect socket
			[[VertxConnectionManager singleton] connectingUponLogin];
			//Handle Delegate
			if (self->_delegate) {
				[self->_delegate didSelectBroker:detailModel];
			}
			//Handle UI below
			[self onDismissAction:nil];
		}];
		*/
	}
}

/*
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
	BrokerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	cell.accessoryType = UITableViewCellAccessoryNone;
}
*/

//+++ FOR HIGHLIGHT CELL
/*
- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"Will Selected At Index Path : %@", @(indexPath.row));
	return indexPath;
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	return YES;
}
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did highlight");
	BrokerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell shouldHighlight:YES];
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath{
	DLog(@"+++ did unhighlight");
	BrokerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	[cell shouldHighlight:NO];
}
*/
#pragma mark - DetailBrokerVCDelegate
- (void)didSelectBroker:(DetailBrokerModel *)broker{
	//Just dismiss self now. Because we did use Notification to update data
	[self onDismissAction:nil];
}
- (void)didCancelSelection{
	[self onDismissAction:nil];
}
@end
