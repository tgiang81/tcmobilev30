//
//  DetailBrokerVC.h
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
@class TCBaseButton;
@class DetailBrokerModel;

@protocol DetailBrokerVCDelegate <NSObject>
@optional
- (void)didSelectBroker:(DetailBrokerModel *)broker;
- (void)didCancelSelection;
@end
@interface DetailBrokerVC : BaseVC{
	
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UITableView *tblContent;
//Passing data
@property (strong, nonatomic) BrokerModel *selectedSupperBroker;
@property (weak, nonatomic) id <DetailBrokerVCDelegate> delegate;

@end
