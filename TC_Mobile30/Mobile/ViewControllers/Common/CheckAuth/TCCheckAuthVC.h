//
//  TCCheckAuthVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN
@protocol TCCheckAuthVCDelegate<NSObject>
@optional
- (void)didSelectCancel;
- (void)didSelectConfirm;
- (void)didSelectTouchId;
- (void)authCompletion;
@end
@interface TCCheckAuthVC : BaseVC
@property (weak, nonatomic) id<TCCheckAuthVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Status;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIButton *btn_Auth;
@property (weak, nonatomic) IBOutlet UITextField *tf_Password;
@property (weak, nonatomic) IBOutlet UIButton *btn_Cancel;
@property (weak, nonatomic) IBOutlet UIButton *btn_Confirm;

@end

NS_ASSUME_NONNULL_END
