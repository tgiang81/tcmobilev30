//
//  TCCheckAuthVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCCheckAuthVC.h"
#import "DeviceUtils.h"
#import "UIViewController+Auth.h"
#import "UIViewController+SetupButton.h"
@interface TCCheckAuthVC ()
{
    CGFloat defaultCornerRadius;
}
@end

@implementation TCCheckAuthVC

- (void)viewDidLoad {
    [super viewDidLoad];
    defaultCornerRadius = 4;
    self.enableTapDismissKeyboard = YES;
    [self.lbl_Status setHidden:YES];
    NSString *iconID = @"touch_id_icon";
    if ([DeviceUtils isFaceIdSupported]) {
        iconID = @"face_id_icon";
    }
    [_btn_Auth setImage:[UIImage imageNamed:iconID] forState:UIControlStateNormal];
    
    self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_mainColor);
    self.lbl_Title.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_textColor);
    self.tf_Password.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_textColor);
    
    
    //alertView.buttonDefaultBgColor = TC_COLOR_FROM_HEX(mainTintColor);//Right
    //alertView.buttonDefaultTextColor = TC_COLOR_FROM_HEX(@"FFFFFF");//Alway white
    //alertView.buttonDefaultBorderColor = TC_COLOR_FROM_HEX(mainTintColor);;
    //
    //alertView.buttonDestructiveBgColor = [UIColor clearColor];//TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_cancelColor);
    //alertView.buttonDestructiveTextColor = TC_COLOR_FROM_HEX(mainTintColor);
    //alertView.buttonDestructiveBorderColor = TC_COLOR_FROM_HEX(mainTintColor);
    
    NSString *mainTintColor = [ThemeManager shareInstance].tintColor;
    [self setupButton:self.btn_Confirm corner:defaultCornerRadius borderColor:TC_COLOR_FROM_HEX(mainTintColor) bgColor:TC_COLOR_FROM_HEX(mainTintColor) textColor:TC_COLOR_FROM_HEX(@"FFFFFF")];
    
    [self setupButton:self.btn_Cancel corner:defaultCornerRadius borderColor:TC_COLOR_FROM_HEX(mainTintColor) bgColor:[UIColor clearColor] textColor:TC_COLOR_FROM_HEX(mainTintColor)];
}

- (IBAction)cancel:(id)sender {
    [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
        if([self.delegate respondsToSelector:@selector(didSelectCancel)]){
            [self.delegate didSelectCancel];
        }
    }];
}
- (IBAction)confirm:(id)sender {
    [self authPasswordAction:self.tf_Password.text completion:^(BOOL auth, NSError *error) {
        if(auth){
            [UserPrefConstants singleton].isAuthOrderBook = YES;
            [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
                if([self.delegate respondsToSelector:@selector(authCompletion)]){
                    [self.delegate authCompletion];
                }
            }];
        }else{
            [self.lbl_Status setHidden:NO];
        }
    }];
    
}
- (IBAction)touchId:(id)sender {
    [self authIdAction:^(BOOL auth, NSError *error) {
        if(auth){
            [UserPrefConstants singleton].isAuthOrderBook = YES;
            [self dismissPopup:YES completion:^(MZFormSheetController *formSheetController) {
                if([self.delegate respondsToSelector:@selector(authCompletion)]){
                    [self.delegate authCompletion];
                }
            }];
        }
    }];
}


@end
