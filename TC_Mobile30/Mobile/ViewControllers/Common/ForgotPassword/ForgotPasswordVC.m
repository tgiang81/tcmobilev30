//
//  ForgotPasswordVC.m
//  TCiPad
//
//  Created by Kaka on 3/27/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ForgotPasswordVC.h"

#define kVerticalOffsetAnimationDuration        0.30    // the duration of the animation for the view shift

@interface ForgotPasswordVC () {
    BOOL keyup;
    CGFloat keyupValue;
}
@property (nonatomic, retain) IBOutlet UILabel *userIdLabel;
@property (nonatomic, retain) IBOutlet UITextField *userIdField;
@property (nonatomic, retain) IBOutlet UIButton *retrieveHintButton;
@property (nonatomic, retain) IBOutlet UITextField *hintAnswerField;
@property (nonatomic, retain) IBOutlet UILabel *hintTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *hintQuestionLabel;
@property (nonatomic, retain) IBOutlet UILabel *hintAnswerLabel;
@property (nonatomic, retain) IBOutlet UIButton *submitButton;

- (void) shiftView:(int) direction;
- (IBAction) getHint:(id)sender;
- (IBAction) submitForgotPassword:(id)sender;
- (IBAction)backgroundTouched:(id)sender;
@end

@implementation ForgotPasswordVC
@synthesize userIdLabel, userIdField, retrieveHintButton, hintTitleLabel, hintQuestionLabel, hintAnswerLabel, hintAnswerField, submitButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self setBackButtonDefault];
	[self setupLanguage];
    self.userIdLabel.textColor = [UIColor darkGrayColor];
    self.hintTitleLabel.textColor = [UIColor darkGrayColor];
    self.hintQuestionLabel.textColor = [UIColor darkGrayColor];
    self.hintAnswerLabel.textColor = [UIColor darkGrayColor];
    
    [self.retrieveHintButton setBackgroundColor:[UIColor grayColor]];
    [self.submitButton setBackgroundColor:[UIColor grayColor]];
    
    self.hintTitleLabel.alpha = 0;
    self.hintQuestionLabel.alpha = 0;
    self.hintAnswerLabel.alpha = 0;
    self.hintAnswerField.alpha = 0;
    self.submitButton.alpha = 0;
    self.retrieveHintButton.enabled = YES;
    self.userIdField.enabled = YES;
    
    //View offset when keyboard up
    keyup = NO;
    keyupValue = 0.0f;
    
//    [self.view addSubview:activityView];
//    activityView.alpha = 0;
    
//    if ([[GlobalVariables sharedVariables].systemVersion intValue] >= 7) {
//        userIdLabel.font = [UIFont systemFontOfSize:14];
//        hintTitleLabel.font = [UIFont systemFontOfSize:14];
//        hintAnswerLabel.font = [UIFont systemFontOfSize:14];
//    }
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        userIdField.textContentType = @"";
        hintAnswerField.textContentType = @"";
    }
}
- (void)setupLanguage{
	self.title = [LanguageManager stringForKey:@"Forgot Password"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Button Action
- (IBAction) getHint:(id)sender
{
    [userIdField resignFirstResponder];
    //[self shiftView:5];
    if ([[userIdField text] length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"System Message"]
                                                        message:[LanguageManager stringForKey:@"Please key in your User ID."]
                                                       delegate:nil
                                              cancelButtonTitle:[LanguageManager stringForKey:@"OK"]
                                              otherButtonTitles:nil];
        [alert show];
    }
    else {
//        if ([UserPrefConstants singleton].AES_EncryptionEnabled) {
//            if (asiHttpGetKey == nil) {
//                asiHttpGetKey = [[ASIHttpGetKey alloc] init];
//                asiHttpGetKey.delegate = self;
//                asiHttpGetKey.appData = appData;
//            }
//            if ([asiHttpGetKey isRunning]) {
//            }
//            else {
//                if ([AppConfigManager Enable_ATPLoadBalance]) {
//                    [asiHttpGetKey atpGetPK_loadBalance];
//                }
//                else {
//                    [asiHttpGetKey atpGetPK];
//                }
//
//                activityView.alpha = 1;
//                [activityIndicator startAnimating];
//            }
//        }
//        else {
//            if (asiHttpGetHints == nil) {
//                asiHttpGetHints = [[ASIHttpGetHints alloc] init];
//                asiHttpGetHints.delegate = self;
//                asiHttpGetHints.appData = appData;
//            }
//            if ([asiHttpGetHints isRunning]) {
//            }
//            else {
//                [asiHttpGetHints prepareHints:[userIdField text]];
//
//                activityView.alpha = 1;
//                [activityIndicator startAnimating];
//            }
//        }
    }
    
}

- (IBAction) submitForgotPassword:(id)sender
{
    [hintAnswerField resignFirstResponder];
    //[self shiftView:5];
    if ([[hintAnswerField text] length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"System Message"]
                                                        message:[LanguageManager stringForKey:@"Please key in your Answer to Hint."]
                                                       delegate:nil
                                              cancelButtonTitle:[LanguageManager stringForKey:@"OK"]
                                              otherButtonTitles:nil];
        [alert show];
    }
    else {
//        if (asiHttpGetHints == nil) {
//            asiHttpGetHints = [[ASIHttpGetHints alloc] init];
//            asiHttpGetHints.delegate = self;
//            asiHttpGetHints.appData = appData;
//        }
//        if ([asiHttpGetHints isRunning]) {
//        }
//        else {
//            if ([AppConfigManager isEncryption]) {
//                [asiHttpGetHints prepareForgotPINOrPWD2:[userIdField text] withHintAns:[hintAnswerField text] isPIN:NO];
//
//                activityView.alpha = 1;
//                [activityIndicator startAnimating];
//            }
//            else {
//                [asiHttpGetHints prepareForgotPINOrPWD:[userIdField text] withHintAns:[hintAnswerField text] isPIN:NO];
//
//                activityView.alpha = 1;
//                [activityIndicator startAnimating];
//            }
//        }
    }
}

-(IBAction)backgroundTouched:(id)sender
{
    [userIdField resignFirstResponder];
    [hintAnswerField resignFirstResponder];
    //if (keyup) {
    //    [self shiftView:5];
    //}
}

#pragma mark ----- View Shifting for when Keyboard comes up
- (void) shiftView:(int) direction {
    [UIView animateWithDuration:kVerticalOffsetAnimationDuration animations:^(void) {
        CGRect frame = self.view.frame;
        switch (direction) {
            case 1:
                frame.origin.y = -20.f;
                keyup = YES;
                break;
            case 2:
                frame.origin.y = -60.f;
                keyup = YES;
                break;
            case 3:
                frame.origin.y = -90.f;
                keyup = YES;
                break;
            case 4:
                frame.origin.y = -120.f;
                keyup = YES;
                break;
            case 5:
                frame.origin.y = frame.origin.y + keyupValue;
                keyup = NO;
                break;
            default:
                break;
        }
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    [self.view setNeedsDisplay];
}

#pragma mark UITextField delegate override methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    //[self shiftView:5];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
//    keyupValue = [LayoutInfo adjustTextfield:textField ofView:self.view inMainView:self.view withDirection:1];
    if (keyupValue != 0) {
        keyup = YES;
    }
    /*
     if (textField == hintAnswerField) {
     [self shiftView:4];
     }
     */
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (keyup) {
        [self shiftView:5];
    }
}

@end
