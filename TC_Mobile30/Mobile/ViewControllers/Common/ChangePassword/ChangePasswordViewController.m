//
//  ChangePasswordViewController.m
//  TC_Mobile30
//
//  Created by Michael Seven on 2/18/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ChangePasswordViewController.h"
#import "GTMRegex.h"
#import "JNKeychain.h"
#import "UserDefaultData.h"
#import "LanguageManager.h"
#import "AppMacro.h"
#import "ChangePasswordAPI.h"
#import "ChangePinAPI.h"
#import "ErrorCodeConstants.h"
#import "UIViewController+Alert.h"
#import "LanguageKey.h"

#define kVerticalOffsetAnimationDuration        0.30    // the duration of the animation for the view shift
#define SALT_HASH @"FvTivqTqZXsgLLx1v3P8TGRy"   // use for decipher data from keychain

typedef uint32_t CC_LONG;       /* 32 bit unsigned integer */
typedef uint64_t CC_LONG64;     /* 64 bit unsigned integer */

extern unsigned char *CC_MD5(const void *data, CC_LONG len, unsigned char *md)
API_AVAILABLE(macos(10.4), ios(2.0));

@implementation ChangePasswdViewController
@synthesize action_type, fromLoginVC;
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
///        appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createUI];
    
    //View offset when keyboard up
    keyup = NO;
    keyupValue = 0.0f;
    
    [self setLabelAndErrorMsg];
	textCurrentPasswd.text = @"";
	textNewPasswd.text = @"";
	textConfirmPasswd.text = @"";
    [[LanguageManager defaultManager] applyAccessibilityId:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark - CreateUI
- (void)createUI{
    //View offset when keyboard up
    keyup = NO;
    keyupValue = 0.0f;
    [self setBackButtonDefault];
    [self setLabelAndErrorMsg];
    textCurrentPasswd.text = @"";
    textNewPasswd.text = @"";
    textConfirmPasswd.text = @"";
}

- (void)setLabelAndErrorMsg {
    switch(action_type) {
        case CHANGE_PASSWORD_CODE:
            [self setTitle:@"Change Password"];
            labelNewPasswd.text = @"New Password";
            labelCurrentPasswd.text = @"Current Password";
            labelConfirmPasswd.text = @"Confirm Password";
            
            err_msg1 = @"Current Password must not be blank";
            err_msg2 = @"New Password must not be blank";
            err_msg3 = @"Confirm Password must not be blank";
            err_msg4 = @"Your confirm password does not match your new password. Please enter your confirm password correctly.";
            break;
        case CHANGE_PIN_CODE:
            [self setTitle:@"Change PIN"];
            labelNewPasswd.text = @"New PIN";
            labelCurrentPasswd.text = @"Current PIN";
            labelConfirmPasswd.text = @"Confirm PIN";
            
            textNewPasswd.keyboardType = UIKeyboardTypeNumberPad;
            textConfirmPasswd.keyboardType = UIKeyboardTypeNumberPad;
            
            err_msg1 = @"Current PIN must not be blank";
            err_msg2 = @"New PIN must not be blank";
            err_msg3 = @"Confirm PIN must not be blank";
            err_msg4 = @"Your confirm PIN does not match your new PIN. Please enter your confirm PIN correctly.";
            break;
        default:
            break;
    }
}

- (IBAction)updatePassword:(id)sender {
    [textNewPasswd resignFirstResponder];
    [textCurrentPasswd resignFirstResponder];
    [textConfirmPasswd resignFirstResponder];
    
    if ([textCurrentPasswd.text isEqualToString:@""]) {
        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:err_msg1];
        return;
    }
    
    if ([textNewPasswd.text isEqualToString:@""]) {
        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:err_msg2];
        return;
    }
    
    if ([textConfirmPasswd.text isEqualToString:@""]) {
        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:err_msg3];
        return;
    }
    
    if (![textNewPasswd.text isEqualToString:textConfirmPasswd.text]) {
        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:err_msg4];
        return;
    }
    _textCurrentPasswordSave = [[NSString alloc] initWithFormat:@"%@", textCurrentPasswd.text];
    _textNewPasswordSave = [[NSString alloc] initWithFormat:@"%@", textNewPasswd.text];
    
    if (action_type == CHANGE_PASSWORD_CODE) {
        dispatch_group_t group = dispatch_group_create();
        dispatch_group_enter(group);
        [[ChangePasswordAPI shareInstance] doChangePassword:_textCurrentPasswordSave withNewPassword:_textNewPasswordSave completion:^(id response, NSError *error) {
    
            if (error) {
                // The Network connection failed
                [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[error localizedDescription]];
            }else{
                if (response) {
                    self->err_changePWPinFailed = (NSString *)response;
                    if ([self->err_changePWPinFailed isEqualToString:@""] || self->err_changePWPinFailed == nil) {
                        //The process failure
                        self->err_changePWPinFailed = [ErrorCodeConstants ATP_TradeChgPasswd_Fail:YES];
                    }
                    NSString *messageID = [[[response componentsSeparatedByString:@"-"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    DLog(@"Type of response message %@",messageID);
                    if ([messageID isEqualToString:@"1710ATP"]) {
                        [UserSession shareInstance].password = self->_textNewPasswordSave;
                        if([SettingManager shareInstance].isActivateTouchID) {
                           //Save new info into QuickAccess
							NSString *username = [[SettingManager shareInstance] getQuickAccessName];
							[[SettingManager shareInstance] saveQuickAccess:username pass:_textNewPasswordSave];
                        }
                        [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:response
                            withOKAction:^{
                                if (self->fromLoginVC) {//if ChangePassword is called from Login screen, so callback to Login
                                    if ([self->delegate respondsToSelector:@selector(ChangePasswordSuccess)]) {
                                        [self->delegate ChangePasswordSuccess];
                                    }
                                }
                                else {
                                    [self.navigationController popViewControllerAnimated:YES];
                                }
                            }
                            cancelAction:^{}
                         ];
                    }
                    else {
                        [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:response];
                    }
                }
            }
            dispatch_group_leave(group);
        }];
    }
    else {
        dispatch_group_t group = dispatch_group_create();
        dispatch_group_enter(group);
        [[ChangePinAPI shareInstance] doChangePin:_textCurrentPasswordSave withNewPin:_textNewPasswordSave completion:^(id response, NSError *error) {
            if (error) {
                // The Network connection failed
                [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[error localizedDescription]];
            }else{
                if (response) {
                    self->err_changePWPinFailed = (NSString *)response;
                    if ([self->err_changePWPinFailed isEqualToString:@""] || self->err_changePWPinFailed == nil) {
                        //The process failure
                        self->err_changePWPinFailed = [ErrorCodeConstants ATP_TradeChgPin_Fail:YES];
                    }
                    NSString *messageID = [[[response componentsSeparatedByString:@"-"] objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
                    DLog(@"Type of response message %@",messageID);
                    [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:response
                             withOKAction:^{
                                 if (self->fromLoginVC) {//if ChangePin is called from Login screen, so callback to Login
                                     if ([self->delegate respondsToSelector:@selector(ChangePinSuccess)]) {
                                         [self->delegate ChangePinSuccess];
                                     }
                                 }
                                 else {
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }
                             }
                             cancelAction:^{}];
                }
            }
            dispatch_group_leave(group);
        }];
    }
}
//tap to hide background
- (IBAction)backgroundTouched:(id)sender
{
//    [appDelegate.appData timeoutActivity];
    
    [textCurrentPasswd resignFirstResponder];
    [textNewPasswd resignFirstResponder];
    [textConfirmPasswd resignFirstResponder];
    if (keyup) {
        [self shiftView:5];
    }
}

#pragma mark View Shifting for when Keyboard comes up
- (void) shiftView:(int) direction {
    [UIView animateWithDuration:kVerticalOffsetAnimationDuration animations:^(void) {
        CGRect frame = self.view.frame;
        switch (direction) {
            case 1:
                frame.origin.y = -20.f;
                self->keyup = YES;
                break;
            case 2:
                frame.origin.y = -60.f;
                self->keyup = YES;
                break;
            case 3:
                frame.origin.y = -90.f;
                self->keyup = YES;
                break;
            case 5:
                frame.origin.y = frame.origin.y + self->keyupValue;
                self->keyup = NO;
                break;
            default:
                break;
        }
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
    [self.view setNeedsDisplay];
}


#pragma mark - Delegate UITextField
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == textCurrentPasswd) {
        [textNewPasswd becomeFirstResponder];
    }
    else if (textField == textNewPasswd) {
        [textConfirmPasswd becomeFirstResponder];
    }
    else {
        [textConfirmPasswd resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *resultStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int max_length = (action_type == CHANGE_PASSWORD_CODE) ? 20 : 6;
    if (resultStr.length > max_length) {
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (keyupValue != 0) {
        keyup = YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (keyup) {
        [self shiftView:5];
    }
}

@end

