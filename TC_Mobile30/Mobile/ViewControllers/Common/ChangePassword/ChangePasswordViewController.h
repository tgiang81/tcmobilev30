//
//  ChangePasswordViewController.h
//  TC_Mobile30
//
//  Created by Michael Seven on 2/18/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#ifndef ChangePasswordViewController_h
#define ChangePasswordViewController_h

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "BaseVC.h"

@protocol ChangePasswdDelegate
@optional
-(void)ChangePasswordSuccess;
-(void)ChangePinSuccess;
@end
@interface ChangePasswdViewController : BaseVC <UITextFieldDelegate> {
    AppDelegate *appDelegate;
    
    IBOutlet UILabel *labelCurrentPasswd;
    IBOutlet UILabel *labelNewPasswd;
    IBOutlet UILabel *labelConfirmPasswd;
    
    IBOutlet UITextField *textCurrentPasswd;
    IBOutlet UITextField *textNewPasswd;
    IBOutlet UITextField *textConfirmPasswd;
    
    IBOutlet UIButton *buttonSubmit;
    
    IBOutlet UIView *activityView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    NSString *err_msg1;
    NSString *err_msg2;
    NSString *err_msg3;
    NSString *err_msg4;
    NSString *err_changePWPinFailed;
    
//    NSInteger action_type;
    
    BOOL keyup;
    CGFloat keyupValue;
    BOOL fromLoginVC;
    
}
@property (nonatomic, strong) NSString *textCurrentPasswordSave;
@property (nonatomic, strong) NSString *textNewPasswordSave;
@property (nonatomic, assign) id <NSObject, ChangePasswdDelegate> delegate;
//@property (nonatomic, assign) N2NTraderAppDelegate *appDelegate;
@property (nonatomic, assign) NSInteger action_type;
@property BOOL fromLoginVC;
- (IBAction) updatePassword:(id)sender;
- (IBAction) backgroundTouched:(id)sender;
- (void) setLabelAndErrorMsg;
- (void) shiftView:(int) direction;
@end

#endif /* ChangePasswordViewController_h */
