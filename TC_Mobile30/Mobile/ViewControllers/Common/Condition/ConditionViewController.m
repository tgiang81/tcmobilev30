//
//  ConditionViewController.m
//  TCiPad
//
//  Created by conveo cutoan on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "ConditionViewController.h"

@interface ConditionViewController ()

@end

@implementation ConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CreateUI
- (void)createUI{
    [self createLeftMenu];
    [self setupLanguage];
}

- (void)setupLanguage{
    //update language title for all item here
}


@end
