//
//  SelectHomePageVC.m
//  TC_Mobile30
//
//  Created by Kaka on 11/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SelectHomePageVC.h"
#import "TCBaseView.h"
#import "LanguageKey.h"
@interface SelectHomePageVC ()<UITableViewDelegate, UITableViewDataSource>{
	
}
//OUTLETS
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblInstruction;
@property (weak, nonatomic) IBOutlet TCBaseView *viewContent;
@property (weak, nonatomic) IBOutlet UITableView *tblContent;

@end

@implementation SelectHomePageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self createUI];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - CreateUI
- (void)createUI{
	[self setupLanguage];
	//Hide navigation Bar
	[self.navigationController setNavigationBarHidden:YES];
	_tblContent.dataSource = self;
	_tblContent.delegate = self;
	[self.tblContent setSeparatorColor:RGB(242, 250, 254)];
	[self updateTheme:nil];
	AppDelegateAccessor.container.leftViewSwipeGestureEnabled = NO;
}
- (void)setupLanguage{
	_lblTitle.text = [LanguageManager stringForKey:@"HOME"];
	_lblInstruction.text = [LanguageManager stringForKey:@"Select from a list of screens to become your default home page! You may change this later in your Settings page."];
}

- (void)updateTheme:(NSNotification *)noti{
	self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].subBgColor);
	_lblTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].textHomePageColor);
	_lblInstruction.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].textHomePageColor);
	[_tblContent reloadData];
}
#pragma mark - LoadData
- (void)loadData{
	[_tblContent reloadData];
}


#pragma mark - UTILS
- (NSString *)textTitleFromPage:(DefaultPage)dfPage{
	NSString *title = @"";
	switch (dfPage) {
		case DefaultPage_Dashboard:
			title = [LanguageManager stringForKey:_Dashboard];
			break;
		case DefaultPage_Portfolios:
			title = [LanguageManager stringForKey:Portfolios];
			break;
        case DefaultPage_News:
            title = [LanguageManager stringForKey:@"NEWS"];
            break;
		case DefaultPage_Quotes:
			title = [LanguageManager stringForKey:Quotes];
			break;
		case DefaultPage_Watchlists:
			title = [LanguageManager stringForKey:_Watchlist];
			break;
		case DefaultPage_Markets:
			title = [LanguageManager stringForKey:@"Indicies"];
			break;
		default:
			break;
	}
	return title;
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [DEFAULT_PAGE_LIST count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *ID_CELL = @"CELL_PAGE";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID_CELL];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID_CELL];
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	DefaultPage page = [DEFAULT_PAGE_LIST[indexPath.row] integerValue];
	cell.textLabel.text = [Utils textTitleFromPage:page];
	cell.textLabel.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].textTintHomePageColor);
	cell.textLabel.font = AppFont_MainFontMediumWithSize(14);
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	DefaultPage selectedPage = [DEFAULT_PAGE_LIST[indexPath.row] integerValue];
	[SettingManager shareInstance].defaultPage = selectedPage;
	[[SettingManager shareInstance] save];
	//Go to main app
	AppDelegateAccessor.container.leftViewSwipeGestureEnabled = YES;
	[AppDelegateAccessor startMainApp];
}
@end
