//
//  RegisterVC.m
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "RegisterVC.h"
#import "RegisterModel.h"
#import "Utils.h"
#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"
#import "LoginMobileVC.h"
#import "LanguageKey.h"

@interface RegisterVC () <UITextFieldDelegate>
{
    UIEdgeInsets _insetScrollView;
    NSString* myAlertString;
    RegisterModel* myObject;
    NSArray *myHintTypeList;
    NSInteger selectedHint;
    NSString* myCurrentQuestion;
    VertxConnectionManager *vertxConnectionManager;
    ATPAuthenticate *myAuthenticate;
}

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self createUI];
    [self hiddenAutoCorrectionOfKeyboard];
    [self registerNotificationKeyboard];
    self.enableTapDismissKeyboard = YES;
    
    //Hint Picker List
    myHintTypeList = [[NSArray alloc] initWithObjects:@"My own question", @"Mother maiden's name", @"My favourite cartoon", @"My favourite car", @"Name of my pet", nil];
    myCurrentQuestion = @"My own question";
    myObject = [[RegisterModel alloc] init];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_scrollView setContentInset:UIEdgeInsetsMake(0, 0, self.contentView.frame.size.height - self.view.frame.size.height, 0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

#pragma mark - CreateUI
- (void)createUI{
	[self createLeftMenu];
	[self setupLanguage];
}

- (void)setupLanguage{
	//update language title for all item here
}

#pragma mark - Detect Keyboard
- (void)registerNotificationKeyboard{
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

#pragma mark - Notification
#pragma mark - Notification Keyboard
- (void)keyboardWillShow:(NSNotification *)noti{
    [self calculateInsetForKeyboardShow:YES withNotification:noti];
}

- (void)keyboardWillHide:(NSNotification *)noti{
    [self calculateInsetForKeyboardShow:NO withNotification:noti];
}
/*
 ============ Calculate size for Keyboard Show ================
 */

- (void)calculateInsetForKeyboardShow:(BOOL)show withNotification:(NSNotification *)noti{
//    NSValue *value = noti.userInfo[UIKeyboardFrameBeginUserInfoKey];
//    CGRect keyboardFrame = value.CGRectValue;
    float adjustmentHeight = (self.contentView.frame.size.height - self.view.frame.size.height + 100) * (show? 1: -1);
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, adjustmentHeight, 0.0);
    if (SCREEN_HEIGHT_PORTRAIT < 568) {
        if (show) {
            _scrollView.contentInset = contentInsets;
        }else{
            _scrollView.contentInset = _insetScrollView;
            [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        return;
    }
    [UIView animateWithDuration:0.3 animations:^{
//        _scrollView.contentInset = contentInsets;
//        _scrollView.scrollIndicatorInsets = contentInsets;
        [_scrollView setContentInset:UIEdgeInsetsMake(0, 0, self.contentView.frame.size.height - self.view.frame.size.height + 100, 0)];
    }];
}

-(void)hiddenAutoCorrectionOfKeyboard
{
    _txtName.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtPassport.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtPassword.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtUsername.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtSecretAnswer.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtPhoneNumber.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtSecretQuestion .autocorrectionType = UITextAutocorrectionTypeNo;
    _txtConfirmPassword.autocorrectionType = UITextAutocorrectionTypeNo;
}

// Submit
- (IBAction)didTapOnSubmit:(id)sender {
    [self.view endEditing:YES];
    myAlertString = [self verifyInputData];
    
    if ([myAlertString length] == 0) {
        //request register to server
        [[ATPAuthenticate singleton] doRegister:myObject completionHander:^(BOOL success, id result, NSError *error) {
            if (success) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Registration" message:@"You are successful registration." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Login now" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    // Ok action example
                    id controller = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [LoginMobileVC storyboardID]);
                    [AppDelegateAccessor replaceCenterContainerToVC:controller];
                }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            [self showAlert:TC_Pro_Mobile message:myAlertString];
        }];
    }
    else {
        [self showAlert:TC_Pro_Mobile message:myAlertString];
    }
}

//Verify input data
-(NSString*)verifyInputData
{
    int count = 0;
    
    //---- Name ----
    if ([_txtName.text length] == 0) {
        _txtName.layer.borderColor = [[UIColor redColor] CGColor];
        _txtName.layer.borderWidth = 1.0;
        count++;
        myAlertString = @"You must provide a name.";
    }
    else {
        _txtName.layer.borderColor = [[UIColor clearColor] CGColor];
        
        
        //DLog(@"Name:%@", fieldName.text);
        [myObject setName:_txtName.text];
    }
    
    //---- IC Number ----
    if ([_txtPassport.text length] == 0) {
        _txtPassport.layer.borderColor = [[UIColor redColor] CGColor];
        _txtPassport.layer.borderWidth = 1.0;
        count++;
        myAlertString = @"You must provide an I/C number.";
    }
    else {
        _txtPassport.layer.borderColor = [[UIColor clearColor] CGColor];
        
        //DLog(@"IC No:%@", fieldICNo.text);
        [myObject setIcNo:_txtPassport.text];
    }
    
    //---- Email ----
    if ([_txtEmail.text length] == 0) {
        
        _txtEmail.layer.borderColor = [[UIColor redColor] CGColor];
        _txtEmail.layer.borderWidth = 1.0;
        count++;
        myAlertString = @"You must provide an email.";
    }
    else {
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        
        if (![emailTest evaluateWithObject:_txtEmail.text]) {
            _txtEmail.layer.borderColor = [[UIColor redColor] CGColor];
            _txtEmail.layer.borderWidth = 1.0;
            count++;
            myAlertString = @"Invalid Email.";
        }
        else {
            _txtEmail.layer.borderColor = [[UIColor clearColor] CGColor];
            [myObject setEmail:_txtEmail.text];
        }
    }
    
    //---- Mobile Number ----
    if ([_txtPhoneNumber.text length] == 0) {
        _txtPhoneNumber.layer.borderColor = [[UIColor redColor] CGColor];
        _txtPhoneNumber.layer.borderWidth = 1.0;
        count++;
        myAlertString = @"You must provide a mobile number.";
    }
    else {
        _txtPhoneNumber.layer.borderColor = [[UIColor clearColor] CGColor];
        
        //DLog(@"Mobile No:%@", fieldMobileNo.text);
        [myObject setMobileNo:_txtPhoneNumber.text];
    }
    
    //---- Username ----
    if ([_txtUsername.text length] == 0) {
        _txtUsername.layer.borderColor = [[UIColor redColor] CGColor];
        _txtUsername.layer.borderWidth = 1.0;
        count++;
        myAlertString = @"You must provide a username.";
    }
    else {
        _txtUsername.layer.borderColor = [[UIColor clearColor] CGColor];
        
        //DLog(@"Username:%@", fieldUsername.text);
        [myObject setUsername:_txtUsername.text];
    }
    
    //---- Password ----
    if ([_txtPassword.text length] == 0) {
        _txtPassword.layer.borderColor = [[UIColor redColor] CGColor];
        _txtPassword.layer.borderWidth = 1.0;
        count++;
        myAlertString = @"You must provide a password.";
    }
    else {
        NSRange range1;
        NSRange range2;
        NSRange range3;
        range1 = [_txtPassword.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
        range2 = [_txtPassword.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
        range3 = [_txtPassword.text rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
        
        if ([_txtPassword.text length] < 6 || [_txtPassword.text length] > 32) {
            _txtPassword.layer.borderColor = [[UIColor redColor] CGColor];
            _txtPassword.layer.borderWidth = 1.0;
            count++;
            myAlertString = @"Invalid Password, the password minimum length is 6 characters and maximum length is 32 characters.";
        }
        else if (!range1.length) {
            _txtPassword.layer.borderColor = [[UIColor redColor] CGColor];
            _txtPassword.layer.borderWidth = 1.0;
            count++;
            myAlertString = @"Invalid Password, the password must consist at least an alphabet.";
        }
        else if (!range2.length) {
            _txtPassword.layer.borderColor = [[UIColor redColor] CGColor];
            _txtPassword.layer.borderWidth = 1.0;
            count++;
            myAlertString = @"Invalid Password, the password must consist at least a number.";
        }
        else if (range3.length) {
            _txtPassword.layer.borderColor = [[UIColor redColor] CGColor];
            _txtPassword.layer.borderWidth = 1.0;
            count++;
            myAlertString = @"Invalid Password, special characters are not allowed.";
        }
        else {
            _txtPassword.layer.borderColor = [[UIColor clearColor] CGColor];
            
            //DLog(@"Password:%@", fieldPassword.text);
            [myObject setPassword:_txtPassword.text];
        }

    }
    
    //---- Confirm Password ----
    if ([_txtConfirmPassword.text length] == 0) {
        _txtConfirmPassword.layer.borderColor = [[UIColor redColor] CGColor];
        _txtConfirmPassword.layer.borderWidth = 1.0;
//        retypePasswordInfo.hidden = NO;
        count++;
        myAlertString = @"You must provide a confirm password.";
    }
    else {
        if (![_txtPassword.text isEqualToString:_txtConfirmPassword.text]) {
            _txtConfirmPassword.layer.borderColor = [[UIColor redColor] CGColor];
            _txtConfirmPassword.layer.borderWidth = 1.0;
//            retypePasswordInfo.hidden = NO;
            count++;
            myAlertString = @"Password does not match the Confirm Password.";
        }
        else {
            _txtConfirmPassword.layer.borderColor = [[UIColor clearColor] CGColor];
//            retypePasswordInfo.hidden = YES;
            
            //DLog(@"Confirm Password:%@", fieldRetypePassword.text);
            [myObject setRetypePassword:_txtConfirmPassword.text];
        }
    }
    
    //---- Hint ----
    if (selectedHint == 0) {
        if ([_txtSecretQuestion.text length] == 0) {
            _txtSecretQuestion.layer.borderColor = [[UIColor redColor] CGColor];
            _txtSecretQuestion.layer.borderWidth = 1.0;
            count++;
            myAlertString = @"You must provide a secret question.";
        }
        else {
            _txtSecretQuestion.layer.borderColor = [[UIColor clearColor] CGColor];

            [myObject setHintType:@"OTR"];
            [myObject setHint:_txtSecretQuestion.text];
        }
    }
    else {
        _txtSecretQuestion.layer.borderColor = [[UIColor clearColor] CGColor];
        
        [myObject setHintType:[myHintTypeList objectAtIndex:selectedHint]];
        [myObject setHint:[myHintTypeList objectAtIndex:selectedHint]];
    }
    
    //---- Answer ----
    if ([_txtSecretAnswer.text length] == 0) {
        _txtSecretAnswer.layer.borderColor = [[UIColor redColor] CGColor];
        _txtSecretQuestion.layer.borderWidth = 1.0;
        count++;
        myAlertString = @"You must provide an answer to your secret question.";
    }
    else {
        _txtSecretAnswer.layer.borderColor = [[UIColor clearColor] CGColor];
        
        //DLog(@"Answer:%@", fieldAnswer.text);
        [myObject setAnswer:_txtSecretAnswer.text];
    }
    
    if (count == 0) {
        myAlertString = @"";
    }
    else if (count == 1) {
    }
    else if (count > 1) {
        myAlertString = @"Please complete the highlighted field(s) correctly.";
    }
    return myAlertString;
}

-(BOOL)isValidEmail:(NSString*)theEmail
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:theEmail];
}

#pragma mark - action

- (IBAction)didTapOnQuestionButton:(id)sender {
    [UIView transitionWithView:self.btnSecretQuestion
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.view endEditing:YES];
                        [self.view bringSubviewToFront:self.viewPicker];
                        self.backgroundView.hidden = NO;
                        self.viewPicker.hidden = NO;
                        self.viewPicker.layer.cornerRadius = 5.0f;
                        self.viewPicker.layer.masksToBounds = YES;
                    }
                    completion:NULL];
}

- (IBAction)didTapOnSecretCancel:(id)sender {
    self.backgroundView.hidden = YES;
    self.viewPicker.hidden = YES;
}
- (IBAction)didTapOnSecretDone:(id)sender {
    self.backgroundView.hidden = YES;
    self.viewPicker.hidden = YES;
    [_btnSecretQuestion setTitle:myCurrentQuestion forState:UIControlStateNormal];
    selectedHint = [_pkvSecretQuestion selectedRowInComponent:0];
    if (selectedHint != 0) {
        _txtSecretQuestion.hidden = YES;
    } else {
        _txtSecretQuestion.hidden = NO;
    }
    
}


#pragma mark - Pikerview datasource, delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;  // Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return myHintTypeList.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [myHintTypeList objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    myCurrentQuestion = [myHintTypeList objectAtIndex:row];

}


@end
