//
//  RegisterVC.h
//  TCiPad
//
//  Created by Kaka on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@interface RegisterVC : BaseVC
{
    
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

//name
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UILabel *lblNameDes;

// Passport
@property (weak, nonatomic) IBOutlet UILabel *lblPassport;
@property (weak, nonatomic) IBOutlet UITextField *txtPassport;
@property (weak, nonatomic) IBOutlet UILabel *lblPassportDes;

//Email
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailDes;

//Phone
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumberDes;

//Username
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblUsernameDes;

//Password
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblPasswordDes;

//Confirm Password
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;

// Secret Question
@property (weak, nonatomic) IBOutlet UIButton *lblSecretQuestion;
@property (weak, nonatomic) IBOutlet UIButton *btnSecretQuestion;
@property (weak, nonatomic) IBOutlet UITextField *txtSecretQuestion;

// Secret Answer
@property (weak, nonatomic) IBOutlet UILabel *lblSecretAnswer;
@property (weak, nonatomic) IBOutlet UITextField *txtSecretAnswer;
@property (weak, nonatomic) IBOutlet UILabel *lblSecretAnswerDes;
@property (weak, nonatomic) IBOutlet UIPickerView *pkvSecretQuestion;
@property (weak, nonatomic) IBOutlet UIView *viewPicker;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;


@end
