//
//  LeftCommonMenu.h
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@class TCBaseImageView;
@interface TCMenuVC : BaseVC{
	
}
@property (weak, nonatomic) IBOutlet UITableView *tblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet TCBaseImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblPosition;

//Pasing data
@property (assign, nonatomic) TCMenuType menuType;
- (void)selectItemAt:(NSIndexPath *)indexPath;
@end
