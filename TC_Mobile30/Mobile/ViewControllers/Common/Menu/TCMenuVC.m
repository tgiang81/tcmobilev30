//
//  LeftCommonMenu.m
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCMenuVC.h"
#import "CommonLeftMenuCell.h"
#import "Utils.h"
#import "LoginMobileVC.h"
#import "RegisterVC.h"
#import "AnnocementViewController.h"
#import "IntroductionViewController.h"
#import "ConditionViewController.h"
#import "ContactUsViewController.h"
#import "QuoteDetailViewController.h"
#import "AlertVC.h"

#import "MOrderBookVCNew.h"
#import "MStockTrackerVC.h"
#import "MStockStreamerVC.h"

#import "SettingVC.h"
#import "MStockTrackerModeVC.h"
#import "QRCodeReaderViewController.h"
#import "LanguageKey.h"
@interface TCMenuVC ()<UITableViewDataSource, UITableViewDelegate>{
	NSMutableArray *_menuItems;
	MenuContentType _currentSelectedCellType;
}

@end

@implementation TCMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	//Initial data
	_menuItems = @[].mutableCopy;
	self.view.backgroundColor = RGB(231, 231, 231);
	_tblContent.backgroundColor = [UIColor clearColor];
	[_tblContent setBounces:NO];
	[self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - StatusBar
-(BOOL)leftViewStatusBarHidden{
	return YES;
}
- (UIStatusBarStyle) leftViewStatusBarStyle{
	return UIStatusBarStyleDefault;
}
- (UIStatusBarAnimation)leftViewStatusBarUpdateAnimation {
	return UIStatusBarAnimationNone;
}

#pragma mark - CreateUI

#pragma mark - Load Data
- (void)loadData{
    [self registerNotifications];
	_lblVersion.text = [NSString stringWithFormat:@"%@%@", [LanguageManager stringForKey:@"Version"], [Utils currentVersionApp]];
	if (_menuType == TCMenuType_Common) {
		_currentSelectedCellType = MenuContentType_Login;
		_menuItems = @[@(MenuContentType_Login), @(MenuContentType_AboutUs), @(MenuContentType_TermAndCondition), @(MenuContentType_Announcement), @(MenuContentType_ForgotPassword), @(MenuContentType_Setting), @(MenuContentType_Tutorial)].mutableCopy;
		//Hidden Profile View
		_profileView.frame = CGRectZero;
		_profileView.hidden = YES;
	}else{
		_currentSelectedCellType = MenuContentType_DashBoard;
		_menuItems = @[@(MenuContentType_DashBoard), @(MenuContentType_AboutUs), @(MenuContentType_TermAndCondition), @(MenuContentType_Announcement),
                       @(MenuContentType_OrderBook), @(MenuContentType_StockTracker), @(MenuContentType_Portfolio),
                       
                       @(MenuContentType_Setting), @(MenuContentType_Tutorial), @(MenuContentType_Alert), @(MenuContentType_Barcode), @(MenuContentType_LogOut)].mutableCopy;
//        , @(MenuContentType_StockTracker)
		//Set Profile Info
		_lblProfileName.text = [UserSession shareInstance].userName;
		_lblPosition.text = @"####";
		//+++ Use api: atpGetClientList to get info???
	}
	[_tblContent reloadData];
}
- (void)registerNotifications{
    NSNotificationCenter *notCenter = [NSNotificationCenter defaultCenter];
    [notCenter removeObserver:self name:kNotiOrderBook object:nil];
    [notCenter addObserver:self selector:@selector(openOrderBook) name:kNotiOrderBook object:nil];
    
    [notCenter removeObserver:self name:kNotiQuote object:nil];
    [notCenter addObserver:self selector:@selector(openQuote) name:kNotiQuote object:nil];

    
    [notCenter removeObserver:self name:kNotiPortFolio object:nil];
    [notCenter addObserver:self selector:@selector(openPortfolio) name:kNotiPortFolio object:nil];
}

#pragma mark action Notifications
- (void)openQuote{
//    [self selectItemWith:MenuContentType_OrderBook];
}
- (void)openOrderBook{
    [self selectItemWith:MenuContentType_OrderBook];
}

- (void)openPortfolio{
    [self selectItemWith:MenuContentType_Portfolio];
}
#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 45.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [_menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	CommonLeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:[CommonLeftMenuCell reuseIdentifier] forIndexPath:indexPath];
	MenuContentType typeCell = [_menuItems[indexPath.row] integerValue];
	BOOL isHighlight = (_currentSelectedCellType == typeCell) ? YES : NO;
	[cell loadContent:[Utils commonMenuTitleBy:typeCell] atCellType:typeCell withHighlight:isHighlight];
	return cell;
}

#pragma mark - UITableViewDelegate
- (void)selectItemAt:(NSIndexPath *)indexPath{
    
    MenuContentType typeCell = [_menuItems[indexPath.row] integerValue];
    [self selectItemWith:typeCell];
}
- (void)selectItemWith:(MenuContentType)type{
    id controller;
    _currentSelectedCellType = type;
    [_tblContent reloadData];
    
    switch (type) {
        case MenuContentType_DashBoard:
            controller = AppDelegateAccessor.rootTabbarVC;
            break;
        case MenuContentType_Login:
            controller = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [LoginMobileVC storyboardID]);
            break;
        case MenuContentType_Announcement:
            controller = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [AnnocementViewController storyboardID]);
            break;
            
        case MenuContentType_OrderBook:
            controller = NEW_VC_FROM_NIB([MOrderBookVCNew class], @"MOrderBookVCNew");
//            ((MOrderBookVCNew *)controller).isNeededReload = YES;
            break;
            
        case MenuContentType_Portfolio:
            //controller = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [MPortfolioVC storyboardID]);
            break;
            
        case MenuContentType_StockTracker:
            
            controller = NEW_VC_FROM_NIB(MStockTrackerModeVC, @"MStockTrackerModeVC");
            ((MStockTrackerModeVC *)controller).isSteamerMode = ![[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"PH"];
            
            break;
            
        case MenuContentType_TermAndCondition:
            controller = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [ConditionViewController storyboardID]);
            break;
        case MenuContentType_AboutUs:
            controller = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [ContactUsViewController storyboardID]);
            break;
        case MenuContentType_Tutorial:
            controller = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [IntroductionViewController storyboardID]);
            break;
        case MenuContentType_Setting:
            controller = NEW_VC_FROM_STORYBOARD(kSettingStoryboardName, [SettingVC storyboardID]);
            break;
        case MenuContentType_Alert:
            controller = NEW_VC_FROM_STORYBOARD(kMainMobileStoryboardName, [AlertVC storyboardID]);
            break;
        case MenuContentType_LogOut:
            [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:Are_you_sure_want_to_log_out] withOKAction:^{
                [AppDelegateAccessor logout];
            } cancelAction:^{
                //Do nothing
            }];
            return;
        case MenuContentType_Barcode:
            controller = NEW_VC_FROM_NIB(QRCodeReaderViewController, @"QRCodeReaderViewController");
            [self presentViewController:controller animated:YES completion:nil];
            return;
        default:
            break;
    }
    if (!controller) {
        [self showCustomAlertWithMessage:@"Coming soon..."];
        return;
    }
    [AppDelegateAccessor replaceCenterContainerToVC:controller];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectItemAt:indexPath];
}
@end
