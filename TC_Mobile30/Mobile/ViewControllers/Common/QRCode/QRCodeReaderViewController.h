//
//  QRCodeReaderViewController.h
//  TCiPhone_N2NMultiLogin_UAT_V1.4.5
//
//  Created by Ahmad Ashraf Azman on 15/07/2016.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ATPAuthenticate.h"

@interface QRCodeReaderViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate, ATPDelegate>{
    
    BOOL dataProcessing;
    IBOutlet UIImageView *bgImageView;
    IBOutlet UIActivityIndicatorView *activityIndicatorView;
    IBOutlet UIView *blackView;
    IBOutlet UILabel *autheticateLabel;
    IBOutlet UIImageView *qrLineScanner;
    IBOutlet UILabel *qrcodeLabel;
}



@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL isReading;
@property (weak, nonatomic) IBOutlet UIView *viewPreview;
-(BOOL)startReading;
-(void)stopReading;
-(void)loadBeepSound;
- (NSString *) generateMobileToken;

@end
