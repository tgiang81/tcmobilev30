//
//  QRCodeReaderViewController.m
//  TCiPhone_N2NMultiLogin_UAT_V1.4.5
//
//  Created by Ahmad Ashraf Azman on 15/07/2016.
//
//

#import "QRCodeReaderViewController.h"
#import "NSData-AES.h"
#import "NSData+Base64.h"
#import "LanguageManager.h"
#import "NSData-AES.h"
#import "NSData+Base64Additions.h"
#import "StockAlertAPI.h"
#import "DeviceUtils.h"
#import "UserPrefConstants.h"
#import "ConfigureServerSession.h"
#import "LanguageKey.h"

@interface QRCodeReaderViewController ()
{
    NSString *strValue;
    ATPAuthenticate *asiHttpAuthenticate;
    
}

@end

@implementation QRCodeReaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Initially make the captureSession object nil.
    _captureSession = nil;
    
    // Set the initial value of the flag to NO.
    _isReading = NO;
    
    // Begin loading the sound effect so to have it ready for playback when it's needed.
    [self loadBeepSound];
    activityIndicatorView.hidden = YES;
    [activityIndicatorView stopAnimating];
    blackView.hidden = YES;
    autheticateLabel.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self startStopReading:self];
    
    
    CABasicAnimation *animation = [CABasicAnimation
                                   animationWithKeyPath:@"position"];
    
    
    
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(_viewPreview.center.x, qrcodeLabel.frame.origin.y)];
    animation.duration = 4.0;
    animation.repeatCount = HUGE_VAL;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    [qrLineScanner.layer addAnimation:animation forKey:@"position"];
}

- (IBAction)startStopReading:(id)sender {
    if (!_isReading) {
        // This is the case where the app should read a QR code when the start button is tapped.
        if ([self startReading]) {
            // If the startReading methods returns YES and the capture session is successfully
            // running, then change the start button title and the status message.
            
        }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self stopReading];
        // The bar button item's title should change again.
    }
    
    // Set to the flag the exact opposite value of the one that currently has.
    _isReading = !_isReading;
}

#pragma mark - Private method implementation

- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        //DLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}


-(void)stopReading{
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
}

-(void)loadBeepSound{
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    
    NSError *error;
    
    // Initialize the audio player object using the NSURL object previously set.
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        // If the audio player cannot be initialized then log a message.
        //DLog(@"Could not play beep file.");
        //DLog(@"%@", [error localizedDescription]);
    }
    else{
        // If the audio player was successfully initialized then load it in memory.
        [_audioPlayer prepareToPlay];
    }
}
 

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            // If the found metadata is equal to the QR code metadata then update the status label's text,
            // stop reading and change the bar button item's title and the flag's value.
            // Everything is done on the main thread.
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(saveData:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            ////DLog(@"%@",[metadataObj stringValue]);
            
            
            
            _isReading = NO;
            
            // If the audio player is not nil, then play the sound effect.
            if (_audioPlayer) {
                [_audioPlayer play];
            }
        }
    }
}

- (void) saveData:(NSString *)stringValue{
    DLog(@"string Value %@",stringValue);
    
    
    NSData *data = [NSData base64DataFromString:stringValue];
    
    NSData *decrypt = [data AES256DecryptWithKeyCBC:@"4ba2dca2cd70234103611b7f7858cb15"];
    
    DLog(@"WR CODE %@", [NSString stringWithUTF8String:[decrypt bytes]]);
    
    strValue = [NSString stringWithUTF8String:[decrypt bytes]];
    
    activityIndicatorView.hidden = NO;
    [activityIndicatorView startAnimating];
    blackView.hidden = NO;
    autheticateLabel.hidden = NO;
    
    if (asiHttpAuthenticate != nil) {
        asiHttpAuthenticate.delegate = nil;
        asiHttpAuthenticate = nil;
    }
    asiHttpAuthenticate = [ATPAuthenticate singleton];
    asiHttpAuthenticate.delegate = self;
    
    if ([asiHttpAuthenticate isRunning]) {
        DLog(@"Already Running");
    }
    else {
        asiHttpAuthenticate.fromQRCodeLogin = YES;
        if ([UserPrefConstants singleton].ATPLoadBalance) {
            [asiHttpAuthenticate atpGetPK_LoadBalance];
        } else {
            [asiHttpAuthenticate atpGetPK];
        }
    }
    
    
}
-(void)doLoginQRCode {
    [self ASIHttpLoginQRSuccess];
}
-(void)updateLoginProgress:(LoginStages)progress {
    NSString *msg = @"";
    
    switch (progress) {
        case LOGIN_INIT:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"0%"}];
            break;
        case LOGIN_PK:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"20%"}];
            break;
        case LOGIN_KEY2:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"40%"}];
            break;
        case LOGIN_SESSION2:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"60%"}];
            break;
        case LOGIN_E2EE:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"80%"}];
            break;
        case LOGIN_LOGIN:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"95%"}];
            break;
        case LOGIN_SUCCESS:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"100%"}];
            break;
        default:
            break;
    }
}
- (void) ASIHttpLoginQRSuccess {
    
    NSArray *myWords = [strValue componentsSeparatedByString:@"||"];
    if (myWords.count==3) {
        [self authenticateQRCodeLogin:[myWords objectAtIndex:0] andServerToken:[myWords objectAtIndex:1] andTimeStamp:[myWords objectAtIndex:2]];
    }else{
        UIAlertController *alert = [UIAlertController
                 alertControllerWithTitle:[LanguageManager stringForKey:TC_Pro_Mobile]
                 message:[LanguageManager stringForKey:@"Unable to scan this QR Code"]
                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:[LanguageManager stringForKey:@"Ok"]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                    }];
        [alert addAction:yesButton];
        dispatch_async(dispatch_get_main_queue(), ^{
            //Your main thread code goes in here
            [self presentViewController:alert animated:YES completion:nil];
        });
       
    }
}

- (void) authenticateQRCodeLogin:(NSString *)brokerCode
                  andServerToken:(NSString *)token
                    andTimeStamp:(NSString *)timeStamp{
    DLog(@"Broker Code %@",brokerCode);
    DLog(@"token %@",token);
    DLog(@"timeStamp %@",timeStamp);
    StockAlertAPI *api = [StockAlertAPI new];
    NSString *deviceID =  [api identifierForVendor];
    NSString *mobileToken = [api getMobileToken];
    
    NSString *urlString = @"";
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlString = [NSString stringWithFormat:@"https://%@/[%@]QRLoginAuthen?2=%@|1=%@|/=%@|0=%@|.=098", [UserPrefConstants singleton].userATPServerIP,[ConfigureServerSession shareInstance].currentSessionKey, brokerCode,mobileToken,deviceID,[DeviceUtils getDeviceModel]];
    }else{
        urlString = [NSString stringWithFormat:@"http://%@/[%@]QRLoginAuthen?2=%@|1=%@|/=%@|0=%@|.=098", [UserPrefConstants singleton].userATPServerIP,[ConfigureServerSession shareInstance].currentSessionKey, brokerCode,mobileToken,deviceID,[DeviceUtils getDeviceModel]];
    }
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    DLog(@"QRCODE authentication %@", urlString);
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         DLog(@"%@", data);

         
         UIAlertController *alert;
         UIAlertAction* yesButton = [UIAlertAction
                                     actionWithTitle:[LanguageManager stringForKey:@"Done"]
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         [self dismissViewControllerAnimated:YES completion:nil];
                                     }];
         if ([data length] > 0 && error == nil)
         {
             NSString *responseData = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             responseData =  [responseData stringByReplacingOccurrencesOfString:@"(=3|4|\r\n)=" withString:@""];
             NSArray *tokens = [responseData componentsSeparatedByString:@"|"];

             
             if ([[tokens objectAtIndex:0] intValue]==0) {
                 alert = [UIAlertController
                  alertControllerWithTitle:@""
                  message:[LanguageManager stringForKey:[NSString stringWithFormat:@"Successfull Login to TCPlus"]]
                  preferredStyle:UIAlertControllerStyleAlert];
                

             }else{
                 alert = [UIAlertController
                          alertControllerWithTitle:@""
                          message:[NSString stringWithFormat:@"%@", [tokens objectAtIndex:1]]
                          preferredStyle:UIAlertControllerStyleAlert];
                 
             }
             [alert addAction:yesButton];
             dispatch_async(dispatch_get_main_queue(), ^{
                 //Your main thread code goes in here
                 [self presentViewController:alert animated:YES completion:nil];
             });
         }
         else if ([data length] == 0 && error == nil)
         {
             [self dismissViewControllerAnimated:YES completion:nil];
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             [self dismissViewControllerAnimated:YES completion:nil];
         }
         else if (error != nil)
         {
             alert = [UIAlertController
                      alertControllerWithTitle:@""
                      message:error.localizedDescription
                      preferredStyle:UIAlertControllerStyleAlert];
             
             [alert addAction:yesButton];
             dispatch_async(dispatch_get_main_queue(), ^{
                 //Your main thread code goes in here
                 [self presentViewController:alert animated:YES completion:nil];
             });
         }
         
         
         
         
         
     }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)close:(id)sender {
    [self stopReading];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
