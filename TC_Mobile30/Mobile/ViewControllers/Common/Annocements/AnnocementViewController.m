//
//  AnnocementViewController.m
//  TCiPad
//
//  Created by conveo cutoan on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "AnnocementViewController.h"

@interface AnnocementViewController ()

@end

@implementation AnnocementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self loadContentUrl];
    myWebView.delegate = self;
    myWebView.hidden = YES;
    [_loadingActivity startAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CreateUI
- (void)createUI{
    [self createLeftMenu];
    [self setupLanguage];
}

- (void)setupLanguage{
    //update language title for all item here
}

-(void)loadContentUrl
{
    NSString *urlString = @"https://www.itradecimb.com.sg/app/public.mobile.notices.z";
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [myWebView loadRequest:urlRequest];
}

#pragma mark - Webview Delegates

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [myWebView setHidden:NO];
    [_loadingActivity stopAnimating];
    [_loadingActivity setHidden:YES];
    
}

@end
