//
//  AnnocementViewController.h
//  TCiPad
//
//  Created by conveo cutoan on 3/20/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"

@interface AnnocementViewController : BaseVC <UIWebViewDelegate>
{
    __weak IBOutlet UIWebView *myWebView;
    
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivity;



@end
