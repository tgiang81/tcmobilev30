//
//  SplashVC.m
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "SplashVC.h"
#import "ProcessViewController.h"
#import "BrokerAPI.h"
#import "BrokerManager.h"
#import "BrokerModel.h"
#import "DetailBrokerModel.h"
#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"
#import "LanguageKey.h"

@interface SplashVC ()

@end

@implementation SplashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	self.view.backgroundColor = [UIColor blackColor];
}

- (void)viewDidLayoutSubviews{
	[super viewDidLayoutSubviews];
	[self getBrokerList];
}
- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	//initial Common Menu
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Main Actions
- (void)getLocalConfigs{
	//Test Get from Local file...
	NSDictionary *result = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"config_com.n2n.ipad.brokerlist" ofType:@"plist"]];
	BrokerManager *_brokerMng = [BrokerManager shareInstance];
	//Parse to get Broker list
	[_brokerMng parseDataFromDict:result];
	
	//Success
	BrokerModel *_selectedBroker = [self selectedDefaultBrokerFrom:[BrokerManager shareInstance].listBrokers];
	
	result = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource: _selectedBroker.plistLink ofType:@"plist"]];
	
	NSError * error;
	DetailBrokerModel *detailModel = [[DetailBrokerModel alloc] initWithDictionary:result error:&error];
	[BrokerManager shareInstance].detailBroker = detailModel;
	[[BrokerManager shareInstance] save];
	//Cache in old file
	[Utils selectedBroker:detailModel];
	[self startMain];
}
- (void)getBrokerList {
    //Test Get from Local file...
	[self getLocalConfigs];
	return;
	
	[[Utils shareInstance] showWindowHudWithTitle:[LanguageManager stringForKey:@""]];
	[[BrokerAPI shareInstance] getInitialPlistFile:^(id result, NSError *error) {
		DLog(@"Result: %@", result);
		if (error) {
			//[self handleError];
			//Get local config
			[self getLocalConfigs];
		}else{
			if (!result) {
				DLog(@"Cannot get plist file data....");
				[self handleError];
				return ;
			}
			//Success: Save data and update UI
			BrokerManager *_brokerMng = [BrokerManager shareInstance];
			[_brokerMng parseDataFromDict:result];
			//Selected Broker
			BrokerModel *_selectedBroker = [self selectedDefaultBrokerFrom:[BrokerManager shareInstance].listBrokers];
			[[BrokerAPI shareInstance] getDetailBroker:_selectedBroker completion:^(id result, NSError *error) {
				[[Utils shareInstance] dismissWindowHUD];
				if (error || !result) {
					//[self handleError];
					//++++==========+++++
					//Get local config
					[self getLocalConfigs];
					return ;
				}
				//Success
				DetailBrokerModel *detailModel = [[DetailBrokerModel alloc] initWithDictionary:(NSDictionary *)result error:&error];
				[BrokerManager shareInstance].detailBroker = detailModel;
				[[BrokerManager shareInstance] save];
				//Cache in old file
				[Utils selectedBroker:detailModel];
				//DLog(@"+++ Broker Detali: %@: ", result);
				//Connect socket
				//[[VertxConnectionManager singleton] connectingUponLogin];
	 			//Get Exchange List here
	 			//[[ATPAuthenticate singleton] getExchangelist];
				//Handle UI below
				[self startMain];
			}];
		}
	}];
}

- (void)handleError{
	__weak SplashVC *_weakSelf = self;
	dispatch_async(dispatch_get_main_queue(), ^{
		[[Utils shareInstance] dismissWindowHUD];
		[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."] withOKAction:^{
			[_weakSelf getBrokerList];
		} cancelAction:^{
			//Close app
			//Exit app
			exit(0);
		}];
	});
}

- (BrokerModel *)selectedDefaultBrokerFrom:(NSArray *)brokers{
	BrokerModel *selectedBroker = [BrokerManager shareInstance].broker;
	//+++ Force selecting expected broker
	//Get default is N2N Broker
	for (BrokerModel *model in brokers) {
		//For N2N
        //n2n BrokerTag_N2N BrokerTag_SSI
        if (model.tag == BrokerTag_N2N) { //Change this to expected Broker if needed
            selectedBroker = model;
            break;
        }
	}
	[BrokerManager shareInstance].broker = selectedBroker;
	[[BrokerManager shareInstance] save];
	/*
	if (!selectedBroker) {
		//Get default is N2N Broker
		for (BrokerModel *model in brokers) {
			if ([model.appName isEqualToString:@"N2N"]) {
				selectedBroker = model;
				break;
			}
		}
		[BrokerManager shareInstance].broker = selectedBroker;
		[[BrokerManager shareInstance] save];
	}
	*/
	return selectedBroker;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - StartMain
- (void)startMain{
	[UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.alpha = 0.3;
	} completion:^(BOOL finished) {
		//Show Common Menu here
		[AppDelegateAccessor showCommonMenu];
	}];
}
@end
