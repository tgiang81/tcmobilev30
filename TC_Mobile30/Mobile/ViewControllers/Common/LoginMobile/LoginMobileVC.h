//
//  LoginMobileVC.h
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "TCMenuDropDown.h"
#import "TCDropDown.h"
@class PADesignableView;
@class PADesignableButton;
@class TCBaseButton;
@class TCBaseImageView;
@class TCMenuDropDown;
@interface LoginMobileVC : BaseVC{
    BOOL callChangePwdAgain;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet TCBaseButton *btnTouchID;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnChooseBroker;

@property (weak, nonatomic) IBOutlet UIButton *btnLanguage;

@property (weak, nonatomic) IBOutlet TCBaseButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnLogin;

@property (weak, nonatomic) IBOutlet TCBaseButton *btnRememberMe;
@property (weak, nonatomic) IBOutlet UILabel *lblRememberMe;

@property (weak, nonatomic) IBOutlet UITextField *txtUserID;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblTouchID;
@property (weak, nonatomic) IBOutlet UILabel *lblInsOr;
@property (weak, nonatomic) IBOutlet UIView *viewTouchID;
@property (weak, nonatomic) IBOutlet UIView *viewLogin;
@property (weak, nonatomic) IBOutlet UIButton *onSignUp;
@property (weak, nonatomic) IBOutlet TCMenuDropDown *v_DropDown;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;


//For Broker
@property (weak, nonatomic) IBOutlet TCBaseImageView *imgBroker1;
@property (weak, nonatomic) IBOutlet TCBaseImageView *imgBroker2;
@property (weak, nonatomic) IBOutlet UILabel *lblBrokerName1;
@property (weak, nonatomic) IBOutlet UILabel *lblSignIn;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyN2N;
@property (weak, nonatomic) IBOutlet TCBaseImageView *imgChooseLanguage;
@property (weak, nonatomic) IBOutlet UILabel *lblNoAccount;
@property (weak, nonatomic) IBOutlet TCBaseImageView *imgChooseBroker;

- (IBAction) dismissChangePwdOrPin:(id)sender;


@end
