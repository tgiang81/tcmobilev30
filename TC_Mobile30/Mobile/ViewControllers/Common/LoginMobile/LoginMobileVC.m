//
//  LoginMobileVC.m
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "LoginMobileVC.h"
#import "BackgroundVideoObjC.h"
#import "Utils.h"
#import "ProcessViewController.h"
#import "PADesignableButton.h"
#import "PADesignableView.h"
#import "TCBaseImageView.h"
#import "BrokerListVC.h"
#import "ForgotPasswordVC.h"
#import "TCMenuDropDown.h"
#import "TCDropDown.h"

#import "AuthenticationAPI.h"
#import "BrokerManager.h"
#import "DetailBrokerModel.h"
#import "BrokerModel.h"
#import "ATPAuthenticate.h"
#import "VertxConnectionManager.h"
#import "VertxManager.h"
#import "UserDefaultData.h"
#import "TCBaseButton.h"
#import "UILabel+FormattedText.h"
#import "DeviceUtils.h"
#import "BaseNavigationController.h"
#import "StockAlertAPI.h"
#import "DefineChooseSwift.h"
#import "UIViewController+Email.h"
#import "NSString+Util.h"
#import "SelectHomePageVC.h"
#import "SetupQuickAccessVC.h"
#import "DeviceUtils.h"
#import "UIImageView+TCUtil.h"
#import "UIViewController+Auth.h"
#import "UIButton+TCUtils.h"

#import "MYIntroductionPanel.h"
#import "MYBlurIntroductionView.h"
#import "JNKeychain.h"
#import "LanguageKey.h"
#import "ChangePasswordViewController.h"

#import "ApplicationUtils.h"
#import "GCDQueue.h"
#import "UIButton+TCUtils.h"
#define kNSURLSessionTimeout 20.0



@interface LoginMobileVC ()<UITextFieldDelegate, BrokerListVCDelegate, MYIntroductionDelegate,UIPopoverPresentationControllerDelegate, TCMenuDropDownDelegate, ChangePasswdDelegate>{
	//For UIScrollView
	UIEdgeInsets _insetScrollView;
	NSString *lmsServerFromBundleID;
	NSMutableDictionary *ipadLMSDictionary; // main LMS
	NSMutableArray *allowedBrokerListArray; // allowed broker for this device
	NSMutableArray *currentBrokerListArray;
	UIViewController *popoverViewController;
	UIColor *mainTextColor;
    
    NSArray *moreInfos;
    NSMutableArray *dataSource;
    NSMutableArray *values;
}
@property (strong, nonatomic) BackgroundVideoObjC *backgroundVideo;

//OUTLETS
@property (weak, nonatomic) IBOutlet UIImageView *topLogoIcon;
@property (weak, nonatomic) IBOutlet UIImageView *usernameIcon;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *touchID_ConstrainWidthButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewLogin_ConstrainWithSelectBrokerView;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
//Controls
@property (strong, nonatomic) TCDropDown *topRankDropdown;
@property (strong, nonatomic) TCDropDown *moreInfoDropdown;

@end

@implementation LoginMobileVC

- (void)viewDidLoad {
	[super viewDidLoad];
	LogManager *log = [LogManager shared];
	[log setupWithIdentifier:LogManager.loginIdentier];
	// Do any additional setup after loading the view.
	[self createUI];
	//setup
	self.enableTapDismissKeyboard = YES;
	[[IQKeyboardManager sharedManager] setEnable:NO];
	[[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
	//Register Notification
	//==== Register Notification: Did select Broker
	[self addNotification:kNotification_DidSelectBroker selector:@selector(didSelectBrokerNotification:)];
	[self addNotification:kDidChangeLanguageNotification selector:@selector(didChangeLanguageNotification:)];
	ipadLMSDictionary       = [[NSMutableDictionary alloc] init];
	allowedBrokerListArray = [[NSMutableArray alloc] init];
	currentBrokerListArray = [[NSMutableArray alloc] init];
	//    brokerListIdDict        = [NSMutableDictionary dictionary];
	//    brokerListDetailDict    = [NSMutableDictionary dictionary];
	ipadLMSDictionary       = [[NSMutableDictionary alloc] init];
	NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;
	//NSLog(@"bundleIdentifier %@", bundleIdentifier);
	
	/*
	 if (([bundleIdentifier isEqualToString:BUNDLEID_CIMBMY_UAT])||([bundleIdentifier isEqualToString:BUNDLEID_CIMBSG_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_CIMBSGSTAG])) {
	 [_lblTitle setHidden:YES];
	 _logoImage.image = [UIImage imageNamed:@"AppLogoMY"];
	 }else if([bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS]){
	 _logoImage.image = [UIImage imageNamed:@"AppLogoAbacus"];
	 [_logoImage setFrame:CGRectMake(403, 32, 210, 90)];
	 [_lblTitle setHidden:YES];
	 }else if([bundleIdentifier isEqualToString:BUNDLEID_AMSEC_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_AMFUTURES_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_AMACCESS]){
	 _logoImage.image = [UIImage imageNamed:@"AppLogoAmAccess"];
	 [_logoImage setFrame:CGRectMake(359, 32, 297, 125)];
	 [_lblTitle setHidden:YES];
	 }
	 else{
	 _logoImage.hidden = YES;
	 }
	 
	 if ((![bundleIdentifier isEqualToString:BUNDLEID_MULTILOGIN])&&(![bundleIdentifier isEqualToString:BUNDLEID_MULTILOGIN_UAT])) {
	 _lblTitle.text =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
	 _lblBrokerName.text = @"";
	 _activeBrokerNameText.text = @"";
	 }
	 */
	
	if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
		lmsServerFromBundleID = [NSString stringWithFormat:@"http://nopl-ph.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?id=%@&ver=0&device=I",bundleIdentifier];
		
		
	}else{
		
		lmsServerFromBundleID = [NSString stringWithFormat:@"http://nogl.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?id=%@&ver=0&device=I",bundleIdentifier];
	}
	
	//No need call this here now
	//[self getPLISTData];
	[self setNeedsStatusBarAppearanceUpdate];
    [self loadDataForDropDown];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self.navigationController.navigationBar setHidden:YES];
	[self registerNotificationKeyboard];
	
	//Fake info test user
	//mlt788 / cimb1234
	//_txtUserID.text = @"mms0512011" ;//@"mms0512011";//@"testuser24";// @"mms0512011";//
	//_txtPassword.text = @"abc123";
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	//+++ No need to check disclaimer now
	//Check disclaimer user
	/*
	 if ([UserSession shareInstance].isAcceptDislaimerNote == NO) {
	 [self makeDisclaimerHidden:NO];
	 }else{
	 [self setView:_scrollView hidden:NO];
	 }
	 */
	//Re-play background
	//[self.backgroundVideo play];
	[UserPrefConstants singleton].isAuthOrderBook = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
	[self unRegiserNotificationKeyboard];
	//Stop play backgroun video
	[self.backgroundVideo pause];
}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar
- (BOOL)prefersStatusBarHidden {
	return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleDefault;
}

//Override on LGSideMenu
- (BOOL)rootViewStatusBarHidden{
	return NO;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - CreateUI
- (void)createUI{
	[self.navigationController.navigationBar setHidden:YES];
	self.view.backgroundColor = [UIColor whiteColor];
	[self createLeftMenu];
	//Play background Video
	//self.backgroundVideo = [[BackgroundVideoObjC alloc] initOnViewController:self withVideoURL:@"login2.mp4"];
	//[self.backgroundVideo setUpBackground];
	
	//Update user cached data
	[[LanguageManager defaultManager] applyAccessibilityId:self];
	if (![UserPrefConstants singleton].isSupportedMultipleLanguage) {
		_btnLanguage.enabled = NO;
	}
	//+++ disable for now -- coming soon
	//[_btnLanguage shouldEnable:NO];
	[self setupLanguage];
	
	//Forgot password button
	if ([UserPrefConstants singleton].isEnabledPassword) {
		_btnForgotPassword.hidden = NO;
	}else{
		_btnForgotPassword.hidden = YES;
	}
	
	//Label Resource N2N
	BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
	NSUInteger brokerTag = brokerModel.tag;
	if (brokerTag == BrokerTag_SSI) {
		//_lblCompanyN2N.hidden = YES; no need to hide now
	}else{
		_lblCompanyN2N.hidden = NO;
	}
	
	//TouchID button
	NSString *iconID = @"touch_id_icon";
	if ([DeviceUtils isFaceIdSupported]) {
		iconID = @"FaceID_icon";
		[self setTextForTouchIDLabel:[LanguageManager stringForKey:Sign_in_with_Face_ID]];
	}else{
		[self setTextForTouchIDLabel:[LanguageManager stringForKey:Sign_in_with_TouchID]];
	}
	[_btnTouchID setImage:[UIImage imageNamed:iconID] forState:UIControlStateNormal];
	//Disclaimer View
	//_scrollView.hidden = YES;
	//[_viewDisclaimer setHidden:YES];
	//[_viewDisclaimer setBackgroundColor:RGBA(0, 0, 0, 0.3)];
	
	//Logo image
	NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;
	
	if (([bundleIdentifier isEqualToString:BUNDLEID_CIMBMY_UAT])||([bundleIdentifier isEqualToString:BUNDLEID_CIMBSG_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_CIMBSGSTAG])) {
		//_imgLogoApp.image = [UIImage imageNamed:@"AppLogoMY"];
	}else if([bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS]){
		//_imgLogoApp.image = [UIImage imageNamed:@"AppLogoAbacus"];
	}else if([bundleIdentifier isEqualToString:BUNDLEID_AMSEC_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_AMFUTURES_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_AMACCESS]){
		//_imgLogoApp.image = [UIImage imageNamed:@"AppLogoAmAccess"];
	}
	else{
		//_imgLogoApp.hidden = YES;
	}
	
	// For Remember me
	//    [_btnRememberMe setImage:[UIImage imageNamed:@"CheckIconBlack.png"] forState:UIControlStateSelected];
	//    [_btnRememberMe setImage:[UIImage imageNamed:@"Uncheck.png"] forState:UIControlStateNormal];
	
	if ([UserSession shareInstance].isRememberMe == YES) {
		_txtUserID.text = [UserSession shareInstance].userName;
		[UserSession shareInstance].isRememberMe = YES;
		//        [_txtPassword becomeFirstResponder];
	}else{
		_txtUserID.text = @"";
		//        [_txtUserID becomeFirstResponder];
	}
	_btnRememberMe.selected = [UserSession shareInstance].isRememberMe;
	//Disable auto correct on textfield
	_txtUserID.autocorrectionType = UITextAutocorrectionTypeNo;
	_txtPassword.autocorrectionType = UITextAutocorrectionTypeNo;
	[self selectCachedBroker];
	[self updateTheme:nil];
	[self setupdropdown];
}

- (void)updateTheme:(NSNotification *)noti{
	//self.view.backgroundColor = [UIColor purpleColor];
}
- (void)setupLanguage{
	//prepare text title with multi language here
	self.title = [LanguageManager stringForKey:Login];
	_lblSignIn.text = [LanguageManager stringForKey:Sign_In];
	
	_lblInsOr.text =  [LanguageManager stringForKey:__OR__];
	_lblTouchID.text = [LanguageManager stringForKey:Sign_in_with_TouchID];
	[self setTextForTouchIDLabel:[LanguageManager stringForKey:Sign_in_with_TouchID]];
	[_btnForgotPassword setTitle:[LanguageManager stringForKey:Forgot_your_password] forState:UIControlStateNormal];
	[_btnLogin setTitle:[LanguageManager stringForKey:LOGIN] forState:UIControlStateNormal];
	//    [_btnRememberMe setTitle:[LanguageManager stringForKey:Remember_Me] forState:UIControlStateNormal];
	[self setTitleButtonLanguage];
	//    [_onSignUp setTitle:[LanguageManager stringForKey:Dont_have_Account] forState:(UIControlStateNormal)];
	[_onSignUp setTitle:[LanguageManager stringForKey:Sign_Up] forState:(UIControlStateNormal)];
	//Textfield
	_txtUserID.placeholder = [LanguageManager stringForKey:User_ID];
	_txtPassword.placeholder = [LanguageManager stringForKey:Password];
	_lblRememberMe.text=[LanguageManager stringForKey:Remember_Me];
	_lblNoAccount.text = [LanguageManager stringForKey:Dont_have_Account_Sign_Up];
	[_lblNoAccount boldSubstring:[LanguageManager stringForKey:Sign_Up]];
}

- (void)automaticDetectTouchID{
	[DeviceUtils isAvailableToucOrFaceID:^(BOOL isAvailable) {
		if (isAvailable) {
			if ([SettingManager shareInstance].isActivateTouchID) {
				[self.btnTouchID shouldEnable:YES];
			}else{
				[self.btnTouchID shouldEnable:NO];
			}
		}else{
			/*
			//If Biometry is lockout so don't hide the TouchID/FaceID button
			if ([self checkBiometryIsLocked]) {
				[self.btnTouchID shouldEnable:YES];
			}
			else {
				[self.btnTouchID shouldEnable:NO];
			}
			*/
			[self.btnTouchID shouldEnable:NO];
		}
	}];
}

#pragma mark - CONFIGURATIONS
//set dropdown for Language choosen
- (void)setupdropdown {
	self.topRankDropdown = [[TCDropDown alloc] init];
	_topRankDropdown.dataSources = [[LanguageManager defaultManager].dataSource supportedLanguagesName];;
	_topRankDropdown.sourceView = _v_DropDown;
	_topRankDropdown.width = 120;
	_topRankDropdown.isSearchable = NO;
	_topRankDropdown.offset = 10;
	_topRankDropdown.textColor = mainTextColor;
	_v_DropDown.selectedViewColor = [UIColor whiteColor];
	[_topRankDropdown didSelectItem:^(NSString *item, NSInteger index) {
		NSArray *languageArray = [[LanguageManager defaultManager] supportedLanguages];
		NSString *languageSelected = [languageArray objectAtIndex:index];
		[self selectLanguage:languageSelected];
	}];
}

#pragma mark - Utils VC
- (void)selectCachedBroker{
	DetailBrokerModel *detailModel = [BrokerManager shareInstance].detailBroker;
	[self setUIFromSelectedBroker:detailModel];
}
- (void)keepOldVersionDataWhenLoginSuccess{
	[UserPrefConstants singleton].bhCode = [[UserPrefConstants singleton].userInfoDict objectForKey:@"[BrokerCode]"];
	[BrokerManager shareInstance].detailBroker.BHCode = [[UserPrefConstants singleton].userInfoDict objectForKey:@"[BrokerCode]"];
	[[BrokerManager shareInstance] save];
	if ([UserPrefConstants singleton].deviceToken != nil && [UserPrefConstants singleton].ipAddress != nil && [UserPrefConstants singleton].deviceUDID != nil ) {
		NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;
		if ([UserPrefConstants singleton].userInfoDict!=nil || [[UserPrefConstants singleton].userInfoDict objectForKey:@"[BrokerCode]"]!=nil) {
			[[StockAlertAPI instance] storePushNotificationInfo:self.txtUserID.text andUDID:[UserPrefConstants singleton].deviceUDID andExhange:[UserPrefConstants singleton].userCurrentExchange andSponsorCode:[UserPrefConstants singleton].SponsorID andBHCode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[BrokerCode]"] andAppID:@"MD" andBundleId:bundleIdentifier andClientCode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"] andDeviceToken:[UserPrefConstants singleton].deviceToken andIPAddress:[UserPrefConstants singleton].ipAddress andActivationCode:@""];
		}
	}
	
	//+++Disable interactive chart
	//[UserPrefConstants singleton].interactiveChartEnabled = YES;
	// NSLog(@"user Info Dict %@",[UserPrefConstants singleton].userInfoDict);
	
	
	// VARIABLES TO BE RESET AFTER NEW LOGIN
	//	[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"Rememberme"];
	//	[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"TradingPin"];
	//	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	//	[UserPrefConstants singleton].isCollectionView = [def boolForKey:@"QuoteScreenCardView"];
	//	[UserPrefConstants singleton].showDisclaimer = YES;
	//
	//	[UserPrefConstants singleton].sectorToFilter = nil;
	//	[UserPrefConstants singleton].selectedMarket = @"10"; // all
	//	[UserPrefConstants singleton].enableWarrant = NO;
}
- (void)setTitleButtonLanguage{
	if ([UserSession shareInstance].languageType == LanguageType_EN) {
		[_btnLanguage setTitle:[LanguageManager stringForKey:@"English"] forState:UIControlStateNormal];
	}
	else if ([UserSession shareInstance].languageType == LanguageType_CN)
	{
		[_btnLanguage setTitle:[LanguageManager stringForKey:@"Chinese"] forState:UIControlStateNormal];
	}
	else if ([UserSession shareInstance].languageType == LanguageType_VN)
	{
		[_btnLanguage setTitle:[LanguageManager stringForKey:Vietnamese] forState:UIControlStateNormal];
	}
}
- (void)setTextForTouchIDLabel:(NSString *)text{
	_lblTouchID.text = text;
	[_lblTouchID boldSubstring:[LanguageManager stringForKey:Sign_in]];
}
- (void)makeDisclaimerHidden:(BOOL)hidden{
	/*
	 [UIView transitionWithView:_viewDisclaimer duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
	 [_viewDisclaimer setHidden:hidden];
	 } completion:^(BOOL finished) {
	 _scrollView.hidden = !hidden;
	 }];
	 */
}
- (void)setView:(UIView*)view hidden:(BOOL)hidden {
	[UIView transitionWithView:view duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
		[view setHidden:hidden];
	} completion:nil];
}

//Selected Broker
- (void)setUIFromSelectedBroker:(DetailBrokerModel *)detailBroker{
	BrokerModel *brokerModel = [BrokerManager shareInstance].broker;
	NSUInteger brokerTag = brokerModel.tag;
	UIColor *mainTintColor = TC_COLOR_FROM_HEX([BrokerManager shareInstance].detailBroker.color.mainTint);
	UIColor *placeHolderColorLogin;
	switch (brokerTag) {
		case BrokerTag_N2N:
			_imgBroker1.image = [UIImage imageNamed:kDefaultBrokerLogo];
			_imgBroker2.image = [UIImage imageNamed:kDefaultBrokerLogoName];
			placeHolderColorLogin = TC_COLOR_FROM_HEX(kDefaultPlaceholderColor);
			[_btnRememberMe setImage:[UIImage imageNamed: @"UnCheckBlue.png"] forState:UIControlStateNormal];
			[_btnRememberMe setImage:[UIImage imageNamed: @"CheckBlue.png"] forState:UIControlStateSelected];
			_lblCompanyN2N.hidden = NO;
			break;
			
		case BrokerTag_MNA:
			_imgBroker1.image = [UIImage imageNamed:kMNABrokerLogo];
			_imgBroker2.image = [UIImage imageNamed:kMNABrokerLogoName];
			placeHolderColorLogin = TC_COLOR_FROM_HEX(kMNAPlaceholderColor);
			[_btnRememberMe setImage:[UIImage imageNamed: @"UnCheckRed.png"] forState:UIControlStateNormal];
			[_btnRememberMe setImage:[UIImage imageNamed: @"CheckRed.png"] forState:UIControlStateSelected];
			_lblCompanyN2N.hidden = NO;
			break;
			
		case BrokerTag_SSI:
			_imgBroker1.image = [UIImage imageNamed:kSSIBrokerLogo];
			_imgBroker2.image = [UIImage imageNamed:kSSIBrokerLogoName];
			placeHolderColorLogin = TC_COLOR_FROM_HEX(kSSIPlaceholderColor);
			[_btnRememberMe setImage:[UIImage imageNamed: @"UnCheckRed.png"] forState:UIControlStateNormal];
			[_btnRememberMe setImage:[UIImage imageNamed: @"CheckRed.png"] forState:UIControlStateSelected];
			_lblCompanyN2N.hidden = NO;
			break;
		case BrokerTag_AffinHwang:
			_imgBroker1.image = [UIImage imageNamed:kAFFINHWANGBrokerLogo];
			_imgBroker2.image = [UIImage imageNamed:kAFFINHWANGBrokerLogoName];
			placeHolderColorLogin = TC_COLOR_FROM_HEX(kAFFINHWANGPlaceholderColor);
			[_btnRememberMe setImage:[UIImage imageNamed: @"UnCheckBlue.png"] forState:UIControlStateNormal];
			[_btnRememberMe setImage:[UIImage imageNamed: @"CheckBlue.png"] forState:UIControlStateSelected];
			_lblCompanyN2N.hidden = NO;
			break;
		case BrokerTag_Abacus:
			_imgBroker1.image = [UIImage imageNamed:kABACUSBrokerLogo];
			_imgBroker2.image = [UIImage imageNamed:kABACUSBrokerLogoName];
			placeHolderColorLogin = TC_COLOR_FROM_HEX(kABACUSPlaceholderColor);
			[_btnRememberMe setImage:[UIImage imageNamed: @"UnCheckGreen.png"] forState:UIControlStateNormal];
			[_btnRememberMe setImage:[UIImage imageNamed: @"CheckGreen.png"] forState:UIControlStateSelected];
			_lblCompanyN2N.hidden = NO;
			break;
		case BrokerTag_CIMB:
			_imgBroker1.image = [UIImage imageNamed:kCIMBBANKBrokerLogo];
			_imgBroker2.image = [UIImage imageNamed:kCIMBBANKBrokerLogoName];
			placeHolderColorLogin = TC_COLOR_FROM_HEX(kCIMBBANKPlaceholderColor);
			[_btnRememberMe setImage:[UIImage imageNamed: @"UnCheckPurple.png"] forState:UIControlStateNormal];
			[_btnRememberMe setImage:[UIImage imageNamed: @"CheckPurple.png"] forState:UIControlStateSelected];
			_lblCompanyN2N.hidden = NO;
			break;
			
		default:
			_imgBroker1.image = [UIImage imageNamed:kDefaultBrokerLogo];
			_imgBroker2.image = [UIImage imageNamed:kDefaultBrokerLogoName];
			placeHolderColorLogin = TC_COLOR_FROM_HEX(kDefaultPlaceholderColor);
			[_btnRememberMe setImage:[UIImage imageNamed: @"UnCheckBlue.png"] forState:UIControlStateNormal];
			[_btnRememberMe setImage:[UIImage imageNamed: @"CheckBlue.png"] forState:UIControlStateSelected];
			_lblCompanyN2N.hidden = NO;
			break;
	}
	
	//set color
	_lblSignIn.textColor = mainTintColor;
	_txtUserID.textColor = mainTintColor;
	_txtPassword.textColor = mainTintColor;
	_txtUserID.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[LanguageManager stringForKey:User_ID] attributes: @{NSForegroundColorAttributeName: placeHolderColorLogin}];
	_txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[LanguageManager stringForKey:Password] attributes: @{NSForegroundColorAttributeName: placeHolderColorLogin}];
	_txtUserID.tintColor = mainTintColor;
	_txtPassword.tintColor = mainTintColor;
	_lblRememberMe.textColor = mainTintColor;
	[_btnForgotPassword setTitleColor:mainTintColor forState:UIControlStateNormal];
	_lblNoAccount.textColor = mainTintColor;
	_lblCompanyN2N.textColor = mainTintColor;
	_lblVersion.textColor = mainTintColor;
	[_btnTouchID setTintColor:mainTintColor];
	[_usernameIcon setBackgroundColor:mainTintColor];
	[_passwordIcon setBackgroundColor:mainTintColor];
	[_btnLogin setBackgroundColor:mainTintColor];
	[_btnMore setTintColor:mainTintColor];
	[_btnLanguage setTitleColor:mainTintColor forState:UIControlStateNormal];
	[_imgChooseLanguage toColor:mainTintColor];
	[_imgChooseBroker toColor:mainTintColor];
	//set color for Language's dropdown
	mainTextColor = mainTintColor;
	_lblVersion.text = [BrokerManager shareInstance].detailBroker.Version;
	//TouchID flow each broker
	_btnMore.hidden = [BrokerManager shareInstance].detailBroker.linksOnHomePage.count > 0 ? NO : YES;
	[self automaticDetectTouchID];
}

#pragma mark - GO TO MAIN APP
- (void)shouldFirstSettingUp{
	[[Utils shareInstance] dismissWindowHUD];
	//+++ No need now
	//get watch list
	//[[VertxConnectionManager singleton] vertxGetKLCI];
	//[[ATPAuthenticate singleton] getWatchList];
	
	//ggtt
	//[UserPrefConstants singleton].currentExchangeInfo = [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:[UserPrefConstants singleton].userCurrentExchange];
	//Update UI
	[UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.alpha = 0.6;
	} completion:^(BOOL finished) {
		//SelectHomePageVC *selectionVC = NEW_VC_FROM_NIB([SelectHomePageVC class], [SelectHomePageVC nibName]);
		SetupQuickAccessVC *setupVC = NEW_VC_FROM_STORYBOARD(@"Setup", [SetupQuickAccessVC storyboardID]);
		[self nextTo:setupVC animate:YES];
	}];
}
- (void)showActiveTouchScreen{
	[AppDelegateAccessor showActiveTouchID];
}
- (void)gotoMainApp{
	if (![SettingManager shareInstance].didShowSetupFirst) {
		[self shouldFirstSettingUp];
		[SettingManager shareInstance].didShowSetupFirst = YES;
		return;
	}
	[self startMainApp];
}
//========== Start Main App ==============
- (void)startMainApp{
	[AppDelegateAccessor startMainApp];
	[[NetworkManager shared] startTimerCheckingSigleSignOn];
}


/**
 <#Description#>
 
 @param username using userid
 @param password required
 */
- (void)executeLogin:(NSString *)username password:(NSString *)password{
	[[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@""];
	//[[Utils shareInstance] showWindowHudWithTitle:[LanguageManager stringForKey:@"Sign in..."]];
	[Utils showSVProgressHUD:@""];
	//Connect socket first
	[[VertxConnectionManager singleton] connect];
	[[AuthenticationAPI shared] startLoginFrom:username password:password progress:^(LoginStages progress) {
		[[GCDQueue mainQueue] queueBlock:^{
			NSString *statusLogintSTR = [Utils statusLoginForMobileVersion:progress];
			//[[Utils shareInstance] updateProgressForWindowHUD:statusLogintSTR];
			[Utils showSVProgressHUD:statusLogintSTR];
		}];
	} completion:^(id result, NSError *error) {
		[[GCDQueue mainQueue] queueBlock:^{
			if (!error) {
				[UserSession shareInstance].didLogin = YES;
				//Keep Old Data
				[self keepOldVersionDataWhenLoginSuccess];
				//Connect socket here - Connect in get ExchangeList
				//[[VertxConnectionManager singleton] connect];
				//[[VertxConnectionManager singleton] vertxGetExchangeInfo];
				//Save to old cache
				[UserPrefConstants singleton].userName = username;
				[UserPrefConstants singleton].userPassword = password;
				[UserSession shareInstance].userName = username;
				[UserSession shareInstance].password = password;
				[[UserSession shareInstance] save];
				if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
					[[ATPAuthenticate singleton] getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount
														   forPaymentCUT:NO andCurrency:@""];
				[[ThemeManager shareInstance] restoreSessionIfNeeded];
				//goto mainapp
				[self gotoMainApp];
			}else{
				//Close Vertex when login failed
				[[VertxConnectionManager singleton] close];
				[[Utils shareInstance] dismissWindowHUD];
				//check whether Hint&Answer is expired or not
				if (error.code == RESP_CODE_EXCEPTION_CHANGE_HINTANSWER_REQUIRED) {
					[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedRecoverySuggestion withOKAction:^{
						//7_ For further: show Weblink in TCPlus to change Hint&Answer, Password, Pin
					} cancelAction:^{}];
					return;
				}
				//check whether changePasswordPin is expired or not
				else if (error.code == RESP_CODE_EXCEPTION_CHANGE_PASSWORDPIN_REQUIRED){
					[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedRecoverySuggestion withOKAction:^{
						NSString *requiredType = [error localizedDescription];
						[self changePasswordPinRequired:requiredType];
					} cancelAction:^{}];
					return;
				}
				
				[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:error.localizedDescription withOKAction:^{
					//NSLog(@"%@", [[LogManager shared] getUrlLogFileWithIdentifier:LogManager.loginIdentier]);
					//[self sendEmail:@"Support" recipients:[NSMutableArray arrayWithArray:@[@"n2nafesolutions@gmail.com"]] message:@"Problems" attachmentUrl:[[LogManager shared] getUrlLogFileWithIdentifier:LogManager.loginIdentier]];
					//Do nothing now -- Crash on iOS 12 - Real device
				} cancelAction:^{
					
				}];
			}
			//Dismiss indicator
			//[[Utils shareInstance] dismissWindowHUD];
			[Utils dismissSVProgressHUD];
		}];
	}];
}

//++++ Start login TouchID ====
- (void)startingExcuteLoginUsingTouchID{
	[self startingAuthenticateTouchID:^(BOOL auth, NSError *error) {
		if(auth){
			NSString *username = [[SettingManager shareInstance] getQuickAccessName];
			NSString *password = [[SettingManager shareInstance] getQuickAccessPass];
			[self executeLogin:username password:password];
		}
		else {
			if (error) {
				switch (error.code) {
						case LAErrorUserFallback:
						DLog(@"777 User pressed \"Enter Password\"");
						[self->_txtPassword becomeFirstResponder];
						break;
						
					default:
						NSLog(@"777 Touch ID is not configured");
						break;
				}
			}
		}
	}];
}
#pragma mark - Detect Keyboard
- (void)registerNotificationKeyboard{
	// Listen for keyboard appearances and disappearances
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillShow:)
												 name:UIKeyboardWillShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification
											   object:nil];
}
- (void)unRegiserNotificationKeyboard{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Notification
#pragma mark - Notification Keyboard
- (void)keyboardWillShow:(NSNotification *)noti{
	[self calculateInsetForKeyboardShow:YES withNotification:noti];
}

- (void)keyboardWillHide:(NSNotification *)noti{
	[self calculateInsetForKeyboardShow:NO withNotification:noti];
}
/*
 ============ Calculate size for Keyboard Show ================
 */

- (void)calculateInsetForKeyboardShow:(BOOL)show withNotification:(NSNotification *)noti{
	NSNumber *duration = [noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
	NSValue *value = noti.userInfo[UIKeyboardFrameEndUserInfoKey];
	CGRect keyboardFrame = value.CGRectValue;
	float adjustmentHeight = (keyboardFrame.size.height + 44) * (show? 1: -1);
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, adjustmentHeight, 0.0);
	if (SCREEN_HEIGHT_PORTRAIT < 568) {
		if (show) {
			_scrollView.contentInset = contentInsets;
		}else{
			_scrollView.contentInset = _insetScrollView;
			[_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
		}
		return;
	}
	[UIView animateWithDuration:[duration floatValue] animations:^{
		self->_scrollView.contentInset = contentInsets;
		self->_scrollView.scrollIndicatorInsets = contentInsets;
	}];
}
//Validate login
- (BOOL)loginValidated{
	if (self.txtUserID.text.length > 0 && self.txtPassword.text.length > 0) {
		return YES;
	}
	return NO;
}
#pragma mark - Main Actions
//More Info
- (void)loadDataForDropDown{
    //Prepare datasource
    moreInfos = [UserPrefConstants singleton].linksOnHomePage;
    //Convert allKeys to multi languages - look at configs file to update multi languages
    for(NSString *info in moreInfos){
        NSArray *separatedContents = [info componentsSeparatedByString:@"|"];
        NSString *key = [separatedContents[0] trimmedLeadingAndTrailingWhiteSpace];
        NSString *value = [separatedContents[1] trimmedLeadingAndTrailingWhiteSpace];
        if([key isEqualToString:@"Term Of Use"]){
            [UserPrefConstants singleton].termOfUseLink = value;
        }
        [dataSource addObject:[LanguageManager stringForKey:key]];
        [values addObject:value];
    }
}
- (IBAction)onMoreInfoAction:(UIButton *)sender {
	
	if (!_moreInfoDropdown) {
		_moreInfoDropdown = [[TCDropDown alloc] init];
	}
	_moreInfoDropdown.dataSources = [dataSource copy];
	UIView *sourceView = [[UIView alloc] init];
	sourceView.frame = CGRectMake(sender.frame.origin.x - 5, sender.frame.origin.y, sender.frame.size.width, 0);
	_moreInfoDropdown.sourceView = sender;
	_moreInfoDropdown.width = 120;
	_moreInfoDropdown.isSearchable = NO;
	_moreInfoDropdown.offset = -sender.frame.size.height;
	_moreInfoDropdown.textColor = mainTextColor;
	_moreInfoDropdown.numberVisibleRow = dataSource.count;
	_moreInfoDropdown.contentAlign = NSTextAlignmentCenter;
	_moreInfoDropdown.font = AppFont_MainFontRegularWithSize(14);
	_moreInfoDropdown.textColor = [UIColor blackColor];
	_moreInfoDropdown.showSeparatedRow = YES;
	[_moreInfoDropdown didSelectItem:^(NSString *item, NSInteger index) {
		NSString *valuePageLink = values[index];
		[ApplicationUtils open:valuePageLink];
	}];
	[_moreInfoDropdown show];
}

//Menu
- (IBAction)onMenuAction:(id)sender {
	[AppDelegateAccessor toogleLeftMenu:nil];
}

- (IBAction)onNoAccountAction:(id)sender {
	
}


//================== Handle Disclaimer =========
- (IBAction)onAcceptDisclaimerAction:(id)sender {
	[self makeDisclaimerHidden:YES];
	//Save setting user
	[UserSession shareInstance].isAcceptDislaimerNote = YES;
	[[UserSession shareInstance] save];
}

- (IBAction)onDeclineDisclaimer:(id)sender {
	//Exit app
	exit(0);
}

//=================== END ==============
- (IBAction)onChooseBrokerAction:(id)sender {
	BrokerListVC *_brokerVC = NEW_VC_FROM_STORYBOARD(@"MBroker", [BrokerListVC storyboardID]);
	_brokerVC.delegate = self;
	BaseNavigationController *brokerNav = [[BaseNavigationController alloc] initWithRootViewController:_brokerVC];
	[self showPopupWithContent:brokerNav inSize:CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 220) showInCenter:YES topInsect:0 completion:^(MZFormSheetController *formSheetController) {
		//Do something if need
	}];
	
	/*
	 BaseNavigationController *brokerNav = [[BaseNavigationController alloc] initWithRootViewController:_brokerVC];
	 [self showPopupWithContent:brokerNav withCompletion:^(MZFormSheetController *formSheetController) {
	 
	 }];
	 */
	/*
	[_brokerVC setModalPresentationStyle:UIModalPresentationOverCurrentContext];
	_brokerVC.definesPresentationContext = YES;
	[self presentViewController:_brokerVC animated:YES completion:nil];
	*/
}

- (IBAction)onTouchIDAction:(id)sender {
	DLog(@"+++ on touchID Action");
	[_txtUserID resignFirstResponder];
	[_txtPassword resignFirstResponder];
	//[self endEditting];
	[DeviceUtils isAvailableToucOrFaceID:^(BOOL isAvailable) {
		if (!isAvailable) {
			DLog(@"777 check if Biometry is lock out so will show Enterpasscode screen to unlock it");
			[self showEnterpasscodeToUnclockTouchID];
			return;
		}else{
			[UserPrefConstants singleton].isAuthByTouchId = YES;;
			//Login with TouchID
			[self startingExcuteLoginUsingTouchID];
		}
	}];
}

- (IBAction)onLoginAction:(id)sender {
	//Dismiss keyboard
	[self endEditting];
	if (![self loginValidated]) {
		[self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:User_ID_and_Password_cannot_be_empty__]];
		return;
	}
	
	//Set Exchange
	/*
	NSString *exchgStr = [NSString stringWithFormat:@"Exchange%@",[UserPrefConstants singleton].brokerCode];
	NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
	
	if ([[def objectForKey:exchgStr] length]<=0) {
		[UserPrefConstants singleton].userCurrentExchange = [UserPrefConstants singleton].PrimaryExchg;
		[def setObject:[UserPrefConstants singleton].userCurrentExchange forKey:exchgStr];
		[def synchronize];
	}
	else
	[UserPrefConstants singleton].userCurrentExchange = [[NSUserDefaults standardUserDefaults]objectForKey:exchgStr];
	*/
	if ([SettingManager shareInstance].defaultExchangeCode) {
		[UserPrefConstants singleton].userCurrentExchange = [SettingManager shareInstance].defaultExchangeCode;
	}else{
		[UserPrefConstants singleton].userCurrentExchange = [UserPrefConstants singleton].PrimaryExchg;
	}
	//Excute login here
	[self executeLogin:_txtUserID.text password:_txtPassword.text];
}

- (IBAction)onRememberMeAction:(UIButton *)sender {
	sender.selected = !sender.selected;
	[UserSession shareInstance].isRememberMe = sender.selected;
}


- (IBAction)onChangeLanguageAction:(UIButton *)sender {
	//    if(popoverViewController == nil) {
	//
	//        popoverViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"popOverLanguage"];
	//
	//    }
	//    popoverViewController.preferredContentSize = CGSizeMake(300, 120);
	//    popoverViewController.modalPresentationStyle = UIModalPresentationPopover;
	//    UIPopoverPresentationController *popoverController = popoverViewController.popoverPresentationController;
	//    popoverController.permittedArrowDirections = UIPopoverArrowDirectionLeft;
	//    popoverController.delegate = self;
	//    popoverController.sourceView = self.view;
	//    popoverController.sourceRect = [sender frame];
	//    [self presentViewController:popoverViewController animated:YES completion:nil];
	//_topRankDropdown.defaltSelectedItem ;
	self.topRankDropdown.sourceView = sender;
	[self.topRankDropdown show];
}
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
	return UIModalPresentationNone;
}
- (void)selectLanguage: (NSString *)language{
	if ([language isEqualToString:@"en"]) {
		[UserSession shareInstance].languageType = LanguageType_EN;
	}
	if ([language isEqualToString:@"cn"]) {
		[UserSession shareInstance].languageType = LanguageType_CN;
	}
	if ([language isEqualToString:@"vn"]) {
		[UserSession shareInstance].languageType = LanguageType_VN;
	}
	//Update Language
	[[LanguageManager defaultManager] setLanguage:[Utils languageByType:[UserSession shareInstance].languageType]];
	[self setupLanguage];
	//Alway save to session last language choosen
	[[UserSession shareInstance] save];
}
- (IBAction)onForgotPasswordAction:(id)sender {
	//    [self showCustomAlertWithMessage:@"Coming soon"];
	ForgotPasswordVC *_forgotVC = NEW_VC_FROM_STORYBOARD(kSettingStoryboardName, [ForgotPasswordVC storyboardID]);
	[self presentWithNav:_forgotVC animate:YES];
}

//NEWS
- (IBAction)onInformationAction:(id)sender {
	[self showCustomAlertWithMessage:@"Coming soon"];
}

- (IBAction)onHelpAction:(id)sender {
	[self buildIntro];
	
}

- (IBAction)onSignUpAction:(id)sender {
	if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
		//Link to website SSI
		if ([UserSession shareInstance].languageType == LanguageType_EN) {
			[ApplicationUtils open:@"https://www.ssi.com.vn/en/individual-customer/open-account"];
			//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.ssi.com.vn/en/individual-customer/open-account"]];
		}
		if ([UserSession shareInstance].languageType == LanguageType_VN) {
			[ApplicationUtils open:@"https://www.ssi.com.vn/khach-hang-ca-nhan/mo-tai-khoan"];
			//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.ssi.com.vn/khach-hang-ca-nhan/mo-tai-khoan"]];
		}
		return;
	}
	[self showCustomAlertWithMessage:@"Coming soon"];
}

- (void)changePasswordPinRequired: (NSString *)requiredType {
	if (![requiredType isEqualToString:@"Unknow"]) {
		UINavigationController *navCon;
		ChangePasswdViewController *changePasswordVC = [[ChangePasswdViewController alloc] initWithNibName:@"ChangePasswordView" bundle:nil];
		navCon = [[UINavigationController alloc] initWithRootViewController:changePasswordVC];
		
		UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:[LanguageManager stringForKey:@"Back"] style:UIBarButtonItemStylePlain target:self action:@selector(dismissChangePwdOrPin:)];
		changePasswordVC.navigationItem.leftBarButtonItem = leftButton;
		if ([requiredType isEqualToString:@"ChangePasswordRequired"] || [requiredType isEqualToString:@"ChangePasswordAndPinRequired"]) {
			[changePasswordVC setAction_type:CHANGE_PASSWORD_CODE];
			callChangePwdAgain = YES;
		}
		else {
			[changePasswordVC setAction_type:CHANGE_PIN_CODE];
			callChangePwdAgain = NO;
		}
		[changePasswordVC setFromLoginVC:YES];
		[changePasswordVC setDelegate:self];
		[self presentViewController:navCon animated:NO completion:nil];
	}
}

#pragma mark - UITextfieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	if (textField == _txtUserID) {
		[_txtPassword becomeFirstResponder];
	}else{
		[textField resignFirstResponder];
		//Call api login here
		[self onLoginAction:nil];
	}
	return YES;
}

#pragma mark - BrokerListVCDelegate
- (void)didSelectBroker:(DetailBrokerModel *)broker{
	[self setUIFromSelectedBroker:broker];
}

#pragma mark - Main Notification
- (void)didSelectBrokerNotification:(NSNotification *)noti{
	[self setUIFromSelectedBroker:[BrokerManager shareInstance].detailBroker];
}
-(void)didChangeLanguageNotification:(NSNotification *)noti{
	[self setupLanguage];
}

//-(void)getPLISTData {
//    // Download plist for iPad
//
//    NSURL *URL = [NSURL URLWithString:lmsServerFromBundleID];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
//    request.timeoutInterval = kNSURLSessionTimeout;
//
//    NSLog(@"lmsServerFromBundleID %@",lmsServerFromBundleID);
//
//
//    [self showProcessWithMessage:[LanguageManager stringForKey:@"Retrieving Broker Data..."] showRetry:NO showClose:NO activity:YES];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *downloadTask =
//    // [session downloadTaskWithRequest:request
//    //               completionHandler: ^(NSURL *location, NSURLResponse *response, NSError *error) {
//    [session dataTaskWithRequest:request
//               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//
//                   if (error) {
//                       NSLog(@"error %@", error);
//
//                       dispatch_async(dispatch_get_main_queue(), ^ {
//
//
//                           [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."]
//                                              showRetry:YES showClose:NO activity:NO];
//
//                       });
//                       return;
//                   }
//
//                   // read plist saved at "location" and check version with current saved one.
//                   // NSDictionary *tmpLMSDictionary = [NSDictionary dictionaryWithContentsOfFile:[location path]];
//
//                   // NSString *lmsVersion = [tmpLMSDictionary objectForKey:@"Version"];
//                   // NSString *lmsMinVersion = [tmpLMSDictionary objectForKey:@"MinVersion"];
//
//                   //   if ([lmsVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
//                   // currentVersion is lower than the lmsVersion
//                   // update ipadLMSDictionary to new one
//
//                   NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//                   NSData* plistData = [content dataUsingEncoding:NSUTF8StringEncoding];
//
//                   NSPropertyListFormat fmt;
//                   ipadLMSDictionary = [NSPropertyListSerialization propertyListWithData:plistData
//                                                                                 options: NSPropertyListImmutable
//                                                                                  format:&fmt
//                                                                                   error:&error];
//
//
//                   NSLog(@"ipadLMSDictionary %@",ipadLMSDictionary);
//                   // ipadLMSDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:[location path]];
//                   allowedBrokerListArray = [NSMutableArray arrayWithArray:[ipadLMSDictionary objectForKey:@"BrokerList"]];
//                   currentBrokerListArray = [NSMutableArray arrayWithArray:[ipadLMSDictionary objectForKey:@"BrokerList"]];
//
//                   NSLog(@" currentBrokerListArray %@", currentBrokerListArray);
//
//                   // NO BROKER LIST = CUSTOM BROKER APP
//                   if ([allowedBrokerListArray count]<=0) {
//
//                       dispatch_async(dispatch_get_main_queue(), ^ {
////                           [btnBrokerList setHidden:YES];
////                           [_btnBrokerListName setHidden:YES];
////                           [_btnBrokerListActive setHidden:YES];
////                           [_btnBrokerListActiveName setHidden:YES];
//
//                           [UserPrefConstants singleton].brokerName = [ipadLMSDictionary objectForKey:@"BrokerName"];
////                           _lblBrokerName.text = [UserPrefConstants singleton].brokerName;
////                           self.activeBrokerNameText.text = [UserPrefConstants singleton].brokerName;
//                           [self setBrokerSettings:ipadLMSDictionary];
//
//                           [self showProcessWithMessage:[LanguageManager stringForKey:@"Connecting to Feed Server..."]
//                                              showRetry:NO showClose:NO activity:YES];
//                           [[VertxConnectionManager singleton] connectingUponLogin];
//                           [self performSelector:@selector(timeOutVertxConnect) withObject:nil afterDelay:kNSURLSessionTimeout];
//                       });
//
//
//
//                   }else {
//
//                       dispatch_async(dispatch_get_main_queue(), ^ {
//                           //                        NSString *sortKey = [ipadLMSDictionary objectForKey:@"SortBrokerListBy"];
//                           //                        [self populateBrokerIconsWithArray:allowedBrokerListArray andSortKey:sortKey];
////                           [btnBrokerList setEnabled:YES];
////                           [_btnBrokerListName setEnabled:YES];
////                           [_btnBrokerListActive setEnabled:YES];
////                           [_btnBrokerListActiveName setEnabled:YES];
//
//                           NSUserDefaults *def  = [NSUserDefaults standardUserDefaults];
//                           // set default broker
//                           NSDictionary *selectedBroker = nil;
//                           // Saving of broker by index cannot work because we have multiple level menu
//
//                           if ([def objectForKey:@"Brokerhouse"]==nil) { // set first broker for first time
//
//                               selectedBroker = (NSDictionary*)[allowedBrokerListArray objectAtIndex:0];
//                               [def setObject:selectedBroker forKey:@"Brokerhouse"];
//                               [def synchronize];
//
//                           } else { // load previous broker selected
//
//                               // if previous brokerhouse is integer, rewrite with NSDictionary
//                               // to cater for previous version TCiPad that uses number
//                               if ([[def objectForKey:@"Brokerhouse"] isKindOfClass:[NSNumber class]]) {
//
//                                   selectedBroker = (NSDictionary*)[allowedBrokerListArray objectAtIndex:0];
//                                   [def setObject:selectedBroker forKey:@"Brokerhouse"];
//                                   [def synchronize];
//                               } else {
//
//                                   selectedBroker = (NSDictionary*)[def objectForKey:@"Brokerhouse"];
//                               }
//                           }
//
//                           //[UserPrefConstants singleton].brokerName=self.lblBrokerName.text;
//                           [self selectABroker:selectedBroker];
//
//                       });
//                   }
//               }];
//
//    [downloadTask resume];
//}
//
//-(void)showProcessWithMessage:(NSString*)message showRetry:(BOOL)retry showClose:(BOOL)close activity:(BOOL)activity {
//
////    if (_processView==nil) {
//
//        NSDictionary *setupDict = @{@"message": message,
//                                    @"retry" : [NSNumber numberWithBool:retry],
//                                    @"close" : [NSNumber numberWithBool:close],
//                                    @"activity" : [NSNumber numberWithBool:activity],
//                                    };
//
////        [self performSegueWithIdentifier:@"segueProcessID" sender:setupDict];
//
////    } else {
////
////        // update
////        self.processView.messageLabel.text = message;
////        [self.processView.retryButton setHidden:!retry];
////        [self.processView.closeButton setHidden:!close];
////        [self.processView.activityView setHidden:!activity];
////        [self.processView updateButton];
////    }
//
//}
//
//-(void)setBrokerSettings:(NSDictionary*)brokerSettings {
//
//    DLog(@"Broker Settings %@",brokerSettings);
//    [UserPrefConstants singleton].brokerCode = [brokerSettings objectForKey:@"BrokerID"];
//    [UserPrefConstants singleton].SponsorID = [brokerSettings objectForKey:@"LMSSponsor"];
//    [UserPrefConstants singleton].Vertexaddress = [brokerSettings objectForKey:@"VertexServer"];
//    [UserPrefConstants singleton].bhCode = [brokerSettings objectForKey:@"BHCode"];
//
//    [UserPrefConstants singleton].sessionTimeoutSeconds = [[brokerSettings objectForKey:@"LogoutCountdownTime"] intValue]*60;
//
//    if ([UserPrefConstants singleton].sessionTimeoutSeconds<=0)
//        [UserPrefConstants singleton].sessionTimeoutSeconds = kTimeOutDuration; // default 30 mins
//
//    [UserPrefConstants singleton].CreditLimitOrdPad = [[brokerSettings objectForKey:@"CreditLimitOrdPad"] boolValue];
//    [UserPrefConstants singleton].CreditLimitOrdBook = [[brokerSettings objectForKey:@"CreditLimitOrdBook"] boolValue];
//    [UserPrefConstants singleton].CreditLimitPortfolio = [[brokerSettings objectForKey:@"CreditLimitPortfolio"] boolValue];
//    [UserPrefConstants singleton].TrdShortSellIndicator = [brokerSettings objectForKey:@"TrdShortSellIndicator"];
//
//    DLog(@"TrdShortSellIndicator %@",[UserPrefConstants singleton].TrdShortSellIndicator);
//
//    [UserPrefConstants singleton].msgPromptForStopNIfTouchOrdType = [[brokerSettings objectForKey:@"msgPromptForStopNIfTouchOrdType"] boolValue];
//
//    [UserPrefConstants singleton].LoginHelpText_EN = [brokerSettings objectForKey:@"LoginHelpText"];
//    [UserPrefConstants singleton].LoginHelpText_CN = [brokerSettings objectForKey:@"LoginHelpTextCN"];
//    // MARK: testing Chinese language (N2N only)
//    //  [UserPrefConstants singleton].Vertexaddress = @"nogf1-uat.asiaebroker.com";
//
//    //[UserPrefConstants singleton].userATPServerIP = [brokerSettings objectForKey:@"ATPServer"];
//
//    [UserPrefConstants singleton].userATPServerIP = [brokerSettings objectForKey:@"ATPServer"];
//
//    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;
//    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
//        [UserPrefConstants singleton].LMSServerAddress = @"http://125.5.209.29/lms/websub/authen?";
//
//    }else{
//        [UserPrefConstants singleton].LMSServerAddress = [brokerSettings objectForKey:@"LMSServer"];
//    }
//
//    [UserPrefConstants singleton].isEnabledESettlement = [[brokerSettings objectForKey:@"ESettlementEnabled"] boolValue];
//    [UserPrefConstants singleton].eSettlementURL = [brokerSettings objectForKey:@"ESettlementURL"];
//    [UserPrefConstants singleton].ATPLoadBalance = [[brokerSettings objectForKey:@"ATPLoadBalance"] boolValue];
//    [UserPrefConstants singleton].HTTPSEnabled = [[brokerSettings objectForKey:@"HTTPSEnabled"] boolValue];
//    [UserPrefConstants singleton].E2EEEncryptionEnabled = [[brokerSettings objectForKey:@"E2EE_Encryption"] boolValue];
//    NSString *brokername = [brokerSettings objectForKey:@"BrokerName"];
//    if ([brokername length]>0) [UserPrefConstants singleton].brokerName = brokername;
//
//    [UserPrefConstants singleton].PFDisclaimerWebView = [[brokerSettings objectForKey:@"PFDisclaimerWebView"] boolValue];
//    [UserPrefConstants singleton].PFDisclaimerWebViewURL = [brokerSettings objectForKey:@"PFDisclaimerWebViewURL"];
//    [UserPrefConstants singleton].filterShowMarket = [brokerSettings objectForKey:@"filterShowMarket"];
//
//    [UserPrefConstants singleton].isArchiveNews = [[brokerSettings objectForKey:@"ArchiveNewsEnabled"] boolValue];
//    [UserPrefConstants singleton].termAndConditions_EN = [brokerSettings objectForKey:@"TermsNCondContentOnLoginPg"];
//    [UserPrefConstants singleton].termAndConditions_CN = [brokerSettings objectForKey:@"TermsNCondContentOnLoginPgCN"];
//    [UserPrefConstants singleton].PrimaryExchg = [brokerSettings objectForKey:@"PrimaryExchg"];
//    [UserPrefConstants singleton].NewsServerAddress = [brokerSettings objectForKey:@"NewsServer"];
//
//    // Fundamentals/Factset/CPIQ things
//    [UserPrefConstants singleton].fundamentalNewsLabel = [brokerSettings objectForKey:@"FundamentalNewsLabel"];
//    [UserPrefConstants singleton].fundamentalNewsServer= [brokerSettings objectForKey:@"FundamentalNewsServer"];
//    [UserPrefConstants singleton].fundamentalSourceCode= [brokerSettings objectForKey:@"FundamentalSourceCode"];
//    [UserPrefConstants singleton].fundDataCompInfoURL= [brokerSettings objectForKey:@"FundDataCompInfoURL"];
//    [UserPrefConstants singleton].fundFinancialInfoURL= [brokerSettings objectForKey:@"FundDataFinancialInfoURL"];
//    [UserPrefConstants singleton].fundamentalDefaultReportCode= [brokerSettings objectForKey:@"FundamentalDefaultReportCode"];
//
//    // chart
//    [UserPrefConstants singleton].chartType = [brokerSettings objectForKey:@"chartType"];
//    [UserPrefConstants singleton].Register2FAPgURL = [brokerSettings objectForKey:@"2FARegisterPgURL"];
//
//    //stock Alert
//    //http://noglu.asiaebroker.com/mpn/srvs/registerDevice
//    [UserPrefConstants singleton].isEnabledStockAlert = [[brokerSettings objectForKey:@"EnabledStockAlert"] boolValue];
//    [UserPrefConstants singleton].isShownStockAlertDisclaimer = [[brokerSettings objectForKey:@"EnabledStockAlertDisclaimer"] boolValue];
//    [UserPrefConstants singleton].stockAlertURL = [brokerSettings objectForKey:@"StockAlertPageURL"];//@"https://sans-uat.asiaebroker.com/gcSANS/web/html/StockAlert.html";
//    [UserPrefConstants singleton].pushNotificationRegisterURL = [brokerSettings objectForKey:@"PushNotificationRegisterURL"];//@"https://sans-uat.asiaebroker.com/gcSANS/srvs/registerDevice";
//    [UserPrefConstants singleton].stockAlertGetPublicURL =  [brokerSettings objectForKey:@"StockAlertAuthKey"];//@"https://sans-uat.asiaebroker.com/gcSANS/srvs/Auth/getRecKey";
//
//    [UserPrefConstants singleton].interactiveChartEnabled= [[brokerSettings objectForKey:@"InteractiveChartEnabled"] boolValue];
//
//    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
//    if ([def objectForKey:@"QtyIsLot"]==nil) {
//        [UserPrefConstants singleton].quantityIsLot =  ![[brokerSettings objectForKey:@"DefaultTradePrefQtyUnit"] boolValue];
//    } else {
//        [UserPrefConstants singleton].quantityIsLot = [def boolForKey:@"QtyIsLot"];
//    }
//
//
//
//    if ([[UserPrefConstants singleton].chartType length]<=0) [UserPrefConstants singleton].chartType = @"Image";
//    [UserPrefConstants singleton].interactiveChartByExchange = [brokerSettings objectForKey:@"InteractiveChartForExch"];
//    [UserPrefConstants singleton].chartTypeToLoad = [UserPrefConstants singleton].chartType;
//
//
//
//    /* NSLog(@"\n\n ------------------------------------------------------ \n");
//     NSLog(@"Vertex: %@",[UserPrefConstants singleton].Vertexaddress);
//     NSLog(@"ATP: %@",[UserPrefConstants singleton].userATPServerIP);
//     NSLog(@"LMS: %@",[UserPrefConstants singleton].LMSServerAddress);
//     NSLog(@"News Server: %@", [UserPrefConstants singleton].NewsServerAddress);
//     NSLog(@"ChartToLoadDefault: %@",[UserPrefConstants singleton].chartTypeToLoad);
//     NSLog(@"quantityIsLot: %d",[UserPrefConstants singleton].quantityIsLot);
//     NSLog(@"\n\n\n");*/
//
//    //default starting screen
//    if ([def objectForKey:@"ScreenViewMode"]==nil) {
//        [UserPrefConstants singleton].defaultStartingScreen = [brokerSettings objectForKey:@"DefaultStartingScreen"];
//        if ([[UserPrefConstants singleton].defaultStartingScreen length]<=0)
//            [UserPrefConstants singleton].defaultStartingScreen = @"Home";
//
//        if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Home"])
//            [UserPrefConstants singleton].defaultView = 1;
//        else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Quote"])
//            [UserPrefConstants singleton].defaultView = 2;
//        else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"WatchList"])
//            [UserPrefConstants singleton].defaultView = 3;
//
//
//    } else {
//        int defView =  [[def objectForKey:@"ScreenViewMode"] intValue];
//        [UserPrefConstants singleton].defaultView = defView;
//        if (defView==1)
//            [UserPrefConstants singleton].defaultStartingScreen = @"Home";
//        else if (defView==2)
//            [UserPrefConstants singleton].defaultStartingScreen = @"Quote";
//        else if (defView==3)
//            [UserPrefConstants singleton].defaultStartingScreen = @"WatchList";
//    }
//
//    [UserPrefConstants singleton].interactiveChartURL = [brokerSettings objectForKey:@"InteractiveChartURL"];
//    [UserPrefConstants singleton].intradayChartURL = [brokerSettings objectForKey:@"IntradayChartServer"];
//    [UserPrefConstants singleton].historicalChartURL = [brokerSettings objectForKey:@"HistoricalChartServer"];
//    //Settings
//    [UserPrefConstants singleton].companyName = [brokerSettings objectForKey:@"CompanyName"];
//    [UserPrefConstants singleton].companyTel = [brokerSettings objectForKey:@"CompanyTel"];
//    [UserPrefConstants singleton].companyAddress = [brokerSettings objectForKey:@"CompanyAddress"];
//    [UserPrefConstants singleton].companyFax = [brokerSettings objectForKey:@"CompanyFax"];
//    [UserPrefConstants singleton].companyLogo = [brokerSettings objectForKey:@"CompanyLogo"];
//    [UserPrefConstants singleton].shareText = [brokerSettings objectForKey:@"ShareText"];
//    [UserPrefConstants singleton].facebookLink = [brokerSettings objectForKey:@"FacebookLink"];
//    [UserPrefConstants singleton].twitterText = [brokerSettings objectForKey:@"TwitterLink"];
//    [UserPrefConstants singleton].linkInText = [brokerSettings objectForKey:@"LinkedInLink"];
//    [UserPrefConstants singleton].websiteLink = [brokerSettings objectForKey:@"WebsiteLink"];
//    [UserPrefConstants singleton].emailLink = [brokerSettings objectForKey:@"EmailLink"];
//    [UserPrefConstants singleton].aboutUs = [brokerSettings objectForKey:@"AboutUs"];
//    [UserPrefConstants singleton].appName = [brokerSettings objectForKey:@"AppName"];
//
//    [UserPrefConstants singleton].tempQcServer = @"ndgfvu.mytrade.com.ph";//[brokerSettings objectForKey:@"QCServer"];
//    [UserPrefConstants singleton].isShowTradingLimit = [[brokerSettings objectForKey:@"TradingLimitEnabled"] boolValue];
//
//    [UserPrefConstants singleton].tempUserParamQC = @"[vUpJYKw4QvGRMBmhATUxRwv4JrU9aDnwNEuangVyy6OuHxi2YiY=]";
//
//    [UserPrefConstants singleton].AnnouncementURL = [brokerSettings objectForKey:@"AnnouncementURL"];
//
//    [UserPrefConstants singleton].orderHistLimit = [brokerSettings objectForKey:@"OrderHistoryLimit"];
//    // default to 30 days
//    if ([[UserPrefConstants singleton].orderHistLimit length]<=0) [UserPrefConstants singleton].orderHistLimit = @"30";
//
//    // Indices button
//    [UserPrefConstants singleton].indicesEnableByExchange = [brokerSettings objectForKey:@"ExchangesEnableIndices"];
//
//    // OrderPad
//    [UserPrefConstants singleton].defaultCurrency = [brokerSettings objectForKey:@"DefaultCurrency"];
//    [UserPrefConstants singleton].isEnableRDS = [[brokerSettings objectForKey:@"RDS"] boolValue];
//
//    // Override Exchange name
//    [UserPrefConstants singleton].overrideExchangesName = [NSDictionary dictionaryWithDictionary:[brokerSettings objectForKey:@"OverrideExchangeName"]];
//
//
//    // Market Streamer
//
//    [UserPrefConstants singleton].isShowMarketStreamer = [[brokerSettings objectForKey:@"MarketStreamer"] boolValue];
//
//    // update UI
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//        NSString *helpInfoText = @"";
//        if ([[LanguageManager defaultManager].language isEqualToString:@"en"]) {
//            helpInfoText = [UserPrefConstants singleton].LoginHelpText_EN;
//        } else if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
//            helpInfoText = [UserPrefConstants singleton].LoginHelpText_CN;
//        }
//
////        _helpInfoTextView.text = helpInfoText;
//
//
//        if ([helpInfoText length]<=0) {
////            _loginView.frame = CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y,
////                                          _loginView.frame.size.width, 260);
////            _loginView.center = CGPointMake(_loginContainerView.frame.size.width/2.0,
////                                            _loginContainerView.frame.size.height/2.0);
////        } else {
////            _loginView.frame = CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y,
////                                          _loginView.frame.size.width, 320);
////            _loginView.center = CGPointMake(_loginContainerView.frame.size.width/2.0,
////                                            _loginContainerView.frame.size.height/2.0);
//        }
//
////        self.termAndConditionTextView.text = [UserPrefConstants singleton].termAndConditions_EN;
////        self.txtUserName.delegate=self;
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSString *rememberMeStr = [NSString stringWithFormat:@"isRememberMe%@",[UserPrefConstants singleton].brokerCode];
//        NSString *usernameStr = [NSString stringWithFormat:@"userName%@",[UserPrefConstants singleton].brokerCode];
//
//
//        BOOL remembered = [defaults boolForKey:rememberMeStr];
//        if (remembered) {
////            self.txtUserName.text = [defaults objectForKey:usernameStr];
////            self.txtUserName.enabled=NO;
////            self.txtUserName.backgroundColor = [UIColor grayColor];
//            //NSLog(@"Remember %@", [defaults objectForKey:usernameStr]);
//
//        } else {
////            self.txtUserName.text = @"";
////            self.txtUserName.enabled=YES;
////            self.txtUserName.backgroundColor = [UIColor whiteColor];
//        }
//
//        self.btnRememberMe.selected = [defaults boolForKey:rememberMeStr];
//        self.txtPassword.text = @"";
//
//        if ([UserDefaultData getTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode]) {
//            // touchID activated
////            [_deactivateBtn setHidden:NO];
//        } else {
////            [_deactivateBtn setHidden:YES];
//        }
//
//    });
//}
//
//-(void)selectABroker:(NSDictionary*)selectedBroker {
//
////    self.lblBrokerName.text = [selectedBroker objectForKey:@"AppName"];
////    self.activeBrokerNameText.text = [selectedBroker objectForKey:@"AppName"];
//    [UserPrefConstants singleton].brokerName = [selectedBroker objectForKey:@"AppName"];
//
//    NSString *plistLink = [selectedBroker objectForKey:@"PlistLink"];
//    NSLog(@"selectABroker %@",plistLink);
//
//    NSURL *URL = [NSURL URLWithString:plistLink];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
//    request.timeoutInterval = kNSURLSessionTimeout;
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDownloadTask *downloadTask =
//
//    // [session downloadTaskWithRequest:request
//    //              completionHandler: ^(NSURL *location, NSURLResponse *response, NSError *error) {
//
//    [session dataTaskWithRequest:request
//               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                   if (error) {
//                       NSLog(@"response %@", response);
//                       dispatch_async(dispatch_get_main_queue(), ^ {
//
//                           [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
//                                              showRetry:YES showClose:NO activity:NO];
//                       });
//                       return;
//                   }
//
//                   //    NSLog(@"response %@", location.path);
//                   // NSDictionary *brokerSettings = [NSDictionary dictionaryWithContentsOfFile:[location path]];
//
//                   NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//                   NSData* plistData = [content dataUsingEncoding:NSUTF8StringEncoding];
//
//                   NSPropertyListFormat fmt;
//                   NSDictionary *brokerSettings = [NSPropertyListSerialization propertyListWithData:plistData
//                                                                                            options: NSPropertyListImmutable
//                                                                                             format:&fmt
//                                                                                              error:&error];
//
//
//                   NSLog(@"brokerSettings %@",brokerSettings);
//                   if ([brokerSettings count]>0) {
//
//                       dispatch_async(dispatch_get_main_queue(), ^ {
//                           // select and return to main
//                           [self setBrokerSettings:brokerSettings];
//
//                           [self showProcessWithMessage:[LanguageManager stringForKey:@"Connecting to Feed Server..."]
//                                              showRetry:NO showClose:NO activity:YES];
//                           [[VertxConnectionManager singleton] connectingUponLogin];
//                           [self performSelector:@selector(timeOutVertxConnect) withObject:nil afterDelay:kNSURLSessionTimeout];
//
////                           if (!displayingFront) {
////
////                               [UIView transitionWithView:self.loginContainerView
////                                                 duration:0.5
////                                                  options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionBeginFromCurrentState
////                                               animations: ^{
////
////                                                   if (activeTouchID) {
////                                                       self.activeView.hidden = NO;
////                                                       self.loginView.hidden = YES;
////                                                       self.brokerSelectionView.hidden = YES;
////                                                   }else{
////                                                       self.activeView.hidden = YES;
////                                                       self.loginView.hidden = NO;
////                                                       self.brokerSelectionView.hidden = YES;
////                                                   }
////
////
////                                               }
////
////                                               completion:^(BOOL finished) {
////                                                   displayingFront = YES;
////                                                   subMenuLevel = 0;
////                                                   // save the user selection
////                                                   NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
////                                                   [def setObject:selectedBroker forKey:@"Brokerhouse"];
////                                                   [def synchronize];
////                                               }];
////
////                           }
//                       });
//
//                   } else {
//                       [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
//                                          showRetry:YES showClose:NO activity:NO];
//
//
//                   }
//
//               }];
//
//    [downloadTask resume];
//
//}
//
//#pragma mark - Other methods
//
//-(void)timeOutVertxConnect {
//
//    dispatch_async(dispatch_get_main_queue(), ^ {
//
//
//        [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to open Vertx. Check your internet connection and try again. If problem persist, please contact us."]
//                           showRetry:YES showClose:YES activity:NO];
//
//    });
//
//}

#pragma mark - Tutorial
-(void)buildIntro{
	
	//Create Panel From Nib
	MYIntroductionPanel *panel1 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage1"];
	MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage2"];
	MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage3"];
	MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage4"];
	MYIntroductionPanel *panel5 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage5"];
	MYIntroductionPanel *panel6 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage6"];
	MYIntroductionPanel *panel7 = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"TutorialPage7"];
	
	//Add panels to an array
	NSArray *panels = @[panel1,panel2,panel3,panel4,panel5,panel6,panel7];
	
	//Create the introduction view and set its delegate
	MYBlurIntroductionView *introductionView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	introductionView.delegate = self;
	introductionView.BackgroundImageView.image = [UIImage imageNamed:@"main_background_icon.png"];
	[introductionView setBackgroundColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:0.65]];
	//introductionView.LanguageDirection = MYLanguageDirectionRightToLeft;
	
	//Build the introduction with desired panels
	[introductionView buildIntroductionWithPanels:panels];
	
	//Add the introduction to your view
	[self.view addSubview:introductionView];
}

#pragma mark - TCMenuDropDown
- (void)didSelectRowAt:(NSInteger)index inView:(TCMenuDropDown *)menuView{
	NSArray *languageArray = [[LanguageManager defaultManager] supportedLanguages];
	NSString *languageSelected = [languageArray objectAtIndex:index];
	NSLog(@"languageSelected = %@",languageSelected);
	[self selectLanguage:languageSelected];
}

#pragma mark - ChangePasswordPinDelegate
- (void) ChangePasswordSuccess {
	//Call to Change Pin after change password
	if (callChangePwdAgain) {
		UINavigationController *navCon;//7_ For further: need to check whether have to create new navigation or can use self
		ChangePasswdViewController *chngPwVC = [[ChangePasswdViewController alloc]initWithNibName:@"ChangePasswdView" bundle:nil];
		navCon = [[UINavigationController alloc]initWithRootViewController:chngPwVC];
		
		UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:[LanguageManager stringForKey:@"Back"] style:UIBarButtonItemStylePlain target:self action:@selector(dismissChangePwdOrPin:)];
		chngPwVC.navigationItem.leftBarButtonItem = leftButton;
		
		[chngPwVC setAction_type:CHANGE_PIN_CODE];
		[chngPwVC setFromLoginVC:YES];
		[chngPwVC setDelegate:self];
		
		[self presentViewController:navCon animated:NO completion:nil];
	}
}

- (void) ChangePinSuccess {
	callChangePwdAgain = NO;
}

- (IBAction) dismissChangePwdOrPin:(id)sender {
	[self dismissViewControllerAnimated:NO completion:nil];
	//[self dismissModalViewControllerAnimated:navCon];
}

@end

