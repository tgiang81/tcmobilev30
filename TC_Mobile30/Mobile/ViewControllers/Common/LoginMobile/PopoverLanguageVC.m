//
//  PopoverLanguageVC.m
//  TC_Mobile30
//
//  Created by Admin on 2/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "PopoverLanguageVC.h"
#import "LanguageViewCell.h"
#import "LanguageManager.h"
#import "UserSession.h"
#import "Utils.h"
#import "UIViewController+Alert.h"
#import "LanguageKey.h"
@interface PopoverLanguageVC ()

@end

@implementation PopoverLanguageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    languagesSupport=[[NSArray alloc]init];
    NSArray*supportLanguageName=[LanguageManager getNameSupportLanguage];
    self.preferredContentSize=CGSizeMake(240, 120);
    NSArray *temp=[[NSArray alloc]init];
    for (NSInteger i= 0;i<supportLanguageName.count ; i++) {
        temp=[temp arrayByAddingObject:[LanguageManager stringForKey:supportLanguageName[i]]];
    }
    languagesSupport=temp;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return languagesSupport.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LanguageViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellLanguage" forIndexPath:indexPath];
    cell.lblLanguageName.text=languagesSupport[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSArray *languageSupport=[LanguageManager getSupportLanguage];
    if (languageSupport.count>0) {
        [self showCustomAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:[LanguageManager stringForKey:@"Bạn chắc chắn muốn thay đổi ngôn ngữ?"] withOKAction:^{
           [self selectedLanguage:languageSupport[indexPath.row]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeLanguageNotification object:nil];

        [self dismissViewControllerAnimated:YES completion:nil];
        } cancelAction:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
    
}
-(void)selectedLanguage:(NSString *)language{
    if ([language isEqualToString:@"en"]) {
        [UserSession shareInstance].languageType = LanguageType_EN;
    }
    if ([language isEqualToString:@"cn"]) {
        [UserSession shareInstance].languageType = LanguageType_CN;
    }
    if ([language isEqualToString:@"vn"]) {
        [UserSession shareInstance].languageType = LanguageType_VN;
    }
    //Update Language
    [[LanguageManager defaultManager] setLanguage:[Utils languageByType:[UserSession shareInstance].languageType]];
    //Alway save to session last language choosen
    [[UserSession shareInstance] save];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
