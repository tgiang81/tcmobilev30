//
//  PopoverLanguageVC.h
//  TC_Mobile30
//
//  Created by Admin on 2/26/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PopoverLanguageVC : UITableViewController
{
    NSArray *languagesSupport;
}
-(void)selectedLanguage:(NSString *)language;
@end

NS_ASSUME_NONNULL_END
