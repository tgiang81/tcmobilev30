//
//  PopPickerViewController.m
//  PopOver
//
//  Created by Admin on 2/25/19.
//  Copyright © 2019 GreenWorm. All rights reserved.
//

#import "PopPickerViewController.h"
#import "LanguageManager.h"
#import "UserSession.h"
#import "Utils.h"

@interface PopPickerViewController ()

@end

@implementation PopPickerViewController
- (void)viewDidLoad {
    [super viewDidLoad];
   
}
-(void)viewWillAppear:(BOOL)animated{
    dataPicker=[[NSArray alloc]init];
    NSArray*supportLanguageName=[LanguageManager getNameSupportLanguage];
    self.preferredContentSize=CGSizeMake(240, 120);
    NSArray *temp=[[NSArray alloc]init];
    for (NSInteger i= 0;i<supportLanguageName.count ; i++) {
        temp=[temp arrayByAddingObject:[LanguageManager stringForKey:supportLanguageName[i]]];
    }
    dataPicker=temp;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSArray *languageSupport=[LanguageManager getSupportLanguage];
    if (languageSupport.count>0) {
        [self selectedLanguage:languageSupport[row]];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeLanguageNotification object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return dataPicker.count;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return dataPicker[row];
    
}

-(void)selectedLanguage:(NSString *)language{
    if ([language isEqualToString:@"en"]) {
        [UserSession shareInstance].languageType = LanguageType_EN;
    }
    if ([language isEqualToString:@"cn"]) {
        [UserSession shareInstance].languageType = LanguageType_CN;
    }
    if ([language isEqualToString:@"vn"]) {
        [UserSession shareInstance].languageType = LanguageType_VN;
    }
    //Update Language
    [[LanguageManager defaultManager] setLanguage:[Utils languageByType:[UserSession shareInstance].languageType]];
    //Alway save to session last language choosen
    [[UserSession shareInstance] save];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
