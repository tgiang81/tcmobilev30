//
//  BaseTabBarViewController.h
//  TCiPad
//
//  Created by Kaka on 5/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"

@interface BaseTabBarViewController : UITabBarController
- (void)changeViewAtIndex:(NSInteger)index;
@end
