//
//  MyCustomTitleView.m
//  TCiPad
//
//  Created by giangtu on 5/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "MyCustomTitleView.h"
#import "TCBaseButton.h"

@interface MyCustomTitleView ()

@end


@implementation MyCustomTitleView
- (void)awakeFromNib{
    [super awakeFromNib];
	[self setup];
}

- (void)layoutSubviews{
	[super layoutSubviews];
	[self layoutIfNeeded];
	_btnLeftItem.showsTouchWhenHighlighted = NO;
	_btnRightItem.showsTouchWhenHighlighted = NO;
}

- (CGSize)intrinsicContentSize
{
    return UILayoutFittingExpandedSize;
}


#pragma mark - Action
-(IBAction)fnMainNavClick:(id)sender
{
    //Left -> Right... 10 11 12
    UIButton *btn = (UIButton*) sender;
    //set selected
    self.myMainNavCallback(btn);
    
}
- (IBAction)tapTitleAction:(id)sender {
	if(self.didTapTitleHandler){
		self.didTapTitleHandler();
	}
}

#pragma mark - Main
- (void)setup{
	//Default hidden baritems
	_btnLeftItem.hidden = YES;
	_btnRightItem.hidden = YES;
	_btnLeftItem.imageView.tintColor = AppColor_TintColor;
	_btnRightItem.imageView.tintColor = AppColor_TintColor;
	_btnLeftItem.titleLabel.font = AppFont_MainFontMediumWithSize(14);
	_btnRightItem.titleLabel.font = AppFont_MainFontMediumWithSize(14);

	self.clipsToBounds = YES;
	self.constraintWidth.constant = UIScreen.mainScreen.bounds.size.width;
	self.viewNavigation1.backgroundColor = AppColor_MainBackgroundColor;
	_lblTitle.font = AppFont_MainFontMediumWithSize(14);
	_lblTitle.textColor = [UIColor whiteColor];
	_lblTitle.adjustsFontSizeToFitWidth = YES;
	_btnLeftItem.showsTouchWhenHighlighted = NO;
	_btnRightItem.showsTouchWhenHighlighted = NO;

}
- (void)setTitle:(NSString *)title{
	_lblTitle.text = title;
	_title = title;
}

//Control
//Control with Image
- (void)setbarItem:(BarItemType)type icon:(NSString *)iconName selector:(SEL)selector forTarget:(id)target{
	UIImage *imageIcon = [UIImage imageNamed:iconName];
	if (type == BarItemType_Left) {
		[_btnLeftItem removeTarget:nil
						   action:NULL
				 forControlEvents:UIControlEventAllEvents];
		[_btnLeftItem setImage:imageIcon forState:UIControlStateNormal];
		[_btnLeftItem addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
		_btnLeftItem.hidden = NO;
		[_btnLeftItem setTitle:@"" forState:UIControlStateNormal];
	}
	if (type == BarItemType_Right) {
		[_btnRightItem removeTarget:nil
							action:NULL
				  forControlEvents:UIControlEventAllEvents];
		[_btnRightItem setImage:imageIcon forState:UIControlStateNormal];
		[_btnRightItem addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
		_btnRightItem.hidden = NO;
		[_btnRightItem setTitle:@"" forState:UIControlStateNormal];
	}
}

//Control With Title
- (void)setbarItem:(BarItemType)type title:(NSString *)title selector:(SEL)selector forTarget:(id)target{
	if (type == BarItemType_Left) {
		[_btnLeftItem removeTarget:nil
							action:NULL
				  forControlEvents:UIControlEventAllEvents];
		[_btnLeftItem setImage:nil forState:UIControlStateNormal];
		[_btnLeftItem addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
		_btnLeftItem.hidden = NO;
		[_btnLeftItem setTitle:title forState:UIControlStateNormal];
	}
	if (type == BarItemType_Right) {
		[_btnRightItem removeTarget:nil
							 action:NULL
				   forControlEvents:UIControlEventAllEvents];
		[_btnRightItem setImage:nil forState:UIControlStateNormal];
		[_btnRightItem addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
		_btnRightItem.hidden = NO;
		[_btnRightItem setTitle:title forState:UIControlStateNormal];
	}
}

@end
