//
//  MyCustomTitleView.h
//  TCiPad
//
//  Created by giangtu on 5/10/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"
@class TCBaseButton;

typedef void (^callBackMainNav) (UIButton*);

@interface MyCustomTitleView : UIView
@property (weak, nonatomic) IBOutlet UIView *viewNavigation1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidth;
@property (nonatomic, copy) callBackMainNav myMainNavCallback;
@property (nonatomic, copy) dispatch_block_t didTapTitleHandler;


@property (weak, nonatomic) IBOutlet TCBaseButton *btnLeftItem;
@property (weak, nonatomic) IBOutlet TCBaseButton *btnRightItem;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
//Constrain
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLeftButtonConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightLeftButtonConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthRightButtonConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightRightButtonConstrain;



@property (strong, nonatomic) NSString *title;
- (IBAction)fnMainNavClick:(id)sender;
- (void)setbarItem:(BarItemType)type icon:(NSString *)iconName selector:(SEL)selector forTarget:(id)target;
- (void)setbarItem:(BarItemType)type title:(NSString *)title selector:(SEL)selector forTarget:(id)target;

@end
