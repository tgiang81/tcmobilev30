//
//  BaseVC.h
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllCommon.h"
#import "Utils.h"
#import "ProcessViewController.h"
#import "UIViewController+Alert.h"
#import "MyCustomTitleView.h"
#import "ControllerModel.h"
#import "UIViewController+Popup.h"
#import "UIView+TCUtil.h"

@class MZFormSheetController;
@class MyCustomTitleView;

@interface BaseVC : UIViewController{
	
}
@property (assign, nonatomic) BOOL disableTabbarTitle;
@property (assign, nonatomic) BOOL isUsingCustomNavigationBar;
@property (assign, nonatomic) BOOL enableTapDismissKeyboard;
@property (assign, nonatomic) BOOL enableCheckButtonTapDismissKeyboard;
@property (nonatomic, strong, setter = setTagObject:) id tagObject;
@property (nonatomic, strong) MyCustomTitleView *customNavigationBar;
@property (strong, nonatomic) NSString *customBarTitle;
@property(nonatomic, assign) BOOL isHome;
//Passing data if needed
@property (strong, nonatomic) NSIndexPath *indexPath;

//Class function
+ (NSString *)storyboardID;
+ (NSString *)nibName;

#pragma mark - Items on NavigationBar
//Handle Menu
- (void)toogleLeftMenu:(id)sender;
- (void)tapOnView:(UITapGestureRecognizer *)tap;
//Main
- (void)createLeftMenu;
- (void)setBackButtonDefault;
- (void)restrictRotation:(BOOL) restriction;
//Test
//- (void)createRightFavoriteButtonItemWithSelector:(SEL)selector;
//- (void)createCustomRightMenuWithImage:(NSString *)imageName selector:(SEL)selector;

//============= +++ For New Design with Custom +++ =====================
- (void)createCustomBar:(BarItemType)barType icon:(NSString *)iconName selector:(SEL)selector;
- (void)createCustomBar:(BarItemType)barType title:(NSString *)title selector:(SEL)selector;


#pragma mark - End Editting VC
- (void)endEditting;

#pragma mark - Navigate VC
- (void)nextTo:(UIViewController *)vc animate:(BOOL)animate;
- (void)presentWithNav:(UIViewController *)vc animate:(BOOL)animate;
- (void)popToView:(Class)vc animate:(BOOL)animate;
- (void)popView:(BOOL)animate;


#pragma mark - Notification
// Notification Utils
- (void)postNotification:(NSString *)notiName object:(id)obj;
- (void)postNotification:(NSString *)notiName object:(id)obj userInfo:(NSDictionary *)userInfo;

- (void)addNotification:(NSString *)notiName selector:(SEL)selector;
- (void)removeNotification:(NSString *)notiName;
- (void)removeAllNotification;

#pragma mark - MAIN
- (void)createUI;
- (void)setupLanguage:(NSNotification *)noti;
- (void)setupFontAnColor;

- (void)loadAtFirstTime;
- (void)backButtonTapped:(id)sender;

#pragma mark - Update Theme
- (void)updateTheme:(NSNotification *)noti;

#pragma mark - Utils VC
- (BOOL) checkBiometryIsLocked;
- (void)showEnterpasscodeToUnclockTouchID;
@end
