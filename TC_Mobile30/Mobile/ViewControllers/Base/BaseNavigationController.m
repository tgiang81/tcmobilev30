//
//  BaseNavigationController.m
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseNavigationController.h"
#import "UIViewController+TCUtil.h"
#import "UIView+ShadowBorder.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self setupNavigationBar];
	//Update Theme
	[self updateTheme:nil];
	[self addNotification:kDidChangeThemeNotification selector:@selector(updateTheme:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UTILS
- (void)drawShadow{
	CGColorRef shadowColor = [UIColor blackColor].CGColor;
	self.navigationBar.layer.shadowColor = shadowColor;
	self.navigationBar.layer.shadowOpacity = 0.4;
	self.navigationBar.layer.shadowOffset = CGSizeMake(0, 3);
	CGRect shadowPath = CGRectMake(self.navigationBar.layer.bounds.origin.x - 10, self.navigationBar.layer.bounds.size.height - 6, self.navigationBar.layer.bounds.size.width + 20, 1);
	self.navigationBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
	self.navigationBar.layer.shouldRasterize = YES;
}
//Setup
- (void)setupNavigationBar{
	self.navigationBar.barTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].navBgColor);
	self.navigationBar.tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].navTintColor);
	if([UINavigationBar conformsToProtocol:@protocol(UIAppearanceContainer)]) {
		[UINavigationBar appearance].tintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].navTintColor);
	}
	[self.navigationBar setTitleTextAttributes:@{ NSForegroundColorAttributeName: TC_COLOR_FROM_HEX([ThemeManager shareInstance].navTextTintColor), NSFontAttributeName: AppFont_MainFontMediumWithSize(18.0f)}];
	//For iOS 9 up
	[[UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UINavigationBar class]]] setTitleTextAttributes:@{ NSFontAttributeName:AppFont_MainFontMediumWithSize(18.0f) } forState:UIControlStateNormal];
	
	[self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
	self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
	self.navigationBar.layer.masksToBounds = NO;
	self.navigationBar.layer.shouldRasterize = YES;
	
	self.navigationBar.translucent = NO;
	self.navigationBar.opaque = NO;
    
	[UIApplication sharedApplication].statusBarStyle = [[ThemeManager shareInstance] rootViewStatusBarStyle];
	//Remove bottom line from NavigationBar
	//[[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
	//[[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
}

#pragma mark - StatusBar Style - System
#pragma mark - StatusBar Style
//Override on LGSideMenu
- (BOOL)rootViewStatusBarHidden{
	return NO;
}
- (UIStatusBarStyle) preferredStatusBarStyle {
    return [[ThemeManager shareInstance] rootViewStatusBarStyle];
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
	return UIStatusBarAnimationFade;
}
#pragma mark - StatusBar

#pragma mark - Notification
- (void)updateTheme:(NSNotification *)noti{
    [self setupNavigationBar];
	[self setNeedsStatusBarAppearanceUpdate];
}
@end
