 //
//  BaseVC.m
//  TCiPad
//
//  Created by Kaka on 3/19/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseVC.h"
#import "MZFormSheetController.h"
#import "UIViewController+Util.h"
#import "BaseNavigationController.h"
#import "LanguageKey.h"
#import "DeviceUtils.h"
@interface BaseVC () <UIGestureRecognizerDelegate>{
	UITapGestureRecognizer *_tap;
    BOOL isLoaded;
}

@end

@implementation BaseVC
- (id)initWithCoder:(NSCoder *)aDecoder {
	if (self = [super initWithCoder:aDecoder]) {
	}
    
	return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self restrictRotation:YES];
    //BEGIN
    //Custom navigation bar
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.enableTapDismissKeyboard = NO;
	self.isUsingCustomNavigationBar = NO;
	
	//Default background
	//self.view.backgroundColor = AppColor_MainBackgroundColor;
    // Do any additional setup after loading the view.
	//Create Tap action on View
	
	//Remove footer on empty cell
	for (UIView *v in self.view.subviews) {
		if ([v isKindOfClass:[UITableView class]]) {
			UITableView *tblView = (UITableView *)v;
			tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
		}
	}
	[self addNotification:kDidChangeThemeNotification selector:@selector(updateTheme:)];
	//Update Theme
	[self updateTheme:nil];
//    [self addNotification:kLanguageDidChangeNotification selector:@selector(setLanguage:)];
    //Update Language
    [self setupLanguage:nil];

    NSLog(@"My class: %@", NSStringFromClass([self class]));
}
- (void)loadAtFirstTime{
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.isHome){
        [self setTitle:[LanguageManager stringForKey:Home]];
        [self.navigationItem setTitle:[NSString stringWithFormat:@"%@:%@", [LanguageManager stringForKey:Home], [SettingManager getSettingDefaultPageName]]];
    }
}
- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    AppDelegateAccessor.topViewController = self;
    if(isLoaded == NO){
        isLoaded = YES;
        [self loadAtFirstTime];
    }
    [AppDelegateAccessor enableFilterVC:NO];
    if(self.disableTabbarTitle){
        NSString *currentTitle = self.navigationItem.title;
        if(_customBarTitle != nil && ![_customBarTitle isEqualToString:@""]){
            currentTitle = _customBarTitle;
        }else{
            if(self.title != nil && ![self.title isEqualToString:@""]){
                currentTitle = self.title;
            }
        }
        self.title = @"";
        self.navigationItem.title = currentTitle;
        
    }
	DLog(@"VIEWCONTROLLER: %@", NSStringFromClass([self class]));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Orientation
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
	return UIInterfaceOrientationPortrait;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
	return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate {
	return NO;
}
-(void) restrictRotation:(BOOL) restriction
{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Main
//============ Override these function below to make UI ============
- (void)createUI{
	
}

- (void)setupLanguage: (NSNotification *)noti{
    DLog(@"+++ Language did change");
    //Some default value
    
}

- (void)setupFontAnColor{
	
}

#pragma mark - Overide Property
- (void)setIsUsingCustomNavigationBar:(BOOL)isUsingCustomNavigationBar{
	if (isUsingCustomNavigationBar) {
		//Make alway visible bar items
		[self.navigationItem setLeftBarButtonItem: nil];
		[self.navigationItem setRightBarButtonItem: nil];
		self.navigationItem.hidesBackButton = YES;
		_customNavigationBar = [[[NSBundle mainBundle] loadNibNamed:@"MyCustomTitleView" owner:self options:nil] objectAtIndex:0];
		
		self.navigationItem.titleView = _customNavigationBar;
	}else{
		self.navigationItem.titleView = _customNavigationBar;
		self.navigationItem.titleView = nil;
	}
	_isUsingCustomNavigationBar = isUsingCustomNavigationBar;
}
- (void)setEnableTapDismissKeyboard:(BOOL)enableTapDismissKeyboard{
	if (enableTapDismissKeyboard) {
		if (!_tap) {
			_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
		}
        if(self.enableCheckButtonTapDismissKeyboard){
            _tap.delegate = self;
        }
		[_tap setCancelsTouchesInView:NO];
		[self.view addGestureRecognizer:_tap];
	}else{
		if (_tap) {
			[self.view removeGestureRecognizer:_tap];
			_tap = nil;
		}
	}
	_enableTapDismissKeyboard = enableTapDismissKeyboard;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([[touch view] isKindOfClass:[UIButton class]]){
        return NO;
    }
    
    return YES;
}
#pragma mark - Class Function
+ (NSString *)storyboardID{
	return NSStringFromClass([self class]);
}

+ (NSString *)nibName{
	return NSStringFromClass([self class]);
}

//GIANG comment
#pragma mark - Action Custom Navigation
- (void)setCustomBarTitle:(NSString *)customBarTitle{
	if (_isUsingCustomNavigationBar) {
		_customNavigationBar.title = customBarTitle;
	}else{
        self.title = customBarTitle;
	}
	_customBarTitle = customBarTitle;
}

- (void)setBackButtonDefault{
	if (!_isUsingCustomNavigationBar) {
		UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_item_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonTapped:)];
		//[leftBtn setImageInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
		self.navigationItem.leftBarButtonItem = leftBtn;
	}else{
		[self createCustomBar:BarItemType_Left icon:@"back_item_icon" selector:@selector(backButtonTapped:)]; //icon: new_back_icon - item_back_icon
	}
}

- (void)backButtonTapped:(id)sender{
    [self backVC:nil];
    
}

- (void)cusTomBackButtonWithSelector:(SEL)selector{
	if (!_isUsingCustomNavigationBar) {
		UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_item_icon"] style:UIBarButtonItemStylePlain target:self action:selector];
		self.navigationItem.leftBarButtonItem = leftBtn;
	}else{
		[_customNavigationBar setbarItem:BarItemType_Left icon:@"back_item_icon" selector:selector forTarget:self];
	}
}

- (void)createLeftMenu{
	if (!_isUsingCustomNavigationBar) {
		UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(toogleLeftMenu:)];
    	[leftBtn setImageInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    	self.navigationItem.leftBarButtonItem = leftBtn;
	}else{
		[_customNavigationBar setbarItem:BarItemType_Left icon:@"menu_icon" selector:@selector(toogleLeftMenu:) forTarget:self];
	}
}


//RightBarButtonItem
- (void)createRightFavoriteButtonItemWithSelector:(SEL)selector{
//    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuGrid"] style:UIBarButtonItemStylePlain target:self action:selector];
//    //[leftBtn setImageInsets:UIEdgeInsetsMake(0, -8, 0, 0)];
//    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (void)createCustomRightMenuWithImage:(NSString *)imageName selector:(SEL)selector{
	if (!_isUsingCustomNavigationBar) {
		UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:imageName] style:UIBarButtonItemStylePlain target:self action:selector];
		self.navigationItem.rightBarButtonItem = rightBtn;
	}else{
		[self createCustomBar:BarItemType_Right icon:imageName selector:selector];
	}
}

//============= +++ For New Design with Custom +++ =====================
- (void)createCustomBar:(BarItemType)barType icon:(NSString *)iconName selector:(SEL)selector{
	//+++ Unuse default style NavigationBar
	/*
    UIImage* image3 = [UIImage imageNamed:iconName];
    CGRect frameimg = CGRectMake(0, (44 - (image3.size.height) / 2), image3.size.width, image3.size.height);
    UIButton *someButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    someButton.frame = frameimg;
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:selector
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    [someButton setImageEdgeInsets: UIEdgeInsetsMake(0, 18, 0, 6)];
    UIBarButtonItem *barItem =[[UIBarButtonItem alloc] initWithCustomView:someButton];

    //UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithImage:image3 style:UIBarButtonItemStylePlain target:self action:selector];
    if (barType == BarItemType_Left) {
        //[barItem setImageInsets:UIEdgeInsetsMake(0, 14, 0, 0)];
        self.navigationItem.leftBarButtonItem = barItem;
    }else{
        //[barItem setImageInsets:UIEdgeInsetsMake(0, 0, 0, 14)];
        self.navigationItem.rightBarButtonItem = barItem;
    }
	 */
	//[self.navigationItem setLeftBarButtonItem: nil];
	//[self.navigationItem setRightBarButtonItem: nil];
	//self.navigationItem.hidesBackButton = YES;
	if (!_isUsingCustomNavigationBar) {
		UIImage* barImage = [UIImage imageNamed:iconName];
		UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithImage:barImage style:UIBarButtonItemStylePlain target:self action:selector];
		if (barType == BarItemType_Left) {
			self.navigationItem.leftBarButtonItem = barItem;
		}else{
			self.navigationItem.rightBarButtonItem = barItem;
		}
	}else{
		[_customNavigationBar setbarItem:barType icon:iconName selector:selector forTarget:self];
	}
}

//For Title BarItem
- (void)createCustomBar:(BarItemType)barType title:(NSString *)title selector:(SEL)selector{
	if (!_isUsingCustomNavigationBar) {
		UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:selector];
		if (barType == BarItemType_Left) {
			self.navigationItem.leftBarButtonItem = barItem;
		}else{
			self.navigationItem.rightBarButtonItem = barItem;
		}
	}else{
		[_customNavigationBar setbarItem:barType title:title selector:selector forTarget:self];
	}
}

#pragma mark - Actions
- (void)toogleLeftMenu:(id)sender {
	[AppDelegateAccessor toogleLeftMenu:nil];
}


#pragma mark - StatusBar Style
- (BOOL)prefersStatusBarHidden {
	return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	if (![UserSession shareInstance].didLogin) {
		return UIStatusBarStyleDefault;
	}
	if ([[ThemeManager shareInstance] getCurrentTheme] == ThemeType_Light) {
		return UIStatusBarStyleDefault;
	}
	return UIStatusBarStyleLightContent;
}

//Override on LGSideMenu
- (BOOL)rootViewStatusBarHidden{
	return NO;
}
//- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
//	return UIStatusBarAnimationNone;
//}
#pragma mark - UITouchEvent
- (void)tapOnView:(UITapGestureRecognizer *)tap{
	DLog(@"+++ Tap on View");
	if (_enableTapDismissKeyboard == YES) {
		[self endEditting];
	}
}

//Use this for dismiss keyboard
- (void)endEditting{
	[self.view endEditing:YES];
}
#pragma mark - Navigate VC
- (void)nextTo:(UIViewController *)vc animate:(BOOL)animate{
	[self.navigationController pushViewController:vc animated:animate];
}

- (void)presentWithNav:(UIViewController *)vc animate:(BOOL)animate{
    BaseNavigationController *nv = [[BaseNavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nv animated:YES completion:nil];
}
- (void)popToView:(Class)vc animate:(BOOL)animate{
	if (vc) {
		NSArray *viewControllers = self.navigationController.viewControllers;
		if (viewControllers && viewControllers.count > 0) {
			for (UIViewController *view in viewControllers) {
				if ([view isKindOfClass:vc]) {
					[self.navigationController popToViewController:view animated:animate];
					break;
				}
			}
		}
	}
}

- (void)popView:(BOOL)animate{
	[self.navigationController popViewControllerAnimated:animate];
}

#pragma mark - Notification
//Post
- (void)postNotification:(NSString *)notiName object:(id)obj{
	[self postNotification:notiName object:obj userInfo:nil];
}
- (void)postNotification:(NSString *)notiName object:(id)obj userInfo:(NSDictionary *)userInfo{
	[[NSNotificationCenter defaultCenter] postNotificationName:notiName object:obj userInfo:userInfo];
}
//Add
- (void)addNotification:(NSString *)notiName selector:(SEL)selector{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:selector name:notiName object:nil];
}
//Remove
- (void)removeNotification:(NSString *)notiName{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:notiName object:nil];
}
- (void)removeAllNotification{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Update Theme Notification
- (void)updateTheme:(NSNotification *)noti{
	DLog(@"+++ Theme did change");
	//Some default value
	self.view.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	[self setNeedsStatusBarAppearanceUpdate];
}

#pragma mark - Utils VC
- (BOOL) checkBiometryIsLocked {
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        // get BiometryType
        return NO;
    }
    else {
        if (@available(iOS 11.0, *)) {
            if (error.code == LAErrorBiometryLockout) {
                DLog(@"777 The Biometry is lock out with error.code = %ld", (long)error.code);
                return YES;
            }
            else {
                return NO;
            }
        } else {
            // Fallback on earlier versions
            return NO;
        }
    }
}

- (void)showEnterpasscodeToUnclockTouchID {
    NSInteger lABiometryType = [DeviceUtils getLABiometryType];
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    NSString *message;
    if (lABiometryType == TOUCHID_AVAILABLE) {
        message = [LanguageManager stringForKey:Please_enter_device_ID_to_reopen_fingerprint_function];
    }
    else if(lABiometryType == FACEID_AVAILABLE) {
        message = [LanguageManager stringForKey:Please_enter_device_ID_to_reopen_faceID_function];
    }
    else {
        DLog(@"Error 777");
        return;
    }
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&error]) {
        // Authenticate User
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthentication
                localizedReason:message
                          reply:^(BOOL success, NSError *error) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (success) {
                                      DLog(@"777 unlock TouchID successful");
                                  }else{
                                      DLog(@"Failed with error.code = %ld", (long)error.code);
                                  }
                              });
                          }];
    }else{
        [self showCustomAlertWithMessage:error.localizedDescription];
    }
}

@end
