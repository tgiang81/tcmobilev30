//
//  MyContainerVC.m
//  TC_Mobile30
//
//  Created by Kaka on 3/13/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "MyContainerVC.h"
#import "AllCommon.h"
#import "TCBaseButton.h"
#import "UIView+ShadowBorder.h"
#import "MTradeViewController.h"
#import "BaseNavigationController.h"
#import "BrokerModel.h"
#import "BrokerManager.h"
#import "MStockInfoVC.h"
#import "MOrderBookDetailVC.h"
#import "DetailUnrealisedPortfolio.h"
#import "DetailRealisedPortfolio.h"
#import "BaseTabBarViewController.h"


#define kSIZE_FLOAT_BUTTON				50
#define kMARGIN_BOTTOM_FLOAT_BUTTON		20
#define kMARGIN_RIGHT_FLOAT_BUTTON		20

@interface MyContainerVC (){
	TCBaseButton *floatButton;
}

@end

@implementation MyContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.rootViewStatusBarStyle = [[ThemeManager shareInstance] rootViewStatusBarStyle];
	[self registerNotification];
}

- (void)viewWillLayoutSubviews{
	[super viewWillLayoutSubviews];
	self.leftViewPresentationStyle = LGSideMenuPresentationStyleSlideAbove;
	self.rightViewPresentationStyle = LGSideMenuPresentationStyleSlideAbove;
	self.leftViewWidth = SCREEN_WIDTH_PORTRAIT - 60;
	if (floatButton) {
		[self.view bringSubviewToFront:floatButton];
	}
}

- (void)viewDidLayoutSubviews{
	[super viewDidLayoutSubviews];
	[self prepareFloatTradeButton];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Register Notification
- (void)registerNotification{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReciveNotification:) name:kDidUpdateSettingFloatTradingButtonNotification object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Main
- (void)prepareFloatTradeButton{
	//+++Draw Float button
	if ([UserSession shareInstance].didLogin) {
		if ([SettingManager shareInstance].enableFloatTradingButton) {
			if (!floatButton) {
				float tabbarHeight = AppDelegateAccessor.rootTabbarVC.tabBar.frame.size.height;
				floatButton = [TCBaseButton buttonWithType:UIButtonTypeCustom];
				floatButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - (kSIZE_FLOAT_BUTTON + kMARGIN_RIGHT_FLOAT_BUTTON), [UIScreen mainScreen].bounds.size.height - (kSIZE_FLOAT_BUTTON + tabbarHeight + kMARGIN_BOTTOM_FLOAT_BUTTON), kSIZE_FLOAT_BUTTON, kSIZE_FLOAT_BUTTON);
				NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
				floatButton.backgroundColor = TC_COLOR_FROM_HEX(mainTintColor);
				[floatButton setImage:[UIImage imageNamed:@"black_cart_icon"] forState:UIControlStateNormal]; //float_un_shadow_trade_icon - float_trade_icon - trade_hl_menu
				floatButton.isRounded = YES;
				floatButton.borderWidth = 0.0;
				floatButton.showsTouchWhenHighlighted = YES;
				[floatButton addTarget:self action:@selector(onTradeAction:) forControlEvents:UIControlEventTouchDown];
				//Draw shadow
				floatButton.layer.shadowColor = [UIColor blackColor].CGColor;
				[floatButton.layer setShadowOpacity:0.327];
				[floatButton.layer setShadowRadius:2.492];
				[floatButton.layer setShadowOffset:CGSizeMake(1.51, 4.380)];
				floatButton.layer.shouldRasterize = NO;
				floatButton.layer.masksToBounds = NO;
				//[floatButton dropShadow];
				[self.view addSubview:floatButton];
			}
		}
	}
}

#pragma mark - ACTIONS
- (void)onTradeAction:(id)sender{
    UIViewController *currentVC = AppDelegateAccessor.topViewController;
	MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
    if(([currentVC isKindOfClass:[MStockInfoVC class]]) || ([currentVC isKindOfClass:[MOrderBookDetailVC class]]) || ([currentVC isKindOfClass:[DetailUnrealisedPortfolio class]]) || ([currentVC isKindOfClass:[DetailRealisedPortfolio class]])){
        controller.isNotFromTradeButton = NO;
    }else{
        controller.isNotFromTradeButton = YES;
    }
    
	controller.tradeType = TradeType_Buy; //Default
	controller.actionTypeDict = @{};
	//controller.title = [LanguageManager stringForKey:@"Home"];
	//controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
	//controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];
	
	BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
	
	[self presentViewController:navController animated:YES completion:^{
	}];
}

#pragma mark - Notification
- (void)didReciveNotification:(NSNotification *)noti{
	if ([noti.name isEqualToString:kDidUpdateSettingFloatTradingButtonNotification]) {
		if ([SettingManager shareInstance].enableFloatTradingButton) {
			[self prepareFloatTradeButton];
		}else{
			//Remove trading button
			if (floatButton) {
				[floatButton removeFromSuperview];
				floatButton = nil;
			}
		}
	}
}
@end
