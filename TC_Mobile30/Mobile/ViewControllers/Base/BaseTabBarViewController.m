//
//  BaseTabBarViewController.m
//  TCiPad
//
//  Created by Kaka on 5/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BaseTabBarViewController.h"
#import "LGSideMenuView.h"

#import "DXPopover.h"
#import "TradePopupView.h"
#import "MTradeViewController.h"
#import "BaseNavigationController.h"
#import "TCBaseView.h"
#import "TCBaseButton.h"
#import "UIViewController+TCUtil.h"
#import "UIView+ShadowBorder.h"
#import "LanguageKey.h"
#define kSIZE_TRADEVIEW	60
#define kRATIO_SPACING_CONTENT_CENTER_TRADE_VIEW	6
#define kPADDING_BOTTOM_TRADEVIEW					10

@interface BaseTabBarViewController ()<UITabBarDelegate, TradePopupViewDelegate>{
	//For show TradePopupView
	DXPopover *_popover;
	__block BOOL _isShowingPopover;
	TCBaseView *tradeView;
}

@end

@implementation BaseTabBarViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
//		for(UITabBarItem *item in self.tabBar.items) {
//			NSInteger indexOfItem = [self.tabBar.items indexOfObject:item];
//			if (indexOfItem == TabbarItemView_Trade) {
//				item.title = nil;
//				item.image = nil;
//				item.selectedImage = nil;
//				/*
//				 NSInteger offset = 30;
//				 UIEdgeInsets imageInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
//				 item.imageInsets = imageInset;
//				 */
//			}
//		}
	}
	
	return self;
}
- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self registerNotifications];
	[self setup];
	[self updateTheme:nil];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
- (void)viewWillLayoutSubviews{
	[super viewWillLayoutSubviews];
	[self.tabBar invalidateIntrinsicContentSize];
	//Disable TabbarItem at Trade Position
	//UITabBarItem *tradeItem = self.tabBar.items[TabbarItemView_Trade];
	//tradeItem.enabled = NO;
	//tradeItem.title = @"";
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)setup{
    //NSNotificationCenter *notCenter = [NSNotificationCenter defaultCenter];
    // [notCenter removeObserver:self name:kNotiQuote object:nil];
    //[notCenter addObserver:self selector:@selector(openQuote) name:kNotiQuote object:nil];
	self.tabBar.translucent = NO;
	self.tabBar.opaque = YES;
	//Do note create Trade Button now
//	/*
//	_isShowingPopover = NO;
//	_popover = [DXPopover popover];
//
//	tradeView = [[TCBaseView alloc] initWithFrame:CGRectMake(0, 0, kSIZE_TRADEVIEW, kSIZE_TRADEVIEW)];
//	tradeView.isRounded = YES;
//	tradeView.backgroundColor = [UIColor whiteColor];
//	tradeView.borderColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainTextColor);
//	tradeView.borderWidth = 0.0;
//	tradeView.layer.masksToBounds = YES;
//
//	/*
//	CGPoint center = self.tabBar.center;
//	center.y = center.y - (kSIZE_TRADEVIEW - self.tabBar.frame.size.height);
//	tradeView.center = center;
//	*/
//
//	//Use this to fix for iPX...
//	CGRect rectBoundTabbar = [self.tabBar bounds];
//	float xx = CGRectGetMidX(rectBoundTabbar);
//	float yy = CGRectGetMidY(rectBoundTabbar) + kPADDING_BOTTOM_TRADEVIEW; //Add on tabbar using -, otherwise using +
//	if (IS_IPHONE_X || IS_IPHONE_XS || IS_IPHONE_X_MAX) {
//		yy += 30; //20 pixel from bottom to tabbar
//	}
//	tradeView.center = CGPointMake(xx, self.view.frame.size.height - yy);
//
//	//[self.view addSubview:tradeView];
//	[self.view addSubview:tradeView];
//	[self.view bringSubviewToFront:tradeView];
//
//	CGPoint centerTradeView = CGPointMake(kSIZE_TRADEVIEW / 2, kSIZE_TRADEVIEW / 2);
//	//Add iCon
//	/*
//	UIImage *imageTrade = [UIImage imageNamed:@"tabbar_trade_icon"];
//	UIImageView *_imgTrade = [[UIImageView alloc] initWithImage:imageTrade];
//	_imgTrade.frame = CGRectMake(0, 0, imageTrade.size.width, imageTrade.size.height);
//	_imgTrade.center = CGPointMake(centerTradeView.x, centerTradeView.y - (kSIZE_TRADEVIEW / kRATIO_SPACING_CONTENT_CENTER_TRADE_VIEW) + 1);
//	[_imgTrade setContentMode:UIViewContentModeScaleAspectFit];
//	*/
//	UIImage *imageTrade = [UIImage imageNamed:@"tabbar_trade_icon"];
//	UIButton *buttonImage = [UIButton buttonWithType:UIButtonTypeSystem];
//	[buttonImage setImage:[UIImage imageNamed:@"tabbar_trade_icon"] forState:UIControlStateNormal];
//	[buttonImage setUserInteractionEnabled:NO];
//	[buttonImage setContentMode:UIViewContentModeScaleAspectFit];
//	buttonImage.frame = CGRectMake(0, 0, imageTrade.size.width, imageTrade.size.height);
//	buttonImage.center = CGPointMake(centerTradeView.x, centerTradeView.y - (kSIZE_TRADEVIEW / kRATIO_SPACING_CONTENT_CENTER_TRADE_VIEW) + 1);
//	buttonImage.tintColor = [UIColor whiteColor];
//	[tradeView addSubview:buttonImage];
//
//	//Add title
//	UILabel *lblTrade = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kSIZE_TRADEVIEW, 20)];
//	lblTrade.text = [LanguageManager stringForKey:@"Trade"];
//	lblTrade.textColor = [UIColor whiteColor];
//	lblTrade.textAlignment = NSTextAlignmentCenter;
//	lblTrade.center = CGPointMake(centerTradeView.x, centerTradeView.y + (kSIZE_TRADEVIEW / kRATIO_SPACING_CONTENT_CENTER_TRADE_VIEW) - 1);
//	lblTrade.font = AppFont_MainFontMediumWithSize(12);
//	[tradeView addSubview:lblTrade];
//
//	//Trade Button
//	TCBaseButton *_tradeButton = [TCBaseButton buttonWithType:UIButtonTypeCustom];
//	_tradeButton.frame = CGRectMake(0.0, 0.0, kSIZE_TRADEVIEW, kSIZE_TRADEVIEW);
//	[_tradeButton addTarget:self action:@selector(onTradeAction:) forControlEvents:UIControlEventTouchDown];
//	_tradeButton.isRounded = YES;
//	_tradeButton.backgroundColor = [UIColor clearColor];
//	//_tradeButton.opaque = 0.4;
//	[tradeView addSubview:_tradeButton];
//	[tradeView bringSubviewToFront:_tradeButton];
//	// remove the shadow -- separate line color
//	/*
//	 [self.tabBar setShadowImage:nil];
//	 self.tabBar.layer.borderWidth = 0;
//	 self.tabBar.clipsToBounds = YES;
//	 */
//
//	//Popover:
//	__weak BaseTabBarViewController *weakSelf = self;
//	_popover.didDismissHandler = ^{ //The callback of popover dimissal.
//		[weakSelf setShowingPopover:NO];
//	};
//	_popover.didShowHandler = ^{
//		[weakSelf setShowingPopover:YES];
//	};
}

#pragma mark - StatusBar Style
//Override on LGSideMenu
- (BOOL)rootViewStatusBarHidden{
	return NO;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
	return UIStatusBarAnimationFade;
}
#pragma mark - Notification
- (void)registerNotifications{
	//[self addNotification:@"doKickout" selector:@selector(doKickOutNotification:)];
	[self addNotification:kDidChangeThemeNotification selector:@selector(updateTheme:)];
}

#pragma mark - UTILS
- (void)drawShadow{
	CGColorRef shadowColor = [UIColor blackColor].CGColor;
	self.tabBar.layer.shadowColor = shadowColor;
	self.tabBar.layer.shadowOffset = CGSizeMake(0, 0);
	self.tabBar.layer.shadowRadius = 1;
	self.tabBar.layer.shadowOpacity = 0.4;
	self.tabBar.layer.masksToBounds = NO;
	self.tabBar.layer.shouldRasterize = YES;
}
- (void)setShowingPopover:(BOOL)isShowing{
	_isShowingPopover = isShowing;
}
- (void)openQuote{
    [self changeViewAtIndex:MenuType_Quote];
}
- (void)changeViewAtIndex:(NSInteger)index{
    [self setSelectedIndex:index];
}
#pragma mark - ACTION
- (void)onTradeAction:(id)sender{
	//[self showCustomAlert:TITLE_ALERT message:@"Custom here" withOKAction:nil];
//    if([SettingManager shareInstance].isOnOrderType2){
//        [self didTapTradeType:TradeType_Buy];
//    }else{
        if (_isShowingPopover) {
            [_popover dismiss];
            return;
        }
        TradePopupView *_tradeView = [[TradePopupView alloc] initWithFrame:CGRectMake(0, 0, 0.8 * SCREEN_WIDTH_PORTRAIT, 120)];
        _tradeView.backgroundColor = [UIColor clearColor];
        _tradeView.delegate = self;
        _popover.animationIn = 0.3;
        _popover.backgroundColor = [UIColor clearColor];
        [_popover showAtPoint:CGPointMake(self.tabBar.center.x, self.tabBar.center.y - 15) popoverPostion:DXPopoverPositionUp withContentView:_tradeView inView:self.view];
//    }
	
}

#pragma mark - TradePopupViewDelegate
- (void)didTapTradeType:(TradeType)type{
	[_popover dismiss];
	//Call TradeView here...
	MTradeViewController* controller = NEW_VC_FROM_STORYBOARD(kTradeStoryboard, [MTradeViewController storyboardID]);
    controller.tradeType = type;
    controller.actionTypeDict = @{};
    controller.title = [LanguageManager stringForKey:Home];
    controller.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"];
    controller.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"];

    BaseNavigationController *navController = [[BaseNavigationController alloc] initWithRootViewController:controller];
    
    [AppDelegateAccessor.container presentViewController:navController animated:YES completion:^{
    }];
}
#pragma mark - UITabBarDelegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
	//Fix error update theme <Change text color on selected item>
	/*
	[item setTitleTextAttributes: @{
																NSForegroundColorAttributeName : TC_COLOR_FROM_HEX([ThemeManager shareInstance].unselectedTextColor), NSFontAttributeName :AppFont_MainFontRegularWithSize(12)
																} forState:UIControlStateNormal];
	[item setTitleTextAttributes: @{
																NSForegroundColorAttributeName : TC_COLOR_FROM_HEX([ThemeManager shareInstance].navTintColor), NSFontAttributeName :AppFont_MainFontRegularWithSize(12)
																} forState:UIControlStateSelected];
	*/
}

#pragma mark - Notification
- (void)updateTheme:(NSNotification *)noti{
    AppDelegateAccessor.container.rootViewStatusBarStyle = [[ThemeManager shareInstance] rootViewStatusBarStyle];
	[self setNeedsStatusBarAppearanceUpdate];
	UIColor *unselectedColor = [UIColor blackColor];
	UIColor *selectedColor = TC_COLOR_FROM_HEX([BrokerManager shareInstance].detailBroker.color.mainTint);
	if ([BrokerManager shareInstance].broker.tag == BrokerTag_SSI) {
		selectedColor = RGB(224, 40, 49);
	}
	//Clear background color
	self.tabBar.backgroundImage = [[UIImage alloc] init];
	self.tabBar.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tabBgColor);
	
	if (@available(iOS 10.0, *)) {
		//[[UITabBar appearance] setUnselectedItemTintColor:TC_COLOR_FROM_HEX([ThemeManager shareInstance].unselectedTextColor)];
		[[UITabBar appearance] setUnselectedItemTintColor:unselectedColor];
	} else {
		// Fallback on earlier versions
	}
	
	[UITabBarItem.appearance setTitleTextAttributes: @{
													   NSForegroundColorAttributeName : unselectedColor,
													   NSFontAttributeName :AppFont_MainFontRegularWithSize(12)
													   } forState:UIControlStateNormal];
	
	[UITabBarItem.appearance setTitleTextAttributes: @{
													   NSForegroundColorAttributeName : selectedColor,
													   NSFontAttributeName :AppFont_MainFontRegularWithSize(12)
													   } forState:UIControlStateSelected];
	
	[UITabBar appearance].tintColor = selectedColor;
	[UITabBar appearance].barTintColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tabBgColor);
	
	//Way 2:
	tradeView.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	tradeView.borderColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].mainBgColor);
	tradeView.borderWidth = 0.0;
	
	//Shadow
	[tradeView.layer setShadowColor:[RGB(0, 0, 0) CGColor]];
	[tradeView.layer setShadowOffset:CGSizeMake(0, 0)];
	[tradeView.layer setShadowOpacity:1];
	[tradeView.layer setShadowRadius:5.0];
	
	//Draw shadow
	[self.tabBar makeBorderShadow];
    
}

#pragma mark - Handle Notification
//* +++ No use now
//- (void)doKickOutNotification:(NSNotification *)noti{
//	dispatch_async(dispatch_get_main_queue(), ^{
//		DLog(@"DoKICKOUT: %@", noti);
//		NSString *message = noti.userInfo[@"message"];
//		[[NetworkManager shared] stopCheckingSigleSignOn];
//		if (!message) {
//			message = [LanguageManager stringForKey:@"System detect you just login to another device and this session will be stopped."];
//		}
//		[AppDelegateAccessor doKickOut:message];
//		/*
//		[self showCustomAlert:TITLE_ALERT message:message withOKAction:^{
//			[AppDelegateAccessor logout];
//		}];
//		*/
//	});
//}

@end
