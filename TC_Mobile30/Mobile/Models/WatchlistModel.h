//
//  WatchlistModel.h
//  TCiPad
//
//  Created by Kaka on 6/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface WatchlistModel : TCBaseModel{
	
}
@property (assign, nonatomic) NSInteger FavID;
@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *TimeStamp;

+ (NSMutableArray *)arrayFromDicts:(NSArray *)dictArr;

@end
