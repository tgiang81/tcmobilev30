//
//  BrokerModel.h
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface BrokerModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSString *appName;
@property (strong, nonatomic) NSString *brokerID;
@property (strong, nonatomic) NSString *bundleID;
@property (strong, nonatomic) NSString *iconName;
@property (strong, nonatomic) NSString *plistLink;
@property (assign, nonatomic) NSInteger tag;
@property (assign, nonatomic) BOOL enable;


//AppName = SJenie;
//BrokerID = 096;
//BundleID = "com.n2n.ipad.sjuat";
//IconName = "BHI_SJenie_512.png";
//PlistLink = "https://lms-my.asiaebroker.com/ios/BHConfig/ipad/config_com.n2n.ipad.sjuat.plist";
//Tag = 7;

@end
