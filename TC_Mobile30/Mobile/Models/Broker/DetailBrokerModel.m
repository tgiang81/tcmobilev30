//
//  DetailBrokerModel.m
//  TCiPad
//
//  Created by Kaka on 3/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "DetailBrokerModel.h"
@interface DetailBrokerModel (){
	NSArray *ExchangesEnableIndices;
	NSArray *InteractiveChartForExch;
	NSArray *OverrideExchangeName;
	NSArray *TrdShortSellIndicator;
}
@end
@implementation DetailBrokerModel
//- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err{
//	self = [super initWithDictionary:dict error:err];
//	if (self) {
//		/*
//		for (NSString *key in dict.allKeys) {
//			if ([key isEqualToString:@"ExchangesEnableIndices"]) {
//				self.exchangesEnableIndices = dict[key];
//			}
//			if ([key isEqualToString:@"InteractiveChartForExch"]) {
//				self.interactiveChartForExch = dict[key];
//			}
//			if ([key isEqualToString:@"OverrideExchangeName"]) {
//				self.overrideExchangeName = dict[key];
//			}
//			if ([key isEqualToString:@"TrdShortSellIndicator"]) {
//				self.TrdShortSellIndicator = dict[key];
//			}
//		}
//	  */
//	}
//	return self;
//}
+ (JSONKeyMapper *)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
																  @"twoFARegisterPgURL" : @"2FARegisterPgURL"
																  }];
}
@end
