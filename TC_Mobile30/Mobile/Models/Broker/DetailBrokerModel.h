//
//  DetailBrokerModel.h
//  TCiPad
//
//  Created by Kaka on 3/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"
#import "ColorModel.h"

@interface DetailBrokerModel : TCBaseModel{
	
}
//Color configs
@property (strong, nonatomic) ColorModel *color;
//=========== A ==============
@property (strong, nonatomic) NSString *twoFARegisterPgURL; //2FARegisterPgURL
@property (strong, nonatomic) NSString *AES_Encryption;
@property (strong, nonatomic) NSString *ATPAdminKey;
@property (strong, nonatomic) NSString *ATPCompression;
@property (assign, nonatomic) BOOL ATPLoadBalance;
@property (strong, nonatomic) NSString *ATPServer;
@property (strong, nonatomic) NSString *AboutTextViewContent;
@property (strong, nonatomic) NSString *AboutURL;
@property (strong, nonatomic) NSString *AboutUs;
//@property (strong, nonatomic) NSString *Announcement;
@property (strong, nonatomic) NSString *AnnouncementURL;
@property (strong, nonatomic) NSString *AppName;
@property (assign, nonatomic) BOOL ArchiveNewsEnabled;

//=========== B ====================
@property (strong, nonatomic) NSString *BrokerID;
@property (strong, nonatomic) NSString *BHCode; //Addmore
@property (strong, nonatomic) NSString *BrokerName;//Addmore

//Company info ======= C =============
@property (strong, nonatomic) NSString *ChartServer;
@property (assign, nonatomic) NSInteger ClearIDPwdWhenEnterBG;
@property (strong, nonatomic) NSString *CompanyAddress;
@property (strong, nonatomic) NSString *CompanyFax;
@property (strong, nonatomic) NSString *CompanyLogo;
@property (strong, nonatomic) NSString *CompanyLogo2;
@property (strong, nonatomic) NSString *CompanyName;
@property (strong, nonatomic) NSString *CompanyTel;
@property (strong, nonatomic) NSString *ImgCheck;
@property (strong, nonatomic) NSString *ImgUncheck;

//Port
@property (assign, nonatomic) NSInteger ContactUsOnLoginPg;
@property (assign, nonatomic) BOOL CreditLimitOrdBook;
@property (assign, nonatomic) BOOL CreditLimitOrdPad;
@property (assign, nonatomic) BOOL CreditLimitPortfolio;
//DMA
@property (assign, nonatomic) NSInteger DMAAgreement;
@property (strong, nonatomic) NSString *DMAWebViewURL;
@property (strong, nonatomic) NSString *DWTextViewContent;
@property (strong, nonatomic) NSString *DWURL;
//Default
@property (strong, nonatomic) NSString *DefaultCurrency;
@property (strong, nonatomic) NSString *DefaultStartingScreen;
@property (strong, nonatomic) NSString *DefaultTradePrefQtyUnit;
@property (strong, nonatomic) NSString *DefaultViewPrefQtyUnit;
@property (assign, nonatomic) BOOL QtyModifierMode;
@property (assign, nonatomic) BOOL QtyIsLot;

//===== E ======
@property (assign, nonatomic) BOOL ESettlementEnabled; //Add more
@property (strong, nonatomic) NSString *ESettlementURL; //Add more

@property (assign, nonatomic) BOOL isEncryption;
@property (assign, nonatomic) BOOL E2EE_Encryption;
@property (strong, nonatomic) NSString *EmailLink;
@property (assign, nonatomic) BOOL EnablePassword;
@property (assign, nonatomic) BOOL EnablePin;
@property (assign, nonatomic) BOOL EnabledStockAlert;
@property (assign, nonatomic) BOOL EnabledStockAlertDisclaimer;
@property (strong, nonatomic) NSArray *ExchangesEnableIndices;
@property (assign, nonatomic) BOOL EnableIDSS;

//========= F ============
@property (strong, nonatomic) NSString *FacebookLink;
@property (strong, nonatomic) NSString *FundDataCompInfoURL;
@property (strong, nonatomic) NSString *FundDataFinancialInfoURL;
@property (strong, nonatomic) NSArray *filterShowMarket; //Addmore
@property (strong, nonatomic) NSString *FundamentalNewsLabel; //Addmore
@property (strong, nonatomic) NSString *FundamentalNewsServer; //Addmore
@property (strong, nonatomic) NSString *FundamentalSourceCode; //Addmore
@property (strong, nonatomic) NSString *FundamentalDefaultReportCode; //Addmore

//======= H ==============
@property (assign, nonatomic) BOOL HTTPSEnabled;
@property (assign, nonatomic) BOOL HideDOWInSettings;
@property (assign, nonatomic) BOOL HidePPInSettings;
@property (assign, nonatomic) BOOL HideSubsStsInSettings;
@property (assign, nonatomic) BOOL HideSubsSvcInSettings;
@property (assign, nonatomic) BOOL HideTOSInSettings;
@property (strong, nonatomic) NSString *HistoricalChartServer;

//======= I ============
@property (assign, nonatomic) BOOL InteractiveChartEnabled;
@property (strong, nonatomic) NSArray *InteractiveChartForExch;
@property (strong, nonatomic) NSString *InteractiveChartURL;
@property (strong, nonatomic) NSString *IntradayChartServer;
@property (nonatomic, retain) NSMutableArray *DashBoard;
@property (nonatomic, retain) NSNumber *BrokerType;
//========== J ==================
@property (assign, nonatomic) NSInteger JavaQC;

//=========== L =============================
@property (strong, nonatomic) NSString *LoginHelpText; //Add more
@property (strong, nonatomic) NSString *LoginHelpTextCN; //Add more

@property (strong, nonatomic) NSString *LMSServer;
@property (strong, nonatomic) NSString *LMSSponsor;
@property (strong, nonatomic) NSString *LimitNoOfStkInWatchlist;
@property (assign, nonatomic) NSInteger LimitNoOfWatchlist;
@property (strong, nonatomic) NSString *LinkedInLink;
@property (assign, nonatomic) NSInteger LogoutCountdownTime;
@property (strong, nonatomic) NSArray *linksOnHomePage;

//============= M ==========
@property (assign, nonatomic) NSInteger MDLevel;
@property (assign, nonatomic) NSInteger MaxIdleTime;
@property (assign, nonatomic) NSInteger MaxNoOfStkInWatchlist;
@property (assign, nonatomic) NSInteger MaxNoOfWatchlist;
@property (assign, nonatomic) BOOL MarketStreamer;//addmore
@property (strong, nonatomic) NSString *MainTextColorLogin;

//========== N ================
@property (strong, nonatomic) NSString *N2NRegURL;
@property (strong, nonatomic) NSString *N2NSubspStsURL;
@property (strong, nonatomic) NSString *N2NSubspSvcURL;
@property (strong, nonatomic) NSString *NewsServer;
@property (strong, nonatomic) NSString *ElasticNewsServer;
@property (nonatomic, assign) BOOL EnabledNewiBillionaire;
@property (nonatomic, retain) NSArray *FilteriBillionaire;
@property (assign, nonatomic) NSInteger NoRetrySwitchExchg;


//====== O ===================
@property (assign, nonatomic) BOOL OpenAboutInBrowser;
@property (assign, nonatomic) BOOL OpenDWInBrowser;
@property (assign, nonatomic) BOOL OpenPPInBrowser;
@property (strong, nonatomic) NSString *OpenStockAlertKey;
@property (strong, nonatomic) NSString *OpenStockAlertURL;
@property (assign, nonatomic) BOOL OpenTOSInBrowser;
@property (strong, nonatomic) NSString *OrderHistoryLimit;
@property (strong, nonatomic) NSDictionary *OverrideExchangeName;

//======= P ============
@property (assign, nonatomic) BOOL PFDisclaimer;
@property (assign, nonatomic) BOOL PFDisclaimerWebView;
@property (assign, nonatomic) BOOL PFDisclaimerWebViewContainsTitle;
@property (strong, nonatomic) NSString *PFDisclaimerWebViewURL;
@property (strong, nonatomic) NSString *PPTextViewContent;
@property (strong, nonatomic) NSString *PPURL;
@property (strong, nonatomic) NSString *PrimaryExchg;
@property (strong, nonatomic) NSString *PushNotificationRegisterURL;
@property (strong, nonatomic) NSString *PlaceHolderColorLogin;


//======== Q ==============
@property (assign, nonatomic) NSInteger QCCompletionBlock;
@property (assign, nonatomic) NSInteger QCCompression;
@property (assign, nonatomic) NSInteger QCGetNewKey;
@property (strong, nonatomic) NSString *QCServer;

//=========== R ===================
@property (assign, nonatomic) BOOL RDS;
@property (strong, nonatomic) NSString *RDS_Sts_B_Title;
@property (strong, nonatomic) NSString *RDS_Sts_N_Title;
@property (assign, nonatomic) BOOL RegisterEnabled;
@property (strong, nonatomic) NSString *RegisterPushNotificationURL;
@property (assign, nonatomic) BOOL RememberMe;
@property (assign, nonatomic) BOOL RestrictionToViewMY;
@property (assign, nonatomic) BOOL ReverseBVolSVolOnBussDone;

//========== S ==============
@property (strong, nonatomic) NSString *SecondaryExchg;
@property (strong, nonatomic) NSString *ShareText;
@property (strong, nonatomic) NSString *StockAlertAuthKey;
@property (strong, nonatomic) NSString *StockAlertPageURL;

//========= T ===============
@property (strong, nonatomic) NSString *TOSTextViewContent;
@property (strong, nonatomic) NSString *TOSTitle;
@property (strong, nonatomic) NSString *TOSURL;
@property (strong, nonatomic) NSString *TermsNCondContentOnLoginPgCN;//Addmore
@property (strong, nonatomic) NSString *tvChart; //Addmore
@property (strong, nonatomic) NSString *TermsNCondContentOnLoginPg;
@property (assign, nonatomic) NSInteger TermsNCondOnLoginPg;
@property (assign, nonatomic) NSInteger TermsOfAccessOnLoginPg;
@property (strong, nonatomic) NSString *TermsOfAccessURL;
@property (assign, nonatomic) NSInteger TimeOut;
@property (strong, nonatomic) NSString *TradeValueLabelOnQuoteScreen;
@property (strong, nonatomic) NSString *TradedAtLabelOnTimeNSale;
@property (assign, nonatomic) NSInteger TrdLimitQtyDigitForKLExchg;
@property (strong, nonatomic) NSDictionary *TrdShortSellIndicator;
@property (assign, nonatomic) BOOL TrdSkipConfirmation;
@property (strong, nonatomic) NSString *TwitterLink;
@property (assign, nonatomic) BOOL TradingLimitEnabled; //Addmore
@property (strong, nonatomic) NSString *TouchIDFaceIDIcon;
@property (strong, nonatomic) NSString *TValueLabelOnQuoteScreen; //News: Same - TradeValueLabelOnQuoteScreen

//========== R ===================
@property (strong, nonatomic) NSString *Register2FAPgURL; //Addmore

//========== V =============
@property (strong, nonatomic) NSString *Version;
@property (strong, nonatomic) NSString *VertexServer;
@property (strong, nonatomic) NSString *WebsiteLink;
@property (strong, nonatomic) NSString *chartType;
@property (strong, nonatomic) NSString *currencyRateLabel;
@property (strong, nonatomic) NSString *emailContact;

@property (assign, nonatomic) BOOL isEnableQRLogin;
@property (assign, nonatomic) BOOL isEnableScreener;

@property (assign, nonatomic) BOOL msgPromptForStopNIfTouchOrdType;
@property (strong, nonatomic) NSString *phoneContact;
@property (assign, nonatomic) BOOL scrollableOrdPadInputView;

//=========== Some strange properties ==============================

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err;

@end
