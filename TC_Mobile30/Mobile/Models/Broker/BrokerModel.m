//
//  BrokerModel.m
//  TCiPad
//
//  Created by Kaka on 3/22/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "BrokerModel.h"

@implementation BrokerModel
+ (JSONKeyMapper *)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
																  @"appName":@"AppName",
																  @"brokerID":@"BrokerID",
																  @"bundleID":@"BundleID",
																  @"iconName": @"IconName",
																  @"plistLink": @"PlistLink",
																  @"tag": @"Tag",
																  @"enable": @"Enable"
										  }];
}
@end
