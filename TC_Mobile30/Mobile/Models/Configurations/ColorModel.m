//
//  ColorModel.m
//  TC_Mobile30
//
//  Created by Kaka on 5/11/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "ColorModel.h"

@implementation ColorModel
- (instancetype)init{
	self = [super init];
	if (self) {
		//Default
		self.mainTint = @"ffffff";
		self.tradeTint = @"ffffff";
	}
	return self;
}
@end
