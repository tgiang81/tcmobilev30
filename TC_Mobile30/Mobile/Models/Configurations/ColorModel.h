//
//  ColorModel.h
//  TC_Mobile30
//
//  Created by Kaka on 5/11/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "TCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ColorModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSString *mainTint;
@property (strong, nonatomic) NSString *tradeTint;

@end

NS_ASSUME_NONNULL_END
