//
//  MStockTrackerModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseModel.h"

@interface MStockTrackerModel : TCBaseModel
@property(strong, nonatomic) NSString *stockName;
@property(strong, nonatomic) NSString *stockCode;
@property(strong, nonatomic) NSString *quantity;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *bbh;
@property(strong, nonatomic) NSString *sbh;
@property(strong, nonatomic) NSString *value;
@property(strong, nonatomic) NSString *changeValuePercentageString;
@property(strong, nonatomic) NSDate *dateTraded;
@property(assign, nonatomic) float price;
@property(assign, nonatomic) float lacp;
@property(assign, nonatomic) float changeValue;
- (instancetype)initWith:(NSString *)stockName
                andPrice:(float)price
                 andLACP:(float)lacp
             andQuantity:(NSString *)quantity
            andCondition:(NSString *)status
                  andBBH:(NSString *)bbh
                  andSBH:(NSString *)sbh;
- (NSString *)getName;
- (NSString *)getDateString;
+ (MStockTrackerModel *)fromDict:(NSDictionary *)dict;
+ (NSMutableArray *)arrayFromDicts:(NSArray *)dictArr;
@end
