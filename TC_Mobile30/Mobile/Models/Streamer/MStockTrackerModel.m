//
//  MStockTrackerModel.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/1/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "MStockTrackerModel.h"
#import "NSDate+Utilities.h"
#import "SettingManager.h"
#import "NSNumber+Formatter.h"
@implementation MStockTrackerModel
- (instancetype)initWith:(NSString *)stockName
                andPrice:(float)price
                 andLACP:(float)lacp
             andQuantity:(NSString *)quantity
            andCondition:(NSString *)status
                  andBBH:(NSString *)bbh
                  andSBH:(NSString *)sbh{
    self = [super init];
    self.stockName = stockName;
    self.price = price;
    self.lacp = lacp;
    self.quantity = quantity;
    self.status = status;
    self.bbh = bbh;
    self.sbh = sbh;
    return self;
}
- (NSString *)getName{
//    if([SettingManager shareInstance].isVisibleNameStock == YES){
//        return self.stockName;
//    }else{
        return self.stockCode;
//    }
}
- (NSString *)getDateString{
//    @"hh:mma"
    NSString *date = [self.dateTraded stringWithFormat:@"HH:mm"];
    return date;
}
+ (MStockTrackerModel *)fromDict:(NSDictionary *)dict{
    MStockTrackerModel *model = [MStockTrackerModel new];
    if ([dict.allKeys containsObject:@"33"]) {
        model.stockCode = [NSString stringWithFormat:@"%@", [dict objectForKey:@"33"]];
        model.status = [NSString stringWithFormat:@"%@", [dict objectForKey:@"104"]];
        model.lacp = [[dict objectForKey:@"51"] floatValue];
        model.price = [[dict objectForKey:@"98"] floatValue];
        model.value =  [NSString stringWithFormat:@"%@", [dict objectForKey:@"100"]];
        model.changeValue = model.price - model.lacp;
        float changeValuePercentage = model.changeValue / model.lacp  * 100;
        model.changeValuePercentageString =  [[NSNumber numberWithFloat:changeValuePercentage] toPercentWithSign];
        int timeValue = [[dict objectForKey:@"54"] intValue];
        model.dateTraded = [NSDate dateWithTimeIntervalSince1970:timeValue];
        NSString *quantity = @"";
        SET_IF_NOT_NULL(quantity,[dict objectForKey:@"99"]);
        
        NSString *bbh_sbhArray = @"";
        
        //IF PH -> got 120
        SET_IF_NOT_NULL(bbh_sbhArray,[dict objectForKey:@"120"]);
        NSString *bbh = @"";
        NSString *sbh = @"";
        
        if (bbh_sbhArray.length!=0) {
            NSArray *arrTmp = [bbh_sbhArray componentsSeparatedByString:@","];
            
            if (arrTmp.count > 3) {
                bbh = arrTmp[1];
                sbh = arrTmp[3];
            }
        }
        
        model.quantity = [[NSNumber numberWithDouble:[quantity doubleValue]] abbreviateNumber];
        model.bbh = bbh;
        model.sbh = sbh;
    }
    return model;
    
}
+ (NSMutableArray *)arrayFromDicts:(NSArray *)dictArr{
    NSMutableArray *result = @[].mutableCopy;
    [dictArr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [result addObject:[self fromDict:obj]];
    }];
    return result;
}
@end
