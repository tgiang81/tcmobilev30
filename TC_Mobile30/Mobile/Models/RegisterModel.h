//
//  RegisterModel.h
//  TCiPad
//
//  Created by conveo cutoan on 3/23/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegisterModel : NSObject


@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *icNo;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *mobileNo;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *retypePassword;
@property (nonatomic, strong) NSString *hintType;
@property (nonatomic, strong) NSString *hint;
@property (nonatomic, strong) NSString *answer;

@end
