//
//  IndicesModel.h
//  TC_Mobile30
//
//  Created by Kaka on 9/18/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseModel.h"

@interface IndicesModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSString *name; //130
@property (strong, nonatomic) NSString *exStCode; // Include Name
@property (strong, nonatomic) NSNumber *prev; //Close Price
@property (strong, nonatomic) NSNumber *curPrice; //Last done
@property (strong, nonatomic) NSNumber *closePrice; //Close 
@property (strong, nonatomic) NSString *stkCode;
@property (strong, nonatomic) NSString *stkNo;
@property (strong, nonatomic) NSString *stockCode;// Code with last path extension

//Add more
@property (strong, nonatomic) NSNumber *vol;
@property (strong, nonatomic) NSNumber *val;
@property (strong, nonatomic) NSNumber *scoreOfTrd;
@property (strong, nonatomic) NSNumber *unTrd;
@property (strong, nonatomic) NSNumber *inChg;
@property (strong, nonatomic) NSNumber *up;
@property (strong, nonatomic) NSNumber *down;

@end
