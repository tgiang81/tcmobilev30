//
//  IndicesModel.m
//  TC_Mobile30
//
//  Created by Kaka on 9/18/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "IndicesModel.h"

@implementation IndicesModel
+ (JSONKeyMapper *)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
																  @"exStCode":FID_38_S_STOCK_NAME, @"name":@"130", @"prev":FID_50_D_CLOSE, @"curPrice": FID_98_D_LAST_DONE_PRICE,
																  @"stockCode":@"33",
																  @"closePrice":FID_50_D_CLOSE,
																  @"vol":FID_101_I_VOLUME,
																  @"val":FID_102_D_VALUE,
														   @"up":FID_105_I_SCORE_UP,
																  @"down":FID_106_I_SCORE_DW,
																  @"scoreOfTrd":FID_133_D_SCORE_OF_TRD,
																  @"unTrd":FID_108_I_SCORE_NOTRD,
																  @"inChg":FID_107_I_SCORE_UNCHG,
																  }];
	
}

//NSString *stockCode = [[_indicesValArray objectAtIndex:row]objectForKey:FID_38_S_STOCK_NAME];
//NSArray *splitStkCode = [stockCode componentsSeparatedByString:@"."];
//cellIndices.lblName.text = [splitStkCode objectAtIndex:0];
//
//[priceFormatter setRoundingMode: NSNumberFormatterRoundDown];
//
//cellIndices.lblPrev.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_indicesValArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue]]];
//cellIndices.lblCurrent.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue]]];
//CGFloat close = [[[_indicesValArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue];
//CGFloat lastDone = [[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
//CGFloat chg = lastDone - close ;
//cellIndices.lblChg.text = [[UserPrefConstants singleton] abbreviatePrice:chg];
@end
