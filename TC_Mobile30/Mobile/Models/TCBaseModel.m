//
//  CPBaseModel.m
//  SidebarDemo
//
//  Created by Kaka on 11/8/17.
//  Copyright © 2017 N2N Connect Bhd. All rights reserved.
//

#import "TCBaseModel.h"
#import "AppMacro.h"

@implementation TCBaseModel
- (void)printDescription {
	DLog(@"%@", [self toJSONString]);
}
	
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
	return YES;
}

@end
