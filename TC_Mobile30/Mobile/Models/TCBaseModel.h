//
//  CPBaseModel.h
//  SidebarDemo
//
//  Created by Kaka on 11/8/17.
//  Copyright © 2017 N2N Connect Bhd. All rights reserved.
//

#import "JSONModel.h"
#import "AppControl.h"
#import "AppConstants.h"
#import "QCConstants.h"
#import "AppMacro.h"
#import <UIKit/UIKit.h>


//#import "AllCommon.h"
//Custom Date
//static NSString *API_FORMAT_DATE = @"yyyy-MM-dd hh:mm:ss"; //"2017-12-26 04:08:21";
//@interface JSONValueTransformer (CustomNSDate)
//@end
//
//@implementation JSONValueTransformer (CustomTransformer)
//
//- (NSDate *)NSDateFromNSString:(NSString *)string
//{
//	NSDateFormatter *formatter = [NSDateFormatter new];
//	formatter.dateFormat = API_FORMAT_DATE;
//	return [formatter dateFromString:string];
//}
//
//- (NSString *)JSONObjectFromNSDate:(NSDate *)date
//{
//	NSDateFormatter *formatter = [NSDateFormatter new];
//	formatter.dateFormat = API_FORMAT_DATE;
//	return [formatter stringFromDate:date];
//}
//@end
@interface TCBaseModel : JSONModel
- (void)printDescription;
@end
