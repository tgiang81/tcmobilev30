//
//  AlertSettingModel.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "SettingAlertModel.h"

@implementation SettingAlertModel
- (NSString *)getName{
    if([self.type isEqualToString:@"FDP"]){
        return @"Price Alert";
    }else if([self.type isEqualToString:@"ODP"]){
        return @"Order Alert";
    }else if([self.type isEqualToString:@"FM"]){
        return @"When order is 'Fully Matched'";
    }else if([self.type isEqualToString:@"PM"]){
        return @"When order is 'Partially Matched'";
    }else if([self.type isEqualToString:@"C"]){
        return @"When order is 'Cancelled'";
    }else if([self.type isEqualToString:@"R"]){
        return @"When order is 'Rejected'";
    }else if([self.type isEqualToString:@"MKS"]){
        return @"When order is 'Market Summary'";
    }else if([self.type isEqualToString:@"ODS"]){
        return @"When order is 'Order Summary'";
    }
    return @"";
}

- (BOOL)checkEnable{
    _isEnable = [self.value isEqualToString:@"1"];
    return _isEnable;
}
- (void)setIsEnable:(BOOL)isEnable{
    if(isEnable){
        self.value = @"1";
    }else{
        self.value = @"0";
    }
    _isEnable = isEnable;
}
- (BOOL)isOrderAlert{
    return [self.type isEqualToString:@"ODP"];
}
- (BOOL)isPriceAlert{
    return [self.type isEqualToString:@"FDP"];
}
- (BOOL)checkChildrenEnable{
    for (SettingAlertModel *model in self.subAlerts) {
        if(model.isEnable){
            return YES;
        }
    }
    return NO;
}
@end
