//
//  AlertMediaType.h
//  Sample
//
//  Created by Nguyễn Văn Tú on 9/28/18.
//  Copyright © 2018 Nguyễn Văn Tú. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"
@interface AlertMediaType : TCBaseModel
@property (strong, nonatomic) NSString *value;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *Sts;
@end
