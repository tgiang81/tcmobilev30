//
//  AlertSettingModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingAlertModel : TCBaseModel
@property (strong, nonatomic) NSString *clicode;
@property (strong, nonatomic) NSString *created;
@property (strong, nonatomic) NSString *created_dt;
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *parentType;
@property (strong, nonatomic) NSString *updated;
@property (strong, nonatomic) NSString *updated_dt;
@property (strong, nonatomic) NSString *value;
@property (assign, nonatomic) BOOL isEnable;
@property (strong, nonatomic) NSMutableArray *subAlerts;
@property (assign, nonatomic) NSInteger level;
- (NSString *)getName;
- (BOOL)checkEnable;
- (BOOL)isOrderAlert;
- (BOOL)isPriceAlert;
- (BOOL)checkChildrenEnable;
@end

NS_ASSUME_NONNULL_END
