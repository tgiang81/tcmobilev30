//
//  AlertModel.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "AlertModel.h"
#import "LanguageManager.h"
#import "NSString+Util.h"
#import "LanguageKey.h"
//    alrtT
//    Alert Type
//    Stock Price = 98
//    Stock Volume = 101
//    Bid Price = 68
//    Ask Price = 88
//    Bid Volume = 58
//    Ask Volume = 78

//    compT
//    Compare Type
//    Absolute Value = 0
//    % Change = 1

//    Cond
//    Condition
//    “=” = 1
//    “>” = 2
//    “>=” = 3
//    “<” = 4
//    “<=” = 5
@implementation AlertModel
- (double)lstupdDouble{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    return [[formatter dateFromString:self.lstupd] timeIntervalSince1970];
}
- (NSString *)getConditionString{
    if([self.cdt isEqualToString:@"1"]){
        return [AlertModel getListCondition][1];
    }else if([self.cdt isEqualToString:@"2"]){
        return [AlertModel getListCondition][2];
    }
    else if([self.cdt isEqualToString:@"3"]){
        return [AlertModel getListCondition][3];
    }
    else if([self.cdt isEqualToString:@"4"]){
        return [AlertModel getListCondition][4];
    }
    else if([self.cdt isEqualToString:@"5"]){
        return [AlertModel getListCondition][5];
    }
    return [AlertModel getListCondition].firstObject;
}
+ (NSMutableArray *)getListCondition{
    return [NSMutableArray arrayWithArray:@[@"", @"Equal", @"Greater Than", @"Greater Than Or Equal", @"Less Than", @"Less Than Or Equal"]];
}
//    Stock Price = 98
//    Stock Volume = 101
//    Bid Price = 68
//    Ask Price = 88
//    Bid Volume = 58
//    Ask Volume = 78

+ (NSString *)getAlrtName:(NSString *)key{
    for(NSDictionary *tmp in [AlertModel getalrtTs]){
        if([[tmp allKeys].firstObject isEqualToString:key]){
            return [tmp allValues].firstObject;
        }
    }
    return @"";
}
+ (NSMutableArray *)getalrtTs{
    
    //    Stock Price = 98
    //    Stock Volume = 101
    //    Bid Price = 68
    //    Ask Price = 88
    //    Bid Volume = 58
    //    Ask Volume = 78
    
    return [NSMutableArray arrayWithArray:@[@{@"98": [LanguageManager stringForKey:@"Stock Price"]}, @{@"101": [LanguageManager stringForKey:@"Stock Volume"]}, @{@"68": [LanguageManager stringForKey:@"Bid Price"]}, @{@"88": [LanguageManager stringForKey:@"Ask Price"]}, @{@"58": [LanguageManager stringForKey:@"Bid Volume"]}, @{@"78": [LanguageManager stringForKey:@"Ask Volume"]}]];
}
+ (AlertModel *)copyModel:(AlertModel *)model{
    AlertModel *copyModel = [AlertModel new];
    copyModel.num = model.num;
    copyModel.id = model.id;
    copyModel.exStk = model.exStk;
    copyModel.stkCd = model.stkCd;
    copyModel.stkNm = model.stkNm;
    copyModel.alrtt = model.alrtt;
    copyModel.compt = model.compt;
    copyModel.cdt = model.cdt;
    copyModel.lmt = model.lmt;
    copyModel.s = model.s;
    copyModel.rmk = model.rmk;
    copyModel.lstupd = model.lstupd;
    copyModel.ex = model.ex;
    copyModel.bh = model.bh;
    copyModel.mdn = model.mdn;
    copyModel.lsUpdDt = model.lsUpdDt;
    return copyModel;
}
+ (NSMutableArray *)getalrtTNames{
    NSMutableArray *names = [NSMutableArray new];
    for(NSDictionary *tmp in [AlertModel getalrtTs]){
        [names addObject:[tmp allValues].firstObject];
    }
    return names;
}
+ (NSString *)getComTName:(NSString *)key{
    for(NSDictionary *tmp in [AlertModel getComTs]){
        if([[tmp allKeys].firstObject isEqualToString:key]){
            return [tmp allValues].firstObject;
        }
    }
    return @"";
}
- (void)setAlertTIndex:(NSInteger)alertTIndex{
    if(alertTIndex != -1){
        _alertTIndex = alertTIndex;
        self.alrtt = [[AlertModel getalrtTs][alertTIndex] allKeys].firstObject;
    }
    
}
- (void)setComTIndex:(NSInteger)comTIndex{
    if(comTIndex != -1){
        _comTIndex = comTIndex;
        self.compt = [[AlertModel getComTs][comTIndex] allKeys].firstObject;
    }
    
}
- (NSString *)getMdnName{
    if([self.mdn isEqualToString:@"E"] || [self.mdn isEqualToString:@"1"]){
        return [LanguageManager stringForKey:_Email];
    }else if([self.mdn isEqualToString:@"S"] || [self.mdn isEqualToString:@"2"]){
        return [LanguageManager stringForKey:SMS];
    }
    return [LanguageManager stringForKey:PushNotifications];
}
- (BOOL)isValidAlert{
    return ([self.alrtt isValidString] && [self.compt isValidString] && [self.lmt isValidString] && [self.cdt isValidString] && [self.mdn isValidString] && [self.ex isValidString]) ;
}
+ (NSMutableArray *)getComTs{
    return [NSMutableArray arrayWithArray:@[@{@"0": @"Absolute Value"}, @{@"1": @"% Change"}]];
}
+ (NSMutableArray *)getComTNames{
    NSMutableArray *names = [NSMutableArray new];
    for(NSDictionary *tmp in [AlertModel getComTs]){
        [names addObject:[tmp allValues].firstObject];
    }
    return names;
}
//    compT
//    Compare Type
//    Absolute Value = 0
//    % Change = 1
@end
