//
//  SettingAlertWrapModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/19/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingAlertWrapModel : TCBaseModel
@property (strong, nonatomic) NSString *defPricePush;
@property (strong, nonatomic) NSString *ltUpdDt;
@property (strong, nonatomic) NSString *alertSettings;
@property (strong, nonatomic) NSArray *setLs;
@property (strong, nonatomic) NSString *defOrderPush;
@property (strong, nonatomic) NSString *defMarketPush;
@property (strong, nonatomic) NSArray *orderItems;

@end

NS_ASSUME_NONNULL_END
