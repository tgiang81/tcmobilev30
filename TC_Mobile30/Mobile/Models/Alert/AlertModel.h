//
//  AlertModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseModel.h"
@class AlertModel;
@interface AlertModel : TCBaseModel
@property (strong, nonatomic) NSString *num;
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *exStk;
@property (strong, nonatomic) NSString *stkCd;
@property (strong, nonatomic) NSString *stkNm;
@property (strong, nonatomic) NSString *alrtt;
@property (strong, nonatomic) NSString *compt;
@property (strong, nonatomic) NSString *cdt;
@property (strong, nonatomic) NSString *lmt;
@property (strong, nonatomic) NSString *s;
@property (strong, nonatomic) NSString *rmk;
@property (strong, nonatomic) NSString *lstupd;
@property (assign, nonatomic) double lstupdDouble;
@property (strong, nonatomic) NSString *ex;
@property (strong, nonatomic) NSString *bh;
@property (strong, nonatomic) NSString *mdn;
@property (strong, nonatomic) NSString *lsUpdDt;
@property (assign, nonatomic) NSInteger alertTIndex;
@property (assign, nonatomic) NSInteger comTIndex;

- (BOOL)isValidAlert;
- (NSString *)getConditionString;
+ (NSMutableArray *)getListCondition;
+ (NSMutableArray *)getalrtTNames;
+ (NSMutableArray *)getalrtTs;
+ (NSMutableArray *)getComTNames;
+ (NSString *)getComTName:(NSString *)key;
+ (NSString *)getAlrtName:(NSString *)key;
+ (AlertModel *)copyModel:(AlertModel *)model;
- (NSString *)getMdnName;
@end
