//
//  ControllerModel.h
//  TCiPad
//
//  Created by Kaka on 7/24/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface ControllerModel : TCBaseModel
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (assign, nonatomic) float heightContent;
@end
