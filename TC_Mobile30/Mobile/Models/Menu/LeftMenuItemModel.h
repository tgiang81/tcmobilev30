//
//  LeftMenuItemModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface LeftMenuItemModel : TCBaseModel
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSArray *subItems;//maybe use later
@property(strong, nonatomic) NSNumber *menuId;
@property(assign, nonatomic) BOOL enable;
@property(strong, nonatomic) NSString *image;
@property (nonatomic, assign) BOOL isHidden;
@end

NS_ASSUME_NONNULL_END
