//
//  LeftMenuModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/27/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"
#import "LeftMenuItemModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MenuSectionModel : TCBaseModel
@property(strong, nonatomic) NSString *name;
@property (nonatomic) NSArray *items;
@property (nonatomic, assign) BOOL isHidden;

@end

NS_ASSUME_NONNULL_END
