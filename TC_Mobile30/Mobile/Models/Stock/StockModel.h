//
//  StockModel.h
//  TCiPad
//
//  Created by Kaka on 3/30/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"
@class TradeStatus;
@interface StockModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSNumber *trade;
@property (strong, nonatomic) NSNumber *volume; //Double - dynamic 1 volume
@property (strong, nonatomic) NSNumber *value;
@property (strong, nonatomic) NSNumber *highPrice;
@property (strong, nonatomic) NSNumber *lowPrice;
@property (strong, nonatomic) NSNumber *openPrice;
@property (strong, nonatomic) NSNumber *parValue;

@property (strong, nonatomic) NSNumber *fChange; //Change
@property (strong, nonatomic) NSNumber *changePer; //Perchange
@property (strong, nonatomic) NSString *stockName;
@property (strong, nonatomic) NSNumber *iTrd;
@property (strong, nonatomic) NSNumber *dValue; //Double
@property (strong, nonatomic) NSNumber *dClose; //Double
@property (strong, nonatomic) NSNumber *lastDonePrice; //Double - lacp : Last done price - Why ipad use FID_51_D_REF_PRICE value ?
@property (strong, nonatomic) NSNumber *refPrice;
@property (strong, nonatomic) NSString *symbolComp;
@property (strong, nonatomic) NSString *symbolSectionName;
@property (strong, nonatomic) NSString *symbol2;


@property (strong, nonatomic) NSNumber *floorPrice;
@property (strong, nonatomic) NSNumber *iBuyQTY1; //Double
@property (strong, nonatomic) NSString *stockCode;
@property (strong, nonatomic) NSString *compName;

@property (assign, nonatomic) double idssVol;
@property (assign, nonatomic) double idssVa;
@property (strong, nonatomic) NSString *idssTypeString;
@property (strong, nonatomic) NSString *idssTypeCode;
@property (strong, nonatomic) NSString *lotSize;

- (instancetype)initStockFrom:(TradeStatus *)ts;
- (NSString *)changePercentValue;
@end
