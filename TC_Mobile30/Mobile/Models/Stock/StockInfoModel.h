//
//  StockInfoModel.h
//  TCiPad
//
//  Created by Kaka on 7/2/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface StockInfoModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSString *stockCode;
@property (strong, nonatomic) NSString *compName;
@property (strong, nonatomic) NSString *stockName;
@property (strong, nonatomic) NSString *exchange;
@property (strong, nonatomic) NSString *fullName;

//Number - long - abbreviate Number
@property (strong, nonatomic) NSNumber *totalVol;
@property (strong, nonatomic) NSNumber *totalBuyVol;
@property (strong, nonatomic) NSNumber *totalSellVol;
@property (strong, nonatomic) NSNumber *totalBuyTran;
@property (strong, nonatomic) NSNumber *totalSellTran;
@property (strong, nonatomic) NSNumber *totalMktCap;
@property (strong, nonatomic) NSNumber *totalShareIssued;
@property (strong, nonatomic) NSNumber *totalShortSellVol;
@property (strong, nonatomic) NSNumber *idssTotVol; //Key ?//Parse from API
@property (strong, nonatomic) NSNumber *idssTotVal; //Key? //Parse from API
@property (strong, nonatomic) NSString *idssTypeString;

@property (strong, nonatomic) NSNumber *trade;
@property (strong, nonatomic) NSNumber *volume; //Double - dynamic 1 volume
@property (strong, nonatomic) NSNumber *value;
@property (strong, nonatomic) NSNumber *highPrice;
@property (strong, nonatomic) NSNumber *lowPrice;
@property (strong, nonatomic) NSNumber *openPrice;

@property (strong, nonatomic) NSNumber *intradayMktAvgPrice;
@property (strong, nonatomic) NSNumber *avgVolume_10days;//Key?

@property (strong, nonatomic) NSNumber *BETA;//Key?
@property (strong, nonatomic) NSNumber *ROA;//Key?
@property (strong, nonatomic) NSNumber *ROE;//Key?
@property (strong, nonatomic) NSNumber *p_b;//Like P_BKey?
@property (strong, nonatomic) NSNumber *OI;//Key? - KL mở
@property (strong, nonatomic) NSNumber *basis;//Key? - Độ lệch

@property (strong, nonatomic) NSNumber *fChange; //Change
@property (strong, nonatomic) NSNumber *changePer; //Perchange

@property (strong, nonatomic) NSNumber *iTrd;
@property (strong, nonatomic) NSNumber *dValue; //Double
@property (strong, nonatomic) NSNumber *closePrice; //Double same: close
@property (strong, nonatomic) NSNumber *lastDonePrice; //Double -
//Number - float - toString
@property (strong, nonatomic) NSNumber *parValue;
@property (strong, nonatomic) NSNumber *peRatio; //FID_138_S_WINNING

@property (strong, nonatomic) NSNumber *nta; //FID_113_I_REG_NO
@property (strong, nonatomic) NSNumber *dividendYield; //Key ??

@property (strong, nonatomic) NSNumber *wks_52_Low; //FID_57_D_LOW_PRICE
@property (strong, nonatomic) NSNumber *wks_52_High; //FID_56_D_HIGH_PRICE
@property (strong, nonatomic) NSNumber *wks_52_Spread; //Key ?

//Price - float - Price number
@property (strong, nonatomic) NSNumber *lacpFloat; //currency format: FID_153_D_THEORETICAL_PRICE for ABacus - ||| Other-- FID_51_D_REF_PRICE
@property (strong, nonatomic) NSNumber *refPrice; //FID_51_D_REF_PRICE
@property (strong, nonatomic) NSNumber *theoreticalPrice; //FID_153_D_THEORETICAL_PRICE

@property (strong, nonatomic) NSNumber *open; //currency format
@property (strong, nonatomic) NSNumber *close; //currency format
@property (strong, nonatomic) NSNumber *ceil; //currency format
@property (strong, nonatomic) NSNumber *floor; //currency format
@property (strong, nonatomic) NSNumber *bid; //currency format
@property (strong, nonatomic) NSNumber *ask; //currency format
@property (strong, nonatomic) NSNumber *eps;
@property (strong, nonatomic) NSNumber *foreignLimit;
@property (strong, nonatomic) NSNumber *foreignShare; //FID_191_I_BO_QTY_2
//Add More
@property (strong, nonatomic) NSNumber *remainForeign;//Key?
@property (strong, nonatomic) NSNumber *foreignBuy;//Key?
@property (strong, nonatomic) NSNumber *foreignSell;//Key?
@property (strong, nonatomic) NSNumber *underlying;//Key?
@property (strong, nonatomic) NSNumber *underlyingPrice;//Key? - Giá TS cơ sở


@property (strong, nonatomic) NSNumber *ownerLimit;
@property (strong, nonatomic) NSNumber *buyRate;
@property (strong, nonatomic) NSNumber *mktAVGPrice; //Key



//Number - long - abbreviate Number
@property (strong, nonatomic) NSNumber *tradesVal;
@property (strong, nonatomic) NSNumber *bidQtyValue;
@property (strong, nonatomic) NSNumber *askQtyValue;

//String
@property (strong, nonatomic) NSString *sectorName; //at 4 position
@property (strong, nonatomic) NSString *tradingBoard; //at 3 position
@property (strong, nonatomic) NSString *ISINString;
@property (strong, nonatomic) NSString *lotSize;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *indices;
@property (strong, nonatomic) NSString *remark;
@property (strong, nonatomic) NSString *currency;
@property (strong, nonatomic) NSString *cpf; //Key? maybe is lacpFloat
@property (strong, nonatomic) NSString *deliveryBasis;
@property (strong, nonatomic) NSString *daySpread;
@property (strong, nonatomic) NSString *type;

@property (strong, nonatomic) NSString *expiryDate;//49 - Ngày đáo hạn


//Main
- (instancetype)initWithStkCode:(NSString *)stockCode;


//@"Volume",FID_101_I_VOLUME,
//@"High",FID_56_D_HIGH_PRICE,
//@"Low",FID_57_D_LOW_PRICE,
//@"Last",FID_98_D_LAST_DONE_PRICE,
//@"L-Done",FID_99_I_LAST_DONE_LOT,
//@"LACP",FID_51_D_REF_PRICE,
//@"Open",FID_55_D_OPEN_PRICE,
//@"Par",FID_123_D_PAR_VALUE,
//@"EPS",FID_127_D_EPS,
//@"Stock Name",FID_38_S_STOCK_NAME,
//@"Stock Code",FID_33_S_STOCK_CODE,
//@"Company Name",FID_39_S_COMPANY,
//@"Sector",FID_37_S_SECTOR_NAME,
//@"Prev Close",FID_50_D_CLOSE,
////Added trades/tradeval/top Dec2016
//@"Trades",FID_132_I_TOT_OF_TRD,
//@"TradeVal",FID_102_D_VALUE,
//@"Top",FID_153_D_THEORETICAL_PRICE,
//@"Change%",FID_CUSTOM_F_CHANGE_PER,
//@"Change",FID_CUSTOM_F_CHANGE,
//@"Currency",FID_134_S_CURRENCY,
//// Bid/Ask
//@"BuyQuantity1",FID_58_I_BUY_QTY_1,
//@"BuyPrice1",FID_68_D_BUY_PRICE_1,
//@"SellQuantity1",FID_78_I_SELL_QTY_1,
//@"SellPrice1",FID_88_D_SELL_PRICE_1,
//@"Trns_No",FID_103_I_TRNS_NO,
//@"TotalBuyVol",FID_238_I_TOTAL_BVOL,
//@"TotalSellVol",FID_237_I_TOTAL_SVOL,
//@"TotalBuyTrans",FID_241_I_TOTAL_BTRANS,
//@"TotalSellTrans",FID_242_I_TOTAL_STRANS,
//@"M-Cap",FID_100004_F_MKT_CAP,
//@"ShareIssued",FID_41_D_SHARES_ISSUED,
//@"TradingPhase",FID_156_S_TRD_PHASE,
//@"Stock Name CN",FID_130_S_SYSBOL_2,
//@"Stock Sector CN",FID_158_S_SYSBOL2_SECNAME,
//@"Stock Comp CN",FID_159_S_SYSBOL2_COMP,
//@"Stock Remarks",FID_135_S_REMARK,
//nil];
@end
