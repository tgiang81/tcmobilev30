//
//  BusinessDoneModel.h
//  TCiPad
//
//  Created by Kaka on 7/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface BusinessDoneModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSNumber *price;
@property (assign, nonatomic) float buyPercent;
@property (strong, nonatomic) NSNumber *sVol;
@property (strong, nonatomic) NSNumber *bVol;
@property (strong, nonatomic) NSNumber *tVol;
@property (strong, nonatomic) NSNumber *tVal;
@end
