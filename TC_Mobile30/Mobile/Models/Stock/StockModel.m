//
//  StockModel.m
//  TCiPad
//
//  Created by Kaka on 3/30/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockModel.h"
#import "TradeStatus.h"
#import "NSNumber+Formatter.h"
//FID_48_S_STATUS
@implementation StockModel
+ (JSONKeyMapper *)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"lotSize":FID_40_I_SHARE_PER_LOT,
                                                                  @"idssTypeCode": FID_48_S_STATUS,
                                                                  @"trade":FID_132_I_TOT_OF_TRD,
																  @"volume":@"101", @"fChange":FID_CUSTOM_F_CHANGE, @"changePer":FID_CUSTOM_F_CHANGE_PER, @"stockName": @"38", @"compName" : FID_39_S_COMPANY, @"iTrd": @"132", @"dValue": @"102",
																   @"dClose": @"50",
																   @"lastDonePrice": FID_98_D_LAST_DONE_PRICE,
																  @"refPrice":FID_51_D_REF_PRICE,
																  @"iBuyQTY1": @"58", @"stockCode":@"33", @"highPrice":@"56", @"lowPrice":@"57", @"openPrice":@"55", @"parValue":@"123", @"symbolComp": FID_159_S_SYSBOL2_COMP, @"symbolSectionName": FID_158_S_SYSBOL2_SECNAME, @"symbol2": FID_130_S_SYSBOL_2,
																  @"nta": FID_113_I_REG_NO
																   }];

}

- (instancetype)initStockFrom:(TradeStatus *)ts{
    self = [super init];
    if(self){
        _stockName = ts.stock_name;
        _stockCode = ts.stock_code;
    }
    return self;
}
- (NSString *)changePercentValue{
    CGFloat changePoint = 3;
    if ([self.fChange doubleValue]>0) {
        return [NSString stringWithFormat:@"+%@ (+%.2f%%)",[self.fChange toDecimaWithPointer:changePoint],[self.changePer doubleValue]];
    } else{
        return [NSString stringWithFormat:@"%@ (%.2f%%)",[self.fChange toDecimaWithPointer:changePoint],[self.changePer doubleValue]];
    }
}
//if ([dictToAdd objectForKey:FID_101_I_VOLUME]==nil) {
//[dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_101_I_VOLUME];
//}
//
//// gainer/loser
//if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE]==nil) {
//	[dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE];
//}
//
//// gainer/loser %
//if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE_PER]==nil) {
//	[dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE_PER];
//}
//
//// name
//if ([dictToAdd objectForKey:FID_38_S_STOCK_NAME]==nil) {
//	[dictToAdd setObject:@"-" forKey:FID_38_S_STOCK_NAME];
//}
//
//// trades
//if ([dictToAdd objectForKey:FID_132_I_TOT_OF_TRD]==nil) {
//	[dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_132_I_TOT_OF_TRD];
//}
//
////value
//if ([dictToAdd objectForKey:FID_102_D_VALUE]==nil) {
//	[dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_102_D_VALUE];
//}
//
////close
//if ([dictToAdd objectForKey:FID_50_D_CLOSE]==nil) {
//	[dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_50_D_CLOSE];
//}
//
////lacp
//if ([dictToAdd objectForKey:FID_51_D_REF_PRICE]==nil) {
//	[dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_51_D_REF_PRICE];
//}
//
//// bid qty
//if ([dictToAdd objectForKey:FID_58_I_BUY_QTY_1]==nil) {
//	[dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_58_I_BUY_QTY_1];
//}
//dynamicFID1 =FID_101_I_VOLUME; - volume
//dynamicFID2 =FID_56_D_HIGH_PRICE; - HighPrice
//dynamicFID3 =FID_57_D_LOW_PRICE;
//dynamicFID4 =FID_98_D_LAST_DONE_PRICE;
//dynamicFID5 =FID_55_D_OPEN_PRICE;
//dynamicFID6 =FID_123_D_PAR_VALUE;
//dynamicFID7 =FID_127_D_EPS;
//dynamicFID8 =FID_CUSTOM_F_CHANGE_PER;
//dynamicFID9 =FID_CUSTOM_F_CHANGE;
//dynamicFID10 =FID_78_I_SELL_QTY_1;
//dynamicFID11 =FID_88_D_SELL_PRICE_1;
//dynamicFID12 =FID_68_D_BUY_PRICE_1;
//dynamicFID13 =FID_58_I_BUY_QTY_1;
//dynamicFID14 =FID_50_D_CLOSE;
//dynamicFID15 =FID_153_D_THEORETICAL_PRICE;
//dynamicFID16 = FID_156_S_TRD_PHASE;
//dynamicFID17 = FID_102_D_VALUE;
//dynamicFID18 = FID_131_S_EXCHANGE;
//dynamicFID19 = FID_135_S_REMARK;
//
//101 = 31831600;
//
//102 = "27849354.5";
//
//103 = 2454;
//
//123 = 0;
//
//127 = "0.109";
//
//130 = "WCT\U5de5\U7a0b";
//
//132 = 2455;
//
//134 = MYR;
//
//153 = "0.88";
//
//156 = CNT2;
//
//237 = 16401000;
//
//238 = 14437000;
//
//241 = 1168;
//
//242 = 1207;
//
//33 = "9679.KL";
//
//38 = "WCT.KL";
//
//39 = "WCT HOLDINGS BERHAD";
//
//41 = 1415581871;
//
//50 = "0.9";
//
//51 = "0.9";
//
//55 = "0.9";
//
//56 = "0.905";
//
//57 = "0.835";
//
//58 = 32200;
//
//68 = "0.84";
//
//78 = 58700;
//
//88 = "0.845";
//
//98 = "0.84";
//
//99 = 67800;
//
//change = "-0.06";
//
//changePer = "-6.667";
@end
