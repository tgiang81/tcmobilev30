//
//  RatingModel.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/29/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "RatingModel.h"

@implementation RatingModel
- (instancetype)initWithDic:(NSDictionary *)dic{
    self = [super init];
    if(self){
        self.riskLevel = [dic valueForKey:@"x153"];
        self.stockCode = [dic valueForKey:@"33"];
        self.rating = [dic valueForKey:@"x152"];
    }
    return self;
}
- (BOOL)isShow{
    return (self.rating.length !=0 && [self.rating intValue]!=-1);
}
- (UIImage *)getImage{
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@_point_rating_icon",self.rating]];
}
@end
