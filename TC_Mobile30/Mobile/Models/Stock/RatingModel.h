//
//  RatingModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/29/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "TCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RatingModel : NSObject
@property (strong, nonatomic) NSString *stockCode;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *riskLevel;

- (instancetype)initWithDic:(NSDictionary *)dic;
-(BOOL)isShow;
-(UIImage *)getImage;
@end

NS_ASSUME_NONNULL_END
