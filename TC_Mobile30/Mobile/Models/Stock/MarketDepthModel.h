//
//  MarketDepthModel.h
//  TCiPad
//
//  Created by Kaka on 7/12/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface MarketDepthModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSString *buySplit1;
@property (strong, nonatomic) NSNumber *buyPrice;
@property (strong, nonatomic) NSNumber *buyQty;

@property (strong, nonatomic) NSString *sellSplit1;
@property (strong, nonatomic) NSNumber *sellPrice; //Ask
@property (strong, nonatomic) NSNumber *sellQty; //AskQty

@end
