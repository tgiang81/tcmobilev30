//
//  StockInfoModel.m
//  TCiPad
//
//  Created by Kaka on 7/2/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "StockInfoModel.h"
#import "QCData.h"
#import "UserPrefConstants.h"

@implementation StockInfoModel
+ (JSONKeyMapper *)keyMapper
{
	return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
																  @"sectorName": FID_37_S_SECTOR_NAME,
																  @"indices": FID_100002_S_INDICES,
																  @"stockCode": FID_33_S_STOCK_CODE,
																  @"exchange": FID_131_S_EXCHANGE,
																  @"compName": FID_39_S_COMPANY,
																  @"totalVol": FID_101_I_VOLUME,
																  @"totalBuyVol": FID_238_I_TOTAL_BVOL,
																  @"totalSellVol": FID_237_I_TOTAL_SVOL,
																  @"totalBuyTran": FID_241_I_TOTAL_BTRANS,
																  @"totalSellTran": FID_242_I_TOTAL_STRANS,
																  @"totalMktCap": FID_100004_F_MKT_CAP,
																  @"totalShareIssued": FID_41_D_SHARES_ISSUED,
																  @"totalShortSellVol": FID_239_D_TOTAL_SHORT_SELL_VOL,
																  @"trade": FID_132_I_TOT_OF_TRD,
																  @"volume": FID_101_I_VOLUME,
																  @"value": FID_102_D_VALUE,
																  @"highPrice": FID_56_D_HIGH_PRICE,
																  @"lowPrice": FID_57_D_LOW_PRICE,
																  @"openPrice": FID_55_D_OPEN_PRICE,
																  @"parValue": FID_123_D_PAR_VALUE,
																  @"fChange": FID_CUSTOM_F_CHANGE,
																  @"changePer": FID_CUSTOM_F_CHANGE_PER,
																  @"stockName": FID_38_S_STOCK_NAME,
																  @"iTrd": FID_132_I_TOT_OF_TRD,
																  @"dValue": FID_102_D_VALUE,
																  @"dClose": FID_50_D_CLOSE,
																  @"lastDonePrice": FID_98_D_LAST_DONE_PRICE,
																  @"peRatio": FID_138_S_WINNING,
																  @"nta": FID_113_I_REG_NO,
																  @"wks_52_Low": FID_57_D_LOW_PRICE,
																  @"wks_52_High": FID_56_D_HIGH_PRICE,
																  @"refPrice": FID_51_D_REF_PRICE,
																  @"theoreticalPrice": FID_153_D_THEORETICAL_PRICE,
																  @"open": FID_55_D_OPEN_PRICE,
																  @"close": FID_50_D_CLOSE,
																  @"ceil": FID_232_D_CEILING_PRICE,
																  @"floor": FID_231_D_FLOOR_PRICE,
																  @"bid": FID_68_D_BUY_PRICE_1,
																  @"ask": FID_88_D_SELL_PRICE_1,
																  @"eps": FID_127_D_EPS,
																  @"foreignLimit": FID_155_S_FOREIGN_LIMIT,
																  @"foreignShare": FID_191_I_BO_QTY_2,
																  @"foreignBuy":
																	FID_235_D_FOREIGN_BVOL,
																  @"foreignSell":
																	FID_236_D_FOREIGN_SVOL,
																  @"ownerLimit": FID_CUSTOM_FOREIGN_OWNERSHIP,
																  @"buyRate": FID_CUSTOM_F_BUY_RATE,
																  @"remark": FID_135_S_REMARK,
																  @"tradesVal":FID_132_I_TOT_OF_TRD,
																  @"bidQtyValue":FID_58_I_BUY_QTY_1,
																  @"askQtyValue":FID_78_I_SELL_QTY_1,
																  @"tradingBoard":FID_CUSTOM_S_PATH_NAME,
																  @"ISINString": FID_157_S_ISIN,
																  @"lotSize":FID_40_I_SHARE_PER_LOT,
																  @"status":FID_48_S_STATUS,
																  @"fullName":FID_39_S_COMPANY,
																  @"currency":FID_134_S_CURRENCY,
																  @"daySpread":FID_119_S_TEXT_TYPE,
																  @"underlying":FID_244_S_UNDERLYING
																  }];
	
}

- (instancetype)initWithStkCode:(NSString *)stockCode{
//	if (([[[QCData singleton] qcFeedDataDict] objectForKey:stockCode]) && ([stockCode length] > 0)) {
//		if ([[[[QCData singleton] qcFeedDataDict] allKeys] containsObject:stockCode]) {
//			NSDictionary *stockDetail = [[[QCData singleton] qcFeedDataDict]objectForKey:stockCode];
//			return [[StockInfoModel alloc] initWithDictionary:stockDetail error:nil];
//		}
//	}
//	return nil;
	self = [super init];
	if (self) {
		QCData *myQcData = [QCData singleton];
        NSString *myBundleIdentifier = BUNDLEID_HARDCODE_TESTING;
		NSDictionary *dictData = myQcData.qcFeedDataDict[self.stockCode];//[[myQcData qcFeedDataDict]objectForKey:self.stockCode];
		DLog(@"==== Dict Info Stock: %@", dictData)
		
		self.stockCode = stockCode;
		self.compName = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode] objectForKey:FID_39_S_COMPANY];
		self.exchange = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode] objectForKey:FID_131_S_EXCHANGE];
		
		self.fullName = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_39_S_COMPANY];//FID_38_S_STOCK_NAME ?? FID_39_S_COMPANY
		self.indices = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_100002_S_INDICES];
		self.currency = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_134_S_CURRENCY];
		self.remark = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_135_S_REMARK];
		self.eps = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_127_D_EPS]doubleValue]];
		
		self.foreignShare = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_191_I_BO_QTY_2]doubleValue]];
		self.foreignBuy = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_235_D_FOREIGN_BVOL]doubleValue]];
		self.foreignSell = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_236_D_FOREIGN_SVOL]doubleValue]];
		
		self.foreignLimit = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_155_S_FOREIGN_LIMIT]doubleValue]];
		self.ownerLimit = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_CUSTOM_FOREIGN_OWNERSHIP]doubleValue]];
		
		self.buyRate = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_CUSTOM_F_BUY_RATE]doubleValue]];
		self.nta = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_113_I_REG_NO]doubleValue]];
		self.peRatio = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_138_S_WINNING]doubleValue]];
		self.wks_52_Low = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_57_D_LOW_PRICE]doubleValue]];
		self.wks_52_High = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_56_D_HIGH_PRICE]doubleValue]];
		
		self.totalVol = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_101_I_VOLUME]longLongValue]];
		self.volume = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_101_I_VOLUME]longLongValue]];
		self.value = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_102_D_VALUE]longLongValue]];
		
		self.totalBuyVol = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_238_I_TOTAL_BVOL]longLongValue]];
		self.totalSellVol = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_237_I_TOTAL_SVOL]longLongValue]];
		self.totalBuyTran = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_241_I_TOTAL_BTRANS]longLongValue]];
		self.totalSellTran = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_242_I_TOTAL_STRANS]longLongValue]];
		self.totalMktCap = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_100004_F_MKT_CAP]longLongValue]];
		self.totalShareIssued = [NSNumber numberWithLongLong: [[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_41_D_SHARES_ISSUED]longLongValue]];
		self.totalShortSellVol = [NSNumber numberWithLongLong: [[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]longLongValue]];
		
		
		self.lastDonePrice = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue]];
		self.changePer = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue]];
		self.fChange = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_CUSTOM_F_CHANGE] floatValue]];
		CGFloat lacpFloat = 0;
		if ([myBundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [myBundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
			lacpFloat = [[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_153_D_THEORETICAL_PRICE]floatValue];
		}else{
			lacpFloat = [[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
		}
		self.lacpFloat = [NSNumber numberWithFloat:lacpFloat];
		
		self.open = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_55_D_OPEN_PRICE] floatValue]];
		
		self.close = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_50_D_CLOSE] floatValue]];
		
		self.highPrice = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_56_D_HIGH_PRICE] floatValue]];
		self.lowPrice = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_57_D_LOW_PRICE] floatValue]];
		
		self.ceil = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]];
		self.floor = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]];
	
		self.bid = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue]];
		self.ask = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue]];
		self.tradesVal = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_132_I_TOT_OF_TRD]longLongValue]];
		self.bidQtyValue = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_58_I_BUY_QTY_1]longLongValue]];
		self.askQtyValue = [NSNumber numberWithLongLong:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_78_I_SELL_QTY_1]longLongValue]];
		
		self.type = [[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_119_S_TEXT_TYPE] stringValue];
		//Path name
		NSString *pathName = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_CUSTOM_S_PATH_NAME];
		//Sector name
		self.sectorName = [[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
		self.sectorName = [[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_37_S_SECTOR_NAME];
		if (!self.sectorName || self.sectorName.length == 0) {
			self.sectorName = [self getSectorNameFrom:pathName];
		}
		//TradingBoard
		self.tradingBoard = [self getTradingBoardFrom:pathName];
		self.totalShortSellVol = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL] doubleValue]];
		self.ISINString = [NSString stringWithFormat:@"%@",[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_157_S_ISIN]];
		self.parValue = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_123_D_PAR_VALUE] floatValue]];
		self.lotSize = [NSString stringWithFormat:@"%@",[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_40_I_SHARE_PER_LOT]];
		unichar uc = (unichar)[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_48_S_STATUS] characterAtIndex:1];
		self.status = [[UserPrefConstants singleton] getBoardStatus:[NSString stringWithCharacters:&uc length:1]];

        self.idssTypeString = [UserPrefConstants getIDSSStatus:[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_48_S_STATUS]];
		//if (([_totalShortSellVol length]<=0)||[_totalShortSellVol containsString:@"null"]) _totalShortSellVol = @"-";
		if (([_ISINString length]<=0)||[_ISINString containsString:@"null"]) _ISINString = @"-";
		
		/*Status = FID_44_S_STATE
		 3. Trading Board = FID_154_S_GROUP
		 4. Total S.Sell Volume = "239"
		 5. Lot. Size = "40"
		 6. ISIN = "157"
		 7. Foreign Ownership = "666191"
		 8. Par = "123"
		 9. Value Traded = ? It is "108" or "133"
		 10. Delivery Basis = ?
		 */
		
		//9 Apr 2015. Sometimes feed will return negative value...
		//Nov 2016 - feed not return negative! but it was out of range must use long long for "volumes" data
		
		if (_totalMktCap.floatValue <= 0) {
			_totalMktCap = [NSNumber numberWithFloat:([_totalShareIssued floatValue] * [_lastDonePrice floatValue])];
			if ([_lastDonePrice floatValue] <= 0) {
				_totalMktCap = [NSNumber numberWithFloat:([_totalShareIssued floatValue] * [_lacpFloat floatValue])];
			}
		}
		
		self.underlying = [NSNumber numberWithFloat:[[[[myQcData qcFeedDataDict]objectForKey:self.stockCode]objectForKey:FID_244_S_UNDERLYING] doubleValue]];
	}
	return self;
}

#pragma mark - Util
- (NSString *)getTradingBoardFrom:(NSString *)pathName{
	NSArray *separatedStringArr = [pathName componentsSeparatedByString:@"|"];
	if (separatedStringArr.count >= 4) {
		return separatedStringArr[3];
	}else if (separatedStringArr.count <= 3){
		return separatedStringArr[1];
	}else{
		return @"";
	}
}

- (NSString *)getSectorNameFrom:(NSString *)pathName{
	NSArray *separatedStringArr = [pathName componentsSeparatedByString:@"|"];
	if (separatedStringArr.count > 4) {
		return separatedStringArr[4];
	}else{
		return @"";
	}
}
@end
