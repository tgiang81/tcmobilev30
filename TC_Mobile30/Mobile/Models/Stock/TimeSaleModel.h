//
//  TimeSaleModel.h
//  TCiPad
//
//  Created by Kaka on 7/13/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface TimeSaleModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSNumber *incomingPrice;
@property (strong, nonatomic) NSString *PI;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSNumber *vol;

@end
