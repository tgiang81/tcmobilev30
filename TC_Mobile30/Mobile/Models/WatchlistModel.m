//
//  WatchlistModel.m
//  TCiPad
//
//  Created by Kaka on 6/4/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "WatchlistModel.h"

@implementation WatchlistModel
+ (NSMutableArray *)arrayFromDicts:(NSArray *)dictArr{
	NSMutableArray *result = @[].mutableCopy;
	[dictArr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
		WatchlistModel *model = [WatchlistModel new];
		if ([obj.allKeys containsObject:@"FavID"]) {
			model.FavID = [obj[@"FavID"] integerValue];
		}
		if ([obj.allKeys containsObject:@"Name"]) {
			model.Name = obj[@"Name"];
		}
		if ([obj.allKeys containsObject:@"TimeStamp"]) {
			model.TimeStamp = obj[@"TimeStamp"];
		}
		[result addObject:model];
	}];
	return result;
}
@end
