//
//  NewsModel.h
//  TCiPad
//
//  Created by Kaka on 4/21/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "TCBaseModel.h"

@interface NewsModel : TCBaseModel{
	
}
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *timeStamp;
@property (strong, nonatomic) NSString *exchange;
@property (strong, nonatomic) NSString *sourceNews;

@end
