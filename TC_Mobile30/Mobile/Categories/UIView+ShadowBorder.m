//
//  UIView+ShadowBorder.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/14/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "UIView+ShadowBorder.h"
#import "ThemeManager.h"
#import "AppMacro.h"
@implementation UIView (ShadowBorder)
- (void)setupDeauftBorderAndShadow{
    self.clipsToBounds = NO;
    [self.layer setCornerRadius:8.0f];
    [self aroundShadow];
}
- (void)setupDashBoardShashow{
    [self.layer setCornerRadius:1.1];
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    [self.layer setShadowOpacity:0.2527694];
    [self.layer setShadowRadius:1.208459];
    [self.layer setShadowOffset:CGSizeMake(0, 1.007048)];
    self.layer.shouldRasterize = NO;
    self.layer.masksToBounds = NO;
}
- (void)setupDeauftCellBorderAndShadow{
    self.clipsToBounds = NO;
    [self aroundShadow];
//    [self.layer setCornerRadius:2];
//    [self.layer setShadowOpacity:0.2];
    
}
- (void)aroundShadow{
    [self makeBorderShardowWithConner:1.1];
}
- (void)dropShadow{
	self.layer.shadowColor = [UIColor blackColor].CGColor;//TC_COLOR_FROM_HEX([ThemeManager shareInstance].shadowColor).CGColor;//[UIColor blackColor].CGColor;
	self.layer.shadowOpacity = 0.2527694;
	self.layer.shadowOffset = CGSizeMake(0, 0.1007048);
	CGRect shadowPath = CGRectMake(self.layer.bounds.origin.x - 5, self.layer.bounds.size.height - 0, self.layer.bounds.size.width + 10, 1.208459);
	self.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
	self.layer.shouldRasterize = NO;
	self.layer.masksToBounds = NO;
}

- (void)makeBorderShadow{
	[self makeBorderShardowWithConner:0];
}
- (void)makeBorderShardowWithConner:(float)connerRadius{
	// drop shadow
	self.layer.shadowColor = [UIColor blackColor].CGColor;
	[self.layer setShadowOpacity:0.2527694];
	[self.layer setShadowRadius:1.208459];
	[self.layer setShadowOffset:CGSizeMake(0, 0.1007048)];
	[self.layer setCornerRadius:connerRadius]; //2.719033
	self.layer.shouldRasterize = NO;
	self.layer.masksToBounds = NO;
}

//******* Remove Shadow
- (void)removeBorderShadow{
	[self.layer setShadowOpacity:0];
	[self.layer setShadowRadius:0];
	[self.layer setShadowOffset:CGSizeMake(0, 0)];
	self.layer.masksToBounds = YES;
}
@end
