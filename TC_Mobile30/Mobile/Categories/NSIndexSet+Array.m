//
//  NSIndexSet+Array.m
//  TCiPad
//
//  Created by Kaka on 6/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "NSIndexSet+Array.h"

@implementation NSIndexSet (Array)
/**
 *  Returns an NSArray containing the contents of the NSIndexSet in a format that can be persisted.
 */
- (NSArray*)arrayRepresentation {
	NSArray *result = [[NSArray array] objectsAtIndexes:self];
	return [NSArray arrayWithArray:result];
}

/**
 *  Initialises self with the indexes found wtihin the specified array that has previously been
 *  created by the method @see arrayRepresentation.
 */
+ (NSIndexSet*)indexSetWithArrayRepresentation:(NSArray *)array {
	NSMutableIndexSet *result = [NSMutableIndexSet indexSet];
	for (id object in array) {
		[result addIndex:(NSInteger)object];
	}
	return [result copy];
}
@end
