//
//  UIViewController+Email.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@interface UIViewController (Email)<MFMailComposeViewControllerDelegate>
- (void)sendEmail:(NSString *)subject recipients:(NSMutableArray *)recipients message:(NSString *)body attachmentUrl:(NSURL *)attachmentUrl;
@end
