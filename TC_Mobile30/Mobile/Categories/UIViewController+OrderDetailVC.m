//
//  UIViewController+OrderDetailVC.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIViewController+OrderDetailVC.h"
#import "DetailsTableViewCell.h"
#import "ThemeManager.h"
#import "AppMacro.h"
#import "TradeStatus.h"
#import "LanguageManager.h"
#import "NSNumber+Formatter.h"
#import "UILabel+TCUtil.h"
#import "Utils.h"
@implementation UIViewController (OrderDetailVC)
- (DetailsTableViewCell *)setupDateToCellOldWay:(DetailsTableViewCell *)cell detailArray:(NSMutableArray *)detailArray indexPath:(NSIndexPath *)indexPath{

    NSDictionary *dict = [detailArray objectAtIndex:indexPath.row];
    
    NSString *title = [[dict allKeys] objectAtIndex:0];
    NSString *content = [dict valueForKey:title];
    cell.detailsTitle.text = title;

    if ([content length]<=0){
        content = @"-";
    }
    cell.detailsContent.text = content;
    return cell;
}


- (DetailsTableViewCell *)setupDateToCell:(DetailsTableViewCell *)cell detailArray:(NSMutableArray *)detailArray indexPath:(NSIndexPath *)indexPath{
    cell.detailsTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_HightLightTextColor);
    cell.detailsContent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_TextColor);
    
    cell.detailsTitleRight.textColor = cell.detailsTitle.textColor;
    cell.detailsContentRight.textColor = cell.detailsContent.textColor;
    NSDictionary *dict = [detailArray objectAtIndex:indexPath.row];
    
    
    
    NSString *title = [dict objectForKey:@"Title"];
    NSString *content = [dict objectForKey:@"Content"];
    content = [content isEqualToString:@""] ? @"-" : content;
    cell.detailsTitle.text = title;
//    [cell.detailsTitle setAttributeForLabel:title content:content];
    cell.detailsContent.text = content;
    return cell;
}

- (NSMutableArray *)getDetailData:(TradeStatus *)tradeStatus{
    int qtyPoint = 3;
    double ordVal = tradeStatus.order_quantity.longLongValue * tradeStatus.order_price.floatValue;
    NSString *ordQty = tradeStatus.order_quantity;
    NSString *valStr =  [[NSNumber numberWithDouble:ordVal] toDecimaWithPointer:qtyPoint];
    NSString *validityStr = [LanguageManager stringForKey:tradeStatus.validity];
    if ([tradeStatus.validity isEqualToString:@"GTD"]&&([tradeStatus.expiry length]>=8)) {
        //NSLog(@"expiry %@",ts.expiry); eg: 20170620170000.000
        
        NSString *dateStr = [tradeStatus.expiry substringToIndex:8];
        // NSLog(@"dateStr %@", dateStr);
        
        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
        [ndf setDateFormat:@"yyyyMMdd"];
        NSDate *date = [ndf dateFromString:dateStr];
        [ndf setDateFormat:@"dd/MM/yyyy"];
        
        validityStr = [NSString stringWithFormat:@"%@ (%@)", validityStr,[ndf stringFromDate:date]];
        
    }
    
    
    NSString *ordPrice = [[NSNumber numberWithFloat: tradeStatus.order_price.floatValue] toDecimaWithPointer:qtyPoint];
    // this is special rule
    if ([tradeStatus.order_type isEqualToString:@"Market"]||[tradeStatus.order_type isEqualToString:@"MarketToLimit"]) {
        if (tradeStatus.order_price.floatValue<=0) {
            ordPrice = @"-";
        }
    }
    
    NSMutableArray *_detailsArray = [NSMutableArray new];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Action"],@"Content":[self getContent:[LanguageManager stringForKey:tradeStatus.action]], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Status"],@"Content":[self getContent:[LanguageManager stringForKey:tradeStatus.status_text]], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Qty (Unit)"],@"Content":[self getContent:ordQty], @"control_type": @(CONTROL_QUANTITY)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Price"],@"Content":[NSString stringWithFormat:@"%@ %@", tradeStatus.sett_currency, [self getContent:ordPrice]], @"control_type": @(CONTROL_PRICE)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Value"],@"Content":[self getContent:valStr], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Type"],@"Content":[LanguageManager stringForKey:[self getContent:tradeStatus.order_type]], @"control_type": @(CONTROL_ORDERTYPE)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Validity"],@"Content":[self getContent:validityStr], @"control_type": @(CONTROL_VALIDITY)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Settlement Currency"],@"Content":[self getContent:tradeStatus.sett_currency], @"control_type": @(CONTROL_STATEMENTCURRENCY)}];
    
    if ([tradeStatus.payment_type length]>0) {
        if ([tradeStatus.payment_type isEqualToString:@"0"])
            tradeStatus.payment_type = @"-";
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Settlement Mode"],@"Content":[LanguageManager stringForKey:tradeStatus.payment_type], @"control_type": @(CONTROL_PAYMENTTYPE)}];
    }else{
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Settlement Mode"],@"Content":@"-", @"control_type": @(CONTROL_PAYMENTTYPE)}];
    }
    
    NSString *trigPrice = [[NSNumber numberWithFloat: tradeStatus.trigger_price.floatValue] toCurrencyNumber];
    
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Trigger Price"],@"Content":[NSString stringWithFormat:@"%@ %@", tradeStatus.sett_currency,[self getContent:trigPrice]], @"control_type": @(CONTROL_TRIGGERPRICE)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Trigger Type"],@"Content":[self getContent:tradeStatus.triggerType], @"control_type": @(CONTROL_TRIGGER)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Trigger Direction"],@"Content":[self getContent:tradeStatus.triggerDirection], @"control_type": @(CONTROL_DIRECTION)}];
    
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Min Qty(Unit)"],@"Content":[self getContent:tradeStatus.min_quantity], @"control_type": @(CONTROL_MINQUANTITY)}];
    
    
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Unmatched Qty(Unit)"],@"Content":[self getContent:tradeStatus.unmt_quantity], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Matched Qty(Unit)"],@"Content":[self getContent:tradeStatus.mt_quantity], @"control_type": @(-1)}];
    
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Matched Price"],@"Content":[NSString stringWithFormat:@"%@ %@", tradeStatus.sett_currency,[self getContent:tradeStatus.mt_price]], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Matched Value"],@"Content":[NSString stringWithFormat:@"%@ %@", tradeStatus.sett_currency,[self getContent:tradeStatus.mt_value]], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Source"],@"Content":[tradeStatus getOrderSource], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order No."],@"Content":[self getContent:tradeStatus.order_number], @"control_type": @(-1)}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Lot Size"],@"Content":[self getContent:tradeStatus.lot_size], @"control_type": @(-1)}];
    
    return _detailsArray;
}

- (NSString *)getContent:(id)content{
    if([content length] > 0){
        return content;
    }
    return @"-";
}
@end
