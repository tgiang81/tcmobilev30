//
//  UILabel+TCUtil.m
//  TC_Mobile30
//
//  Created by Kaka on 11/30/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UILabel+TCUtil.h"
#import "AppFont.h"
@implementation UILabel (TCUtil)
//For Not null label
- (void)setValueText:(NSString *)text{
	if (!text || text.length == 0 || text == (id)[NSNull null] || [text isEqualToString:@"(null)"] || [text isEqual:[NSNull null]]) {
		text = @"-";
	}
	self.text = text;
}
- (void)setAttributeForLabel:(NSString *)title content:(NSString *)content{
    if ([self respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: self.textColor,
                                  NSFontAttributeName: AppFont_MainFontRegularWithSize(self.font.pointSize)
                                  };
        NSMutableAttributedString *attributedText;
        if(![title isEqualToString:@""]){
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@: %@", title, content]
                                                                    attributes:attribs];
            
            UIFont *boldFont = AppFont_MainFontBoldWithSize(self.font.pointSize);
            NSRange boldTextRange = [[NSString stringWithFormat:@"%@: %@", title, content] rangeOfString:title];
            [attributedText setAttributes:@{
                                            NSFontAttributeName:boldFont}
                                    range:boldTextRange];
        }else{
            attributedText = [[NSMutableAttributedString alloc] initWithString:@""
                                                                    attributes:attribs];
        }
        
        
        self.attributedText = attributedText;
    }
}
//- (void)setText:(NSString *)text{
//	[self setValueText:text];
//}

#pragma mark - ALIGNMENT
- (void)alignTop {
	CGSize size = [self.text sizeWithAttributes:
				   @{NSFontAttributeName: self.font}];
	
	// Values are fractional -- you should take the ceilf to get equivalent values
	CGSize fontSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
	//CGSize fontSize = [self.text sizeWithFont:self.font];
	double finalHeight = fontSize.height * self.numberOfLines;
	double finalWidth = self.frame.size.width;    //expected width of label
	CGSize theStringSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:self.lineBreakMode];
	int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
	for(int i=0; i<newLinesToPad; i++)
		self.text = [self.text stringByAppendingString:@"\n "];
}

- (void)alignBottom {
	CGSize fontSize = [self.text sizeWithFont:self.font];
	double finalHeight = fontSize.height * self.numberOfLines;
	double finalWidth = self.frame.size.width;    //expected width of label
	CGSize theStringSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:self.lineBreakMode];
	int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
	for(int i=0; i<newLinesToPad; i++)
		self.text = [NSString stringWithFormat:@" \n%@",self.text];
}
@end
