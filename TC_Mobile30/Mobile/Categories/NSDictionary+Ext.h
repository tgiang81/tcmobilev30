//
//  NSDictionary+Ext.h
//  TCiPad
//
//  Created by Kaka on 5/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionary)

- (void)setValueString:(NSString *)strValue nullValue:(NSString *)nullValue forKey:(NSString *)key;
- (NSString *)getValueStringFromKey:(NSString *)key;

@end
