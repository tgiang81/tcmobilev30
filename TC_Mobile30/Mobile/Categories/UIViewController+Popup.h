//
//  UIViewController+Popup.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CAPopUpViewController;
@class MZFormSheetController;
@interface UIViewController (Popup)
- (void)setupPopup:(NSArray *)data fromView:(UIView *)fromView width:(CGFloat)width rowHeight:(CGFloat)rowHeight completion:(void (^)(CAPopUpViewController * popupVC, UITableViewCell *popCell, NSInteger row, NSInteger section))completion;
- (void)setupPopup:(NSArray *)data fromView:(UIView *)fromView selectedItems:(NSArray *)selectedItems offsetX:(CGFloat)offsetX offsetY:(CGFloat)offsetY width:(CGFloat)width rowHeight:(CGFloat)rowHeight completion:(void (^)(CAPopUpViewController * popupVC, UITableViewCell *popCell, NSInteger row, NSInteger section))completion;

#pragma mark - Popup VC
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size shouldDismissOnBackgroundViewTap:(BOOL)shouldDismissOnBackgroundViewTap completion:(void (^)(MZFormSheetController *formSheetController))completion;
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size completion:(void (^)(MZFormSheetController *formSheetController))completion;
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size showInCenter:(BOOL)showInCenter topInsect:(CGFloat)topInsect completion:(void (^)(MZFormSheetController *formSheetController))completion;

- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size showInCenter:(BOOL)showInCenter topInsect:(CGFloat)topInsect showBg:(BOOL)showBg completion:(void (^)(MZFormSheetController *formSheetController))completion;
//Show With Default Size (0.8 * SCREEN_WIDTH, 0.8 * SCREEN_HEIGHT)
- (void)showPopupWithContent:(UIViewController *)contentVC withCompletion:(void (^)(MZFormSheetController *formSheetController))completion;
- (void)dismissPopup:(BOOL)animated completion:(void (^)(MZFormSheetController *formSheetController))completion;
//Default using animation
- (void)dismissPopupWithCompletion:(void (^)(MZFormSheetController *formSheetController))completion;

- (void)didDismissPopup;
@end
