//
//  UIViewController+Util.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/20/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Util)
- (void)backVC: (void (^ __nullable)(void))completion;
- (void)backVCToRoot: (void (^ __nullable)(void))completion;
- (BOOL)isModal;
- (CGFloat)getNavHeight;
@end
