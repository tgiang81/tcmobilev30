//
//  NSIndexSet+Array.h
//  TCiPad
//
//  Created by Kaka on 6/6/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSIndexSet (Array){
	
}
- (NSArray*)arrayRepresentation ;
+ (NSIndexSet*)indexSetWithArrayRepresentation:(NSArray*)array;

@end
