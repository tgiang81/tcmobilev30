//
//  NewLineCollectionViewFlowLayout.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/14/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol NewLineCollectionViewFlowLayoutDelegate<NSObject>
- (CGRect)sizeAtIndex:(NSIndexPath *)index;
- (CGFloat)spaceBetweenLines;
- (CGFloat)spaceBetweenItems;
@end
@interface NewLineCollectionViewFlowLayout : UICollectionViewFlowLayout
@property (weak, nonatomic) id<NewLineCollectionViewFlowLayoutDelegate> delegate;
@property (assign, nonatomic) NSInteger numberOfItemsInLine;
@end

NS_ASSUME_NONNULL_END
