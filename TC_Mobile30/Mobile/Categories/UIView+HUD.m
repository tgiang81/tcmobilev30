//
//  UIView+HUD.m
//  TCiPad
//
//  Created by Kaka on 4/5/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "UIView+HUD.h"
#import "MBProgressHUD.h"

@implementation UIView (HUD)
#pragma mark - HUD
- (void)showHUD{
	[MBProgressHUD showHUDAddedTo:self animated:YES];
}

- (void)hideHUD{
	[MBProgressHUD hideHUDForView:self animated:YES];
}
@end
