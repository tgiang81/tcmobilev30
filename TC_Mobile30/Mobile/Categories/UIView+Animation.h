//
//  UIView+Animation.h
//  TCiPad
//
//  Created by Kaka on 5/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StockModel;
@interface UIView (Animation)

#pragma mark - Blink Animation
//2: Animate with price changed - for mobile version
- (void)animateBlinkForPriceChanged:(BOOL)isUp;
//- (void)animateBlinkForPriceChanged:(BOOL)isUp completion:(void(^)(void))completion;

//Use this for make animation
- (void)makeAnimateBy:(StockModel *)oldModel newStock:(StockModel *)newStock completion:(void(^)(void))completion;

- (void)makeAnimateByCheckingValue:(float)oldValue newValue:(float)newValue completion:(void(^)(void))completion;

@end
