//
//  UIButton+RightImage.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIButton+RightImage.h"

@implementation UIButton (RightImage)
- (void)setupRightImage{
    self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
}
@end
