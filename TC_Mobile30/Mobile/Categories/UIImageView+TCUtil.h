//
//  UIImageView+TCUtil.h
//  TC_Mobile30
//
//  Created by Kaka on 11/26/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (TCUtil){
	
}
- (void)toColor:(UIColor *)newColor;

@end
