//
//  UIView+TCUtil.h
//  TC_Mobile30
//
//  Created by Kaka on 10/22/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DGActivityIndicatorView.h"
@interface UIView (TCUtil)
#pragma mark - MAIN
#pragma mark - StatusView
- (void)removeStatusViewIfNeeded;
- (void)showStatusViewWithMessage:(NSString *)message retryAction:(void (^)(void))completion;
- (void)showStatusMessage:(NSString *)message;


#pragma mark - NetworkStatusView
//- (void)showErrorNetworkStatusView;
//- (void)hideNetworkStatusView;
//- (void)hideNetworkStatusViewWhenConnected;

#pragma mark - PrivateHUD
- (void)showPrivateHUD;
- (void)dismissPrivateHUD;
@end
