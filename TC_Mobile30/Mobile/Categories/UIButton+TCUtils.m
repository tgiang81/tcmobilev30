//
//  UIButton+TCUtils.m
//  TC_Mobile30
//
//  Created by Kaka on 12/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIButton+TCUtils.h"
@implementation UIButton (TCUtils)
//For Enable
- (void)shouldEnable:(BOOL)isEnable{
	self.enabled = isEnable;
	self.alpha = isEnable? 1.0 : 0.3;
}
+ (UIButton *)setupNavItem:(NSString *)imageName{
    UIImage *filterImage = [UIImage imageNamed:imageName];
    CGRect alertFrame = CGRectMake(0, 0, filterImage.size.width, filterImage.size.height);
    UIButton *filterButton = [UIButton buttonWithType:UIButtonTypeSystem];
    filterButton.frame = alertFrame;
    [filterButton setImage:filterImage forState:UIControlStateNormal];
    return filterButton;
}

- (void)underLineText:(NSString *)text color:(UIColor *)color{
	NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:text];
	//[commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
	[commentString addAttributes:@{NSForegroundColorAttributeName: color, NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0, [commentString length])];
	[self setAttributedTitle:commentString forState:UIControlStateNormal];
}

- (void)makeUnderLine{
	[self underLineText:self.titleLabel.text color:self.titleLabel.textColor];
}
@end
