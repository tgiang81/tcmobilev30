//
//  UIView+Animation.m
//  TCiPad
//
//  Created by Kaka on 5/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "UIView+Animation.h"
#import "AllCommon.h"

@implementation UIView (Animation)

#pragma mark - Blink Animation
//1: Animate to color
- (void)animateBlinkWithColor:(UIColor *)color completion:(void(^)(void))completion{
	self.layer.backgroundColor = color.CGColor;
	[UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
		self.layer.backgroundColor = [UIColor clearColor].CGColor;
	} completion:^(BOOL finished) {
		self.layer.backgroundColor = [UIColor clearColor].CGColor;
		if (completion) {
			completion();
		}
	}];
}

//2: Animate with price changed - for mobile version
- (void)animateBlinkForPriceChanged:(BOOL)isUp{
	UIColor *blinkColor = isUp ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	[self animateBlinkWithColor: blinkColor completion:nil];
}
- (void)animateBlinkForPriceChanged:(BOOL)isUp completion:(void(^)(void))completion{
	UIColor *blinkColor = isUp ? TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor) : TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	[self animateBlinkWithColor: blinkColor completion:^{
		if (completion) {
			completion();
		}
	}];
}

- (void)makeAnimateBy:(StockModel *)oldModel newStock:(StockModel *)newStock completion:(void(^)(void))completion{
	if (oldModel.fChange.floatValue == newStock.fChange.floatValue) {
		if (completion) {
			completion();
		}
		return;
	}
	BOOL isUp = newStock.fChange.floatValue > oldModel.fChange.floatValue;
	[self animateBlinkForPriceChanged:isUp completion:^{
		if (completion) {
			completion();
		}
	}];
}

- (void)makeAnimateByCheckingValue:(float)oldValue newValue:(float)newValue completion:(void(^)(void))completion{
	if (oldValue == newValue) {
		if (completion) {
			completion();
		}
		return;
	}
	BOOL isUp = newValue > oldValue;
	[self animateBlinkForPriceChanged:isUp completion:^{
		if (completion) {
			completion();
		}
	}];
}
@end
