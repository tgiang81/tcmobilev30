//
//  UIButton+RightImage.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 11/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (RightImage)
-(void)setupRightImage;
@end

NS_ASSUME_NONNULL_END
