//
//  TCSession.m
//  TCMobile
//
//  Created by Kaka on 9/7/17.
//  Copyright © 2017 MMS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSNumber (Formatter){
    
}
#pragma mark - To Decimal
//Using Pointer
- (NSString *)toCurrencyNumber;
- (NSString *)toDecimaWithPointer:(NSInteger)pointer;
- (NSString *)toDecimaUsingMeaningPointer:(NSInteger)pointer;

// For Group
- (NSString*)toShortcutNumber;
- (NSString *)groupNumber;
- (NSString *)abbreviateNumber;

#pragma mark - To String
//Return number of char after "."
- (NSString *)toStringByPointer:(int)pointer;
- (NSString *)toStringUsingSystemPointer;

//Just use fixed decimal
- (NSString *)toStringPecent;
- (NSString *)toStringChangedValue;


#pragma mark - Color by Compare Pric
- (UIColor *)colorByCompareToPrice:(float)comparePrice;
- (UIColor *)colorByCompareToPriceExtend:(float)comparePrice;

#pragma mark - String WithSign
- (NSString *)toStringWithSign;
- (NSString *)toPercentWithSign;
- (NSString *)toChangedStringWithSign;

+ (NSString *) numberFormat2D:(double) input;
- (double)fromGroupNumber;
@end
