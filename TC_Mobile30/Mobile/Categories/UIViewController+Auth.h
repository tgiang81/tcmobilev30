//
//  UIViewController+Auth.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^CompletionAuth)(BOOL success, NSError *error);
NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Auth)
- (void)authPasswordAction:(NSString *)password completion:(CompletionAuth)completion;
- (void)authIdAction:(CompletionAuth)completion;
- (void)startingAuthenticateTouchID:(CompletionAuth)completion;
@end

NS_ASSUME_NONNULL_END
