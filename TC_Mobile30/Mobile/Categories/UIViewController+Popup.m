//
//  UIViewController+Popup.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/10/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIViewController+Popup.h"
#import "CAPopUpViewController.h"
#import "MZFormSheetController.h"
#import "AppMacro.h"
@implementation UIViewController (Popup)
- (void)setupPopup:(NSArray *)data fromView:(UIView *)fromView width:(CGFloat)width rowHeight:(CGFloat)rowHeight completion:(void (^)(CAPopUpViewController * popupVC, UITableViewCell *popCell, NSInteger row, NSInteger section))completion{
    [self setupPopup:data fromView:fromView selectedItems:@[] offsetX:0 offsetY:0 width:width rowHeight:rowHeight completion:completion];
}
- (void)setupPopup:(NSArray *)data fromView:(UIView *)fromView selectedItems:(NSArray *)selectedItems offsetX:(CGFloat)offsetX offsetY:(CGFloat)offsetY width:(CGFloat)width rowHeight:(CGFloat)rowHeight completion:(void (^)(CAPopUpViewController * popupVC, UITableViewCell *popCell, NSInteger row, NSInteger section))completion{
    CAPopUpViewController *popup = [[CAPopUpViewController alloc] init];
    popup.selectedItemsArray = selectedItems;
    popup.offsetX = offsetX;
    popup.offsetY = offsetY;
    popup.itemsArray = data;
    popup.sourceView = fromView;
    popup.backgroundColor = [UIColor whiteColor];
    popup.backgroundImage = nil;
    popup.itemTitleColor = [UIColor blackColor];
    popup.itemSelectionColor = [UIColor lightGrayColor];
    popup.arrowDirections = UIPopoverArrowDirectionUp;
    popup.arrowColor = nil;
    popup.m_SizeWidth = width;
    popup.m_HeightCellTbl = rowHeight;
    
    [popup setPopCellBlock:^(CAPopUpViewController *popupVC, UITableViewCell *popCell, NSInteger row, NSInteger section) {
        [popupVC dismissViewControllerAnimated:YES completion:^{
            if(completion){
                completion(popupVC, popCell, row, section);
            }
        }];
        
    }];
    
    [self presentViewController:popup animated:YES completion:^{
        
    }];
}

#pragma mark - Show PopUp
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size showInCenter:(BOOL)showInCenter shouldDismissOnBackgroundViewTap:(BOOL)shouldDismissOnBackgroundViewTap topInsect:(CGFloat)topInsect showBg:(BOOL)showBg completion:(void (^)(MZFormSheetController *formSheetController))completion{
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:contentVC];
    formSheet.presentedFormSheetSize = size;
    formSheet.shadowRadius = 8.0;
    formSheet.shouldShowBg = showBg;
    formSheet.shadowOpacity = 0;
    formSheet.cornerRadius = 8.0;
    formSheet.portraitTopInset = topInsect;
    formSheet.shouldCenterVertically = showInCenter;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard;
	
	//Enable background window
	MZFormSheetController.sharedBackgroundWindow.backgroundBlurEffect = NO;
	[MZFormSheetController.sharedBackgroundWindow setBlurRadius:5.0];
	[MZFormSheetController.sharedBackgroundWindow setBackgroundColor:RGBA(0, 0, 0, 0.7)];

    formSheet.transitionStyle = MZFormSheetTransitionStyleFade;
    formSheet.shouldDismissOnBackgroundViewTap = shouldDismissOnBackgroundViewTap;
    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        //passing data here
        if (completion) {
            completion(formSheet);
        }
    }];
    
    formSheet.didDismissCompletionHandler = ^(UIViewController *vc) {
        [self didDismissPopup];
    };
}
- (void)didDismissPopup{
    
}

- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size showInCenter:(BOOL)showInCenter topInsect:(CGFloat)topInsect completion:(void (^)(MZFormSheetController *formSheetController))completion{
    [self showPopupWithContent:contentVC inSize:size showInCenter:showInCenter shouldDismissOnBackgroundViewTap:YES topInsect:topInsect showBg:YES completion:completion];
}
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size showInCenter:(BOOL)showInCenter topInsect:(CGFloat)topInsect showBg:(BOOL)showBg completion:(void (^)(MZFormSheetController *formSheetController))completion{
    [self showPopupWithContent:contentVC inSize:size showInCenter:showInCenter shouldDismissOnBackgroundViewTap:YES topInsect:topInsect  showBg:showBg completion:completion];
}
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size completion:(void (^)(MZFormSheetController *formSheetController))completion{
    [self showPopupWithContent:contentVC inSize:size showInCenter:YES topInsect:0 completion:completion];
}
- (void)showPopupWithContent:(UIViewController *)contentVC inSize:(CGSize)size shouldDismissOnBackgroundViewTap:(BOOL)shouldDismissOnBackgroundViewTap completion:(void (^)(MZFormSheetController *formSheetController))completion{
    [self showPopupWithContent:contentVC inSize:size showInCenter:YES shouldDismissOnBackgroundViewTap:shouldDismissOnBackgroundViewTap topInsect:0  showBg:YES completion:completion];
}
//Using default size
- (void)showPopupWithContent:(UIViewController *)contentVC withCompletion:(void (^)(MZFormSheetController *formSheetController))completion{
    CGSize defaultSize = CGSizeMake(0.8 * SCREEN_WIDTH_PORTRAIT, 300);
    [self showPopupWithContent:contentVC inSize:defaultSize completion:completion];
}
- (void)dismissPopup:(BOOL)animated completion:(void (^)(MZFormSheetController *formSheetController))completion{
    [self mz_dismissFormSheetControllerAnimated:animated completionHandler:completion];
}
//Default using animation
- (void)dismissPopupWithCompletion:(void (^)(MZFormSheetController *formSheetController))completion{
    [self dismissPopup:YES completion:completion];
}
@end
