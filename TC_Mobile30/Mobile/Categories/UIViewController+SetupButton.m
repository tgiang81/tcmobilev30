//
//  UIViewController+SetupButton.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "UIViewController+SetupButton.h"

@implementation UIViewController (SetupButton)
- (void)setupButton:(UIButton *)button corner:(CGFloat)corner borderColor:(UIColor *)borderColor bgColor:(UIColor *)bgColor textColor:(UIColor *)textColor{
    button.layer.cornerRadius = corner;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [borderColor CGColor];
    button.backgroundColor = bgColor;
    [button setTitleColor:textColor forState:UIControlStateNormal];
}
@end
