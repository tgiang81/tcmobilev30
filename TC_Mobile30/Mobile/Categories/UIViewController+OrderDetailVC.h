//
//  UIViewController+OrderDetailVC.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/17/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DetailsTableViewCell;
@class TradeStatus;
NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (OrderDetailVC)
- (DetailsTableViewCell *)setupDateToCell:(DetailsTableViewCell *)cell detailArray:(NSMutableArray *)detailArray indexPath:(NSIndexPath *)indexPath;
- (DetailsTableViewCell *)setupDateToCellOldWay:(DetailsTableViewCell *)cell detailArray:(NSMutableArray *)detailArray indexPath:(NSIndexPath *)indexPath;
- (NSMutableArray *)getDetailData:(TradeStatus *)tradeStatus;
@end

NS_ASSUME_NONNULL_END
