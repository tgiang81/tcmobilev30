//
//  UIViewController+TCUtil.m
//  TC_Mobile30
//
//  Created by Kaka on 11/2/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIViewController+TCUtil.h"
#import "AllCommon.h"

@implementation UIViewController (TCUtil)

#pragma mark - Class Function
+ (NSString *)storyboardID{
	return NSStringFromClass([self class]);
}

+ (NSString *)nibName{
	return NSStringFromClass([self class]);
}

#pragma mark - Actions
- (void)toogleLeftMenu:(id)sender {
	[AppDelegateAccessor toogleLeftMenu:nil];
}

#pragma mark - Navigate VC
- (void)nextTo:(UIViewController *)vc animate:(BOOL)animate{
	[self.navigationController pushViewController:vc animated:animate];
}

- (void)popToView:(Class)vc animate:(BOOL)animate{
	if (vc) {
		NSArray *viewControllers = self.navigationController.viewControllers;
		if (viewControllers && viewControllers.count > 0) {
			for (UIViewController *view in viewControllers) {
				if ([view isKindOfClass:vc]) {
					[self.navigationController popToViewController:view animated:animate];
					break;
				}
			}
		}
	}
}

- (void)popView:(BOOL)animate{
	[self.navigationController popViewControllerAnimated:animate];
}

-(void)addVC:(UIViewController *)vc toView:(UIView *)currentView animation:(BOOL)animation{
    [vc willMoveToParentViewController:self];
    [currentView addSubview:vc.view];
    vc.view.frame = currentView.bounds;
    vc.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                UIViewAutoresizingFlexibleHeight);
    vc.view.translatesAutoresizingMaskIntoConstraints = YES;
    
    
    if(animation == YES){
        vc.view.alpha = 0;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            vc.view.alpha = 1.0;
        } completion:^(BOOL finished) {
            [self addChildViewController:vc];
            [vc didMoveToParentViewController:self];
        }];
        
    }else{
        [self addChildViewController:vc];
        [vc didMoveToParentViewController:self];
    }
    
}

-(void)addVC:(UIViewController *)vc toView:(UIView *)currentView{
    [self addVC:vc toView:currentView animation:NO];
}
#pragma mark - Notification
//Post
- (void)postNotification:(NSString *)notiName object:(id)obj{
	[[NSNotificationCenter defaultCenter] postNotificationName:notiName object:obj];
}
- (void)postNotification:(NSString *)notiName object:(id)obj userInfo:(NSDictionary *)userInfo{
	[[NSNotificationCenter defaultCenter] postNotificationName:notiName object:obj userInfo:userInfo];
}
//Add
- (void)addNotification:(NSString *)notiName selector:(SEL)selector{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:selector name:notiName object:nil];
}
//Remove
- (void)removeNotification:(NSString *)notiName{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:notiName object:nil];
}
- (void)removeAllNotification{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
