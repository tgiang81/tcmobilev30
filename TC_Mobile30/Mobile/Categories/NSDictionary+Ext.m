//
//  NSDictionary+Ext.m
//  TCiPad
//
//  Created by Kaka on 5/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//


#import "NSDictionary+Ext.h"

//@implementation NSDictionary_Ext
@implementation NSDictionary (NSDictionary)

- (void)setValueString:(NSString *)strValue nullValue:(NSString *)nullValue forKey:(NSString *)key {
    if (strValue && strValue != nil && strValue != [NSNull null] && [strValue length] > 0 && ![strValue isEqualToString:@"(null)"] && ![strValue isEqualToString:@"<null>"] && ![strValue isEqualToString:@"null"]) {
        [self setValue:strValue forKeyPath:key];
    }
    else {
        [self setValue:nullValue forKeyPath:key];
    }
}

- (NSString *)getValueStringFromKey:(NSString *)key {
    NSString *str = [self[key] description];
    
    if (str == nil || [str isEqualToString:@"<null>"] || [str isEqualToString:@"null"] || [str isEqualToString:@"(null)"]) {
        str = @"";
    }
    return str;
}

@end
