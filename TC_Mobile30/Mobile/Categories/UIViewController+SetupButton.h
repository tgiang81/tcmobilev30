//
//  UIViewController+SetupButton.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/9/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (SetupButton)
- (void)setupButton:(UIButton *)button corner:(CGFloat)corner borderColor:(UIColor *)borderColor bgColor:(UIColor *)bgColor textColor:(UIColor *)textColor;
@end

NS_ASSUME_NONNULL_END
