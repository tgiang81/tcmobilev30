//
//  NSString+Util.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Util)
+ (NSString *)getDirectoryURL;
- (NSNumber *)toNumber;

//Remove White space
- (NSString *)removeAllWhitespace;
- (NSString *)trimmedLeadingAndTrailingWhiteSpace;

- (NSString *)getDateStringFromServer:(NSString *)inputFm andOutputFm:(NSString *)outputFm;

- (NSString *)removeSpecialChars:(NSString *)specialString;
- (NSString *)getStringNumber;
+ (NSString *)getASCII:(int)code;
- (BOOL)isValidString;
- (BOOL)isEmpty;
@end
