//
//  TCService.m
//  TCMobile
//
//  Created by Kaka on 9/7/17.
//  Copyright © 2017 MMS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utilities)
- (NSString *)stringWithFormat:(NSString *)format;
- (NSString *)toString;
//For TCProject
- (NSString *)formatDateWithTimeZone;
#pragma mark - Compare Date
- (BOOL) isToday;
- (BOOL)isTomorrow;
- (NSInteger) compareSameMonthAndYearWithOtherDay:(NSDate *)dateToCompare;
- (NSComparisonResult) compareMonth:(NSDate *)dateToCompare;
- (BOOL) isLessThanCurrentMonth;

//CompareDate later or early
- (BOOL) isLaterThanOrEqualTo:(NSDate*)date;
- (BOOL) isEarlierThanOrEqualTo:(NSDate*)date;
- (BOOL) isLaterThan:(NSDate*)date;
- (BOOL) isEarlierThan:(NSDate*)date;
#pragma mark - Current Day
- (NSInteger)getCurrenDayByDate:(NSDate *)curDate;
#pragma mark - Date From String
+ (NSDate *)dateFromString:(NSString *)strDate format:(NSString *)format;

#pragma mark - Adjusting date
- (NSDate *) dateByAddingDays: (NSInteger) dDays;
- (NSDate *) dateBySubtractingDays: (NSInteger) dDays;
- (NSInteger)totalDayInThisMonth;
+ (NSInteger)currentHour;
+ (NSInteger)currentMinute;
+ (NSInteger)getCurrentYear;
+ (NSInteger)getCurrentMonth;
+ (NSInteger)getTotalDayOfMonth:(NSInteger)month year:(NSInteger)year;
+ (NSArray *)getDaysInThisWeekTotal;
+ (NSString *)dateMode;
- (NSString *)daySuffixForDate;
@end

