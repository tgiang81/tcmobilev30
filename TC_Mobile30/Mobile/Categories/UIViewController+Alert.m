//
//  UIViewController+Alert.m
//  SidebarDemo
//
//  Created by Kaka on 11/13/17.
//  Copyright © 2017 N2N Connect Bhd. All rights reserved.
//

#import "UIViewController+Alert.h"
#import "AppConstants.h"
#import "AllCommon.h"
#import "TSMessage.h"
#import "AppMacro.h"
#import "LanguageKey.h"
@implementation UIViewController (Alert)

#pragma mark - CustomAlert
- (void)showCustomAlert:(NSString *)title message:(NSString *)message{
	[self showCustomAlert:TC_Pro_Mobile message:message okTitle:Confirm withOKAction:nil cancelTitle:nil cancelAction:nil];
}

#pragma mark - Wrap to User
- (void)showCustomAlertWithMessage:(NSString *)message{
    [self showCustomAlert:TC_Pro_Mobile message:message];
}

- (void)showErrorCustomAlertWithMessage:(NSString *)message{
    [self showCustomAlert:TC_Pro_Mobile message:message];
}
#pragma mark - Custom Alert With Action
- (void)showCustomAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction{
	[self showCustomAlert:title message:message okTitle:Confirm withOKAction:okAction cancelTitle:nil cancelAction:nil];
}

- (void)showCustomAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction cancelAction:(void (^)(void))cancelAction{
    [self showCustomAlert:title message:message okTitle:Confirm withOKAction:okAction cancelTitle:Cancel cancelAction:cancelAction];
}

#pragma mark - MAIN
- (void)showCustomAlert:(NSString *)title message:(NSString *)message okTitle:(NSString *)okTitle withOKAction:(void (^)(void))okAction cancelTitle:(NSString *)cancelTitle cancelAction:(void (^)(void))cancelAction{
	
    TYAlertView *alertView = [TYAlertView alertViewWithTitle:title message:message];
    //Configs
    NSString *mainTintColor = [BrokerManager shareInstance].detailBroker.color.mainTint;
    alertView.buttonDefaultBgColor = TC_COLOR_FROM_HEX(mainTintColor);
    alertView.buttonDefaultTextColor = TC_COLOR_FROM_HEX(@"FFFFFF");//Alway white
    alertView.buttonDefaultBorderColor = TC_COLOR_FROM_HEX(mainTintColor);;
    
	alertView.buttonDestructiveBgColor = [UIColor clearColor];//TC_COLOR_FROM_HEX([ThemeManager shareInstance].ca_cancelColor);
    alertView.buttonDestructiveTextColor = TC_COLOR_FROM_HEX(mainTintColor);
    alertView.buttonDestructiveBorderColor = TC_COLOR_FROM_HEX(mainTintColor);;

    //Cancel Action
	if (cancelTitle) {
		[alertView addAction:[TYAlertAction actionWithTitle:[LanguageManager stringForKey:cancelTitle] style:TYAlertActionStyleDestructive handler:^(TYAlertAction *action) {
			if (cancelAction) {
				cancelAction();
			}
		}]];
	}
	 //Ok Action
	if (okTitle) {
		[alertView addAction:[TYAlertAction actionWithTitle:[LanguageManager stringForKey:okTitle] style:TYAlertActionStyleDefault handler:^(TYAlertAction *action) {
			if (okAction) {
				okAction();
			}
		}]];
	}
	
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - InPut
- (void)showInputCustomAlert:(NSString *)title message:(NSString *)message placeholder:(NSString *)placeholder completion:(void(^)(NSArray *textFields))completionHandler{
    TYAlertView *alertView = [TYAlertView alertViewWithTitle:title message:message];
	//Configs
	alertView.buttonCancelBgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	alertView.buttonDefaultBgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	alertView.buttonDestructiveBgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].deleteColor);
	
    __typeof (alertView) __weak weakAlertView = alertView;
    //Add Action
    [alertView addAction:[TYAlertAction actionWithTitle:[LanguageManager stringForKey:OK] style:TYAlertActionStyleDefault handler:^(TYAlertAction *action) {
        completionHandler(weakAlertView.textFieldArray);
    }]];
    // Add TextField
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = placeholder;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.textAlignment = NSTextAlignmentCenter;
        textField.textColor = [UIColor colorWithRed:66/255.0 green:90/255.0 blue:108/255.0 alpha:1.0];
    }];
    
    //first way to show
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showInputCustomAlert:(NSString *)title message:(NSString *)message placeholder:(NSString *)placeholder okAction:(void(^)(NSArray *textFields))okAction cancelAction:(void (^)(void))cancelAction{
    TYAlertView *alertView = [TYAlertView alertViewWithTitle:title message:message];
	//Configs
	alertView.buttonCancelBgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	alertView.buttonDefaultBgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].tintColor);
	alertView.buttonDestructiveBgColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].deleteColor);
	
    __typeof (alertView) __weak weakAlertView = alertView;
    //Cancel Action
    [alertView addAction:[TYAlertAction actionWithTitle:[LanguageManager stringForKey:Cancel] style:TYAlertActionStyleDestructive handler:^(TYAlertAction *action) {
        if (cancelAction) {
            cancelAction();
        }
    }]];
    //Add Action
    [alertView addAction:[TYAlertAction actionWithTitle:[LanguageManager stringForKey:OK] style:TYAlertActionStyleDefault handler:^(TYAlertAction *action) {
        okAction(weakAlertView.textFieldArray);
    }]];
    
    // Add TextField
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = placeholder;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.textAlignment = NSTextAlignmentCenter;
        textField.textColor = [UIColor colorWithRed:66/255.0 green:90/255.0 blue:108/255.0 alpha:1.0];
    }];
    
    //first way to show
    TYAlertController *alertController = [TYAlertController alertControllerWithAlertView:alertView preferredStyle:TYAlertControllerStyleAlert];
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - System Alert
#pragma mark - Alert Normal
- (void)showAlertWithMessage:(NSString *)message{
    if (!message) {
        message = @"Something wrongs";
    }
    [self showCustomAlertWithMessage:message];
}
- (void)showErrorWithAlert:(NSString *)errorMessage{
    if (!errorMessage) {
        errorMessage = @"Something wrongs";
    }
    [self showCustomAlertWithMessage:errorMessage];
}

- (void)showAlert:(NSString *)title message:(NSString *)message{
    [self showCustomAlert:title message:message];
}

#pragma mark - Alert With Completion
- (void)showAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction{
    [self showCustomAlert:title message:message withOKAction:okAction];
}

- (void)showAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction cancelAction:(void (^)(void))cancelAction{
    [self showCustomAlert:title message:message withOKAction:okAction cancelAction:cancelAction];
}

/*
 - (void)showAlert:(NSString *)title message:(NSString *)message onConfirm:(void (^)(id)) theConfirmBlock onCancel:(void (^)(id)) theCancelBlock
 {
 UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
 message:message
 preferredStyle:UIAlertControllerStyleAlert];
 
 UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action) {
 if (theConfirmBlock != nil) {
 theConfirmBlock (self);
 }
 }];
 UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
 handler:^(UIAlertAction * action) {
 if (theCancelBlock != nil) {
 theCancelBlock (self);
 }
 }];
 
 [alert addAction:okAction];
 [alert addAction:cancelAction];
 [self presentViewController:alert animated:YES completion:nil];
 }
 */

- (void)showActiveAlertWithTitle:(NSString *)title message:(NSString *)message placeHolderField:(NSString *)placeHolder withConfirmAction:(void (^)(NSString *activeCode))confirmBlock{
    // use UIAlertController
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:title
                               message:message
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:OK] style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   //Do Some action here
                                                   UITextField *textField = alert.textFields[0];
                                                   if (confirmBlock) {
                                                       confirmBlock(textField.text);
                                                   }
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:Cancel] style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = placeHolder;
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.textAlignment = NSTextAlignmentCenter;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Notification
- (void)showSuccessNotification:(NSString *)message{
    [TSMessage showNotificationWithTitle:[LanguageManager stringForKey:Success_]
                                subtitle:message
                                    type:TSMessageNotificationTypeSuccess];
}
- (void)showWarningNotification:(NSString *)message{
    [TSMessage showNotificationWithTitle:[LanguageManager stringForKey:Warning_]
                                subtitle:message
                                    type:TSMessageNotificationTypeWarning];
}
- (void)showErrorNotification:(NSString *)message{
    [TSMessage showNotificationWithTitle:[LanguageManager stringForKey:Error_]
                                subtitle:message
                                    type:TSMessageNotificationTypeError];
}
- (void)showErrorNotification:(NSString *)message inViewController:(UIViewController *)viewController{
    dispatch_async(dispatch_get_main_queue(), ^{
        [TSMessage showNotificationInViewController:viewController title:[LanguageManager stringForKey:Error_] subtitle:message type:TSMessageNotificationTypeError];
    });
    
    
}
@end
