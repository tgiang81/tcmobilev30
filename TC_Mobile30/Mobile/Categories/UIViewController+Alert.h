//
//  UIViewController+Alert.h
//  SidebarDemo
//
//  Created by Kaka on 11/13/17.
//  Copyright © 2017 N2N Connect Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TYAlertController.h"

#define TITLE_ALERT  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]
@interface UIViewController (Alert){
	
}

#pragma mark - Alert Normal
- (void)showAlertWithMessage:(NSString *)message;
- (void)showErrorWithAlert:(NSString *)errorMessage;
- (void)showAlert:(NSString *)title message:(NSString *)message;

#pragma mark - Alert With Completion
- (void)showAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction;
- (void)showAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction cancelAction:(void (^)(void))cancelAction;
- (void)showActiveAlertWithTitle:(NSString *)title message:(NSString *)message placeHolderField:(NSString *)placeHolder withConfirmAction:(void (^)(NSString *activeCode))confirmBlock;
- (void)showCustomAlert:(NSString *)title message:(NSString *)message okTitle:(NSString *)okTitle withOKAction:(void (^)(void))okAction cancelTitle:(NSString *)cancelTitle cancelAction:(void (^)(void))cancelAction;
#pragma mark - Custom Alert
- (void)showCustomAlert:(NSString *)title message:(NSString *)message;
- (void)showCustomAlertWithMessage:(NSString *)message;
- (void)showErrorCustomAlertWithMessage:(NSString *)message;

- (void)showCustomAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction;
- (void)showCustomAlert:(NSString *)title message:(NSString *)message withOKAction:(void (^)(void))okAction cancelAction:(void (^)(void))cancelAction;

//Input with alert
- (void)showInputCustomAlert:(NSString *)title message:(NSString *)message placeholder:(NSString *)placeholder completion:(void(^)(NSArray *textFields))completionHandler;
- (void)showInputCustomAlert:(NSString *)title message:(NSString *)message placeholder:(NSString *)placeholder okAction:(void(^)(NSArray *textFields))okAction cancelAction:(void (^)(void))cancelAction;

#pragma mark - Notification
- (void)showSuccessNotification:(NSString *)message;
- (void)showWarningNotification:(NSString *)message;
- (void)showErrorNotification:(NSString *)message;
- (void)showErrorNotification:(NSString *)message inViewController:(UIViewController *)viewController;
@end
