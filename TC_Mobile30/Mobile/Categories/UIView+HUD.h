//
//  UIView+HUD.h
//  TCiPad
//
//  Created by Kaka on 4/5/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (HUD)
//======= HUD =========
- (void)showHUD;
- (void)hideHUD;
@end
