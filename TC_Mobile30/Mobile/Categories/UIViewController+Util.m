//
//  UIViewController+Util.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/20/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIViewController+Util.h"

@implementation UIViewController (Util)
- (CGFloat)getNavHeight{
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
                            (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    return topbarHeight;
}
- (void)backVC: (void (^ __nullable)(void))completion{
    if([self isModal]){
        if(self.navigationController){
            [self.navigationController dismissViewControllerAnimated:TRUE completion:completion];
        }else{
            [self dismissViewControllerAnimated:YES completion:completion];
        }
        
    }else{
        if(self.navigationController){
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
}
- (void)backVCToRoot: (void (^ __nullable)(void))completion{
    if([self isModal]){
        if(self.navigationController){
            [self.navigationController dismissViewControllerAnimated:TRUE completion:completion];
        }else{
            [self dismissViewControllerAnimated:YES completion:completion];
        }
        
    }else{
        if(self.navigationController){
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    }
}
- (BOOL)isModal {
    if([self presentingViewController])
        return YES;
    if([[[self navigationController] presentingViewController] presentedViewController] == [self navigationController])
        return YES;
    if([[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]])
        return YES;
    
    return NO;
}
@end
