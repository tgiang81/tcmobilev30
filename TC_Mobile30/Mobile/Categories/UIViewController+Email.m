//
//  UIViewController+Email.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIViewController+Email.h"
#import "LanguageKey.h"

@implementation UIViewController (Email)
- (void)sendEmail:(NSString *)subject recipients:(NSMutableArray *)recipients message:(NSString *)body attachmentUrl:(NSURL *)attachmentUrl{

    if ([MFMailComposeViewController canSendMail]) {
        if(recipients.count == 0){
            [recipients addObject:@[@"n2n_sales@n2nconnect.com"]];
        }
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:subject];
        [mailCont setToRecipients:recipients];
        [mailCont setMessageBody:body isHTML:NO];
        
        NSData *attachmentData = [NSData dataWithContentsOfURL:attachmentUrl];
        if(attachmentData.length > 0){
            [mailCont addAttachmentData:attachmentData mimeType:@"text/plain" fileName:[[attachmentUrl lastPathComponent] stringByDeletingPathExtension]];
        }
        // Present mail view controller on screen
        [self presentViewController:mailCont animated:YES completion:NULL];
    }else{
        
        UIAlertView  *alert = [[UIAlertView alloc]initWithTitle:TC_Pro_Mobile message:@"Email has not been setup on this device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
