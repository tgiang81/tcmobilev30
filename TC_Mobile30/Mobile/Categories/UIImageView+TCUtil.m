//
//  UIImageView+TCUtil.m
//  TC_Mobile30
//
//  Created by Kaka on 11/26/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIImageView+TCUtil.h"

@implementation UIImageView (TCUtil)
- (void)toColor:(UIColor *)newColor{
	self.image = [self.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	[self setTintColor:newColor];
}
@end
