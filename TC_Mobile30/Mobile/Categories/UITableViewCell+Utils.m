//
//  UITableViewCell+Utils.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/17/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "UITableViewCell+Utils.h"

@implementation UITableViewCell (Utils)
- (void)showFullSeperator{
    self.preservesSuperviewLayoutMargins = NO;
    self.separatorInset = UIEdgeInsetsZero;
    self.layoutMargins = UIEdgeInsetsZero;
}
@end
