//
//  NSString+Watchlist.m
//  TCiPad
//
//  Created by Kaka on 8/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import "NSString+Watchlist.h"
#import "AllCommon.h"
#define kDefaultWatchlistKey	@"kDefaultWatchlistKey"

@implementation NSString (Watchlist)
#pragma mark - Store, Get Default Watchlist
- (void)saveAsDefaultWatchlist{
	NSMutableDictionary *defaultDict = [[USER_DEFAULT objectForKey:kDefaultWatchlistKey] mutableCopy];
	if (!defaultDict) {
		defaultDict = @{}.mutableCopy;
	}
	[defaultDict setObject:self forKey:[UserSession shareInstance].userName];
	[USER_DEFAULT setObject:defaultDict forKey:kDefaultWatchlistKey];
	[USER_DEFAULT synchronize];
}
+ (NSString *)getDefaultWatchlist{
	NSMutableDictionary *defaultDict = [USER_DEFAULT objectForKey:kDefaultWatchlistKey];
	if (defaultDict) {
		return [defaultDict objectForKey:[UserSession shareInstance].userName];
	}
	return nil;
}

#pragma mark - UTILS / Check is default watchlist
- (BOOL)isDefaultWatchlist{
	NSString *theDefaultWL = [NSString getDefaultWatchlist];
	if (!theDefaultWL) {
		return NO;
	}
	return [self isEqualToString:theDefaultWL];
}
@end
