//
//  UIView+TCUtil.m
//  TC_Mobile30
//
//  Created by Kaka on 10/22/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIView+TCUtil.h"
#import "TCStatusInfoView.h"
#import "Utils.h"
#import "NetworkStatusView.h"

@implementation UIView (TCUtil)

#pragma mark - StatusView
- (void)removeStatusViewIfNeeded{
	for (UIView *v in self.subviews) {
		if ([v isKindOfClass:[TCStatusInfoView class]]) {
			[v removeFromSuperview];
		}
	}
}

- (void)showStatusViewWithMessage:(NSString *)message retryAction:(void (^)(void))completion{
	TCStatusInfoView *_statusView = [TCStatusInfoView new];
	[_statusView showStatus:message inView:self retryAction:^{
		if (completion) {
			completion();
		}
	}];
}

- (void)showStatusMessage:(NSString *)message{
	TCStatusInfoView *_statusView = [TCStatusInfoView new];
	[_statusView justShowStatus:message inView:self];
}

#pragma mark - NetworkStatusView
- (void)showErrorNetworkStatusView{
	NetworkStatusView *nView = [[NetworkStatusView alloc] init];
	[nView showErrorNetwork:nil fromView:self];
}
- (void)hideNetworkStatusView{
	for (UIView *v in self.subviews) {
		if ([v isKindOfClass:[NetworkStatusView class]]) {
			[v removeFromSuperview];
		}
	}
}
- (void)hideNetworkStatusViewWhenConnected{
	for (UIView *v in self.subviews) {
		if ([v isKindOfClass:[NetworkStatusView class]]) {
			NetworkStatusView *nView = (NetworkStatusView *)v;
			[nView hideNetworkStatusViewWhenConnected];
		}
	}
}
#pragma mark - PrivateHUD
- (void)showPrivateHUD{
	//Demo show activity on StockView
	DGActivityIndicatorView *_activityView = [[Utils shareInstance]createPrivateActivity];
	[[Utils shareInstance] showActivity:_activityView inView:self];
}

- (void)dismissPrivateHUD{
	for (UIView *v in self.subviews) {
		if ([v isKindOfClass:[DGActivityIndicatorView class]]) {
			DGActivityIndicatorView *activeHUD = (DGActivityIndicatorView *)v;
			[activeHUD stopAnimating];
			[activeHUD removeFromSuperview];
		}
	}
}
@end
