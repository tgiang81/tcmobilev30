//
//  NSString+Watchlist.h
//  TCiPad
//
//  Created by Kaka on 8/14/18.
//  Copyright © 2018 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Watchlist)
#pragma mark - Store, Get Default Watchlist
- (void)saveAsDefaultWatchlist;
+ (NSString *)getDefaultWatchlist;

#pragma mark - UTILS / Check is default watchlist
- (BOOL)isDefaultWatchlist;
@end
