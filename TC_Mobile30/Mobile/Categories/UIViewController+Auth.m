//
//  UIViewController+Auth.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/7/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UIViewController+Auth.h"
#import "LanguageManager.h"
#import "DeviceUtils.h"
#import "Utils.h"
#import "UIViewController+Alert.h"
#import "UserPrefConstants.h"
#import "LanguageKey.h"
@implementation UIViewController (Auth)
- (void)authPasswordAction:(NSString *)password completion:(CompletionAuth)completion{
    completion([[UserPrefConstants singleton].userPassword isEqualToString:password], nil);
}
- (void)authIdAction:(CompletionAuth)completion{
    DLog(@"+++ on touchID Action");
    NSString *message = [LanguageManager stringForKey:@"The Touch ID is not active. Please active it to use"];
    if ([DeviceUtils isFaceIdSupported]) {
        message = [LanguageManager stringForKey:@"The Face ID is not active. Please active it to use"];
    }
    [DeviceUtils isAvailableToucOrFaceID:^(BOOL isAvailable) {
        if (!isAvailable) {
            //Device not active touch
            [self showCustomAlert:TC_Pro_Mobile message:message];
            completion(NO, nil);
        }else{
            //Check if has no account in the keychain <Navigate to Enter Menual>
            if ([Utils isExistedAccountLogined] == NO) {
                [self showCustomAlertWithMessage:[LanguageManager stringForKey:@"The TouchId does not match any account. Please login manually"]];
                completion(NO, nil);
            }else{
                [self startingAuthenticateTouchID:completion];
            }
            //Login with TouchID
        }
    }];
}

- (void)startingAuthenticateTouchID:(CompletionAuth)completion{
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    NSString *titleMessage;
    NSString *msgLAErrorAuthenticationFailed;
    NSInteger lABiometryType = [DeviceUtils getLABiometryType];
    
    if (lABiometryType == TOUCHID_AVAILABLE) {
        titleMessage = [LanguageManager stringForKey:Place_your_finger_on_the_Home_button_to_Login];
        msgLAErrorAuthenticationFailed = [LanguageManager stringForKey:Invalid_fingerprint___Please_try_again];
    }
    else if(lABiometryType == FACEID_AVAILABLE) {
        titleMessage = [LanguageManager stringForKey:Place_your_face_in_front_of_Camera_to_log_in];
        msgLAErrorAuthenticationFailed = [LanguageManager stringForKey:Invalid_faceID___Please_try_again];
    }
    else {
        DLog(@"Error 777");
        return;
    }
    
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        // Authenticate User
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:titleMessage
                          reply:^(BOOL success, NSError *error) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (success) {
                                      completion(YES, error);
                                  }else{
                                      switch (error.code) {
                                          case LAErrorAuthenticationFailed:
                                          {
                                              [self showAlert:[LanguageManager stringForKey:TC_Pro_Mobile] message:msgLAErrorAuthenticationFailed];
                                              break;
                                          }
                                          case LAErrorBiometryLockout:
                                          {
                                              DLog(@"777 The TouchID is lock out");
                                              [self showCustomAlert:TITLE_ALERT message:error.localizedDescription];
                                              break;
                                          }
                                              
                                          default:
                                              DLog(@"777 Touch ID is not configured");
                                              break;
                                      }
                                      completion(NO, error);
                                  }
                              });
                          }];
    }else{
		completion(NO, error);
    }
}
@end
