//
//  UITableView+Util.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/29/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UITableView+Util.h"

@implementation UITableView (Util)
- (void)reloadWithAutoSizingCell{
    [self setHidden:YES];
    [self reloadData];
    [self setNeedsLayout];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self layoutIfNeeded];
        [self reloadData];
        [self setHidden:NO];
    });
    
}
@end

