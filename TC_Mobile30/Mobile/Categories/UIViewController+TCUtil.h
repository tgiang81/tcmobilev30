//
//  UIViewController+TCUtil.h
//  TC_Mobile30
//
//  Created by Kaka on 11/2/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"

@interface UIViewController (TCUtil){
	
}

//Class function
+ (NSString *)storyboardID;
+ (NSString *)nibName;

#pragma mark - Items on NavigationBar
//Handle Menu
- (void)toogleLeftMenu:(id)sender;

#pragma mark - Navigate VC
- (void)nextTo:(UIViewController *)vc animate:(BOOL)animate;
- (void)popToView:(Class)vc animate:(BOOL)animate;
- (void)popView:(BOOL)animate;


#pragma mark - Notification
// Notification Utils
- (void)postNotification:(NSString *)notiName object:(id)obj;
- (void)postNotification:(NSString *)notiName object:(id)obj userInfo:(NSDictionary *)userInfo;

- (void)addNotification:(NSString *)notiName selector:(SEL)selector;
- (void)removeNotification:(NSString *)notiName;
- (void)removeAllNotification;

-(void)addVC:(UIViewController *)vc toView:(UIView *)currentView;
-(void)addVC:(UIViewController *)vc toView:(UIView *)currentView animation:(BOOL)animation;
@end
