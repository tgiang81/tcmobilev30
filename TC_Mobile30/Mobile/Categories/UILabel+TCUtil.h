//
//  UILabel+TCUtil.h
//  TC_Mobile30
//
//  Created by Kaka on 11/30/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (TCUtil){
	
}
//Not Empty Text
- (void)setValueText:(NSString *)text;
- (void)setAttributeForLabel:(NSString *)title content:(NSString *)content;

#pragma mark - Alignment
- (void)alignTop;
- (void)alignBottom;
@end
