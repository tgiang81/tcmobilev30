//
//  UISegmentedControl+Util.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UISegmentedControl+Util.h"

@implementation UISegmentedControl (Util)
- (void)changeTextColor:(UIColor *)textColor font:(UIFont *)font{
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                font, NSFontAttributeName,
                                textColor, NSForegroundColorAttributeName,
                                nil];
    [self setTitleTextAttributes:attributes forState:UIControlStateNormal];
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [self setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
}

- (void)removeBorderColor{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(1, self.frame.size.height), NO, 0.0);
    UIImage *blank = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setDividerImage:blank
                    forLeftSegmentState:UIControlStateNormal
                      rightSegmentState:UIControlStateNormal
                             barMetrics:UIBarMetricsDefault];
}
@end
