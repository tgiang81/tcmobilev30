//
//  UISwitch+TCUtils.m
//  TC_Mobile30
//
//  Created by Kaka on 12/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UISwitch+TCUtils.h"

@implementation UISwitch (TCUtils)
//Off Color
- (void)setOffColor:(UIColor *)color{
	self.tintColor = color;
	self.layer.cornerRadius = self.frame.size.height / 2;
	self.backgroundColor = color;
}

@end
