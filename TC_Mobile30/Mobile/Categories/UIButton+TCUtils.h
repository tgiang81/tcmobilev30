//
//  UIButton+TCUtils.h
//  TC_Mobile30
//
//  Created by Kaka on 12/13/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (TCUtils)
//For Enable
- (void)shouldEnable:(BOOL)isEnable;
+ (UIButton *)setupNavItem:(NSString *)imageName;

#pragma mark - Underline
- (void)makeUnderLine;
- (void)underLineText:(NSString *)text color:(UIColor *)color;

@end
