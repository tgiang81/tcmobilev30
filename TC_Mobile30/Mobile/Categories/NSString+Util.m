//
//  NSString+Util.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/12/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "NSString+Util.h"

@implementation NSString (Util)
+ (NSString *)getDirectoryURL{
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPaths objectAtIndex:0];
    return documentPath;
}
- (NSNumber *)toNumber{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *number = [f numberFromString:self];
    return number;
}

#pragma mark - Remove White space
//Remove All white space
- (NSString *)removeAllWhitespace{
	return [self stringByReplacingOccurrencesOfString:@"\\s" withString:@""
											  options:NSRegularExpressionSearch
												range:NSMakeRange(0, [self length])];
}

- (NSString *)trimmedLeadingAndTrailingWhiteSpace{
	// TODO: To be implemented from tutorial.
	NSString *leadingTrailingWhiteSpacesPattern = @"(?:^\\s+)|(?:\\s+$)";
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:leadingTrailingWhiteSpacesPattern options:NSRegularExpressionCaseInsensitive error:NULL];
	NSRange stringRange = NSMakeRange(0, self.length);
	NSString *trimmedString = [regex stringByReplacingMatchesInString:self options:NSMatchingReportProgress range:stringRange withTemplate:@"$1"];
	return trimmedString;
}


- (NSString *)getDateStringFromServer:(NSString *)inputFm andOutputFm:(NSString *)outputFm{
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:inputFm];
    NSDate *date = [ndf dateFromString:self];
    [ndf setDateFormat:outputFm];
    return [ndf stringFromDate:date];
}

- (NSString *)removeSpecialChars:(NSString *)specialString{
	// Create character set with specified characters
	NSMutableCharacterSet *characterSet =
	[NSMutableCharacterSet characterSetWithCharactersInString:specialString];
	
	// Build array of components using specified characters as separtors
	NSArray *arrayOfComponents = [self componentsSeparatedByCharactersInSet:characterSet];
	// Create string from the array components
	return [arrayOfComponents componentsJoinedByString:@""];
}

- (BOOL)isEmpty {
    return self.length == 0;
}

- (BOOL)isValidString{
    return ![[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEmpty];
}
- (NSString *)getStringNumber{
    if([self isValidString]){
        return self;
    }
    return @"0";
}
+ (NSString *)getASCII:(int)code{
    return [NSString stringWithFormat:@"%c", code];
}
@end
