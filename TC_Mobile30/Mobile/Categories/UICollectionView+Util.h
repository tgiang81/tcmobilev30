//
//  UICollectionView+Util.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/29/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionView (Util)
- (void)reloadWithAutoSizingCell;
@end

NS_ASSUME_NONNULL_END
