//
//  NewLineCollectionViewFlowLayout.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 12/14/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "NewLineCollectionViewFlowLayout.h"
#import "AppMacro.h"
@implementation NewLineCollectionViewFlowLayout
- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *attributesInRect = [super layoutAttributesForElementsInRect:rect];
    for (UICollectionViewLayoutAttributes *cellAttributes in attributesInRect) {
        [self modifyLayoutAttributes:cellAttributes];
    }
    return attributesInRect;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForItemAtIndexPath:indexPath];
    [self modifyLayoutAttributes:attributes];
    return attributes;
}

- (void)modifyLayoutAttributes:(UICollectionViewLayoutAttributes *)attributes
{
    if([self.delegate respondsToSelector:@selector(sizeAtIndex:)]){
        CGRect rect = [self.delegate sizeAtIndex:attributes.indexPath];
        CGRect frame = attributes.frame;
        if(rect.size.width > frame.size.width){
            frame.origin.x = rect.origin.x;
            rect.origin.y = frame.origin.y + (attributes.indexPath.row%self.numberOfItemsInLine == 0 ? 0 : rect.size.height) + [self.delegate spaceBetweenLines];
            attributes.frame = rect;
        }
    }
}
@end
