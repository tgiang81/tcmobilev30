//
//  TCSession.m
//  TCMobile
//
//  Created by Kaka on 9/7/17.
//  Copyright © 2017 MMS. All rights reserved.
//

#import "NSNumber+Formatter.h"
#import "AllCommon.h"

@implementation NSNumber (Formatter)
#pragma mark - Decimal Style
- (NSString *)toCurrencyNumber{
	return [self toDecimaWithPointer:[UserPrefConstants singleton].pointerDecimal];
}

- (NSString *)toDecimaWithPointer:(NSInteger)pointer{
	NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
	[priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	priceFormatter.minimumFractionDigits = pointer;
	priceFormatter.maximumFractionDigits = pointer;
	priceFormatter.decimalSeparator = @".";
	[priceFormatter setGroupingSeparator:@","];
	[priceFormatter setGroupingSize:3];
	return [priceFormatter stringFromNumber:self];
}

+ (NSString *) numberFormat2D:(double) input {
    return [[NSNumber numberWithDouble:input] toDecimaWithPointer:2];
}

//Just use max pointer
- (NSString *)toDecimaUsingMeaningPointer:(NSInteger)pointer{
	NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
	[priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	priceFormatter.maximumFractionDigits = pointer;
	priceFormatter.decimalSeparator = @".";
	[priceFormatter setGroupingSeparator:@","];
	[priceFormatter setGroupingSize:3];
	return [priceFormatter stringFromNumber:self];
}
#pragma mark - Abbreviate Number

- (NSString *)groupNumber{
	if (!self) {
		return @"";
	}
	NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
	[quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[quantityFormatter setGroupingSeparator:@","];
	[quantityFormatter setGroupingSize:3];
	[quantityFormatter setGeneratesDecimalNumbers:YES];
	NSString *numberAsString = [quantityFormatter stringFromNumber:self];
	return numberAsString;
}
- (double)fromGroupNumber:(NSString *)stringNumber{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
    numberFormatter.locale = [NSLocale currentLocale];// this ensures the right separator behavior
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.usesGroupingSeparator = YES;
    return [[numberFormatter numberFromString:stringNumber] doubleValue];
}
#pragma mark - SHORT NUMBER
- (NSString*)toShortcutNumber{
	if (!self) {
		return @"";
	}
	if ((self.longLongValue > -1000) && (self.longLongValue < 1000)) {
		return [NSString stringWithFormat:@"%d", self.intValue];
	}
	
	/*
	if ((self.longLongValue > -1000000) && (self.longLongValue < 1000000)) {
		return [self toDecimaWithPointer:2];
	}
	*/
	long long value = [self longLongValue];
	NSString *signChar = @"";
	if (self.doubleValue < 0) {
		signChar = @"-";
		value = llabs(value);
	}
	NSUInteger index = 0;
	double dvalue = (double)value;
	NSArray *suffix = @[@"", @"K", @"M", @"B", @"T", @"P", @"E" ];
	while ((value /= 1000) && ++index) dvalue /= 1000;
	
	//NSString *svalue = [NSString stringWithFormat:@"%.4f%@", dvalue, [suffix objectAtIndex:index]];
	//+++ Using system poiter
	//Check using pointer
	NSString *pointerValue = [[NSNumber numberWithDouble:dvalue] toDecimaWithPointer:2]; //+++ just using 2 char after decimal
	NSString *svalue = [NSString stringWithFormat:@"%@%@%@",signChar, pointerValue, [suffix objectAtIndex:index]];
	return svalue;
}

- (NSString *)abbreviateNumber{
	return [self toShortcutNumber];
	/*
	NSString *abbrevNum;
	long long number = [self longLongValue];
	//Prevent numbers smaller than 1000 to return NULL
	if (number >= 1000) {
		NSArray *abbrev = @[@"K", @"M", @"B"];
		for (NSInteger i = abbrev.count - 1; i >= 0; i--) {
			// Convert array index to "1000", "1000000", etc
			int size = pow(10,(i+1)*3);
			if(size <= number) {
				// Removed the round and dec to make sure small numbers are included like: 1.1K instead of 1K
				number = number/size;
				NSString *numberString = [self floatToString:number];
				// Add the letter for the abbreviation
				abbrevNum = [NSString stringWithFormat:@"%@%@", numberString, [abbrev objectAtIndex:i]];
			}
		}
	} else {
		// Numbers like: 999 returns 999 instead of NULL
		abbrevNum = [NSString stringWithFormat:@"%d", (int)number];
	}
	return abbrevNum;
	 */
}

- (NSString *)floatToString:(float) val {
	if (!self) {
		return @"";
	}
	NSString *ret = [NSString stringWithFormat:@"%.4f", val];
	unichar c = [ret characterAtIndex:[ret length] - 1];
	
	while (c == 48) { // 0
		ret = [ret substringToIndex:[ret length] - 1];
		c = [ret characterAtIndex:[ret length] - 1];
		
		//After finding the "." we know that everything left is the decimal number, so get a substring excluding the "."
		if(c == 46) { // .
			ret = [ret substringToIndex:[ret length] - 1];
		}
	}
	return ret;
}

#pragma mark - To String
//Return number of char after "."
- (NSString *)toStringByPointer:(int)pointer{
	if (!self) {
		return @"";
	}
	float val = [self floatValue];
	switch (pointer) {
		case 2:
			return [NSString stringWithFormat:@"%.2f", val];
		case 3:
			return [NSString stringWithFormat:@"%.3f", val];
		case 4:
			return [NSString stringWithFormat:@"%.4f", val];
		case 5:
			return [NSString stringWithFormat:@"%.5f", val];
		case 6:
			return [NSString stringWithFormat:@"%.6f", val];
		default:
			return [NSString stringWithFormat:@"%.0f", val];
	}
}

- (NSString *)toStringUsingSystemPointer{
	return [self toStringByPointer:[UserPrefConstants singleton].pointerDecimal];
}

- (NSString *)toStringPecent{
	float val = [self floatValue];
	//Should have no decimal when value is min or max
	if (val == 100 || val == 0) {
		return [NSString stringWithFormat:@"%.0f", val];
	}
	return [NSString stringWithFormat:@"%.2f", val];
}

- (NSString *)toStringChangedValue{
	float val = [self floatValue];
	return [NSString stringWithFormat:@"%.3f", val];
}

#pragma mark - Color by Compare Price
- (UIColor *)colorByCompareToPrice:(float)comparePrice{
	if (self.floatValue >= comparePrice) {
		return TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);
	}
	if (self.floatValue < comparePrice) {
		return TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
	}
	return TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor);

}

- (UIColor *)colorByCompareToPriceExtend:(float)comparePrice{
    if (self.floatValue > comparePrice) {
        return TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor);;
    }
    if (self.floatValue < comparePrice) {
        return TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor);
    }
    return RGB(229, 228, 77);
}

#pragma mark - String WithSign
- (NSString *)toStringWithSign{
	if (!self) {
		return @"";
	}
	NSString *signChar = @"";
	float value = [self floatValue];
	if (value > 0) {
		signChar = @"+";
	}else if (value < 0){
		signChar = @"-";
	}
	value =  fabsf(value);
	return [NSString stringWithFormat:@"%@%@", signChar, [[NSNumber numberWithFloat:value] toStringPecent]];
	//Maybe use later
	//return [NSString stringWithFormat:@"%@%@", signChar, [[NSNumber numberWithFloat: value] toStringUsingSystemPointer]];
}

- (NSString *)toChangedStringWithSign{
	if (!self) {
		return @"";
	}
	NSString *signChar = @"";
	float value = [self floatValue];
	if (value > 0) {
		signChar = @"+";
	}else if (value < 0){
		signChar = @"-";
	}
	value =  fabsf(value);
	return [NSString stringWithFormat:@"%@%@", signChar, [[NSNumber numberWithFloat:value] toStringChangedValue]];
}


- (NSString *)toPercentWithSign{
	NSString *originalValueWithSign = [self toStringWithSign];
	return [NSString stringWithFormat:@"%@%@", originalValueWithSign, @"%"];
}
@end
