//
//  UISegmentedControl+Util.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 9/5/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegmentedControl (Util)
- (void)changeTextColor:(UIColor *)textColor font:(UIFont *)font;
- (void)removeBorderColor;
@end
