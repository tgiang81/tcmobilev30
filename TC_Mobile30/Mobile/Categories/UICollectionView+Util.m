//
//  UICollectionView+Util.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 10/29/18.
//  Copyright © 2018 n2nconnect. All rights reserved.
//

#import "UICollectionView+Util.h"

@implementation UICollectionView (Util)
- (void)reloadWithAutoSizingCell{
    [self reloadData];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self reloadData];
}
@end
