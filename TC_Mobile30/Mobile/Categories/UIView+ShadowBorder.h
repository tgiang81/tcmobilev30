//
//  UIView+ShadowBorder.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 1/14/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (ShadowBorder)
- (void)setupDeauftBorderAndShadow;
- (void)setupDeauftCellBorderAndShadow;
- (void)makeBorderShadow;
- (void)setupDashBoardShashow;
- (void)dropShadow;
- (void)aroundShadow;
- (void)makeBorderShardowWithConner:(float)connerRadius;
//REMOVE SHADOW
- (void)removeBorderShadow;
@end

NS_ASSUME_NONNULL_END
