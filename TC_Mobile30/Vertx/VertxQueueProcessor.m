//
//  VertxQueueProcessor.m
//  TCPlusUniversal
//
//  Created by Scott Thoo on 4/4/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "VertxQueueProcessor.h"
#import "QCData.h"
#import "QCConstants.h"
#import "AppConstants.h"
#import "UserPrefConstants.h"

@interface VertxQueueProcessor()
{
    
}
@end

@implementation VertxQueueProcessor
@synthesize dict = _dict;
- (id)initWithDictionary :(NSDictionary *)dict delegate :(id<VertxProcessorDelegate>) theDelegate
{
    self = [super init];
    if (self) {
        self.delegate = theDelegate;
        _dict = dict;
    }
    
    return self;
}

- (void)main
{
    //Update FeedData Singleton, get updated stockCode is which row (loop), Post Notification blink.
    [self getRow];
}

- (void)getUpdatedColumnPosition
{

}

- (void)getRow
{
}
//    NSMutableDictionary* posDict = [NSMutableDictionary dictionary]; //Consist ONLY 2 Data = Row and Column Index;
//    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];

//    if([_dict objectForKey:@"data"]) //if have any data, get coming data stock code, loop thru QCData to get row.
//    {
//        
//        NSString *incomingStockCode = [[_dict objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE];
//
//        NSDictionary *prevDict =[[[QCData singleton]qcFeedDataDict]objectForKey:incomingStockCode];
//
//        NSArray *incomingAllKeys = [[_dict objectForKey:@"data"]allKeys];
//        NSArray *incomingAllValues = [[_dict objectForKey:@"data"]allValues];
    
//        ////NSLog(@"prevDict : %@",prevDict);
//        ////NSLog(@"incoming : %@",[_dict objectForKey:@"data"]);
//        ////NSLog(@"incomingAllKeys : %@",incomingAllKeys);
//        ////NSLog(@"incomingAllValues : %@",incomingAllValues);

//        if(prevDict){
//            for(int i =0;i<[[_dict objectForKey:@"data"]count];i++)
//            {
//                if([[prevDict objectForKey:[incomingAllKeys objectAtIndex:i]]floatValue]!=[[incomingAllValues objectAtIndex:i]floatValue])
//                {
////                    ////NSLog(@"≠ %@",[incomingAllKeys objectAtIndex:i]);
//                    ////NSLog(@"Prev Value : %f",[[prevDict valueForKey:[incomingAllKeys objectAtIndex:i]]floatValue]);
//                    ////NSLog(@"New Value : %f",[[incomingAllValues objectAtIndex:i]floatValue]);
//                    [notificationData setObject:incomingStockCode forKey:@"stkCode"];
//                    [notificationData setObject:[incomingAllKeys objectAtIndex:i] forKey:@"fid"];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"blinkNotificationReceived" object:self userInfo:notificationData];
//                }
//            }
//        }
//        [[[[QCData singleton]qcFeedDataDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:[_dict objectForKey:@"data"]]; //Replace received data to FeedDataDict
//        incomingStockCode = nil;
//        _dict = nil;
//    }
//    columnArr = nil;
//}

@end
