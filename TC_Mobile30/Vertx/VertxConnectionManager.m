//
//  VertxConnectionManager.m
//  TCPlusUniversal
//
//  Created by Scott Thoo on 4/4/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "VertxConnectionManager.h"
#import "VertxQueueProcessor.h"
#import "SRWebSocket.h"
#import "QCData.h"
#import "QCConstants.h"
#import "AppConstants.h"
#import "UserPrefConstants.h"
#import <UIKit/UIKit.h>
#import "ExchangeData.h"
#import "Utils.h"
#import "NetworkManager.h"
#import "NSDictionary+Ext.h"

#define kCloseLogoutCode	111
#define kReasonLogout		@"User Logout"

//Incoming Define
#define kINCOMING_STOCK_CHART_EVENT_ID	@"11"
@interface VertxConnectionManager()
{
    NSOperationQueue *_operationQueueProcess;
    VertxQueueProcessor *_processor;
    NSString *_msg;
    NSString *cmdCall;
    int eventIdCounter;
    int listenerIdCounter;
    int getKLCIcounter;
    NSMutableArray *subscribedListenerArr;
    NSString *eventId;
    NSMutableDictionary *industryDict ;
    NSMutableDictionary *vertexFeedDict06;
    
    //Incoming type;
    NSString *incomingType;
    NSString *incomingMessage;
    NSString *incomingEventId;
    NSString *incomingQid;
    BOOL incomingEnd;
    BOOL incomingHasNext;
    NSString *keepQid;
    NSString *currentCall;
    NSMutableDictionary *responseDict; // Use for NSNotification Center.
    NSMutableArray *sortedresults;
    NSMutableArray *subscribedListenerArrCustom;
    NSMutableArray *MarketDepthResults;
    NSString *SearchString;
    NSDictionary *FixedExchangeListIndex;
}

@end

@implementation VertxConnectionManager
@synthesize webSocket = _webSocket;
//@synthesize delegate;

+ (VertxConnectionManager *)singleton
{	
	static VertxConnectionManager *_instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_instance = [[VertxConnectionManager alloc] init];
	});
	return _instance;
        
}
- (id)init
{
    if(self =[super init])
    {
        //NSLog(@"New Init Vertx Manager");
		_socketError = nil;
        [self connect];
        _operationQueueProcess = [[NSOperationQueue alloc]init];
        [_operationQueueProcess setMaxConcurrentOperationCount:1];
        subscribedListenerArr = @[].mutableCopy;
        industryDict =[NSMutableDictionary dictionary];
        vertexFeedDict06 = [NSMutableDictionary dictionary];
        listenerIdCounter = 0;
        eventIdCounter = 0;
		
		responseDict = @{}.mutableCopy;
		sortedresults = @[].mutableCopy;
		subscribedListenerArrCustom = @[].mutableCopy;
		self.connected = NO;
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNetworkChangeNotification:) name:kNetworkDidChangeNotification object:nil];
    }
    else
    {
        ////NSLog(@"Already Have Vertx Manager");
    }
    FixedExchangeListIndex=[[NSDictionary alloc]initWithDictionary:[AppConstants FixedExchangeListIndex] copyItems:YES];
                            return self;
}

#pragma mark -
#pragma mark LOGIN

- (void)vertxLogin
{
    cmdCall = @"LOGIN";
    ////NSLog(@"vertxLogin");
//    msg call
//    _msg = @"{\"action\":\"session\"}";
//    
//    _executor = [[VertxQueueExecutor alloc]initWithMsgString:_msg delegate:self];
//    [_operationQueueExecute addOperation:_executor];
//    
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    
    [sendMessage setObject:@"session" forKey:@"action"];
//    [sendMessage setObject:@"true" forKey:@"numAsStr"];
    [sendMessage setObject:@"n2n" forKey:@"token"];
    [sendMessage setObject:cmdCall forKey:@"eventId"];

    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted error: nil];
	
    NSLog(@"vertxLogin sendMessage : %@",sendMessage);

    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)vertxGetExchangeInfo
{
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"context" forKey:@"action"];
    [sendMessage setObject:@"GETEXCHANGEINFO" forKey:@"eventId"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted error: nil];
    if ([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}

#pragma mark - UNSUBCRIBE
- (void)unsubscribeAll{
	[self vertxUnsubscribeKLCI];
	[self vertxUnsubscribeAllInQcFeed];
	[self vertxUnsubscribeMktDepth];
	[self vertxUnsubscribeMarketStreamer];
	[self vertxUnSubcribeGetRating];
}
#pragma mark -
#pragma mark REGISTER

- (void)vertxGetRating:(NSString *)stockCode exchange:(NSString *)exchange{
    /*
     x152=rating
     x153=risk level
	 x148: week High
	 x149: week low
     */
    [self vertxGetRatingWithColl:@[@"17"] columns:@[@"33", @"x152",@"x153", @"x148", @"x149"] filter:@[@{@"field":@"33",@"value":stockCode,@"comparison":@"eq"}] exchange:exchange];
}
- (void) vertxUnSubcribeGetRating{
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"unregister" forKey:@"action"];
    [sendMessage setObject:@"getScreenerRating" forKey:@"eventId"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}
- (void)vertxGetRatingWithColl:(NSArray *)coll columns:(NSArray *)columns filter:(NSArray *)filter exchange:(NSString *)exchange
{
    cmdCall = @"getScreenerRating";
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"];
    [sendMessage setObject:coll forKey:@"coll"];
    [sendMessage setObject:columns forKey:@"column"];
    [sendMessage setObject:filter forKey:@"filter"];
    [sendMessage setObject:exchange forKey:@"exch"];
    [sendMessage setObject:cmdCall forKey:@"eventId"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted error: nil];
    
    NSLog(@"vertxGetRatingSendMessage : %@",sendMessage);
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)vertxRegister
{
    cmdCall = @"REGISTER";
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    
    [sendMessage setObject:@"session" forKey:@"action"];
    //    [sendMessage setObject:@"true" forKey:@"numAsStr"];
    [sendMessage setObject:@"n2n" forKey:@"token"];
    [sendMessage setObject:cmdCall forKey:@"eventId"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted error: nil];
    
    NSLog(@"sendMessage : %@",sendMessage);
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

#pragma mark - CHART
- (void)getChartDataFrom:(NSString *)code inExchange:(NSString *)exchange forDay:(NSInteger)days{
	cmdCall = @"11";
	NSMutableDictionary *sendDict = @{}.mutableCopy;
	[sendDict setObject:@"qc" forKey:@"action"];
	[sendDict setObject:@"Chart" forKey:@"req"];
	[sendDict setObject:@"json" forKey:@"type"];
	NSString *paramStr = [NSString stringWithFormat:@"%@,7,0,0,%ld,0,0", code, (long)days]; //7,0,0,7,0,0"
	[sendDict setObject:@{@"[ReadData]": paramStr} forKey:@"params"];
	[sendDict setValueString:exchange nullValue:@"" forKey:@"exch"];
	[sendDict setObject:cmdCall forKey:@"eventId"];
	//To json data
	NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendDict options:NSJSONWritingPrettyPrinted error: nil];
	if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}
- (void)getDefaultWeekChartDataFrom:(NSString *)code inExchange:(NSString *)exchange completion:(TCVertxCompletionHandler)handler{
	[self getChartDataFrom:code inExchange:exchange forDay:7];
	if (handler) {
		self.stockDataChartCompletion = handler;
		handler = nil;
	}
}

#pragma mark -
#pragma mark HOME



- (void)vertxGetScoreBoardWithExchange: (NSString *)exchange marketId:(NSString *)marketId
{
    // clear scoreboard
    
    //    {"action":"query","coll":["02"],"column":["36","37","35","109","131","105","106","107","108","110","111"],"filter":[{"field":"131","value":"KL","comparison":"eq"},{"field":"35","value":"2201"}],"eventId":"13”}
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"];
    NSArray *collArr = [[NSArray alloc] initWithObjects:@"02", nil];
    [sendMessage setObject:collArr forKey:@"coll"];
    NSArray *column = [[NSArray alloc] initWithObjects:@"36",@"37",@"35",@"109",@"131",@"105",@"106",@"107",@"108",@"110",@"111", nil];
    [sendMessage setObject:column forKey:@"column"];
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    [filterDict setObject:@"131" forKey:@"field"];
    [filterDict setObject:exchange forKey:@"value"];
    [filterDict setObject:@"eq" forKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];
    [filterDict setObject:@"35" forKey:@"field"];
    [filterDict setObject:marketId forKey:@"value"];
    [filterDict removeObjectForKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
    [sendMessage setObject:@"vertxGetScoreBoardWithExchange" forKey:@"eventId"];
    
    ////NSLog(@"vertxGetScoreBoardWithExchange : %@",sendMessage);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    ////NSLog(@"%@ MIKE ",[NSString stringWithUTF8String:[jsonData bytes]]);
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
}


- (void)vertxSortForTicker:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange //Get 10 items **Notice the "limit"
{
    cmdCall = @"SortForTicker";
    
    
    NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    [filterDict setObject:@"38" forKey:@"field"];
    [filterDict setObject:@"" forKey:@"value"];
    [filterDict setObject:@"ne" forKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];

    
    [filterDict setObject:FID_151_I_MARKET forKey:@"field"];
    [filterDict setObject:@"eq" forKey:@"comparison"];
    [filterDict setObject:@"10" forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
    
//    [filterDict setObject:@"path" forKey:@"field"];
//    [filterDict setObject:@"|10|" forKey:@"value"];
//    [filterDict removeObjectForKey:@"comparison"];
//    [filterArr addObject:[filterDict copy]];
    
//    [filterDict setObject:@"path" forKey:@"field"];
//    [filterDict setObject:@"" forKey:@"value"];
//    [filterArr addObject:[filterDict copy]];
    
    NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
    [sortDict setObject:property forKey:@"property"];
    [sortDict setObject:direction forKey:@"direction"];
    
    NSMutableArray *sortArr = [[NSMutableArray alloc]init];
    [sortArr addObject:sortDict];
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"]; //String
    [sendMessage setObject:@[COLL_05_QUOTE,COLL_07_TRANS_PUSH] forKey:@"coll"]; //Array
    [sendMessage setObject:[NSNumber numberWithInt:0] forKey:@"start"]; //int
    [sendMessage setObject:[NSNumber numberWithInt:10] forKey:@"limit"]; //int
    [sendMessage setObject:collumnArr forKey:@"column"]; //Array
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
    [sendMessage setObject:exchange forKey:@"exch"]; //String
    [sendMessage setObject:sortArr forKey:@"sort"]; //Array
    [sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    //NSLog(@"SortByHomeLeft sendMessage : %@",[NSString stringWithUTF8String:[jsonData bytes]]);
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}


- (void)vertxSortByLeft:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange //Get 10 items **Notice the "limit"
{
    cmdCall = @"SortByHomeLeft";
    
    
    NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    [filterDict setObject:@"38" forKey:@"field"];
    [filterDict setObject:@"" forKey:@"value"];
    [filterDict setObject:@"ne" forKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];
    
    
    // all market
    [filterDict setObject:FID_151_I_MARKET forKey:@"field"];
    [filterDict setObject:@"eq" forKey:@"comparison"];
    [filterDict setObject:@"10" forKey:@"value"];
    [filterArr addObject:[filterDict copy]];

//    [filterDict setObject:@"path" forKey:@"field"];
//    [filterDict setObject:@"|10|" forKey:@"value"];
//    [filterDict removeObjectForKey:@"comparison"];
//    [filterArr addObject:[filterDict copy]];
    
//    [filterDict setObject:@"path" forKey:@"field"];
//    [filterDict setObject:@"" forKey:@"value"];
//    [filterArr addObject:[filterDict copy]];
    
    NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
    [sortDict setObject:property forKey:@"property"];
    [sortDict setObject:direction forKey:@"direction"];
    
    NSMutableArray *sortArr = [[NSMutableArray alloc]init];
    [sortArr addObject:sortDict];
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"]; //String
    [sendMessage setObject:@[COLL_05_QUOTE,COLL_07_TRANS_PUSH] forKey:@"coll"]; //Array
    [sendMessage setObject:[NSNumber numberWithInt:0] forKey:@"start"]; //int
    
    //Request get 20 stocks.
    [sendMessage setObject:[NSNumber numberWithInt:20] forKey:@"limit"]; //int
    [sendMessage setObject:collumnArr forKey:@"column"]; //Array
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
    [sendMessage setObject:exchange forKey:@"exch"]; //String
    [sendMessage setObject:sortArr forKey:@"sort"]; //Array
    [sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    //NSLog(@"SortByHomeLeft sendMessage : %@",[NSString stringWithUTF8String:[jsonData bytes]]);
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}

- (void)vertxSortByRight:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange
{
    cmdCall = @"SortByHomeRight";
    
    
    NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
    
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    [filterDict setObject:@"38" forKey:@"field"];
    [filterDict setObject:@"" forKey:@"value"];
    [filterDict setObject:@"ne" forKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];

    [filterDict setObject:FID_151_I_MARKET forKey:@"field"];
    [filterDict setObject:@"eq" forKey:@"comparison"];
    [filterDict setObject:@"10" forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
    
//    [filterDict setObject:@"path" forKey:@"field"];
//    [filterDict setObject:@"|10|" forKey:@"value"];
//    [filterDict removeObjectForKey:@"comparison"];
//    [filterArr addObject:[filterDict copy]];
    
    [filterDict setObject:@"path" forKey:@"field"];
    [filterDict setObject:@"" forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
    
    NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
    [sortDict setObject:property forKey:@"property"];
    [sortDict setObject:direction forKey:@"direction"];
    
    NSMutableArray *sortArr = [[NSMutableArray alloc]init];
    [sortArr addObject:sortDict];
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"]; //String
    [sendMessage setObject:@[COLL_05_QUOTE,COLL_07_TRANS_PUSH] forKey:@"coll"]; //Array
    [sendMessage setObject:[NSNumber numberWithInt:0] forKey:@"start"]; //int
    [sendMessage setObject:[NSNumber numberWithInt:10] forKey:@"limit"]; //int
    [sendMessage setObject:collumnArr forKey:@"column"]; //Array
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
    [sendMessage setObject:exchange forKey:@"exch"]; //String
    [sendMessage setObject:sortArr forKey:@"sort"]; //Array
    [sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
    //NSLog(@"SortByHomeright sendMessage : %@",[NSString stringWithUTF8String:[jsonData bytes]]);
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}

#pragma mark - STOCK DETAILS (MKT DEPTH/TIMESALES/BIZDONE/NEWS)


-(void)vertxUnsubscribeMktDepth {
    
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"unregister" forKey:@"action"];
    [sendMessage setObject:@"marketDepth" forKey:@"listenId"];
    [sendMessage setObject:@"SUBSCRIBEMARKETDEPTH" forKey:@"eventId"];
    
    // NSLog(@"Unsub MktDepth %@",sendMessage);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    
   
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];

}



-(void)vertxSubscribeMktDepthLevel:(int)feature withStockCode:(NSString*)stkCode {
    MarketDepthResults = [[NSMutableArray alloc]init];
    cmdCall = @"SUBSCRIBEMARKETDEPTH";
    
   
    NSMutableDictionary *sendMessage= [[NSMutableDictionary alloc]init];
    
    
    NSLog(@"MDLfeature Subscribe %d",feature);
    
    if (feature==0) {
        // Non MDL
        NSString *MDValue;
        int MDLfeature = 0;
        // old MDL detection
//        for (ExchangeData *ed in [[UserPrefConstants singleton]userExchagelist]) {
//            //NSLog(@"ED: %@ %@",ed.feed_exchg_code, ed.mktDepthLevel);
//            if ([ed.feed_exchg_code isEqualToString:[[UserPrefConstants singleton] userCurrentExchange]]) {
//                MDValue =ed.mktDepthLevel;
//                MDLfeature = ed.MDLfeature;
//            }
//        }

        ExchangeInfo *currInfo = [UserPrefConstants singleton].currentExchangeInfo;
        for (MDInfo *inf in currInfo.qcParams.exchgInfo) {
            
            if ([inf.exchange isEqualToString:[UserPrefConstants singleton].userCurrentExchange]) {
                MDValue = inf.mdLevel;
                MDLfeature = ([inf.isMDL isEqualToString:@"Y"]) ? 1: 0;
                
                NSLog(@"vertxSubscribeMktDepthLevel MDLfeature Subscribe %d",MDLfeature);
                
                if (([inf.exchange isEqualToString:@"KL"])||
                    ([inf.exchange isEqualToString:@"KLD"])||
                    ([inf.exchange isEqualToString:@"MY"])||
                    ([inf.exchange isEqualToString:@"PH"])||
                    ([inf.exchange isEqualToString:@"PHD"])||
                    ([inf.exchange isEqualToString:@"SG"])||
                    ([inf.exchange isEqualToString:@"SGD"]))  {
                    inf.isMDL = @"Y";
                    MDLfeature = 1;
                }
                
            }
        }
        
        
        
        int mdLevel = [MDValue intValue];
        if (mdLevel>=10) mdLevel = 10; // maximum field supported for nonMDL is 10 only.
        
        NSMutableArray *columnArr = [NSMutableArray arrayWithObjects:@"33",@"38",@"40",@"98",@"51",@"50",@"139",@"255",nil];
        
        int fid_buy_split_start = 170;
        int fid_sell_split_start = 180;
        int fid_buy_qty_start = 58;
        int fid_sell_qty_start = 78;
        int fid_buy_price_start = 68;
        int fid_sell_price_start = 88;
        
        // add mktdepth data FID based on level
        for (int i=0; i<mdLevel; i++) {
            // add FIDs
            NSString *fid_buy_qty = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
            NSString *fid_sell_qty = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
            NSString *fid_buy_price = [NSString stringWithFormat:@"%d", fid_buy_price_start+i];
            NSString *fid_sell_price = [NSString stringWithFormat:@"%d", fid_sell_price_start+i];
            NSString *fid_buy_split = [NSString stringWithFormat:@"%d", fid_buy_split_start+i];
            NSString *fid_sell_split = [NSString stringWithFormat:@"%d", fid_sell_split_start+i];
            
            [columnArr addObject:fid_buy_qty];
            [columnArr addObject:fid_sell_qty];
            [columnArr addObject:fid_buy_price];
            [columnArr addObject:fid_sell_price];
            [columnArr addObject:fid_buy_split];
            [columnArr addObject:fid_sell_split];
        }

        [sendMessage setObject:@"register" forKey:@"action"];
        [sendMessage setObject:@"marketDepth" forKey:@"listenId"];
        [sendMessage setObject:@[COLL_05_QUOTE] forKey:@"coll"];
        [sendMessage setObject:[NSNumber numberWithBool:NO] forKey:@"query"];
        [sendMessage setObject:columnArr forKey:@"column"];
        NSDictionary *filterDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"33",          @"field",
                                    @[stkCode],     @"value",
                                    @"in",          @"comparison",
                                    nil];
        [sendMessage setObject:@[filterDict] forKey:@"filter"];
        [sendMessage setObject:@[] forKey:@"column"];
        [sendMessage setObject:cmdCall forKey:@"eventId"];

        
    } else if (feature==1) {
        // MDL
        [sendMessage setObject:@"register" forKey:@"action"];
        [sendMessage setObject:@"marketDepth" forKey:@"listenId"];
        [sendMessage setObject:@[COLL_12_MARKETDEPTH] forKey:@"coll"];
        [sendMessage setObject:[NSNumber numberWithBool:NO] forKey:@"query"];
        NSDictionary *filterDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"33",          @"field",
                                    @[stkCode],     @"value",
                                    @"in",          @"comparison",
                                    nil];
        [sendMessage setObject:@[filterDict] forKey:@"filter"];
        [sendMessage setObject:@[] forKey:@"column"];
        [sendMessage setObject:cmdCall forKey:@"eventId"];
    }
    
    
    //// NSLog(@"SUBSCRIBE MD COMMAND FEATURE %d: %@ ", feature, sendMessage );
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    
}



-(void)vertxGetMarketDepth:(NSString *)stkCode andCollarr:(NSString *)colExchange
{
    
   // NSLog(@"EXCHG INFO: %@",[UserPrefConstants singleton].currentExchangeInfo.qcParams.exchgInfo);
    
    MarketDepthResults = [[NSMutableArray alloc]init];
    cmdCall= @"GETMARKETDEPTH";
    NSString *MDValue;
    int MDLfeature = 0;
    
    // OLD MDL CHECKING (VIA LMS)
//    for (ExchangeData *ed in [[UserPrefConstants singleton]userExchagelist]) {
//        //NSLog(@"ED: %@ %@",ed.feed_exchg_code, ed.mktDepthLevel);
//        if ([ed.feed_exchg_code isEqualToString:[[UserPrefConstants singleton] userCurrentExchange]]) {
//            MDValue =ed.mktDepthLevel;
//            
//            // override (KL, KLD, MY, SG, SGD) use MDL
//            // Info from Najma 12/Apr/2017
//            //
//            if (([ed.feed_exchg_code isEqualToString:@"KL"])||
//                ([ed.feed_exchg_code isEqualToString:@"KLD"])||
//                ([ed.feed_exchg_code isEqualToString:@"MY"])||
//                ([ed.feed_exchg_code isEqualToString:@"SG"])||
//                ([ed.feed_exchg_code isEqualToString:@"SGD"])) {
//                
//                
//                ed.MDLfeature = 1;
//                
//            }
//            MDLfeature = ed.MDLfeature;
//        }
//    }
    
    // NEW MDL CHECKING (VIA VERTX EXCHGINFO)
	//+++ re-check MDLevel
	ExchangeData *curExData = [UserPrefConstants singleton].currentExchangeData;//[Utils curExchangeDataFrom:[UserPrefConstants singleton].userExchagelist byExchange:[UserPrefConstants singleton].userCurrentExchange];
	MDLfeature = curExData.MDLfeature ? 1 : 0;
	MDValue = curExData.mktDepthLevel;
	/*
    ExchangeInfo *currInfo = [UserPrefConstants singleton].currentExchangeInfo;
    for (MDInfo *inf in currInfo.qcParams.exchgInfo) {
        
        if ([inf.exchange isEqualToString:[UserPrefConstants singleton].userCurrentExchange]) {
            
            MDLfeature = ([inf.isMDL isEqualToString:@"Y"]) ? 1: 0;
            MDValue = inf.mdLevel;
            
            // override (KL, KLD, MY, SG, SGD) use MDL
            //            // Info from Najma 12/Apr/2017
            
            NSLog(@"NEW MDL CHECKING");
            
                        if (([inf.exchange isEqualToString:@"KL"])||
                            ([inf.exchange isEqualToString:@"KLD"])||
                            ([inf.exchange isEqualToString:@"MY"])||
                            ([inf.exchange isEqualToString:@"PH"])||
                            ([inf.exchange isEqualToString:@"PHD"])||
                            ([inf.exchange isEqualToString:@"SG"])||
                            ([inf.exchange isEqualToString:@"SGD"]))  {
                            inf.isMDL = @"Y";
                            MDLfeature = 1;
                        }
        }
    }
	*/
    NSLog(@"MDLfeature Get %d",MDLfeature);
        
    if ([MDValue isEqualToString:@"0"]) {
        MarketDepthResults = [NSMutableArray array];
        responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:MarketDepthResults,@"marketdepthresults", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishMarketDepth" object:self userInfo:responseDict];
        return;
    }
    
    NSMutableDictionary *sendMessage= [[NSMutableDictionary alloc]init];
    
    if (MDLfeature==0) { // NON-MDL
        
        //NSLog(@"NON MDL");
        
        int mdLevel = [MDValue intValue];
        if (mdLevel>=10) mdLevel = 10; // maximum field supported is 10 only for non MDL
        
        // NSLog(@"mdLevel %d", mdLevel);
        
        NSMutableArray *columnArr = [NSMutableArray arrayWithObjects:@"33",@"38",@"40",@"98",@"51",@"50",@"139",@"255",nil];
        
        int fid_buy_split_start = 170;
        int fid_sell_split_start = 180;
        int fid_buy_qty_start = 58;
        int fid_sell_qty_start = 78;
        int fid_buy_price_start = 68;
        int fid_sell_price_start = 88;
        
        // add mktdepth data FID based on level
        for (int i=0; i<mdLevel; i++) {
            // add FIDs
            NSString *fid_buy_qty = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
            NSString *fid_sell_qty = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
            NSString *fid_buy_price = [NSString stringWithFormat:@"%d", fid_buy_price_start+i];
            NSString *fid_sell_price = [NSString stringWithFormat:@"%d", fid_sell_price_start+i];
            NSString *fid_buy_split = [NSString stringWithFormat:@"%d", fid_buy_split_start+i];
            NSString *fid_sell_split = [NSString stringWithFormat:@"%d", fid_sell_split_start+i];
            
            [columnArr addObject:fid_buy_qty];
            [columnArr addObject:fid_sell_qty];
            [columnArr addObject:fid_buy_price];
            [columnArr addObject:fid_sell_price];
            [columnArr addObject:fid_buy_split];
            [columnArr addObject:fid_sell_split];
        }
        
        [sendMessage setObject:@"query" forKey:@"action"];
        [sendMessage setObject:@[COLL_05_QUOTE] forKey:@"coll"]; // NON MDL is using STK DATA FORMAT
       
        [sendMessage setObject:columnArr forKey:@"column"];
        
        NSDictionary *filterDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"33",      @"field",
                                    stkCode,    @"value",
                                    @"eq",      @"comparison",
                                    nil];
        NSArray *filterArr = [NSArray arrayWithObjects:filterDict, nil];
        [sendMessage setObject:filterArr forKey:@"filter"];
        [sendMessage setObject:cmdCall forKey:@"eventId"];
        
        
    } else if (MDLfeature==1) { //  MDL
        
        // RESPONSE {"33":"5196.KL","45":"A12","58":"0","68":"0","105":"2","106":"2","107":"4","118":"55","131":"KL","170":"0"}
        //NSLog(@"MDL");
        
        [sendMessage setObject:@"query" forKey:@"action"];
        [sendMessage setObject:@[COLL_12_MARKETDEPTH] forKey:@"coll"];
        NSMutableArray *filterArr = [[NSMutableArray alloc]init];
        NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
        [filterDict setObject:@"33" forKey:@"field"];
        [filterDict setObject:stkCode forKey:@"value"];
        [filterDict setObject:@"eq" forKey:@"comparison"];
        [filterArr addObject:[filterDict copy]];
        [filterDict setObject:@"107" forKey:@"field"];
        [filterDict setObject:MDValue forKey:@"value"];
        [filterDict setObject:@"lt" forKey:@"comparison"];
        [filterArr addObject:[filterDict copy]];
        NSMutableArray *sortArr = [[NSMutableArray alloc]init];
        NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
        [sortDict setObject:@"107" forKey:@"property"];
        [sortDict setObject:@"ASC" forKey:@"direction"];
        [sortArr addObject:[sortDict copy]];
        [sendMessage setObject:filterArr forKey:@"filter"];
        [sendMessage setObject:sortArr forKey:@"sort"];
        [sendMessage setObject:cmdCall forKey:@"eventId"];

        
    }
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    //NSString* newStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"newStr for market depth %@, ",newStr);
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    //}
    
}



- (void)vertxGetBusinessDoneByStockCode:(NSString *)stkCode exchg:(NSString *)exchange
{
    cmdCall = @"GETBUSINESSDONE";
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"qc" forKey:@"action"];
    [sendMessage setObject:@"BusinessDone" forKey:@"req"];
    [sendMessage setObject:@"json" forKey:@"type"];
    NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:@"",[NSString stringWithFormat:@"0,%@,%@,10,10",exchange,stkCode], nil];
    [sendMessage setObject:params forKey:@"params"];
    [sendMessage setObject:exchange forKey:@"exch"];
    [sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
    eventIdCounter++;
    
    /// NSLog(@"getBusiness sendMessage : %@",sendMessage);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    
}

- (void)vertxGetTimeAndSalesByStockCode:(NSString *)stkCode begin:(NSString *)beginTranId end:(NSString *) endTranId exchange:(NSString *)exchange
{
    cmdCall = @"TIMEANDSALES";
    // Original Sting Call
    // {"action":"qc","req":"TRANSACT","type":"json","params":{"[T]":"8567.KL,0,0,0,1396,1496"},"exch":"KL","eventId":"36"}
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"qc" forKey:@"action"];
    [sendMessage setObject:@"TRANSACT" forKey:@"req"];
    [sendMessage setObject:@"json" forKey:@"type"];
    NSDictionary *params =[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@,0,0,0,%@,%@",stkCode,beginTranId,endTranId],@"[T]", nil];
    [sendMessage setObject:params forKey:@"params"];
    [sendMessage setObject:exchange forKey:@"exch"];
    [sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
    eventIdCounter++;
    
    // NSLog(@"get Time and sales sendMessage : %@",sendMessage);
    //    _msg = [NSString stringWithFormat:@"{\"action\":\"qc\",\"req\":\"TRANSACT\",\"type\":\"json\",\"params\":{\"[T]\":\"000100002.KL,0,0,0,%@,%@\"},\"exch\":\"KL\",\"eventId\":\"vertxGetIndicesTimeAndSales\"}",start,end];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    ////NSLog(@"%@", [NSString stringWithUTF8String:[jsonData bytes]]);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}


- (void)vertxGetStockNews:(NSString*)stkCode
{
    //    Send Message {"action":"qc","req":"News","type":"json","exch":"KL","params":{",3395.KL,,,,":""},"eventId":"25"}
    
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"qc" forKey:@"action"];
    [sendMessage setObject:@"News" forKey:@"req"];
    [sendMessage setObject:@"json" forKey:@"type"];
    [sendMessage setObject:[UserPrefConstants singleton].userCurrentExchange forKey:@"exch"];
    // NSArray *params = [[NSArray alloc] initWithObjects:@"",stkCode,@"",@"",@"",@{@"":@""}, nil];
    NSString * str = [NSString stringWithFormat:@",%@,,,,\"",stkCode];
    [sendMessage setObject:@{str:@""} forKey:@"params"];
    [sendMessage setObject:@"GetStockNews" forKey:@"eventId"];
    
    NSLog(@"vertxGetStockNews : %@",sendMessage);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
}

- (void)vertxCompanyNews : (NSString *)stkCode
{
    cmdCall = @"GetStockNews";
    _msg = [NSString stringWithFormat:@"{\"action\":\"qc\",\"req\":\"News\",\"type\":\"json\",\"exch\":\"%@\",\"params\":{\",%@,,,,\":\"\"},\"eventId\":\"GetStockNews\"}",[UserPrefConstants singleton].userCurrentExchange,stkCode];
    NSLog(@"vertxCompanyNews _msg : %@",_msg);    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}



#pragma  mark - QUOTE SCREEN

// NOT USED
- (void)vertxSortWithExchange:(NSString*)exchg sortFID:(NSString *)fid isDescending:(BOOL)isDesc
{
    cmdCall = @"SORTTOP";
//    [[[QCData singleton]qcFeedDataDict]removeAllObjects];
    
    NSString *directionStr ;
    if (isDesc) {
        directionStr = @"DESC";
    }
    else
        directionStr = @"ASC";
    
    NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
    
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    [filterDict setObject:@"38" forKey:@"field"];
    [filterDict setObject:@"" forKey:@"value"];
    [filterDict setObject:@"ne" forKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];
    
    [filterDict setObject:@"path" forKey:@"field"];
    [filterDict setObject:@"|10|" forKey:@"value"];
    [filterDict removeObjectForKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];
    
    [filterDict setObject:@"path" forKey:@"field"];
    [filterDict setObject:@"" forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
    
    NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
    [sortDict setObject:fid forKey:@"property"];
    [sortDict setObject:directionStr forKey:@"direction"];
    
    NSMutableArray *sortArr = [[NSMutableArray alloc]init];
    [sortArr addObject:sortDict];
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"]; //String
    [sendMessage setObject:@[COLL_05_QUOTE,COLL_07_TRANS_PUSH] forKey:@"coll"]; //Array
    [sendMessage setObject:[NSNumber numberWithInt:0] forKey:@"start"]; //int
    [sendMessage setObject:[NSNumber numberWithInt:10] forKey:@"limit"]; //int
    [sendMessage setObject:collumnArr forKey:@"column"]; //Array
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
    [sendMessage setObject:exchg forKey:@"exch"]; //String
    [sendMessage setObject:sortArr forKey:@"sort"]; //Array
    [sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];

    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    
    //Check
    //    Send Message {"action":"query",
    //                    "coll":["05"],
    //                    "start":0,
    //                    "limit":11,
    //                    "column":["33","38","36","98","48","88","51","134","135","50","40","35","39","139","103","238","56","57","58","68","78","101","153","156","49","245","140","141","142","244","243","666157","666134","666244","55","52","104","99","132","54","102","666135","41"],
    //                    "filter":[{"field":"38","value":"","comparison":"ne"},{"field":"path","value":"|10|"},{"field":"path","value":""}],
    //                    "exch":"KL",
    //                    "sort":[{"property":"101","direction":"DESC"}],
    //                    "eventId":"6"}
}



- (void)vertxUnsubscribeAllInQcFeed
{
    //Send Message {"action":"unregister","listenId":"listener05","eventId":"8"}
    
    for(NSString *listener in subscribedListenerArr)
    {
        NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
        [sendMessage setObject:@"unregister" forKey:@"action"];
        [sendMessage setObject:listener forKey:@"listenId"];
        [sendMessage setObject:[NSString stringWithFormat:@"%d",eventIdCounter]forKey:@"eventId"];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
    }
}

- (void)vertxSubscribeAllInQcFeed
{
    cmdCall = @"SUBSCRIBE";
    
    [self vertxUnsubscribeAllInQcFeed]; // Unsubscribe on every subscription.
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    // NSLog(@" SUBSCRIBE [[QCData singleton]qcFeedDataDict]allKeys]  : %@",[[[QCData singleton]qcFeedDataDict]allKeys] );

    if([[[QCData singleton]qcFeedDataDict]allKeys] !=nil)
    {
        NSMutableArray *valueArr = [[NSMutableArray alloc]initWithArray:[[[QCData singleton]qcFeedDataDict]allKeys] copyItems:YES];
        [filterDict setObject:@"33" forKey:@"field"];
        [filterDict setObject:valueArr forKey:@"value"];
        [filterDict setObject:@"in" forKey:@"comparison"];
        [filterArr addObject:[filterDict copy]];
        
        NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
        
        [sendMessage setObject:@"register" forKey:@"action"];
        [sendMessage setObject:[NSNumber numberWithBool:YES] forKey:@"query"];
        [sendMessage setObject:[NSString stringWithFormat:@"listener%d",listenerIdCounter] forKey:@"listenId"];
        [sendMessage setObject:@[COLL_05_QUOTE] forKey:@"coll"];
        // [sendMessage setObject:collumnArr forKey:@"column"]; //Array
        [sendMessage setObject:filterArr forKey:@"filter"];
        [sendMessage setObject:cmdCall forKey:@"eventId"];
         NSLog(@"subscribe sendMessage : %@",sendMessage);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        
        
        if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
        [subscribedListenerArr addObject:[NSString stringWithFormat:@"listener%d",listenerIdCounter]];
        eventIdCounter++;
        listenerIdCounter++;
		 [self subscribeTo06Vertx:filterArr andFilterDict:filterDict];
    }
}
- (void)subscribeTo06Vertx:(NSMutableArray *)filterArr andFilterDict:(NSMutableDictionary *)filterDict{

    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    
    [sendMessage setObject:@"query" forKey:@"action"];
    [sendMessage setObject:[NSNumber numberWithBool:YES] forKey:@"query"];
    [sendMessage setObject:[NSString stringWithFormat:@"listener%d",listenerIdCounter] forKey:@"listenId"];
    [sendMessage setObject:@[COLL_06_QUOTE] forKey:@"coll"];
    // [sendMessage setObject:collumnArr forKey:@"column"]; //Array
    [sendMessage setObject:filterArr forKey:@"filter"];
    [sendMessage setObject:@"GetStockCategory" forKey:@"eventId"];
    NSLog(@"subscribe sendMessage COLL_06_QUOTE: %@",sendMessage);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    [subscribedListenerArr addObject:[NSString stringWithFormat:@"listener%d",listenerIdCounter]];
    eventIdCounter++;
    listenerIdCounter++;
}

- (void)vertxUnsubscribeCustom
{
    for(NSString *listener in subscribedListenerArrCustom)
    {
        NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
        [sendMessage setObject:@"unregister" forKey:@"action"];
        [sendMessage setObject:listener forKey:@"listenId"];
        [sendMessage setObject:[NSString stringWithFormat:@"%d",eventIdCounter]forKey:@"eventId"];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
    }
}


//{
//    "not": [{
//        "or": [{
//                "field": "119",
//                "value": ["8",
//                      "16",
//                      "17",
//                      "19",
//                      "20",
//                      "31",
//                      "32",
//                      "35",
//                      "36",
//                      "37",
//                      "38",
//                      "39",
//                      "40",
//                      "41",
//                      "42"],
//                "comparison": "in"
//                },
//                {
//                   "and": [{
//                                "field": "119",
//                                "value": 4999,
//                                "comparison": "gt"
//                           },
//                           {
//                                "field": "119",
//                                "value": 10000,
//                                "comparison": "lt"
//                           }]
//                }]
//    }]
//},


/*
 TC PLUS LOGIC FOR MARKET/SECTOR
 -- info from Won Chin. July 2017
 
 function query({
 exch,
 market, // only work if coll = 05
 sectorList, // only work if coll = 05
 sector, // only work if coll = 05
 coll,
 stkList,
 config,
 column,
 readCompact = false
 }){
 const cfg = {
 action: 'query',
 coll: coll,
 start: 0,
 limit: 30,
 mode: 'compact',
 filter: []
 };
 if(config){
 _.defaults(cfg, config);
 }
 cfg.filter = [].concat(cfg.filter); // fix if it is not an array
 
 // additional configurations
 if(exch) cfg.exch = exch;
 if(column) cfg.column = column;
 
 if(market) {
 const marketInt = parseInt(market);
 if(marketInt>=1000){
 cfg.filter.push({"field": "36", comparison: "eq", value: market});
 } else {
 cfg.filter.push({"field": "151", comparison: "eq", value: market});
 }
 }
 if(stkList){
 if(_.isArray(stkList)){
 cfg.filter.push({"field": "33", comparison: "in", value: stkList})
 } else {
 cfg.filter.push({"field": "33", comparison: "eq", value: stkList})
 }
 }
 if(sectorList){
 const sectorListInt = parseInt(sectorList);
 if(sectorListInt<=2200 || sectorListInt>2299){
 deferred.reject('Invalid sector list ' + sectorList);
 return;
 }
 cfg.filter.push({"field": "35", comparison: "biton", value: 1<< (sectorListInt%2201)});
 }
 if(sector) cfg.filter.push({"field": "36", comparison: "eq", value: sector});
 
 return conn.request(cfg).then(function(r){
 if(!readCompact){return msgUncompact(r)} else {return r}
 });
 }
 
 */

//For Mobile Version: Sort Vertx with Sector filter
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange sectorFilter:(SectorInfo *)sectorFilter{
	cmdCall = @"SortByServer";
	
	NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
	NSMutableArray *filterArr = [[NSMutableArray alloc]init];
	NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
	[filterDict setObject:FID_38_S_STOCK_NAME forKey:@"field"];
	[filterDict setObject:@"" forKey:@"value"];
	[filterDict setObject:@"ne" forKey:@"comparison"];
	[filterArr addObject:[filterDict copy]];
	
	// NSString *formattedBoardLot = [NSString stringWithFormat:@"|%@|", [UserPrefConstants singleton].selectedMarket];
	NSString *marketStr = [UserPrefConstants singleton].selectedMarket;
	int marketInt = [marketStr intValue];
	
	if (marketInt>=1000) {
		[filterDict setObject:FID_36_I_SECTOR forKey:@"field"];
	} else {
		[filterDict setObject:FID_151_I_MARKET forKey:@"field"];
	}
	[filterDict setObject:@"eq" forKey:@"comparison"];
	[filterDict setObject:marketStr forKey:@"value"];
	[filterArr addObject:[filterDict copy]];
	
	// if filter by sector
	if (sectorFilter != nil) {
		int sectorCode = [sectorFilter.sectorCode intValue];
		if ((sectorCode>=2201)&&(sectorCode<=2299)) {
			[filterDict setObject:FID_35_I_INDEX forKey:@"field"];
			[filterDict setObject:@"biton" forKey:@"comparison"];
			[filterDict setObject:[NSNumber numberWithInt:(1<<(sectorCode%2201))] forKey:@"value"];
		} else {
			[filterDict setObject:FID_36_I_SECTOR forKey:@"field"];
			[filterDict setObject:@"eq" forKey:@"comparison"];
			[filterDict setObject:[NSNumber numberWithInt:sectorCode] forKey:@"value"];
		}
		[filterArr addObject:[filterDict copy]];
	}
	if ([UserPrefConstants singleton].enableWarrant) {
		NSDictionary *andDict = @{@"and":@[
										  @{@"field" : FID_119_S_TEXT_TYPE,@"value":[NSNumber numberWithInt:4999],@"comparison":@"gt"},
										  @{@"field" : FID_119_S_TEXT_TYPE,@"value":[NSNumber numberWithInt:10000],@"comparison":@"lt"}
										  ]};
		
		NSDictionary *fieldsDict = @{@"field": FID_119_S_TEXT_TYPE,
									 @"value":@[@"8",@"16",@"17",@"19",@"20",@"31",@"32",@"35",@"36",@"37",@"38",@"39",@"40",@"41",@"42"],
									 @"comparison": @"in"};
		
		NSDictionary *orDict = @{@"or": @[fieldsDict, andDict]};
		
		NSDictionary *notDict = @{@"not":@[orDict]};
		
		[filterArr addObject:notDict];
		
	}
	NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
	[sortDict setObject:property forKey:@"property"];
	[sortDict setObject:direction forKey:@"direction"];
	
	NSMutableArray *sortArr = [[NSMutableArray alloc]init];
	[sortArr addObject:sortDict];
	NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
	[sendMessage setObject:@"query" forKey:@"action"]; //String
	[sendMessage setObject:@[COLL_05_QUOTE,COLL_07_TRANS_PUSH] forKey:@"coll"]; //Array
	
	int startIndex = [UserPrefConstants singleton].quoteScrPage * 20;
	
	[sendMessage setObject:[NSNumber numberWithInt:startIndex] forKey:@"start"]; //int
	[sendMessage setObject:[NSNumber numberWithInt:20] forKey:@"limit"]; //int
	[sendMessage setObject:collumnArr forKey:@"column"]; //Array
	[sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
	[sendMessage setObject:exchange forKey:@"exch"]; //String
	[sendMessage setObject:sortArr forKey:@"sort"]; //Array
	[sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
	
	NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
													   options:NSJSONWritingPrettyPrinted
														 error: nil];
	NSLog(@"sortByServer sendMessage : %@",sendMessage);
	///NSLog(@"%@", [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding]);
	if([UserPrefConstants singleton].isConnected == YES && _webSocket != nil){
		[_webSocket send:jsonData];
	}else{
		DLog(@"Not yet connect socket");
	}
	eventIdCounter ++;
}

- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange{
    [self vertxSortByServer:property Direction:direction exchg:exchange keyword:@""];
}

- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange keyword:(NSString *)keyword
{
    cmdCall = @"SortByServer";

    NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc] initWithDictionary:@{@"field":FID_38_S_STOCK_NAME,
                                                                                        @"type":@"string",
                                                                                        @"comparison":@"regex",
                                                                                        @"value":[NSString stringWithFormat:@"(?i:^%@[\\s\\S]*)",keyword]}];
    [filterArr addObject:[filterDict copy]];
    
    
//    NSDictionary *filter4 = @{@"field":FID_38_S_STOCK_NAME,
//                              @"type":@"string",
//                              @"comparison":@"regex",
//                              @"value":[NSString stringWithFormat:@"(?i:^%@[\\s\\S]*)",str]};
    
   // NSString *formattedBoardLot = [NSString stringWithFormat:@"|%@|", [UserPrefConstants singleton].selectedMarket];
    NSString *marketStr = [UserPrefConstants singleton].selectedMarket;
    int marketInt = [marketStr intValue];
    
    if (marketInt>=1000) {
        [filterDict setObject:FID_36_I_SECTOR forKey:@"field"];
    } else {
        [filterDict setObject:FID_151_I_MARKET forKey:@"field"];
    }
    [filterDict setObject:@"eq" forKey:@"comparison"];
    [filterDict setObject:marketStr forKey:@"value"];
    [filterArr addObject:[filterDict copy]];

    // if filter by sector
    if ([UserPrefConstants singleton].sectorToFilter!=nil){
        
        int sectorCode = [[UserPrefConstants singleton].sectorToFilter.sectorCode intValue];
        if ((sectorCode>=2201)&&(sectorCode<=2299)) {
            [filterDict setObject:FID_35_I_INDEX forKey:@"field"];
            [filterDict setObject:@"biton" forKey:@"comparison"];
            [filterDict setObject:[NSNumber numberWithInt:(1<<(sectorCode%2201))] forKey:@"value"];
        } else {
            [filterDict setObject:FID_36_I_SECTOR forKey:@"field"];
            [filterDict setObject:@"eq" forKey:@"comparison"];
            [filterDict setObject:[NSNumber numberWithInt:sectorCode] forKey:@"value"];
        }
        [filterArr addObject:[filterDict copy]];
    }
    
    if ([UserPrefConstants singleton].enableWarrant) {
        
        NSDictionary *andDict = @{@"and":@[
                                          @{@"field" : FID_119_S_TEXT_TYPE,@"value":[NSNumber numberWithInt:4999],@"comparison":@"gt"},
                                          @{@"field" : FID_119_S_TEXT_TYPE,@"value":[NSNumber numberWithInt:10000],@"comparison":@"lt"}
                                          ]};
        
        NSDictionary *fieldsDict = @{@"field": FID_119_S_TEXT_TYPE,
                                     @"value":@[@"8",@"16",@"17",@"19",@"20",@"31",@"32",@"35",@"36",@"37",@"38",@"39",@"40",@"41",@"42", @"158", @"159"],
                                     @"comparison": @"in"};
        
        NSDictionary *orDict = @{@"or": @[fieldsDict, andDict]};
        
        NSDictionary *notDict = @{@"not":@[orDict]};
        
        [filterArr addObject:notDict];
    }
    
    
    NSMutableDictionary *sortDict = [[NSMutableDictionary alloc]init];
    [sortDict setObject:property forKey:@"property"];
    [sortDict setObject:direction forKey:@"direction"];
    
    NSMutableArray *sortArr = [[NSMutableArray alloc]init];
    [sortArr addObject:sortDict];
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"]; //String
    [sendMessage setObject:@[COLL_05_QUOTE,COLL_07_TRANS_PUSH] forKey:@"coll"]; //Array
    
    int startIndex = [UserPrefConstants singleton].quoteScrPage * kNUMBER_ITEMS_PER_PAGE;
    
    [sendMessage setObject:[NSNumber numberWithInt:startIndex] forKey:@"start"]; //int
    [sendMessage setObject:[NSNumber numberWithInt:kNUMBER_ITEMS_PER_PAGE] forKey:@"limit"]; //int
    [sendMessage setObject:collumnArr forKey:@"column"]; //Array
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
    [sendMessage setObject:exchange forKey:@"exch"]; //String
    [sendMessage setObject:sortArr forKey:@"sort"]; //Array
    [sendMessage setObject:cmdCall forKey:@"eventId"]; //For define incoming feed
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];
     NSLog(@"sortByServer sendMessage : %@",sendMessage);
    ///NSLog(@"%@", [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding]);
	if([UserPrefConstants singleton].isConnected == YES && _webSocket != nil){
		[_webSocket send:jsonData];
	}else{
		DLog(@"Not yet connect socket");
	}
    eventIdCounter ++;
}


#pragma mark - SEARCHING

// This not working. Not sure why. THis is more readable
// all method should be like this

- (void)vertxSearch2 : (NSString *)str
{
    cmdCall = @"vertxSearch";
    
    NSArray *collumnArr = @[
                            FID_38_S_STOCK_NAME,
                            FID_33_S_STOCK_CODE
                            ];

    
    NSDictionary *filter1 = @{@"field":FID_38_S_STOCK_NAME,
                              @"value":@"",
                              @"type":@"string",
                              @"comparison":@"ne"};
    
    NSDictionary *filter2 = @{@"field":FID_131_S_EXCHANGE,
                              @"value":@[@"MY",@"KL",@"JKD",@"SID",@"BKD",@"OD",@"ND",@"AD",@"HKD",@"PH"],
                              @"comparison":@"in"};
    
    NSDictionary *filter3 = @{@"field":@"path",
                              @"value":@"|10|"};
    
    NSDictionary *filter4 = @{@"field":FID_38_S_STOCK_NAME,
                              @"type":@"string",
                              @"comparison":@"regex",
                              @"value":[NSString stringWithFormat:@"(?i:^%@[\\s\\S]*)",str]};
    
    NSDictionary *sortDict = @{@"property":FID_33_S_STOCK_CODE,
                               @"direction":@"ASC"};
    
    NSDictionary *sendMessage =
    @{
      @"action":@"query",
      @"coll":@[COLL_05_QUOTE],
      @"start":@"0",
      @"limit":@"22",
      @"column":collumnArr,
      @"filter":@[filter1,filter2,filter3,filter4],
      @"sort":@[sortDict],
      @"eventId":cmdCall
      };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: nil];

     if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}

//+++ Search in exchange
- (void)getIDSSInfo:(NSString *)stockCode{
    _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"06\"],\"column\":[\"56\",\"57\",\"76\",\"74\",\"191\",\"101\",\"102\",\"33\"],\"filter\":[{\"field\":\"33\",\"value\":\"%@\",\"comparison\":\"eq\"}],\"eventId\":\"30\"}", stockCode];
    SearchString = stockCode;
    if([UserPrefConstants singleton].isConnected){
        [_webSocket send:_msg];
    }
}
- (void)vertxSearch:(NSString *)searchText fromExchange:(NSArray *)exchanges startPage:(NSInteger)startPage limit:(NSInteger)limit{
	int cnt = 0;
	long tot = [exchanges count];
	NSString *exchStr = @"";
	NSInteger startRec = startPage * kNUMBER_ITEMS_PER_PAGE;
	for (ExchangeData *dat in exchanges) {
		if (cnt<tot-1)
			exchStr = [NSString stringWithFormat:@"%@\"%@\",",exchStr,dat.feed_exchg_code];
		else
			exchStr = [NSString stringWithFormat:@"%@\"%@\"",exchStr,dat.feed_exchg_code];
		cnt++;
	}
	//_msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":%@,\"column\":[\"38\",\"36\",\"98\",\"48\",\"44\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"125\",\"232\",\"231\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\"],\"exch\":[%@],\"filter\":[{\"field\":\"33\",\"value\":\"\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"path\",\"value\":\"|10|\"},{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}],\"sort\":[{\"property\":\"stkCode\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec, @(limit), exchStr, searchText];
	
	 _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":%ld,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":[%@],\"filter\":[{\"field\":\"33\",\"value\":\"\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"path\",\"value\":\"|10|\"},{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}],\"sort\":[{\"property\":\"stkCode\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec, limit, exchStr, searchText];
	
	SearchString = searchText;
	if([UserPrefConstants singleton].isConnected){
		[_webSocket send:_msg];
	}
}

- (void)vertxSearchV3: (NSString *)str
{
//    [self vertxSearch:str];
//    return;
    int cnt = 0;
    long tot = [[UserPrefConstants singleton].userExchagelist count];
    NSString *exchStr = @"";
    for (ExchangeData *dat in [UserPrefConstants singleton].userExchagelist) {
        if (cnt<tot-1)
            exchStr = [NSString stringWithFormat:@"%@\"%@\",",exchStr,dat.feed_exchg_code];
        else
            exchStr = [NSString stringWithFormat:@"%@\"%@\"",exchStr,dat.feed_exchg_code];
        cnt++;
    }
    
    
    NSLog(@"resolved exchStr: %@", exchStr);
    
    //Previously hardcoded: [\"MY\",\"KL\",\"JKD\",\"SID\",\"BKD\",\"OD\",\"ND\",\"AD\",\"HKD\",\"PH\"]
    
    long startRec = [UserPrefConstants singleton].searchPage * 20;
    
    
    _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":20,\"column\":[\"38\",\"36\",\"98\",\"48\",\"44\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"125\",\"232\",\"231\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\"],\"exch\":[%@],\"filter\":[{\"field\":\"33\",\"value\":\"\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"path\",\"value\":\"|10|\"},{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}],\"sort\":[{\"property\":\"stkCode\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,exchStr,str];
    
    
    SearchString=str;
    
    NSLog(@"Sending _msg vertxSearch : %@",_msg);
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

- (void)vertxSearch : (NSString *)str
{
    int cnt = 0;
    long tot = [[UserPrefConstants singleton].userExchagelist count];
    NSString *exchStr = @"";
    for (ExchangeData *dat in [UserPrefConstants singleton].userExchagelist) {
        if (cnt<tot-1)
            exchStr = [NSString stringWithFormat:@"%@\"%@\",",exchStr,dat.feed_exchg_code];
        else
            exchStr = [NSString stringWithFormat:@"%@\"%@\"",exchStr,dat.feed_exchg_code];
        cnt++;
    }
    
   /* if (exchStr.length==0) {
        exchStr = [UserPrefConstants singleton].userCurrentExchange;
    }*/
    
    //exchStr = [NSString stringWithFormat:@"[\"%@\"]",exchStr];
    
    
    
     NSLog(@"resolved exchStr: %@", exchStr);
    
    //Previously hardcoded: [\"MY\",\"KL\",\"JKD\",\"SID\",\"BKD\",\"OD\",\"ND\",\"AD\",\"HKD\",\"PH\"]
    
    long startRec = [UserPrefConstants singleton].searchPage * 20;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        //  {\"field\":\"131\",\"value\":%@,\"comparison\":\"in\"}
  //  _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":20,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"filter\":[{\"field\":\"38\",\"value\":\".+\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"151\",\"value\":\"|10|\"},{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\":\"regex\",\"type\":\"string\"},{\"field\":\"33\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\":\"regex\",\"type\":\"string\"}]}],\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,exchStr,str];
    
   // _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\": %ld,\"limit\": 20,\"filter\": [{\"field\": \"38\",\"value\": \".+\",\"comparison\":\"regex\"},{\"or\": [{\"field\": \"38\",\"value\": \"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\": \"regex\"}]},{\"field\": \"151\",\"comparison\": \"eq\",\"value\": \"10\"}],\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,str,str,exchStr];
    
  /*  _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\": %ld,\"limit\": 20,\"filter\": [{\"field\": \"38\",\"value\": \".+\",\"comparison\":\"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"151\",\"comparison\": \"eq\",\"value\": \"10\"}],\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,str,exchStr];*/
    
    //{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}
    
    
   // \"filter\":[{\"field\":\"38\",\"value\":\".+\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"151\",\"value\":\"|10|\"},{\"or\":[{\"field\": \"38\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"}]}]
    
   // _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":20,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"filter\": [{\"field\": \"38\",\"value\": \".+\",\"comparison\": \"regex\"},{\"or\":[{\"field\": \"38\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"}]},{\"field\": \"151\",\"comparison\": \"eq\",\"value\": \"10\"}],\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,exchStr,str,str];

         _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":20,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":[%@],\"filter\":[{\"field\":\"33\",\"value\":\"\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"path\",\"value\":\"|10|\"},{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}],\"sort\":[{\"property\":\"stkCode\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,exchStr,str];
    
    
    SearchString=str;
    
    NSLog(@"Sending _msg vertxSearch : %@",_msg);

    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

- (void)vertxSearchStkCode : (NSString *)str
{
    
    int cnt = 0;
    long tot = [[UserPrefConstants singleton].userExchagelist count];
    NSString *exchStr = @"";
    for (ExchangeData *dat in [UserPrefConstants singleton].userExchagelist) {
        if (cnt<tot-1)
            exchStr = [NSString stringWithFormat:@"%@\"%@\",",exchStr,dat.feed_exchg_code];
        else
            exchStr = [NSString stringWithFormat:@"%@\"%@\"",exchStr,dat.feed_exchg_code];
        cnt++;
    }
    
    /* if (exchStr.length==0) {
     exchStr = [UserPrefConstants singleton].userCurrentExchange;
     }*/
    
    //exchStr = [NSString stringWithFormat:@"[\"%@\"]",exchStr];
    
    
    
    NSLog(@"resolved exchStr: %@", exchStr);
    
    //Previously hardcoded: [\"MY\",\"KL\",\"JKD\",\"SID\",\"BKD\",\"OD\",\"ND\",\"AD\",\"HKD\",\"PH\"]
    
    long startRec = [UserPrefConstants singleton].searchPage * 20;
    
    //  {\"field\":\"131\",\"value\":%@,\"comparison\":\"in\"}
    //  _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":20,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"filter\":[{\"field\":\"38\",\"value\":\".+\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"151\",\"value\":\"|10|\"},{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\":\"regex\",\"type\":\"string\"},{\"field\":\"33\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\":\"regex\",\"type\":\"string\"}]}],\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,exchStr,str];
    
    // _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\": %ld,\"limit\": 20,\"filter\": [{\"field\": \"38\",\"value\": \".+\",\"comparison\":\"regex\"},{\"or\": [{\"field\": \"38\",\"value\": \"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\": \"regex\"}]},{\"field\": \"151\",\"comparison\": \"eq\",\"value\": \"10\"}],\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,str,str,exchStr];
    
    /*  _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\": %ld,\"limit\": 20,\"filter\": [{\"field\": \"38\",\"value\": \".+\",\"comparison\":\"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\\\s\\\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"151\",\"comparison\": \"eq\",\"value\": \"10\"}],\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,str,exchStr];*/
    
    //{\"field\":\"38\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}
    
    
    // \"filter\":[{\"field\":\"38\",\"value\":\".+\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"151\",\"value\":\"|10|\"},{\"or\":[{\"field\": \"38\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"}]}]
    
    // _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":20,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":%@,\"filter\": [{\"field\": \"38\",\"value\": \".+\",\"comparison\": \"regex\"},{\"or\":[{\"field\": \"38\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"},{\"field\": \"33\",\"value\": \"(?i:^%@[\\s\\S]*)\",\"comparison\": \"regex\"}]},{\"field\": \"151\",\"comparison\": \"eq\",\"value\": \"10\"}],\"sort\":[{\"property\":\"33\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearch\"}",startRec,exchStr,str,str];
    
    _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":20,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\",\"131\"],\"exch\":[%@],\"filter\":[{\"field\":\"33\",\"value\":\"\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"path\",\"value\":\"|10|\"},{\"field\":\"33\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}],\"sort\":[{\"property\":\"131\",\"direction\":\"ASC\"},{\"property\":\"stkCode\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearchStkCode\"}",startRec,exchStr,str];
    
    
    SearchString=str;
    
    NSLog(@"Sending _msg vertxSearchStkCode : %@",_msg);
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

- (void)vertxSearchStkCodeDetail : (NSString *)str
{
    
    int cnt = 0;
    long tot = [[UserPrefConstants singleton].userExchagelist count];
    NSString *exchStr = @"";
    for (ExchangeData *dat in [UserPrefConstants singleton].userExchagelist) {
        if (cnt<tot-1)
            exchStr = [NSString stringWithFormat:@"%@\"%@\",",exchStr,dat.feed_exchg_code];
        else
            exchStr = [NSString stringWithFormat:@"%@\"%@\"",exchStr,dat.feed_exchg_code];
        cnt++;
    }
    exchStr = [NSString stringWithFormat:@"[%@]",exchStr];
    
    long startRec = 0;
    //{\"field\":\"131\",\"value\":%@,\"comparison\":\"in\"}
    _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"05\"],\"start\":%ld,\"limit\":15,\"column\":[\"38\",\"36\",\"98\",\"48\",\"51\",\"134\",\"135\",\"50\",\"40\",\"35\",\"39\",\"139\",\"103\",\"238\",\"56\",\"57\",\"101\",\"153\",\"156\",\"49\",\"245\",\"140\",\"141\",\"142\",\"244\",\"243\",\"55\",\"52\",\"104\",\"99\",\"132\",\"54\",\"102\",\"41\",\"buyRate\",\"33\",\"58\",\"68\",\"78\",\"88\",\"change\",\"changePer\",\"170\",\"171\",\"172\",\"173\",\"174\",\"58\",\"59\",\"60\",\"61\",\"62\",\"68\",\"69\",\"70\",\"71\",\"72\",\"88\",\"89\",\"90\",\"91\",\"92\",\"78\",\"79\",\"80\",\"81\",\"82\",\"180\",\"181\",\"182\",\"183\",\"184\"],\"exch\":[%@],\"filter\":[{\"field\":\"33\",\"value\":\"\",\"type\":\"string\",\"comparison\":\"ne\"},{\"field\":\"path\",\"value\":\"|10|\"},{\"field\":\"33\",\"value\":\"(?i:^%@[\\\\s\\\\S]*)\",\"type\":\"string\",\"comparison\":\"regex\"}],\"sort\":[{\"property\":\"stkCode\",\"direction\":\"ASC\"}],\"eventId\":\"vertxSearchStkCodeDetail\"}",startRec,exchStr,str];
    
    NSLog(@"Sending _msg vertxSearchStkCode: %@",_msg);
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}




// For use with watchlist orderbook and portfolio stocks
// TAG = 0 Watchlist
// TAG = 1 OrderBook
// TAG = 2 Portfolio
#pragma mark - Stock Details of a List (WatchList/OrderBook/PortFolio)
- (void)getDetailStock:(NSString *)stkCode{
	NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
	NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
	[sendMessage setObject:@"query" forKey:@"action"];
	[sendMessage setObject:@[COLL_05_QUOTE] forKey:@"coll"];
	[sendMessage setObject:collumnArr forKey:@"column"];
	
	NSMutableArray *filterArr = [[NSMutableArray alloc]init];
	NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
	[filterDict setObject:@"33" forKey:@"field"];
	[filterDict setObject:@"eq" forKey:@"comparison"];
	[filterDict setObject:stkCode forKey:@"value"];
	[filterArr addObject:[filterDict copy]];
	
	[sendMessage setObject:@"vertDetailOneStock" forKey:@"eventId"];
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
													   options:NSJSONWritingPrettyPrinted
														 error:nil];
	if([UserPrefConstants singleton].isConnected) {
		[_webSocket send:jsonData];
	}
}
- (void)vertxGetStockDetailWithStockArr:(NSArray *)stkCodeArr FIDArr:(NSArray *)collFIDArr tag:(int)tag{
    NSLog(@"stkCodeArr %@",stkCodeArr);
    
    NSArray *collumnArr = [[NSArray alloc]initWithArray:[[AppConstants SortingList] allKeys] copyItems:YES];
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"query" forKey:@"action"];
    [sendMessage setObject:@[COLL_05_QUOTE] forKey:@"coll"];
    [sendMessage setObject:collumnArr forKey:@"column"];
    
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
	[filterDict setObject:@"33" forKey:@"field"];
	[filterDict setObject:@"in" forKey:@"comparison"];
	[filterDict setObject:stkCodeArr forKey:@"value"];
	[filterArr addObject:[filterDict copy]];
	
	//Get detail: No need filtering
	/*
	[filterDict setObject:@"38" forKey:@"field"];
	[filterDict setObject:@"ne" forKey:@"comparison"];
    [filterDict setObject:@"" forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
	 
    [filterDict setObject:@"33" forKey:@"field"];
    [filterDict setObject:@"in" forKey:@"comparison"];
    [filterDict setObject:stkCodeArr forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
    */
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
    switch (tag) {
            
            // ATP getWatchlist
            case 0:
            [sendMessage setObject:@"vertxGetDetails" forKey:@"eventId"];
            break;
            
            // ATP getTradeStatus call this (tag:1)
            case 1:
            [sendMessage setObject:@"vertxGetStockInfo" forKey:@"eventId"];
            break;
            
            // ATP PreparePortfolio call this (tag:2)
            case 2:
            [sendMessage setObject:@"vertxGetStockInfoportfolio" forKey:@"eventId"];
            break;
            // ATP PreparePortfolio futures summary call this (tag:3)
        case 3:
            [sendMessage setObject:@"vertxGetFuturesInfoportfolio" forKey:@"eventId"];
            break;
            
        default:
            
            break;
    }
    
    NSLog(@"vertxGetStockDetailWithStockArr %@", sendMessage);

    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
	if([UserPrefConstants singleton].isConnected) {
		[_webSocket send:jsonData];
	}
}




//{
//    "action":"query",
//    "coll":["01"],
//    "filter":[{
//                "field":"131",
//                "value":"KL",
//                "comparison":"eq"}
//              ],
//    "eventId":"GetIndices"
//}

#pragma mark - INDICES

- (void)vertxSubscribeArr :(NSArray *)subscribeArr
{
    cmdCall = @"SUBSCRIBEINDICES";
    [self vertxUnsubscribeCustom];
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    [filterDict setObject:@"33" forKey:@"field"];
    [filterDict setObject:subscribeArr forKey:@"value"];
    [filterDict setObject:@"in" forKey:@"comparison"];
    [filterArr addObject:[filterDict copy]];
    
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    
    [sendMessage setObject:@"register" forKey:@"action"];
    [sendMessage setObject:[NSNumber numberWithBool:YES] forKey:@"query"];
    [sendMessage setObject:[NSString stringWithFormat:@"listener%d",listenerIdCounter] forKey:@"listenId"];
    [sendMessage setObject:@[COLL_01_SCOREBOARD] forKey:@"coll"];
    [sendMessage setObject:filterArr forKey:@"filter"];
    [sendMessage setObject:cmdCall forKey:@"eventId"];
    ////NSLog(@"subscribe custom sendMessage : %@",sendMessage);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    ////NSLog(@"%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    [subscribedListenerArrCustom addObject:[NSString stringWithFormat:@"listener%d",listenerIdCounter]];
    listenerIdCounter++;
}


- (void)vertxGetIndices {
	[self getIndicesFromExchange:[UserPrefConstants singleton].userCurrentExchange];
}

- (void)getIndicesFromExchange:(NSString *)exCode{
	[QCData singleton].qcIndicesDict = [[NSMutableDictionary alloc] init];
	
	////NSLog(@"%s", __PRETTY_FUNCTION__);
	_msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"01\"],\"filter\":[{\"field\":\"131\",\"value\":\"%@\",\"comparison\":\"eq\"}],\"eventId\":\"GetIndices\"}", exCode];
	//NSLog(@"vertxGetIndices _msg : %@",_msg);
	if([UserPrefConstants singleton].isConnected){
		[_webSocket send:_msg];
	}
}

#pragma mark - CURRENT EXCHANGE INDEX DATA

//{
//    "action":"query",
//    "coll":["01"],
//    "filter":
//        [{
//            "field":"131",
//            "value":"KL",
//            "comparison":"eq"
//        },
//         {
//             "field":"33",
//             "value":"020000000.KL",
//             "comparison":"eq"
//         }],
//    "eventId":"GetKLCI"
//}


//{
//    "action":"query",
//    "coll":["02"],
//    "filter":
//            [{
//                "field":"37",
//                "value":"TOTAL"
//            },
//             {
//                 "field":"131",
//                 "value":"KL",
//                 "comparison":"eq"
//             }],
//    "eventId":"GetKLCI2"
//}

- (void)getDetailIndices:(NSString *)indicesCode{
	_msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"01\"],\"filter\":[{\"field\":\"131\",\"value\":\"%@\",\"comparison\":\"eq\"},{\"field\":\"33\",\"value\":\"%@\",\"comparison\":\"eq\"}],\"eventId\":\"GetKLCI\"}", indicesCode, [FixedExchangeListIndex valueForKey:[UserPrefConstants singleton].userCurrentExchange]];
	if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

- (void)vertxGetKLCI
{
    // [QCData singleton].qcKlciDict = [[NSMutableDictionary alloc] init];    
    ////NSLog(@"%s", __PRETTY_FUNCTION__);
    _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"01\"],\"filter\":[{\"field\":\"131\",\"value\":\"%@\",\"comparison\":\"eq\"},{\"field\":\"33\",\"value\":\"%@\",\"comparison\":\"eq\"}],\"eventId\":\"GetKLCI\"}",[UserPrefConstants singleton].userCurrentExchange, [FixedExchangeListIndex valueForKey:[UserPrefConstants singleton].userCurrentExchange]];
    // NSLog(@"vertxGetKLCI Sending _msg : %@",_msg);
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

-(void)vertxGetKLCI2 {
    _msg = [NSString stringWithFormat:@"{\"action\":\"query\",\"coll\":[\"02\"],\"filter\":[{\"field\":\"37\",\"value\":\"TOTAL\"},{\"field\":\"131\",\"value\":\"%@\",\"comparison\":\"eq\"}],\"eventId\":\"GetKLCI2\"} ",[UserPrefConstants singleton].userCurrentExchange];
    
    //NSLog(@"vertxGetKLCI2 Sending _msg : %@",_msg);
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}


- (void)vertxSubscribeKLCI{
    ////NSLog(@"%s", __PRETTY_FUNCTION__);
    _msg = [NSString stringWithFormat:@"{\"action\":\"register\",\"listenId\":\"update01\",\"coll\":\"01\",\"query\":false,\"column\":[\"98\",\"51\",\"45\"],\"filter\":[{\"field\":\"131\",\"value\":[\"%@\"],\"comparison\":\"in\"},{\"field\":\"33\",\"value\":[\"%@\"],\"comparison\":\"in\"}],\"eventId\":\"vertxSubscribeKLCI\"}",[UserPrefConstants singleton].userCurrentExchange,[FixedExchangeListIndex valueForKey:[UserPrefConstants singleton].userCurrentExchange]];
 //   NSLog(@"Sending vertxSubscribeKLCI _msg : %@",_msg);

    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
    
//    _msg = [NSString stringWithFormat:@"{\"action\":\"register\",\"listenId\":\"update02\",\"coll\":\"02\",\"query\":false,\"column\":[\"105\",\"106\",\"107\",\"108\",\"110\",\"111\",\"133\",\"45\"],\"filter\":[{\"field\":\"131\",\"value\":[\"%@\"],\"comparison\":\"in\"},{\"field\":\"37\",\"value\":\"TOTAL\",\"comparison\":\"in\"}],\"eventId\":\"vertxSubscribeKLCI2\"}",[UserPrefConstants singleton].userCurrentExchange];
//   // NSLog(@"Sending vertxSubscribeKLCI _msg : %@",_msg);
//
//    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

-(void)vertxUnsubscribeKLCI {
    
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"unregister" forKey:@"action"];
    [sendMessage setObject:@"update01" forKey:@"listenId"];
    [sendMessage setObject:@"vertxSubscribeKLCI" forKey:@"eventId"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    
//    sendMessage = [[NSMutableDictionary alloc]init];
//    [sendMessage setObject:@"unregister" forKey:@"action"];
//    [sendMessage setObject:@"update02" forKey:@"listenId"];
//    [sendMessage setObject:@"vertxSubscribeKLCI2" forKey:@"eventId"];
//    jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
//                                                       options:NSJSONWritingPrettyPrinted
//                                                         error:nil];
//    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}


- (void) vertxGetMarketStreamer:(NSArray *)stockArray andValue:(int)value andQuantity:(int)quantity andCondition:(int)condition{
    [self vertxGetMarketStreamer:stockArray andValue:value andQuantity:quantity andCondition:condition exch:[UserPrefConstants singleton].userCurrentExchange];
}

- (void) vertxGetMarketStreamer:(NSArray *)stockArray andValue:(int)value andQuantity:(int)quantity andCondition:(int)condition exch:(NSString *)exch{

    int valueData = value == 0 ? 0 : value;
    int quantityData = quantity == 0 ? 0 : quantity;
    
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"register" forKey:@"action"];
    [sendMessage setObject:@[COLL_07_TRANS_PUSH] forKey:@"coll"];
    [sendMessage setObject:@"listenStreamer" forKey:@"listenId"];
    [sendMessage setObject:[NSString stringWithFormat:@"%@",[UserPrefConstants singleton].userCurrentExchange] forKey:@"exch"];
    
    NSMutableArray *filterArr = [[NSMutableArray alloc]init];
    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc]init];
    [filterDict setObject:@"100" forKey:@"field"];
    [filterDict setObject:@"gt" forKey:@"comparison"];
    [filterDict setObject:[NSString stringWithFormat:@"%d",valueData] forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
    
    [filterDict setObject:@"99" forKey:@"field"];
    [filterDict setObject:@"gt" forKey:@"comparison"];
    [filterDict setObject:[NSString stringWithFormat:@"%d",quantityData] forKey:@"value"];
    [filterArr addObject:[filterDict copy]];
    
    if (stockArray.count!=0) {
        [filterDict setObject:@"33" forKey:@"field"];
        [filterDict setObject:@"in" forKey:@"comparison"];
        [filterDict setObject:stockArray forKey:@"value"];
        [filterArr addObject:[filterDict copy]];
        
    }
    
    [sendMessage setObject:filterArr forKey:@"filter"]; //Dictionary
  
     NSLog(@"vertxGetMarketStreamer %@", sendMessage);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
    
    
   // _msg = [NSString stringWithFormat:@"{\"action\":\"register\",\"listenId\":\"listenStreamer\",\"exch\":\"%@\",\"coll\":[\"07\"],\"filter\":[{\"field\":\"100\",\"value\":%d,\"comparison\":\"lt\"},{\"field\":\"99\",\"value\":%d,\"comparison\":\"lt\"}],\"eventId\":\"vertxGetMarketStreamer\"}",[UserPrefConstants singleton].userCurrentExchange,valueData,quantityData];
    
    //_msg = [NSString stringWithFormat:@"{\"action\":\"register\",\"listenId\":\"listenStreamer\",\"exch\":\"%@\",\"coll\":[\"07\"],\"eventId\":\"vertxGetMarketStreamer\"}",[UserPrefConstants singleton].userCurrentExchange];
    //NSLog(@"vertxGetMarketStreamer %@",_msg);
    
    //if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

- (void) vertxUnsubscribeMarketStreamer{
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:@"unregister" forKey:@"action"];
    [sendMessage setObject:@"listenStreamer" forKey:@"listenId"];
    [sendMessage setObject:@"vertxGetMarketStreamer" forKey:@"eventId"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sendMessage
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:nil];
    if([UserPrefConstants singleton].isConnected) [_webSocket send:jsonData];
}

- (void)vertxSubscribeStreamer
{
    
    /*[sendMessage setObject:@"query" forKey:@"action"];
    [sendMessage setObject:@[COLL_05_QUOTE] forKey:@"coll"]; // NON MDL is using STK DATA FORMAT
    [sendMessage setObject:@"999999" forKey:@"coll"];
    [sendMessage setObject:columnArr forKey:@"limit"];
    
    NSDictionary *filterDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"33",      @"field",
                                @"",    @"value",
                                @"ne",      @"comparison",
                                nil];
    NSArray *filterArr = [NSArray arrayWithObjects:filterDict, nil];
    [sendMessage setObject:filterArr forKey:@"filter"];
      [sendMessage setObject:[NSS] forKey:@"coll"];
    [sendMessage setObject:cmdCall forKey:@"eventId"];*/
    
   
    
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
    
}


#pragma mark - PRIVATE METHODS

-(NSString*)returnString:(id)obj {
    NSString *result;
    
    if (obj==nil) {
        
        return @"-";
    }
    
    if ([obj isKindOfClass:[NSString class]]) {
        result = obj;
    } else {
        // string could be int or double
        NSNumber *num = (NSNumber*)obj;
        if((strcmp([num objCType], @encode(long))) == 0) {
            result = [NSString stringWithFormat:@"%ld",[num longValue]];
        } else if((strcmp([num objCType], @encode(double))) == 0) {
            result = [NSString stringWithFormat:@"%f",[num doubleValue]];
        }
    }
    
    
    
    return result;
    
}

-(NSNumber*)returnNumber:(id)obj {
    
    if (obj==nil) {
        
        return [NSNumber numberWithInt:0];
    }
    
    NSNumber *result;
    if ([obj isKindOfClass:[NSString class]]) {
        // string could be int or double
        NSString *str = (NSString*)obj;
        if ([str rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound)
        {
            if ([str rangeOfString:@"."].location != NSNotFound) {
                
                result = [NSNumber numberWithDouble:[str doubleValue]];
                
            } else {
                
                result = [NSNumber numberWithLongLong:[str longLongValue]];
            }
        }

    } else {
        result = obj;
    }
    
    
    return result;
}



-(NSMutableDictionary*)validateVertx2ScoreBoard:(NSDictionary*)dict {
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    id obj105 = [dict objectForKey:@"105"];
    id obj106 = [dict objectForKey:@"106"];
    id obj107 = [dict objectForKey:@"107"];
    id obj108 = [dict objectForKey:@"108"];
    id obj109 = [dict objectForKey:@"109"];
    id obj110 = [dict objectForKey:@"110"];
    id obj111 = [dict objectForKey:@"111"];
    id obj131 = [dict objectForKey:@"131"];
    id obj35 = [dict objectForKey:@"35"];
    id obj36 = [dict objectForKey:@"36"];
    id obj37 = [dict objectForKey:@"37"];
    
    [result setObject:[self returnNumber:obj105] forKey:@"105"];
    [result setObject:[self returnNumber:obj106] forKey:@"106"];
    [result setObject:[self returnNumber:obj107] forKey:@"107"];
    [result setObject:[self returnNumber:obj108] forKey:@"108"];
    [result setObject:[self returnNumber:obj109] forKey:@"109"];
    [result setObject:[self returnNumber:obj110] forKey:@"110"];
    [result setObject:[self returnNumber:obj111] forKey:@"111"];
    
    [result setObject:[self returnNumber:obj35] forKey:@"35"];
    [result setObject:[self returnNumber:obj36] forKey:@"36"];
    
    [result setObject:[self returnString:obj131] forKey:@"131"];
    [result setObject:[self returnString:obj37] forKey:@"37"];
    
    return result;
}

// ensure all fields' object type in vertx2 returns follow to vertx1 type
// so all code in other VC can be used.

// Vertx1 string object
// buyRate,change,path,pathName,sellRate,stkCode,stkNo

- (NSMutableDictionary*)validateVertx2ObjectTypes:(NSDictionary*)dict {
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    NSArray *fields = @[@33,@38,@39,@45,@48,@100,@104,@122,@130,@131,@134,@135,@147,@154,@156,@157,@231,@232,@244, @113];
    
    for (int i=33; i<=245; i++) {
        // must be string
        NSString *key = [NSString stringWithFormat:@"%d",i];
        
        
        if ([fields containsObject:[NSNumber numberWithInt:i]]) {
            id obj = [dict objectForKey:key];
             if (obj!=nil) {
                 if ([obj isKindOfClass:[NSString class]]) {
                     [result setObject:obj forKey:key];
                 } else {// if not string, set as string
                     [result setObject:[NSString stringWithFormat:@"%@",obj] forKey:key];
                 }
             } else {
                 // field not exist in dict, empty str
             }
        // must be integer/float or FIELD NOT EXIST
        } else {
            
            id obj = [dict objectForKey:key];
            if (obj!=nil) {
              
                if ([obj isKindOfClass:[NSString class]]) {
                    NSString *str = (NSString*)obj;
                    
                    if ([str rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound)
                    {
                       if ([str rangeOfString:@"."].location != NSNotFound) {
                           
                           [result setObject:[NSNumber numberWithDouble:[str doubleValue]] forKey:key];

                       } else {
                           
                           [result setObject:[NSNumber numberWithLongLong:[str longLongValue]] forKey:key];
                       }
                    }

                
                } else {// if not string, set as it is
                    [result setObject:obj forKey:key];
                }
            } else {
                // if field not exist, do nothing
            }
        }
    }
    
    // changePer is number
    NSArray *fieldStr = @[@"buyRate",@"change",@"changePer",@"path",@"pathName",@"sellRate",@"stkCode",@"stkNo"];
    
    for (int i=0; i<[fieldStr count]; i++) {
        NSString *key = [fieldStr objectAtIndex:i];
        id obj = [dict objectForKey:key];
        if (obj!=nil) {
            if ([key isEqualToString:@"changePer"]||[key isEqualToString:@"change"]) {
                if ([obj isKindOfClass:[NSString class]]) {
                    NSString *str = (NSString*)obj;
                    if ([str rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound)
                    {
                        if ([str rangeOfString:@"."].location != NSNotFound) {
                            
                            [result setObject:[NSNumber numberWithDouble:[str doubleValue]] forKey:key];
                            
                        } else {
                            
                            [result setObject:[NSNumber numberWithLongLong:[str longLongValue]] forKey:key];
                        }
                    }
                } else{
                    [result setObject:obj forKey:key];
                }
                
            } else {
                [result setObject:obj forKey:key];
            }
            
        } else {
            // field not exist, do nothing
           
        }
    }
    
  //  NSLog(@"dict result: %@ \n\n %@", dict, result);
    
    return result;
}

-(NSMutableDictionary*)validateVertx2ObjectTypesStreamer:(NSDictionary*)dict {
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    NSArray *fields = @[@33,@38,@39,@45,@48,@51,@54,@98,@99,@100,@103,@104,@120,@122,@130,@131,@134,@135,@147,@154,@156,@157,@231,@232,@244];
    
    for (int i=33; i<=245; i++) {
        // must be string
        NSString *key = [NSString stringWithFormat:@"%d",i];
        
        
        if ([fields containsObject:[NSNumber numberWithInt:i]]) {
            id obj = [dict objectForKey:key];
            if (obj!=nil) {
                if ([obj isKindOfClass:[NSString class]]) {
                    [result setObject:obj forKey:key];
                } else {// if not string, set as string
                    [result setObject:[NSString stringWithFormat:@"%@",obj] forKey:key];
                }
            } else {
                // field not exist in dict, empty str
            }
            // must be integer/float or FIELD NOT EXIST
        } else {
            
            id obj = [dict objectForKey:key];
            if (obj!=nil) {
                
                if ([obj isKindOfClass:[NSString class]]) {
                    NSString *str = (NSString*)obj;
                    
                    if ([str rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound)
                    {
                        if ([str rangeOfString:@"."].location != NSNotFound) {
                            
                            [result setObject:[NSNumber numberWithDouble:[str doubleValue]] forKey:key];
                            
                        } else {
                            
                            [result setObject:[NSNumber numberWithLongLong:[str longLongValue]] forKey:key];
                        }
                    }
                    
                    
                } else {// if not string, set as it is
                    [result setObject:obj forKey:key];
                }
            } else {
                // if field not exist, do nothing
            }
        }
    }
    
    // changePer is number
    NSArray *fieldStr = @[@"buyRate",@"change",@"changePer",@"path",@"pathName",@"sellRate",@"stkCode",@"stkNo"];
    
    for (int i=0; i<[fieldStr count]; i++) {
        NSString *key = [fieldStr objectAtIndex:i];
        id obj = [dict objectForKey:key];
        if (obj!=nil) {
            if ([key isEqualToString:@"changePer"]||[key isEqualToString:@"change"]) {
                if ([obj isKindOfClass:[NSString class]]) {
                    NSString *str = (NSString*)obj;
                    if ([str rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound)
                    {
                        if ([str rangeOfString:@"."].location != NSNotFound) {
                            
                            [result setObject:[NSNumber numberWithDouble:[str doubleValue]] forKey:key];
                            
                        } else {
                            
                            [result setObject:[NSNumber numberWithLongLong:[str longLongValue]] forKey:key];
                        }
                    }
                } else{
                    [result setObject:obj forKey:key];
                }
                
            } else {
                [result setObject:obj forKey:key];
            }
            
        } else {
            // field not exist, do nothing
            
        }
    }
    
    //  NSLog(@"dict result: %@ \n\n %@", dict, result);
    
    return result;
}




- (NSString *)generateRegStrWithSortDict:(NSDictionary *)dict;
{
    NSString *result = @"";
    for(NSString *str in [dict allKeys])
    {
        result = [result stringByAppendingString:[NSString stringWithFormat:@"\"%@\",",str]];
    }
    if(result!=nil)
        result = [result substringToIndex:[result length]-1];
    
    return result;
}

#pragma mark - Control Vertx
- (void)close{
	[_webSocket closeWithCode:kCloseLogoutCode reason:kReasonLogout];
}
- (void)connectingUponLogin{
	[self connect];
}


-(void)stopVertxTimerKeepAlive {
    if (_vertxTimer!=nil) {
        [_vertxTimer invalidate];
        _vertxTimer = nil;
    }
    
}

- (void)connect{
	if (self.connected) {
		return;
	}
    if (_vertxTimer!=nil) {
        [_vertxTimer invalidate];
        _vertxTimer = nil;
    }
	
    if ([UserPrefConstants singleton].Vertexaddress!=nil) {
		//Remove old socket
		_webSocket.delegate = nil;
		[_webSocket close];
		//Make new session
		//_webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"wss://ndgfv1.asiaebroker.com/eventbus/websocket"]]];
		
		//+++ NEW SOCKET
        if(IS_DumpmySocketSV){
            _webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kDumpmySocketURL]]];
        }else{
          _webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"wss://%@/eventbus/websocket", [UserPrefConstants singleton].Vertexaddress]]]];
        }
		
		_webSocket.delegate = self;
		//Can be use other dispatch Queue...
		//_webSocket setDelegateDispatchQueue:(dispatch_queue_t)
		[_webSocket open];
    }
}

- (void)reconnect{
	if ([UserSession shareInstance].didLogin) {
		if (!self.connected) {
			if ([NetworkManager shared].networkStatus != NotReachable) {
				DLog(@"+++ Reconnecting socket....");
				[self connect];
			}
		}
	}
}
- (void) openWebSocket{
    if (_vertxTimer!=nil) {
        [_vertxTimer invalidate];
        _vertxTimer = nil;
    }
    [_webSocket open];
}


/* Note: Oct 2016.
 sending data to websocket when no open connection is available will cause CRASH!.
 So everytime a new connection is establised (each broker points to different vertex address),
 we need to invalidate timer and reschedule it again in webSocketdidOpen.
 */

-(void) KeepAliveCommand
{
    _msg =@".";
    if([UserPrefConstants singleton].isConnected) [_webSocket send:_msg];
}

 /*
  NOTES ON VERTEX 2.0
  New vertex sends all data under "e" incomingType.
  And all data sent by "data" key.
 */


#pragma mark -
#pragma mark WebSocket Delegate
//====================================================================
//                        WebSocket Delegate
//====================================================================
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
	//Disable activity
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:message options:0 error:&localError];
    incomingType    = [ parsedObject objectForKey:@"type"];
    incomingEventId = [ parsedObject objectForKey:@"eventId"];
    incomingQid     = [[parsedObject objectForKey:@"qId"]stringValue];
    incomingMessage = [ parsedObject objectForKey:@"message"];
    incomingHasNext = [[parsedObject objectForKey:@"hasNext"]boolValue];
    incomingEnd     = [[parsedObject objectForKey:@"end"]boolValue];
	
    // Pay attention on data responses, there are several type of the response. Categorized by
    // Differenciate Data Call (EventID)
    // if has next, next coming data just have data and QID.
    // Every incoming have FeedType
    // incomingEventId is set during send msg to server (it can be any id or string, to identify the response from server)
    
    //NSLog(@"WebSocket Delegate incomingType : %@",incomingType);
      //NSLog(@"Parse Data %@", parsedObject);
    //This casse for
	
    if([incomingType isEqualToString:@"e"])
    {
        ////NSLog(@"Request type");
        if([incomingEventId isEqualToString:@"LOGIN"])
        {
            
            
        }
        else if ([incomingEventId isEqualToString:@"GETEXCHANGEINFO"]) {
            NSDictionary *dataDict = [[NSDictionary alloc] initWithDictionary:[parsedObject objectForKey:@"data"] copyItems:YES];
            if ([dataDict objectForKey:@"exchInfos"]!=nil) {
				[UserPrefConstants singleton].vertxExchangeInfo = @{}.mutableCopy;
                NSArray *allExchangeCodes = [[dataDict objectForKey:@"exchInfos"] allKeys];
                [allExchangeCodes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    NSDictionary *eachExchInfos = [[dataDict objectForKey:@"exchInfos"] objectForKey:(NSString*)obj];
                    ExchangeInfo *ei = [[ExchangeInfo alloc] init];
                    ei.exchCode = (NSString*)obj;
                    ei.indicesCode = [eachExchInfos objectForKey:@"indices"]; // indice code for this exchange
                    ei.marketInfos = [eachExchInfos objectForKey:@"marketInfos"];
                    ei.sectorInfos = [eachExchInfos objectForKey:@"sectorInfos"];
                    ei.sectorParents = [eachExchInfos objectForKey:@"sectorParents"];
                    ei.qcParams.indices = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[Indices]"];
                    ei.qcParams.encryption = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[Encryption]"];
                    ei.qcParams.qcType = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[QCType]"];
                    ei.qcParams.exDate = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[ExDate]"];
                    ei.qcParams.timeSync = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[TimeSync]"];
                    ei.qcParams.exchgDate = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[ExchgDate]"];
                    ei.qcParams.fullModeExchange = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[FullModeExchange]"];
                    ei.qcParams.sector = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[Sector]"];
                    ei.qcParams.session = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[Session]"];
                    ei.qcParams.unitOrLot = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[UnitOrLot]"];
                    ei.qcParams.userParams = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[UserParams]"];
                    ei.qcParams.aliveTimeOut = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[AliveTimeOut]"];
                    ei.qcParams.transactWithBrokerID = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[TransactWithBrokerID]"];
                    
                    NSArray *exchgInfoArr =  [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[ExchgInfo]"];
                    ei.qcParams.exchgInfo  = [[NSMutableArray alloc] init];
                    [exchgInfoArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        
                        NSString *mdStr = (NSString*)obj;
                        NSArray *mdArr = [mdStr componentsSeparatedByString:@","];
                        MDInfo *mdInfo = [[MDInfo alloc] init];
                        mdInfo.exchange = mdArr[0];
                        mdInfo.isMDL = mdArr[1];
                        mdInfo.mdLevel = mdArr[2];
                        [ei.qcParams.exchgInfo addObject:mdInfo];
                    }];
                    
                    ei.qcParams.exchange = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[Exchange]"];
                    ei.qcParams.version = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[Version]"];
                    ei.qcParams.frontEndLoadLocalData = [[eachExchInfos objectForKey:@"qcParams"] objectForKey:@"[FrontEndLoadLocalData]"];
                    [[UserPrefConstants singleton].vertxExchangeInfo setObject:ei forKey:(NSString*)obj];
                }];
				//For current Exchange Info
				NSString *userCurrentExchange = [UserPrefConstants singleton].userCurrentExchange;
				if (userCurrentExchange && [UserPrefConstants singleton].vertxExchangeInfo.count > 0) {
					[UserPrefConstants singleton].currentExchangeInfo = [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:userCurrentExchange];
				}
            } else {
                NSLog(@"GetExchangeInfo no data!");
            }
            //++++ Update to selected Default exchange here
			NSArray *supportedExchange = [UserPrefConstants singleton].supportedExchanges;
			if (supportedExchange.count > 0) {
				ExchangeData *defaultEx = [Utils getExchange:[SettingManager shareInstance].defaultExchangeCode];
				if (defaultEx) {
					[Utils selectedNewExchange:defaultEx];
				}else{
					ExchangeData *firstEx = supportedExchange.firstObject;
					[SettingManager shareInstance].defaultExchangeCode = firstEx.feed_exchg_code;
					[Utils selectedNewExchange:firstEx];
					[[SettingManager shareInstance] save];
				}
			}
            // send notification to LoginVC to remove loadingView.
            [[NSNotificationCenter defaultCenter] postNotificationName:@"vertxGetExchangeInfoFinished" object:self userInfo:nil];
            
        }
        else if ([incomingEventId isEqualToString:@"TIMEANDSALES"])
        {
            /// NSLog(@"Done TIMEANDSALES %@", parsedObject);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedTimeAndSales" object:self userInfo:parsedObject];
        }
        else if ([incomingEventId isEqualToString:@"GETBUSINESSDONE"])
        {
            //// NSLog(@"Done GETBUSINESSDONE ");
             [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedBusinessDone" object:self userInfo:parsedObject];
        }
        else if ([incomingEventId isEqualToString:@"GetStockNews"])
        {
            //NSLog(@"Done GetStockNews %@",parsedObject);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedGetStockNews" object:self userInfo:parsedObject];
            
             [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedGetAllStockNews" object:self userInfo:parsedObject];
            
           //NSLog(@"GetStockNews %@", parsedObject);
        }
        else if ([incomingEventId isEqualToString:@"vertxGetIndicesTimeAndSales"])
        {
            ////NSLog(@"Done vertxGetIndicesTimeAndSales");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedVertxGetIndicesTimeAndSales" object:self userInfo:parsedObject];
        }
       
        
        // ----------------------------------------------------------------------------------------------------
        //
        //                          MARK: NEW ADDITION FOR "e" VERTEX 2.0
        //
        // ----------------------------------------------------------------------------------------------------

        else if([incomingEventId isEqualToString:@"SortForTicker"])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]]; // array of stocknames (NSString)
            sortedresults = [NSMutableArray array];
            
            // NSLog(@"SortByHomeLeft %@",[tmpArray objectAtIndex:0]);
            
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [sortedresults addObject:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
            }
			
            responseDict = [NSMutableDictionary dictionary];
            //  [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
            responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:sortedresults,@"sortedresults", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortForTicker" object:self userInfo:responseDict];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            sortedresults=nil;
            [self vertxSubscribeAllInQcFeed];
            return;
        }
        
        else if([incomingEventId isEqualToString:@"SortByHomeLeft"])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]]; // array of stocknames (NSString)
            sortedresults = [NSMutableArray array];
            
            // NSLog(@"SortByHomeLeft %@",[tmpArray objectAtIndex:0]);
            
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [sortedresults addObject:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
            }
            
            // NSLog(@"QcFeed: %@",[[QCData singleton]qcFeedDataDict]);
            
            responseDict = [NSMutableDictionary dictionary];
            //  [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
            responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:sortedresults,@"sortedresults", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortTopLeft" object:self userInfo:responseDict];
			if (IS_IPHONE) {
				if (self.topRankCompletion) {
					self.topRankCompletion(responseDict, nil);
				}
			}
			
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            sortedresults=nil;
            [self vertxSubscribeAllInQcFeed];
            return;
        }
        else if([incomingEventId isEqualToString:@"GetStockCategory"]){
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            
           // NSLog(@"GetStockCategory %@",tmpArray);
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
           // NSLog(@"responseDict %@",responseDict);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"GetStockCategoryReceived" object:responseDict];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            //[self vertxSubscribeAllInQcFeed];
           // [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxGetScoreBoardWithExchange" object:nil];
            return;
        }
        else if([incomingEventId isEqualToString:@"SortByHomeRight"])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]]; // array of stocknames (NSString)
            sortedresults = [NSMutableArray array];
            
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [sortedresults addObject:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
            }
            responseDict = [NSMutableDictionary dictionary];
            //  [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
            responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:sortedresults,@"sortedresults", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortTopRight" object:self userInfo:responseDict];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            sortedresults=nil;
            [self vertxSubscribeAllInQcFeed];
            return;
            
        }
        
        // SCOREBOARD vtx2 not enough data sent (only 2201 and 2203, but no 2204) by CIMBSG
        // So CIMB SG dont have piechart (enable/disable by plist)
        
        else if ([incomingEventId isEqualToString:@"vertxGetScoreBoardWithExchange"])
        {
     
             //NSLog(@"vertxGetScoreBoardWithExchange %@", parsedObject);
            
             NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ScoreBoard:[tmpArray objectAtIndex:i]];
                [industryDict setObject:eachDict forKey:[eachDict objectForKey:@"37"]];
                [[[QCData singleton]qcScoreBoardDict]setObject:[industryDict copy] forKey:[eachDict objectForKey:@"35"]];
            }
            
          //  NSLog(@"qcScoreBoardDict %@", [[QCData singleton]qcScoreBoardDict]);
        
            [industryDict removeAllObjects];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxGetScoreBoardWithExchange" object:nil];
             return;
      }
        else if([incomingEventId isEqualToString:kVertxAPI_SortByServer])
        {
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]]; // array of stocknames (NSString)
            responseDict = [NSMutableDictionary dictionary];
            //NSLog(@"tmpArray %@",tmpArray);
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [sortedresults addObject:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortByServer" object:self userInfo:responseDict];
			//Completion Mobile
			if (IS_IPHONE) {
				if (self.sortByServerCompletion) {
					//Just return array result for Quote Screen
					self.sortByServerCompletion(tmpArray, nil);
				}
			}
			responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
			//Alway continue to get data
			[self vertxSubscribeAllInQcFeed];
            return;
        }
        else if ([incomingEventId isEqualToString:@"GETMARKETDEPTH"])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            
           // NSLog(@"parsedObject %@",parsedObject);
            
            responseDict = [NSMutableDictionary dictionary];
            MarketDepthResults = [NSMutableArray array];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [MarketDepthResults addObject:eachDict];
            }
            responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:MarketDepthResults,@"marketdepthresults", nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishMarketDepth" object:self userInfo:responseDict];
            
            // NSLog(@"GETMARKETDEPTH %@",responseDict);
            
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            MarketDepthResults=nil;
            return;
        }
        else if ([incomingEventId isEqualToString:@"vertxGetStockInfoportfolio"]) // Portfolio
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"eqtyUnrealPFReceived" object:self];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
			[self vertxSubscribeAllInQcFeed];
			
            return;
        }
        else if ([incomingEventId isEqualToString:@"vertxGetFuturesInfoportfolio"]) // Portfolio
        {
          /*  NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"futuresPFSummaryReceived" object:self];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;*/
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            
            NSLog(@"vertxGetFuturesInfoportfolio %@",tmpArray);
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            NSLog(@"responseDict %@",responseDict);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"futuresPFSummaryReceived" object:responseDict];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            [self vertxSubscribeAllInQcFeed];
            return;
        }
        
        else if ([incomingEventId isEqualToString:@"vertxGetStockInfo"]) // Orderbook /History
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"orderListReceived" object:self userInfo:nil];
			[[NSNotificationCenter defaultCenter] postNotificationName:kDidGetStockInfo_Notification object:nil userInfo:nil];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
			[self vertxSubscribeAllInQcFeed];
            return;
        }
        else if ([incomingEventId isEqualToString:@"vertxGetDetails"])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            //Send post notification
            NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceivedWatchListItems" object:self userInfo:notificationData];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            [self vertxSubscribeAllInQcFeed];
            return;
            
		} else if ([incomingEventId isEqualToString:@"vertDetailOneStock"]){
			DLog(@"***** vertDetailOneStock: %@", parsedObject);
//			NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
//			responseDict = [NSMutableDictionary dictionary];
//			for (long i=0; i<[tmpArray count]; i++) {
//				NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
//				[responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
//				[[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
//			}
//			//Send post notification
//			NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
//			[[NSNotificationCenter defaultCenter] postNotificationName:@"didReceivedWatchListItems" object:self userInfo:notificationData];
//			responseDict = nil;
//			incomingHasNext = false;
//			keepQid = nil;
//			[self vertxSubscribeAllInQcFeed];
			return;
		}else if([incomingEventId isEqualToString:@"SORTTOP"]){
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            //Send post notification
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortTop" object:self userInfo:responseDict];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            [self vertxSubscribeAllInQcFeed];
            return;
         
        }else if ([incomingEventId isEqualToString:@"GetKLCI"])
        {
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            
            
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [[[QCData singleton]qcKlciDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            
           //NSLog(@"GetKLCI reply %@", [[QCData singleton]qcKlciDict]);
            [self vertxGetKLCI2];
            
            incomingHasNext = false;
            keepQid = nil;
            return;

        }
        else if ([incomingEventId isEqualToString:@"GetKLCI2"])
        {
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            
            if ([tmpArray count]<=0) return;
            
            NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:0]];
            
            // update current data for current exchange
            [[[[QCData singleton]qcKlciDict] objectForKey:[[AppConstants FixedExchangeListIndex]
                                             valueForKey:[UserPrefConstants singleton].userCurrentExchange]]
                                             setValuesForKeysWithDictionary:eachDict];
            
            /// NSLog(@"GetKLCI2 reply %d %@", getKLCIcounter, [[QCData singleton]qcKlciDict]);
            //Send post notification
            
            [self vertxSubscribeKLCI];
            
            NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:[[QCData singleton]qcKlciDict]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetKLCI" object:self userInfo:notificationData];
            keepQid = nil;
            return;
        }
        else if ([incomingEventId isEqualToString:@"GetIndices"])
        {
            ///NSLog(@"DoneGetIndices %@",eventId);w
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcIndicesDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            //Send post notification
            
            NSString *incomingStockCode = [responseDict objectForKey:FID_33_S_STOCK_CODE];
            
            NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
            
            [[[[QCData singleton]qcIndicesDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:notificationData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetIndices" object:incomingStockCode userInfo:[NSDictionary dictionary]];
			if (IS_IPHONE) {
				dispatch_async(dispatch_get_main_queue(), ^{
					if (self.indicesCompletion) {
						self.indicesCompletion(incomingStockCode, nil);
					}
				});
			}
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
			[self vertxSubscribeArr:[[NSMutableArray alloc]initWithArray:[[[QCData singleton]qcIndicesDict]allKeys] copyItems:YES]];
            return;
        }
        else if ([incomingEventId isEqualToString:@"30"])
        {
            NSMutableArray *tmp = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            if(tmp.count > 0){
                responseDict = [[NSMutableDictionary alloc]initWithDictionary:tmp[0]];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kGetISDDInfo object:self userInfo:responseDict];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            // Note: no need to subscribe search results!
            //[self vertxSubscribeAllInQcFeed];
            return;
            
        }
        else if ([incomingEventId isEqualToString:@"vertxSearch"])
        {
            
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            
            //NSLog(@"vertxSearch %@",tmpArray);
            
            responseDict = [NSMutableDictionary dictionary];
            
             NSMutableArray *responseArray = [[NSMutableArray alloc] init];
            
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseArray addObject:eachDict];
                //[[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
          
            
             [responseDict setObject:responseArray forKey:FID_33_S_STOCK_CODE];
              NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
            
            if ([tmpArray count]>0)
                [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxSearch" object:self userInfo:notificationData];
            else
                [self vertxSearchStkCode:SearchString];
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            // Note: no need to subscribe search results!
            //[self vertxSubscribeAllInQcFeed];
            return;
            
        }
        else if ([incomingEventId isEqualToString:@"vertxSearchStkCode"])
        {
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            NSMutableArray *responseArray = [[NSMutableArray alloc] init];
          
            
            for (long i=0; i<[tmpArray count]; i++) {
                
                //NSLog(@"%ld = vertxSearchStkCode Result: %@",i,[tmpArray objectAtIndex:i]);
                
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseArray addObject:eachDict];
                
               // [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                
                
                //[[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            
            [responseDict setObject:responseArray forKey:FID_33_S_STOCK_CODE];
            
            NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxSearch" object:self userInfo:notificationData];
            
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            // Note: no need to subscribe search results!
            //[self vertxSubscribeAllInQcFeed];
            return;
        }else if([incomingEventId isEqualToString:@"vertxSearchStkCodeDetail"]){
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:[parsedObject objectForKey:@"data"]];
            responseDict = [NSMutableDictionary dictionary];
            for (long i=0; i<[tmpArray count]; i++) {
                NSDictionary *eachDict = [self validateVertx2ObjectTypes:[tmpArray objectAtIndex:i]];
                [responseDict setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
                [[[QCData singleton]qcFeedDataDict] setObject:eachDict forKey:[eachDict objectForKey:FID_33_S_STOCK_CODE]];
            }
            NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxSearchStock" object:self userInfo:notificationData];
            
            responseDict = nil;
            incomingHasNext = false;
            keepQid = nil;
            // Note: no need to subscribe search results!
            //[self vertxSubscribeAllInQcFeed];
            return;
        }else if([incomingEventId isEqualToString:@"getScreenerRating"]){
            //[UserPrefConstants singleton].vertxExchangeInfo = [[NSMutableDictionary alloc] init];
            // send notification to LoginVC to remove loadingView.
            [[NSNotificationCenter defaultCenter] postNotificationName:@"vertxGetScreenerRatingFinished" object:[parsedObject objectForKey:@"data"] userInfo:nil];
            
		} else if ([incomingEventId isEqualToString:kINCOMING_STOCK_CHART_EVENT_ID]){
			//Dlog post notification or use block here
			dispatch_async(dispatch_get_main_queue(), ^{
				NSDictionary *dataDict = parsedObject[@"data"];
				if (self.stockDataChartCompletion) {
					self.stockDataChartCompletion(dataDict, nil);
				}
				//+++For notification
				 [[NSNotificationCenter defaultCenter] postNotificationName:kDidLoadDataChartNotification object:dataDict userInfo:nil];
			});
		}
        incomingEventId = nil;
    }
    
    // data feed that comes in repeatedly (push by server) (ie. SUBSCRIBED)
    // "a" for V1 and V2, "u" are used for V1 only.
    // MARK: "A" and "U" INCOMING (SUBSCRIBED DATA)
    
    else if([incomingType isEqualToString:@"a"] || [incomingType isEqualToString:@"u"]  )
    {
        
     
        NSDictionary *dataDict = [[NSDictionary alloc] init];
        // then it is Vertx2
        if ([incomingQid length]<=0) {
            dataDict = [self validateVertx2ObjectTypesStreamer:[parsedObject objectForKey:@"data"]];
            
        // otherwise it is Vertx1
        } else {
            dataDict = [parsedObject objectForKey:@"data"];
        }
        // Here data feed type is denoted by FID 45. As for types that are recognized, refer QCConstants.h.
        
        // UPDATE DATA
        //NSLog(@"Data Dict %@",dataDict);
        // U12 is MDL update)
        if ([[dataDict objectForKey:FID_45_S_EX] isEqualToString:TYPE_U12_MARKET_DEPTH]) {

            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"mdDidUpdate"
                                                                object:nil userInfo:dataDict];
        }
        
        if ([[dataDict objectForKey:FID_45_S_EX] isEqualToString:TYPE_U07_TRANS_PUSH]) {
            //NSLog(@"Parse Data %@", dataDict);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"marketStreamerReceived"
                                                                object:nil userInfo:dataDict];
        }
        
        
        // FOR IMPROVEMENT IN FUTURE, BETTER USE LIKE THIS (note this code is not tested):
        
//        else if ([[dataDict objectForKey:FID_45_S_EX] isEqualToString:TYPE_U05_QUOTE]) {
//            NSString *incomingStockCode = [dataDict objectForKey:FID_33_S_STOCK_CODE];
//            if ([[[[QCData singleton]qcFeedDataDict] allKeys] containsObject:incomingStockCode]) {
//                [[[[QCData singleton]qcFeedDataDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:dataDict];
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"didUpdate" object:incomingStockCode];
//            }
//        } else if ([[dataDict objectForKey:FID_45_S_EX] isEqualToString:TYPE_U01_SCORE_BOARD]) {
//            NSString *incomingStockCode = [dataDict objectForKey:FID_33_S_STOCK_CODE];
//            [[[[QCData singleton]qcIndicesDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:dataDict];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetIndices" object:incomingStockCode userInfo:[NSDictionary dictionary]];
//            
//        // ADD DATA
//        } else if ([[dataDict objectForKey:FID_45_S_EX] isEqualToString:TYPE_A05_QUOTE]) {
//            if([parsedObject objectForKey:FID_33_S_STOCK_CODE]) {
//                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dataDict];
//                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[dataDict objectForKey:FID_33_S_STOCK_CODE]];
//            }
//        }
        
        
        // OLD CODE BELOW NOT GOOD DETECTION OF FEED TYPE ------------------------------------------------------------------------
        // -------------------------------------------------------------------------------------------------------------------
        NSString *incomingStockCode = [dataDict objectForKey:FID_33_S_STOCK_CODE];
        if([incomingStockCode isEqualToString:[[AppConstants FixedExchangeListIndex] valueForKey:
                                               [UserPrefConstants singleton].userCurrentExchange]] )
        {
           // NSLog(@"Goes to KLCI Dict");
            
            ////NSLog(@" Before %@", [[[QCData singleton]qcKlciDict]objectForKey:incomingStockCode]);
            [[[[QCData singleton]qcKlciDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:dataDict];
            //NSLog(@" after %@", [[[QCData singleton]qcKlciDict]objectForKey:incomingStockCode]);

            [[NSNotificationCenter defaultCenter] postNotificationName:@"subscribeKLCI" object:self userInfo:dataDict];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetKLCI" object:self userInfo:nil];
        }
        else if ([[dataDict objectForKey:@"37"] isEqual:@"TOTAL"])
        {
            [[[[QCData singleton]qcKlciDict] objectForKey:[[AppConstants FixedExchangeListIndex]
                                                           valueForKey:[UserPrefConstants singleton].userCurrentExchange]]
                                setValuesForKeysWithDictionary:[parsedObject objectForKey:@"data"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"subscribeKLCI" object:self userInfo:dataDict];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetKLCI" object:self userInfo:nil];
        }
        
            
        if ([[[[QCData singleton]qcFeedDataDict] allKeys] containsObject:incomingStockCode]||[[[[QCData singleton]qcIndicesDict]allKeys]containsObject:incomingStockCode])
        {
            
            
            if ([[[[QCData singleton]qcFeedDataDict] allKeys] containsObject:incomingStockCode]) {
                [[[[QCData singleton]qcFeedDataDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:dataDict];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"didUpdate" object:incomingStockCode];
            } else if ([[[[QCData singleton]qcIndicesDict] allKeys]containsObject:incomingStockCode]) {
                [[[[QCData singleton]qcIndicesDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:dataDict];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetIndices" object:incomingStockCode userInfo:[NSDictionary dictionary]];
            }
        } else {
            // else add into qcFeedDataDict
            if(dataDict) {
                if([parsedObject objectForKey:FID_33_S_STOCK_CODE]) {
                    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:dataDict]; //**important to set it to MutableDict for mod when receive new data.
                    [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[dataDict objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
                }
            }
        }
        
        
    }
    
    
    // MARK: "Q" INCOMING
    else if([incomingType isEqualToString:@"q"])
    {
        ////NSLog(@"Type = Query");
        ////NSLog(@"keepQid : %@",keepQid);

        if(incomingEventId) //Header coming in
        {
            keepQid = incomingEventId;
        }
        
        else //Data coming in
        {
            if([keepQid isEqualToString:@"SORTTOP"])
            {
                if(!responseDict)
                    responseDict = [NSMutableDictionary dictionary];
                // responseDict : collect data, send thru post notification to requester.
                
                if(incomingEnd)
                {
                    ////NSLog(@"Sort top Done");
                    //Send post notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortTop" object:self userInfo:responseDict];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    [self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                //Processing Sort Top
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                ////NSLog(@"tempDict SingleStock: %@",tempDict);
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
                tempDict = nil;
            }
            
            else if ([keepQid isEqualToString:@"vertxGetScoreBoardWithExchange"])
            {
                if(incomingEnd)
                {
                    ////NSLog(@"Done vertxGetScoreBoardWithExchange %@",eventId);
                    ////NSLog(@"%@",industryDict );
                    [industryDict removeAllObjects];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxGetScoreBoardWithExchange" object:nil];
                    
                }
            
                else
                {
                    NSMutableDictionary *tempDictDetail = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                    [industryDict setObject:tempDictDetail forKey:[tempDictDetail objectForKey:@"37"]];
                    [[[QCData singleton]qcScoreBoardDict]setObject:[industryDict copy] forKey:[tempDictDetail objectForKey:@"35"]];
                }
            }
            else if ([keepQid isEqualToString:@"30"])
            {
                if(!responseDict)
                    responseDict = [NSMutableDictionary dictionary];
                if(incomingEnd)
                {
                    ////NSLog(@"Done vertxSearch %@",eventId);
                    //Send post notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:kGetISDDInfo object:self userInfo:responseDict];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    // Note: No need to subscribe search results!
                    //[self vertxSubscribeAllInQcFeed];
                    return;
                }
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                //[[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for response dict (Temp);
                tempDict = nil;
            }
            else if ([keepQid isEqualToString:@"vertxSearch"])
            {
                if(!responseDict)
                    responseDict = [NSMutableDictionary dictionary];
                if(incomingEnd)
                {
                    ////NSLog(@"Done vertxSearch %@",eventId);
                    //Send post notification
                    
                    NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
                    if ([responseDict count]>0)
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxSearch" object:self userInfo:notificationData];
                    else
                        [self vertxSearchStkCode:SearchString];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    // Note: No need to subscribe search results!
                    //[self vertxSubscribeAllInQcFeed];
                    return;
                }
                    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                    //[[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter
                    [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for response dict (Temp);
                    tempDict = nil;
            }
            else if ([keepQid isEqualToString:@"vertxSearchStkCode"])
            {
                if(!responseDict)
                    responseDict = [NSMutableDictionary dictionary];
                if(incomingEnd)
                {
                    ////NSLog(@"Done vertxSearch %@",eventId);
                    //Send post notification
                    NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
                   // [[NSNotificationCenter defaultCenter] postNotificationName:@"doneVertxSearch" object:self userInfo:notificationData];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    // Note: No need to subscribe search results!
                   // [self vertxSubscribeAllInQcFeed];
                    return;
                    
                }
               
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                //[[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for response dict (Temp);
                tempDict = nil;
            }
            else if ([keepQid isEqualToString:@"vertxGetDetails"])
            {
                if(!responseDict)
                {
                    responseDict = [NSMutableDictionary dictionary];
                }
                if(incomingEnd)
                {
                    //Send post notification
                    NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceivedWatchListItems" object:self userInfo:notificationData];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    [self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                ///NSLog(@"vertxGetDetails tempDict : %@",tempDict);
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for response dict (Temp);
                tempDict = nil;
                
            }
            else if ([keepQid isEqualToString:@"vertxGetStockInfo"])
            {
                if(!responseDict)
                {
                    responseDict = [NSMutableDictionary dictionary];
                }
                if(incomingEnd)
                {
                    // Send post notification
                    // NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"orderListReceived" object:self userInfo:nil];
					[[NSNotificationCenter defaultCenter] postNotificationName:kDidGetStockInfo_Notification object:nil userInfo:nil];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    [self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                ////NSLog(@"vertxGetDetails tempDict : %@",tempDict);
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for response dict (Temp);
                tempDict = nil;
                
            }
            else if ([keepQid isEqualToString:@"vertxGetStockInfoportfolio"])
            {
                if(!responseDict)
                {
                    responseDict = [NSMutableDictionary dictionary];
                }
                if(incomingEnd)
                {
                    // Send post notification
                    // NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"eqtyUnrealPFReceived" object:self];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    [self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
               //// NSLog(@"vertxGetDetails tempDict : %@",[tempDict objectForKey:@"38"]);
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for response dict (Temp);
                tempDict = nil;
                
            }

            else if ([keepQid isEqualToString:@"GETMARKETDEPTH"])
            {
                
                //NSLog(@"parsedObject Dict: %@",parsedObject);
                
                if(!responseDict)
                {
                    responseDict = [NSMutableDictionary dictionary];
                    MarketDepthResults = [NSMutableArray array];
                }
                if(incomingEnd)
                {
                    
                    responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:MarketDepthResults,@"marketdepthresults", nil];
                   /// NSLog(@"marketdepthresults %@",responseDict);
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishMarketDepth" object:self userInfo:responseDict];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    MarketDepthResults=nil;

                }
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]];
                [MarketDepthResults addObject:tempDict];
                tempDict=nil;
                
            }
            
        
            else if ([keepQid isEqualToString:@"SortByServer"])
            {
                if(!responseDict) {
                    responseDict = [NSMutableDictionary dictionary];
                // responseDict : collect data, send thru post notification to requester.
                    
                }
                
                if(incomingEnd) {
                    ////NSLog(@"Sort top Done");
                    //Send post notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortByServer"
                                                                        object:self userInfo:responseDict];
					//+++ For mobile
					if (IS_IPHONE) {
						if (self.sortByServerCompletion) {
							self.sortByServerCompletion(responseDict, nil);
						}
					}
					responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
					[self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                //Processing Sort Top
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
                tempDict = nil;
				//+++ For mobile: Case nil
				if (IS_IPHONE) {
					if (self.sortByServerCompletion) {
						self.sortByServerCompletion(responseDict, nil);
					}
				}
            }
            else if([keepQid isEqualToString:@"SortForTicker"])
            {
                if(!responseDict)
                {
                    responseDict = [NSMutableDictionary dictionary];
                    sortedresults = [NSMutableArray array];
                }
                // responseDict : collect data, send thru post notification to requester.
                
                if(incomingEnd)
                {
                    ////NSLog(@"Sort top left Done");
                    //Send post notification
                    
                    
                    
                    responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:sortedresults,@"sortedresults", nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortForTicker" object:self userInfo:responseDict];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    sortedresults=nil;
                    [self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                //Processing Sort Top
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
                [sortedresults addObject:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]];
                tempDict = nil;
            }
            else if([keepQid isEqualToString:@"SortByHomeLeft"])
            {
                if(!responseDict)
                {
                    responseDict = [NSMutableDictionary dictionary];
                    sortedresults = [NSMutableArray array];
                }
                // responseDict : collect data, send thru post notification to requester.
                
                if(incomingEnd)
                {
                    ////NSLog(@"Sort top left Done");
                    //Send post notification
                    
                    
                    
                    responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:sortedresults,@"sortedresults", nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortTopLeft" object:self userInfo:responseDict];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    sortedresults=nil;
                    [self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                //Processing Sort Top
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
                [sortedresults addObject:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]];
                tempDict = nil;
            }
            else if([keepQid isEqualToString:@"SortByHomeRight"])
            {
                if(!responseDict)
                {
                    responseDict = [NSMutableDictionary dictionary];
                    sortedresults = [NSMutableArray array];
                }
                
                // responseDict : collect data, send thru post notification to requester.
                
                if(incomingEnd)
                {
                    ////NSLog(@"Sort top right Done");
                    //Send post notification
                    responseDict= [NSMutableDictionary dictionaryWithObjectsAndKeys:sortedresults,@"sortedresults", nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishedSortTopRight" object:self userInfo:responseDict];
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    sortedresults=nil;
                    [self vertxSubscribeAllInQcFeed];
                    return;
                }
                
                //Processing Sort Top
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                [[[QCData singleton]qcFeedDataDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For QCDataCenter
                //[responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //For temp response dict.
                [sortedresults addObject:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]];
                tempDict = nil;
            }
            
            else if ([keepQid isEqualToString:@"GetKLCI"])
            {
                if(incomingEnd)
                {
                    ////NSLog(@"DoneGetKLCI %@",eventId);
                    incomingHasNext = false;
                    keepQid = nil;
                    [self vertxGetKLCI2];
                    
                    //NSLog(@"qcKLCI tempDict %@",[[QCData singleton]qcKlciDict]);
                    return;
                    
                }
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
               // NSLog(@"DoneGetKLCI tempDict %@",tempDict);
                
               
                [[[QCData singleton]qcKlciDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter's KLCI
                tempDict = nil;
            }
            else if ([keepQid isEqualToString:@"GetKLCI2"])
            {
                ////NSLog(@"DoneGetKLCI %@",eventId);
              //  NSLog(@"responseDict GetKLCI2: %@",[parsedObject objectForKey:@"data"]);
                
                [[[[QCData singleton]qcKlciDict] objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]] setValuesForKeysWithDictionary:[parsedObject objectForKey:@"data"]];
                
                
                
                //This is Replace + Adding. **setValuesForKeysWithDictionary
                /// NSLog(@"[[QCData singleton]qcKlciDict] : %@",[[QCData singleton]qcKlciDict]);
                //Send post notification
                [self vertxSubscribeKLCI];
                NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:[[QCData singleton]qcKlciDict]];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetKLCI" object:self userInfo:notificationData];
                keepQid = nil;

            }
            
            else if ([keepQid isEqualToString:@"GetIndices"])
            {
                ////NSLog(@"DoneGetIndices %@",eventId);w

                if(!responseDict)
                    responseDict = [NSMutableDictionary dictionary];
                if(incomingEnd)
                {
                    
                    //Send post notification
                    NSMutableDictionary *notificationData = [NSMutableDictionary dictionaryWithDictionary:responseDict];
                    
                    
                    NSString *incomingStockCode = [responseDict objectForKey:FID_33_S_STOCK_CODE];
                    
                   
                    
                    [[[[QCData singleton]qcIndicesDict] objectForKey:incomingStockCode] setValuesForKeysWithDictionary:notificationData];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DoneGetIndices" object:incomingStockCode userInfo:[NSDictionary dictionary]];
                    
                    responseDict = nil;
                    incomingHasNext = false;
                    keepQid = nil;
                    [self vertxSubscribeArr:[[NSMutableArray alloc]initWithArray:[[[QCData singleton]qcIndicesDict]allKeys] copyItems:YES]];
                    return;
                    
                }
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]initWithDictionary:[parsedObject objectForKey:@"data"]]; //**important to set it to MutableDict for mod when receive new data.
                [[[QCData singleton]qcIndicesDict] setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for QCDataCenter
                [responseDict setObject:tempDict forKey:[[parsedObject objectForKey:@"data"]objectForKey:FID_33_S_STOCK_CODE]]; //This is for response dict (Temp);
                tempDict = nil;
            }

        }
    }
}

//=========== Status Socker ====================
- (void)webSocketDidOpen:(SRWebSocket *)webSocket{
	[UserPrefConstants singleton].isConnected = YES;
	self.connected = YES;
	//Send post notification
	[[NSNotificationCenter defaultCenter] postNotificationName:@"pnWebSocketConnected" object:self userInfo:nil];
	[self vertxLogin];
	if ([UserPrefConstants singleton].vertxExchangeInfo.count == 0) {
		DLog(@"++++++++++********=======: GET EXCHANGE INFO");
		[self vertxGetExchangeInfo];
	}
	if ([[[[QCData singleton]qcFeedDataDict] allKeys] count] > 0) {
		[self performSelector:@selector(vertxSubscribeAllInQcFeed) withObject:nil afterDelay:2.0];
	}
	if (_vertxTimer==nil)
		_vertxTimer = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(KeepAliveCommand) userInfo:nil repeats:YES];
}
- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
	NSLog(@"====== Socket Error: Lost connection");
    _webSocket = nil;
	[UserPrefConstants singleton].isConnected = NO;
	self.connected = NO;
	_socketError = error;
	
	//Make reconnect
	if (IS_IPAD) {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"pnWebSocketDisconnected" object:self userInfo:nil];
		[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(reconnect) userInfo:nil repeats:NO];
	}else{
		[self performSelector:@selector(reconnect) withObject:nil afterDelay:2.0];
		NSError *errorSocket = [self createErrorSocket];
		//Just show 'No connection' with socket while lost internet connection
		if ([NetworkManager shared].networkStatus == NotReachable) {
			[[NSNotificationCenter defaultCenter] postNotificationName:@"pnWebSocketDisconnected" object:errorSocket userInfo:nil];
		}
		//[[NSNotificationCenter defaultCenter] postNotificationName:@"pnWebSocketDisconnected" object:errorSocket userInfo:nil];
	}
}
- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
	//Make reconnect
	self.connected = NO;
	[UserPrefConstants singleton].isConnected = NO;
	if (IS_IPAD) {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"pnWebSocketDisconnected" object:self userInfo:nil];
		[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(reconnect) userInfo:nil repeats:NO];
	}else{
		//Just make re-connect when user did login
		if ([UserSession shareInstance].didLogin == YES) {
			if (code != kCloseLogoutCode && ![reason isEqualToString:kReasonLogout]) {
				if ([NetworkManager shared].networkStatus == NotReachable) {
					[[NSNotificationCenter defaultCenter] postNotificationName:@"pnWebSocketDisconnected" object:[self createErrorSocket] userInfo:nil];
				}
				[self performSelector:@selector(reconnect) withObject:nil afterDelay:2.0];
			}else{
				DLog(@"++Nothing to do");
				//App did Logout or force close
			}
		}
	}
}

#pragma mark - UTILS
- (NSError *)createErrorSocket{
	if (_socketError) {
		return _socketError;
	}
	NSDictionary *userInfo = @{
							   NSLocalizedDescriptionKey: [LanguageManager stringForKey:@"Unable to establish connection, please try again shortly"],
							   NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The operation timed out.", nil),
							   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Please check your internet connection", nil)
							   };
	return [NSError errorWithDomain:[UserPrefConstants singleton].userCurrentExchange code:RESP_CODE_EXCEPTION_ERROR_SOCKET userInfo:userInfo];
}
- (void)setCompleteAllTaskWithError:(NSError *)error{
	if (IS_IPHONE) {
		//Create error object
		NSError *theError = [self createErrorSocket];
		
		if (self.sortByServerCompletion) {
			self.sortByServerCompletion(nil, theError);
		}
		if (self.topRankCompletion) {
			self.topRankCompletion(nil, theError);
		}
	}
}

#pragma mark - Main Action for Mobile
//1: @"SortByServer"
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange completion:(TCVertxCompletionHandler)completion{
    [self vertxSortByServer:property Direction:direction exchg:exchange keyword:@"" completion:completion];
}
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange keyword:(NSString *)keyword completion:(TCVertxCompletionHandler)completion{
	if ([UserPrefConstants singleton].isConnected == NO) {
		completion(nil, [self createErrorSocket]);
		return;
	}
	[self vertxSortByServer:property Direction:direction exchg:exchange keyword:keyword];
	self.sortByServerCompletion = completion;
	completion = nil;
}

//1.1: SortByServer with Sector Filter
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange sectorFilter:(SectorInfo *)filterInfo completion:(TCVertxCompletionHandler)completion{
	if ([UserPrefConstants singleton].isConnected == NO) {
		completion(nil, [self createErrorSocket]);
		return;
	}
	[self vertxSortByServer:property Direction:direction exchg:exchange sectorFilter:filterInfo];
	self.sortByServerCompletion = completion;
	completion = nil;
}

//2:SortByLeft
- (void)vertxSortByLeft:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange completion:(TCVertxCompletionHandler)completion{ //Get 10 items **Notice the "limit"
	if ([UserPrefConstants singleton].isConnected == NO) {
		completion(nil, [self createErrorSocket]);
		return;
	}
	[self vertxSortByLeft:property Direction:direction exchg:exchange];
	self.topRankCompletion = completion;
	completion = nil;
}

//3: Get Indices
- (void)getIndicesFromExchange:(NSString *)exCode completion:(TCVertxCompletionHandler)completion{
	if ([UserPrefConstants singleton].isConnected == NO) {
		completion(nil, [self createErrorSocket]);
		return;
	}
	[self getIndicesFromExchange:exCode];
	if (completion) {
		self.indicesCompletion = completion;
		completion = nil;
	}
}

#pragma mark - Handle Notification
- (void)didReceiveNetworkChangeNotification:(NSNotification *)noti{
	[self connect];
}
@end
