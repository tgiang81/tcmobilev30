//
//  MDInfo.m
//  TCiPad
//
//  Created by n2n on 12/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "MDInfo.h"

@implementation MDInfo

-(id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}



// to enable NSLog to work on array of this object (this is cool trick).
-(NSString *)description{
    
    return [NSString stringWithFormat:
            @"<MDInfo>exchange: %@, isMDL: %@, mdLevel: %@",
            _exchange, _isMDL, _mdLevel];
    
    
}

@end
