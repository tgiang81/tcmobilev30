//
//  MDInfo.h
//  TCiPad
//
//  Created by n2n on 12/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDInfo : NSObject

@property (nonatomic, strong) NSString *exchange;
@property (nonatomic, strong) NSString *isMDL;
@property (nonatomic, strong) NSString *mdLevel;


@end
