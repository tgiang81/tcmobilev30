//
//  VertxQueueProcessor.h
//  TCPlusUniversal
//
//  Created by Scott Thoo on 4/4/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol VertxProcessorDelegate ;

@interface VertxQueueProcessor : NSOperation
{

}

@property (nonatomic, retain) id <VertxProcessorDelegate> delegate;
@property (nonatomic, retain) NSDictionary *dict;


- (id)initWithDictionary :(NSDictionary *)dict delegate :(id<VertxProcessorDelegate>) theDelegate;
@end

//====================================================================
//                        Delegate Protocol
//====================================================================
@protocol VertxProcessorDelegate <NSObject>
- (void)didVertxProcessed:(VertxQueueProcessor *)processor;

@end
