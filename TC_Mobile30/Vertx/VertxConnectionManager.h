//
//  VertxConnectionManager.h
//  TCPlusUniversal
//
//  Created by Scott Thoo on 4/4/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VertxQueueProcessor.h"
#import "SRWebSocket.h"
#import "ExchangeInfo.h"
#import "QCParams.h"
#import "MDInfo.h"
#import "AllCommon.h"

//@protocol VertxConnectionDelegate <NSObject>
//@optional
//- (void)didReceiveLoginSuccess;
//- (void)didReceiveTimeAndSales:(NSDictionary *)response;
//- (void)didReceiveBusinessDone:(NSDictionary *)response;
//- (void)didReceiveStockNews:(NSDictionary *)response;
//- (void)didSortTopFinishedProcessing:(NSDictionary *)response;;
//@end

typedef void (^TCVertxCompletionHandler)(id result, NSError *error);

@interface VertxConnectionManager : NSObject <SRWebSocketDelegate>{
	NSError *_socketError;
}
@property (assign, nonatomic) BOOL connected;
#pragma mark - MAIN
- (void)close;
- (void)connect;
- (void)connectingUponLogin;
- (void)vertxLogin;
- (void)vertxSortWithExchange:(NSString*)exchg sortFID:(NSString *)fid isDescending:(BOOL)isDesc;
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange; //General
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange keyword:(NSString *)keyword;
- (void)vertxSortByLeft:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange;   //Customized For HomeScreen Left
- (void)vertxSortByRight:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange;  //Customized For HomeScreen Right
- (void)vertxSortForTicker:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange; // For Ticker

//UNSUBCRIBE
- (void)unsubscribeAll;
- (void)vertxGetExchangeInfo;
- (void)vertxSubscribeAllInQcFeed;
- (void)vertxUnsubscribeAllInQcFeed;
- (void)vertxUnSubcribeGetRating;
- (void)vertxGetScoreBoardWithExchange: (NSString *)exchange marketId:(NSString *)marketId;
#pragma mark - Search
//+++ Search in exchange
- (void)getIDSSInfo:(NSString *)stockCode;
- (void)vertxSearch:(NSString *)searchText fromExchange:(NSArray *)exchanges startPage:(NSInteger)startPage limit:(NSInteger)limit;
- (void)vertxSearch : (NSString *)str;
- (void)vertxSearchV3: (NSString *)str;

//Detail
- (void)getDetailStock:(NSString *)stkCode;
- (void)vertxGetStockDetailWithStockArr: (NSArray *)stkCodeArr FIDArr:(NSArray *)collFIDArr tag:(int)tag;
- (void)vertxSubscribeArr :(NSArray *)subscribeArr;

-(void)stopVertxTimerKeepAlive;

//Stock Details
- (void)vertxGetBusinessDoneByStockCode:(NSString *)stkCode exchg:(NSString *)exchange;
- (void)vertxGetTimeAndSalesByStockCode:(NSString *)stkCode begin:(NSString *)beginTranId end:(NSString *) endTranId exchange:(NSString *)exchange ;
- (void)vertxGetStockNews:(NSString*)stkCode;
- (void)vertxCompanyNews : (NSString *)stkCode;
- (void)vertxSearchStkCode : (NSString *)str;
- (void)vertxSearchStkCodeDetail : (NSString *)str;

//Mkt Depth
-(void)vertxSubscribeMktDepthLevel:(int)feature withStockCode:(NSString*)stkCode;
-(void)vertxGetMarketDepth:(NSString *)stkCode andCollarr:(NSString *)colExchange;
-(void)vertxUnsubscribeMktDepth;
- (void)vertxGetRating:(NSString *)stockCode exchange:(NSString *)exchange;
- (void)vertxGetRating;
//KLCI
- (void)vertxGetKLCI;
-(void)vertxUnsubscribeKLCI;

//Get indices
- (void)vertxGetIndices;//For gloabl
- (void)getIndicesFromExchange:(NSString *)exCode; //For private controller

// Vertex Streamer
- (void) vertxGetMarketStreamer:(NSArray *)stockArray andValue:(int)value andQuantity:(int)quantity andCondition:(int)condition;
- (void) vertxGetMarketStreamer:(NSArray *)stockArray andValue:(int)value andQuantity:(int)quantity andCondition:(int)condition exch:(NSString *)exch;
-(NSMutableDictionary*)validateVertx2ObjectTypesStreamer:(NSDictionary*)dict ;
- (void) vertxUnsubscribeMarketStreamer;

// Mobile
- (void)vertxRegister;

@property(nonatomic,retain)NSTimer  *vertxTimer;

@property(nonatomic,retain)SRWebSocket *webSocket;

+ (VertxConnectionManager *)singleton;

//@property (nonatomic, weak) id<VertxConnectionDelegate> delegate;

#pragma mark - Main for Mobile
//========== 1: Block Property ===============
@property (strong, nonatomic) TCVertxCompletionHandler sortByServerCompletion;
@property (strong, nonatomic) TCVertxCompletionHandler topRankCompletion;
@property (strong, nonatomic) TCVertxCompletionHandler indicesCompletion;
@property (strong, atomic) TCVertxCompletionHandler stockDataChartCompletion;
//========== 2: Action =======================
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange completion:(TCVertxCompletionHandler)completion;
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange keyword:(NSString *)keyword completion:(TCVertxCompletionHandler)completion;
//1.1: SortByServer With Filter Sector
- (void)vertxSortByServer:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange sectorFilter:(SectorInfo *)filterInfo completion:(TCVertxCompletionHandler)completion;

//2:SortByLeft
- (void)vertxSortByLeft:(NSString *)property Direction:(NSString*)direction exchg:(NSString *)exchange completion:(TCVertxCompletionHandler)completion; //Get 10 items **Notice the "limit"

//3: Get Indices
- (void)getIndicesFromExchange:(NSString *)exCode completion:(TCVertxCompletionHandler)completion;


//************************* CHART **********************************
- (void)getChartDataFrom:(NSString *)code inExchange:(NSString *)exchange forDay:(NSInteger)days;

- (void)getDefaultWeekChartDataFrom:(NSString *)code inExchange:(NSString *)exchange completion:(TCVertxCompletionHandler)handler;

@end


#define kGetISDDInfo @"DoneGetISDDInfo"
