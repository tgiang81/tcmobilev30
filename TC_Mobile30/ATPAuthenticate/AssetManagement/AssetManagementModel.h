//
//  AssetManagementModel.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/6/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AssetManagementModel : NSObject
@property (nonatomic, retain) NSString *AccountId;//43
@property (nonatomic, retain) NSString *CollateralAsset;//44
@property (nonatomic, retain) NSString *MarginRequirement;//45
@property (nonatomic, retain) NSString *Call_LMW;//46
@property (nonatomic, retain) NSString *Liability;//47
@property (nonatomic, retain) NSString *BuyMR;//48
@property (nonatomic, retain) NSString *Force_LMV;//49
@property (nonatomic, retain) NSString *Equity;//50
@property (nonatomic, retain) NSString *EE;//51
@property (nonatomic, retain) NSString *CallMargin;//52
@property (nonatomic, retain) NSString *CashBalance;//53
@property (nonatomic, retain) NSString *PurchasingPower;//54
@property (nonatomic, retain) NSString *CallForcesell;//55
@property (nonatomic, retain) NSString *LMV;//56
@property (nonatomic, retain) NSString *MarginCall;//57
@property (nonatomic, retain) NSString *Withdrawal;//58
@property (nonatomic, retain) NSString *Collateral_A;//59
@property (nonatomic, retain) NSString *Action;//60
@property (nonatomic, retain) NSString *MarginRatio;//62
@property (nonatomic, retain) NSString *Debt;//63
@property (nonatomic, retain) NSString *AccruedInterest;//64
@property (nonatomic, retain) NSString *fHoldRight;//65
@property (nonatomic, retain) NSString *Pre_Loan;//66
@property (nonatomic, retain) NSString *Fees;//67
@property (nonatomic, retain) NSString *BuyUnmatch;//68
@property (nonatomic, retain) NSString *AP;//69
@property (nonatomic, retain) NSString *AP_T1;//70
@property (nonatomic, retain) NSString *CIA;//71
@property (nonatomic, retain) NSString *AR;//72
@property (nonatomic, retain) NSString *AR_T1;//73
@property (nonatomic, retain) NSString *PP_Credit;//74
@property (nonatomic, retain) NSString *CreditLimit;//75
@property (nonatomic, retain) NSString *TotalAssets;//76
@property (nonatomic, retain) NSString *LMV_non_marginable;//77
@property (nonatomic, retain) NSString *EE_Credit;//78
@property (nonatomic, retain) NSString *TotalEquity;//79
@property (nonatomic, retain) NSString *SellUnmatch;//80
@property (nonatomic, retain) NSString *BuyCREE90;//81
@property (nonatomic, retain) NSString *BuyCREE80;//82
@property (nonatomic, retain) NSString *BuyCREE70;//83
@property (nonatomic, retain) NSString *BuyCREE60;//84
@property (nonatomic, retain) NSString *BuyCREE50;//85

@end

NS_ASSUME_NONNULL_END
