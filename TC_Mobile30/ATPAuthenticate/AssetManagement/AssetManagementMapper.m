//
//  AssetManagementMapper.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/6/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "AssetManagementMapper.h"
#import "NSString+Util.h"
#import <UIKit/UIKit.h>
#import "ThemeManager.h"
#import "AppMacro.h"
@interface AssetManagementKey()

@end

@implementation AssetManagementKey
- (instancetype)init{
    self = [super init];
    _AccountId = 43;//43
    _CollateralAsset = 44;//44
    _MarginRequirement = 45;//45
    _Call_LMW = 46;//46
    _Liability = 47;//47
    _BuyMR = 48;//48
    _Force_LMV = 49;//49
    _Equity = 50;//50
    _EE = 51;//51
    _CallMargin = 52;//52
    _CashBalance = 53;//53
    _PurchasingPower = 54;//54
    _CallForcesell = 55;//55
    _LMV = 56;//56
    _MarginCall = 57;//57
    _Withdrawal = 58;//58
    _Collateral_A = 59;//59
    _Action = 60;//60
    _MarginRatio = 62;//62
    _Debt = 63;//63
    _AccruedInterest = 64;//64
    _fHoldRight = 65;//65
    _Pre_Loan = 66;//66
    _Fees = 67;//67
    _BuyUnmatch = 68;//68
    _AP = 69;//69
    _AP_T1 = 70;//70
    _CIA = 71;//71
    _AR = 72;//72
    _AR_T1 = 73;//73
    _PP_Credit = 74;//74
    _CreditLimit = 75;//75
    _TotalAssets = 76;//76
    _LMV_non_marginable = 77;//77
    _EE_Credit = 78;//78
    _TotalEquity = 79;//79
    _SellUnmatch = 80;//80
    _BuyCREE90 = 81;//81
    _BuyCREE80 = 82;//82
    _BuyCREE70 = 83;//83
    _BuyCREE60 = 84;//84
    _BuyCREE50 = 85;//85
    
    return self;
}
- (NSArray *)getTitle:(int)code{
//    @"000000"
    UIColor *textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ob_textColor);
    UIColor *blueColor = TC_COLOR_FROM_HEX(@"007FCA");
    UIColor *redColor = TC_COLOR_FROM_HEX(@"F60000");
    NSString *title = @"";
    if(code == self.CollateralAsset){
        title = @"Collateral asset";
    }
    else if(code == self.Liability){
        textColor = redColor;
        title = @"Liabilities";
    }
    else if(code == self.Equity){
        title = @"Equity";
    }
    else if(code == self.CashBalance){
        title = @"Cash Balance";
    }
    else if(code == self.LMV){
        title = @"LMV";
    }
    else if(code == self.Collateral_A){
        title = @"Collateral";
    }
    else if(code == self.Debt){
        title = @"Debt";
    }
    else if(code == self.Pre_Loan){
        textColor = redColor;
        title = @"Pre-Loan";
    }
    else if(code == self.AP){
        title = @"AP";
    }
    else if(code == self.AR){
        textColor = redColor;
        title = @"AR";
    }
    else if(code == self.CreditLimit){
        title = @"Credit Limit";
    }
    else if(code == self.LMV_non_marginable){
        textColor = redColor;
        title = @"LMV non-marginable";
    }
    else if(code == self.MarginRequirement){
        title = @"Margin requirement";
    }
    else if(code == self.BuyMR){
        title = @"Buy MR";
    }
    else if(code == self.EE){
        textColor = blueColor;
        title = @"Excess Equity";
    }
    else if(code == self.PurchasingPower){
        textColor = blueColor;
        title = @"Purchasing Power";
    }
    else if(code == self.MarginCall){
        title = @"Margin Call";
    }
    else if(code == self.Action){
        textColor = redColor;
        title = @"Action";
    }
    else if(code == self.AccruedInterest){
        title = @"Accrued Interest";
    }
    else if(code == self.Fees){
        title = @"Fees";
    }
    else if(code == self.AP_T1){
        title = @"Ap T1";
    }
    else if(code == self.AR_T1){
        title = @"Ar T2";
    }
    else if(code == self.EE_Credit){
        title = @"EE credit";
    }
    else if(code == self.TotalEquity){
        textColor = redColor;
        title = @"Total Equity";
    }
    else if(code == self.Call_LMW){
        title = @"Call-LMV";
    }
    else if(code == self.Force_LMV){
        title = @"Force_LMV";
    }
    else if(code == self.CallMargin){
        title = @"Call Margin";
    }
    else if(code == self.CallForcesell){
        title = @"Call forcesell";
    }
    else if(code == self.Withdrawal){
        title = @"Withdrawl";
    }
    else if(code == self.MarginRatio){
        title = @"Margin ratio";
    }
    else if(code == self.SellUnmatch){
        textColor = redColor;
        title = @"Margin Call LMV Sold";
    }
    else if(code == self.CIA){
        title = @"Cash in Advance";
    }
    //Margin Call LMV Sold textColor = redColor;
    //Cash in Advance
    else if(code == self.fHoldRight){
        title = @"Hold right";
    }
    else if(code == self.BuyUnmatch){
        title = @"Buy UnMatch";
    }
    
    else if(code == self.PP_Credit){
        title = @"PP Credit";
    }
    else if(code == self.TotalAssets){
        textColor = redColor;
        title = @"Total Asset";
    }
    return @[title, textColor];
}
@end

@interface AssetManagementMapper()

@end
@implementation AssetManagementMapper
- (instancetype)init{
    self = [super init];
    _AccountId = -1;
    _CollateralAsset = -1;
    _MarginRequirement = -1;
    _Call_LMW = -1;
    _Liability = -1;
    _BuyMR = -1;
    _Force_LMV = -1;
    _Equity = -1;
    _EE = -1;
    _CallMargin = -1;
    _CashBalance = -1;
    _PurchasingPower = -1;
    _CallForcesell = -1;
    _LMV = -1;
    _MarginCall = -1;
    _Withdrawal = -1;
    _Collateral_A = -1;
    _Action = -1;
    _MarginRatio = -1;
    _Debt = -1;
    _AccruedInterest = -1;
    _fHoldRight = -1;
    _Pre_Loan = -1;
    _Fees = -1;
    _BuyUnmatch = -1;
    _AP = -1;
    _AP_T1 = -1;
    _CIA = -1;
    _AR = -1;
    _AR_T1 = -1;
    _PP_Credit = -1;
    _CreditLimit = -1;
    _TotalAssets = -1;
    _LMV_non_marginable = -1;
    _EE_Credit = -1;
    _TotalEquity = -1;
    _SellUnmatch = -1;
    _BuyCREE90 = -1;
    _BuyCREE80 = -1;
    _BuyCREE70 = -1;
    _BuyCREE60 = -1;
    _BuyCREE50 = -1;
    return self;
}

//get index of properties
- (void)getIndex:(NSArray *)tokens{
    AssetManagementKey *assetKey = [AssetManagementKey new];
    NSArray *properties = [self allPropertyeyKeys];
    int pos = 0;
    for(NSString *token in tokens){
        for(NSString *key in properties){
            int code = [[assetKey valueForKey:key] intValue];
            if([token isEqualToString:[NSString getASCII:code]]){
                [self setValue:[NSNumber numberWithInt:pos] forKey:key];
            }
        }
        pos++;
    }
}
//get model from tokens
- (AssetManagementModel *)getModel:(NSArray *)tokens{
    AssetManagementModel *model = [AssetManagementModel new];
    NSArray *properties = [self allPropertyeyKeys];
    for(NSString *property in properties){
        int indexProperty = [[self valueForKey:property] intValue];
        if (indexProperty != -1) {
            [model setValue:[tokens objectAtIndex:indexProperty] forKey:property];
        }
    }
    return model;
}

@end
