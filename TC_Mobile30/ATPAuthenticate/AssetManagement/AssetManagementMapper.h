//
//  AssetManagementMapper.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/6/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AssetManagementModel.h"
#import "BaseMapper.h"
NS_ASSUME_NONNULL_BEGIN
@interface AssetManagementKey : NSObject
@property (nonatomic, assign) int AccountId;//43
@property (nonatomic, assign) int CollateralAsset;//44
@property (nonatomic, assign) int MarginRequirement;//45
@property (nonatomic, assign) int Call_LMW;//46
@property (nonatomic, assign) int Liability;//47
@property (nonatomic, assign) int BuyMR;//48
@property (nonatomic, assign) int Force_LMV;//49
@property (nonatomic, assign) int Equity;//50
@property (nonatomic, assign) int EE;//51
@property (nonatomic, assign) int CallMargin;//52
@property (nonatomic, assign) int CashBalance;//53
@property (nonatomic, assign) int PurchasingPower;//54
@property (nonatomic, assign) int CallForcesell;//55
@property (nonatomic, assign) int LMV;//56
@property (nonatomic, assign) int MarginCall;//57
@property (nonatomic, assign) int Withdrawal;//58
@property (nonatomic, assign) int Collateral_A;//59
@property (nonatomic, assign) int Action;//60
@property (nonatomic, assign) int MarginRatio;//62
@property (nonatomic, assign) int Debt;//63
@property (nonatomic, assign) int AccruedInterest;//64
@property (nonatomic, assign) int fHoldRight;//65
@property (nonatomic, assign) int Pre_Loan;//66
@property (nonatomic, assign) int Fees;//67
@property (nonatomic, assign) int BuyUnmatch;//68
@property (nonatomic, assign) int AP;//69
@property (nonatomic, assign) int AP_T1;//70
@property (nonatomic, assign) int CIA;//71
@property (nonatomic, assign) int AR;//72
@property (nonatomic, assign) int AR_T1;//73
@property (nonatomic, assign) int PP_Credit;//74
@property (nonatomic, assign) int CreditLimit;//75
@property (nonatomic, assign) int TotalAssets;//76
@property (nonatomic, assign) int LMV_non_marginable;//77
@property (nonatomic, assign) int EE_Credit;//78
@property (nonatomic, assign) int TotalEquity;//79
@property (nonatomic, assign) int SellUnmatch;//80
@property (nonatomic, assign) int BuyCREE90;//81
@property (nonatomic, assign) int BuyCREE80;//82
@property (nonatomic, assign) int BuyCREE70;//83
@property (nonatomic, assign) int BuyCREE60;//84
@property (nonatomic, assign) int BuyCREE50;//85
- (NSArray *)getTitle:(int)code;
@end
@interface AssetManagementMapper : BaseMapper

@property (nonatomic, assign) int AccountId;
@property (nonatomic, assign) int CollateralAsset;
@property (nonatomic, assign) int MarginRequirement;
@property (nonatomic, assign) int Call_LMW;
@property (nonatomic, assign) int Liability;
@property (nonatomic, assign) int BuyMR;
@property (nonatomic, assign) int Force_LMV;
@property (nonatomic, assign) int Equity;
@property (nonatomic, assign) int EE;
@property (nonatomic, assign) int CallMargin;
@property (nonatomic, assign) int CashBalance;
@property (nonatomic, assign) int PurchasingPower;
@property (nonatomic, assign) int CallForcesell;
@property (nonatomic, assign) int LMV;
@property (nonatomic, assign) int MarginCall;
@property (nonatomic, assign) int Withdrawal;
@property (nonatomic, assign) int Collateral_A;
@property (nonatomic, assign) int Action;
@property (nonatomic, assign) int MarginRatio;
@property (nonatomic, assign) int Debt;
@property (nonatomic, assign) int AccruedInterest;
@property (nonatomic, assign) int fHoldRight;
@property (nonatomic, assign) int Pre_Loan;
@property (nonatomic, assign) int Fees;
@property (nonatomic, assign) int BuyUnmatch;
@property (nonatomic, assign) int AP;
@property (nonatomic, assign) int AP_T1;
@property (nonatomic, assign) int CIA;
@property (nonatomic, assign) int AR;
@property (nonatomic, assign) int AR_T1;
@property (nonatomic, assign) int PP_Credit;
@property (nonatomic, assign) int CreditLimit;
@property (nonatomic, assign) int TotalAssets;
@property (nonatomic, assign) int LMV_non_marginable;
@property (nonatomic, assign) int EE_Credit;
@property (nonatomic, assign) int TotalEquity;
@property (nonatomic, assign) int SellUnmatch;
@property (nonatomic, assign) int BuyCREE90;
@property (nonatomic, assign) int BuyCREE80;
@property (nonatomic, assign) int BuyCREE70;
@property (nonatomic, assign) int BuyCREE60;
@property (nonatomic, assign) int BuyCREE50;

- (AssetManagementModel *)getModel:(NSArray *)tokens;
- (void)getIndex:(NSArray *)tokens;
@end



NS_ASSUME_NONNULL_END
