//
//  BaseMapper.h
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/6/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseMapper : NSObject
- (NSArray *)allPropertyeyKeys;
@end

NS_ASSUME_NONNULL_END
