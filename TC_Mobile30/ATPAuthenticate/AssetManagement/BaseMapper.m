//
//  BaseMapper.m
//  TC_Mobile30
//
//  Created by Nguyễn Văn Tú on 5/6/19.
//  Copyright © 2019 n2nconnect. All rights reserved.
//

#import "BaseMapper.h"
#import <objc/runtime.h>
@implementation BaseMapper
- (NSArray *)allPropertyeyKeys
{
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    NSMutableArray *propertiesName = [NSMutableArray array];
    unsigned i;
    for (i = 0; i < count; i++)
    {
        objc_property_t property = properties[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        [propertiesName addObject:name];
    }
    
    free(properties);
    
    return propertiesName;
}
@end
