//
//  ATPAuthenticate.m
//  ATPTestConn
//
//  Created by Scott Thoo on 8/7/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "ATPAuthenticate.h"
#import "NSData-Zlib.h"
#import "PortfolioData.h"
#import "RSACrypt.h"
#import "NSString-Hash.h"
#import "NSData-AES.h"
#import "AppConstants.h"
#import "QCData.h"
#import "VertxConnectionManager.h"
#import "ExchangeData.h"
#import "GTMRegex.h"
#import "TradingRules.h"
#import "OrderControl.h"
#import "CurrencyRate.h"
#import "AdditionalPaymentData.h"
#import "TradeData.h"
#import "DefineChooseSwift.h"
//#import "StockDetailViewController.h"
#import "OrderBookandPortfolioViewController.h"
#import "E2EECrypt.h"
#import "ConfigureServerSession.h"

#import <netinet/in.h> 
#import <ifaddrs.h> 
#import <sys/socket.h>
#import <arpa/inet.h>

#import "AppDelegate.h"
#import "ElasticNewsData.h"

#import "Utils.h"

#define kLogoutCountdownTime    30  // seconds
#define kURLRequestTimeout      30 // seconds
//Testing Dummy Data
#define ATPSERVER @"nmga.affintrade.com"
#define USERNAME @"usern2n"
#define PASSWORD @"cvb123"
//#define ATPSERVER @"218.100.22.129:20010"
//#define userName @"seongwei73"
//#define password @"abc123"
#define SCREEN_HEIGHT_PIXELS [[UIScreen mainScreen] bounds].size.height * [[UIScreen mainScreen] scale]
#define SCREEN_WIDTH_PIXELS [[UIScreen mainScreen] bounds].size.width * [[UIScreen mainScreen] scale]
#define START_OF_TEXT 2
#define END_OF_TEXT 3
#define userSenderCode @"A00039" //@"G00090" //Hard-coded, can be found in loginReturned.

static id ObjectOrNull(id object)
{
    return object ?: [NSNull null];
}

@interface ATPAuthenticate () <UIAlertViewDelegate>
{
    NSString *userParam;
    
    //Login items
    NSString *atpServer;
    NSString *userName;
    NSString *password;
    NSString *currentSessionKey;
    
    UserAccountClientData *userAccountClientData;
    BOOL alertLogoutShown;
}



@end

@implementation ATPAuthenticate

@synthesize delegate,autoLogoutTimer,idleCheckTimer,isIdle, time_expire, countDown;
@synthesize userInfoDict, userExchangelist, e2eeInfo, e2eeInfo1FA;

+ (ATPAuthenticate *)singleton
{
    static ATPAuthenticate * singleton = nil;
    @synchronized(self)
    {
        if(!singleton)
            singleton = [[ATPAuthenticate alloc]init];
    }
    return singleton;
    
}

- (id) init {
    self = [super init];
    if (self) {
        userInfoDict = [NSMutableDictionary dictionary];
        userExchangelist = @[].mutableCopy;
        atpServer = [UserPrefConstants singleton].userATPServerIP;
        alertLogoutShown = NO;
        countDown = [UserPrefConstants singleton].sessionTimeoutSeconds;
        self.userPref = [UserPrefConstants singleton];
    }
    return self;
}

#pragma mark - Helper Methods
- (void)overrideExchangeNames  {
    for (ExchangeData *ed in [UserPrefConstants singleton].userExchagelist) {
        if ([[UserPrefConstants singleton].overrideExchangesName objectForKey:ed.feed_exchg_code]!=nil) {
            // if key exist, then override
            ed.exchange_name = [[UserPrefConstants singleton].overrideExchangesName objectForKey:ed.feed_exchg_code];
        }
    }
}

- (NSString *)formatDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

-(NSString *) ArchiveNewsEncryption
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    [dateFormatter setDateFormat:@"yyMMdd hhmmss"];
    
    NSString *formattedDate = [dateFormatter stringFromDate:[NSDate date]];
    NSArray *arr = [formattedDate componentsSeparatedByString:@" "];
    NSString *time = arr[1]; //current time with GMT+0 in hhmmss
    NSString *encryptedTime = @"";
    NSString *key = arr[0]; //today date with GMT+0 in yymmdd
    const char  *timeArray = [time UTF8String];
    const char *keyArray = [key UTF8String];
    for(int i=0;i<strlen(timeArray) ;i++)
    {
        //Using bitwise xor operator
        int xor =  ((char) timeArray[i] ^ keyArray[i]);
        //convert integer to hex with 2 digit
        
        encryptedTime =[NSString stringWithFormat:@"%@%%%02X", encryptedTime,xor];
        
    }
    //NSLog(@"%@",encryptedTime );
    return encryptedTime;
}


- (NSString *) getValue:(NSString *) input {
    NSArray *tmpArr = [input componentsSeparatedByString:@"="];
    if ([tmpArr count] == 2) {
        return (NSString *)[tmpArr objectAtIndex:1];
    }
    return nil;
}

- (NSString *) generate_guid {
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject)) ;
    
    return uuidStr;
}


- (void) initTimerLogin {
    
    if (self.idleCheckTimer==nil) {
        self.isIdle = NO;
        time_expire = [[NSDate date] dateByAddingTimeInterval:[UserPrefConstants singleton].sessionTimeoutSeconds];
        self.idleCheckTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(checkTick) userInfo:nil repeats:YES];
        activity = NO;
    }
}


- (void) invalidateAutoLogoutTimer {
    if (self.autoLogoutTimer != nil) {
        [self.autoLogoutTimer invalidate];
        self.autoLogoutTimer = nil;
    }
}

- (void) invalidateIdleCheckTimer {
    
    if (self.idleCheckTimer != nil) {
        [self.idleCheckTimer invalidate];
        self.idleCheckTimer = nil;
        
    }
    if (time_expire != nil) {
        time_expire = nil;
    }
}

- (void) reValidateIdleCheckTimer {
    
    if (self.idleCheckTimer == nil) {
        self.idleCheckTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(checkTick) userInfo:nil repeats:YES];
    }
    if (time_expire == nil) {
        time_expire = [[NSDate date] dateByAddingTimeInterval:[UserPrefConstants singleton].sessionTimeoutSeconds];
    }
}


- (void) timeoutActivity {
    activity = YES;
    ///NSLog(@"Expired Time: %@ Current Time: %@", time_expire, [NSDate date]);
    
    if (time_expire != nil) {
        time_expire = nil;
    }
    time_expire = [[NSDate date] dateByAddingTimeInterval:[UserPrefConstants singleton].sessionTimeoutSeconds];
    ///NSLog(@"TimeoutActivity Expired Time: %@ Current Time: %@", time_expire, [NSDate date]);
    
    countDown = [UserPrefConstants singleton].sessionTimeoutSeconds;
    
    activity = NO;
}

- (void) checkTick {
    
    /// NSLog(@"Expired Time: %@ Current Time: %@", time_expire, [NSDate date]);
    
    countDown -= 5;
    
    if (activity) {
        return;
    }
    if ([time_expire timeIntervalSince1970] < [[NSDate date] timeIntervalSince1970]) {
        
        [self setIsIdle:YES];
        [self invalidateIdleCheckTimer];
        
        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
        [ndf setDateFormat:@"HH:mm:ss"];
        
        if (!alertLogoutShown) {
            alertLogoutShown = YES;
            
            NSString *seconds = [NSString stringWithFormat:@"%.0f",[UserPrefConstants singleton].sessionTimeoutSeconds/60.0];
            NSString *cntDown = [NSString stringWithFormat:@"%d",kLogoutCountdownTime];
            
            UIAlertView  *alert = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"System Idle"]
                                                             message:[LanguageManager stringForKey:@"IdleLogoutMessage" withPlaceholders:@{@"%minutes%":seconds,
                                                                                                                                            @"%expireSec%":cntDown,
                                                                                                                                            @"%idleStart%":[ndf stringFromDate:[NSDate date]]}]
                                                                      
                                                            delegate:self
                                                   cancelButtonTitle:[LanguageManager stringForKey:@"Cancel"]
                                                   otherButtonTitles:[LanguageManager stringForKey:@"Ok"], nil];
            [alert setTag:3];
            [alert show];
            
        }
        
        self.autoLogoutTimer = [NSTimer scheduledTimerWithTimeInterval:kLogoutCountdownTime target:self selector:@selector(logout) userInfo:nil repeats:NO];
        
    }
}

#pragma mark - Login/Out Methods

- (void) logout
{
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"doAutoLogout" object:self userInfo:notificationData];
    
}


-(void)atpDoLogout {
    NSString *urlString;
    //atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlString = [NSString stringWithFormat:@"https://%@/[%@]TradeLogout",atpServer,currentSessionKey];
    } else {
        urlString = [NSString stringWithFormat:@"http://%@/[%@]TradeLogout",atpServer,currentSessionKey];
    }
    
   // NSLog(@"Url String %@",urlString);

    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSURLSessionDataTask *downloadTask =
    [[NSURLSession sharedSession] dataTaskWithRequest:request
                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                        
                                        if (error) {
                                            NSLog(@"Log out %@", error);
                                        }
                                        // done logout
                                        
                                    }];
    [downloadTask resume];
}



- (void) atpGetPK_LoadBalance
{
    [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@""];
    atpServer = [UserPrefConstants singleton].userATPServerIP;
    userName  = [UserPrefConstants singleton].userName;
    password  = [UserPrefConstants singleton].userPassword;
    isE2EE_Encryption = [UserPrefConstants singleton].E2EEEncryptionEnabled;
    NSString *cmdStr = @"TradeGetPK2A";
    
         if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
             cmdStr = @"TradeGetPK2A";
         }else{
             cmdStr = @"TradeGetPK";
         }
    
    [self.delegate updateLoginProgress:LOGIN_INIT];
	//+++ Update Progress
	if (self.loginProgressHandler) {
		self.loginProgressHandler(LOGIN_INIT);
	}
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?+=1|%@", atpServer,cmdStr, [self generateTimeStampURLEncoded]];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/%@?+=1|%@", atpServer,cmdStr, [self generateTimeStamp]];
    }
    
	NSLog(@"1111: ATP login using urlStr : %@",urlStr);
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         
         if ([data length] > 0 && error == nil)
         {
             
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             NSString *public_key = @"";
             NSLog(@"atpGetKey Returned : %@",content);
             int rsa_public_key = -1;
             int atp_public_ip = -1;
             int atp_private_ip = -1;
             int secondary_port = -1;
             int modulus = -1;
             int exponent = -1;
             
             NSString *modulus_key = @"";
             NSString *exponent_key = @"";
             
             NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
             
             for (NSString *s in arr) {
                 if ([metaregex matchesString:s]) {
                     
                     NSString *s2 = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s2 componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@","]) {
                             rsa_public_key = pos;
                         }
                         else if ([token isEqualToString:@"-"]) {
                             atp_public_ip = pos;
                         }
                         else if ([token isEqualToString:@"."]) {
                             atp_private_ip = pos;
                         }
                         else if ([token isEqualToString:@"0"]) {
                             secondary_port = pos;
                         }else if([token isEqualToString:@"1"]){
                             modulus = pos;
                         }else if([token isEqualToString:@"2"]){
                             exponent = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     NSString *s2 = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s2 componentsSeparatedByString:@"|"];
                     
                     if (rsa_public_key != -1 && [tokens count] > rsa_public_key) {
                         public_key = [tokens objectAtIndex:rsa_public_key];
                     }
                     if (atp_public_ip != -1 && [tokens count] > atp_public_ip) {
                         NSData *public_ip_data = [self base64Decode:[tokens objectAtIndex:atp_public_ip]];
                         NSString *public_ip = [[NSString alloc] initWithData:public_ip_data encoding:NSASCIIStringEncoding];
                         [UserPrefConstants singleton].atpPublicIP = public_ip;
                     }
                     if (atp_private_ip != -1 && [tokens count] > atp_private_ip) {
                         NSData *private_ip_data = [self base64Decode:[tokens objectAtIndex:atp_private_ip]];
                         NSString *private_ip = [[NSString alloc] initWithData:private_ip_data encoding:NSASCIIStringEncoding];
                         [UserPrefConstants singleton].atpPrivateIP = private_ip;
                     }
                     if (secondary_port != -1 && [tokens count] > secondary_port) {
                         NSData *secondary_port_data = [self base64Decode:[tokens objectAtIndex:secondary_port]];
                         NSString *secondary_port = [[NSString alloc] initWithData:secondary_port_data encoding:NSASCIIStringEncoding];
                         [UserPrefConstants singleton].atpPort = secondary_port;
                         
                     }
                     if (modulus != -1 && [tokens count] > modulus) {
                         modulus_key = [tokens objectAtIndex:modulus];
                         
                     }
                     if (exponent != -1 && [tokens count] > exponent) {
                         exponent_key = [tokens objectAtIndex:exponent];
                         
                     }
                 }
             }
             if ([UserPrefConstants singleton].atpPublicIP != nil) {
                 [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@"save atpPublicIP"];
                 atpServer = [UserPrefConstants singleton].atpPublicIP;
                 if ([UserPrefConstants singleton].atpPort != nil) {
                     atpServer = [NSString stringWithFormat:@"%@:%@", atpServer, [UserPrefConstants singleton].atpPort];
                 }
             } else {
                 atpServer = [UserPrefConstants singleton].userATPServerIP;
                 [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@"save userATPServerIP"];
             }
             
             [self.delegate updateLoginProgress:LOGIN_PK];
			 //+++ Update Progress
			 if (self.loginProgressHandler) {
				 self.loginProgressHandler(LOGIN_PK);
			 }
             
              if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
				  DLog(@"2222: atpGetKey2 - modulus_key: %@ - exponent_key: %@", modulus_key, exponent_key);
				   [self atpGetKey2:modulus_key andExponent:exponent_key];
              }else{
				   DLog(@"2222: atpGetKey2 - public_key: %@", public_key);
                   [self atpGetKey2:public_key andExponent:nil];
              }
         }
		 
         else if ([data length] == 0 && error == nil)
         {
             ////NSLog(@"Empty");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Empty" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             ////NSLog(@"Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Timed Out" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate timedOut];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil)
         {
             ////NSLog(@"Download Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Download Error" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate downloadError:error];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
     }];
}


- (void) atpGetPK
{
    [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@""];
    atpServer = [UserPrefConstants singleton].userATPServerIP;
    userName  = [UserPrefConstants singleton].userName;
    password  = [UserPrefConstants singleton].userPassword;
    isE2EE_Encryption = [UserPrefConstants singleton].E2EEEncryptionEnabled;
    
    [self.delegate updateLoginProgress:LOGIN_INIT];
	//+++ Update Progress
	if (self.loginProgressHandler) {
		self.loginProgressHandler(LOGIN_INIT);
	}
    NSString *cmdStr = @"TradeGetPK";
     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
    
         cmdStr = @"TradeGetPK2A";
         if ([UserPrefConstants singleton].HTTPSEnabled) {
             urlStr = [NSString stringWithFormat:@"https://%@/%@?+=1|%@", atpServer,cmdStr, [self generateTimeStampURLEncoded]];
         } else {
             urlStr = [NSString stringWithFormat:@"http://%@/%@?+=1|%@", atpServer,cmdStr, [self generateTimeStamp]];
         }
         
         NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
         NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
         urlRequest.timeoutInterval = kURLRequestTimeout;
         NSOperationQueue *queue = [NSOperationQueue mainQueue];
         
         [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
          {
              GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
              GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
              
              if ([data length] > 0 && error == nil)
              {
                  
                  NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                  NSLog(@"atpGetKey Returned : %@",content);
                  NSString *public_key = @"";
                  
                  int rsa_public_key = -1;
                  int atp_public_ip = -1;
                  int atp_private_ip = -1;
                  int secondary_port = -1;
                  int modulus = -1;
                  int exponent = -1;
                  
                  NSString *modulus_key = @"";
                  NSString *exponent_key = @"";
                  
                  NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
                  
                  for (NSString *s in arr) {
                      if ([metaregex matchesString:s]) {
                          
                          NSString *s2 = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                          NSArray *tokens = [s2 componentsSeparatedByString:@"|"];
                          int pos = 0;
                          for (NSString *token in tokens) {
                              if ([token isEqualToString:@","]) {
                                  rsa_public_key = pos;
                              }
                              else if ([token isEqualToString:@"-"]) {
                                  atp_public_ip = pos;
                              }
                              else if ([token isEqualToString:@"."]) {
                                  atp_private_ip = pos;
                              }
                              else if ([token isEqualToString:@"0"]) {
                                  secondary_port = pos;
                              }else if([token isEqualToString:@"1"]){
                                  modulus = pos;
                              }else if([token isEqualToString:@"2"]){
                                  exponent = pos;
                              }
                              pos++;
                          }
                      }
                      else if ([dataregex matchesString:s]) {
                          NSString *s2 = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                          NSArray *tokens = [s2 componentsSeparatedByString:@"|"];
                          
                          if (rsa_public_key != -1 && [tokens count] > rsa_public_key) {
                              public_key = [tokens objectAtIndex:rsa_public_key];
                          }
                          if (atp_public_ip != -1 && [tokens count] > atp_public_ip) {
                              NSData *public_ip_data = [self base64Decode:[tokens objectAtIndex:atp_public_ip]];
                              NSString *public_ip = [[NSString alloc] initWithData:public_ip_data encoding:NSASCIIStringEncoding];
                              [UserPrefConstants singleton].atpPublicIP = public_ip;
                          }
                          if (atp_private_ip != -1 && [tokens count] > atp_private_ip) {
                              NSData *private_ip_data = [self base64Decode:[tokens objectAtIndex:atp_private_ip]];
                              NSString *private_ip = [[NSString alloc] initWithData:private_ip_data encoding:NSASCIIStringEncoding];
                              [UserPrefConstants singleton].atpPrivateIP = private_ip;
                          }
                          if (secondary_port != -1 && [tokens count] > secondary_port) {
                              NSData *secondary_port_data = [self base64Decode:[tokens objectAtIndex:secondary_port]];
                              NSString *secondary_port = [[NSString alloc] initWithData:secondary_port_data encoding:NSASCIIStringEncoding];
                              [UserPrefConstants singleton].atpPort = secondary_port;
                              
                          }
                          if (modulus != -1 && [tokens count] > modulus) {
                              modulus_key = [tokens objectAtIndex:modulus];
                              
                          }
                          if (exponent != -1 && [tokens count] > exponent) {
                              exponent_key = [tokens objectAtIndex:exponent];
                              
                          }
                      }
                  }
                  if ([UserPrefConstants singleton].atpPublicIP != nil) {
                      [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@"get atpPublicIP"];
                      atpServer = [UserPrefConstants singleton].atpPublicIP;
                      if ([UserPrefConstants singleton].atpPort != nil) {
                          atpServer = [NSString stringWithFormat:@"%@:%@", atpServer, [UserPrefConstants singleton].atpPort];
                      }
                  } else {
                      [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@"get userATPServerIP"];
                      atpServer = [UserPrefConstants singleton].userATPServerIP;
                  }
                  
                  [self.delegate updateLoginProgress:LOGIN_PK];
				  //+++ Update Progress
				  if (self.loginProgressHandler) {
					  self.loginProgressHandler(LOGIN_PK);
				  }
                  
                  
                  [self atpGetKey2:modulus_key andExponent:exponent_key];
              }
              
              
              else if ([data length] == 0 && error == nil)
              {
                  ////NSLog(@"Empty");
                  //Send post notification
                  NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                  [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                  [notificationData setObject:[LanguageManager stringForKey:@"%@ Empty" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
				  //+++ Completion Handler
				  if (self.loginCompletionHandler) {
					  self.loginCompletionHandler(NO, notificationData, error);
				  }
              }
              else if (error != nil && error.code == NSURLErrorTimedOut)
              {
                  ////NSLog(@"Error");
                  //Send post notification
                  NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                  [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                  [notificationData setObject:[LanguageManager stringForKey:@"%@ Timed Out" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
                  //            [delegate timedOut];
				  //+++ Completion Handler
				  if (self.loginCompletionHandler) {
					  self.loginCompletionHandler(NO, notificationData, error);
				  }
              }
              else if (error != nil)
              {
                  ////NSLog(@"Download Error");
                  //Send post notification
                  NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                  [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                  [notificationData setObject:[LanguageManager stringForKey:@"%@ Download Error" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
                  //            [delegate downloadError:error];
				  //+++ Completion Handler
				  if (self.loginCompletionHandler) {
					  self.loginCompletionHandler(NO, notificationData, error);
				  }
              }
          }];
         
     }else{
	     cmdStr = @"TradeGetPK";
         if ([UserPrefConstants singleton].HTTPSEnabled) {
          urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", atpServer,cmdStr, [self generateTimeStampURLEncoded]];
          } else {
          urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", atpServer,cmdStr, [self generateTimeStamp]];
          }
         
         NSLog(@"atpGetPK %@",urlStr);
         
         
         
         
         ////NSLog(@"ATP login using urlStr : %@",urlStr);
         NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
         NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
         urlRequest.timeoutInterval = kURLRequestTimeout;
         NSOperationQueue *queue = [NSOperationQueue mainQueue];
         
         [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
          {
              if ([data length] > 0 && error == nil)
              {
                  NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                  ////NSLog(@"atpGetKey Returned : %@",content);
                  [self.delegate updateLoginProgress:LOGIN_PK];
				  //+++ Update Progress
				  if (self.loginProgressHandler) {
					  self.loginProgressHandler(LOGIN_PK);
				  }
                  [self atpGetKey2:content andExponent:nil];
                  
              }
              else if ([data length] == 0 && error == nil)
              {
                  ////NSLog(@"Empty");
                  //Send post notification
                  NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                  [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                  [notificationData setObject:[LanguageManager stringForKey:@"%@ Empty" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
				  //+++ Completion Handler
				  if (self.loginCompletionHandler) {
					  self.loginCompletionHandler(NO, notificationData, error);
				  }
              }
              else if (error != nil && error.code == NSURLErrorTimedOut)
              {
                  ////NSLog(@"Error");
                  //Send post notification
                  NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                  [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                  [notificationData setObject:[LanguageManager stringForKey:@"%@ Timed Out" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
                  //            [delegate timedOut];
				  //+++ Completion Handler
				  if (self.loginCompletionHandler) {
					  self.loginCompletionHandler(NO, notificationData, error);
				  }
              }
              else if (error != nil)
              {
                  ////NSLog(@"Download Error");
                  //Send post notification
                  NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                  [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                  [notificationData setObject:[LanguageManager stringForKey:@"%@ Download Error" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
                  //            [delegate downloadError:error];
				  //+++ Completion Handler
				  if (self.loginCompletionHandler) {
					  self.loginCompletionHandler(NO, notificationData, error);
				  }
              }
          }];
     }
    
}

- (void) atpGetKey2:(NSString *)certContent andExponent:(NSString *)exponentKey
{
    [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@""];
	NSLog(@"Modulus : %@",certContent);
    NSLog(@"Exponent : %@",exponentKey);
    userInfoEncryptionKey = [self generateAESKey];
    NSString *rsaEncryptedKey = [self rsaEncryptData:userInfoEncryptionKey withCertContent:certContent andTag:@"atp_publicKey" andExponent:exponentKey];
   
    
    NSLog(@"userInfoEncryptionKey : %@",rsaEncryptedKey);
     [self removeKeyRefWithTag:@"atp_publicKey"];
    
    NSString *cmdStr = @"TradeGetKey2A";
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
        cmdStr = @"TradeGetKey2A";
    }else{
         cmdStr = @"TradeGetKey2";
    }
    
    //atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", atpServer,cmdStr, rsaEncryptedKey];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", atpServer,cmdStr, rsaEncryptedKey];
    }
    
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
     NSLog(@"TradeGetKey2 : %@",[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]);
	DLog(@"2222: TradeGetKey2 : %@", urlStr);
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
			 
             NSLog(@"atpGetKey2 Returned : %@",content);
             if([content containsString:@"Error"])
             {
                 NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                 [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                 [notificationData setObject:content forKey:@"error"];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             }
             else if([content containsString:@"ERROR"])
             {
                 NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                 [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                 [notificationData setObject:content forKey:@"error"];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             }
             else
             {
                 NSData *decodedContent = [self base64Decode:content];
                 NSData *doubleHashKey = [self getDoubleHashEncryptionKey:userInfoEncryptionKey];
                 if ([decodedContent isEqualToData:doubleHashKey]) {
                     [self.delegate updateLoginProgress:LOGIN_SESSION2];
					 //+++ Update Progress
					 if (self.loginProgressHandler) {
						 self.loginProgressHandler(LOGIN_SESSION2);
					 }
					 DLog(@"3333: atpGetSession2 - rsaEncryptedKey: %@", rsaEncryptedKey);
                     [self atpGetSession2:rsaEncryptedKey];
				 }else{
					 //Miss this case ???
					 //+++ Completion Handler
					 NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
					 [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
					 [notificationData setObject:content forKey:@"error"];
					 if (self.loginCompletionHandler) {
						 self.loginCompletionHandler(NO, notificationData, error);
					 }
				 }
             }
         }
         else if ([data length] == 0 && error == nil)
         {
             ////NSLog(@"Empty");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Empty" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             ////NSLog(@"Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Timed Out" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate timedOut];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil)
         {
             ////NSLog(@"Download Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Download Error" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate downloadError:error];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
	 }];
}

// used for login
- (void) atpGetE2EEParams {
    [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@"check isE2EE_Encryption"];
    NSString *cmdStr  = @"TradeGetE2EEParams";
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", atpServer,cmdStr, [self generateTimeStamp]];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", atpServer,cmdStr, [self generateTimeStamp]];
    }
    
   // NSLog(@"GetE2EEParams URL: %@", urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
         /// NSLog(@"TradeGetE2EEParams Done");
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         // NSLog(@"\n\n---- TradeGetE2EEParams Returned Data ----\n%@\n\n", content);
         
         GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
         GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
         
         if ([errorReg1 matchesString:content] || [errorReg2 matchesString:content] || [content isEqualToString:@""]) {
             self.error_message = content;
             if (self.error_message == nil || [self.error_message isEqualToString:@""]) {
                 
             }
             ///NSLog(@"TradeGetE2EEParams Failed: %@", self.error_message);
             
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:self.error_message forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             
             
         } else if ([content rangeOfString:@"(="].location == NSNotFound) {
             
            /// NSLog(@"TradeGetE2EEParams Failed: %@", self.error_message);
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:self.error_message forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
         } else {
             NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
             
             int pubKey = -1;
             int sessionID = -1;
             int randomNumber = -1;
             int modulus = -1;
             int exponent = -1;
             
             if (e2eeInfo != nil) {
                 e2eeInfo = nil;
             }
             e2eeInfo = [[E2EEInfo alloc] init];
             
             for (NSString *str in arr) {
                 if ([str hasPrefix:@"(="]) {
                     NSString *s = [str stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@"+"]) {
                             pubKey = pos;
                         }
                         else if ([token isEqualToString:@","]) {
                             sessionID = pos;
                         }
                         else if ([token isEqualToString:@"-"]) {
                             randomNumber = pos;
                         }
                         else if ([token isEqualToString:@"."]) {
                             modulus = pos;
                         }
                         else if ([token isEqualToString:@"/"]) {
                             exponent = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([str hasPrefix:@")="]) {
                     NSString *s = [str stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if (pubKey != -1) {
                         e2eeInfo.pubKey = [tokens objectAtIndex:pubKey];
                     }
                     if (sessionID != -1) {
                         e2eeInfo.sessionID = [tokens objectAtIndex:sessionID];
                     }
                     if (randomNumber != -1) {
                         e2eeInfo.randomKey = [tokens objectAtIndex:randomNumber];
                     }
                     if (modulus != -1) {
                         e2eeInfo.modulus = [tokens objectAtIndex:modulus];
                     }
                     if (exponent != -1) {
                         e2eeInfo.exponent = [tokens objectAtIndex:exponent];
                     }
                 }
             }
             
            
             //Generate E2EE Encrypted Password
             e2eeInfo.e2ee_EncryptedPassword =
             [E2EECrypt e2eePassword:e2eeInfo.pubKey randomKey:e2eeInfo.randomKey session:e2eeInfo.sessionID password:password];
             
             [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@"Generate E2EE Encrypted Password"];
             
             if (e2eeInfo.e2ee_EncryptedPassword != nil && ![e2eeInfo.e2ee_EncryptedPassword isEqualToString:@""]) {
                 
                 
                 [self.delegate updateLoginProgress:LOGIN_LOGIN];
				 //+++ Update Progress
				 if (self.loginProgressHandler) {
					 self.loginProgressHandler(LOGIN_LOGIN);
				 }
                 [self atpDoLogin];
                 
                 
             } else {
                 if (self.error_message == nil || [self.error_message isEqualToString:@""]) {
                     self.error_message = @"";
                 }
             }
         }
     }];

}

- (void) atpGetSession2:(NSString *)rsaEncryptedKey
{
    [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@""];
	 NSString *cmdStr = @"TradeGetSession2A";
     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0")){
         cmdStr = @"TradeGetSession2A";
     }else{
        cmdStr = @"TradeGetSession2";
     }
    
    atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", atpServer,cmdStr, rsaEncryptedKey];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", atpServer,cmdStr, rsaEncryptedKey];
    }
	NSLog(@"3333: TradeGetSession URL: %@",urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSLog(@"Session Data %@",data);
         
         if ([data length] > 0 && error == nil)
         {
             NSString *responseData = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             
              NSLog(@"atpGetSession2 Returned : %@",responseData);
             
             NSString *content = [self atpDecryptionProcess:responseData];
             
             NSLog(@"atpGetSession2 Returned : %@", content);
             [ConfigureServerSession shareInstance].currentSessionKey = content;
             self->currentSessionKey = content;
             [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@"check isE2EE_Encryption"];
             if (self->isE2EE_Encryption) {
                 [self.delegate updateLoginProgress:LOGIN_E2EE];
				 //+++ Update Progress
				 if (self.loginProgressHandler) {
					 self.loginProgressHandler(LOGIN_E2EE);
				 }
                 [self atpGetE2EEParams];
             }else if(self.fromQRCodeLogin){
                 self.fromQRCodeLogin = NO;
                 [self.delegate doLoginQRCode];
             }
             else {
                 [self.delegate updateLoginProgress:LOGIN_LOGIN];
				 //+++ Update Progress
				 if (self.loginProgressHandler) {
					 self.loginProgressHandler(LOGIN_LOGIN);
				 }
				 DLog(@"4444: atpDoLogin");
                 [self atpDoLogin];
             }
         }
         else if ([data length] == 0 && error == nil)
         {
             ////NSLog(@"Empty");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Empty" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             ////NSLog(@"Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Timed Out" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate timedOut];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil)
         {
             ////NSLog(@"Download Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Download Error" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate downloadError:error];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
     }];
}

- (NSString *)getIPAddress {
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]; // pdp_ip0
                //NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
                
                if([name isEqualToString:@"en0"]) {
                    // Interface is the wifi connection on the iPhone
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        // Interface is the cell connection on the iPhone
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    NSString *addr = wifiAddress ? wifiAddress : cellAddress;
    
    return addr;
}

- (void) atpDoLogin
{
    [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@""];
    NSString *cmdStr = @"TradeLogin";
    
    //atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]%@", atpServer,cmdStr];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[0]%@", atpServer,cmdStr];
    }
    
	
    // clear the 2FA for new user login
    // clear the SMS OTP for new user login
    [UserPrefConstants singleton].verified2FA = NO;
   // [UserPrefConstants singleton].deviceListFor2FA = [[NSMutableArray alloc] init];
    [[UserPrefConstants singleton].deviceListFor2FA removeAllObjects];
    [[UserPrefConstants singleton].deviceListForSMSOTP removeAllObjects];
    [UserPrefConstants singleton].byPass2FA = @"";
    [UserPrefConstants singleton].is2FARequired =  NO;
    [UserPrefConstants singleton].dvData2FA = nil;
    [UserPrefConstants singleton].clientLimitOptionDict = [[NSMutableDictionary alloc] init];
    
    // resetPwd things
    __block int resetPwdPinMode = 0; // 0=no reset, 1=new login, 2=pwpin expire
    
    
    NSString *postStr = @"";
    NSString *systemVersion =@"6";
    NSString *deviceModel =@"1";
    NSString *networkType =@"Wifi";
    NSString *carrierName =@"MAXIS";
    NSString *bundleVersion =@"0.1.2";
    NSString *screenHeightWidth =@"960x640";
    //    NSString *screenHeightWidth =[NSString stringWithFormat:@"%.fx%.f", SCREEN_HEIGHT_PIXELS, SCREEN_WIDTH_PIXELS];
    NSString *isATPCommpression =@"2"; //([AppConfigManager isATPCompression]?@"2":@"0")
    
    if (isE2EE_Encryption) {
        postStr = [NSString stringWithFormat:@"Request=[@RSAEnabled@]UserName=%@|Password=%@|RSAPassword=%@|E2EERandomNumber=%@|AppName=M|AppCode=MD|OSName=iOS|Version=%@|Model=%@|NetType=%@|Provider=%@|Content=%@|ScreenSize=%@|PullMode=1|PullCompress=%@|Encryption=0|EncryptedUP=0|ClientIP=%@",
                   userName,
                   e2eeInfo.e2ee_EncryptedPassword,
                   [self rsaEncryptData:password withModulus:e2eeInfo.modulus andExponent:e2eeInfo.exponent andTag:@"e2ee_publicKey"],
                   e2eeInfo.randomKey,
                   systemVersion,
                   deviceModel,
                   networkType,
                   carrierName,
                   bundleVersion,
                   screenHeightWidth,
                   isATPCommpression,
                   [self getIPAddress]];
        
        [self removeKeyRefWithTag:@"e2ee_publicKey"];
    }
    else {
    
        postStr = [NSString stringWithFormat:@"Request=UserName=%@|Password=%@|AppName=M|AppCode=MD|OSName=iOS|Version=%@|Model=%@|NetType=%@|Provider=%@|Content=%@|ScreenSize=%@|PullMode=1|PullCompress=%@|Encryption=0|EncryptedUP=1|ClientIP=%@",
                   [self encryptString:userName],
                   [self encryptString:password],
                   systemVersion,
                   deviceModel,
                   networkType,
                   carrierName,
                   bundleVersion,
                   screenHeightWidth ,
                   isATPCommpression,
                   [self getIPAddress]];
    }
    
    if ([UserPrefConstants singleton].bhCode != nil && ![[UserPrefConstants singleton].bhCode isEqualToString:@""]) {
        postStr = [NSString stringWithFormat:@"%@|BHCode=%@", postStr, [UserPrefConstants singleton].bhCode];
    }
	
	//+++ Change session key
	postStr = [NSString stringWithFormat:@"%@&Userparams=%@", postStr, currentSessionKey];
    //postStr = [NSString stringWithFormat:@"%@&Userparams=%@", postStr, [ConfigureServerSession shareInstance].currentSessionKey];
    
	NSLog(@"\n\n---- 4444: TradeLogin (Plain) ----\n%@\n%@\n\n", urlStr, postStr);
    
    postStr = [postStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding];
    // if encryption (Sure it is)
    postStr = [NSString stringWithFormat:@"10|%c%@|%@%cE",
               START_OF_TEXT,
               [self base64Encode:currentSessionKey],
               [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
               END_OF_TEXT];
    
    NSMutableData *postMutableData = [[NSMutableData alloc] initWithData:[postStr dataUsingEncoding:NSISOLatin1StringEncoding]];
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    //    NSMutableData *authData =[[NSMutableData alloc] init];;
    
    ///NSLog(@"postStr : %@",postStr);
    
    // NSLog(@"postMutableData : %@",postMutableData);
    
    // Create the request
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)postMutableData.length] forHTTPHeaderField:@"Content-Length"];
    [urlRequest setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postMutableData];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    // NSLog(@"Do Login url : %@",url);
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
                          NSLog(@"atpDoLogin Returned : %@",content);
             //             GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
             //             GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
             //GTMRegex *userParam = [GTMRegex regexWithPattern:@"^.UserParam.*"];
             //             GTMRegex *atpAliveTimeOut = [GTMRegex regexWithPattern:@"^.AliveTimeOut.*"];
             //             GTMRegex *senderCode = [GTMRegex regexWithPattern:@"^.SenderCode.*"];
             //             GTMRegex *brokerCode = [GTMRegex regexWithPattern:@"^.BrokerCode.*"];
             //             GTMRegex *branchCode = [GTMRegex regexWithPattern:@"^.BranchCode.*"];
             //             GTMRegex *senderCategory = [GTMRegex regexWithPattern:@"^.SenderCategory.*"];
             GTMRegex *skipPin = [GTMRegex regexWithPattern:@"^.SkipPin.*"];
             //             GTMRegex *publicIP = [GTMRegex regexWithPattern:@"^.PublicIP.*"];
             //             GTMRegex *privateIP = [GTMRegex regexWithPattern:@"^.PrivateIP.*"];
             GTMRegex *exchange = [GTMRegex regexWithPattern:@"^\\[Exchange\\].*"];
             GTMRegex *currencyRate = [GTMRegex regexWithPattern:@"^\\[CurrencyRate\\].*"];
             
             GTMRegex *is2FARequired = [GTMRegex regexWithPattern:@"^\\[Is2FARequired\\].*"];
             GTMRegex *byPass2FA = [GTMRegex regexWithPattern:@"^\\[2FAByPass\\].*"];
             GTMRegex *deviceList = [GTMRegex regexWithPattern:@"^\\[DeviceList\\].*"];
             
             
             GTMRegex *localExchg = [GTMRegex regexWithPattern:@"^\\[LocalExchange\\].*"];
             GTMRegex *rdsVersion = [GTMRegex regexWithPattern:@"^\\[RDSVersion\\].*"];
             GTMRegex *rdsStatus = [GTMRegex regexWithPattern:@"^\\[RDSStatus\\].*"];
             GTMRegex *resetPWReg = [GTMRegex regexWithPattern:@"^\\[ResetPwdPin\\].*"];
             GTMRegex *actionReg = [GTMRegex regexWithPattern:@"^\\[Action\\].*"];
             GTMRegex *orderTypeReg = [GTMRegex regexWithPattern:@"^\\[OrderType\\].*"];
             GTMRegex *validityReg = [GTMRegex regexWithPattern:@"^\\[Validity\\].*"];
             GTMRegex *paymentReg = [GTMRegex regexWithPattern:@"^\\[Payment\\].*"];
             GTMRegex *PaymentCfg_Payment = [GTMRegex regexWithPattern:@"^\\[PaymentCfg_Payment\\].*"];
             GTMRegex *currencyReg = [GTMRegex regexWithPattern:@"^\\[Currency\\].*"];
             GTMRegex *currencyNotSupported = [GTMRegex regexWithPattern:@"^\\[CurrencyNotSupported\\].*"];
             GTMRegex *reviseReg = [GTMRegex regexWithPattern:@"^\\[Revise\\].*"];
             GTMRegex *rev_orderTypeReg = [GTMRegex regexWithPattern:@"^\\[ReviseOT\\].*"];
             GTMRegex *rev_validityReg = [GTMRegex regexWithPattern:@"^\\[ReviseV\\].*"];
             GTMRegex *rev_currencyReg = [GTMRegex regexWithPattern:@"^\\[ReviseC\\].*"];
             GTMRegex *rev_paymentReg = [GTMRegex regexWithPattern:@"^\\[ReviseP\\].*"];
             GTMRegex *triggerPriceType = [GTMRegex regexWithPattern:@"^\\[TriggerPriceType\\].*"];
             GTMRegex *triggerPriceDirection = [GTMRegex regexWithPattern:@"^\\[TriggerPriceDirection\\].*"];
             GTMRegex *orderCtrlReg = [GTMRegex regexWithPattern:@"^\\[OrderCtrl\\].*"];
             GTMRegex *orderCtrlReg2 = [GTMRegex regexWithPattern:@"^\\[OrderCtrl2\\].*"];
             GTMRegex *reviseCtrlReg = [GTMRegex regexWithPattern:@"^\\[ReviseCtrl\\].*"];
             GTMRegex *email = [GTMRegex regexWithPattern:@"^.Email.*"];
             GTMRegex *privateIP = [GTMRegex regexWithPattern:@"^.PrivateIP.*"];
             GTMRegex *mobileNumber2FA = [GTMRegex regexWithPattern:@"^\\[2FAMobPhone\\].*"];
             GTMRegex *smsOTPInterval = [GTMRegex regexWithPattern:@"^\\[2FASMSOTPInterval\\].*"];
             GTMRegex *lastLoginDateTime = [GTMRegex regexWithPattern:@"^.LastLoginDateTime.*"];
             GTMRegex *phoneNumber = [GTMRegex regexWithPattern:@"^.Mobile.*"];
             GTMRegex *senderName = [GTMRegex regexWithPattern:@"^.SenderName.*"];
             //             GTMRegex *tncReg = [GTMRegex regexWithPattern:@"^\\[TNCInfo\\].*"];
            GTMRegex *clientLimitOptionReg = [GTMRegex regexWithPattern:@"^\\[ClientLimitOption\\].*"];
            GTMRegex *trxFeeFormulaReg = [GTMRegex regexWithPattern:@"^.TrxFeeFormula.*"];
            GTMRegex *idssEnabled = [GTMRegex regexWithPattern:@"^.IDSSEnabled.*"];
             // this one need for Portfolio data display (not yet implement)
             //             GTMRegex *mapPaymentCodeReg = [GTMRegex regexWithPattern:@"^\\[MapPaymentCode\\].*"];
             
             
            //// NSLog(@"ATPDoLogin : %@",content);
             
             NSArray *errorAndMsg = [content componentsSeparatedByString:@","];
             
             if([[errorAndMsg[0] uppercaseString] containsString:@"ERROR"])
             {
                 
                 NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                 [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
                 [notificationData setObject:content forKey:@"error"];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
				 //+++ Completion Handler
				 if (self.loginCompletionHandler) {
					 self.loginCompletionHandler(NO, notificationData, error);
				 }
				 DLog(@"EEEEERRRRROOOOOOORRRRRR");
                 return ;
             }
             
             else
             {
                 NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
                 
                 if(arr)
                 {
                     for (int i=0; i<arr.count; i++)
                     {
                         NSArray *userInfoItem = [arr[i] componentsSeparatedByString:@"="];
                         if(userInfoItem)
                         {
                             if(userInfoItem.count == 2){ // Item have both key and value. Eg:"[END]= " is invalid, "[UserParam]=abc" is valid
                                 if ([userInfoItem[0] length]>0)
                                     [userInfoDict setObject:userInfoItem[1] forKey:userInfoItem[0]];
                             }
                         }
                     }
                     
                     NSArray *userParamArr = [arr[0] componentsSeparatedByString:@"[UserParam]="];
                     if ([userParamArr count]>1) {
                         userParam = userParamArr[1];
                     }
                     
                 }
                 
                 for (NSString *s in arr) {
                     
                     if ([exchange matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Exchange]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             for (NSString *exchg in tok) {
                                 TradingRules *trdRules = [[TradingRules alloc] init];
                                 trdRules.exchg_code = exchg;
                                 if ([exchg length]>0)
                                     [[UserPrefConstants singleton].tradingRules setObject:trdRules forKey:trdRules.exchg_code];
                             }
                         }
                         
                     }
                     
                     else if ([currencyRate matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[CurrencyRate]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@";"];
                             for (NSString *str in tok) {
                                 CurrencyRate *curr = [[CurrencyRate alloc] init];
                                 NSArray *tok2 = [str componentsSeparatedByString:@","];
                                 int i = 0;
                                 for (NSString *item in tok2) {
                                     switch (i) {
                                         case 1: curr.currency = item;
                                             break;
                                         case 4: curr.buy_rate = [item doubleValue];
                                             break;
                                         case 5: curr.sell_rate = [item doubleValue];
                                             break;
                                         case 6: curr.denomination = [item intValue];
                                             break;
                                         default:
                                             break;
                                     }
                                     i++;
                                 }
                                 if ([curr.currency length]>0)
                                     [[UserPrefConstants singleton].currencyRateDict setObject:curr forKey:curr.currency];
                             }
                         }
                     }
                     
                     else if([resetPWReg matchesString:s]){
                         int number = [[self getValue:s] intValue];
                         
                        
                         //Enhancement - 12/8/2014
                         //Current possible number = 0-7, will need to change logic if in future more numbers adding in
                         //0 = Bypass,
                         //1 = ChgPwd,
                         //2 = ChgPin,
                         //3 = 1(ChgPwd) + 2(ChgPin),
                         //4 = ChgHint&Answer,
                         //5 = 4(ChgHint&Answer) + 1(ChgPwd),
                         //6 = 4(ChgHint&Answer) + 2(ChgPin),
                         //7 = 4(ChgHint&Answer) + 1(ChgPwd) + 2(ChgPin)
                         
                         //only cater number 0-7
                         if ((number & 8) == 0) {
                             //number 4,5,6,7 will enter
                             if ((number & 4) > 0) {
//                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
//                                                                                 message:@"Dear Customer, for first time login, please visit TC Plus at https://futures.rhbinvest.com to change your password, trading pin and security questions."
//                                                                                delegate:nil
//                                                                       cancelButtonTitle:@"OK"
//                                                                       otherButtonTitles:nil];
//                                 [alert show];
//                                 [alert release];
                                 
                                 resetPwdPinMode = 2; 
                             }
                             //number 0,1,2,3
                             else {
                                 int bitwiseResult = number & 3;
                                 // [delegate ASIHttpChangePasswordRequired:bitwiseResult];
                                 
                                 if (bitwiseResult != 0) {
                                     resetPwdPinMode = 2;
                                 }
                             }
                         }
                         
                         //Discontinue Login Process to LMS and QC once force change password/pin is required
                         if (resetPwdPinMode>0) {
                            //  [delegate ASIHttpAuthenticateCallback:self];
                            
                         }
                     }
                     
                     else if ([is2FARequired matchesString:s]) {
                         NSString *ss = [self getValue:s];
                         [UserPrefConstants singleton].is2FARequired = NO;
                         if ([ss isEqualToString:@"Y"] || [ss isEqualToString:@"y"]) {
                             [UserPrefConstants singleton].is2FARequired = YES;
                         }
                     }
                     else if ([byPass2FA matchesString:s]) {
                         [UserPrefConstants singleton].byPass2FA = [self getValue:s] ?: @"";
                     }
                 
                     
                     else if ([deviceList matchesString:s]) {

                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[DeviceList]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             for (NSString *device in tok) {
                                 DeviceDataFor2FA *dvData = [[DeviceDataFor2FA alloc] init];
                                 NSArray *tok2 = [device componentsSeparatedByString:@"|"];
                                 int i = 0;
                                 for (NSString *item in tok2) {
                                     switch (i) {
                                         case 0: dvData.deviceID = item;
                                             break;
                                         case 1: dvData.icNo = item;
                                             break;
                                         default:
                                             break;
                                     }
                                     i++;
                                 }
                                 [[UserPrefConstants singleton].deviceListFor2FA addObject:dvData];
                              
                             }
                         }
                     }
                     else if([mobileNumber2FA matchesString:s]){
                         NSString *ss = [self getValue:s];
                         NSLog(@"mobileNumber2FA %@",ss);
                         if (ss.length>3) {
                             [[UserPrefConstants singleton].deviceListForSMSOTP addObject:ss];
                         }
                     }else if([smsOTPInterval matchesString:s]){
                         [UserPrefConstants singleton].smsOTPInterval = [[self getValue:s] intValue];
                     }
                     
                     else if ([localExchg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[LocalExchange]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             for (NSString *exchg in tok) {
                                 [[UserPrefConstants singleton].localExchgList addObject:exchg];
                             }
                         }
                     }
                     
                     else if ([rdsVersion matchesString:s]) {
                         if ([UserPrefConstants singleton].isEnableRDS) {
                             [UserPrefConstants singleton].rdsData.rdsVersion = [self getValue:s];
                         }
                     }
                     else if ([rdsStatus matchesString:s]) {
                         if ([UserPrefConstants singleton].isEnableRDS) {
                             NSString *ss = [s stringByReplacingOccurrencesOfString:@"[RDSStatus]=" withString:@""];
                             //For debugging purpose
                             //ss = @"N|http://stgntp.itradecimb.com/gcCIMB/doc/TermCondRDSv1.0.htm";
                             //ss = @"B|http://stgntp.itradecimb.com/gcCIMB/doc/TermCondRDSBlockv1.0.htm";
                             if (![ss isEqualToString:@""]) {
                                 NSArray *tok = [ss componentsSeparatedByString:@"|"];
                                 int i = 0;
                                 for (NSString *item in tok) {
                                     switch (i) {
                                         case 0: [UserPrefConstants singleton].rdsData.rdsStatus = item;
                                             break;
                                         case 1: [UserPrefConstants singleton].rdsData.rdsURL = item;
                                             break;
                                         default:
                                             break;
                                     }
                                     i++;
                                 }
                             }
                         }
                     }

                     
                     
                     else if ([actionReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Action]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     [trdRules.actionList addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([orderTypeReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[OrderType]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     [trdRules.orderTypeList addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([validityReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Validity]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     [trdRules.validityList addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     

                     // Added PaymentCfg_Payment for CIMB SG only (Mar 2017)- this is for CUT payment
                     else if ([PaymentCfg_Payment matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[PaymentCfg_Payment]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@"|"];
                             if ([tok count] > 1) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules
                                                                           objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 PaymentCUT *paymentConfigCutData=[[PaymentCUT alloc]init];
                                 paymentConfigCutData.paymentCountry=[tok objectAtIndex:0];
                                 paymentConfigCutData.AccountType=[[tok objectAtIndex:1] stringByReplacingOccurrencesOfString:@"AccType:" withString:@""];
                                 paymentConfigCutData.paymentOptions=[[[tok objectAtIndex:2] stringByReplacingOccurrencesOfString:@"Payment:" withString:@""]componentsSeparatedByString:@","];
                                 if ([tok count]>3 )
                                     paymentConfigCutData.paymentSettings=[[tok objectAtIndex:3] stringByReplacingOccurrencesOfString:@"Sett:" withString:@""];

                                 [trdRules.paymentCfgList addObject:paymentConfigCutData];
                             }
                         }
                     }
                     
                     else if ([paymentReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Payment]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     if ([(NSString *)[tok objectAtIndex:i] rangeOfString:@"=+"].location != NSNotFound) {
                                         NSArray *tok2 = [(NSString *)[tok objectAtIndex:i] componentsSeparatedByString:@"=+"];
                                         if ([tok2 count] > 1) {
                                             AdditionalPaymentData *payment = [[AdditionalPaymentData alloc] init];
                                             payment.payment_name = (NSString *)[tok2 objectAtIndex:0];
                                             if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Buy"].location != NSNotFound) {
                                                 payment.isAllowedForBuyAction = YES;
                                             }
                                             if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Sell"].location != NSNotFound) {
                                                 payment.isAllowedForSellAction = YES;
                                             }
                                             [trdRules.paymentPlusList addObject:payment];
                                         }
                                     }
                                     else {
                                         if (![trdRules.paymentList containsObject:[tok objectAtIndex:i]]) {
                                             [trdRules.paymentList addObject:[tok objectAtIndex:i]];
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     else if ([currencyReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Currency]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     if (![trdRules.currencyList containsObject:[tok objectAtIndex:i]]) {
                                         [trdRules.currencyList addObject:[tok objectAtIndex:i]];
                                     }
                                 }
                             }
                         }
                     }
                     else if ([currencyNotSupported matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[CurrencyNotSupported]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 trdRules.currencyNotSupported = [tok objectAtIndex:2];
                             }
                         }
                     }
                     else if ([reviseReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[Revise]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     [trdRules.reviseParam addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([rev_orderTypeReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseOT]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     [trdRules.reviseOrdTypeParam addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([rev_validityReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseV]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     [trdRules.reviseValidityParam addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([rev_currencyReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseC]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     [trdRules.reviseCurrencyParam addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([rev_paymentReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ReviseP]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 2) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 2; i < [tok count]; i++) {
                                     if ([(NSString *)[tok objectAtIndex:i] rangeOfString:@"=+"].location != NSNotFound) {
                                         NSArray *tok2 = [(NSString *)[tok objectAtIndex:i] componentsSeparatedByString:@"=+"];
                                         if ([tok2 count] > 1) {
                                             AdditionalPaymentData *payment = [[AdditionalPaymentData alloc] init];
                                             payment.payment_name = (NSString *)[tok2 objectAtIndex:0];
                                             if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Buy"].location != NSNotFound) {
                                                 payment.isAllowedForBuyAction = YES;
                                             }
                                             if ([(NSString *)[tok2 objectAtIndex:1] rangeOfString:@"Sell"].location != NSNotFound) {
                                                 payment.isAllowedForSellAction = YES;
                                             }
                                             [trdRules.revisePaymentPlusParam addObject:payment];
                                         }
                                     }
                                     else {
                                         if (![trdRules.revisePaymentParam containsObject:[tok objectAtIndex:i]]) {
                                             [trdRules.revisePaymentParam addObject:[tok objectAtIndex:i]];
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     else if ([triggerPriceType matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[TriggerPriceType]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 1) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 1; i < [tok count]; i++) {
                                     [trdRules.triggerPriceTypeList addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([triggerPriceDirection matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[TriggerPriceDirection]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] > 1) {
                                 TradingRules *trdRules = (TradingRules *)[[UserPrefConstants singleton].tradingRules objectForKey:(NSString *)[tok objectAtIndex:0]];
                                 for (int i = 1; i < [tok count]; i++) {
                                     [trdRules.triggerPriceDirectionList addObject:[tok objectAtIndex:i]];
                                 }
                             }
                         }
                     }
                     else if ([clientLimitOptionReg matchesString:s]) {
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[ClientLimitOption]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] == 2) {
                                 [[UserPrefConstants singleton].clientLimitOptionDict setObject:[tok objectAtIndex:1] forKey:[tok objectAtIndex:0]];
                             }
                         }
                     }
                     else if([idssEnabled matchesString:s]){
                         [UserPrefConstants singleton].idssValue = [self getValue:s];
                         
                         //IDSS
                         //                                                            0 - enable both IDSS buy and sell
                         //                                                            1 - enable IDSS buy
                         //                                                            2 - only enable IDSS sell
                         
                         
                     }
                     else if ([orderCtrlReg matchesString:s]) {
                         NSString *content = [s stringByReplacingOccurrencesOfString:@"[OrderCtrl]=" withString:@""];
                         if (![content isEqualToString:@""]) {
                             NSArray *temp = [content componentsSeparatedByString:@";"];
                             
                             OrderControl *ordCtrl = [[OrderControl alloc] init];
                             NSString *exchg = @"";
                             
                             if ([temp count] > 1) {
                                 
                                 NSArray *part1 = [(NSString *)[temp objectAtIndex:0] componentsSeparatedByString:@","];
                                 int i = 0;
                                 for (NSString *item in part1) {
                                     switch (i) {
                                         case 0: exchg = item;
                                             break;
                                         case 1: ordCtrl.orderType = item;
                                             break;
                                         default: [ordCtrl.validityList addObject:item];
                                             break;
                                     }
                                     i++;
                                 }
                                 
                                 NSArray *part2 = [(NSString *)[temp objectAtIndex:1] componentsSeparatedByString:@","];
                                 for (NSString *field in part2) {
                                     NSArray *field_parts = [field componentsSeparatedByString:@"="];
                                     int i = 0;
                                     NSString *enable_field = @"";
                                     for (NSString *part in field_parts) {
                                         switch (i) {
                                             case 0: [ordCtrl.enable_fields addObject:part];
                                                 enable_field = part;
                                                 break;
                                             case 1:
                                             {
                                                 if ([enable_field isEqualToString:@"Disclosed"]) {
                                                     [ordCtrl.disclosed_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
                                                 }
                                                 else if ([enable_field isEqualToString:@"Min"]) {
                                                     [ordCtrl.min_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
                                                 }
                                                 break;
                                             }
                                             default:
                                                 break;
                                         }
                                         i++;
                                     }
                                 }
                             }
                             TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:exchg];
                             if ([ordCtrl.orderType length]>0)
                                 [trdRules.ordCtrlConditions setObject:ordCtrl forKey:ordCtrl.orderType];
                         }
                     }
                     else if ([skipPin matchesString:s]) {
                         BOOL found = NO;
                         for (NSString *category in (NSArray *)[[self getValue:s] componentsSeparatedByString:@","]) {
                             if ([category isEqualToString:[userInfoDict objectForKey:@"[SenderCategory]"]]) {
                                 found = YES;
                                 break;
                             }
                         }
                         if (found) {
                             [UserPrefConstants singleton].skipPin = YES;
                         }
                         else {
                             [UserPrefConstants singleton].skipPin = NO;
                         }
                     }
                     else if([email matchesString:s]){
                         [UserPrefConstants singleton].userEmail = [self getValue:s];
                     }
                     else if([lastLoginDateTime matchesString:s]){
                         [UserPrefConstants singleton].lastLoginTime = [self getValue:s];
                     }
                     else if([phoneNumber matchesString:s]){
                         [UserPrefConstants singleton].phoneNumber = [self getValue:s];
                     }
                     else if([senderName matchesString:s]){
                         [UserPrefConstants singleton].senderName = [self getValue:s];
                     }
                     
                     else if ([orderCtrlReg2 matchesString:s]) {
                         NSString *content = [s stringByReplacingOccurrencesOfString:@"[OrderCtrl2]=" withString:@""];
                         if (![content isEqualToString:@""]) {
                             NSArray *temp = [content componentsSeparatedByString:@";"];
                             
                             OrderControl *ordCtrl = [[OrderControl alloc] init];
                             NSString *exchg = @"";
                             
                             if ([temp count] > 1) {
                                 
                                 NSArray *part1 = [(NSString *)[temp objectAtIndex:0] componentsSeparatedByString:@","];
                                 int i = 0;
                                 for (NSString *item in part1) {
                                     switch (i) {
                                         case 0: exchg = item;
                                             break;
                                         case 1: ordCtrl.orderType = item;
                                             break;
                                         default: [ordCtrl.validityList addObject:item];
                                             break;
                                     }
                                     i++;
                                 }
                                 
                                 NSArray *part2 = [(NSString *)[temp objectAtIndex:1] componentsSeparatedByString:@","];
                                 for (NSString *field in part2) {
                                     NSArray *field_parts = [field componentsSeparatedByString:@"="];
                                     int i = 0;
                                     NSString *enable_field = @"";
                                     for (NSString *part in field_parts) {
                                         switch (i) {
                                             case 0: [ordCtrl.enable_fields addObject:part];
                                                 enable_field = part;
                                                 break;
                                             case 1:
                                             {
                                                 if ([enable_field isEqualToString:@"Disclosed"]) {
                                                     [ordCtrl.disclosed_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
                                                 }
                                                 else if ([enable_field isEqualToString:@"Min"]) {
                                                     [ordCtrl.min_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
                                                 }
                                                 break;
                                             }
                                             default:
                                                 break;
                                         }
                                         i++;
                                     }
                                 }
                             }
                             TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:exchg];
                             if ([ordCtrl.orderType length]>0)
                                 [trdRules.ordCtrlConditions setObject:ordCtrl forKey:ordCtrl.orderType];
                         }
                     }else if ([privateIP matchesString:s]){
                           [UserPrefConstants singleton].privateIP = [self getValue:s];
                     }
                     else if([trxFeeFormulaReg matchesString:s]){
                         
                         NSString *ss = [s stringByReplacingOccurrencesOfString:@"[TrxFeeFormula]=" withString:@""];
                         if (![ss isEqualToString:@""]) {
                             NSArray *tok = [ss componentsSeparatedByString:@","];
                             if ([tok count] == 3) {
                                 [[UserPrefConstants singleton].trxFeeFormulaDict setObject:[tok objectAtIndex:2] forKey:[tok objectAtIndex:1]];
                             }else if([tok count] == 4){
                                 
                                 DLog(@"MinValue %@",[tok objectAtIndex:3]);
                                 NSString *minValue = [[[tok objectAtIndex:3] componentsSeparatedByString:@"="] objectAtIndex:1];
                                 DLog(@"MinValue %@",minValue);
                                 NSString *keyValue =  [[[tok objectAtIndex:3] componentsSeparatedByString:@"="] objectAtIndex:0];
                                 [[UserPrefConstants singleton].trxFeeFormulaDict setObject:[tok objectAtIndex:2] forKey:[tok objectAtIndex:1]];
                                 [[UserPrefConstants singleton].trxFeeFormulaDict setObject:minValue forKey:keyValue];
                             }
                         }
                         DLog(@"userAccountInfo.trxFeeFormulaDict %@",[UserPrefConstants singleton].trxFeeFormulaDict);
                         
                     }
                     else if ([reviseCtrlReg matchesString:s]) {
                         NSString *content = [s stringByReplacingOccurrencesOfString:@"[ReviseCtrl]=" withString:@""];
                         if (![content isEqualToString:@""]) {
                             NSArray *temp = [content componentsSeparatedByString:@";"];
                             
                             OrderControl *ordCtrl = [[OrderControl alloc] init];
                             NSString *exchg = @"";
                             
                             if ([temp count] > 1) {
                                 
                                 NSArray *part1 = [(NSString *)[temp objectAtIndex:0] componentsSeparatedByString:@","];
                                 int i = 0;
                                 for (NSString *item in part1) {
                                     switch (i) {
                                         case 0: exchg = item;
                                             break;
                                         case 1: ordCtrl.orderType = item;
                                             break;
                                         default: [ordCtrl.validityList addObject:item];
                                             break;
                                     }
                                     i++;
                                 }
                                 
                                 NSArray *part2 = [(NSString *)[temp objectAtIndex:1] componentsSeparatedByString:@","];
                                 for (NSString *field in part2) {
                                     NSArray *field_parts = [field componentsSeparatedByString:@"="];
                                     int i = 0;
                                     NSString *enable_field = @"";
                                     for (NSString *part in field_parts) {
                                         switch (i) {
                                             case 0: [ordCtrl.enable_fields addObject:part];
                                                 enable_field = part;
                                                 break;
                                             case 1:
                                             {
                                                 if ([enable_field isEqualToString:@"Disclosed"]) {
                                                     [ordCtrl.disclosed_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
                                                 }
                                                 else if ([enable_field isEqualToString:@"Min"]) {
                                                     [ordCtrl.min_validityList addObjectsFromArray:[part componentsSeparatedByString:@"."]];
                                                 }
                                                 break;
                                             }
                                             default:
                                                 break;
                                         }
                                         i++;
                                     }
                                 }
                             }
                             TradingRules *trdRules = [[UserPrefConstants singleton].tradingRules objectForKey:exchg];
                             if ([ordCtrl.orderType length]>0)
                                 [trdRules.reviseCtrlConditions setObject:ordCtrl forKey:ordCtrl.orderType];
                             
                         }
                     }
                 }
             }
             
             
             ////NSLog(@"userParam : %@",userParam);
             // //NSLog(@"userInfoDict : %@",[UserPrefConstants singleton].tradingRules);
             [UserPrefConstants singleton].userInfoDict = userInfoDict;
             //[self getWatchList];
             [self getExchangelist];
             //Send post notification login success
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Success" forKey:@"loginStatusCode"];
             
             if (resetPwdPinMode>0) {
                 [notificationData setObject:[NSNumber numberWithInt:resetPwdPinMode] forKey:@"Password"];
             }
             
             [self.delegate updateLoginProgress:LOGIN_SUCCESS];
			 //+++ Update Progress
			 if (self.loginProgressHandler) {
				 self.loginProgressHandler(LOGIN_SUCCESS);
			 }
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
			 //+++ Timer for ipad Version
			 if (IS_IPAD) {
				 [self initTimerLogin];
			 }
			 [self atpGetClientList];
			 //+++ Completion Handler Successfully
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(YES, content, error);
			 }
         }
         else if ([data length] == 0 && error == nil)
         {
             ////NSLog(@"Empty");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Empty" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             ////NSLog(@"Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Timed Out" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate timedOut];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
         else if (error != nil)
         {
             ////NSLog(@"Download Error");
             //Send post notification
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:[LanguageManager stringForKey:@"%@ Download Error" withPlaceholders:@{@"%apiObject%":cmdStr}] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             //            [delegate downloadError:error];
			 //+++ Completion Handler
			 if (self.loginCompletionHandler) {
				 self.loginCompletionHandler(NO, notificationData, error);
			 }
         }
     }];
}






#pragma mark - Watchlist


- (void) getWatchList
{
    NSString *postStr = [NSString stringWithFormat:@"*=GetFavListInfo|+=%@|,=1", [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"]];
    
    //atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
        
    }
    
    /// NSLog(@"\n\n---- Favorate GetFavListInfo (Plain) ----\n%@\n\n", urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
             
             
             [UserPrefConstants singleton].userWatchListArray = [[NSMutableArray alloc] init];
             
             NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
             // NSLog(@"getWatchList contentArr : %@",contentArr);
             if(contentArr)
             {
             
                 NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                 [dateFormatter setDateFormat:@"yyyyMMddHHmmss.SSS"];
                 
                 for (int i=1; i<contentArr.count; i++)
                 {
                     NSArray *userWatchListHeaderItem = [contentArr[i] componentsSeparatedByString:@"|"];
                     if(userWatchListHeaderItem)
                     {
                         NSMutableDictionary *userWatchListDict= [[NSMutableDictionary alloc] init];
                         [userWatchListDict setObject:userWatchListHeaderItem[1] forKey:@"FavID"];
                         [userWatchListDict setObject:userWatchListHeaderItem[2] forKey:@"Name"];
                         
                         NSDate *timeStampDate = [dateFormatter dateFromString:userWatchListHeaderItem[7]];
                         // NSLog(@"timeStampDate %@", timeStampDate);
                         
                         [userWatchListDict setObject:timeStampDate forKey:@"TimeStamp"];
                         [[UserPrefConstants singleton].userWatchListArray addObject:userWatchListDict];
                     }
                 }
             }
             
             
             if ([[UserPrefConstants singleton].userWatchListArray count]>0) {
                 // sort it based on time created
                 NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"TimeStamp" ascending:YES];
                 NSArray *sortedArr = [[UserPrefConstants singleton].userWatchListArray sortedArrayUsingDescriptors:@[descriptor]];
                 [UserPrefConstants singleton].userWatchListArray = [sortedArr mutableCopy];
             }
			 
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Send post notification getWatchlistSuccess
				 [[NSNotificationCenter defaultCenter] postNotificationName:@"getWatchlistSuccess" object:self userInfo:nil];
				 //Success with completion
				 if(self.watchlistCompletion){
					 self.watchlistCompletion(YES,  [UserPrefConstants singleton].userWatchListArray, nil);
				 }
			 });
         }
         else if ([data length] == 0 && error == nil)
         {
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Send post notification getWatchlistSuccess
				 if(self.watchlistCompletion){
					 self.watchlistCompletion(NO, nil, error);
				 }
			 });
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Send post notification getWatchlistSuccess
				 if(self.watchlistCompletion){
					 self.watchlistCompletion(NO, nil, error);
				 }
			 });
         }
         else if (error != nil)
         {
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Send post notification getWatchlistSuccess
				 if(self.watchlistCompletion){
					 self.watchlistCompletion(NO, nil, error);
				 }
			 });
		 }else{
			 if(self.watchlistCompletion){
				 self.watchlistCompletion(NO, nil, error);
			 }
		 }
     }];
    
}

// Getting a list of stocks for a particular WatchList
- (void) getWatchListItems:(int)favID
{
    NSString *postStr = [NSString stringWithFormat:@"*=GetFavListStkCodeCache|+=%@|-=%d", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {

        urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr];
    }
	
    ///// NSLog(@"\n\n---- Favorate GetFavListInfo (Plain) ----\n%@\n\n", urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
	
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
											 cachePolicy:NSURLRequestReloadIgnoringCacheData
										 timeoutInterval:kURLRequestTimeout];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
             NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
             // NSLog(@"getWatchListItems contentArr : %@",contentArr);
             NSMutableArray *stockListArr = [[NSMutableArray alloc] init];

             if(contentArr)
             {
                 for (int i=1; i<contentArr.count; i++)
                 {
                     NSString *cleanStr = [[[contentArr[i] description]
                                            stringByReplacingOccurrencesOfString: @")" withString: @""]
                                           stringByReplacingOccurrencesOfString: @"=" withString: @""];
                     //NSLog(@"cleanStr %@",cleanStr);
                     
                     NSArray *userWatchListItem = [cleanStr componentsSeparatedByString:@"|"];
                     NSString *stkCodeOnly = userWatchListItem[0];
                     NSString *exchangeOnly = userWatchListItem[1];
                     
                     if ([exchangeOnly length]<=0) exchangeOnly = [UserPrefConstants singleton].userCurrentExchange;
                     
                     if ([exchangeOnly isEqualToString:@"SI"]) exchangeOnly = @"SG";
                     
                     NSString *stkCodeFull = [[stkCodeOnly stringByAppendingString:@"."]stringByAppendingString:exchangeOnly];
                     
                     
                     // NSLog(@"Adding %@",stkCodeFull);
                     
                     if (![stockListArr containsObject:stkCodeFull]) [stockListArr addObject:stkCodeFull];
                 }
             }
             //Collected array of stock , Now get details of stocks and let VCM handle the QCData Update.
             //NSLog(@"stockListArr : %@",stockListArr);
			 dispatch_async(dispatch_get_main_queue(), ^{
				 [UserPrefConstants singleton].userWatchListStockCodeArr = stockListArr;
				 [[VertxConnectionManager singleton]vertxGetStockDetailWithStockArr:stockListArr FIDArr:[[UserPrefConstants singleton]userQuoteScreenColumn] tag:0];
			 });
			 return;
         }
         else if ([data length] == 0 && error == nil)
         {
             //NSLog(@"Empty");
             
             //            [delegate emptyReply];
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             //NSLog(@"Error");
             //            [delegate timedOut];
         }
         else if (error != nil)
         {
             //NSLog(@"Download Error %@",error);
             //            [delegate downloadError:error];
         }
		
		 dispatch_async(dispatch_get_main_queue(), ^{
			 //+++ Post notification error loading content of watchlist
			 [[NSNotificationCenter defaultCenter] postNotificationName:kDidErrorLoadingContentOfWatchlist object:error];
		 });
     }];
}

// Getting a list of stocks for a particular WatchList
- (void) getWatchListItemsForStreamer:(int)favID
{
    NSString *postStr = [NSString stringWithFormat:@"*=GetFavListStkCodeCache|+=%@|-=%d", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
    
    //    urlStr = [NSString stringWithFormat:@"http://%@/[0]Favorate?10|%c%@|%@%cE",
    //              atpServer,
    //              START_OF_TEXT,
    //              [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
    //              [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey], END_OF_TEXT];
    
    //atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr];
    }
    
    
    
    ///// NSLog(@"\n\n---- Favorate GetFavListInfo (Plain) ----\n%@\n\n", urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
             NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
             // NSLog(@"getWatchListItems contentArr : %@",contentArr);
             NSMutableArray *stockListArr = [[NSMutableArray alloc] init];
             
             if(contentArr)
             {
                 for (int i=1; i<contentArr.count; i++)
                 {
                     NSString *cleanStr = [[[contentArr[i] description]
                                            stringByReplacingOccurrencesOfString: @")" withString: @""]
                                           stringByReplacingOccurrencesOfString: @"=" withString: @""];
                     //NSLog(@"cleanStr %@",cleanStr);
                     
                     NSArray *userWatchListItem = [cleanStr componentsSeparatedByString:@"|"];
                     NSString *stkCodeOnly = userWatchListItem[0];
                     NSString *exchangeOnly = userWatchListItem[1];
                     
                     if ([exchangeOnly length]<=0) exchangeOnly = [UserPrefConstants singleton].userCurrentExchange;
                     
                     if ([exchangeOnly isEqualToString:@"SI"]) exchangeOnly = @"SG";
                     
                     NSString *stkCodeFull = [[stkCodeOnly stringByAppendingString:@"."]stringByAppendingString:exchangeOnly];
                     
                     
                     // NSLog(@"Adding %@",stkCodeFull);
                     
                     if (![stockListArr containsObject:stkCodeFull]) [stockListArr addObject:stkCodeFull];
                 }
             }
             //Collected array of stock , Now get details of stocks and let VCM handle the QCData Update.
             //NSLog(@"stockListArr : %@",stockListArr);
             [UserPrefConstants singleton].userWatchListStockCodeArr = stockListArr;
           [[NSNotificationCenter defaultCenter] postNotificationName:@"getWatchlistContentSuccess" object:self userInfo:nil];
         }
         else if ([data length] == 0 && error == nil)
         {
             //NSLog(@"Empty");
             
             //            [delegate emptyReply];
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             //NSLog(@"Error");
             //            [delegate timedOut];
         }
         else if (error != nil)
         {
             //NSLog(@"Download Error %@",error);
             //            [delegate downloadError:error];
         }
     }];
    
}



- (void) addOrUpdateWatchlistName:(int)favID withFavName:(NSString *)favName
{
    //Apr 6 Use back Non-encryption.
    //    urlStr = [NSString stringWithFormat:@"http://%@/[0]Favorate", atpServer];
    urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"]];
    
    NSString *postStr = [NSString stringWithFormat:
                         @"Request=*=UpdFavListName|+=%@|-=%d|.=%@|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],
                         favID,
                         favName];
    ////NSLog(@"\n\n---- Favorate UpdFavListName (Plain) ----\n%@\n%@\n\n", urlStr, postStr);
    CFStringRef stringRef = CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)postStr, NULL, (CFStringRef)@"!*'\"();:@&=+$,/?%#[]-.", kCFStringEncodingUTF8);
    postStr = (__bridge_transfer NSString *)stringRef;
    //Apr 6 Use non-encryption.
    //    postStr = [NSString stringWithFormat:@"10|%c%@|%@%cE", START_OF_TEXT, [self base64Encode:[[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"]],  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey], END_OF_TEXT];
    ////NSLog(@"\n\n---- Favorate UpdFavListName (AES Encrypted + Base64 Encoding) ----\n%@\n%@\n\n", urlStr, postStr);
    NSMutableData *postMutableData = [[NSMutableData alloc] initWithData:[postStr dataUsingEncoding:NSISOLatin1StringEncoding]];
    // Create the request
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)postMutableData.length] forHTTPHeaderField:@"Content-Length"];
    [urlRequest setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postMutableData];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {

             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
             
             // NSLog(@"addOrUpdateWatchlistName Returned atpDecrypted : %@",content);
             NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
            // NSLog(@"addOrUpdateWatchlistName contentArr : %@",contentArr);
             if(contentArr)
             {
             }
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Success with completion
				 if(self.addOrUpdateWatchlistCompletion){
					 self.addOrUpdateWatchlistCompletion(NO, @"There is some error from server", nil);
				 }
			 });
             
         }
         else if ([data length] == 0 && error == nil)
         {
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Send post notification getWatchlistSuccess
				 [[NSNotificationCenter defaultCenter] postNotificationName:@"addOrUpdateWatchlistNameSuccessfully" object:self userInfo:nil];
				 //Success with completion
				 if(self.addOrUpdateWatchlistCompletion){
					 self.addOrUpdateWatchlistCompletion(YES, nil, nil);
				 }
			 });
         }
		 else if (error != nil && error.code == NSURLErrorTimedOut){
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Success with completion
				 if(self.addOrUpdateWatchlistCompletion){
					 self.addOrUpdateWatchlistCompletion(NO, nil, error);
				 }
			 });
		 }else if (error != nil){
			 dispatch_async(dispatch_get_main_queue(), ^{
				 //Success with completion
				 if(self.addOrUpdateWatchlistCompletion){
					 self.addOrUpdateWatchlistCompletion(NO, nil, error);
				 }
			 });
			 NSLog(@"addOrUpdateWatchlistName Download Error %@",error);
		 }
     }];
}


- (void) deleteWatchlist:(int)favID
{
    NSString *postStr = [NSString stringWithFormat:@"*=DelFavList|+=%@|-=%d|4=1", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
    urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    // NSLog(@"Delete URL: %@", urlStr);
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             ////NSLog(@"deleteWatchlist response : %@",response);
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             ////NSLog(@"deleteWatchlist Returned : %@",content);
             content = [self atpDecryptionProcess:content];
             ////NSLog(@"deleteWatchlist Returned atpDecrypted : %@",content);
             NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
             ////NSLog(@"deleteWatchlist contentArr : %@",contentArr);
             if(contentArr)
             {
             }
			 dispatch_async(dispatch_get_main_queue(), ^{
				 ////NSLog(@"deleteWatchlist Deleted Successfully");
				 //Success with completion
				 if(self.deleteWatchlistCompletion){
					 self.deleteWatchlistCompletion(NO, nil, error);
				 }
			 });
         }
         else if ([data length] == 0 && error == nil)
         {
			 dispatch_async(dispatch_get_main_queue(), ^{
				 ////NSLog(@"deleteWatchlist Deleted Successfully");
				 [[NSNotificationCenter defaultCenter] postNotificationName:@"watchlistDeleted" object:self userInfo:nil];
				 //Success with completion
				 if(self.deleteWatchlistCompletion){
					 self.deleteWatchlistCompletion(YES, nil, nil);
				 }
			 });
             
         }
         else if (error != nil && error.code == NSURLErrorTimedOut){
			 dispatch_async(dispatch_get_main_queue(), ^{
				 ////NSLog(@"deleteWatchlist Deleted Successfully");
				 //Success with completion
				 if(self.deleteWatchlistCompletion){
					 self.deleteWatchlistCompletion(NO, nil, error);
				 }
			 });
         }
            // NSLog(@"deleteWatchlist Error");
         else if (error != nil){
			 dispatch_async(dispatch_get_main_queue(), ^{
				 ////NSLog(@"deleteWatchlist Deleted Successfully");
				 //Success with completion
				 if(self.deleteWatchlistCompletion){
					 self.deleteWatchlistCompletion(NO, nil, error);
				 }
			 });
         }
            // NSLog(@"deleteWatchlist Download Error %@",error);
     }];
}

// With duplicate checking.
// Honestly, the duplicate checking should be done at back end...... zzzz

- (void)addWatchlistCheckItem:(int)favID withItem:(NSString *)stock_code
{
    NSString *postStr = [NSString stringWithFormat:@"*=GetFavListStkCodeCache|+=%@|-=%d", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID];
	__block NSString *_atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]Favorate?%@",
                  _atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@",
                  _atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr];
    }
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
             NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
             //  //NSLog(@"stockListArr : %@",stockListArr);
             if(contentArr)
             {
                 __block NSMutableArray *listArr = [[NSMutableArray alloc] init];
                 
                 for (int i=1; i<contentArr.count; i++)
                 {
                     NSString *cleanStr = [[[contentArr[i] description]
                                            stringByReplacingOccurrencesOfString: @")" withString: @""]
                                           stringByReplacingOccurrencesOfString: @"=" withString: @""];
                     // NSLog(@"cleanStr %@",cleanStr);
                     
                     NSArray *userWatchListItem = [cleanStr componentsSeparatedByString:@"|"];
                     NSString *stkCodeOnly = userWatchListItem[0];
                     NSString *exchangeOnly = userWatchListItem[1];
                     
                     if ([exchangeOnly length]<=0) exchangeOnly = [[UserPrefConstants singleton].userCurrentExchange copy];
                     
                     if ([exchangeOnly isEqualToString:@"SI"]) exchangeOnly = @"SG";
                     
                     NSString *stkCodeFull = [[stkCodeOnly stringByAppendingString:@"."]stringByAppendingString:exchangeOnly];
                     [listArr addObject:stkCodeFull];
                 
                 }
                 
                 //NSLog(@"listArr %@", listArr);
                 
                 // Filter can add stocks here
                 NSMutableArray *addingStocks = [[stock_code componentsSeparatedByString:@","] mutableCopy];
                 NSMutableArray *duplicateStocks = [[NSMutableArray alloc] init];
                 
                 //NSLog(@"addingStocks %@", addingStocks);
                 
                 for (int i=0; i<[addingStocks count]; i++) {
                     NSString *stk = [addingStocks objectAtIndex:i];
                     if ([listArr containsObject:stk]) {
                         [addingStocks removeObject:stk];
                         [duplicateStocks addObject:stk];
                     }
                 }
                 
                 __block NSString *someDuplicate = @"";
                 
                 // no stock to add since all stocks are duplicates
                 if (([duplicateStocks count]>0)&&([addingStocks count]<=0)) {
                     
                     NSDictionary *replyDict = @{@"replyDict":@"AllDuplicate",@"DuplicateStocks":[duplicateStocks componentsJoinedByString:@","]};
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self
                                                                       userInfo:replyDict];
					 dispatch_async(dispatch_get_main_queue(), ^{
						 ////NSLog(@"deleteWatchlist Deleted Successfully");
						 //Success with completion
						 if(self.addStockToWatchlistCompletion){
							 self.addStockToWatchlistCompletion(YES, replyDict, nil);
						 }
					 });
                     
                     return;
                 }
                 
                 // if duplicateStocks exist, but addingstocks also exist
                 
                 if (([duplicateStocks count]>0)&&([addingStocks count]>0)) {
                     someDuplicate = [duplicateStocks componentsJoinedByString:@","];
                 }
                 
                 NSString *resolvedStocksToAdd = [addingStocks componentsJoinedByString:@","];
                 
                 NSString *postStr = [NSString stringWithFormat:
                                      @"*=UpdFavListStk|+=%@|-=%d|.=A|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=|2=|6=%@",
                                      [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],
                                      favID,
                                      resolvedStocksToAdd];
                 urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", _atpServer,[[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
                 
                 NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
                 NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
                 NSOperationQueue *queue = [NSOperationQueue mainQueue];
                 
                 [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
                  {
                      
                      NSMutableArray *replyArr = [[NSMutableArray alloc] init];
                      
                      if ([data length] > 0 && error == nil)
                      {
                          
                          NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                          // NSLog(@"addWatchlistItem Returned : %@",content);
                          content = [self atpDecryptionProcess:content];
                          // NSLog(@"addWatchlistItem Returned atpDecrypted : %@",content);
                          NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
                          // NSLog(@"addWatchlistItem Returned atpDecrypted : %@",contentArr);
                          
                          /*
                           1=stkcode
                           7=return val
                           8=msg
                           "(=1|7|8|",
                           ")=01288|0||"
                           
                           Funny thing about this reply is if same stock added to the list,
                           it also returns same reply: stkcode|0||.
                           
                           */
                          
                          
                          
                          if ([contentArr count]>1) {
                              
                              GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
                              GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
                              
                              /// NSLog(@"TradeStatus Reply --------- \n\n %@ \n\n", content);
                              
                              int stk_code = -1;             //1
                              int return_val = -1;			//7
                              int return_msg = -1;			//8
                              
                              
                              
                              NSString *s;
                              for (s in contentArr) {
                                  
                                  if ([metaregex matchesString:s]) {
                                      
                                      s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                                      NSArray *tokens = [s componentsSeparatedByString:@"|"];
                                      if ([tokens count] > 2) {
                                          int pos = 0;
                                          for (NSString *token in tokens) {
                                              if ([token isEqualToString:@"1"]) {
                                                  stk_code = pos;
                                              }
                                              if ([token isEqualToString:@"7"]) {
                                                  return_val = pos;
                                              }
                                              if ([token isEqualToString:@"8"]) {
                                                  return_msg = pos;
                                              }
                                              pos++;
                                          }
                                      }
                                  } else if ([dataregex matchesString:s]) {
                                      s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                                      NSArray *tokens = [s componentsSeparatedByString:@"|"];
                                      if ([tokens count] > 2) {
                                          NSMutableDictionary *WLReplyDict = [[NSMutableDictionary alloc] init];
                                          if (stk_code != -1) {
                                              [WLReplyDict setObject:[tokens objectAtIndex:stk_code] forKey:@"1"];
                                          }
                                          if (return_val != -1) {
                                              [WLReplyDict setObject:[tokens objectAtIndex:return_val] forKey:@"7"];
                                          }
                                          if (return_msg != -1) {
                                              [WLReplyDict setObject:[tokens objectAtIndex:return_msg] forKey:@"8"];
                                          }
                                          [replyArr addObject:WLReplyDict];
                                      }
                                  }
                              }
                          }
                          
                          NSDictionary *replyDict = @{@"replyArr":replyArr,@"DuplicateStocks":someDuplicate};
                          
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
						  dispatch_async(dispatch_get_main_queue(), ^{
							  if(self.addStockToWatchlistCompletion){
								  self.addStockToWatchlistCompletion(YES, replyDict, nil);
							  }
						  });
                          
                      }
                      else if ([data length] == 0 && error == nil)
                      {
                          NSDictionary *replyDict = @{@"replyDict":@"NoData"};
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
						  dispatch_async(dispatch_get_main_queue(), ^{
							  if(self.addStockToWatchlistCompletion){
								  self.addStockToWatchlistCompletion(YES, replyDict, nil);
							  }
						  });
                      }
                      else if (error != nil && error.code == NSURLErrorTimedOut) {
                          NSDictionary *replyDict = @{@"replyDict":@"TimedOut"};
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
						  dispatch_async(dispatch_get_main_queue(), ^{
							  if(self.addStockToWatchlistCompletion){
								  self.addStockToWatchlistCompletion(NO, replyDict, error);
							  }
						  });
                      } else if (error != nil) {
                          NSDictionary *replyDict = @{@"replyDict":@"Error"};
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
						  dispatch_async(dispatch_get_main_queue(), ^{
							  if(self.addStockToWatchlistCompletion){
								  self.addStockToWatchlistCompletion(NO, replyDict, error);
							  }
						  });
                          
                      }
                  }];
                 
                 
             }
         }
         else if ([data length] == 0 && error == nil)
         {
             //NSLog(@"Empty");
             
             //            [delegate emptyReply];
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             //NSLog(@"Error");
             //            [delegate timedOut];
         }
         else if (error != nil)
         {
             //NSLog(@"Download Error %@",error);
             //            [delegate downloadError:error];
         }
     }];
}


// DEPRECATED BY EMIR JUN 2017---
// Pls use the addWatchlistCheckItem instead
// Needed to check if there are duplicates. Apparently checking is not done on back end side.


- (void) addWatchlistItem:(int)favID withItem:(NSString *)stock_code
{
    
    NSString *postStr = [NSString stringWithFormat:
                         @"*=UpdFavListStk|+=%@|-=%d|.=A|/=0|0=AC2B9B1AB3654898A049E153A06F5AF80.8621339868992101|1=|2=|6=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],
                         favID,
                         stock_code];
    urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", atpServer,[[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         NSMutableArray *replyArr = [[NSMutableArray alloc] init];
         
         if ([data length] > 0 && error == nil)
         {
             
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             // NSLog(@"addWatchlistItem Returned : %@",content);
             content = [self atpDecryptionProcess:content];
             // NSLog(@"addWatchlistItem Returned atpDecrypted : %@",content);
             NSArray *contentArr = [content componentsSeparatedByString:@"\r\n"];
             // NSLog(@"addWatchlistItem Returned atpDecrypted : %@",contentArr);
             
             /*
              1=stkcode
              7=return val
              8=msg
              "(=1|7|8|",
              ")=01288|0||"
              
              Funny thing about this reply is if same stock added to the list,
              it also returns same reply: stkcode|0||.
              
              */
             
             
             
             if ([contentArr count]>1) {
                 
                 GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
                 GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
                 
                 /// NSLog(@"TradeStatus Reply --------- \n\n %@ \n\n", content);
                 
                 int stk_code = -1;             //1
                 int return_val = -1;			//7
                 int return_msg = -1;			//8
                 
                 
                 
                 NSString *s;
                 for (s in contentArr) {
                     
                     if ([metaregex matchesString:s]) {
                         
                         s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                         NSArray *tokens = [s componentsSeparatedByString:@"|"];
                         if ([tokens count] > 2) {
                             int pos = 0;
                             for (NSString *token in tokens) {
                                 if ([token isEqualToString:@"1"]) {
                                     stk_code = pos;
                                 }
                                 if ([token isEqualToString:@"7"]) {
                                     return_val = pos;
                                 }
                                 if ([token isEqualToString:@"8"]) {
                                     return_msg = pos;
                                 }
                                 pos++;
                             }
                         }
                     } else if ([dataregex matchesString:s]) {
                         s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                         NSArray *tokens = [s componentsSeparatedByString:@"|"];
                         if ([tokens count] > 2) {
                             NSMutableDictionary *WLReplyDict = [[NSMutableDictionary alloc] init];
                             if (stk_code != -1) {
                                 [WLReplyDict setObject:[tokens objectAtIndex:stk_code] forKey:@"1"];
                             }
                             if (return_val != -1) {
                                 [WLReplyDict setObject:[tokens objectAtIndex:return_val] forKey:@"7"];
                             }
                             if (return_msg != -1) {
                                 [WLReplyDict setObject:[tokens objectAtIndex:return_msg] forKey:@"8"];
                             }
                             [replyArr addObject:WLReplyDict];
                         }
                     }
                 }
             }
             
             NSDictionary *replyDict = [NSDictionary dictionaryWithObject:replyArr forKey:@"replyArr"];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
             
         }
         else if ([data length] == 0 && error == nil)
         {
             NSDictionary *replyDict = @{@"replyDict":@"NoData"};
             [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
         }
         else if (error != nil && error.code == NSURLErrorTimedOut) {
             NSDictionary *replyDict = @{@"replyDict":@"TimedOut"};
             [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
         } else if (error != nil) {
             NSDictionary *replyDict = @{@"replyDict":@"Error"};
             [[NSNotificationCenter defaultCenter] postNotificationName:@"addWatchListItemsReply" object:self userInfo:replyDict];
         
         }
     }];
}

- (void) deleteWatchlistItem:(int)favID withItem:(NSString *)stock_code
{
    
    NSArray *stockArr = [stock_code componentsSeparatedByString:@"."];
    NSString *symbol = [stockArr firstObject];
    NSString *exchg = [stockArr lastObject];
    
    NSString *postStr = [NSString stringWithFormat:@"*=DelFavListStk|+=%@|-=%d|1=%@|2=%@", [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"], favID, symbol, exchg];
    urlStr = [NSString stringWithFormat:@"http://%@/[%@]Favorate?%@", atpServer, [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"], postStr];
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             ////NSLog(@"deleteWatchlistItem response : %@",response);
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             
             content = [self atpDecryptionProcess:content];
             
             content = [NSString stringWithFormat:@"%@ (ID:%d)",content, favID];
             
             if ([content rangeOfString:@"Error"].location != NSNotFound) {
				NSDictionary *errorDict = [NSDictionary dictionaryWithObject:content forKey:@"message"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteWatchlistItemSuccessfully" object:self userInfo:errorDict];
             }
			 
			 dispatch_async(dispatch_get_main_queue(), ^{
				 if(self.removeStockFromWatchlistCompletion){
					 self.removeStockFromWatchlistCompletion(YES, nil, nil);
				 }
			 });
         }
         else if ([data length] == 0 && error == nil)
         {
             ////NSLog(@"deleteWatchlistItem Deleted Successfully");
             [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteWatchlistItemSuccessfully" object:self userInfo:nil];
			 dispatch_async(dispatch_get_main_queue(), ^{
				 if(self.removeStockFromWatchlistCompletion){
					 self.removeStockFromWatchlistCompletion(YES, nil, nil);
				 }
			 });
             
         }
		 else if (error != nil && error.code == NSURLErrorTimedOut){
			 NSLog(@"deleteWatchlist Error");
			 dispatch_async(dispatch_get_main_queue(), ^{
				 if(self.removeStockFromWatchlistCompletion){
					 self.removeStockFromWatchlistCompletion(NO, nil, error);
				 }
			 });
		 }
		 
		 else if (error != nil){
			 DLog(@"deleteWatchlist Download Error %@",error);
			 dispatch_async(dispatch_get_main_queue(), ^{
				 if(self.removeStockFromWatchlistCompletion){
					 self.removeStockFromWatchlistCompletion(NO, nil, error);
				 }
			 });
		 }
     }];
}

#pragma mark - Get Exchanges List for a Broker

- (void) getExchangelist
{
    userExchangelist=[NSMutableArray array];
    [UserPrefConstants singleton].userExchagelist = userExchangelist;
    //    if ([[UserPrefConstants singleton].brokerName  isEqual:@"CIMB SG"]) {
    //        urlStr= [NSString stringWithFormat:@"https://lms-cimbsguat.asiaebroker.com/lms/websub/authen?identifier=%@&sponsor=%@&prod=TCiPhone",[UserPrefConstants singleton].userName,[UserPrefConstants singleton].SponsorID];
    //    }
    //    else if ([[UserPrefConstants singleton].brokerName  isEqual:@"PSE"]) {
    //        urlStr= [NSString stringWithFormat:@"http://172.17.2.211/lms/websub/authen?identifier=%@&sponsor=%@&prod=TCiPhone",[UserPrefConstants singleton].userName,[UserPrefConstants singleton].SponsorID];
    //    }
    //    else if ([[UserPrefConstants singleton].brokerName  isEqual:@"CIMB MY"]) {
    //        urlStr= [NSString stringWithFormat:@"http://lms-cimbuat.asiaebroker.com/lmscimbtemp/websub/authen?identifier=%@&sponsor=%@&prod=TCiPhone",[UserPrefConstants singleton].userName,[UserPrefConstants singleton].SponsorID];
    //    }
    //    else
    //    urlStr= [NSString stringWithFormat:@"http://beacon-uat.getmsl.com/lmstemp/websub/authen?identifier=%@&sponsor=%@&prod=TCiPhone",[UserPrefConstants singleton].userName,[UserPrefConstants singleton].SponsorID];
    
    // OCT 2016 - All URL get from LMS plist
    
    urlStr= [NSString stringWithFormat:@"%@identifier=%@&sponsor=%@&prod=TCiPhone",
             [UserPrefConstants singleton].LMSServerAddress,
             [UserPrefConstants singleton].userName,
             [UserPrefConstants singleton].SponsorID];
    
    //NSLog(@"%@",urlStr);
	
	[CPService requestDataFromURL:urlStr completion:^(id result, NSError *error) {
		if (error) {
			DLog(@"+++ Can not get Exchange List: %@", error.localizedDescription);
			//[[NSNotificationCenter defaultCenter] postNotificationName:@"didErrorGetExchangeList" object:error];
			//+++alway reload if have internet connection
			[self performSelector:@selector(getExchangelist) withObject:nil afterDelay:1.0];
			return ;
		}
		if (error == nil){
			NSString *content = result;
			//NSLog(@"GetExchangelist Returned : %@",content);
			NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
			NSArray *dataTokens = [[NSArray alloc] init];
			NSArray *MDToken =[[NSArray alloc]init];
			int data = 0;
			
			NSLog(@"arr %@", arr);
			
			for (NSString *line in arr) {
				if (data != 1) {
					if ([line isEqualToString:@"--_BeginData_"]) {
						data = 1;
					}
				}
				else {
					if ([line isEqualToString:@"--_EndData_"]) {
						data = 2;
					}
					else {
						dataTokens = [line componentsSeparatedByString:@"|"];
					}
				}
				
				if (data == 2) {
					break;
				}
			}
			
			if ([dataTokens count] >= 2) {
				if([[dataTokens objectAtIndex:0] isEqualToString:@"0"]) { // SUCCESS?
					//Begin All Service
					int service = 0;
					for (NSString *line in arr) {
						
						if (service != 1) {
							if ([line isEqualToString:@"--_BeginAllService_"]) {
								service = 1;
							}
						}
						else {
							if ([line isEqualToString:@"--_EndAllService_"]) {
								service = 2;
							}
							else {
								NSArray *tokens = [line componentsSeparatedByString:@"|"];
								if ([tokens count] >= 18) {
									ExchangeData *ed = [[ExchangeData alloc] init];
									ed.equityOrDer = [[tokens objectAtIndex:2] intValue];
									ed.exchange_name = [tokens objectAtIndex:3];
									ed.exchange_description = [tokens objectAtIndex:4];
									ed.exchange_type = [tokens objectAtIndex:5];
									ed.feed_exchg_code = [tokens objectAtIndex:6];
									ed.currency = [tokens objectAtIndex:7];
									
									
									// if null, then set mktDepth = 1;
									if (([[tokens objectAtIndex:9] isEqual:[NSNull null]]) ||
										([[tokens objectAtIndex:9] isEqualToString:@"null"]))
										ed.mktDepthLevel = @"1";
									else
										ed.mktDepthLevel = [tokens objectAtIndex:9];
									
									NSLog(@"Resolved: %@",ed.mktDepthLevel);
									
									ed.MDLfeature = 0;
									NSString *feedString = [tokens objectAtIndex:16];
									if (feedString.length>0){
										
										ed.exchange_qc_server = [[[tokens objectAtIndex:16] componentsSeparatedByString:@":"] objectAtIndex:1];
										[UserPrefConstants singleton].qcServer = ed.exchange_qc_server;
										
									}
									
									NSLog(@" [UserPrefConstants singleton].qcServer %@", [UserPrefConstants singleton].qcServer);
									
									
									//[UserPrefConstants singleton].qcServer = ed.exchange_qc_server;
									
									ed.trade_exchange_code = [tokens objectAtIndex:17];
									ed.subsCPIQCompSynp = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:0];
									ed.subsCPIQCompInfo = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:1];
									ed.subsCPIQAnnounce = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:2];
									ed.subsCPIQCompKeyPer = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:3];
									ed.subsCPIQShrHoldSum = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:4];
									ed.subsCPIQFinancialRep = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:5];
									//DLog(@"%@ %d %d %d %d %d %d", ed.feed_exchg_code, ed.subsCPIQFinancialRep, ed.subsCPIQShrHoldSum, ed.subsCPIQCompKeyPer, ed.subsCPIQAnnounce, ed.subsCPIQCompInfo, ed.subsCPIQCompSynp);
									
									
									[self->userExchangelist addObject:ed];
									
								}
							}
						}
						
						if (service == 2) {
							break;
						}
					}
					
				}
				
			}
			data =0;
			for (NSString *line in arr) {
				if (data != 1) {
					if ([line isEqualToString:@"--_BeginFeatures_"]) {
						data = 1;
					}
				} else {
					if ([line isEqualToString:@"--_EndFeatures_"]) {
						data = 2;
					} else {
						dataTokens = [line componentsSeparatedByString:@"|"];
						MDToken = [[dataTokens objectAtIndex:0]componentsSeparatedByString:@"-"];
						
						if (([MDToken count] >= 2)&&([[MDToken objectAtIndex:0] isEqualToString:@"MD"])) {
							// update override MD level for listed exchange
							for (ExchangeData *ed in self->userExchangelist) {
								
								if ([ed.feed_exchg_code.uppercaseString isEqual:[[MDToken objectAtIndex:1] uppercaseString]]) {
									ed.mktDepthLevel =[NSString stringWithFormat:@"%d",[[MDToken objectAtIndex:2] intValue]];
									ed.MDLfeature = 1;
								}
							}
						}
					}
				}
				
				if (data == 2) { // END
					break;
				}
			}
			
			[UserPrefConstants singleton].userExchagelist = self->userExchangelist;
			//+++
			//+++keep select first for iphone: Maybe need change logic here
			if (IS_IPHONE) {
				if (self->userExchangelist.count > 0) {
					ExchangeData *defaultEx = [Utils getExchange:[SettingManager shareInstance].defaultExchangeCode];
					if (defaultEx) {
						[Utils selectedNewExchange:defaultEx];
					}else{
						ExchangeData *firstEx = self->userExchangelist.firstObject;
						[SettingManager shareInstance].defaultExchangeCode = firstEx.feed_exchg_code;
						[Utils selectedNewExchange:firstEx];
						[[SettingManager shareInstance] save];
					}
				}
			}
			
			// Override exchange names
			// [self overrideExchangeNames]; // 16 June 2017 info: LMS will update on their side
			//+++Should Get Exchange Info here
			[[VertxConnectionManager singleton] vertxGetExchangeInfo];
			[[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveExchangeList" object:nil]; // pnExchangeListDone
			[[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveExchangeListMain" object:nil]; // pnExchangeListDone
			[self serverLogIn];
		}
	}];
//    NSLog(@"\n\n---- GetExchangelist ----\n%@\n\n", urlStr);
//    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
//
//    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
//    urlRequest.timeoutInterval = kURLRequestTimeout;
//    NSOperationQueue *queue = [NSOperationQueue mainQueue];
//
//    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
//
//     {
//		 if (error) {
//			 DLog(@"+++ Can not get Exchange List: %@", error.localizedDescription);
//			 return ;
//		 }
//         if ([data length] > 0 && error == nil)
//         {
//             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//             //NSLog(@"GetExchangelist Returned : %@",content);
//             NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
//             NSArray *dataTokens = [[NSArray alloc] init];
//             NSArray *MDToken =[[NSArray alloc]init];
//             int data = 0;
//
//            NSLog(@"arr %@", arr);
//
//             for (NSString *line in arr) {
//                 if (data != 1) {
//                     if ([line isEqualToString:@"--_BeginData_"]) {
//                         data = 1;
//                     }
//                 }
//                 else {
//                     if ([line isEqualToString:@"--_EndData_"]) {
//                         data = 2;
//                     }
//                     else {
//                         dataTokens = [line componentsSeparatedByString:@"|"];
//                     }
//                 }
//
//                 if (data == 2) {
//                     break;
//                 }
//             }
//
//             if ([dataTokens count] >= 2) {
//                 if([[dataTokens objectAtIndex:0] isEqualToString:@"0"]) { // SUCCESS?
//                     //Begin All Service
//                     int service = 0;
//                     for (NSString *line in arr) {
//
//                         if (service != 1) {
//                             if ([line isEqualToString:@"--_BeginAllService_"]) {
//                                 service = 1;
//                             }
//                         }
//                         else {
//                             if ([line isEqualToString:@"--_EndAllService_"]) {
//                                 service = 2;
//                             }
//                             else {
//                                 NSArray *tokens = [line componentsSeparatedByString:@"|"];
//                                 if ([tokens count] >= 18) {
//                                     ExchangeData *ed = [[ExchangeData alloc] init];
//                                     ed.equityOrDer = [[tokens objectAtIndex:2] intValue];
//                                     ed.exchange_name = [tokens objectAtIndex:3];
//                                     ed.exchange_description = [tokens objectAtIndex:4];
//                                     ed.exchange_type = [tokens objectAtIndex:5];
//                                     ed.feed_exchg_code = [tokens objectAtIndex:6];
//                                     ed.currency = [tokens objectAtIndex:7];
//
//
//                                     // if null, then set mktDepth = 1;
//                                     if (([[tokens objectAtIndex:9] isEqual:[NSNull null]]) ||
//                                         ([[tokens objectAtIndex:9] isEqualToString:@"null"]))
//                                         ed.mktDepthLevel = @"1";
//                                     else
//                                         ed.mktDepthLevel = [tokens objectAtIndex:9];
//
//                                    NSLog(@"Resolved: %@",ed.mktDepthLevel);
//
//                                     ed.MDLfeature = 0;
//                                     NSString *feedString = [tokens objectAtIndex:16];
//                                   if (feedString.length>0){
//
//                                        ed.exchange_qc_server = [[[tokens objectAtIndex:16] componentsSeparatedByString:@":"] objectAtIndex:1];
//                                        [UserPrefConstants singleton].qcServer = ed.exchange_qc_server;
//
//                                   }
//
//                                     NSLog(@" [UserPrefConstants singleton].qcServer %@", [UserPrefConstants singleton].qcServer);
//
//
//                                    //[UserPrefConstants singleton].qcServer = ed.exchange_qc_server;
//
//                                     ed.trade_exchange_code = [tokens objectAtIndex:17];
//                                     ed.subsCPIQCompSynp = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:0];
//                                     ed.subsCPIQCompInfo = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:1];
//                                     ed.subsCPIQAnnounce = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:2];
//                                     ed.subsCPIQCompKeyPer = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:3];
//                                     ed.subsCPIQShrHoldSum = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:4];
//                                     ed.subsCPIQFinancialRep = (BOOL)[self isBitOnBase64:[tokens objectAtIndex:18] forBitIndex:5];
//                                     //DLog(@"%@ %d %d %d %d %d %d", ed.feed_exchg_code, ed.subsCPIQFinancialRep, ed.subsCPIQShrHoldSum, ed.subsCPIQCompKeyPer, ed.subsCPIQAnnounce, ed.subsCPIQCompInfo, ed.subsCPIQCompSynp);
//
//
//									 [self->userExchangelist addObject:ed];
//
//                                 }
//                             }
//                         }
//
//                         if (service == 2) {
//                             break;
//                         }
//                     }
//
//                 }
//
//             }
//             data =0;
//             for (NSString *line in arr) {
//                 if (data != 1) {
//                     if ([line isEqualToString:@"--_BeginFeatures_"]) {
//                         data = 1;
//                     }
//                 } else {
//                     if ([line isEqualToString:@"--_EndFeatures_"]) {
//                         data = 2;
//                     } else {
//                         dataTokens = [line componentsSeparatedByString:@"|"];
//                         MDToken = [[dataTokens objectAtIndex:0]componentsSeparatedByString:@"-"];
//
//                         if (([MDToken count] >= 2)&&([[MDToken objectAtIndex:0] isEqualToString:@"MD"])) {
//                             // update override MD level for listed exchange
//                             for (ExchangeData *ed in userExchangelist) {
//
//                                 if ([ed.feed_exchg_code isEqual:[MDToken objectAtIndex:1]]) {
//                                     ed.mktDepthLevel =[NSString stringWithFormat:@"%d",[[MDToken objectAtIndex:2] intValue]];
//                                     ed.MDLfeature = 1;
//                                 }
//                             }
//                         }
//
//                     }
//                 }
//
//                 if (data == 2) { // END
//                     break;
//                 }
//             }
//
//			 [UserPrefConstants singleton].userExchagelist = self->userExchangelist;
//
//             // Override exchange names
//             // [self overrideExchangeNames]; // 16 June 2017 info: LMS will update on their side
//
//             [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveExchangeList" object:nil]; // pnExchangeListDone
//             [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveExchangeListMain" object:nil]; // pnExchangeListDone
//             [self serverLogIn];
//         }}];
	//+++ update this
     //[UserPrefConstants singleton].userExchagelist = [userExchangelist mutableCopy];
    // Above line might be problematic. You have to set the userExchlist first before calling PN.
    ////NSLog(@"%@",userExchangelist);
}

- (void) serverLogIn{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddhhmmss.S"];
    NSDate *now = [[NSDate alloc] init];
    NSString *timeStamp = [dateFormat stringFromDate:now];
    // request key string
    
    if ([UserPrefConstants singleton].qcServer==NULL) {
        [UserPrefConstants singleton].qcServer = [UserPrefConstants singleton].tempQcServer;
    }
    
    NSString *reqURLstr = [NSString stringWithFormat:@"http://%@/GetNewKey|||%@", [UserPrefConstants singleton].qcServer, timeStamp];
    
    NSLog(@"reqURLstr %@",reqURLstr);
    
    NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy;
    [chars removeCharactersInRange:NSMakeRange('&', 1)]; // %26 exception for & character
    NSURL *url = [NSURL URLWithString:[reqURLstr stringByAddingPercentEncodingWithAllowedCharacters:chars]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithURL:url
      
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                // handle response
                if (error==NULL) {
                    NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                    
                    NSArray *lines = [dataStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                    __block NSString *getNumID = @"";
                    
                    [lines enumerateObjectsUsingBlock:^(id x, NSUInteger index, BOOL *stop) {
                        if ([[lines objectAtIndex:index] length]>5) {
                            getNumID = [lines objectAtIndex:index];
                        }
                    }];
                    
                    if ([getNumID length]>0) {
                        [UserPrefConstants singleton].accountID = getNumID;
                        
                        isTimeOut = NO;
                        
                        NSLog(@"_accID %@", [UserPrefConstants singleton].accountID);
                        
                        [self getUserKey];
                    }
                }
            }
      ] resume];
}

- (void) getUserKey{
    NSString *reqURLstr = [NSString stringWithFormat:@"http://%@/[%@]Login2?ClientApp=mobile&PullMode=1|||",[UserPrefConstants singleton].qcServer, [UserPrefConstants singleton].accountID];
    NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy;
    [chars removeCharactersInRange:NSMakeRange('&', 1)]; // %26 exception for & character
    NSURL *url = [NSURL URLWithString:[reqURLstr stringByAddingPercentEncodingWithAllowedCharacters:chars]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [
     [session dataTaskWithURL:url
      
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                
                
                NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                
                NSArray *lines = [dataStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                NSLog(@"dataStr %@",dataStr);
                [lines enumerateObjectsUsingBlock:^(id x, NSUInteger index, BOOL *stop) {
                    NSString *eachLine = [lines objectAtIndex:index];
                    NSArray *KeyAndValPair = [eachLine componentsSeparatedByString:@"="];
                    
                    if ([[KeyAndValPair objectAtIndex:0] isEqualToString:@"[UserParams]"]) {
                        [UserPrefConstants singleton].userParamQC = [KeyAndValPair objectAtIndex:1];
                        [self saveUserKey];
                        [self getSourceNews:dataStr];
                    }
                }];
                
                if ([[UserPrefConstants singleton].userParamQC length]>0) {
                  //  NSLog(@"userParamQC %@",[UserPrefConstants singleton].userParamQC);
                }
            }
      ] resume
     ];
}

- (void) getSourceNews:(NSString *)dataStr{

    NSArray *lineArr = [dataStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    int header = -1;
    int footer = -1;
    for (int j = 0; j < [lineArr count]; j++) {
    NSString *line = [lineArr objectAtIndex:j];
        if ([line isEqualToString:@"--_BeginNewsCat_"]) {
            header = j;
        }
        if ([line isEqualToString:@"--_EndNewsCat_"]) {
            footer = j;
        }
    }
                                      
     if (header != -1 && footer != -1) {
         for (int i = header + 1; i < footer; i++) {
             NSString *dataRow = [lineArr objectAtIndex:i];
			 //+++ fix: No use when the tokens is empty
			 if ([dataRow isEqualToString:@","] || dataRow.length == 0) {
				 continue;
			 }
             DLog(@"data Row %@",dataRow);
             NSArray *tokens = [dataRow componentsSeparatedByString:@"|"];
             ElasticNewsData *elasticNewsData = [[ElasticNewsData alloc] init];
             int i = 0;
             for (NSString *token in tokens) {
                 switch (i) {
                     case 0:
                         elasticNewsData.sourceString = token;
                         DLog(@"Tokens String %@",token);
                         break;
                     case 1:
                         elasticNewsData.categoryString = token;
                         break;
                     case 2:
                         elasticNewsData.categoryLevel = token;
                     case 3:
                         elasticNewsData.newsID = token;
                         break;
                     default:
                         break;
                 }
                 i++;
             }
             [[UserPrefConstants singleton].elasticCat addObject:elasticNewsData];
         }
     }
}

- (BOOL)saveUserKey {
    BOOL success = NO;
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:[UserPrefConstants singleton].userParamQC forKey:@"UserParamKey"];
    NSDate *now = [NSDate date];
    [def setObject:now forKey:@"UserParamKeyTime"];
    if ([def synchronize]) {
        success = YES;
    }
    return success;
}

- (void)getUserDefaultData {
	
}

/*
 //Type 1 - Company Info
 //Type 2 - Financial Info
 - (NSString *) getFundamentalDataURL:(int)type andCategory:(NSString *)category {
 
 if (type == 1) {
 
 if ([AppConfigManager getFundamentalNewsLabel]!=nil && ![[AppConfigManager getFundamentalNewsLabel] isEqual:@""]) {
 
 return [NSString stringWithFormat:@"%@stkCompInfom.jsp?SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&BHCode=%@",
 [AppConfigManager getFundamentalNewsServerInfoURL], [AppConfigManager getFundamentalSourceCode],
 trade_exchg_code, strip_stock_code, category,[AppConfigManager getLMSSponsor],appData.userAccountInfo.brokerCode];
 }
 else
 return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&BHCode=%@", [AppConfigManager getFundamentalDataCompanyInfoURL], @"CPIQ", trade_exchg_code, strip_stock_code, category,[AppConfigManager getLMSSponsor],appData.userAccountInfo.brokerCode];
 }
 else if (type == 2) {
 if ([AppConfigManager getFundamentalNewsLabel]!=nil && ![[AppConfigManager getFundamentalNewsLabel] isEqual:@""]) {
 
 return [NSString stringWithFormat:@"%@stkFincInfom.jsp?SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=IS&Sponsor=%@&BHCode=%@", [AppConfigManager getFundamentalNewsServerInfoURL], [AppConfigManager getFundamentalSourceCode], trade_exchg_code, strip_stock_code,[AppConfigManager getLMSSponsor],appData.userAccountInfo.brokerCode];
 }
 else
 return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=INC&Sponsor=%@&BHCode=%@", [AppConfigManager getFundamentalDataFinancialInfoURL], @"CPIQ", trade_exchg_code, strip_stock_code,[AppConfigManager getLMSSponsor],appData.userAccountInfo.brokerCode];
 }
 
 return @"";
 }
 
 */


/* TCMobile news feed
 
 NSArray *exchg = [stkCode componentsSeparatedByString:@"."];
 urlStr = [NSString stringWithFormat:@"http://sgnewsstg.itradecimb.com/ebcNews/web/ext/getReutersNews.jsp?t=%@&df=%@&dt=%@&s=%@&k=&ns=REUTERS&r=%@&tr=20&sz=20",[self ArchiveNewsEncryption],[self formatDate:[[NSDate date] dateByAddingTimeInterval:-30*24*60*60]],[self formatDate:[NSDate date] ],[exchg objectAtIndex:0], [appData.userAccountInfo.exchangeList objectAtIndex:appData.userAccountInfo.selected_exchange] ];
 
 */




#pragma mark - NEWS

- (void)getJarNews
{
    NSString *exchange = ([UserPrefConstants singleton].userCurrentExchange.length>2)?([[UserPrefConstants singleton].userCurrentExchange substringToIndex:2]):([UserPrefConstants singleton].userCurrentExchange);
    
    if ([exchange isEqualToString:@"SG"]) {
        exchange = @"SI";
    }else if([exchange isEqualToString:@"MY"]){
        exchange = @"KL";
    }
    
    NSMutableArray *newsDataSource = [[NSMutableArray alloc] init];
    
    NSString *urlString =[NSString stringWithFormat:@"http://jar.asiaebroker.com/ref/generalnews_%@.txt",exchange];
     NSLog(@"Jar News url %@",urlString);
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
           NSLog(@"Data news: %@",data);
         if ([data length]==0) {
             NSDictionary *dict = @{@"data":newsDataSource};
             [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveJarNews" object:nil userInfo:dict];
             return;
        
         } else if ([data length] > 0 && error == nil)
         {
             
             
             NSString *responseData = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             /// NSLog(@"NEWS responseData : %@",responseData);
             //if (([responseData rangeOfString:@"The page cannot be found"].location==NSNotFound))
             if (([responseData rangeOfString:@"--_BeginData_"].location!=NSNotFound))
             {
                 NSRange r1 = [responseData rangeOfString:@"--_BeginData_"];
                 NSRange r2 = [responseData rangeOfString:@"--_EndData_"];
                 NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
                 NSString *sub = [responseData substringWithRange:rSub];
                 
                 NSCharacterSet *LFCR = [NSCharacterSet newlineCharacterSet];
                 NSMutableArray *tmpArray = [[NSMutableArray alloc]initWithArray:[sub componentsSeparatedByCharactersInSet:LFCR]];
                 
                 
                 
                 [tmpArray enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
                     
                     NSString *str = (NSString*)obj;
                     if ([str length]>0) {
                         [newsDataSource addObject:obj];
                     }
                     
                 }];
                 
             }

         }
         
         NSDictionary *dict = @{@"data":newsDataSource};
         [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveJarNews" object:nil userInfo:dict];
         
         
     }];
}

-(void)getArchiveNewsForStock:(NSString*)stock_code forDays:(long)days {
    
    if ([stock_code isEqualToString:@"Home"]) { // archive news for Home page
        
        urlStr = [NSString stringWithFormat:@"%@/ebcNews/web/ext/getReutersNews.jsp?t=%@&df=%@&dt=%@&s=&k=&ns=REUTERS&r=%@&tr=20&sz=20",
                  _userPref.NewsServerAddress,
                  [self ArchiveNewsEncryption],
                  [self formatDate:[[NSDate date] dateByAddingTimeInterval:-days*24*60*60]],
                  [self formatDate:[NSDate date]],//[exchg objectAtIndex:0],
                  [_userPref stripDelayedSymbol:_userPref.userCurrentExchange]];
        
    } else { // archive news for single stock
        
        NSArray *stk = [stock_code componentsSeparatedByString:@"."];
        
        urlStr = [NSString stringWithFormat:@"%@/ebcNews/web/ext/getReutersNews.jsp?t=%@&df=%@&dt=%@&s=%@&k=&ns=REUTERS&r=%@&tr=20&sz=20",
                  _userPref.NewsServerAddress,
                  [self ArchiveNewsEncryption],
                  [self formatDate:[[NSDate date] dateByAddingTimeInterval:-days*24*60*60]],
                  [self formatDate:[NSDate date] ],[stk objectAtIndex:0],
                  [_userPref stripDelayedSymbol:_userPref.userCurrentExchange]];
        
    }
    
    //NSLog(@"archiveNews URL: %@", urlStr);
    
    NSURL *URL = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.timeoutInterval = kURLRequestTimeout;
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *downloadTask =
    
    [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
           // NSLog(@"error getting archive news%@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishGetArchiveNews" object:self userInfo:nil];
            return;
        }
        
        NSError *jsonError;
        
        NSString *isoString = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
        NSData *dataUTF8 = [isoString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataUTF8
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        resultList = [[NSMutableArray alloc] init];
        resultList = [[json objectForKey:@"val"] mutableCopy];
        
        
        NSDictionary *newsDict;
        if ([resultList count]>0) {
            newsDict = [NSDictionary dictionaryWithObject:resultList forKey:@"News"];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishGetArchiveNews" object:self userInfo:newsDict];
        
    }];
    
    [downloadTask resume];
}


- (NSString *)formatDateWithTimeZone:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

- (NSString *)qcDecompressionProcess:(NSString *)qcResponseStr {
    
    if ([UserPrefConstants singleton].isQCCompression) {
        NSRange range1 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)13]];
        NSRange range2 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%C", (unichar)129]];
        NSRange range3 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)30]];
        NSRange range4 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)10]];
        
        if (range1.location != NSNotFound && range2.location != NSNotFound && range3.location != NSNotFound && range4.location != NSNotFound) {
            
            qcResponseStr = [qcResponseStr substringWithRange:NSMakeRange(range3.location+1, [qcResponseStr length]-range3.location-2)];
            NSString *resultStr = [[NSString alloc] initWithData:[[qcResponseStr dataUsingEncoding:NSISOLatin1StringEncoding] zlibInflate] encoding:NSISOLatin1StringEncoding];
            return resultStr;
        }
        else {
            return qcResponseStr;
        }
    }
    else {
        return qcResponseStr;
    }
}


-(void)getElasticNewsForStocks:(NSString*)stock_code forDays:(long)days withKeyword:(NSString *)keyword andTargetID:(NSString *)targetID andSource:(NSString *)source andCategory:(NSString *)category {
    
    if ([stock_code isEqualToString:@"Home"]) { // archive news for Home page
        
         urlStr = [NSString stringWithFormat:@"http://%@/%@V2NEWS?[KWORD]=%@&[TARGET]=%@&[GUID]=&[CAT]=%@&[SOURCE]=%@&[ACTIVE]=&[FRDT]=%@&[TODT]=%@&[SIZE]=1000&[FROM]=0|||", [UserPrefConstants singleton].qcServer, [UserPrefConstants singleton].tempUserParamQC,keyword,targetID,category,source,[self formatDateWithTimeZone:[[NSDate date] dateByAddingTimeInterval:-days*24*60*60]],[self formatDateWithTimeZone:[NSDate date]]];
        
    } else { // archive news for single stock
        
        NSArray *stk = [stock_code componentsSeparatedByString:@"."];
        
     urlStr = [NSString stringWithFormat:@"http://%@/%@V2NEWS?[KWORD]=%@&[TARGET]=%@&[GUID]=&[CAT]=%@&[SOURCE]=%@&[ACTIVE]=&[FRDT]=%@&[TODT]=%@&[SIZE]=1000&[FROM]=0|||", [UserPrefConstants singleton].qcServer, [UserPrefConstants singleton].tempUserParamQC,[stk objectAtIndex:0],targetID,category,source,[self formatDateWithTimeZone:[[NSDate date] dateByAddingTimeInterval:-days*24*60*60]],[self formatDateWithTimeZone:[NSDate date]]];
        
    }
    
    NSLog(@"archiveNews URL: %@", urlStr);
    
    NSURL *URL = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.timeoutInterval = kURLRequestTimeout;
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *downloadTask =
    
    [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            // NSLog(@"error getting archive news%@", error);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishGetElasticNews" object:self userInfo:nil];
            return;
        }
        
        NSError *jsonError;
        
        NSString *isoString = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
        NSData *dataUTF8 = [isoString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataUTF8
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        resultList = [[NSMutableArray alloc] init];
        resultList = [[json objectForKey:@"val"] mutableCopy];
        
        
        NSDictionary *newsDict;
        if ([resultList count]>0) {
            newsDict = [NSDictionary dictionaryWithObject:resultList forKey:@"News"];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishGetElasticNews" object:self userInfo:newsDict];
        
    }];
    
    [downloadTask resume];
}


#pragma mark - Alert Delegate
- (void)actionShortSell:(BOOL)isBuyOrSell{
    if(isBuyOrSell){
        [self prepareTradeSubmit:YES forAccount:self.confirmUACD forOrder:self.confirmOD ];
    }else{
        [self prepareTradeRevise:YES forAccount:self.confirmUACD forOrder:self.confirmOD forExistingOrder:self.confirmTS];
    }
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //Alert from buy/sell order submission
    if (alertView.tag == 1) {
        if (buttonIndex == 0) {
            //DLog(@"Clicked button OK tag 1");
            [self prepareTradeSubmit:YES forAccount:self.confirmUACD forOrder:self.confirmOD ];
        }
        else if (buttonIndex == 1) {
            //DLog(@"Clicked button cancel tag 1");
            //[self setCancelConfirmation:YES];
            // [delegate ASIHttpTradeSubmitCallback:self];
        }
    }
    //Alert from revise order submission
    else if (alertView.tag == 2) {
        if (buttonIndex == 0) {
            //DLog(@"Clicked button OK tag 2");
            [self prepareTradeRevise:YES forAccount:self.confirmUACD forOrder:self.confirmOD forExistingOrder:self.confirmTS];
        }
        else if (buttonIndex == 1) {
            //DLog(@"Clicked button cancel tag 2");
        }
    }
    
    if ([alertView tag] == 3) {
        
        alertLogoutShown = NO;
        
        if (buttonIndex == 0) {
            [self logout];
        }
        else if (buttonIndex == 1) {
            [self reValidateIdleCheckTimer];
        }
        [self invalidateAutoLogoutTimer];
        
    }
}


#pragma mark - TRADING

- (void) atpGetClientList {
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@", [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"]];
    
    //atpServer = [UserPrefConstants singleton].userATPServerIP;
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]TradeClient?10|%c%@|%@%cE",
                  atpServer, START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[0]TradeClient?10|%c%@|%@%cE",
                  atpServer, START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
        
    }
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    if ([UserPrefConstants singleton].userAccountlist.count>0) {
        [UserPrefConstants singleton].userAccountlist =[NSMutableArray array];
    }
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecompressionProcess:[self atpDecryptionProcess:content]];
         NSArray *contentCheckingArr = [content componentsSeparatedByString:@","];
		 
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         
         if([contentCheckingArr[0] isEqualToString:@"ERROR"])
         {
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:contentCheckingArr[1] forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             return ;
         }
         
         
         
         else if ([data length] > 0 && error == nil)
         {
             int client_account_number = -1;
             int client_code = -1;
             int client_name = -1;
             int broker_code = -1;
             int broker_branch_code = -1;
             int credit_limit = -1;
			 int net_cash_limit = -1; //Cash
             int buy_limit = -1;
             int sell_limit = -1;
             int account_status = -1;
             int supported_exchanges = -1;
             int client_acc_exchange = -1;
             int isMarginAcc = -1;
             int account_type = -1;
             
             NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
             NSString *s;
             for (s in arr) {
                 if ([metaregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@","]) {
                             client_account_number = pos;
                         }
                         else if ([token isEqualToString:@"+"]) {
                             client_code = pos;
                         }
                         else if ([token isEqualToString:@"-"]) {
                             client_name = pos;
                         }
                         else if ([token isEqualToString:@"0"]) {
                             broker_code = pos;
                         }
                         else if ([token isEqualToString:@"1"]) {
                             broker_branch_code = pos;
                         }
                         else if ([token isEqualToString:@"2"]) {
                             credit_limit = pos;
                         }
						 else if ([token isEqualToString:@"K"]) {
							 net_cash_limit = pos;
						 }
                         else if ([token isEqualToString:@"3"]) {
                             buy_limit = pos;
                         }
                         else if ([token isEqualToString:@"4"]) {
                             sell_limit = pos;
                         }
                         else if ([token isEqualToString:@"6"]) {
                             account_status = pos;
                         }
                         else if ([token isEqualToString:@"J"]) {
                             supported_exchanges = pos;
                         }
                         else if ([token isEqualToString:@"?"]) {
                             client_acc_exchange = pos;
                         }
                         else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)136]]) {
                             isMarginAcc = pos;
                         }
                         else if ([token isEqualToString:@"P"]) {
                             account_type = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     UserAccountClientData *uacd = [[UserAccountClientData alloc] init];
                     
                     if (client_account_number != -1) {
                         uacd.client_account_number = [tokens objectAtIndex:client_account_number];
                     }
                     if (client_code != -1) {
                         uacd.client_code = [tokens objectAtIndex:client_code];
                     }
                     if (client_name != -1) {
                         uacd.client_name = [tokens objectAtIndex:client_name];
                         DLog(@"Client Name %@",[tokens objectAtIndex:client_name]);
                     }
                     if (broker_code != -1) {
                         uacd.broker_code = [tokens objectAtIndex:broker_code];
                     }
                     if (broker_branch_code != -1) {
                         uacd.broker_branch_code = [tokens objectAtIndex:broker_branch_code];
                     }
                     if (credit_limit != -1) {
                         uacd.credit_limit = [[tokens objectAtIndex:credit_limit] doubleValue];
                     }
					 if (net_cash_limit != -1) {
						 uacd.net_cash_limit = [[tokens objectAtIndex:net_cash_limit] doubleValue];
					 }
                     if (buy_limit != -1) {
                         uacd.buy_limit = [[tokens objectAtIndex:buy_limit] doubleValue];
                     }
                     if (sell_limit != -1) {
                         uacd.sell_limit = [[tokens objectAtIndex:sell_limit] doubleValue];
                     }
                     if (account_status != -1) {
                         uacd.account_status = [[tokens objectAtIndex:account_status] intValue];
                     }
                     if (supported_exchanges != -1) {
                         NSString *se = [tokens objectAtIndex:supported_exchanges];
                         if (se != nil && [se length] > 0) {
                             [uacd.supported_exchanges removeAllObjects];
                             [uacd.supported_exchanges addObjectsFromArray:[se componentsSeparatedByString:@","]];
                         }
                     }
                     if (client_acc_exchange != -1) {
                         uacd.client_acc_exchange = [tokens objectAtIndex:client_acc_exchange];
                     }
                     if (isMarginAcc != -1) {
                         uacd.isMarginAccount = [[tokens objectAtIndex:isMarginAcc] intValue];
                     }
                     if (account_type != -1) {
                         uacd.account_type = [tokens objectAtIndex:account_type];
                     }
                     //DLog(@"ACCOUNT-Authen: %@ %f %f %f %d %@ %@", uacd.client_account_number, uacd.credit_limit, uacd.buy_limit, uacd.sell_limit, uacd.account_status, uacd.supported_exchanges, uacd.isMarginAccount == 0 ? @"Non-Margin" : @"Margin");
                     [[UserPrefConstants singleton].userAccountlist addObject:uacd];
                 }
             }
             
             // select first account
             if ([[UserPrefConstants singleton].userAccountlist count]>0) {
                 [UserPrefConstants singleton].userSelectedAccount = [[UserPrefConstants singleton].userAccountlist objectAtIndex:0];
             }
             ////NSLog(@"%@", content);
             [[NSNotificationCenter defaultCenter] postNotificationName:@"didReceiveUserAccountsList" object:nil];
         }
         
         
         else if ([data length] == 0 && error == nil)
         {
             ////NSLog(@"Empty");
             //Send post notification
             /*NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
              [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
              [notificationData setObject:@"Empty" forKey:@"error"];
              [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];*/
         }
         else if (error != nil && error.code == NSURLErrorTimedOut)
         {
             ////NSLog(@"Error");
             //Send post notification
             /*NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
              [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
              [notificationData setObject:@"timedOut" forKey:@"error"];
              [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];*/
             //            [delegate timedOut];
         }
         else if (error != nil)
         {
             ////NSLog(@"Download Error");
             //Send post notification
             /* NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
              [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
              [notificationData setObject:@"Download Error" forKey:@"error"];
              [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];*/
             //            [delegate downloadError:error];
         }
         
         
     }];
    
    
    
}

- (void) getTradeLimitForAccount:(UserAccountClientData *) uacd  forPaymentCUT:(BOOL)paymentCUT andCurrency:(NSString *)settCurr
{
    userAccountClientData = uacd;
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|/=%@|3=%@|M=%@|P=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],	// '
                         uacd.client_account_number,			// .
                         uacd.client_code,						// /
                         uacd.broker_code,						// 3
                         uacd.broker_branch_code,				// M
                         uacd.client_acc_exchange               // P
                         ];
    if(([BrokerManager shareInstance].broker.tag == BrokerTag_SSI)){
        postStr = [NSString stringWithFormat:@"%@|þ=8401", postStr];
    }
    if (paymentCUT) {
        postStr = [NSString stringWithFormat:@"%@|%C=CUT|%C=%@", postStr, (unichar)137, (unichar)138, settCurr];
    }
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradeClientLimit?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradeClientLimit?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
   // NSLog(@"TradeClientLimit %@", postStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecryptionProcess:content];
         ////NSLog(@"%@", content);
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         NSString *s;
         int credit_limit = -1;      //2
         int buy_limit = -1;         //3
         int sell_limit = -1;
         NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
         
         // NSLog(@"TradeLimit %@", arr);
         
         for (s in arr) {
             if ([metaregex matchesString:s]) {
                 s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                 NSArray *tokens = [s componentsSeparatedByString:@"|"];
                 int pos = 0;
                 for (NSString *token in tokens) {
                     if ([token isEqualToString:@"2"]) {
                         credit_limit = pos;
                     }
                     else if ([token isEqualToString:@"3"]) {
                         buy_limit = pos;
                     }
                     else if ([token isEqualToString:@"4"]) {
                         sell_limit = pos;
                     }
                     pos++;
                 }
             }
             else if ([dataregex matchesString:s]) {
                 s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                 NSArray *tokens = [s componentsSeparatedByString:@"|"];
                 
                 if (credit_limit != -1) {
                     userAccountClientData.credit_limit = [[tokens objectAtIndex:credit_limit] doubleValue];
                 }
                 if (buy_limit != -1) {
                     userAccountClientData.buy_limit = [[tokens objectAtIndex:buy_limit] doubleValue];
                 }
                 if (sell_limit != -1) {
                     userAccountClientData.sell_limit = [[tokens objectAtIndex:sell_limit] doubleValue];
                 }
             }
         }
         
         for (UserAccountClientData *u in [UserPrefConstants singleton].userAccountlist)
         {
             if(u.client_account_number==userAccountClientData.client_account_number) {
                 u.credit_limit =userAccountClientData.credit_limit;
             }
         }
         
         [[NSNotificationCenter defaultCenter] postNotificationName:@"tradeLimitReceived" object:self userInfo:nil];
         
         
         
     }];
}

- (void)getTradeStatus:(int)account_selection completion:(void (^)(NSArray *orderBooks))completion {
    [[UserPrefConstants singleton].orderbooklist removeAllObjects];
    
    UserAccountClientData *uacd = nil;
    uacd = [UserPrefConstants singleton].userSelectedAccount;
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@",
                         [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"],        //' SenderCode
                         uacd.client_account_number,                //.    ClientAccountNumber
                         uacd.broker_code,                          //3 BrokerCode
                         uacd.broker_branch_code                    //M    BrokerBranchCode
                         ];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradeStatus?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradeStatus?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSMutableArray *result = [NSMutableArray new];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         
         content = [self atpDecompressionProcess:[self atpDecryptionProcess:content]];
         if ([content containsString:@"Error"]||[content containsString:@"ERROR"]) {
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"tradesubmitstatus"];
             [notificationData setObject:content forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatus" object:self userInfo:notificationData];
         }
         else if ([data length] > 0 && error == nil)
         {
             GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
             GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
             
             // NSLog(@"TradeStatus Reply --------- \n\n %@ \n\n", content);
             
             
             
             int account_number = -1;        //.
             int client_code = -1;            ///
             int sender_code = -1;            //2
             int broker_code = -1;            //3
             int exchange = -1;              //{
             
             int stock_name = -1;            //N
             int stock_code = -1;            //5
             
             int order_number = -1;            //+
             int order_time = -1;            //7
             int guid = -1;                    //*
             int fix_order_number = -1;        //,
             
             int action = -1;                //4
             int order_src = -1;                //8
             
             int order_type = -1;            //6
             int validity = -1;                //9
             int sett_currency = -1;         //B
             int payment_type = -1;          //Ascii138
             
             int order_quantity = -1;        //;
             int cancelled_quantity = -1;    //C
             int unmt_quantity = -1;            //D
             int mt_quantity = -1;            //E
             int min_quantity = -1;          //@
             int disclosed_quantity = -1;    //A
             int order_price = -1;            //<
             int trigger_price = -1;         //?
             int mt_price = -1;                //F
             int expiry = -1;                //:
             int trigger_type = -1;          //Ascii177
             int trigger_direction = -1;     //Ascii178
             
             int mt_value = -1;                //G
             
             int status = -1;                //I
             int status_text = -1;            //J
             int message = -1;                //M
             
             int lot_size = -1;              //Ascii158
             int last_update = -1;           //L
             
             NSArray *arr = [content componentsSeparatedByString:@"\r"];
             NSString *s;
             for (s in arr) {
                 s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                 
                 // NSLog(@"TradeStatus: %@",s);
                 
                 if ([metaregex matchesString:s]) {
                     //                DLog(@"META: %@", s);
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if ([tokens count] > 5) {
                         int pos = 0;
                         for (NSString *token in tokens) {
                             if ([token isEqualToString:@"."]) {
                                 account_number = pos;
                             }
                             else if ([token isEqualToString:@"/"]) {
                                 client_code = pos;
                             }
                             else if ([token isEqualToString:@"2"]) {
                                 sender_code = pos;
                             }
                             else if ([token isEqualToString:@"3"]) {
                                 broker_code = pos;
                             }
                             else if ([token isEqualToString:@"{"]) {
                                 exchange = pos;
                             }
                             else if ([token isEqualToString:@"N"]) {
                                 stock_name = pos;
                             }
                             else if ([token isEqualToString:@"5"]) {
                                 stock_code = pos;
                             }
                             else if ([token isEqualToString:@"+"]) {
                                 order_number = pos;
                             }
                             else if ([token isEqualToString:@"7"]) {
                                 order_time = pos;
                             }
                             else if ([token isEqualToString:@"*"]) {
                                 guid = pos;
                             }
                             else if ([token isEqualToString:@","]) {
                                 fix_order_number = pos;
                             }
                             else if ([token isEqualToString:@"4"]) {
                                 action = pos;
                             }
                             else if ([token isEqualToString:@"8"]) {
                                 order_src = pos;
                             }
                             else if ([token isEqualToString:@"6"]) {
                                 order_type = pos;
                             }
                             else if ([token isEqualToString:@"9"]) {
                                 validity = pos;
                             }
                             else if ([token isEqualToString:@"B"]) {
                                 sett_currency = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)138]]) {
                                 payment_type = pos;
                             }
                             else if ([token isEqualToString:@";"]) {
                                 order_quantity = pos;
                             }
                             else if ([token isEqualToString:@"C"]) {
                                 cancelled_quantity = pos;
                             }
                             else if ([token isEqualToString:@"D"]) {
                                 unmt_quantity = pos;
                             }
                             else if ([token isEqualToString:@"E"]) {
                                 mt_quantity = pos;
                             }
                             else if ([token isEqualToString:@"@"]) {
                                 min_quantity = pos;
                             }
                             else if ([token isEqualToString:@"A"]) {
                                 disclosed_quantity = pos;
                             }
                             else if ([token isEqualToString:@"<"]) {
                                 order_price = pos;
                             }
                             else if ([token isEqualToString:@"?"]) {
                                 trigger_price = pos;
                             }
                             else if ([token isEqualToString:@"F"]) {
                                 mt_price = pos;
                             }
                             else if ([token isEqualToString:@":"]) {
                                 expiry = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)177]]) {
                                 trigger_type = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)178]]) {
                                 trigger_direction = pos;
                             }
                             else if ([token isEqualToString:@"G"]) {
                                 mt_value = pos;
                             }
                             else if ([token isEqualToString:@"I"]) {
                                 status = pos;
                             }
                             else if ([token isEqualToString:@"J"]) {
                                 status_text = pos;
                             }
                             else if ([token isEqualToString:@"M"]) {
                                 message = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)158]]) {
                                 lot_size = pos;
                             }
                             else if ([token isEqualToString:@"L"]) {
                                 last_update = pos;
                             }
                             
                             pos++;
                         }
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if ([tokens count] > 5) {
                         TradeStatus *ts = [[TradeStatus alloc] init];
                         if (account_number != -1) {
                             ts.account_number = [tokens objectAtIndex:account_number];
                         }
                         if (client_code != -1) {
                             ts.client_code = [tokens objectAtIndex:client_code];
                         }
                         if (sender_code != -1) {
                             ts.sender_code = [tokens objectAtIndex:sender_code];
                         }
                         if (broker_code != -1) {
                             ts.broker_code = [tokens objectAtIndex:broker_code];
                         }
                         if (exchange != -1) {
                             ts.exchg_code = [tokens objectAtIndex:exchange];
                         }
                         if (stock_name != -1) {
                             ts.stock_name = [tokens objectAtIndex:stock_name];
                         }
                         if (stock_code != -1) {
                             ts.stock_code = [tokens objectAtIndex:stock_code];
                             [[UserPrefConstants singleton].orderbookliststkCodeArr addObject:ts.stock_code];
                             
                         }
                         if (order_number != -1) {
                             ts.order_number = [tokens objectAtIndex:order_number];
                         }
                         if (order_time != -1) {
                             ts.order_time = [tokens objectAtIndex:order_time];
                         }
                         if (guid != -1) {
                             ts.guid = [tokens objectAtIndex:guid];
                         }
                         if (fix_order_number != -1) {
                             ts.fix_order_number = [tokens objectAtIndex:fix_order_number];
                         }
                         if (action != -1) {
                             ts.action = [tokens objectAtIndex:action];
                         }
                         if (order_src != -1) {
                             ts.order_src = [tokens objectAtIndex:order_src];
                         }
                         if (order_type != -1) {
                             ts.order_type = [tokens objectAtIndex:order_type];
                         }
                         if (validity != -1) {
                             ts.validity = [tokens objectAtIndex:validity];
                         }
                         if (sett_currency != -1) {
                             ts.sett_currency = [tokens objectAtIndex:sett_currency];
                             /*if ([ts.sett_currency isEqualToString:@""]) {
                              ts.sett_currency = [AppConfigManager getDefaultCurrency];
                              }*/
                         }
                         if (payment_type != -1) {
                             ts.payment_type = [tokens objectAtIndex:payment_type];
                             /*if ([ts.payment_type isEqualToString:@""]) {
                              ts.payment_type = @"Cash";
                              }*/
                         }
                         if (order_quantity != -1) {
                             ts.order_quantity = [tokens objectAtIndex:order_quantity];
                         }
                         if (cancelled_quantity != -1) {
                             ts.cancelled_quantity = [tokens objectAtIndex:cancelled_quantity];
                         }
                         if (unmt_quantity != -1) {
                             ts.unmt_quantity = [tokens objectAtIndex:unmt_quantity];
                         }
                         if (mt_quantity != -1) {
                             ts.mt_quantity = [tokens objectAtIndex:mt_quantity];
                         }
                         if (min_quantity != -1) {
                             ts.min_quantity = [tokens objectAtIndex:min_quantity];
                         }
                         if (disclosed_quantity != -1) {
                             ts.disclosed_quantity = [tokens objectAtIndex:disclosed_quantity];
                         }
                         if (order_price != -1) {
                             ts.order_price = [tokens objectAtIndex:order_price];
                         }
                         if (trigger_price != -1) {
                             ts.trigger_price= [tokens objectAtIndex:trigger_price];
                         }
                         if (mt_price != -1) {
                             ts.mt_price = [tokens objectAtIndex:mt_price];
                         }
                         if (expiry != -1) {
                             ts.expiry = [tokens objectAtIndex:expiry];
                         }
                         if (trigger_type != -1) {
                             ts.triggerType = [tokens objectAtIndex:trigger_type];
                         }
                         if (trigger_direction != -1) {
                             ts.triggerDirection = [tokens objectAtIndex:trigger_direction];
                         }
                         if (mt_value != -1) {
                             ts.mt_value = [tokens objectAtIndex:mt_value];
                         }
                         if (status != -1) {
                             ts.status = [tokens objectAtIndex:status];
                         }
                         if (status_text != -1) {
                             ts.status_text = [tokens objectAtIndex:status_text];
                         }
                         if (message != -1) {
                             ts.message = [tokens objectAtIndex:message];
                         }
                         if (lot_size != -1) {
                             ts.lot_size = [tokens objectAtIndex:lot_size];
                         }
                         if (last_update != -1) {
                             ts.last_update = [tokens objectAtIndex:last_update];
                         }
                         
                         //unmatched quantity calculation
                         if ([ts.unmt_quantity isEqualToString:@""] || ts.unmt_quantity == nil) {
                             long long unmatched_qty = [ts.order_quantity longLongValue] - [ts.mt_quantity longLongValue] - [ts.cancelled_quantity longLongValue];
                             ts.unmt_quantity = [NSString stringWithFormat:@"%qi",unmatched_qty];
                         }
                         
                         [result addObject:ts];
                         
                     }
                 }
             } // for
             
         } // if datalength >0
         
         
         completion(result);
         
         
     }];
}
- (void)getTradeStatus:(int)account_selection {
    //refresh orderbooklist
    [self getTradeStatus:account_selection completion:^(NSArray *orderBooks) {
        [[UserPrefConstants singleton].orderbooklist addObjectsFromArray:orderBooks];
        [[VertxConnectionManager singleton] vertxGetStockDetailWithStockArr:[UserPrefConstants singleton].orderbookliststkCodeArr FIDArr:[[UserPrefConstants singleton]userQuoteScreenColumn] tag:1];
    }];
}

// added by emir March 2017
- (void) getTradeHistoryFromDate:(NSString*)dateFrom_yyyymmdd toDate:(NSString*)toDate_yyyymmdd  completion:(void (^ __nullable)(NSArray *orderBooks))completion{

    //refresh orderbooklist
    [[UserPrefConstants singleton].orderbooklist removeAllObjects];
    
    /*
     ‘ Ascii 39    Y    Sender Code
     . Ascii 46    Y    Client Account
     3 Ascii 51    Y    Broker Code
     M Ascii 77    Y    Branch Code
     P Ascii 80    Y    Exchange Code
     [ Ascii 91    Y    Status History either 1(True) or 0(False)
     ^ Ascii 94    Y    From Date – Formatted as yyyymmdd
     _ Ascii 95    Y    To Date – Formatted as yyyymmdd
     b Ascii 98    N    Filter Field ID
     c Ascii 99    N    Filter Field Value
     d Ascii 100    N    Size. Refer to number of record per page
     e Ascii 101    N    Page No
     { Ascii 123    N    Trade Cond
     */
    
    // http://192.168.0.44:20010/[<Session>]TradeHistory?'=A90008|.=test8|[=1|^=20100701|_=20100706|M=001|3=065|P=KL|{=1
    
    
    // NSLog(@"DATES: %@ - %@",dateFrom_yyyymmdd, toDate_yyyymmdd);
    
    UserAccountClientData *uacd = [UserPrefConstants singleton].userSelectedAccount;
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|^=%@|_=%@|[=1|P=%@",
                         [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"],        //' SenderCode
                         uacd.client_account_number,                  //.    ClientAccountNumber
                         uacd.broker_code,                          //3   BrokerCode
                         uacd.broker_branch_code,                      //M    BrokerBranchCode
                         dateFrom_yyyymmdd,
                         toDate_yyyymmdd,
                         uacd.client_acc_exchange
                         ];
    
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradeHistory?%@",
                  atpServer,
                  [[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"],
                  postStr
                  ];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradeHistory?%@",
                  atpServer,
                  [[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
    // NSLog(@" -------- GetTradeHistory submit -------- \n\n %@ \n\n ", urlStr);
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSMutableArray *orderBooks = [NSMutableArray new];
         if ([data length] > 0 && error == nil)
         {
             
             
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
             
             
             //NSLog(@" -------- GetTradeHistory Reply -------- \n\n %@ \n\n", content);
             
             GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
             GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
             
             int account_number = -1;        //.
             int client_code = -1;            ///
             int sender_code = -1;            //2
             int broker_code = -1;            //3
             int exchange = -1;             //{
             
             int stock_name = -1;            //N
             int stock_code = -1;            //5
             
             int order_number = -1;            //+
             int order_time = -1;            //7
             int guid = -1;                    //*
             int fix_order_number = -1;        //,
             
             int action = -1;                //4
             int order_src = -1;            //8
             
             int order_type = -1;            //6
             int validity = -1;                //9
             int sett_currency = -1;         //B
             int payment_type = -1;          //Ascii138
             
             int order_quantity = -1;        //;
             int cancelled_quantity = -1;    //C
             int unmt_quantity = -1;            //D
             int mt_quantity = -1;            //E
             int min_quantity = -1;          //@
             int disclosed_quantity = -1;    //A
             int order_price = -1;            //<
             int order_value = -1;            //>
             int trigger_price = -1;         //?
             int mt_price = -1;                //F
             int expiry = -1;                //:
             int trigger_type = -1;          //Ascii177
             int trigger_direction = -1;     //Ascii178
             
             int mt_value = -1;                //G
             
             int status = -1;                //I
             int status_text = -1;            //J
             int message = -1;                //M
             
             int lot_size = -1;              //Ascii158
             int last_update = -1;           //L
             
             NSArray *arr = [content componentsSeparatedByString:@"\r"];
             
             NSString *s;
             
             
             
             for ( s in arr) {
                 s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                 if ([metaregex matchesString:s]) {
                     //                DLog(@"META: %@", s);
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if ([tokens count] > 5) {
                         int pos = 0;
                         for (NSString *token in tokens) {
                             if ([token isEqualToString:@"."]) {
                                 account_number = pos;
                             }
                             else if ([token isEqualToString:@"/"]) {
                                 client_code = pos;
                             }
                             else if ([token isEqualToString:@"2"]) {
                                 sender_code = pos;
                             }
                             else if ([token isEqualToString:@"3"]) {
                                 broker_code = pos;
                             }
                             else if ([token isEqualToString:@"{"]) {
                                 exchange = pos;
                             }
                             else if ([token isEqualToString:@"N"]) {
                                 stock_name = pos;
                             }
                             else if ([token isEqualToString:@"5"]) {
                                 stock_code = pos;
                             }
                             else if ([token isEqualToString:@"+"]) {
                                 order_number = pos;
                             }
                             else if ([token isEqualToString:@"7"]) {
                                 order_time = pos;
                             }
                             else if ([token isEqualToString:@"*"]) {
                                 guid = pos;
                             }
                             else if ([token isEqualToString:@","]) {
                                 fix_order_number = pos;
                             }
                             else if ([token isEqualToString:@"4"]) {
                                 action = pos;
                             }
                             else if ([token isEqualToString:@"8"]) {
                                 order_src = pos;
                             }
                             else if ([token isEqualToString:@"6"]) {
                                 order_type = pos;
                             }
                             else if ([token isEqualToString:@"9"]) {
                                 validity = pos;
                             }
                             else if ([token isEqualToString:@"B"]) {
                                 sett_currency = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)138]]) {
                                 payment_type = pos;
                             }
                             else if ([token isEqualToString:@";"]) {
                                 order_quantity = pos;
                             }
                             else if ([token isEqualToString:@"C"]) {
                                 cancelled_quantity = pos;
                             }
                             else if ([token isEqualToString:@"D"]) {
                                 unmt_quantity = pos;
                             }
                             else if ([token isEqualToString:@"E"]) {
                                 mt_quantity = pos;
                             }
                             else if ([token isEqualToString:@"@"]) {
                                 min_quantity = pos;
                             }
                             else if ([token isEqualToString:@"A"]) {
                                 disclosed_quantity = pos;
                             }
                             else if ([token isEqualToString:@"<"]) {
                                 order_price = pos;
                             }
                             else if ([token isEqualToString:@">"]) {
                                 order_value = pos;
                             }
                             else if ([token isEqualToString:@"?"]) {
                                 trigger_price = pos;
                             }
                             else if ([token isEqualToString:@"F"]) {
                                 mt_price = pos;
                             }
                             else if ([token isEqualToString:@":"]) {
                                 expiry = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)177]]) {
                                 trigger_type = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)178]]) {
                                 trigger_direction = pos;
                             }
                             else if ([token isEqualToString:@"G"]) {
                                 mt_value = pos;
                             }
                             else if ([token isEqualToString:@"I"]) {
                                 status = pos;
                             }
                             else if ([token isEqualToString:@"J"]) {
                                 status_text = pos;
                             }
                             else if ([token isEqualToString:@"M"]) {
                                 message = pos;
                             }
                             else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)158]]) {
                                 lot_size = pos;
                             }
                             else if ([token isEqualToString:@"L"]) {
                                 last_update = pos;
                             }
                             
                             pos++;
                         }
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if ([tokens count] > 5) {
                         TradeStatus *ts = [[TradeStatus alloc] init];
                         if (account_number != -1) {
                             ts.account_number = [tokens objectAtIndex:account_number];
                         }
                         if (client_code != -1) {
                             ts.client_code = [tokens objectAtIndex:client_code];
                         }
                         if (sender_code != -1) {
                             ts.sender_code = [tokens objectAtIndex:sender_code];
                         }
                         if (broker_code != -1) {
                             ts.broker_code = [tokens objectAtIndex:broker_code];
                         }
                         if (exchange != -1) {
                             ts.exchg_code = [tokens objectAtIndex:exchange];
                         }
                         if (stock_name != -1) {
                             ts.stock_name = [tokens objectAtIndex:stock_name];
                         }
                         if (stock_code != -1) {
                             ts.stock_code = [tokens objectAtIndex:stock_code];
                             [[UserPrefConstants singleton].orderbookliststkCodeArr addObject:ts.stock_code];
                         }
                         if (order_number != -1) {
                             ts.order_number = [tokens objectAtIndex:order_number];
                         }
                         if (order_time != -1) {
                             ts.order_time = [tokens objectAtIndex:order_time];
                         }
                         if (guid != -1) {
                             ts.guid = [tokens objectAtIndex:guid];
                         }
                         if (fix_order_number != -1) {
                             ts.fix_order_number = [tokens objectAtIndex:fix_order_number];
                         }
                         if (action != -1) {
                             ts.action = [tokens objectAtIndex:action];
                         }
                         if (order_src != -1) {
                             ts.order_src = [tokens objectAtIndex:order_src];
                         }
                         if (order_type != -1) {
                             ts.order_type = [tokens objectAtIndex:order_type];
                         }
                         if (validity != -1) {
                             ts.validity = [tokens objectAtIndex:validity];
                         }
                         if (sett_currency != -1) {
                             ts.sett_currency = [tokens objectAtIndex:sett_currency];
                             /*if ([ts.sett_currency isEqualToString:@""]) {
                              ts.sett_currency = [AppConfigManager getDefaultCurrency];
                              }*/
                         }
                         if (payment_type != -1) {
                             ts.payment_type = [tokens objectAtIndex:payment_type];
                             /*if ([ts.payment_type isEqualToString:@""]) {
                              ts.payment_type = @"Cash";
                              }*/
                         }
                         if (order_quantity != -1) {
                             ts.order_quantity = [tokens objectAtIndex:order_quantity];
                         }
                         if (cancelled_quantity != -1) {
                             ts.cancelled_quantity = [tokens objectAtIndex:cancelled_quantity];
                         }
                         if (unmt_quantity != -1) {
                             ts.unmt_quantity = [tokens objectAtIndex:unmt_quantity];
                         }
                         if (mt_quantity != -1) {
                             ts.mt_quantity = [tokens objectAtIndex:mt_quantity];
                         }
                         if (min_quantity != -1) {
                             ts.min_quantity = [tokens objectAtIndex:min_quantity];
                         }
                         if (disclosed_quantity != -1) {
                             ts.disclosed_quantity = [tokens objectAtIndex:disclosed_quantity];
                         }
                         if (order_price != -1) {
                             ts.order_price = [tokens objectAtIndex:order_price];
                         }
                         if (order_value != -1) {
                             ts.value = [tokens objectAtIndex:order_value];
                         }
                         if (trigger_price != -1) {
                             ts.trigger_price= [tokens objectAtIndex:trigger_price];
                         }
                         if (mt_price != -1) {
                             ts.mt_price = [tokens objectAtIndex:mt_price];
                         }
                         if (expiry != -1) {
                             ts.expiry = [tokens objectAtIndex:expiry];
                         }
                         if (trigger_type != -1) {
                             ts.triggerType = [tokens objectAtIndex:trigger_type];
                         }
                         if (trigger_direction != -1) {
                             ts.triggerDirection = [tokens objectAtIndex:trigger_direction];
                         }
                         if (mt_value != -1) {
                             ts.mt_value = [tokens objectAtIndex:mt_value];
                         }
                         if (status != -1) {
                             ts.status = [tokens objectAtIndex:status];
                         }
                         if (status_text != -1) {
                             ts.status_text = [tokens objectAtIndex:status_text];
                         }
                         if (message != -1) {
                             ts.message = [tokens objectAtIndex:message];
                         }
                         if (lot_size != -1) {
                             ts.lot_size = [tokens objectAtIndex:lot_size];
                         }
                         if (last_update != -1) {
                             ts.last_update = [tokens objectAtIndex:last_update];
                         }
                         
                         //unmatched quantity calculation
                         if ([ts.unmt_quantity isEqualToString:@""] || ts.unmt_quantity == nil) {
                             long long unmatched_qty = [ts.order_quantity longLongValue] - [ts.mt_quantity longLongValue] - [ts.cancelled_quantity longLongValue];
                             ts.unmt_quantity = [NSString stringWithFormat:@"%qi",unmatched_qty];
                         }
                         
                         [orderBooks addObject:ts];
                         
                     }
                 }
             }
             
             
             //In case of the existence of orders with same ticket id, eliminate redundant orders by using dictionary to store the last update order only
             NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"last_update" ascending:YES];
             [orderBooks sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor1]];
             
             
             NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
             for (TradeStatus *ts in orderBooks) {
                 [tempDict setObject:ts forKey:ts.guid];
             }
             orderBooks = [NSMutableArray arrayWithArray:[tempDict allValues]];

         }
         completion(orderBooks);
         
         
     }];
    
}
-(void)getTradeHistoryFromDate:(NSString*)dateFrom_yyyymmdd toDate:(NSString*)toDate_yyyymmdd{
    [self getTradeHistoryFromDate:dateFrom_yyyymmdd toDate:toDate_yyyymmdd completion:^(NSArray *orderBooks) {
        [[UserPrefConstants singleton].orderbooklist addObjectsFromArray:orderBooks];
        [[VertxConnectionManager singleton] vertxGetStockDetailWithStockArr:[UserPrefConstants singleton].orderbookliststkCodeArr FIDArr:[[UserPrefConstants singleton]userQuoteScreenColumn] tag:1];
    }];
}





- (void) prepareTradeSubmit:(BOOL)orderConfirmation forAccount:(UserAccountClientData *) uacd forOrder:(OrderDetails *) od {
    
    if (!orderConfirmation) {
        od.ticket_no = [self generate_guid];
    }
    NSArray *stkCodeAndExchangeArr= [ od.stock_code componentsSeparatedByString:@"."];
    
    //    *={76c12e29-2bba-40a2-8baa-80c52caa6de1}|
    //    7=20161130105801.792|
    //    .=000133135|
    //    /=A00018|
    //    2=A00018|
    //    3=098|
    //    M=001|
    //    P=KL|
    //    8=W|
    //    4=Buy|
    //    5=0082.KL|
    //    6=Market|
    //    9=Day|
    //    B=MYR|
    //    F=|
    //    <=0.000|
    //    =0.000|
    //    ;=100|
    //    @=100|
    //    A=0|
    //    =|
    //    =|
    //    R=ÌÉÍÊÎË|
    //    =MI
    
    //Get trading exchange code
    NSString *trade_exchg_code = [[UserPrefConstants singleton] stripDelayedSymbol:[stkCodeAndExchangeArr objectAtIndex:1]];
    NSString *stock_code = [NSString stringWithFormat:@"%@.%@", [stkCodeAndExchangeArr objectAtIndex:0],trade_exchg_code];
    // od.currency = @"MYR";
    
    double price = [od.price doubleValue];
    double trigprice = [od.trigger_price doubleValue];
    
    if ([od.disclosedQuantity length]<=0) od.disclosedQuantity = @"0";
    
    NSString *postStr = [NSString stringWithFormat:@"*={%@}|7=%@|.=%@|/=%@|2=%@|3=%@|M=%@|P=%@|8=%@|4=%@|5=%@|6=%@|9=%@|B=%@|F=%@|<=%@|%C=%@|;=%@|@=%@|A=%@|%C=%@|%C=%@|R=%@|%C=MD",
                         [od.ticket_no lowercaseString],                                               //*
                         [self generateTimeStamp],                                                     //7
                         uacd.client_account_number,                                                   //.
                         uacd.client_code,                                                             ///
                         [[[UserPrefConstants singleton]userInfoDict] objectForKey:@"[SenderCode]"],   //2
                         uacd.broker_code,                                                             //3
                         uacd.broker_branch_code,                                                      //M
                         uacd.client_acc_exchange,                                                     //P
                         od.order_source,                                                              //8
                         od.order_action,                                                              //4
                         [stock_code stringByReplacingOccurrencesOfString:@"\n" withString:@""],       //5
                         od.order_type,                                                                //6
                         od.validity,                                                                  //9
                         od.currency,                                                                  //B
                         od.payment_type,                                                              //F
                         [self numberFormatStockPriceForOrderSubmission:price forExchange:trade_exchg_code], //<
                         (unichar)159,                                                                 //Ascii159
                         [self numberFormatStockPriceForOrderSubmission:trigprice forExchange:trade_exchg_code],
                         od.quantity,                             //;
                         od.minQuantity,                          //@
                         od.disclosedQuantity,                    //A
                         (unichar)154,                                                                 //Ascii154
                         od.triggerType,
                         (unichar)155,                                                                 //Ascii155
                         od.triggerDirection,
                         [self encryptString:(![UserPrefConstants singleton].skipPin ? od.pin : @"123456")],//R
                         (unichar)136                                                                  //ˆ
                         ];
    if ([od.validity isEqualToString:@"GTD"]) {
        postStr = [NSString stringWithFormat:@"%@|:=%@", postStr, od.expiry];
    }
    
    if ([UserPrefConstants singleton].verified2FA && ![od.otpPin isEqualToString:@""] && od.otpPin != nil) {
        postStr = [NSString stringWithFormat:@"%@|%C=%@", postStr, (unichar)140, od.otpPin];
    }
    if ([UserPrefConstants singleton].verified2FA && ![od.deviceID isEqualToString:@""] && od.deviceID != nil) {
        postStr = [NSString stringWithFormat:@"%@|%C=%@", postStr, (unichar)142, od.deviceID];
    }
    
    if ([od.order_action isEqualToString:@"Sell"]) {
        if (od.shortSellState) {
            postStr = [NSString stringWithFormat:@"%@|%C=262144", postStr, (unichar)129];
        }
    }
    if (orderConfirmation) {
        postStr = [NSString stringWithFormat:@"%@|L=1", postStr];
    }
    
     //NSLog(@"TradeSubmit PostStr: %@", postStr);
    
    /* urlStr = [NSString stringWithFormat:@"http://%@/[0]TradeTicket?10|%c%@|%@%cE",
     atpServer,
     START_OF_TEXT,
     [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]], [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
     END_OF_TEXT
     ];*/
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
    
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradeTicket?%@",atpServer,
                  [[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"],
                  postStr];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradeTicket?%@",atpServer,
                  [[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"],
                  postStr];
    }
    
    NSLog(@" -------- TradeTicket submit -------- \n\n %@ \n\n ", urlStr);
    
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             
             NSMutableDictionary *tempDict =[NSMutableDictionary dictionary];
             NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
             content = [self atpDecryptionProcess:content];
             
             NSLog(@" -------- TradeTicket Reply -------- \n\n %@ \n\n", content);
             
             if ([content rangeOfString:@"Error 502  50"].location != NSNotFound) {
                 
                 self.success = NO;
                 self.confirmUACD = uacd;
                 self.confirmOD = od;
                 NSArray *tokens = [content componentsSeparatedByString:@"||"];
                 NSString *msg = [tokens objectAtIndex:1];
                 if ([msg rangeOfString:@"82"].location != NSNotFound) {
                     msg = [msg stringByReplacingOccurrencesOfString:@"82" withString:@""];
                 }
                 
                 if([self.delegate respondsToSelector:@selector(showShortSellConfirm:msg:)]){
                     [self.delegate showShortSellConfirm:YES msg:msg];
                 }else{
                     UIAlertView *alertShortSell = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"Short Sell Confirmation"]
                                                                              message:msg delegate:self
                                                                    cancelButtonTitle:[LanguageManager stringForKey:@"Ok"]
                                                                    otherButtonTitles:[LanguageManager stringForKey:@"Cancel"], nil];
                     //Set tag=1 to indicate the alert is from buy/sell order submission
                     [alertShortSell setTag:1];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [alertShortSell show];
                     });
                 }
                 
             }  else if ([content rangeOfString:@"Error 509  50"].location != NSNotFound) {
                 
                 self.success = NO;
                 self.confirmUACD = uacd;
                 self.confirmOD = od;
                 
                 NSArray *tokens = [content componentsSeparatedByString:@"||"];
                 NSString *msg = [tokens objectAtIndex:1];
                 if ([msg rangeOfString:@"82"].location != NSNotFound) {
                     msg = [msg stringByReplacingOccurrencesOfString:@"82" withString:@""];
                 }
                 
                 UIAlertView *alertExceed5Bids = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
                 //Set tag=1 to indicate the alert is from buy/sell order submission
                 [alertExceed5Bids setTag:1];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [alertExceed5Bids show];
                 });
                 
             } else {
                 
                 GTMRegex *errorRegex1 = [GTMRegex regexWithPattern:@"ERROR.*"];
                 GTMRegex *errorRegex2 = [GTMRegex regexWithPattern:@"Error.*"];
                 
                 if ([content rangeOfString:@"Error 510  50"].location != NSNotFound) {
                     self.success = NO;
                     
                     NSArray *tokens = [content componentsSeparatedByString:@"||"];
                     NSString *msg = [tokens objectAtIndex:1];
                     if ([msg rangeOfString:@"82"].location != NSNotFound) {
                         msg = [msg stringByReplacingOccurrencesOfString:@"82" withString:@""];
                     }
                     NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                     [notificationData setObject:@"Failed" forKey:@"tradesubmitstatus"];
                     [notificationData setObject:msg forKey:@"error"];
                     //                     if ([delegate class] == [StockDetailViewController class]) {
                     //                         [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatusSD" object:self userInfo:notificationData];
                     //
                     //                     }
                     //                     else if([delegate class]==[OrderBookandPortfolioViewController class])
                     //                     [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatus" object:self userInfo:notificationData];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatus" object:self userInfo:notificationData];
                     
                 } else if ([errorRegex1 matchesString:content] || [errorRegex2 matchesString:content])  {
                     
                     NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                     [notificationData setObject:@"Failed" forKey:@"tradesubmitstatus"];
                     [notificationData setObject:content forKey:@"error"];
                     //                     if ([delegate class] == [StockDetailViewController class]) {
                     //                         [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatusSD" object:self userInfo:notificationData];
                     //
                     //                     }
                     //                     else if([delegate class]==[OrderBookandPortfolioViewController class])
                     //                     [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatus" object:self userInfo:notificationData];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatus" object:self userInfo:notificationData];
                     
                 }  else {
                     
                     NSArray *arr = [content componentsSeparatedByString:@"|"];
                     ////NSLog(@"arr : %@",arr);
                     if(arr)
                     {
                         for (int i=0; i<arr.count; i++)
                         {
                             NSArray *userInfoItem = [arr[i] componentsSeparatedByString:@"="];
                             if(userInfoItem)
                             {
                                 if(userInfoItem.count == 2){ // Item have both key and value. Eg:"[END]= " is invalid, "[UserParam]=abc" is valid
                                     [tempDict setObject:userInfoItem[1] forKey:userInfoItem[0]];
                                 }
                             }
                         }
                     }
                     
                     //NSLog(@"Trade Submit Returned: %@",content);
                     NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                     [notificationData setObject:@"Submitted" forKey:@"tradesubmitstatus"];
                     [notificationData setObject:tempDict forKey:@"data"];
                     
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatus" object:self userInfo:notificationData];
                     
                     //                     if ([delegate class] == [StockDetailViewController class]) {
                     //                         [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatusSD" object:self userInfo:notificationData];
                     //
                     //                     }
                     //                     else if([delegate class]==[OrderBookandPortfolioViewController class])
                     //                         [[NSNotificationCenter defaultCenter] postNotificationName:@"TradeSubmitStatus" object:self userInfo:notificationData];
                 }
             }
             
         } else if ([data length] == 0 && error == nil) {
             
             //Send post notification
             
         } else if (error != nil && error.code == NSURLErrorTimedOut) {
             
         } else if (error != nil) {
             //NSLog(@"%@",error.localizedDescription );
         }
         
     }];
}


- (void) prepareTradeRevise:(BOOL) orderConfirmation forAccount:(UserAccountClientData *) uacd forOrder:(OrderDetails *) od forExistingOrder:(TradeStatus *) ts {
    
    double price = [od.price doubleValue];
    double trigprice = [od.trigger_price doubleValue];
    
    NSString *postStr = [NSString stringWithFormat:@"*=%@|+=%@|,=%@|D=%@|E=%@|7=%@|.=%@|/=%@|2=%@|3=%@|M=%@|P=%@|8=%@|4=%@|5=%@|6=%@|9=%@|B=%@|F=%@|<=%@|%C=%@|;=%@|@=%@|A=%@|%C=%@|%C=%@|R=%@|%C=MD",
                         ts.guid,                                                                        //*
                         ts.order_number,                                                                //+
                         ts.fix_order_number,                                                            //,
                         ts.unmt_quantity,                                                               //D
                         ts.mt_quantity,                                                                 //E
                         [self generateTimeStamp],                                                       //7
                         uacd.client_account_number,                                                     //.
                         uacd.client_code,                                                               ///
                         [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"],                                             //2
                         uacd.broker_code,                                                               //3
                         uacd.broker_branch_code,                                                        //M
                         uacd.client_acc_exchange,                                                       //P
                         od.order_source,                                                                //8
                         od.order_action,                                                                //4
                         [ts.stock_code stringByReplacingOccurrencesOfString:@"\n" withString:@""],      //5
                         od.order_type,                                                                  //6
                         od.validity,                                                                    //9
                         od.currency,                                                                    //B
                         od.payment_type,                                                                //F
                         [self numberFormatStockPriceForOrderSubmission:price forExchange:ts.exchg_code], //<
                         (unichar)159,                                                                 //Ascii159
                         [self numberFormatStockPriceForOrderSubmission:trigprice forExchange:ts.exchg_code],
                         od.quantity,                               //;
                         od.minQuantity,                            //@
                         od.disclosedQuantity,                      //A
                         (unichar)154,                                                                   //Ascii154
                         od.triggerType,
                         (unichar)155,                                                                   //Ascii155
                         od.triggerDirection,
                         [self encryptString:(![UserPrefConstants singleton].skipPin ? od.pin : @"123456")],                                                                //R
                         (unichar)136                                                                    //ˆ
                         ];
    
    if ([od.validity isEqualToString:@"GTD"]) {
        postStr = [NSString stringWithFormat:@"%@|:=%@", postStr, od.expiry];
    }
    if ([UserPrefConstants singleton].verified2FA && ![od.otpPin isEqualToString:@""] && od.otpPin != nil) {
        postStr = [NSString stringWithFormat:@"%@|%C=%@", postStr, (unichar)140, od.otpPin];
    }
    if ([UserPrefConstants singleton].verified2FA && ![od.deviceID isEqualToString:@""] && od.deviceID != nil) {
        postStr = [NSString stringWithFormat:@"%@|%C=%@", postStr, (unichar)142, od.deviceID];
    }
    if (orderConfirmation) {
        postStr = [NSString stringWithFormat:@"%@|L=1", postStr];
    }
    
     if ([UserPrefConstants singleton].HTTPSEnabled) {
         urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradeTicket?%@",
                   atpServer,
                   [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                   postStr
                   ];
         
     } else {
         urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradeTicket?%@",
                   atpServer,
                   [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                   postStr
                   ];
         
     }
    
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecryptionProcess:content];
         ////NSLog(@"%@", content);
         
         
         if ([content rangeOfString:@"Error 509  50"].location != NSNotFound) {
             self.success = NO;
             self.confirmUACD = uacd;
             self.confirmOD = od;
             self.confirmTS = ts;
             
             NSArray *tokens = [content componentsSeparatedByString:@"||"];
             NSString *msg = [tokens objectAtIndex:1];
             if ([msg rangeOfString:@"82"].location != NSNotFound) {
                 msg = [msg stringByReplacingOccurrencesOfString:@"82" withString:@""];
             }
             
             UIAlertView *alertExceed5Bids = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
             //Set tag=2 to indicate the alert is from revise order submission
             [alertExceed5Bids setTag:2];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [alertExceed5Bids show];
             });         }
         
         
         else if ([content rangeOfString:@"Error 502  50"].location != NSNotFound)
         {
             
             self.success = NO;
             self.confirmUACD = uacd;
             self.confirmOD = od;
             self.confirmTS = ts;
             
             NSArray *tokens = [content componentsSeparatedByString:@"||"];
             NSString *msg = [tokens objectAtIndex:1];
             if ([msg rangeOfString:@"82"].location != NSNotFound) {
                 msg = [msg stringByReplacingOccurrencesOfString:@"82" withString:@""];
             }
             
             if([self.delegate respondsToSelector:@selector(showShortSellConfirm:msg:)]){
                 [self.delegate showShortSellConfirm:NO msg:msg];
             }else{
                 UIAlertView *alertShortSell = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"Short Sell Confirmation"]
                                                                          message:msg delegate:self
                                                                cancelButtonTitle:[LanguageManager stringForKey:@"Ok"]
                                                                otherButtonTitles:[LanguageManager stringForKey:@"Cancel"], nil];
                 //Set tag=2 to indicate the alert is from revise order submission
                 [alertShortSell setTag:2];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [alertShortSell show];
                 });
             }

             
         }
         else
         {
             GTMRegex *errorRegex1 = [GTMRegex regexWithPattern:@"ERROR.*"];
             GTMRegex *errorRegex2 = [GTMRegex regexWithPattern:@"Error.*"];
             
             //Error - Insufficient limit of CUT account
             if ([content rangeOfString:@"Error 510  50"].location != NSNotFound) {
                 self.success = NO;
                 
                 NSArray *tokens = [content componentsSeparatedByString:@"||"];
                 NSString *msg = [tokens objectAtIndex:1];
                 if ([msg rangeOfString:@"82"].location != NSNotFound) {
                     msg = [msg stringByReplacingOccurrencesOfString:@"82" withString:@""];
                 }
                 [notificationData setObject:msg forKey:@"msg"];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"tradeReviseNotification" object:self userInfo:notificationData];
                 
             }
             else if ([errorRegex1 matchesString:content] || [errorRegex2 matchesString:content])
             {
                 self.success = NO;
                 self.message = content;
                 [notificationData setObject:content forKey:@"msg"];
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"tradeReviseNotification" object:self userInfo:notificationData];
                 
             }
             else
             {
                 NSArray *tokens = [content componentsSeparatedByString:@"|"];
                 for (NSString *tok in tokens)
                 {
                     NSArray *comps = [tok componentsSeparatedByString:@"="];
                     if ([comps count] == 2)
                     {
                         NSString *param = [comps objectAtIndex:0];
                         if ([param isEqualToString:@"I"])
                         {
                             [notificationData setObject:[comps objectAtIndex:1] forKey:@"loginStatus"];  ;
                         }
                         else if ([param isEqualToString:@"M"])
                         {
                             [notificationData setObject:[comps objectAtIndex:1] forKey:@"msg"];
                         }
                     }
                     
                 }
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"tradeReviseNotification" object:self userInfo:notificationData];
             }
         }
         
     }];
}



- (void) prepareTradeCancel:(TradeData *) tradeData forAccount:(UserAccountClientData *) uacd forOrder:(OrderDetails *)od forExistingOrder:(TradeStatus *) ts {
    
    
    NSString *postStr = [NSString stringWithFormat:@"*=%@|+=%@|,=%@|D=%@|E=%@|7=%@|.=%@|/=%@|2=%@|3=%@|M=%@|P=%@|8=%@|4=%@|5=%@|6=%@|9=%@|B=%@|F=%@|<=%@|%C=%@|;=%@|@=%@|A=%@|%C=%@|%C=%@|R=%@|%C=MD",
                         ts.guid,                                                                   //*
                         ts.order_number,                                                           //+
                         ts.fix_order_number,                                                       //,
                         ts.unmt_quantity,                                                          //D
                         ts.mt_quantity,                                                            //E
                         [self generateTimeStamp],                                                  //7
                         uacd.client_account_number,                                                //.
                         uacd.client_code,                                                          ///
                         [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"],                                        //2
                         uacd.broker_code,                                                          //3
                         uacd.broker_branch_code,                                                   //M
                         uacd.client_acc_exchange,                                                  //P
                         od.order_source,                                                           //8
                         od.order_action,                                                           //4
                         [ts.stock_code stringByReplacingOccurrencesOfString:@"\n" withString:@""], //5
                         ts.order_type,                                                             //6
                         ts.validity,                                                               //9
                         ts.sett_currency,                                                         //B
                         ts.payment_type,                                                           //F
                         ts.order_price,                                                            //<
                         (unichar)159,                                                              //Ascii159
                         ts.trigger_price,
                         ts.order_quantity,                                                         //;
                         ts.min_quantity,                                                           //@
                         ts.disclosed_quantity,                                                     //A
                         (unichar)154,                                                              //Ascii154
                         ts.triggerType,
                         (unichar)155,                                                              //Ascii155
                         ts.triggerDirection,
                         [self encryptString:(![UserPrefConstants singleton].skipPin ? od.pin : @"123456")],                                                           //R
                         (unichar)136                                                               //ˆ
                         ];
    
    if ([ts.validity isEqualToString:@"GTD"]) {
        postStr = [NSString stringWithFormat:@"%@|:=%@", postStr, ts.expiry];
    }
    if ([UserPrefConstants singleton].verified2FA && ![od.otpPin isEqualToString:@""] && od.otpPin != nil) {
        postStr = [NSString stringWithFormat:@"%@|%C=%@", postStr, (unichar)140, od.otpPin];
    }
    if ([UserPrefConstants singleton].verified2FA && ![od.deviceID isEqualToString:@""] && od.deviceID != nil) {
        postStr = [NSString stringWithFormat:@"%@|%C=%@", postStr, (unichar)142, od.deviceID];
    }
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradeTicket?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradeTicket?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecryptionProcess:content];
       //  NSLog(@"CANCEL REPLY: %@ \n\n", content);
         
         // Add error message handling Nov 2016
         // Eg ERROR,1492 ATP - Order Price cannot be more than 4 decimal places
         
         if (([content rangeOfString:@"Error"].location != NSNotFound)||
             ([content rangeOfString:@"ERROR"].location != NSNotFound)) {
             
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             NSArray *errorMsg = [content componentsSeparatedByString:@"-"];
             if ([errorMsg count]==2) {
                 [notificationData setObject:[errorMsg objectAtIndex:0] forKey:@"loginStatus"];
                 [notificationData setObject:[errorMsg objectAtIndex:1] forKey:@"msg"];
             } else {
                 [notificationData setObject:@"Error" forKey:@"loginStatus"];
                 [notificationData setObject:content forKey:@"msg"];
             }
             
             [[NSNotificationCenter defaultCenter] postNotificationName:@"tradeCancelNotification" object:self userInfo:notificationData];
             return;
         }
         
         
         NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
         NSArray *tokens = [content componentsSeparatedByString:@"|"];
         for (NSString *tok in tokens)
         {
             NSArray *comps = [tok componentsSeparatedByString:@"="];
             if ([comps count] == 2)
             {
                 NSString *param = [comps objectAtIndex:0];
                 if ([param isEqualToString:@"I"])
                 {
                     [notificationData setObject:[comps objectAtIndex:1] forKey:@"loginStatus"];  ;
                 }
                 else if ([param isEqualToString:@"M"])
                 {
                     [notificationData setObject:[comps objectAtIndex:1] forKey:@"msg"];
                 }
             }
             
         }
         
         
         [[NSNotificationCenter defaultCenter] postNotificationName:@"tradeCancelNotification" object:self userInfo:notificationData];
         
         
     }];
    
}

- (void)getPortfolio:(NSInteger)account_selection {
    
    UserAccountClientData *uacd = nil;
    uacd = [UserPrefConstants singleton].userSelectedAccount;
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|b=%@|Z=%ld",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.	bhclicode
                         uacd.broker_code,							//3	broker code
                         uacd.broker_branch_code,                   //M	broker branch code
                         uacd.client_acc_exchange,                  //b client acc exchange code,
                         (long)account_selection                    //Z realized PF=0 / unrealized PF=1
                         /*@"KL"*/                                  //d stock exchg code:
                                                                    //  act as filter to show only what specified here
                         ];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradePortfolio?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradePortfolio?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
    [UserPrefConstants singleton].eqtyUnrealPFResultList = [[NSMutableArray alloc] init];
    [UserPrefConstants singleton].eqtyUnrealPFResultStkCodeArr = [[NSMutableArray alloc] init];
   
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecryptionProcess:content];
         //NSLog(@"getPortfolio %@", content);
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         
         int stockcode = -1;					//+
         int stockname = -1;					//,
         int stockcode_cn = -1;				//-
         int price_high = -1;				//.
         int price_low = -1;					///
         int price_year_high = -1;			//0
         int price_year_low = -1;			//1
         int exchange_ref_number = -1;		//2
         int client_account = -1;			//3
         int branch_code = -1;				//4
         int brokerage = -1;					//5
         int quantity_available = -1;		//6
         int quantity_on_hand = -1;			//7
         int total_price_P = -1;				//8
         int total_price_B = -1;				//9
         int total_quantity_P = -1;			//:
         int total_comm_P = -1;				//;
         int sell_qty_in_proc = -1;          //ascii132
         
         int total_price_S = -1;				//>
         int rms_code = -1;					//?
         int price_last_done = -1;			//@
         int price_ref = -1;					//A
         int price_AS = -1;					//B
         int price_AP = -1;					//C
         int volume = -1;					//D
         int lot_size = -1;					//E
         int change_amount = -1;				//F
         int change_percentage = -1;			//G
         int sector_code = -1;				//H
         int stock_class = -1;				//I
         int stock_status = -1;				//J
         int price_base = -1;				//K
         int price_day_high = -1;			//L
         int price_day_low = -1;				//M
         int price_avg_purchase = -1;		//N
         int limit_upper = -1;				//O
         int limit_lower = -1;				//P
         int gross_market_value = -1;		//Q//last done * qty on hadn
         int unrealized_gain_loss_amount = -1;		//R.
         int unrealized_gain_loss_percentage = -1;	//S
         int total_value_P = -1;				//T
         int total_value_S = -1;				//U
         int total_quantity_S = -1;			//V
         int total_brokerage = -1;			//W
         int gain_loss = -1;					//X
         int aggregated_buy_price = -1;		//Y
         int aggregated_sell_price = -1;		//Z
         int total_quantity_short = -1;		//[
         int total_comm_S = -1;				//]
         int currency = -1;					//^
         int settlement_mode = -1;           //Ascii 151
         int realized_gain_loss_amount = -1;	//}
         int realized_gain_loss_percentage = -1;		//~
         
         NSArray *arr = [content componentsSeparatedByString:@"\r"];
         NSString *s;
         
         // NSLog(@"PFData %@",arr);
         
         for (s in arr) {
             s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
             if ([metaregex matchesString:s]) {
                 s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                 NSArray *tokens = [s componentsSeparatedByString:@"|"];
                 if ([tokens count] > 5) {
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@"+"]) {
                             stockcode = pos;
                         }
                         else if ([token isEqualToString:@","]) {
                             stockname = pos;
                         }
                         else if ([token isEqualToString:@"-"]) {
                             stockcode_cn = pos;
                         }
                         else if ([token isEqualToString:@"."]) {
                             price_high = pos;
                         }
                         else if ([token isEqualToString:@"/"]) {
                             price_low = pos;
                         }
                         else if ([token isEqualToString:@"0"]) {
                             price_year_high = pos;
                         }
                         else if ([token isEqualToString:@"1"]) {
                             price_year_low = pos;
                         }
                         else if ([token isEqualToString:@"2"]) {
                             exchange_ref_number = pos;
                         }
                         else if ([token isEqualToString:@"3"]) {
                             client_account = pos;
                         }
                         else if ([token isEqualToString:@"4"]) {
                             branch_code = pos;
                         }
                         else if ([token isEqualToString:@"5"]) {
                             brokerage = pos;
                         }
                         else if ([token isEqualToString:@"6"]) {
                             quantity_available = pos;
                         }
                         
                         else if ([token isEqualToString:@"7"]) {
                             quantity_on_hand = pos;
                         }
                         else if ([token isEqualToString:@"8"]) {
                             total_price_P = pos;
                         }
                         else if ([token isEqualToString:@"9"]) {
                             total_price_B = pos;
                         }
                         else if ([token isEqualToString:@":"]) {
                             total_quantity_P = pos;
                         }
                         else if ([token isEqualToString:@";"]) {
                             total_comm_P = pos;
                         }
                         else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)132]]) {
                             sell_qty_in_proc = pos;
                         }
                         else if ([token isEqualToString:@">"]) {
                             total_price_S = pos;
                         }
                         else if ([token isEqualToString:@"?"]) {
                             rms_code = pos;
                         }
                         else if ([token isEqualToString:@"@"]) {
                             price_last_done = pos;
                         }
                         else if ([token isEqualToString:@"A"]) {
                             price_ref = pos;
                         }
                         
                         else if ([token isEqualToString:@"B"]) {
                             price_AS = pos;
                         }
                         else if ([token isEqualToString:@"C"]) {
                             price_AP = pos;
                         }
                         else if ([token isEqualToString:@"D"]) {
                             volume = pos;
                         }
                         else if ([token isEqualToString:@"E"]) {
                             lot_size = pos;
                         }
                         else if ([token isEqualToString:@"F"]) {
                             change_amount = pos;
                         }
                         else if ([token isEqualToString:@"G"]) {
                             change_percentage = pos;
                         }
                         else if ([token isEqualToString:@"H"]) {
                             sector_code = pos;
                         }
                         else if ([token isEqualToString:@"I"]) {
                             stock_class = pos;
                         }
                         else if ([token isEqualToString:@"J"]) {
                             stock_status = pos;
                         }
                         else if ([token isEqualToString:@"K"]) {
                             price_base = pos;
                         }
                         
                         else if ([token isEqualToString:@"L"]) {
                             price_day_high = pos;
                         }
                         else if ([token isEqualToString:@"M"]) {
                             price_day_low = pos;
                         }
                         else if ([token isEqualToString:@"N"]) {
                             price_avg_purchase = pos;
                         }
                         else if ([token isEqualToString:@"O"]) {
                             limit_upper = pos;
                         }
                         else if ([token isEqualToString:@"P"]) {
                             limit_lower = pos;
                         }
                         else if ([token isEqualToString:@"Q"]) {
                             gross_market_value = pos;
                         }
                         else if ([token isEqualToString:@"R"]) {
                             unrealized_gain_loss_amount = pos;
                         }
                         else if ([token isEqualToString:@"S"]) {
                             unrealized_gain_loss_percentage = pos;
                         }
                         else if ([token isEqualToString:@"T"]) {
                             total_value_P = pos;
                         }
                         else if ([token isEqualToString:@"U"]) {
                             total_quantity_S = pos;
                         }
                         else if ([token isEqualToString:@"V"]) {
                             total_value_S = pos;
                         }
                         else if ([token isEqualToString:@"W"]) {
                             total_brokerage = pos;
                         }
                         else if ([token isEqualToString:@"X"]) {
                             gain_loss = pos;
                         }
                         else if ([token isEqualToString:@"Y"]) {
                             aggregated_buy_price = pos;
                         }
                         else if ([token isEqualToString:@"Z"]) {
                             aggregated_sell_price = pos;
                         }
                         else if ([token isEqualToString:@"["]) {
                             total_quantity_short = pos;
                         }
                         else if ([token isEqualToString:@"]"]) {
                             total_comm_S = pos;
                         }
                         else if ([token isEqualToString:@"^"]) {
                             currency = pos;
                         }
                         else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)151]]) {
                             settlement_mode = pos;
                         }
                         else if ([token isEqualToString:@"}"]) {
                             realized_gain_loss_amount = pos;
                         }
                         else if ([token isEqualToString:@"~"]) {
                             realized_gain_loss_percentage = pos;
                         }
                         pos++;
                     }
                 }
             }
             else if ([dataregex matchesString:s]) {
                 s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                 NSArray *tokens = [s componentsSeparatedByString:@"|"];
                 if ([tokens count] > 5) {
                     PortfolioData *pd = [[PortfolioData alloc] init];
                     if (stockcode != -1) {
                         pd.stockcode = [tokens objectAtIndex:stockcode];
                         [[UserPrefConstants singleton].eqtyUnrealPFResultStkCodeArr addObject:pd.stockcode];
                     }
                     if (stockname != -1) {
                         pd.stockname = [tokens objectAtIndex:stockname];
                     }
                     if (stockcode_cn != -1) {
                         pd.stockcode_cn = [tokens objectAtIndex:stockcode_cn];
                     }
                     if (price_high != -1) {
                         pd.price_high = [tokens objectAtIndex:price_high];
                     }
                     if (price_low != -1) {
                         pd.price_low = [tokens objectAtIndex:price_low];
                     }
                     if (price_year_high != -1) {
                         pd.price_year_high = [tokens objectAtIndex:price_year_high];
                     }
                     if (price_year_low != -1) {
                         pd.price_year_low = [tokens objectAtIndex:price_year_low];
                     }
                     if (exchange_ref_number != -1) {
                         pd.exchange_ref_number = [tokens objectAtIndex:exchange_ref_number];
                     }
                     if (client_account != -1) {
                         pd.client_account = [tokens objectAtIndex:client_account];
                     }
                     if (branch_code != -1) {
                         pd.branch_code = [tokens objectAtIndex:branch_code];
                     }
                     if (brokerage != -1) {
                         pd.brokerage = [tokens objectAtIndex:brokerage];
                     }
                     
                     if (quantity_available != -1) {
                         pd.quantity_available = [tokens objectAtIndex:quantity_available];
                     }
                     if (quantity_on_hand != -1) {
                         pd.quantity_on_hand = [tokens objectAtIndex:quantity_on_hand];
                     }
                     if (total_price_P != -1) {
                         pd.total_price_P = [tokens objectAtIndex:total_price_P];
                     }
                     if (total_price_B != -1) {
                         pd.total_price_B = [tokens objectAtIndex:total_price_B];
                     }
                     if (total_quantity_P != -1) {
                         pd.total_quantity_P = [tokens objectAtIndex:total_quantity_P];
                     }
                     if (total_comm_P != -1) {
                         pd.total_comm_P = [tokens objectAtIndex:total_comm_P];
                     }
                     if (sell_qty_in_proc != -1) {
                         pd.sell_qty_in_proc = [tokens objectAtIndex:sell_qty_in_proc];
                     }
                     if (total_price_S != -1) {
                         pd.total_price_S = [tokens objectAtIndex:total_price_S];
                     }
                     if (rms_code != -1) {
                         pd.rms_code = [tokens objectAtIndex:rms_code];
                     }
                     if (price_last_done != -1) {
                         pd.price_last_done = [tokens objectAtIndex:price_last_done];
                     }
                     if (price_ref != -1) {
                         pd.price_ref = [tokens objectAtIndex:price_ref];
                     }
                     
                     if (price_AS != -1) {
                         pd.price_AS = [tokens objectAtIndex:price_AS];
                     }
                     if (price_AP != -1) {
                         pd.price_AP = [tokens objectAtIndex:price_AP];
                     }
                     if (volume != -1) {
                         pd.volume = [tokens objectAtIndex:volume];
                     }
                     if (lot_size != -1) {
                         pd.lot_size = [tokens objectAtIndex:lot_size];
                     }
                     if (change_amount != -1) {
                         pd.change_amount = [tokens objectAtIndex:change_amount];
                     }
                     if (change_percentage != -1) {
                         pd.change_percentage = [tokens objectAtIndex:change_percentage];
                     }
                     if (sector_code != -1) {
                         pd.sector_code = [tokens objectAtIndex:sector_code];
                     }
                     if (stock_class != -1) {
                         pd.stock_class = [tokens objectAtIndex:stock_class];
                     }
                     if (stock_status != -1) {
                         pd.stock_status = [tokens objectAtIndex:stock_status];
                     }
                     if (price_base != -1) {
                         pd.price_base = [tokens objectAtIndex:price_base];
                     }
                     if (price_day_high != -1) {
                         pd.price_day_high = [tokens objectAtIndex:price_day_high];
                     }
                     if (price_day_low != -1) {
                         pd.price_day_low = [tokens objectAtIndex:price_day_low];
                     }
                     if (price_avg_purchase != -1) {
                         pd.price_avg_purchase = [tokens objectAtIndex:price_avg_purchase];
                     }
                     if (limit_upper != -1) {
                         pd.limit_upper = [tokens objectAtIndex:limit_upper];
                     }
                     if (limit_lower != -1) {
                         pd.limit_lower = [tokens objectAtIndex:limit_lower];
                     }
                     if (gross_market_value != -1) {
                         pd.gross_market_value = [tokens objectAtIndex:gross_market_value];
                     }
                     if (unrealized_gain_loss_amount != -1) {
                         pd.unrealized_gain_loss_amount = [tokens objectAtIndex:unrealized_gain_loss_amount];
                     }
                     if (unrealized_gain_loss_percentage != -1) {
                         pd.unrealized_gain_loss_percentage = [tokens objectAtIndex:unrealized_gain_loss_percentage];
                     }
                     if (total_value_P != -1) {
                         pd.total_value_P = [tokens objectAtIndex:total_value_P];
                     }
                     if (total_value_S != -1) {
                         pd.total_value_S = [tokens objectAtIndex:total_value_S];
                     }
                     if (total_quantity_S != -1) {
                         pd.total_quantity_S = [tokens objectAtIndex:total_quantity_S];
                     }
                     if (total_brokerage != -1) {
                         pd.total_brokerage = [tokens objectAtIndex:total_brokerage];
                     }
                     if (gain_loss != -1) {
                         pd.gain_loss = [tokens objectAtIndex:gain_loss];
                     }
                     if (aggregated_buy_price != -1) {
                         pd.aggregated_buy_price = [tokens objectAtIndex:aggregated_buy_price];
                     }
                     if (aggregated_sell_price != -1) {
                         pd.aggregated_sell_price = [tokens objectAtIndex:aggregated_sell_price];
                     }
                     if (total_quantity_short != -1) {
                         pd.total_quantity_short = [tokens objectAtIndex:total_quantity_short];
                     }
                     if (total_comm_S != -1) {
                         pd.total_comm_S = [tokens objectAtIndex:total_comm_S];
                     }
                     if (currency != -1) {
                         pd.currency = [tokens objectAtIndex:currency];
                     }
                     if (settlement_mode != -1) {
                         pd.settlement_mode = [tokens objectAtIndex:settlement_mode];
                     }
                     if (realized_gain_loss_amount != -1) {
                         pd.realized_gain_loss_amount = [tokens objectAtIndex:realized_gain_loss_amount];
                     }
                     if (realized_gain_loss_percentage != -1) {
                         pd.realized_gain_loss_percentage = [tokens objectAtIndex:realized_gain_loss_percentage];
                     }
                     [[UserPrefConstants singleton].eqtyUnrealPFResultList addObject:pd];
                     
                    // NSLog(@"pd.stockcode %@", pd.stockcode);
                 }
             }
         }
         [[VertxConnectionManager singleton]vertxGetStockDetailWithStockArr:[UserPrefConstants singleton].eqtyUnrealPFResultStkCodeArr FIDArr:[[UserPrefConstants singleton]userQuoteScreenColumn] tag:2];
         
         
     }];
    
}


- (void) doTradePrtfSummRpt:(int)account_selection isDerivative:(BOOL)isDerivative {
    UserAccountClientData *uacd = nil;
    uacd = [UserPrefConstants singleton].userSelectedAccount;
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|c=%@|e=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.	bhclicode
                         uacd.broker_code,							//3	broker code
                         uacd.broker_branch_code,                   //M	broker branch code
                         uacd.client_acc_exchange,                  //c client acc exchange code,
                         isDerivative?@"D":@"E"                     //e Market Type.‘E’=Equity or ‘D’=Derivative
                         ];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradePrtfSummRpt?%@",
                                                   atpServer,
                                                   [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                                                   postStr
                                                   ];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradePrtfSummRpt?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
     NSLog(@"doTradePrtfSummRpt %@",urlStr);
    
    [UserPrefConstants singleton].summaryReportArray = [[NSMutableArray alloc] init];
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecryptionProcess:content];
        

        NSLog(@"\n\n---- TradePrtfSummRpt Returned Data ----\n%@\n\n", content);
        
        GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
        GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
        
        int open_long= -1;					//c Ascii 99
        int open_short = -1;				//d Ascii 100
        int cash_balance = -1;				//g Ascii 103
        int deposit = -1;                   //h Ascii 104
        int withdrawal = -1;				//i Ascii 105
        int eligible_collateral = -1;		//j Ascii 106
        int initial_margin = -1;			//k Ascii 107
        int maintenance_margin = -1;		//l Ascii 108
        int margin_call = -1;               //m Ascii 109
        int realized_pl = -1;				//} Ascii 125
        
        NSArray *arr = [content componentsSeparatedByString:@"\r"];
         
       // NSLog(@"TradePrtfSummRpt : %@",arr);

         
        NSString *s;
        for (s in arr) {
            s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            if ([metaregex matchesString:s]) {
                s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                int pos = 0;
                for (NSString *token in tokens) {
                    if ([token isEqualToString:@"c"]) {
                        //open long
                        open_long = pos;
                    }
                    else if ([token isEqualToString:@"d"]) {
                        //open short
                        open_short = pos;
                    }
                    else if ([token isEqualToString:@"g"]) {
                        //cash balance
                        cash_balance = pos;
                    }
                    else if ([token isEqualToString:@"h"]) {
                        //deposit
                        deposit = pos;
                    }
                    else if ([token isEqualToString:@"i"]) {
                        //withdrawal
                        withdrawal = pos;
                    }
                    else if ([token isEqualToString:@"j"]) {
                        //eligible collateral
                        eligible_collateral = pos;
                    }
                    else if ([token isEqualToString:@"k"]) {
                        //initial margin
                        initial_margin = pos;
                    }
                    else if ([token isEqualToString:@"l"]) {
                        //maintenance margin
                        maintenance_margin = pos;
                    }
                    else if ([token isEqualToString:@"m"]) {
                        //margin call
                        margin_call = pos;
                    }
                    else if ([token isEqualToString:@"}"]) {
                        //realized gl
                        realized_pl = pos;
                    }
                    pos++;
                }
            }
            else if ([dataregex matchesString:s]) {
                PrtfSummRptData *prtfSumRptData = [[PrtfSummRptData alloc] init];
                
                s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                
                if (open_long != -1) {
                    prtfSumRptData.open_long = [tokens objectAtIndex:open_long];
                }
                if (open_short != -1) {
                    prtfSumRptData.open_short = [tokens objectAtIndex:open_short];
                }
                if (cash_balance != -1) {
                    prtfSumRptData.bf_cash_balance = [tokens objectAtIndex:cash_balance];
                }
                if (deposit != -1) {
                    prtfSumRptData.deposit = [tokens objectAtIndex:deposit];
                }
                if (withdrawal != -1) {
                    prtfSumRptData.withdrawal = [tokens objectAtIndex:withdrawal];
                }
                if (eligible_collateral != -1) {
                    prtfSumRptData.eligible_collateral = [tokens objectAtIndex:eligible_collateral];
                }
                if (initial_margin != -1) {
                    prtfSumRptData.initial_margin = [tokens objectAtIndex:initial_margin];
                }
                if (maintenance_margin != -1) {
                    prtfSumRptData.maintenance_margin = [tokens objectAtIndex:maintenance_margin];
                }
                if (margin_call != -1) {
                    prtfSumRptData.margin_call = [tokens objectAtIndex:margin_call];
                }
                if (realized_pl != -1) {
                    prtfSumRptData.realized_pl = [tokens objectAtIndex:realized_pl];
                }
                //Calculation: Current Balance = Cash Balance + Deposit - Withdrawal + Realized PL
                if (cash_balance != -1 && deposit != -1 && withdrawal != -1 && realized_pl != -1) {
                    prtfSumRptData.current_balance = ([prtfSumRptData.bf_cash_balance doubleValue] + [prtfSumRptData.deposit doubleValue]) - [prtfSumRptData.withdrawal doubleValue] + [prtfSumRptData.realized_pl doubleValue];
                }
                
                [[UserPrefConstants singleton].summaryReportArray addObject:prtfSumRptData];
                
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SummaryReportDone" object:nil];
    }];
}


- (void) doTradePrtfSubDtlRpt:(int)account_selection isDerivative:(BOOL)isDerivative {
    UserAccountClientData *uacd = nil;
    
    uacd = [UserPrefConstants singleton].userSelectedAccount;
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|c=%@|e=%@",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.	bhclicode
                         uacd.broker_code,							//3	broker code
                         uacd.broker_branch_code,                   //M	broker branch code
                         uacd.client_acc_exchange,                  //c client acc exchange code,
                         isDerivative?@"D":@"E"                     //e Market Type.‘E’=Equity or ‘D’=Derivative
                         ];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradePrtfSubDtlRpt?%@",
                                                   atpServer,
                                                   [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                                                   postStr
                                                   ];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradePrtfSubDtlRpt?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
    
    
    [UserPrefConstants singleton].subDetailReportArray = [[NSMutableArray alloc] init];
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecryptionProcess:content];
        //NSLog(@"\n\n---- TradePrtfSubDtlRpt Returned Data ----\n%@\n\n", content);
        
        GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
        GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
        
        int sequence_no = -1;				//* Ascii 42
        int stock_code = -1;				//+ Ascii 43
        int stock_name = -1;                //, Ascii 44
        int average_purchase_price = -1;	//n Ascii 78
        int unrealized_pl = -1;             //r Ascii 82
        int gross_buy = -1;                 //_ Ascii 95
        int gross_sell = -1;                //` Ascii 96
        int day_buy = -1;                   //a Ascii 97
        int day_sell = -1;                  //b Ascii 98
        int open_buy = -1;                  //c Ascii 99
        int open_sell = -1;                 //d Ascii 100
        int product_code = -1;              //o Ascii 111
        int contract_per_val = -1;          //v Ascii 118
        int realized_pl = -1;               //} Ascii 125
        int home_currency = -1;             //Ascii 153
        int forex_exchange_rate= -1;        //Ascii 154
        int stock_currency = -1;            //Ascii 155
        
        NSArray *arr = [content componentsSeparatedByString:@"\r"];
         
         //NSLog(@"SubDetailReportDone %@",arr);
         
        NSString *s;
        for (s in arr) {
            s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            if ([metaregex matchesString:s]) {
                s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                int pos = 0;
                for (NSString *token in tokens) {
                    if ([token isEqualToString:@"*"]) {
                        //sequence no
                        sequence_no = pos;
                    }
                    else if ([token isEqualToString:@"+"]) {
                        //stock code
                        stock_code = pos;
                    }
                    else if ([token isEqualToString:@","]) {
                        //stock name
                        stock_name = pos;
                    }
                    else if ([token isEqualToString:@"N"]) {
                        //average purchse price
                        average_purchase_price = pos;
                    }
                    else if ([token isEqualToString:@"R"]) {
                        //unrealized gl
                        unrealized_pl = pos;
                    }
                    else if ([token isEqualToString:@"_"]) {
                        //gross buy
                        gross_buy = pos;
                    }
                    else if ([token isEqualToString:@"`"]) {
                        //gross sell
                        gross_sell = pos;
                    }
                    else if ([token isEqualToString:@"a"]) {
                        //day buy
                        day_buy = pos;
                    }
                    else if ([token isEqualToString:@"b"]) {
                        //day sell
                        day_sell = pos;
                    }
                    else if ([token isEqualToString:@"c"]) {
                        //open buy
                        open_buy = pos;
                    }
                    else if ([token isEqualToString:@"d"]) {
                        //open sell
                        open_sell = pos;
                    }
                    else if ([token isEqualToString:@"o"]) {
                        //product code
                        product_code = pos;
                    }
                    else if ([token isEqualToString:@"v"]) {
                        //contract per val
                        contract_per_val = pos;
                    }
                    else if ([token isEqualToString:@"}"]) {
                        //realized gl
                        realized_pl = pos;
                    }
                    else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)153]]) {
                        //home currency
                        home_currency = pos;
                    }
                    else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)154]]) {
                        //exchange rate
                        forex_exchange_rate = pos;
                    }
                    else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)155]]) {
                        //stock currency
                        stock_currency = pos;
                    }
                    pos++;
                }
            }
            else if ([dataregex matchesString:s]) {
                PrtfSubDtlRptData *prtfSubDtlRptData = [[PrtfSubDtlRptData alloc] init];
                
                s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                
                if (sequence_no != -1) {
                    prtfSubDtlRptData.sequence_no = [[tokens objectAtIndex:sequence_no] intValue];
                }
                if (stock_code != -1) {
                    prtfSubDtlRptData.stock_code = [tokens objectAtIndex:stock_code];
                    [[UserPrefConstants singleton].futuresUnrealPFResultStkCodeArr addObject:prtfSubDtlRptData.stock_code];
                }
                if (stock_name != -1) {
                    prtfSubDtlRptData.stock_name = [tokens objectAtIndex:stock_name];
                }
                if (average_purchase_price != -1) {
                    prtfSubDtlRptData.average_purchase_price = [tokens objectAtIndex:average_purchase_price];
                }
                if (unrealized_pl != -1) {
                    prtfSubDtlRptData.unrealized_pl = [[tokens objectAtIndex:unrealized_pl] doubleValue];
                }
                if (gross_buy != -1) {
                    prtfSubDtlRptData.gross_buy = [tokens objectAtIndex:gross_buy];
                }
                if (gross_sell != -1) {
                    prtfSubDtlRptData.gross_sell = [tokens objectAtIndex:gross_sell];
                }
                if (day_buy != -1) {
                    prtfSubDtlRptData.day_buy = [tokens objectAtIndex:day_buy];
                }
                if (day_sell != -1) {
                    prtfSubDtlRptData.day_sell = [tokens objectAtIndex:day_sell];
                }
                if (open_buy != -1) {
                    prtfSubDtlRptData.bf_buy = [tokens objectAtIndex:open_buy];
                }
                if (open_sell != -1) {
                    prtfSubDtlRptData.bf_sell = [tokens objectAtIndex:open_sell];
                }
                if (product_code != -1) {
                    NSString *productCode = [tokens objectAtIndex:product_code];
                    NSArray *arr = [productCode componentsSeparatedByString:@"~"];
                    if ([arr count] == 2) {
                        prtfSubDtlRptData.unrealized_pl = [[arr objectAtIndex:0] doubleValue];
                        prtfSubDtlRptData.price_current = [[arr objectAtIndex:1] doubleValue];
                    }
                }
                if (contract_per_val != -1) {
                    prtfSubDtlRptData.contractPerVal = [[tokens objectAtIndex:contract_per_val] doubleValue];
                }
                if (realized_pl != -1) {
                    prtfSubDtlRptData.realized_pl = [tokens objectAtIndex:realized_pl];
                }
                if (home_currency != -1) {
                    prtfSubDtlRptData.home_currency = [tokens objectAtIndex:home_currency];
                }
                if (forex_exchange_rate != -1) {
                    prtfSubDtlRptData.exchange_rate = [tokens objectAtIndex:forex_exchange_rate];
                }
                if (stock_currency != -1) {
                    prtfSubDtlRptData.stock_currency = [tokens objectAtIndex:stock_currency];
                }
                //Calculation: Nett Position = Gross Buy - Gross Sell
                if (gross_buy != -1 && gross_sell != -1) {
                    prtfSubDtlRptData.nett_position = [prtfSubDtlRptData.gross_buy intValue] - [prtfSubDtlRptData.gross_sell intValue];
                }
                
                [[UserPrefConstants singleton].subDetailReportArray addObject:prtfSubDtlRptData];
                
            }
        }
         
         
       
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sequence_no" ascending:YES];
        [[UserPrefConstants singleton].subDetailReportArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SubDetailReportDone" object:nil];
         NSLog(@"[UserPrefConstants singleton].futuresUnrealPFResultStkCodeArr %@",[UserPrefConstants singleton].futuresUnrealPFResultStkCodeArr);
        [[VertxConnectionManager singleton]vertxGetStockDetailWithStockArr:[UserPrefConstants singleton].futuresUnrealPFResultStkCodeArr FIDArr:[[UserPrefConstants singleton]userQuoteScreenColumn] tag:3];
         
    }];
    
}

- (void) doTradePrtfDtlRpt:(int)account_selection isDerivative:(BOOL)isDerivative ofSubDtlRpt:(PrtfSubDtlRptData *)psdrd {
    UserAccountClientData *uacd = nil;
    uacd = [UserPrefConstants singleton].userSelectedAccount;
    
    NSArray *stkCodeArr = [psdrd.stock_code componentsSeparatedByString:@"."];
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@|.=%@|3=%@|M=%@|c=%@|e=%@|+=%@|b=%@|g=%d",
                         [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[SenderCode]"],        //' exclicode
                         uacd.client_account_number,                //.	bhclicode
                         uacd.broker_code,							//3	broker code
                         uacd.broker_branch_code,                   //M	broker branch code
                         uacd.client_acc_exchange,                  //c client acc exchange code,
                         isDerivative?@"D":@"E",                    //e Market Type.‘E’=Equity or ‘D’=Derivative
                         stkCodeArr[0],    //+ stock code
                         stkCodeArr[1], //b stock exchange
                         [UserPrefConstants singleton].detailReportType == 0 ? 2 : 1                   //g Mode 1-Overnight, 2-Day
                         ];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[%@]TradePrtfDtlRpt?%@",
                                                  atpServer,
                                                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                                                  postStr
                                                  ];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[%@]TradePrtfDtlRpt?%@",
                  atpServer,
                  [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
                  postStr
                  ];
    }
    
    if ( [UserPrefConstants singleton].detailReportType==0) {
        [UserPrefConstants singleton].detailDayReportArray = [[NSMutableArray alloc] init];
    } else {
         [UserPrefConstants singleton].detailNightReportArray = [[NSMutableArray alloc] init];
    }
        
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecryptionProcess:content];

        NSLog(@"\n\n---- TradePrtfDtlRpt Returned Data ----\n%@\n\n", content);
        
        GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
        GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
        
        int sequence_no = -1;				//* Ascii 42
        int side = -1;                      //r Ascii 114
        int matched_qty = -1;				//t Ascii 116
        int matched_price = -1;             //u Ascii 117
        int contract_per_val = -1;			//v Ascii 118
        int home_currency = -1;             //Ascii 153
        int forex_exchange_rate= -1;        //Ascii 154
        int stock_currency = -1;            //Ascii 155
        
        NSArray *arr = [content componentsSeparatedByString:@"\r"];
         NSString *s;
         
        // NSLog(@"TradePrtfDtlReport : %@",arr);
         
        for (s in arr) {
            s = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            if ([metaregex matchesString:s]) {
                s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                int pos = 0;
                for (NSString *token in tokens) {
                    if ([token isEqualToString:@"*"]) {
                        //sequence no
                        sequence_no = pos;
                    }
                    else if ([token isEqualToString:@"r"]) {
                        //side
                        side = pos;
                    }
                    else if ([token isEqualToString:@"t"]) {
                        //matched quantity
                        matched_qty = pos;
                    }
                    else if ([token isEqualToString:@"u"]) {
                        //matched price
                        matched_price = pos;
                    }
                    else if ([token isEqualToString:@"v"]) {
                        //contract per val
                        contract_per_val = pos;
                    }
                    else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)153]]) {
                        //home currency
                        home_currency = pos;
                    }
                    else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)154]]) {
                        //exchange rate
                        forex_exchange_rate = pos;
                    }
                    else if ([token isEqualToString:[NSString stringWithFormat:@"%C", (unichar)155]]) {
                        //stock currency
                        stock_currency = pos;
                    }
                    pos++;
                }
            }
            else if ([dataregex matchesString:s]) {
                PrtfDtlRptData *prtfDtlRptData = [[PrtfDtlRptData alloc] init];
                
                s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                NSArray *tokens = [s componentsSeparatedByString:@"|"];
                
                if (sequence_no != -1) {
                    prtfDtlRptData.sequence_no = [[tokens objectAtIndex:sequence_no] intValue];
                }
                if (side != -1) {
                    prtfDtlRptData.side = [tokens objectAtIndex:side];
                }
                if (matched_qty != -1) {
                    prtfDtlRptData.matched_qty = [tokens objectAtIndex:matched_qty];
                }
                if (matched_price != -1) {
                    prtfDtlRptData.matched_price = [tokens objectAtIndex:matched_price];
                }
                if (contract_per_val != -1) {
                    prtfDtlRptData.contractPerVal = [[tokens objectAtIndex:contract_per_val] doubleValue];
                }
                if (home_currency != -1) {
                    prtfDtlRptData.home_currency = [tokens objectAtIndex:home_currency];
                }
                if (forex_exchange_rate != -1) {
                    prtfDtlRptData.exchange_rate = [tokens objectAtIndex:forex_exchange_rate];
                }
                if (stock_currency != -1) {
                    prtfDtlRptData.stock_currency = [tokens objectAtIndex:stock_currency];
                }
                
                if ([UserPrefConstants singleton].detailReportType == 0) {
                    [[UserPrefConstants singleton].detailDayReportArray addObject:prtfDtlRptData];
                }
                else if ([UserPrefConstants singleton].detailReportType == 1) {
                    [[UserPrefConstants singleton].detailNightReportArray addObject:prtfDtlRptData];
                }
                
            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sequence_no" ascending:YES];
        [[UserPrefConstants singleton].detailDayReportArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        [[UserPrefConstants singleton].detailNightReportArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
     
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DetailReportDone" object:nil];
    }];
    
  
}



#pragma mark - 1FA/2FA

-(void) doValidate1FAPassword:(NSString*)pwd {
    
    self.passwd1FA = pwd;
    
    
    // GetE2EE
    NSString *cmdStr  = @"TradeGetE2EEParams";
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/%@?%@", atpServer,cmdStr, [self generateTimeStamp]];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/%@?%@", atpServer,cmdStr, [self generateTimeStamp]];
    }
    
    // NSLog(@"GetE2EEParams URL: %@", urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         /// NSLog(@"TradeGetE2EEParams Done");
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         // NSLog(@"\n\n---- TradeGetE2EEParams Returned Data ----\n%@\n\n", content);
         
         GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
         GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
         
         if ([errorReg1 matchesString:content] || [errorReg2 matchesString:content] || [content isEqualToString:@""]) {
             self.error_message = content;
             if (self.error_message == nil || [self.error_message isEqualToString:@""]) {
                 
             }
             ///NSLog(@"TradeGetE2EEParams Failed: %@", self.error_message);
             
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:self.error_message forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
             
             
         } else if ([content rangeOfString:@"(="].location == NSNotFound) {
             
             /// NSLog(@"TradeGetE2EEParams Failed: %@", self.error_message);
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:@"Failed" forKey:@"loginStatusCode"];
             [notificationData setObject:self.error_message forKey:@"error"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"atpLoginStatus" object:self userInfo:notificationData];
         } else {
             NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
             
             int pubKey = -1;
             int sessionID = -1;
             int randomNumber = -1;
             int modulus = -1;
             int exponent = -1;
             
             if (e2eeInfo1FA != nil) {
                 e2eeInfo1FA = nil;
             }
             e2eeInfo1FA = [[E2EEInfo alloc] init];
             
             for (NSString *str in arr) {
                 if ([str hasPrefix:@"(="]) {
                     NSString *s = [str stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@"+"]) {
                             pubKey = pos;
                         }
                         else if ([token isEqualToString:@","]) {
                             sessionID = pos;
                         }
                         else if ([token isEqualToString:@"-"]) {
                             randomNumber = pos;
                         }
                         else if ([token isEqualToString:@"."]) {
                             modulus = pos;
                         }
                         else if ([token isEqualToString:@"/"]) {
                             exponent = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([str hasPrefix:@")="]) {
                     NSString *s = [str stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if (pubKey != -1) {
                         e2eeInfo1FA.pubKey = [tokens objectAtIndex:pubKey];
                     }
                     if (sessionID != -1) {
                         e2eeInfo1FA.sessionID = [tokens objectAtIndex:sessionID];
                     }
                     if (randomNumber != -1) {
                         e2eeInfo1FA.randomKey = [tokens objectAtIndex:randomNumber];
                     }
                     if (modulus != -1) {
                         e2eeInfo1FA.modulus = [tokens objectAtIndex:modulus];
                     }
                     if (exponent != -1) {
                         e2eeInfo1FA.exponent = [tokens objectAtIndex:exponent];
                     }
                 }
             }
             
             
             //Generate E2EE Encrypted Password
             e2eeInfo1FA.e2ee_EncryptedPassword =
             [E2EECrypt e2eePassword:e2eeInfo1FA.pubKey randomKey:e2eeInfo1FA.randomKey session:e2eeInfo1FA.sessionID password:self.passwd1FA];
             
             
             if (e2eeInfo1FA.e2ee_EncryptedPassword != nil && ![e2eeInfo1FA.e2ee_EncryptedPassword isEqualToString:@""]) {
                 
                 
                 [self atpValidate1FA];
                 
                 
             } else {
                 if (self.error_message == nil || [self.error_message isEqualToString:@""]) {
                     self.error_message = @"";
                 }
                 
                 
             }
         }
         
         
         
     }];
}



- (void)atpValidate1FA {
    
    NSString *postStr = [NSString stringWithFormat:@"+=%@|,=%@|-=%@|.=%@", userName, self.passwd1FA,

                         self.e2eeInfo1FA.e2ee_EncryptedPassword, self.e2eeInfo1FA.randomKey];
    
    // is surely encrypted for all broker (10|%c%@|%@%cE indicator that encrypted)
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]ValidateOneFA?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[0]ValidateOneFA?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
    }
    

    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];

    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecompressionProcess:[self atpDecryptionProcess:content]];

         // NSLog(@"\n\n---- ValidateOneFA Returned Data ----\n%@\n\n", content);
         
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
         GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
         
         NSString *statusCode = @"";
         NSString *statusMsg = @"";
         NSString *errorMsg = @"";
         
         int statusIndex = -1;		//-
         int messageIndex = -1;      //.
         
         NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
         
         
         NSString *s;
         for (s in arr) {
             if ([errorReg1 matchesString:s] || [errorReg2 matchesString:s]) {
                 errorMsg = s;
             }
             else {
                 if ([metaregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@"/"]) {
                             statusIndex = pos;
                         }
                         else if ([token isEqualToString:@"0"]) {
                             messageIndex = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     
                     if (statusIndex != -1) {
                         statusCode = [tokens objectAtIndex:statusIndex];
                     }
                     if (messageIndex != -1) {
                         statusMsg = [tokens objectAtIndex:messageIndex];
                     }
                     if (![self.status_code isEqualToString:@"200"]) {
                         errorMsg = statusMsg;
                     }
                 }
             }
         }
        
         NSMutableDictionary *replyDict = [NSMutableDictionary dictionary];
         [replyDict setObject:statusCode forKey:@"statusCode"];
         [replyDict setObject:statusMsg forKey:@"statusMsg"];
         [replyDict setObject:errorMsg forKey:@"errorMsg"];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"atpValidate1FADone" object:self userInfo:replyDict];

    }];
    
    
}

// OTP Pin
-(void)validate2FAPinForDevice:(DeviceDataFor2FA*)dvData forTrade:(BOOL)isTrade {
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@|+=%@|,=%@|/=%@",
                         [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"],
                         dvData.deviceID,
                         dvData.otpPin,
                         dvData.icNo];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]ValidateOTP?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[0]ValidateOTP?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
    }
    
   // NSLog(@"validate2FAPinForDevice postStr %@", postStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecompressionProcess:[self atpDecryptionProcess:content]];
         
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
         GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
         
         int statusIndex = -1;		//-
         int messageIndex = -1;      //.
  
         
         NSString *statusCode = @"";
         NSString *statusMsg = @"";
         NSString *errorMsg = @"";
         
         
         NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
         
         NSString *s;
         for (s in arr) {
             if ([errorReg1 matchesString:s] || [errorReg2 matchesString:s]) {
                 self.error_message = s;
             }
             else {
                 if ([metaregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@"-"]) {
                             statusIndex = pos;
                         }
                         else if ([token isEqualToString:@"."]) {
                             messageIndex = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     
                     if (statusIndex != -1) {
                         statusCode = [tokens objectAtIndex:statusIndex];
                     }
                     if (messageIndex != -1) {
                         statusMsg = [tokens objectAtIndex:messageIndex];
                     }
                     if (![self.status_code isEqualToString:@"0"]) {
                         errorMsg = statusMsg;
                     }
                 }
             }
         }
         
         NSMutableDictionary *replyDict = [NSMutableDictionary dictionary];
         [replyDict setObject:statusCode forKey:@"statusCode"];
         [replyDict setObject:statusMsg forKey:@"statusMsg"];
         [replyDict setObject:errorMsg forKey:@"errorMsg"];
         if (isTrade) {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"validateTrade2FAPinForDeviceDone" object:self userInfo:replyDict];
         } else {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"validate2FAPinForDeviceDone" object:self userInfo:replyDict];
         }
     }];
}



#pragma mark - Request for SMS OTP

- (void) doRequestSMSOTP{
    
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@|+=%@|,=|1=0103|2=Y",   [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"], [[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:0]];
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]ValidateOTP?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[0]ValidateOTP?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
    }
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecompressionProcess:[self atpDecryptionProcess:content]];
         
         NSLog(@"content %@",content);
         
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
         GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
         
         int statusIndex = -1;        //-
         int messageIndex = -1;      //.
        int smsOTPKeyIndex = -1;
         
         NSString *statusCode = @"";
         NSString *statusMsg = @"";
         NSString *errorMsg = @"";
         NSString *messageSMSOTP = @"";
         
    
         NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
         
         NSString *s;
         for (s in arr) {
             NSLog(@"s %@",s);
             if ([errorReg1 matchesString:s] || [errorReg2 matchesString:s]) {
                 self.error_message = s;
             }
             else {
                 if ([metaregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@"-"]) {
                             statusIndex = pos;
                         }
                         else if ([token isEqualToString:@"."]) {
                             messageIndex = pos;
                         }else if([token isEqualToString:@"3"]) {
                             smsOTPKeyIndex = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     
                     if (statusIndex != -1) {
                         statusCode = [tokens objectAtIndex:statusIndex];
                     }
                     if (messageIndex != -1) {
                         statusMsg = [tokens objectAtIndex:messageIndex];
                     }
                     if (![statusCode isEqualToString:@"0"]) {
                         errorMsg = statusMsg;
                     }
                     if (smsOTPKeyIndex != -1) {
            
                         messageSMSOTP = [tokens objectAtIndex:smsOTPKeyIndex];
                        NSLog(@"messageSMSOTP %@",messageSMSOTP);
                       
                     }
                 }
             }
         }
         
         NSMutableDictionary *replyDict = [NSMutableDictionary dictionary];
         [replyDict setObject:statusCode forKey:@"statusCode"];
         [replyDict setObject:statusMsg forKey:@"statusMsg"];
         [replyDict setObject:errorMsg forKey:@"errorMsg"];
         [replyDict setObject:messageSMSOTP forKey:@"messageSMSOTP"];

         [[NSNotificationCenter defaultCenter] postNotificationName:@"requestSMSOTPPinForDeviceDone" object:self userInfo:replyDict];
       
     }];
 
}

- (void) doValidateSMSOTP:(NSString *)dvData andTrade:(BOOL)isTrade{
    
    
    NSString *postStr = [NSString stringWithFormat:@"'=%@|+=%@|,=%@|1=0103|2=N|4=%@",   [[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"], [[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:0],dvData,[UserPrefConstants singleton].smsOTPHeaderKey];
    
    
    NSLog(@"post Str %@",postStr);
    
    if ([UserPrefConstants singleton].HTTPSEnabled) {
        urlStr = [NSString stringWithFormat:@"https://%@/[0]ValidateOTP?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
        
    } else {
        urlStr = [NSString stringWithFormat:@"http://%@/[0]ValidateOTP?10|%c%@|%@%cE",
                  atpServer,
                  START_OF_TEXT,
                  [self base64Encode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[UserParam]"]],
                  [self AESEncryptAndBase64Encode:postStr withKey:userInfoEncryptionKey],
                  END_OF_TEXT];
    }
    
    NSLog(@"url Str %@",urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         content = [self atpDecompressionProcess:[self atpDecryptionProcess:content]];
         
         NSLog(@"content %@",content);
         
         GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
         GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
         GTMRegex *errorReg1 = [GTMRegex regexWithPattern:@"^Error.*"];
         GTMRegex *errorReg2 = [GTMRegex regexWithPattern:@"^ERROR.*"];
         
         int statusIndex = -1;        //-
         int messageIndex = -1;      //.
         int smsOTPKeyIndex = -1;
         
         NSString *statusCode = @"";
         NSString *statusMsg = @"";
         NSString *errorMsg = @"";
         NSString *messageSMSOTP = @"";
         
         
         NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
         
         NSString *s;
         for (s in arr) {
             NSLog(@"s %@",s);
             if ([errorReg1 matchesString:s] || [errorReg2 matchesString:s]) {
                 self.error_message = s;
             }
             else {
                 if ([metaregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     int pos = 0;
                     for (NSString *token in tokens) {
                         if ([token isEqualToString:@"-"]) {
                             statusIndex = pos;
                         }
                         else if ([token isEqualToString:@"."]) {
                             messageIndex = pos;
                         }else if([token isEqualToString:@"3"]) {
                             smsOTPKeyIndex = pos;
                         }
                         pos++;
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     
                     if (statusIndex != -1) {
                         statusCode = [tokens objectAtIndex:statusIndex];
                     }
                     if (messageIndex != -1) {
                         statusMsg = [tokens objectAtIndex:messageIndex];
                     }
                     if (![statusCode isEqualToString:@"0"]) {
                         errorMsg = statusMsg;
                     }
                     if (smsOTPKeyIndex != -1) {
                         
                         messageSMSOTP = [tokens objectAtIndex:smsOTPKeyIndex];
                         NSLog(@"messageSMSOTP %@",messageSMSOTP);
                         
                     }
                 }
             }
         }
         
         NSMutableDictionary *replyDict = [NSMutableDictionary dictionary];
         [replyDict setObject:statusCode forKey:@"statusCode"];
         [replyDict setObject:statusMsg forKey:@"statusMsg"];
         [replyDict setObject:errorMsg forKey:@"errorMsg"];
         [replyDict setObject:messageSMSOTP forKey:@"messageSMSOTP"];
         
         if (isTrade) {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"validateTrade2FAPinForDeviceDone" object:self userInfo:replyDict];
         } else {
             [[NSNotificationCenter defaultCenter] postNotificationName:@"validate2FAPinForDeviceDone" object:self userInfo:replyDict];
         }
         
     }];
 
}



#pragma mark - Check Single Logon (to autokick if user login at other device)
- (void)doCheckSingleLogon{
    NSString *postStr = [NSString stringWithFormat:@"+=%@", [UserPrefConstants singleton].userName];
    urlStr = [NSString stringWithFormat:@"http://%@/[%@]CheckSingleLogon?%@",
              atpServer,
              [[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"],
              postStr];
	
    self.status_code = @"0";
    self.message = @"";
	
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *responseData = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         NSString *content = [self atpDecryptionProcess:responseData];
         NSArray *arr = [content componentsSeparatedByString:@"\r\n"];
         //NSLog(@"doCheckSingleLogon %@", content);
         if ([arr count] < 2) {
             if (([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1116"].location != NSNotFound) ||
                 ([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1102"].location != NSNotFound) ||
                 ([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1201"].location != NSNotFound) ||
                 ([content rangeOfString:@"Error" options:NSCaseInsensitiveSearch].location != NSNotFound && [content rangeOfString:@"1100"].location != NSNotFound)) {
                 
                 self.message = content;
                 self.status_code = content;
             }
         }
         else {
             GTMRegex *metaregex = [GTMRegex regexWithPattern:@"\\(=.*"];
             GTMRegex *dataregex = [GTMRegex regexWithPattern:@"\\)=.*"];
             
             int message = -1;   //,
             int status = -1;    //-
             NSString *s;
             for (s in arr) {
                 if ([metaregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@"(=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if ([tokens count] >= 4 && [tokens count] < 60) {
                         int pos = 0;
                         for (NSString *token in tokens) {
                             if ([token isEqualToString:@","]) {
                                 message = pos;
                             }
                             else if ([token isEqualToString:@"-"]) {
                                 status = pos;
                             }
                             pos++;
                         }
                     }
                 }
                 else if ([dataregex matchesString:s]) {
                     s = [s stringByReplacingOccurrencesOfString:@")=" withString:@""];
                     NSArray *tokens = [s componentsSeparatedByString:@"|"];
                     if ([tokens count] >= 4 && [tokens count] < 60) {
                         if (message != -1) {
                             self.message = [tokens objectAtIndex:message];
                         }
                         if (status != -1) {
                             self.status_code = [tokens objectAtIndex:status];
                         }
                     }
                 }
             }
         }
         //NSLog(@"CheckSingleLogon Status Code: %@",self.status_code);
         
         if (![self.status_code isEqualToString:@"0"]) {
             
             NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
             [notificationData setObject:self.message forKey:@"message"];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"doKickout" object:self userInfo:notificationData];
         }
     }];
}

#pragma mark - Main for Mobile Function
#pragma mark - MOBILE FUNCTIONS
- (void)startLoginBy:(NSString *)userName password:(NSString *)password progress:(AUProgressHandler)progressHandler completion:(AUCompletionHandler)completionHandler{
    [[LogManager shared] writeWithMethodName:[NSString stringWithFormat:@"[%@:%d] %s",NSStringFromClass([self class]), __LINE__, __FUNCTION__] content:@""];
	[UserPrefConstants singleton].userName = userName;
	[UserPrefConstants singleton].userPassword = password;
	if ([UserPrefConstants singleton].ATPLoadBalance) {
		DLog(@"1111: atpGetPK_LoadBalance")
		[self atpGetPK_LoadBalance];
	} else {
		DLog(@"1111: atpGetPK")
		[self atpGetPK];
	}
	self.loginProgressHandler = progressHandler;
	self.loginCompletionHandler = completionHandler;
	progressHandler = nil;
	completionHandler = nil;
}

#pragma mark - Watchlist
- (void)getWatchlistWithCompletion:(AUCompletionHandler)handler{
	[self getWatchList];
	self.watchlistCompletion = handler;
	handler = nil;
}

- (void)addOrUpdateWatchlist:(NSInteger)watchlistID name:(NSString *)name completion:(AUCompletionHandler)handler{
	[self addOrUpdateWatchlistName:(int)watchlistID withFavName:name];
	self.addOrUpdateWatchlistCompletion = handler;
	handler = nil;
}

//===== +++ Delete watchlis with completion =====
- (void)deleteWatchlist:(NSInteger)wlID completion:(AUCompletionHandler)handler{
	[self deleteWatchlist:(int)wlID];
	self.deleteWatchlistCompletion = handler;
	handler = nil;
}

- (void)addStock:(NSString *)stockCode toWatchlist:(int)watchlistID completion:(AUCompletionHandler)handler{
	[self addWatchlistCheckItem:watchlistID withItem:stockCode];
	self.addStockToWatchlistCompletion = handler;
	handler = nil;
}

- (void)removeStock:(NSString *)stockCode fromWatchlist:(NSInteger)watchlistID completion:(AUCompletionHandler)handler{
	[self deleteWatchlistItem:(int)watchlistID withItem:stockCode];
	self.removeStockFromWatchlistCompletion = handler;
	handler = nil;
}
#pragma mark - Register

-(void) doRegister:(RegisterModel*)theNewUser completionHander:(AUCompletionHandler)theHander
{
//    NSString *postStr = [NSString stringWithFormat:@"+=%@", [UserPrefConstants singleton].userName];
     urlStr = [NSString stringWithFormat:@"http://plc.asiaebroker.com/gcN2N/acctReg.jsp?/email=%@&mobile=%@&loginID=%@&pwd=%@&txtConfirmed=%@&hintType=%@&hint=%@&hintAns=%@&&cliName=%@&icNo=%@", [self base64Encode:theNewUser.email], [self base64Encode:theNewUser.mobileNo], [self base64Encode:theNewUser.username], [self base64Encode:theNewUser.password], [self base64Encode:theNewUser.retypePassword], [self base64Encode:theNewUser.hintType], [self base64Encode:theNewUser.hint], [self base64Encode:theNewUser.answer],[self base64Encode:theNewUser.name],[self base64Encode:theNewUser.icNo]];
    
    
    self.status_code = @"0";
    self.message = @"";
    
    //NSLog(@"doCheckSingleLogon %@",urlStr);
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    urlRequest.timeoutInterval = kURLRequestTimeout;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString *responseData = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
         NSString *content = [self atpDecryptionProcess:responseData];
         if ([content containsString:@"|tradeStatus=S|"]) {
             theHander(YES,responseData,error);
         } else if ([content containsString:@"|tradeStatus=F|"]){
             theHander(NO,responseData,error);
         }
     }];
}

#pragma mark - Parse ExchangeList

@end

