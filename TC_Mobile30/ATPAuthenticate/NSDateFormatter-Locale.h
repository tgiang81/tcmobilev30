//
//  NSDateFormatter-Locale.h
//  TCiPhone_CIMB
//
//  Created by Don lee on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter(Locale)

- (id)initWithSafeLocale;

@end
