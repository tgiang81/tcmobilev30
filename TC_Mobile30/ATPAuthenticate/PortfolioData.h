//
//  PortfolioData.h
//  N2NTrader
//
//  Created by Adrian Lo on 3/21/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCBaseModel.h"

@interface PortfolioData : TCBaseModel {
    NSString *stockcode;
	NSString *stockname;
	NSString *stockcode_cn;

	NSString *price_high;
	NSString *price_low;
	NSString *price_year_high;
	NSString *price_year_low;
	
	NSString *exchange_ref_number;		//CDS Code
	NSString *client_account;			//BHCLICode
	NSString *branch_code;
	
	NSString *brokerage;
	
	NSString *quantity_available;
	NSString *quantity_on_hand;
	
	NSString *total_price_P;
	NSString *total_price_B;
	NSString *total_quantity_P;
	NSString *total_comm_P;
	
	NSString *sell_qty_in_proc;
	NSString *total_price_S;

	NSString *rms_code;
	
	NSString *price_last_done;
	NSString *price_ref;
	
	NSString *price_AS;
	NSString *price_AP;
	
	NSString *volume;
	NSString *lot_size;
	
	NSString *change_amount;
	NSString *change_percentage;
	
	NSString *sector_code;
	NSString *stock_class;
	NSString *stock_status;
	
	NSString *price_base;
	NSString *price_day_high;
	NSString *price_day_low;	
	NSString *price_avg_purchase;  //Average BP
	
	NSString *limit_upper;
	NSString *limit_lower;
	
	NSString *gross_market_value;
	
	NSString *unrealized_gain_loss_amount;
	NSString *unrealized_gain_loss_percentage;
	
	NSString *total_value_P;
	NSString *total_value_S;
	NSString *total_quantity_S;
	
	NSString *total_brokerage;
	NSString *gain_loss;
	
	NSString *aggregated_buy_price;
	NSString *aggregated_sell_price;
	
	NSString *total_quantity_short;
	NSString *total_comm_S;
	
	NSString *currency;
    NSString *settlement_mode;

	NSString *realized_gain_loss_amount;
	NSString *realized_gain_loss_percentage;
    NSString *qty_sold;
}
@property (nonatomic, retain) NSString *stockcode;
@property (nonatomic, retain) NSString *stockname;
@property (nonatomic, retain) NSString *stockcode_cn;
@property (nonatomic, retain) NSString *price_high;
@property (nonatomic, retain) NSString *price_low;
@property (nonatomic, retain) NSString *price_year_high;
@property (nonatomic, retain) NSString *price_year_low;
@property (nonatomic, retain) NSString *exchange_ref_number;		//CDS Code
@property (nonatomic, retain) NSString *client_account;			//BHCLICode
@property (nonatomic, retain) NSString *branch_code;
@property (nonatomic, retain) NSString *brokerage;
@property (nonatomic, retain) NSString *quantity_available;
@property (nonatomic, retain) NSString *quantity_on_hand;
@property (nonatomic, retain) NSString *total_price_P;
@property (nonatomic, retain) NSString *total_price_B;
@property (nonatomic, retain) NSString *total_quantity_P;
@property (nonatomic, retain) NSString *total_comm_P;
@property (nonatomic, retain) NSString *sell_qty_in_proc;
@property (nonatomic, retain) NSString *total_price_S;
@property (nonatomic, retain) NSString *rms_code;
@property (nonatomic, retain) NSString *price_last_done;
@property (nonatomic, retain) NSString *price_ref;
@property (nonatomic, retain) NSString *price_AS;
@property (nonatomic, retain) NSString *price_AP;
@property (nonatomic, retain) NSString *volume;
@property (nonatomic, retain) NSString *lot_size;
@property (nonatomic, retain) NSString *change_amount;
@property (nonatomic, retain) NSString *change_percentage;
@property (nonatomic, retain) NSString *sector_code;
@property (nonatomic, retain) NSString *stock_class;
@property (nonatomic, retain) NSString *stock_status;
@property (nonatomic, retain) NSString *price_base;
@property (nonatomic, retain) NSString *price_day_high;
@property (nonatomic, retain) NSString *price_day_low;	
@property (nonatomic, retain) NSString *price_avg_purchase;
@property (nonatomic, retain) NSString *limit_upper;
@property (nonatomic, retain) NSString *limit_lower;
@property (nonatomic, retain) NSString *gross_market_value;
@property (nonatomic, retain) NSString *unrealized_gain_loss_amount;
@property (nonatomic, retain) NSString *unrealized_gain_loss_percentage;
@property (nonatomic, retain) NSString *total_value_P;
@property (nonatomic, retain) NSString *total_value_S;
@property (nonatomic, retain) NSString *total_quantity_S;
@property (nonatomic, retain) NSString *total_brokerage;
@property (nonatomic, retain) NSString *gain_loss;
@property (nonatomic, retain) NSString *aggregated_buy_price;
@property (nonatomic, retain) NSString *aggregated_sell_price;
@property (nonatomic, retain) NSString *total_quantity_short;
@property (nonatomic, retain) NSString *total_comm_S;
@property (nonatomic, retain) NSString *currency;
@property (nonatomic, retain) NSString *settlement_mode;
@property (nonatomic, retain) NSString *realized_gain_loss_amount;
@property (nonatomic, retain) NSString *realized_gain_loss_percentage;
@property (nonatomic, retain) NSString *qty_sold;
@property (nonatomic, retain) NSString * bought;
@property (nonatomic, retain) NSString * sold ;
@property (nonatomic, retain) NSString * unsettled_bought_t1 ;
@property (nonatomic, retain) NSString * unsettled_sold_t1 ;
@property (nonatomic, retain) NSString * settled_bought_t2 ;
@property (nonatomic, retain) NSString * settled_sold_t2  ;
@property (nonatomic, retain) NSString * pledge ;
@property (nonatomic, retain) NSString * rights ;
@property (nonatomic, retain) NSString * registered ;
@property (nonatomic, retain) NSString * cash_dividend ;
@end
