//
//  TradeStatus.m
//  N2NTrader
//
//  Created by Adrian Lo on 3/4/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import "TradeStatus.h"
#import "NSDate+Utilities.h"
#import "ThemeManager.h"
#import "NSDate+Utilities.h"
#import "AppControl.h"
#import "TC_Calculation.h"
#import "LanguageManager.h"
@implementation TradeStatus
@synthesize account_number, client_code, broker_code, sender_code, exchg_code, stock_name, stock_code, order_number, order_time, guid, fix_order_number, action, order_src, order_type, validity, sett_currency, payment_type, order_quantity, cancelled_quantity, unmt_quantity, mt_quantity, min_quantity, disclosed_quantity, trigger_price, mt_price, expiry, triggerType, triggerDirection, mt_value, status, status_text, message, lot_size, last_update;
@synthesize compulsaryField;

- (id) init {
	self = [super init];
	if (self) {
		account_number = @"";
        client_code = @"";
		broker_code = @"";
		sender_code = @"";
        exchg_code = @"";
		stock_name = @"";
		stock_code = @"";
        order_number = @"";
		order_time = @"";
        guid = @"";
        fix_order_number = @"";
		action = @"";
        order_src = @"";
		order_type = @"";
		validity = @"";
		sett_currency = @"";
        payment_type = @"";
        order_quantity = @"";
        cancelled_quantity = @"";
        unmt_quantity = @"";
        mt_quantity = @"";
        min_quantity = @"";
        disclosed_quantity = @"";
        order_price = @"";
        trigger_price = @"";
        mt_price = @"";
        expiry = @"";
        triggerType = @"";
        triggerDirection = @"";
        mt_value = @"";
        status = @"";
		status_text = @"";
		message = @"";
		lot_size = @"";
        last_update = @"";
        compulsaryField = [[NSMutableDictionary alloc] init];
	}
	return self;
}
- (NSString *)dateStringToCompare{
    return [self getServerDateToCompare:self.order_time];
}
- (NSString *)getServerDateToCompare:(NSString *)svDate{
    NSDate *date = [NSDate dateFromString:svDate format:kSERVER_FORMAT_ORDERBOOK_DATE];
    return [date stringWithFormat:@"yyyyMMdd"];
}
-(void) reset
{
    account_number = @"";
    client_code = @"";
    broker_code = @"";
    sender_code = @"";
    exchg_code = @"";
    stock_name = @"";
    stock_code = @"";
    order_number = @"";
    order_time = @"";
    guid = @"";
    fix_order_number = @"";
    action = @"";
    order_src = @"";
    order_type = @"";
    validity = @"";
    sett_currency = @"";
    payment_type = @"";
    order_quantity = @"";
    cancelled_quantity = @"";
    unmt_quantity = @"";
    mt_quantity = @"";
    min_quantity = @"";
    disclosed_quantity = @"";
    order_price = @"";
    trigger_price = @"";
    mt_price = @"";
    expiry = @"";
    triggerType = @"";
    triggerDirection = @"";
    mt_value = @"";
    status = @"";
    status_text = @"";
    message = @"";
    lot_size = @"";
    last_update = @"";

    [compulsaryField removeAllObjects];
}
- (NSString *)getDate{
    return [NSString stringWithFormat:@"%@-%@-%@\n%@:%@:%@", [self.last_update substringWithRange:NSMakeRange(0, 4)], [self.last_update substringWithRange:NSMakeRange(4, 2)], [self.last_update substringWithRange:NSMakeRange(6, 2)],[self.last_update substringWithRange:NSMakeRange(8, 2)],[self.last_update substringWithRange:NSMakeRange(10, 2)],[self.last_update substringWithRange:NSMakeRange(12, 2)]];
}
- (NSString *)getQtyBg{
    if ([self.action isEqualToString:@"Buy"]) {
        return [ThemeManager shareInstance].trade_BuyBg;
    }
    else {
        return[ThemeManager shareInstance].trade_SellBg;
    }
}

- (void)setOrder_price:(NSString *)order_price{
    _order_price = order_price;
    self.total = [TC_Calculation getPriceWithStockLotSize:-1 settcurrency:nil stkPrice:[order_price doubleValue] stockCurrency:@"" quantity:[self.order_quantity longLongValue]];
}
- (NSString *)getOrderSource{
    
    if ([self.order_src isEqualToString:@"I"]) {
        return [LanguageManager stringForKey:@"Internet Retail"];
    }
    else if ([self.order_src isEqualToString:@"D"]) {
        return [LanguageManager stringForKey:@"Dealer"];
    }
    else if ([self.order_src isEqualToString:@"W"]) {
        return [LanguageManager stringForKey:@"Wap, Mobile"];
    }
    else if ([self.order_src isEqualToString:@"S"]) {
        return [LanguageManager stringForKey:@"SMS"];
    }
    else if ([self.order_src isEqualToString:@"P"]) {
        return [LanguageManager stringForKey:@"Phone"];
    }
    else if ([self.order_src isEqualToString:@"F"]) {
        return [LanguageManager stringForKey:@"Fix"];
    }
    return @"-";
}
- (NSString *)statusMode{
    if(_isQtyMode){
        return self.order_quantity;
    }else{
        NSString *status_abr = @"";
        NSString *action_abr = @"";
        if ([self.status isEqualToString:@"0"]) {
            status_abr = @"Q";
        }
        else if ([self.status isEqualToString:@"1"]) {
            status_abr = @"PFL";
        }
        else if ([self.status isEqualToString:@"2"]) {
            status_abr = @"FL";
        }
        else if ([self.status isEqualToString:@"3"]) {
            status_abr = @"DN";
        }
        else if ([self.status isEqualToString:@"4"]) {
            status_abr = @"CN";
        }
        else if ([self.status isEqualToString:@"5"]) {
            status_abr = @"RP";
        }
        else if ([self.status isEqualToString:@"6"]) {
            status_abr = @"PCN";
        }
        else if ([self.status isEqualToString:@"7"]) {
            status_abr = @"PRL";
        }
        else if ([self.status isEqualToString:@"8"]) {
            status_abr = @"RJ";
        }
        else if ([self.status isEqualToString:@"9"]) {
            status_abr = @"SPS";
        }
        else if ([self.status isEqualToString:@"A"]) {
            status_abr = @"PQ";
        }
        else if ([self.status isEqualToString:@"B"]) {
            status_abr = @"CAL";
        }
        else if ([self.status isEqualToString:@"C"]) {
            status_abr = @"EXP";
        }
        else if ([self.status isEqualToString:@"D"]) {
            status_abr = @"AFB";
        }
        else if ([self.status isEqualToString:@"E"]) {
            status_abr = @"PRP";
        }
        
        if ([self.action isEqualToString:@"Buy"]) {
            
            action_abr = @"B";
            
        }
        else {
            action_abr = @"S";
            
        }
        return [NSString stringWithFormat:@"%@-%@", action_abr, status_abr];
    }
}

@end
