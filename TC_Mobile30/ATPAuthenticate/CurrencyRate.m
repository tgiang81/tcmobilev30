//
//  CurrencyRate.m
//  TCiPhone_CIMB
//
//  Created by Don Lee on 12/28/12.
//
//

#import "CurrencyRate.h"

@implementation CurrencyRate
@synthesize currency, denomination, buy_rate, sell_rate;

- (id) init {
    self = [super init];
    if (self) {
    }
    return self;
}

// to enable NSLog to work on array of this object (this is cool trick).
-(NSString *)description{
    
    return [NSString stringWithFormat:@"<CurrencyRate>currency: %@, denomination: %d, buy_rate: %f, sell_rate: %f",
    currency, denomination, buy_rate, sell_rate];
}


@end
