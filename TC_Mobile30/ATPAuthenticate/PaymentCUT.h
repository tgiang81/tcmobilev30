//
//  PaymentCUT.h
//  TCiPhone_CIMB_SG_UAT
//
//  Created by Scott Thoo on 4/9/15.
//
//

#import <Foundation/Foundation.h>

@interface PaymentCUT : NSObject
{
    NSString *paymentCountry;
    NSString *AccountType;
    NSArray *paymentOptions;
    NSString *paymentSettings;
}

@property (nonatomic, retain) NSString *paymentCountry;
@property (nonatomic, retain) NSString *AccountType;
@property (nonatomic, retain) NSArray *paymentOptions;
@property (nonatomic, retain) NSString *paymentSettings;

-(NSString*)description;

@end
