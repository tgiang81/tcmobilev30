//
//  PrtfSummRptData.m
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 2/28/14.
//
//

#import "PrtfSummRptData.h"

@implementation PrtfSummRptData
@synthesize open_long, open_short, bf_cash_balance, deposit, withdrawal, eligible_collateral, initial_margin, maintenance_margin, margin_call, realized_pl, current_balance, total_unrealized_pl, total_gross_buy, total_gross_sell;

- (id) init {
    self = [super init];
    if (self) {
    }
    return self;
}


@end
