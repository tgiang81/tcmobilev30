//
//  ASIHttpGeneric.h
//  N2NTrader
//
//  Created by Adrian Lo on 3/31/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ApplicationData.h"
//#import "ASIHTTPRequest.h"

#define START_OF_TEXT 2
#define END_OF_TEXT 3

@interface ASIHttpGeneric : NSObject {
    NSString *urlStr;
    //	__block ASIHTTPRequest *conn;
    
    //	ApplicationData *appData;
    
    NSString *QCserver;
    // NSString *ATPserver;
    NSString *ATPAdminKey;
    NSString *chartServer;
    NSString *registerURL;
    
    int timeout;
    BOOL isEncryption;
    BOOL isE2EE_Encryption;
    BOOL isATPCompression;
    BOOL isQCCompression;
    BOOL isJavaQC;
    BOOL connerror;
    NSString *error_message;
    NSMutableArray *resultList;
    NSString *userInfoEncryptionKey;
    
}
- (void)stop;
- (BOOL)isRunning;
- (void)reloadQC;
- (NSString *)encryptString:(NSString *)str;
- (NSString *)hexStringFromBytes:(NSData *)data forURL:(BOOL)appendPerc;
- (NSData *)bytesFromHexString:(NSString *)aString;
- (NSString *)base64Encode:(NSString *)str;
- (NSData *)base64Decode:(NSString *)str;
- (NSString *)generateAESKey;
- (NSString *)getHashEncryptionKey:(NSString *)key;
- (NSData *)getDoubleHashEncryptionKey:(NSString *)key;
- (NSString *)AESEncryptAndBase64Encode:(NSString *)str withKey:(NSString *)key;
- (NSString *)Base64DecodeAndAESDecrypt:(NSString *)str withKey:(NSString *)key;
- (NSString *)getATPEncryptedStr:(NSString *)atpResponseStr;
- (NSString *)atpDecryptionProcess:(NSString *)atpResponseStr;
- (NSString *)atpDecryptionProcess:(NSString *)atpResponseStr currentUserInfoEncryptionKey:(NSString *)currentUserInfoEncryptionKey;
- (NSString *)atpDecompressionProcess:(NSString *)atpResponseStr;
- (NSString *)qcDecompressionProcess:(NSString *)qcResponseStr;
- (NSString *)rsaEncryptData:(NSString *)plainText withModulus:(NSString *)modulus andExponent:(NSString *)exponent andTag:(NSString *)tag;
- (NSString *)rsaEncryptData:(NSString *)plainText withCertContent:(NSString *)certContent andTag:(NSString *)tag andExponent:(NSString *)exponent;
- (void)removeKeyRefWithTag:(NSString *)tag;
- (BOOL) isBitOnBase64:(NSString *)base64EncodedStr forBitIndex:(int)bitIdx;
- (NSString *) generateTimeStamp;
- (NSString *) generateTimeStampURLEncoded;
- (NSString *)numberFormatStockPriceForOrderSubmission:(double)input forExchange:(NSString *)exchg;
@property (nonatomic, retain) NSString *ATPserver;
@property (nonatomic, assign) BOOL isEncryption;
@property (nonatomic, assign) BOOL isE2EE_Encryption;
@property (nonatomic, assign) BOOL isATPCompression;
@property (nonatomic, assign) BOOL isQCCompression;
@property (nonatomic, assign) BOOL isJavaQC;
@property (nonatomic, assign) BOOL connerror;
@property (nonatomic, retain) NSString *error_message;
@property (nonatomic, assign) NSMutableArray *resultList;
@property (nonatomic, retain) NSString *userInfoEncryptionKey;
//@property (nonatomic, assign) ApplicationData *appData;


#pragma mark - Class Functions
//+ (NSString *)base64Encode:(NSString *)str;
//+ (NSData *)base64Decode:(NSString *)str;

@end
