//
//  NSDateFormatter-Locale.m
//  TCiPhone_CIMB
//
//  Created by Don lee on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSDateFormatter-Locale.h"

@implementation NSDateFormatter(Locale)

- (id)initWithSafeLocale {
    static NSLocale *enUSPOSIXLocale = nil;
    self = [self init];
    if (enUSPOSIXLocale == nil) {
        enUSPOSIXLocale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease];
    }
    [self setLocale:enUSPOSIXLocale];
    ////NSLog(@"Category's object: %@ and object's locale: %@", self.description, [self.locale localeIdentifier]);
    return self;    
}

@end
