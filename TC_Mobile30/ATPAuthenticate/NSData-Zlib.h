//
//  NSData-Zlib.h
//  TCiPhone_CIMB
//
//  Created by Don lee on 6/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSData(Zlib) 

- (NSData *)zlibInflate;
- (NSData *)zlibDeflate;

@end
