//
//  AdditionalPaymentData.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 1/8/13.
//
//

#import <Foundation/Foundation.h>

@interface AdditionalPaymentData : NSObject {
    NSString *payment_name;
    BOOL isAllowedForBuyAction;
    BOOL isAllowedForSellAction;
}
@property (nonatomic, retain) NSString *payment_name;
@property (nonatomic, assign) BOOL isAllowedForBuyAction;
@property (nonatomic, assign) BOOL isAllowedForSellAction;
@end
