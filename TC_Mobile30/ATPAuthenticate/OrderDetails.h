//
//  OrderDetails.h
//  N2NTrader
//
//  Created by Adrian Lo on 1/15/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OrderDetails : NSObject {
	
    NSString *ticket_no;
    
//	I
//	D
//	W
//	S
//	P
//	F
	NSString *order_source;
    
//	Buy
//	Sell
//	Cancel
//	Revise
	NSString *order_action;
    
    NSString *ticker;
	NSString *company_name;
	NSString *stock_code;
	
//	Limit
//	Market
//	Stop
//	StopLimit
//	MarketToLimit
	NSString *order_type;
    
//	Day
//	GTC
//	OPG
//	FAK
//	FOK
//	GTX
//	GTD
//	Session
	NSString *validity;
	
    NSString *currency;
    NSString *payment_type;
    
  
    NSString *expiry;
    NSString *triggerType;
    NSString *triggerDirection;
	
    NSString *pin;
    
    NSString *otpPin;
    NSString *deviceID;
    
    BOOL shortSellState;
    BOOL fullOrder;
    
    NSMutableDictionary *compulsaryField;
}

@property (nonatomic, strong) NSString *ticket_no;
@property (nonatomic, strong) NSString *order_source;
@property (nonatomic, strong) NSString *order_action;
@property (nonatomic, strong) NSString *ticker;
@property (nonatomic, strong) NSString *company_name;
@property (nonatomic, strong) NSString *stock_code;
@property (nonatomic, strong) NSString *order_type;
@property (nonatomic, strong) NSString *validity;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *payment_type;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *trigger_price;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *minQuantity;
@property (nonatomic, strong) NSString *disclosedQuantity;
@property (nonatomic, strong) NSString *expiry;
@property (nonatomic, strong) NSString *triggerType;
@property (nonatomic, strong) NSString *triggerDirection;
@property (nonatomic, strong) NSString *pin;
@property (nonatomic, strong) NSString *otpPin;
@property (nonatomic, strong) NSString *deviceID;
@property (nonatomic) BOOL shortSellState;
@property (nonatomic) BOOL fullOrder;
@property (nonatomic, strong) NSMutableDictionary *compulsaryField;

-(void) reset;
@end
