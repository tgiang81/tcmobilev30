//
//  OrderControl.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 11/9/12.
//
//

#import <Foundation/Foundation.h>

@interface OrderControl : NSObject {
    NSString *orderType;
    NSMutableArray *validityList;
    NSMutableArray *enable_fields;
    NSMutableArray *disclosed_validityList;
    NSMutableArray *min_validityList;
}
@property (nonatomic, retain) NSString *orderType;
@property (nonatomic, retain) NSMutableArray *validityList;
@property (nonatomic, retain) NSMutableArray *enable_fields;
@property (nonatomic, retain) NSMutableArray *disclosed_validityList;
@property (nonatomic, retain) NSMutableArray *min_validityList;
@end
