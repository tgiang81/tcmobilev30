//
//  ASIHttpGeneric.m
//  N2NTrader
//
//  Created by Adrian Lo on 3/31/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import "ASIHttpGeneric.h"
//#import "AppConfigManager.h"
//#import "N2NTraderAppDelegate.h"
#import "NSString-Hash.h"
#import "NSData-AES.h"
#import "Base64.h"
#import "RSACrypt.h"
#import "NSData-Zlib.h"
#import "NSDateFormatter-Locale.h"

@implementation ASIHttpGeneric
@synthesize ATPserver, resultList, connerror, error_message, isEncryption, isE2EE_Encryption, isATPCompression, userInfoEncryptionKey,
isQCCompression, isJavaQC;

- (id) init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void) dealloc {
    
    ////NSLog(@"ASIHttpGeneric dealloc");
    [error_message release];
    if (registerURL != nil) {
        [registerURL release];
    }
    if (chartServer != nil) {
        [chartServer release];
    }
    if (QCserver != nil) {
        [QCserver release];
    }
    
    [ATPserver release];
    if (ATPAdminKey != nil) {
        [ATPAdminKey release];
    }
    if (resultList != nil) {
        [resultList release];
        resultList = nil;
    }
    //	if (conn != nil) {
    //		[conn cancel];
    //		[conn release];
    //		conn = nil;
    //	}
    
    [super dealloc];
}

- (void) stop {
    //	if (conn != nil) {
    //		[conn clearDelegatesAndCancel];
    //		[conn release];
    //		conn = nil;
    //	}
}

- (BOOL) isRunning {
    //	if (conn) {
    //		return YES;
    //	}
    //	else {
    return NO;
    //	}
}

- (void) reloadQC {
    //	if (QCserver != nil) {
    //		[QCserver release];
    //		QCserver = nil;
    //	}
    //	QCserver = [[AppConfigManager getQCServer] retain];
}
/*
 - (NSString *)encryptString:(NSString *)str {
	
 NSString *result = @"";
 const char *s = [str cStringUsingEncoding:NSISOLatin1StringEncoding];
	int length = [str length];
	int cl = length / 2 + length % 2;
	
 for (int i = 0; i < cl; i++)
 {
 int s1 = (int)s[i];
 int s2;
 
 if (cl + i > length - 1) {
 s2 = 0;
 }
 else {
 s2 = (int)s[cl + i];
 }
 
 int c1 = ((s1 & 0xf0) | (s2 & 0xf)) ^ 0xf8;
 int c2 = ((s2 & 0xf0) | (s1 & 0xf)) ^ 0xf8;
 
 char *tmp = (char *) malloc(3);
 tmp[0] = (char)c1;
 tmp[1] = (char)c2;
 tmp[2] = '\0';
 free(tmp);
 result = [NSString stringWithFormat:@"%@%@", result, [NSString stringWithCString:tmp encoding:NSISOLatin1StringEncoding]];
	}
	return result;
 }
 */

- (NSString *)encryptString:(NSString *)str
{
    NSString *result = @"";
    long length = [str length];
    long cl = length / 2 + length % 2;
    
    for (int i=0; i<cl; i++) {
        int s1 = [str characterAtIndex:i];
        int s2;
        
        if (cl+i > length-1) {
            s2 = 0;
        }
        else {
            s2 = [str characterAtIndex:cl+i];
        }
        
        int c1 = ((s1 & 0xf0) | (s2 & 0xf))^0xf8;
        int c2 = ((s2 & 0xf0) | (s1 & 0xf))^0xf8;
        
        result = [NSString stringWithFormat:@"%@%X", result, c1];
        result = [NSString stringWithFormat:@"%@%X", result, c2];
    }
    NSString *finalResult = [[[NSString alloc] initWithData:[self bytesFromHexString:result] encoding:NSISOLatin1StringEncoding] autorelease];
    
    return finalResult;
}

- (NSString *)hexStringFromBytes:(NSData *)data forURL:(BOOL)appendPerc
{
    NSString *sbResult = @"";
    int i;
    int idata;
    Byte *byteData = (Byte*)malloc([data length]);
    memcpy(byteData, [data bytes], [data length]);
    for (i=0; i<[data length]; i++) {
        if (appendPerc) {
            sbResult = [NSString stringWithFormat:@"%@%@", sbResult, @"%"];
        }
        idata = (int) byteData[i];
        if ((idata & 0xff) < 0x10) {
            sbResult = [NSString stringWithFormat:@"%@%i", sbResult, 0];
        }
        idata = idata & 0xff;
        sbResult = [NSString stringWithFormat:@"%@%x", sbResult, idata];
    }
    free(byteData);
    
    return sbResult;
}

- (NSData*)bytesFromHexString:(NSString *)aString
{
    // NSString *theString = [[aString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString:nil];
    
    NSString *theString = [[aString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString:@""];
    
    NSMutableData* data = [NSMutableData data];
    int idx;
    for (idx = 0; idx+2 <= theString.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [theString substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        if ([scanner scanHexInt:&intValue])
            [data appendBytes:&intValue length:1];
    }
    return data;
}

//Base64 Encode & Decode
- (NSString *)base64Encode:(NSString *)str {
    [Base64 initialize];
    NSString *b64encodedStr = [Base64 encode:[str dataUsingEncoding:NSISOLatin1StringEncoding]];
    
    return b64encodedStr;
}

- (NSData *)base64Decode:(NSString *)str {
    [Base64 initialize];
    NSData *b64decodedData = [Base64 decode:str];
    return b64decodedData;
}

//Generate Random AES Encryption Key
- (NSString *)generateAESKey {
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = [(NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidObject) autorelease];
    CFRelease(uuidObject);
    NSLog(@"uuidStr:%@", uuidStr);
    return uuidStr;
}

//SHA256 Hash
- (NSString *)getHashEncryptionKey:(NSString *)key
{
    NSString *hashKey = [key sha256HexDigest];
    NSData *hashKeyData = [self bytesFromHexString:hashKey];
    NSString *hashKeyStr = [[[NSString alloc] initWithData:hashKeyData encoding:NSISOLatin1StringEncoding] autorelease];
    
    return hashKeyStr;
}

- (NSData *)getDoubleHashEncryptionKey:(NSString *)key
{
    NSString *doubleHashKey = [[key sha256HexDigest] sha256HexDigest];
    NSData *doubleHashKeyData = [self bytesFromHexString:doubleHashKey];
    
    //For comparison between ATP double hash key and My double hash key
    NSString *doubleHashKeyStr = [[NSString alloc] initWithData:doubleHashKeyData encoding:NSISOLatin1StringEncoding];
    // NSString *b64DoubleHashKeyStr = [self base64Encode:doubleHashKeyStr];
    ////NSLog(@"Double Hash Key:%@", b64DoubleHashKeyStr);
    // [doubleHashKeyStr release];
    
    return doubleHashKeyData;
}

//AES 256 Encryption
- (NSString *)AESEncryptAndBase64Encode:(NSString *)str withKey:(NSString *)key
{
    NSData *encryptedData = [[str dataUsingEncoding:NSISOLatin1StringEncoding] AES256EncryptWithKey:[self getHashEncryptionKey:key]];
    
    [Base64 initialize];
    NSString *encoded_encryptedStr = [Base64 encode:encryptedData];
    
    return encoded_encryptedStr;
}

- (NSString *)Base64DecodeAndAESDecrypt:(NSString *)str withKey:(NSString *)key
{
    [Base64 initialize];
    NSData *decodedData = [Base64 decode:str];
    
    NSData *decrypted_decodedData = [decodedData AES256DecryptWithKey:[self getHashEncryptionKey:key]];
    NSString *decrypted_decodedStr = [[[NSString alloc] initWithData:decrypted_decodedData encoding:NSISOLatin1StringEncoding] autorelease];
    
    return decrypted_decodedStr;
}

- (NSString *)getATPEncryptedStr:(NSString *)atpResponseStr
{
    atpResponseStr = [atpResponseStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"10|%c", START_OF_TEXT] withString:@""];
    
    if ([atpResponseStr rangeOfString:[NSString stringWithFormat:@"%cE", END_OF_TEXT]].location != NSNotFound) {
        atpResponseStr = [atpResponseStr stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%cE", END_OF_TEXT] withString:@""];
    }
    
    return atpResponseStr;
}

- (NSString *)atpDecryptionProcess:(NSString *)atpResponseStr currentUserInfoEncryptionKey:(NSString *)currentUserInfoEncryptionKey{
    NSRange range = [atpResponseStr rangeOfString:[NSString stringWithFormat:@"10|%c", START_OF_TEXT]];
    if (range.location != NSNotFound) {
        int index = range.location;
        NSString *encryptedStr = [self getATPEncryptedStr:[atpResponseStr substringFromIndex:index]];
        return [NSString stringWithFormat:@"%@%@", [atpResponseStr substringToIndex:index], [self Base64DecodeAndAESDecrypt:encryptedStr withKey:currentUserInfoEncryptionKey]];
    }
    return atpResponseStr;
}


- (NSString *)atpDecryptionProcess:(NSString *)atpResponseStr {
    return [self atpDecryptionProcess:atpResponseStr currentUserInfoEncryptionKey:userInfoEncryptionKey];
}

//Zlib Decompression
//The compressed data will be formatted as "#13 + #129 + LengthOfCompressedText + #30 + CompressedText + #10"

/*---------------------- ATP Decompression -----------------------*/
//During ATP TradeLogin Process, if you pass in "PullCompress=2", you are required to decompress data returned from TradeClient &TradeStatus
//Decompress Steps = Get data in between #30 and #10 ----> Base64 decode ----> zlib decompress
- (NSString *)atpDecompressionProcess:(NSString *)atpResponseStr {
    
    if (!isATPCompression) {
        NSRange range1 = [atpResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)13]];
        NSRange range2 = [atpResponseStr rangeOfString:[NSString stringWithFormat:@"%C", (unichar)129]];
        NSRange range3 = [atpResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)30]];
        NSRange range4 = [atpResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)10]];
        
        if (range1.location != NSNotFound && range2.location != NSNotFound && range3.location != NSNotFound && range4.location != NSNotFound) {
            
            atpResponseStr = [atpResponseStr substringWithRange:NSMakeRange(range3.location+1, [atpResponseStr length]-range3.location-2)];
            [Base64 initialize];
            NSString *resultStr = [[[NSString alloc] initWithData:[[Base64 decode:atpResponseStr] zlibInflate] encoding:NSISOLatin1StringEncoding] autorelease];
            return resultStr;
        }
        else {
            return atpResponseStr;
        }
    }
    else {
        return atpResponseStr;
    }
}

/*---------------------- QC Decompression -----------------------*/
//During QC Login2 Process, if you pass in "useEncryption=1", you are required to decompress data returned from all QC requests
//Decompress Steps = Get data in between #30 and #10 ----> zlib decompress
- (NSString *)qcDecompressionProcess:(NSString *)qcResponseStr {
    if (isQCCompression) {
        NSRange range1 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)13]];
        NSRange range2 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%C", (unichar)129]];
        NSRange range3 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)30]];
        NSRange range4 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)10]];
        
        if (range1.location != NSNotFound && range2.location != NSNotFound && range3.location != NSNotFound && range4.location != NSNotFound) {
            
            qcResponseStr = [qcResponseStr substringWithRange:NSMakeRange(range3.location+1, [qcResponseStr length]-range3.location-2)];
            NSString *resultStr = [[[NSString alloc] initWithData:[[qcResponseStr dataUsingEncoding:NSISOLatin1StringEncoding] zlibInflate] encoding:NSISOLatin1StringEncoding] autorelease];
            return resultStr;
        }
        else {
            return qcResponseStr;
        }
    }
    else {
        return qcResponseStr;
    }
}

//RSA Encryption
//Special for E2EE Implementation (CIMB SG) in which the second password field is encrypted by RSA 1024. The public key is generated by modulus and exponent and the output result is in Hexadecimal string
- (NSString *)rsaEncryptData:(NSString *)plainText withModulus:(NSString *)modulus andExponent:(NSString *)exponent andTag:(NSString *)tag {
    NSData *rsaEncryptedStr = [RSACrypt encryptToData:plainText withPublicKeyBits:[RSACrypt generatePublicKeyBits:NO withCertContent:nil orWithModulus:modulus andExponent:exponent] andTag:tag];
    return [self hexStringFromBytes:rsaEncryptedStr forURL:NO];
}
//Special for AES Implementation (CIMB KL) in which the AES Key is encrypted by RSA 512. The public key is generated by certificate content (in X509 format) and the output result is in Base64 encoded string
//Special for AES Implementation (CIMB KL) in which the AES Key is encrypted by RSA 512. The public key is generated by certificate content (in X509 format) and the output result is in Base64 encoded string
- (NSString *)rsaEncryptData:(NSString *)plainText withCertContent:(NSString *)certContent andTag:(NSString *)tag andExponent:(NSString *)exponent{
    NSString *rsaEncryptedStr = [RSACrypt encryptAndB64EncodeToStr:plainText withPublicKeyBits:[RSACrypt generatePublicKeyBits:YES withCertContent:certContent orWithModulus:certContent andExponent:exponent] andTag:tag];
    return rsaEncryptedStr;
}

- (void)removeKeyRefWithTag:(NSString *)tag {
    [RSACrypt removeKeyRefWithTag:tag];
}

- (BOOL) isBitOnBase64:(NSString *)base64EncodedStr forBitIndex:(int)bitIdx {
    //Testing Code
    //NSData *data = [self bytesFromHexString:@""];
    //NSString *dataStr = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
    //base64EncodedStr = [self base64Encode:dataStr];
    //[dataStr release];
    
    if (![base64EncodedStr isEqualToString:@""]) {
        int vIdx = (bitIdx / 8);        //to find the index of byte in bytes array, 1 byte = 8 bits
        int vPos = 1 << (bitIdx % 8);   //to find the index of bit in bits
        NSData *data = [self base64Decode:base64EncodedStr];
        int *byteData = (int *)[data bytes];
        size_t byteDataLen = [data length];
        
        if (byteDataLen > vIdx) {
            int b = byteData[vIdx];
            b &= 0xFF;
            return (b & vPos) == vPos;
        }
    }
    
    return NO;
}

- (NSString *) generateTimeStamp {
    NSDateFormatter *ndf = [[[NSDateFormatter alloc] initWithSafeLocale] autorelease];
    [ndf setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:28800]];
    [ndf setDateFormat:@"yyyyMMddHHmmss.SSS"];
    NSString *timestamp = [ndf stringFromDate:[NSDate date]];
	return timestamp;
}


- (NSString *) generateTimeStampURLEncoded {
    NSDateFormatter *ndf = [[[NSDateFormatter alloc] initWithSafeLocale] autorelease];
    [ndf setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:28800]];
    [ndf setDateFormat:@"yyyyMMddHHmmss.SSS"];
    NSString *timestamp = [ndf stringFromDate:[NSDate date]];
    
    NSString *encodedString = (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                  NULL,
                                                                                  (CFStringRef)timestamp,
                                                                                  NULL,
                                                                                  (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                  kCFStringEncodingISOLatin1);
    
    return [encodedString autorelease];//7_
}


- (NSString *)numberFormatStockPriceForOrderSubmission:(double)input forExchange:(NSString *)exchg
{
    if ([exchg isEqualToString:@"JK"] || [exchg isEqualToString:@"JKD"]) {
        return [NSString stringWithFormat:@"%.0f", input];
    }
    else if ([exchg isEqualToString:@"PH"] || [exchg isEqualToString:@"PHD"]) {
        return [NSString stringWithFormat:@"%.4f", input];
    }
    else {
        if (input > 1000 || input < -1000) {
            return [NSString stringWithFormat:@"%.2f", input];
        }
        else {
            return [NSString stringWithFormat:@"%.3f", input];
        }
    }
    
    return nil;
}

#pragma mark - Class Functions
+ (NSString *)base64Encode:(NSString *)str {
	NSString *b64encodedStr = [Base64 encode:[str dataUsingEncoding:NSISOLatin1StringEncoding]];
	return b64encodedStr;
}

+ (NSData *)base64Decode:(NSString *)str {
	NSData *b64decodedData = [Base64 decode:str];
	return b64decodedData;
}
@end
