//
//  TradingRules.m
//  TCiPhone_CIMB
//
//  Created by Don Lee on 11/12/12.
//
//

#import "TradingRules.h"
#import "BrokerManager.h"
@implementation TradingRules
@synthesize exchg_code, actionList, orderTypeList, validityList, paymentList, currencyList, paymentPlusList, paymentCfgList,
reviseParam, reviseOrdTypeParam, reviseValidityParam, reviseCurrencyParam, revisePaymentParam, revisePaymentPlusParam,
triggerPriceTypeList, triggerPriceDirectionList, ordCtrlConditions, reviseCtrlConditions, currencyNotSupported;

- (id) init {
    self = [super init];
    if (self) {
        actionList = [[NSMutableArray alloc] init];
        orderTypeList = [[NSMutableArray alloc] init];
        validityList = [[NSMutableArray alloc] init];
        paymentList = [[NSMutableArray alloc] init];//[[NSMutableArray alloc] initWithObjects:@"Cash", nil];
        currencyList = [[NSMutableArray alloc] init];
        paymentCfgList = [[NSMutableArray alloc] init];
        paymentPlusList = [[NSMutableArray alloc] init];
        reviseParam = [[NSMutableArray alloc] init];
        reviseOrdTypeParam = [[NSMutableArray alloc] init];
        reviseValidityParam = [[NSMutableArray alloc] init];
        reviseCurrencyParam = [[NSMutableArray alloc] init];
        revisePaymentParam = [[NSMutableArray alloc] init];
        revisePaymentPlusParam = [[NSMutableArray alloc] init];
        triggerPriceTypeList = [[NSMutableArray alloc] init];
        triggerPriceDirectionList = [[NSMutableArray alloc] init];
        ordCtrlConditions = [[NSMutableDictionary alloc] init];
        reviseCtrlConditions = [[NSMutableDictionary alloc] init];
    }
    return self;
}
- (NSMutableArray *)orderTypeList{
    if([BrokerManager shareInstance].broker.tag == BrokerTag_SSI){
        return [NSMutableArray arrayWithArray:@[@"Limit"]];
    }
    return  orderTypeList;
}


@end
