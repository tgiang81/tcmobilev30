//
//  CurrencyRate.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 12/28/12.
//
//

#import <Foundation/Foundation.h>

@interface CurrencyRate : NSObject {
    NSString *currency;
    int denomination;
    double buy_rate;
    double sell_rate;
}
@property (nonatomic, retain) NSString *currency;
@property (nonatomic, assign) int denomination;
@property (nonatomic, assign) double buy_rate;
@property (nonatomic, assign) double sell_rate;


@end
