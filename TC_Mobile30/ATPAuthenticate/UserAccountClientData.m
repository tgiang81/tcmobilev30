//
//  UserAccountClientData.m
//  N2NTrader
//
//  Created by Adrian Lo on 2/27/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import "UserAccountClientData.h"
#import "UserPrefConstants.h"
#import "NumberHandler.h"
@implementation UserAccountClientData
@synthesize client_code, client_name, client_account_number, broker_code, broker_branch_code, client_acc_exchange, isMarginAccount;
@synthesize net_cash_limit, sell_limit, buy_limit, account_status, supported_exchanges, account_type;

- (id) init {
	self = [super init];
	if (self) {
		supported_exchanges = [[NSMutableArray alloc] init];
	}
	return self;
}


// March 2017 added this so we can nslog/print custom object easily
// if you added new property, please update this 

-(NSString*)description
{
    return [NSString stringWithFormat:
            @"<%@> client_code: %@ "
            "client_account_number: %@ "
            "client_name: %@ "
            "broker_code: %@ "
            "broker_branch_code: %@ "
            "client_acc_exchange: %@ "
            "supported_exchanges: %@ "
            "credit_limit: %.2f "
            "buy_limit: %.2f "
            "sell_limit: %.2f "
            "account_status: %d "
            "isMarginAccount: %d "
            "account_type: %@ "
            ,
            NSStringFromClass([self class]),
            self.client_code,
            self.client_account_number,
            self.client_name,
            self.broker_code,
            self.broker_branch_code,
            self.client_acc_exchange,
            self.supported_exchanges,
            self.credit_limit,
            self.buy_limit,
            self.sell_limit,
            self.account_status,
            self.isMarginAccount,
            self.account_type];
}
//[NSString stringWithFormat:@"%@-%@ (%@ %@)", model.broker_branch_code, model.client_account_number, [UserPrefConstants singleton].defaultCurrency, [NumberHandler numberFormat2D:0.f]]
- (NSString *)display_name{
    return [NSString stringWithFormat:@"%@-%@ %@ %@",self.client_account_number, self.client_name, [UserPrefConstants singleton].defaultCurrency, [NumberHandler numberFormat2D:self.credit_limit]];
}
- (void)setCredit_limit:(double)credit_limit{
    _credit_limit = credit_limit;
}
@end

