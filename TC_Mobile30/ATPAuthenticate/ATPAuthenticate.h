//
//  ATPAuthenticate.h
//  ATPTestConn
//
//  Created by Scott Thoo on 8/7/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ASIHttpGeneric.h"
#import "UserAccountClientData.h"
#import "TradeStatus.h"
#import "E2EEInfo.h"
#import "OrderDetails.h"
#import "TradeData.h"
#import "UserPrefConstants.h"
#import "PrtfDtlRptData.h"
#import "PrtfSummRptData.h"
#import "PrtfSubDtlRptData.h"
#import "DeviceDataFor2FA.h"
#import "RDSData.h"
#import "PaymentCUT.h"
#import "RegisterModel.h"
#import "CPService.h"

//typedef enum LoginStages {
//    LOGIN_INIT, // before PK
//    LOGIN_PK, // before Key2
//    LOGIN_KEY2, // before session2
//    LOGIN_SESSION2, // before e2ee or login
//    LOGIN_E2EE, // before e2ee
//    LOGIN_LOGIN, //  before login
//    LOGIN_SUCCESS
//} LoginStages;

//Configure for Mobile function
typedef void (^AUProgressHandler)(LoginStages progress);
typedef void (^AUCompletionHandler)(BOOL success, id result, NSError *error);

@protocol ATPDelegate <NSObject>
@optional
-(void)updateLoginProgress:(LoginStages)progress;
-(void)doLoginQRCode;
-(void)showShortSellConfirm:(BOOL)isBuyOrSell msg:(NSString *)msg;
@end

@interface ATPAuthenticate : ASIHttpGeneric
{
    NSTimer *idleCheckTimer;
    NSTimer *autoLogoutTimer;
    
    BOOL activity;
    BOOL isIdle;
    BOOL isTimeOut;
    

}

@property (nonatomic, weak) id<ATPDelegate> delegate;
@property (nonatomic, strong) NSMutableDictionary *userInfoDict;
// @property (nonatomic, strong) NSMutableDictionary *userWatchListDict;
@property (nonatomic, strong) NSMutableArray *userExchangelist;

@property (nonatomic, strong) E2EEInfo *e2eeInfo, *e2eeInfo1FA;
@property (nonatomic, strong) UserPrefConstants *userPref;

@property (nonatomic, strong) UserAccountClientData *confirmUACD;
@property (nonatomic, strong) OrderDetails *confirmOD;
@property (nonatomic, strong) TradeStatus *confirmTS;
@property (nonatomic, assign) BOOL success;
@property (nonatomic, assign) BOOL fromQRCodeLogin;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *status_code;
@property (nonatomic, strong) NSString *passwd1FA;

@property (nonatomic, strong) NSTimer *idleCheckTimer;
@property (nonatomic, strong) NSTimer *autoLogoutTimer;
@property (nonatomic, assign) BOOL isIdle;

@property (nonatomic, strong) NSDate *time_expire;
@property (nonatomic, assign) int countDown;

//Block using for mobilde function
@property (strong, nonatomic) AUProgressHandler loginProgressHandler;
@property (strong, nonatomic) AUCompletionHandler loginCompletionHandler;
@property (strong, nonatomic) AUCompletionHandler watchlistCompletion;
@property (strong, nonatomic) AUCompletionHandler addOrUpdateWatchlistCompletion;
@property (strong, nonatomic) AUCompletionHandler deleteWatchlistCompletion;
@property (strong, nonatomic) AUCompletionHandler addStockToWatchlistCompletion;
@property (strong, nonatomic) AUCompletionHandler removeStockFromWatchlistCompletion;
//if (self.ringAction) {
//	self.ringAction(self, TCAction_Close);
//}
//_tcAlert.ringAction = completionBlock;

+ (ATPAuthenticate *)singleton;
- (void) atpGetPK;
- (void) atpGetPK_LoadBalance;
- (void) atpDoLogout;
- (void) atpGetKey2:(NSString *)certContent andExponent:(NSString *)exponentKey;
- (void) atpDoLogin;

// WATCHLIST
- (void) getWatchList;
- (void) deleteWatchlist:(int)favID;
- (void) addOrUpdateWatchlistName:(int)favID withFavName:(NSString *)favName;
- (void) getWatchListItems:(int)favID;
- (void) addWatchlistItem:(int)favID withItem:(NSString *)stock_code;
- (void) addWatchlistCheckItem:(int)favID withItem:(NSString *)stock_code;
- (void) deleteWatchlistItem:(int)favID withItem:(NSString *)wli;
- (void) getWatchListItemsForStreamer:(int)favID;

- (void) checkTick;
- (void) timeoutActivity;
- (void) logout;
- (void) doCheckSingleLogon;
- (void) initTimerLogin;
- (void) invalidateIdleCheckTimer;

//Exchange
- (void)getExchangelist;

#pragma mark - TRADING
- (void) atpGetClientList;


// NEWS
- (void) getArchiveNewsForStock:(NSString*)stock_code forDays:(long)days; // archivenews
- (void) getJarNews;

// TRADING
- (void) getTradeLimitForAccount:(UserAccountClientData *) uacd   forPaymentCUT:(BOOL)paymentCUT andCurrency:(NSString *)settCurr;
- (void) getTradeHistoryFromDate:(NSString*)dateFrom_yyyymmdd toDate:(NSString*)toDate_yyyymmdd;
- (void) getTradeHistoryFromDate:(NSString*)dateFrom_yyyymmdd toDate:(NSString*)toDate_yyyymmdd completion:(void (^ __nullable)(NSArray *orderBooks))completion;
- (void) getTradeStatus:(int)account_selection;
- (void) getTradeStatus:(int)account_selection completion:(void (^ __nullable)(NSArray *orderBooks))completion;
- (void) getPortfolio:(NSInteger)account_selection; // realized or unrealized
- (void) prepareTradeRevise:(BOOL) orderConfirmation forAccount:(UserAccountClientData *) uacd forOrder:(OrderDetails *) od forExistingOrder:(TradeStatus *) ts ;
- (void) prepareTradeCancel:(TradeData *) tradeData forAccount:(UserAccountClientData *) uacd forOrder:(OrderDetails *) od forExistingOrder:(TradeStatus *) ts ;
- (void) prepareTradeSubmit:(BOOL) orderConfirmation forAccount:(UserAccountClientData *) uacd forOrder:(OrderDetails *) od;
- (void) doTradePrtfDtlRpt:(int)account_selection isDerivative:(BOOL)isDerivative ofSubDtlRpt:(PrtfSubDtlRptData *)psdrd;
- (void) doTradePrtfSubDtlRpt:(int)account_selection isDerivative:(BOOL)isDerivative;
- (void) doTradePrtfSummRpt:(int)account_selection isDerivative:(BOOL)isDerivative;

// 1FA/2FA
-(void) doValidate1FAPassword:(NSString*)pwd; // validate login passwd again
-(void) validate2FAPinForDevice:(DeviceDataFor2FA*)deviceData forTrade:(BOOL)isTrade; // validate OTP pin
- (void) serverLogIn;
- (void) doRequestSMSOTP;
- (void) doValidateSMSOTP:(NSString *)dvData andTrade:(BOOL)isTrade;
-(void)getElasticNewsForStocks:(NSString*)stock_code forDays:(long)days withKeyword:(NSString *)keyword andTargetID:(NSString *)targetID andSource:(NSString *)source andCategory:(NSString *)category;

# //FOR MOBILE - Main for mobile function
#pragma mark - Main for mobile function
- (void)startLoginBy:(NSString *)userName password:(NSString *)password progress:(AUProgressHandler)progressHandler completion:(AUCompletionHandler)completionHandler;
#pragma mark - Watchlist
- (void)getWatchlistWithCompletion:(AUCompletionHandler)handler;
- (void)addOrUpdateWatchlist:(NSInteger)watchlistID name:(NSString *)name completion:(AUCompletionHandler)handler;
//For Mobile version: Using block style
- (void)deleteWatchlist:(NSInteger)wlID completion:(AUCompletionHandler)handler;
//Add stock to watchlist
- (void)addStock:(NSString *)stockCode toWatchlist:(int)watchlistID completion:(AUCompletionHandler)handler;
- (void)removeStock:(NSString *)stockCode fromWatchlist:(NSInteger)watchlistID completion:(AUCompletionHandler)handler;

#pragma mark - Register
// REGISTER
- (void) doRegister:(RegisterModel*)theNewUser completionHander:(AUCompletionHandler)theHander;
//New
- (void)actionShortSell:(BOOL)isBuyOrSell;



#pragma mark - UTILS: Encode/Decode...
- (NSString *)atpDecryptionProcess:(NSString *)atpResponseStr;
- (NSString *)getValue:(NSString *)input;
- (BOOL) isBitOnBase64:(NSString *)base64EncodedStr forBitIndex:(int)bitIdx;
- (NSString *)getIPAddress;


@end







