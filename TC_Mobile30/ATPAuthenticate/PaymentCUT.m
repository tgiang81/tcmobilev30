//
//  PaymentCUT.m
//  TCiPhone_CIMB_SG_UAT
//
//  Created by Scott Thoo on 4/9/15.
//
//

#import "PaymentCUT.h"

@implementation PaymentCUT
@synthesize paymentCountry,AccountType,paymentOptions,paymentSettings;

- (id) init {
    self = [super init];
    if (self) {
        paymentSettings=@"";
        paymentCountry=@"";
        AccountType=@"";
        paymentOptions=nil;
    }
    return self;
}


-(NSString*)description
{
    return [NSString stringWithFormat:
            @"paymentCtry:%@, AccType:%@, paymentOptions:%@, paymentSettings:%@",
            paymentCountry,AccountType,paymentOptions,paymentSettings];
    
}


@end
