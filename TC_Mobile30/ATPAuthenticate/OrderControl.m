//
//  OrderControl.m
//  TCiPhone_CIMB
//
//  Created by Don Lee on 11/9/12.
//
//

#import "OrderControl.h"

@implementation OrderControl
@synthesize orderType, validityList, enable_fields, disclosed_validityList, min_validityList;

- (id) init {
    self = [super init];
    if (self) {
        validityList = [[NSMutableArray alloc] init];
        enable_fields = [[NSMutableArray alloc] init];
        disclosed_validityList = [[NSMutableArray alloc] init];
        min_validityList = [[NSMutableArray alloc] init];
    }
    return self;
}



@end
