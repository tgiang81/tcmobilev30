//
//  PrtfSubDtlRptData.h
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 2/28/14.
//
//

#import <Foundation/Foundation.h>

@interface PrtfSubDtlRptData : NSObject {
    int sequence_no;
    NSString *stock_code;
    NSString *average_purchase_price;
    NSString *gross_buy;
    NSString *gross_sell;
    NSString *day_buy;
    NSString *day_sell;
    NSString *bf_buy;
    NSString *bf_sell;
    double contractPerVal;        //Multiplier
    NSString *realized_pl;
    NSString *stock_name;
    NSString *home_currency;
    NSString *stock_currency;
    NSString *exchange_rate;
    
    int nett_position;         //Gross Buy - Gross Sell
    double unrealized_pl;      //Get from ATP at first, replace by result of calculation with feed last done price
    double price_current;      //Get from ATP at first, replace by feed last done price
}
@property (nonatomic, assign) int sequence_no;
@property (nonatomic, retain) NSString *stock_code;
@property (nonatomic, retain) NSString *average_purchase_price;
@property (nonatomic, retain) NSString *gross_buy;
@property (nonatomic, retain) NSString *gross_sell;
@property (nonatomic, retain) NSString *day_buy;
@property (nonatomic, retain) NSString *day_sell;
@property (nonatomic, retain) NSString *bf_buy;
@property (nonatomic, retain) NSString *bf_sell;
@property (nonatomic, assign) double contractPerVal;
@property (nonatomic, retain) NSString *realized_pl;
@property (nonatomic, retain) NSString *stock_name;
@property (nonatomic, retain) NSString *home_currency;
@property (nonatomic, retain) NSString *stock_currency;
@property (nonatomic, retain) NSString *exchange_rate;
@property (nonatomic, assign) int nett_position;
@property (nonatomic, assign) double unrealized_pl;
@property (nonatomic, assign) double price_current;

@end
