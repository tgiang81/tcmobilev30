//
//  RDSData.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 3/29/13.
//
//

#import <Foundation/Foundation.h>

@interface RDSData : NSObject {
    NSString *rdsVersion;
    NSString *rdsStatus;
    NSString *rdsURL;
}
@property (nonatomic, retain) NSString *rdsVersion;
@property (nonatomic, retain) NSString *rdsStatus;
@property (nonatomic, retain) NSString *rdsURL;

@end
