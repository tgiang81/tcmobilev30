//
//  TradeStatus.h
//  N2NTrader
//
//  Created by Adrian Lo on 3/4/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TradeStatus : NSObject {
    NSString *account_number;		//.
    NSString *client_code;			///
	NSString *sender_code;			//2
	NSString *broker_code;			//3
    NSString *exchg_code;           //{
    
	NSString *stock_name;			//N
	NSString *stock_code;			//5
    
    NSString *order_number;			//+ OMS Order No.
	NSString *order_time;			//7 Order Time Stamp
    NSString *guid;					//* Ticket No.
	NSString *fix_order_number;		//, Fix Order No.
    
	NSString *action;				//4
    NSString *order_src;			//8
    
    NSString *order_type;			//6
	NSString *validity;				//9
    NSString *sett_currency;        //B
    NSString *payment_type;         //Ascii138
	
    NSString *order_quantity;		//;
    NSString *cancelled_quantity;	//C
	NSString *unmt_quantity;		//D
    NSString *mt_quantity;			//E
    NSString *min_quantity;         //@
    NSString *disclosed_quantity;   //A
	NSString *order_price;			//<
    NSString *value;                //>
    NSString *trigger_price;        //?
	NSString *mt_price;				//F
    NSString *expiry;				//:
    NSString *triggerType;          //Ascii177
    NSString *triggerDirection;     //Ascii178

	NSString *mt_value;				//G
    
    NSString *status;				//I
	NSString *status_text;			//J
	NSString *message;				//M
	
    NSString *lot_size;             //Ascii158
    NSString *last_update;          //L
    
    NSMutableDictionary *compulsaryField;
}
@property (nonatomic, retain) NSString *dateStringToCompare;
@property (nonatomic, retain) NSString *value;
@property (nonatomic, retain) NSString *account_number;
@property (nonatomic, retain) NSString *client_code;
@property (nonatomic, retain) NSString *sender_code;
@property (nonatomic, retain) NSString *broker_code;
@property (nonatomic, retain) NSString *exchg_code;
@property (nonatomic, retain) NSString *stock_name;
@property (nonatomic, retain) NSString *stock_code;
@property (nonatomic, retain) NSString *order_number;
@property (nonatomic, retain) NSString *order_time;
@property (nonatomic, retain) NSString *guid;
@property (nonatomic, retain) NSString *fix_order_number;
@property (nonatomic, retain) NSString *action;
@property (nonatomic, retain) NSString *order_src;
@property (nonatomic, retain) NSString *order_type;
@property (nonatomic, retain) NSString *validity;
@property (nonatomic, retain) NSString *sett_currency;
@property (nonatomic, retain) NSString *payment_type;
@property (nonatomic, retain) NSString *order_quantity;
@property (nonatomic, retain) NSString *cancelled_quantity;	//C
@property (nonatomic, retain) NSString *unmt_quantity;		//D
@property (nonatomic, retain) NSString *mt_quantity;
@property (nonatomic, retain) NSString *min_quantity;
@property (nonatomic, retain) NSString *disclosed_quantity;
@property (nonatomic, retain) NSString *order_price;	
@property (nonatomic, retain) NSString *trigger_price;
@property (nonatomic, retain) NSString *mt_price;		
@property (nonatomic, retain) NSString *expiry;
@property (nonatomic, retain) NSString *triggerType;
@property (nonatomic, retain) NSString *triggerDirection;
@property (nonatomic, retain) NSString *mt_value;				//G
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *status_text;
@property (nonatomic, retain) NSString *statusMode;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *lot_size;
@property (nonatomic, retain) NSString *last_update;
@property (nonatomic, retain) NSNumber *total;
@property (nonatomic, retain) NSMutableDictionary *compulsaryField;
@property (nonatomic, retain) NSString *ticker;
@property (nonatomic, retain) NSString *price_ref;

@property (assign, nonatomic) BOOL isQtyMode;
@property (assign, nonatomic) BOOL isExpanded;
@property (assign, nonatomic) BOOL isSelected;
-(void) reset;
- (NSString *)getDate;
- (NSString *)getQtyBg;
- (NSString *)getOrderSource;
@end
