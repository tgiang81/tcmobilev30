//
//  PrtfSubDtlRptData.m
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 2/28/14.
//
//

#import "PrtfSubDtlRptData.h"

@implementation PrtfSubDtlRptData
@synthesize sequence_no, stock_code, average_purchase_price, gross_buy, gross_sell, day_buy, day_sell, bf_buy, bf_sell, contractPerVal, realized_pl, stock_name, home_currency, stock_currency, exchange_rate, nett_position, unrealized_pl, price_current;

- (id) init {
    self = [super init];
    if (self) {
    }
    return self;
}



@end
