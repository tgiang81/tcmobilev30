//
//  PrtfDtlRptData.h
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 3/6/14.
//
//

#import <Foundation/Foundation.h>

@interface PrtfDtlRptData : NSObject {
    int sequence_no;
    NSString *side;
    NSString *matched_qty;
    NSString *matched_price;
    double contractPerVal;
    NSString *home_currency;
    NSString *stock_currency;
    NSString *exchange_rate;
    
    //Front End Calculation
    double unrealized_pl;
}
@property (nonatomic, assign) int sequence_no;
@property (nonatomic, retain) NSString *side;
@property (nonatomic, retain) NSString *matched_qty;
@property (nonatomic, retain) NSString *matched_price;
@property (nonatomic, assign) double contractPerVal;
@property (nonatomic, retain) NSString *home_currency;
@property (nonatomic, retain) NSString *stock_currency;
@property (nonatomic, retain) NSString *exchange_rate;
@property (nonatomic, assign) double unrealized_pl;

@end
