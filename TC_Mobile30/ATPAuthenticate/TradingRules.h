//
//  TradingRules.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 11/12/12.
//
//

#import <Foundation/Foundation.h>

@interface TradingRules : NSObject {
    NSString *exchg_code;
    
    NSMutableArray *actionList;
    NSMutableArray *orderTypeList;
    NSMutableArray *validityList;
    NSMutableArray *paymentList;
    NSMutableArray *currencyList;
    NSMutableArray *paymentPlusList;
    NSMutableArray *paymentCfgList;
    NSMutableArray *reviseParam;
    NSMutableArray *reviseOrdTypeParam;
    NSMutableArray *reviseValidityParam;
    NSMutableArray *reviseCurrencyParam;
    NSMutableArray *revisePaymentParam;
    NSMutableArray *revisePaymentPlusParam;
    NSMutableArray *triggerPriceTypeList;
    NSMutableArray *triggerPriceDirectionList;
    NSMutableDictionary *ordCtrlConditions;
    NSMutableDictionary *reviseCtrlConditions;
}
@property (nonatomic, retain) NSString *exchg_code;
@property (nonatomic, retain) NSMutableArray *actionList;
@property (nonatomic, retain) NSMutableArray *orderTypeList;
@property (nonatomic, retain) NSMutableArray *validityList;
@property (nonatomic, retain) NSMutableArray *paymentList;
@property (nonatomic, retain) NSMutableArray *currencyList;
@property (nonatomic, retain) NSMutableArray *paymentPlusList;
@property (nonatomic, retain) NSMutableArray *paymentCfgList;
@property (nonatomic, retain) NSMutableArray *reviseParam;
@property (nonatomic, retain) NSMutableArray *reviseOrdTypeParam;
@property (nonatomic, retain) NSMutableArray *reviseValidityParam;
@property (nonatomic, retain) NSMutableArray *reviseCurrencyParam;
@property (nonatomic, retain) NSMutableArray *revisePaymentParam;
@property (nonatomic, retain) NSMutableArray *revisePaymentPlusParam;
@property (nonatomic, retain) NSMutableArray *triggerPriceTypeList;
@property (nonatomic, retain) NSMutableArray *triggerPriceDirectionList;
@property (nonatomic, retain) NSMutableDictionary *ordCtrlConditions;
@property (nonatomic, retain) NSMutableDictionary *reviseCtrlConditions;
@property (nonatomic, retain) NSString *currencyNotSupported;

@end
