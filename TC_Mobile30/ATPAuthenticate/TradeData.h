//
//  TradeData.h
//  N2NTrader
//
//  Created by Adrian Lo on 2/28/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TradeData : NSObject {
	//Possible Values:
	//Buy, Sell, Cancel, Revise
    NSString *action;

	NSString *ticker;
	
	//Possible Values:
	//Market, Limit, Stop, StopLimit, MarketToLimit, MarketOnOpen, MarketOnClose
	NSString *order_type;
	
	//Possible Values:
	//Day, GTC, OPG, FAK, FOK, GTX, GTD, Session
	NSString *validity;
	
	int order_quantity;
	float order_price;
}

@end
