//
//  PortfolioData.m
//  N2NTrader
//
//  Created by Adrian Lo on 3/21/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import "PortfolioData.h"


@implementation PortfolioData
@synthesize stockcode, stockname, stockcode_cn, price_high, price_low, price_year_high, price_year_low;
@synthesize exchange_ref_number, client_account, branch_code, brokerage, quantity_available, quantity_on_hand;
@synthesize total_price_P, total_price_B, total_quantity_P, total_comm_P, sell_qty_in_proc, total_price_S, rms_code;
@synthesize price_last_done, price_ref, price_AS, price_AP, volume, lot_size, change_amount, change_percentage;
@synthesize sector_code, stock_class, stock_status, price_base, price_day_high, price_day_low, 	price_avg_purchase;
@synthesize limit_upper, limit_lower, gross_market_value, unrealized_gain_loss_amount, unrealized_gain_loss_percentage;
@synthesize total_value_P, total_value_S, total_quantity_S, total_brokerage, gain_loss, aggregated_buy_price, aggregated_sell_price;
@synthesize total_quantity_short, total_comm_S, currency, settlement_mode, realized_gain_loss_amount, realized_gain_loss_percentage;




@end
