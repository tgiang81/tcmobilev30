//
//  PrtfSummRptData.h
//  TCiPhone_CIMB
//
//  Created by Grace Loke on 2/28/14.
//
//

#import <Foundation/Foundation.h>

@interface PrtfSummRptData : NSObject {
    NSString *open_long;
    NSString *open_short;
    NSString *bf_cash_balance;
    NSString *deposit;
    NSString *withdrawal;
    NSString *eligible_collateral;
    NSString *initial_margin;
    NSString *maintenance_margin;
    NSString *margin_call;
    NSString *realized_pl;
    
    double current_balance;         //Cash Balance + Deposit - Withdrawal
    double total_unrealized_pl;
    int total_gross_buy;
    int total_gross_sell;
}
@property (nonatomic, retain) NSString *open_long;
@property (nonatomic, retain) NSString *open_short;
@property (nonatomic, retain) NSString *bf_cash_balance;
@property (nonatomic, retain) NSString *deposit;
@property (nonatomic, retain) NSString *withdrawal;
@property (nonatomic, retain) NSString *eligible_collateral;
@property (nonatomic, retain) NSString *initial_margin;
@property (nonatomic, retain) NSString *maintenance_margin;
@property (nonatomic, retain) NSString *margin_call;
@property (nonatomic, retain) NSString *realized_pl;
@property (nonatomic, assign) double current_balance;
@property (nonatomic, assign) double total_unrealized_pl;
@property (nonatomic, assign) int total_gross_buy;
@property (nonatomic, assign) int total_gross_sell;

@end
