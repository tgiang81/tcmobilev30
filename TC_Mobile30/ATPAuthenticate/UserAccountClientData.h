//
//  UserAccountClientData.h
//  N2NTrader
//
//  Created by Adrian Lo on 2/27/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserAccountClientData : NSObject {
	NSString *client_code;
	NSString *client_account_number;
	NSString *client_name;
	NSString *broker_code;
	NSString *broker_branch_code;
    NSString *client_acc_exchange;
    NSString *account_type;
	NSMutableArray *supported_exchanges;

	double credit_limit;
	double buy_limit;
	double net_cash_limit;
	double sell_limit;
	int account_status;
    int isMarginAccount;

}
@property (nonatomic, strong) NSString *client_code;
@property (nonatomic, strong) NSString *client_account_number;
@property (nonatomic, strong) NSString *client_name;
@property (nonatomic, strong) NSString *broker_code;
@property (nonatomic, strong) NSString *broker_branch_code;
@property (nonatomic, strong) NSString *client_acc_exchange;
@property (nonatomic, strong) NSString *account_type;
@property (nonatomic, strong) NSString *display_name;
@property (nonatomic, strong) NSMutableArray *supported_exchanges;

@property (nonatomic, assign) double credit_limit;
@property (nonatomic, assign) double buy_limit;
@property (nonatomic, assign) double sell_limit;
@property (nonatomic, assign) double net_cash_limit;
@property (nonatomic, assign) int account_status;
@property (nonatomic, assign) int isMarginAccount;
@property (nonatomic, assign) int isDerivative;
@end
