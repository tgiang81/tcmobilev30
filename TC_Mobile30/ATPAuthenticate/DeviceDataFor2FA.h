//
//  DeviceDataFor2FA.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 1/18/13.
//
//

#import <Foundation/Foundation.h>

@interface DeviceDataFor2FA : NSObject {
    NSString *deviceID;
    NSString *icNo;
    NSString *otpPin;
}
@property (nonatomic, retain) NSString *deviceID;
@property (nonatomic, retain) NSString *icNo;
@property (nonatomic, retain) NSString *otpPin;
@end
