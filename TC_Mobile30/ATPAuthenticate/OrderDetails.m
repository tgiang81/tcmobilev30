//
//  OrderDetails.m
//  N2NTrader
//
//  Created by Adrian Lo on 1/15/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import "OrderDetails.h"


@implementation OrderDetails
@synthesize ticket_no, order_source, order_action, ticker, company_name, stock_code,
order_type, validity, currency, payment_type, price, trigger_price, quantity, minQuantity, disclosedQuantity, expiry, triggerType, triggerDirection, pin, otpPin, deviceID, shortSellState;
@synthesize compulsaryField, fullOrder;

- (id) init {
	self = [super init];
	if (self) {
        ticket_no = @"";
        order_source = @"W";
        order_action = @"";
		ticker = @"";
        company_name = @"";
		stock_code = @"";
        order_type = @"";
		validity = @"";
		currency = @"";
        payment_type = @"";
        price = @"";
        trigger_price = @"";
		quantity = @"";
		minQuantity = @"";
        disclosedQuantity = @"";
        expiry = @"";
        triggerType = @"";
        triggerDirection = @"";
        pin = @"";
        otpPin = @"";
        deviceID = @"";
        shortSellState = NO;
        fullOrder = NO;
        compulsaryField = [[NSMutableDictionary alloc] init];
	}
	return self;
}

-(void) reset
{
    ticket_no = @"";
    order_source = @"W";
    order_action = @"";
    ticker = @"";
    company_name = @"";
    stock_code = @"";
    order_type = @"";
    validity = @"";
    currency = @"";
    payment_type = @"";
    price = @"";
    trigger_price = @"";
    quantity = @"";
    minQuantity = @"";
    disclosedQuantity = @"";
    expiry = @"";
    triggerType = @"";
    triggerDirection = @"";
    pin = @"";
    otpPin = @"";
    deviceID = @"";
    shortSellState = NO;
    fullOrder = NO;
    [compulsaryField removeAllObjects];
}
@end
