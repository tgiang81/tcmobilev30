//
//  AppDelegate.h
//  TCUniversal
//
//  Created by Scott Thoo on 8/28/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import <UserNotifications/UserNotifications.h>
#import "AllCommon.h"
#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "MyContainerVC.h"

@class BaseTabBarViewController;
/*
 giang , account list for ssi - ssi02/ssi03/ssi04/ssi05(abc123)
 clientid: ssi002...ssi0011
 DealerID: S0001..0005

 
 */
//#ifdef DEBUG
//#    define DLog(...) NSLog(__VA_ARGS__)
//#else
//#    define DLog(...) /* */
//#endif

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@protocol AppDelegateDelegate <NSObject>

-(void)delegateDidBecomeActive;
-(void)delegateWillResignActive;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
@property (assign) BOOL restrictRotation;
@property (strong, nonatomic) UIWindow *window;
@property (weak,nonatomic) id<AppDelegateDelegate> delegateDelegate;
@property (strong, nonatomic) MyContainerVC *container;
@property (strong, nonatomic) BaseTabBarViewController *rootTabbarVC;
@property (strong, nonatomic) UIViewController *topViewController;
#pragma mark - Main
- (void)showCommonMenu;
- (void)startMainApp;
- (void)setHomeVC;
- (void)logout;
- (void)doKickOut:(NSString *)message;

- (void)showContinueLogin;
- (void)enableFilterVC:(BOOL)isEnable;
//CONTAINER
- (void)toogleLeftMenu:(void (^)(void))completion;
- (void)toogleRightMenu:(void (^)(void))completion;
- (void)replaceCenterContainerToVC:(UIViewController *)newVC;
- (void)openNewContent:(UIViewController *)newContentVC atType:(MenuItemType)type; //For new design

//Show Active Touch
- (void)showActiveTouchID;

@end

