//
//  SkipConfirmationViewController.m
//  TCiPad
//
//  Created by Sri Ram on 12/3/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import "SkipConfirmationViewController.h"
#import "AppControl.h"
#import "UIViewController+Popup.h"
#import "ThemeManager.h"
#import "SettingManager.h"
#import "AroundShadowView.h"
#import "TradeUtils.h"
@interface SkipConfirmationViewController ()
{
    NSString *action;
    BOOL sendOnce, runOnce;
    UIColor *themeColor;
    CGFloat defaultCornerRadius;
}

@end

@implementation SkipConfirmationViewController

@synthesize skipDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _detailArray = [[NSMutableArray alloc] init];
    defaultCornerRadius = 4;
    _submitBtn.layer.cornerRadius = kButtonRadius*_submitBtn.frame.size.height;
    _submitBtn.clipsToBounds = YES;
    _cancelBtn.layer.cornerRadius = kButtonRadius*_submitBtn.frame.size.height;
    _cancelBtn.clipsToBounds = YES;
    self.confirmTable.estimatedRowHeight = 35;
    if(IS_IPHONE){
        _submitBtn.layer.cornerRadius = defaultCornerRadius;
        _cancelBtn.layer.cornerRadius = defaultCornerRadius;
        
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnCancelPressed:) name:@"dismissAllModalViews" object:nil];
    
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoBg);
    self.StkName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.StkCode.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.changeValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.changePercent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    if([self.skipDelegate respondsToSelector:@selector(needToReloadSkipVCData)]){
        [self.skipDelegate needToReloadSkipVCData];
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2==0) {
        cell.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.90 alpha:1];;
    } else {
        cell.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.80 alpha:1];
    }
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"confirmCellID";
    
    ConfirmTableViewCell *cell = (ConfirmTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell==nil) {
        cell = [[ConfirmTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    NSDictionary *eachLine = [_detailArray objectAtIndex:indexPath.row];
    
    
    cell.dataTitle.text = [[eachLine allKeys] objectAtIndex:0];
    NSString *contentStr = [eachLine objectForKey:cell.dataTitle.text];
    
    if ([contentStr length]<=0) contentStr = @"-";
    cell.dataContent.text = contentStr;
    
    
    
    
    //    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc] initWithString:actionStr];
    //    [attrib addAttribute:NSForegroundColorAttributeName
    //                  value:clr
    //                  range:NSMakeRange(0, [action length])];
    
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_detailArray count];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupArrayDetail:(OrderDetails *)od arrayDetail:(NSMutableArray *)arrayDetail{
    
}
-(void)loadData:(NSString *)str orderdetails:(OrderDetails *)od tradeStat:(TradeStatus*)tradeStatus fullMode:(BOOL)full withStockCurrency:(NSString *)stockCurrency
{
    
    // NSLog(@"Order Details %@",od);
    
    //NSLog(@"od.price %@", od.price);
    sendOnce = NO;
    _StkName.text=od.ticker;
    _StkCode.text=od.stock_code;
    
    [_detailArray removeAllObjects];
    action=od.order_action;
    
    if ([action isEqualToString:@"Buy"]){
        if(IS_IPAD){
            themeColor = [UIColor colorWithRed:0.16 green:0.33 blue:0.07 alpha:1.00];
        }else{
            themeColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].buyColor);
        }
    }
    if ([action isEqualToString:@"Sell"]){
        if(IS_IPAD){
            themeColor = [UIColor colorWithRed:0.58 green:0.00 blue:0.07 alpha:1.00];
        }else{
            themeColor = TC_COLOR_FROM_HEX([SettingManager shareInstance].sellColor);
        }
        
    }
    if ([action isEqualToString:@"Revise"]){
        if(IS_IPAD){
            themeColor = [UIColor colorWithRed:0.00 green:0.43 blue:0.57 alpha:1.00];
        }else{
            themeColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_NormalButtonBg);
        }
        
    }
    if ([action isEqualToString:@"Cancel"]){
        if(IS_IPAD){
            themeColor = [UIColor colorWithRed:0.73 green:0.31 blue:0.04 alpha:1.00];
        }else{
            themeColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_SpecialButtonBg);
        }
        
    }
    
    _confirmTitle.backgroundColor = themeColor;
    NSString *actionStr = @"";
    
    if (([action isEqualToString:@"Revise"]||[action isEqualToString:@"Cancel"])) {
        
        actionStr = [NSString stringWithFormat:@"%@, %@", [LanguageManager stringForKey:tradeStatus.action], [LanguageManager stringForKey:od.order_type]];
    } else {
        actionStr = [NSString stringWithFormat:@"%@", [LanguageManager stringForKey:od.order_type]];
    }
    
    NSDictionary *dat1 = [NSDictionary dictionaryWithObject:str forKey:[LanguageManager stringForKey:@"Account No."]];
    [_detailArray addObject:dat1];
    
    NSDictionary *actionDic = [NSDictionary dictionaryWithObject:[LanguageManager stringForKey:action] forKey:[LanguageManager stringForKey:@"Action"]];
    [_detailArray addObject:actionDic];
    
    
    NSDictionary *indicactive = [NSDictionary dictionaryWithObject:[TradeUtils getCurrencyRateOf:od.currency forAction:[action isEqualToString:@"Sell"] == YES ? 1 : 0] forKey:[LanguageManager stringForKey:@"Indicative Exch. Rate"]];
    [_detailArray addObject:indicactive];
    
    
    NSString *currencyPlus =[NSString stringWithFormat:@"%@",[[[[QCData singleton] qcFeedDataDict]objectForKey:od.stock_code]objectForKey:FID_134_S_CURRENCY]];
    NSArray *buySellArray = [TradeUtils getTradeLimitString:[UserPrefConstants singleton].userSelectedAccount orderDetail:od stkExchange:[Utils stripExchgCodeFromStockCode:od.stock_code] action:[action isEqualToString:@"Sell"] == YES ? 1 : 0 currencyPlus:currencyPlus];
    
    NSDictionary *buySellLimit = [NSDictionary dictionaryWithObject:buySellArray[1] forKey:[LanguageManager stringForKey:buySellArray[0]]];
    [_detailArray addObject:buySellLimit];
    
    NSDictionary *dat2 = [NSDictionary dictionaryWithObject:actionStr forKey:[LanguageManager stringForKey:@"Order Type"]];
    [_detailArray addObject:dat2];
    
    if ([od.validity isEqualToString:@"GTD"]) {
        
        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
        [ndf setDateFormat:@"yyyyMMdd"];
        NSDate *date = [ndf dateFromString:od.expiry];
        [ndf setDateFormat:@"dd/MM/yyyy"];
        NSString *dateStr  = [NSString stringWithFormat:@"%@ (%@)", [LanguageManager stringForKey:@"GTD"], [ndf stringFromDate:date]];
        
        NSDictionary *dat3 = [NSDictionary dictionaryWithObject:dateStr  forKey:[LanguageManager stringForKey:@"Validity"]];
        [_detailArray addObject:dat3];
        
    } else  {
        NSDictionary *dat3 = [NSDictionary dictionaryWithObject:[LanguageManager stringForKey:od.validity] forKey:[LanguageManager stringForKey:@"Validity"]];
        [_detailArray addObject:dat3];
    }
    
    
    NSDictionary *dat12 = [NSDictionary dictionaryWithObject:od.currency forKey:[LanguageManager stringForKey:@"Settlement Currentcy"]];
    [_detailArray addObject:dat12];
    
    if ([od.payment_type length]>0) {
        NSDictionary *dat11 = [NSDictionary dictionaryWithObject:[LanguageManager stringForKey:od.payment_type] forKey:[LanguageManager stringForKey:@"Payment"]];
        [_detailArray addObject:dat11];
    }
    
    NSDictionary *dat4 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@ %@",stockCurrency,od.price]
                                                     forKey:[LanguageManager stringForKey:@"Order Price"]];
    [_detailArray addObject:dat4];
    
    NSDictionary *dat5 = [NSDictionary dictionaryWithObject:od.quantity forKey:[LanguageManager stringForKey:@"Order Qty (Unit)"]];
    [_detailArray addObject:dat5];
    
    
    
    
    if (full) {
        // add full orderpad data
        if ([od.minQuantity length]>0) {
            NSDictionary *dat50 = [NSDictionary dictionaryWithObject:od.minQuantity forKey:[LanguageManager stringForKey:@"Min.Qty"]];
            [_detailArray addObject:dat50];
        }
        
        
        if ([od.trigger_price length]>0) {
            NSDictionary *dat7 = [NSDictionary dictionaryWithObject:od.trigger_price forKey:[LanguageManager stringForKey:@"Trigger Price"]];
            [_detailArray addObject:dat7];
            
        }
        
        if ([od.triggerType length]>0) {
            NSDictionary *dat8 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",od.triggerType] forKey:[LanguageManager stringForKey:@"Trigger Type"]];
            [_detailArray addObject:dat8];
        }
        
        if ([od.triggerDirection length]>0) {
            NSDictionary *dat9 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",od.triggerDirection] forKey:[LanguageManager stringForKey:@"Trigger Direction"]];
            [_detailArray addObject:dat9];
        }
        
        if ([od.disclosedQuantity length]>0) {
            NSDictionary *dat10 = [NSDictionary dictionaryWithObject:od.disclosedQuantity forKey:[LanguageManager stringForKey:@"Disclosed"]];
            [_detailArray addObject:dat10];
        }
        
        
    }
    
    // TOTAL ORDER VALUE
    
    double resolvedPrice = 0;
    
    double bidPrice = [[[[[QCData singleton] qcFeedDataDict] objectForKey:od.stock_code] objectForKey:FID_68_D_BUY_PRICE_1] doubleValue];
    double askPrice = [[[[[QCData singleton] qcFeedDataDict] objectForKey:od.stock_code] objectForKey:FID_88_D_SELL_PRICE_1] doubleValue];
    
    
    if ([od.price length]<=0) {
        if ([tradeStatus.action isEqualToString:@"Buy"]) {
            resolvedPrice = bidPrice;// bid or ask price
        } else if ([tradeStatus.action isEqualToString:@"Sell"]) {
            resolvedPrice = askPrice;// bid or ask price
        }
    } else {
        resolvedPrice = od.price.doubleValue;
    }
    
    double totVal = resolvedPrice * od.quantity.longLongValue;
    
    NSDictionary *dat100 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@ %.2f", stockCurrency, totVal] forKey:[LanguageManager stringForKey:@"Order Value"]];
    [_detailArray addObject:dat100];
    
    [self.confirmTable reloadData];
    
    
    
    //    UIColor *clr;
    
    //
    
    /* ----- Prompt message for Stop / If Touch Order Type ----- */
    if ([UserPrefConstants singleton].msgPromptForStopNIfTouchOrdType) {
        if ([od.order_type isEqualToString:@"Stop"] || [od.order_type isEqualToString:@"StopLimit"]) {
            
            if ([od.order_action isEqualToString:@"Buy"]) {
                
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"To submit a Buy Stop order, Trigger Price should be > Trigger (LastTrade, BestBid, BestOffer)."]
                                             inController:self withTitle:@""];
            }
            else if ([od.order_action isEqualToString:@"Sell"]) {
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"To submit a Sell Stop Order, Trigger Price should be < Trigger (LastTrade, BestBid, BestOffer)."]
                                             inController:self withTitle:@""];
                
                
            }
        }
        else if ([od.order_type isEqualToString:@"LimitIfTouched"] || [od.order_type isEqualToString:@"MarketIfTouched"]) {
            
            if ([od.order_action isEqualToString:@"Buy"]) {
                
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"To submit a Buy If-Touch Order, Trigger Price should be < Trigger (LastTrade, BestBid, BestOffer)."]
                                             inController:self withTitle:@""];
                
                
            }
            else if ([od.order_action isEqualToString:@"Sell"]) {
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"To submit a Sell If-Touch Order, Trigger Price should be > Trigger (LastTrade, BestBid, BestOffer)."]
                                             inController:self withTitle:@""];
                
            }
        }
    }
    
    long totalTicketItem = [_detailArray count];
    self.preferredContentSize =  CGSizeMake(320, 150 + totalTicketItem*35.0);
}



- (IBAction)btnCancelPressed:(id)sender {
    
    // NSLog(@"conf cancelled");
    
    if(IS_IPAD){
        [self dismissViewControllerAnimated:YES completion:^{
            [self.skipDelegate skipDialogDidCancel];
        }];
    }else{
        [self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
            [self.skipDelegate skipDialogDidCancel];
        }];
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnSubmitPressed:(UIButton*)sender {
    //    if ([action isEqual:@"Revise"]) {
    //        //NSLog(@"skipconfirmation");
    //        [[NSNotificationCenter defaultCenter]postNotificationName:@"doReviseOrder" object:nil];
    //
    //    }
    //    else if ([action isEqual:@"Cancel"])
    //    {
    //            [[NSNotificationCenter defaultCenter]postNotificationName:@"doCancel" object:nil];
    //    }
    //
    //    else
    //
    //    {
    //        //NSLog(@"%@", [delegate class]);
    //
    //        if ([delegate class]==[StockDetailViewController class]) {
    //            [[NSNotificationCenter defaultCenter]postNotificationName:@"doTradeSD" object:nil];
    //        }
    //        else if ([delegate class]==[OrderBookandPortfolioViewController class])
    //        [[NSNotificationCenter defaultCenter]postNotificationName:@"doTrade" object:nil];
    //    }
    
    if (!sendOnce) {
        
        if(IS_IPAD){
            [self dismissViewControllerAnimated:YES completion:^{
                [self.skipDelegate skipDialogDidSubmit:self->action];
            }];
        }else{
            [CATransaction begin];
            [CATransaction setCompletionBlock:^{
                [self.skipDelegate skipDialogDidSubmit:self->action];
            }];
            [self.navigationController popViewControllerAnimated:YES];
            [CATransaction commit];
            
        }
        
        sendOnce = YES;
    }
}


- (void)setChangeValueContent:(CGFloat)changeValueContent{
    if (changeValueContent>0) {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
    } else if (changeValueContent<0) {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
    } else {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
    }
}


@end
