//
//  KnowledgeTableViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/10/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KnowledgeTableDelegate <NSObject>
@optional
-(void)tableRowDidClicked:(NSString*)segue;
@end

@interface KnowledgeTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>{
     IBOutlet UITableView *tableView;
}
@property (nonatomic, assign) CGRect frameToUse;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (weak, nonatomic) id <KnowledgeTableDelegate> tocDelegate;
@end
