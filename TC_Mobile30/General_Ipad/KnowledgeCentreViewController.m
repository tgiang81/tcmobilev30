//
//  KnowledgeCentreViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/10/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "KnowledgeCentreViewController.h"
#import "ContainerKnowledgeCenterViewController.h"
#import "ATPAuthenticate.h"
#import "KnowledgeTableViewController.h"
@interface KnowledgeCentreViewController () <KnowledgeTableDelegate>{
        BOOL runOnce;
}

@property (nonatomic, weak) ContainerKnowledgeCenterViewController *containerViewController;

@end

@implementation KnowledgeCentreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    runOnce = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableRowDidClicked:(NSString *)segue {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    
    [self.containerViewController switchToViewController:segue];
}

-(void)viewDidLayoutSubviews {
    
    if (runOnce) {
        runOnce = NO;
        CGFloat xRatio = 1024/977.0;
        CGSize scrSize = [[UIScreen mainScreen] bounds].size;
        
        _tocContainerView.frame = CGRectMake(_tocContainerView.frame.origin.x, _tocContainerView.frame.origin.y,
                                             250.0, _tocContainerView.frame.size.height);
        
        _contentContainerView.frame = CGRectMake(_tocContainerView.frame.size.width, 0,
                                                 (scrSize.width-290.0-_tocContainerView.frame.size.width)/xRatio,
                                                 _contentContainerView.frame.size.height);
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    
    if ([segue.identifier isEqualToString:@"knowledgeTable"])
    {
        KnowledgeTableViewController *tocVC = (KnowledgeTableViewController*)[segue destinationViewController];
        tocVC.frameToUse = _TOCContainer.frame;
        tocVC.tocDelegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"KnowledgeContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
