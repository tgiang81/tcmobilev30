//
//  ESettlementTableViewCell.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 18/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESettlementTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel *eSettlementLabel;

@end
