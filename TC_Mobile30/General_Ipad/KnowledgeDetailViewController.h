//
//  KnowledgeDetailViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/10/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPrefConstants.h"

@interface KnowledgeDetailViewController : UIViewController <UIWebViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webview;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, weak) IBOutlet UILabel *titleView;
@property (nonatomic, strong) IBOutlet UIButton *backButton;

- (void) reloadPage:(NSString *)urlLink andTitle:(NSString *)title;

@end
