//
//  ButtonEffect.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 29/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "ButtonEffect.h"

@implementation ButtonEffect

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


static ButtonEffect *sharedFlareViewCenter = nil;

+ (ButtonEffect *)sharedCenter {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ // make sure sharedCenter has been initialed once in a life time
        if (!sharedFlareViewCenter) {
            sharedFlareViewCenter = [[super allocWithZone:NULL] init];
        }
    });
    return sharedFlareViewCenter;
}

-(void) flarify: (UIView* ) chilView inParentView:(UIView*) rootView withColor: (UIColor *)fillColor enableUponCompletion:(BOOL)enabled {
    
    chilView.userInteractionEnabled = NO; // prevent user presses continuously
    flareColor = fillColor;
    chilView.transform = CGAffineTransformMakeTranslation(0, 0);
    /*[UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:0.2 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
     chilView.transform = CGAffineTransformMakeScale(0.5, 0.5);
     
     } completion:^(BOOL finished){
     UIView* vortexView = [[UIView alloc] initWithFrame:CGRectMake(chilView.frame.origin.x, chilView.frame.origin.y, chilView.frame.size.width, chilView.frame.size.height)];
     
     CAShapeLayer *vortexLayer = [CAShapeLayer layer];
     vortexView.bounds = chilView.bounds;
     //set colors
     [vortexLayer setStrokeColor:[flareColor CGColor]];
     [vortexLayer setFillColor:[[UIColor clearColor] CGColor]];
     [vortexLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:vortexView.bounds] CGPath]];
     [vortexView.layer addSublayer:vortexLayer];
     [rootView addSubview:vortexView];
     
     //Animate circle
     [vortexView setTransform:CGAffineTransformMakeScale(0, 0)];
     [UIView animateWithDuration:1 animations:^{
     [vortexView setTransform:CGAffineTransformMakeScale(1.3, 1.3)];
     
     } completion:^(BOOL finished) {
     vortexView.hidden = YES;
     //start next animation
     // [self createFlares:chilView rootView:rootView];
     }];
     
     }];*/
    [self createFlares:chilView rootView:rootView enableUponCompletion:enabled];
    
}

-(void) createFlares: (UIView*) chilView rootView: (UIView *) rootView enableUponCompletion:(BOOL)enabled {
    
    
    [chilView setTransform:CGAffineTransformMakeScale(0.8, 0.8)];
    //animate icon
    
    [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.1 initialSpringVelocity:4.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        chilView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        
    } completion:^(BOOL finished){
        chilView.userInteractionEnabled = enabled;
    }];
    
    
//    [UIView animateWithDuration:0.3/1.5 animations:^{
//        chilView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.3/2 animations:^{
//            chilView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
//        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:0.3/2 animations:^{
//                chilView.transform = CGAffineTransformIdentity;
//            }];
//        }];
//    }];
    
    
    //add circles around the icon
   /* int numberOfFlares = 20;
    CGPoint center = chilView.center;
    float radius= 16;
    BOOL isBurlyFlare = YES;;
    
    for (int i = 0; i<numberOfFlares; i++) {
        
        //x(t) = r cos(t) + j => r = radius; t= M_PI/numberOfCircles*i*2; j = center.x
        //y(t) = r sin(t) + j => r = radius; t= M_PI/numberOfCircles*i*2; j = center.y
        // TO DO - RECHECK TO MIMIC REAL EFFECT
        
        float x = radius*cos(M_PI/numberOfFlares*i*2) + center.x;
        float y = radius*sin(M_PI/numberOfFlares*i*2) + center.y;
        
        float circleRadius = 10;
        if (isBurlyFlare) {
            circleRadius = 5;
            isBurlyFlare = NO;
        }else{
            isBurlyFlare = YES;
        }
        
        UIView* vortexView = [[UIView alloc] initWithFrame:CGRectMake(x, y, circleRadius, circleRadius)];
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        [circleLayer setStrokeColor:[flareColor CGColor]];
        [circleLayer setFillColor:[flareColor CGColor]];
        [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:vortexView.bounds] CGPath]];
        [vortexView.layer addSublayer:circleLayer];
        [rootView addSubview:vortexView];*/
        

//        [UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//            //[vortexView setTransform:CGAffineTransformMakeTranslation(radius/3*cos(M_PI/numberOfFlares*i*2), radius/3*sin(M_PI/numberOfFlares*i*2))];
//         //   [vortexView setTransform:CGAffineTransformScale(vortexView.transform, 0.01, 0.01)];
//        } completion:^(BOOL finished) {
//         //   [vortexView setTransform:CGAffineTransformMakeScale(0, 0)];
//            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//
//                chilView.transform = CGAffineTransformIdentity;
//            } completion:^(BOOL finished){
//                
//                chilView.userInteractionEnabled = enabled;
//            }];
//        }];
    
        
    //}
}


@end
