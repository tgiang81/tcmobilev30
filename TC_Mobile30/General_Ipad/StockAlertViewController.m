//
//  StockAlertViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 10/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "StockAlertViewController.h"
#import "UserPrefConstants.h"
#import "Base64.h"
#import "NSData-AES.h"
#import "NSString-Hash.h"
#import "JWT.h"
#import "AFNetworking.h"
#import "NSString+MD5.h"
#import "ATPAuthenticate.h"
#import "Reachability.h"
#import <sys/utsname.h>
#import "APIStockAlert.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "AlertModel.h"
@interface StockAlertViewController () <UIWebViewDelegate, UIAlertViewDelegate>{
    IBOutlet UIWebView *webView;
    IBOutlet UIActivityIndicatorView *activityIndicatorView;
    NSString *publicKey;
}

@end

@implementation StockAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [activityIndicatorView startAnimating];
    // Do any additional setup after loading the view.
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CheckNotifications

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSString *iOSversion = [[UIDevice currentDevice] systemVersion];
    NSString *prefix = [[iOSversion componentsSeparatedByString:@"."] firstObject];
    float versionVal = [prefix floatValue];
    
    if (versionVal >= 8)
    {
        if ([[UIApplication sharedApplication] currentUserNotificationSettings].types != UIUserNotificationTypeNone)
        {
            
            [self getPublicKeyURL];
            
        }
        else
        {
            
            NSString *msg = [LanguageManager stringForKey:@"Please press ON to enable Push Notification"];
            UIAlertView *alert_push = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"Push Notification Service Disable"] message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Setting", nil];
            alert_push.tag = 2;
            [alert_push show];
        }
    }
    else
    {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types != UIRemoteNotificationTypeNone)
            
        {
            
            [self getPublicKeyURL];
            
        }
        else
        {
            NSString *msg = [LanguageManager stringForKey:@"Please press ON to enable Push Notification"];
            UIAlertView *alert_push = [[UIAlertView alloc] initWithTitle:[LanguageManager stringForKey:@"Push Notification Service Disable"] message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alert_push.tag = 1;
            [alert_push show];
        }
        
    }
    
}

- (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key {
    return [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKeyCBC:key];
}

- (void) getPublicKeyURL{
    NSString *bhCode = [UserPrefConstants singleton].brokerCode;
    NSString *appID = [UserPrefConstants singleton].appID;
    NSString *url = [UserPrefConstants singleton].stockAlertGetPublicURL;
    
    [APIStockAlert getPublicKeyWithAppURL:url appId:appID bhCode:bhCode completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            NSLog(@"Error: %@", error.localizedDescription);
        }else{
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                 options:kNilOptions
                                                                   error:&error];
            NSString *algString = [[[json objectForKey:@"keys"] objectAtIndex:0] objectForKey:@"alg"];
            NSString *eString = [[[json objectForKey:@"keys"] objectAtIndex:0] objectForKey:@"e"];
            NSString *kidString = [[[json objectForKey:@"keys"] objectAtIndex:0] objectForKey:@"kid"];
            NSString *ktyString = [[[json objectForKey:@"keys"] objectAtIndex:0] objectForKey:@"kty"];
            NSString *nString = [[[json objectForKey:@"keys"] objectAtIndex:0] objectForKey:@"n"];
            
            self->publicKey = [NSString stringWithFormat:@"%@%@%@%@%@",algString,eString,kidString,ktyString,nString];
            
            [self requestAlerts:algString andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString];
        }
    }];
}

- (void) requestAlerts:(NSString *)algSring
               andEString:(NSString *)eString
             andKidString:(NSString *)kidString
             andKtyString:(NSString *)ktyString
               andNString:(NSString *)nString{
    NSString *bhCode = [UserPrefConstants singleton].bhCode;
    
    NSString *jwtToken = [APIStockAlert generateJWTToken:algSring andEString:eString andKidString:kidString andKtyString:ktyString andNString:nString andAppId:@"MD"];
//    jwtToken
    
    NSString *urlString = [NSString stringWithFormat:@"%@?jwt=%@&bh=%@&appId=MD",[UserPrefConstants singleton].stockAlertURL,jwtToken,bhCode];
    
    NSLog(@"Url String Stock Alert %@",urlString);
    NSURL* fileURL = [NSURL URLWithString:urlString];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:fileURL];
    [webView loadRequest:urlRequest];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //return FALSE; //to stop loading
    NSURL *url = [request URL];
    NSString *urlStr = url.absoluteString;
    NSString *protocolPrefixStockAlert = @"stockalert://";
    
    if ([[urlStr lowercaseString] hasPrefix:protocolPrefixStockAlert]) {
        
        [[ATPAuthenticate singleton] timeoutActivity];
        
        return NO;
    }
    
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    activityIndicatorView.alpha = 1;
    [activityIndicatorView startAnimating];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    activityIndicatorView.alpha = 0;
    [activityIndicatorView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    activityIndicatorView.alpha = 0;
    [activityIndicatorView stopAnimating];
    
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    if ([webView isLoading]) {
        [webView stopLoading];
    }
    
    if (webView.delegate == self) {
        webView.delegate = nil;
    }
    
}

- (IBAction)tapToReload:(id)sender{
    [self getPublicKeyURL];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            if ([webView isLoading]) {
                [webView stopLoading];
            }
        }else{
            NSURL* settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:settingsURL];
        }
    }
}

    
@end
