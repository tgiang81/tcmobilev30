//
//  LoginViewController.m
//  TCUniversal
//
//  Created by Scott Thoo on 9/25/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "LoginViewController.h"
#import "ATPAuthenticate.h"
#import "VertxConnectionManager.h"
#import "UserPrefConstants.h"
//#import <Parse/Parse.h>
#import "ActionSheetStringPicker.h"
#import "MBProgressHUD.h"
#import "BrokerHouseIcon.h"
#import  <SystemConfiguration/SystemConfiguration.h>
#import <Crashlytics/Crashlytics.h>

#import "AppControl.h"
#import "AppDelegate.h"
#import "UserDefaultData.h"
#import "QCData.h"
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>


#import "StockAlertAPI.h"
#import "LanguageKey.h"
//#define LMS_SERVER_UAT_URL @"http://lms-my.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?id=com.n2n.ipad.tcmobilemultiloginuat&ver=0&device=I"

@interface LoginViewController () <AppDelegateDelegate, ATPDelegate, ProcessViewDelegate, MFMailComposeViewControllerDelegate>
{
    
    ATPAuthenticate *atp ;
    //  NSMutableDictionary *brokerListIdDict;
    //  NSMutableDictionary *brokerListDetailDict;
    
    NSMutableDictionary *ipadLMSDictionary; // main LMS
    NSMutableArray *allowedBrokerListArray; // allowed broker for this device
    NSMutableArray *currentBrokerListArray;
    
    NSString *lmsServerFromBundleID;
    
    //    NSMutableArray *brokerListATPServer; //Server address
    //    NSMutableArray *vertexAddress;
    //    NSMutableArray *BrokersponserID;
    
    BOOL displayingFront;
    long subMenuLevel;
    BOOL runOnce;
    
    BOOL activeTouchID;
    BOOL isEquitySelected;
    //   NSMutableArray *allEntries;
}

@property (weak, nonatomic) IBOutlet UIView *loginContainerView;
@property (weak, nonatomic) IBOutlet UIView *loginContainerView2;
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRememberMe;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblErrorMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnBrokerList;
@property (weak, nonatomic) IBOutlet UIButton *btnBrokerListName;
@property (weak, nonatomic) IBOutlet UILabel *lblBrokerName;
@property (weak, nonatomic) IBOutlet UIView *brokerSelectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewBrokerList;
@property (weak, nonatomic) IBOutlet UIView *DisclaimerView;
@property (weak, nonatomic) IBOutlet UIImageView *Backgroundimage;
@property (weak, nonatomic) IBOutlet UILabel *Disclaimertext;
@property (weak, nonatomic) IBOutlet UIView *DisclaimertextBackground;
@property (weak, nonatomic) IBOutlet UIButton *AcceptBtn;
@property (weak,nonatomic) IBOutlet UIButton *btnBrokerListActive;
@property (weak,nonatomic) IBOutlet UIButton *btnBrokerListActiveName;
@property (weak, nonatomic) IBOutlet UIButton *releaseNoteBtn;

// For LoginViewController 2
@property (weak, nonatomic) IBOutlet UITextField *view2TxtUserName;
@property (weak, nonatomic) IBOutlet UITextField *view2TxtPassword;
@property (weak, nonatomic) IBOutlet UIButton *view2LoginButton;
@property (weak, nonatomic) IBOutlet UIButton *view2ThumbPrintButton;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *marketSelectionButton;
@property (weak, nonatomic) IBOutlet UIView *customMenuBarView;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIImageView *companyLogo;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIImageView *bigAppLogo;

// Active Touch ID Screen
@property (weak, nonatomic) IBOutlet UIView *activeView;
@property (weak, nonatomic) IBOutlet UILabel *activeBrokerNameText;
@property (weak, nonatomic) IBOutlet UITextView *termAndConditionTextView;
@property (weak, nonatomic) IBOutlet UITextField *activeTextUsername;
@property (weak, nonatomic) IBOutlet UITextField *activeTextPassword;

@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerItem *playerItem;

@property (strong, nonatomic) NSString *touchIDUsername;
@property (strong, nonatomic) NSString *touchIDPassword;
@property (weak, nonatomic) IBOutlet UIButton *DeclineBtn;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;


- (IBAction)DeclineBtnPressed:(id)sender;
- (IBAction)AcceptbtnPressed:(id)sender;
- (IBAction)marketSelectionButtonPressed:(id)sender;

@end

@implementation LoginViewController

@synthesize btnBrokerList;

#define kNSURLSessionTimeout 20.0

- (IBAction)marketSelectionButtonPressed:(id)sender{
    isEquitySelected = !isEquitySelected;
    
    [self.view2TxtUserName setText:@""];
    [self.view2TxtPassword setText:@""];
    if (isEquitySelected) {
        
        NSLog(@"Equity Selected %d",isEquitySelected);
       
        [_marketSelectionButton setImage:[UIImage imageNamed:@"MarketSelect1.png"] forState:UIControlStateNormal];
          [self clickedBrokerBtnByTag:1];
    }else{
        NSLog(@"Futures Selected %d",isEquitySelected);
        
        [_marketSelectionButton setImage:[UIImage imageNamed:@"MarketSelect2.png"] forState:UIControlStateNormal];
          [self clickedBrokerBtnByTag:2];
    }
}

- (IBAction) view2TouchIDButtonClicked:(id)sender{
    
}


- (IBAction)openReleaseNote:(id)sender {
    [[UserPrefConstants singleton] showLocalReleaseNotesInView:self.view];
}

-(bool)canDevicePlaceAPhoneCall {
    /*
     
     Returns YES if the device can place a phone call
     
     */
    
    // Check if the device can place a phone call
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
        // Device supports phone calls, lets confirm it can place one right now
        CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
        CTCarrier *carrier = [netInfo subscriberCellularProvider];
        NSString *mnc = [carrier mobileNetworkCode];
        if (([mnc length] == 0) || ([mnc isEqualToString:@"65535"])) {
            // Device cannot place a call at this time.  SIM might be removed.
            return NO;
        } else {
            // Device can place a phone call
            return YES;
        }
    } else {
        // Device does not support phone calls
        return  NO;
    }
}

- (IBAction) forgotPassword:(id)sender{
    //https://www.amesecurities.com.my/gc/main.jsp
    UIAlertView  *alert = [[UIAlertView alloc]initWithTitle:@"Notice!" message:[NSString stringWithFormat:@"This features is temporary disabled"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}
- (IBAction) clickCall:(id)sender{
    if ([self canDevicePlaceAPhoneCall]) {
        NSURL *phoneNumber = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",[UserPrefConstants singleton].phoneNumber]];
        [[UIApplication sharedApplication] openURL:phoneNumber];
    }else{
        UIAlertView  *alert = [[UIAlertView alloc]initWithTitle:@"Notice!" message:[NSString stringWithFormat:@"Phone call is not supported on this device, please call %@",[UserPrefConstants singleton].phoneNumber] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)clickEmail:(id)sender{
    if ([MFMailComposeViewController canSendMail]) {
        
        NSBundle *bundle = [NSBundle mainBundle];
        NSDictionary *info = [bundle infoDictionary];
        NSString *emailTitle = [NSString stringWithFormat:@"%@",[info objectForKey:@"CFBundleDisplayName"]];
        // Email Content
        NSString *messageBody =  [NSString stringWithFormat:@"Your Name: \nPhone: \nEmail: \nPlease summarize your feature requests or bug reports here:"];
        // To address
        NSArray *toRecipients = [NSArray arrayWithObjects:[UserPrefConstants singleton].emailLink,nil];
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        
        [mc setSubject:emailTitle];
        [mc setToRecipients:toRecipients];
        
        [mc setMessageBody:messageBody isHTML:NO];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }else{
        
        UIAlertView  *alert = [[UIAlertView alloc]initWithTitle:TC_Pro_Mobile message:@"Email has not been setup on this device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}




-(BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - LeftMenuButton

- (IBAction)clickMenuButton:(id)sender{
    
}

#pragma mark - Video background

-(AVPlayerLayer*)playerLayer{
    if(_playerLayer==nil){
        
        // find movie file
        NSString *moviePath = [[NSBundle mainBundle] pathForResource:@"login2" ofType:@"mp4"];
        NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
        _playerLayer = [AVPlayerLayer playerLayerWithPlayer:[[AVPlayer alloc]initWithURL:movieURL]];
        _playerLayer.frame = CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height);
        _playerLayer.videoGravity = @"AVLayerVideoGravityResizeAspectFill";
        [_playerLayer.player play];
        
    }
    return _playerLayer;
}

-(void)replayMovie:(NSNotification *)notification
{
    [self.playerLayer.player seekToTime:kCMTimeZero];
    [self.playerLayer.player play];
}

#pragma mark - Languages TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2==0) {
        
        cell.backgroundColor = kCellLightGray1;
    } else {
        
        cell.backgroundColor = kCellLightGray2;
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // NSLog(@"[[[LanguageManager defaultManager] supportedLanguages] count] %lu",(unsigned long)[[[LanguageManager defaultManager] supportedLanguages] count]);
    return [[[LanguageManager defaultManager] supportedLanguages] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *defaultCell = [tableView dequeueReusableCellWithIdentifier:@"default"];
    if (defaultCell==nil) {
        defaultCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"default"];
    }
    
    
    NSString *key = [[[LanguageManager defaultManager] supportedLanguages] objectAtIndex:indexPath.row];
    defaultCell.textLabel.text = [LanguageManager getLanguage:key andKey:@"LanguageTitle"];
    // defaultCell.textLabel.textColor = [UIColor whiteColor];
    
    if ([key isEqualToString:[LanguageManager defaultManager].language]) {
        [defaultCell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [defaultCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    defaultCell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"lang_%@.png",key]];
    
    return defaultCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *selectedLang = [[[LanguageManager defaultManager] supportedLanguages] objectAtIndex:indexPath.row];
    // NSLog(@"Selected Language%@",selectedLang);
    
    [[LanguageManager defaultManager] setLanguage:selectedLang];
    
    // [UserPrefConstants singleton].currentLanguage = selectedLang;
    [_columnPickerPopover dismissPopoverAnimated:YES];
    
    [self updateLanguage:selectedLang];
}


-(void)updateLanguage:(NSString*)selectedLang {
    // reload
    // LanguageManager *lm = [LanguageManager defaultManager];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    
    // update icon
    [_languageBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"lang_%@.png",selectedLang]] forState:UIControlStateNormal];
    
    NSString *helpInfoText = @"";
    if ([[LanguageManager defaultManager].language isEqualToString:@"en"]) {
        helpInfoText = [UserPrefConstants singleton].LoginHelpText_EN;
    } else if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
        helpInfoText = [UserPrefConstants singleton].LoginHelpText_CN;
    }
    
    _helpInfoTextView.text = helpInfoText;
}

/*
 LOGIN_INIT, // before PK
 LOGIN_PK, // before Key2
 LOGIN_KEY2, // before session2
 LOGIN_SESSION2, // before e2ee or login
 LOGIN_E2EE, // before e2ee
 LOGIN_LOGIN //  before login
 LOGIN_SUCCESS // success!
 */


#pragma mark - ATP Delegate
-(void)updateLoginProgress:(LoginStages)progress {
    
    NSString *msg = @"";
    
    switch (progress) {
        case LOGIN_INIT:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"0%"}];
            break;
        case LOGIN_PK:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"20%"}];
            break;
        case LOGIN_KEY2:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"40%"}];
            break;
        case LOGIN_SESSION2:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"60%"}];
            break;
        case LOGIN_E2EE:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"80%"}];
            break;
        case LOGIN_LOGIN:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"95%"}];
            break;
        case LOGIN_SUCCESS:
            msg = [LanguageManager stringForKey:@"Logging In... %@" withPlaceholders:@{@"%loginStage%":@"100%"}];
            break;
        default:
            break;
    }
    
    [self showProcessWithMessage:msg showRetry:NO showClose:NO activity:YES];
    
}

#pragma mark - Process View


-(void)showProcessWithMessage:(NSString*)message showRetry:(BOOL)retry showClose:(BOOL)close activity:(BOOL)activity {
    
    if (_processView==nil) {
        
        NSDictionary *setupDict = @{@"message": message,
                                    @"retry" : [NSNumber numberWithBool:retry],
                                    @"close" : [NSNumber numberWithBool:close],
                                    @"activity" : [NSNumber numberWithBool:activity],
                                    };
        
        [self performSegueWithIdentifier:@"segueProcessID" sender:setupDict];
        
    } else {
        
        // update
        self.processView.messageLabel.text = message;
        [self.processView.retryButton setHidden:!retry];
        [self.processView.closeButton setHidden:!close];
        [self.processView.activityView setHidden:!activity];
        [self.processView updateButton];
    }
    
}

-(void)removeProcessView {
    if (self.processView!=nil) {
        [self.processView dismissViewControllerAnimated:NO completion:^(void) {
            self.processView = nil;
        }];
    }
}

#pragma mark - ProcessView Delegate
-(void)btnCloseDidClicked {
    self.processView = nil;
}

-(void)btnRetryDidClicked {
    self.processView = nil;
    [self getPLISTData];
}

#pragma mark - View LifeCycle Delegates

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueProcessID"]) {
     
        // NSLog(@"destinationViewController %@",segue.destinationViewController);
        
        self.processView = (ProcessViewController*)segue.destinationViewController;
        self.processView.delegate = self;
        
        NSDictionary *dict = (NSDictionary*)sender;
        
        self.processView.setupDict = dict;

    }
    
    if ([segue.identifier isEqualToString:@"segueLoginTOC"]) {
        LoginTermsViewController *vc = (LoginTermsViewController*)segue.destinationViewController;
        
        NSString *tocText = @"";
        if ([[LanguageManager defaultManager].language isEqualToString:@"en"]) {
            tocText = [UserPrefConstants singleton].termAndConditions_EN;
        } else if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
            tocText = [UserPrefConstants singleton].termAndConditions_CN;
        }
        
        vc.tocText = tocText;
    }
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    _playerLayer.frame = self.view.bounds;
    

    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    
    isEquitySelected = YES;
    [UserPrefConstants singleton].isLoginPage2Activated = NO;
    if([UserPrefConstants singleton].isLoginPage2Activated){
        _releaseNoteBtn.hidden = YES;
        _releaseNoteBtn.enabled = NO;
        _logoImage.hidden = YES;
        _loginContainerView.hidden = YES;
        _versionLabel.hidden = YES;
        self.loginContainerView2.hidden = NO;
        self.loginContainerView2.alpha = 0;
        [_bgImage setHidden:NO];
        
    }else{
        [_bgImage setHidden:YES];
        self.loginContainerView2.hidden = YES;
        self.loginContainerView2.alpha = 0;
        [_bgImage setHidden:YES];
        [_logoImage setHidden:YES];
        [_menuView setHidden:YES];
        [_menuButton setHidden:YES];
        [_annoucementWebView setHidden:YES];
        [_customMenuBarView setHidden:YES];
        [_callButton setHidden:YES];
        [_emailButton setHidden:YES];
        [_companyLogo setHidden:YES];
        [_forgotPasswordButton setHidden:YES];
        [_bigAppLogo setHidden:YES];
    }
    
    //Hide ForgotPassword button base on each Broker's config
    if (![UserPrefConstants singleton].isEnabledPassword) {
        [_forgotPasswordButton setHidden:YES];
    }
    
    if ([UserPrefConstants singleton].AnnouncementURL.length>0) {
        NSString *urlStr = [NSString stringWithFormat:@"%@",[UserPrefConstants singleton].AnnouncementURL];
        DLog(@"urlStr %@",urlStr);
        NSURL *url = [NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
        
        NSString* js =
        @"var meta = document.createElement('meta'); " \
        "meta.setAttribute( 'name', 'viewport' ); " \
        "meta.setAttribute( 'content', 'width = device-width, initial-scale=1.0, minimum-scale=0.2, maximum-scale=5.0; user-scalable=1;' ); " \
        "document.getElementsByTagName('head')[0].appendChild(meta)";
        
        [self.annoucementWebView stringByEvaluatingJavaScriptFromString: js];
        NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url];
      
        [self.annoucementWebView.scrollView setBounces:NO];
        
        [self.annoucementWebView loadRequest:req];
        [self.annoucementWebView setScalesPageToFit:NO];
    }
    
    if (![UserPrefConstants singleton].isSupportedMultipleLanguage) {
        _languageBtn.enabled = NO;
    }
    
    self.languageTableViewVC = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    self.languageTableViewVC.tableView.delegate=self;
    self.languageTableViewVC.tableView.dataSource=self;
    self.languageTableViewVC.tableView.tag=2;
    
    atp = [ATPAuthenticate singleton];
    atp.delegate = self;
    [_versionLabel.layer setCornerRadius:5.0f];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.delegateDelegate = self;
    
    [self setNeedsStatusBarAppearanceUpdate];
    runOnce = YES;
    
    
    if (DEBUG_MODE==1) {
        [_lblErrorMessage setHidden:NO];
    } else {
        [_lblErrorMessage setHidden:YES];
    }
    
    _lblErrorMessage.layer.cornerRadius = 5.0;
    _lblErrorMessage.layer.masksToBounds = YES;
    
    
    _loginView.layer.cornerRadius = 10.0;
    _loginView.layer.masksToBounds = YES;
    _brokerSelectionView.layer.cornerRadius = 10.0;
    _brokerSelectionView.layer.masksToBounds = YES;
    _brokerSelectionView.alpha = 0.8;
    
    _activeView.layer.cornerRadius = 10.0;
    _activeView.layer.masksToBounds = YES;
    
    
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    //NSLog(@"bundleIdentifier %@", bundleIdentifier);
    
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_CIMBMY_UAT]) {
        [_lblTitle setHidden:YES];
        _logoImage.image = [UIImage imageNamed:@"AppLogoMY"];
    }
    else if([bundleIdentifier isEqualToString:BUNDLEID_CIMBSG_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_CIMBSGSTAG]){
        [_lblTitle setHidden:YES];
        _logoImage.image = [UIImage imageNamed:@"AppLogoSG"];
         [_logoImage setFrame:CGRectMake(403, 32, 200, 44)];
        
    }else if([bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS]){
        _logoImage.image = [UIImage imageNamed:@"AppLogoAbacus"];
        [_logoImage setFrame:CGRectMake(403, 32, 210, 90)];
        [_lblTitle setHidden:YES];
    }else if([bundleIdentifier isEqualToString:BUNDLEID_AMSEC_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_AMFUTURES_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_AMACCESS]){
        _logoImage.image = [UIImage imageNamed:@"AppLogoAmAccess"];
        [_logoImage setFrame:CGRectMake(359, 32, 297, 125)];
        [_lblTitle setHidden:YES];
    }
    else{
        _logoImage.hidden = YES;
    }
    
    if ((![bundleIdentifier isEqualToString:BUNDLEID_MULTILOGIN])&&(![bundleIdentifier isEqualToString:BUNDLEID_MULTILOGIN_UAT])) {
        _lblTitle.text =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        _lblBrokerName.text = @"";
        _activeBrokerNameText.text = @"";

    
    }
    
    if (DEBUG_MODE==1) {
        _lblTitle.text = @"TCPro Tab";
    }

    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
            lmsServerFromBundleID = [NSString stringWithFormat:@"http://nopl-ph.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?id=%@&ver=0&device=I",bundleIdentifier];
        
    
    }else{
        
        lmsServerFromBundleID = [NSString stringWithFormat:@"http://nogl.asiaebroker.com/ebcServlet/LMSGetSetting.jsp?id=%@&ver=0&device=I",bundleIdentifier];
    }
    
    
    // update Version label
    _versionLabel.text = [NSString stringWithFormat:@"v%@ (%@)",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey]];
    
     NSLog(@"lmsServerFromBundleID %@",lmsServerFromBundleID);
    
    subMenuLevel = 0;
    displayingFront = YES;
    
    
    for(UIView *v in [self.view subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
            btn.layer.masksToBounds = YES;
        }
    }
    
    _versionLabel.layer.cornerRadius = kButtonRadius*_versionLabel.frame.size.height;
    _versionLabel.layer.masksToBounds = YES;
    
    [_DisclaimerView.layer setCornerRadius:5.0f];
    [_DisclaimerView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_DisclaimerView.layer setBorderWidth:2.0f];
    [_Backgroundimage.layer setCornerRadius:5.0f];
    [_Backgroundimage.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_Backgroundimage.layer setBorderWidth:2.0f];
    
    [_DisclaimertextBackground.layer setCornerRadius:5.0f];
    [_DisclaimertextBackground.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_DisclaimertextBackground.layer setBorderWidth:2.0f];
    [_Disclaimertext setTextColor:[UIColor whiteColor]];
    //_Disclaimertext.textContainerInset = UIEdgeInsetsMake(10.0f, 10.0f,10.0f, 10.0f);
    [_AcceptBtn.layer setCornerRadius:5.0f];
    [_AcceptBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_AcceptBtn.layer setBorderWidth:2.0f];
    [_DeclineBtn.layer setCornerRadius:5.0f];
    [_DeclineBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_DeclineBtn.layer setBorderWidth:2.0f];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self.txtUserName setDelegate:self];
    [self.txtPassword setDelegate:self];
    //Add Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(atpLoginStatusReceived:) name:@"atpLoginStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pnWebSocketConnected:) name:@"pnWebSocketConnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vertxGetExchangeInfoFinished:) name:@"vertxGetExchangeInfoFinished" object:nil];
    
    //    BrokerList Part
    //    brokerList              = [NSMutableArray array];
    //    brokerListATPServer     = [NSMutableArray array];
    //    BrokersponserID         = [NSMutableArray array];
    //    vertexAddress           = [NSMutableArray array];
    //    allEntries              = [NSMutableArray array];
    allowedBrokerListArray = [[NSMutableArray alloc] init];
    currentBrokerListArray = [[NSMutableArray alloc] init];
    //    brokerListIdDict        = [NSMutableDictionary dictionary];
    //    brokerListDetailDict    = [NSMutableDictionary dictionary];
    ipadLMSDictionary       = [[NSMutableDictionary alloc] init];
    
    
    
    [btnBrokerList setEnabled:NO];

    
    //dummy
    self.txtPassword.text = @"";
    self.activeView.hidden = YES;
    [self.loginView setAlpha:0];
    [self.DisclaimerView setHidden:YES];
    
    for (UIView *view in _loginView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton*)view;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
        }
    }
    
    for (UIView *view in _activeView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton*)view;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
        }
    }
}


-(void)delegateDidBecomeActive {
    
   // NSLog(@"delegateDidBecomeActive");
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(replayMovie:)
                                                 name: AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];
    [self.playerLayer.player seekToTime:kCMTimeZero];
    [_playerLayer.player play];
}

-(void)delegateWillResignActive {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [_playerLayer.player pause];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self delegateWillResignActive];
}


- (void)viewWillAppear:(BOOL)animated;
{
    
    [super viewWillAppear:animated];
    
    [self delegateDidBecomeActive];
    
    [self.view2TxtUserName setText:@""];
    [self.view2TxtPassword setText:@""];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // touch ID
    if ([defaults objectForKey:@"TouchIDMode"]==nil) {
        // hide touch id
        [self showTouchID:NO];
    } else {
        BOOL isTouchID = [defaults boolForKey:@"TouchIDMode"];
        if (isTouchID) {
            
            long status = [UserPrefConstants checkAuthenticateByTouchId];
            if (status==-7) { [self showTouchID:NO]; [defaults removeObjectForKey:@"TouchIDMode"]; }
            if (status==-6) { [self showTouchID:NO]; [defaults removeObjectForKey:@"TouchIDMode"]; }
            if (status==0) [self showTouchID:YES];
            
        } else {
            [self showTouchID:NO];
        }
    }
    
    
}
- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    atp.delegate = nil;
    
    [[QCData singleton] clearAllData];
    
    [UserPrefConstants singleton].userSelectingStockCode = nil;
    
    [atp invalidateIdleCheckTimer];
    
    // clear previous touchIdUsername/pass
    _touchIDUsername = @"";
    _touchIDPassword = @"";
    
    if ([UserDefaultData getTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode]) {
        // touchID activated
        [_deactivateBtn setHidden:NO];
    } else {
        [_deactivateBtn setHidden:YES];
    }
    
    [_languageBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"lang_%@.png",[[LanguageManager defaultManager] getInitialLanguage]]] forState:UIControlStateNormal];
    
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"disclaimer"] isEqual:@"YES"]) {

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDelay:2.2];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        
        
        [self.loginView setAlpha:0.7];
        [self.loginContainerView2 setAlpha:0.7];
        
        [UIView commitAnimations];
    }
    else{
        CGRect animTitleFrameEndPosition = self.lblTitle.frame;
        animTitleFrameEndPosition.origin.y = 10;
        self.lblTitle.frame = animTitleFrameEndPosition;
        [self.DisclaimerView setHidden:NO];
        [self.view bringSubviewToFront:self.DisclaimerView];
    }
    
    [self.bgImage setImage:[UIImage imageNamed:@"bgrd"]];
    if ([UserPrefConstants singleton].isEnabledCustomBackground) {
        
      //  [UserPrefConstants singleton].customBackgroundURL = @"http://www.n2nconnect.com/sites/default/files/about-building.png";
        
        [self downloadImageFromURL:[UserPrefConstants singleton].customBackgroundURL];
    }
    
    if (runOnce) {
        runOnce = NO;
        [[LanguageManager defaultManager] translateStringsForViewController:self];
        if(![UserPrefConstants singleton].isLoginPage2Activated){
           [self.view.layer insertSublayer:self.playerLayer atIndex:0];
        }
        //  [self.view.layer insertSublayer:self.playerLayer atIndex:0];
        
        [self getPLISTData];
    }
    
    
   // NSLog(@"lgogout %@",[UserPrefConstants singleton].logoutDueToKicked);
    
    
    if ([[UserPrefConstants singleton].logoutDueToKicked count]>0) {
       
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:[[UserPrefConstants singleton].logoutDueToKicked objectForKey:@"message"]
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:[LanguageManager stringForKey:@"Ok"]
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 
                                 
                             }];
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        [UserPrefConstants singleton].logoutDueToKicked = [NSDictionary new];
    }
    
    [self updateLanguage:[LanguageManager defaultManager].language];
    
    
}

-(void) downloadImageFromURL :(NSString *)imageUrl{
    
    NSURL  *url = [NSURL URLWithString:imageUrl];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSLog(@"Downloading started...");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"dwnld_image.png"];
        NSLog(@"FILE : %@",filePath);
        [urlData writeToFile:filePath atomically:YES];
        UIImage *image1=[UIImage imageWithContentsOfFile:filePath];
        self.bgImage.image=image1;
        NSLog(@"Completed...");
    }
    
}


- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //NSLog(@"LoginViewController viewDidDisappear");
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"atpLoginStatus" object:nil];
    
    [self.loginView setHidden:NO];
    [self.activeView setHidden:YES];
    
    self.activeTextUsername.text = @"";
    self.activeTextPassword.text = @"";
    
    activeTouchID = NO;
    //    [brokerList removeAllObjects];
    
    //    [brokerListATPServer removeAllObjects];
    //    [vertexAddress removeAllObjects];
    // [allEntries removeAllObjects];
    /* _lblTitle = nil;
     _lblErrorMessage = nil;
     _lblBrokerName = nil;
     _loginContainerView = nil;
     _loginView = nil;
     _btnRememberMe = nil;
     _txtUserName = nil;
     _txtPassword = nil;
     btnBrokerList = nil;
     _brokerSelectionView = nil;
     _scrollViewBrokerList= nil;
     _DisclaimerView = nil;
     _Backgroundimage = nil;
     _Disclaimertext = nil;
     _AcceptBtn = nil;
     _DeclineBtn = nil;*/
    /*   for (CALayer* layer in [self.view.layer sublayers])
     {
     [layer removeAllAnimations];
     }*/
}


#pragma mark - Other methods

-(void)timeOutVertxConnect {
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        
        [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to open Vertx. Check your internet connection and try again. If problem persist, please contact us."]
                           showRetry:YES showClose:YES activity:NO];
        
    });
    
}

//
//-(void)storeMessageLocally:(PFObject*)message
//{
//    NSArray * allKeys = [message allKeys];
//    NSMutableDictionary * retDict = [[NSMutableDictionary alloc] init];
//
//    for (NSString * key in allKeys)
//    {
//        [retDict setObject:[message objectForKey:key] forKey:key];
//    }
//
//    ////NSLog(@"retDict : %@",retDict);
//}

-(void)clearBrokerIcons {
    for(UIView * subView in self.scrollViewBrokerList.subviews ) // here write Name of you ScrollView.
    {
        
        if([subView isKindOfClass:[BrokerHouseIcon class]]) // Check is SubView Class Is UILabel class?
        {
            [subView removeFromSuperview];
        }
    }
    
    
}

-(void)populateBrokerIconsWithArray:(NSMutableArray*)brArray andSortKey:(NSString*)sortKey
{
    
    [self clearBrokerIcons];
    
    __block float x = 80;
    __block float y = 10;
    __block float gapX = 46;
    __block float gapY = 20;
    // int tag = 0;
    // //NSLog(@"Broker List %d",brokerList.count);
    
    
    //    NSMutableDictionary *userDictionary=[[NSMutableDictionary alloc]init];
    //
    //    for (int x=0;x<brokerList.count;x++)
    //    {
    //        NSMutableDictionary *middleEntry = [[NSMutableDictionary alloc] init];
    //
    //
    //        [middleEntry setValue:[BrokersponserID objectAtIndex:x] forKey:@"BrokerListID"];
    //        [middleEntry setValue:[brokerListATPServer objectAtIndex:x] forKey:@"ATPServer"];
    //        [middleEntry setValue:[brokerList objectAtIndex:x] forKey:@"Name"];
    //        [middleEntry setValue:[vertexAddress objectAtIndex:x] forKey:@"VertexAddress"];
    //
    //        [allEntries addObject:middleEntry];
    //        [userDictionary setObject:middleEntry forKey:[brokerList objectAtIndex:x]];
    //
    //    }
    
    
    
    ////NSLog(@"all Entries %@",allEntries);
    
    // NSLog(@"BEFORE SORT: %@",brArray);
    
    /*
     compare:
     NSString, NSMutableString, NSDate, NSCalendarDate, NSValue (scalar types and unsigned char only), NSNumber
     caseInsensitiveCompare:
     NSString, NSMutableString
     localizedCompare:
     NSString, NSMutableString
     localizedCaseInsensitiveCompare:
     NSString, NSMutableString
     localizedStandardCompare:
     NSString, NSMutableString
     */
    
    NSSortDescriptor *sort;
    if ([sortKey isEqualToString:@"AppName"]) {
        sort = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:YES selector:@selector(caseInsensitiveCompare:)];
    } else if ([sortKey isEqualToString:@"Tag"]) {
        sort = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:YES selector:@selector(compare:)];
    }
    
    
    
    [brArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    //  NSLog(@"AFTER SORT: %@",brArray);
    
    [brArray enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop){
        
        NSDictionary *eachBroker = (NSDictionary*)obj;
        BrokerHouseIcon *brokerhouse =[[BrokerHouseIcon alloc]initWithBrokerName:[eachBroker objectForKey:@"AppName"]
                                                                     andIconName:[eachBroker objectForKey:@"IconName"]];
                float iconWidth = brokerhouse.frame.size.width;
        float iconHeight = brokerhouse.frame.size.height;
        
        brokerhouse.btnIcon.tag = [[eachBroker objectForKey:@"Tag"] integerValue];
        
        [brokerhouse.btnIcon addTarget:self action:@selector(clickedBrokerBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        //NSString *isEnable = [eachBroker objectForKey:@"Enable"];
        [brokerhouse.notAvail setHidden:YES];
        
        //        if ([isEnable isEqualToString:@"NO"]) {
        //            [brokerhouse.imgLogo setAlpha:0.2];
        //            [brokerhouse setUserInteractionEnabled:NO];
        //            [brokerhouse.notAvail setHidden:NO];
        //        }
        
        [self.scrollViewBrokerList addSubview:brokerhouse];
        
        brokerhouse.frame = CGRectMake(x, y, 110, 90);
        x += iconWidth + gapX;
        
        if (x > (iconWidth * 7))
        {
            y += iconHeight + gapY;
            x = 80; //x back to default
        }
        
        
    }];
    
    
    //    for (NSString *brokerName in allEntries)
    //    {
    //        BrokerHouseIcon *brokerhouse =[[BrokerHouseIcon alloc]initWithBrokerName:[[allEntries objectAtIndex:tag] objectForKey:@"Name"]];
    //        float iconWidth = brokerhouse.frame.size.width;
    //        float iconHeight = brokerhouse.frame.size.height;
    //
    //        brokerhouse.btnIcon.tag = tag;
    //        [brokerhouse.btnIcon addTarget:self action:@selector(clickedBrokerBtn:) forControlEvents:UIControlEventTouchUpInside];
    //
    //        [brokerListArray addObject:brokerhouse];
    //        [self.scrollViewBrokerList addSubview:brokerhouse];
    //
    //        brokerhouse.frame = CGRectMake(x, y, 50, 90);
    //        x += iconWidth + gapX;
    //
    //        if (x > (iconWidth * 7))
    //        {
    //            y += iconHeight + gapY;
    //            x = 80; //x back to default
    //        }
    //        tag++;
    ////        ////NSLog(@"brokerhouse.frame %f",brokerhouse.frame.origin.x);
    //    }
    self.scrollViewBrokerList.contentSize = CGSizeMake(440,720);
    if (brArray.count<15) {
        self.scrollViewBrokerList.scrollEnabled = YES;
    }
}


#pragma mark - DOWNLOADING PLIST FROM LMS -------------

-(void)getPLISTData {
    // Download plist for iPad
    
    NSURL *URL = [NSURL URLWithString:lmsServerFromBundleID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.timeoutInterval = kNSURLSessionTimeout;
    
     NSLog(@"lmsServerFromBundleID %@",lmsServerFromBundleID);
    
    
    [self showProcessWithMessage:[LanguageManager stringForKey:@"Retrieving Broker Data..."] showRetry:NO showClose:NO activity:YES];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *downloadTask =
    // [session downloadTaskWithRequest:request
    //               completionHandler: ^(NSURL *location, NSURLResponse *response, NSError *error) {
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   
                   if (error) {
                        NSLog(@"error %@", error);
                       
                       dispatch_async(dispatch_get_main_queue(), ^ {
                           
                           
                           [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."]
                                              showRetry:YES showClose:NO activity:NO];
                           
                       });
                       return;
                   }
                   
                   // read plist saved at "location" and check version with current saved one.
                   // NSDictionary *tmpLMSDictionary = [NSDictionary dictionaryWithContentsOfFile:[location path]];
                   
                   // NSString *lmsVersion = [tmpLMSDictionary objectForKey:@"Version"];
                   // NSString *lmsMinVersion = [tmpLMSDictionary objectForKey:@"MinVersion"];
                   
                   //   if ([lmsVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
                   // currentVersion is lower than the lmsVersion
                   // update ipadLMSDictionary to new one
                   
                   NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                   NSData* plistData = [content dataUsingEncoding:NSUTF8StringEncoding];
                   
                   NSPropertyListFormat fmt;
                   ipadLMSDictionary = [NSPropertyListSerialization propertyListWithData:plistData
                                                                                 options: NSPropertyListImmutable
                                                                                  format:&fmt
                                                                                   error:&error];
                   
                   
                   NSLog(@"ipadLMSDictionary %@",ipadLMSDictionary);
                   // ipadLMSDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:[location path]];
                   allowedBrokerListArray = [NSMutableArray arrayWithArray:[ipadLMSDictionary objectForKey:@"BrokerList"]];
                   currentBrokerListArray = [NSMutableArray arrayWithArray:[ipadLMSDictionary objectForKey:@"BrokerList"]];
                   
                    NSLog(@" currentBrokerListArray %@", currentBrokerListArray);
                   
                   // NO BROKER LIST = CUSTOM BROKER APP
                   if ([allowedBrokerListArray count]<=0) {
                       
                       dispatch_async(dispatch_get_main_queue(), ^ {
                           [btnBrokerList setHidden:YES];
                           [_btnBrokerListName setHidden:YES];
                           [_btnBrokerListActive setHidden:YES];
                           [_btnBrokerListActiveName setHidden:YES];
                           
                           [UserPrefConstants singleton].brokerName = [ipadLMSDictionary objectForKey:@"BrokerName"];
                           _lblBrokerName.text = [UserPrefConstants singleton].brokerName;
                           self.activeBrokerNameText.text = [UserPrefConstants singleton].brokerName;
                           [self setBrokerSettings:ipadLMSDictionary];
                           
                           [self showProcessWithMessage:[LanguageManager stringForKey:@"Connecting to Feed Server..."]
                                              showRetry:NO showClose:NO activity:YES];
                           [[VertxConnectionManager singleton] connectingUponLogin];
                           [self performSelector:@selector(timeOutVertxConnect) withObject:nil afterDelay:kNSURLSessionTimeout];
                       });
                       
                       
                       
                   }else {
                       
                       dispatch_async(dispatch_get_main_queue(), ^ {
                           //                        NSString *sortKey = [ipadLMSDictionary objectForKey:@"SortBrokerListBy"];
                           //                        [self populateBrokerIconsWithArray:allowedBrokerListArray andSortKey:sortKey];
                           [btnBrokerList setEnabled:YES];
                           [_btnBrokerListName setEnabled:YES];
                           [_btnBrokerListActive setEnabled:YES];
                           [_btnBrokerListActiveName setEnabled:YES];
                           
                           NSUserDefaults *def  = [NSUserDefaults standardUserDefaults];
                           // set default broker
                           NSDictionary *selectedBroker = nil;
                           // Saving of broker by index cannot work because we have multiple level menu
                           
                           if ([def objectForKey:@"Brokerhouse"]==nil) { // set first broker for first time
                               
                               selectedBroker = (NSDictionary*)[allowedBrokerListArray objectAtIndex:0];
                               [def setObject:selectedBroker forKey:@"Brokerhouse"];
                               [def synchronize];
                               
                           } else { // load previous broker selected
                               
                               // if previous brokerhouse is integer, rewrite with NSDictionary
                               // to cater for previous version TCiPad that uses number
                               if ([[def objectForKey:@"Brokerhouse"] isKindOfClass:[NSNumber class]]) {
                                   
                                   selectedBroker = (NSDictionary*)[allowedBrokerListArray objectAtIndex:0];
                                   [def setObject:selectedBroker forKey:@"Brokerhouse"];
                                   [def synchronize];
                               } else {
                                   
                                   selectedBroker = (NSDictionary*)[def objectForKey:@"Brokerhouse"];
                                   
                               }
                           }
                           
                           //[UserPrefConstants singleton].brokerName=self.lblBrokerName.text;
                           NSLog(@"Selected Broker %@",selectedBroker);
                           
                           [self selectABroker:selectedBroker];
                           
                       });
                       
                       
                   }
                   
               }];
    
    [downloadTask resume];
    
}


-(void)selectABroker:(NSDictionary*)selectedBroker {
    
    self.lblBrokerName.text = [selectedBroker objectForKey:@"AppName"];
    self.activeBrokerNameText.text = [selectedBroker objectForKey:@"AppName"];
    [UserPrefConstants singleton].brokerName = [selectedBroker objectForKey:@"AppName"];
    if([UserPrefConstants singleton].isLoginPage2Activated){
        NSString *bundleID = [selectedBroker objectForKey:@"BundleID"];
        
        if ([bundleID isEqualToString:@"com.n2n.ipad.amsecuat"] || [bundleID isEqualToString:@"com.n2n.ipad.amsec"]) {
            isEquitySelected = YES;
            [_marketSelectionButton setImage:[UIImage imageNamed:@"MarketSelect1.png"] forState:UIControlStateNormal];
          
        }else if([bundleID isEqualToString:@"com.n2n.ipad.amefuturesuat"] || [bundleID isEqualToString:@"com.n2n.ipad.amefuturesuat"]){
            isEquitySelected = NO;
            [_marketSelectionButton setImage:[UIImage imageNamed:@"MarketSelect2.png"] forState:UIControlStateNormal];
        }
        
    }
  
    NSString *plistLink = [selectedBroker objectForKey:@"PlistLink"];
    NSLog(@"selectABroker %@",plistLink);
    
    NSURL *URL = [NSURL URLWithString:plistLink];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.timeoutInterval = kNSURLSessionTimeout;
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask *downloadTask =
    
   // [session downloadTaskWithRequest:request
     //              completionHandler: ^(NSURL *location, NSURLResponse *response, NSError *error) {
    
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                       if (error) {
                           NSLog(@"response %@", response);
                           dispatch_async(dispatch_get_main_queue(), ^ {
                              
                               [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
                                                  showRetry:YES showClose:NO activity:NO];
                           });
                           return;
                       }
                       
                     //    NSLog(@"response %@", location.path);
                      // NSDictionary *brokerSettings = [NSDictionary dictionaryWithContentsOfFile:[location path]];
                   
                   NSString *content = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                   NSData* plistData = [content dataUsingEncoding:NSUTF8StringEncoding];
                   
                   NSPropertyListFormat fmt;
                   NSDictionary *brokerSettings = [NSPropertyListSerialization propertyListWithData:plistData
                                                                                 options: NSPropertyListImmutable
                                                                                  format:&fmt
                                                                                   error:&error];
                       
                       
                       NSLog(@"brokerSettings %@",brokerSettings);
                       if ([brokerSettings count]>0) {
                           
                           dispatch_async(dispatch_get_main_queue(), ^ {
                               // select and return to main
                               [self setBrokerSettings:brokerSettings];
                               
                               [self showProcessWithMessage:[LanguageManager stringForKey:@"Connecting to Feed Server..."]
                                                  showRetry:NO showClose:NO activity:YES];
                               [[VertxConnectionManager singleton] connectingUponLogin];
                               [self performSelector:@selector(timeOutVertxConnect) withObject:nil afterDelay:kNSURLSessionTimeout];
                               
                               if (!displayingFront) {
                                   
                                   if([UserPrefConstants singleton].isLoginPage2Activated){
                                       [UIView transitionWithView:self.loginContainerView2
                                                         duration:0.5
                                                          options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionBeginFromCurrentState
                                                       animations: ^{
                                                           
                                                           
                                                           
                                                       }
                                        
                                                       completion:^(BOOL finished) {
                                                           displayingFront = YES;
                                                          
                                                           // save the user selection
                                                           NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                                                           [def setObject:selectedBroker forKey:@"Brokerhouse"];
                                                           [def synchronize];
                                                       }];
                                       
                                   }
                                   else{
                                       [UIView transitionWithView:self.loginContainerView
                                                         duration:0.5
                                                          options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionBeginFromCurrentState
                                                       animations: ^{
                                                           
                                                           if (activeTouchID) {
                                                               self.activeView.hidden = NO;
                                                               self.loginView.hidden = YES;
                                                               self.brokerSelectionView.hidden = YES;
                                                           }else{
                                                               self.activeView.hidden = YES;
                                                               self.loginView.hidden = NO;
                                                               self.brokerSelectionView.hidden = YES;
                                                           }
                                                           
                                                           
                                                       }
                                        
                                                       completion:^(BOOL finished) {
                                                           displayingFront = YES;
                                                           subMenuLevel = 0;
                                                           // save the user selection
                                                           NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                                                           [def setObject:selectedBroker forKey:@"Brokerhouse"];
                                                           [def synchronize];
                                                       }];
                                       
                                   }
                               }
                               
                                   
                                  
                           });
                           
                       } else {
                           [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
                                              showRetry:YES showClose:NO activity:NO];
                           
                           
                       }
                       
                   }];
    
    [downloadTask resume];
    
}

- (void)setBrokerSettings:(NSDictionary*)brokerSettings {
	
     DLog(@"Broker Settings %@",brokerSettings);
    [UserPrefConstants singleton].brokerCode = [brokerSettings objectForKey:@"BrokerID"];
    [UserPrefConstants singleton].SponsorID = [brokerSettings objectForKey:@"LMSSponsor"];
    [UserPrefConstants singleton].Vertexaddress = [brokerSettings objectForKey:@"VertexServer"];
    [UserPrefConstants singleton].bhCode = [brokerSettings objectForKey:@"BHCode"];
    
    [UserPrefConstants singleton].sessionTimeoutSeconds = [[brokerSettings objectForKey:@"LogoutCountdownTime"] intValue]*60;
    
    if ([UserPrefConstants singleton].sessionTimeoutSeconds<=0)
        [UserPrefConstants singleton].sessionTimeoutSeconds = kTimeOutDuration; // default 30 mins
    
    [UserPrefConstants singleton].CreditLimitOrdPad = [[brokerSettings objectForKey:@"CreditLimitOrdPad"] boolValue];
    [UserPrefConstants singleton].CreditLimitOrdBook = [[brokerSettings objectForKey:@"CreditLimitOrdBook"] boolValue];
    [UserPrefConstants singleton].CreditLimitPortfolio = [[brokerSettings objectForKey:@"CreditLimitPortfolio"] boolValue];
    [UserPrefConstants singleton].TrdShortSellIndicator = [brokerSettings objectForKey:@"TrdShortSellIndicator"];
    
    DLog(@"TrdShortSellIndicator %@",[UserPrefConstants singleton].TrdShortSellIndicator);
    
    [UserPrefConstants singleton].msgPromptForStopNIfTouchOrdType = [[brokerSettings objectForKey:@"msgPromptForStopNIfTouchOrdType"] boolValue];
    
    [UserPrefConstants singleton].LoginHelpText_EN = [brokerSettings objectForKey:@"LoginHelpText"];
    [UserPrefConstants singleton].LoginHelpText_CN = [brokerSettings objectForKey:@"LoginHelpTextCN"];
    // MARK: testing Chinese language (N2N only)
    //  [UserPrefConstants singleton].Vertexaddress = @"nogf1-uat.asiaebroker.com";
    
    //[UserPrefConstants singleton].userATPServerIP = [brokerSettings objectForKey:@"ATPServer"];
    
    [UserPrefConstants singleton].userATPServerIP = [brokerSettings objectForKey:@"ATPServer"];
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//     NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
        [UserPrefConstants singleton].LMSServerAddress = @"http://125.5.209.29/lms/websub/authen?";

    }else{
         [UserPrefConstants singleton].LMSServerAddress = [brokerSettings objectForKey:@"LMSServer"];
        
    }
    
    [UserPrefConstants singleton].isHideAccountNameAtTradeView = YES;
    
    [UserPrefConstants singleton].isEnabledESettlement = [[brokerSettings objectForKey:@"ESettlementEnabled"] boolValue];
    [UserPrefConstants singleton].eSettlementURL = [brokerSettings objectForKey:@"ESettlementURL"];
    [UserPrefConstants singleton].ATPLoadBalance = [[brokerSettings objectForKey:@"ATPLoadBalance"] boolValue];
    [UserPrefConstants singleton].HTTPSEnabled = [[brokerSettings objectForKey:@"HTTPSEnabled"] boolValue];
    [UserPrefConstants singleton].E2EEEncryptionEnabled = [[brokerSettings objectForKey:@"E2EE_Encryption"] boolValue];
    NSString *brokername = [brokerSettings objectForKey:@"BrokerName"];
    if ([brokername length]>0) [UserPrefConstants singleton].brokerName = brokername;
    
    [UserPrefConstants singleton].PFDisclaimerWebView = [[brokerSettings objectForKey:@"PFDisclaimerWebView"] boolValue];
    [UserPrefConstants singleton].PFDisclaimerWebViewURL = [brokerSettings objectForKey:@"PFDisclaimerWebViewURL"];
    [UserPrefConstants singleton].filterShowMarket = [brokerSettings objectForKey:@"filterShowMarket"];
    
    [UserPrefConstants singleton].isArchiveNews = [[brokerSettings objectForKey:@"ArchiveNewsEnabled"] boolValue];
    [UserPrefConstants singleton].termAndConditions_EN = [brokerSettings objectForKey:@"TermsNCondContentOnLoginPg"];
    [UserPrefConstants singleton].termAndConditions_CN = [brokerSettings objectForKey:@"TermsNCondContentOnLoginPgCN"];
    [UserPrefConstants singleton].PrimaryExchg = [brokerSettings objectForKey:@"PrimaryExchg"];
    [UserPrefConstants singleton].NewsServerAddress = [brokerSettings objectForKey:@"NewsServer"];
    
    [UserPrefConstants singleton].ElasticNewsServerAddress = [brokerSettings objectForKey:@"ElasticNewsServer"];
    
    [UserPrefConstants singleton].enabledNewiBillionaire = (BOOL)[brokerSettings objectForKey:@"EnabledNewiBillionaire"];
    [UserPrefConstants singleton].enableIDSS = (BOOL)[brokerSettings objectForKey:@"EnableIDSS"];
    if ([UserPrefConstants singleton].ElasticNewsServerAddress.length<=5) {
        [UserPrefConstants singleton].ElasticNewsServerAddress = @"http://sgnews.itradecimb.com/gcNEWS/web/html/NewsDetails.html";
    }
    
    [UserPrefConstants singleton].researchReportDetailNewsAddress = [brokerSettings objectForKey:@"researchReportDetailNewsAddress"];
    
    if ([UserPrefConstants singleton].researchReportDetailNewsAddress.length<=5) {
        [UserPrefConstants singleton].researchReportDetailNewsAddress = @"http://sgnews.itradecimb.com/gcNEWS/srvs/displayNews";
    }
    // Fundamentals/Factset/CPIQ things
    [UserPrefConstants singleton].fundamentalNewsLabel = [brokerSettings objectForKey:@"FundamentalNewsLabel"];
    [UserPrefConstants singleton].fundamentalNewsServer= [brokerSettings objectForKey:@"FundamentalNewsServer"];
    [UserPrefConstants singleton].fundamentalSourceCode= [brokerSettings objectForKey:@"FundamentalSourceCode"];
    [UserPrefConstants singleton].fundDataCompInfoURL= [brokerSettings objectForKey:@"FundDataCompInfoURL"];
    [UserPrefConstants singleton].fundFinancialInfoURL= [brokerSettings objectForKey:@"FundDataFinancialInfoURL"];
    [UserPrefConstants singleton].fundamentalDefaultReportCode= [brokerSettings objectForKey:@"FundamentalDefaultReportCode"];
    
    // chart
    [UserPrefConstants singleton].chartType = [brokerSettings objectForKey:@"chartType"];
    [UserPrefConstants singleton].Register2FAPgURL = [brokerSettings objectForKey:@"2FARegisterPgURL"];
    
    //stock Alert
    //http://noglu.asiaebroker.com/mpn/srvs/registerDevice
    [UserPrefConstants singleton].isEnabledStockAlert = [[brokerSettings objectForKey:@"EnabledStockAlert"] boolValue];
    [UserPrefConstants singleton].isShownStockAlertDisclaimer = [[brokerSettings objectForKey:@"EnabledStockAlertDisclaimer"] boolValue];
    [UserPrefConstants singleton].stockAlertURL = [brokerSettings objectForKey:@"StockAlertPageURL"];//@"https://sans-uat.asiaebroker.com/gcSANS/web/html/StockAlert.html";
    [UserPrefConstants singleton].pushNotificationRegisterURL = [brokerSettings objectForKey:@"PushNotificationRegisterURL"];//@"https://sans-uat.asiaebroker.com/gcSANS/srvs/registerDevice";
    [UserPrefConstants singleton].stockAlertGetPublicURL =  [brokerSettings objectForKey:@"StockAlertAuthKey"];//@"https://sans-uat.asiaebroker.com/gcSANS/srvs/Auth/getRecKey";
    
    [UserPrefConstants singleton].interactiveChartEnabled= [[brokerSettings objectForKey:@"InteractiveChartEnabled"] boolValue];

    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    if ([def objectForKey:@"QtyIsLot"]==nil) {
        [UserPrefConstants singleton].quantityIsLot =  ![[brokerSettings objectForKey:@"DefaultTradePrefQtyUnit"] boolValue];
    } else {
        [UserPrefConstants singleton].quantityIsLot = [def boolForKey:@"QtyIsLot"];
    }

    if ([[UserPrefConstants singleton].chartType length]<=0) [UserPrefConstants singleton].chartType = @"Image";
    [UserPrefConstants singleton].interactiveChartByExchange = [brokerSettings objectForKey:@"InteractiveChartForExch"];
    [UserPrefConstants singleton].chartTypeToLoad = [UserPrefConstants singleton].chartType;
    
   /* NSLog(@"\n\n ------------------------------------------------------ \n");
    NSLog(@"Vertex: %@",[UserPrefConstants singleton].Vertexaddress);
    NSLog(@"ATP: %@",[UserPrefConstants singleton].userATPServerIP);
    NSLog(@"LMS: %@",[UserPrefConstants singleton].LMSServerAddress);
    NSLog(@"News Server: %@", [UserPrefConstants singleton].NewsServerAddress);
    NSLog(@"ChartToLoadDefault: %@",[UserPrefConstants singleton].chartTypeToLoad);
    NSLog(@"quantityIsLot: %d",[UserPrefConstants singleton].quantityIsLot);
    NSLog(@"\n\n\n");*/
    
    //default starting screen
    if ([def objectForKey:@"ScreenViewMode"]==nil) {
        [UserPrefConstants singleton].defaultStartingScreen = [brokerSettings objectForKey:@"DefaultStartingScreen"];
        if ([[UserPrefConstants singleton].defaultStartingScreen length]<=0)
            [UserPrefConstants singleton].defaultStartingScreen = @"Home";
        
        if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Home"])
            [UserPrefConstants singleton].defaultView = 1;
        else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Quote"])
            [UserPrefConstants singleton].defaultView = 2;
        else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"WatchList"])
            [UserPrefConstants singleton].defaultView = 3;
        
        //giangtt
        NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
        [def setValue:[NSString stringWithFormat:@"%d",[UserPrefConstants singleton].defaultView] forKey:@"ScreenViewMode"];
        [def synchronize];

    } else {
        int defView =  [[def objectForKey:@"ScreenViewMode"] intValue];
        [UserPrefConstants singleton].defaultView = defView;
        if (defView==1)
            [UserPrefConstants singleton].defaultStartingScreen = @"Home";
        else if (defView==2)
            [UserPrefConstants singleton].defaultStartingScreen = @"Quote";
        else if (defView==3)
            [UserPrefConstants singleton].defaultStartingScreen = @"WatchList";
    }
    [UserPrefConstants singleton].dashBoard = [brokerSettings objectForKey:@"DashBoard"];
    [UserPrefConstants singleton].interactiveChartURL = [brokerSettings objectForKey:@"InteractiveChartURL"];
    [UserPrefConstants singleton].intradayChartURL = [brokerSettings objectForKey:@"IntradayChartServer"];
    [UserPrefConstants singleton].historicalChartURL = [brokerSettings objectForKey:@"HistoricalChartServer"];
    //Settings
    [UserPrefConstants singleton].companyName = [brokerSettings objectForKey:@"CompanyName"];
    [UserPrefConstants singleton].companyTel = [brokerSettings objectForKey:@"CompanyTel"];
    [UserPrefConstants singleton].companyAddress = [brokerSettings objectForKey:@"CompanyAddress"];
    [UserPrefConstants singleton].companyFax = [brokerSettings objectForKey:@"CompanyFax"];
    [UserPrefConstants singleton].companyLogo = [brokerSettings objectForKey:@"CompanyLogo"];
    [UserPrefConstants singleton].shareText = [brokerSettings objectForKey:@"ShareText"];
    [UserPrefConstants singleton].facebookLink = [brokerSettings objectForKey:@"FacebookLink"];
    [UserPrefConstants singleton].twitterText = [brokerSettings objectForKey:@"TwitterLink"];
    [UserPrefConstants singleton].linkInText = [brokerSettings objectForKey:@"LinkedInLink"];
    [UserPrefConstants singleton].websiteLink = [brokerSettings objectForKey:@"WebsiteLink"];
    [UserPrefConstants singleton].emailLink = [brokerSettings objectForKey:@"EmailLink"];
    [UserPrefConstants singleton].aboutUs = [brokerSettings objectForKey:@"AboutUs"];
    [UserPrefConstants singleton].appName = [brokerSettings objectForKey:@"AppName"];
    
     [UserPrefConstants singleton].isBrokerAnnoucement =  [[brokerSettings objectForKey:@"isEnabledBrokerAnnoucement"] boolValue];
     [UserPrefConstants singleton].brokerAnnoucement =  [brokerSettings objectForKey:@"BrokerAnnoucement"];
    
    [UserPrefConstants singleton].tempQcServer = @"42.61.42.71";//[brokerSettings objectForKey:@"QCServer"];

    [UserPrefConstants singleton].tempUserParamQC = @"[vUpJYKw4QvGRMBmhATUxRwv4JrU9aDnwNEuangVyy6OuHxi2YiY=]";
    
    [UserPrefConstants singleton].AnnouncementURL = [brokerSettings objectForKey:@"AnnouncementURL"];
    
    [UserPrefConstants singleton].orderHistLimit = [brokerSettings objectForKey:@"OrderHistoryLimit"];
    // default to 30 days
    if ([[UserPrefConstants singleton].orderHistLimit length]<=0) [UserPrefConstants singleton].orderHistLimit = @"30";

    // Indices button
    [UserPrefConstants singleton].indicesEnableByExchange = [brokerSettings objectForKey:@"ExchangesEnableIndices"];
    
    // OrderPad
    [UserPrefConstants singleton].defaultCurrency = [brokerSettings objectForKey:@"DefaultCurrency"];
    [UserPrefConstants singleton].isEnableRDS = [[brokerSettings objectForKey:@"RDS"] boolValue];
    
    // Override Exchange name
    [UserPrefConstants singleton].overrideExchangesName = [NSDictionary dictionaryWithDictionary:[brokerSettings objectForKey:@"OverrideExchangeName"]];
    
    [UserPrefConstants singleton].isEnabledElasticNews = [[brokerSettings objectForKey:@"isEnabledElasticNews"] boolValue];
    
    // Market Streamer
    
    [UserPrefConstants singleton].isShowMarketStreamer = YES;//[[brokerSettings objectForKey:@"MarketStreamer"] boolValue];
    
    // Order Trade Limit in Portfolio
    [UserPrefConstants singleton].isShowTradingLimit = YES;//[[brokerSettings objectForKey:@"TradingLimitEnabled"] boolValue];
    
    // update UI
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *helpInfoText = @"";
        if ([[LanguageManager defaultManager].language isEqualToString:@"en"]) {
            helpInfoText = [UserPrefConstants singleton].LoginHelpText_EN;
        } else if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
            helpInfoText = [UserPrefConstants singleton].LoginHelpText_CN;
        }
        
        _helpInfoTextView.text = helpInfoText;
    
        
        if ([helpInfoText length]<=0) {
            _loginView.frame = CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y,
                                          _loginView.frame.size.width, 260);
            _loginView.center = CGPointMake(_loginContainerView.frame.size.width/2.0,
                                            _loginContainerView.frame.size.height/2.0);
        } else {
            _loginView.frame = CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y,
                                          _loginView.frame.size.width, 320);
            _loginView.center = CGPointMake(_loginContainerView.frame.size.width/2.0,
                                            _loginContainerView.frame.size.height/2.0);
        }
        
        NSLog(@"isBrokerAnnoucement %d",[UserPrefConstants singleton].isBrokerAnnoucement);
        if ([UserPrefConstants singleton].isBrokerAnnoucement) {
            
            [_lblErrorMessage setHidden:NO];
        
            if([UserPrefConstants singleton].brokerAnnoucement.length<5){
                  [_lblErrorMessage setText:@"This is DEMO-UAT Version of N2N, Please contact us at www.n2nconnect.com if you would like to try request for DEMO account"];
            }else{
                  [_lblErrorMessage setText:[UserPrefConstants singleton].brokerAnnoucement];
            }
                
            
       
            
        }
        
        self.termAndConditionTextView.text = [UserPrefConstants singleton].termAndConditions_EN;
        self.txtUserName.delegate=self;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *rememberMeStr = [NSString stringWithFormat:@"isRememberMe%@",[UserPrefConstants singleton].brokerCode];
        NSString *usernameStr = [NSString stringWithFormat:@"userName%@",[UserPrefConstants singleton].brokerCode];
        
        
        BOOL remembered = [defaults boolForKey:rememberMeStr];
        if (remembered) {
            self.txtUserName.text = [defaults objectForKey:usernameStr];
            self.txtUserName.enabled=NO;
            self.txtUserName.backgroundColor = [UIColor grayColor];
            //NSLog(@"Remember %@", [defaults objectForKey:usernameStr]);
            
        } else {
            self.txtUserName.text = @"";
            self.txtUserName.enabled=YES;
            self.txtUserName.backgroundColor = [UIColor whiteColor];
        }
        
        self.btnRememberMe.selected = [defaults boolForKey:rememberMeStr];
        self.txtPassword.text = @"";
        
        if ([UserDefaultData getTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode]) {
            // touchID activated
            [_deactivateBtn setHidden:NO];
        } else {
            [_deactivateBtn setHidden:YES];
        }
        
    });
}


-(void)showTouchID:(BOOL)show {

    if (show) {
        [_loginView setFrame:CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y,
                                        596, _loginView.frame.size.height)];

    } else {
        [_loginView setFrame:CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y,
                                        427, _loginView.frame.size.height)];

    }
    [_loginView setCenter:CGPointMake(_loginContainerView.frame.size.width/2.0,
                                      _loginView.center.y)];
    
}
-(void)clickedBrokerBtnByTag:(int)tag{
    if (displayingFront) {
        
        //sender tag is LMS tag value not array index!
        __block NSDictionary *selectedBroker;
        
        [currentBrokerListArray enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop){
            
            NSDictionary *eachBroker = (NSDictionary*)obj;
            long tagBroker = [[eachBroker objectForKey:@"Tag"] integerValue];
            NSLog(@"tag %d and Tag broker %ld",tag,tagBroker);
            if (tag==tagBroker) {
                selectedBroker = eachBroker;
            }
        }];
        
        
        if (selectedBroker!=nil) {
            
            NSString *plistlinkStr = [selectedBroker objectForKey:@"PlistLink"];
            
            if ([plistlinkStr length]>0) {
                if ([plistlinkStr rangeOfString:@"multilogin"].location == NSNotFound) {
                    // download broker detail
                    
                    [self showProcessWithMessage:[LanguageManager stringForKey:LOADING______]
                                       showRetry:NO showClose:NO activity:YES];
                    [self selectABroker:selectedBroker];
                } else {
                    [self showProcessWithMessage:[LanguageManager stringForKey:LOADING______]
                                       showRetry:NO showClose:NO activity:YES];
                    [self openSubmenu:plistlinkStr];
                }
            } else {
                
                [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
                                   showRetry:YES showClose:NO activity:YES];
            }
            
        } else {
            // broker not found (tag problem?)
            [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
                               showRetry:YES showClose:NO activity:YES];
        }
        
    }
}

-(void)clickedBrokerBtn:(UIButton*)sender {
    
    if (!displayingFront) {
        
        //sender tag is LMS tag value not array index!
        
        __block NSDictionary *selectedBroker;
        
        [currentBrokerListArray enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop){
            
            NSDictionary *eachBroker = (NSDictionary*)obj;
            long tag = [[eachBroker objectForKey:@"Tag"] integerValue];
            
            if (sender.tag==tag) {
                selectedBroker = eachBroker;
            }
            
        }];
        
        
        if (selectedBroker!=nil) {
            
            NSString *plistlinkStr = [selectedBroker objectForKey:@"PlistLink"];
            
            if ([plistlinkStr length]>0) {
                if ([plistlinkStr rangeOfString:@"multilogin"].location == NSNotFound) {
                    // download broker detail
            
                    [self showProcessWithMessage:[LanguageManager stringForKey:LOADING______]
                                       showRetry:NO showClose:NO activity:YES];
                    [self selectABroker:selectedBroker];
                } else {
                    [self showProcessWithMessage:[LanguageManager stringForKey:LOADING______]
                                       showRetry:NO showClose:NO activity:YES];
                    [self openSubmenu:plistlinkStr];
                }
            } else {
                
                [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
                                   showRetry:YES showClose:NO activity:YES];
            }
            
        } else {
            // broker not found (tag problem?)
            [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker data. Check your internet connection and try again. If problem persist, please contact us."]
                               showRetry:YES showClose:NO activity:YES];
        }
        
    }
    
}




-(void)openSubmenu:(NSString*)plistLink {
    
    NSURL *URL = [NSURL URLWithString:plistLink];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    request.timeoutInterval = kNSURLSessionTimeout;
    
   // NSLog(@"link %@",plistLink);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request
                                                            completionHandler:
                                              ^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                  
                                                  if (error) {
                                                      //NSLog(@"response %@", response);
                                                      dispatch_async(dispatch_get_main_queue(), ^ {
                                    
                                                          [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."]
                                                                             showRetry:YES showClose:NO activity:YES];
                                                      });
                                                      return;
                                                  }
                                                  
                                                  // read plist saved at "location" and check version with current saved one.
                                                  NSDictionary *tmpLMSDictionary = [NSDictionary dictionaryWithContentsOfFile:[location path]];
                                                  
                                                  // NSLog(@"submenu %@", tmpLMSDictionary);
                                                  
                                                  
                                                  if ([tmpLMSDictionary count]>0) {
                                                      
                                                      // currentVersion is lower than the lmsVersion
                                                      currentBrokerListArray = [NSMutableArray arrayWithArray:[tmpLMSDictionary objectForKey:@"BrokerList"]];
                                                      dispatch_async(dispatch_get_main_queue(), ^ {
                                                          
                                                          [UIView transitionWithView:self.loginContainerView
                                                                            duration:0.5
                                                                             options:UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionBeginFromCurrentState
                                                                          animations: ^{
                                                                              self.loginView.hidden = YES;
                                                                              self.brokerSelectionView.hidden = NO;
                                                                              NSString *sortKey = [tmpLMSDictionary objectForKey:@"SortBrokerListBy"];
                                                                              [self populateBrokerIconsWithArray:currentBrokerListArray andSortKey:sortKey];
                                                                          }
                                                           
                                                                          completion:^(BOOL finished) {
                                                                              displayingFront = NO;
                                                                              subMenuLevel += 1;
                                                                          }];
                                                          [self removeProcessView];
                                                      });
                                                      
                                                  } else {
                                                      dispatch_async(dispatch_get_main_queue(), ^ {
                                                          [self showProcessWithMessage:[LanguageManager stringForKey:@"[Network] Unable to get broker list. Check your internet connection and try again. If problem persist, please contact us."]
                                                                             showRetry:YES showClose:NO activity:YES];

                                                      });
                                                  }
                                                  
                                              }];
    
    [downloadTask resume];
    
}




- (IBAction)callLanguageSelection:(UIButton*)sender {
    
    
    [_languageTableViewVC.tableView reloadData];
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:_languageTableViewVC];
    navController.navigationBar.topItem.title=[LanguageManager stringForKey:@"Languages"];
    
//    [navController.navigationBar
//     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor cyanColor]}];
//    navController.navigationBar.barTintColor = kPanelColor;
//    _languageTableViewVC.tableView.backgroundColor = kPanelColor;
    _languageTableViewVC.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    
    NSInteger totalToDisplay = [[[LanguageManager defaultManager] supportedLanguages] count];
    if (totalToDisplay>=5) totalToDisplay = 5;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*40);

    _columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
   // _columnPickerPopover.backgroundColor = kPanelColor;
    [_columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:self.view
                       permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
}

- (IBAction)brokerBack:(id)sender {
    
    subMenuLevel = subMenuLevel - 1;
    
    
    if (subMenuLevel>=1) {
        
        [UIView transitionWithView:self.loginContainerView
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionBeginFromCurrentState
                        animations: ^{
                            self.loginView.hidden = YES;
                            self.brokerSelectionView.hidden = NO;
                            self.activeView.hidden = YES;
                            currentBrokerListArray = allowedBrokerListArray;
                            
                            NSString *sortKey = [ipadLMSDictionary objectForKey:@"SortBrokerListBy"];
                            [self populateBrokerIconsWithArray:currentBrokerListArray andSortKey:sortKey];
                        }
         
                        completion:^(BOOL finished) {
                            displayingFront = NO;
                            
                        }];
        
    } else {
        
        [UIView transitionWithView:self.loginContainerView
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionBeginFromCurrentState
                        animations: ^{
                            
                            if (activeTouchID) {
                                self.activeView.hidden = NO;
                                self.loginView.hidden = YES;
                                self.brokerSelectionView.hidden = YES;
                            }else{
                                self.activeView.hidden = YES;
                                self.loginView.hidden = NO;
                                self.brokerSelectionView.hidden = YES;
                            }
                            
                            
                            
                        }
         
                        completion:^(BOOL finished) {
                            displayingFront = YES;
                            
                        }];
    }
    
}


- (IBAction)openBrokerList:(UIButton *)sender
{
    
    if (btnBrokerList.hidden) return;
    
    //NSArray *sortedStrings =
    //[brokerList sortedArrayUsingSelector:@selector(compare:)];
    
    [self dismissKeyboard];
    // BrokerHouseIcon *broker = (BrokerHouseIcon *)sender;
    // broker.imgLogo.tintColor = [UIColor grayColor];
    
    //NSDictionary *selectedBroker = [allowedBrokerListArray objectAtIndex:sender.tag];
    currentBrokerListArray = allowedBrokerListArray;
    
    // NSLog(@"selectedBroker %@", selectedBroker);
    
    //   [UserPrefConstants singleton].userATPServerIP = [[allEntries objectAtIndex:sender.tag] objectForKey:@"ATPServer"];
    //    [UserPrefConstants singleton].SponsorID=[[allEntries objectAtIndex:sender.tag] objectForKey:@"BrokerListID"];
    //    [UserPrefConstants singleton].Vertexaddress=[[allEntries objectAtIndex:sender.tag] objectForKey:@"VertexAddress"];
    
    //  Store data for Push Notifications.
    
    // OPEN BROKER LIST
    
    NSString *sortKey = [ipadLMSDictionary objectForKey:@"SortBrokerListBy"];
    [self populateBrokerIconsWithArray:allowedBrokerListArray andSortKey:sortKey];
    
    //self.lblBrokerName.text = [selectedBroker objectForKey:@"AppName"];
    //[UserPrefConstants singleton].brokerName=self.lblBrokerName.text;
    
    //
    //        [[NSUserDefaults standardUserDefaults]setInteger:sender.tag forKey:@"Brokerhouse"];
    //        [[NSUserDefaults standardUserDefaults]synchronize];
    [UIView transitionWithView:self.loginContainerView
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations: ^{
                        
                        self.activeView.hidden = YES;
                        self.loginView.hidden = YES;
                        self.brokerSelectionView.hidden = NO;
                        
                    }
     
                    completion:^(BOOL finished) {
                        displayingFront = NO;
                        subMenuLevel = 1;
                    }];
    
}




-(void)dismissKeyboard {
    [self.txtUserName resignFirstResponder];
    [self.txtPassword resignFirstResponder];
}


/*
 [brokerList addObject:obj[@"Name"]];
 //                    [brokerListATPServer addObject:obj[@"ATPServer"]];
 //                    [BrokersponserID addObject:obj[@"Sponsor"]];
 //                    [vertexAddress addObject:obj[@"Vertex"]];
 */




-(void)shakeView {
    
    CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
    [shake setDuration:0.1];
    [shake setRepeatCount:2];
    [shake setAutoreverses:YES];
    [shake setFromValue:[NSValue valueWithCGPoint:
                         CGPointMake(self.loginView.center.x - 5,self.loginView.center.y)]];
    [shake setToValue:[NSValue valueWithCGPoint:
                       CGPointMake(self.loginView.center.x + 5, self.loginView.center.y)]];
    [self.loginView.layer addAnimation:shake forKey:@"position"];
}

- (IBAction)clickedRememberMe:(UIButton *)sender
{
    sender.selected = !sender.selected;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *rememberMeStr = [NSString stringWithFormat:@"isRememberMe%@",[UserPrefConstants singleton].brokerCode];
    NSString *usernameStr = [NSString stringWithFormat:@"userName%@",[UserPrefConstants singleton].brokerCode];
    
    if(sender.selected == YES)
    {
        [defaults setObject:self.txtUserName.text forKey:usernameStr];
        self.txtUserName.enabled=NO;
        self.txtUserName.backgroundColor = [UIColor grayColor];
    }
    else
    {
        [defaults setObject:@"" forKey:usernameStr];
        self.txtUserName.enabled=YES;
        self.txtUserName.backgroundColor = [UIColor whiteColor];
        //        if ([defaults objectForKey:@"Exchange"]!=nil) {
        //            [defaults removeObjectForKey:@"Exchange"];
        //        }
    }
    [defaults setObject:[NSNumber numberWithBool:sender.selected] forKey:rememberMeStr];
    [defaults synchronize];
    
}


-(void)executeLogin {
    NSString *exchgStr = [NSString stringWithFormat:@"Exchange%@",[UserPrefConstants singleton].brokerCode];
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    if ([[def objectForKey:exchgStr] length]<=0) {
        [UserPrefConstants singleton].userCurrentExchange =  [UserPrefConstants singleton].PrimaryExchg;
        [def setObject:[UserPrefConstants singleton].userCurrentExchange forKey:exchgStr];
        [def synchronize];
    }
    else
        [UserPrefConstants singleton].userCurrentExchange =[[NSUserDefaults standardUserDefaults]objectForKey:exchgStr];
    
    
    // NSLog(@"Login Exchange Set: %@ - %@",exchgStr, [UserPrefConstants singleton].userCurrentExchange);
    
    if([UserPrefConstants singleton].isLoginPage2Activated){
        [UserPrefConstants singleton].userName = _touchIDUsername.length>0 ? _touchIDUsername: self.view2TxtUserName.text;
        [UserPrefConstants singleton].userPassword = _touchIDPassword.length>0 ? _touchIDPassword : self.view2TxtPassword.text;
    }else{
        [UserPrefConstants singleton].userName = _touchIDUsername.length>0 ? _touchIDUsername: self.txtUserName.text;
        [UserPrefConstants singleton].userPassword = _touchIDPassword.length>0 ? _touchIDPassword : self.txtPassword.text;
    }
 
    
   // NSLog(@"_touch id username %@, _touch id password %@",[UserPrefConstants singleton].userName, [UserPrefConstants singleton].userPassword);
    
    [[VertxConnectionManager singleton] vertxLogin];
    
    if ([UserPrefConstants singleton].ATPLoadBalance) {
        [atp atpGetPK_LoadBalance];
    } else {
        [atp atpGetPK];
    }
    
    [self showProcessWithMessage:[LanguageManager stringForKey:@"Logging In..."]
                       showRetry:NO showClose:NO activity:YES];
}


- (IBAction)clickedSubmit:(id)sender
{
    //    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //    button.frame = CGRectMake(20, 50, 100, 30);
    //    [button setTitle:@"Crash" forState:UIControlStateNormal];
    //    [button addTarget:self action:@selector(crashButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:button];
    //self.txtUserName.text = @"rvreyes";
    //self.txtPassword.text = @"P@$sw0rd";
    //self.txtUserName.text = @"mms0512011";
    //self.txtPassword.text = @"abc123";
    //self.txtUserName.text = @"johnatienza";
    //self.txtPassword.text = @"abc123";
    if ([self loginValidated])
    {
        //Show Loading indicator
        //Do validation here
        //ATP Login
        //Vertx Login
        /* Currently Dummy Production Connected to :
         #define ATPSERVER @"nmga.affintrade.com"
         #define userName @"usern2n"
         #define password @"cvb123"
         */
        
        // save exchange based on broker

        [self executeLogin];

    }
    else
    {
        [self shakeView];
        [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"User ID and Password cannot be empty."]
                                     inController:self withTitle:@""];

    }
    [self dismissKeyboard];
}
- (IBAction)clickedSubmit2:(id)sender

{
    
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    //[self performSegueWithIdentifier:@"segueHomeScreen" sender:self];
}


- (BOOL)loginValidated
{
    if([UserPrefConstants singleton].isLoginPage2Activated){
        if (self.view2TxtUserName.text.length>0 && self.view2TxtPassword.text.length >0) {
            return YES;
        }
    }else{
        if (self.txtUserName.text.length>0 && self.txtPassword.text.length >0) {
            return YES;
        }
    }
   
    
    return NO;
}



- (BOOL) activeValidated{
    
    
    
    if (self.activeTextUsername.text.length>0 && self.activeTextPassword.text.length >0) {
        _touchIDUsername = self.activeTextUsername.text;
        _touchIDPassword = self.activeTextPassword.text;
        return YES;
    }
    
    return NO;
}






#pragma mark - TextField Delegates
- (IBAction)textFieldEditingChanged:(id)sender {
    if ([UserDefaultData getTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode]) {
        // touchID activated
        [_deactivateBtn setHidden:NO];
    } else {
        [_deactivateBtn setHidden:YES];
    }
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
   
   
    if([UserPrefConstants singleton].isLoginPage2Activated){
        CGRect animTitleFrameEndPosition = self.loginContainerView2.frame;
        animTitleFrameEndPosition.origin.y = -40;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.35];
        [UIView setAnimationDelay:0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.loginContainerView2.frame = animTitleFrameEndPosition;
        [UIView commitAnimations];
    }else{
        CGRect animTitleFrameEndPosition = self.loginContainerView.frame;
        animTitleFrameEndPosition.origin.y = -40;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.35];
        [UIView setAnimationDelay:0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.loginContainerView.frame = animTitleFrameEndPosition;
         [UIView commitAnimations];
    }
    
    
   
    
    if (![self.activeView isHidden]) {

        [UIView transitionWithView:_logoImage
                          duration:0.5
                           options:UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                        animations: ^{
                            
                            
                            [_logoImage setAlpha:0.0f];
                            
                        }
         
                        completion:^(BOOL finished) {
                             [_logoImage setAlpha:0.0f];
                            
                        }];
        
    }
   
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
      if([UserPrefConstants singleton].isLoginPage2Activated){
    CGRect animTitleFrameEndPosition = self.loginContainerView2.frame;
    animTitleFrameEndPosition.origin.y = 84;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    [UIView setAnimationDelay:0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    self.loginContainerView2.frame = animTitleFrameEndPosition;
    
    [UIView commitAnimations];
      }else{
          CGRect animTitleFrameEndPosition = self.loginContainerView.frame;
          animTitleFrameEndPosition.origin.y = 84;
          [UIView beginAnimations:nil context:nil];
          [UIView setAnimationDuration:0.35];
          [UIView setAnimationDelay:0];
          [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
          
          self.loginContainerView.frame = animTitleFrameEndPosition;
          
          [UIView commitAnimations];
      }
    
    
    if ([self.activeView isHidden]) {
        
        [UIView transitionWithView:_logoImage
                          duration:0.5
                           options:UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                        animations: ^{
                            
                            
                            [_logoImage setAlpha:1.0f];
                            
                        }
         
                        completion:^(BOOL finished) {
                            [_logoImage setAlpha:1.0f];
                            
                        }];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    ////NSLog(@"Typed");
    
    if (textField == self.txtUserName) {
        [self.txtPassword becomeFirstResponder];
       
    } else if (textField == self.txtPassword )
    {
        [self.txtPassword resignFirstResponder];
        [self.txtUserName resignFirstResponder];
        //   [self clickedSubmit:self];
    } else if(textField == self.activeTextUsername){
        
        [self.activeTextPassword becomeFirstResponder];
        
    } else if(textField == self.activeTextPassword){
        [self.activeTextPassword resignFirstResponder];
        [self.activeTextUsername resignFirstResponder];
        //   [self clickedSubmit:self];
    } else if (textField == self.view2TxtUserName) {
        [self.view2TxtPassword becomeFirstResponder];
        
    } else if (textField == self.view2TxtPassword )
    {
        [self.view2TxtPassword resignFirstResponder];
        [self.view2TxtUserName resignFirstResponder];
        //   [self clickedSubmit:self];
    }
    return YES;
}

#pragma mark -
#pragma mark PostNotification
//Retrieval Selector
- (void) logUser {
    // TODO: Use the current user's information
    // You can call any combination of these three methods
    [CrashlyticsKit setUserIdentifier:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"]];
    [CrashlyticsKit setUserName:self.txtUserName.text];
    [CrashlyticsKit setUserEmail:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[Email]"]];
}



- (void)atpLoginStatusReceived:(NSNotification *)notification
{
    NSDictionary * notficationDict = [notification.userInfo copy];
    NSString *loginStatus = [notficationDict objectForKey:@"loginStatusCode"];
    NSString *errorMsg = [notficationDict objectForKey:@"error"];
    NSNumber *pwResetMode = [notficationDict objectForKey:@"Password"];
    
    // //NSLog(@"atpLoginStatusReceived notficationDict : %@",notficationDict);
    
    if ([pwResetMode intValue]==2) {
        
        NSString *productName = [NSString stringWithFormat:@"%@",[UserPrefConstants singleton].appName];
        
        NSLog(@"[UserPrefConstants singleton].appName %@",[UserPrefConstants singleton].appName);
        
        [self showProcessWithMessage:[LanguageManager stringForKey:@"Your password has expired. Please visit %@ in your browser to reset it to a new one." withPlaceholders:@{@"%ProductName%":productName}]
                                showRetry:NO showClose:YES activity:NO];
         
        
        return;
    }
    
    
    if([loginStatus isEqualToString:@"Success"])
    {
        
        [self logUser];
        
        ////NSLog(@"Device Token %@, iPAddress %@, deviceUDID %@",[UserPrefConstants singleton].deviceToken,  [UserPrefConstants singleton].ipAddress, [UserPrefConstants singleton].deviceUDID);
        // API for Store Push Notifications Info to WMS Server.
          [UserPrefConstants singleton].bhCode = [[UserPrefConstants singleton].userInfoDict objectForKey:@"[BrokerCode]"];
        
        if ([UserPrefConstants singleton].deviceToken != nil && [UserPrefConstants singleton].ipAddress != nil && [UserPrefConstants singleton].deviceUDID != nil ) {
            NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//            NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
            
        
            // DLog(@"user Info Dict %@", [UserPrefConstants singleton].bhCode);
            
            if ([UserPrefConstants singleton].userInfoDict!=nil || [[UserPrefConstants singleton].userInfoDict objectForKey:@"[BrokerCode]"]!=nil) {
                   [[StockAlertAPI instance] storePushNotificationInfo:self.txtUserName.text andUDID:[UserPrefConstants singleton].deviceUDID andExhange:[UserPrefConstants singleton].userCurrentExchange andSponsorCode:[UserPrefConstants singleton].SponsorID andBHCode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[BrokerCode]"] andAppID:@"MD" andBundleId:bundleIdentifier
                                                         andClientCode:[[UserPrefConstants singleton].userInfoDict objectForKey:@"[SenderCode]"]
                                                        andDeviceToken:[UserPrefConstants singleton].deviceToken
                                                          andIPAddress:[UserPrefConstants singleton].ipAddress andActivationCode:@""];
            }
        }
        
        // NSLog(@"user Info Dict %@",[UserPrefConstants singleton].userInfoDict);
        
        
        // VARIABLES TO BE RESET AFTER NEW LOGIN
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"Rememberme"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"TradingPin"];
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        [UserPrefConstants singleton].isCollectionView = [def boolForKey:@"QuoteScreenCardView"];
        [UserPrefConstants singleton].showDisclaimer = YES;
        
        [UserPrefConstants singleton].sectorToFilter = nil;
        [UserPrefConstants singleton].selectedMarket = @"10"; // all
        [UserPrefConstants singleton].enableWarrant = NO;
        
        ////NSLog(@"performSegueWithIdentifier...");
        
        if (activeTouchID) {
            
            [UserDefaultData setTouchIDFlag:YES forUser:[UserPrefConstants singleton].brokerCode];
            [UserDefaultData saveUsername:self.activeTextUsername.text];
            [UserDefaultData savePassword:self.activeTextPassword.text];
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:[LanguageManager stringForKey:@"Success"]
                                         message: [LanguageManager stringForKey:@"TouchIDSuccessMessage" withPlaceholders:@{@"%brokerName%":[UserPrefConstants singleton].brokerName}]
                                         preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:[LanguageManager stringForKey:@"Ok"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           
                                           [self performSegueWithIdentifier:@"segueHomeScreen" sender:self];
                                           //Handle your yes please button action here
                                       }];
            [alert addAction:okButton];
            [self removeProcessView];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            [self performSegueWithIdentifier:@"segueHomeScreen" sender:self];
            [self removeProcessView];
        }
        
        self.txtPassword.text = nil;
    }
    else if([loginStatus isEqualToString:@"Failed"])
    {
        ////NSLog(@"Failed1");
        [UserDefaultData setTouchIDFlag:NO forUser:[UserPrefConstants singleton].brokerCode];
        [UserDefaultData saveUsername:@""];
        [UserDefaultData savePassword:@""];
        if ([errorMsg length]<=0) errorMsg = [LanguageManager stringForKey:@"Error Unknown"];
        [self showProcessWithMessage:errorMsg
                           showRetry:NO showClose:YES activity:NO];

    }
    else
    {
        ////NSLog(@"Failed1");
        [UserDefaultData setTouchIDFlag:NO forUser:[UserPrefConstants singleton].brokerCode];
        errorMsg = [LanguageManager stringForKey:@"Error Unknown"];
        [self showProcessWithMessage:errorMsg
                           showRetry:NO showClose:YES activity:NO];
    }
}

-(void)vertxGetExchangeInfoFinished:(NSNotification *)notification {
    //NSLog(@"ExchangeInfo finished");
    
}

- (void) pnWebSocketConnected:(NSNotification *)notification{
   // NSLog(@"socket connected");
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(timeOutVertxConnect) object:nil];
    
    [self removeProcessView];
    
}

#pragma mark -
#pragma mark TouchLogin

//====================================================================
//                Touch ID Login
//====================================================================

- (IBAction) cancelTouchID:(id)sender{
    [UIView transitionWithView:self.loginContainerView
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionBeginFromCurrentState
                    animations: ^{
                        
                        self.activeView.hidden = YES;
                        self.loginView.hidden = NO;
                        self.brokerSelectionView.hidden = YES;
                        activeTouchID = NO;
                        
                    }
     
                    completion:^(BOOL finished) {
                        displayingFront = YES;
                        
                    }];
    
    
    if ([self.activeView isHidden]) {
        
        [UIView transitionWithView:_logoImage
                          duration:0.5
                           options:UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                        animations: ^{
                            
                            
                            [_logoImage setAlpha:1.0f];
                            
                        }
         
                        completion:^(BOOL finished) {
                            [_logoImage setAlpha:1.0f];
                            
                        }];
    }
    
}


- (IBAction)openTerms:(id)sender {
    if ([[UserPrefConstants singleton].termAndConditions_EN length]>0)
    [self performSegueWithIdentifier:@"segueLoginTOC" sender:nil];
}

- (IBAction)tncAcceptToggle:(id)sender {
    
    UIButton *tncBtn = (UIButton*)sender;
    
    _tncAccept = !_tncAccept;
    if (_tncAccept) {
        [tncBtn setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
    } else {
        [tncBtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
    
}

- (IBAction)deactivateTouchID:(id)sender {

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Deactivate TouchID?"]
                                                                   message:[LanguageManager stringForKey:@"Deactivate TouchID for current user?"] preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *btnNo = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"No"] style:UIAlertActionStyleCancel
                                                  handler:^(UIAlertAction *action) {
                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                  }];
    
    
    UIAlertAction *btnYes = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Yes"] style:UIAlertActionStyleDestructive
                                                  handler:^(UIAlertAction *action) {
                                                      
                                                      // delete the key and everything
                                                      [_deactivateBtn setHidden:YES];
                                                      
                                                      [UserDefaultData removeTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode];
                                                      
                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                  }];
    
    [alert addAction:btnNo]; [alert addAction:btnYes];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void) activeTouchIDLogin{

    
    if ([self activeValidated] && ![UserDefaultData getTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode])
    {
        [self executeLogin];
    }
    else
    {
        [self shakeView];
        [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"User ID and Password cannot be empty."]
                                     inController:self  withTitle:@""];

    }
    [self dismissKeyboard];
}


- (IBAction)activeTouchID:(id)sender{
    
    if (_tncAccept) {
    
        activeTouchID = YES;
        [self activeTouchIDLogin];

    } else {
        
        [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"You must accept Terms & Conditions before activating TouchID!"]
                                     inController:self  withTitle:@""];
        
    }
    
    [self.activeTextUsername resignFirstResponder];
    [self.activeTextPassword resignFirstResponder];
}

- (void) loginByTouchId{
    
    //====================================================================
    //  Should Check if Touch ID already active by getting the Token.
    //  If token if available then Show Touch ID Pop
    //  If token not available then show pop up to active the touch ID
    //====================================================================
    
    // Get Username from Keychain
    
    NSString *userName = [UserDefaultData getUsername];//_txtUserName.text;
    
    if ([UserDefaultData getTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode]) {
        _touchIDUsername = userName;
        // Get password from keychain
        _touchIDPassword = [UserDefaultData getPassword];
       // NSLog(@"username %@, password %@",_touchIDUsername, _touchIDPassword);
        
        if (_touchIDUsername.length>0 && _touchIDPassword.length>0) {
            [self executeLogin];
        }else{
            [self showUserActiveTouchID];
        }
    }else{
        [self showUserActiveTouchID];
    }
    
}


- (IBAction) touchIDButtonClicked:(id)sender{
    
    LAContext *context = [[LAContext alloc] init];
    NSError *error = nil;
    
    NSString *userName = [UserDefaultData getUsername];
    
    if ([UserDefaultData getTouchIDFlagForUser:[UserPrefConstants singleton].brokerCode]) {
        
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            
            
            // Authenticate User
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                    localizedReason:[LanguageManager stringForKey:@"Place your finger on the Home button to Login"]
                              reply:^(BOOL success, NSError *error) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      if (success) {
                                          
                                          //NSLog(@"User is authenticated successfully");
                                          [self loginByTouchId];
                                      } else {
                                          
                                          switch (error.code) {
                                              case LAErrorAuthenticationFailed:
                                                 // NSLog(@"Authentication Failed");
                                                  [self showAlertView:[LanguageManager stringForKey:@"Authentication failed, please proceed to normal login"] andTag:AuthenticationErrorFailed];
                                                  break;
                                                  
                                                  
                                              case LAErrorUserCancel:
                                                  //NSLog(@"User pressed Cancel button");
                                                  
                                                  
                                                  break;
                                                  
                                              case LAErrorUserFallback:
                                                //  NSLog(@"User pressed \"Enter Password\"");
                                                  
                                                  break;
                                              case LAErrorTouchIDNotEnrolled:
                                                  [self showAlertView:[LanguageManager stringForKey:@"Session Timeout!"] andTag:AuthenticationErrorTouchIDNotEnrolled];
                                                  
                                                  break;
                                              case kLAErrorTouchIDNotAvailable:
                                                  
                                                  [self showAlertView:[LanguageManager stringForKey:@"TouchID is not available on this device."] andTag:AuthenticationErrorTouchIDNotAvailable];
                                                  
                                                  break;
                                                  
                                              default:
                                                  
                                                //  NSLog(@"Touch ID is not configured");
                                                  [self showAlertView:[LanguageManager stringForKey:@"Authentication failed, please proceed to normal login"] andTag:AuthenticationErrorNotConfigured];
                                                  break;
                                          }
                                         // NSLog(@"Authentication Fails");
                                      }
                                  });
                                  
                                  
                                  
                                  
                              }];
        }
        else {
            
            [self showAlertView:[LanguageManager stringForKey:@"Your device cannot authenticate using TouchID."] andTag:AuthenticationErrorTouchIDNotEnrolled];
        }
    }else{
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            [self showUserActiveTouchID];
        } else {
            [self showAlertView:[LanguageManager stringForKey:@"Your device cannot authenticate using TouchID."] andTag:AuthenticationErrorTouchIDNotEnrolled];
        }
    }
    
}

- (void) showAlertView:(NSString *)errorDescription andTag:(AuthenticationError)authenID{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:[LanguageManager stringForKey:TC_Pro_Mobile]
                                  message:errorDescription
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:[LanguageManager stringForKey:OK]
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                             if (authenID==AuthenticationErrorNotConfigured) {
                                 [self showUserActiveTouchID];
                             }
                             
                             
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) showUserActiveTouchID{
    
    _activeTextUsername.text = _txtUserName.text;
    
    [UIView transitionWithView:self.loginContainerView
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromRight | UIViewAnimationOptionBeginFromCurrentState
                    animations: ^{
                        self.activeView.hidden = NO;
                        self.loginView.hidden = YES;
                        self.brokerSelectionView.hidden = YES;
                        activeTouchID = YES;
                    }
     
                    completion:^(BOOL finished) {
                        displayingFront = YES;
                        
                    }];
    
    if (![self.activeView isHidden]) {
        
        [UIView transitionWithView:_logoImage
                          duration:0.5
                           options:UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                        animations: ^{
                            [_logoImage setAlpha:0.0f];
                        }
         
                        completion:^(BOOL finished) {
                            [_logoImage setAlpha:0.0f];
                            
                        }];
    }
    
}

#pragma mark -
#pragma mark VertxConnectionManager
//====================================================================
//                VertxConnectionManager Delegate
//====================================================================




- (IBAction)AcceptbtnPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"disclaimer"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.5];
    [UIView setAnimationDelay:.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [self.DisclaimerView setHidden:YES];
    [self.loginView setAlpha:0.7];
    [self.loginContainerView2 setAlpha:0.7];
   
    [UIView commitAnimations];
    
}
- (IBAction)DeclineBtnPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"disclaimer"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    exit(0);
    
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail sent");
        {
            UIAlertView  *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Mail sent" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];

            
        }
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView  *alert = [[UIAlertView alloc]initWithTitle:@"Failed" message:[NSString stringWithFormat:@"Mail sent failure %@",[error localizedDescription]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];

            
        }
            //NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void) dealloc{
    
    //NSLog(@"goodbye Login View");
    
}


@end
