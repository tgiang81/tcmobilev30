//
//  MagnifyEffect.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 28/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "MagnifyEffect.h"
#import <QuartzCore/QuartzCore.h>

#define SYSTEM_VERSION_EQUAL_OR_GREATER_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

static CGFloat const kACLoupeDefaultRadius = 64;

@implementation MagnifyEffect

- (id)init {
    self = [self initWithFrame:CGRectMake(0, 0, kACLoupeDefaultRadius*2.5, kACLoupeDefaultRadius*2.5)];
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.layer.borderWidth = 0;
        
        UIImageView *magnifyImage = nil;
        
        magnifyImage = [[UIImageView alloc] initWithFrame:CGRectOffset(CGRectInset(self.bounds, -3.0, -3.0), 0, 2.5)];
        magnifyImage.image = [UIImage imageNamed:@"MagnifyEffect"];
       
        magnifyImage.backgroundColor = [UIColor clearColor];
        [self addSubview:magnifyImage];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
