//
//  StockTrackerViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 14/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "StockTrackerViewController.h"
#import "StockTracker.h"
#import "VertxConnectionManager.h"
#import "UserPrefConstants.h"
#import "ATPAuthenticate.h"
#import "LanguageKey.h"

@interface StockTrackerViewController () <UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *watchListArr;
    VertxConnectionManager *vertxConnectionManager;
    ATPAuthenticate *atp;
    UITableViewController *popupTableViewController;
    UITableViewController *popupFilterTableViewController;
    UIPopoverController *columnPickerPopover;
    int counter;
    int valueCount;
    int quantityCount;
    NSArray *filterByArray;
    int numberOfLine;
    BOOL leftDirection;
}
@property (nonatomic, strong) NSMutableArray *tickerArray;
@end

@implementation StockTrackerViewController
@synthesize tickerArray;
@synthesize directionButton;

- (IBAction) clickButtonDirection:(id)sender{
    leftDirection =! leftDirection;
   
    if (leftDirection) {
        [directionButton setImage:[UIImage imageNamed:@"BackwardButton"] forState:UIControlStateNormal];

    }else{
        [directionButton setImage:[UIImage imageNamed:@"ForwardButton"] forState:UIControlStateNormal];
;
    }
    
    for (int x=0;x<3;x++)
    {
        for(int y=0; y<9;y++){
            
            int count = x*9+y;
            
             StockTracker *ticker = (StockTracker *)[self.containerView viewWithTag:count+2];
            if (leftDirection) {
           
                [ticker setFrame:CGRectMake(860-y*(105),50+x*(155), 100, 150)];
                
            }else{
               
               [ticker setFrame:CGRectMake(20+y*(105),50+x*(155), 100, 150)];
            }
        }
    }
    
}

- (IBAction) clickButtonLine:(id)sender{
    numberOfLine++;
    if (numberOfLine>3) {
        numberOfLine=1;
    }
    arrayLimit = numberOfLine*9;
    [_lineButton setTitle:[NSString stringWithFormat:@"%d",numberOfLine] forState:UIControlStateNormal];
    
    for(int x=0;x<27;x++){
        
        if (x>=arrayLimit) {
            StockTracker *ticker = (StockTracker *)[self.containerView viewWithTag:x+2];
            ticker.alpha = 0;
        }
    }
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    counter =0;
    numberOfLine =1;
    arrayLimit = numberOfLine*9;
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestWatchListContentReceived) name:@"getWatchlistContentSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(marketStreamerReceived:)  name:@"marketStreamerReceived" object:nil];
}

- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to
{
    if (to != from) {
        id obj = [tickerArray objectAtIndex:from];
        [tickerArray removeObjectAtIndex:from];
        if (to >= [tickerArray count]) {
            [tickerArray addObject:obj];
        } else {
            [tickerArray insertObject:obj atIndex:to];
        }
    }
}

#pragma mark - Receive notification from Vertx
- (void)marketStreamerReceived:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary * response = [notification.userInfo copy];
        
        if(tickerArray.count<arrayLimit){
            if (tickerArray.count==0) {
                [tickerArray addObject:response];
            }else{
                [tickerArray insertObject:response atIndex:0];
            }
          //  NSLog(@"marketStreamerReceived %@ = %d",response,counter);
        }else{
            if (counter>=arrayLimit) {
                counter=0;
            }
             [tickerArray insertObject:response atIndex:0];
            
            //[tickerArray replaceObjectAtIndex:counter withObject:response];
            //NSLog(@"replaceObjectAtIndex %@ = %d",response,counter);
        }
        
        if (counter<arrayLimit) {
            // NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:leftCount inSection:0];
            // NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            // [tableViewLeft reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
            
            // Update Content
            for (int x=0;x<=counter;x++) {
                [self setTickerContent:x];
            }
        }
        
        // populate the data to screen
        counter++;
        NSMutableArray *discardedItems = [NSMutableArray array];
        if (tickerArray.count>arrayLimit) {
            int x=0;
            for (NSDictionary * response in tickerArray) {
                if (x>arrayLimit) {
                    [discardedItems addObject:response];
                }
                x++;
            }
         [tickerArray removeObjectsInArray:discardedItems];
        }
        
        NSLog(@"Ticker Array Size %lu",(unsigned long)tickerArray.count);
    });
    
    
   
}

- (void) setTickerContent:(int)tag{
 //   NSLog(@"ticker Tag %d",tag);
    StockTracker *ticker;
    ticker = (StockTracker *)[self.containerView viewWithTag:tag+2];
    ticker.alpha = 1;
    
    NSString *stockCode = [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag] objectForKey:@"33"]];
    NSString *conditon =  [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag] objectForKey:@"104"]];
 
    float lacp =  [[[tickerArray objectAtIndex:tag] objectForKey:@"51"] floatValue];
    float price = [[[tickerArray objectAtIndex:tag] objectForKey:@"98"] floatValue];
    
    NSString *quantity = @"";
    SET_IF_NOT_NULL(quantity,[[tickerArray objectAtIndex:tag] objectForKey:@"99"]);
    
    NSString *bbh_sbhArray = @"";
    SET_IF_NOT_NULL(bbh_sbhArray,[[tickerArray objectAtIndex:tag] objectForKey:@"120"]);
    
    //NSString *quantity =  [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag] objectForKey:@"99"]];
    
   // NSString *bbh_sbhArray =  [NSString stringWithFormat:@"%@", [[tickerArray objectAtIndex:tag]  objectForKey:@"120"]];
    //NSString *bbh = [NSString stringWithFormat:@"%d",test];//
    NSString *bbh = @"";
    NSString *sbh = @"";
    if (bbh_sbhArray.length!=0) {
        bbh = [[bbh_sbhArray componentsSeparatedByString:@","] objectAtIndex:1];
        
        sbh = [[bbh_sbhArray componentsSeparatedByString:@","] objectAtIndex:3];
    }

    [ticker updateContent:stockCode
                 andPrice:price
                  andLACP:lacp
              andQuantity:quantity
             andCondition:conditon
                  andBBH:bbh
                andSBH:sbh];
    
}

- (void) latestWatchListContentReceived{
   // NSLog(@"latestWatchListContentReceived %@",[UserPrefConstants singleton].userWatchListStockCodeArr);
    [vertxConnectionManager vertxGetMarketStreamer:[UserPrefConstants singleton].userWatchListStockCodeArr andValue:valueCount andQuantity:quantityCount andCondition:0];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    popupTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    popupTableViewController.tableView.delegate=self;
    popupTableViewController.tableView.dataSource=self;
    [popupTableViewController.tableView setTag:1];
    
    popupFilterTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    popupFilterTableViewController.tableView.delegate=self;
    popupFilterTableViewController.tableView.dataSource=self;
    [popupFilterTableViewController.tableView setTag:2];
    
    filterByArray = [[NSArray alloc] initWithObjects:@"None",@"Value >= 2M",@"Vol >= 50K",@"Vol >= 30K & Value >= 1M",@"Vol >= 50K & Value >= 2M",nil];
    vertxConnectionManager = [VertxConnectionManager singleton];
    [vertxConnectionManager vertxGetMarketStreamer:nil andValue:0 andQuantity:0 andCondition:0];
     tickerArray = [[NSMutableArray alloc] init];
    atp = [ATPAuthenticate singleton];
    
 //NSLog(@" (wid=%f,height=%f)",self.containerView.frame.size.width,self.containerView.frame.size.height);
    for (int x=0;x<3;x++)
    {
        for(int y=0; y<9;y++){
        
        int count = x*9+y;
            
        StockTracker *ticker = [[StockTracker alloc] initWithFrame:CGRectMake(860-y*(105),50+x*(155), 100, 150)];
        [ticker setTag:count+2];
        
        ticker.autoresizesSubviews = YES;
       
        [self.containerView addSubview:ticker];
         //   NSLog(@"size = %d (wid=%f,height=%f)",count+2,ticker.frame.origin.x,ticker.frame.origin.y);
        ticker.alpha = 0;
      //ticker.lblStkName.text = [NSString stringWithFormat:@"%d",count+2];
            
        }
    }
     [self loadWatchList];
    self.containerView.autoresizesSubviews = NO;
    //[NSTimer scheduledTimerWithTimeInterval: 1.0                                     target: self                                   selector: @selector(handleTimer)                                   userInfo: nil                                    repeats: YES];

    
    // Do any additional setup after loading the view.
}


- (void) handleTimer{
    [self testStreamer];
}

- (void) testStreamer{
    
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"TestStreamer" ofType:@"plist"];
    NSDictionary *response = [NSDictionary dictionaryWithContentsOfFile:file];

    if(tickerArray.count<arrayLimit){
        if (tickerArray.count==0) {
            [tickerArray addObject:response];
        }else{
            [tickerArray insertObject:response atIndex:0];
        }
        
      //  NSLog(@"marketStreamerReceived %@ = %d",response,counter);
    }else{
        if (counter>=arrayLimit) {
            counter=0;
        }
         [tickerArray insertObject:response atIndex:0];
        //[tickerArray replaceObjectAtIndex:counter withObject:response];
        //NSLog(@"replaceObjectAtIndex %@ = %d",response,counter);
    }
    
    if (counter<arrayLimit) {
        // NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:leftCount inSection:0];
        // NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        // [tableViewLeft reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
        
        // Update Content
        for (int x=0;x<=counter;x++) {
            [self setTickerContent:x];
        }
    // populate the data to screen
    counter++;
    }
}

-(void) loadWatchList{
    watchListArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
    [watchListArr insertObject:@"None" atIndex:0];
    //NSLog(@"watchListArr %@",watchListArr);
    // if watchlist exist, select the first one
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    [[NSNotificationCenter defaultCenter] removeObserver:@"getWatchlistContentSuccess"];
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag==1) {
        return watchListArr.count;
    }else{
        return 5;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *SimpleCellIdentifier = @"Cell";
    UITableViewCell *simpleCell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:SimpleCellIdentifier];
    if (simpleCell == nil) {
        simpleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleCellIdentifier];
    }
    simpleCell.textLabel.backgroundColor = [UIColor clearColor];
    simpleCell.textLabel.textColor = [UIColor darkTextColor];
    simpleCell.textLabel.font =  [UIFont systemFontOfSize:16];
    simpleCell.textLabel.numberOfLines = 2;
    simpleCell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
   // NSLog(@"left Count = %d",counter);
    if (tableView.tag==1) {
        if (indexPath.row==0) {
            simpleCell.textLabel.text = @"None";
        }else{
            simpleCell.textLabel.text = [[watchListArr objectAtIndex:indexPath.row] objectForKey:@"Name"];
        }
    }else{
        simpleCell.textLabel.text = [filterByArray objectAtIndex:indexPath.row];
    }
    return simpleCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag==1) {
        
        if (indexPath.row==0) {
            _watchListLabel.text = @"None";
            favID = 0;
        }else{
            _watchListLabel.text = [[watchListArr objectAtIndex:indexPath.row] objectForKey:@"Name"];
            favID = [[[watchListArr objectAtIndex:indexPath.row] objectForKey:@"FavID"] intValue];
        }
        
        
    }else{
        _filterByLabel.text = [filterByArray objectAtIndex:indexPath.row];
        switch (indexPath.row) {
            case 0:
                valueCount = 0;
                quantityCount = 0;
                break;
            case 1:
                valueCount = 2000000;
                quantityCount = 0;
                break;
            case 2:
                valueCount = 0;
                quantityCount = 50000;
                break;
            case 3:
                valueCount = 100000;
                quantityCount = 30000;
                break;
            case 4:
                valueCount = 2000000;
                quantityCount = 50000;
                break;
            default:
                break;
        }
    }
    [columnPickerPopover dismissPopoverAnimated:TRUE];
}

- (IBAction)clickApplyButton:(UIButton *)sender{
    
    counter = 0;
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    for(int x=0;x<arrayLimit;x++){
        StockTracker *ticker = (StockTracker *)[self.containerView viewWithTag:x+2];
      
        ticker.alpha = 0;
    }
    [tickerArray removeAllObjects];
    
    if (favID!=0) {
     //   NSLog(@"Fav ID %d",favID);
        [atp getWatchListItemsForStreamer:favID];
    }else{
        [vertxConnectionManager vertxGetMarketStreamer:nil andValue:valueCount andQuantity:quantityCount andCondition:0];
    }
}

- (IBAction)clickWatchList:(UIButton *)sender{
    [popupTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupTableViewController];
    navController.navigationBar.topItem.title= _WatchList;
    
    popupTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    NSInteger totalToDisplay = 5;
    if (totalToDisplay>=10) totalToDisplay = 10;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*60);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 40;
}

- (IBAction)clickFilterBy:(UIButton *)sender{
    [popupFilterTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupFilterTableViewController];
    navController.navigationBar.topItem.title= @"Filter By";
    
    popupFilterTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    navController.preferredContentSize=CGSizeMake(325, 200);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
