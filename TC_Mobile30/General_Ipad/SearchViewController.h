//
//  SearchViewController.h
//  TCiPad
//
//  Created by n2n on 23/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Speech/Speech.h>
#import "LanguageManager.h"
#import "QCData.h"
#import "VertxConnectionManager.h"
#import "SCSiriWaveformView.h"
#import <AVFoundation/AVFoundation.h>
#import "StocksCollectionViewCell.h"
#import "WatchListStk.h"

typedef enum {
    PANEL_MODE_SEARCH = 1,
    PANEL_MODE_ADDSTOCK
} PanelMode;


@interface SearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,
UIGestureRecognizerDelegate, SFSpeechRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout, StockCellDelegate>

@property (weak, nonatomic) IBOutlet UILabel *recognizedTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *panelTitle;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet UITableView *searchTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *tableDataArr;
@property (assign, nonatomic) PanelMode panelMode;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

// add to watchlist
@property (nonatomic, strong) NSMutableArray *stocksArray;
@property (weak, nonatomic) IBOutlet UICollectionView *stockCollectionView;
@property (weak, nonatomic) IBOutlet UIView *stockListView;

// waveform
@property (weak, nonatomic) IBOutlet SCSiriWaveformView *waveFormView;
@property (nonatomic, assign) CGFloat averagePowerForChannel0;

// Speech recognition
@property (weak, nonatomic) IBOutlet UIView *speechWindow;
@property (nonatomic, strong) SFSpeechRecognizer *speechRecognizer;
@property (nonatomic, strong) SFSpeechAudioBufferRecognitionRequest *recognitionRquest;
@property (nonatomic, strong) SFSpeechRecognitionTask *recognitionTask;
@property (nonatomic, strong) AVAudioEngine *audioEngine;
@property (nonatomic, assign) BOOL speechOpened, speechAvail;
@property (weak, nonatomic) IBOutlet UIButton *dictateBtn;
@property (weak, nonatomic) IBOutlet UIButton *bigDictateBtn;
@property (nonatomic, strong) NSMutableArray *wordsArr;
@property (nonatomic, assign) int idxToEdit;
@property (weak, nonatomic) IBOutlet UILabel *speechTitle;
@property (weak, nonatomic) IBOutlet UIButton *activateButton;
@property (nonatomic, strong) AVAudioPlayer *sndPlayer;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

// pullmore
@property (strong, nonatomic) IBOutlet UIView *pullMoreView;
@property (weak, nonatomic) IBOutlet UIView *pullMoreContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *pullActivity;
@property (weak, nonatomic) IBOutlet UILabel *pullMoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pullImg1;
@property (weak, nonatomic) IBOutlet UIImageView *pullImg2;
@property (nonatomic, assign) int canLoadData;

- (IBAction)cancelSearch:(id)sender;
- (IBAction)goBack:(id)sender;
- (IBAction)openSpeechWindow:(id)sender; // by big button
- (IBAction)openSpeechWindow2:(id)sender; // by small button
- (IBAction)closeSpeechWindow:(id)sender;
- (IBAction)activateSpeech:(id)sender;
- (IBAction)addToWatchlist:(id)sender;

@end
