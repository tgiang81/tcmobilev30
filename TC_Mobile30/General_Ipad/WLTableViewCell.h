//
//  WLTableViewCell.h
//  TCiPad
//
//  Created by n2n on 21/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WLTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *watchListName;
@property (strong, nonatomic) IBOutlet UILabel *watchListArrow;

@end
