//
//  StockDetailViewController.m
//  TCUniversal
//
//  Created by Scott Thoo on 11/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "StockDetailViewController.h"
#import "UserPrefConstants.h"
#import "QCData.h"
#import "QCConstants.h"
#import "VertxConnectionManager.h"
#import "TimeAndSalesTableViewCell.h"
#import "ATPAuthenticate.h"
#import "MainFrameViewController.h"
#import "ContainerViewController.h"
#import "TransitionDelegate.h"
#import "NewsModalViewController.h"
#import "UIImageUtility.h"
#import "AppConstants.h"
#import "MDTableViewCell.h"
#import "AppControl.h"
#import "TraderViewController.h"
#import "MarqueeLabel.h"
#import "ButtonEffect.h"
#import "NewsTableViewCell.h"
#import "BDoneTableViewCell.h"
#import "ImgChartManager.h"
#import "ExchangeData.h"
#import "TradingRules.h"
#import "LanguageKey.h"


@interface StockDetailViewController () <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,
UIWebViewDelegate,NSURLConnectionDelegate,TraderDelegate, MDCellDelegate>
{
    UserPrefConstants *_userPref;
    VertxConnectionManager *_vcm;
    TraderViewController *tradeVC;
    ATPAuthenticate *_atp;
    QCData *_qcData;

    int lotSizeStringInt;
    MarqueeLabel *sectorLabel;
    ExchangeData *currentEd;
    MDInfo *mdInfo;
    TradingRules *trdRules;
    
    __weak IBOutlet UIScrollView *scrollViewStockDetail;
    __weak IBOutlet UIPageControl *pageControlStockDetail;
    NSMutableData *_responseData;
    NSMutableDictionary *tempDictionary;
    BOOL isUpdate;
    BOOL canRefreshTimeSales, canRefreshBizDone;
    BOOL ismarketDepthupd;
    BOOL mktDepthData;
    BOOL mktDepthZero;
    BOOL isLoadingNews;
    NSMutableArray *MDBuyArr;
    NSMutableArray *MDBuyPrevArr;
    NSMutableArray *MDSellArr;
    NSMutableArray *MDSellPrevArr;
    
    NSArray *mktDepthArr;
    BOOL isNewsOpen;
    NSArray *arrayWithTwoStrings;
    NSNumberFormatter *quantityFormatterTimeAndSales;

    NSArray *stkCodeAndExchangeArr;

    UIViewController *viewcont;

    int prev_selection ;
    int selection ;
    long tag;
    
    NSNumberFormatter *priceFormatter;
    
    BOOL isFullScreenChart;
    CGRect webViewOriginalFrame;
    BOOL callOnce;
    long currentTotDays;
    
    // MKT DEPTH FLASH PURPOSE
    long long p_totalBuy;
    long long p_totalSell;
    long long p_totBuySplit; long long p_totBuyQty;  CGFloat p_aveBuyBid;
    long long p_totSellSplit; long long p_totSellQty; CGFloat p_aveSellAsk;
    
    NSString *bundleIdentifier;
}
@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation StockDetailViewController
@synthesize chartWebView;
@synthesize timeAndSalesTableView;
@synthesize companyNewsTableView;
@synthesize lblMarketCap,lblShareIssued,lblTotalBuyTrans,lblTotalBuyVolume,lblTotalSellTrans,lblTotalSellVolume;
@synthesize btnloading;
@synthesize transitionController;
@synthesize marketCapLabel;
@synthesize shareIssueLabel;
@synthesize stockLabel;
@synthesize tradingBoardLabel;
@synthesize ssellVolumeLabel;
@synthesize lotSizeLabel;
@synthesize isinLabel;
@synthesize parLabel;
@synthesize valueTradedLabel;
@synthesize activeLabel;
@synthesize archiveNewsTableDataArr,companyNewsTableDataArr,bizDoneTableDataArr;

@synthesize lblTotalShortSellVol;
@synthesize lblCurrency;
-(void)hideAllTabItems {
    
    [companyNewsTableView setHidden:YES];
    [_timeSalesView setHidden:YES];
    [_bizDoneView setHidden:YES];
    [_fundamentalView setHidden:YES];
    
}


#pragma mark - N2Tabs Delegate
-(void)tabDidClicked:(UIButton *)tabButton atTab:(N2Tabs*)tab {
    [self hideAllTabItems];
    [_refreshControl endRefreshing];
    switch (tabButton.tag) {
        case 0:{
            [timeAndSalesTableView addSubview:_refreshControl];
            canRefreshTimeSales = YES;
            [_timeSalesView setHidden:NO];
            [self populateTimeAndSales];
        }break;
        case 1:{
            [_bizDoneTable addSubview:_refreshControl];
            canRefreshBizDone = YES;
            [_bizDoneView setHidden:NO];
            [self populateBusinessDone];
        }break;
        case 2:{
            [companyNewsTableView setHidden:NO];
            [self populateCompanyNews];
        }break;
        case 3:{
            [_fundamentalView setHidden:NO];
        }break;
        default:
            break;
    }
    
}

#pragma mark - TradeAccount Delegate

-(void)accountSelected:(UserAccountClientData *)account {
   // update account details in trader
    
    [tradeVC updateTradingAccount:account];
    
}

#pragma mark - GeneralSelector Delegates
-(void)selectorItemSelected:(NSIndexPath *)indexPath selectorType:(GeneralSelectorType)type {
    
    if (type==TYPE_WLIST) {
        SectorInfo *eachWL = [self.sortedWLArr objectAtIndex:indexPath.section];
//        NSLog(@"WL: %@",eachWL.sectorName);
        
        [_atp addWatchlistCheckItem:[eachWL.sectorCode intValue]  withItem:_stkCode];
    }
    
}


#pragma mark - View LifeCycles Delegates


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"embedTraderID"])
    {
        tradeVC = (TraderViewController*)segue.destinationViewController;
        tradeVC.containerFrame = _traderContainer.frame;
        //NSLog(@"Stock Lot Size %d",lotSizeStringInt);
        tradeVC.stockLotSize = lotSizeStringInt;
        tradeVC.delegate = self;
        tradeVC.parentController = self;
    }
    
    if ([segue.identifier isEqualToString:@"tradeAccountSegueID2"]) {
        TradeAccountController *vc = (TradeAccountController*)segue.destinationViewController;
        vc.delegate = self;
    }
    
    if ([segue.identifier isEqualToString: @"segueAddToWatchlist"]) {
        
        long cnt = [UserPrefConstants singleton].userWatchListArray.count;
        NSMutableArray *wlArray = [[NSMutableArray alloc] init];
        for (int i=0; i<cnt; i++) {
            SectorInfo *inf = [[SectorInfo alloc] init];
            NSDictionary *eachWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:i];
            inf.sectorCode = [eachWL objectForKey:@"FavID"];
            inf.sectorName = [eachWL objectForKey:@"Name"];
            [wlArray addObject:inf];
        }
        
        // sort
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sectorName"
                                                     ascending:YES];
        self.sortedWLArr = [wlArray sortedArrayUsingDescriptors:@[sortDescriptor]];
        
        GeneralSelectorViewController *vc = (GeneralSelectorViewController*)segue.destinationViewController;
        vc.selectorArray = [[NSMutableArray alloc] initWithArray:_sortedWLArr];
        vc.delegate = self;
        vc.selectorType = TYPE_WLIST;
        
    }
}

-(void)viewDidLayoutSubviews {
    
    if (!callOnce) {
        
        // get Current Exchange data
        currentEd = [[ExchangeData alloc] init];
        for (ExchangeData *ed in [_userPref userExchagelist]) {
            // NSLog(@"ed %@ currentExch %@",ed.feed_exchg_code, _userPref.userCurrentExchange);
            if ([ed.feed_exchg_code isEqualToString:[_userPref userCurrentExchange]]) {
                currentEd = ed;
            }
        }
        
        
        
        //Stock details pages
        int totalPages = 3;
        scrollViewStockDetail.delegate  = self;
        scrollViewStockDetail.contentSize = CGSizeMake(scrollViewStockDetail.frame.size.width*totalPages, 268);
        
        
        [scrollViewStockDetail addSubview:_detail0View];
        [scrollViewStockDetail addSubview:_detail1View];
        [scrollViewStockDetail addSubview:_detail2View];
        pageControlStockDetail.numberOfPages = totalPages;
        pageControlStockDetail.currentPage = 0;
        
        webViewOriginalFrame = _chartView.frame;
        callOnce = YES;
        
        [_detail0View setBackgroundColor:[UIColor clearColor]];
        [_detail1View setBackgroundColor:[UIColor clearColor]];
        
        _detail0View.frame = CGRectMake(scrollViewStockDetail.frame.size.width*0,
                                        0,
                                        scrollViewStockDetail.frame.size.width,
                                        scrollViewStockDetail.frame.size.height);
        
        _detail1View.frame = CGRectMake(scrollViewStockDetail.frame.size.width*1,
                                        0,
                                        scrollViewStockDetail.frame.size.width,
                                        scrollViewStockDetail.frame.size.height);
        
        _detail2View.frame = CGRectMake(scrollViewStockDetail.frame.size.width*2,
                                        0,
                                        scrollViewStockDetail.frame.size.width,
                                        scrollViewStockDetail.frame.size.height);
        
        _traderContainer.frame = _chartView.frame;
        
        _btnBuy.layer.cornerRadius = kButtonRadius*_btnBuy.frame.size.height;
        _btnBuy.layer.masksToBounds = YES;
        _btnSell.layer.cornerRadius = kButtonRadius*_btnSell.frame.size.height;
        _btnSell.layer.masksToBounds = YES;
        _heart.layer.cornerRadius = kButtonRadius*_heart.frame.size.height;
        _heart.layer.masksToBounds = YES;
        
        NSString *fundamentalLabel = [UserPrefConstants singleton].fundamentalNewsLabel;
        NSString *fundTitle = @"";
        if ([fundamentalLabel length]>0) {
            fundTitle = fundamentalLabel;
        } else {
            fundTitle = @"S&P Capital IQ";
        }
        
        [_fundamentalView setBackgroundColor:kPanelColor];
        
        if ([self populateFundamentals]) {
            
            NSArray *params = [NSArray arrayWithObjects:@[@"Time & Sales",@"Business Done",@"News",fundTitle],
                               [UIColor cyanColor],
                               kPanelColor, nil];
            N2Tabs *tabs = [[N2Tabs alloc] initWithParams:params withFrame:_tabsPlaceholder.frame andFontSize:15.0f];
            tabs.tabDelegate = self;
            [self.view addSubview:tabs];
            
        } else {
            NSArray *params = [NSArray arrayWithObjects:@[@"Time & Sales",@"Business Done",@"News"],
                               [UIColor cyanColor],
                               kPanelColor, nil];
            N2Tabs *tabs = [[N2Tabs alloc] initWithParams:params withFrame:_tabsPlaceholder.frame andFontSize:15.0f];
            tabs.tabDelegate = self;
            [self.view addSubview:tabs];
            
        }
        
        [self.view bringSubviewToFront:_chartView];
        
        
        for (MDInfo *inf in [UserPrefConstants singleton].currentExchangeInfo.qcParams.exchgInfo) {
            if ([inf.exchange isEqualToString:_userPref.userCurrentExchange]) {
                mdInfo = inf;
            }
        }
        
        
        
        [[LanguageManager defaultManager] applyAccessibilityId:self];
        [[LanguageManager defaultManager] translateStringsForViewController:self];
    }
    
    sectorLabel.frame = stockLabel.frame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//   bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
        _lacpLabel.text = @"TOP";
    }
    
    timeAndSalesTableView.delegate = self;
    timeAndSalesTableView.dataSource = self;
    
    companyNewsTableView.delegate = self;
    companyNewsTableView.dataSource = self;

    [self setChartStatus:5];

    mktDepthData = NO;
    mktDepthZero = NO;
    [_mdFooter setHidden:YES];
    
    p_totalBuy = p_totalSell = p_totBuySplit = p_totBuyQty = p_aveBuyBid = p_totSellSplit= p_totSellQty =  p_aveSellAsk =0;
    
    callOnce = NO;
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTappedChart:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    doubleTapGesture.delegate = self;
    isFullScreenChart = NO;
    [_chartView addGestureRecognizer:doubleTapGesture];
    
    currentTotDays = K_1YEAR; // default

     self.transitionController = [[TransitionDelegate alloc] init];
    _userPref = [UserPrefConstants singleton];
    _qcData = [QCData singleton];
    _atp = [ATPAuthenticate singleton];
 
    _vcm = [VertxConnectionManager singleton];
    
  // NSLog(@"USERSTKCODE: %@", _userPref.userSelectingStockCode);
    
   if (_userPref.userSelectingStockCode == nil)
    {
        if ([[UserPrefConstants singleton].tickerStocks count]>0) {
            TickerData *firstStock = [[UserPrefConstants singleton].tickerStocks objectAtIndex:0];
            [UserPrefConstants singleton].userSelectingStockCode = firstStock.stkCode;
            _stkCode = firstStock.stkCode;
        } else {
            // input N2N stock as default when no feed no data no anything from server
            [[VertxConnectionManager singleton] vertxGetStockDetailWithStockArr:@[@"0108.KL"]
                                                                         FIDArr:[[AppConstants SortingList] allKeys] tag:0];
        }
    }
    else
    {
        _stkCode = _userPref.userSelectingStockCode ;
    }
    
    // NSLog(@"viewDidLoad _stkCode %@",_stkCode);

    stkCodeAndExchangeArr= [ _stkCode componentsSeparatedByString:@"."];
    trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];


    priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    [priceFormatter setGeneratesDecimalNumbers:YES];
    ////NSLog(@" Stock Detail Page : _userPref.userSelecting_stkCode : %@",_userPref.userSelectingStockCode);
    [UserPrefConstants singleton].quoteScreenFavID=nil;


    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor whiteColor];

    
    //Solving separator inset issue
    timeAndSalesTableView.separatorColor = [UIColor colorWithRed:255 green:242 blue:13 alpha:1.0];
    NSString *iosversion = [[UIDevice currentDevice] systemVersion];
    int version = [iosversion intValue];
    if(version>6)
    {
        timeAndSalesTableView.separatorInset = UIEdgeInsetsZero;
    }
    
    [chartWebView setDelegate:self];
    isUpdate =false;
    ismarketDepthupd= false;
    
    MDBuyArr= [[NSMutableArray alloc]init];
    MDBuyPrevArr= [[NSMutableArray alloc]init];
    MDSellArr= [[NSMutableArray alloc]init];
    MDSellPrevArr= [[NSMutableArray alloc]init];
    mktDepthArr = [[NSArray alloc]init];
    
    bizDoneTableDataArr = [[NSArray alloc] init];
    
    quantityFormatterTimeAndSales = [NSNumberFormatter new];
    [quantityFormatterTimeAndSales setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatterTimeAndSales setGroupingSeparator:@","];
    [quantityFormatterTimeAndSales setGroupingSize:3];
    [quantityFormatterTimeAndSales setGeneratesDecimalNumbers:YES];
    
    companyNewsTableView.separatorColor = [UIColor whiteColor];
    
    
    //Hide "Trade" button is ATP did not return trading rules or no account found
   /* NSString *temp_feed_exchg_code = feed_exchg_code;
    if (!derivative && marketFromLastScreen != nil) {
        temp_feed_exchg_code = [NSString stringWithFormat:@"%@%@", temp_feed_exchg_code, marketFromLastScreen.exchgMarketSymbol];
    }
    if ([appData.userAccountInfo.tradingRulesDict objectForKey:temp_feed_exchg_code] == nil || [appData.userAccountInfo.accountList count] == 0) {
        buttonBuySell.hidden = YES;
        marketDepthTableViewController.isButtonBidAskDisable = YES;
    }*/
    


}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // NSLog(@"stock DetailView will Disappear"); didFinishedBusinessDone
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishGetElasticNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doneVertxSearchStock" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didFinishedBusinessDone" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didFinishedGetStockNews" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didFinishedTimeAndSales" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didUpdate" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"mdDidUpdate" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didFinishMarketDepth" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didReceiveExchangeList" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"addWatchListItemsReply" object:nil];
    
    
    [_vcm vertxUnsubscribeMktDepth];
    
    [MDBuyPrevArr removeAllObjects];
    [MDSellPrevArr removeAllObjects];
    
    //giangtest stop timer request
    //Cancel perfrom request getting Time & Sale.
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(populateTimeAndSales) object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
    //    vc.view.backgroundColor = [UIColor clearColor];
    //    [vc setTransitioningDelegate:transitionController];
    //    vc.modalPresentationStyle= UIModalPresentationCustom;
    //    [self presentViewController:vc animated:YES completion:nil];
    
}



- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[UserPrefConstants singleton]userAccountlist].count==0 || trdRules.exchg_code==nil){
        [_btnSell setHidden:YES];
        [_btnBuy setHidden:YES];
    }
    
    [_traderContainer setHidden:YES];
    [_atp timeoutActivity];
    
    //NSLog(@"stock DetailView will appear");


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchDataDictReceived:)  name:@"doneVertxSearchStock" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetArchiveNews:) name:@"didFinishGetArchiveNews" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetElasticNews:) name:@"didFinishGetElasticNews" object:nil];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedGetStockNews:) name:@"didFinishedGetStockNews" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedBusinessDone:) name:@"didFinishedBusinessDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedTimeAndSales:) name:@"didFinishedTimeAndSales" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdate:) name:@"didUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mdDidUpdate:) name:@"mdDidUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishMarketDepth:) name:@"didFinishMarketDepth" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMarketDepth) name:@"didReceiveExchangeList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addWatchListItemsReply:) name:@"addWatchListItemsReply" object:nil];
    
   // NSLog(@"USERSTKCODE: %@ : %@", _userPref.userSelectingStockCode,_stkCode);
    
    NSString *tranNo;
    tranNo = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode ]objectForKey:FID_103_I_TRNS_NO];
    
//    [[VertxConnectionManager singleton] vertxSearchStkCodeDetail:_stkCode];

    if (tranNo==nil) {
        NSLog(@"TranNo empty");

        // Need to call back the APIs.
      //  [[VertxConnectionManager singleton] vertxSearchStkCodeDetail:_stkCode];
        [[VertxConnectionManager singleton] vertxSearchStkCodeDetail:_stkCode];

    }else{

        [btnloading hidesWhenStopped];
        [btnloading startAnimating];
        [self populateWebView];
        [self populateStockDetails];
        [self getMarketDepth];

        [self hideAllTabItems];
        [timeAndSalesTableView addSubview:_refreshControl];
        canRefreshTimeSales = YES;
        [_timeSalesView setHidden:NO];
        [self populateTimeAndSales];
    }
    
}


#pragma mark - Receive notification from Vertx
- (void)searchDataDictReceived:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [btnloading hidesWhenStopped];
        [btnloading startAnimating];
        [self populateWebView];
        [self populateStockDetails];
        [self getMarketDepth];
        
        [self hideAllTabItems];
        [timeAndSalesTableView addSubview:_refreshControl];
        canRefreshTimeSales = YES;
        [_timeSalesView setHidden:NO];
        [self populateTimeAndSales];
        
    });
}


#pragma mark - TapGesture delegates

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


-(UIImage*)captureWebView
{
    UIGraphicsBeginImageContext(_chartView.bounds.size);
    [_chartView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}




-(void)setButtonFontScale:(CGFloat)scale {
    
    
    for(UIView *v in [self.chartView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            if (btn.tag>50) {
                [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:scale]];
            }
        }
    }
    
}

-(void)doubleTappedChart:(UITapGestureRecognizer*)recognizer {
    
   // NSLog(@"chart type %@", [UserPrefConstants singleton].chartType);
    
    if (!isFullScreenChart) {
        [self.view bringSubviewToFront:_chartView];
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        dummyView.frame = _chartView.frame;
        dummyView.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:dummyView];
        
        __block CGFloat scaleFont = _mainView.frame.size.width/dummyView.frame.size.width;
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            
            dummyView.frame = _mainView.frame;
            _chartView.hidden = YES;
            _chartView.frame = _mainView.frame;
            UIButton *tmp = (UIButton*)[self.chartView viewWithTag:51];
            scaleFont = scaleFont*tmp.titleLabel.font.pointSize;
            //NSLog(@"Enlarge: %f",scaleFont);
            [self setButtonFontScale:scaleFont];
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethodForDays:currentTotDays];
        } completion:^(BOOL finished) {
            _chartView.hidden = NO;
            [dummyView removeFromSuperview];
            isFullScreenChart= YES;
        }];
        
        
    } else {
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        dummyView.frame = _chartView.frame;
        dummyView.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:dummyView];
        __block CGFloat scaleFont = webViewOriginalFrame.size.width/dummyView.frame.size.width;
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            
            _chartView.frame = webViewOriginalFrame;
            dummyView.frame = webViewOriginalFrame;
            UIButton *tmp = (UIButton*)[self.chartView viewWithTag:51];
            scaleFont = scaleFont*tmp.titleLabel.font.pointSize;
            [self setButtonFontScale:scaleFont];
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethodForDays:currentTotDays];
            
        } completion:^(BOOL finished) {
            [dummyView removeFromSuperview];
            isFullScreenChart= NO;
        }];
        
        
    }
}


#pragma mark - Passive Fundamentals
//Type 1 - Company Info
//Type 2 - Financial Info
- (NSString *) getFundamentalDataURL:(int)type andCategory:(NSString *)category {
    
    NSString *fundamentalLabel = [UserPrefConstants singleton].fundamentalNewsLabel;
    NSArray *codeComponents = [[UserPrefConstants singleton].userSelectingStockCode componentsSeparatedByString:@"."];
    
    NSString *trade_exchg_code = [UserPrefConstants singleton].userCurrentExchange;
    NSString *strip_stock_code = @"";
    NSString *broker_code = @"";
    
    if ([codeComponents count]>1) {
        strip_stock_code = [codeComponents objectAtIndex:0]; // 0108.KL = get 0108
    }
    
    broker_code = [UserPrefConstants singleton].brokerCode;
    
   // NSLog(@"fundamentalNewsServer %@",[UserPrefConstants singleton].fundamentalNewsServer);
    
    if (type == 1) { // Company Info
        
        if ([fundamentalLabel length]>0) {
            
            return [NSString stringWithFormat:@"%@stkCompInfom.jsp?SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundamentalNewsServer,
                    [UserPrefConstants singleton].fundamentalSourceCode,
                    trade_exchg_code,
                    strip_stock_code,
                    category,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
        }
        else
            return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundDataCompInfoURL,
                    @"CPIQ",
                    trade_exchg_code,
                    strip_stock_code,
                    category,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
    }
    else if (type == 2) { // Financial Report
        if ([fundamentalLabel length]>0) {
            
            return [NSString stringWithFormat:@"%@stkFincInfom.jsp?SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=IS&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundamentalNewsServer,
                    [UserPrefConstants singleton].fundamentalSourceCode,
                    trade_exchg_code,
                    strip_stock_code,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
        }
        else
            return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=INC&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundFinancialInfoURL,
                    @"CPIQ",
                    trade_exchg_code,
                    strip_stock_code,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
    }
    
    return @"";
}

#pragma mark - Responsive Fundamentals (added Dec 2016)

//StkCode=7054.KL&SourceCode=FCS&Sponsor=59&bhCode=086&loginID=n2nuser&prodId=TCMOBILE
// &Type=5&key=%01%05%03%00%06%01&PackageID=0&Menu=N

/*Implement 23 Dec 2016
 
 PackageID
 To open specific package.
 √ 0 = Company Info
 √ 1 = Company Key Person
 √ 2 = Shareholding Summary
 √ 3 = Announcement
 √ 4 = Financial Report
 X 5 = Estimates
 X 6 = Supply Chain
 X 7 = Market Activity
 
 
 Menu
 To control hide/show header and menu bar
 Value put as "N" to hide, default will show
 
 */




-(NSString*)getResponsiveFundamentalDataURLWithPackageID:(int)PackageID {
    
    
    
    return [NSString stringWithFormat:@"%@rw.jsp?StkCode=%@&SourceCode=%@&Sponsor=%@&bhCode=%@&loginID=%@&prodId=TCMOBILE&Type=5&key=%@&PackageID=%d&Menu=N",
            [UserPrefConstants singleton].fundamentalNewsServer,
            _stkCode,
            [UserPrefConstants singleton].fundamentalSourceCode,
            [UserPrefConstants singleton].SponsorID,
            [UserPrefConstants singleton].brokerCode,
            [UserPrefConstants singleton].userName,
            [[UserPrefConstants singleton] encryptTime],
            PackageID];

}

- (void) estimatesButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:5];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Estimates"]];
}


- (void) companySynopsisButtonPressed:(id)sender {
    [_atp timeoutActivity];

     //NSString *url = [self getFundamentalDataURL:1 andCategory:@"Synopsis"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:0];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Company Synopsis"]];
}


 - (void) companyInfoButtonPressed:(id)sender {
     
     [_atp timeoutActivity];
     
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Info"];
     NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:0];
     
    NSLog(@"companyInfoButtonPressed %@",url);
     
     [self openNews:url withTitle:[LanguageManager stringForKey:@"Company Info"]];
     
 }

- (void) companySupplyChainButtonPressed:(id)sender {
    
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Info"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:6];
    
    
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Supply Chain"]];
    
}


- (void) companyMarketActivityButtonPressed:(id)sender {
    
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Info"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:0];
    
    
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Market Activity"]];
    
}

 - (void) companyKeyPersonButtonPressed:(id)sender {
     [_atp timeoutActivity];
     
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"KeyPerson"];
     NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:1];
     [self openNews:url withTitle:[LanguageManager stringForKey:@"Company Key Persons"]];
 }


- (void) announcementButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
   // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Announcement"];
     NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:3];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Announcements"]];
    
}

- (void) financialReportButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
   // NSString *url = [self getFundamentalDataURL:2 andCategory:nil];
     NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:4];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Financial Reports"]];

}


 - (void) shareholdingSummaryButtonPressed:(id)sender {
     
     [_atp timeoutActivity];
     
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"ShareHolders"];
      NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:2];
     
     NSLog(@"shareholdingSummaryButtonPressed %@",url);
     
     [self openNews:url withTitle:[LanguageManager stringForKey:@"Shareholding Summary"]];
     
 }



 


-(void)createButtonAndAddToArray:(NSMutableArray*)array withLabelToArray:(NSMutableArray*)lblArray
                  withImageNamed:(NSString*)imageName andTag:(long)mytag andLabel:(NSString*)title andSelector:(SEL)selector {
    
    CGFloat buttonWidth = _fundamentalView.frame.size.width/12.0;
    CGFloat labelWidthFactor = 2.2;
    CGFloat labelHeight = 35;
    
    UIButton *eachButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [eachButton setFrame:CGRectMake(0, 0, buttonWidth, buttonWidth)];
    [eachButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [eachButton setTag:mytag];
    [eachButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [array addObject:eachButton];
    
    
    UILabel *eachLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, buttonWidth*labelWidthFactor, labelHeight)];
    [eachLabel setText:title];
    if ([title isEqualToString:@"Announcements"]) {
        [eachLabel setNumberOfLines:1];
        [eachLabel setAdjustsFontSizeToFitWidth:YES];
        eachLabel.minimumScaleFactor = 0.5;
    } else {
        [eachLabel setAdjustsFontSizeToFitWidth:NO];
        [eachLabel setNumberOfLines:2];
    }
    [eachLabel setTag:mytag-10];
    [eachLabel setTextAlignment:NSTextAlignmentCenter];
    UIFont *font = [UIFont systemFontOfSize:12];
    [eachLabel setFont:font];
    [eachLabel setTextColor:[UIColor whiteColor]];
    [lblArray addObject:eachLabel];

}





-(BOOL)populateFundamentals {
    
    BOOL canShow = NO;
    
   // NSLog(@"currentEd %@",currentEd);
    
    if (currentEd!=nil) {
        
        // if either one is enabled, then load it up
        if (currentEd.subsCPIQAnnounce||currentEd.subsCPIQCompInfo||currentEd.subsCPIQCompSynp
            ||currentEd.subsCPIQCompKeyPer||currentEd.subsCPIQShrHoldSum||currentEd.subsCPIQFinancialRep) {

            canShow = YES;
            
            // create buttons based on number of factset enabled
            
            NSMutableArray *btnArray = [[NSMutableArray alloc] init];
            NSMutableArray *lblArray = [[NSMutableArray alloc] init];
            
            /*
             √ 0 = Company Info
             √ 1 = Company Key Person
             √ 2 = Shareholding Summary
             √ 3 = Announcement
             √ 4 = Financial Report
             X 5 = Estimates
             X 6 = Supply Chain
             X 7 = Market Activity
             
             FCS = 8
             TRKD = 0,1,4
             
             */
            
            NSString *source = [UserPrefConstants singleton].fundamentalSourceCode;
            
            if (currentEd.subsCPIQCompSynp) {
               
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_Synopsis" andTag:51 andLabel:[LanguageManager stringForKey:@"Company Info"] andSelector:@selector(companyInfoButtonPressed:)];
                
            }
            
            if (currentEd.subsCPIQCompKeyPer) {
                
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_KeyPerson" andTag:54 andLabel:[LanguageManager stringForKey:@"Company Key Persons"] andSelector:@selector(companyKeyPersonButtonPressed:)];
                
            }
            
            if (currentEd.subsCPIQShrHoldSum) {
                if (![source isEqualToString:@"TRKD"])
                    [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_ShareHolding" andTag:55 andLabel:[LanguageManager stringForKey:@"ShareHolding Summary"] andSelector:@selector(shareholdingSummaryButtonPressed:)];
            }
            
            
            if (currentEd.subsCPIQAnnounce) {
                if (![source isEqualToString:@"TRKD"])
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_Announcement" andTag:53 andLabel:[LanguageManager stringForKey:@"Announcements"] andSelector:@selector(announcementButtonPressed:)];
                
            }
            
            if (currentEd.subsCPIQFinancialRep) {
                
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_FinReport" andTag:56 andLabel:[LanguageManager stringForKey:@"Financial Report"] andSelector:@selector(financialReportButtonPressed:)];
                
            }
            
//            if (currentEd.subsCPIQCompInfo) {
//                if (![source isEqualToString:@"TRKD"])
//                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_CompInfo" andTag:52 andLabel:@"Estimates" andSelector:@selector(estimatesButtonPressed:)];
//                
//            }
            
             //add buttons in orderly fashion
            
                long buttonsPerRow = 3;
            
            
                CGSize scrSize = _fundamentalView.frame.size;
            
                CGFloat buttonWidth = _fundamentalView.frame.size.width/12.0;
            
                CGFloat rowGapFactor = 2.2; // adjust the vertical gap between each rows
            
                CGFloat buttonHeight = buttonWidth;
            
                CGFloat mostBottomYPos = 0.0; // to resize scrollview content if buttons placed too far down
            
                CGFloat buttonWidthPerRow = buttonsPerRow * buttonWidth;
                CGFloat spacePerRow = scrSize.width - buttonWidthPerRow;
                CGFloat eachSpaces = spacePerRow / (buttonsPerRow+1);
            
                CGFloat xOffset = eachSpaces + buttonWidth/2.0;
                CGFloat yOffset = eachSpaces/2.0;
            
                long yVal = 1;
                long xVal = 0;
            
                for (long i=0; i<[btnArray count]; i++) {
                    xVal += 1;
            
                    UIButton *eachButton = [btnArray objectAtIndex:i];
                    UIButton *eachLabel = [lblArray objectAtIndex:i];
                    
                    [_fundamentalView addSubview:eachButton];
                    [_fundamentalView addSubview:eachLabel];
            
                    [eachButton setCenter: CGPointMake(xOffset, yOffset)];
                    [eachLabel setCenter:CGPointMake(xOffset,
                                                         eachButton.center.y+eachButton.frame.size.height/2.0+eachLabel.frame.size.height/2.0)];
            
            
            
                    if (i==[btnArray count]-1) {
            
                        mostBottomYPos =  eachLabel.center.y + eachButton.frame.size.height*1.2;
                        //NSLog(@"%f", mostBottomYPos);
                    }
            
            
                    xOffset += eachSpaces + buttonWidth;
            
            
                    if (xVal % buttonsPerRow==0) {
                        xOffset = eachSpaces + buttonWidth/2.0;
                        yVal += 1;
                        xVal = 0;
                        yOffset += buttonHeight*rowGapFactor;
                    }
                    
                }
            
        } else {
            // do nothing
            
        }
        
        
    }
    return canShow;

}

#pragma mark - Charts loading




-(void)setChartStatus:(long)status {
    
    NSString *errormsg = [LanguageManager stringForKey:@"Chart Not Available"];
    
    //success
    if (status==1) {
        [_chartErrorMsg setHidden:YES];
        [btnloading setHidden:YES];
        [_imgChart setHidden:NO];
    }
    
    //no url
    if (status==2) {
        [_chartErrorMsg setHidden:NO];
        [btnloading setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // error
    if (status==3) {
        [_chartErrorMsg setHidden:NO];
        [btnloading setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // loading
    if (status==4) {
        [_chartErrorMsg setHidden:YES];
        [btnloading setHidden:NO];
        [_imgChart setHidden:YES];
    }
    
    // init
    if (status==5) {
        [_chartErrorMsg setHidden:YES];
        [btnloading setHidden:YES];
        [_imgChart setHidden:YES];
        
    }
    
    // error
    if (status==6) {
        errormsg = [LanguageManager stringForKey:@"Load Chart Fail. Try again later."];
        [_chartErrorMsg setHidden:NO];
        [btnloading setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    [_chartErrorMsg setTitle:errormsg forState:UIControlStateNormal];
}


-(void)loadChartMethodForDays:(long)days {
    

    
    for(UIView *v in [self.chartView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            if (btn.tag>50) {
                [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
        }
    }
    
    switch (days) {
        case K_DAY:
            [[_chartView viewWithTag:51] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_WEEK:
            [[_chartView viewWithTag:52] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_1MONTH:
            [[_chartView viewWithTag:53] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_3MONTH:
            [[_chartView viewWithTag:54] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_6MONTH:
            [[_chartView viewWithTag:55] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_1YEAR:
            [[_chartView viewWithTag:56] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_2YEAR:
            [[_chartView viewWithTag:57] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        default:  [[_chartView viewWithTag:56] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
    }
    
    NSString *chartTypeToLoad = [UserPrefConstants singleton].chartTypeToLoad;
    if (days==K_DAY) {
         chartTypeToLoad = @"Image";
    }
    
    NSString *resolvedChartURL = [UserPrefConstants singleton].interactiveChartURL;
    
    // override for HK N2N
//    if ([_userPref.userCurrentExchange isEqualToString:@"HK"]||[_userPref.userCurrentExchange isEqualToString:@"HKD"]) {
//        
//        chartTypeToLoad = @"Modulus";
//        resolvedChartURL = @"https://poc.asiaebroker.com/mchart/index_POC.jsp?";
//    }

    
    // ========================================
    //                MODULUS
    // ========================================
    
    //resolvedChartURL = @"https://poc.asiaebroker.com/tradingview/chart.html?exchg=KL&code=3182&color=b&view=m&mode=d
    NSLog(@"chartTypeToLoad %@",chartTypeToLoad);
    if ([chartTypeToLoad isEqualToString:@"Modulus"]) {
        
        [self setChartStatus:5];
        
        NSString *url = [NSString stringWithFormat:@"%@exchg=%@&code=%@&color=b&view=f&amount=%ld",
                         resolvedChartURL,
                         [stkCodeAndExchangeArr objectAtIndex:1],[stkCodeAndExchangeArr objectAtIndex:0],currentTotDays];
        
      //  NSString *url = @"https://news-ph.asiaebroker.com/mchart/index.jsp?exchg=PH&Code=ACR&color=b&view=f&amount=100";
        
        //NSLog(@"Modulus Chart URL %@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        chartWebView.scrollView.scrollEnabled = NO;
        chartWebView.scrollView.bounces = NO;
        [chartWebView loadRequest:request];
        
        [_imgChart setHidden:YES];
        [chartWebView setHidden:NO];
    }
    

    // ========================================
    //                IMAGE
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Image"]) {
        
        [self setChartStatus:4];
        
        NSString *chartURL = [UserPrefConstants singleton].historicalChartURL;
        
        if (currentTotDays==1) {
            chartURL = [UserPrefConstants singleton].intradayChartURL;
        } else {
            chartURL = [UserPrefConstants singleton].historicalChartURL;
        }
        
        NSString *urlString = [NSString stringWithFormat:@"%@&w=%.0f&h=%.0f&k=%@&c=b&d=%ld",chartURL,
                               _imgChart.frame.size.width, _imgChart.frame.size.height, _stkCode,days];
        
        NSLog(@"%@",urlString);
        ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
        [imgChartManager getImageChartWithURL:urlString
                            completedWithData:^(NSData *data) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIImage *chartImg = [UIImage imageWithData:data];
                                    _imgChart.image = chartImg;
                                    [self setChartStatus:1];
                                });
                            }
                                      failure:^(NSString *errorMsg) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self setChartStatus:3];
                                          });
                                      }];
        
        [_imgChart setHidden:NO];
        [chartWebView setHidden:YES];
    }

    
    // ========================================
    //                TELETRADER
    // ========================================

    if ([chartTypeToLoad isEqualToString:@"TeleTrader"]) {
        
        [self setChartStatus:5];
        
        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
        NSString *url = [NSString stringWithFormat:@"%@code=%@&Name=%@&exchg=%@&mode=d&color=b&lang=en&key=%@&amount=%ld",
                         resolvedChartURL,
                         [splitStkCode objectAtIndex:0],
                         [splitStkCode objectAtIndex:1],
                         [UserPrefConstants singleton].userCurrentExchange,
                         [[UserPrefConstants singleton] encryptTime],
                         days];
        //NSLog(@"%@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        chartWebView.scrollView.scrollEnabled = NO;
        chartWebView.scrollView.bounces = NO;
        [chartWebView loadRequest:request];
        
        [_imgChart setHidden:YES];
        [chartWebView setHidden:NO];
    }
    
}


#pragma mark - Chart Features




-(IBAction)loadChart:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    
    switch (btn.tag) {
        case 51:
            currentTotDays = K_DAY;
            break;
        case 52:
            currentTotDays = K_WEEK;
           // [self loadChartMethodForDays:5];//[self openChartOption:2];
            break;
        case 53:
            currentTotDays = K_1MONTH;
           // [self loadChartMethodForDays:20];//[self openChartOption:3];
            break;
        case 54:
            currentTotDays = K_3MONTH;
           // [self loadChartMethodForDays:60];//[self openChartOption:4];
            break;
        case 55:
            currentTotDays = K_6MONTH;
            //[self loadChartMethodForDays:120];//[self openChartOption:5];
            break;
        case 56:
            currentTotDays = K_1YEAR;
           // [self loadChartMethodForDays:240];//[self openChartOption:6];
            break;
        case 57:
            currentTotDays = K_2YEAR;
           // [self loadChartMethodForDays:480];//[self openChartOption:7];
            break;
            
        default:currentTotDays = K_1YEAR;//[self loadChartMethodForDays:200];//[self openChartOption:5];
            break;
    }
    [self loadChartMethodForDays:currentTotDays];//[self openChartOption:1];
}



// NSLog(@"chartURL %@",url);

// NSString *url = [NSString stringWithFormat:@"http://chartwc.asiaebroker.com/chart/stockchart/chartTCPlus.jsp?code=%@&exchg=%@&amount=200&color=b",[stkCodeAndExchangeArr objectAtIndex:0],[stkCodeAndExchangeArr objectAtIndex:1]];

//-(void)openChartOption:(long)option {
//    
//    NSString *urlFormat = @"";
//    
//    for(UIView *v in [self.chartView subviews])
//    {
//        if([v isKindOfClass:[UIButton class]])
//        {
//            UIButton *btn = (UIButton*)v;
//            if (btn.tag>50) {
//                [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
//            }
//        }
//    }
//    
//    
//    switch (option) {
//        case 1:{
//            urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=1";
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:51];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        }break;
//        case 2:{
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:52];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=5";
//        }break;
//        case 3:{
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:53];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=20";
//        }break;
//        case 4:{
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:54];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=60";
//        }break;
//        case 5:{
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:55];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=120";
//        }break;
//        case 6:{
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:56];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=240";
//        }break;
//        case 7:{
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:57];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=480";
//        }break;
//            
//        default:{ urlFormat = @"%@exchg=%@&code=%@&color=b&view=f&amount=240";
//            UIButton *btn = (UIButton*)[_chartView viewWithTag:55];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        }break;
//    }
//    
//    NSString *url = [NSString stringWithFormat:urlFormat,[UserPrefConstants singleton].interactiveChartURL,[stkCodeAndExchangeArr objectAtIndex:1],[stkCodeAndExchangeArr objectAtIndex:0]];
//    
//
//    NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
//    ////NSLog(@"url = %@",url);
//    webView.scrollView.scrollEnabled = NO;
//    webView.scrollView.bounces = NO;
//    [webView loadRequest:request];
//    
//}


#pragma mark - Others




-(void)setColorForLabel:(UILabel*)label withPrice:(CGFloat)value {
   
    if (value>0.0001) {
        label.textColor = [UIColor greenColor];
    } else if (value<-0.0001) {
        label.textColor = [UIColor redColor];
    }  else {
        label.textColor = [UIColor whiteColor];
    }
}


- (void)populateStockDetails
{
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    // NSLog(@"PopulateStockDetails [_qcData qcFeedDataDict] %@",[[_qcData qcFeedDataDict] allKeys]);
    
    NSString *stkNameStr = @"";
    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
        stkNameStr = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_130_S_SYSBOL_2];
    } else {
        stkNameStr = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_38_S_STOCK_NAME];
    }
     
    _lblStockName.text = stkNameStr;
    _lblStockCode.text = [[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_33_S_STOCK_CODE];
    
    long long totalVol = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]longLongValue];
    long long totalBuyVol     =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]longLongValue];
    long long totalSellVol    =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]longLongValue];
    long long totalBuyTran    =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]longLongValue];
    long long totalSellTran   =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]longLongValue];
    long long totalMarCap     =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]longLongValue];
    long long totalShareIssued=[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]longLongValue];
    
    CGFloat lacpFloat = 0;
    
   if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
        lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_153_D_THEORETICAL_PRICE]floatValue];
    }else{
         lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
    }
  
    
    _lblLacp.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lacpFloat]];

    CGFloat lastDoneFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
    _lblLastDn.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lastDoneFloat]];
    [self setColorForLabel:_lblLastDn withPrice:(lastDoneFloat-lacpFloat)];

    
    CGFloat floatPercent = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue];
    CGFloat floatChange = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE] floatValue];
    
    if (floatPercent>0) {
        _lblChangerPer.textColor = [UIColor greenColor];
        _lblChange.textColor = [UIColor greenColor];
        _lblChangerPer.text = [NSString stringWithFormat:@"%.3f%%",floatPercent]; // cqa request no + sign
        _lblChange.text = [NSString stringWithFormat:@"%.3f",floatChange]; // cqa request no + sign
    } else if (floatPercent<0) {
        _lblChangerPer.textColor = [UIColor redColor];
        _lblChange.textColor = [UIColor redColor];
        _lblChangerPer.text = [NSString stringWithFormat:@"%.3f%%",floatPercent];
        _lblChange.text = [NSString stringWithFormat:@"%.3f",floatChange];
    } else {
        _lblChangerPer.textColor = [UIColor whiteColor];
        _lblChange.textColor = [UIColor whiteColor];
        _lblChangerPer.text = [NSString stringWithFormat:@"%.3f%%",floatPercent];
        _lblChange.text = [NSString stringWithFormat:@"%.3f",floatChange];
    }
    
    CGFloat floatOpen = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE] floatValue];
    _lblOpen.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatOpen]];
    [self setColorForLabel:_lblOpen withPrice:(floatOpen-lacpFloat)];
    
    CGFloat floatClose = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE] floatValue];
    _lblClose.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatClose]];
    [self setColorForLabel:_lblClose withPrice:(floatClose-lacpFloat)];
    
    CGFloat floatHigh =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE] floatValue];
    _lblHigh.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatHigh]];
    [self setColorForLabel:_lblHigh withPrice:(floatHigh-lacpFloat)];
    
    CGFloat floatLow =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE] floatValue];
    _lblLow.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatLow]];
    [self setColorForLabel:_lblLow withPrice:(floatLow-lacpFloat)];
    
    CGFloat floatCeil = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue];
    _lblCeil.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatCeil]];
    [self setColorForLabel:_lblCeil withPrice:(floatCeil-lacpFloat)];
    
    CGFloat floatFloor = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue];
    _lblFloor.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatFloor]];
    [self setColorForLabel:_lblFloor withPrice:(floatFloor-lacpFloat)];
    
    CGFloat floatBid = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
    _lblBid.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatBid]];
    [self setColorForLabel:_lblBid withPrice:(floatBid-lacpFloat)];
    
    CGFloat floatAsk = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue];
    _lblAsk.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatAsk]];
    [self setColorForLabel:_lblAsk withPrice:(floatAsk-lacpFloat)];
    
    long long valueTradedVal =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]longLongValue];
    valueTradedLabel.text = [[UserPrefConstants singleton]abbreviateNumber:valueTradedVal];
    
    long long tradesVal =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]longLongValue];
    _lblTrades.text = [[UserPrefConstants singleton]abbreviateNumber:tradesVal];
    
     long long bidQtyValue  =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
    _lblBidQty.text = [[UserPrefConstants singleton]abbreviateNumber:bidQtyValue];
   
    long long askQtyValue  =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]longLongValue];
    _lblAskQty.text = [[UserPrefConstants singleton]abbreviateNumber:askQtyValue];
    
    // _lblChangerPer.text = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER] stringValue];
    NSString *stockSectorName = [[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
    NSString *stockTradingBoard =[[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:3];
    NSString *stockTotalSellVolume =[NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]];
    NSString *ISINString = [NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_157_S_ISIN]];
    float stockParString =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE] floatValue];
    
    
    NSString *lotSizeString = [NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]];
    lotSizeStringInt = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT] intValue];
    // NSLog(@"Lot Size String %d",lotSizeStringInt);
    unichar uc = (unichar)[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_48_S_STATUS] characterAtIndex:1]; //Just extend to 16 bits
    
    
    long long shortSellVolumeString  =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]longLongValue];
    lblTotalShortSellVol.text = [[UserPrefConstants singleton]abbreviateNumber:shortSellVolumeString];
    
    NSString *currencyPlus =[NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_134_S_CURRENCY]];
    lblCurrency.text = currencyPlus;
    ////NSLog(@"stockTotalSellVolume %@ = %@",stockTotalSellVolume,[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]);
    
   // NSLog(@"ISINString %@", ISINString);
    
     CGFloat dur = [stockSectorName length]/10.0;
    
    if (sectorLabel==nil)
    sectorLabel=[[MarqueeLabel alloc]initWithFrame:stockLabel.frame duration:dur andFadeLength:5.0f];
    [sectorLabel setScrollDuration:dur];
    [_detail0View addSubview:sectorLabel];
    [stockLabel setHidden:YES];
    [sectorLabel setTextColor:[UIColor whiteColor]];
    
    if ([stockSectorName length]<=0) stockSectorName = @"-";
    sectorLabel.text = stockSectorName;
    tradingBoardLabel.text = stockTradingBoard;
    lotSizeLabel.text = lotSizeString;
    
    
    if (([stockTotalSellVolume length]<=0)||[stockTotalSellVolume containsString:@"null"]) stockTotalSellVolume = @"-";
    ssellVolumeLabel.text = stockTotalSellVolume;
    
    
    if (([ISINString length]<=0)||[ISINString containsString:@"null"]) ISINString = @"-";
    isinLabel.text = ISINString;
    parLabel.text =  [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:stockParString]];
    
    activeLabel.text = [[UserPrefConstants singleton] getBoardStatus:[NSString stringWithCharacters:&uc length:1]];


    
    if (totalMarCap<=0) {
        
        totalMarCap = totalShareIssued * lastDoneFloat;
        
        if (lastDoneFloat<=0) {
            
            totalMarCap = totalShareIssued * lacpFloat;
        }
    }


    
    NSString *lblVolumeString = [[UserPrefConstants singleton] abbreviateNumber:totalVol];
    NSString *lblBuyString = [[UserPrefConstants singleton] abbreviateNumber:totalBuyVol];
    NSString *lblSellString = [[UserPrefConstants singleton] abbreviateNumber:totalSellVol];
    _lblVolume.text    = lblVolumeString; //[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalVol]];

   
    lblTotalBuyVolume.text  =lblBuyString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalBuyVol]];
    lblTotalSellVolume.text = lblSellString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalSellVol]];
    lblTotalBuyTrans.text   =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalBuyTran]];
    lblTotalSellTrans.text  =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalSellTran]];
    lblShareIssued.text     =[[UserPrefConstants singleton]abbreviateNumber:totalShareIssued];

    lblMarketCap.text       =[[UserPrefConstants singleton]abbreviateNumber:totalMarCap];
    stkCodeAndExchangeArr = [ _userPref.userSelectingStockCode componentsSeparatedByString:@"."];
    
    if ([[stkCodeAndExchangeArr objectAtIndex:1] isEqualToString:@"MY"]) {
        [lblShareIssued setHidden:YES];
        [lblMarketCap setHidden:YES];
        [marketCapLabel setHidden:YES];
        [shareIssueLabel setHidden:YES];
    }
    
    
    
    
    
    if (isUpdate) {
        
         // ------------------------------- PRICES --------------------------------
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue])
        {
            _lblLastDn.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue])
            _lblLastDn.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblLastDn.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue])
        {
            _lblChange.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue])
            _lblChange.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblChange.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue])
        {
            _lblChangerPer.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue])
            _lblChangerPer.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblChangerPer.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue])
        {
            _lblAsk.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue])
            _lblAsk.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblAsk.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue])
        {
            _lblBid.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue])
            _lblBid.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblBid.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue])
        {
            _lblLow.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue])
            _lblLow.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblLow.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue])
        {
            _lblHigh.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue])
            _lblHigh.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblHigh.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue])
        {
            _lblClose.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue])
            _lblClose.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblClose.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue])
        {
            _lblOpen.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue])
            _lblOpen.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblOpen.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue])
        {
            ssellVolumeLabel.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue])
            ssellVolumeLabel.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.ssellVolumeLabel.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue])
        {
            parLabel.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue])
            parLabel.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.parLabel.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue])
        {
            valueTradedLabel.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue])
            valueTradedLabel.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.valueTradedLabel.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue])
        {
            lotSizeLabel.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue])
            lotSizeLabel.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lotSizeLabel.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue])
        {
            _lblLacp.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue])
            _lblLacp.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblLacp.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue])
        {
            _lblCeil.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue])
            _lblCeil.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblCeil.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue])
        {
            _lblFloor.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue])
            _lblFloor.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblFloor.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        // ------------------------------- QUANTITIES --------------------------------
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue])
        {
            _lblTrades.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue])
            _lblTrades.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblTrades.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue])
        {
            _lblAskQty.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue])
            _lblAskQty.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblAskQty.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue])
        {
            _lblBidQty.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue])
            _lblBidQty.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _lblBidQty.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue])
        {
            _lblVolume.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue])
            _lblVolume.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblVolume.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];

        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue])
        {
            lblTotalBuyVolume.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue])
            lblTotalBuyVolume.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblTotalBuyVolume.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue])
        {
            lblTotalSellVolume.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue])
            lblTotalSellVolume.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblTotalSellVolume.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];

        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue])
        {
            lblTotalBuyTrans.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue])
            lblTotalBuyTrans.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblTotalBuyTrans.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue])
        {
            lblTotalSellTrans.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue])
            lblTotalSellTrans.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblTotalSellTrans.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue])
        {
            lblShareIssued.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue])
            lblShareIssued.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblShareIssued.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];

        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue])
        {
            lblMarketCap.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue])
            lblMarketCap.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            self.lblMarketCap.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
    }
    isUpdate= false;
    tempDictionary = [[NSMutableDictionary alloc]initWithDictionary:[_qcData qcFeedDataDict] copyItems:YES];

    
}

- (void)populateWebView
{
    
    
  // [self openChartOption:6];

    [self loadChartMethodForDays:currentTotDays];
    
}

//- (void)populateMarketDepth
//{
//    //Can do Text and background color here.
//   // //NSLog(@"Market Depth %@",marketDepthArr);
//    priceFormatter = [NSNumberFormatter new];
//    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    [priceFormatter setMinimumFractionDigits:3];
//    [priceFormatter setMaximumFractionDigits:3];
//    [priceFormatter setGroupingSeparator:@","];
//    [priceFormatter setGroupingSize:3];
//    
//    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
//    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    [quantityFormatter setGroupingSeparator:@","];
//    [quantityFormatter setGroupingSize:3];
//    [quantityFormatter setGeneratesDecimalNumbers:YES];
//    
//    //Implementing data to labels
//    ////NSLog(@"Market Depth Count %lu",(unsigned long)marketDepthArr.count);
//    ////NSLog(@"Market Depth %@",marketDepthArr);
//    if(marketDepthArr.count==0){
//        // NSLog(@"Empty Market Depth");
//        return;
//    } else {
//        
//        mktDepthData = YES;
//        
//    }
//    
//    if ([[arrayWithTwoStrings objectAtIndex:1]isEqualToString:@"KL"]) {
//        
//        lblBuySplit1.text   = [[[marketDepthArr objectAtIndex:0]  objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
//        lblBuySplit2.text   = marketDepthArr.count> 2 ? [[[marketDepthArr objectAtIndex:2] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue] : @"";
//        lblBuySplit3.text   = marketDepthArr.count> 4 ?[[[marketDepthArr objectAtIndex:4] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue] : @"";
//        lblBuySplit4.text   =  marketDepthArr.count> 6 ? [[[marketDepthArr objectAtIndex:6] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue] : @"";
//        lblBuySplit5.text   = marketDepthArr.count > 8 ? [[[marketDepthArr objectAtIndex:8] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue] : @"";
//        
//        lblBidQuantity1.text = [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:0] objectForKey:FID_58_I_BUY_QTY_1] intValue]]];
//        lblBidQuantity2.text = marketDepthArr.count> 2 ? [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:2] objectForKey:FID_58_I_BUY_QTY_1] intValue]]] : @"";
//        lblBidQuantity3.text = marketDepthArr.count> 4 ? [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:4] objectForKey:FID_58_I_BUY_QTY_1] intValue]]] : @"";
//        lblBidQuantity4.text = marketDepthArr.count> 6 ? [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:6] objectForKey:FID_58_I_BUY_QTY_1] intValue]]] : @"";
//        lblBidQuantity5.text = marketDepthArr.count> 8 ? [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:8] objectForKey:FID_58_I_BUY_QTY_1] intValue]]] : @"";
//        
//        lblBidPrice1.text =  [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:0] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]];
//        lblBidPrice2.text =  marketDepthArr.count> 2 ?[priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:2] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        lblBidPrice3.text =  marketDepthArr.count> 4 ?[priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:4] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        lblBidPrice4.text =  marketDepthArr.count> 6 ? [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:6] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        lblBidPrice5.text =  marketDepthArr.count> 8 ? [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:8] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        
//        lblAskPrice1.text = marketDepthArr.count> 1 ? [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:1] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        lblAskPrice2.text = marketDepthArr.count> 3 ? [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:3] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        lblAskPrice3.text = marketDepthArr.count>5 ? [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:5] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        lblAskPrice4.text = marketDepthArr.count> 7? [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:7] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]] : @"";
//        lblAskPrice5.text = marketDepthArr.count> 9 ? [priceFormatter stringFromNumber:[NSNumber numberWithFloat: [[[marketDepthArr objectAtIndex:9] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]]: @"";
//        
//        lblAskQuantity1.text = marketDepthArr.count> 1 ?[quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:1] objectForKey:FID_58_I_BUY_QTY_1] intValue]]] : @"";
//        lblAskQuantity2.text = marketDepthArr.count> 3 ? [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:3] objectForKey:FID_58_I_BUY_QTY_1] intValue]]]: @"";
//        lblAskQuantity3.text = marketDepthArr.count> 5 ?[quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:5] objectForKey:FID_58_I_BUY_QTY_1] intValue]]]: @"";
//        lblAskQuantity4.text = marketDepthArr.count> 7 ? [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:7] objectForKey:FID_58_I_BUY_QTY_1] intValue]]]: @"";
//        lblAskQuantity5.text = marketDepthArr.count> 9 ? [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:9] objectForKey:FID_58_I_BUY_QTY_1] intValue]]]: @"";
//        
//        lblSellSplit1.text = marketDepthArr.count> 1 ? [[[marketDepthArr objectAtIndex:1] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue] :@"";
//        lblSellSplit2.text = marketDepthArr.count> 3 ? [[[marketDepthArr objectAtIndex:3] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue] :@"";
//        lblSellSplit3.text = marketDepthArr.count> 5 ?[[[marketDepthArr objectAtIndex:5] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue] :@"";
//        lblSellSplit4.text = marketDepthArr.count> 7 ?[[[marketDepthArr objectAtIndex:7] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue]:@"";
//        lblSellSplit5.text = marketDepthArr.count> 9 ?[[[marketDepthArr objectAtIndex:9] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue]:@"";
//        if (ismarketDepthupd && tempArr.count>0) {
//            ////NSLog(@" marketDepth is %@",marketDepthArr);
//            ////NSLog(@" tempArr is %@",tempArr);
//            if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_68_D_BUY_PRICE_1] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_68_D_BUY_PRICE_1] floatValue])
//                lblBidPrice1.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//            else if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_68_D_BUY_PRICE_1] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_68_D_BUY_PRICE_1] floatValue])
//                lblBidPrice1.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//            // __weak StockDetailViewController *weakSelf = self;
//            [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                self.lblBidPrice1.layer.backgroundColor=[UIColor clearColor].CGColor;
//            } completion:nil];
//            
//            if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_58_I_BUY_QTY_1] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_58_I_BUY_QTY_1] integerValue])
//                lblBidQuantity1.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//            else if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_58_I_BUY_QTY_1] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_58_I_BUY_QTY_1] integerValue])
//                lblBidQuantity1.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//            [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                self.lblBidQuantity1.layer.backgroundColor=[UIColor clearColor].CGColor;
//            } completion:nil];
//            if (tempArr.count>1) {
//                if ([[[marketDepthArr objectAtIndex:1]objectForKey:FID_68_D_BUY_PRICE_1] floatValue]> [[[tempArr objectAtIndex:1] objectForKey:FID_68_D_BUY_PRICE_1] floatValue])
//                    lblAskPrice1.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//                else if ([[[marketDepthArr objectAtIndex:1]objectForKey:FID_68_D_BUY_PRICE_1] floatValue]> [[[tempArr objectAtIndex:1] objectForKey:FID_68_D_BUY_PRICE_1] floatValue])
//                    lblAskPrice1.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//                [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                    self.lblAskPrice1.layer.backgroundColor=[UIColor clearColor].CGColor;
//                } completion:nil];
//                
//                if ([[[marketDepthArr objectAtIndex:1]objectForKey:FID_58_I_BUY_QTY_1] integerValue]> [[[tempArr objectAtIndex:1] objectForKey:FID_58_I_BUY_QTY_1] integerValue])
//                    lblAskQuantity1.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//                else if ([[[marketDepthArr objectAtIndex:1]objectForKey:FID_58_I_BUY_QTY_1] integerValue]> [[[tempArr objectAtIndex:1] objectForKey:FID_58_I_BUY_QTY_1] integerValue])
//                    lblAskQuantity1.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//                [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                    self.lblAskQuantity1.layer.backgroundColor=[UIColor clearColor].CGColor;
//                } completion:nil];
//            }
//          
//        }
//    }else{
//         /*lblBuySplit1.text   = [[[marketDepthArr objectAtIndex:0]  objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
//         lblBidQuantity1.text = [quantityFormatter stringFromNumber:[NSNumber numberWithInt:[[[marketDepthArr objectAtIndex:0] objectForKey:FID_58_I_BUY_QTY_1] intValue]]];
//        lblBidPrice1.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[marketDepthArr objectAtIndex:0] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]];*/
//        
//        lblBuySplit1.text   = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
//         lblBuySplit2.text   = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"171"] stringValue];
//         lblBuySplit3.text   = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"172"] stringValue];
//         lblBuySplit4.text   = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"173"] stringValue];
//         lblBuySplit5.text   = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"174"] stringValue];
//        lblBidQuantity1.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_58_I_BUY_QTY_1] longValue]]];
//          lblBidQuantity2.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"59"] longValue]]];
//          lblBidQuantity3.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"60"] longValue]]];
//          lblBidQuantity4.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"61"] longValue]]];
//          lblBidQuantity5.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"62"] longValue]]];
//        lblBidPrice1.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_68_D_BUY_PRICE_1] floatValue]]];
//         lblBidPrice2.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"69"] floatValue]]];
//         lblBidPrice3.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"70"] floatValue]]];
//         lblBidPrice4.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"71"] floatValue]]];
//         lblBidPrice5.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"72"] floatValue]]];
//        lblAskPrice1.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]]];
//           lblAskPrice2.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"89"] floatValue]]];
//                   lblAskPrice3.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"90"] floatValue]]];
//                   lblAskPrice4.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"91"] floatValue]]];
//                   lblAskPrice5.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"92"] floatValue]]];
//        lblAskQuantity1.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"78"] longValue]]];
//        lblAskQuantity2.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"79"] longValue]]];
//        lblAskQuantity3.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"80"] longValue]]];
//        lblAskQuantity4.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"81"] longValue]]];
//        lblAskQuantity5.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"82"] longValue]]];
//        lblSellSplit1.text = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"180"] stringValue];
//                lblSellSplit2.text = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"181"] stringValue];
//                lblSellSplit3.text = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"182"] stringValue];
//                lblSellSplit4.text = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"183"] stringValue];
//                lblSellSplit5.text = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"184"] stringValue];
//        
//         if (ismarketDepthupd && tempArr.count>0) {
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_68_D_BUY_PRICE_1] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_68_D_BUY_PRICE_1] floatValue])
//                 lblBidPrice1.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_68_D_BUY_PRICE_1] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_68_D_BUY_PRICE_1] floatValue])
//                 lblBidPrice1.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             // __weak StockDetailViewController *weakSelf = self;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidPrice1.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"69"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"69"] floatValue])
//                 lblBidPrice2.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"69"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"69"] floatValue])
//                 lblBidPrice2.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             // __weak StockDetailViewController *weakSelf = self;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidPrice2.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"70"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"70"] floatValue])
//                 lblBidPrice3.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"70"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"70"] floatValue])
//                 lblBidPrice3.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             // __weak StockDetailViewController *weakSelf = self;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidPrice3.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"71"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"71"] floatValue])
//                 lblBidPrice4.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"71"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"71"] floatValue])
//                 lblBidPrice4.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             // __weak StockDetailViewController *weakSelf = self;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidPrice4.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"72"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"72"] floatValue])
//                 lblBidPrice5.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"72"] floatValue]> [[[tempArr objectAtIndex:0] objectForKey:@"72"] floatValue])
//                 lblBidPrice5.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             // __weak StockDetailViewController *weakSelf = self;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidPrice5.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_58_I_BUY_QTY_1] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_58_I_BUY_QTY_1] integerValue])
//                 lblBidQuantity1.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:FID_58_I_BUY_QTY_1] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:FID_58_I_BUY_QTY_1] integerValue])
//                 lblBidQuantity1.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidQuantity1.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"59"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"59"] integerValue])
//                 lblBidQuantity2.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"59"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"59"] integerValue])
//                 lblBidQuantity2.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidQuantity2.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"60"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"60"] integerValue])
//                 lblBidQuantity3.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"60"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"60"] integerValue])
//                 lblBidQuantity3.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidQuantity3.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"61"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"61"] integerValue])
//                 lblBidQuantity4.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"61"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"61"] integerValue])
//                 lblBidQuantity4.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidQuantity4.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//             if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"62"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"62"] integerValue])
//                 lblBidQuantity5.layer.backgroundColor=[UIColor colorWithRed:0 green:0.718 blue:0 alpha:1].CGColor;
//             else if ([[[marketDepthArr objectAtIndex:0]objectForKey:@"62"] integerValue]> [[[tempArr objectAtIndex:0] objectForKey:@"62"] integerValue])
//                 lblBidQuantity5.layer.backgroundColor=[UIColor colorWithRed:0.718 green:0 blue:0 alpha:1].CGColor;
//             [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
//                 self.lblBidQuantity5.layer.backgroundColor=[UIColor clearColor].CGColor;
//             } completion:nil];
//             
//         }
//    }
//    tempArr=[NSMutableArray arrayWithArray:marketDepthArr];
//    ismarketDepthupd =false;
//    
//    
//    [_btnBuy setHidden:NO];
//    [_btnSell setHidden:NO];
//    
//}

-(void)refreshBizDone {
    
    if (canRefreshBizDone) {
        canRefreshBizDone = NO;
        [self performSelector:@selector(populateBusinessDone) withObject:nil afterDelay:1.0];
    }
}


-(void)populateBusinessDone{
    
    [_vcm vertxGetBusinessDoneByStockCode:_stkCode exchg:[UserPrefConstants singleton].userCurrentExchange];

}


-(void)refreshTimeSales {
    
    if (canRefreshTimeSales) {
        canRefreshTimeSales = NO;
        [self performSelector:@selector(populateTimeAndSales) withObject:nil afterDelay:1.0];
    }
}

- (void)populateTimeAndSales
{
    NSString *tranNo;
    tranNo = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode ]objectForKey:FID_103_I_TRNS_NO];
    NSLog(@"populateTimeAndSales %@",tranNo);
    
    [_vcm vertxGetTimeAndSalesByStockCode:_stkCode begin:@"0" end:tranNo exchange:[UserPrefConstants singleton].userCurrentExchange];    
}

- (void)populateCompanyNews
{
    isLoadingNews = YES;
    [companyNewsTableView reloadData];
    
    if ([UserPrefConstants singleton].isElasticNews) {
        [_atp getElasticNewsForStocks:_stkCode forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
    }else
        
    if ([UserPrefConstants singleton].isArchiveNews) {
        //NSLog(@"archive");
        [_atp getArchiveNewsForStock:_stkCode forDays:30];
        
    } else {
        [_vcm vertxCompanyNews:_stkCode];
        
    }
}

- (void)getMarketDepth
{
    arrayWithTwoStrings = [_stkCode componentsSeparatedByString:@"."];
    
    if (_stkCode !=nil) {
        if (!mktDepthData) {
            
          //  [_mktDepthActivity startAnimating];
        }
            
        //NSLog(@"stkCode %@ coll %@",_stkCode, [arrayWithTwoStrings objectAtIndex:1]);
        [_vcm vertxGetMarketDepth:_stkCode andCollarr:[arrayWithTwoStrings objectAtIndex:1]];

    }

}


/* Example Archive news return:
 {
 dt = "2016-10-27 04:01:30.0";
 id = "N_urn_newsml_reuters.com_20161027_nL4N1CX1UR";
 stk = ".N";
 t = "UPDATE 1-Baseball-Cubs level World Series 1-1 against Indians";
 },
 
 Example Vertex news return:
 {
 0 = KL;
 1 = "0165.KL";
 10 = "";
 11 = "XOX.KL";
 2 = N201610253000182;
 3 = "NEW ISSUE OF SECURITIES (CHAPTER 6 OF LISTING REQUIREMENTS)";
 4 = "2016-10-25 07:00:01";
 5 = "2016-10-27 07:00:04";
 6 = N201610253000182;
 7 = 30;
 8 = BURSA;
 9 = "";
 }
 
 */

#pragma mark - ATP Notification Received

-(void) addWatchListItemsReply:(NSNotification *)notification {

    NSDictionary *dict = notification.userInfo;
    NSArray *replyArr = [dict objectForKey:@"replyArr"];
    
    
    if (replyArr) {
        
        NSString *someDuplicate = [dict objectForKey:@"DuplicateStocks"];
        
        NSLog(@"someDuplicate %@", someDuplicate);
        
        if ([someDuplicate length]>0) {
            
            [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":someDuplicate}]
                                         inController:self withTitle:[LanguageManager stringForKey:Duplicate_Stock]];
        } else {
            
            [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]
                                         inController:self withTitle:[LanguageManager stringForKey:Duplicate_Stock]];
            
        }
        
    } else  {
        // NoData, Error, TimedOut
        NSString *replyStr = [dict objectForKey:@"replyDict"];
        
        if ([replyStr isEqualToString:@"NoData"]) {
            
        } else if ([replyStr isEqualToString:@"Error"]) {
            
        } else if ([replyStr isEqualToString:@"TimedOut"]) {
            
        } else if ([replyStr isEqualToString:@"AllDuplicate"]) {
            
            NSString *duplicates = [dict objectForKey:@"DuplicateStocks"];
            
            [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:StockAlreadyAddedError withPlaceholders:@{@"%WatchlistName%":duplicates}]
                                         inController:self withTitle:Duplicate_Stock];
        }
    }
    
}


-(void)didFinishGetArchiveNews:(NSNotification*)notification {
    
    isLoadingNews = NO;
    
    NSArray *newsArray = [notification.userInfo objectForKey:@"News"];
    // NSLog(@"NEWS ARCHIVE: %@",newsArray);
    
    if ([newsArray count]>0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            archiveNewsTableDataArr = [NSArray arrayWithArray:newsArray];
            
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"dt" ascending: NO];
            archiveNewsTableDataArr =   [archiveNewsTableDataArr sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
            
            [companyNewsTableView reloadData];
        });
    } else {
        //news not available
        dispatch_async(dispatch_get_main_queue(), ^{
             [companyNewsTableView reloadData];
        });
    }
    
}

-(void)didFinishGetElasticNews:(NSNotification*)notification {
    
    
    
}


#pragma mark - Vertex Notification Received




- (void)didUpdate:(NSNotification *)notification
{
    NSString * response = [notification object];
    
    if([_stkCode isEqual:response])
    {
        isUpdate = true;
        [self populateStockDetails];
        // only update Non MDL mkt depth if it is a non MDL type
        
        NSLog(@"currentEd.MDLfeature %d",currentEd.MDLfeature);
        
         if (currentEd.MDLfeature==0) [self updateNonMDLMktDepth];
    }
}


//Retrieval Selector
- (void)didFinishedGetStockNews:(NSNotification *)notification
{
    
    isLoadingNews = NO;
    
    NSDictionary * response = [notification.userInfo copy];
    //  NSLog(@"NEWS NORMAL: %@",response);
    
    NSArray *data =[response objectForKey:@"data"];
    //    NSArray *reversed = [[[data sortedArrayUsingSelector:@selector(compare:)] reverseObjectEnumerator] allObjects];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        companyNewsTableDataArr = [data copy];
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"4" ascending: NO];
        companyNewsTableDataArr =   [companyNewsTableDataArr sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
        [companyNewsTableView reloadData];
    });
    
}



-(void)updateMktDepthAndFlash {
    
    
    // compare prevMDArr and marketDepthArr value
    
    // can compare only if both arrays has the same amount
    
    if (mktDepthZero) return;
    
    [self updateFooterData];
    
    if ([MDBuyArr count]==[MDBuyPrevArr count]) {
        
        // then flash as needed
        for (long i=0; i<[MDBuyArr count]; i++) {
            
            NSDictionary *buyData1 = [MDBuyArr objectAtIndex:i];
            NSDictionary *buyData2 = [MDBuyPrevArr objectAtIndex:i];
            
            // only reloadcell if it is visible
            if ([_mdTable.indexPathsForVisibleRows containsObject:[NSIndexPath indexPathForRow:i inSection:0]]) {
            
                if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
            
            }
        }
        
    }
    
    if ([MDSellArr count]==[MDSellPrevArr count]) {
        
        // then flash as needed
        for (long i=0; i<[MDSellArr count]; i++) {
            
            NSDictionary *sellData1 = [MDSellArr objectAtIndex:i];
            NSDictionary *sellData2 = [MDSellPrevArr objectAtIndex:i];
            
            // only reloadcell if it is visible
            if ([_mdTable.indexPathsForVisibleRows containsObject:[NSIndexPath indexPathForRow:i inSection:0]]) {
                
                if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                    [_mdTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }
                
            }
            
        }
        
    }

   
    MDBuyPrevArr = [NSMutableArray arrayWithArray:MDBuyArr];
    MDSellPrevArr = [NSMutableArray arrayWithArray:MDSellArr];
    
}


/* Mkt Depth data example MDL TYPE
 {
 105 = 2;
 106 = 2;
 107 = 0;
 118 = 1479;
 131 = KL;
 170 = 193;
 33 = "7036WC.KL";
 45 = U12;
 58 = 34893700;
 68 = "0.095";
 }
 */
// add updated data to array

-(void)updateNonMDLMktDepth {
    
    NSLog(@"updateNonMDLMktDepth");
    
    NSDictionary *stkData = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] copy];
    
    // check if fields exist
    int fid_buy_split_start = 170;
    int fid_sell_split_start = 180;
    int fid_buy_qty_start = 58;
    int fid_sell_qty_start = 78;
    int fid_buy_price_start = 68;
    int fid_sell_price_start = 88;
    
    // replace only within existing array size
    for (int i=0; i<[MDBuyArr count]; i++)
    {
        NSMutableDictionary *buyMD = [[NSMutableDictionary alloc] init];
        
        NSString *buySplitKey = [NSString stringWithFormat:@"%d",fid_buy_split_start+i];
        if ([[stkData allKeys] containsObject:buySplitKey]) {
            [buyMD setObject:[stkData objectForKey:buySplitKey] forKey:FID_170_I_BUY_SPLIT_1];
        }
        
        NSString *buyPriceKey = [NSString stringWithFormat:@"%d",fid_buy_price_start+i];
        if ([[stkData allKeys] containsObject:buyPriceKey]) {
            [buyMD setObject:[stkData objectForKey:buyPriceKey] forKey:FID_68_D_BUY_PRICE_1];
        }
        
        NSString *buyQtyKey = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
        if ([[stkData allKeys] containsObject:buyQtyKey]) {
            [buyMD setObject:[stkData objectForKey:buyQtyKey] forKey:FID_58_I_BUY_QTY_1];
        }
        
        // set FID 107
        [buyMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
        // set FID 106 1= Buy/Bid
        [buyMD setObject:[NSNumber numberWithInt:1] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
        
        [MDBuyArr replaceObjectAtIndex:i withObject:buyMD];
    }
    
    for (int i=0; i<[MDSellArr count]; i++)
    {
        NSMutableDictionary *sellMD = [[NSMutableDictionary alloc] init];
        
        NSString *sellSplitKey = [NSString stringWithFormat:@"%d",fid_sell_split_start+i];
        if ([[stkData allKeys] containsObject:sellSplitKey]) {
            [sellMD setObject:[stkData objectForKey:sellSplitKey] forKey:FID_170_I_BUY_SPLIT_1];
        }
        
        NSString *sellPriceKey = [NSString stringWithFormat:@"%d",fid_sell_price_start+i];
        if ([[stkData allKeys] containsObject:sellPriceKey]) {
            [sellMD setObject:[stkData objectForKey:sellPriceKey] forKey:FID_68_D_BUY_PRICE_1];
        }
        
        NSString *sellQtyKey = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
        if ([[stkData allKeys] containsObject:sellQtyKey]) {
            [sellMD setObject:[stkData objectForKey:sellQtyKey] forKey:FID_58_I_BUY_QTY_1];
        }
        
        // set FID 107
        [sellMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
        // set FID 106 2= Sell/Ask
        [sellMD setObject:[NSNumber numberWithInt:2] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
        
         [MDSellArr replaceObjectAtIndex:i withObject:sellMD];
    }
    
}


// MDL subscription update
-(void)mdDidUpdate:(NSNotification*)notification {
    
    ismarketDepthupd =true;
    
    NSDictionary * response = [notification.userInfo copy];
    
    NSLog(@"mdDidUpdate %d",currentEd.MDLfeature);
    if (currentEd.MDLfeature==0) {
        // non MDL
        // handled by populateNonMDLMktDepth method
        
    } else if (currentEd.MDLfeature==1) {
        // MDL
        long bidOrAsk = [[response objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] longValue];
        
        if (bidOrAsk==1) { // bid
            
            // get which level to update (position = index)
            long indexToReload = [[response objectForKey:FID_107_I_MARKET_DEPTH_POSITION] longValue];
            
            if ((indexToReload>=0)&&(indexToReload<[MDBuyArr count])) {
                
                
                // update latest data
                [MDBuyArr replaceObjectAtIndex:indexToReload withObject:response];
            }
            
        } else if (bidOrAsk==2) { // ask
            
            // get which level to update (position = index)
            long indexToReload = [[response objectForKey:FID_107_I_MARKET_DEPTH_POSITION] longValue];
            
            if ((indexToReload>=0)&&(indexToReload<[MDSellArr count])) {
                
                // update latest data
                [MDSellArr replaceObjectAtIndex:indexToReload withObject:response];
            }
        }
    }
    
   
}


-(void)didFinishMarketDepth:(NSNotification *)notification
{
    
    NSDictionary * response = [notification.userInfo copy];
    mktDepthArr = [response objectForKey:@"marketdepthresults"];
    
    mktDepthZero = NO;
    
    
    // NSLog(@"mktDepthArr %@",mktDepthArr);
    
    NSMutableArray *tmpBuyArr = [[NSMutableArray alloc] init];
    NSMutableArray *tmpSellArr = [[NSMutableArray alloc] init];

    
    // Non Feature md data is all in single dictionary, so we convert it to same
    // format as featured MD.
    
   // if (currentEd.MDLfeature==0) { // OLD WAY TO CHECK MDL
    
    int MDLfeature = ([mdInfo.isMDL isEqualToString:@"Y"])?1:0;
    
     NSLog(@"MDLfeature %d",MDLfeature);
    
    if (MDLfeature==0) { // NEW WAY TO CHECK MDL
        
    // non MDL - all mkt depth data is in one object (max 10 level only)
        
        NSLog(@"mktDepthArr count %lu",(unsigned long)[mktDepthArr count]);
        
        // if server give any data
        if ([mktDepthArr count]>0) {
        
            NSDictionary *stkDataWithMDDict = [mktDepthArr objectAtIndex:0];
            
            
            int fid_buy_split_start = 170;
            int fid_sell_split_start = 180;
            int fid_buy_qty_start = 58;
            int fid_sell_qty_start = 78;
            int fid_buy_price_start = 68;
            int fid_sell_price_start = 88;
            
            
            // populate tmpBuyArr (loop 10 and if found, create it and add)
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *buyMD = [[NSMutableDictionary alloc] init];
                
                NSString *buySplitKey = [NSString stringWithFormat:@"%d",fid_buy_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buySplitKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buySplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *buyPriceKey = [NSString stringWithFormat:@"%d",fid_buy_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyPriceKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *buyQtyKey = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyQtyKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[buyMD allKeys] count]>0) {
                    // set FID 107
                    [buyMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106 1= Buy/Bid
                    [buyMD setObject:[NSNumber numberWithInt:1] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    
                    [tmpBuyArr addObject:buyMD];
                }
            }
            
            // populate tmpSellArr
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *sellMD = [[NSMutableDictionary alloc] init];
                
                NSString *sellSplitKey = [NSString stringWithFormat:@"%d",fid_sell_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellSplitKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellSplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *sellPriceKey = [NSString stringWithFormat:@"%d",fid_sell_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellPriceKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *sellQtyKey = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellQtyKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[sellMD allKeys] count]>0) {
                    // set FID 107
                    [sellMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106 2= Sell/Ask
                    [sellMD setObject:[NSNumber numberWithInt:2] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    [tmpSellArr addObject:sellMD];
                }
            }
        
        } else {
            ; // empty data = do nothing
        }
        
        
   // } else if (currentEd.MDLfeature==1) { OLD WAY TO CHECK MDL
        
        
    } else if (MDLfeature==1) { // NEW WAY TO CHECK MDL
        
        NSLog(@"mktDepthArr count %lu",(unsigned long)[mktDepthArr count]);
        
        // MDL type - mkt depth data in separate objects
        
        // if server give any data
        if ([mktDepthArr count]>0) {
        
            for (int i=0; i<[mktDepthArr count]; i++) {
                
                NSDictionary *eachMDdata = [mktDepthArr objectAtIndex:i];
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==1) {
                    // BID
                    [tmpBuyArr addObject:eachMDdata];
                }
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==2) {
                    // SELL
                    [tmpSellArr addObject:eachMDdata];
                }
            }
            
        } else {
            ; // no data from server. do nothing
        }

    }
    

    
    
    // if tmpBuyArr empty, prepare zeroes based on resolved mdLevel for this exchange
    
    if ([tmpBuyArr count]<=0) {
        // int mdLevel = [currentEd.mktDepthLevel intValue]; // OLD WAY TO GET MDLevel
        int mdLevel = [mdInfo.mdLevel intValue]; // NEW WAY TO GET MDLevel
        
        mktDepthZero = YES;
        
        // NSLog(@"mdLevel %d",mdLevel);
        
        int alternate = 1;
        int pos = 0;
        for (long i=0; i<mdLevel*2; i++) {
            NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:2],@"105",
                                      [NSNumber numberWithInt:alternate],@"106",
                                      [NSNumber numberWithInt:pos],@"107",
                                      [NSNumber numberWithInt:0],@"118",
                                      @"KL",@"131",
                                      [NSNumber numberWithInt:0],@"170",
                                      @"5517.KL",@"33",
                                      @"0",@"45",
                                      [NSNumber numberWithInt:0],@"58",
                                      [NSNumber numberWithInt:0],@"68",
                                      nil];
            
            if (alternate==1) {
                [tmpBuyArr addObject:zeroDict];
                alternate = 2;
                pos++;
            } else {
                [tmpSellArr addObject:zeroDict];
                alternate = 1;
            }
        }
    }
    
    
    // sort data based on FID 107
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_107_I_MARKET_DEPTH_POSITION ascending:YES];
    NSArray *limitedArrayB = [tmpBuyArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSArray *limitedArrayS = [tmpSellArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [tmpBuyArr removeAllObjects]; [tmpSellArr removeAllObjects];
    
    if ([limitedArrayB count]>0) {
        
        [_mdFooter setHidden:NO];
        MDBuyArr = [NSMutableArray arrayWithArray:[limitedArrayB mutableCopy]];
        MDSellArr = [NSMutableArray arrayWithArray:[limitedArrayS mutableCopy]];
        
        
        // ensure both MDBuyArr and MDSellArr has same amount of data.
        
        if ([MDBuyArr count]>[MDSellArr count]) {
            for (int i=0; i<[MDBuyArr count]; i++) {
                if ([MDSellArr count]<=i) {
                        NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [NSNumber numberWithInt:2],@"105",
                                              [NSNumber numberWithInt:2],@"106", // 2 is sell, 1 is buy
                                              [NSNumber numberWithInt:i],@"107",
                                              [NSNumber numberWithInt:0],@"118",
                                              @"KL",@"131",
                                              [NSNumber numberWithInt:0],@"170",
                                              @"5517.KL",@"33",
                                              @"0",@"45",
                                              [NSNumber numberWithInt:0],@"58",
                                              [NSNumber numberWithInt:0],@"68",
                                              nil];
                    [MDSellArr addObject:zeroDict];
                }
            }
        }
        
        if ([MDSellArr count]>[MDBuyArr count]) {
            for (int i=0; i<[MDSellArr count]; i++) {
                if ([MDBuyArr count]<=i) {
                    NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [NSNumber numberWithInt:2],@"105",
                                              [NSNumber numberWithInt:2],@"106", // 2 is sell, 1 is buy
                                              [NSNumber numberWithInt:i],@"107",
                                              [NSNumber numberWithInt:0],@"118",
                                              @"KL",@"131",
                                              [NSNumber numberWithInt:0],@"170",
                                              @"5517.KL",@"33",
                                              @"0",@"45",
                                              [NSNumber numberWithInt:0],@"58",
                                              [NSNumber numberWithInt:0],@"68",
                                              nil];
                    [MDBuyArr addObject:zeroDict];
                }
            }
            
        }
        
        
        [_mdTable reloadData];
        [self updateFooterData];
        
        // then send subscribe data (subscribe only if there is mktdepth)
        [_vcm vertxSubscribeMktDepthLevel:MDLfeature withStockCode:_stkCode];
        
        // assign timertick handler
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"halfSecondTimerTick" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMktDepthAndFlash)
                                                     name:@"halfSecondTimerTick" object:nil];
    }
    
    // If other VC (stkprev,orderbk) calls to buy/sell/revise/cancel, then open up
    // traderVC here (trader needs Market Depth data)
    
    int tmp = [UserPrefConstants singleton].callBuySellReviseCancel;
    if (tmp!=0) {
        [UserPrefConstants singleton].callBuySellReviseCancel = 0;
        
        UIButton *reviseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        reviseBtn.tag = 12;
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.tag = 13;
        
        if (tmp==1) [self executeTrade:_btnBuy];
        if (tmp==2) [self executeTrade:_btnSell];
        if (tmp==3) [self executeTrade:reviseBtn];
        if (tmp==4) [self executeTrade:cancelBtn];
    }
}

-(void)didFinishedBusinessDone:(NSNotification*)notification {
    
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    
    
   // NSLog(@"BIZDONE: %@", data);
    
    if ([data count]>4)
    bizDoneTableDataArr = [NSArray arrayWithArray:[data subarrayWithRange:NSMakeRange(0, [data count]-4)]];
    
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    
    canRefreshBizDone = YES;
    NSDateFormatter *dateFormatterscoreboard = [[NSDateFormatter alloc] init];
    [dateFormatterscoreboard setDateFormat:@"HH:mm:ss"];
    NSDate *currentTime = [NSDate date];
    _lastUpdatedStr = [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];
    [_bizDoneTable reloadData];
}

- (void)didFinishedTimeAndSales:(NSNotification *)notification
{

    NSLog(@">>>>> [didFinishedTimeAndSales]");
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];
    _timeAndSalesTableDataArr = [NSArray arrayWithArray:data];
        
//        NSLog(@"_timeAndSalesTableDataArr %@", _timeAndSalesTableDataArr);
    
    if ([self.refreshControl isRefreshing]) {
        
        [self.refreshControl endRefreshing];
    }
    
    canRefreshTimeSales = YES;
        
    NSDateFormatter *dateFormatterscoreboard = [[NSDateFormatter alloc] init];
    [dateFormatterscoreboard setDateFormat:@"HH:mm:ss"];
    NSDate *currentTime = [NSDate date];
    _lastUpdatedStr = [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];
    [timeAndSalesTableView reloadData];
    
    // Perform get T&S
    [self performSelector:@selector(populateTimeAndSales) withObject:nil afterDelay:10];

}


/*
 
 for (MarketDepth *md in mdlist) {
 totalBuy += (double)(md.buyQty * md.buyPrice);
 totalSell += (double)(md.sellQty * md.sellPrice);
 
 if (md.buyPrice == -999001 || md.buyPrice == 999001 || md.buyPrice == -999002 || md.buyPrice == 999002) {
 bid_MP_MO = YES;
 }
 if (md.sellPrice == -999001 || md.sellPrice == 999001 || md.sellPrice == -999002 || md.sellPrice == 999002) {
 ask_MP_MO = YES;
 }
	}
 */

-(void)updateFooterData {
    
    
    
    long long totalBuy = 0;
    long long totalSell = 0;
    
    long totBuySplit = 0; long long totBuyQty = 0;  CGFloat aveBuyBid = 0;
    long totSellSplit = 0; long long totSellQty = 0; CGFloat aveSellAsk = 0;

    
    for (long i=0; i<[MDBuyArr count]; i++)  {
        NSDictionary *buyData = [MDBuyArr objectAtIndex:i];
        
        long eachBuySplit = [[buyData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
        totBuySplit += eachBuySplit;
        
        long long eachBuyQty = [[buyData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
        totBuyQty += eachBuyQty;
        
        CGFloat eachBuyBid = [[buyData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
        aveBuyBid += eachBuyBid*eachBuyQty;
       
        totalBuy += eachBuyQty*eachBuyBid;
        
    }
    
    
    for (long i=0; i<[MDSellArr count]; i++)  {
        
        NSDictionary *sellData = [MDSellArr objectAtIndex:i];
        
        long eachSellSplit = [[sellData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
        totSellSplit += eachSellSplit;
        
        long long eachSellQty = [[sellData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
        totSellQty += eachSellQty;
        
        CGFloat eachSellAsk = [[sellData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
        aveSellAsk += eachSellAsk*eachSellQty;
        
        totalSell += eachSellQty*eachSellAsk;
        
    }
    
    priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    if (totBuyQty<=0) aveBuyBid=0; else aveBuyBid = aveBuyBid/totBuyQty;
    if (totSellQty<=0) aveSellAsk=0; else aveSellAsk = aveSellAsk/totSellQty;

    
    // check to blink totals
    [self checkForBlink:_mdFootTotBuySplit compareQty:totBuySplit with:p_totBuySplit];
    [self checkForBlink:_mdFootTotBuyQty compareQty:totBuyQty with:p_totBuyQty];
    [self checkForBlink:_mdFootAveBuyBid comparePrice:aveBuyBid with:p_aveBuyBid];
    
    [self checkForBlink:_mdFootTotSellSplit compareQty:totSellSplit with:p_totSellSplit];
    [self checkForBlink:_mdFootTotSellQty compareQty:totSellQty with:p_totSellQty];
    [self checkForBlink:_mdFootAveSellAsk comparePrice:aveSellAsk with:p_aveSellAsk];
    
    _mdFootTotBuySplit.text = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:totBuySplit]];
    _mdFootTotBuyQty.text = [[UserPrefConstants singleton] abbreviateNumber:totBuyQty];
    _mdFootAveBuyBid.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveBuyBid]];
    
    _mdFootTotSellSplit.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totSellSplit]];
    _mdFootTotSellQty.text =  [[UserPrefConstants singleton] abbreviateNumber:totSellQty];
    _mdFootAveSellAsk.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveSellAsk]];
    
    // footer coloring
    double lacp = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
    if (aveBuyBid>lacp) {
        _mdFootAveBuyBid.textColor = [UIColor greenColor];
    } else if (aveBuyBid<lacp) {
        _mdFootAveBuyBid.textColor = [UIColor redColor];
    } else {
        _mdFootAveBuyBid.textColor = [UIColor whiteColor];
    }
    
    if (aveSellAsk>lacp) {
        _mdFootAveSellAsk.textColor = [UIColor greenColor];
    } else if (aveSellAsk<lacp) {
        _mdFootAveSellAsk.textColor = [UIColor redColor];
    } else {
        _mdFootAveSellAsk.textColor = [UIColor whiteColor];
    }
    
    // save prev data
    p_totBuySplit = totBuySplit;
    p_totBuyQty = totBuyQty;
    p_aveBuyBid = aveBuyBid;
    
    p_totSellSplit = totSellSplit;
    p_totSellQty = totSellQty;
    p_aveSellAsk = aveSellAsk;
    
    
    [self checkForBlink:_mdTotalBidLabel compareQty:totalBuy with:p_totalBuy];
    [self checkForBlink:_mdTotalAskLabel compareQty:totalSell with:p_totalSell];
    
    _mdTotalBidLabel.text = [LanguageManager stringForKey:@"Total Bid: %@"
                                       withPlaceholders:@{@"%totBid%":[quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totalBuy]]}];
    _mdTotalAskLabel.text = [LanguageManager stringForKey:@"Total Ask: %@"
                                       withPlaceholders:@{@"%totAsk%":[quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totalSell]]}];
    
    
    p_totalBuy = totalBuy;
    p_totalSell = totalSell;
    
}

-(void)checkForBlink:(UILabel*)label compareQty:(long long)current with:(long long)previous {
    if (current>previous) {
        label.layer.backgroundColor=[kGrnFlash CGColor];
    } else if (current<previous) {
        label.layer.backgroundColor=[kRedFlash CGColor];
    }
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        label.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
}


-(void)checkForBlink:(UILabel*)label comparePrice:(CGFloat)current with:(CGFloat)previous {

    if (current>previous) {
        label.layer.backgroundColor=[kGrnFlash CGColor];
    } else if (current<previous) {
        label.layer.backgroundColor=[kRedFlash CGColor];
    }
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        label.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
}

#pragma mark -
#pragma mark TableView Delegate
//====================================================================
//                        TableView Delegate
//====================================================================
/* Tag
 1 = Time and Sales
 2 = Company News
 3 = Business Done
 4 = Mkt Depth
 */

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView==timeAndSalesTableView)
    {
        return 20;
    }
    
    if (tableView.tag==2) {
        return 0;
    }
    
    if (tableView==_bizDoneTable) {
        return 20;
    }
    
    if (tableView.tag==4) {
        return 0;
        
    }
    
    return 0;
    
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView==timeAndSalesTableView)
    {
        CGRect headerFrame =  CGRectMake(0, 0, tableView.frame.size.width, 20);
        _lastUpdatedHeader = [[UILabel alloc] initWithFrame:headerFrame];
        [_lastUpdatedHeader setBackgroundColor:[UIColor blackColor]];
        [_lastUpdatedHeader setTextColor:[UIColor whiteColor]];
        [_lastUpdatedHeader setFont:[UIFont systemFontOfSize:10]];
        [_lastUpdatedHeader setTextAlignment:NSTextAlignmentCenter];
        [_lastUpdatedHeader setText:_lastUpdatedStr];
        return _lastUpdatedHeader;
    }
    
    if (tableView.tag==2) {
         return nil;
    }
    
    if (tableView==_bizDoneTable) {
        CGRect headerFrame =  CGRectMake(0, 0, tableView.frame.size.width, 20);
        _lastUpdatedHeader = [[UILabel alloc] initWithFrame:headerFrame];
        [_lastUpdatedHeader setBackgroundColor:[UIColor blackColor]];
        [_lastUpdatedHeader setTextColor:[UIColor whiteColor]];
        [_lastUpdatedHeader setFont:[UIFont systemFontOfSize:10]];
        [_lastUpdatedHeader setTextAlignment:NSTextAlignmentCenter];
        [_lastUpdatedHeader setText:_lastUpdatedStr];
        return _lastUpdatedHeader;
    }
    
    if (tableView.tag==4) {
        return nil;
       
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    if(tableView.tag ==1) //Time and sales
    {
        if (_timeAndSalesTableDataArr.count>0)
        {
            UIView *bgView = [[UIView alloc] initWithFrame:timeAndSalesTableView.frame];
            bgView.backgroundColor = [UIColor clearColor];
            timeAndSalesTableView.backgroundView = bgView;
            timeAndSalesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
        }
        else
        {
            // Display a message when the table is empty
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            
            messageLabel.text = [LanguageManager stringForKey:@"No Results."];
            messageLabel.textColor = [UIColor whiteColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            [messageLabel sizeToFit];
            
            timeAndSalesTableView.backgroundView = messageLabel;
            timeAndSalesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
    }
    
    else if(tableView.tag == 2) //Company News
    {
        if ([companyNewsTableDataArr count]>0)
        {
            UIView *bgView = [[UIView alloc] initWithFrame:companyNewsTableView.frame];
            bgView.backgroundColor = [UIColor clearColor];
            companyNewsTableView.backgroundView = bgView;
            companyNewsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
        } else if ([archiveNewsTableDataArr count]>0)
        {
            UIView *bgView = [[UIView alloc] initWithFrame:companyNewsTableView.frame];
            bgView.backgroundColor = [UIColor clearColor];
            companyNewsTableView.backgroundView = bgView;
            companyNewsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
        }
        else
        {
            if (isLoadingNews) {
                UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                [loading startAnimating];
                companyNewsTableView.backgroundView = loading;
                companyNewsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            } else {
                // Display a message when the table is empty
                UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
                messageLabel.text = [LanguageManager stringForKey:@"No Available News Data"];
                messageLabel.textColor = [UIColor whiteColor];
                messageLabel.numberOfLines = 0;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                [messageLabel sizeToFit];
                companyNewsTableView.backgroundView = messageLabel;
                companyNewsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            }
        }
    }
    
    else if(tableView.tag == 5) {
        
            return 1;
        
        // MKT DEPTH
    } else if(tableView.tag == 4) {
        
            UIView *bgView = [[UIView alloc] initWithFrame:_mdTable.frame];
            bgView.backgroundColor = [UIColor clearColor];
            
            _mdTable.backgroundView = bgView;
            return 1;
        
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag ==1)
        return _timeAndSalesTableDataArr.count;
    else if(tableView.tag==2) {
        if ([UserPrefConstants singleton].isArchiveNews)
            return archiveNewsTableDataArr.count;
        else
            return companyNewsTableDataArr.count;
    } else if (tableView.tag ==5)
        return bizDoneTableDataArr.count;
    else if (tableView.tag==4) {
        if ([MDBuyArr count]>[MDSellArr count])
            return [MDBuyArr count];
        else
            return [MDSellArr count];
    }
        return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView==companyNewsTableView)
        return 50;
    else
        return 33;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // MARK: TIME AND SALES CELLS
    if (tableView.tag==1)
    {
        static NSString *CellIdentifier = @"CellTimeAndSales";
        TimeAndSalesTableViewCell *cell = (TimeAndSalesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        if(!cell)
            cell = [[TimeAndSalesTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellTimeAndSales"];
        
        //Inverting Order of table data
        NSUInteger row = _timeAndSalesTableDataArr.count - 1 - indexPath.row;
        
        if (row==0) {
            [cell setHidden:YES];
        }
        //Get Close price and incoming Price
        float closePrice;
        float incomingPrice;
        closePrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue];
        incomingPrice =[[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"2"]floatValue];
        if (incomingPrice > closePrice)
            cell.lblPrice.textColor = [UIColor greenColor];
        else if (incomingPrice < closePrice)
            cell.lblPrice.textColor = [UIColor redColor];
        else if (incomingPrice == closePrice)
            cell.lblPrice.textColor = [UIColor whiteColor];
        
        // follow TCPlus b=red, s=green
        if ([[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"1"] isEqualToString:@"b"])
        {
            cell.lblTradeAt.textColor = [UIColor greenColor];
        }
        else if ([[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"1"] isEqualToString:@"s"])
        {
            cell.lblTradeAt.textColor = [UIColor redColor];
        } else
        {
            cell.lblTradeAt.textColor = [UIColor whiteColor];
        }
        
        //Convert Incoming StrDate to proper one.
        ////NSLog(@"Time: : %@",[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"0"]);
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"HHmmss"];
        NSDate *currentDate = [dateFormater dateFromString:[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"0"]];
        ////NSLog(@"currentDate : %@",currentDate);
        
        [dateFormater setDateFormat:@"HH:mm:ss"];
        NSString *convertedDateString = [dateFormater stringFromDate:currentDate];
        ////NSLog(@"convertedDateString : %@",convertedDateString);
        
        NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
        [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [quantityFormatter setGroupingSeparator:@","];
        [quantityFormatter setGroupingSize:3];
        [quantityFormatter setGeneratesDecimalNumbers:NO];
        
        cell.lblTime.text       = convertedDateString;
        cell.lblTradeAt.text    = [[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"1"];
        cell.lblPrice.text      =         [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"2"] floatValue]]];

        NSString *tempVol       = [[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"3"];
       // cell.lblVolume.text     = [quantityFormatterTimeAndSales stringFromNumber:[NSNumber numberWithInt:[tempVol intValue]]];
        cell.lblVolume.text = [[UserPrefConstants singleton] abbreviateNumber:[tempVol intValue]];
    
       // cell.lblVolume.text     = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"3"] longValue]]];
        
        return cell;

    }
   
   
    // MARK: BIZ DONE CELLS
    
    else if (tableView.tag ==5) {
        static NSString *CellIdentifier = @"BizDoneCellID";
        BDoneTableViewCell *cell = (BDoneTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil) {
            cell = [[BDoneTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        CGFloat lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
        [cell updateCellWithDictionary:[bizDoneTableDataArr objectAtIndex:indexPath.row] withLACP:lacpFloat];
        
        return cell;
    }
    // MARK: NEWS CELLS
    else if(tableView.tag ==2) // NEWS
    {
        static NSString *CellIdentifier = @"Cell2";
        NewsTableViewCell *cell = (NewsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[NewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.detailTextLabel.textColor = [UIColor grayColor];
        }
        int row = (int)indexPath.row;
        
        if ([UserPrefConstants singleton].isArchiveNews) {
            NSDictionary *item = [archiveNewsTableDataArr objectAtIndex:row];
            
            if (item) {
                
                cell.newsDate.text = [NSString stringWithFormat:@"%@", [[UserPrefConstants singleton]  getLogicalDate:[item objectForKey:@"dt"]]];
                cell.newsTitle.text =  [[UserPrefConstants singleton] stringByStrippingHTML:[item objectForKey:@"t"]];
                
            }
            
        } else {
           // NSLog(@"not archive");
            
            NSDictionary *item = [companyNewsTableDataArr objectAtIndex:row];
            if (item) {
                cell.newsDate.text = [NSString stringWithFormat:@"%@",[[UserPrefConstants singleton] getLogicalDate:[item objectForKey:@"4"]]];
                cell.newsTitle.text = [item objectForKey:@"3"];
                
               
            }
            
        }
        return cell;
        
    }
    // MARK: MARKET DEPTH CELLS
    else if(tableView.tag ==4) //
    {
        static NSString *CellIdentifier = @"CellMD";
        MDTableViewCell *cell = (MDTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[MDTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            
        }
        long row = indexPath.row;
        
        priceFormatter = [NSNumberFormatter new];
        [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        [priceFormatter setGroupingSeparator:@","];
        [priceFormatter setGroupingSize:3];
        
        cell.delegate = self;
       
        cell.buyBid.textColor = [UIColor whiteColor];
        cell.sellAsk.textColor= [UIColor whiteColor];;
        
        
        double lacp = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
        
        NSDictionary *sellData1 = [NSDictionary new];
        NSDictionary *buyData1 = [NSDictionary new];
        
        if ([MDBuyArr count]>0) {
            
            buyData1 = [MDBuyArr objectAtIndex:row];
            cell.buySplit.text = [[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
            cell.buyQty.text = [[UserPrefConstants singleton] abbreviateNumber:[[buyData1 objectForKey:FID_58_I_BUY_QTY_1] integerValue]];
            cell.buyBid.text = [priceFormatter stringFromNumber:[buyData1 objectForKey:FID_68_D_BUY_PRICE_1]];
            
            
            double buyPrice = [[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue];
            if (buyPrice>lacp) {
                cell.buyBid.textColor = [UIColor greenColor];
            } else
                if (buyPrice<lacp) {
                    cell.buyBid.textColor = [UIColor redColor];
                } else {
                    cell.buyBid.textColor = [UIColor whiteColor];
                }

        }
        
        if ([MDSellArr count]>0) {
            
            sellData1 = [MDSellArr objectAtIndex:row];
            
            cell.sellSplit.text = [[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
            cell.sellQty.text = [[UserPrefConstants singleton] abbreviateNumber:[[sellData1 objectForKey:FID_58_I_BUY_QTY_1] integerValue]];
            cell.sellAsk.text = [priceFormatter stringFromNumber:[sellData1 objectForKey:FID_68_D_BUY_PRICE_1]];
            
            double sellPrice = [[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue];
            if (sellPrice>lacp) {
                cell.sellAsk.textColor = [UIColor greenColor];
            } else
                if (sellPrice<lacp) {
                    cell.sellAsk.textColor = [UIColor redColor];
                } else {
                    cell.sellAsk.textColor = [UIColor whiteColor];
                }
        }
        
        
        
        cell.mdIndex.text = [NSString stringWithFormat:@"%ld", row+1];
        
        
       
        

        
        // set text color
        /*
         // ----- Set Label Buy Price TextColor -----
         if (md.buyPrice == sd.price_ref || md.buyPrice == -999001 || md.buyPrice == 999001 || md.buyPrice == -999002 || md.buyPrice == 999002 || md.buyPrice == 0) {
         label_bidPrice.textColor = [UIColor darkGrayColor];
         }
         else if (md.buyPrice > sd.price_ref) {
         label_bidPrice.textColor = [UIColor colorWithRed:(6.0/255.0) green:(220.0/255.0) blue:(167.0/255.0)alpha:1];
         }
         else if (md.buyPrice < sd.price_ref) {
         label_bidPrice.textColor = [UIColor colorWithRed:(255.0/255.0) green:(23.0/255.0) blue:(68.0/255.0)alpha:1];
         }
         */
        
        
   
        
      
        
        
        if (mktDepthZero) {
            cell.buyBid.textColor = [UIColor whiteColor];
            cell.sellAsk.textColor = [UIColor whiteColor];
        }
        
        // update cell
        
        if ([MDBuyPrevArr count]>0) {
    
            NSDictionary *buyData2 = [MDBuyPrevArr objectAtIndex:row];
            
            
            struct CGColor *flashRed = [kRedFlash CGColor];
            struct CGColor *flashGreen = [kGrnFlash CGColor];
            struct CGColor *clearClr = [[UIColor clearColor] CGColor];
            CGFloat animDur = 1.0;
            
            
            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [cell.buySplit.layer setBackgroundColor:flashGreen];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.buySplit.layer setBackgroundColor:clearClr]; }];
            }
            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [cell.buySplit.layer setBackgroundColor:flashRed];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.buySplit.layer setBackgroundColor:clearClr]; }];
            }
            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [cell.buyQty.layer setBackgroundColor:flashGreen];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyQty.layer setBackgroundColor:clearClr]; }];
            }
            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [cell.buyQty.layer setBackgroundColor:flashRed];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyQty.layer setBackgroundColor:clearClr]; }];
            }
            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [cell.buyBid.layer setBackgroundColor:flashGreen];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyBid.layer setBackgroundColor:clearClr]; }];
            }
            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [cell.buyBid.layer setBackgroundColor:flashRed];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyBid.layer setBackgroundColor:clearClr]; }];
            }
            
        }
        
        
        if ([MDSellPrevArr count]>0) {
            NSDictionary *sellData2 = [MDSellPrevArr objectAtIndex:row];
            
            struct CGColor *flashRed = [kRedFlash CGColor];
            struct CGColor *flashGreen = [kGrnFlash CGColor];
            struct CGColor *clearClr = [[UIColor clearColor] CGColor];
            CGFloat animDur = 1.0;
            
            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [cell.sellSplit.layer setBackgroundColor:flashGreen];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellSplit.layer setBackgroundColor:clearClr]; }];
            }
            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [cell.sellSplit.layer setBackgroundColor:flashRed];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellSplit.layer setBackgroundColor:clearClr]; }];
            }
            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [cell.sellQty.layer setBackgroundColor:flashGreen];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellQty.layer setBackgroundColor:clearClr]; }];
            }
            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [cell.sellQty.layer setBackgroundColor:flashRed];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellQty.layer setBackgroundColor:clearClr]; }];
            }
            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [cell.sellAsk.layer setBackgroundColor:flashGreen];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellAsk.layer setBackgroundColor:clearClr]; }];
            }
            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [cell.sellAsk.layer setBackgroundColor:flashRed];
                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellAsk.layer setBackgroundColor:clearClr]; }];
            }
            
        }
        
        return cell;
        
    }
    else
        return 0;

}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_atp timeoutActivity];

    // MARK: NEWS SELECTION
    if (tableView.tag==2) { // NEWS
    
        ////NSLog(@"Clicked News: %@",indexPath);
        int row = (int)indexPath.row;
        
        if ([UserPrefConstants singleton].isArchiveNews) {
            NSDictionary *item = [archiveNewsTableDataArr objectAtIndex:row];
            NSString *url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,[item objectForKey:@"id"]];
            //      NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            //      [webView loadRequest:request];
            // NSLog(@"ArcNews URL %@", url);
            [self openNews:url withTitle:[LanguageManager stringForKey:@"Stock News"]];
        } else {
        
            NSDictionary *item = [companyNewsTableDataArr objectAtIndex:row];
            NSString *url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,[item objectForKey:@"6"]];
            //      NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            //      [webView loadRequest:request];
            [self openNews:url withTitle:[LanguageManager stringForKey:@"Stock News"]];
        }
        
    }
    


}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (scrollView.tag==3) {
        CGFloat pageWidth = scrollViewStockDetail.frame.size.width;
        int page = floor((scrollViewStockDetail.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControlStockDetail.currentPage = page;
    }
    
    if ((scrollView.tag==1)||(scrollView.tag==5)) {
        if (scrollView.contentOffset.y < -60 && ![self.refreshControl isRefreshing]) {
            [self.refreshControl beginRefreshing];
            if (![_timeSalesView isHidden]) {
                [self refreshTimeSales];
            }
            if (![_bizDoneView isHidden]) {
                [self refreshBizDone];
            }
        }
    }
   
   
}


#pragma mark - MDCellDegate
-(void)mdPriceDidTapped:(int)mdGroup atCell:(UITableViewCell *)cell {
    
    NSIndexPath *indexPath = [_mdTable indexPathForCell:cell];
    NSString *price = @"0";
    if (mdGroup==0) {
        NSDictionary *buyData1 = [MDBuyArr objectAtIndex:indexPath.row];
        price = [[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] stringValue];
    } else {
        
        NSDictionary *sellData1 = [MDSellArr objectAtIndex:indexPath.row];
        price = [[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] stringValue];
    }
    
    
    if (price!=nil) {
    NSDictionary *dict = @{@"Price": price};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"mdPriceTappedNotification" object:nil userInfo:dict];
    }
}


#pragma mark -
#pragma mark WebView Delegate
//====================================================================
//                        WebView Delegate
//====================================================================

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    [btnloading startAnimating];
 
    
    //To avoid loading all the google ads.
    if([request.URL.absoluteString rangeOfString:@"googleads"].location != NSNotFound)
        return NO;
    
    
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Can't Load Charts at this Moment.Please Check Back Later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [alert show];
    [self setChartStatus:6];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//    NSString *htmlData =[webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
//    ////NSLog(@"htmlData : %@",htmlData);
//
//    if ([htmlData containsString:@"Time out, please try again."])
//    {
//        [self populateWebView];
//    } else
//    {
//        ////NSLog(@"Loaded Successfully");
//    }
    [btnloading stopAnimating];
    
    
    NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) urlResponse.response;
    NSInteger statusCode = httpResponse.statusCode;
    if (statusCode > 399) {
        //NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response":httpResponse}];
        // Forward the error to webView:didFailLoadWithError: or other
        //NSLog(@"%@", error);
        [_chartErrorMsg setHidden:NO];
        [_imgChart setHidden:YES];
        [chartWebView setHidden:YES];
        [_chartErrorMsg setHidden:NO];
    }
    else {
        // No HTTP error
        [_chartErrorMsg setHidden:YES];
        [_imgChart setHidden:YES];
        [chartWebView setHidden:NO];
        [_chartErrorMsg setHidden:YES];
    }
    
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
}

#pragma mark - Trader Delegate

-(void)traderDidCancelled {
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
      //  [_traderContainer setAlpha:0.0];
    } completion:^(BOOL finished) {
      //  [_traderContainer setHidden:YES];
        [self enableTradeButtons:YES];
    }];
}

-(void)traderDidCancelledNormal {
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [_traderContainer setAlpha:0.0];
    } completion:^(BOOL finished) {
        [_traderContainer setHidden:YES];
        [self enableTradeButtons:YES];
    }];
}

#pragma mark - New Independent Trade VC 


-(void)executeTrade:(id)sender {
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    UIButton *btn = (UIButton *)sender;
    
    CGFloat defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_88_D_SELL_PRICE_1] floatValue];
    
    //NSLog(@"b4 %f",defPrice);
    if (defPrice<=0) {
        defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
      //  NSLog(@"zero %f",defPrice);
    }
    
    
    if (btn.tag==10) { // BUY
        
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    @"BUY",@"Action",
                    
                    _lblStockCode.text,@"StockCode",
                    _lblStockName.text,@"StockName",
                    [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
                    [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _lblStockName.text],@"stkNname",
                    trdRules, @"TradeRules",
                    nil];
        }else{
            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    @"BUY",@"Action",
                    
                    _lblStockCode.text,@"StockCode",
                    _lblStockName.text,@"StockName",
                    [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
                    [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _lblStockName.text],@"stkNname",
                    trdRules, @"TradeRules",
                    nil];
        }
      
        
        
    } else if (btn.tag==11) { // SELL
        
         if ([UserPrefConstants singleton].pointerDecimal==3) {
             dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                     @"SELL",@"Action",
                     _lblStockCode.text,@"StockCode",
                     _lblStockName.text,@"StockName",
                     [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
                     [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
                     [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _lblStockName.text],@"stkNname",
                     trdRules, @"TradeRules",
                     nil];
         }else{
             dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                     @"SELL",@"Action",
                     _lblStockCode.text,@"StockCode",
                     _lblStockName.text,@"StockName",
                     [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
                     [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
                     [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], _lblStockName.text],@"stkNname",
                     trdRules, @"TradeRules",
                     nil];
         }
        
       
        
        
    } else  if (btn.tag==12) { // REVISE
        
        dict = [[UserPrefConstants singleton].reviseCancelDict mutableCopy];
        
        
    } else  if (btn.tag==13) { // CANCEL
        
        dict = [[UserPrefConstants singleton].reviseCancelDict mutableCopy];

    }
    
    
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    TraderViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TraderViewController"];
    tradeVC.actionTypeDict = dict;
    //[vc setTransitioningDelegate:transitionController];
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES; //
    //    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    //    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    //    [self presentViewController:vc animated:NO completion:nil];
    
    tradeVC.stockLotSize = lotSizeStringInt;
    [self.view bringSubviewToFront:_traderContainer];
    [_traderContainer setAlpha:0.0];
    [_traderContainer setHidden:NO];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [_traderContainer setAlpha:1.0];
    } completion:^(BOOL finished) {
        
        [tradeVC callTrade];
        [self enableTradeButtons:NO];
    }];
    
    
    

}

-(IBAction)goTrade:(id)sender {
    
    [_atp timeoutActivity];
    

    [[ButtonEffect sharedCenter]flarify:sender inParentView:self.view withColor:[UIColor redColor] enableUponCompletion:NO];

    [self executeTrade:sender];
    
}

-(void)enableTradeButtons:(BOOL)enable {
    [_btnSell setUserInteractionEnabled:enable];
    [_btnBuy setUserInteractionEnabled:enable];
    if (enable) {
        [_btnBuy setAlpha:1.0];
        [_btnSell setAlpha:1.0];
    } else {
        [_btnBuy setAlpha:0.4];
        [_btnSell setAlpha:0.4];

    }
}

#pragma mark -


- (IBAction)clickedWatchListButton:(UIButton *)sender
{
    [_atp timeoutActivity];

    
    [[ButtonEffect sharedCenter]flarify:self.heart inParentView:self.view withColor:[UIColor redColor] enableUponCompletion:YES];
    
    // use General Selector is nicer than Popover.
    [self performSegueWithIdentifier:@"segueAddToWatchlist" sender:nil];
    
    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Add to..."]
//                                                                             message:[LanguageManager stringForKey:@"WatchList"]
//                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    for(int i =0;i<[UserPrefConstants singleton].userWatchListArray.count;i++)
//    {
//        NSDictionary *eachWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:i];
//        UIAlertAction *watchListAction = [UIAlertAction actionWithTitle:[eachWL objectForKey:@"Name"]
//                                                                  style:UIAlertActionStyleDefault
//                                                                handler:^(UIAlertAction *action)
//                                          {
//                                              ////NSLog(@"Clicked WatchList item");
//                                              ////NSLog(@"Watchlist items fav id :  %@, stockcode:%@ ",watchListHeaderKey[i],_stkCode);
//                                              [_atp addWatchlistCheckItem:[[eachWL objectForKey:@"FavID"] intValue]  withItem:_stkCode];
//                                              
//                                          }];
//        
//        [alertController addAction:watchListAction];
//    }
//    
//    
//    
//    UIPopoverPresentationController *popover = alertController.popoverPresentationController;
//    if (popover)
//    {
//        popover.sourceView = sender;
//        popover.sourceRect = sender.bounds;
//        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
//    }
//    [self presentViewController:alertController animated:YES completion:nil];
//    [alertController.view layoutIfNeeded];
}



- (void) openNews:(NSString *)url withTitle:(NSString*)title{

    [_atp timeoutActivity];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
    vc.view.backgroundColor = [UIColor clearColor];
    vc.newsTitleLabel.text = title;
    
    if (![title isEqualToString:[LanguageManager stringForKey:@"Stock News"]]) {
        vc.currentEd = currentEd;
        vc.companyNameLabel.text = [[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_39_S_COMPANY];
    } else {
        vc.currentEd = nil;
    }
    
    [vc openUrl:url withJavascript:YES];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];

}





- (IBAction)pageNoticeControlClicked:(id)sender {
    pageControlStockDetail=sender;
    int page = (int)pageControlStockDetail.currentPage;
    CGRect frame = scrollViewStockDetail.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollViewStockDetail scrollRectToVisible:frame animated:YES];
    

}


@end
