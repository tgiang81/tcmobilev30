//
//  KnowledgeCentreViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/10/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowledgeCentreViewController : UIViewController{
    
  
    
}

@property (strong, nonatomic) IBOutlet UIView *TOCContainer;
@property (strong, nonatomic) IBOutlet UIView *tocContainerView;
@property (strong, nonatomic) IBOutlet UIView *contentContainerView;



@end
