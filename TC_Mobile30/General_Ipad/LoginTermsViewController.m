//
//  LoginTermsViewController.m
//  TCiPad
//
//  Created by n2n on 10/07/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "LoginTermsViewController.h"

@interface LoginTermsViewController ()

@end

@implementation LoginTermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    
    self.termsTextView.text = self.tocText;
    [self.termsTextView setTextColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismissTerms:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
