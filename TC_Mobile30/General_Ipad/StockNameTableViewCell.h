//
//  StockNameTableViewCell.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 1/4/16.
//  Copyright (c) 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockNameTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelStockName;

@property (weak, nonatomic) IBOutlet UILabel *labelStockCode;
@property (strong, nonatomic) IBOutlet UILabel *labelStockArrow;

@end
