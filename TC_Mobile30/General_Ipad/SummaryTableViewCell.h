//
//  SummaryTableViewCell.h
//  TCiPad
//
//  Created by n2n on 22/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nettPosLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastLabel;

@end
