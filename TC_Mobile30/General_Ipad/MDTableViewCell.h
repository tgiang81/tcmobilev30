//
//  MDTableViewCell.h
//  TCiPad
//
//  Created by n2n on 11/11/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

/*
 
 This class is used in 2 Viewcontrollers - StockDetailViewController and StockPreViewController
 It is used to display Market Depth of selected stock
 
 */



#import <UIKit/UIKit.h>

@protocol MDCellDelegate <NSObject>
-(void)mdPriceDidTapped:(int)mdGroup atCell:(UITableViewCell*)cell; // mdGroup: 0=buy, 1=sell
@end

@interface MDTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *mdIndex;
@property (strong, nonatomic) IBOutlet UILabel *buySplit;
@property (strong, nonatomic) IBOutlet UILabel *buyQty;
@property (strong, nonatomic) IBOutlet UILabel *buyBid;
@property (strong, nonatomic) IBOutlet UILabel *sellAsk;
@property (strong, nonatomic) IBOutlet UILabel *sellQty;
@property (strong, nonatomic) IBOutlet UILabel *sellSplit;
@property (strong, nonatomic) IBOutlet UIView *mdBuyBgView;
@property (strong, nonatomic) IBOutlet UIView *mdSellBgView;

@property (nonatomic, weak) id<MDCellDelegate>delegate;

- (IBAction)askGroupClicked:(id)sender;
- (IBAction)buyGroupClicked:(id)sender;

@end
