//
//  HomeNewsTableViewCell.h
//  TCiPad
//
//  Created by n2n on 08/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeNewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;
@property (weak, nonatomic) IBOutlet UILabel *newsExch;
@property (weak, nonatomic) IBOutlet UILabel *newsTimestamp;

@end
