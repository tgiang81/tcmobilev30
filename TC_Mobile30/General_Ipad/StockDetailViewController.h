//
//  StockDetailViewController.h
//  TCUniversal
//
//  Created by Scott Thoo on 11/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuantityPadViewController.h"
#import "TradeAccountController.h"
#import "N2Tabs.h"
#import "LanguageManager.h"
#import "ExchangeInfo.h"
#import "GeneralSelectorViewController.h"
#import "SectorInfo.h"

@interface StockDetailViewController : UIViewController<UITextFieldDelegate, UIGestureRecognizerDelegate,
TradeAccountDelegate, N2TabsDelegate, GeneralSelectorDelegate>

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIWebView *chartWebView;
@property (weak, nonatomic) IBOutlet UIView *chartView;

@property (weak, nonatomic) IBOutlet UITableView *mdTable;
@property (weak, nonatomic) IBOutlet UIView *mdFooter;
@property (weak, nonatomic) IBOutlet UILabel *mdFootTotBuySplit;
@property (weak, nonatomic) IBOutlet UILabel *mdFootTotBuyQty;
@property (weak, nonatomic) IBOutlet UILabel *mdFootAveBuyBid;
@property (weak, nonatomic) IBOutlet UILabel *mdFootTotSellSplit;
@property (weak, nonatomic) IBOutlet UILabel *mdFootTotSellQty;
@property (weak, nonatomic) IBOutlet UILabel *mdFootAveSellAsk;
@property (weak, nonatomic) IBOutlet UILabel *mdTotalBidLabel;
@property (weak, nonatomic) IBOutlet UILabel *mdTotalAskLabel;


@property (weak, nonatomic) IBOutlet UIView *tabsPlaceholder;
@property (weak, nonatomic) IBOutlet UIView *timeSalesView;
@property (weak, nonatomic) IBOutlet UIView *bizDoneView;
@property (weak, nonatomic) IBOutlet UITableView *bizDoneTable;


@property (weak, nonatomic) IBOutlet UILabel *lblStockName;
@property (weak, nonatomic) IBOutlet UILabel *lblStockCode;
@property (weak, nonatomic) IBOutlet UILabel *lblLacp;
@property (weak, nonatomic) IBOutlet UILabel *lblHigh;
@property (weak, nonatomic) IBOutlet UILabel *lblLow;
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalBuyVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalSellVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalBuyTrans;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalSellTrans;
@property (weak, nonatomic) IBOutlet UILabel *lblShareIssued;
@property (weak, nonatomic) IBOutlet UILabel *lblMarketCap;
@property (weak, nonatomic) IBOutlet UILabel *lblOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblClose;
@property (weak, nonatomic) IBOutlet UILabel *valueTradedLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblTrades;
@property (weak, nonatomic) IBOutlet UILabel *lblCeil;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@property (weak, nonatomic) IBOutlet UILabel *lblBid;
@property (weak, nonatomic) IBOutlet UILabel *lblBidQty;
@property (weak, nonatomic) IBOutlet UILabel *lblAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblAskQty;
@property (weak, nonatomic) IBOutlet UILabel *shareIssueLabel;
@property (weak, nonatomic) IBOutlet UILabel *marketCapLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblChange;
@property (weak, nonatomic) IBOutlet UILabel *lblChangerPer;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDn;
@property (weak, nonatomic) IBOutlet UILabel *lacpLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalShortSellVol;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *btnloading;
@property (weak, nonatomic) IBOutlet UIView *fundamentalView;


@property (weak, nonatomic) IBOutlet UIView *detail0View;
@property (weak, nonatomic) IBOutlet UIView *detail1View;
@property (weak, nonatomic) IBOutlet UIView *detail2View;



@property (strong, nonatomic) NSString *stkCode;
@property (strong, nonatomic) NSArray *timeAndSalesTableDataArr;
@property (strong, nonatomic) NSArray *companyNewsTableDataArr;
@property (strong, nonatomic) NSArray *archiveNewsTableDataArr;
@property (strong, nonatomic) NSArray *bizDoneTableDataArr;
@property (strong, nonatomic) NSArray *sortedWLArr;

@property (weak, nonatomic) IBOutlet UITableView *timeAndSalesTableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UITableView *companyNewsTableView;



@property (weak, nonatomic) IBOutlet UIButton *btnBuy;
@property (weak, nonatomic) IBOutlet UIButton *btnSell;

@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (weak, nonatomic) IBOutlet UILabel *tradingBoardLabel;
@property (weak, nonatomic) IBOutlet UILabel *ssellVolumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *lotSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *isinLabel;
@property (weak, nonatomic) IBOutlet UILabel *parLabel;


@property (weak, nonatomic) IBOutlet UILabel *activeLabel;
@property (weak, nonatomic) IBOutlet UIButton *chartErrorMsg;
@property (weak, nonatomic) IBOutlet UIImageView *imgChart;
@property (weak, nonatomic) IBOutlet UIView *traderContainer;

@property (weak, nonatomic) IBOutlet UIButton *heart;

@property (nonatomic,strong) UILabel *lastUpdatedHeader;
@property (nonatomic,strong) NSString *lastUpdatedStr;

- (IBAction)goTrade:(id)sender;
-(IBAction)loadChart:(id)sender;

@end
