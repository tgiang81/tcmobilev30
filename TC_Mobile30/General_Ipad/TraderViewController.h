//
//  TraderViewController.h
//  TCiPad
//
//  Created by n2n on 10/10/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetails.h"
#import "UserPrefConstants.h"
#import "UIImageUtility.h"
#import "TradingRules.h"
#import "OrderControl.h"
#import "AdditionalPaymentData.h"
#import "QCData.h"
#import "QCConstants.h"
#import "ATPAuthenticate.h"

#import "SkipConfirmationViewController.h"
#import "TradeStatusViewController.h"
#import "ValidatePINViewController.h"
#import "QuantityPadViewController.h"
#import "PricePadViewController.h"

#import "MarqueeLabel.h"
#import "DGActivityIndicatorView.h"
#import "UserAccountClientData.h"
#import "NewsModalViewController.h"
#import "TransitionDelegate.h"
#import "ExchangeData.h"
#import "LanguageManager.h"
#import "CurrencyRate.h"
#import "N2DatePicker.h"
#import "GTMRegex.h"


@protocol TraderDelegate <NSObject>
@required
-(void)traderDidCancelled;
-(void)traderDidCancelledNormal;
@end

@interface TraderViewController : UIViewController <UIPickerViewDataSource,
                                                    UIPickerViewDelegate,
                                                    UITextFieldDelegate,
                                                    QuantityPadDelegate,
                                                    PricePadDelegate,
                                                    UIGestureRecognizerDelegate,
                                                    SkipDialogDelegate,
                                                    TradeStatusMsgDelegate,
                                                    ValidatePINDelegate,
                                                    N2DatePickerDelegate>
{
    
    long validityRow, priceRow, ordTypeRow, triggerRow, directionRow,
        currencyRow, paymentRow, whichQty, whichPrice, triggerPriceRow;
    NSMutableArray *pickerList;
    UIPickerView *myPickerView;
    NSMutableArray *marketDepthArr;
    UIPopoverPresentationController *popoverController;
    UIPopoverController *columnPickerPopover;
    QuantityPadViewController *qtyPadBtn;
    PricePadViewController *pricePad;
   // UIDatePicker *datePickerView;
    N2DatePicker *datePicker;
    NSDate *dateRow;
    BOOL padLarge;
    UITableView *accTableView;
    UIViewController *viewcont;
    NSMutableArray *acclist;
    TradingRules *trdRules;
    ATPAuthenticate *atp;
    SkipConfirmationViewController *skipVC;
    TradeStatusViewController *tsVC;
    ValidatePINViewController *validVC;
    long currencyPlusIndex;
    NSNumberFormatter *priceFormatter;
    UserAccountClientData *uacd;
    
    
    BOOL  revisePrice,priceAdd,priceMinus,reviseLimit,limitAdd,limitMinus,reviseQty,qtyAdd,
    qtyMinus,reviseMinQty,minQtyAdd,minQtyMinus,reviseDisclosedQty,disclosedQtyAdd,disclosedQtyMinus,
    reviseTriggerType,reviseTriggerDirection,reviseValidity, reviseOrdType, reviseCurrency, revisePayment;
}

@property (nonatomic, strong) OrderDetails *myOrderDetails;
@property (nonatomic, strong) TradeStatus *myTradeStatus;

@property (weak, nonatomic) id<TraderDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *accountListBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *expandBtn;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;

@property (weak, nonatomic) IBOutlet UIView *headerBg;
@property (weak, nonatomic) IBOutlet UIButton *headerTitle;
@property (weak, nonatomic) IBOutlet UIView *padView;
@property (weak, nonatomic) IBOutlet UIView *toolBarView;
@property (strong, nonatomic) NSDictionary *actionTypeDict;

@property (weak, nonatomic) IBOutlet UIPickerView *selectorPicker;

@property (weak, nonatomic) IBOutlet UIPickerView *selectorPickerSmallSize;
@property (weak, nonatomic) IBOutlet UIView *pickerContainter;

@property (weak, nonatomic) IBOutlet UILabel *titleQty;
@property (weak, nonatomic) IBOutlet UILabel *titleMinQty;
@property (weak, nonatomic) IBOutlet UILabel *titleDiscQty;

// field containing view
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UIView *qtyView;
@property (weak, nonatomic) IBOutlet UIView *minQtyView;
@property (weak, nonatomic) IBOutlet UIView *trigPriceView;
@property (weak, nonatomic) IBOutlet UIView *triggerVIew;
@property (weak, nonatomic) IBOutlet UIView *directionView;
@property (weak, nonatomic) IBOutlet UIView *discloseView;

@property (weak, nonatomic) IBOutlet UILabel *accountName;
@property (weak, nonatomic) IBOutlet UILabel *tradeLimit;
@property (weak, nonatomic) IBOutlet UILabel *tradeLimitTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *triggerType;
@property (weak, nonatomic) IBOutlet UITextField *triggerPrice;
@property (weak, nonatomic) IBOutlet UILabel *triggerDirection;
@property (weak, nonatomic) IBOutlet UITextField *discloseQty;
@property (weak, nonatomic) IBOutlet UIView *loadingView;

// textfields
@property (weak, nonatomic) IBOutlet UITextField *stkPrice;
@property (weak, nonatomic) IBOutlet UILabel *stkNname;
@property (weak, nonatomic) IBOutlet UITextField *quantity;
@property (weak, nonatomic) IBOutlet UITextField *minquantity;
@property (weak, nonatomic) IBOutlet UILabel *totalValue;
@property (weak, nonatomic) IBOutlet UILabel *unitsnprice;
@property (weak, nonatomic) IBOutlet UIButton *shortSellBtn;

@property (weak, nonatomic) IBOutlet UIButton *btnSkipConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *errorMsgLabel;
@property (strong, nonatomic) DGActivityIndicatorView *traderWorking;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;
@property (weak, nonatomic) IBOutlet UIView *boxView;

@property (assign, nonatomic) BOOL fromStockDetail, isShortSell;
@property (assign, nonatomic) CGRect containerFrame;
@property (strong, nonatomic) UIViewController *parentController;
@property (strong, nonatomic) MarqueeLabel  *accLabel;
@property (strong, nonatomic) NSString *stockCurrency, *exchangeCurrency, *stkExchange;
@property (nonatomic, strong) TransitionDelegate *transitionController;
@property (nonatomic) long stockLotSize;
@property (assign, nonatomic) double resolvedLimit;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;


@property (strong, nonatomic) ExchangeData *currentED;
@property (nonatomic, strong) NSMutableArray *currencyList, *paymentList, *orderValidityList, *orderTypeList;


- (IBAction)cancelTrade:(id)sender;
- (IBAction)optionBtnPressed:(UIButton *)sender;
- (IBAction)openQtyoptions:(UIButton *)sender;
- (IBAction)openPricePad:(id)sender;
- (IBAction)dateBtnPressed:(UIButton *)sender;
- (IBAction)btnSkipConfirmationPressed:(UIButton *)sender;
- (IBAction)toggleMoreFields:(id)sender;
- (IBAction)submitButton:(id)sender;
- (IBAction)refreshTrdLmtBtnPressed:(id)sender;
- (IBAction)accountBtnPressed:(UIButton *)sender;
-(void)callTrade;
-(void)updateTradingAccount:(UserAccountClientData*)account;

// FIELDS TYPED
- (IBAction)priceEditingChanged:(id)sender;
- (IBAction)qtyEditingChanged:(id)sender;
- (IBAction)minQtyEditingChanged:(id)sender;
- (IBAction)discQtyEditingChanged:(id)sender;

- (IBAction)triggerPriceEditingChanged:(id)sender;
- (IBAction)toggleShortSell:(id)sender;



@end
