//
//  AppInfoViewController.m
//  TCiPad
//
//  Created by n2n on 04/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "AppInfoViewController.h"
#import "NewsModalViewController.h"
#import "TransitionDelegate.h"
#import "AppControl.h"
#import "UserPrefConstants.h"

@interface AppInfoViewController ()

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end



@implementation AppInfoViewController
@synthesize transitionController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _appLabel.text = [UserPrefConstants singleton].appName;
    
    _logoImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[UserPrefConstants singleton].companyLogo]];
    
    _termAndConditionTextView.text = [UserPrefConstants singleton].aboutUs;
    
    _contactsView.text = [NSString stringWithFormat:@"%@ \n\nAddress: %@ \n\nTel: %@ \nFax: %@\nEmail: %@\nWebsite: %@\n",
                          [UserPrefConstants singleton].companyName,
                          [UserPrefConstants singleton].companyAddress,
                          [UserPrefConstants singleton].companyTel,
                          [UserPrefConstants singleton].companyFax,
                          [UserPrefConstants singleton].emailLink,
                          [UserPrefConstants singleton].websiteLink];

    _versionLabel.text = [NSString stringWithFormat:@"Version %@, Build %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey]];
    
    for(UIView *v in [self.view subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
            btn.layer.masksToBounds = YES;
        }
    }
    
    [self.view setBackgroundColor:kPanelColor];
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)shareApp:(id)sender {
    
    UIButton *rect = (UIButton*)sender;
    
    NSString *textToShare = [NSString stringWithFormat:@"Download %@ for iPad",[UserPrefConstants singleton].appName];
    NSURL *myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[UserPrefConstants singleton].websiteLink]];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
    [popup presentPopoverFromRect:rect.frame
                           inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    //[self presentViewController:activityVC animated:YES completion:nil];
    
}

- (IBAction)clickFacebook:(id)sender{
     if ([UserPrefConstants singleton].facebookLink.length>0) {
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
         vc.view.backgroundColor = [UIColor clearColor];
         vc.newsTitleLabel.text = @"Facebook";
         vc.currentEd = nil;
         
         NSString *url = [NSString stringWithFormat:@"%@",[UserPrefConstants singleton].facebookLink];
    
         [vc openUrl:url withJavascript:NO];
         [vc setTransitioningDelegate:transitionController];
         vc.modalPresentationStyle= UIModalPresentationCustom;
         [self presentViewController:vc animated:YES completion:nil];
     }
         else{
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Not Available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
             [alertView show];
         }
}
- (IBAction)clickTwitter:(id)sender{
    
    if ([UserPrefConstants singleton].twitterText.length>0) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
        vc.view.backgroundColor = [UIColor clearColor];
        vc.newsTitleLabel.text = @"Twitter";
        
        vc.currentEd = nil;
        
        NSString *url = [NSString stringWithFormat:@"%@",[UserPrefConstants singleton].twitterText];
        
        [vc openUrl:url withJavascript:NO];
        [vc setTransitioningDelegate:transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"Not Available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];

    }
    
    
    
}
- (IBAction)clickLinkedIn:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
    vc.view.backgroundColor = [UIColor clearColor];
    vc.newsTitleLabel.text = @"LinkedIn";
    
    vc.currentEd = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@",[UserPrefConstants singleton].linkInText];
    
    
    
    [vc openUrl:url withJavascript:NO];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}
- (IBAction)clickWebsite:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
    vc.view.backgroundColor = [UIColor clearColor];
    vc.newsTitleLabel.text = @"Abacus Securities Corporation";
    
    vc.currentEd = nil;
    
    NSString *url = [NSString stringWithFormat:@"%@",@"https://www.mytrade.com.ph"];
    
    [vc openUrl:url withJavascript:NO];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
//    vc.view.backgroundColor = [UIColor clearColor];
//    vc.newsTitleLabel.text = @"Website";
//    
//    vc.currentEd = nil;
//    
//    NSString *url = [NSString stringWithFormat:@"%@",[UserPrefConstants singleton].websiteLink];
//    
//    [vc openUrl:url withJavascript:NO];
//    [vc setTransitioningDelegate:transitionController];
//    vc.modalPresentationStyle= UIModalPresentationCustom;
//    [self presentViewController:vc animated:YES completion:nil];
    
    // CIMB disable loading website in WebView. So better to open in Safari directly.
    
  //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[UserPrefConstants singleton].websiteLink] options:@{} completionHandler:nil];
    
}

@end
