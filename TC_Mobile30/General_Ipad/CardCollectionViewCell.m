//
//  CardCollectionViewCell.m
//  TCiPad
//
//  Created by n2n on 19/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "CardCollectionViewCell.h"

@interface CardCollectionViewCell () {

    BOOL runOnce;
}

@end

@implementation CardCollectionViewCell

@synthesize delegate;

#pragma mark - Init

-(void)layoutSubviews {
    [super layoutSubviews];

    [_detailScroll setPagingEnabled:YES];
    [_detailScroll setContentSize:CGSizeMake(_detail1View.frame.size.width*2.0, _detailScroll.frame.size.height)];
    [_detailScroll addSubview:_detail1View]; [_detailScroll addSubview:_detail2View];
    
    _detail1View.center = CGPointMake(0.5*_detailScroll.frame.size.width, _detailScroll.frame.size.height/2.0);
    _detail2View.center = CGPointMake(1.5*_detailScroll.frame.size.width, _detailScroll.frame.size.height/2.0);

}


-(void)awakeFromNib {
    [super awakeFromNib];
    
    
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.chartManager = [[ImgChartManager alloc] init];
        
        
        _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidDoubleTapped:)];
        _doubleTap.numberOfTapsRequired = 2;
        [self addGestureRecognizer:_doubleTap];
        
        _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(cellDidLongPress:)];
        _longPress.minimumPressDuration = 1.5;
        [self addGestureRecognizer:_longPress];
        
        [self setBackgroundColor:[UIColor clearColor]];
        [self.layer setBackgroundColor:kPanelColor.CGColor];
        [self.layer setCornerRadius:5.0];
        [_reloadBtn setHidden:YES];
        
   }
    return self;
}



-(void)cellDidLongPress:(UILongPressGestureRecognizer*)gesture {
    
    // NSLog(@"Cell %ld", gesture.view.tag);
    if ([self.delegate respondsToSelector:@selector(cellGoEditMode:)]) {
        [self.delegate cellGoEditMode:gesture.view.tag];
    }
    
}


-(void)cellDidDoubleTapped:(UITapGestureRecognizer*)gesture {
    
  // NSLog(@"Cell %ld", gesture.view.tag);
    if ([self.delegate respondsToSelector:@selector(goDetailDidClicked:)]) {
        [self.delegate goDetailDidClicked:gesture.view.tag];
    }
}

-(void)loadChartWithDict:(NSDictionary*)dataDict reload:(BOOL)isReload {
    
    NSString *stkCode = [dataDict objectForKey:FID_33_S_STOCK_CODE];

    CGFloat wid = 0;
    if (isReload) {
        wid  = (_stkImgChart.frame.size.width-1) * 2.0;
    } else {
        wid = _stkImgChart.frame.size.width * 2.0;
    }
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@&w=%.0f&h=%.0f&k=%@&c=b&d=%d",
                           [UserPrefConstants singleton].historicalChartURL,
                           wid,
                           _stkImgChart.frame.size.height * 2.0,
                           stkCode,
                           60]; // 3months
    
    NSLog(@"stkCode: %@",urlString);
    
    [_chartManager getImageChartWithURL:urlString
                      completedWithData:^(NSData *data) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [_imgLoading stopAnimating];
                              [[UserPrefConstants singleton].imgChartCache setObject:data forKey:stkCode];
                              _stkImgChart.image = [UIImage imageWithData:data];
                              [_reloadBtn setHidden:YES];
                          });
                      }
                                failure:^(NSString *errorMsg) {
                                    NSLog(@"errorMsg chart %@,stkCode=%@",errorMsg,stkCode);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                       // [_imgLoading stopAnimating];
                                        
                               
                                        
                                        [self performSelector:@selector(automaticReloadChart:) withObject:stkCode afterDelay:5.0f];
                                        
                                       
                                    });
                                }];
}

- (void) automaticReloadChart:(NSString *)stkCode{
    
    CGFloat wid  = (_stkImgChart.frame.size.width-1) * 2.0;
  
    NSString *urlString = [NSString stringWithFormat:@"%@&w=%.0f&h=%.0f&k=%@&c=b&d=%d",
                           [UserPrefConstants singleton].historicalChartURL,
                           wid,
                           _stkImgChart.frame.size.height * 2.0,
                           stkCode,
                           60];
    
  
    NSLog(@"automaticReloadChart %@",urlString);
    [_chartManager getImageChartWithURL:urlString
                      completedWithData:^(NSData *data) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [_imgLoading stopAnimating];
                              [[UserPrefConstants singleton].imgChartCache setObject:data forKey:stkCode];
                              _stkImgChart.image = [UIImage imageWithData:data];
                              [_reloadBtn setHidden:YES];
                          });
                      }
                                failure:^(NSString *errorMsg) {
                                    NSLog(@"errorMsg chart %@",errorMsg);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [_imgLoading stopAnimating];
                                        [_reloadBtn setHidden:NO];
                                    });
                                }];
}

#pragma mark - Update Cell Contents

// For Reusable cell
-(void)populateCellsWithDictionary:(NSDictionary*)dataDict
                          fromDict:(NSDictionary*)oldDict
                            forRow:(long)row
                           doFlash:(BOOL)isUpdate
                        detailPage:(NSInteger)page
                          withCode:(NSString*)stockCode
                       isWatchList:(BOOL)isWatchList
{
    
    self.tag = row;
    _reloadBtn.tag = row;
    _butBtn.tag = row;
    _sellBtn.tag = row;
    _favBtn.tag = row;
    _detailBtn.tag = row;
    
    _pgCtrl.currentPage = page;
    _detailScroll.contentOffset = CGPointMake(_detail1View.frame.size.width*page, _detailScroll.contentOffset.y);
    
    //[_favBtn setHidden:isWatchList?YES:NO];
    
    // NSLog(@"%ld",_idx);
    [_reloadBtn setHidden:YES];
    
    if ([dataDict.allKeys count]<=0) {
        // no data
        [_stkImgChart.layer setBackgroundColor:[UIColor blackColor].CGColor];
        [_stkImgChart.layer setCornerRadius:8.0];
        _stkImgChart.image = nil;
        
        [_imgLoading stopAnimating];
        _lastDone.text = @"-"; _lastDone.textColor = [UIColor whiteColor];
        _change.text = @"-"; _change.textColor = [UIColor whiteColor];
        _changePer.text = @"-"; _changePer.textColor = [UIColor whiteColor];
        _volume.text = @"-"; _volume.textColor = [UIColor whiteColor];
        _high.text = @"-"; _high.textColor = [UIColor whiteColor];
        _low.text = @"-"; _low.textColor = [UIColor whiteColor];
        _open.text = @"-"; _open.textColor = [UIColor whiteColor];
        _lacp.text = @"-"; _lacp.textColor = [UIColor whiteColor];
        _value.text = @"-"; _value.textColor = [UIColor whiteColor];
        _trades.text = @"-"; _trades.textColor = [UIColor whiteColor];
        _prevClose.text = @"-"; _prevClose.textColor = [UIColor whiteColor];
        _mktCap.text = @"-"; _mktCap.textColor = [UIColor whiteColor];
        _bid.text = @"-"; _bid.textColor = [UIColor whiteColor];
        _bidQty.text = @"-"; _bidQty.textColor = [UIColor whiteColor];
        _ask.text = @"-"; _bid.textColor = [UIColor whiteColor];
        _askQty.text = @"-"; _bidQty.textColor = [UIColor whiteColor];
        _stkName.text = [NSString stringWithFormat:@"%@ - No Data",stockCode];
        
        [self removeGestureRecognizer:_doubleTap];
        
        return;
    }
    
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setRoundingMode: NSNumberFormatterRoundDown];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    
    
    NSString *stkNameStr = @"";
    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
        stkNameStr = [dataDict objectForKey:FID_130_S_SYSBOL_2];
    } else {
        stkNameStr = [dataDict objectForKey:FID_38_S_STOCK_NAME];
    }
    
    _stkName.text = stkNameStr;
    [_stkImgChart.layer setBackgroundColor:[UIColor blackColor].CGColor];
    [_stkImgChart.layer setCornerRadius:8.0];
    _stkImgChart.image = nil;
    
    NSString *stkCode = [dataDict objectForKey:FID_33_S_STOCK_CODE];
    NSData *cachedData = [[UserPrefConstants singleton].imgChartCache objectForKey:stkCode];
    
    if ([cachedData length]>0) {
        _stkImgChart.image =  [UIImage imageWithData:cachedData];
        [_imgLoading stopAnimating];
    } else {
        if (![_imgLoading isAnimating]) {
            [_imgLoading startAnimating];
            
            [self loadChartWithDict:dataDict reload:NO];
            
        }
    }
    
    // LAST DONE
    NSNumber *value =[dataDict objectForKey:FID_98_D_LAST_DONE_PRICE];
    CGFloat lastDoneFloat = [value doubleValue];
    
    
    
    NSString *lastDoneString = [priceFormatter stringFromNumber:value];
    if (lastDoneString==nil) {
        if ([UserPrefConstants singleton].pointerDecimal==4) {
            lastDoneString = [NSString stringWithFormat:@"%.4f",lastDoneFloat];
        }else{
            lastDoneString = [NSString stringWithFormat:@"%.3f",lastDoneFloat];
        }
    }
    
    
    NSLog(@"lastDoneFloat %.3f",lastDoneFloat);
    _lastDone.text = lastDoneString;//[priceFormatter stringFromNumber:value];
    CGFloat priceChanged = [value doubleValue]-[[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
    [_lastDone setTextColor:[self getColorFromDifference:priceChanged]];
    
    // CHANGE
    value = [dataDict objectForKey:FID_CUSTOM_F_CHANGE];
    _change.text = [priceFormatter stringFromNumber:value];
    // if ([value doubleValue] >0) _change.text = [NSString stringWithFormat:@"+%@",_change.text]; // CQA request to remove
    [_change setTextColor:[self getColorFromDifference:[value doubleValue]]];
    
   
    // CHANGEPER
    value = [dataDict objectForKey:FID_CUSTOM_F_CHANGE_PER];
    
    if ([UserPrefConstants singleton].pointerDecimal==3) {
            _changePer.text = [NSString stringWithFormat:@"%.3f%%",[value doubleValue]];
    }else{
            _changePer.text = [NSString stringWithFormat:@"%.4f%%",[value doubleValue]];
    }
    

   // if ([value doubleValue] >0) _changePer.text = [NSString stringWithFormat:@"+%@",_changePer.text]; // CQA request to remove
    [_changePer setTextColor:_change.textColor];
    
    // VOLUME
    value = [dataDict objectForKey:FID_101_I_VOLUME];
    _volume.text = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
    [_volume setTextColor:[UIColor yellowColor]];
    
    // VALUE
     value = [dataDict objectForKey:FID_102_D_VALUE];
    _value.text = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
    
    // HIGH
    value =[dataDict objectForKey:FID_56_D_HIGH_PRICE];
    _high.text = [priceFormatter stringFromNumber:value];
    priceChanged = [value doubleValue]-[[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
    [_high setTextColor:[self getColorFromDifference:priceChanged]];

    // LOW
    value =[dataDict objectForKey:FID_57_D_LOW_PRICE];
    _low.text = [priceFormatter stringFromNumber:value];
    priceChanged = [value doubleValue]-[[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
    [_low setTextColor:[self getColorFromDifference:priceChanged]];
    
    // OPEN
    value =[dataDict objectForKey:FID_55_D_OPEN_PRICE];
    _open.text = [priceFormatter stringFromNumber:value];
    priceChanged = [value doubleValue]-[[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
    [_open setTextColor:[self getColorFromDifference:priceChanged]];
    
    // LACP
    value =[dataDict objectForKey:FID_51_D_REF_PRICE];
    
    
    CGFloat lacpFloat = 0;
    lacpFloat = [[dataDict objectForKey:FID_51_D_REF_PRICE] floatValue];
    
    
    NSString *lacpString = [priceFormatter stringFromNumber:value];
    if (lacpString==nil) {
        if ([UserPrefConstants singleton].pointerDecimal==4) {
            lacpString = [NSString stringWithFormat:@"%.4f",lacpFloat];
        }else{
            lacpString = [NSString stringWithFormat:@"%.3f",lacpFloat];
        }
    }
    
    _lacp.text = lacpString;//[priceFormatter stringFromNumber:value];
    
    // TRADES
    value = [dataDict objectForKey:FID_132_I_TOT_OF_TRD];
    _trades.text = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
    
    // PREVCLOSE
    value =[dataDict objectForKey:FID_50_D_CLOSE];
    _prevClose.text = [priceFormatter stringFromNumber:value];
    priceChanged = [value doubleValue]-[[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
    [_prevClose setTextColor:[self getColorFromDifference:priceChanged]];
    
    // BID PRICE
    value =[dataDict objectForKey:FID_68_D_BUY_PRICE_1];
    _bid.text = [priceFormatter stringFromNumber:value];
    priceChanged = [value doubleValue]-[[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
    [_bid setTextColor:[self getColorFromDifference:priceChanged]];
    
    // BID QTY
    value = [dataDict objectForKey:FID_58_I_BUY_QTY_1];
    _bidQty.text = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
    [_bidQty setTextColor:[UIColor yellowColor]];
    
    // ASK PRICE
    value =[dataDict objectForKey:FID_88_D_SELL_PRICE_1];
    _ask.text = [priceFormatter stringFromNumber:value];
    priceChanged = [value doubleValue]-[[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
    [_ask setTextColor:[self getColorFromDifference:priceChanged]];
    
    // ASK QTY
    value = [dataDict objectForKey:FID_78_I_SELL_QTY_1];
    _askQty.text = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
    [_askQty setTextColor:[UIColor yellowColor]];
    
    // MKTCAP
    long long totalMarCap     =[[dataDict objectForKey:FID_100004_F_MKT_CAP]longLongValue];
    long long totalShareIssued=[[dataDict objectForKey:FID_41_D_SHARES_ISSUED]longLongValue];
    if (totalMarCap<=0) {
        totalMarCap = totalShareIssued * lastDoneFloat;
        if (lastDoneFloat<=0) {
            totalMarCap = totalShareIssued * [[dataDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        }
    }
    _mktCap.text = [[UserPrefConstants singleton] abbreviateNumber:totalMarCap];
    [_mktCap setTextColor:[UIColor yellowColor]];
    
    
    if (isUpdate) {
        
        // LAST DONE
        [self flashPrice:dataDict fromDict:oldDict field:FID_98_D_LAST_DONE_PRICE forLabel:_lastDone];
        
        // CHANGE
        [self flashPrice:dataDict fromDict:oldDict field:FID_CUSTOM_F_CHANGE forLabel:_change];
        
        // CHANGEPER
        [self flashPrice:dataDict fromDict:oldDict field:FID_CUSTOM_F_CHANGE_PER forLabel:_changePer];
        
        // VOLUME
        [self flashQuantity:dataDict fromDict:oldDict field:FID_101_I_VOLUME forLabel:_volume];
        
        // VALUE
        [self flashQuantity:dataDict fromDict:oldDict field:FID_102_D_VALUE forLabel:_value];

        // HIGH
        [self flashPrice:dataDict fromDict:oldDict field:FID_56_D_HIGH_PRICE forLabel:_high];
        
        // LOW
        [self flashPrice:dataDict fromDict:oldDict field:FID_57_D_LOW_PRICE forLabel:_low];
        
        // OPEN
        [self flashPrice:dataDict fromDict:oldDict field:FID_55_D_OPEN_PRICE forLabel:_open];
        
        // LACP
        [self flashPrice:dataDict fromDict:oldDict field:FID_51_D_REF_PRICE forLabel:_lacp];
        
        // TRADES
        [self flashQuantity:dataDict fromDict:oldDict field:FID_132_I_TOT_OF_TRD forLabel:_trades];
        
        // PREVCLOSE
        [self flashPrice:dataDict fromDict:oldDict field:FID_50_D_CLOSE forLabel:_prevClose];
        
        // BID PRICE
        [self flashPrice:dataDict fromDict:oldDict field:FID_68_D_BUY_PRICE_1 forLabel:_bid];
        
        // BID QTY
        [self flashQuantity:dataDict fromDict:oldDict field:FID_58_I_BUY_QTY_1 forLabel:_bidQty];
        
        // ASK PRICE
        [self flashPrice:dataDict fromDict:oldDict field:FID_88_D_SELL_PRICE_1 forLabel:_ask];
        
        // ASK QTY
        [self flashQuantity:dataDict fromDict:oldDict field:FID_78_I_SELL_QTY_1 forLabel:_askQty];
        
        // MKTCAP
        [self flashQuantity:dataDict fromDict:oldDict field:FID_100004_F_MKT_CAP forLabel:_mktCap];
        
        // VWAP
        
        // AVETRADEPRICE
    }
    
}


// -----------------------------SCROLLVIEW DELEGATE-------------------------
//                             Set the pagecontrol
// -------------------------------------------------------------------------


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = _detailScroll.frame.size.width;
    int page = floor((_detailScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pgCtrl.currentPage = page;
    if ([self.delegate respondsToSelector:@selector(detailDidScrollToPage:atRow:)]) {
        [self.delegate detailDidScrollToPage:page atRow:self.tag];
    }
    
}

// -----------------------------HELPER FUNC---------------------------------
//                          Help to flash labels
// -------------------------------------------------------------------------

-(void)flashQuantity:(NSDictionary*)dataDict fromDict:(NSDictionary*)oldDict field:(NSString*)FID forLabel:(UILabel*)lbl {
    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        if ([[dataDict objectForKey:FID] longLongValue]> [[oldDict objectForKey:FID] longLongValue])
            lbl.layer.backgroundColor=kGrnFlash.CGColor;
        else if([[dataDict objectForKey:FID] longLongValue]<[[oldDict objectForKey:FID] longLongValue])
            lbl.layer.backgroundColor=kRedFlash.CGColor;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            lbl.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
    }];
}

-(void)flashPrice:(NSDictionary*)dataDict fromDict:(NSDictionary*)oldDict field:(NSString*)FID forLabel:(UILabel*)lbl {
    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        if ([[dataDict objectForKey:FID] doubleValue]> [[oldDict objectForKey:FID] doubleValue])
            lbl.layer.backgroundColor=kGrnFlash.CGColor;
        else if([[dataDict objectForKey:FID] doubleValue]<[[oldDict objectForKey:FID] doubleValue])
            lbl.layer.backgroundColor=kRedFlash.CGColor;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            lbl.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
    }];
    
}

-(UIColor*)getColorFromDifference:(double)changed {
    
    UIColor *result = [UIColor whiteColor];
    if (changed<0) {
        result = [UIColor redColor];
    } else if (changed>0) {
        result = [UIColor greenColor];
    } else {
        result = [UIColor whiteColor];
    }
    return result;
}

// -----------------------------DELEGATES ----------------------------------
//                          Handles button taps
// -------------------------------------------------------------------------

-(void)reloadChartWithDict:(NSDictionary *)dict {
    [_reloadBtn setHidden:YES];
    [_imgLoading startAnimating];
    [self loadChartWithDict:dict reload:YES];
}

- (IBAction)buyStock:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(buyStockDidClicked:)]) {
        [self.delegate buyStockDidClicked:sender.tag];
    }
}

- (IBAction)sellStock:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(sellStockDidClicked:)]) {
        [self.delegate sellStockDidClicked:sender.tag];
    }
}

- (IBAction)addToWatchList:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(addToWatchListClicked:atButton:)]) {
        [self.delegate addToWatchListClicked:sender.tag atButton:sender];
    }
}

- (IBAction)goDetail:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(goDetailDidClicked:)]) {
        [self.delegate goDetailDidClicked:sender.tag];
    }
}


- (IBAction)reloadChart:(UIButton*)sender {
    if ([self.delegate respondsToSelector:@selector(reloadButtonDidClicked:)]) {
        [self.delegate reloadButtonDidClicked:sender.tag];
    }
    
}




@end
