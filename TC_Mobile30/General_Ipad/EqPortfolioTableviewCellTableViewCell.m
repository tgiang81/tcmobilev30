//
//  EqPortfolioTableviewCellTableViewCell.m
//  TCiPad
//
//  Created by Sri Ram on 12/15/15.
//  Copyright © 2015 N2N. All rights reserved.
//

#import "EqPortfolioTableviewCellTableViewCell.h"

@implementation EqPortfolioTableviewCellTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}
- (IBAction)buyBtnPressed:(id)sender {
    if (_delegate) {
        [_delegate buyorderBtnPressed:self];
    }
}

- (IBAction)sellButtonPressed:(id)sender {
    if (_delegate) {
        [_delegate sellorderBtnPressed:self];
    }
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
