//
//  StockTracker.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 14/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockTracker : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *lblStkName;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *sbhLabel;
@property (weak, nonatomic) IBOutlet UILabel *bbhLabel;
@property (weak, nonatomic) IBOutlet UIView *animationView;

- (void) updateContent:(NSString *)stockName
              andPrice:(float)price
               andLACP:(float)lacp
           andQuantity:(NSString *)quantity
              andCondition:(NSString *)status
               andBBH:(NSString *)bbh
             andSBH:(NSString *)sbh;


@end
