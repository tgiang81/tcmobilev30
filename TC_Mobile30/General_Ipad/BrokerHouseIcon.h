//
//  BrokerHouseIcon.h
//  TCiPad
//
//  Created by Scott Thoo on 1/6/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+AddtionalFunctionalities.h"
@interface BrokerHouseIcon : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIButton *btnIcon;
@property (strong, nonatomic) IBOutlet UILabel *lblBrokerName;
@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;
@property (strong, nonatomic) IBOutlet UILabel *notAvail;


- (id)initWithBrokerName:(NSString *)brokerName  andIconName:(NSString*)iconName;


@end
