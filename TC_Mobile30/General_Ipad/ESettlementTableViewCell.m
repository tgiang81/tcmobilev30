//
//  ESettlementTableViewCell.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 18/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "ESettlementTableViewCell.h"

@implementation ESettlementTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
