//
//  CollectionReusableHeaderView.h
//  TCiPad
//
//  Created by n2n on 08/02/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"

@interface CollectionReusableHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UIButton *toggleWLBtn;
@property (weak, nonatomic) IBOutlet UIButton *sortStkNameBtn;
@property (weak, nonatomic) IBOutlet UIButton *addStkBtn;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;


-(void)hideSortButtons:(BOOL)hide;

@end
