//
//  QuantityPadViewController.m
//  TCiPad
//
//  Created by Sri Ram on 1/4/16.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "QuantityPadViewController.h"
#import "AppControl.h"

@interface QuantityPadViewController ()

@end

@implementation QuantityPadViewController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    for(UIView *v in [self.view subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = 5.0;
            btn.layer.masksToBounds = YES;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnPressed:(UIButton *)sender {
    
    
    
    [self.delegate   quantityBtnPressed:sender];
    
    //NSLog(@"%ld", (long)sender.tag);
    
    
    
}
@end
