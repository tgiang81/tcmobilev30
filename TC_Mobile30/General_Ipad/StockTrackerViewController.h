//
//  StockTrackerViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 14/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockTrackerViewController : UIViewController{
        int favID;
    int arrayLimit;
    int test;
}
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UILabel *watchListLabel;
@property (nonatomic, weak) IBOutlet UILabel *filterByLabel;

@property (nonatomic, strong) IBOutlet UIButton *directionButton;
@property (nonatomic, strong) IBOutlet UIButton *lineButton;

@end
