//
//  QuoteScreenViewController.h
//  TCUniversal
//
//  Created by Scott Thoo on 11/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockPreViewController.h"
#import "MutualDelegate.h"
#import "CardCollectionViewCell.h"
#import "LanguageManager.h"
#import "SectorInfo.h"
#import "GeneralSelectorViewController.h"
#import "AppControl.h"

@interface QuoteScreenViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate,
UIGestureRecognizerDelegate, StockPreViewDelegate, GeneralSelectorDelegate,
UICollectionViewDelegate, UICollectionViewDataSource,
CardCollectionDelegate, NSURLSessionDelegate,NSURLSessionTaskDelegate>

@property (weak, nonatomic) id <QuoteScreenDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (nonatomic, strong) DynamicColumnSelectorViewController *columnPicker;
@property (nonatomic, strong) UIPopoverController *columnPickerPopover;
@property (nonatomic, strong) UILabel *keyboardLabel;
@property (weak, nonatomic) IBOutlet UIButton *hideWLbutton;
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic) IBOutlet UIView *mktSectorGroup;
@property (weak, nonatomic) IBOutlet UIButton *warrantButton;

@property (weak, nonatomic) IBOutlet UITableView *frontTableView;
@property (weak, nonatomic) IBOutlet UITableView *watchlistTable;
@property (weak, nonatomic) IBOutlet UIButton *showListBtn;
@property (weak, nonatomic) IBOutlet UIButton *hideListBtn;
@property (weak, nonatomic) IBOutlet UILabel *watchListTitle;
@property (weak, nonatomic) IBOutlet UIButton *stockTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDynamic1;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic2;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic3;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic4;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic5;
@property (weak, nonatomic) IBOutlet UILabel *Realtimeordelayedlabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

// table headers must be STRONG otherwise table releases it
@property (strong, nonatomic) IBOutlet UIView *compactHeader;
@property (strong, nonatomic) IBOutlet UIView *symbolHeader;
@property (strong, nonatomic) IBOutlet UIView *watchHeader;

@property (weak, nonatomic) IBOutlet UILabel *SortByTitle;
@property (weak, nonatomic) IBOutlet UIView *stockContainerView;
@property (weak, nonatomic) IBOutlet UIButton *noResultsMsgBtn;
@property (weak, nonatomic) IBOutlet UIButton *boardLotButton;
@property (weak, nonatomic) IBOutlet UIButton *sectorButton;

// PULL TO LOAD (PAGINATION) OBJECTS
@property (weak, nonatomic) IBOutlet UIView *customPullContainer;
@property (weak, nonatomic) IBOutlet UILabel *customPullLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *customPullActivity;
@property (weak, nonatomic) IBOutlet UIView *customPullView;
@property (weak, nonatomic) IBOutlet UIImageView *pullImg1;
@property (weak, nonatomic) IBOutlet UIImageView *pullImg2;

@property (weak, nonatomic) IBOutlet UIView *sortBtnContainer;
@property (weak, nonatomic) IBOutlet UIButton *stockAddBtn;
@property (weak, nonatomic) IBOutlet UIButton *stockEditBtn;
@property (weak, nonatomic) IBOutlet UIButton *watchListEditBtn;

@property (weak, nonatomic) IBOutlet UIButton *btnCompactView;
@property (nonatomic, assign) BOOL observingContentOffset;
@property (strong, nonatomic) IBOutletCollection(UITableView) NSArray *tableViewCollection;
@property (weak, nonatomic) IBOutlet UIView *traderContainer;

// COLLECTION VIEW (JAN 2017)
@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;
@property (nonatomic, strong) NSMutableDictionary *detailPageDict;

@property (nonatomic, strong) UIPickerView *boardLotPicker;
@property (nonatomic, strong) NSMutableArray *mktInfoArray, *sectorInfoArray;

@property (nonatomic, strong) NSArray *sortedWLArr;
@property (nonatomic, strong) NSString *stkToAddToWL;

@property (nonatomic, strong) NSArray *selectedArr;
@property (nonatomic, strong) NSMutableDictionary *filteredDict;
@property (nonatomic, strong) NSArray *Quotestockarray;
@property (nonatomic, strong) NSMutableDictionary *tempDict;
@property (nonatomic, strong) NSMutableArray *rankedByNameArr;
@property (nonatomic, strong) NSMutableArray *rankedByKeyArr;
@property (nonatomic, strong) NSMutableArray *bufferUpdateRow;
@property (nonatomic, strong) NSMutableArray *bufferUpdateStocks;
@property (nonatomic, strong) NSMutableArray *watchListArray;

- (IBAction)callSectorPicker:(UIButton*)sender;
- (IBAction)callBoardLotPicker:(UIButton*)sender;
- (IBAction)compactViewPressed:(UIButton *)sender;
- (IBAction)editStocks:(id)sender;
- (IBAction)editWatchList:(id)sender;
- (IBAction)addWatchList:(id)sender;
- (IBAction)addStock:(id)sender;
- (IBAction)toggleWatchList:(id)sender;
- (IBAction)toggleWarrant:(id)sender;





@end
