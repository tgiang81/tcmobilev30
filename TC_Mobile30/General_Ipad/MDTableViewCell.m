//
//  MDTableViewCell.m
//  TCiPad
//
//  Created by n2n on 11/11/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "MDTableViewCell.h"

@implementation MDTableViewCell

@synthesize mdIndex, buyBid, buyQty, buySplit, sellAsk, sellQty, sellSplit;
@synthesize mdBuyBgView, mdSellBgView, delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)askGroupClicked:(id)sender {
    [self.delegate mdPriceDidTapped:1 atCell:self];
}

- (IBAction)buyGroupClicked:(id)sender {
    [self.delegate mdPriceDidTapped:0 atCell:self];
}
@end
