//
//  IndicesTableViewCell.h
//  TCiPad
//
//  Created by Scott Thoo on 2/27/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndicesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrev;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrent;
@property (weak, nonatomic) IBOutlet UILabel *lblChg;
@property (weak, nonatomic) IBOutlet UILabel *lblChgPer;

@end
