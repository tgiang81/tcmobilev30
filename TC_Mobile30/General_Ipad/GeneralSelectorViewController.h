//
//  GeneralSelectorViewController.h
//  TCiPad
//
//  Created by n2n on 11/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectorTableViewCell.h"
#import "LanguageManager.h"
#import "ATPAuthenticate.h"
#import "SectorInfo.h"
#import "AppControl.h"

typedef enum GeneralSelectorType {
  TYPE_MARKET,
  TYPE_SECTOR,
  TYPE_WLIST
} GeneralSelectorType;

@protocol GeneralSelectorDelegate <NSObject>
-(void)selectorItemSelected:(NSIndexPath*)indexPath selectorType:(GeneralSelectorType)type;
@end

@interface GeneralSelectorViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, SelectorCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) NSMutableArray *selectorArray;
@property (weak, nonatomic) IBOutlet UITableView *selectorTable;
@property (nonatomic, weak) id<GeneralSelectorDelegate> delegate;
@property (nonatomic, assign) GeneralSelectorType selectorType;
@property (nonatomic, strong) NSString *selectedItem;
@property (nonatomic, assign) int expandedSectionIndex;


- (IBAction)cancelSelect:(id)sender;

@end
