//
//  TOCTableViewController.h
//  TCiPad
//
//  Created by n2n on 04/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"

@protocol TOCDelegate <NSObject>
@optional
-(void)tableRowDidClicked:(NSString*)segue;
@end

@interface TOCTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (nonatomic, assign) CGRect frameToUse;
@property (weak, nonatomic) id <TOCDelegate> tocDelegate;
- (IBAction)showDevTool:(id)sender;

@end
