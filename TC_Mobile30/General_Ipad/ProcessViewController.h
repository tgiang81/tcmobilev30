//
//  ProcessViewController.h
//  TCiPad
//
//  Created by n2n on 06/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DGActivityIndicatorView.h"
#import "AppControl.h"
#import "BaseVC.h"



@class ProcessViewController;
typedef NS_ENUM (NSInteger, TCAction){
    TCAction_Close = 0,
    TCAction_Restry
};
typedef void(^TCAlertControlRingAction)(ProcessViewController *alertControl, TCAction actionIndex);


@protocol ProcessViewDelegate <NSObject>
-(void)btnCloseDidClicked;
-(void)btnRetryDidClicked;
@end

@interface ProcessViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) NSDictionary *setupDict;
@property (weak, nonatomic) id<ProcessViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *boxView;

//For style block
@property (nonatomic, strong) TCAlertControlRingAction ringAction;


@property (nonatomic, strong) DGActivityIndicatorView *activityView;
- (IBAction)close:(id)sender;
- (IBAction)retry:(id)sender;
- (void)updateButton;

//For style block
+ (void)showPopupFromVC:(UIViewController *)vc withMessage:(NSString *)message completionBlock:(TCAlertControlRingAction)completionBlock;
+ (void)updateError:(NSString *)errorMsg isShowCloseButton:(BOOL)isCloseShow isShowRestryButton:(BOOL)isRestryShow;
+ (void)updateStateMessage:(NSString *)message;
//======== Dismiss ===============
+ (void)dismssWithCompletion:(void (^)(void))completion;
+ (void)dismiss;

@end
