//
//  IndicesViewController.h
//  TCiPad
//
//  Created by Scott Thoo on 2/27/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"

@interface IndicesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,
UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *aceView;
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UITableView *indiceTableView;
@property (weak, nonatomic) IBOutlet UIWebView *movementAndChartWebView;
@property (weak, nonatomic) IBOutlet UITableView *timeSalesTableView;
@property (weak, nonatomic) IBOutlet UITableView *mainMarketTableView;
@property (weak, nonatomic) IBOutlet UITableView *strcwarrTableView;
@property (weak, nonatomic) IBOutlet UITableView *aceMarketTableView;
@property (weak, nonatomic) IBOutlet UILabel *MainMarketVoltotal;
@property (weak, nonatomic) IBOutlet UILabel *MMupTotal;
@property (weak, nonatomic) IBOutlet UILabel *MMDownTotal;
@property (weak, nonatomic) IBOutlet UILabel *MMUnchagTotal;
@property (weak, nonatomic) IBOutlet UILabel *MMUntradTotal;
@property (weak, nonatomic) IBOutlet UILabel *MMTotalTotal;
@property (weak, nonatomic) IBOutlet UILabel *wartotalvol;
@property (weak, nonatomic) IBOutlet UILabel *wartotalup;
@property (weak, nonatomic) IBOutlet UILabel *wartotaldown;
@property (weak, nonatomic) IBOutlet UILabel *wartotalunchg;
@property (weak, nonatomic) IBOutlet UILabel *wartotaluntrd;
@property (weak, nonatomic) IBOutlet UILabel *wartotaltotal;
@property (weak, nonatomic) IBOutlet UILabel *acetotalvol;
@property (weak, nonatomic) IBOutlet UILabel *acetotalup;
@property (weak, nonatomic) IBOutlet UILabel *acetotaldown;
@property (weak, nonatomic) IBOutlet UILabel *acetotalunchg;
@property (weak, nonatomic) IBOutlet UILabel *acetotaluntrd;
@property (weak, nonatomic) IBOutlet UILabel *acetotaltotal;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UILabel *mainMarketTimeStamp;
@property (weak, nonatomic) IBOutlet UILabel *warrantMarketTimeSTamp;
@property (weak, nonatomic) IBOutlet UILabel *aceMarketTimeStamp;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityAnimation;
@property (weak, nonatomic) IBOutlet UIImageView *imgChart;
@property (weak, nonatomic) IBOutlet UILabel *chartErrorMsg;
@property (weak, nonatomic) IBOutlet UILabel *chartTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeSalesTimeStamp;
@property (weak, nonatomic) IBOutlet UILabel *aceTotalLabel;
@property (nonatomic, strong) NSMutableArray *sectorInfoArray;
@property (weak, nonatomic) IBOutlet UILabel *aceMarketLabel;
@property (weak, nonatomic) IBOutlet UILabel *aceSectorLabel;
@property (weak, nonatomic) IBOutlet UILabel *aceTotalMarketLabel;
@property (weak, nonatomic) IBOutlet UILabel *aceVolLabel;
@property (weak, nonatomic) IBOutlet UIImageView *aceUpImage;
@property (weak, nonatomic) IBOutlet UIImageView *aceDownImage;
@property (weak, nonatomic) IBOutlet UIImageView *aceNoChangeImage;
@property (weak, nonatomic) IBOutlet UIImageView *aceEqualImage;
@property (weak, nonatomic) IBOutlet UIButton *advertisingBanner;


- (IBAction)refreshButtonPressed:(id)sender;
- (IBAction)clickAdvertisingBanner:(id)sender;

@end
