//
//  SearchViewController.m
//  TCiPad
//
//  Created by n2n on 23/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchTableViewCell.h"
#import "VertxConnectionManager.h"
#import "ATPAuthenticate.h"
#import "QCConstants.h"
#import "UserPrefConstants.h"

@interface SearchViewController () {

    CGSize scrSize;
    CGFloat bufferPreviewX;
    CGPoint panelViewOriPos;
}
@end

@implementation SearchViewController

#define K_AUTOSEARCH_TIME 2.0




#pragma mark - slide gesture



-(void)searchPanelDidPanned:(UIPanGestureRecognizer*)recognizer {
    
    UIView *view = recognizer.view;
    
    // CGPoint velocity = [recognizer velocityInView:view.superview];
    
    // if (velocity.x>0) {
    
    CGPoint translation = [recognizer translationInView:view.superview];
    
    if (translation.x>=50) {
        bufferPreviewX = translation.x-49;
    } else {
        bufferPreviewX = 0;
    }
    
    if (bufferPreviewX>0) {
        
        CGFloat distanceToDismiss = 150;
        
        if(recognizer.state == UIGestureRecognizerStateBegan){
            
            
            
        } else if(recognizer.state == UIGestureRecognizerStateEnded){
            
            if (_panelView.center.x<=panelViewOriPos.x+distanceToDismiss) {
                // return back
                [UIView animateWithDuration:0.2 animations:^{
                    _panelView.center = panelViewOriPos;
                }completion:^(BOOL finished) {
                    
                }];
            }
        } else {
            // swping
            _panelView.center = CGPointMake(panelViewOriPos.x+bufferPreviewX, panelViewOriPos.y);
            
            if (_panelView.center.x>panelViewOriPos.x+distanceToDismiss) {
                [view removeGestureRecognizer:recognizer];
                [self cancelSearch:nil];
            }
            
        }
        
    } else {
        
        [UIView animateWithDuration:0.2 animations:^{
            _panelView.center = panelViewOriPos;
        }completion:^(BOOL finished) {
            
        }];
        
    }
    
}


#pragma mark - view lifecycle deleegates


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // send speech window off screen
    _speechWindow.center = CGPointMake(self.view.frame.size.width*2.0, self.view.frame.size.height/2.5);
    
    _speechWindow.layer.cornerRadius = 10.0;
    _speechWindow.layer.masksToBounds = NO;
    
    _speechWindow.layer.shadowColor = [UIColor blackColor].CGColor;
    _speechWindow.layer.shadowRadius = 10;
    _speechWindow.layer.shadowOpacity = 1.0;
    _speechWindow.layer.shadowOffset = CGSizeMake(2, 2);
    
    _stockListView.frame = CGRectMake(_stockListView.frame.origin.x, _stockListView.frame.origin.y,
                                      scrSize.width-_panelView.frame.size.width, _stockListView.frame.size.height);
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doneVertxSearch" object:nil];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchDataDictReceived:)  name:@"doneVertxSearch" object:nil];

    [UIView animateWithDuration:0.2 animations:^(void) {
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^(void) {
            _panelView.frame = CGRectMake(scrSize.width-_panelView.frame.size.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);

        } completion:^(BOOL finished) {
            panelViewOriPos = _panelView.center;
        }];
        
        // if add stock mode, bring bottom panel
        if (_panelMode==PANEL_MODE_ADDSTOCK) {
            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^(void) {
                _stockListView.frame = CGRectMake(0, scrSize.height-_stockListView.frame.size.height, _stockListView.frame.size.width, _stockListView.frame.size.height);
                
            } completion:^(BOOL finished) {
                
            }];
        }
        
    }];
}


-(void)MicrophonePermission {
    
    // Microphone permission
    AVAudioSession *session = [AVAudioSession sharedInstance];
    AVAudioSessionRecordPermission sessionRecordPermission = [session recordPermission];
    switch (sessionRecordPermission) {
        case AVAudioSessionRecordPermissionUndetermined:{
            [session requestRecordPermission:^(BOOL granted) {
                
                if (granted) {
                    //NSLog(@"Initiate Speech");

                    [self initiateSpeechRecognizer];
                } else {
                    // cant do it
                }
                
            }];
        }break;
        case AVAudioSessionRecordPermissionDenied:
            //NSLog(@"Mic permission denied. Call method for denied stuff.");
            break;
        case AVAudioSessionRecordPermissionGranted:{
            
                //NSLog(@"Initiate Speech");

            [self initiateSpeechRecognizer];

        }break;
        default:
            break;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];

    self.speechOpened = NO; // default speech is not opened
    self.speechAvail = NO;
    
    [_activateButton setHidden:YES];
    
    [_recognizedTextLabel setHidden:YES];
    _resultLabel.text = @"";
    
    self.stocksArray = [[NSMutableArray alloc] init];
    
    // Speech recognition permission
    
    [_backButton setHidden:YES];
    _backButton.layer.cornerRadius = kButtonRadius * _backButton.frame.size.height;
    _backButton.layer.masksToBounds = YES;
    
    _activateButton.layer.cornerRadius = kButtonRadius * _activateButton.frame.size.height;
    _activateButton.layer.masksToBounds = YES;
    
    _closeButton.layer.cornerRadius = _closeButton.frame.size.height/2.0;
    _closeButton.layer.masksToBounds = YES;
    
    
    [_searchTable setBackgroundView:_pullMoreView];
   
    _searchBar.placeholder = [LanguageManager stringForKey:@"Enter Search Keywords"];
    
    if ([SFSpeechRecognizer class]) {
        _speechAvail = YES;
        [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
            
            switch (status) {
                case SFSpeechRecognizerAuthorizationStatusDenied:
                    
                    break;
                case SFSpeechRecognizerAuthorizationStatusRestricted:
                    
                    break;
                case SFSpeechRecognizerAuthorizationStatusNotDetermined:
                    
                    break;
                case SFSpeechRecognizerAuthorizationStatusAuthorized:{
                    
                    [self MicrophonePermission];
                    //NSLog(@"Speech Granted");
                }break;
                default:
                    break;
            }
            
        }];
        
    } else {
        
        _speechAvail = NO;
    }
    
    bufferPreviewX = 0;

    switch (_panelMode) {
        case PANEL_MODE_SEARCH: {
            _panelTitle.text = [LanguageManager stringForKey:@"Search Stock"];
        } break;
            
        case PANEL_MODE_ADDSTOCK: {
            _panelTitle.text = [LanguageManager stringForKey:@"Find Stock to Add"];
        } break;
            
        default:
            break;
    }
    
    _tableDataArr = [[NSMutableArray alloc] init];
    scrSize = [[UIScreen mainScreen] bounds].size;
    
    _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
    _stockListView.frame = CGRectMake(0, scrSize.height, _stockListView.frame.size.width, _stockListView.frame.size.height);
    
    _panelView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _panelView.layer.shadowRadius = 10;
    _panelView.layer.shadowOpacity = 1.0;
    _panelView.layer.shadowOffset = CGSizeMake(-10, 0);
    
    
    _stockListView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _stockListView.layer.shadowRadius = 10;
    _stockListView.layer.shadowOpacity = 1.0;
    _stockListView.layer.shadowOffset = CGSizeMake(-10, 0);

    UIPanGestureRecognizer *panGesturePanel = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(searchPanelDidPanned:)];
    panGesturePanel.delegate = self;
    [_panelView addGestureRecognizer:panGesturePanel];
    
    // tap outside of news view, dismiss it
//    UITapGestureRecognizer *touch = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
//    touch.numberOfTouchesRequired = 1;
//    [self.view addGestureRecognizer:touch];
//    [touch setDelegate:self];
    
    
    [self.waveFormView setPrimaryWaveLineWidth:2.0f];
    [self.waveFormView setSecondaryWaveLineWidth:1.0];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelSearch:) name:@"dismissAllModalViews" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    
    CGPoint p = [gestureRecognizer locationInView:self.view];
    if (CGRectContainsPoint([_panelView frame], p)) {
        
        
    } else {
        [self cancelSearch:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isDescendantOfView:_panelView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            return YES;
        } else {
            return NO;
        }
    }
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (IBAction)cancelSearch:(id)sender {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    
    
    if (_panelMode==PANEL_MODE_ADDSTOCK) {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^(void) {
            _stockListView.frame = CGRectMake(0, scrSize.height, _stockListView.frame.size.width, _stockListView.frame.size.height);
            
        } completion:^(BOOL finished) {
            
        }];
    }
    
    [UIView animateWithDuration:0.2 animations:^(void) {
        
        _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
        
    }completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.2 animations:^(void) {
           
            [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.0]];

        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:NO completion:nil];

        }];
        
    }];
    
    if (_speechOpened) [self closeSpeechWindow:nil];
    
    
}

#pragma mark - Speech Recognition Codes


- (IBAction)openSpeechWindow:(id)sender {
    
    if (_speechAvail) {
    
        if (!_speechOpened) {
            _speechOpened = YES;
            
            [UIView animateWithDuration:0.8 animations:^(void) {
                
                _speechWindow.center = CGPointMake((self.view.frame.size.width-_panelView.frame.size.width)/2.0,
                                                   self.view.frame.size.height/2.5);
                
            }completion:^(BOOL finished) {
                
                [self playSound:1];
                [self performSelector:@selector(startRecording) withObject:nil afterDelay:0.8];
                
                
                
            }];
        }
        
    } else {
        [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"SpeechRecognitionNA"]
                                     inController:self withTitle:[LanguageManager stringForKey:@"Info"]];
    }
}

- (IBAction)openSpeechWindow2:(id)sender {
    [self openSpeechWindow:nil];
}

- (IBAction)closeSpeechWindow:(id)sender {
    
    _speechOpened = NO;
    
    [UIView animateWithDuration:0.4 animations:^(void) {
        
        _speechWindow.center = CGPointMake(self.view.frame.size.width*2.0, self.view.frame.size.height/2.5);
        
    }completion:^(BOOL finished) {
        
        [self stopRecording];
        
    }];
}

- (IBAction)activateSpeech:(id)sender {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    
    [_activateButton setHidden:YES];
    [self playSound:1];
    [self performSelector:@selector(startRecording) withObject:nil afterDelay:0.8];
    
}








- (IBAction)goBack:(id)sender {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    
    [UserPrefConstants singleton].searchPage -= 1;
    [self executeSearch];
    [_backButton setHidden:YES];
}



-(void)initiateSpeechRecognizer {
    
    
        _speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:[LanguageManager stringForKey:@"SPEECH_LOCALE"]]];
        _speechRecognizer.delegate = self;
        
        _audioEngine = [[AVAudioEngine alloc] init];
        

        
     
}



-(void)executeSpeechSearch {
 
    [self stopRecording];
    _searchBar.text = _recognizedTextLabel.text;
    [self searchBarSearchButtonClicked:_searchBar];
}


-(void)loopDeleteWord {
    NSString *editWord = [_wordsArr objectAtIndex:_idxToEdit];
    if ([editWord length]>0) {
        editWord = [editWord substringToIndex:[editWord length]-1];
        [_wordsArr replaceObjectAtIndex:_idxToEdit withObject:editWord];
    } else {
        // select previous word that is not empty
        for (int j=_idxToEdit; j>=0; j--) {
            NSString *eachWord2 = [_wordsArr objectAtIndex:j];
            if ([eachWord2 length]>0) {
                _idxToEdit = j;
                [self loopDeleteWord];
                break;
            }
        }
    }
}


-(void)playSound:(int)mode {
    
    NSString *soundPath;
    if (mode==1) {
        soundPath  = [NSString stringWithFormat:@"%@/start.wav", [[NSBundle mainBundle] resourcePath]];
    } else {
        soundPath  = [NSString stringWithFormat:@"%@/stop.wav", [[NSBundle mainBundle] resourcePath]];
    }
    
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    if (_sndPlayer!=nil) _sndPlayer = nil;
    _sndPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:nil];
    _sndPlayer.volume = 1.0;
    [_sndPlayer prepareToPlay];
    [_sndPlayer play];
}


-(void)stopRecording {
    
    
    
    // UI START
    [_searchBar setHidden:NO];
    [_recognizedTextLabel setHidden:YES];
    [_activateButton setHidden:NO];
    [_waveFormView setHidden:YES];
    _speechTitle.text = [LanguageManager stringForKey:@"Speech Recognition"];
    _speechTitle.textColor = [UIColor whiteColor];
    [_speechTitle.layer removeAllAnimations];
    [_speechTitle setAlpha:1.0];
    
    // ENGINE
    AVAudioInputNode *inputNode = [_audioEngine inputNode];
    [_audioEngine stop];
    [inputNode removeTapOnBus:0];
    [self.recognitionTask cancel];
    
    self.recognitionRquest = nil;
    self.recognitionTask = nil;
    _dictateBtn.enabled = YES;
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [session setMode:AVAudioSessionModeDefault error:nil];
   
    [self playSound:2];
    
}

-(void)startRecording {
    
    
    // UI START
    [_searchBar setHidden:YES];
    _recognizedTextLabel.text = @"";
    [_recognizedTextLabel setHidden:NO];
    [_activateButton setHidden:YES];
    [_waveFormView setHidden:NO];
    _speechTitle.text = [LanguageManager stringForKey:@"Speech Recognition Active"];
    _speechTitle.textColor = [UIColor cyanColor];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionRepeat|UIViewAnimationOptionAutoreverse animations:^(void){
        [_speechTitle setAlpha:0.2];
    }completion:nil];
    
    // ENGINE START
    if (_recognitionTask != nil) {
        [_recognitionTask cancel];
        _recognitionTask = nil;
    }
    
    
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    @try {
        NSError *error;
        [session setCategory:AVAudioSessionCategoryRecord error:&error];
        [session setMode:AVAudioSessionModeMeasurement error:&error];
    
        [session setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:&error];
    } @catch (NSException *exception) {
        NSLog(@"audioSession failed");
    } @finally {
        ;
    }
    
    
    self.recognitionRquest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    
    // Set Hint: SFSpeechRcognitionTaskHintConfirmation "yes, no, maybe"
    //           SFSpeech​Recognition​Task​Hint​Dictation general dictation
    //           SFSpeech​Recognition​Task​Hint​Search search actions
    //           SFSpeech​Recognition​Task​Hint​Unspecified unspecified dictation
    
    self.recognitionRquest.taskHint = SFSpeechRecognitionTaskHintSearch;
    AVAudioInputNode *inputNode = [_audioEngine inputNode];
    
    if (inputNode==nil) {
        NSLog(@"audioEngine no input mode");
    
    }
    
    _recognitionRquest.shouldReportPartialResults = YES;
    
    // This runs in a loop and continues to detect speeches until timeout or when user end it, isFinal=YES;
    _recognitionTask = [_speechRecognizer recognitionTaskWithRequest:_recognitionRquest
                                                       resultHandler:^(SFSpeechRecognitionResult *result, NSError *error)
                        {
                            
                            if (result != nil)
                            {
                                
                                NSString *latestTranscribe = [[result bestTranscription].formattedString lowercaseString];
                                BOOL executeSearch = NO;
                                self.wordsArr = [[NSMutableArray alloc] initWithArray:[latestTranscribe componentsSeparatedByString:@" "]  copyItems:YES];
                                self.idxToEdit = -1;
                                
                                // process everything and show
                                // this is difficult. O_o
                                // probably has bug :D
                                
                                // NSLog(@"wordsArr b4: %@", _wordsArr);
                                
                                for (int i=0; i<[_wordsArr count]; i++) {
                                    
                                    NSString *eachWord = [_wordsArr objectAtIndex:i];
                                    
                                    if ([eachWord isEqualToString:@"delete"]) {
                                        if (_idxToEdit>=0) {
                                            [_wordsArr replaceObjectAtIndex:i withObject:@""];
                                            [self loopDeleteWord];
                                            
                                        }
                                    } else if ([eachWord isEqualToString:@"clear"]) {
                                        if (_idxToEdit>=0) {
                                            [_wordsArr replaceObjectAtIndex:i withObject:@""];
                                            // clear all words before this
                                            for (int j=0; j<=_idxToEdit;j++)
                                            [_wordsArr replaceObjectAtIndex:j withObject:@""];
                                        }
                                    } else if ([eachWord isEqualToString:@"search"]) {
                                        [_wordsArr replaceObjectAtIndex:i withObject:@""];
                                        executeSearch = YES;
                                    } else {
                                        // a search word was spoken
                                        _idxToEdit = i;
                                        //NSLog(@"spoken");
                                    }
                                }
                                
                               // NSLog(@"wordsArr %@",_wordsArr);
                                __block NSString *filteredResult = @"";
                                // join the words
                                [_wordsArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                    filteredResult = [NSString stringWithFormat:@"%@%@",filteredResult,obj];
                                }];
                                
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     _recognizedTextLabel.text =filteredResult;
                                 });
                                
                                // if timeout?
                                if (result.isFinal) {
                                    executeSearch = YES;
                                    
                                }
                                
                                if ([filteredResult length]>0) {
                                   // NSLog(@"start perform");
                                    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                                             selector:@selector(executeSpeechSearch) object:nil];
                                    [self performSelector:@selector(executeSpeechSearch)
                                               withObject:nil afterDelay:K_AUTOSEARCH_TIME];
                                } else {
                                   // NSLog(@"cancel perform");
                                    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                                             selector:@selector(executeSpeechSearch) object:nil];
                                    
                                }
                                
                                
                                if (executeSearch) {
                                    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                                             selector:@selector(executeSpeechSearch) object:nil];
                                    if ([_recognizedTextLabel.text length]>0)
                                        [self executeSpeechSearch];
                                    return;
                                }
                                
                            }
                            else {
                                 //NSLog(@"result nil: %@",error.description);
                                [NSObject cancelPreviousPerformRequestsWithTarget:self
                                                                         selector:@selector(executeSpeechSearch) object:nil];
                                
                                    [self stopRecording];
                                   // [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"Speech Recognition server is not responding. Please try again later."]
                                    //                             inController:self];
                                
                            }
                        }];
    
    /*
    AVAudioFormat *recordingFormat = [inputNode outputFormatForBus:0];
    [inputNode installTapOnBus:0 bufferSize:1024 format:recordingFormat block:^(AVAudioPCMBuffer *buffer, AVAudioTime* when) {
        
        // pass audio to speech recognizer
        [_recognitionRquest appendAudioPCMBuffer:buffer];
    
        
        // pass audio to waveform view
        [buffer setFrameLength:1024];
        UInt32 inNumberFrames = buffer.frameLength;
        
        CGFloat LEVEL_LOWPASS_TRIG = 0.1;
        
        if(buffer.format.channelCount>0)
        {
            
            Float32* samples = (Float32*)buffer.floatChannelData[0];
            Float32 avgValue = 0;
            
            vDSP_meamgv((Float32*)samples, 1, &avgValue, inNumberFrames);
            self.averagePowerForChannel0 = (LEVEL_LOWPASS_TRIG*((avgValue==0)?-100:20.0*log10f(avgValue)))
                                            + ((1-LEVEL_LOWPASS_TRIG)*self.averagePowerForChannel0) ;
 
            //NSLog(@"self.averagePowerForChannel0 %f", self.averagePowerForChannel0);
            
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self.waveFormView updateWithLevel:20*[self _normalizedPowerLevelFromDecibels:self.averagePowerForChannel0]];
             });
        }
        
    }];
    */
    [_audioEngine prepare];
    
    @try {
        NSError *error;
        [_audioEngine startAndReturnError:&error];
    } @catch (NSException *exception) {
        NSLog(@"AudioEngine can't start");
    } @finally {
        ;
    }
    
}


- (CGFloat)_normalizedPowerLevelFromDecibels:(CGFloat)decibels
{
    if (decibels < -60.0f || decibels == 0.0f) {
        return 0.0f;
    }
    
    return powf((powf(10.0f, 0.05f * decibels) - powf(10.0f, 0.05f * -60.0f)) * (1.0f / (1.0f - powf(10.0f, 0.05f * -60.0f))), 1.0f / 2.0f);
}

//-(NSString *)modifyResult:(NSString*)str {
//    NSString *res = str;
//    
//    if ([str isEqualToString:@"and two and"]) res = @"N2N";
//    if ([str isEqualToString:@"end two end"]) res = @"N2N";
//    if ([str isEqualToString:@"and two end"]) res = @"N2N";
//    if ([str isEqualToString:@"end two and"]) res = @"N2N";
//    
//    if ([str isEqualToString:@"and to and"]) res = @"N2N";
//    if ([str isEqualToString:@"end to end"]) res = @"N2N";
//    if ([str isEqualToString:@"and to end"]) res = @"N2N";
//    if ([str isEqualToString:@"end to and"]) res = @"N2N";
//    
//    return res;
//}



#pragma mark - Speech Recognition Delegates

-(void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available {
    if (available) {
        _dictateBtn.enabled = YES;
    } else {
        _dictateBtn.enabled = NO;
    }
}

#pragma mark - Receive notification from Vertx 
- (void)searchDataDictReceived:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [_backButton setHidden:YES];
        
        _searchTable.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
        _searchTable.contentOffset = CGPointMake(0, 0);
        
        [_pullMoreLabel setHidden:NO];
        [_pullImg1 setHidden:NO];[_pullImg2 setHidden:NO];
        [_pullActivity stopAnimating];
        if ([UserPrefConstants singleton].searchPage==0) _pullMoreContainer.hidden = YES;
        else _pullMoreContainer.hidden = NO;
        
        [_searchBar resignFirstResponder];
        NSDictionary * searchResultDict = [notification.userInfo copy];
        // sort using alphabetical
        
        NSLog(@"searchResult: %@",searchResultDict);
        
        //NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_33_S_STOCK_CODE ascending:NO];
        
       // NSArray * sortedArray = [[searchResultDict allValues] sortedArrayUsingDescriptors:@[sortDescriptor]];
          //NSArray * sortedArray = [searchResultDict allValues];
         _tableDataArr = [NSMutableArray arrayWithArray:[searchResultDict objectForKey:FID_33_S_STOCK_CODE]];
     
        
        if ([_tableDataArr count]>0) {
            
            long tot = [_tableDataArr count]-1;
            
            long recNum = 1+([UserPrefConstants singleton].searchPage)*20;
            NSString *fromRecStr = [NSString stringWithFormat:@"%ld",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%ld",recNum+tot];
            _resultLabel.text = [LanguageManager stringForKey:@"Showing Results %@ - %@"
                                             withPlaceholders:@{@"%fromRes%":fromRecStr, @"%toRes%":toRecStr}];
        } else {
            
            if ([UserPrefConstants singleton].searchPage>0)
            {
                
                _resultLabel.text = [LanguageManager stringForKey:@"No more items"];
                [_backButton setHidden:NO];
                
            } else {
                
                _resultLabel.text = @"";
            }
            
        }
        
        [_searchTable reloadData];
    });
    
}

#pragma mark - Search Delegates

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    // show activity
    [_tableDataArr removeAllObjects];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_searchTable reloadData];
    });
    
    [UserPrefConstants singleton].searchPage = 0;
  
    [[VertxConnectionManager singleton] vertxSearch:_searchBar.text];

    [[ATPAuthenticate singleton] timeoutActivity];
    
}

// to load other page
-(void)executeSearch {
    [[VertxConnectionManager singleton] vertxSearch:_searchBar.text];
    [[ATPAuthenticate singleton] timeoutActivity];
}

#pragma mark - ScrollView Delegate (of tableview)

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    
    if (_canLoadData!=kDataLoadCannot) {
        // do loading here
        // set offset, hide label, show activity
        
        
        if (_canLoadData==kDataLoadNext) {
            
            _searchTable.contentInset = UIEdgeInsetsMake(0, 0, 60.0, 0);
            [_pullMoreLabel setHidden:YES];
            [_pullImg1 setHidden:YES];[_pullImg2 setHidden:YES];
            [_pullActivity startAnimating];
            [UserPrefConstants singleton].searchPage += 1;
            
            [self performSelector:@selector(executeSearch) withObject:nil afterDelay:1.0];
        }
        if (_canLoadData==kDataLoadPrev) {

            _searchTable.contentInset = UIEdgeInsetsMake(60, 0, 0.0, 0);
            [_pullMoreLabel setHidden:YES];
            [_pullImg1 setHidden:YES];[_pullImg2 setHidden:YES];
            [_pullActivity startAnimating];
            [UserPrefConstants singleton].searchPage -= 1;
            
            [self performSelector:@selector(executeSearch) withObject:nil afterDelay:1.0];
        }
        _canLoadData = kDataLoadCannot;
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat scrollViewHeight;
    CGFloat scrollContentSizeHeight;
    CGFloat bottomInset;
    CGFloat scrollViewBottomOffset;
    CGFloat currentYOffset;
    CGFloat pullContainerXPos;
    

    scrollViewHeight = _searchTable.bounds.size.height;
    scrollContentSizeHeight = _searchTable.contentSize.height;
    bottomInset = _searchTable.contentInset.bottom;
    scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
    currentYOffset = _searchTable.contentOffset.y;
    pullContainerXPos = _searchTable.frame.size.width/2.0;
    
    
    [_pullMoreContainer setAlpha:0.0];
    
    // AT THE TOP
    if(currentYOffset < 0) {
        
        _pullImg1.image = _pullImg2.image = [UIImage imageNamed:@"pulldn"];
        _pullMoreContainer.center = CGPointMake(pullContainerXPos, _pullMoreContainer.frame.size.height/2.0);
        
        CGFloat draggedDist = - currentYOffset;
        if (draggedDist>=kPaginationTrigger) draggedDist = kPaginationTrigger;
        
        if (draggedDist>=kPaginationTrigger) {
            [_pullMoreContainer setAlpha:1.0];
            _pullImg1.transform = CGAffineTransformMakeRotation(M_PI);
            _pullImg2.transform = CGAffineTransformMakeRotation(-M_PI);
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Release to Load"];
            if ([UserPrefConstants singleton].searchPage>=1) _canLoadData = kDataLoadPrev;
        } else {
            if ([_pullActivity isAnimating]) {
                [_pullMoreContainer setAlpha:1.0];
            } else {
                [_pullMoreContainer setAlpha:draggedDist/kPaginationTrigger];
            }
            _pullImg1.transform = CGAffineTransformIdentity;
            _pullImg2.transform = CGAffineTransformIdentity;
            long recNum = 1+([UserPrefConstants singleton].searchPage-1)*20;
            
            NSString *fromRecStr = [NSString stringWithFormat:@"%ld",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%ld",recNum+19];
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Pull Down For Results %d-%d" withPlaceholders:@{@"%fromRecord%":fromRecStr,@"%toRecord%":toRecStr}];
            _canLoadData = kDataLoadCannot;
            if ([UserPrefConstants singleton].searchPage>=1) {
                [_pullMoreContainer setHidden:NO];
            } else {
                [_pullMoreContainer setHidden:YES];
            }
        }
        
    }
    

    
    // AT THE BOTTOM
    if(currentYOffset > scrollViewBottomOffset) {
        
        _pullImg1.image = _pullImg2.image = [UIImage imageNamed:@"pull"];
        _pullMoreContainer.center = CGPointMake(pullContainerXPos,
                                                  _searchTable.frame.size.height-_pullMoreContainer.frame.size.height/2.0);
        
        CGFloat draggedDist = currentYOffset - scrollViewBottomOffset;
        if (draggedDist>=kPaginationTrigger) draggedDist = kPaginationTrigger;
        
        
        
        if (draggedDist>=kPaginationTrigger) {
            [_pullMoreContainer setAlpha:1.0];
            _pullImg1.transform = CGAffineTransformMakeRotation(M_PI);
            _pullImg2.transform = CGAffineTransformMakeRotation(-M_PI);
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Release to Load"];
            if ([_tableDataArr count]>=20) {
                _canLoadData = kDataLoadNext;
                _pullMoreContainer.hidden = NO;
            } else {
                _pullMoreContainer.hidden = YES;
            }
        } else {
            if ([_pullActivity isAnimating]) {
                [_pullMoreContainer setAlpha:1.0];
            } else {
                [_pullMoreContainer setAlpha:draggedDist/kPaginationTrigger];
            }
            _pullImg1.transform = CGAffineTransformIdentity;
            _pullImg2.transform = CGAffineTransformIdentity;
            long recNum = 1+([UserPrefConstants singleton].searchPage+1)*20;
            NSString *fromRecStr = [NSString stringWithFormat:@"%ld",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%ld",recNum+19];
            _pullMoreLabel.text = [LanguageManager stringForKey:@"Pull Up For Results %d-%d" withPlaceholders:@{@"%fromRecord%":fromRecStr,@"%toRecord%":toRecStr}];
            _canLoadData = kDataLoadCannot;
            if ([_tableDataArr count]<20) {
                [_pullMoreContainer setHidden:YES];
            } else {
                [_pullMoreContainer setHidden:NO];
            }
        }
        
    }
    
    
    
}


#pragma mark - TableView Delegates


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 63.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *eachDict = [_tableDataArr objectAtIndex:indexPath.row];
    NSString *_stkCode = [eachDict objectForKey:FID_33_S_STOCK_CODE];
    NSString *stkName = [eachDict objectForKey:FID_38_S_STOCK_NAME];
    
    // subscribe selected stock only.
    [[[QCData singleton] qcFeedDataDict] setObject:eachDict forKey:_stkCode];
   // NSLog(@"didSelectRowAtIndexPath %@",[[QCData singleton] qcFeedDataDict]);
    
    [[VertxConnectionManager singleton] vertxSubscribeAllInQcFeed];
    
    if (_panelMode==PANEL_MODE_SEARCH) {
        [UserPrefConstants singleton].userSelectingStockCode = _stkCode;
        
        [UIView animateWithDuration:0.2 animations:^(void) {
            
            _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
            
        }completion:^(BOOL finished) {
            
            [self closeSpeechWindow:nil];
            
            [UIView animateWithDuration:0.2 animations:^(void) {
                
                [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.0]];
                
            } completion:^(BOOL finished) {
                [self dismissViewControllerAnimated:NO completion:nil];
                NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                [notificationData setObject:_stkCode forKey:@"stkCode"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
            }];
            
        }];
        
       
    }
    
     if (_panelMode==PANEL_MODE_ADDSTOCK) {
         
         __block BOOL canAdd = YES;
         [_stocksArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
             WatchListStk *wlObj = (WatchListStk*)obj;
             if ([wlObj.stkCode isEqualToString:_stkCode]) {
                 canAdd = NO;
             }
         }];
         
         if (canAdd) {
             
             WatchListStk *stkObj = [[WatchListStk alloc] init];
             stkObj.stkCode = _stkCode;
             stkObj.stkName = stkName;
             
             [_stocksArray addObject:stkObj];
             [_stockCollectionView reloadData];
         }
         

      
     }
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    if ([_tableDataArr count]<=0) {
        
        if ([UserPrefConstants singleton].searchPage>0) {
            _searchTable.backgroundView = _pullMoreView;
            _searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;

        } else {
        
            // Display a message when the table is empty
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            
            messageLabel.text = [LanguageManager stringForKey:@"No Match."];
            messageLabel.textColor = [UIColor whiteColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            [messageLabel sizeToFit];
            
            _searchTable.backgroundView = messageLabel;
            _searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 0;
        }
    } else {
        _searchTable.backgroundView = _pullMoreView;
        _searchTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_tableDataArr count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"searchCellID";
    
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell==nil) {
        cell = [[SearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        
    }
    
    NSDictionary *eachDict = [_tableDataArr objectAtIndex:indexPath.row];
    
    cell.searchResultLabel.text = [eachDict objectForKey:FID_33_S_STOCK_CODE];
    cell.searchResultLabel2.text = [eachDict objectForKey:FID_39_S_COMPANY];
    // load data
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 ==0) {
        cell.backgroundColor = [UIColor colorWithWhite:0.1 alpha:1.0];
        
    } else {
        cell.backgroundColor = [UIColor colorWithWhite:0.13 alpha:1.0];
    }

}


#pragma mark - CollectionView Delegates (selected stocks)


- (IBAction)addToWatchlist:(id)sender {
    
    if ([_stocksArray count]<=0) {
        
        return;
    }
    
    if (_speechOpened) {
        [self closeSpeechWindow:nil];
    }
    
    
    [UIView animateWithDuration:0.2 animations:^(void) {
        
        _stockListView.frame = CGRectMake(0, scrSize.height, _stockListView.frame.size.width, _stockListView.frame.size.height);
        
    }completion:nil];
    
    [UIView animateWithDuration:0.2 animations:^(void) {
        
        _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
        
    }completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.2 animations:^(void) {
            
            [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.0]];
            
        } completion:^(BOOL finished) {
            
            // process stocksArray
            
            NSMutableArray *stkCodeArr = [[NSMutableArray alloc] init];
            for (int i=0; i<[_stocksArray count]; i++) {
                WatchListStk *obj = [_stocksArray objectAtIndex:i];
                [stkCodeArr addObject:obj.stkCode];
            }
            
            NSString *stocksCommaSeparated = [stkCodeArr componentsJoinedByString:@","];
//            NSLog(@"stocksCommaSeparated %@", stocksCommaSeparated);
            

            [self dismissViewControllerAnimated:NO completion:nil];
            NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
            [notificationData setObject:stocksCommaSeparated forKey:@"stkCode"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"watchListAddThisStock" object:self userInfo:notificationData];
        }];
        
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    WatchListStk *obj = [_stocksArray objectAtIndex:indexPath.row];
    NSString *str = obj.stkName;
    UIFont *myFont = [UIFont systemFontOfSize:15.0];
    CGSize textSize = [str sizeWithAttributes:@{NSFontAttributeName:myFont}];
    CGSize addPaddingSize = CGSizeMake(textSize.width+50, textSize.height+50);
    
    return addPaddingSize;
}


-(void)removeButtonDidClicked:(NSIndexPath *)indexPath {
    
    [_stocksArray removeObjectAtIndex:indexPath.row];
    [_stockCollectionView reloadData];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return [_stocksArray count];
    
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    StocksCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"StockCellID" forIndexPath:indexPath];
    
    if (cell==nil) {
        cell = [[StocksCollectionViewCell alloc] init];
    }
    
    WatchListStk *stkObj = [_stocksArray objectAtIndex:indexPath.row];
    
    cell.stockLabel.text = stkObj.stkName;
    cell.myPath = indexPath;
    cell.delegate = self;
    
    return cell;
}





@end
