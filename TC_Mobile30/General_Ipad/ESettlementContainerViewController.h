//
//  ESettlementContainerViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 19/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESettlementContainerViewController : UIViewController

- (void)switchToViewController : (NSString*)segueString;
- (void)reloadPage:(NSString *)urlLink  andTitle:(NSString *)title;

@end
