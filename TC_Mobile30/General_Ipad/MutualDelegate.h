
/*
 This header is used to avoid conflicts in syntax when
 Viewcontrollers are using each other's delegate - emir.Dec2016
 
 */

@class StockPreViewController;
@class QuoteScreenViewController;
@class OrderBookandPortfolioViewController;

@protocol StockPreViewDelegate <NSObject>
@required
-(void)chartDidEnlarge:(BOOL)enlarge;
-(void)chartWillEnlarge;
-(void)chartWillShrink;
-(void)closePreviewBtnClicked;
@end

@protocol QuoteScreenDelegate <NSObject>
@required
-(void)stockDidSelected:(NSString*)stkCode andName:(NSString*)stkName watchList:(BOOL)isWatchList;
-(void)qcFeedDidUpdateForSelectedStock;
@end


@protocol PortfolioDelegate <NSObject>
@required
-(void)stockPFDidSelected:(NSString*)stkCode andName:(NSString*)stkName watchList:(BOOL)isWatchList;
@end