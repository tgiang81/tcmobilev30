//
//  OrderStatusTableViewCell.h
//  TCiPad
//
//  Created by Sri Ram on 12/8/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TradeStatus.h"
#import "LanguageManager.h"


/*
 @property (weak, nonatomic) IBOutlet UILabel *ord_date;
 @property (weak, nonatomic) IBOutlet UILabel *ord_time;
 @property (weak, nonatomic) IBOutlet UILabel *acc_no;
 @property (weak, nonatomic) IBOutlet UILabel *ord_no;
 @property (weak, nonatomic) IBOutlet UILabel *stock_symbol;
 @property (weak, nonatomic) IBOutlet UILabel *ord_action;
 @property (weak, nonatomic) IBOutlet UILabel *ord_type;
 @property (weak, nonatomic) IBOutlet UILabel *ord_validity;
 @property (weak, nonatomic) IBOutlet UILabel *ord_status;
 @property (weak, nonatomic) IBOutlet UILabel *ord_qty;
 @property (weak, nonatomic) IBOutlet UILabel *ord_price;
 @property (weak, nonatomic) IBOutlet UILabel *totmatchqty;
 @property (weak, nonatomic) IBOutlet UILabel *ave_price;
 @property (weak, nonatomic) IBOutlet UILabel *matchval;
 @property (weak, nonatomic) IBOutlet UILabel *sett_curr;
 @property (weak, nonatomic) IBOutlet UILabel *set_mode;
 */

@interface OrderStatusTableViewCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UILabel *lblStkSymbol;
@property (weak, nonatomic) IBOutlet UILabel *lblOrdTime;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblbOrdQty;
@property (weak, nonatomic) IBOutlet UILabel *lblOrdPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblAction;
@property (weak, nonatomic) IBOutlet UILabel *selectedArrow;

-(void)populateHistoryWithData:(TradeStatus*)ts;



@end
