//
//  ConfirmTableViewCell.h
//  TCiPad
//
//  Created by n2n on 01/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *dataTitle;
@property (strong, nonatomic) IBOutlet UILabel *dataContent;
@property (weak, nonatomic) IBOutlet UILabel *dataTitle2;
@property (weak, nonatomic) IBOutlet UILabel *DataContent2;

@end
