//
//  BrokerHouseIcon.m
//  TCiPad
//
//  Created by Scott Thoo on 1/6/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import "BrokerHouseIcon.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageUtility.h"
@implementation BrokerHouseIcon
@synthesize view,lblBrokerName,btnIcon,imgLogo;

//Programmatically
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"BrokerHouseIcon" owner:self options:nil];
        // 2. adjust bounds
        self.bounds = self.view.bounds;
        // 3. Add as a subview
        [self addSubview:self.view];
        
    }
    return self;
}

//With Storyboard
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"BrokerHouseIcon" owner:self options:nil];
        imgLogo.layer.cornerRadius = imgLogo.frame.size.width / 2;
        imgLogo.layer.borderWidth = 3.0f;
        imgLogo.layer.borderColor = [UIColor blackColor].CGColor;
        imgLogo.clipsToBounds = YES;
        // 2. Add as a subview
        [self addSubview:self.view];
    }
    
    return self;
}

- (id)initWithBrokerName:(NSString *)brokerName andIconName:(NSString*)iconName
{
    self = [super initWithFrame:self.frame];
    if (self) {
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"BrokerHouseIcon" owner:self options:nil];
        // 2. adjust bounds
        self.bounds = self.view.bounds;
        
        //Assign variables
        lblBrokerName.text   = brokerName;
        imgLogo.image = [UIImage imageNamed:iconName];
        
        
        //        if ([brokerName isEqualToString:@"AffinHwang"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_affinTrade_512.png"];
        ////            imgLogo.image = [UIImage imageNamed:@"Login.png"];
        //        else if([brokerName isEqualToString:@"N2N"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_TCMobile_512.png"];
        //        else if([brokerName isEqualToString:@"TAFutures"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_TA_512.png"];
        //        else if([brokerName isEqualToString:@"TAOnline"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_TA_512.png"];
        //        else if([brokerName isEqualToString:@"CIMB MY"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_CIMB_MY_512.png"];
        //        else if([brokerName isEqualToString:@"affin"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_affinTrade_512.png"];
        //        else if([brokerName isEqualToString:@"PMLink2U"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_PM_512.png"];
        //        else if([brokerName isEqualToString:@"iPacOnline"])
        //                imgLogo.image = [UIImage imageFromResource:@"BHI_InterPac_512.png"];
        //        else if([brokerName isEqualToString:@"AmeSecurities"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_AmSec_512.png"];
        //        else if([brokerName isEqualToString:@"AmFutures"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_AmSec_512.png"];
        //        else if([brokerName isEqualToString:@"CIMB SG"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_CIMB_SG_512.png"];
        //        else if([brokerName isEqualToString:@"Apex"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_Apex_512.png"];
        //        else if([brokerName isEqualToString:@"DBSV SG"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_DBS_512.png"];
        //        else if([brokerName isEqualToString:@"FA"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_FA_512.png"];
        //        else if([brokerName isEqualToString:@"HDCap"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_HDcap_512.png"];
        //        else if([brokerName isEqualToString:@"KAF"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_KAF_512.png"];
        //        else if([brokerName isEqualToString:@"SJenie"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_SJenie_512.png"];
        //        else if([brokerName isEqualToString:@"PSE"])
        //            imgLogo.image = [UIImage imageFromResource:@"BHI_PSE_512.png"];
        
        imgLogo.layer.cornerRadius = imgLogo.frame.size.width / 2;
        imgLogo.layer.borderWidth = 1.0f;
        imgLogo.layer.borderColor = [UIColor grayColor].CGColor;
        //        imgLogo.layer.backgroundColor = [UIColor clearColor].CGColor;
        imgLogo.layer.masksToBounds = YES;
        //        imgLogo.clipsToBounds = YES;
        //        [imgLogo setHidden:YES];
        
        
        // 3. Add as a subview
        [self addSubview:self.view];
        
    }
    return self;
}


@end

@interface UIImage(Overlay)
@end

@implementation UIImage(Overlay)

- (UIImage *)imageWithColor:(UIColor *)color1
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextClipToMask(context, rect, self.CGImage);
    [color1 setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end









