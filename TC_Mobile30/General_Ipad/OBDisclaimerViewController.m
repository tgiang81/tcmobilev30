//
//  OBDisclaimerViewController.m
//  TCiPad
//
//  Created by n2n on 20/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "OBDisclaimerViewController.h"

@interface OBDisclaimerViewController () {
   
    
}

@end

@implementation OBDisclaimerViewController

#define LINK1 @"http://stgntp2.itradecimb.com/gcCIMB/doc/CIMBDMA_disclaimer.html"

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _okBtn.layer.cornerRadius = kButtonRadius*_okBtn.frame.size.height;
    _okBtn.clipsToBounds = YES;
    _cancelBtn.layer.cornerRadius = kButtonRadius*_cancelBtn.frame.size.height;
    _cancelBtn.clipsToBounds = YES;
    
}



-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //NSLog(@"PFDisclaimerWebViewURL %@",[UserPrefConstants singleton].PFDisclaimerWebViewURL);
    
    
    NSURL *url = [NSURL URLWithString:[UserPrefConstants singleton].PFDisclaimerWebViewURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.disclaimerWebview loadRequest:request];

    [_loadingActivity startAnimating];
    [_disclaimerWebview setHidden:YES];
    _errorMessageLabel.text = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeDisclaimer) name:@"dismissAllModalViews" object:nil];
    
}


-(void)closeDisclaimer{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissAllModalViews" object:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
   // NSLog(@"startload");
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
   // NSLog(@"finished");
     dispatch_async(dispatch_get_main_queue(), ^{
         [_disclaimerWebview setHidden:NO];
         [_loadingActivity stopAnimating];
         _errorMessageLabel.text = @"";
     });
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [_disclaimerWebview setHidden:YES];
    [_loadingActivity stopAnimating];
    
    if ([error.description containsString:@"timed out"]) {
        _errorMessageLabel.text = [LanguageManager stringForKey:@"Page could not be loaded. Request Timed Out."];
    } else {
        // other error
        _errorMessageLabel.text = [LanguageManager stringForKey:@"Page could not be loaded. Try again later."];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)okClicked:(id)sender {
    // 1FA or 2FA
    [[ATPAuthenticate singleton] timeoutActivity];
    [self dismissViewControllerAnimated:YES completion:^(void) {
        
        [self.delegate disclaimerClickedOk];
    }];

}








@end
