//
//  QuantityPadViewController.h
//  TCiPad
//
//  Created by Sri Ram on 1/4/16.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuantityPadDelegate <NSObject>
-(void)quantityBtnPressed:(UIButton *)btn;
@end

@interface QuantityPadViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *v_Content;

@property (nonatomic,weak) id <QuantityPadDelegate> delegate;

- (IBAction)btnPressed:(UIButton *)sender;


@end
