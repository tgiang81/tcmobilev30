//
//  OrderBookandPortfolioViewController.h
//  TCiPad
//
//  Created by Sri Ram on 12/7/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderStatusTableViewCell.h"
#import "EqPortfolioTableviewCellTableViewCell.h"
#import "DetailsTableViewCell.h"
#import "VirtualPLTableViewCell.h"
#import "SummaryTableViewCell.h"
#import "ATPAuthenticate.h"

#import "MutualDelegate.h"
#import "TradeAccountController.h"
#import "N2Tabs.h"
#import "MarqueeLabel.h"
#import "LanguageManager.h"
#import "AppConstants.h"
#import "PrtfSummRptData.h"
#import "N2DatePicker.h"
#import "OBDisclaimerViewController.h"

@interface OrderBookandPortfolioViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate,
UITextFieldDelegate,EqPortfolioDelegate,
UIGestureRecognizerDelegate, TradeAccountDelegate, N2TabsDelegate, N2DatePickerDelegate, OBDisclaimerDelegate>



@property (weak, nonatomic) id <PortfolioDelegate> delegate;
@property (nonatomic, assign) NSInteger pfMode;

@property (weak, nonatomic) IBOutlet UIView *tabsPlaceHolder;
@property (weak, nonatomic) IBOutlet UILabel *ordBkAccountName;
@property (weak, nonatomic) IBOutlet UILabel *tradeLimitTitle;
@property (weak, nonatomic) IBOutlet UILabel *ordBkTradeLimit;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@property (weak, nonatomic) IBOutlet UIView *orderHistPanel;
@property (weak, nonatomic) IBOutlet UIView *portFolioPanel;
@property (weak, nonatomic) IBOutlet UILabel *fromDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *toDateTitle;

@property (weak, nonatomic) IBOutlet UITableView *tableView; // orderbook table (for orderbook and orderhistory)
@property (weak, nonatomic) IBOutlet UITableView *portfolioTableView; // portfolio table (for realized and unrealized)
@property (weak, nonatomic) IBOutlet UITableView *detailsTableview; // to show extra data fields
@property (weak, nonatomic) IBOutlet UIView *detailsContainer;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *reviseButton;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *sellButton;

// Derivatives portfolio container
@property (weak, nonatomic) IBOutlet UIView *derivativesContainer;
@property (weak, nonatomic) IBOutlet UITableView *virtualPLtable;
@property (weak, nonatomic) IBOutlet UITableView *summaryTable;
@property (nonatomic, strong) NSMutableArray *virtualPLData;
@property (strong, nonatomic) IBOutlet UIView *SummaryHeaderView;


// HEADER IBOutlets must be STRONG otherwise tableview releases them
@property (strong, nonatomic) IBOutlet UIView *ordBkHeaderView;
@property (strong, nonatomic) IBOutlet UIView *eqPortHeaderView;
@property (strong, nonatomic) IBOutlet UILabel *mktValHeader;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *btnLoading;
@property (nonatomic, strong) MarqueeLabel *accLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchHistoryBtn;
@property (weak, nonatomic) IBOutlet UIButton *clearHistoryBtn;

@property (weak, nonatomic) IBOutlet UILabel *dateFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateToLabel;
@property (nonatomic, strong) NSDate *fromDate, *toDate;

// Details
@property (nonatomic, strong) NSMutableArray *detailsArray;
@property (weak, nonatomic) IBOutlet UILabel *detailStkCode;
@property (weak, nonatomic) IBOutlet UILabel *detailStkName;
@property (weak, nonatomic) IBOutlet UILabel *detailLastDone;
@property (weak, nonatomic) IBOutlet UILabel *detailChange;

@property (nonatomic, strong) N2Tabs *tabs;


- (IBAction)clearHistory:(id)sender;
- (IBAction)refreshBtnPressed:(id)sender;
- (IBAction)accoutnBtnPressed:(id)sender;
- (IBAction)searchHistory:(id)sender;
- (IBAction)callDatePicker:(id)sender;

- (IBAction)cancelOrderBtnPressed:(UIButton *)sender;
- (IBAction)buyorderBtnPressed:(UIButton *)sender;
- (IBAction)sellorderBtnPressed:(UIButton *)sender;
- (IBAction)reviseOrderBtnPressed:(UIButton *)sender;

@end
