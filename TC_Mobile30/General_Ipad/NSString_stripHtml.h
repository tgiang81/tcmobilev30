//
//  NSString_stripHtml.h
//  TCiPhone_N2NMultiLogin_UAT_V1.4.5
//
//  Created by Ahmad Ashraf Azman on 21/06/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSString (stripHtml)
- (NSString*)stripHtml;
@end
