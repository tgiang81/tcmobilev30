//
//  StockStreamer.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 12/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "StockStreamer.h"

@implementation StockStreamer
@synthesize lblStkName;
@synthesize lblCondition;
@synthesize lblPrice;
@synthesize lblQuantity;
@synthesize lblValue;
@synthesize lblTime;
@synthesize lblChange;
@synthesize lblChangePercentage;
@synthesize animationView;
//Programmatically
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockStreamer" owner:self options:nil];
        // 2. adjust bounds
        self.bounds = self.view.bounds;
        
        // self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        // 3. Add as a subview
        
        [self addSubview:self.view];
        
    }
    return self;
}

//With Storyboard
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockStreamer" owner:self options:nil];
        // 2. Add as a subview
        
        
        //self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
       // self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;

        [self addSubview:self.view];
    }
    
    return self;
}

- (void) updateContent:(NSString *)stockName
                 andPI:(NSString *)pi
              andPrice:(NSString *)price
           andQuantity:(NSString *)quantity
              andValue:(NSString *)value
               andTime:(NSString *)time
             andChange:(NSString *)change
   andChangePercentage:(NSString *)changePercentage
        andChangeValue:(float)changeValue;{
    
    
    [UIView animateWithDuration:0.5f animations:^{
        
        if(changeValue>0){
            [animationView setBackgroundColor:[UIColor greenColor]];
        }else if(changeValue<0){
            [animationView setBackgroundColor:[UIColor redColor]];
        }else{
            [animationView setBackgroundColor:[UIColor orangeColor]];
        }
        [animationView setAlpha:0.5f];
        
    } completion:^(BOOL finished) {
        
        //fade out
        [UIView animateWithDuration:0.5f animations:^{
            [animationView setBackgroundColor:[UIColor clearColor]];
            [animationView setAlpha:0.0f];
            
        } completion:nil];
        
    }];
    
    if(changeValue>0){
        [lblCondition setTextColor:[UIColor greenColor]];
        [lblTime setTextColor:[UIColor greenColor]];
        [lblPrice  setTextColor:[UIColor greenColor]];
        [lblChange  setTextColor:[UIColor greenColor]];
        [lblChangePercentage  setTextColor:[UIColor greenColor]];
    }
    else if (changeValue<0){
        [lblCondition setTextColor:[UIColor redColor]];
        [lblTime setTextColor:[UIColor redColor]];
        [lblPrice  setTextColor:[UIColor redColor]];
        [lblChange  setTextColor:[UIColor redColor]];
        [lblChangePercentage  setTextColor:[UIColor redColor]];
    }
    else{
        [lblCondition setTextColor:[UIColor orangeColor]];
        [lblTime setTextColor:[UIColor orangeColor]];
        [lblPrice  setTextColor:[UIColor orangeColor]];
        [lblChange  setTextColor:[UIColor orangeColor]];
        [lblChangePercentage  setTextColor:[UIColor greenColor]];
    }
    
    
    [lblStkName setText:stockName];
    [lblCondition setText:pi];
    [lblPrice setText:price];
    [lblQuantity setText:quantity];
    [lblValue setText:value];
    [lblTime setText:time];
    [lblChange setText:change];
    [lblChangePercentage setText:changePercentage];
    
    // Add blink effect to this
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
