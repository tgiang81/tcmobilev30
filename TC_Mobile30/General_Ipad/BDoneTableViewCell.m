//
//  BDoneTableViewCell.m
//  TCiPad
//
//  Created by n2n on 17/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "BDoneTableViewCell.h"
#import "UserPrefConstants.h"

@implementation BDoneTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/* Data example:
 {
 0 = "0.480";
 1 = 113000;
 2 = 3880600;
 3 = 3993600;
 4 = 1916928;
 },
 */

-(void)updateCellWithDictionary:(NSDictionary*)dataDict withLACP:(CGFloat)lacpPrice {
    
    long long svol = [[dataDict objectForKey:@"1"] longLongValue];
    long long bvol = [[dataDict objectForKey:@"2"] longLongValue];
    long long tvol = [[dataDict objectForKey:@"3"] longLongValue];
    long long tval = [[dataDict objectForKey:@"4"] longLongValue];
    
    CGFloat priceFloat = [[dataDict objectForKey:@"0"] floatValue];
    
    if ([UserPrefConstants singleton].pointerDecimal==3) {
    
        _bdPrice.text = [NSString stringWithFormat:@"%.3f",priceFloat];
        
    }else{
        _bdPrice.text = [NSString stringWithFormat:@"%.4f",priceFloat];
    }
    
    if (priceFloat>lacpPrice) {
        _bdPrice.textColor = [UIColor greenColor];
    } else if (priceFloat<lacpPrice) {
        _bdPrice.textColor = [UIColor redColor];
    } else {
        _bdPrice.textColor = [UIColor whiteColor];
    }
    
    _bdBvol.text = [[UserPrefConstants singleton] abbreviateNumber:bvol];
    _bdSvol.text = [[UserPrefConstants singleton] abbreviateNumber:svol];
    _bdTotalVol.text = [[UserPrefConstants singleton] abbreviateNumber:tvol];
    _bdTotalVal.text = [[UserPrefConstants singleton] abbreviateNumber:tval];
    
    float ratio =0;
    
    if (bvol+svol>0) { // prevent NaN
     ratio = (float)bvol/((float)bvol+(float)svol);
    }
        
    _bdBuyPercent.text = [NSString stringWithFormat:@"%.2f%%",ratio*100.0];
    [_bdBuyPercent setRatio:ratio];
    [_bdBuyPercent setColor1:kGreenColor];
    [_bdBuyPercent setColor2:kRedColor];
}

@end
