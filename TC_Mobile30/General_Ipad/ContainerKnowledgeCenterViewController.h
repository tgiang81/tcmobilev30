//
//  ContainerKnowledgeCenterViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/10/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerKnowledgeCenterViewController : UIViewController

- (void)switchToViewController : (NSString*)segueString;

@end
