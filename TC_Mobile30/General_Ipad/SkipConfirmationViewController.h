//
//  SkipConfirmationViewController.h
//  TCiPad
//
//  Created by Sri Ram on 12/3/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetails.h"
#import "TradeStatus.h"
#import "ConfirmTableViewCell.h"
#import "LanguageManager.h"
#import "QCData.h"
#import "QCConstants.h"
#import "BaseVC.h"
@class AroundShadowView;
@protocol SkipDialogDelegate <NSObject>
@optional
-(void)skipDialogDidSubmit:(NSString*)action;
-(void)skipDialogDidCancel;
- (void)needToReloadSkipVCData;
- (void)backFromSkip;
- (void)subConfirmViewBack;
- (NSArray *)getCurrentSubmitColor;
@end

@interface SkipConfirmationViewController : BaseVC <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UILabel *StkName;
@property (strong, nonatomic) IBOutlet UILabel *StkCode;


@property (nonatomic, assign) id<SkipDialogDelegate> skipDelegate;
@property (nonatomic, strong) NSMutableArray *detailArray;
@property (strong, nonatomic) IBOutlet UITableView *confirmTable;
@property (strong, nonatomic) IBOutlet UILabel *confirmTitle;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UILabel *changeValue;
@property (weak, nonatomic) IBOutlet UILabel *changePercent;
@property (assign, nonatomic) CGFloat changeValueContent;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;
- (IBAction)btnSubmitPressed:(id)sender;
-(void)loadData:(NSString *)str orderdetails:(OrderDetails *)od tradeStat:(TradeStatus*)tradeStatus  fullMode:(BOOL)full withStockCurrency:(NSString*)stockCurrency;
- (IBAction)btnCancelPressed:(id)sender;

@end
