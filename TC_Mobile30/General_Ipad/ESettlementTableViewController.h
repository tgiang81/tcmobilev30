//
//  ESettlementTableViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 18/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESettlementTableViewCell.h"
#import "LanguageManager.h"

@protocol ESettlementTableViewControllerDelegate <NSObject>

@optional
- (void) didClickTableView:(NSString *)index andTitle:(NSString *)title;

@end

@interface ESettlementTableViewController : UITableViewController{
    int sectionNumber;
}

@property (nonatomic, assign) CGRect frameToUse;
@property (weak, nonatomic) id <ESettlementTableViewControllerDelegate> delegate;

@end
