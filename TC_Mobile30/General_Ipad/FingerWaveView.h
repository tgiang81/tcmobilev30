//
//  FingerWaveView.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 29/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FingerWaveView : UIView <CAAnimationDelegate>

+ (instancetype)showInView:(UIView *)view center:(CGPoint)center;

@end
