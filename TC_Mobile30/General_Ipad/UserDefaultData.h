//
//  UserDefaultData.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 27/10/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultData : NSObject


//Touch ID Preferences

+ (void)setTouchIDFlag:(BOOL)touchIDFlag forUser:(NSString*)brokerCode;
+ (BOOL)getTouchIDFlagForUser:(NSString*)brokerCode;
+(void)removeTouchIDFlagForUser:(NSString*)brokerCode;

+ (void)createTouchIDSecretKey;
+ (NSString *)getTouchIDSecretKey;

// Save and Set Username for Touch ID
+ (void) saveUsername:(NSString *)username;
+ (NSString *) getUsername;

// Save and Set Password for Touch ID
+ (void) savePassword:(NSString *)password;
+ (NSString *) getPassword;

@end
