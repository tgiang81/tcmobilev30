//
//  SettingsViewController.m
//  TCiPad
//
//  Created by n2n on 02/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "SettingsViewController.h"
#import "TOCTableViewController.h"
#import "ContainerSettingsController.h"
#import "ATPAuthenticate.h"

@interface SettingsViewController () <TOCDelegate> {
    BOOL runOnce;
}


@property (nonatomic, weak) ContainerSettingsController *containerViewController;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    runOnce = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableRowDidClicked:(NSString *)segue {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    
    [self.containerViewController switchToViewController:segue];
}

-(void)viewDidLayoutSubviews {
    
    if (runOnce) {
        runOnce = NO;
        CGFloat xRatio = 1024.0/977.0;
        CGSize scrSize = [[UIScreen mainScreen] bounds].size;
        
        _tocContainerView.frame = CGRectMake(_tocContainerView.frame.origin.x, _tocContainerView.frame.origin.y,
                                             275.0, _tocContainerView.frame.size.height);
        
        _contentContainerView.frame = CGRectMake(_tocContainerView.frame.size.width, 0,
                                                 (scrSize.width-_tocContainerView.frame.size.width)/xRatio,
                                                 _contentContainerView.frame.size.height);
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    
    if ([segue.identifier isEqualToString:@"segueTOC"])
    {
        TOCTableViewController *tocVC = (TOCTableViewController*)[segue destinationViewController];
        tocVC.frameToUse = _TOCContainer.frame;
        tocVC.tocDelegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"embedSettingsContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }

}



@end
