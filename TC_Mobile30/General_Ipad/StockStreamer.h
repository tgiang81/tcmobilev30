//
//  StockStreamer.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 12/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockStreamer : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *lblStkName;
@property (weak, nonatomic) IBOutlet UILabel *lblCondition;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblChange;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePercentage;
@property (weak, nonatomic) IBOutlet UIView *animationView;
- (void) updateContent:(NSString *)stockName
                 andPI:(NSString *)pi
              andPrice:(NSString *)price
           andQuantity:(NSString *)quantity
              andValue:(NSString *)value
               andTime:(NSString *)time
             andChange:(NSString *)change
   andChangePercentage:(NSString *)changePercentage
        andChangeValue:(float)changeValue;
@end
