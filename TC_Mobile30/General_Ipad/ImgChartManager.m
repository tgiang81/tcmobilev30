//
//  ImgChartManager.m
//  TCiPad
//
//  Created by n2n on 19/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "ImgChartManager.h"
#import "SDWebImageDownloader.h"
#import "CPService.h"
#import "SDImageCache.h"
#import "AllCommon.h"

@implementation ImgChartManager



#pragma mark - Init

-(id)init {
    self = [super init];
    if (self) {
        self.stkCode = @"";
    }
    return self;
}

// Get chart URL in bulk- NOT USED

-(void)getImageChartURL:(NSString *)urlString
          completedWithLink:(void(^)(NSString *data))completedWithLink
{
    
    NSURLSession *session = [NSURLSession sharedSession];
   _urlTask= [session dataTaskWithURL:[NSURL URLWithString:urlString]
                                        completionHandler:^(NSData *data,
                                                            NSURLResponse *response,
                                                            NSError *error) {
                                            
                                            if (error==NULL) {
                                                
                                                NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                                
                                                NSRange rangeHttp = [dataStr rangeOfString:@"http"];
                                                NSRange rangePng = [dataStr rangeOfString:@"png"];
                                                
                                                if ((rangeHttp.location!=NSNotFound)&&(rangePng.location!=NSNotFound)) {
                                                    
                                                    NSString *chartLink = [dataStr substringWithRange:NSMakeRange(rangeHttp.location, rangePng.location+3-rangeHttp.location)];
                                                    
                                                    completedWithLink(chartLink);
                                                } else {
                                                    completedWithLink(@"");
                                                }
                                            } else {
                                                 completedWithLink(@"");
                                            }
                                        }];
    [_urlTask resume];
}


// For single chart usage 

-(void)getImageChartWithURL:(NSString *)urlString
                    completedWithData:(void(^)(NSData *data))completedWithData
                    failure:(void(^)(NSString *errorMessage))failure
{
    
    
    // LOADING CHART IMAGE FROM URL
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithURL:[NSURL URLWithString:urlString]
                                        completionHandler:^(NSData *data,
                                                            NSURLResponse *response,
                                                            NSError *error) {
                                            
                                            if (error==NULL) {
                                                
                                                NSString* dataStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                                
                                                NSRange rangeHttp = [dataStr rangeOfString:@"http"];
                                                NSRange rangePng = [dataStr rangeOfString:@"png"];
                                                
                                                if ((rangeHttp.location!=NSNotFound)&&(rangePng.location!=NSNotFound)) {
                                                    
                                                    NSString *chartLink = [dataStr substringWithRange:NSMakeRange(rangeHttp.location, rangePng.location+3-rangeHttp.location)];
                                                    
                                                    //NSLog(@"chartLink %@",chartLink);
                                                    
                                                    NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy;
                                                    NSURL *url = [NSURL URLWithString:[chartLink stringByAddingPercentEncodingWithAllowedCharacters:chars]];
                                                    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                                                                             completionHandler:^(NSData *data,
                                                                                                                                 NSURLResponse *response,
                                                                                                                                 NSError *error) {
                                                                                                                 
                                                                                                                 if ((error==NULL)&&([data length]>0)) {
                                                                                                                     //NSLog(@"%ld",[data length]);
                                                                                                                     // success
                                                                                                                    completedWithData(data);
                                                                                                                 } else {
                                                                                                                     // fail
                                                                                                                     failure(@"NO_DATA");
                                                                                                                 }
                                                                                                                 
                                                                                                             }];
                                                    
                                                    // task.taskDescription = @"chartTask2";
                                                    [task resume];
                                                } else {
                                                    failure(@"NO_LINK");
                                                }
                                                
                                            } else {
                                                
                                            }
                                        }];
    // task.taskDescription = @"chartTask1";
    [task resume];
}




#pragma mark - Download Image With Cache from URL
+ (void)loadImageFromURL:(NSString *)url completion:(void (^)(UIImage *image, NSError *error))completion{
	//Chech cache from disk
	UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:url];
	if (cachedImage) {
		DLog(@"+++Cached from disk");
		completion(cachedImage, nil);
		return;
	}
	//Chech cache from default
	cachedImage = [[SDImageCache sharedImageCache] imageFromCacheForKey:url];
	if (cachedImage) {
		DLog(@"+++Cached from default cache");
		completion(cachedImage, nil);
		return;
	}
	[CPService requestDataFromURL:url completion:^(id result, NSError *error) {
		if (error) {
			completion(nil, error);
			return ;
		}
		NSString* dataStr = (NSString *)result;
		NSRange rangeHttp = [dataStr rangeOfString:@"http"];
		NSRange rangePng = [dataStr rangeOfString:@"png"];
		if ((rangeHttp.location!=NSNotFound)&&(rangePng.location!=NSNotFound)) {
			NSString *chartLink = [dataStr substringWithRange:NSMakeRange(rangeHttp.location, rangePng.location+3-rangeHttp.location)];
			//NSLog(@"chartLink %@",chartLink);
			
			NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy;
			NSURL *imageURL = [NSURL URLWithString:[chartLink stringByAddingPercentEncodingWithAllowedCharacters:chars]];
			//Download image chart
			[[SDWebImageDownloader sharedDownloader] downloadImageWithURL:imageURL options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
				// Cache image to disk or memory
				if (image) {
					[[SDImageCache sharedImageCache] storeImage:image forKey:url completion:^{
						completion(image, error);
					}];
				}else{
					completion(image, error);
				}
			}];
		} else {
			//Create error object
			NSDictionary *userInfo = @{
									   NSLocalizedDescriptionKey: NSLocalizedString(@"NO_LINK", nil),
									   NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"The operation timed out.", nil),
									   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Need to try again", nil)
									   };
			NSError *error = [NSError errorWithDomain:dataStr
												 code:-57
											 userInfo:userInfo];
			//Completion
			completion(nil, error);
		}
	}];
}

+ (void)cleareCache{
	[[SDImageCache sharedImageCache] clearMemory];
	[[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
}
@end
