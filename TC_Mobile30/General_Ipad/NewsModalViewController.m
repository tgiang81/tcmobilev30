//
//  NewsModalViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 10/8/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import "NewsModalViewController.h"
#import "StockDetailViewController.h"
#import "ExchangeData.h"
#import "UserPrefConstants.h"
#import "ATPAuthenticate.h"
#import "UserPrefConstants.h"
#import "LanguageKey.h"

@interface NewsModalViewController () {
    
    CGPoint newsContainerOriPos;
    CGFloat bufferNewsX;
    ATPAuthenticate *atp;
    BOOL runOnce, dismissing;
}

@end

@implementation NewsModalViewController
@synthesize newsWebView = _newsWebView;
@synthesize indicatorView = _indicatorView;
@synthesize closebUtton = _closebUtton;
@synthesize currentEd;



- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [_indicatorView stopAnimating];
    _indicatorView.hidden = YES;
    runOnce = YES;
    dismissing = NO;
    
    // set newsContainer position off screen
    [_newsContainerView setCenter:CGPointMake(1846*1.2, _newsContainerView.center.y)];
    
    // add 3 finger gesture to dismiss news view
    UISwipeGestureRecognizer *threeFingerSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissNews)];
    threeFingerSwipe.delegate = self;
    threeFingerSwipe.numberOfTouchesRequired = 2;
    [self.view addGestureRecognizer:threeFingerSwipe];
    
    atp = [ATPAuthenticate singleton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissNews) name:@"dismissAllModalViews" object:nil];
}


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (runOnce) {
        runOnce = NO;
        if (currentEd!=nil) {
            [_companyNameLabel setHidden:NO];
            [_fundamentalView setHidden:NO];
            _newsWebView.frame = CGRectMake(0, _newsWebView.frame.origin.y,
                                            _newsWebView.frame.size.width,
                                            _newsWebView.frame.size.height - _fundamentalView.frame.size.height);
            [self populateFundamentals];
            
        } else {
            _newsWebView.frame = CGRectMake(0, _newsWebView.frame.origin.y,
                                            _newsWebView.frame.size.width,
                                            _newsWebView.frame.size.height);
            [_fundamentalView setHidden:YES];
            [_companyNameLabel setHidden:YES];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.5]];
        
    } completion:^(BOOL finished) {
        // slide in the newsContainer
        
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            _newsContainerView.center = CGPointMake(self.view.frame.size.width/2.0, _newsContainerView.center.y);
            
        } completion:^(BOOL finished) {
            newsContainerOriPos = _newsContainerView.center;
            UIPanGestureRecognizer *panGestureNews = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewNewsDidPanned:)];
            panGestureNews.delegate = self;
            panGestureNews.maximumNumberOfTouches = 1;
            [_newsContainerView addGestureRecognizer:panGestureNews];
        }];
    
    }];
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissAllModalViews" object:nil];
    
    [super viewDidDisappear:animated];
}

#pragma mark - fundamental buttons


-(void)createButtonAndAddToArray:(NSMutableArray*)array withLabelToArray:(NSMutableArray*)lblArray withImageNamed:(NSString*)imageName andTag:(long)mytag andLabel:(NSString*)title andSelector:(SEL)selector {
    
    CGFloat buttonWidth = 40.0;
    CGFloat labelWidthFactor = 2;
    CGFloat labelHeight = 35;
    
    UIButton *eachButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [eachButton setFrame:CGRectMake(0, 0, buttonWidth, buttonWidth)];
    [eachButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [eachButton setTag:mytag];
    [eachButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [array addObject:eachButton];
    
    
    UILabel *eachLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, buttonWidth*labelWidthFactor, labelHeight)];
    [eachLabel setText:title];
    [eachLabel setNumberOfLines:2];
    [eachLabel setTag:mytag-10];
    [eachLabel setTextAlignment:NSTextAlignmentCenter];
    UIFont *font = [UIFont systemFontOfSize:10];
    [eachLabel setFont:font];
    [eachLabel setTextColor:[UIColor whiteColor]];
    [lblArray addObject:eachLabel];
    
}


/*
 √ 0 = Company Info
 √ 1 = Company Key Person
 √ 2 = Shareholding Summary
 √ 3 = Announcement
 √ 4 = Financial Report
 X 5 = Estimates
 X 6 = Supply Chain
 X 7 = Market Activity
 */

-(void)populateFundamentals {
    
    
    
    // if either one is enabled, then load it up
    if (currentEd.subsCPIQAnnounce||currentEd.subsCPIQCompInfo||currentEd.subsCPIQCompSynp
        ||currentEd.subsCPIQCompKeyPer||currentEd.subsCPIQShrHoldSum||currentEd.subsCPIQFinancialRep) {
        
        // create buttons based on number of factset enabled
        
        NSMutableArray *btnArray = [[NSMutableArray alloc] init];
        NSMutableArray *lblArray = [[NSMutableArray alloc] init];
        
        
        NSString *source = [UserPrefConstants singleton].fundamentalSourceCode;
        
        if (currentEd.subsCPIQCompSynp) {
            
            [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_Synopsis" andTag:51 andLabel:[LanguageManager stringForKey:@"Company Info"] andSelector:@selector(companyInfoButtonPressed:)];
            
        }
        
        if (currentEd.subsCPIQCompKeyPer) {
            
            [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_KeyPerson" andTag:54 andLabel:[LanguageManager stringForKey:@"Company Key Persons"] andSelector:@selector(companyKeyPersonButtonPressed:)];
            
        }
        
        if (currentEd.subsCPIQShrHoldSum) {
            if (![source isEqualToString:@"TRKD"])
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_ShareHolding" andTag:55 andLabel:[LanguageManager stringForKey:@"ShareHolding Summary"] andSelector:@selector(shareholdingSummaryButtonPressed:)];
        }
        
        
        if (currentEd.subsCPIQAnnounce) {
            if (![source isEqualToString:@"TRKD"])
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_Announcement" andTag:53 andLabel:[LanguageManager stringForKey:@"Announcements"] andSelector:@selector(announcementButtonPressed:)];
            
        }
        
        
        if (currentEd.subsCPIQFinancialRep) {
            
            [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_FinReport" andTag:56 andLabel:[LanguageManager stringForKey:@"Financial Report"] andSelector:@selector(financialReportButtonPressed:)];
            
        }
        
//        if (currentEd.subsCPIQCompInfo) {
//            if (![source isEqualToString:@"TRKD"])
//            [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_CompInfo" andTag:52 andLabel:@"Estimates" andSelector:@selector(estimatesButtonPressed:)];
//            
//        }
//        
//        // currently no LMS settings, so just display it
//        if (![source isEqualToString:@"TRKD"])
//        [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_CompInfo" andTag:57 andLabel:@"Supply Chain" andSelector:@selector(supplyChainButtonPressed:)];
//        
//        if (![source isEqualToString:@"TRKD"])
//        [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_CompInfo" andTag:58 andLabel:@"Market Activity" andSelector:@selector(mktActivityButtonPressed:)];
//        
        //add buttons in orderly fashion
        
        CGFloat buttonWidth = 30.0;
        
        CGSize scrSize = _fundamentalView.frame.size;
        

        
        CGFloat buttonWidthPerRow = [btnArray count] * buttonWidth;
        CGFloat spacePerRow = scrSize.width - buttonWidthPerRow;
        CGFloat eachSpaces = spacePerRow / ([btnArray count]+1);
        
        CGFloat xOffset = _fundamentalView.frame.size.width/([btnArray count]+1);
        CGFloat yOffset = _fundamentalView.frame.size.height/3.0;
        
        long xVal = 0;
        
        for (long i=0; i<[btnArray count]; i++) {
            xVal += 1;
            
            UIButton *eachButton = [btnArray objectAtIndex:i];
            UIButton *eachLabel = [lblArray objectAtIndex:i];
            
            [_fundamentalView addSubview:eachButton];
            [_fundamentalView addSubview:eachLabel];
            
            [eachButton setCenter: CGPointMake(xOffset, yOffset)];
            [eachLabel setCenter:CGPointMake(xOffset,
                                             eachButton.center.y+eachButton.frame.size.height/2.0+eachLabel.frame.size.height/2.0)];
  
            xOffset += eachSpaces + buttonWidth;
            
        }
        
    } else {
        // do nothing
        
    }
}




//Type 1 - Company Info
//Type 2 - Financial Info
- (NSString *) getFundamentalDataURL:(int)type andCategory:(NSString *)category {
    
    NSString *fundamentalLabel = [UserPrefConstants singleton].fundamentalNewsLabel;
    NSArray *codeComponents = [[UserPrefConstants singleton].userSelectingStockCode componentsSeparatedByString:@"."];
    
    NSString *trade_exchg_code = [UserPrefConstants singleton].userCurrentExchange;
    NSString *strip_stock_code = @"";
    NSString *broker_code = @"";
    
    if ([codeComponents count]>1) {
        strip_stock_code = [codeComponents objectAtIndex:0]; // 0108.KL = get 0108
    }
    
    
    broker_code = [UserPrefConstants singleton].brokerCode;
    
    if (type == 1) { // Company Info
        
        if ([fundamentalLabel length]>0) {
            
            return [NSString stringWithFormat:@"%@?SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&bhCode=%@&loginID",
                    [UserPrefConstants singleton].fundamentalNewsServer,
                    [UserPrefConstants singleton].fundamentalSourceCode,
                    trade_exchg_code,
                    strip_stock_code,
                    category,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
        }
        else
            return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundDataCompInfoURL,
                    @"CPIQ",
                    trade_exchg_code,
                    strip_stock_code,
                    category,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
    }
    else if (type == 2) { // Financial Report
        if ([fundamentalLabel length]>0) {
            
            return [NSString stringWithFormat:@"%@stkFincInfom.jsp?SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=IS&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundamentalNewsServer,
                    [UserPrefConstants singleton].fundamentalSourceCode,
                    trade_exchg_code,
                    strip_stock_code,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
        }
        else
            return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=INC&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundFinancialInfoURL,
                    @"CPIQ",
                    trade_exchg_code,
                    strip_stock_code,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
    }
    
    return @"";
}

-(NSString*)getResponsiveFundamentalDataURL:(int)packageID{
    
    
    return [NSString stringWithFormat:@"%@rw.jsp?StkCode=%@&SourceCode=%@&Sponsor=%@&bhCode=%@&loginID=%@&prodId=TCMOBILE&Type=5&key=%@&PackageID=%d&Menu=N",
            [UserPrefConstants singleton].fundamentalNewsServer,
            [UserPrefConstants singleton].userSelectingStockCode,
            [UserPrefConstants singleton].fundamentalSourceCode,
            [UserPrefConstants singleton].SponsorID,
            [UserPrefConstants singleton].brokerCode,
            [UserPrefConstants singleton].userName,
            [[UserPrefConstants singleton] encryptTime],
            packageID
            ];
    
}

- (IBAction) mktActivityButtonPressed:(id)sender {
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:7];
    
    
   // NSLog(@"url %@", url);
    
    [self openUrl:url withJavascript:YES];
    
    
    _newsTitleLabel.text = @"Market Activity";
}

- (IBAction) supplyChainButtonPressed:(id)sender {
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:6];
    
    
    [self openUrl:url withJavascript:YES];
    
    
    _newsTitleLabel.text = @"Supply Chain";
}


- (IBAction) estimatesButtonPressed:(id)sender {
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:5];
    
    
    [self openUrl:url withJavascript:YES];
    
    
    _newsTitleLabel.text = @"Estimates";
}


- (IBAction) companySynopsisButtonPressed:(id)sender {
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:0];


    [self openUrl:url withJavascript:YES];
    

    _newsTitleLabel.text = @"Company Synopsis";
}


- (IBAction) companyInfoButtonPressed:(id)sender {
    
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:0];

     [self openUrl:url withJavascript:YES];

    _newsTitleLabel.text = @"Company Info";
}

- (IBAction) companyKeyPersonButtonPressed:(id)sender {
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:1];


     [self openUrl:url withJavascript:YES];

    _newsTitleLabel.text = @"Company Key Persons";
}


- (IBAction) announcementButtonPressed:(id)sender {
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:3];

     [self openUrl:url withJavascript:YES];

    _newsTitleLabel.text = @"Announcements";
}

- (IBAction) financialReportButtonPressed:(id)sender {
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:4];

     [self openUrl:url withJavascript:YES];

    _newsTitleLabel.text = @"Financial Reports";
}


- (IBAction) shareholdingSummaryButtonPressed:(id)sender {
    
    
    [atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURL:2];

    [self openUrl:url withJavascript:YES];

    
    _newsTitleLabel.text = @"ShareHolding Summary";
    
    // [appData timeoutActivity];
    //
    // FundamentalCapitalIQViewController *fciqv = [[FundamentalCapitalIQViewController alloc] initWithNibName:@"FundamentalCapitalIQView" bundle:nil];
    // fciqv.urlStr = [self getFundamentalDataURL:1 andCategory:@"ShareHolders"];
    //
    // //Set title View
    // fciqv.title = @"Shareholding Summary";
    // CGSize size = [[GlobalVariables sharedVariables].systemVersion intValue] < 7 ? [fciqv.title sizeWithFont:[UIFont boldSystemFontOfSize:20.0]] : [fciqv.title sizeWithAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:20.0]}];
    // CGRect frame = CGRectMake(0, 0, size.width, 44);
    // UILabel *label = [[UILabel alloc] initWithFrame:frame];
    // label.text = fciqv.title;
    // label.textAlignment = UITextAlignmentCenter;
    // label.font = [UIFont boldSystemFontOfSize:19.0];
    // label.backgroundColor = [UIColor clearColor];
    // label.textColor = [UIColor whiteColor];
    // fciqv.navigationItem.titleView = label;
    // [label release];
    //
    // [self.navigationController pushViewController:fciqv animated:YES];
    // [fciqv release];
}



#pragma mark - Gesture methods


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    
    
    return YES;
}

-(void)viewNewsDidPanned:(UIPanGestureRecognizer*)recognizer {
    
    UIView *view = recognizer.view;
    
   // CGPoint velocity = [recognizer velocityInView:view.superview];
    
   // if (velocity.x>0) {
    
    CGPoint translation = [recognizer translationInView:view.superview];
    
    if (translation.x>=80) {
        bufferNewsX = translation.x-79;
    } else {
        bufferNewsX = 0;
    }
    
    if (bufferNewsX>0) {
    
        CGFloat distanceToDismiss = 250;
        
        if(recognizer.state == UIGestureRecognizerStateBegan){
            
            
            
        }else if(recognizer.state == UIGestureRecognizerStateEnded){
            
            if (_newsContainerView.center.x<=newsContainerOriPos.x+distanceToDismiss) {
                // return back
                [UIView animateWithDuration:0.2 animations:^{
                    _newsContainerView.center = newsContainerOriPos;
                }completion:^(BOOL finished) {
                    
                }];
            }
        } else {
            // swping
            
           
            
            _newsContainerView.center = CGPointMake(newsContainerOriPos.x+bufferNewsX, newsContainerOriPos.y);
            
            if (_newsContainerView.center.x>=newsContainerOriPos.x+distanceToDismiss) {
                // DISMISS
                [view removeGestureRecognizer:recognizer];
                [self dismissNews];
            }
            
        }
        
    } else {
        
        [UIView animateWithDuration:0.2 animations:^{
            _newsContainerView.center = newsContainerOriPos;
        }completion:^(BOOL finished) {
            
        }];
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dismissNews {

    
    if (!dismissing) {
        dismissing = YES;
        
       
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            [_newsContainerView setCenter:CGPointMake(_newsContainerView.frame.size.width*1.5, _newsContainerView.center.y)];
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                UIVisualEffectView *beView = [self.view viewWithTag:100];
                beView.alpha = 0.0;
                
            } completion:^(BOOL finished) {
                
                [self goBackToMainView];
            }];
        }];
        
    }
    
}


- (void) goBackToMainView
{
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void) openUrl:(NSString *)urlString withJavascript:(BOOL)jsEnabled {
    
    
    NSString* js =
    @"var meta = document.createElement('meta'); " \
    "meta.setAttribute( 'name', 'viewport' ); " \
    "meta.setAttribute( 'content', 'width = device-width, initial-scale=1.0, minimum-scale=0.2, maximum-scale=5.0; user-scalable=1;' ); " \
    "document.getElementsByTagName('head')[0].appendChild(meta)";
    
    
    if (jsEnabled) {
        [_newsWebView stringByEvaluatingJavaScriptFromString: js];
    }
    NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval: 20.0];
    [_newsWebView loadRequest:request];
    [_newsWebView setScalesPageToFit:NO];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_indicatorView stopAnimating];
     _indicatorView.hidden = YES;
    // Do whatever you want here
    UserPrefConstants *UPC = [UserPrefConstants singleton];
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                          (unsigned long)UPC.newsFontSize];
    [_newsWebView stringByEvaluatingJavaScriptFromString:jsString];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [_indicatorView stopAnimating];
    _indicatorView.hidden = YES;
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:TC_Pro_Mobile message:[NSString stringWithFormat:@"%@. Can't Load News at this Moment. Please Check Back Later.",error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [_indicatorView startAnimating];
     _indicatorView.hidden = NO;
}

- (IBAction)closeWebView:(id)sender{
    [self dismissNews];
}
    
- (IBAction)smallerFont:(id)sender {

    UserPrefConstants *UPC = [UserPrefConstants singleton];
    
    UPC.newsFontSize = (UPC.newsFontSize > 50) ? UPC.newsFontSize -5 : UPC.newsFontSize;

    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                          (unsigned long)UPC.newsFontSize];
    [_newsWebView stringByEvaluatingJavaScriptFromString:jsString];
}
    
- (IBAction)biggerFont:(id)sender {

    UserPrefConstants *UPC = [UserPrefConstants singleton];
    
    UPC.newsFontSize = (UPC.newsFontSize < 160) ? UPC.newsFontSize +5 : UPC.newsFontSize;
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                          (unsigned long)UPC.newsFontSize];
    [_newsWebView stringByEvaluatingJavaScriptFromString:jsString];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
