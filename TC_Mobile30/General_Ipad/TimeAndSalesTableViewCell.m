//
//  TimeAndSalesTableViewCell.m
//  TCUniversal
//
//  Created by Scott Thoo on 11/18/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "TimeAndSalesTableViewCell.h"

@implementation TimeAndSalesTableViewCell
@synthesize lblPrice = _lblPrice;
@synthesize lblTime = _lblTime;
@synthesize lblTradeAt = _lblTradeAt;
@synthesize lblVolume = _lblVolume;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
