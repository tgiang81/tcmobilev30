//
//  ContainerViewController.m
//  TCUniversal
//
//  Created by Scott Thoo on 11/5/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "ContainerViewController.h"
#import "HomeViewController.h"
#import "QuoteScreenViewController.h"
#import "StockDetailViewController.h"
#import "MBProgressHUD.h"
#import "IndicesViewController.h"
#import "UserPrefConstants.h"
#import "OrderBookandPortfolioViewController.h"
#import "SettingsViewController.h"
#import "ESettlementViewController.h"
#import "StockAlertViewController.h"
#import "ElasticNewsViewController.h"
#import "MarketStreamerViewController.h"
#import "StockTrackerViewController.h"

#define segueHome           @"segueHome"
#define segueQuoteScreen    @"segueQuoteScreen"
#define segueStockDetail    @"segueStockDetail"
#define segueWatchList      @"segueWatchList"
#define segueIndices        @"segueIndices"
#define segueOrderbook  @"segueOrderbook"
#define segueSettings  @"segueSettings"
#define segueESettlement  @"segueESettlement"
#define segueStockAlert  @"segueStockAlert"
#define segueElasticNews @"segueElasticNews"
#define segueMarketStreamer @"segueMarketStreamer"
#define segueMarketTracker @"segueStockTracker"

@interface ContainerViewController ()
{
    NSString *currentSegue;
}

@property (strong, nonatomic) QuoteScreenViewController *quoteVC;
@property (strong, nonatomic) HomeViewController *homeVC;
@property (strong, nonatomic) StockDetailViewController *stockVC;
@property (strong, nonatomic) IndicesViewController *indicesVC;
@property (strong, nonatomic) SettingsViewController *settingsVC;
@property (strong, nonatomic) ESettlementViewController *settlementVC;
@property (strong, nonatomic) StockAlertViewController *stockAlertVC;
@property (strong, nonatomic) ElasticNewsViewController *elasticVC;
@property (strong, nonatomic) OrderBookandPortfolioViewController *orderBookVC;
@property (strong, nonatomic) MarketStreamerViewController *streamerVC;
@property (strong, nonatomic) StockTrackerViewController *trackerVC;

@property (assign, nonatomic) BOOL transitionInProgress;

@end

@implementation ContainerViewController
@synthesize currentSegue;
@synthesize quoteVC;
@synthesize homeVC;
@synthesize stockVC;
@synthesize orderBookVC;
@synthesize indicesVC;
@synthesize settingsVC;
@synthesize settlementVC;
@synthesize stockAlertVC;
@synthesize elasticVC;
@synthesize streamerVC;
@synthesize trackerVC;

- (void)viewDidLoad {
    [super viewDidLoad];
     self.transitionInProgress = NO;
//     NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
//    [UserPrefConstants singleton].defaultView =  [[def objectForKey:@"ScreenViewMode"] intValue];
//    
    
    
//    NSLog(@" [UserPrefConstants singleton].defaultView %d", [UserPrefConstants singleton].defaultView);
//    switch ( [UserPrefConstants singleton].defaultView) {
//        case 1:
//            currentSegue = segueHome;
//             [self performSegueWithIdentifier:segueHome sender:nil];
//            break;
//        case 2:
//             [UserPrefConstants singleton].fromWatchlist = NO;
//             currentSegue = segueQuoteScreen;
//             [self performSegueWithIdentifier:segueQuoteScreen sender:nil];
//            break;
//        case 3:
//              [UserPrefConstants singleton].fromWatchlist = YES;
//              currentSegue = segueQuoteScreen;
//            [self performSegueWithIdentifier:segueQuoteScreen sender:nil];
//            break;
//        default:
//             currentSegue = segueHome;
//            [self performSegueWithIdentifier:segueHome sender:nil];
//            break;
//    }
    
    if ([[UserPrefConstants singleton].defaultStartingScreen length]<=0) {
        currentSegue = segueHome;
        [self performSegueWithIdentifier:segueHome sender:nil];
    } else {
        if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Home"]) {
            currentSegue = segueHome;
            [self performSegueWithIdentifier:segueHome sender:nil];
        } else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"Quote"]) {
            [UserPrefConstants singleton].fromWatchlist = NO;
            currentSegue = segueQuoteScreen;
            [self performSegueWithIdentifier:segueQuoteScreen sender:nil];
        } else if ([[UserPrefConstants singleton].defaultStartingScreen isEqualToString:@"WatchList"]) {
            [UserPrefConstants singleton].fromWatchlist = YES;
            currentSegue = segueQuoteScreen;
            [self performSegueWithIdentifier:segueQuoteScreen sender:nil];
        } else {
            // default is always home
            currentSegue = segueHome;
            [self performSegueWithIdentifier:segueHome sender:nil];
        }
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    //NSLog(@"%s", __PRETTY_FUNCTION__);
    ////NSLog(@"self.childViewControllers.count : %lu",(unsigned long)self.childViewControllers.count);

    if ([segue.identifier isEqualToString:segueHome])
    {
        self.homeVC = segue.destinationViewController;
        if(self.childViewControllers.count >0)
        {
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.homeVC];
        }
        else
        {
           // [self addChildViewController:homeVC];
//            [self addChildViewController:segue.destinationViewController];
//            UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            HomeViewController *homeViewController=[mystoryboard instantiateViewControllerWithIdentifier:@"segueHome"];
//            dstView = homeViewController.view;
            [self addChildViewController:self.homeVC];
            UIView* destView = ((UIViewController *)self.homeVC).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:segueQuoteScreen]) {
        self.quoteVC = segue.destinationViewController;
        if(self.childViewControllers.count >0)
        {

        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.quoteVC];
        }
        else
        {
            // [self addChildViewController:homeVC];
            //            [self addChildViewController:segue.destinationViewController];
            //            UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //            HomeViewController *homeViewController=[mystoryboard instantiateViewControllerWithIdentifier:@"segueHome"];
            //            dstView = homeViewController.view;
            [self addChildViewController:self.quoteVC];
            UIView* destView = ((UIViewController *)self.quoteVC).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:segueStockDetail]) {
         stockVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:stockVC];
    }
    else if ([segue.identifier isEqualToString:segueWatchList]) {
         quoteVC = segue.destinationViewController;
        if(self.childViewControllers.count >0){
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:quoteVC];
        }else
        {
            // [self addChildViewController:homeVC];
            //            [self addChildViewController:segue.destinationViewController];
            //            UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //            HomeViewController *homeViewController=[mystoryboard instantiateViewControllerWithIdentifier:@"segueHome"];
            //            dstView = homeViewController.view;
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    else if ([segue.identifier isEqualToString:segueIndices]) {
         indicesVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:indicesVC];
    }
    else if ([segue.identifier isEqualToString:segueOrderbook]) {
        orderBookVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:orderBookVC];
    }
    else if ([segue.identifier isEqualToString:segueSettings]) {
        settingsVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:settingsVC];
    }else if([segue.identifier isEqualToString:segueESettlement]){
        settlementVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:settlementVC];
    }else if ([segue.identifier isEqualToString:segueStockAlert]){
        stockAlertVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:stockAlertVC];
    }else if ([segue.identifier isEqualToString:segueElasticNews]){
        elasticVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:elasticVC];
    }else if ([segue.identifier isEqualToString:segueMarketStreamer]){
        streamerVC = segue.destinationViewController;
         [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:streamerVC];
    }else if ([segue.identifier isEqualToString:segueMarketTracker]){
        trackerVC = segue.destinationViewController;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:trackerVC];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
//    CGFloat screenHeight = screenRect.size.height;
   // toViewController.view.frame = toViewController==indicesVC?CGRectMake(0, 0, self.view.frame.size.width,screenHeight-62 ): CGRectMake(0, 0, self.view.frame.size.width, screenHeight-112);
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:1.0 options:UIViewAnimationOptionTransitionNone animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        
    }];
}


- (void)switchToViewController : (NSString*)segueString
{
    ////NSLog(@"switchToViewController %@",segueString);
    [self performSegueWithIdentifier:segueString sender:nil];
    currentSegue = segueString;
}

@end
