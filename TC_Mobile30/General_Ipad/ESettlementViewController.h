//
//  ESettlementViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 18/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESettlementTableViewController.h"

@interface ESettlementViewController : UIViewController <ESettlementTableViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIView *TOCContainer;
@property (strong, nonatomic) IBOutlet UIView *tocContainerView;
@property (strong, nonatomic) IBOutlet UIView *contentContainerView;



@end
