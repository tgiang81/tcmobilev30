//
//  N2Tabs.h
//  test
//
//  Created by Emir on 16/01/2017.
//  Copyright © 2017 n2n. All rights reserved.
//
//  Simple Tabs "component".
//  Not really a custom control, but why reinvent the wheel

#import <UIKit/UIKit.h>

@class N2Tabs;

@protocol N2TabsDelegate <NSObject>
@required
-(void)tabDidClicked:(UIButton*)tabButton atTab:(N2Tabs*)tab;
@end

//typedef enum : NSUInteger {
//    TAB_LOCATION_TOP,
//    TAB_LOCATION_BTM
//} tabLocation;

@interface N2Tabs : UIView

@property (nonatomic, weak) id<N2TabsDelegate>tabDelegate;
@property (nonatomic, strong) NSArray *tabButtons;
@property (nonatomic, strong) UIColor *txtColor, *btnColor;
@property (nonatomic, assign) NSInteger selectedTabIndex;
@property (nonatomic, strong) NSString *tabName;

-(id)initWithParams:(NSArray*)params withFrame:(CGRect)frame andFontSize:(CGFloat)fontSize;
-(id)initWithFrame:(CGRect)frame;
-(void)updateTabAppearance;

@end
