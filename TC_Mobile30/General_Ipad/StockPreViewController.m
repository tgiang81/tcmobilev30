//
//  StockPreViewController.m
//  TCiPad
//
//  Created by n2n on 17/11/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "StockPreViewController.h"
#import "QCData.h"
#import "QCConstants.h"
#import "UserPrefConstants.h"
#import "AppControl.h"
#import "AppConstants.h"
#import "TransitionDelegate.h"
#import "ATPAuthenticate.h"
#import "MarqueeLabel.h"
#import "MDTableViewCell.h"
#import "ExchangeData.h"
#import "TimeAndSalesTableViewCell.h"
#import "NewsModalViewController.h"
#import "BDoneTableViewCell.h"

@interface StockPreViewController () {
    
    MarqueeLabel *sectorLabel;
    NSArray *stkCodeAndExchangeArr;
    ATPAuthenticate *_atp;
    NSString *stkCodeDisplay; // shortened without exchange end
    NSString *_stkCode; // used for getting data from qcfeed
    BOOL isUpdate, isFullScreenChart, runOnce, mktDepthZero;
    NSMutableDictionary *tempDictionary;
    long currentTotDays;
    CGRect webViewOriginalFrame;
    CGFloat oriStkNameWidth;
    ExchangeData *currentEd;
    MDInfo *mdInfo;
    QCData *_qcData;
    BOOL loadedMD,loadedTS, loadedNews, loadedBD;
    BOOL canRefreshTimeSales, canRefreshBizDone;
    NSString *bundleIdentifier;
    TradingRules *trdRules;
    
    void (^localBlock)(int);
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@end

@implementation StockPreViewController

@synthesize  delegate, transitionController;

#pragma mark - TableView Delegate


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    if (tableView==_mktDepthTable) {
        
        if (loadedMD) {
            tableView.backgroundView = nil;
            return 1;
        } else {
            // loading
            UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [act startAnimating];
            tableView.backgroundView = act;
            
            return 0;
        }
        
    } else if (tableView==_timeSalesTable) {
        
        if (loadedTS) {
            if ([_timeSalesArr count]>0) {
                tableView.backgroundView = nil;
                return 1;
            } else {
                // Display a message when the table is empty
                UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
                messageLabel.text = [LanguageManager stringForKey:@"No Results."];
                messageLabel.textColor = [UIColor whiteColor];
                messageLabel.numberOfLines = 0;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                [messageLabel sizeToFit];
                tableView.backgroundView = messageLabel;
                return 0;
            }
            
        } else {
            
            UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [act startAnimating];
            tableView.backgroundView = act;
            return 0;
        }
    } else if (tableView==_bizDoneTable) {
        
        if (loadedBD) {
            if ([_bizDoneTableDataArr count]>0) {
                tableView.backgroundView = nil;
                return 1;
            } else {
                // Display a message when the table is empty
                UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
                messageLabel.text = [LanguageManager stringForKey:@"No Results."];
                messageLabel.textColor = [UIColor whiteColor];
                messageLabel.numberOfLines = 0;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                [messageLabel sizeToFit];
                tableView.backgroundView = messageLabel;
                return 0;
            }
            
        } else {
            
            UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [act startAnimating];
            tableView.backgroundView = act;
            return 0;
        }
    } else if (tableView==_newsTable) {
        
        if (loadedNews) {
            if (([_newsArr count]>0)||([_newsArcArr count]>0)) {
            
                tableView.backgroundView = nil;
                return 1;
            } else {
                UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
                messageLabel.text = [LanguageManager stringForKey:@"No Results."];
                messageLabel.textColor = [UIColor whiteColor];
                messageLabel.numberOfLines = 0;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                [messageLabel sizeToFit];
                tableView.backgroundView = messageLabel;
           
                return 0;
            }
        } else {
            UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [act startAnimating];
            tableView.backgroundView = act;
            return 0;
        }
        
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

        
        if (indexPath.row % 2==0) {
            cell.contentView.backgroundColor = kCellGray1;
        } else {
            cell.contentView.backgroundColor = kCellGray2;
        }

    
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView==_mktDepthTable) {
        return [_MDBuyArr count];
    }else if (tableView==_timeSalesTable) {
        return [_timeSalesArr count];
    } else if (tableView==_newsTable) {
        if ([UserPrefConstants singleton].isArchiveNews) {
            return [_newsArcArr count];
        } else {
            return [_newsArr count];
        }
    } else if (tableView==_bizDoneTable) {
        return [_bizDoneTableDataArr count];
    } else {
        return 0;
    }
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // MARK: MARKET DEPTH
    if (tableView==_mktDepthTable) {
    
        static NSString *cellID = @"MDCellID";
        
        MDTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell==nil) {
            
            cell = [[MDTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        }
        
        NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
        [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        [priceFormatter setGroupingSeparator:@","];
        [priceFormatter setGroupingSize:3];
        
        long row = indexPath.row;
        cell.buyBid.textColor = [UIColor whiteColor];
        cell.sellAsk.textColor= [UIColor whiteColor];;
        
        double lacp = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
        
        
        NSDictionary *sellData1 = [NSDictionary new];
        NSDictionary *buyData1 = [NSDictionary new];
        
        NSLog(@"_MDBuyArr count = %lu, _MDSellArr count = %lu",(unsigned long)_MDBuyArr.count,(unsigned long)_MDSellArr.count);
        
        if ([_MDBuyArr count]>0) {
            buyData1 = [_MDBuyArr objectAtIndex:row];
            
            cell.buySplit.text = [[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
            cell.buyQty.text = [[UserPrefConstants singleton] abbreviateNumber:[[buyData1 objectForKey:FID_58_I_BUY_QTY_1] integerValue]];
            cell.buyBid.text = [priceFormatter stringFromNumber:[buyData1 objectForKey:FID_68_D_BUY_PRICE_1]];
            
            double buyPrice = [[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue];
            if (buyPrice>lacp) {
                cell.buyBid.textColor = [UIColor greenColor];
            } else
                if (buyPrice<lacp) {
                    cell.buyBid.textColor = [UIColor redColor];
                } else {
                    cell.buyBid.textColor = [UIColor whiteColor];
                }
        }
        
        if ([_MDSellArr count]>0) {
            
             sellData1 = [_MDSellArr objectAtIndex:row];
            
            cell.sellSplit.text = [[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] stringValue];
            cell.sellQty.text = [[UserPrefConstants singleton] abbreviateNumber:[[sellData1 objectForKey:FID_58_I_BUY_QTY_1] integerValue]];
            cell.sellAsk.text = [priceFormatter stringFromNumber:[sellData1 objectForKey:FID_68_D_BUY_PRICE_1]];
         
            double sellPrice = [[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue];
            if (sellPrice>lacp) {
                cell.sellAsk.textColor = [UIColor greenColor];
            } else
                if (sellPrice<lacp) {
                    cell.sellAsk.textColor = [UIColor redColor];
                } else {
                    cell.sellAsk.textColor = [UIColor whiteColor];
                }
        }
        
        cell.mdIndex.text = [NSString stringWithFormat:@"%ld", row+1];
        
        
        if (mktDepthZero) {
            cell.buyBid.textColor = [UIColor whiteColor];
            cell.sellAsk.textColor = [UIColor whiteColor];
        }
        
        // update cell: no update for preview
        
//        if ([_MDBuyPrevArr count]>0) {
//            NSDictionary *buyData2 = [_MDBuyPrevArr objectAtIndex:row];
//            NSDictionary *sellData2 = [_MDSellPrevArr objectAtIndex:row];
//            struct CGColor *flashRed = [kRedFlash CGColor];
//            struct CGColor *flashGreen = [kGrnFlash CGColor];
//            struct CGColor *clearClr = [[UIColor clearColor] CGColor];
//            CGFloat animDur = 1.0;
//            
//            
//            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
//                [cell.buySplit.layer setBackgroundColor:flashGreen];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.buySplit.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
//                [cell.buySplit.layer setBackgroundColor:flashRed];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.buySplit.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
//                [cell.buyQty.layer setBackgroundColor:flashGreen];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyQty.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
//                [cell.buyQty.layer setBackgroundColor:flashRed];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyQty.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
//                [cell.buyBid.layer setBackgroundColor:flashGreen];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyBid.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
//                [cell.buyBid.layer setBackgroundColor:flashRed];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.buyBid.layer setBackgroundColor:clearClr]; }];
//            }
//            
//            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
//                [cell.sellSplit.layer setBackgroundColor:flashGreen];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellSplit.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
//                [cell.sellSplit.layer setBackgroundColor:flashRed];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellSplit.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
//                [cell.sellQty.layer setBackgroundColor:flashGreen];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellQty.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
//                [cell.sellQty.layer setBackgroundColor:flashRed];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellQty.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
//                [cell.sellAsk.layer setBackgroundColor:flashGreen];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellAsk.layer setBackgroundColor:clearClr]; }];
//            }
//            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
//                [cell.sellAsk.layer setBackgroundColor:flashRed];
//                [UIView animateWithDuration:animDur animations:^(void) { [cell.sellAsk.layer setBackgroundColor:clearClr]; }];
//            }
//            
//        }
        return cell;
        
        // MARK: TIME & SALES
    } else if (tableView==_timeSalesTable) {
        
        static NSString *cellID = @"TSCellID";
        
        TimeAndSalesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell==nil) {
            
            cell = [[TimeAndSalesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];

        }
        
        //Inverting Order of table data
        NSUInteger row = _timeSalesArr.count - 1 - indexPath.row;
        if (row==0)[cell setHidden:YES];
        
        //Get Close price and incoming Price
        float closePrice;
        float incomingPrice;
        closePrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue];
        incomingPrice =[[[_timeSalesArr objectAtIndex:row]objectForKey:@"2"]floatValue];
        if (incomingPrice > closePrice)
            cell.lblPrice.textColor = [UIColor greenColor];
        else if (incomingPrice < closePrice)
            cell.lblPrice.textColor = [UIColor redColor];
        else if (incomingPrice == closePrice)
            cell.lblPrice.textColor = [UIColor whiteColor];
        
        // follow TCPlus b=red, s=green
        if ([[[_timeSalesArr objectAtIndex:row]objectForKey:@"1"] isEqualToString:@"b"])
        {
            cell.lblTradeAt.textColor = [UIColor greenColor];
        }
        else if ([[[_timeSalesArr objectAtIndex:row]objectForKey:@"1"] isEqualToString:@"s"])
        {
            cell.lblTradeAt.textColor = [UIColor redColor];
        } else
        {
            cell.lblTradeAt.textColor = [UIColor whiteColor];
        }
        
        //Convert Incoming StrDate to proper one.
        
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"HHmmss"];
        NSDate *currentDate = [dateFormater dateFromString:[[_timeSalesArr objectAtIndex:row]objectForKey:@"0"]];
        
        [dateFormater setDateFormat:@"HH:mm:ss"];
        NSString *convertedDateString = [dateFormater stringFromDate:currentDate];
        NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
        [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [quantityFormatter setGroupingSeparator:@","];
        [quantityFormatter setGroupingSize:3];
        [quantityFormatter setGeneratesDecimalNumbers:NO];
        
        NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
        [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        [priceFormatter setGroupingSeparator:@","];
        [priceFormatter setGroupingSize:3];
        
        cell.lblTime.text       = convertedDateString;
        cell.lblTradeAt.text    = [[_timeSalesArr objectAtIndex:row]objectForKey:@"1"];
        cell.lblPrice.text      = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_timeSalesArr objectAtIndex:row]objectForKey:@"2"] floatValue]]];
        
        NSString *tempVol       = [[_timeSalesArr objectAtIndex:row]objectForKey:@"3"];
        // cell.lblVolume.text     = [quantityFormatterTimeAndSales stringFromNumber:[NSNumber numberWithInt:[tempVol intValue]]];
        cell.lblVolume.text = [[UserPrefConstants singleton] abbreviateNumber:[tempVol intValue]];
        
        // cell.lblVolume.text     = [quantityFormatter stringFromNumber:[NSNumber numberWithLong:[[[_timeAndSalesTableDataArr objectAtIndex:row]objectForKey:@"3"] longValue]]];
        
        return cell;
        
        
        // MARK: BIZ DONE CELLS
        
    }   else if (tableView==_bizDoneTable) {
            static NSString *CellIdentifier = @"BizDoneCellID";
            BDoneTableViewCell *cell = (BDoneTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell==nil) {
                cell = [[BDoneTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
        
            CGFloat lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
            
            [cell updateCellWithDictionary:[_bizDoneTableDataArr objectAtIndex:indexPath.row] withLACP:lacpFloat];
            
            return cell;
        
        // MARK: NEWS
    } else if (tableView==_newsTable) {
        static NSString *cellID = @"NewsCellID";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (cell==nil) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.detailTextLabel.textColor = [UIColor grayColor];
        }
        
        long row = indexPath.row;
        
        // archive news
        if ([UserPrefConstants singleton].isArchiveNews) {
            NSDictionary *item = [_newsArcArr objectAtIndex:row];
            
            if (item) {
                
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [[UserPrefConstants singleton]  getLogicalDate:[item objectForKey:@"dt"]]];
                cell.textLabel.text =  [[UserPrefConstants singleton] stringByStrippingHTML:[item objectForKey:@"t"]];
                
            }
            
        } else {
            // NSLog(@"not archive");
            
            NSDictionary *item = [_newsArr objectAtIndex:row];
            if (item) {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",[[UserPrefConstants singleton] getLogicalDate:[item objectForKey:@"4"]]];
                cell.textLabel.text = [item objectForKey:@"3"];
                
                
            }
            
        }

        return cell;
    }
    
    return 0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==_newsTable) {
     
        [_atp timeoutActivity];
        
        int row = (int)indexPath.row;
        
        if ([UserPrefConstants singleton].isArchiveNews) {
            NSDictionary *item = [_newsArcArr objectAtIndex:row];
            NSString *url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,[item objectForKey:@"id"]];
            //      NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            //      [webView loadRequest:request];
            [self openNews:url withTitle:[LanguageManager stringForKey:@"Stock News"]];
        } else {
            
            NSDictionary *item = [_newsArr objectAtIndex:row];
            NSString *url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,[item objectForKey:@"6"]];
            //      NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            //      [webView loadRequest:request];
            [self openNews:url withTitle:[LanguageManager stringForKey:@"Stock News"]];
        }

        
    }
}

#pragma mark - Scrollview delegates

- (void)scrollViewDidScroll:(UIScrollView *)sender
{

    CGFloat pageWidth = _detailScroll.frame.size.width;
    int page = floor((_detailScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pgControl.currentPage = page;
    
    if ((page==1)&&(!loadedMD)) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishMarketDepth:) name:@"didFinishMarketDepth" object:nil];
        [self getMarketDepth];
        loadedMD = YES;
    }
    
    if (page==2) {
        [_timeSalesTable addSubview:self.refreshControl];
        if (!loadedTS) {
            loadedTS = YES;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedTimeAndSales:) name:@"didFinishedTimeAndSales" object:nil];
            [self getTimeAndSales];
        }
    }
    
    if (page==3) {
        [_bizDoneTable addSubview:self.refreshControl];
        if (!loadedBD) {
            loadedBD = YES;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedBusinessDone:) name:@"didFinishedBusinessDone" object:nil];
            [self getBizDone];
        }
    }
    
    if ((page==4)&&(!loadedNews)) {
        loadedNews = YES;
        [self getCompanyNews];
    }
    
    
    
}


#pragma mark - GeneralSelector Delegates
-(void)selectorItemSelected:(NSIndexPath *)indexPath selectorType:(GeneralSelectorType)type {
    
    if (type==TYPE_WLIST) {
        SectorInfo *eachWL = [self.sortedWLArr objectAtIndex:indexPath.section];
        //        NSLog(@"WL: %@",eachWL.sectorName);
        
        [_atp addWatchlistCheckItem:[eachWL.sectorCode intValue]  withItem:_stkCode];
    }
    
}


#pragma mark - View LifeCycles Delegates

 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

     if ([segue.identifier isEqualToString:@"segueAddToWatchlist"]) {
         long cnt = [UserPrefConstants singleton].userWatchListArray.count;
         NSMutableArray *wlArray = [[NSMutableArray alloc] init];
         for (int i=0; i<cnt; i++) {
             SectorInfo *inf = [[SectorInfo alloc] init];
             NSDictionary *eachWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:i];
             inf.sectorCode = [eachWL objectForKey:@"FavID"];
             inf.sectorName = [eachWL objectForKey:@"Name"];
             [wlArray addObject:inf];
         }
         
         // sort
         NSSortDescriptor *sortDescriptor;
         sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sectorName"
                                                      ascending:YES];
         self.sortedWLArr = [wlArray sortedArrayUsingDescriptors:@[sortDescriptor]];
         
         GeneralSelectorViewController *vc = (GeneralSelectorViewController*)segue.destinationViewController;
         vc.selectorArray = [[NSMutableArray alloc] initWithArray:_sortedWLArr];
         vc.delegate = self;
         vc.selectorType = TYPE_WLIST;
     }
 }




-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
   
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishGetElasticNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishMarketDepth" object:nil];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (runOnce) {
        webViewOriginalFrame = _chartView.frame;
        runOnce = NO;
        _parentViewFrame = self.parentViewController.view.frame;
        // NSLog(@"%f", _parentViewFrame.size.width);
        oriStkNameWidth = _stockName.frame.size.width;
        
        // load things
        [_detailScroll addSubview:_detail2View];

        _detail2View.frame = CGRectMake(0, 0, _detail2View.frame.size.width, _detail2View.frame.size.height);
        _mktDepthView.frame = CGRectMake(_detailScroll.frame.size.width, 0, _mktDepthView.frame.size.width, _mktDepthView.frame.size.height);
        _timeSalesView.frame =CGRectMake(_detailScroll.frame.size.width*2, 0, _timeSalesView.frame.size.width, _timeSalesView.frame.size.height);
        _bizDoneView.frame = CGRectMake(_detailScroll.frame.size.width*3, 0, _bizDoneView.frame.size.width, _bizDoneView.frame.size.height);
        _newsView.frame =CGRectMake(_detailScroll.frame.size.width*4, 0, _newsView.frame.size.width, _newsView.frame.size.height);

        _detailScroll.contentSize = CGSizeMake(_detailScroll.frame.size.width*5, _detailScroll.frame.size.height);
        
        _btnBuy.layer.cornerRadius = kButtonRadius*_btnBuy.frame.size.height;
        _btnBuy.layer.masksToBounds = YES;
        _btnSell.layer.cornerRadius = kButtonRadius*_btnSell.frame.size.height;
        _btnSell.layer.masksToBounds = YES;
        
        _watchListBtn.layer.cornerRadius = kButtonRadius*_watchListBtn.frame.size.height;
        _watchListBtn.layer.masksToBounds = YES;
        _btnStkDetail.layer.cornerRadius = kButtonRadius*_btnStkDetail.frame.size.height;
        _btnStkDetail.layer.masksToBounds = YES;
        
        [[LanguageManager defaultManager] applyAccessibilityId:self];
        [[LanguageManager defaultManager] translateStringsForViewController:self];
        
        currentEd = [[ExchangeData alloc] init];
        for (ExchangeData *ed in [[UserPrefConstants singleton] userExchagelist]) {
            if ([ed.feed_exchg_code isEqual:[[UserPrefConstants singleton] userCurrentExchange]]) {
                currentEd = ed;
                // NSLog(@"currentEd %@", currentEd.mktDepthLevel);
            }
        }
        
        for (MDInfo *inf in [UserPrefConstants singleton].currentExchangeInfo.qcParams.exchgInfo) {
            if ([inf.exchange isEqualToString:[UserPrefConstants singleton].userCurrentExchange]) {
                mdInfo = inf;
            }
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  
    bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//    bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
        _lacpLabel.text = @"TOP";
    }
    [_mainView setHidden:YES];
    
    _detailScroll.pagingEnabled  = YES;
    _detailScroll.delegate  = self;
    [_fundamentalView setBackgroundColor:[UIColor clearColor]];
    
    [_detailScroll addSubview:_mktDepthView];
    [_detailScroll addSubview:_timeSalesView];
    [_detailScroll addSubview:_bizDoneView];
    [_detailScroll addSubview:_newsView];
    [_detailScroll addSubview:_fundamentalView];
    [_fundamentalView setHidden:YES];
    _pgControl.currentPage = 0;
    
    _MDSellArr = [[NSMutableArray alloc] init];
    _MDBuyArr = [[NSMutableArray alloc] init];
    _MDBuyPrevArr = [[NSMutableArray alloc] init];
    _MDSellPrevArr = [[NSMutableArray alloc] init];
    _mktDepthArr = [[NSMutableArray alloc] init];
    
    _timeSalesArr = [[NSMutableArray alloc] init];
    _newsArr = [[NSMutableArray alloc] init];
    _newsArcArr = [[NSMutableArray alloc] init];
    _bizDoneTableDataArr =  [[NSMutableArray alloc] init];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.backgroundColor = [UIColor clearColor];
    _refreshControl.tintColor = [UIColor whiteColor];
    [_refreshControl addTarget:self
                            action:@selector(getTimeAndSalesOrBDone)
                  forControlEvents:UIControlEventValueChanged];

    canRefreshBizDone = NO;
    canRefreshTimeSales = NO;
    
    runOnce = YES;
    _atp = [ATPAuthenticate singleton];
    _qcData = [QCData singleton];
    
    loadedBD = loadedMD = loadedTS = loadedNews = NO;
    isUpdate = NO;
    isFullScreenChart = NO;
    currentTotDays = K_1YEAR;
    tempDictionary = [[NSMutableDictionary alloc] init];
    
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTappedChart:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    doubleTapGesture.delegate = self;
    isFullScreenChart = NO;
    [_chartView addGestureRecognizer:doubleTapGesture];
    
    UITapGestureRecognizer *doubleTapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGoToDetail:)];
    doubleTapGesture2.numberOfTapsRequired = 2;
    doubleTapGesture2.delegate = self;
    [_detailsView addGestureRecognizer:doubleTapGesture2];
    
    [_mktDepthView setBackgroundColor:[UIColor clearColor]];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)doubleTapGoToDetail:(UITapGestureRecognizer*)recognizer {
    
    // NSLog(@"Go to Detail");
    
    [self gotoStockDetails:nil];
}

#pragma mark - TapGesture delegates

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
    
}

#pragma mark - add To Watchlist

- (IBAction)addToWatchlist:(UIButton*)sender {

    [_atp timeoutActivity];
    
    
    [self performSegueWithIdentifier:@"segueAddToWatchlist" sender:nil];
    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Add to..."]
//                                                                             message:[LanguageManager stringForKey:@"WatchList"]
//                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    for(int i =0;i<[UserPrefConstants singleton].userWatchListArray.count;i++)
//    {
//         NSDictionary *eachWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:i];
//        UIAlertAction *watchListAction = [UIAlertAction actionWithTitle:[eachWL objectForKey:@"Name"]
//                                                                  style:UIAlertActionStyleDefault
//                                                                handler:^(UIAlertAction *action)
//                                          {
//                                              ////NSLog(@"Clicked WatchList item");
//                                              ////NSLog(@"Watchlist items fav id :  %@, stockcode:%@ ",watchListHeaderKey[i],_stkCode);
//                                              // [_atp addWatchlistItem:[[eachWL objectForKey:@"FavID"] intValue]  withItem:_stkCode];
//                                               [_atp addWatchlistCheckItem:[[eachWL objectForKey:@"FavID"] intValue]  withItem:_stkCode];
//                                          }];
//        
//        [alertController addAction:watchListAction];
//    }
//    
//
//    
//    UIPopoverPresentationController *popover = alertController.popoverPresentationController;
//    if (popover)
//    {
//        popover.sourceView = sender;
//        popover.sourceRect = sender.bounds;
//        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
//    }
//    [self presentViewController:alertController animated:YES completion:nil];
//    [alertController.view layoutIfNeeded];
}

#pragma mark - Chart methods
-(void)setButtonFontScale:(CGFloat)scale {
    
    for(UIView *v in [self.chartView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            if (btn.tag>50) {
                [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:scale]];
            }
        }
    }
    
}


-(UIImage*)captureWebView
{
    UIGraphicsBeginImageContext(_chartView.bounds.size);
    [_chartView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}


-(void)doubleTappedChart:(UITapGestureRecognizer*)recognizer {
    
    // NSLog(@"chart type %@", [UserPrefConstants singleton].chartType);
    
    [_atp timeoutActivity];
    
    if (!isFullScreenChart) { // enlarge
        [self.delegate chartWillEnlarge];
        
        [_btnClose setHidden:YES];
        
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        dummyView.frame = _chartView.frame;
        dummyView.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:dummyView];
        
        __block CGFloat scaleFont = self.parentViewController.view.frame.size.width/dummyView.frame.size.width;
        
        //CGFloat factor = 977.0/1024.0;
        
        CGFloat yPosContainer = 0;
        
        if ([_controller isEqualToString:@"QuoteScreen"]) {
            QuoteScreenViewController *qsCtrl = (QuoteScreenViewController*)self.parentViewController;
            yPosContainer = qsCtrl.stockContainerView.frame.origin.y;
        }
//        else  if ([_controller isEqualToString:@"OrderBook"]) {
//            OrderBookandPortfolioViewController *qsCtrl = (OrderBookandPortfolioViewController*)self.parentViewController;
//            yPosContainer = qsCtrl.stkPreviewContainer.frame.origin.y;
//        }
        
        CGRect enlargedFrame = CGRectMake(-(_parentViewFrame.size.width-self.view.frame.size.width),
                                          -yPosContainer,
                                          _parentViewFrame.size.width,
                                          _parentViewFrame.size.height);
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            
            dummyView.frame = enlargedFrame;
            _chartView.hidden = YES;
            _chartView.frame = enlargedFrame;
            UIButton *tmp = (UIButton*)[self.chartView viewWithTag:51];
            scaleFont = scaleFont*tmp.titleLabel.font.pointSize;
            //NSLog(@"Enlarge: %f",scaleFont);
            [self setButtonFontScale:scaleFont];
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethodForDays:currentTotDays];
        } completion:^(BOOL finished) {
            _chartView.hidden = NO;
            [dummyView removeFromSuperview];
            isFullScreenChart= YES;
            [self.delegate chartDidEnlarge:YES];
            [self.mainView bringSubviewToFront:_chartView];
        }];
        
        
    } else {
        
        [self.delegate chartWillShrink];
        
        
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        dummyView.frame = _chartView.frame;
        dummyView.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:dummyView];
        __block CGFloat scaleFont = webViewOriginalFrame.size.width/dummyView.frame.size.width;
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            
            _chartView.frame = webViewOriginalFrame;
            dummyView.frame = webViewOriginalFrame;
            UIButton *tmp = (UIButton*)[self.chartView viewWithTag:51];
            scaleFont = scaleFont*tmp.titleLabel.font.pointSize;
            [self setButtonFontScale:scaleFont];
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethodForDays:currentTotDays];
            
        } completion:^(BOOL finished) {
              [_btnClose setHidden:NO];
            [dummyView removeFromSuperview];
            isFullScreenChart= NO;
            [self.delegate chartDidEnlarge:NO];
        }];
        
        
    }
}




-(IBAction)loadChart:(id)sender {
    
    
    [_atp timeoutActivity];
    
    UIButton *btn = (UIButton*)sender;
    
    switch (btn.tag) {
        case 51:
            currentTotDays = K_DAY;
            
            break;
        case 52:
            currentTotDays = K_WEEK;
            // [self loadChartMethodForDays:5];//[self openChartOption:2];
            break;
        case 53:
            currentTotDays = K_1MONTH;
            // [self loadChartMethodForDays:20];//[self openChartOption:3];
            break;
        case 54:
            currentTotDays = K_3MONTH;
            // [self loadChartMethodForDays:60];//[self openChartOption:4];
            break;
        case 55:
            currentTotDays = K_6MONTH;
            //[self loadChartMethodForDays:120];//[self openChartOption:5];
            break;
        case 56:
            currentTotDays = K_1YEAR;
            // [self loadChartMethodForDays:240];//[self openChartOption:6];
            break;
        case 57:
            currentTotDays = K_2YEAR;
            // [self loadChartMethodForDays:480];//[self openChartOption:7];
            break;
            
        default:currentTotDays = K_1YEAR;//[self loadChartMethodForDays:200];//[self openChartOption:5];
            break;
    }
    [self loadChartMethodForDays:currentTotDays];//[self openChartOption:1];
}





-(void)setChartStatus:(long)status {
    //success
    if (status==1) {
        [_chartErrorMsg setHidden:YES];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:NO];
    }
    
    //no url
    if (status==2) {
      [_chartErrorMsg setHidden:NO];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // error
    if (status==3) {
        [_chartErrorMsg setHidden:NO];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // loading
    if (status==4) {
       [_chartErrorMsg setHidden:YES];
        [_btnLoading setHidden:NO];
        [_imgChart setHidden:YES];
    }
    
    // init
    if (status==5) {
        [_chartErrorMsg setHidden:YES];
        [_btnLoading setHidden:YES];
        [_imgChart setHidden:YES];
        
    }
}


-(void)loadChartMethodForDays:(long)days {
    
    for(UIView *v in [self.chartView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            if (btn.tag>50) {
                [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            }
        }
    }
    
    switch (days) {
        case K_DAY:
            [[_chartView viewWithTag:51] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_WEEK:
            [[_chartView viewWithTag:52] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_1MONTH:
            [[_chartView viewWithTag:53] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_3MONTH:
            [[_chartView viewWithTag:54] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_6MONTH:
            [[_chartView viewWithTag:55] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_1YEAR:
            [[_chartView viewWithTag:56] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        case K_2YEAR:
            [[_chartView viewWithTag:57] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        default:  [[_chartView viewWithTag:56] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
    }
    
    NSString *chartTypeToLoad = [UserPrefConstants singleton].chartTypeToLoad;
    
    if (days==K_DAY) {
        chartTypeToLoad = @"Image";
    }
    
    
    NSString *resolvedChartURL = [UserPrefConstants singleton].interactiveChartURL;
    
//    // override for HK N2N
//    if ([[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"HK"]||[[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"HKD"]) {
//        
//        chartTypeToLoad = @"Modulus";
//        resolvedChartURL = @"https://poc.asiaebroker.com/mchart/index_POC.jsp?";
//    }
    
    // chartTypeToLoad = @"Modulus";
    
    // ========================================
    //                MODULUS
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Modulus"]) {
        
        
        [self setChartStatus:5];
        
        NSString *url = [NSString stringWithFormat:@"%@exchg=%@&code=%@&color=b&view=f&amount=%ld",
                        resolvedChartURL,
                         [stkCodeAndExchangeArr objectAtIndex:1],[stkCodeAndExchangeArr objectAtIndex:0],days];
        
        // NSLog(@"%@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        _webView.scrollView.scrollEnabled = NO;
        _webView.scrollView.bounces = NO;
        [_webView loadRequest:request];
        
        [_imgChart setHidden:YES];
        [_webView setHidden:NO];
    }
    
    
    // ========================================
    //                IMAGE
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Image"]) {
        
        [self setChartStatus:4];
        
        NSString *chartURL = [UserPrefConstants singleton].historicalChartURL;
        
        if (currentTotDays==1) {
            chartURL = [UserPrefConstants singleton].intradayChartURL;
        } else {
            chartURL = [UserPrefConstants singleton].historicalChartURL;
        }
        
        NSString *urlString = [NSString stringWithFormat:@"%@&w=%.0f&h=%.0f&k=%@&c=b&d=%ld",chartURL,
                               _imgChart.frame.size.width, _imgChart.frame.size.height, _stkCode,days];
        
        // NSLog(@"%@",urlString);
        
        ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
        [imgChartManager getImageChartWithURL:urlString
                            completedWithData:^(NSData *data) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIImage *chartImg = [UIImage imageWithData:data];
                                    _imgChart.image = chartImg;
                                    [self setChartStatus:1];
                                });
                            }
                                      failure:^(NSString *errorMsg) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self setChartStatus:3];
                                          });
                                      }];
        
        
        [_imgChart setHidden:NO];
        [_webView setHidden:YES];
    }
    
    
    // ========================================
    //                TELETRADER
    // ========================================
    
    
    if ([chartTypeToLoad isEqualToString:@"TeleTrader"]) {
        
        [self setChartStatus:5];
        
        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
        NSString *url = [NSString stringWithFormat:@"%@code=%@&Name=%@&exchg=%@&mode=d&color=b&lang=en&key=%@&amount=%ld",
                         resolvedChartURL,
                         [splitStkCode objectAtIndex:0],
                         [splitStkCode objectAtIndex:1],
                         [UserPrefConstants singleton].userCurrentExchange,
                         [[UserPrefConstants singleton] encryptTime], days];
        //NSLog(@"%@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        _webView.scrollView.scrollEnabled = NO;
        _webView.scrollView.bounces = NO;
        [_webView loadRequest:request];
        
        [_imgChart setHidden:YES];
        [_webView setHidden:NO];
    }
    
}


#pragma mark - Passive Fundamentals
//Type 1 - Company Info
//Type 2 - Financial Info
- (NSString *) getFundamentalDataURL:(int)type andCategory:(NSString *)category {
    
    NSString *fundamentalLabel = [UserPrefConstants singleton].fundamentalNewsLabel;
    NSArray *codeComponents = [[UserPrefConstants singleton].userSelectingStockCode componentsSeparatedByString:@"."];
    
    NSString *trade_exchg_code = [UserPrefConstants singleton].userCurrentExchange;
    NSString *strip_stock_code = @"";
    NSString *broker_code = @"";
    
    if ([codeComponents count]>1) {
        strip_stock_code = [codeComponents objectAtIndex:0]; // 0108.KL = get 0108
    }
    
    broker_code = [UserPrefConstants singleton].brokerCode;
    
    //NSLog(@"fundamentalNewsServer %@",[UserPrefConstants singleton].fundamentalNewsServer);
    
    if (type == 1) { // Company Info
        
        if ([fundamentalLabel length]>0) {
            
            return [NSString stringWithFormat:@"%@stkCompInfom.jsp?SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundamentalNewsServer,
                    [UserPrefConstants singleton].fundamentalSourceCode,
                    trade_exchg_code,
                    strip_stock_code,
                    category,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
        }
        else
            return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Category=%@&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundDataCompInfoURL,
                    @"CPIQ",
                    trade_exchg_code,
                    strip_stock_code,
                    category,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
    }
    else if (type == 2) { // Financial Report
        if ([fundamentalLabel length]>0) {
            
            return [NSString stringWithFormat:@"%@stkFincInfom.jsp?SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=IS&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundamentalNewsServer,
                    [UserPrefConstants singleton].fundamentalSourceCode,
                    trade_exchg_code,
                    strip_stock_code,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
        }
        else
            return [NSString stringWithFormat:@"%@SourceCode=%@&ExchCode=%@&StkCode=%@&Report=Annual&DataItemType=INC&Sponsor=%@&BHCode=%@",
                    [UserPrefConstants singleton].fundFinancialInfoURL,
                    @"CPIQ",
                    trade_exchg_code,
                    strip_stock_code,
                    [UserPrefConstants singleton].SponsorID,
                    broker_code];
    }
    
    return @"";
}

#pragma mark - Responsive Fundamentals (added Dec 2016)

//StkCode=7054.KL&SourceCode=FCS&Sponsor=59&bhCode=086&loginID=n2nuser&prodId=TCMOBILE
// &Type=5&key=%01%05%03%00%06%01&PackageID=0&Menu=N

/*Implement 23 Dec 2016
 
 PackageID
 To open specific package.
 √ 0 = Company Info
 √ 1 = Company Key Person
 √ 2 = Shareholding Summary
 √ 3 = Announcement
 √ 4 = Financial Report
 X 5 = Estimates
 X 6 = Supply Chain
 X 7 = Market Activity
 
 
 Menu
 To control hide/show header and menu bar
 Value put as "N" to hide, default will show
 
 */




-(NSString*)getResponsiveFundamentalDataURLWithPackageID:(int)PackageID {
    return [NSString stringWithFormat:@"%@rw.jsp?StkCode=%@&SourceCode=%@&Sponsor=%@&bhCode=%@&loginID=%@&prodId=TCMOBILE&Type=5&key=%@&PackageID=%d&Menu=N",
            [UserPrefConstants singleton].fundamentalNewsServer,
            _stkCode,
            [UserPrefConstants singleton].fundamentalSourceCode,
            [UserPrefConstants singleton].SponsorID,
            [UserPrefConstants singleton].brokerCode,
            [UserPrefConstants singleton].userName,
            [[UserPrefConstants singleton] encryptTime],
            PackageID];
    
}

- (IBAction) estimatesButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:5];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Estimates"]];
}


- (IBAction) companySynopsisButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
    //NSString *url = [self getFundamentalDataURL:1 andCategory:@"Synopsis"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:0];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Company Synopsis"]];
}


- (IBAction) companyInfoButtonPressed:(id)sender {
    
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Info"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:0];
    
    
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Company Info"]];
    
}

- (IBAction) companySupplyChainButtonPressed:(id)sender {
    
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Info"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:6];
    
    
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Supply Chain"]];
    
}


- (IBAction) companyMarketActivityButtonPressed:(id)sender {
    
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Info"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:0];
    
    
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Market Activity"]];
    
}

- (IBAction) companyKeyPersonButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"KeyPerson"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:1];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Company Key Persons"]];
}


- (IBAction) announcementButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"Announcement"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:3];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Announcements"]];
    
}

- (IBAction) financialReportButtonPressed:(id)sender {
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:2 andCategory:nil];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:4];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Financial Report"]];
    
}


- (IBAction) shareholdingSummaryButtonPressed:(id)sender {
    
    [_atp timeoutActivity];
    
    // NSString *url = [self getFundamentalDataURL:1 andCategory:@"ShareHolders"];
    NSString *url = [self getResponsiveFundamentalDataURLWithPackageID:2];
    [self openNews:url withTitle:[LanguageManager stringForKey:@"Shareholding Summary"]];
    
}






-(void)createButtonAndAddToArray:(NSMutableArray*)array withLabelToArray:(NSMutableArray*)lblArray
                  withImageNamed:(NSString*)imageName andTag:(long)mytag andLabel:(NSString*)title andSelector:(SEL)selector {
    
    CGFloat buttonWidth = _fundamentalView.frame.size.width/9.5;
    CGFloat labelWidthFactor = 2.2;
    CGFloat labelHeight = 45;
    
    UIButton *eachButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [eachButton setFrame:CGRectMake(0, 0, buttonWidth, buttonWidth)];
    [eachButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [eachButton setTag:mytag];
    [eachButton addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [array addObject:eachButton];
    
    
    UILabel *eachLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, buttonWidth*labelWidthFactor, labelHeight)];
    [eachLabel setText:title];
    if ([title isEqualToString:[LanguageManager stringForKey:@"Announcements"]]) {
        [eachLabel setNumberOfLines:1];
        [eachLabel setAdjustsFontSizeToFitWidth:YES];
        eachLabel.minimumScaleFactor = 0.5;
    } else {
        [eachLabel setAdjustsFontSizeToFitWidth:NO];
        [eachLabel setNumberOfLines:3];
    }
    [eachLabel setTag:mytag-10];
    [eachLabel setTextAlignment:NSTextAlignmentCenter];
    UIFont *font = [UIFont systemFontOfSize:12];
    [eachLabel setFont:font];
    [eachLabel setTextColor:[UIColor whiteColor]];
    [lblArray addObject:eachLabel];
    
}


-(void)populateFundamentals {
    
    
    NSString *fundTitle = [UserPrefConstants singleton].fundamentalNewsLabel;
    if ([fundTitle length] <= 0) fundTitle = [LanguageManager stringForKey:@"Fundamentals"];
    _fundamentalTitle.text = fundTitle;
    
    
    currentEd = [[ExchangeData alloc] init];
    BOOL data = NO;
    
    UserPrefConstants *_userPref = [UserPrefConstants singleton];
    
    for (ExchangeData *ed in [_userPref userExchagelist]) {
        
        if ([ed.feed_exchg_code isEqual:[_userPref userCurrentExchange]]) {
            currentEd = ed;
            data = YES;
        }
        
    }
    
    if (data) {
        
        // if either one is enabled, then load it up
        if (currentEd.subsCPIQAnnounce||currentEd.subsCPIQCompInfo||currentEd.subsCPIQCompSynp
            ||currentEd.subsCPIQCompKeyPer||currentEd.subsCPIQShrHoldSum||currentEd.subsCPIQFinancialRep) {
            
            
            _fundamentalView.frame =CGRectMake(_fundamentalView.frame.size.width*5, 0, _newsView.frame.size.width, _newsView.frame.size.height);
            _detailScroll.contentSize = CGSizeMake(_detailScroll.frame.size.width*6, _detailScroll.frame.size.height);
            

            [_fundamentalView setHidden:NO];
            _pgControl.numberOfPages = 6;
            
            // create buttons based on number of factset enabled
            
            NSMutableArray *btnArray = [[NSMutableArray alloc] init];
            NSMutableArray *lblArray = [[NSMutableArray alloc] init];
            
            NSString *source = [UserPrefConstants singleton].fundamentalSourceCode;
            
            if (currentEd.subsCPIQCompSynp) {
                
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_Synopsis" andTag:51 andLabel:[LanguageManager stringForKey:@"Company Info"] andSelector:@selector(companyInfoButtonPressed:)];
                
            }
            
            if (currentEd.subsCPIQCompKeyPer) {
                
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_KeyPerson" andTag:54 andLabel:[LanguageManager stringForKey:@"Company Key Persons"] andSelector:@selector(companyKeyPersonButtonPressed:)];
                
            }
            
            if (currentEd.subsCPIQShrHoldSum) {
                if (![source isEqualToString:@"TRKD"])
                    [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_ShareHolding" andTag:55 andLabel:[LanguageManager stringForKey:@"ShareHolding Summary"] andSelector:@selector(shareholdingSummaryButtonPressed:)];
            }
            
            
            if (currentEd.subsCPIQAnnounce) {
                if (![source isEqualToString:@"TRKD"])
                    [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_Announcement" andTag:53 andLabel:[LanguageManager stringForKey:@"Announcements"] andSelector:@selector(announcementButtonPressed:)];
                
            }
            
            
            if (currentEd.subsCPIQFinancialRep) {
                
                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_FinReport" andTag:56 andLabel:[LanguageManager stringForKey:@"Financial Report"] andSelector:@selector(financialReportButtonPressed:)];
                
            }
            
//            if (currentEd.subsCPIQCompInfo) {
//                
//                [self createButtonAndAddToArray:btnArray withLabelToArray:lblArray withImageNamed:@"ButIcon_CompInfo" andTag:52 andLabel:@"Estimates" andSelector:@selector(estimatesButtonPressed:)];
//                
//            }
            
            //add buttons in orderly fashion
            
            long buttonsPerRow = 3;
            
            
            CGSize scrSize = _fundamentalView.frame.size;
            
            CGFloat buttonWidth = _fundamentalView.frame.size.width/9.5;
            
            CGFloat rowGapFactor = 2.2; // adjust the vertical gap between each rows
            
            CGFloat buttonHeight = buttonWidth;
            
            CGFloat mostBottomYPos = 0.0; // to resize scrollview content if buttons placed too far down
            
            CGFloat buttonWidthPerRow = buttonsPerRow * buttonWidth;
            CGFloat spacePerRow = scrSize.width - buttonWidthPerRow;
            CGFloat eachSpaces = spacePerRow / (buttonsPerRow+1);
            
            CGFloat xOffset = eachSpaces + buttonWidth/2.0;
            CGFloat yOffset = eachSpaces;
            
            long yVal = 1;
            long xVal = 0;
            
            for (long i=0; i<[btnArray count]; i++) {
                xVal += 1;
                
                UIButton *eachButton = [btnArray objectAtIndex:i];
                UIButton *eachLabel = [lblArray objectAtIndex:i];
                
                [_fundamentalView addSubview:eachButton];
                [_fundamentalView addSubview:eachLabel];
                
                [eachButton setCenter: CGPointMake(xOffset, yOffset)];
                [eachLabel setCenter:CGPointMake(xOffset,
                                                 eachButton.center.y+eachButton.frame.size.height/2.0+eachLabel.frame.size.height/2.0)];
                
                
                
                if (i==[btnArray count]-1) {
                    
                    mostBottomYPos =  eachLabel.center.y + eachButton.frame.size.height*1.2;
                    //NSLog(@"%f", mostBottomYPos);
                }
                
                
                xOffset += eachSpaces + buttonWidth;
                
                
                if (xVal % buttonsPerRow==0) {
                    xOffset = eachSpaces + buttonWidth/2.0;
                    yVal += 1;
                    xVal = 0;
                    yOffset += buttonHeight*rowGapFactor;
                }
                
            }
            
        } else {
            // do nothing
            
        }
        
        
    }
    
    
}


-(void)getTimeAndSalesOrBDone {
    if (_pgControl.currentPage==2) {
        [self refreshTimeSales];
    } else if (_pgControl.currentPage==3) {
        [self refreshBizDone];
    } else {
        ; // do nothing
    }
}



#pragma mark - Biz Done

-(void)refreshBizDone {
    
    if (canRefreshBizDone) {
        canRefreshBizDone = NO;
        [self performSelector:@selector(getBizDone) withObject:nil afterDelay:1.0];
    }
}


-(void)getBizDone {
    
    //NSLog(@"get biz done");
    
    [[VertxConnectionManager singleton]
     vertxGetBusinessDoneByStockCode:_stkCode exchg:[UserPrefConstants singleton].userCurrentExchange];
    
}


-(void)didFinishedBusinessDone:(NSNotification*)notification {
    
    //NSLog(@"biz done done");
    
    [self.refreshControl endRefreshing];
    
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"data"];

    if ([data count]>4)
        _bizDoneTableDataArr = [NSMutableArray arrayWithArray:[data subarrayWithRange:NSMakeRange(0, [data count]-4)]];
    
    canRefreshBizDone = YES;
    [_bizDoneTable reloadData];
    
    NSDateFormatter *dateFormatterscoreboard = [[NSDateFormatter alloc] init];
    [dateFormatterscoreboard setDateFormat:@"HH:mm:ss"];
    NSDate *currentTime = [NSDate date];
    
    _bizDoneLastUpdate.text = [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];
}


#pragma mark - Time&Sales

-(void)refreshTimeSales {
    
    if (canRefreshTimeSales) {
        canRefreshTimeSales = NO;
        [self performSelector:@selector(getTimeAndSales) withObject:nil afterDelay:1.0];
    }
}


- (void)getTimeAndSales
{
    NSString *tranNo;
    tranNo = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode ]objectForKey:FID_103_I_TRNS_NO];
    [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:_stkCode begin:@"0"
                            end:tranNo exchange:[UserPrefConstants singleton].userCurrentExchange];
}


- (void)didFinishedTimeAndSales:(NSNotification *)notification
{
    
    canRefreshTimeSales = YES;
        [self.refreshControl endRefreshing];

        NSDictionary * response = [notification.userInfo copy];
        NSArray *data =[response objectForKey:@"data"];
        _timeSalesArr = [NSMutableArray arrayWithArray:data ];
        
        //   NSLog(@"_timeAndSalesTableDataArr %@", _timeAndSalesTableDataArr);
        
        [_timeSalesTable reloadData];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *currentTime = [NSDate date];
        _timeSalesLastUpdate.text = [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatter stringFromDate: currentTime]}];
    
}

#pragma mark - News
- (void) openNews:(NSString *)url withTitle:(NSString*)title{
    
    [_atp timeoutActivity];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
    vc.view.backgroundColor = [UIColor clearColor];
    vc.newsTitleLabel.text = title;
    
    if (![title isEqualToString:[LanguageManager stringForKey:@"Stock News"]]) {
        vc.currentEd = currentEd;
         vc.companyNameLabel.text = [[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_39_S_COMPANY];
    } else {
        vc.currentEd = nil;
       
    }
    
    [vc openUrl:url withJavascript:YES];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

// ARCHIVE NEWS FROM ATP
-(void)didFinishGetArchiveNews:(NSNotification*)notification {
    
    NSArray *newsArray = [notification.userInfo objectForKey:@"News"];
    
    
    if ([newsArray count]>0) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *newsDat = [NSArray arrayWithArray:newsArray];
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"dt" ascending: NO];
            _newsArcArr =  [NSMutableArray arrayWithArray:[newsDat sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]]];
            
            [_newsTable reloadData];
        });
    } else {
        //news not available
        dispatch_async(dispatch_get_main_queue(), ^{
            [_newsTable reloadData];
        });
    }
    
}

-(void)didFinishGetElasticNews:(NSNotification*)notification {
    
    
    
}

// NORMAL NEWS FROM VERTX
- (void)didFinishedGetStockNews:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    // ////NSLog(@"didFinishedGetStockNews response : %@",response);
    
    NSArray *data =[response objectForKey:@"data"];
    //    NSArray *reversed = [[[data sortedArrayUsingSelector:@selector(compare:)] reverseObjectEnumerator] allObjects];

    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *dataArr = [data copy];
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"4" ascending: NO];
        
        _newsArr =  [NSMutableArray arrayWithArray:[dataArr sortedArrayUsingDescriptors:[NSArray arrayWithObject: sortOrder]]];
       // NSLog(@"newsArr %@",_newsArr);
        [_newsTable reloadData];
    });
    
}

- (void)getCompanyNews
{
    if ([UserPrefConstants singleton].isElasticNews) {
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetElasticNews:) name:@"didFinishGetElasticNews" object:nil];
         [_atp getElasticNewsForStocks:_stkCode forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
    }else if ([UserPrefConstants singleton].isArchiveNews) {
        //NSLog(@"archive");
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetArchiveNews:) name:@"didFinishGetArchiveNews" object:nil];
        [_atp getArchiveNewsForStock:_stkCode forDays:30];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedGetStockNews:) name:@"didFinishedGetStockNews" object:nil];
        [[VertxConnectionManager singleton] vertxCompanyNews:_stkCode];
    }
}

#pragma mark - Market Depth


-(void)updateFooterData {
    
    
    
    long long totalBuy = 0;
    long long totalSell = 0;
    
    long totBuySplit = 0; long long totBuyQty = 0;  CGFloat aveBuyBid = 0;
    long totSellSplit = 0; long long totSellQty = 0; CGFloat aveSellAsk = 0;
    
    
    for (long i=0; i<[_MDBuyArr count]; i++)  {
        NSDictionary *buyData = [_MDBuyArr objectAtIndex:i];
        
        long eachBuySplit = [[buyData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
        totBuySplit += eachBuySplit;
        
        long long eachBuyQty = [[buyData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
        totBuyQty += eachBuyQty;
        
        CGFloat eachBuyBid = [[buyData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
        aveBuyBid += eachBuyBid*eachBuyQty;
        
        totalBuy += eachBuyQty*eachBuyBid;
        
    }
    
    NSLog(@"_MDSellArr %@",_MDSellArr);
    
     for (long i=0; i<[_MDSellArr count]; i++)  {
         
         NSDictionary *sellData = [_MDSellArr objectAtIndex:i];
         
         long eachSellSplit = [[sellData objectForKey:FID_170_I_BUY_SPLIT_1]longValue];
         totSellSplit += eachSellSplit;
         
         long long eachSellQty = [[sellData objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
         totSellQty += eachSellQty;
         
         CGFloat eachSellAsk = [[sellData objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
         aveSellAsk += eachSellAsk*eachSellQty;
         
         
         totalSell += eachSellQty*eachSellAsk;
         
     }
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    if (totBuyQty<=0) aveBuyBid=0; else aveBuyBid = aveBuyBid/totBuyQty;
    if (totSellQty<=0) aveSellAsk=0; else aveSellAsk = aveSellAsk/totSellQty;
    
    
    _totBidSplit.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totBuySplit]];
    _totBidQty.text =  [[UserPrefConstants singleton] abbreviateNumber:round(totBuyQty)];
    _totBid.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveBuyBid/[_MDBuyArr count]]];
    
    _totAskSplit.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totSellSplit]];
    _totAskQty.text =  [[UserPrefConstants singleton] abbreviateNumber:round(totSellQty)];
    _totAsk.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:aveSellAsk/[_MDSellArr count]]];
    
    _totalBidLabel.text = [LanguageManager stringForKey:@"Total Bid: %@"
                                       withPlaceholders:@{@"%totBid%":[quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totalBuy]]}];
    _totalAskLabel.text = [LanguageManager stringForKey:@"Total Ask: %@"
                                       withPlaceholders:@{@"%totAsk%":[quantityFormatter stringFromNumber:[NSNumber numberWithFloat:totalSell]]}];
    

    
}


-(void)updateMktDepthAndFlash {
    
    // compare prevMDArr and marketDepthArr value
    
    // can compare only if both arrays has the same amount
    
    if (mktDepthZero) return;
    
    [self updateFooterData];
    
    if ([_MDBuyArr count]==[_MDBuyPrevArr count]) {
        
        // then flash as needed
        for (long i=0; i<[_MDBuyArr count]; i++) {
            
            NSDictionary *buyData1 = [_MDBuyArr objectAtIndex:i];
            NSDictionary *buyData2 = [_MDBuyPrevArr objectAtIndex:i];
            
            
            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[buyData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[buyData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[buyData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[buyData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        }
        
    }
    
    if ([_MDSellArr count]==[_MDSellPrevArr count]) {
        NSLog(@"_MDSellArr %@",_MDSellArr);
        // then flash as needed
        for (long i=0; i<[_MDSellArr count]; i++) {
            
            NSDictionary *sellData1 = [_MDSellArr objectAtIndex:i];
            NSDictionary *sellData2 = [_MDSellPrevArr objectAtIndex:i];
            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]>[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]<[[sellData2 objectForKey:FID_170_I_BUY_SPLIT_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]>[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_58_I_BUY_QTY_1] floatValue]<[[sellData2 objectForKey:FID_58_I_BUY_QTY_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]>[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([[sellData1 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]<[[sellData2 objectForKey:FID_68_D_BUY_PRICE_1] floatValue]) {
                [_mktDepthTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        }
        
    }
    
    _MDBuyPrevArr = [NSMutableArray arrayWithArray:_MDBuyArr];
    _MDSellPrevArr = [NSMutableArray arrayWithArray:_MDSellArr];
}




-(void)didFinishMarketDepth:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    _mktDepthArr = [response objectForKey:@"marketdepthresults"];
    
    
    mktDepthZero = NO;
    
     NSLog(@"mktDepthArr %@",_mktDepthArr);
    
    NSMutableArray *tmpBuyArr = [[NSMutableArray alloc] init];
    NSMutableArray *tmpSellArr = [[NSMutableArray alloc] init];
    
    
    int MDLfeature = ([mdInfo.isMDL isEqualToString:@"Y"])?1:0;
    
    
    // Non Feature md data is all in single dictionary, so we convert it to same
    // format as featured MD.
    if (MDLfeature==0) {
        
        // if server give any data
        if ([_mktDepthArr count]>0) {

        
            // non MDL - all mkt depth data is in one object (max 10 level only)
            NSDictionary *stkDataWithMDDict = [_mktDepthArr objectAtIndex:0];
            
            
            int fid_buy_split_start = 170;
            int fid_sell_split_start = 180;
            int fid_buy_qty_start = 58;
            int fid_sell_qty_start = 78;
            int fid_buy_price_start = 68;
            int fid_sell_price_start = 88;
            
            
            // populate tmpBuyArr (loop 10 and if found, create it and add)
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *buyMD = [[NSMutableDictionary alloc] init];
                
                NSString *buySplitKey = [NSString stringWithFormat:@"%d",fid_buy_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buySplitKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buySplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *buyPriceKey = [NSString stringWithFormat:@"%d",fid_buy_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyPriceKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *buyQtyKey = [NSString stringWithFormat:@"%d",fid_buy_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:buyQtyKey]) {
                    [buyMD setObject:[stkDataWithMDDict objectForKey:buyQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[buyMD allKeys] count]>0) {
                    // set FID 107
                    [buyMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [buyMD setObject:[NSNumber numberWithInt:1] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    
                    [tmpBuyArr addObject:buyMD];
                }
            }
            
            // NSLog(@"tmpBuyArr %@", tmpBuyArr);
            
            // populate tmpSellArr
            for (int i=0; i<10; i++)
            {
                NSMutableDictionary *sellMD = [[NSMutableDictionary alloc] init];
                
                NSString *sellSplitKey = [NSString stringWithFormat:@"%d",fid_sell_split_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellSplitKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellSplitKey] forKey:FID_170_I_BUY_SPLIT_1];
                }
                
                NSString *sellPriceKey = [NSString stringWithFormat:@"%d",fid_sell_price_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellPriceKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellPriceKey] forKey:FID_68_D_BUY_PRICE_1];
                }
                
                NSString *sellQtyKey = [NSString stringWithFormat:@"%d",fid_sell_qty_start+i];
                if ([[stkDataWithMDDict allKeys] containsObject:sellQtyKey]) {
                    [sellMD setObject:[stkDataWithMDDict objectForKey:sellQtyKey] forKey:FID_58_I_BUY_QTY_1];
                }
                
                if ([[sellMD allKeys] count]>0) {
                    // set FID 107
                    [sellMD setObject:[NSNumber numberWithInt:i] forKey:FID_107_I_MARKET_DEPTH_POSITION];
                    // set FID 106
                    [sellMD setObject:[NSNumber numberWithInt:2] forKey:FID_106_I_BID_OR_ASK_INDICATOR];
                    [tmpSellArr addObject:sellMD];
                }
            }
            
            
        } else {
            ; // empty data = do nothing (will be handled below)
        }
        
        
    } else if (MDLfeature==1) {
        // MDL type - mkt depth data in separate objects
        
        // if server give any data
        if ([_mktDepthArr count]>0) {
        
            for (int i=0; i<[_mktDepthArr count]; i++) {
                
                NSDictionary *eachMDdata = [_mktDepthArr objectAtIndex:i];
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==1) {
                    // BID
                    [tmpBuyArr addObject:eachMDdata];
                }
                if ([[eachMDdata objectForKey:FID_106_I_BID_OR_ASK_INDICATOR] integerValue]==2) {
                    // SELL
                    [tmpSellArr addObject:eachMDdata];
                }
            }
            
        } else {
            ; // empty data = do nothing  (will be handled below)
        }
        
    }
    
    //NSLog(@"tmpBuycount %d",[tmpBuyArr count]);
    
    // if tmpBuyArr empty, prepare zeroes
    if ([tmpBuyArr count]<=0) {
        int mdLevel = [mdInfo.mdLevel intValue];
        
        // NSLog(@"mdLevel %d",mdLevel);
        
        int alternate = 1;
        int pos = 0;
        for (long i=0; i<mdLevel*2; i++) {
            NSDictionary *zeroDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:2],@"105",
                                      [NSNumber numberWithInt:alternate],@"106",
                                      [NSNumber numberWithInt:pos],@"107",
                                      [NSNumber numberWithInt:0],@"118",
                                      @"KL",@"131",
                                      [NSNumber numberWithInt:0],@"170",
                                      @"5517.KL",@"33",
                                      @"0",@"45",
                                      [NSNumber numberWithInt:0],@"58",
                                      [NSNumber numberWithInt:0],@"68",
                                      nil];
            
            if (alternate==1) {
                [tmpBuyArr addObject:zeroDict];
                alternate = 2;
                pos++;
            } else {
                [tmpSellArr addObject:zeroDict];
                alternate = 1;
            }
        }
        
    }
    
    
    // sort data based on FID 107
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_107_I_MARKET_DEPTH_POSITION ascending:YES];
    NSArray *limitedArrayB = [tmpBuyArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSArray *limitedArrayS = [tmpSellArr sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    [tmpBuyArr removeAllObjects]; [tmpSellArr removeAllObjects];
    
    //NSLog(@"limitedArrayB %@",limitedArrayB);
    //NSLog(@"limitedArrayS %@",limitedArrayS);
    
    if ([limitedArrayB count]>0) {
//        
//        [_mdFooter setHidden:NO];
        _MDBuyArr = [NSMutableArray arrayWithArray:[limitedArrayB mutableCopy]];
        _MDSellArr = [NSMutableArray arrayWithArray:[limitedArrayS mutableCopy]];
        [_mktDepthTable reloadData];
        [self updateFooterData];
        
        // then send subscribe data (subscribe only if there is mktdepth)
        // TODO: subscribe and flashing 
       // [[VertxConnectionManager singleton] vertxSubscribeMktDepthLevel:MDLfeature withStockCode:_stkCode];
    }

    
    
}


- (void)getMarketDepth
{
    NSArray* arrayWithTwoStrings = [_stkCode componentsSeparatedByString:@"."];
    
    if (_stkCode !=nil) {
        
        //NSLog(@"stkCode %@ coll %@",_stkCode, [arrayWithTwoStrings objectAtIndex:1]);
        [[VertxConnectionManager singleton] vertxGetMarketDepth:_stkCode andCollarr:[arrayWithTwoStrings objectAtIndex:1]];
        
    }
    
}


#pragma mark - Trade

-(void) doBlock:(void (^)(int))stkPreviewClick
{
    localBlock = stkPreviewClick;
}

- (IBAction)closeStkPreview:(id)sender {
    [self.delegate closePreviewBtnClicked];
}

-(void) fnControlButtons :(BOOL) enable
{
    [_btnSell setUserInteractionEnabled:enable];
    [_btnBuy setUserInteractionEnabled:enable];
    if (enable) {
        [_btnBuy setAlpha:1.0];
        [_btnSell setAlpha:1.0];
    } else {
        [_btnBuy setAlpha:0.4];
        [_btnSell setAlpha:0.4];
        
    }
}

-(IBAction)goTrade:(UIButton*)sender {
    // detects button sender and set SELL/BUY accordingly
    //
    [self fnControlButtons:NO];
    
    [_atp timeoutActivity];
    
  //  [self getMarketDepth];
    
    localBlock((int)sender.tag);
    
    UIButton *btn = (UIButton *)sender;
    
    if (btn==_btnBuy) {

        NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
        [notificationData setObject:_stkCode forKey:@"stkCode"];
        
        [UserPrefConstants singleton].userSelectingStockCode = _stkCode;
        
        [UserPrefConstants singleton].callBuySellReviseCancel = 1;
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
    
    }
    
    if (btn==_btnSell) {
        
        NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
        [notificationData setObject:_stkCode forKey:@"stkCode"];
        
        [UserPrefConstants singleton].userSelectingStockCode = _stkCode;
        
        [UserPrefConstants singleton].callBuySellReviseCancel = 2;
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
        
    }
    

//    NSDictionary *dict;
//    QCData *_qcData = [QCData singleton];
//    NSString *stockCode = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_33_S_STOCK_CODE];
//    NSString *stockName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_38_S_STOCK_NAME];

    
    
    
//    TradingRules *trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
//    
//    if (btn==_btnBuy) {
//        
//        dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                @"BUY",@"Action",
//                stockCode,@"StockCode",
//                stockName,@"StockName",
//                [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
//                [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
//                [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"],@"DefaultPrice",
//                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], stockName],@"stkNname",
//                trdRules, @"TradeRules",
//                nil];
//        
//        
//    } else if (btn==_btnSell)  {
//        
//        dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                @"SELL",@"Action",
//                stockCode,@"StockCode",
//                stockName,@"StockName",
//                [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
//                [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
//                [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"],@"DefaultPrice",
//                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], stockName],@"stkNname",
//                trdRules, @"TradeRules",
//                nil];
//        
//        
//    } else {
//        
//        NSLog(@"Unknown Sender");
//    }
//    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    TraderViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TraderViewController"];
//    vc.actionTypeDict = dict;
//    vc.fromStockDetail = NO;
//    [vc setTransitioningDelegate:transitionController];
//    vc.modalPresentationStyle= UIModalPresentationCustom;
//    [self presentViewController:vc animated:NO completion:nil];
    
}



#pragma mark - QuoteScreenDelegate

-(void)qcFeedDidUpdateForSelectedStock {
    // flash the data changed
    isUpdate = YES;
    [self populateCurrentStockDetail];
}



-(void)stockDidSelected:(NSString *)stkCode andName:(NSString *)stkName watchList:(BOOL)isWatchList {
    
    [self fnControlButtons:TRUE];
    
    [_atp timeoutActivity];
    
    _pgControl.currentPage = 0;
    [_detailScroll setContentOffset:CGPointMake(0, 0)];
    loadedMD = NO;
    loadedTS = NO;
    loadedNews = NO;
    
    [self populateFundamentals];
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedTimeAndSales" object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedGetStockNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishMarketDepth" object:nil];
    [_MDSellArr removeAllObjects];
    [_MDBuyArr removeAllObjects];
    [_MDBuyPrevArr removeAllObjects];
    [_MDSellPrevArr removeAllObjects];
    [_mktDepthArr removeAllObjects];
    [_newsArcArr removeAllObjects];
    [_newsArr removeAllObjects];
    [_timeSalesArr removeAllObjects];
    
   // [_watchListBtn setHidden:isWatchList];
    _stockName.text = stkName;

    _stkCode = stkCode;
    stkCodeAndExchangeArr = [_stkCode componentsSeparatedByString:@"."];
    stkCodeDisplay = [stkCodeAndExchangeArr objectAtIndex:0];
    
    
    [_btnClose setHidden:NO];
    [_stockName setFrame:CGRectMake(_btnClose.frame.origin.x+_btnClose.frame.size.width+10,
                                    _stockName.frame.origin.y,
                                    oriStkNameWidth-_btnClose.frame.size.width-10,
                                    _stockName.frame.size.height)];
    
    //_stockCode.text = stkCodeDisplay;
    
    if ([[[[QCData singleton] qcFeedDataDict]allKeys]containsObject:_stkCode]) {
        // check if ceiling price exist or not. maybe can add more checkings for other fields.
        if ([[[[QCData singleton] qcFeedDataDict] objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]==nil) {
            [_mainView setHidden:YES];
        } else {
        
            [_mainView setHidden:NO];
            [self populateCurrentStockDetail];
            [self loadChartMethodForDays:K_1YEAR];

        }
    } else {
        [_mainView setHidden:YES];
    }
    
    
    
}

#pragma mark - Portfolio delegate (called by Portfolio table item selection) NO LONGER USED

-(void)stockPFDidSelected:(NSString *)stkCode andName:(NSString *)stkName  watchList:(BOOL)isWatchList {
    
    [_atp timeoutActivity];
    
    _pgControl.currentPage = 0;
    [_detailScroll setContentOffset:CGPointMake(0, 0)];
    loadedMD = NO;
    loadedTS = NO;
    loadedNews = NO;
    
     [self populateFundamentals];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedTimeAndSales" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedGetStockNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishMarketDepth" object:nil];
    [_MDSellArr removeAllObjects];
    [_MDBuyArr removeAllObjects];
    [_MDBuyPrevArr removeAllObjects];
    [_MDSellPrevArr removeAllObjects];
    [_mktDepthArr removeAllObjects];
    [_newsArcArr removeAllObjects];
    [_newsArr removeAllObjects];
    [_timeSalesArr removeAllObjects];
    
   // [_watchListBtn setHidden:isWatchList];
    _stockName.text = stkName;
    
    _stkCode = stkCode;
    stkCodeAndExchangeArr = [_stkCode componentsSeparatedByString:@"."];
    stkCodeDisplay = [stkCodeAndExchangeArr objectAtIndex:0];
    
    [_btnClose setHidden:NO];
    [_stockName setFrame:CGRectMake(_btnClose.frame.origin.x+_btnClose.frame.size.width+10,
                                    _stockName.frame.origin.y,
                                    oriStkNameWidth-_btnClose.frame.size.width-10,
                                    _stockName.frame.size.height)];

    //_stockCode.text = stkCodeDisplay;
    
    if ([[[[QCData singleton] qcFeedDataDict]allKeys]containsObject:_stkCode]) {
        [_mainView setHidden:NO];
        [_noDataLbl setHidden:YES];
        [self populateCurrentStockDetail];
        [self loadChartMethodForDays:K_1YEAR];

    } else {
        [_mainView setHidden:YES];
        [_noDataLbl setHidden:NO];
    }
    

    
}

#pragma mark - Stock Details

-(void)setColorForLabel:(UILabel*)label withPrice:(CGFloat)value {
    
    if (value>0.0001) {
        label.textColor = [UIColor greenColor];
    } else if (value<-0.0001) {
        label.textColor = [UIColor redColor];
    }  else {
        label.textColor = [UIColor whiteColor];
    }
}

-(void)populateCurrentStockDetail {

    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    
    // FID_158_S_SYSBOL2_SECNAME
    // FID_159_S_SYSBOL2_COMP
    
    NSString *compName = @"";
//    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
//        compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_159_S_SYSBOL2_COMP];
//    } else {
//        compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_39_S_COMPANY];
//    }
    
    compName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_39_S_COMPANY];
    _stockCode.text = compName;
    
    long long totalVol = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]longLongValue];
    long long totalBuyVol     =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]longLongValue];
    long long totalSellVol    =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]longLongValue];
    long long totalBuyTran    =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]longLongValue];
    long long totalSellTran   =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]longLongValue];
    long long totalMarCap     =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]longLongValue];
    long long totalShareIssued=[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]longLongValue];
    
    CGFloat lacpFloat = 0;
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT]) {
        lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_153_D_THEORETICAL_PRICE]floatValue];
    }else{
        lacpFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue];
    }
    
    _stockLacp.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lacpFloat]];
    
    CGFloat lastDoneFloat = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
    _stockLastDone.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lastDoneFloat]];
    [self setColorForLabel:_stockLastDone withPrice:(lastDoneFloat-lacpFloat)];

    CGFloat floatPercent = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue];
    CGFloat floatChange = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE] floatValue];
    
    if (floatPercent>0) {
        _stockChangePer.textColor = [UIColor greenColor];
        _stockChange.textColor = [UIColor greenColor];
        _stockChangePer.text = [NSString stringWithFormat:@"%.3f%%",floatPercent]; // cqa request no + sign
          if ([UserPrefConstants singleton].pointerDecimal==3) {
              _stockChange.text = [NSString stringWithFormat:@"%.3f",floatChange]; // cqa request no + sign
          }else{
              _stockChange.text = [NSString stringWithFormat:@"%.4f",floatChange]; // cqa request no + sign

          }
    } else if (floatPercent<0) {
        _stockChangePer.textColor = [UIColor redColor];
        _stockChange.textColor = [UIColor redColor];
        _stockChangePer.text = [NSString stringWithFormat:@"%.3f%%",floatPercent];
         if ([UserPrefConstants singleton].pointerDecimal==3) {
             _stockChange.text = [NSString stringWithFormat:@"%.3f",floatChange];
         }else{
             _stockChange.text = [NSString stringWithFormat:@"%.4f",floatChange];

         }
    } else {
        _stockChangePer.textColor = [UIColor whiteColor];
        _stockChange.textColor = [UIColor whiteColor];
        _stockChangePer.text = [NSString stringWithFormat:@"%.3f%%",floatPercent];
         if ([UserPrefConstants singleton].pointerDecimal==3) {
             _stockChange.text = [NSString stringWithFormat:@"%.3f",floatChange];
         }else{
             _stockChange.text = [NSString stringWithFormat:@"%.4f",floatChange];
         }
    }
    
    CGFloat floatOpen = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE] floatValue];
    _stockOpen.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatOpen]];
    [self setColorForLabel:_stockOpen withPrice:(floatOpen-lacpFloat)];
    
    CGFloat floatClose = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE] floatValue];
    _stockPrevClose.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatClose]];
    [self setColorForLabel:_stockPrevClose withPrice:(floatClose-lacpFloat)];
    
    CGFloat floatHigh =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE] floatValue];
    _stockHigh.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatHigh]];
    [self setColorForLabel:_stockHigh withPrice:(floatHigh-lacpFloat)];
    
    CGFloat floatLow =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE] floatValue];
    _stockLow.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatLow]];
    [self setColorForLabel:_stockLow withPrice:(floatLow-lacpFloat)];
    
    CGFloat floatCeil = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue];
    _stockCeiling.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatCeil]];
    [self setColorForLabel:_stockCeiling withPrice:(floatCeil-lacpFloat)];
    
    CGFloat floatFloor = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue];
    _stockFloor.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatFloor]];
    [self setColorForLabel:_stockFloor withPrice:(floatFloor-lacpFloat)];
    
    CGFloat floatBid = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue];
    _stockBid.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatBid]];
    [self setColorForLabel:_stockBid withPrice:(floatBid-lacpFloat)];
    
    CGFloat floatAsk = [[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue];
    _stockAsk.text  = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:floatAsk]];
    [self setColorForLabel:_stockAsk withPrice:(floatAsk-lacpFloat)];
    
    long long valueTradedVal =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]longLongValue];
    _stockValue.text = [[UserPrefConstants singleton]abbreviateNumber:valueTradedVal];
    
    long long tradesVal =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]longLongValue];
    _stockTrades.text = [[UserPrefConstants singleton]abbreviateNumber:tradesVal];
    
    long long bidQtyValue  =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]longLongValue];
    _stockBidQty.text = [[UserPrefConstants singleton]abbreviateNumber:bidQtyValue];
    
    long long askQtyValue  =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]longLongValue];
    _stockAskQty.text = [[UserPrefConstants singleton]abbreviateNumber:askQtyValue];
    
    // FID_158_S_SYSBOL2_SECNAME

    NSString *stockSectorName = @"";
//    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
//        stockSectorName = [[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_158_S_SYSBOL2_SECNAME];
//    } else {
//        stockSectorName = [[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
//    }
    
     stockSectorName = [[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:4];
    
    NSString *stockTradingBoard =[[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_S_PATH_NAME] componentsSeparatedByString:@"|"] objectAtIndex:3];
    NSString *stockTotalSellVolume =[NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]];
    NSString *ISINString = [NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_157_S_ISIN]];
    float stockParString =[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE] floatValue];
    NSString *lotSizeString = [NSString stringWithFormat:@"%@",[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]];
    unichar uc = (unichar)[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_48_S_STATUS] characterAtIndex:1]; //Just extend to 16 bits
    
    ////NSLog(@"stockTotalSellVolume %@ = %@",stockTotalSellVolume,[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]);
    
    // NSLog(@"ISINString %@", ISINString);
    
    CGFloat dur = [stockSectorName length]/10.0;
    
    if (sectorLabel==nil)
        sectorLabel=[[MarqueeLabel alloc]initWithFrame:_stockSector.frame duration:dur andFadeLength:5.0f];
    [sectorLabel setScrollDuration:dur];
    [_detail2View addSubview:sectorLabel];
    
    if ([stockSectorName length]<=0) stockSectorName = @"-";
    sectorLabel.text = stockSectorName;
    [sectorLabel setFont:[UIFont systemFontOfSize:14]];
    [_stockSector removeFromSuperview];
    [sectorLabel setTextColor:[UIColor whiteColor]];
    
    _stockTradingBoard.text = stockTradingBoard;
    _stockLotSize.text = lotSizeString;
    
    if (([stockTotalSellVolume length]<=0)||[stockTotalSellVolume containsString:@"null"]) stockTotalSellVolume = @"-";
    _stockSellVolume.text = stockTotalSellVolume;
    
    
    if (([ISINString length]<=0)||[ISINString containsString:@"null"]) ISINString = @"-";
    _stockISIN.text = ISINString;
    

    _stockParVal.text =  [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:stockParString]];
    
    _stockStatus.text = [[UserPrefConstants singleton] getBoardStatus:[NSString stringWithCharacters:&uc length:1]];
    

    
    /*Status = FID_44_S_STATE
     3. Trading Board = FID_154_S_GROUP
     4. Total S.Sell Volume = "239"
     5. Lot. Size = "40"
     6. ISIN = "157"
     7. Foreign Ownership = "666191"
     8. Par = "123"
     9. Value Traded = ? It is "108" or "133"
     10. Delivery Basis = ?
     */
    
    //9 Apr 2015. Sometimes feed will return negative value...
    //Nov 2016 - feed not return negative! but it was out of range must use long long for "volumes" data
    
    if (totalMarCap<=0) {
        
        totalMarCap = totalShareIssued * lastDoneFloat;

        if (lastDoneFloat<=0) {
            
            totalMarCap = totalShareIssued * lacpFloat;
        }
    }
    
    NSString *lblVolumeString = [[UserPrefConstants singleton] abbreviateNumber:totalVol];
    NSString *lblBuyString = [[UserPrefConstants singleton] abbreviateNumber:totalBuyVol];
    NSString *lblSellString = [[UserPrefConstants singleton] abbreviateNumber:totalSellVol];
    _stockVolume.text    = lblVolumeString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalVol]];
    _stockCeiling.text      = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]]];
    _stockFloor.text       = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]]];
    
    // lblPrevClose.text = [quantityFormatter stringFromNumber:[NSNumber numberWithFloat:[[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue]]];
    
    _stockBuyTotal.text  =lblBuyString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalBuyVol]];
    _stockSellTotal.text = lblSellString;//[quantityFormatter stringFromNumber:[NSNumber numberWithInt:totalSellVol]];
    _stockBuyTrans.text   =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalBuyTran]];
    _stockSellTrans.text  =[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalSellTran]];
    _stockShareIssued.text     =[[UserPrefConstants singleton]abbreviateNumber:totalShareIssued];
    _stockMktCap.text       =[[UserPrefConstants singleton]abbreviateNumber:totalMarCap];
    

    
    if ([[stkCodeAndExchangeArr objectAtIndex:1] isEqualToString:@"MY"]) {
        [_stockMktCap setHidden:YES];
        [_stockShareIssued setHidden:YES];
        [_stockShareIssuedTitle setHidden:YES];
        [_stockMktCapTitle setHidden:YES];
    }
    
    
    
    
    if (isUpdate) {
        
        
        // ------------------------------- PRICES --------------------------------
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue])
        {
            _stockLastDone.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue])
            _stockLastDone.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockLastDone.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue])
        {
            _stockChange.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE]floatValue])
            _stockChange.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockChange.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue])
        {
            _stockChangePer.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue])
            _stockChangePer.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockChangePer.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue])
        {
            _stockAsk.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_88_D_SELL_PRICE_1]floatValue])
            _stockAsk.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockAsk.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue])
        {
            _stockBid.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_68_D_BUY_PRICE_1]floatValue])
            _stockBid.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockBid.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue])
        {
            _stockLow.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_57_D_LOW_PRICE]floatValue])
            _stockLow.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockLow.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue])
        {
            _stockHigh.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_56_D_HIGH_PRICE]floatValue])
            _stockHigh.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockHigh.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue])
        {
            _stockPrevClose.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_50_D_CLOSE]floatValue])
            _stockPrevClose.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockPrevClose.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue])
        {
            _stockOpen.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_55_D_OPEN_PRICE]floatValue])
            _stockOpen.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockOpen.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue])
        {
            _stockSellVolume.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_239_D_TOTAL_SHORT_SELL_VOL]floatValue])
            _stockSellVolume.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockSellVolume.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue])
        {
            _stockParVal.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_123_D_PAR_VALUE]floatValue])
            _stockParVal.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockParVal.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue])
        {
            _stockValue.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_102_D_VALUE]floatValue])
            _stockValue.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockValue.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue])
        {
            _stockLotSize.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_40_I_SHARE_PER_LOT]floatValue])
            _stockLotSize.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockLotSize.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue])
        {
            _stockLacp.layer.backgroundColor=kGrnFlash.CGColor;
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_51_D_REF_PRICE]floatValue])
            _stockLacp.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockLacp.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue])
        {
            _stockCeiling.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_232_D_CEILING_PRICE]floatValue])
            _stockCeiling.layer.backgroundColor=kRedFlash.CGColor;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockCeiling.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue])
        {
            _stockFloor.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_231_D_FLOOR_PRICE]floatValue])
            _stockFloor.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockFloor.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        // ------------------------------- QUANTITIES --------------------------------
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue])
        {
            _stockTrades.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_132_I_TOT_OF_TRD]integerValue])
            _stockTrades.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockTrades.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue])
        {
            _stockAskQty.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_78_I_SELL_QTY_1]integerValue])
            _stockAskQty.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockAskQty.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue])
        {
            _stockBidQty.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_58_I_BUY_QTY_1]integerValue])
            _stockBidQty.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockBidQty.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue])
        {
            _stockVolume.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_101_I_VOLUME]integerValue])
            _stockVolume.layer.backgroundColor= [kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockVolume.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue])
        {
            _stockBuyTotal.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_238_I_TOTAL_BVOL]integerValue])
            _stockBuyTotal.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockBuyTotal.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue])
        {
            _stockSellTotal.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_237_I_TOTAL_SVOL]integerValue])
            _stockSellTotal.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockSellTotal.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue])
        {
            _stockBuyTrans.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_241_I_TOTAL_BTRANS]integerValue])
            _stockBuyTrans.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockBuyTrans.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue])
        {
            _stockSellTrans.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_242_I_TOTAL_STRANS]integerValue])
            _stockSellTrans.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockSellTrans.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue])
        {
            _stockShareIssued.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_41_D_SHARES_ISSUED]integerValue])
            _stockShareIssued.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockShareIssued.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
        
        if ([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue]>[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue])
        {
            _stockMktCap.layer.backgroundColor=[kGrnFlash CGColor];
        }
        else if([[[[_qcData qcFeedDataDict]objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue]<[[[tempDictionary objectForKey:_stkCode]objectForKey:FID_100004_F_MKT_CAP]integerValue])
            _stockMktCap.layer.backgroundColor=[kRedFlash CGColor];
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
            _stockMktCap.layer.backgroundColor=[UIColor clearColor].CGColor;
        } completion:nil];
        
    }
    isUpdate= false;
    tempDictionary = [[NSMutableDictionary alloc]initWithDictionary:[_qcData qcFeedDataDict] copyItems:YES];
}




- (IBAction)gotoStockDetails:(id)sender {
    
    [_atp timeoutActivity];
    
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:_stkCode forKey:@"stkCode"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
    
}
@end
