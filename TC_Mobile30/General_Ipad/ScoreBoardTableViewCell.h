//
//  ScoreBoardTableViewCell.h
//  TCiPad
//
//  Created by Scott Thoo on 2/27/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreBoardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblVol;
@property (weak, nonatomic) IBOutlet UILabel *lblUp;
@property (weak, nonatomic) IBOutlet UILabel *lblDown;
@property (weak, nonatomic) IBOutlet UILabel *lblUnchg;
@property (weak, nonatomic) IBOutlet UILabel *lblUnTrd;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@end
