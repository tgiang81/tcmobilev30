//
//  HomeViewController.h
//  TCUniversal
//
//  Created by Scott Thoo on 10/16/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPTGraphHostingView.h"
#import "MBProgressHUD.h"
#import "VertxConnectionManager.h"
#import "QCData.h"
#import "UserPrefConstants.h"
#import "QCConstants.h"
#import "StockFlag.h"
#import "StockTicker.h"
#import <QuartzCore/QuartzCore.h>
#import "CPTGraphHostingView.h"
#import "CPTGraph.h"
#import "CPTPieChart.h"
#import "CPTGradient.h"
#import "CPTXYGraph.h"
#import "CPTTextLayer.h"
#import "CPTMutableTextStyle.h"
#import "CPTFill.h"
#import "CPTTheme.h"
#import "CPTColor.h"
#import "AppConstants.h"
#import "ReleaseNotesView.h"
#import "ATPAuthenticate.h"
#import "AppControl.h"
#import "ImgChartManager.h"
#import "LanguageManager.h"
#import "HomeNewsTableViewCell.h"
#import "N2Tabs.h"

@interface HomeViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,
    N2TabsDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewKLCI;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *scoreBoardLabel;
@property (weak, nonatomic) IBOutlet UILabel *loadingNewsLabel;
@property (weak, nonatomic) IBOutlet UIView *tabsPlaceholder;
@property (weak, nonatomic) IBOutlet UIWebView *announcementWebview;

@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIView *viewNews;
@property (weak, nonatomic) IBOutlet UIView *viewMarketActivities;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNews;
@property (weak, nonatomic) IBOutlet UIImageView *imgChart;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UIView *legendView;
@property (weak, nonatomic) IBOutlet UIView *legendDown;
@property (weak, nonatomic) IBOutlet UIView *legendUp;
@property (weak, nonatomic) IBOutlet UIView *legendUnch;
@property (weak, nonatomic) IBOutlet UIView *legendUntr;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingBtmLeft;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingBtmRight;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *chartLoadActivity;
@property (weak, nonatomic) IBOutlet UILabel *chartErrorMsg;
@property (weak, nonatomic) IBOutlet UIView *viewSummary;
@property (weak, nonatomic) IBOutlet UITableView *watchListTable;

@property (weak, nonatomic) IBOutlet UIView *viewHolderLeft;
@property (weak, nonatomic) IBOutlet UIView *viewHolderRight;
@property (weak, nonatomic) IBOutlet UIView *viewHolderWatchList;

@property (weak, nonatomic) IBOutlet UIView *viewBottomRightSelection;
@property (weak, nonatomic) IBOutlet UIView *viewBottomLeftSelection;
@property (weak, nonatomic) IBOutlet UIView *viewWatchListSelection;

@property (weak, nonatomic) IBOutlet UIView *viewBottomRight;
@property (weak, nonatomic) IBOutlet UIView *viewBottomLeft;
@property (weak, nonatomic) IBOutlet UIView *viewBottomWatchList;

@property (weak, nonatomic) IBOutlet UILabel *watchListTitle;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *watchStockLoading;
@property (weak, nonatomic) IBOutlet UIButton *watchListErrorMsg;


@property (weak, nonatomic) IBOutlet UITableView *tableviewLeftSelection;
@property (weak, nonatomic) IBOutlet UITableView *tableviewRightSelection;
@property (weak, nonatomic) IBOutlet UILabel *lblLeftTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblRightTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalScoreOfTrade;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalUp;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalDown;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalUnTrd;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalUnChg;
@property (weak, nonatomic) IBOutlet UIWebView *webViewKLCI;
@property (strong,nonatomic)  NSMutableDictionary *totalMarketDict;
@property (weak, nonatomic) IBOutlet UILabel *MainMarketNtAvailLabel;
@property (weak, nonatomic) IBOutlet UILabel *AcemarketNtAvaiLabel;
@property (weak, nonatomic) IBOutlet UILabel *WarrantMarketNtAvailLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mainNewsIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *scoreBoardIndicator;

@property (weak, nonatomic) IBOutlet UIButton *bannerButton1;
@property (weak, nonatomic) IBOutlet UIButton *bannerButton2;
@property (weak, nonatomic) IBOutlet UIButton *bannerButton3;

@property (nonatomic, strong) UserPrefConstants *userPrefs;
@property (nonatomic, strong) NSMutableArray *watchListArr;

- (IBAction)animateNewsOpen:(id)sender;
- (IBAction)animateNewsClose:(id)sender;
- (IBAction)pageNoticeControlClicked:(id)sender;
- (IBAction)openWatchList:(id)sender;
- (IBAction)smallerFont:(id)sender;
- (IBAction)biggerFont:(id)sender;

- (IBAction)clickedWatchListSelectionButton:(UIButton *)sender;

@end
