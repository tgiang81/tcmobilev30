//
//  ConfirmTableViewCell.m
//  TCiPad
//
//  Created by n2n on 01/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "ConfirmTableViewCell.h"

@implementation ConfirmTableViewCell

@synthesize dataTitle, dataContent;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
