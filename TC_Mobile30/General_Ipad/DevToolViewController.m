//
//  DevToolViewController.m
//  TCiPad
//
//  Created by n2n on 14/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "DevToolViewController.h"

@interface DevToolViewController ()

@end

@implementation DevToolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.UPC = [UserPrefConstants singleton];
    
    self.dataArray = [[NSMutableArray alloc] init];
    
    [self.dataArray addObject:@{@"Title":@"UserID",@"Content":_UPC.userName}];
    [self.dataArray addObject:@{@"Title":@"ATP Server",@"Content":_UPC.userATPServerIP}];
    [self.dataArray addObject:@{@"Title":@"Vertx Server",@"Content":_UPC.Vertexaddress}];
    [self.dataArray addObject:@{@"Title":@"InteractiveChartURL",@"Content":_UPC.interactiveChartURL}];
    [self.dataArray addObject:@{@"Title":@"User Selected Exchange",@"Content":_UPC.userCurrentExchange}];
    [self.dataArray addObject:@{@"Title":@"IntraChartURL",@"Content":_UPC.intradayChartURL}];
    [self.dataArray addObject:@{@"Title":@"HistChartURL",@"Content":_UPC.historicalChartURL}];
    [self.dataArray addObject:@{@"Title":@"AnnouncementURL",@"Content":_UPC.AnnouncementURL}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataArray count];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DevCellID"];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"DevCellID"];
    }
    
    NSDictionary *obj = [_dataArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [obj objectForKey:@"Title"];
    cell.detailTextLabel.text = [obj objectForKey:@"Content"];
    
    return cell;
}





@end
