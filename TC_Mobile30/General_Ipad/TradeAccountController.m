//
//  TradeAccountController.m
//  TCiPad
//
//  Created by n2n on 12/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "TradeAccountController.h"

@interface TradeAccountController () {
    
    CGSize scrSize;
    CGFloat bufferPreviewX;
    CGPoint panelViewOriPos;
    NSInteger searchBy;
}

@end

@implementation TradeAccountController

@synthesize delegate;





#pragma mark - LifeCycles View Delegates

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}



-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.2 animations:^(void) {
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^(void) {
            _panelView.frame = CGRectMake(scrSize.width-_panelView.frame.size.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
            
        } completion:^(BOOL finished) {
            panelViewOriPos = _panelView.center;
            
            // scroll to current selection
            long idx = [_tableDataArr indexOfObject:[UserPrefConstants singleton].userSelectedAccount];
            
            if (idx>0) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
                [_accTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
            
        }];
    }];
    
    [[LanguageManager defaultManager] translateStringsForViewController:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    
    bufferPreviewX = 0;
    searchBy = 0;
    
    _searchBar.placeholder = [LanguageManager stringForKey:@"Enter Search Keywords"];
    
    [_searchOptions setTitle:[LanguageManager stringForKey:@"Name"] forSegmentAtIndex:0];
    [_searchOptions setTitle:[LanguageManager stringForKey:@"Account No."] forSegmentAtIndex:1];
    
    _tableDataArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userAccountlist];
    scrSize = [[UIScreen mainScreen] bounds].size;
    
    _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
    
    _panelView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _panelView.layer.shadowRadius = 10;
    _panelView.layer.shadowOpacity = 1.0;
    _panelView.layer.shadowOffset = CGSizeMake(-10, 0);
    
    UIPanGestureRecognizer *panGesturePanel = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(tradeAccPanelDidPanned:)];
    panGesturePanel.delegate = self;
    [_panelView addGestureRecognizer:panGesturePanel];
    
    // tap outside of news view, dismiss it
    UITapGestureRecognizer *touch = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
    touch.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:touch];
    [touch setDelegate:self];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelAccount:) name:@"dismissAllModalViews" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Gestures Delegate

-(void)tradeAccPanelDidPanned:(UIPanGestureRecognizer*)recognizer {
    
    UIView *view = recognizer.view;
    
    // CGPoint velocity = [recognizer velocityInView:view.superview];
    
    // if (velocity.x>0) {
    
    CGPoint translation = [recognizer translationInView:view.superview];
    
    if (translation.x>=50) {
        bufferPreviewX = translation.x-49;
    } else {
        bufferPreviewX = 0;
    }
    
    if (bufferPreviewX>0) {
        
        CGFloat distanceToDismiss = 150;
        
        if(recognizer.state == UIGestureRecognizerStateBegan){
            
        } else if(recognizer.state == UIGestureRecognizerStateEnded){
            
            if (_panelView.center.x<=panelViewOriPos.x+distanceToDismiss) {
                // return back
                [UIView animateWithDuration:0.2 animations:^{
                    _panelView.center = panelViewOriPos;
                }completion:^(BOOL finished) {
                    
                }];
            }
        } else {
            // swping
            _panelView.center = CGPointMake(panelViewOriPos.x+bufferPreviewX, panelViewOriPos.y);
            
            if (_panelView.center.x>panelViewOriPos.x+distanceToDismiss) {
                [view removeGestureRecognizer:recognizer];
                [self cancelAccount:nil];
            }
            
        }
        
    } else {
        
        [UIView animateWithDuration:0.2 animations:^{
            _panelView.center = panelViewOriPos;
        }completion:^(BOOL finished) {
            
        }];
        
    }
    
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.view];
    if (CGRectContainsPoint([_panelView frame], p)) {
        ; // do nothing
    } else {
        [self cancelAccount:nil];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isDescendantOfView:_panelView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (IBAction)cancelAccount:(id)sender {
    [[ATPAuthenticate singleton] timeoutActivity];
    [UIView animateWithDuration:0.2 animations:^(void) {
        _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^(void) {
            [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.0]];
        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
    }];
}

#pragma mark - Other methods

- (IBAction)searchByField:(id)sender {
    
    UISegmentedControl *searchField = (UISegmentedControl*)sender;
    searchBy = searchField.selectedSegmentIndex;
    [_searchBar setText:@""];
}


#pragma mark - SearchBar Delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if ([searchText length]<=0) {
        _tableDataArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userAccountlist];
        [_accTable reloadData];
        
        // then scroll to current selection
        long idx = [_tableDataArr indexOfObject:[UserPrefConstants singleton].userSelectedAccount];
        
        if (idx>0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
            [_accTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    } else{
    
        NSString *filter;
        if (searchBy==1) {
            filter = @"client_account_number BEGINSWITH[cd] %@";
        } else {
            filter = @"client_name BEGINSWITH[cd] %@";
        }
        NSPredicate* predicate = [NSPredicate predicateWithFormat:filter, searchText];
        NSArray *filteredArr  = [[UserPrefConstants singleton].userAccountlist filteredArrayUsingPredicate:predicate];
        _tableDataArr = [[NSMutableArray alloc] initWithArray:filteredArr];
        [_accTable reloadData];
    }
    
}

#pragma mark - TableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [UserPrefConstants singleton].userSelectedAccount = [_tableDataArr objectAtIndex:indexPath.row];
    if ([self.delegate respondsToSelector:@selector(accountSelected:)]) {
        [self.delegate accountSelected:[UserPrefConstants singleton].userSelectedAccount];
    }
    [self cancelAccount:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([_tableDataArr count]<=0) {
        // Display a message when the table is empty
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = [LanguageManager stringForKey:@"No Match."];
        messageLabel.textColor = [UIColor whiteColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        [messageLabel sizeToFit];
        
        _accTable.backgroundView = messageLabel;
        _accTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
        
    } else {
        _accTable.backgroundView = nil;
        _accTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_tableDataArr count];
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AccountTableViewCell *cell = (AccountTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"AccountCellID"];
    if (cell==nil) {
        cell = [[AccountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AccountCellID"];
    }
    [cell populateItemsAtIndexPath:indexPath withArray:_tableDataArr];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 ==0) {
        cell.backgroundColor = [UIColor colorWithWhite:0.1 alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor colorWithWhite:0.13 alpha:1.0];
    }
}

@end
