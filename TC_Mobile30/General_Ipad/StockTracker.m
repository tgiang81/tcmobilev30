//
//  StockTracker.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 14/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "StockTracker.h"

@implementation StockTracker
@synthesize lblStkName;
@synthesize priceLabel;
@synthesize quantityLabel;
@synthesize sbhLabel;
@synthesize bbhLabel;
@synthesize animationView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockTracker" owner:self options:nil];
        // 2. adjust bounds
        self.bounds = self.view.bounds;
        
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        // 3. Add as a subview
        
        [self addSubview:self.view];
        
    }
    return self;
}

//With Storyboard
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        
        // Scott
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockTracker" owner:self options:nil];
        // 2. Add as a subview
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        
        [self addSubview:self.view];
    }
    
    return self;
}

- (void) updateContent:(NSString *)stockName
              andPrice:(float)price
               andLACP:(float)lacp
           andQuantity:(NSString *)quantity
          andCondition:(NSString *)status
                andBBH:(NSString *)bbh
                andSBH:(NSString *)sbh{
    
   
    [UIView animateWithDuration:0.5f animations:^{
        
         if([status isEqualToString:@"b"]){
             [animationView setBackgroundColor:[UIColor greenColor]];
         }else if([status isEqualToString:@"s"]){
               [animationView setBackgroundColor:[UIColor redColor]];
         }else{
               [animationView setBackgroundColor:[UIColor orangeColor]];
         }
        [animationView setAlpha:0.5f];
        
    } completion:^(BOOL finished) {
        
        //fade out
        [UIView animateWithDuration:0.5f animations:^{
          [animationView setBackgroundColor:[UIColor clearColor]];
            [animationView setAlpha:0.0f];
            
        } completion:nil];
        
    }];
    
    if([status isEqualToString:@"b"]){
            [lblStkName setTextColor:[UIColor greenColor]];
            [priceLabel setTextColor:[UIColor greenColor]];
            [quantityLabel setTextColor:[UIColor greenColor]];
            [sbhLabel setTextColor:[UIColor greenColor]];
            [bbhLabel setTextColor:[UIColor greenColor]];
    }
    else if ([status isEqualToString:@"s"]){
            [lblStkName setTextColor:[UIColor redColor]];
            [priceLabel setTextColor:[UIColor redColor]];
            [quantityLabel setTextColor:[UIColor redColor]];
            [sbhLabel setTextColor:[UIColor redColor]];
            [bbhLabel setTextColor:[UIColor redColor]];
    }
   else{
            [lblStkName setTextColor:[UIColor orangeColor]];
            [priceLabel setTextColor:[UIColor orangeColor]];
            [quantityLabel setTextColor:[UIColor orangeColor]];
            [sbhLabel setTextColor:[UIColor orangeColor]];
            [bbhLabel setTextColor:[UIColor orangeColor]];
    }
    [lblStkName setText:stockName];
    [priceLabel setText:[NSString stringWithFormat:@"%.2f",price]];
    [quantityLabel setText:quantity];
    [sbhLabel setText:sbh];
    [bbhLabel setText:bbh];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
