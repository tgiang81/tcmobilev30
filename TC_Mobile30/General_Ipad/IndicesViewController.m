//
//  IndicesViewController.m
//  TCiPad
//
//  Created by Scott Thoo on 2/27/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import "IndicesViewController.h"
#import "QCData.h"
#import "VertxConnectionManager.h"
#import "IndicesTableViewCell.h"
#import "IndicesTimeSalesTableViewCell.h"
#import "ScoreBoardTableViewCell.h"
#import "QCConstants.h"
#import "UserPrefConstants.h"
#import "ATPAuthenticate.h"
#import "AppConstants.h"
#import "AppControl.h"
#import "ImgChartManager.h"
#import "NewsModalViewController.h"
#import "TransitionDelegate.h"


@interface IndicesViewController ()
{
    NSMutableArray *indicesTimeSalesArr;
    NSMutableDictionary *mainMarketDict;
    NSMutableDictionary *warrantMarketDict;
    NSMutableDictionary *aceMarketDict;
    NSMutableDictionary *totalMarketDict;
    long long totalUp;
    long long totalDown;
    long long totalUnchg;
    long long totalUntrd;
    long long totalVol;
    long long totalTotal;
    long long totalValue;
    
    long selectedRow, prevSelectedRow;
    long selectedRow1, prevSelectedRow1;
    long selectedRow2, prevSelectedRow2;
    long selectedRow3, prevSelectedRow3;
    
    NSDateFormatter *dateFormatterscoreboard;
    NSDate *currentTime;
    
    BOOL isupdate;
    long indexCounter;
    NSString *stockcodeTimeandSales;
   // NSTimer *Timer;
    UIRefreshControl *refreshControlMain;
    UIRefreshControl *refreshControlAce;
    UIRefreshControl *refreshControlWarrant;
    UIRefreshControl *refreshControlTimeSales;
    ATPAuthenticate *atp;
    
    BOOL isFullScreenChart;
    CGRect webViewOriginalFrame;
    BOOL callOnce;
    BOOL callOnceForChart;
    CGFloat ratioX,ratioY;
    long currentTotDays;
    
    NSString *bundleIdentifier ;
    
}

@property (nonatomic, strong) NSMutableArray *indicesValArray;
@property (nonatomic, retain) NSMutableArray *indicesTempArray;
@property (nonatomic, retain) NSMutableArray *mainMktArray;
@property (nonatomic, retain) NSMutableArray *warrMktArray;
@property (nonatomic, retain) NSMutableArray *acesMktArray;
@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation IndicesViewController
@synthesize indiceTableView,movementAndChartWebView,timeSalesTableView,mainMarketTableView,strcwarrTableView,
aceMarketTableView,activityAnimation;


#pragma mark - View LifeCycles Delegates


-(void)viewDidLayoutSubviews {
    
    if (!callOnce) {
        webViewOriginalFrame = _chartView.frame;
        callOnce = YES;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}
-(void) viewDidAppear:(BOOL)animated
{
//    if((![[UserPrefConstants singleton].userCurrentExchange isEqual:@"KL"])&&(![[UserPrefConstants singleton].userCurrentExchange isEqual:@"MY"])){
//        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"NOTE"
//                                                         message:@"\nIndices are Currently Available only for KL Market."
//                                                        delegate:self
//                                               cancelButtonTitle:@"DONE"
//                                               otherButtonTitles: nil];
//        [alert show];
//    }
    [super viewDidAppear:animated];
}
- (void)viewWillAppear:(BOOL)animated
{
    [atp timeoutActivity];
    [activityAnimation startAnimating];
    [activityAnimation hidesWhenStopped];
    [super viewWillAppear:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];

    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
        
        
        [_advertisingBanner setImage:[UIImage imageNamed:@"AppLogoABACUS"] forState:UIControlStateNormal];
        
    }else{
        
        [_advertisingBanner setImage:[UIImage imageNamed:@"banner1"] forState:UIControlStateNormal];
    }

    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    
    [self setChartStatus:5];
    
    callOnce = NO;
    callOnceForChart=YES;
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTappedChart:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    doubleTapGesture.delegate = self;
    isFullScreenChart = NO;
    [_chartView addGestureRecognizer:doubleTapGesture];
    [movementAndChartWebView setBackgroundColor:[UIColor clearColor]];
    [movementAndChartWebView setOpaque:NO];
    
    atp = [ATPAuthenticate singleton];

    selectedRow = -1;
    prevSelectedRow = -1;
    selectedRow1 = -1;
    prevSelectedRow1 = -1;
    selectedRow2 = -1;
    prevSelectedRow2 = -1;
    selectedRow3 = -1;
    prevSelectedRow3 = -1;
    
    refreshControlMain = [[UIRefreshControl alloc] init];
    [refreshControlMain addTarget:self action:@selector(refreshButtonMain:) forControlEvents:UIControlEventValueChanged];
    [mainMarketTableView addSubview:refreshControlMain];
    
    refreshControlAce = [[UIRefreshControl alloc] init];
    [refreshControlAce addTarget:self action:@selector(refreshButtonAce:) forControlEvents:UIControlEventValueChanged];
    [aceMarketTableView addSubview:refreshControlAce];
    
    bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//    bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_CIMBSG] || [bundleIdentifier isEqualToString:BUNDLEID_CIMBSG_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_CIMBSGSTAG]) {
        aceMarketTableView.hidden = YES;
        _aceView.hidden = NO;
        [_aceMarketLabel setText:@"Advertising Space"];
        [_aceMarketTimeStamp setHidden:YES];
        [_aceSectorLabel  setHidden:YES];
        [_aceTotalMarketLabel setHidden:YES];
        [_aceVolLabel setHidden:YES];
        [_aceUpImage setHidden:YES];
        [_aceDownImage setHidden:YES];
        [_aceNoChangeImage setHidden:YES];
        [_aceEqualImage setHidden:YES];
        [_aceMarketTimeStamp setHidden:YES];
        _aceTotalLabel.hidden = YES;
    }
    
    
    refreshControlWarrant = [[UIRefreshControl alloc] init];
    [refreshControlWarrant addTarget:self action:@selector(refreshButtonWarrant:) forControlEvents:UIControlEventValueChanged];
    [strcwarrTableView addSubview:refreshControlWarrant];
    
    refreshControlTimeSales = [[UIRefreshControl alloc] init];
    [refreshControlTimeSales addTarget:self action:@selector(refreshTimeAndSales:) forControlEvents:UIControlEventValueChanged];
    [timeSalesTableView addSubview:refreshControlTimeSales];
    
    [UserPrefConstants singleton].quoteScreenFavID=nil;

    //Vertx Call
    //-First get indices data, then
    //-Once returned, get The first item's Time and sales and display on front end. (Vertex call at didFinishedDoneGetIndices
    //-Get Scoreboard
    
    [[VertxConnectionManager singleton] vertxGetIndices];
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"]; //Main
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"]; //Warrant
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"]; //Ace
    
    //Add Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneVertxGetScoreBoardWithExchange:) name:@"doneVertxGetScoreBoardWithExchange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedDoneGetIndices:) name:@"DoneGetIndices" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedVertxGetIndicesTimeAndSales:) name:@"didFinishedTimeAndSales" object:nil];

    //Default WebView Callhttp://chartwc.asiaebroker.com/chart/stockchart/chartTCPlus.jsp?code=020000000&exchg=KL&amount=200&color=b
    
    // to sort data alphabetically
    self.indicesValArray  = [[NSMutableArray alloc] init];
    self.indicesTempArray = [[NSMutableArray alloc] init];
    
    self.mainMktArray = [[NSMutableArray alloc] init];
    self.warrMktArray = [[NSMutableArray alloc] init];
    self.acesMktArray = [[NSMutableArray alloc] init];
    
   // NSString *url = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m",[[UserPrefConstants singleton].stockChartarr objectAtIndex:0], @"020000000",[UserPrefConstants singleton].userCurrentExchange];
    [movementAndChartWebView setDelegate:self];

    
    ////NSLog(@"qcScore : %@",[[QCData singleton]qcScoreBoardDict]);
    mainMarketDict          = [NSMutableDictionary dictionary];
    warrantMarketDict       = [NSMutableDictionary dictionary];
    aceMarketDict           = [NSMutableDictionary dictionary];
    totalMarketDict         = [NSMutableDictionary dictionary];
    currentTime = [NSDate date];
    dateFormatterscoreboard = [[NSDateFormatter alloc] init];
    [dateFormatterscoreboard setDateFormat:@"HH:mm:ss"];
    _mainMarketTimeStamp.text=
    _warrantMarketTimeSTamp.text=
    _aceMarketTimeStamp.text=
    [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];


    isupdate = NO;
    indexCounter=0;
    
    
    
    ExchangeInfo *currInfo = [UserPrefConstants singleton].currentExchangeInfo;
    // Update Market Info indicator button (all stocks/odd lot/ etc)
    //NSLog(@"ExchgInfo %@", currInfo);
    
    // LOAD SECTOR INFO
    self.sectorInfoArray = [[NSMutableArray alloc] init];
    NSArray *sectArr = [currInfo.sectorParents allKeys];
    
    //NSLog(@"sectArr %@", sectArr);
    
    for (int i=0; i<[sectArr count]; i++) {
        NSString *key = [sectArr objectAtIndex:i];
        SectorInfo *inf = [[SectorInfo alloc] init];
        inf.sectorCode = key;
        inf.sectorName = [currInfo.sectorInfos objectForKey:key];
        inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"[" withString:@""];
        inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"]" withString:@""];
        
        if ([inf.sectorName isEqualToString:@"TOTAL"]) inf.sectorName = @"All";
        
        NSString *parent = [currInfo.sectorParents objectForKey:key];
        
        // add parents only
        if ([parent isEqualToString:@"0"])
            [_sectorInfoArray addObject:inf];
    };
    
    
    // then populate childrens
    for (int i=0; i<[_sectorInfoArray count]; i++) {
        SectorInfo *inf = [_sectorInfoArray objectAtIndex:i];
        
        for (int j=0; j<[sectArr count]; j++) {
            NSString *key = [sectArr objectAtIndex:j];
            NSString *parent = [currInfo.sectorParents objectForKey:key];
            
            // NSLog(@"parent %@ %@ %@",parent, key, inf.sectorCode);
            
            if ([parent isEqualToString:inf.sectorCode]) {
                SectorInfo *subInf = [[SectorInfo alloc] init];
                subInf.sectorName = [currInfo.sectorInfos objectForKey:key];
                subInf.sectorCode = key;
                [inf.subItemsArray addObject:subInf];
                [_sectorInfoArray replaceObjectAtIndex:i withObject:inf];
            }
        }
    }
}

- (IBAction)clickAdvertisingBanner:(id)sender{
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
        vc.view.backgroundColor = [UIColor clearColor];
        vc.newsTitleLabel.text = @"Abacus Securities Corporation";
        
        vc.currentEd = nil;
        
        NSString *url = [NSString stringWithFormat:@"%@",@"https://www.mytrade.com.ph"];
        
        [vc openUrl:url withJavascript:NO];
        [vc setTransitioningDelegate:_transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
        vc.view.backgroundColor = [UIColor clearColor];
        vc.newsTitleLabel.text = @"N2NConnect Berhad";//@"CIMB Securities (Singapore) Pte Ltd";
        
        vc.currentEd = nil;
        
        NSString *url = [NSString stringWithFormat:@"%@",@"http://www.n2nconnect.com"];
        
        [vc openUrl:url withJavascript:NO];
        [vc setTransitioningDelegate:_transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - TapGesture delegates

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


-(UIImage*)captureWebView
{
    UIGraphicsBeginImageContext(_chartView.bounds.size);
    [movementAndChartWebView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

-(void)doubleTappedChart:(UITapGestureRecognizer*)recognizer {
    
    // NSLog(@"double tapped");
    
    __block NSArray *splitStkCode = [stockcodeTimeandSales componentsSeparatedByString:@"."];
    
    if (!isFullScreenChart) {
        [self.view bringSubviewToFront:_chartView];
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        dummyView.contentMode = UIViewContentModeScaleToFill;
        dummyView.frame = _chartView.frame;
        [self.view addSubview:dummyView];
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            
            _chartView.frame = _mainView.frame;
            dummyView.frame = _mainView.frame;
            movementAndChartWebView.hidden = YES;
            
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethodForIndex:[splitStkCode objectAtIndex:0]];
            
        } completion:^(BOOL finished) {
            
            movementAndChartWebView.hidden = NO;
            [dummyView removeFromSuperview];
            isFullScreenChart= YES;
        }];
        
        
    } else {
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        dummyView.frame = _chartView.frame;
        dummyView.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:dummyView];
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            _chartView.frame = webViewOriginalFrame;
            dummyView.frame = webViewOriginalFrame;
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethodForIndex:[splitStkCode objectAtIndex:0]];
            
        } completion:^(BOOL finished) {
            [dummyView removeFromSuperview];
            isFullScreenChart= NO;
        }];
        
        
    }
}


-(void)refreshTimeAndSales:(id)sender {
    [atp timeoutActivity];
    [refreshControlTimeSales endRefreshing];
    NSString *latestTradedNumber = [[[[QCData singleton]qcIndicesDict]objectForKey:stockcodeTimeandSales]objectForKey:FID_103_I_TRNS_NO];
    [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:stockcodeTimeandSales begin:@"0" end:latestTradedNumber exchange:[UserPrefConstants singleton].userCurrentExchange ];
    currentTime = [NSDate date];
    _timeSalesTimeStamp.text= [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];

}

- (void) refreshButtonMain:(id)sender{
    [atp timeoutActivity];
    [mainMarketTableView reloadData];
    [refreshControlMain endRefreshing];
    currentTime = [NSDate date];
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"]; //Main
    _mainMarketTimeStamp.text= [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];
}

- (void) refreshButtonAce:(id)sender{
    [atp timeoutActivity];

    [aceMarketTableView reloadData];
    [refreshControlAce endRefreshing];
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
    _aceMarketTimeStamp.text= [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];

}

- (void) refreshButtonWarrant:(id)sender{
    [atp timeoutActivity];

    [strcwarrTableView reloadData];
    [refreshControlWarrant endRefreshing];
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"]; //Warrant
    _warrantMarketTimeSTamp.text=  [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];

}



- (void)doneVertxGetScoreBoardWithExchange:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
    //NSDictionary * response = [notification.userInfo copy];
    ////NSLog(@"doneVertxGetScoreBoardWithExchange response : %@",response);
    
    // NSLog(@"doneVertxGetScoreBoardWithExchange: %@",[[QCData singleton]qcScoreBoardDict]);
    NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_37_S_SECTOR_NAME ascending:YES];
    
    mainMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2201]];
    NSArray * sortedArray = [[mainMarketDict allValues] sortedArrayUsingDescriptors:@[sortDescriptor]];
    _mainMktArray = [NSMutableArray arrayWithArray:sortedArray];
        
     
    warrantMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2203]];
    NSArray * sortedArray2 = [[warrantMarketDict allValues] sortedArrayUsingDescriptors:@[sortDescriptor]];
    _warrMktArray = [NSMutableArray arrayWithArray:sortedArray2];
    
    aceMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2204]];
    NSArray * sortedArray3 = [[aceMarketDict allValues] sortedArrayUsingDescriptors:@[sortDescriptor]];
    _acesMktArray = [NSMutableArray arrayWithArray:sortedArray3];
    
    //Self calculate 20 jan 2015
    [self calculateCustomFields:mainMarketDict];
    [self calculateCustomFields:aceMarketDict];
    [self calculateCustomFields:warrantMarketDict];
    [mainMarketTableView reloadData];
    [strcwarrTableView reloadData];
    [aceMarketTableView reloadData];
    });
}




- (void)didFinishedDoneGetIndices:(NSNotification *)notification
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
//    NSDictionary * response = [notification userInfo];
   NSString * response = [notification object];
    
   ///NSLog(@"didFinishedDoneGetIndices3 Response : %@",response);
    ////NSLog(@"[[[QCData singleton]qcIndicesDict]count] : %lu",(unsigned long)[[[QCData singleton]qcIndicesDict]count]);
    ////NSLog(@"[[[QCData singleton]qcIndicesDict]allKeys count] : %lu",(unsigned long)[[[[QCData singleton]qcIndicesDict]allKeys]count]);
    
  //  NSString *firstItemStockCode = [[[[QCData singleton]qcIndicesDict]allKeys]objectAtIndex:0];
  //  NSString *latestTradedNumber = [[[[[QCData singleton]qcIndicesDict]objectForKey:firstItemStockCode]objectForKey:FID_103_I_TRNS_NO]stringValue];
    
    if (indexCounter>100) indexCounter = 1;
    
   // NSLog(@"update ");
    
    // if no data, then don't process
    if ([[[QCData singleton] qcIndicesDict] count]<=0) return;
        
    
    // FIRST TIME
    if (indexCounter==0) {
        // SORT IT
        NSMutableArray *independentArray = [[NSMutableArray alloc] init];
        for(id key in [[QCData singleton] qcIndicesDict]) {
            NSDictionary *eachDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:key]];
            [independentArray addObject:eachDict];
        }
        NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:FID_130_S_SYSBOL_2 ascending:YES];
        NSArray *sortedArray = [independentArray sortedArrayUsingDescriptors:@[sortDescriptor]];
        _indicesValArray = [NSMutableArray arrayWithArray:sortedArray];
        
        NSString *firstItemStockCode = [[_indicesValArray objectAtIndex:0] objectForKey:FID_33_S_STOCK_CODE];
        NSString *latestTradedNumber = [[[_indicesValArray objectAtIndex:0] objectForKey:FID_103_I_TRNS_NO] stringValue];
        
        stockcodeTimeandSales=firstItemStockCode;
        
        [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:stockcodeTimeandSales begin:@"0"
                                                                        end:latestTradedNumber
                                                                   exchange:[UserPrefConstants singleton].userCurrentExchange ];
    
        
            _chartTitleLabel.text = [LanguageManager stringForKey:@"Movement & Chart - %@" withPlaceholders:@{@"%moveChart%":[[_indicesValArray objectAtIndex:0] objectForKey:FID_39_S_COMPANY]}];
            selectedRow = 0;
            [indiceTableView reloadData];
            NSArray *splitStkCode = [firstItemStockCode componentsSeparatedByString:@"."];
            [self loadChartMethodForIndex:[splitStkCode objectAtIndex:0]];
       
        
//        if (Timer!=nil) {
//            [Timer invalidate];
//            Timer = nil;
//        }
//        Timer= [NSTimer scheduledTimerWithTimeInterval:60.0f target:self selector:@selector(ReloadTimeAndSales) userInfo:nil repeats:YES ];
//        
        // LOAD CHART
        //NSString *stockCode = [[[[[QCData singleton]qcIndicesDict]allValues]objectAtIndex:0]objectForKey:FID_33_S_STOCK_CODE];

        
    // UPDATING
    }  else {
        
        // NSLog(@"------------------------------ \n\n");
        
        if ([_indicesValArray count]==0) return;
        
        __block long updateRow = -1;
        [_indicesValArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary *eachIndex = (NSDictionary*)obj;
           // NSLog(@"Each Index %@",[eachIndex objectForKey:FID_33_S_STOCK_CODE]);
            
            if ([[eachIndex objectForKey:FID_33_S_STOCK_CODE] isEqualToString:response]) {
                updateRow = idx;
                *stop = YES;
            }
        }];
        
        if (updateRow>=0) {
            
            _indicesTempArray = [NSMutableArray arrayWithArray:_indicesValArray];
            
            // NSLog(@"Compare %@, %@", [_indicesValArray objectAtIndex:updateRow], [[[QCData singleton] qcIndicesDict] objectForKey:response]);
            
            // update indicesValArray from qcIndices dict
            NSDictionary *updatedDict = [NSDictionary dictionaryWithDictionary:[[[QCData singleton] qcIndicesDict] objectForKey:response]];
            
            [_indicesValArray replaceObjectAtIndex:updateRow withObject:updatedDict];
            //NSLog(@"wat %@",[_indicesValArray objectAtIndex:updateRow]);
            
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:updateRow inSection:0];

            
            // check if row is visible and already loaded before reloading
            if ([indiceTableView.indexPathsForVisibleRows containsObject:rowToReload]) {
                isupdate =true;
                [indiceTableView reloadRowsAtIndexPaths:@[rowToReload] withRowAnimation:UITableViewRowAnimationNone];
            }
           

        }
    }
    
    
    indexCounter++;
     });
   // NSLog(@"qcIndices %@",[[QCData singleton]qcIndicesDict]);
}


- (void)didFinishedVertxGetIndicesTimeAndSales:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
    NSDictionary * response = [notification.userInfo copy];
    indicesTimeSalesArr = [response objectForKey:@"data"];
    //NSLog(@"indicesTimeSalesArr %@",indicesTimeSalesArr);
    [timeSalesTableView reloadData];
    currentTime = [NSDate date];
    _timeSalesTimeStamp.text= [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];

    });
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //[Timer invalidate];
    NSMutableURLRequest * reqBlank =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]];
    [movementAndChartWebView loadRequest:reqBlank];
    
}



#pragma mark - Charts

-(void)setChartStatus:(long)status {
    
    NSString *errormsg = [LanguageManager stringForKey:@"Chart Not Available"];
    
    //success
    if (status==1) {
        [_chartErrorMsg setHidden:YES];
        [activityAnimation setHidden:YES];
        [_imgChart setHidden:NO];
    }
    
    //no url
    if (status==2) {
        [_chartErrorMsg setHidden:NO];
        [activityAnimation setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // error
    if (status==3) {
        [_chartErrorMsg setHidden:NO];
        [activityAnimation setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // loading
    if (status==4) {
        [_chartErrorMsg setHidden:YES];
        [activityAnimation setHidden:NO];
        [_imgChart setHidden:YES];
    }
    
    if (status==5) {
        [_chartErrorMsg setHidden:YES];
        [activityAnimation setHidden:YES];
        [_imgChart setHidden:YES];
        
    }
    
    // error
    if (status==6) {
        errormsg = [LanguageManager stringForKey:@"Load Chart Fail. Try again later."];
        [_chartErrorMsg setHidden:NO];
        [activityAnimation setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    _chartErrorMsg.text = errormsg;
}




-(void)loadChartMethodForIndex:(NSString*)index {

    
    NSString *chartTypeToLoad = [UserPrefConstants singleton].chartTypeToLoad;
    
    
    // ========================================
    //                MODULUS
    // ========================================
    //chartTypeToLoad = @"Modulus";

    
    if ([chartTypeToLoad isEqualToString:@"Modulus"]) {
        
        [self setChartStatus:5];
        
        // &isstock=N
        
        NSString *url = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m&isstock=N",[UserPrefConstants singleton].interactiveChartURL, index,[UserPrefConstants singleton].userCurrentExchange];
        
        //NSLog(@"modulus %@", url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        movementAndChartWebView.scrollView.scrollEnabled = NO;
        movementAndChartWebView.scrollView.bounces = NO;
        [movementAndChartWebView loadRequest:request];
        
        [_imgChart setHidden:YES];
        [movementAndChartWebView setHidden:NO];
    }
    
    
    //NSString *urlString = [NSString stringWithFormat:@"http://chart.asiaebroker.com/chart/intraday?w=300&h=200&k=%@",swdc.stockCodeForDetail];
    // ========================================
    //                IMAGE
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Image"]) {
        
        [self setChartStatus:4];
        
        NSString *urlString = [NSString stringWithFormat:@"%@t=1&w=%.0f&h=%.0f&k=%@.%@&c=b",
                               [UserPrefConstants singleton].intradayChartURL,
                               _imgChart.frame.size.width,
                               _imgChart.frame.size.height,
                               index,[UserPrefConstants singleton].userCurrentExchange];
        
        ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
        [imgChartManager getImageChartWithURL:urlString
                            completedWithData:^(NSData *data) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIImage *chartImg = [UIImage imageWithData:data];
                                    _imgChart.image = chartImg;
                                    [self setChartStatus:1];
                                });
                            }
                                      failure:^(NSString *errorMsg) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self setChartStatus:3];
                                          });
                                      }];
        
        [_imgChart setHidden:NO];
        [movementAndChartWebView setHidden:YES];
    }
    
    //https://poc.asiaebroker.com/mchart/index_POC.jsp?code=1023&Name=Annica&exchg=KL&mode=d&color=b&lang=en&key=%01%0E%01%03%00%00
    
    // ========================================
    //                TELETRADER
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"TeleTrader"]) {
        
        [self setChartStatus:5];
        
        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];

        NSString *url = [NSString stringWithFormat:@"%@code=%@&Name=%@&exchg=%@&mode=d&color=b&lang=en&key=%@",
                         [UserPrefConstants singleton].interactiveChartURL,
                         index,
                         [splitStkCode objectAtIndex:1],
                         [UserPrefConstants singleton].userCurrentExchange,
                         [[UserPrefConstants singleton] encryptTime]];
        //NSLog(@"%@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        movementAndChartWebView.scrollView.scrollEnabled = NO;
        movementAndChartWebView.scrollView.bounces = NO;
        [movementAndChartWebView loadRequest:request];
        
        [_imgChart setHidden:YES];
        [movementAndChartWebView setHidden:NO];
    }
    
}



#pragma mark -
#pragma mark TableView Delegate
//====================================================================
//                        TableView Delegate
//====================================================================

-(void)setNoDataForMarketsForTableView:(UITableView*)tableView {
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = [LanguageManager stringForKey:@"Not Available for the Selected Exchange."];
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = [UIFont boldSystemFontOfSize:19];
    [messageLabel sizeToFit];
    
    tableView.backgroundView = messageLabel;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==11) { // mainmarket
        if ([_mainMktArray count]<=0) {
            [self setNoDataForMarketsForTableView:tableView];
            //NSLog(@"no main");
            return 0;
        } else {
            tableView.backgroundView = nil;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
        }
    } else
    
    if (tableView.tag==12) { // warrantmarket
        if ([_warrMktArray count]<=0) {
            [self setNoDataForMarketsForTableView:tableView];
            //NSLog(@"no warrant");
            return 0;
        } else {
            tableView.backgroundView = nil;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
        }
        
    } else
    
    if (tableView.tag==13) { // acemarket
        if ([_acesMktArray count]<=0) {
            [self setNoDataForMarketsForTableView:tableView];
            //NSLog(@"no ace");
            return 0;
        } else {
            tableView.backgroundView = nil;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
        }
        
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // tag 1    - IndiceTableView
    // tag 2    - Time and sales
    // tag 11   - Main Market
    // tag 12   - StrWarr Market
    // tag 13   - Ace Market

    switch (tableView.tag) {
        case 1: //Indices Tableview
            return [_indicesValArray count];
            break;
        case 2: //Indices Time and sales Table View
        {
            int count = 0;
            if([[[[QCData singleton]qcIndicesDict]allKeys]count]<1)
            {
                count =1;
            }  else {
                if ([_indicesValArray count]>0) {
                    NSLog(@"_indicesValArray %@",_indicesValArray);
                    NSString *latestTradedNumber = [[_indicesValArray objectAtIndex:0] objectForKey:FID_103_I_TRNS_NO];
                    if (count<20) {
                        count = [latestTradedNumber intValue];
                    }else{
                        count=20;
                    }
                }
            }
            ////NSLog(@"Indices Time and sales Table View count : %d",count);

            return count;

        }
            break;
        case 11:
            return [_mainMktArray count];
        break;
        case 12:
            return [_warrMktArray count];
            break;
        case 13:
            return [_acesMktArray count];
            break;
        default:
            break;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellDefault";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Formatters
//    NSNumber *value;
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    switch (tableView.tag) {
            // MARK: INDICES CELLS
        case 1:
        {
            static NSString *CellIdentifier2 = @"CellIndices";
            IndicesTableViewCell *cellIndices = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
            if (cellIndices == nil)
            {
                cellIndices = [[IndicesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2];
            }
            int row = (int)indexPath.row;
            
            
            if (row==selectedRow) {
                cellIndices.lblName.textColor = [UIColor cyanColor];
            } else {
                cellIndices.lblName.textColor = [UIColor whiteColor];
            }
            
            //NSString *stockCode = [[[[[QCData singleton]qcIndicesDict]allValues]objectAtIndex:indexPath.row]objectForKey:FID_38_S_STOCK_NAME];
            NSString *stockCode = [[_indicesValArray objectAtIndex:row]objectForKey:FID_38_S_STOCK_NAME];
            NSArray *splitStkCode = [stockCode componentsSeparatedByString:@"."];
            cellIndices.lblName.text = [splitStkCode objectAtIndex:0];
            
            [priceFormatter setRoundingMode: NSNumberFormatterRoundDown];

            cellIndices.lblPrev.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_indicesValArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue]]];
            cellIndices.lblCurrent.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue]]];
            CGFloat close = [[[_indicesValArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue];
            CGFloat lastDone = [[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
            CGFloat chg = lastDone - close ;
            cellIndices.lblChg.text = [[UserPrefConstants singleton] abbreviatePrice:chg];
            
            chg = chg / close * 100;
            if (close<=0) chg = 0;
                
            NSString *chgP = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:chg]];
            // chgP = [chgP stringByAppendingString:@"%"]; NO NEED % SYMBOL (FOLLOW TCPLUS STANDAARD)
            cellIndices.lblChgPer.text= chgP;
            
            if(chg <0)  {
                cellIndices.lblChg.textColor = [UIColor redColor];
                cellIndices.lblChgPer.textColor = [UIColor redColor];
                cellIndices.lblCurrent.textColor = [UIColor redColor];
            } else if(chg>0) {
                cellIndices.lblChg.textColor = [UIColor greenColor];
                cellIndices.lblChgPer.textColor = [UIColor greenColor];
                cellIndices.lblCurrent.textColor = [UIColor greenColor];
            } else  {
                cellIndices.lblChg.textColor = [UIColor whiteColor];
                cellIndices.lblChgPer.textColor = [UIColor whiteColor];
                cellIndices.lblCurrent.textColor = [UIColor greenColor];
            }
            
            
            if (isupdate && _indicesTempArray.count>0) {
                
                CGFloat indicesCurrentValue=[[[_indicesValArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
                CGFloat tempCurrentValue=[[[_indicesTempArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
                
               // NSLog(@"%f, %f", indicesCurrentValue, tempCurrentValue);
                
                
                if(indicesCurrentValue > tempCurrentValue)
                    cellIndices.lblCurrent.layer.backgroundColor=[kGrnFlash CGColor];
                else if(indicesCurrentValue < tempCurrentValue )
                    cellIndices.lblCurrent.layer.backgroundColor=[kRedFlash CGColor];
                UIColor *prevClr = cellIndices.lblCurrent.textColor;
                cellIndices.lblCurrent.textColor = [UIColor whiteColor];
                [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                    cellIndices.lblCurrent.layer.backgroundColor=[UIColor clearColor].CGColor;
                } completion:^(BOOL finished) { cellIndices.lblCurrent.textColor = prevClr; }];
                
                CGFloat tempclose = [[[_indicesTempArray objectAtIndex:row]objectForKey:FID_50_D_CLOSE]floatValue];
                CGFloat templastDone = [[[_indicesTempArray objectAtIndex:row]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
                CGFloat tempchg = templastDone - tempclose ;
                tempchg = tempchg / tempclose * 100;
                NSString *tempchgP = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:tempchg]];
                
                if (chg > tempchg)
                    cellIndices.lblChg.layer.backgroundColor=[kGrnFlash CGColor];
                else if(chg < tempchg)
                    cellIndices.lblChg.layer.backgroundColor=[kRedFlash CGColor];
                prevClr = cellIndices.lblChg.textColor;
                cellIndices.lblChg.textColor = [UIColor whiteColor];
                [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                    cellIndices.lblChg.layer.backgroundColor=[UIColor clearColor].CGColor;
                } completion:^(BOOL finished) { cellIndices.lblChg.textColor = prevClr; }];
                
                if (chgP.floatValue > tempchgP.floatValue)
                    cellIndices.lblChgPer.layer.backgroundColor=[kGrnFlash CGColor];
                else if(chgP.floatValue <tempchgP.floatValue)
                    cellIndices.lblChgPer.layer.backgroundColor=[kRedFlash CGColor];
                prevClr = cellIndices.lblChgPer.textColor;
                cellIndices.lblChgPer.textColor = [UIColor whiteColor];
                [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
                    cellIndices.lblChgPer.layer.backgroundColor=[UIColor clearColor].CGColor;
                } completion:^(BOOL finished) { cellIndices.lblChgPer.textColor = prevClr; }];
            }
            
            isupdate = NO;
            return cellIndices;
            
        }break;
            
            // MARK: TIME SALES CELLS
        case 2:
        {
          
            ////NSLog(@"Goes Time and sales");
            static NSString *CellIdentifierTimeSale = @"CellTimeSales";
            IndicesTimeSalesTableViewCell *cellTimeSale = [tableView dequeueReusableCellWithIdentifier:CellIdentifierTimeSale forIndexPath:indexPath];
            if (cellTimeSale == nil)
            {
                cellTimeSale = [[IndicesTimeSalesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierTimeSale];
            }
            NSUInteger row = indicesTimeSalesArr.count - 1 - indexPath.row;
            
            if (row<=0) {
                [cell setHidden:YES];
                
            }
            //Convert Incoming StrDate to proper one.
            ////NSLog(@"Time: : %@",[[indicesTimeSalesArr objectAtIndex:row]objectForKey:@"0"]);
            
            if ([indicesTimeSalesArr count]>1) {
            
                NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
                [dateFormater setDateFormat:@"HHmmss"];
                if ((int)row>=0) {
                    
                    
                    NSDate *currentDate = [dateFormater dateFromString:[[indicesTimeSalesArr objectAtIndex:row]objectForKey:@"0"]];
                    ////NSLog(@"currentDate : %@",currentDate);
                    
                    [dateFormater setDateFormat:@"HH:mm:ss"];
                    //    [dateFormater setDateFormat:@"H:mm:ss"];
                    NSString *convertedDateString = [dateFormater stringFromDate:currentDate];
                    ////NSLog(@"convertedDateString : %@",convertedDateString);
                    
                    cellTimeSale.lblTime.text= convertedDateString;
                    
                    cellTimeSale.lblVol.text = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:[[[indicesTimeSalesArr objectAtIndex:row]objectForKey:@"2"] floatValue] ]];
                    
                    // cellTimeSale.percent.text =
                    
                    cellTimeSale.separatorInset = UIEdgeInsetsZero;
                }
            }
            return cellTimeSale;
            
        }break;
            // MARK: MAIN MKT CELLS
        case 11:
        {
            ////NSLog(@"Goes Main");
            static NSString *CellIdentifierMain = @"CellMain";
            ScoreBoardTableViewCell *cellScoreBoardMain = [tableView dequeueReusableCellWithIdentifier:CellIdentifierMain];
            if (cellScoreBoardMain == nil)
            {
                cellScoreBoardMain = [[ScoreBoardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierMain];
            }
            int row = (int)indexPath.row;
            //   //NSLog(@"Row 11 %d and Count 11 %lu",row, (unsigned long)[[[[QCData singleton]qcScoreBoardDict] objectForKey:[NSNumber numberWithInt:2201]] allKeys].count);

            if (row==selectedRow1) {
                cellScoreBoardMain.lblName.textColor = [UIColor cyanColor];
            } else {
                cellScoreBoardMain.lblName.textColor = [UIColor whiteColor];
            }
            
                 
            NSString *stockCode = [[_mainMktArray objectAtIndex:row]objectForKey:FID_37_S_SECTOR_NAME];
            cellScoreBoardMain.lblName.text  = stockCode;
            NSNumber *vol =[[_mainMktArray objectAtIndex:row]objectForKey:FID_110_I_SCORE_VOL];
            NSString *volume = [[UserPrefConstants singleton] abbreviateNumber:[vol intValue]];
            cellScoreBoardMain.lblVol.text   = volume;//[quantityFormatter stringFromNumber:vol];
            cellScoreBoardMain.lblUp.text    = [[[_mainMktArray objectAtIndex:row] objectForKey:FID_105_I_SCORE_UP]stringValue];
            cellScoreBoardMain.lblDown.text  = [[[_mainMktArray objectAtIndex:row] objectForKey:FID_106_I_SCORE_DW]stringValue];
            cellScoreBoardMain.lblUnchg.text = [[[_mainMktArray objectAtIndex:row]objectForKey:FID_107_I_SCORE_UNCHG]stringValue];
            cellScoreBoardMain.lblUnTrd.text = [[[_mainMktArray objectAtIndex:row]objectForKey:FID_108_I_SCORE_NOTRD]stringValue];
            
            NSString *totalMainScore = [[UserPrefConstants singleton] abbreviateNumber:[[totalMarketDict objectForKey:@"mainTotalVol"] longLongValue] ];
            
            cellScoreBoardMain.lblTotal.text = [[[_mainMktArray objectAtIndex:row]objectForKey:FID_109_I_SCORE_TOTAL]stringValue];
            
            
            
            _MainMarketVoltotal.text= totalMainScore;//[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalVol"] ];
            _MMupTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalUp"] ];
            _MMDownTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalDown"] ];
            _MMUnchagTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalUnchg"] ];
            _MMUntradTotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalUntraded"] ];
            _MMTotalTotal.text= [quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"mainTotalTotal"] ];


            UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                        action:@selector(doubleTapMainMarket:)];
            doubleTap.numberOfTapsRequired = 2;
            [cellScoreBoardMain addGestureRecognizer:doubleTap];
            
            //changing row selected color
            UIView *selectionColor = [[UIView alloc] init];
            if (indexPath.row % 2==0) {
                selectionColor.backgroundColor = kCellGray1;
            } else {
                selectionColor.backgroundColor =  kCellGray2;
            }
            cellScoreBoardMain.selectedBackgroundView = selectionColor;
            cellScoreBoardMain.separatorInset = UIEdgeInsetsZero;
            return cellScoreBoardMain;
            
        }break;
            
            // MARK: WARRANT MKT CELLS
        case 12:
        {
            ////NSLog(@"Goes Warrant");
            static NSString *CellIdentifierWarrant = @"CellWarrant";
            ScoreBoardTableViewCell *cellScoreBoardWarrant = [tableView dequeueReusableCellWithIdentifier:CellIdentifierWarrant];
            if (cellScoreBoardWarrant == nil)
            {
                cellScoreBoardWarrant = [[ScoreBoardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierWarrant];
            }
            int row = (int)indexPath.row;
            ////NSLog(@"Row 12 %d and Count 12 %lu",row, (unsigned long)[[[[QCData singleton]qcScoreBoardDict] objectForKey:[NSNumber numberWithInt:2203]] allKeys].count);
         
            if (row==selectedRow2) {
                cellScoreBoardWarrant.lblName.textColor = [UIColor cyanColor];
            } else {
                cellScoreBoardWarrant.lblName.textColor = [UIColor whiteColor];
            }
            
            NSString *stockCode = [[_warrMktArray objectAtIndex:row]objectForKey:FID_37_S_SECTOR_NAME];
            cellScoreBoardWarrant.lblName.text  = stockCode;
            NSNumber *vol =[[_warrMktArray objectAtIndex:row]objectForKey:FID_110_I_SCORE_VOL];
            NSString *volume = [[UserPrefConstants singleton] abbreviateNumber:[vol intValue]];
                 
            cellScoreBoardWarrant.lblVol.text   = volume;//[quantityFormatter stringFromNumber:vol];
            cellScoreBoardWarrant.lblUp.text    = [[[_warrMktArray objectAtIndex:row]objectForKey:FID_105_I_SCORE_UP]stringValue];
            cellScoreBoardWarrant.lblDown.text  = [[[_warrMktArray objectAtIndex:row]objectForKey:FID_106_I_SCORE_DW]stringValue];
            cellScoreBoardWarrant.lblUnchg.text = [[[_warrMktArray objectAtIndex:row]objectForKey:FID_107_I_SCORE_UNCHG]stringValue];
            cellScoreBoardWarrant.lblUnTrd.text = [[[_warrMktArray objectAtIndex:row]objectForKey:FID_108_I_SCORE_NOTRD]stringValue];
            cellScoreBoardWarrant.lblTotal.text = [[[_warrMktArray objectAtIndex:row]objectForKey:FID_109_I_SCORE_TOTAL]stringValue];
                 
                 
            NSString *totalMainScore = [[UserPrefConstants singleton] abbreviateNumber:[[totalMarketDict objectForKey:@"warTotalVol"] intValue] ];
                 
            _wartotalvol.text= totalMainScore;//[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"warTotalVol"] ];
            _wartotalup.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"warTotalUp"] ];
            _wartotaldown.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"warTotalDown"] ];
            _wartotalunchg.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"warTotalUnchg"] ];
            _wartotaluntrd.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"warTotalUntraded"] ];
            _wartotaltotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"warTotalTotal"] ];
            
            


            UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(doubleTapWarrMarket:)];
            doubleTap.numberOfTapsRequired = 2;
            [cellScoreBoardWarrant addGestureRecognizer:doubleTap];
            
            //changing row selected color
            UIView *selectionColor = [[UIView alloc] init];
            if (indexPath.row % 2==0) {
                selectionColor.backgroundColor = kCellGray1;
            } else {
                selectionColor.backgroundColor =  kCellGray2;
            }
            cellScoreBoardWarrant.selectedBackgroundView = selectionColor;
            cellScoreBoardWarrant.separatorInset = UIEdgeInsetsZero;
            return cellScoreBoardWarrant;
            
        }break;
            
            // MARK: ACE MKT CELLS
        case 13:
        {
            ////NSLog(@"Goes Ace");
            static NSString *CellIdentifierAce = @"CellAce";
            ScoreBoardTableViewCell *cellScoreBoardAce = [tableView dequeueReusableCellWithIdentifier:CellIdentifierAce];
            if (cellScoreBoardAce == nil)
            {
                cellScoreBoardAce = [[ScoreBoardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifierAce];
            }
            int row = (int)indexPath.row;
             // //NSLog(@"Row 13 %d and Count 13 %lu",row, (unsigned long)[[[[QCData singleton]qcScoreBoardDict] objectForKey:[NSNumber numberWithInt:2204]] allKeys].count);
            
            
            if (row==selectedRow3) {
                cellScoreBoardAce.lblName.textColor = [UIColor cyanColor];
            } else {
                cellScoreBoardAce.lblName.textColor = [UIColor whiteColor];
            }
            
            
            NSString *stockCode = [[_acesMktArray objectAtIndex:row]objectForKey:FID_37_S_SECTOR_NAME];
            cellScoreBoardAce.lblName.text  = stockCode;
            NSNumber *vol =[[_acesMktArray objectAtIndex:row]objectForKey:FID_110_I_SCORE_VOL];
            NSString *volume = [[UserPrefConstants singleton] abbreviateNumber:[vol intValue]];
            
            cellScoreBoardAce.lblVol.text   = volume;//[quantityFormatter stringFromNumber:vol];
            cellScoreBoardAce.lblUp.text    = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_105_I_SCORE_UP]stringValue];
            cellScoreBoardAce.lblDown.text  = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_106_I_SCORE_DW]stringValue];
            cellScoreBoardAce.lblUnchg.text = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_107_I_SCORE_UNCHG]stringValue];
            cellScoreBoardAce.lblUnTrd.text = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_108_I_SCORE_NOTRD]stringValue];
            cellScoreBoardAce.lblTotal.text = [[[_acesMktArray objectAtIndex:row]objectForKey:FID_109_I_SCORE_TOTAL]stringValue];
            
            NSString *totalMainScore = [[UserPrefConstants singleton] abbreviateNumber:[[totalMarketDict objectForKey:@"aceTotalVol"] intValue] ];
            
            
            _acetotalvol.text= totalMainScore;//[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"aceTotalVol"] ];
            _acetotalup.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"aceTotalUp"] ];
            _acetotaldown.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"aceTotalDown"] ];
            _acetotalunchg.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"aceTotalUnchg"] ];
            _acetotaluntrd.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"aceTotalUntraded"] ];
            _acetotaltotal.text=[quantityFormatter stringFromNumber:[totalMarketDict objectForKey:@"aceTotalTotal"] ];
            

            UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(doubleTapAceMarket:)];
            doubleTap.numberOfTapsRequired = 2;
            [cellScoreBoardAce addGestureRecognizer:doubleTap];
            
            //changing row selected color
            UIView *selectionColor = [[UIView alloc] init];
            if (indexPath.row % 2==0) {
                selectionColor.backgroundColor = kCellGray1;
            } else {
                selectionColor.backgroundColor =  kCellGray2;
            }
            cellScoreBoardAce.selectedBackgroundView = selectionColor;
            cellScoreBoardAce.separatorInset = UIEdgeInsetsZero;
            return cellScoreBoardAce;
            
        }break;
            
        default:
            return cell;

            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [atp timeoutActivity];

    switch (tableView.tag) {
        case 1:
        {
            NSString *stockCode = [[_indicesValArray objectAtIndex:indexPath.row] objectForKey:FID_33_S_STOCK_CODE];
            stockcodeTimeandSales=stockCode;
            NSArray *splitStkCode = [stockCode componentsSeparatedByString:@"."];
            
            if (selectedRow!=indexPath.row) {
                prevSelectedRow = selectedRow;
                selectedRow = indexPath.row;
                if (selectedRow>=0) {
                    IndicesTableViewCell *currCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
                    currCell.lblName.textColor = [UIColor cyanColor];
                }
                if (prevSelectedRow>=0) {
                    IndicesTableViewCell *prevCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:prevSelectedRow inSection:0]];
                    prevCell.lblName.textColor = [UIColor whiteColor];
                }
            }
            
            
            [self loadChartMethodForIndex:[splitStkCode objectAtIndex:0]];
            
            //Time and sales Refresh
            NSString *latestTradedNumber = [[_indicesValArray objectAtIndex:indexPath.row] objectForKey:FID_103_I_TRNS_NO];
            [[VertxConnectionManager singleton] vertxGetTimeAndSalesByStockCode:stockCode begin:@"0" end:latestTradedNumber exchange:[UserPrefConstants singleton].userCurrentExchange ];

             _chartTitleLabel.text = [LanguageManager stringForKey:@"Movement & Chart - %@" withPlaceholders:@{@"%moveChart%":[[_indicesValArray objectAtIndex:indexPath.row] objectForKey:FID_39_S_COMPANY]}];
            
            
        } break;
            
        case 11: {
            
            if (selectedRow1!=indexPath.row) {
                prevSelectedRow1 = selectedRow1;
                selectedRow1 = indexPath.row;
                if (selectedRow1>=0) {
                    IndicesTableViewCell *currCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow1 inSection:0]];
                    currCell.lblName.textColor = [UIColor cyanColor];
                }
                if (prevSelectedRow1>=0) {
                    IndicesTableViewCell *prevCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:prevSelectedRow1 inSection:0]];
                    prevCell.lblName.textColor = [UIColor whiteColor];
                }
                
               // NSLog(@"_mainMktArray Selected %@",[_mainMktArray objectAtIndex:indexPath.row]);
            }
            
        } break;
            
        case 12: {
            
            if (selectedRow2!=indexPath.row) {
                prevSelectedRow2 = selectedRow2;
                selectedRow2 = indexPath.row;
                if (selectedRow2>=0) {
                    IndicesTableViewCell *currCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow2 inSection:0]];
                    currCell.lblName.textColor = [UIColor cyanColor];
                }
                if (prevSelectedRow2>=0) {
                    IndicesTableViewCell *prevCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:prevSelectedRow2 inSection:0]];
                    prevCell.lblName.textColor = [UIColor whiteColor];
                }
            }
            
        } break;
            
        case 13: {
            
            if (selectedRow3!=indexPath.row) {
                prevSelectedRow3 = selectedRow3;
                selectedRow3 = indexPath.row;
                if (selectedRow3>=0) {
                    IndicesTableViewCell *currCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow3 inSection:0]];
                    currCell.lblName.textColor = [UIColor cyanColor];
                }
                if (prevSelectedRow3>=0) {
                    IndicesTableViewCell *prevCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:prevSelectedRow3 inSection:0]];
                    prevCell.lblName.textColor = [UIColor whiteColor];
                }
            }
            
        } break;
            
        default:
            break;
    }

    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // tag 1    - IndiceTableView
    // tag 2    - Time and sales
    // tag 11   - Main Market
    // tag 12   - StrWarr Market
    // tag 13   - Ace Market
   

        if (indexPath.row % 2==0) {
            cell.contentView.backgroundColor = kCellGray1;
        } else {
            cell.contentView.backgroundColor =  kCellGray2;
        }
    
    
    if (tableView.tag==1) {
        
        
        if (indexPath.row % 2==0) {
            cell.selectedBackgroundView.backgroundColor = kCellGray1;
        } else {
            cell.selectedBackgroundView.backgroundColor = kCellGray2;
        }

        
    }
    
    if((tableView.tag==13) && ([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)){
            //end of loading
            //for example [activityIndicator stopAnimating];
        _refreshButton.enabled=YES;

    }
    
}

#pragma mark - Get Sector By Name
//++++ Sector for sect Name
-(SectorInfo*)getSectorInfoObjFor:(NSString*)sectName {
    for (int i=0; i<[_sectorInfoArray count]; i++) {
        
        SectorInfo *inf = [_sectorInfoArray objectAtIndex:i];
        if ([inf.sectorName isEqualToString:sectName]) {
            return inf;
        }
        
        if ([inf.subItemsArray count]>0) {
            for (int j=0; j<[inf.subItemsArray count]; j++) {
                SectorInfo *subInf = [inf.subItemsArray objectAtIndex:j];
                if ([subInf.sectorName isEqualToString:sectName]) {
                    return subInf;
                }
            }
        }
        
    }
    return nil;
}


#pragma mark - doubleTap Market indices handlers

-(void)doubleTapMainMarket:(UITapGestureRecognizer*)tap {
    CGPoint point = [tap locationInView:self.mainMarketTableView];
    NSIndexPath* indexPath = [self.mainMarketTableView indexPathForRowAtPoint: point];
    ScoreBoardTableViewCell* cell = [self.mainMarketTableView cellForRowAtIndexPath:indexPath];
    
    [UserPrefConstants singleton].sectorToFilter = [self getSectorInfoObjFor:cell.lblName.text];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"openQuoteScreenFromIndices" object:self userInfo:nil];
}

-(void)doubleTapWarrMarket:(UITapGestureRecognizer*)tap {
    CGPoint point = [tap locationInView:self.strcwarrTableView];
    NSIndexPath* indexPath = [self.strcwarrTableView indexPathForRowAtPoint: point];
    ScoreBoardTableViewCell* cell = [self.strcwarrTableView cellForRowAtIndexPath:indexPath];
    
    [UserPrefConstants singleton].sectorToFilter = [self getSectorInfoObjFor:cell.lblName.text];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"openQuoteScreenFromIndices" object:self userInfo:nil];
}

-(void)doubleTapAceMarket:(UITapGestureRecognizer*)tap {
    CGPoint point = [tap locationInView:self.aceMarketTableView];
    NSIndexPath* indexPath = [self.aceMarketTableView indexPathForRowAtPoint: point];
    ScoreBoardTableViewCell* cell = [self.aceMarketTableView cellForRowAtIndexPath:indexPath];
    
    [UserPrefConstants singleton].sectorToFilter = [self getSectorInfoObjFor:cell.lblName.text];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"openQuoteScreenFromIndices" object:self userInfo:nil];
}

#pragma mark - Other methods

- (void)calculateCustomFields:(NSDictionary *)marketDict
{
    NSArray *headerFIDs = @[@"37",@"110",@"105",@"106",@"107",@"108",@"109",@"111"];
    
    for (NSDictionary *dict in [marketDict allValues])//Mining, REITS, IPC, Finance...
    {
        for(NSString *fid in headerFIDs)//UP, Down, Unchanged..
        {
            if([fid isEqualToString:@"105"])
            {
                totalUp += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"106"])
            {
                totalDown += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"107"])
            {
                totalUnchg += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"108"])
            {
                totalUntrd += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"109"])
            {
                totalTotal += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"110"])
            {
                totalVol += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"111"])
            {
                totalValue += [[dict objectForKey:fid]integerValue];
            }
        }
    }
    
    NSString *marketID;
    if (marketDict)
    {
        marketID = [[[[marketDict allValues]objectAtIndex:0]objectForKey:@"35"]stringValue];
    }
    if([marketID isEqualToString:@"2201"])
    {
        ////NSLog(@"v2201: %@",[marketDict objectForKey:@"35"]);
        
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUp]     forKey:@"mainTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalDown]   forKey:@"mainTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUnchg]  forKey:@"mainTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUntrd]  forKey:@"mainTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalTotal]  forKey:@"mainTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"mainTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalValue]  forKey:@"mainTotalValue"];
    }
    else if([marketID isEqualToString:@"2203"])
        
    {
        ////NSLog(@"v2203 %@",[marketDict objectForKey:@"35"]);
        
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUp]     forKey:@"warTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalDown]   forKey:@"warTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUnchg]  forKey:@"warTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUntrd]  forKey:@"warTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalTotal]  forKey:@"warTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"warTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalValue]  forKey:@"warTotalValue"];
        
    }
    else if([marketID isEqualToString:@"2204"])
    {
        ////NSLog(@"v2204: %@",[marketDict objectForKey:@"35"]);
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUp]     forKey:@"aceTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalDown]   forKey:@"aceTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUnchg]  forKey:@"aceTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalUntrd]  forKey:@"aceTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalTotal]  forKey:@"aceTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"aceTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalValue]  forKey:@"aceTotalValue"];
    }
    
    ////NSLog(@"totalUp : %d",totalUp);
    ////NSLog(@"totalDown : %d",totalDown);
    ////NSLog(@"totalUnchg : %d",totalUnchg);
    ////NSLog(@"totalUntrd : %d",totalUntrd);
    ////NSLog(@"totalTotal : %d",totalTotal);
    ////NSLog(@"totalVol : %d",totalVol);
    ////NSLog(@"totalValue : %d",totalValue);
    totalUp = 0;
    totalDown = 0;
    totalUnchg = 0;
    totalUntrd = 0;
    totalTotal = 0;
    totalVol = 0;
    totalValue = 0;
}


- (IBAction)refreshButtonPressed:(UIButton *)sender {
    currentTime = [NSDate date];
    NSString *resultString =  [LanguageManager stringForKey:@"Last Updated: %@" withPlaceholders:@{@"%lastupdate%":[dateFormatterscoreboard stringFromDate: currentTime]}];

    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"]; //Main
        _mainMarketTimeStamp.text=resultString;
  
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"]; //Warrant
        _warrantMarketTimeSTamp.text=resultString;
   
    [[VertxConnectionManager singleton] vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
        _aceMarketTimeStamp.text=resultString;
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    // Do whatever you want here

    [activityAnimation stopAnimating];
    
    NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) urlResponse.response;
    NSInteger statusCode = httpResponse.statusCode;
    if (statusCode > 399) {
        //NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response":httpResponse}];
        // Forward the error to webView:didFailLoadWithError: or other
        //NSLog(@"%@", error);
        [_chartErrorMsg setHidden:NO];
        [_imgChart setHidden:YES];
        [movementAndChartWebView setHidden:YES];
        [_chartErrorMsg setHidden:NO];
    }
    else {
        // No HTTP error
        [_chartErrorMsg setHidden:YES];
        [_imgChart setHidden:YES];
        [movementAndChartWebView setHidden:NO];
        [_chartErrorMsg setHidden:YES];
    }
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Can't Load Charts at this Moment.Please Check Back Later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [alert show];
    
    [self setChartStatus:6];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    
}




- (void) dealloc{
    
//    //NSLog(@"goodbye Indices");
//    indiceTableView = nil;
//    movementAndChartWebView= nil;
//    timeSalesTableView= nil;
//    mainMarketTableView= nil;
//    strcwarrTableView= nil;
//    aceMarketTableView= nil;
//    _MainMarketVoltotal= nil;
//    //NSLog(@"viewDidDisappear Indices");
//    _MMupTotal= nil;
//    _MMDownTotal= nil;
//    _MMUnchagTotal= nil;
//    _MMUntradTotal= nil;
//    _MMTotalTotal= nil;
//    _wartotalvol= nil;
//    _wartotalup= nil;
//    _wartotaldown= nil;
//    _wartotalunchg= nil;
//    _wartotaluntrd= nil;
//    _wartotaltotal= nil;
//    _acetotalvol= nil;
//    _acetotalup= nil;
//    _acetotaldown= nil;
//    _acetotalunchg= nil;
//    _acetotaluntrd= nil;
//    _acetotaltotal= nil;
//    
//    _refreshButton= nil;
//    _mainMarketTimeStamp= nil;
//    _warrantMarketTimeSTamp= nil;
//    _aceMarketTimeStamp= nil;
//    
//    activityAnimation= nil;
//    for (CALayer* layer in [self.view.layer sublayers])
//    {
//        [layer removeAllAnimations];
//    }
    //Add Observer
 //   [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doneVertxGetScoreBoardWithExchange" object:nil];
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DoneGetIndices" object:nil];
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedTimeAndSales" object:nil];
    
}

@end
