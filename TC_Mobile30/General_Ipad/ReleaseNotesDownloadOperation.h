//
//  ReleaseNotesDownloadOperation.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 12/31/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReleaseNotesDownloadOperation : NSOperation

@property (readonly, copy, nonatomic) NSString *releaseNotesText;

@property (readonly, strong, nonatomic) NSError *error;

- (id)initWithAppIdentifier:(NSString *)appIdentifier;

@end
