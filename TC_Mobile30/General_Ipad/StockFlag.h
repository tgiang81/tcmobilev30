//
//  StockPlate.h
//  TCUniversal
//
//  Created by Scott Thoo on 10/1/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPrefConstants.h"
@protocol StockFlagDelegate <NSObject>
@optional
- (void)didPressStockFlagWithStockCode:(NSString*)stkCode;

@end


@interface StockFlag : UIView
@property (nonatomic, weak) id<StockFlagDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *lblStockName;
@property (weak, nonatomic) IBOutlet UILabel *lblPercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDone;
@property (weak, nonatomic) IBOutlet UILabel *lblLastChange;
@property (weak, nonatomic) IBOutlet UIImageView *imgFlag;

@property (weak,nonatomic) NSString *stockCode;


- (id)initWithStockDetails:(NSString *)stkCode
                   stkName:(NSString *)stkName
             stkPercentage:(NSString *)stkPercentage
                 stkVolume:(NSString *)stkVolume
               stkLastDone:(NSString *)stkLastDone
             stkLastChange:(NSString *)stkLastChange;

- (IBAction)btnPressed:(id)sender;

@property (weak, nonatomic) UserPrefConstants *userPref;

@end
