//
//  VirtualPLTableViewCell.h
//  TCiPad
//
//  Created by n2n on 22/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VirtualPLTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
