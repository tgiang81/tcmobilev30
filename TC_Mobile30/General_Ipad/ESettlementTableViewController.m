//
//  ESettlementTableViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 18/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "ESettlementTableViewController.h"
#import "AppControl.h"
#import "UserPrefConstants.h"
#import <Foundation/Foundation.h>
#define CLIENT_SUMMARY 1
#define MONTHLY_STATEMENT 2
#define MARGIN_ACCOUNT_SUMMARY 3
#define IBEYOND 15
#define TRADER_DEPOSIT 16
#define E_SETTLEMENT 21
#define E_STATUS 22
#define E_DEPOSIT 23
#define NEWS_RESEARCH 44
#define STOCK_SCREENER 46
#define OTHER_SERVICES 24
#define NEW_SUBSCRIPTION 25
#define CURRENT_SUBSCRIPTION 26
#define ACCOUNT_REPORT 18
#define PURCHASE_SALES_REPORT 19

@interface ESettlementTableViewController () <UITableViewDelegate, UITableViewDataSource>{
    
    NSArray *tableDataSection1;
    NSArray *tableDataSection2;
        NSArray *tableDataSection4;
    NSArray *tableConstantSection1;
    NSArray *tableConstantSection2;
        NSArray *tableConstantSection4;
    
}

@end

@implementation ESettlementTableViewController


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 58;
}


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    

    
    // [self.tableView setFrame:newRect];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    /* TCPlus titles
     
     Client Summary
     Monthly Statement
     Margin Account Summary
     Trader Deposit Report
     TradeBeyond Report
     */
    
      NSString *exchange = ([UserPrefConstants singleton].userCurrentExchange.length>2)?([[UserPrefConstants singleton].userCurrentExchange substringToIndex:2]):([UserPrefConstants singleton].userCurrentExchange);
  //  NSLog(@"Exchange %@",exchange);
  
    if ([exchange isEqualToString:@"PH"]) {
        tableDataSection1 = [NSArray arrayWithObjects:
                             [LanguageManager stringForKey:@"Client Summary"],
                             [LanguageManager stringForKey:@"Statement of Account Report"],
                             [LanguageManager stringForKey:@"Purchase and Sales Invoice Report"],
                             nil];
        
        tableConstantSection1 = [NSArray arrayWithObjects:
                                 [NSNumber numberWithInt:CLIENT_SUMMARY],
                                 [NSNumber numberWithInt:ACCOUNT_REPORT],
                                 [NSNumber numberWithInt:PURCHASE_SALES_REPORT],
        
                                 nil];
        
        
        
        sectionNumber = 1;
    }else{
        tableDataSection1 = [NSArray arrayWithObjects:
                             [LanguageManager stringForKey:@"Client Summary"],
                             [LanguageManager stringForKey:@"Monthly Statement"],
                             [LanguageManager stringForKey:@"Margin Account Summary"],
                             [LanguageManager stringForKey:@"Trader Deposit Report"],
                             [LanguageManager stringForKey:@"TradeBeyond Report"],
                             nil];
        
        tableDataSection2 = [NSArray arrayWithObjects:
                             [LanguageManager stringForKey:@"eSettlement"],
                             [LanguageManager stringForKey:@"Settlement Status"],
                             [LanguageManager stringForKey:@"eDeposit"],nil];
        
        tableDataSection4 = [NSArray arrayWithObjects:
                             [LanguageManager stringForKey:@"New Subscription"],
                             [LanguageManager stringForKey:@"Current Subscription"],nil];
        
        tableConstantSection1 = [NSArray arrayWithObjects:
                                 [NSNumber numberWithInt:CLIENT_SUMMARY],
                                 [NSNumber numberWithInt:MONTHLY_STATEMENT],
                                 [NSNumber numberWithInt:MARGIN_ACCOUNT_SUMMARY],
                                 [NSNumber numberWithInt:TRADER_DEPOSIT],
                                 [NSNumber numberWithInt:IBEYOND],
                                 nil];
        
        tableConstantSection2 = [NSArray arrayWithObjects:[NSNumber numberWithInt:E_SETTLEMENT],
                                 [NSNumber numberWithInt:E_STATUS],
                                 [NSNumber numberWithInt:E_DEPOSIT],nil];
        
        
        tableConstantSection4 = [NSArray arrayWithObjects:
                                 [NSNumber numberWithInt:NEW_SUBSCRIPTION],[NSNumber numberWithInt:CURRENT_SUBSCRIPTION],nil];
        
        
        sectionNumber = 3;
    }
    
    [self.tableView setBackgroundColor:[UIColor colorWithHue:0 saturation:0 brightness:0.1 alpha:1]];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return sectionNumber;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section==0) {
        return tableDataSection1.count;
    }
    if(section==1){
        return tableDataSection2.count;
    }
    if(section==2){
        return tableDataSection4.count;
    }
    return 0;

}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor cyanColor]];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return [LanguageManager stringForKey:@"Reports"];
    if (section == 1)
        return [LanguageManager stringForKey:@"Settlement"];
    if (section == 2) {
        return [LanguageManager stringForKey:@"Subscription"];
    }
    
    return @"undefined";
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"ESettlementTableViewCell";
    
    ESettlementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[ESettlementTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if (indexPath.section == 0) {
         if (indexPath.row < [tableDataSection1 count]) {
             cell.eSettlementLabel.text = [tableDataSection1 objectAtIndex:indexPath.row];
         }
    }
    
    if(indexPath.section == 1){
        if (indexPath.row < [tableDataSection2 count]) {
            cell.eSettlementLabel.text = [tableDataSection2 objectAtIndex:indexPath.row];
        }
    }
    

    
    if(indexPath.section == 2)
    {
        if (indexPath.row < [tableDataSection4 count]) {
            cell.eSettlementLabel.text = [tableDataSection4 objectAtIndex:indexPath.row];
        }
    }
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2==0) {
        cell.backgroundColor = kCellGray1;
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.backgroundColor = kCellGray2;
        cell.contentView.backgroundColor = kCellGray2;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if (indexPath.row < [tableDataSection1 count]) {
            if (_delegate) {
                
                
                
                NSString *userATP = [UserPrefConstants singleton].privateIP;
                
                NSString *userParam = [self urlencodedString:[[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"]];
                //https://uat.itradecimb.com.my/gcCIMBV5/ext/rw.jsp
                NSString *urlString = [NSString stringWithFormat:@"%@?ref=%@~1~%@~%@~80~%@&enc=N",[UserPrefConstants singleton].eSettlementURL,userParam,userATP,[UserPrefConstants singleton].userName,[tableConstantSection1 objectAtIndex:indexPath.row]];
               // NSLog(@"url String %@",urlString);
                
                [_delegate didClickTableView:urlString andTitle:[LanguageManager stringForKey:@"Reports"]];
            }
        }
    }
    else if (indexPath.section == 1) {
        if (indexPath.row < [tableDataSection2 count]) {
            if (_delegate) {
                NSString *userParam = [self urlencodedString:[[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"]];
                
                       NSString *userATP = [UserPrefConstants singleton].privateIP;
                
                NSString *urlString = [NSString stringWithFormat:@"%@?ref=%@~1~%@~%@~80~%@&enc=N",[UserPrefConstants singleton].eSettlementURL,userParam,userATP,[UserPrefConstants singleton].userName,[tableConstantSection2 objectAtIndex:indexPath.row]];
                NSLog(@"url String %@",urlString);
                
                [_delegate didClickTableView:urlString andTitle:[LanguageManager stringForKey:@"Settlement"]];
            }
        }
    }

    else if (indexPath.section == 2) {
        if (indexPath.row < [tableDataSection4 count]) {
            if (_delegate) {
                NSString *userParam = [self urlencodedString:[[[UserPrefConstants singleton] userInfoDict] objectForKey:@"[UserParam]"]];
                
                    NSString *userATP = [UserPrefConstants singleton].privateIP;
                
                NSString *urlString = [NSString stringWithFormat:@"%@?ref=%@~1~%@~%@~80~%@&enc=N",[UserPrefConstants singleton].eSettlementURL,userParam,userATP,[UserPrefConstants singleton].userName,[tableConstantSection4 objectAtIndex:indexPath.row]];
               // NSLog(@"url String %@",urlString);
                
                [_delegate didClickTableView:urlString andTitle:[LanguageManager stringForKey:@"Subscription"]];
            }
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)urlencodedString:(NSString *)input{
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)input,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingISOLatin1));
    return encodedString;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
