//
//  DetailsTableViewCell.h
//  TCiPad
//
//  Created by n2n on 19/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailsTitle;
@property (weak, nonatomic) IBOutlet UILabel *detailsContent;


@property (weak, nonatomic) IBOutlet UILabel *detailsTitleRight;
@property (weak, nonatomic) IBOutlet UILabel *detailsContentRight;

- (void)setupColor;
@end
