//
//  StockAlertAPI.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 10/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "StockAlertAPI.h"
#import "UserPrefConstants.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <sys/utsname.h>
#import "Reachability.h"
#import "JWT.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "NSData-AES.h"
#import "JNKeychain.h"
#import "NSString+MD5.h"
#import "AFNetworking.h"
#import "APIStockAlert.h"
#define SALT_HASH @"FvTivqTqZXsgLLx1v3P8TGRy" // use for decipher data from keychain

static StockAlertAPI *_instance = nil;



@implementation StockAlertAPI

+(StockAlertAPI *)instance
{
    if(_instance) return _instance;
    
    @synchronized([StockAlertAPI class])
    {
        if(!_instance)
        {
            _instance = [[self alloc] init];
            
        }
        return _instance;
    }
    
    return nil;
}


- (void) getAESKeyAPI{
    
    [UserPrefConstants singleton].ipAddress = [self getIPAddress];
    [UserPrefConstants singleton].deviceUDID = [self identifierForVendor];
}

-(NSString*) deviceName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}
-(BOOL)hasCellularCoverage
{
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    if (!carrier.isoCountryCode) {
       // NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return NO;
    }
    
    return YES;
}

- (void) storePushNotificationInfo:(NSString *)accountName
                           andUDID:(NSString *)deviceUDID
                        andExhange:(NSString *)exchange
                    andSponsorCode:(NSString *)sponsorCode
                         andBHCode:(NSString *)bhCode
                          andAppID:(NSString *)appID
                       andBundleId:(NSString *)bundleID
                     andClientCode:(NSString *)clientCode
                    andDeviceToken:(NSString *)deviceToken
                      andIPAddress:(NSString *)ipAddress
                 andActivationCode:(NSString *)activationCode{
    [APIStockAlert storePushNotificationInfo:accountName andUDID:deviceUDID andExhange:exchange andSponsorCode:sponsorCode andBHCode:bhCode andAppID:appID andBundleId:bundleID andClientCode:clientCode andDeviceToken:deviceToken andIPAddress:ipAddress andActivationCode:activationCode completeHandler:^(BOOL success, id responseObject, NSError *error) {
        if(error != nil){
            [UserPrefConstants singleton].isActivatePushNotification = YES;
            NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
            [def setBool:YES forKey:@"PushNotificationMode"];
            [def synchronize];
            
            NSLog(@"Register JSON: %@", responseObject);
        }else{
            NSLog(@"Register Failed: %@", error);
        }
        
    }];
    /* ggtt comment -> should update new AFNetworking method
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
   // NSLog(@"BhCode %@",bhCode);
    NSMutableDictionary *sendMessage = [[NSMutableDictionary alloc]init];
    [sendMessage setObject:exchange forKey:@"ex"];
    [sendMessage setObject:sponsorCode forKey:@"spc"];
    [sendMessage setObject:bhCode forKey:@"bh"];
    [sendMessage setObject:accountName forKey:@"lid"];
    [sendMessage setObject:appID forKey:@"appId"];
    [sendMessage setObject:clientCode forKey:@"cc"];
    [sendMessage setObject:bundleID forKey:@"bdlID"];
    [sendMessage setObject:deviceToken forKey:@"dvTkn"];
    
    //platform name
    [sendMessage setObject:@"iOS" forKey:@"pfmNm"];
    //application version
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [sendMessage setObject:appVersion forKey:@"appV"];
    
    //devicemodel
    [sendMessage setObject:[self deviceName] forKey:@"dvMdl"];
    
    //devicenewtype
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status == ReachableViaWiFi)
    {
        //WiFi
        [sendMessage setObject:@"Wifi" forKey:@"dvNwT"];
    }
    else if (status == ReachableViaWWAN)
    {
        //3G
        [sendMessage setObject:@"3G" forKey:@"dvNwT"];
    }
    
    //devicenewprovider
    if ([self hasCellularCoverage]) {
        CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
        CTCarrier *carrier = [netinfo subscriberCellularProvider];
        [sendMessage setObject:[carrier carrierName] forKey:@"dvNwPvd"];
    }else{
        
        [sendMessage setObject:@"NoCarrierName" forKey:@"dvNwPvd"];
    }
    
    // Device UDID
    [sendMessage setObject:[UserPrefConstants singleton].deviceUDID forKey:@"imei"];
    
    //platforminfo
    float osSystem = [[UIDevice currentDevice].systemVersion floatValue];
    [sendMessage setObject:[NSString stringWithFormat:@"OSVer/%.2f",osSystem] forKey:@"pfmInfo"];
    //platformscreensize
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    NSString *screenSize = [NSString stringWithFormat:@"%.0fx%.0f",screenWidth,screenHeight];
    [sendMessage setObject:screenSize forKey:@"pfmScSz"];
    
    NSArray *keys = [sendMessage allKeys];
    
    keys = [[keys mutableCopy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableString *result = [NSMutableString string];
    for (NSString *key in keys) {
        id value = sendMessage[key];
        if (result.length) {
            [result appendString:@","];
        }
        [result appendFormat:@"\"%@\":\"%@\"", key, [value description]];
    }
    
    NSString *md5String = [NSString stringWithFormat:@"{%@}",result];
    
    NSLog(@"MD5 String %@",result);
    
    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
    NSDate* newDate = [[NSDate date] dateByAddingTimeInterval:600];
    NSDate* oldDate = [[NSDate date] dateByAddingTimeInterval:-600];
    long distantFuture = (long)(NSTimeInterval)([newDate timeIntervalSince1970]);
    long notBeforeAt = (long)(NSTimeInterval)([oldDate timeIntervalSince1970]);
    
    NSDictionary *payload = @{@"iss" : bhCode,
                              @"aud": @"SANS",
                              @"exp": [NSNumber numberWithUnsignedInteger:distantFuture],
                              @"jti": @"",
                              @"iat": [NSNumber numberWithUnsignedInteger:currentTime],
                              @"nbf": [NSNumber numberWithUnsignedInteger:notBeforeAt],
                              @"sub": @"",
                              @"ev": md5String
                              };
    
    NSString *algorithmName = @"RS256";
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Secret_Key_Certificates" ofType:@"p12"];
    NSData *privateKeySecretData = [NSData dataWithContentsOfFile:filePath];
    
    NSString *passphraseForPrivateKey = @"secret";
    
    JWTBuilder *builder = [JWTBuilder encodePayload:payload].secretData(privateKeySecretData).privateKeyCertificatePassphrase(passphraseForPrivateKey).algorithmName(algorithmName);
    NSString *jwtToken = builder.encode;
    
    NSMutableDictionary *sendMessageJwt = [[NSMutableDictionary alloc]init];
    [sendMessageJwt setObject:jwtToken forKey:@"jwt"];
    [sendMessageJwt setObject:bhCode forKey:@"bh"];
    [sendMessageJwt setObject:appID forKey:@"appId"];
    
    
    NSArray *keysjwt = [sendMessageJwt allKeys];
    
    keysjwt = [[keysjwt mutableCopy] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSMutableString *resultJwt = [NSMutableString string];
    for (NSString *key in keysjwt) {
        id value = sendMessageJwt[key];
        if (resultJwt.length) {
            [resultJwt appendString:@","];
        }
        [resultJwt appendFormat:@"\"%@\":\"%@\"", key, [value description]];
    }
    
    // NSLog(@"resultJwt %@",resultJwt);
    
    NSString *evStringJWT = [NSString stringWithFormat:@"{%@}",resultJwt];
    
    NSData *encryptedJWTData = [self encryptString:evStringJWT withKey:@"4ba2dca2cd70234103611b7f7858cb15"];
    
    NSString *signatureString = [encryptedJWTData base64EncodedStringWithOptions:0];
    
    NSString *urlPushStoreInfo = [NSString stringWithFormat:@"%@?ct=3&ae=2&ev=%@&cs=%@",[UserPrefConstants singleton].pushNotificationRegisterURL,signatureString,[signatureString MD5]];
    NSString *encoded = [urlPushStoreInfo stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"urlPushStoreInfo %@",urlPushStoreInfo);
     //NSLog(@"encoded %@",encoded);
    
    [manager POST:encoded parameters:sendMessage
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              
              [UserPrefConstants singleton].isActivatePushNotification = YES;
              
              NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
              [def setBool:YES forKey:@"PushNotificationMode"];
              [def synchronize];
              
              NSLog(@"Register JSON: %@", responseObject);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               NSLog(@"Register Failed: %@", error);
          }];
    */
}

- (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key {
    return [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKeyCBC:key];
}

#pragma mark GetDeviceUDID

- (NSString *)getDeviceUDID{
    
    NSString *password = SALT_HASH;
    
    NSString *keyUDID = [NSString stringWithFormat:@"%@+udid",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]];
    
    NSData *ciphertext =  [JNKeychain loadValueForKey:keyUDID forAccessGroup:nil];
    
    if (ciphertext==nil) {
        return @"No UDID";
    }
    
    NSData *keyData = [self hashKey:password];
    
    NSError *error = nil;
    NSData * _secretData = [self tripleDesDecryptData:ciphertext key:keyData error:&error];
    
    NSString *dataString = [NSString stringWithUTF8String:[_secretData bytes]];
    
    return dataString;
}

- (void) saveUDID:(NSString *)deviceUDID{
    
    NSString* keyPassword = SALT_HASH;
    
    NSData *data = [deviceUDID dataUsingEncoding:NSUTF8StringEncoding];
    NSData *keyData = [self hashKey:keyPassword];
    
    NSData * _secretData = [self tripleDesEncryptData:data key:keyData error:nil];
    
    //NSLog(@"encrypted data length: %lu", (unsigned long)_secretData.length);
    
    [self saveKeychain:_secretData forKey:[NSString stringWithFormat:@"%@+udid",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]] andAccessGroup:nil];
}

#pragma mark Encryption/Decryption

- (NSString *) identifierForVendor
{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
        
        
        NSString *udid = [self getDeviceUDID];
        
    //    NSLog(@"Device UDID %@",udid);
        
        if (udid.length==0 || [udid isEqualToString:@"No UDID"]) {
            udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
            [self saveUDID:udid];
            
      //      NSLog(@"Save UDID %@",udid);
        }
        
        return udid;
        //  return
    }
    return @"";
}



- (NSData *) hashKey:(NSString *)hash{
    unsigned char result[1000];
    const char *cStr = [hash UTF8String];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    
    for (int jval = 0, kval = 16; jval < 8;) {
        result[kval++] = result[jval++];
    }
    return [NSMutableData dataWithBytes:result length:24];;
}

- (NSData *)tripleDesDecryptData:(NSData *)inputData
                             key:(NSData *)keyData
                           error:(NSError **)error
{
    NSParameterAssert(inputData);
    NSParameterAssert(keyData);
    
    size_t outLength;
    
    NSAssert(keyData.length == kCCKeySize3DES, @"the keyData is an invalid size");
    
    NSMutableData *outputData = [NSMutableData dataWithLength:(inputData.length  +  kCCBlockSize3DES)];
    
    CCCryptorStatus
    result = CCCrypt(kCCDecrypt, // operation
                     kCCAlgorithm3DES, // Algorithm
                     kCCOptionPKCS7Padding | kCCOptionECBMode, // options
                     keyData.bytes, // key
                     keyData.length, // keylength
                     nil,// iv
                     inputData.bytes, // dataIn
                     inputData.length, // dataInLength,
                     outputData.mutableBytes, // dataOut
                     outputData.length, // dataOutAvailable
                     &outLength); // dataOutMoved
    
    if (result != kCCSuccess) {
        if (error != NULL) {
            *error = [NSError errorWithDomain: BUNDLEID_HARDCODE_TESTING
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    [outputData setLength:outLength];
    return outputData;
}

- (NSData *)tripleDesEncryptData:(NSData *)inputData
                             key:(NSData *)keyData
                           error:(NSError **)error
{
    NSParameterAssert(inputData);
    NSParameterAssert(keyData);
    
    size_t outLength;
    
    NSAssert(keyData.length == kCCKeySize3DES, @"the keyData is an invalid size");
    
    NSMutableData *outputData = [NSMutableData dataWithLength:(inputData.length  +  kCCBlockSize3DES)];
    
    CCCryptorStatus
    result = CCCrypt(kCCEncrypt, // operation
                     kCCAlgorithm3DES, // Algorithm
                     kCCOptionPKCS7Padding | kCCOptionECBMode, // options
                     keyData.bytes, // key
                     keyData.length, // keylength
                     nil,// iv
                     inputData.bytes, // dataIn
                     inputData.length, // dataInLength,
                     outputData.mutableBytes, // dataOut
                     outputData.length, // dataOutAvailable
                     &outLength); // dataOutMoved
    
    if (result != kCCSuccess) {
        if (error != NULL) {
            *error = [NSError errorWithDomain: BUNDLEID_HARDCODE_TESTING
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    [outputData setLength:outLength];
    return outputData;
}

- (void)saveKeychain:(NSData *)value forKey:(NSString *)key andAccessGroup:(NSString *)group
{
    NSString *forGroupLog = (group ? [NSString stringWithFormat:@" for access group '%@'", group] : @"");
    
    if ([JNKeychain saveValue:value forKey:key forAccessGroup:group]) {
      //  NSLog(@"Correctly saved value '%@' for key '%@'%@", value, key, forGroupLog);
    } else {
        //NSLog(@"Failed to save!%@", forGroupLog);
    }
    
    //NSLog(@"Value for key '%@' is: '%@'%@", key, [JNKeychain loadValueForKey:key forAccessGroup:group], forGroupLog);
    
    
}

- (void) deleteKeychain:(NSString *)value forKey:(NSString *)key andAccessGroup:(NSString *)group{
    
    NSString *forGroupLog = (group ? [NSString stringWithFormat:@" for access group '%@'", group] : @"");
    if ([JNKeychain deleteValueForKey:key forAccessGroup:group]) {
      //  NSLog(@"Deleted value for key '%@'. Value is: '%@'%@", key, [JNKeychain loadValueForKey:key forAccessGroup:group], forGroupLog);
    } else {
       // NSLog(@"Failed to delete!%@", forGroupLog);
    }
}

#pragma mark IPAddress

- (NSString *)getIPAddress {
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString *addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]; // pdp_ip0
                //NSLog(@"NAME: \"%@\" addr: %@", name, addr); // see for yourself
                
                if([name isEqualToString:@"en0"]) {
                    // Interface is the wifi connection on the iPhone
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        // Interface is the cell connection on the iPhone
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    NSString *addr = wifiAddress ? wifiAddress : cellAddress;
    
    return addr;
    
}

- (NSString *)genRandStringLength:(int)len {
    static NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    return randomString;
}

- (NSString *) getMobileToken{
    
    // Temporary Used the Mobile Token NSUder Default for this POC
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *mtoken = [defaults stringForKey:@"MobileToken"];
    if (mtoken.length==0) {
        
        mtoken = [self genRandStringLength:100];
        [defaults setObject:mtoken forKey:@"MobileToken"];
        [defaults synchronize];
    }
    return mtoken;
}




@end
