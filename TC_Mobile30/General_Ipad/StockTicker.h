//
//  StockTicker.h
//  TCUniversal
//
//  Created by Scott Thoo on 10/30/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPrefConstants.h"

@interface StockTicker : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *lblStkName;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceChanged;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDone;
@property (weak, nonatomic) IBOutlet UIImageView *imgFlag;
@property (weak, nonatomic) IBOutlet UIButton *tickerButton;

@property (weak,nonatomic) NSString *stockCode;

@property (weak, nonatomic) UserPrefConstants *userPref;

- (IBAction)clickedTicker:(id)sender;
@end
