//
//  ContainerSettingsController.m
//  TCiPad
//
//  Created by n2n on 02/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "ContainerSettingsController.h"
#import "AppInfoViewController.h"
#import "ATPAuthenticate.h"

@interface ContainerSettingsController ()

@property (nonatomic, retain) AppInfoViewController *appInfoVC;
@property (nonatomic, retain) NSString *currentSegue;

@end

@implementation ContainerSettingsController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    self.appInfoVC = segue.destinationViewController;
    if(self.childViewControllers.count >0)
    {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.appInfoVC];
    }
    else
    {
        [self addChildViewController:segue.destinationViewController];
        UIView* destView = ((UIViewController *)segue.destinationViewController).view;
        destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:destView];
        [segue.destinationViewController didMoveToParentViewController:self];
    }
    
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    //NSLog(@"%s", __PRETTY_FUNCTION__);
    //    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //    CGFloat screenWidth = screenRect.size.width;
    //    CGFloat screenHeight = screenRect.size.height;
    // toViewController.view.frame = toViewController==indicesVC?CGRectMake(0, 0, self.view.frame.size.width,screenHeight-62 ): CGRectMake(0, 0, self.view.frame.size.width, screenHeight-112);
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:1.0 options:UIViewAnimationOptionTransitionNone animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        
    }];
}


- (void)switchToViewController : (NSString*)segueString
{
     [[ATPAuthenticate singleton] timeoutActivity];
    
    ////NSLog(@"switchToViewController %@",segueString);
    [self performSegueWithIdentifier:segueString sender:nil];
    _currentSegue = segueString;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _currentSegue = @"segueUnitLot";
    
    [self performSegueWithIdentifier:@"segueUnitLot" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
