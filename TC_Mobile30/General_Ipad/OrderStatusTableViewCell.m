//
//  OrderStatusTableViewCell.m
//  TCiPad
//
//  Created by Sri Ram on 12/8/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import "OrderStatusTableViewCell.h"
#import "UserPrefConstants.h"

@implementation OrderStatusTableViewCell



- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




-(void)populateHistoryWithData:(TradeStatus*)ts {
    
    
    NSNumberFormatter *priceFormattercell = [NSNumberFormatter new];
    [priceFormattercell setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [priceFormattercell setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormattercell setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    
    _lblStkSymbol.text =ts.stock_name;
    
   // NSLog(@"s.stock_name %@",ts.stock_name);
    
    _lblAction.text = [LanguageManager stringForKey:ts.action];
    if ([ts.action isEqual:@"Buy"]) {
        _lblAction.textColor = [UIColor greenColor];
    }
    else if ([ts.action isEqual:@"Sell"]) {
        _lblAction.textColor = [UIColor redColor];
    }
    
    // CQA said ordPrice should always green. so:
    //if (ts.order_price.floatValue>0) {
    _lblOrdPrice.textColor = [UIColor greenColor];
    //}
    
    NSString *order_year =  [ts.last_update substringWithRange:NSMakeRange(0, 4)];
    NSString *order_mon =  [ts.last_update substringWithRange:NSMakeRange(4, 2)];
    NSString *order_day =  [ts.last_update substringWithRange:NSMakeRange(6, 2)];
    
    NSString *order_hour = [ts.last_update substringWithRange:NSMakeRange(8, 2)];
    NSString *order_minute = [ts.last_update substringWithRange:NSMakeRange(10, 2)];
    NSString *order_second = [ts.last_update substringWithRange:NSMakeRange(12, 2)];
    
    NSString *ordDate = [NSString stringWithFormat:@"%@/%@/%@", order_day, order_mon, order_year];
    NSString *ordTime = [NSString stringWithFormat:@"%@:%@:%@", order_hour, order_minute, order_second];
    
    _lblOrdTime.text = [NSString stringWithFormat:@"%@\n%@",ordDate,ordTime];
    
    _lblStatus.text = [LanguageManager stringForKey:ts.status_text];
    [_lblStatus setNumberOfLines:1];
    
    _lblbOrdQty.text=[quantityFormatter stringFromNumber:[NSNumber numberWithLongLong: ts.order_quantity.longLongValue]];
    _lblOrdPrice.text = [priceFormattercell stringFromNumber:[NSNumber numberWithFloat: ts.order_price.floatValue]];
    
    // special rule. zzzz
    if ([ts.order_type isEqualToString:@"Market"]||[ts.order_type isEqualToString:@"MarketToLimit"]) {
        if (ts.order_price.floatValue<=0) {
            _lblOrdPrice.text = @"-";
        }
    }
    
    
    [[LanguageManager defaultManager] translateStringsForView:self.contentView];
    
    
}


@end
