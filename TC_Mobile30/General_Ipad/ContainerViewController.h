//
//  ContainerViewController.h
//  TCUniversal
//
//  Created by Scott Thoo on 11/5/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerViewController : UIViewController

@property (nonatomic, strong) NSString *currentSegue;
- (void)switchToViewController : (NSString*)segueString;

@end
