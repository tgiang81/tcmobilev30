//
//  AccountTableViewCell.m
//  TCiPad
//
//  Created by n2n on 12/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "AccountTableViewCell.h"

@implementation AccountTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



-(void)populateItemsAtIndexPath:(NSIndexPath*)indexPath withArray:(NSMutableArray*)array {
    
    UserAccountClientData *uacd = [array objectAtIndex:indexPath.row];
    self.userName.text = uacd.client_name;
    self.accountNumber.text = [NSString stringWithFormat:@"%@-%@",uacd.client_account_number, uacd.broker_branch_code];
    
    [self.selectedView.layer setBorderColor:[UIColor cyanColor].CGColor];
    [self.selectedView.layer setBorderWidth:1];
    [self.selectedView.layer setCornerRadius:self.selectedView.frame.size.width/2.0];
    [self.selectedView.layer setMasksToBounds:NO];
    
    
    UserAccountClientData *selectedAcc = [UserPrefConstants singleton].userSelectedAccount;
    if ([uacd.client_account_number isEqualToString:selectedAcc.client_account_number]) {
         [self.selectedView.layer setBackgroundColor:[UIColor cyanColor].CGColor];
    } else{
        [self.selectedView.layer setBackgroundColor:[UIColor clearColor].CGColor];
    }
}

@end
