//
//  NumberHandler.h
//  N2NTrader
//
//  Created by Adrian Lo on 3/20/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NumberHandler : NSObject {
    
}
+ (NSString *) numberFormatCurrencyRate:(double)input;
+ (NSString *) numberFormatIndicesPrice:(double)input forExchange:(NSString *)exchg;
+ (NSString *) numberFormatStockPrice:(double)input isPrice:(BOOL)isPrice isPriceChange:(BOOL)isPriceChange forExchange:(NSString *)exchg;
+ (NSString *) numberFormatStockPriceForOrderSubmission:(double)input forExchange:(NSString *)exchg;
+ (NSString *) shortenNumberWithLong:(long)input;
+ (NSString *) shortenNumberWithLongLong:(long long)input;
+ (NSString *) shortenNumberWithDouble:(double)input;

+ (NSString *) numberFormat:(double) input;
+ (NSString *) numberFormat2D:(double) input;
+ (NSString *) numberFormat0D:(long long) input;
+ (NSString *) numberFormat0DWithDouble:(double) input;
+ (NSString *)formatDecimalNumber:(double)dInput longInput:(long)lInput llInput:(long long)llInput withDecimalPlaces:(BOOL)withDP noOfDecimalPlaces:(int)noOfDP isDoubleInput:(BOOL)isDouble isLLInput:(BOOL)isLongLong withThousandsSeperator:(NSString *)tseparator shortenValue:(BOOL)shortenValue orgHasDP:(BOOL)orgHasDP;
+ (BOOL)isEmpty:(id)thing;
@end
