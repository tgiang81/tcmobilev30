//
//  HomeViewController.m
//  TCUniversal
//
//  Created by Scott Thoo on 10/16/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "HomeViewController.h"

#import "NewsModalViewController.h"
#import "TransitionDelegate.h"
#import "AsyncImageView.h"
#import "MarqueeLabel.h"
#import "LanguageKey.h"
@interface StockCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet MarqueeLabel *movingLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblPercentage;
@property (weak, nonatomic) IBOutlet UILabel *lblVolume;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDone;
@property (weak, nonatomic) IBOutlet UILabel *lblLastChange;
@property (weak, nonatomic) IBOutlet UIImageView *imgFlag;

@property (weak,nonatomic) NSString *stockCode;


- (void)initWithStockDetails:(NSString *)stkCode
                     stkName:(NSString *)stkName
               stkPercentage:(NSString *)stkPercentage
                   stkVolume:(NSString *)stkVolume
                 stkLastDone:(NSString *)stkLastDone
               stkLastChange:(NSString *)stkLastChange;

- (IBAction)btnPressed:(id)sender;

@property (weak, nonatomic) UserPrefConstants *userPref;

@end

@implementation StockCell


-(void) initWithStockDetails:(NSString *)stkCode
                     stkName:(NSString *)stkName
               stkPercentage:(NSString *)stkPercentage
                   stkVolume:(NSString *)stkVolume
                 stkLastDone:(NSString *)stkLastDone
               stkLastChange:(NSString *)stkLastChange
{
    
    UIFont *myFont = [UIFont boldSystemFontOfSize:16];
    UIFont *myFontBig = [UIFont boldSystemFontOfSize:20];
    
    UIFont *myFont2 = [UIFont systemFontOfSize:13];
    UIFont *myFontBig2 = [UIFont systemFontOfSize:17];
    
    UIFont *myFont2Bold = [UIFont boldSystemFontOfSize:13];
    UIFont *myFontBig2Bold = [UIFont boldSystemFontOfSize:17];
    
    _lblVolume.text      = stkVolume;
    _lblPercentage.text  = [NSString stringWithFormat:@"%.3f%%",[stkPercentage floatValue]];
    _lblLastDone.text    = stkLastDone;
    _lblLastChange.text  = stkLastChange;
    
    CGFloat lastChangeFloat = [stkLastChange floatValue];
    
    _movingLabel.scrollDuration = 2;
    
    _movingLabel.text=stkName;
    
    if ([UserPrefConstants singleton].isBigIpad) {
        [_movingLabel setFont:myFontBig];
        [_lblVolume setFont:myFontBig2];
        [_lblPercentage setFont:myFontBig2];
        [_lblLastDone setFont:myFontBig2Bold];
        [_lblLastChange setFont:myFontBig2];
    } else {
        [_movingLabel setFont:myFont];
        [_lblVolume setFont:myFont2];
        [_lblPercentage setFont:myFont2];
        [_lblLastDone setFont:myFont2Bold];
        [_lblLastChange setFont:myFont2];
    }
    
    
    self.view.layer.cornerRadius = kButtonRadius*self.frame.size.height;
    self.view.layer.masksToBounds = NO; // dont change to YES. It affects ticker performance
    self.view.backgroundColor = [UIColor clearColor];
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    ////NSLog(@"firstCharStockPercentage %@",firstCharStockPercentage);
    if(lastChangeFloat < 0)
    {
        ////NSLog(@"firstCharStockPercentage : %@",firstCharStockPercentage);
        self.view.layer.backgroundColor = kRedColorF.CGColor;//[self colorWithHexString:@"570307"]; //Dark Red
        self.imgFlag.image = [UIImage imageNamed:@"downFlag.png"];
        //self.lblVolume.textColor=[UIColor redColor];
        _movingLabel.textColor=[UIColor colorWithRed:0.86 green:0.46 blue:0.46 alpha:1.00];
    }
    else if(lastChangeFloat>0)
    {
        self.view.layer.backgroundColor = kGreenColorF.CGColor;//[self colorWithHexString:@"112A11"]; //Dark Green
        self.imgFlag.image = [UIImage imageNamed:@"upFlag.png"];
        //self.lblVolume.textColor=[UIColor greenColor];
        _lblLastChange.text = [NSString stringWithFormat:@"+%@",_lblLastChange.text];
        _lblPercentage.text = [NSString stringWithFormat:@"+%@",_lblPercentage.text];
        _movingLabel.textColor=[UIColor colorWithRed:0.36 green:0.54 blue:0.30 alpha:1.00];
    }
    else
    {
        self.view.layer.backgroundColor = kGrayColorF.CGColor;//[self colorWithHexString:@"363535"]; //Dark Gray
        self.imgFlag.image = nil; //[UIImage imageNamed:@"unChangeFlag.png"];
        // self.lblVolume.textColor=[UIColor grayColor];
        _movingLabel.textColor=[UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1.00];;
    }
    
    //Adding dummy animation
    //int randNum = arc4random() % (10 - 3) + 3; // rand num btw 10 and 3;
    //[NSTimer scheduledTimerWithTimeInterval:randNum target:self selector:@selector(dummyFlipAnimation) userInfo:nil repeats:YES];
    [self dummyFlipAnimation];
    
    //assigning stockcode to local
    self.stockCode = stkCode;
    
    //assigning selected stockCode to UserPref
    _userPref = [UserPrefConstants singleton];
    
    if ([stkName length]<=0)  {
        _movingLabel.text= stkCode;
        _lblVolume.text = @"NO DATA";
        _lblPercentage.text  = @"";
        _lblLastDone.text    = @"";
        _lblLastChange.text  = @"";
    }
}

- (void)dummyFlipAnimation
{
    
    //    int randNum = arc4random() % (10 - 5) + 5;
    //
    //    if (randNum >5)
    //    {
    //        self.view.backgroundColor = [self colorWithHexString:@"112A11"]; //Dark Green
    //        self.imgFlag.image = [UIImage imageNamed:@"upFlag.png"];
    //    }
    //    else
    //    {
    //        self.view.backgroundColor = [self colorWithHexString:@"570307"]; //Dark Red
    //        self.imgFlag.image = [UIImage imageNamed:@"downFlag.png"];
    //    }
    //__weak StockFlag *weakSelf = self;
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                         self.view.transform = CGAffineTransformMakeScale(1, 0);
                     }
                     completion:^ (BOOL finished){
                         if (finished) {
                             [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                              animations:^(void) {
                                                  self.view.transform = CGAffineTransformMakeScale(1, 1);
                                                  
                                              }
                                              completion:nil];
                         }
                     }];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (IBAction)btnPressed:(id)sender
{
    //    int rand = arc4random()%100;
    //    ////NSLog(@"StockId : %d",rand);
    
    if ([_lblVolume.text isEqualToString:@"NO DATA"]) return;
    
    if ([_stockCode length]<=0) return;
    
    _userPref.userSelectingStockCode = _stockCode;
    ////NSLog(@"_userPref.userSelectingStockCode : %@",_userPref.userSelectingStockCode);
    
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:_stockCode forKey:@"stkCode"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
    
    
}

@end



static NSString *identifier = @"Cell";
static NSString *identifier2 = @"Cell2";

@interface HomeViewController () <CPTPlotDelegate, CPTPlotDataSource,UIPageViewControllerDelegate, UIGestureRecognizerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
{
    BOOL isJarNews;
    QCData *_qcData;
    VertxConnectionManager *_vcm;
    NSString *currentSortCmd;
    
    NSArray *sortedArr;
    NSArray *leftStkCodeArr;
    NSArray *rightStkCodeArr;
    NSArray *wlistStkCodeArr;
    NSString *currentSelectedRankedLeft;
    NSString *currentSelectedRankedRight;
    
    __weak IBOutlet UIScrollView *scrollViewLatestNotice;
    __weak IBOutlet UIPageControl *pageControlLatestNotice;
    __weak IBOutlet UILabel *lblLatestNotice;
    __weak IBOutlet UIButton *btnHideNews;
    NSArray *newsDataSourceArray;
    NSMutableArray *newsDataSource;
    NSArray *archiveNewsTableDataArr;
    __weak IBOutlet UIActivityIndicatorView *loadingNews;
    
    IBOutlet UILabel *lblNotavaialblergt;
    IBOutlet UILabel *lblNotavailable;
    __weak IBOutlet UIScrollView *scrollViewTop;
    __weak IBOutlet UIPageControl *pageControlTop;
    
    
    __weak IBOutlet UICollectionView *collectionView_Left;
    __weak IBOutlet UICollectionView *collectionView_Right;
    
    //Market Summary
    __weak IBOutlet CPTGraphHostingView *mainHostingView;
    __weak IBOutlet CPTGraphHostingView *warrantHostingView;
    __weak IBOutlet CPTGraphHostingView *AceHostingView;
    NSMutableArray *dataForMainChart;
    NSMutableArray *dataForWarrantChart;
    NSMutableArray *dataForAceChart;
    CGRect newsFrame;
    //Self Calculate fields
    int vertxMarketQueryCount;
    
    int data2201;
    int data2203;
    int data2204;
    
    int totalUp;
    int totalDown;
    int totalUnchg;
    int totalUntrd;
    long long totalVol;
    int totalTotal;
    int totalValue;
    int newsOpened;
    int watchListID;
    
    NSMutableDictionary *mainMarketDict;
    NSMutableDictionary *warrantMarketDict;
    NSMutableDictionary *aceMarketDict;
    //Create a dictionary just to store the totals
    ATPAuthenticate *atp;
    NSMutableArray *rankedByNameArr;
    NSMutableArray *rankedByKeyArr;
    NSMutableArray *stockListArr;
    NSMutableArray *wlListArr;
    
    BOOL leftDisplayingFront;
    BOOL rightDisplayingFront;
    BOOL wListDisplayingFront;
    
    NSInteger scoreBoardUpdateCnt;
    
    BOOL newsOpen;
    CGPoint startLocation;
    
    BOOL callOnce;
    BOOL isFullScreenChart;
    BOOL pieChartsAdded;
    CGRect webViewOriginalFrame;
    
    CGPoint newsViewOriginalPos;
    CGFloat bufferNewsX;
    
    
}

@property (nonatomic, strong) TransitionDelegate *transitionController;
@property (nonatomic, strong) NSTimer *scoreBoardTimer;
@property (nonatomic, strong) CPTGraph *mainGraph, *warrantGraph, *aceGraph;
@property (nonatomic, strong) CPTPieChart *pieMainChart, *pieWarrantChart, *pieAceChart;
@property (nonatomic, strong) UIView *mainMarketStat, *warrantMarketStat, *aceMarketStat;


@end

@implementation HomeViewController

@synthesize viewBottomRight = _viewBottomRight;
@synthesize viewMain = _viewMain;
@synthesize viewNews = _viewNews;
@synthesize viewBottomLeft = _viewBottomLeft;
@synthesize tableViewNews;
@synthesize webview;
@synthesize loadingBtmLeft,loadingBtmRight;
@synthesize viewHolderLeft,viewBottomLeftSelection;
@synthesize viewHolderRight,viewBottomRightSelection;
@synthesize tableviewLeftSelection,tableviewRightSelection;
@synthesize lblLeftTitle,lblRightTitle;
@synthesize lblTotalDown,lblTotalScoreOfTrade,lblTotalUnChg,lblTotalUnTrd,lblTotalUp,lblTotalValue,lblTotalVolume,
webViewKLCI,totalMarketDict,MainMarketNtAvailLabel,AcemarketNtAvaiLabel,WarrantMarketNtAvailLabel;
@synthesize scoreBoardLabel;
@synthesize mainNewsIndicator;
@synthesize scoreBoardIndicator, watchListArr;


/*
 NEWS CATEGORY AS IN TCMOBILE
 
 newsCat = [[NSArray alloc] initWithObjects:@"All News", @"Special Announcement",
 @"Member Circular", @"Investor Alert Announcement", @"General Announcement/Listing Circular",
 @"Financial Summary", @"Entitlement", @"Changes in BoardRoom/Chief Executive Officer", @"Trading of Rights",
 @"Change in Substantial Shareholder's Interest(CI)", @"Change in Director's Interest",
 @"Notice of Person Interest to be Substantial Shareholder(NA)", @"Notice of Person Ceasing",
 @"Bernama Summary Economic News", @"Bernama General Economic News", @"Change of Company Secretary",
 @"Notice of Shares Buy Back-Form 28A(NS)", @"Change of Address(CA)", @"Change of Registrar(CR)",
 @"Notice of Shares Buy Back by a Company-Form 28B", @"Notice of Shares Buy Back Immediate Announcement",
 @"Notice of Resale/Cancel of Share", @"Changes in Audit Committee", nil];
 
 */





//- (void)showRemoteReleaseNotesView
//{
//    NSString *currentAppVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
//    [ReleaseNotesView setupViewWithAppIdentifier:@"972011951" releaseNotesTitle:[NSString stringWithFormat:@"What's new in version %@:", currentAppVersion] closeButtonTitle:@"Close" completionBlock:^(ReleaseNotesView *releaseNotesView, NSString *releaseNotesText, NSError *error){
//        if (error)
//        {
//            //NSLog(@"An error occurred: %@", [error localizedDescription]);
//        }
//        else
//        {
//            // Create and show release notes view
//            [releaseNotesView showInView:self.view];
//        }
//    }];
//}


#pragma mark - TapGesture delegates

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    if ([otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        [otherGestureRecognizer requireGestureRecognizerToFail:gestureRecognizer];
        
        // //NSLog(@"added failure requirement to: %@", otherGestureRecognizer);
    }
    
    return YES;
}


-(UIImage*)captureWebView
{
    [_viewKLCI setBackgroundColor:[UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.00]];
    UIGraphicsBeginImageContext(_viewKLCI.bounds.size);
    [_viewKLCI.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

-(void)doubleTappedChart:(UITapGestureRecognizer*)recognizer {
    
    // NSLog(@"double tapped");
    
    
    if (!isFullScreenChart) {
        
        // CGPoint ptInMain = [_viewKLCI.superview convertPoint:_viewKLCI.center toView:nil];
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        [_mainView addSubview:_viewKLCI];
        _viewKLCI.frame = scrollViewTop.frame;
        
        dummyView.frame = _viewKLCI.frame;
        dummyView.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:dummyView];
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            
            dummyView.frame = _mainView.frame;
            _viewKLCI.hidden = YES;
            _viewKLCI.frame = _mainView.frame;
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethod];
            
        } completion:^(BOOL finished) {
            
            _viewKLCI.hidden = NO;
            [dummyView removeFromSuperview];
            isFullScreenChart= YES;
            
        }];
        
        
    } else {
        
        UIImageView *dummyView = [[UIImageView alloc] initWithImage:[self captureWebView]];
        dummyView.frame = _viewKLCI.frame;
        dummyView.contentMode = UIViewContentModeScaleToFill;
        [self.view addSubview:dummyView];
        
        if (pieChartsAdded) {
            [scrollViewTop addSubview:_viewKLCI];
            _viewKLCI.frame = webViewOriginalFrame;
        } else {
            [_viewMarketActivities addSubview:_viewKLCI];
            _viewKLCI.frame = scrollViewTop.frame;
        }
        
        
        
        //  NSLog(@"%f,%f,%f,%f",_viewKLCI.frame.origin.x, _viewKLCI.frame.origin.y, _viewKLCI.frame.size.width,_viewKLCI.frame.size.height);
        
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void) {
            
            dummyView.frame = scrollViewTop.frame;
            if ([[UserPrefConstants singleton].chartTypeToLoad isEqualToString:@"Image"])[self loadChartMethod];
            
        } completion:^(BOOL finished) {
            [dummyView removeFromSuperview];
            
            isFullScreenChart= NO;
        }];
        
        
    }
}


- (void) loadBanner{
    
    [self downloadImageFromURL:@"https://assets.theedgemarkets.com/n2n_building_7.png"];
}

-(void) downloadImageFromURL :(NSString *)imageUrl{
    
    NSURL  *url = [NSURL URLWithString:imageUrl];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"dwnld_image.png"];
        [urlData writeToFile:filePath atomically:YES];
        UIImage *image1=[UIImage imageWithContentsOfFile:filePath];
        [_bannerButton1 setImage:image1 forState:UIControlStateNormal];
    }
    
}


#pragma mark - N2Tabs delegates

-(void)tabDidClicked:(UIButton *)tabButton  atTab:(N2Tabs*)tab {
    if ([tabButton.titleLabel.text isEqualToString:[LanguageManager stringForKey:@"Announcements"]]) {
        [self.announcementWebview setHidden:NO];
        [self.tableViewNews setHidden:YES];
    } else {
        [self.announcementWebview setHidden:YES];
        [self.tableViewNews setHidden:NO];
    }
}

#pragma mark - View Lifecycle delegates

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // NSLog(@"didLoad");
    
    UICollectionViewFlowLayout *collectionViewFlowLayout = [UICollectionViewFlowLayout new];
    
    [collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical] ;
    collectionViewFlowLayout.minimumInteritemSpacing = 5;
    collectionViewFlowLayout.minimumLineSpacing = 10;
    
    //Left
    collectionView_Left.backgroundColor = UIColor.clearColor;
    
    [collectionView_Left registerNib:[UINib nibWithNibName:@"StockCell" bundle:nil] forCellWithReuseIdentifier:  identifier ];
    
    collectionView_Left.delegate = self;
    collectionView_Left.dataSource = self;
    
    collectionView_Left.collectionViewLayout = collectionViewFlowLayout;
    //Right
    
    UICollectionViewFlowLayout *collectionViewFlowLayout2 = [UICollectionViewFlowLayout new];
    
    [collectionViewFlowLayout2 setScrollDirection:UICollectionViewScrollDirectionVertical] ;
    collectionViewFlowLayout2.minimumInteritemSpacing = 5;
    collectionViewFlowLayout2.minimumLineSpacing = 10;
    
    collectionView_Right.backgroundColor = UIColor.clearColor;
    
    [collectionView_Right registerNib:[UINib nibWithNibName:@"StockCell" bundle:nil] forCellWithReuseIdentifier:  identifier2 ];
    
    collectionView_Right.delegate = self;
    collectionView_Right.dataSource = self;
    collectionView_Right.collectionViewLayout = collectionViewFlowLayout2;
    
    
    
    
    
    newsDataSource = [[NSMutableArray alloc] init];
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
        
        
        
        [_bannerButton1 setImage:[UIImage imageNamed:@"AppLogoABACUS"] forState:UIControlStateNormal];
        
    }else{
        
        [_bannerButton1 setImage:[UIImage imageNamed:@"banner1"] forState:UIControlStateNormal];
    }
    
     [self loadBanner];
    
    _userPrefs = [UserPrefConstants singleton];
    
    data2201 = 0;
    data2203 = 0;
    data2204 = 0;
    
    atp = [ATPAuthenticate singleton];
    _vcm = [VertxConnectionManager singleton];
    _qcData = [QCData singleton];
    
    
    watchListArr = [[NSMutableArray alloc] init];
    
    
    
    // NSLog(@"userInfoDict %@",[[UserPrefConstants singleton] userInfoDict]);
    
    [[UserPrefConstants singleton] overrideChartSettings];
    
    _watchListErrorMsg.titleLabel.textAlignment = NSTextAlignmentCenter;
    _watchListErrorMsg.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    // Load views into UISCrollView (some broker dont want pie chart)
    
    [scrollViewTop addSubview:_viewKLCI];
    [scrollViewTop addSubview:_viewSummary];
    [scrollViewTop setHidden:YES];
    [pageControlTop setHidden:YES];
    
    //    if ([UserPrefConstants singleton].homePiechart) {
    //        [scrollViewTop addSubview:_viewKLCI];
    //        [scrollViewTop addSubview:_viewSummary];
    //    } else {
    //        _viewKLCI.frame = scrollViewTop.frame;
    //        [scrollViewTop setHidden:YES];
    //        [pageControlTop setHidden:YES];
    //    }
    
    // Create pie charts/graphs object once because we will keep on updating it
    _pieMainChart = [[CPTPieChart alloc] init];
    _pieMainChart.dataSource = self;
    _pieMainChart.delegate = self;
    _pieWarrantChart = [[CPTPieChart alloc] init];
    _pieWarrantChart.dataSource = self;
    _pieWarrantChart.delegate = self;
    _pieAceChart = [[CPTPieChart alloc] init];
    _pieAceChart.dataSource = self;
    _pieAceChart.delegate = self;
    
    
    _mainGraph = [[CPTXYGraph alloc] initWithFrame:mainHostingView.bounds];
    mainHostingView.hostedGraph = _mainGraph;
    _warrantGraph = [[CPTXYGraph alloc] initWithFrame:warrantHostingView.bounds];
    warrantHostingView.hostedGraph = _warrantGraph;
    _aceGraph = [[CPTXYGraph alloc] initWithFrame:AceHostingView.bounds];
    AceHostingView.hostedGraph = _aceGraph;
    
    
    
    _mainMarketStat = [[UIView alloc]init];
    [_mainMarketStat setHidden:YES];
    [_mainMarketStat setFrame:CGRectMake(mainHostingView.frame.origin.x, mainHostingView.frame.origin.y+mainHostingView.frame.size.height+8, mainHostingView.frame.size.width, 50)];
    [scrollViewTop addSubview: _mainMarketStat];
    [self addStatComponentsTo:_mainMarketStat];
    
    
    _warrantMarketStat = [[UIView alloc]init];
    [_warrantMarketStat setHidden:YES];
    [_warrantMarketStat setFrame:CGRectMake(warrantHostingView.frame.origin.x, warrantHostingView.frame.origin.y+warrantHostingView.frame.size.height+8, warrantHostingView.frame.size.width, 50)];
    [scrollViewTop addSubview: _warrantMarketStat];
    [self addStatComponentsTo:_warrantMarketStat];
    
    _aceMarketStat = [[UIView alloc]init];
    [_aceMarketStat setHidden:YES];
    [_aceMarketStat setFrame:CGRectMake(AceHostingView.frame.origin.x, AceHostingView.frame.origin.y+AceHostingView.frame.size.height+8, AceHostingView.frame.size.width, 50)];
    [scrollViewTop addSubview: _aceMarketStat];
    [self addStatComponentsTo:_aceMarketStat];
    
    callOnce = NO;
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTappedChart:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    doubleTapGesture.delegate = self;
    isFullScreenChart = NO;
    [_viewKLCI addGestureRecognizer:doubleTapGesture];
    [webViewKLCI setBackgroundColor:[UIColor clearColor]];
    [webViewKLCI setOpaque:NO];
    vertxMarketQueryCount =0;
    
    [UserPrefConstants singleton].quoteScreenFavID=nil;
    _legendView.hidden = YES;
    newsOpened = 1;
    startLocation = CGPointZero;
    
    //Market Summary
    mainMarketDict          = [NSMutableDictionary dictionary];
    warrantMarketDict       = [NSMutableDictionary dictionary];
    aceMarketDict           = [NSMutableDictionary dictionary];
    totalMarketDict         = [NSMutableDictionary dictionary];
    
    dataForMainChart      = [NSMutableArray array];
    dataForWarrantChart   = [NSMutableArray array];
    dataForAceChart       = [NSMutableArray array];
    
    //News
    tableViewNews.dataSource = self;
    tableViewNews.delegate = self;
    [loadingNews startAnimating];
    [loadingBtmRight startAnimating];
    [loadingBtmLeft startAnimating];
    [_loadingNewsLabel setText:[LanguageManager stringForKey:Loading______]];
    [_loadingNewsLabel setHidden:NO];
    
    if ([UserPrefConstants singleton].isElasticNews) {
        [atp getElasticNewsForStocks:@"Home" forDays:30 withKeyword:@"" andTargetID:@"" andSource:@"" andCategory:@""];
    }else
        
        if ([UserPrefConstants singleton].isArchiveNews) {
            // NSLog(@"Archive");
            [atp getArchiveNewsForStock:@"Home" forDays:30];
        } else {
            isJarNews = YES;
            [atp getJarNews];
        }
    
    
    
    //Top Scroller
    rankedByNameArr = [[NSMutableArray alloc] init];
    rankedByKeyArr = [[NSMutableArray alloc] init];
    stockListArr = [[NSMutableArray alloc] init];
    wlListArr = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[[AppConstants SortByListArray] count]; i++) {
        NSDictionary *eachDict = (NSDictionary*)[[AppConstants SortByListArray] objectAtIndex:i];
        [rankedByNameArr addObject:[[eachDict allValues] objectAtIndex:0]];
        
    }
    
    for (int i=0; i<[[AppConstants SortByListArray] count]; i++) {
        NSDictionary *eachDict = (NSDictionary*)[[AppConstants SortByListArray] objectAtIndex:i];
        [rankedByKeyArr addObject:[[eachDict allKeys] objectAtIndex:0]];
    }
    
    NSString *name = [LanguageManager stringForKey:[rankedByNameArr objectAtIndex:0]];
    lblLeftTitle.text = [LanguageManager stringForKey:@"Sorted By %@" withPlaceholders:@{@"%sortName%":name}];
    
    
    tableviewRightSelection.delegate    = self;
    tableviewRightSelection.dataSource  = self;
    tableviewLeftSelection.delegate     = self;
    tableviewLeftSelection.dataSource   = self;
    leftDisplayingFront  = YES;
    rightDisplayingFront = YES;
    wListDisplayingFront = YES;
    
    //Default Get SortTop Left and Right
    [_vcm vertxSortByLeft:@"101" Direction:@"DESC" exchg:[UserPrefConstants singleton].userCurrentExchange];
    [_vcm vertxSortByRight:@"changePer" Direction:@"DESC" exchg:[UserPrefConstants singleton].userCurrentExchange];
    
    //KLCIs
    [_vcm vertxGetKLCI]; // need this here again (first one in MainFrame)
    [_legendView.layer setCornerRadius:5.0f];
    
    //  Get indices
    //  [_vcm vertxGetIndices];
    lblNotavaialblergt.hidden=YES;
    lblNotavailable.hidden=YES;
    [scrollViewTop setPagingEnabled:YES];
    
    [self setChartStatus:4];
    [self loadChartMethod];
    
    //UIPanGestureRecognizer *swipe = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    // swipe.direction = UISwipeGestureRecognizerDirectionRight;
    // view is your UIViewController's view that contains your UIWebView as a subview
    //[self.view addGestureRecognizer:swipe];
    //swipe.delegate = self;
    
    currentSelectedRankedLeft = [[NSString alloc] initWithFormat:@"Volume"];
    currentSelectedRankedRight = [[NSString alloc] initWithFormat:@"Gainers%%"];
    
    
    // add 3 finger gesture to dismiss stk preview
    UISwipeGestureRecognizer *threeFingerSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(animateNewsClose:)];
    threeFingerSwipe.delegate = self;
    threeFingerSwipe.numberOfTouchesRequired = 3;
    [_viewNews addGestureRecognizer:threeFingerSwipe];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    //    MainMarketNtAvailLabel = nil;
    //    AcemarketNtAvaiLabel = nil;
    //    WarrantMarketNtAvailLabel = nil;
    //   lblLeftTitle = nil;
    //    lblRightTitle  = nil;
    //    lblTotalVolume  = nil;
    //    lblTotalValue  = nil;
    //   lblTotalScoreOfTrade  = nil;
    //    lblTotalUp  = nil;
    //    lblTotalDown  = nil;
    //    lblTotalUnTrd = nil;
    //    lblTotalUnChg = nil;
    //    lblNotavaialblergt = nil;
    //    lblNotavailable = nil;
    //    for (CALayer* layer in [self.view.layer sublayers])
    //    {
    //        [layer removeAllAnimations];
    //    }
    
    
}

- (void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    
    
    // //NSLog(@"Score board - %@",[UserPrefConstants singleton].userCurrentExchange);
    
    
}



-(void)viewDidLayoutSubviews
{
    if (!callOnce) {
        webViewOriginalFrame = _viewKLCI.frame;
        callOnce = YES;
        
        //The Latest Notice
        scrollViewLatestNotice.delegate  = self;
        scrollViewLatestNotice.contentSize = CGSizeMake(scrollViewLatestNotice.frame.size.width, 120);
        pageControlLatestNotice.numberOfPages = 1;
        pageControlLatestNotice.currentPage = 0;
        
        scrollViewTop.delegate = self;
        scrollViewTop.contentSize = CGSizeMake(scrollViewTop.frame.size.width*2, 287);
        pageControlTop.numberOfPages = 2;
        pageControlTop.currentPage = 0;
        
        NSArray *menuArr;
        
        if ([[UserPrefConstants singleton].AnnouncementURL length]<=0) {
            menuArr = @[[LanguageManager stringForKey:@"News"]];
        } else {
            menuArr = @[[LanguageManager stringForKey:@"News"],[LanguageManager stringForKey:@"Announcements"]];
            NSURL *url = [NSURL URLWithString:[[UserPrefConstants singleton].AnnouncementURL stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            //NSLog(@"ANN %@",url.absoluteString);
            [self.announcementWebview loadRequest:request];
        }
        
        [self.announcementWebview setHidden:YES];
        
        NSArray *params = [NSArray arrayWithObjects:menuArr,
                           [UIColor cyanColor],
                           kPanelColor, nil];
        N2Tabs *tabs = [[N2Tabs alloc] initWithParams:params withFrame:_tabsPlaceholder.frame andFontSize:15.0f];
        tabs.tabDelegate = self;
        [self.viewNews addSubview:tabs];
    }
}



- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    isJarNews = YES;
    //Add Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedGetAllNews:) name:@"didFinishedGetAllStockNews" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveExchangeList) name:@"didReceiveExchangeList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetArchiveNews:) name:@"didFinishGetArchiveNews" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishGetElasticNews:) name:@"didFinishGetElasticNews" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveJarNews:) name:@"didReceiveJarNews" object:nil];
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedSortTop:) name:@"didFinishedSortTop" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneVertxGetScoreBoardWithExchange:) name:@"doneVertxGetScoreBoardWithExchange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedSortTopLeft:) name:@"didFinishedSortTopLeft" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdate:) name:@"didUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedSortTopRight:) name:@"didFinishedSortTopRight" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedDoneGetKLCI:) name:@"DoneGetKLCI" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedDoneGetIndices:) name:@"DoneGetIndices" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestWatchListReceived) name:@"getWatchlistSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceivedWatchListItems:) name:@"didReceivedWatchListItems" object:nil];
    
    
    [scoreBoardLabel setText:[LanguageManager stringForKey:@"Scoreboard - %@" withPlaceholders:@{@"%exchg%":[UserPrefConstants singleton].userCurrentExchange}]];
    
    scrollViewTop.userInteractionEnabled = YES;
    
    scoreBoardUpdateCnt = 0;
    [self populateScoreBoard];
    
    
    // reload watchlist
    [self latestWatchListReceived];
    
}


-(void)populateScoreBoard {
    
    //    if([[UserPrefConstants singleton].userCurrentExchange isEqual:@"KL"])
    //    {
    scoreBoardUpdateCnt=1;
    
    
    [_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"];
    [_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"];
    [_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
    MainMarketNtAvailLabel.hidden=AcemarketNtAvaiLabel.hidden=WarrantMarketNtAvailLabel.hidden =YES;
    
    // update timer
    if (_scoreBoardTimer!=nil) {
        [_scoreBoardTimer invalidate];
        _scoreBoardTimer = nil;
    }
    _scoreBoardTimer = [NSTimer scheduledTimerWithTimeInterval:15.0 target:self selector:@selector(updateScoreBoard) userInfo:nil
                                                       repeats:YES];
    
    
    //    } else {
    //        MainMarketNtAvailLabel.hidden=AcemarketNtAvaiLabel.hidden=WarrantMarketNtAvailLabel.hidden =NO;
    //        [scoreBoardIndicator stopAnimating];
    //        scrollViewTop.userInteractionEnabled = NO;
    //    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (_scoreBoardTimer!=nil) {
        [_scoreBoardTimer invalidate];
        _scoreBoardTimer = nil;
    }
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"didFinishedGetAllStockNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didReceiveExchangeList" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishGetArchiveNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishGetElasticNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didReceiveJarNews" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedSortTop" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doneVertxGetScoreBoardWithExchange" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedSortTopLeft" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedSortTopRight" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DoneGetKLCI" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DoneGetIndices" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"getWatchlistSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didReceivedWatchListItems" object:nil];
}

-(void)updateScoreBoard {
    scoreBoardUpdateCnt++;
    if (scoreBoardUpdateCnt>=100) scoreBoardUpdateCnt = 10;
    
    [_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2201"];
    [_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2203"];
    [_vcm vertxGetScoreBoardWithExchange:[UserPrefConstants singleton].userCurrentExchange marketId:@"2204"];
    MainMarketNtAvailLabel.hidden=AcemarketNtAvaiLabel.hidden=WarrantMarketNtAvailLabel.hidden =YES;
}

-(void)addStatComponentsTo:(UIView*)statView {
    
    UIView *buyView  = [[UIView alloc] init]; buyView.tag = 10;
    UIView *sellView  = [[UIView alloc] init]; sellView.tag = 11;
    UIView *untradedView  = [[UIView alloc] init]; untradedView.tag = 12;
    UIView *equalView  = [[UIView alloc] init]; equalView.tag = 13;
    [statView addSubview:buyView]; [statView addSubview:sellView];
    [statView addSubview:untradedView]; [statView addSubview:equalView];
    
    UILabel *lblUp      = [[UILabel alloc]init];    lblUp.tag = 20;
    UILabel *lblDown    = [[UILabel alloc]init];    lblDown.tag = 21;
    UILabel *lblUntrade = [[UILabel alloc]init];    lblUntrade.tag = 22;
    UILabel *lblEqual   = [[UILabel alloc]init];    lblEqual.tag = 23;
    UILabel *lblVol     = [[UILabel alloc]init];    lblVol.tag = 24;
    [statView addSubview:lblUp];[statView addSubview:lblDown];[statView addSubview:lblUntrade];
    [statView addSubview:lblEqual];[statView addSubview:lblVol];
}

#pragma mark - Watchlist in bottom right (Jan 2017)

- (void)didReceivedWatchListItems:(NSNotification *)notification // From vertx reply
{
    // load watchlist stocks
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [_watchStockLoading stopAnimating];
        wlistStkCodeArr = [[NSArray alloc]initWithArray:[UserPrefConstants singleton].userWatchListStockCodeArr copyItems:YES];
        
        if ([wlistStkCodeArr count]<=0) {
            [_watchListErrorMsg setHidden:NO];
            [_watchListErrorMsg setTitle:@"No Stocks. Choose another \nWatchlist or Tap Here\n to Manage WatchList." forState:UIControlStateNormal];
            return;
        }
        
        
        
        [_watchListErrorMsg setHidden:YES];
        
        // load into dictionary, sort and be done
        NSMutableDictionary *dictToSort = [[NSMutableDictionary alloc] init];
        for (NSString *stkCode in wlistStkCodeArr)
        {
            if (([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
                
                NSDictionary *dictToAdd = [[NSDictionary alloc] initWithDictionary:
                                           [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];
                
                [dictToSort setObject:dictToAdd forKey:stkCode];
                
            }
            
        }
        // sort it by stock name (previously sorted by volume)
        NSString *sortBy = FID_38_S_STOCK_NAME;
        
        sortedArr = [dictToSort keysSortedByValueUsingComparator:
                     ^NSComparisonResult(NSDictionary* obj1, NSDictionary* obj2)
                     {
                         NSComparisonResult result = NSOrderedSame;
                         if(obj1[sortBy] != nil && obj2[sortBy]!=nil)
                         {
                             return [obj1[sortBy] compare:obj2[sortBy]];
                         }
                         else
                             return result;
                     }];
        
        // clamp to 10 stocks
        //        if ([sortedArr count]>10) {
        //            NSArray *tenStocks = [sortedArr subarrayWithRange:NSMakeRange(0, 10)];
        //            sortedArr = [[NSArray alloc]initWithArray:tenStocks];
        //        }
        
        [self generateStockFlagToView:FALSE withStockStrArr:sortedArr];
        
    });
}




-(void)latestWatchListReceived {
    
    watchListArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
    
    // if watchlist exist, select the first one
    if ([watchListArr count]>0) {
        
        [_watchListErrorMsg setHidden:YES];
        // get stocks of first watchlist
        
        NSMutableArray *exceptionArray = [[NSMutableArray alloc] init];
        [exceptionArray addObjectsFromArray:[UserPrefConstants singleton].tickerStockCodes];
        [exceptionArray addObjectsFromArray:leftStkCodeArr];
        if (_userPrefs.userSelectingStockCode!=nil) [exceptionArray addObject:_userPrefs.userSelectingStockCode];
        [[QCData singleton] removeAllQcFeedExceptThese:exceptionArray];
        
        NSDictionary *firstWL = [watchListArr objectAtIndex:0];
        int favID = [[firstWL objectForKey:@"FavID"] intValue];
        [UserPrefConstants singleton].prevWatchlistFavID = favID;
        [UserPrefConstants singleton].watchListName = [firstWL objectForKey:@"Name"];
        
        _watchListTitle.text = [LanguageManager stringForKey:@"WatchList: %@" withPlaceholders:@{@"%WLName%":[UserPrefConstants singleton].watchListName}];
        [atp getWatchListItems:favID];
        [UserPrefConstants singleton].quoteScreenFavID = [NSNumber numberWithInt:favID];
        
    } else {
        [_watchStockLoading stopAnimating];
        _watchListTitle.text = [LanguageManager stringForKey:@"No WatchList"];
        [_watchListErrorMsg setHidden:NO];
        [_watchListErrorMsg setTitle:[LanguageManager stringForKey:@"WatchList Is Not Setup.\nTap Here to Manage WatchList."] forState:UIControlStateNormal];
    }
    
    [_watchListTable reloadData];
    
}


- (IBAction)clickBanner:(id)sender{
    
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
        vc.view.backgroundColor = [UIColor clearColor];
        vc.newsTitleLabel.text = @"Abacus Securities Corporation";
        
        vc.currentEd = nil;
        
        NSString *url = [NSString stringWithFormat:@"%@",@"https://www.mytrade.com.ph"];
        
        [vc openUrl:url withJavascript:NO];
        [vc setTransitioningDelegate:_transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
        vc.view.backgroundColor = [UIColor clearColor];
        vc.newsTitleLabel.text = @"N2NConnect Berhad";//@"CIMB Securities (Singapore) Pte Ltd";
        
        vc.currentEd = nil;
        
        NSString *url = [NSString stringWithFormat:@"%@",@"http://www.n2nconnect.com"];
        
        [vc openUrl:url withJavascript:NO];
        [vc setTransitioningDelegate:_transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
    }
    
    
}



- (void)didFinishedGetAllNews:(NSNotification *)notification
{
    
    
    NSDictionary * response = [notification.userInfo copy];
    // NSLog(@"NEWS NORMAL: %@",response);
    
    //  newsDataSource = [response objectForKey:@"data"];
    NSArray *unSortedArray = [response objectForKey:@"data"];
    
    if (unSortedArray.count>0) {
        
        /*for (NSDictionary *dict in [response objectForKey:@"data"]) {
         [unSortedArray addObject:dict];
         }*/
        
        NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"4" ascending:NO];
        NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
        newsDataSourceArray = [unSortedArray sortedArrayUsingDescriptors:descriptors];
        
        for (NSDictionary *dict in newsDataSourceArray) {
            if (![[dict objectForKey:@"1"] isEqualToString:@"FMETF."]) {
                [newsDataSource addObject:dict];
            }
            
        }
        
        //NSLog(@"NEWS NORMAL: %@",newsDataSource);
        
        // NSLog(@"NEWS NORMAL: %@",newsDataSource);
        dispatch_async(dispatch_get_main_queue(), ^{
            [tableViewNews reloadData];
            [_loadingNewsLabel setHidden:YES];
        });
    }else{
        [_loadingNewsLabel setText:[LanguageManager stringForKey:@"No Available News Data"]];
        [_loadingNewsLabel setHidden:NO];
    }
    
    
}


#pragma mark - Charts

-(void)setChartStatus:(long)status {
    
    
    NSString *errormsg = [LanguageManager stringForKey:@"Chart Not Available"];
    //success
    if (status==1) {
        [_chartErrorMsg setHidden:YES];
        [_chartLoadActivity setHidden:YES];
        [_imgChart setHidden:NO];
    }
    
    //no url
    if (status==2) {
        [_chartErrorMsg setHidden:NO];
        [_chartLoadActivity setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // error
    if (status==3) {
        [_chartErrorMsg setHidden:NO];
        [_chartLoadActivity setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // loading
    if (status==4) {
        [_chartErrorMsg setHidden:YES];
        [_chartLoadActivity setHidden:NO];
        [_imgChart setHidden:YES];
    }
    
    if (status==5) {
        [_chartErrorMsg setHidden:YES];
        [_chartLoadActivity setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    // error
    if (status==6) {
        errormsg = [LanguageManager stringForKey:@"Load Chart Fail. Try again later."];
        [_chartErrorMsg setHidden:NO];
        [_chartLoadActivity setHidden:YES];
        [_imgChart setHidden:YES];
    }
    
    _chartErrorMsg.text = errormsg;
    
    if (DEBUG_MODE==1) {
        [_imgChart setHidden:YES];
    }
}



-(void)loadChartMethod {
    
    NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex]
                              valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
    
    NSString *chartTypeToLoad = [UserPrefConstants singleton].chartTypeToLoad;
    
    
    // NSLog(@"Split Stk Code %@ - %@",splitStkCode, chartTypeToLoad);
    
    if ([splitStkCode count]<2) {
        //[self setChartStatus:2];
        //  return;
    }
    
    // ========================================
    //                MODULUS
    // ========================================
    
    
    
    if ([chartTypeToLoad isEqualToString:@"Modulus"]) {
        
        [self setChartStatus:5];
        
        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
        // eg https://poc.asiaebroker.com/mchart/index.jsp?code=020000000&exchg=KL&mode=d&color=b&view=m
        // eg https://news-ph.asiaebroker.com/mchart/index.jsp?code=MRC&exchg=PH&mode=h&amount=1&color=w&view=m
        
        NSString *url = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m&isstock=N",
                         [UserPrefConstants singleton].interactiveChartURL,
                         [splitStkCode objectAtIndex:0],
                         [_userPrefs stripDelayedSymbol:_userPrefs.userCurrentExchange]];
        NSLog(@"Home chart %@",url);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        webViewKLCI.scrollView.scrollEnabled = NO;
        webViewKLCI.scrollView.bounces = NO;
        [webViewKLCI loadRequest:request];
        
        [_imgChart setHidden:YES];
        [webViewKLCI setHidden:NO];
    }
    
    
    //NSString *urlString = [NSString stringWithFormat:@"http://chart.asiaebroker.com/chart/intraday?w=300&h=200&k=%@",swdc.stockCodeForDetail];
    // ========================================
    //                IMAGE
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"Image"]) {
        
        [self setChartStatus:4];
        [_viewKLCI bringSubviewToFront:_imgChart];
        
        NSString *urlString = [NSString stringWithFormat:@"%@t=1&w=%.0f&h=%.0f&k=%@.%@&c=b",
                               [UserPrefConstants singleton].intradayChartURL,
                               _imgChart.frame.size.width,
                               _imgChart.frame.size.height,
                               [splitStkCode objectAtIndex:0],
                               [_userPrefs stripDelayedSymbol:_userPrefs.userCurrentExchange]];
        
        // NSLog(@"Home chart %@",urlString);
        
        ImgChartManager *imgChartManager = [[ImgChartManager alloc] init];
        [imgChartManager getImageChartWithURL:urlString
                            completedWithData:^(NSData *data) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    UIImage *chartImg = [UIImage imageWithData:data];
                                    _imgChart.image = chartImg;
                                    [self setChartStatus:1];
                                });
                            }
                                      failure:^(NSString *errorMsg) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [self setChartStatus:3];
                                          });
                                      }];
        
        [_imgChart setHidden:NO];
        [webViewKLCI setHidden:YES];
    }
    
    //https://poc.asiaebroker.com/mchart/index_POC.jsp?code=1023&Name=Annica&exchg=KL&mode=d&color=b&lang=en&key=%01%0E%01%03%00%00
    
    // ========================================
    //                TELETRADER
    // ========================================
    if ([chartTypeToLoad isEqualToString:@"TeleTrader"]) {
        
        [self setChartStatus:5];
        
        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
        NSString *urlString = [NSString stringWithFormat:@"%@code=%@&exchg=%@&mode=d&color=b&view=m&lang=en&key=%@",
                               [UserPrefConstants singleton].interactiveChartURL,
                               [splitStkCode objectAtIndex:0],
                               [_userPrefs stripDelayedSymbol:_userPrefs.userCurrentExchange],
                               [[UserPrefConstants singleton] encryptTime]
                               ];
        // NSLog(@"Home chart %@",urlString);
        
        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        webViewKLCI.scrollView.scrollEnabled = NO;
        webViewKLCI.scrollView.bounces = NO;
        [webViewKLCI loadRequest:request];
        
        [_imgChart setHidden:YES];
        [webViewKLCI setHidden:NO];
        
        //        [self setChartStatus:5];
        //
        //        NSArray *splitStkCode = [[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange] componentsSeparatedByString:@"."];
        //        NSString *url = [NSString stringWithFormat:@"%@code=%@&Name=%@&exchg=%@&mode=d&color=b&lang=en&key=%@",
        //                         [UserPrefConstants singleton].interactiveChartURL,
        //                         [splitStkCode objectAtIndex:0],
        //                         [splitStkCode objectAtIndex:1],
        //                         [UserPrefConstants singleton].userCurrentExchange,
        //                         [[UserPrefConstants singleton] encryptTime]];
        //        //NSLog(@"%@",url);
        //
        //        NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        //
        //        webViewKLCI.scrollView.scrollEnabled = NO;
        //        webViewKLCI.scrollView.bounces = NO;
        //        [webViewKLCI loadRequest:request];
        //
        //        [_imgChart setHidden:YES];
        //        [webViewKLCI setHidden:NO];
    }
    
}


#pragma mark - Gestures Methods


-(void)viewNewsDidPanned:(UIPanGestureRecognizer*)recognizer {
    
    UIView *view = recognizer.view;
    
    CGPoint translation = [recognizer translationInView:view.superview];
    
    // CGPoint velocity = [recognizer velocityInView:view.superview];
    
    //if (velocity.x>0) {
    
    if (translation.x>=80) {
        bufferNewsX = translation.x-79;
    } else {
        bufferNewsX = 0;
    }
    
    
    if (bufferNewsX>0) {
        
        CGFloat distanceToDismiss = 250;
        
        if(recognizer.state == UIGestureRecognizerStateBegan){
            
        } else if(recognizer.state == UIGestureRecognizerStateEnded){
            
            if (_viewNews.center.x<=newsViewOriginalPos.x+distanceToDismiss) {
                // return back
                [UIView animateWithDuration:0.2 animations:^{
                    _viewNews.center = newsViewOriginalPos;
                }completion:^(BOOL finished) {
                    
                }];
            }
        } else {
            
            _viewNews.center = CGPointMake(newsViewOriginalPos.x+bufferNewsX, newsViewOriginalPos.y);
            
            if (_viewNews.center.x>=newsViewOriginalPos.x+distanceToDismiss) {
                
                [view removeGestureRecognizer:recognizer];
                [self animateNewsClose:nil];
            }
        }
    }
}

//- (void)handleTapGesture:(UIPanGestureRecognizer *)gestureRecognizer
//{
//    // this is called after gestureRecognizer:shouldRecognizeSimultaneouslyWithGestureRecognizer: so we can safely remove the delegate here
////    if (gestureRecognizer.delegate) {
////        //NSLog(@"Removing delegate...");
////        gestureRecognizer.delegate = nil;
////    }
//    [self panGesture:gestureRecognizer];
//
//}
//- (void)panGesture:(UIPanGestureRecognizer *)sender {
//
//    if (sender.state == UIGestureRecognizerStateBegan) {
//        startLocation = [sender translationInView:self.view];
//    }
//    else if (sender.state == UIGestureRecognizerStateEnded) {
//        CGPoint stopLocation = [sender translationInView:self.view];
//        CGFloat dx = stopLocation.x - startLocation.x;
//     //   CGFloat dy = stopLocation.y - startLocation.y;
//     //   CGFloat distance = sqrt(dx*dx + dy*dy );
//       // //NSLog(@"Distance: %f", dx);
//        if (newsOpen && dx > 200.0) {
//            [self animateNewsClose:nil];
//        }
//    }
//}
/*
 65.0f/255.0f green:117.0f/255.0f blue:5.0f/255.0f alpha:1.0f]];
 else if (idx==1)
 return areaGradientFill= [CPTFill fillWithColor:[CPTColor colorWithComponentRed:87.0f/255.0f green:3.0f/255.0f blue:7.0f/255.0f alpha:1.0f]];
 */

#pragma mark - Market Summary updates

- (void)updateScoreRecStatForView:(UIView*)resultView withUpStock:(NSInteger)upStock
                        downStock:(NSInteger)downStock untraded:(NSInteger)untraded equal:(NSInteger)equal vol:(long long)vol
{
    //UIView *resultView = [[UIView alloc]init];
    
    
    float rectHeight = 7 ;
    float fullWidthBuySell = mainHostingView.frame.size.width;
    float totalBuySell = upStock + downStock;
    
    float buyPosX = 0;
    float buyPosY = 14;
    
    if (totalBuySell<=0) totalBuySell = 1;
    
    float buyPercentage  = upStock / totalBuySell;
    float buyWidth = buyPercentage * fullWidthBuySell;
    ////NSLog(@"%f, %f", buyPercentage, buyWidth);
    
    float sellPosX = buyWidth;
    float sellPosY = buyPosY;
    float sellWidth = (1 - buyPercentage) * fullWidthBuySell;
    ////NSLog(@"%f, %f", buyPercentage, sellWidth);
    
    
    UIView *buyView  = [resultView viewWithTag:10];
    buyView.frame = CGRectMake(buyPosX, buyPosY, buyWidth, rectHeight);
    buyView.backgroundColor = kGreenColor;
    
    UIView *sellView  =  [resultView viewWithTag:11];
    sellView.frame = CGRectMake(sellPosX, sellPosY, sellWidth, rectHeight);
    sellView.backgroundColor = kRedColor;
    
    float totalUntradeEqual = untraded + equal;
    float untradePosX = 0;
    float untradePosY = 22;
    
    
    if (totalUntradeEqual<=0) totalUntradeEqual = 1;
    float untradePercentage  = untraded / totalUntradeEqual;
    float untradeWidth = untradePercentage * fullWidthBuySell;
    
    float equalPosX = untradeWidth;
    float equalPosY = untradePosY;
    float equalWidth = (1 - untradePercentage) * fullWidthBuySell;
    
    UIView *untradedView  = [resultView viewWithTag:12];
    untradedView.frame = CGRectMake(untradePosX, untradePosY, untradeWidth, rectHeight);
    untradedView.backgroundColor = kWhiteColor;
    
    UIView *equalView  = [resultView viewWithTag:13];
    equalView.frame = CGRectMake(equalPosX, equalPosY, equalWidth, rectHeight);
    equalView.backgroundColor = kGrayColor;
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [formatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal]; //3 = 3 decimals.
    [formatter setMinimumFractionDigits:0];
    NSString *valueStr = [formatter stringFromNumber:[NSNumber numberWithLongLong:vol]];
    
    //Labels
    UILabel *lblUp      = [resultView viewWithTag:20];
    lblUp.frame = CGRectMake(0, 0, 45, 15);
    UILabel *lblDown    = [resultView viewWithTag:21];
    lblDown.frame = CGRectMake(mainHostingView.frame.size.width-45, 0, 45, 15);
    UILabel *lblUntrade = [resultView viewWithTag:22];
    lblUntrade.frame = CGRectMake(0, 30, 45, 15);
    UILabel *lblEqual   = [resultView viewWithTag:23];
    lblEqual.frame = CGRectMake(mainHostingView.frame.size.width-45, 30, 45, 15);
    UILabel *lblVol     = [resultView viewWithTag:24];
    lblVol.frame = CGRectMake(40, 30, 90, 15);
    
    lblUp.textColor = [UIColor whiteColor];
    lblDown.textColor = [UIColor whiteColor];
    lblUntrade.textColor = [UIColor whiteColor];
    lblEqual.textColor = [UIColor whiteColor];
    lblVol.textColor = [UIColor whiteColor];
    
    lblUp.text = [NSString stringWithFormat:@"%ld",(long)upStock];
    lblDown.text = [NSString stringWithFormat:@"%ld",(long)downStock];
    lblUntrade.text = [NSString stringWithFormat:@"%ld",(long)untraded];
    lblEqual.text = [NSString stringWithFormat:@"%ld",(long)equal];
    lblVol.text = valueStr;
    
    lblUp.textAlignment = NSTextAlignmentLeft;
    lblDown.textAlignment = NSTextAlignmentRight;
    lblUntrade.textAlignment = NSTextAlignmentLeft;
    lblEqual.textAlignment = NSTextAlignmentRight;
    lblVol.textAlignment = NSTextAlignmentCenter;
    
    [lblUp setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    [lblDown setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    [lblUntrade setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    [lblEqual setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    [lblVol setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    
    
    _legendUp.backgroundColor = kGreenColor;
    _legendDown.backgroundColor = kRedColor;
    _legendUnch.backgroundColor = kGrayColor;
    _legendUntr.backgroundColor = kWhiteColor;
    
}

- (void)constructMainMarketPieChart
{
    
    if ([[_mainGraph allPlots] count]>0) {
        
        [_mainGraph removePlot:_pieMainChart];
        
    }
    // 1 - Create and initialise graph
    
    [dataForMainChart removeAllObjects];
    
    _mainGraph.paddingLeft = 0.0f;
    _mainGraph.paddingTop = 0.0f;
    _mainGraph.paddingRight = 0.0f;
    _mainGraph.paddingBottom = 0.0f;
    _mainGraph.axisSet = nil;
    //    [graph applyTheme:[CPTTheme themeNamed:kCPTPlainBlackTheme]];
    
    //    pieChart.pieRadius = (mainHostingView.bounds.size.height * 0.7) / 2;
    _pieMainChart.pieRadius = (mainHostingView.bounds.size.height * 1) / 2.1;
    // _pieMainChart.pieInnerRadius = (((mainHostingView.bounds.size.height * 0.7) / 2)*0.7);
    _pieMainChart.identifier = @"MainMarketChart";
    _pieMainChart.startAngle = M_PI_4;
    _pieMainChart.sliceDirection = CPTPieDirectionClockwise;
    _pieMainChart.borderLineStyle = nil;
    
    // 3 - Create gradient
    //    CPTGradient *overlayGradient = [[CPTGradient alloc] init];
    //    overlayGradient.gradientType = CPTGradientTypeRadial;
    //    overlayGradient              = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.0] atPosition:0.0];
    //    overlayGradient              = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.3] atPosition:0.9];
    //    overlayGradient              = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.7] atPosition:1.0];
    //    pieChart.overlayFill = [CPTFill fillWithGradient:overlayGradient];
    //
    
    // 4 - Add chart to graph
    [_mainGraph addPlot:_pieMainChart];
    
    _mainGraph.borderLineStyle = nil;
    
    //     Add some initial data
    [dataForMainChart addObject:[totalMarketDict objectForKey:@"mainTotalUp"]];
    [dataForMainChart addObject:[totalMarketDict objectForKey:@"mainTotalDown"]];
    [dataForMainChart addObject:[totalMarketDict objectForKey:@"mainTotalUnchg"]];
    [dataForMainChart addObject:[totalMarketDict objectForKey:@"mainTotalUntraded"]];
    
    NSInteger up       =[[totalMarketDict objectForKey:@"mainTotalUp"]integerValue];
    NSInteger down     =[[totalMarketDict objectForKey:@"mainTotalDown"]integerValue];
    NSInteger equal    =[[totalMarketDict objectForKey:@"mainTotalUnchg"]integerValue];
    NSInteger untraded =[[totalMarketDict objectForKey:@"mainTotalUntraded"]integerValue];
    long long volume   =[[totalMarketDict objectForKey:@"mainTotalVol"]longLongValue];
    
    [_mainMarketStat setHidden:NO];
    [self updateScoreRecStatForView:_mainMarketStat withUpStock:up downStock:down untraded:untraded equal:equal vol:volume];
    [_mainMarketStat setFrame:CGRectMake(mainHostingView.frame.origin.x, mainHostingView.frame.origin.y+mainHostingView.frame.size.height+8, mainHostingView.frame.size.width, 50)];
    //   [scrollViewTop addSubview: _mainMarketStat];
    ////NSLog(@"Loaded MainMarketChart");
    
}
- (void)constructWarrantMarketPieChart
{
    
    if ([[_warrantGraph allPlots] count]>0) {
        [_warrantGraph removePlot:_pieWarrantChart];
    }
    // 1 - Create and initialise graph
    
    [dataForWarrantChart removeAllObjects];
    
    _warrantGraph.paddingLeft = 0.0f;
    _warrantGraph.paddingTop = 0.0f;
    _warrantGraph.paddingRight = 0.0f;
    _warrantGraph.paddingBottom = 0.0f;
    _warrantGraph.axisSet = nil;
    //        [graph applyTheme:[CPTTheme themeNamed:kCPTPlainBlackTheme]];
    
    
    _pieWarrantChart.pieRadius = (warrantHostingView.bounds.size.height * 1) / 2.1;
    _pieWarrantChart.identifier = @"WarrantMarketChart";
    _pieWarrantChart.startAngle = M_PI_4;
    _pieWarrantChart.sliceDirection = CPTPieDirectionClockwise;
    _pieWarrantChart.borderLineStyle = nil;
    
    // 4 - Add chart to graph
    [_warrantGraph addPlot:_pieWarrantChart];
    _warrantGraph.borderLineStyle = nil;
    
    [dataForWarrantChart addObject:[totalMarketDict objectForKey:@"warTotalUp"]];
    [dataForWarrantChart addObject:[totalMarketDict objectForKey:@"warTotalDown"]];
    [dataForWarrantChart addObject:[totalMarketDict objectForKey:@"warTotalUnchg"]];
    [dataForWarrantChart addObject:[totalMarketDict objectForKey:@"warTotalUntraded"]];
    
    
    NSInteger up       = [[totalMarketDict objectForKey:@"warTotalUp"]integerValue];
    NSInteger down     = [[totalMarketDict objectForKey:@"warTotalDown"]integerValue];
    NSInteger equal    = [[totalMarketDict objectForKey:@"warTotalUnchg"]integerValue];
    NSInteger untraded = [[totalMarketDict objectForKey:@"warTotalUntraded"]integerValue];
    long long volume   = [[totalMarketDict objectForKey:@"warTotalVol"]longLongValue];
    
    [_warrantMarketStat setHidden:NO];
    [self updateScoreRecStatForView:_warrantMarketStat withUpStock:up downStock:down untraded:untraded equal:equal vol:volume];
    [_warrantMarketStat setFrame:CGRectMake(warrantHostingView.frame.origin.x, warrantHostingView.frame.origin.y+warrantHostingView.frame.size.height+8, warrantHostingView.frame.size.width, 50)];
    
    
    /*[dataForAceChart addObject:@375];
     [dataForAceChart addObject:@203];
     [dataForAceChart addObject:@403];
     [dataForAceChart addObject:@120];*/
    
}

- (void)constructAceMarketPieChart
{
    if ([[_aceGraph allPlots] count]>0) {
        [_aceGraph removePlot:_pieAceChart];
    }
    // 1 - Create and initialise graph
    
    [dataForAceChart removeAllObjects];
    
    _aceGraph.paddingLeft = 0.0f;
    _aceGraph.paddingTop = 0.0f;
    _aceGraph.paddingRight = 0.0f;
    _aceGraph.paddingBottom = 0.0f;
    _aceGraph.axisSet = nil;
    //        [graph applyTheme:[CPTTheme themeNamed:kCPTPlainBlackTheme]];
    
    _pieAceChart.pieRadius = (AceHostingView.bounds.size.height * 1) / 2.1;
    _pieAceChart.identifier = @"AceMarketChart";
    _pieAceChart.startAngle = M_PI_4;
    _pieAceChart.sliceDirection = CPTPieDirectionClockwise;
    _pieAceChart.borderLineStyle = nil;
    // 4 - Add chart to graph
    [_aceGraph addPlot:_pieAceChart];
    _aceGraph.borderLineStyle = nil;
    
    // Add some initial data
    [dataForAceChart addObject:[totalMarketDict objectForKey:@"aceTotalUp"]];
    [dataForAceChart addObject:[totalMarketDict objectForKey:@"aceTotalDown"]];
    [dataForAceChart addObject:[totalMarketDict objectForKey:@"aceTotalUnchg"]];
    [dataForAceChart addObject:[totalMarketDict objectForKey:@"aceTotalUntraded"]];
    
    NSInteger up       = [[totalMarketDict objectForKey:@"aceTotalUp"]integerValue];
    NSInteger down     = [[totalMarketDict objectForKey:@"aceTotalDown"]integerValue];
    NSInteger equal    = [[totalMarketDict objectForKey:@"aceTotalUnchg"]integerValue];
    NSInteger untraded = [[totalMarketDict objectForKey:@"aceTotalUntraded"]integerValue];
    long long volume   = [[totalMarketDict objectForKey:@"aceTotalVol"]longLongValue];
    
    [_aceMarketStat setHidden:NO];
    [self updateScoreRecStatForView:_aceMarketStat withUpStock:up downStock:down untraded:untraded equal:equal vol:volume];
    [_aceMarketStat setFrame:CGRectMake(AceHostingView.frame.origin.x, AceHostingView.frame.origin.y+AceHostingView.frame.size.height+8, AceHostingView.frame.size.width, 50)];
    
    [_legendView setHidden:NO];
    ////NSLog(@"Loaded AceMarketChart");
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void) printRect:(CGRect)rect
{
    NSLog(@"%0.1f, %0.1f, %0.1f, %0.1f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

- (IBAction)animateNewsOpen:(id)sender
{
    
    if (newsOpened){
        newsFrame =_viewNews.frame;
        newsOpened = 0;
    }
    //    CGRect destFrame = CGRectMake(0,0, _viewNews.frame.size.width, _viewNews.frame.size.height) ;
    //    CGRect destLblLatestNotice =CGRectMake(50, 10, lblLatestNotice.frame.size.width, lblLatestNotice.frame.size.height) ;
    //    CGRect destBtnHide = CGRectMake(10,5, btnHideNews.frame.size.width, btnHideNews.frame.size.height) ;
    //    [UIView beginAnimations:nil context:nil];
    //    [UIView setAnimationDuration:0.5];
    //    [UIView setAnimationDelay:0.0];
    //    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //
    //    _viewNews.frame = destFrame;
    //    lblLatestNotice.frame = destLblLatestNotice;
    //    btnHideNews.frame = destBtnHide;
    //    newsOpen = YES;
    //    [UIView commitAnimations];
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect destFrame = CGRectMake(0,0, _viewNews.frame.size.width, _viewNews.frame.size.height) ;
        CGRect destLblLatestNotice =CGRectMake(50, 10, lblLatestNotice.frame.size.width, lblLatestNotice.frame.size.height) ;
        CGRect destBtnHide = CGRectMake(10,5, btnHideNews.frame.size.width, btnHideNews.frame.size.height) ;
        _viewNews.frame = destFrame;
        lblLatestNotice.frame = destLblLatestNotice;
        btnHideNews.frame = destBtnHide;
        
    } completion:^(BOOL finished) {
        newsOpen = YES;
        newsViewOriginalPos = _viewNews.center;
        
        // clear all gestures first
        for (UIGestureRecognizer *gr in _viewNews.gestureRecognizers) {
            [_viewNews removeGestureRecognizer:gr];
        }
        
        // Pan gesture for orderpad dismissal
        UIPanGestureRecognizer *panGestureNews = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewNewsDidPanned:)];
        [panGestureNews setMinimumNumberOfTouches:2];
        panGestureNews.delegate = self;
        [_viewNews addGestureRecognizer:panGestureNews];
        
    }];
    
    
}



- (IBAction)checkQCFeed:(id)sender
{
    ////NSLog(@"QC Count : %lu",(unsigned long)[[[QCData singleton]qcFeedDataDict]count]);
    ////NSLog(@"[[[QCData singleton]qcFeedDataDict]allKeys] : %@",[[[QCData singleton]qcFeedDataDict]allKeys]);
    ////NSLog(@"QC Count : %lu",(unsigned long)[[[QCData singleton]qcScoreBoardDict]count]);
    
}

- (IBAction)animateNewsClose:(id)sender
{
    newsOpen = NO;
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect destFrame = newsFrame;
        CGRect destLblLatestNotice =CGRectMake(10, 10, lblLatestNotice.frame.size.width, lblLatestNotice.frame.size.height) ;
        CGRect destBtnHide = CGRectMake(-60,5, btnHideNews.frame.size.width, btnHideNews.frame.size.height) ;
        _viewNews.frame = destFrame;
        lblLatestNotice.frame = destLblLatestNotice;
        btnHideNews.frame = destBtnHide;
        
    } completion:^(BOOL finished) {
        
        
    }];
}


#pragma mark - ATP replies

-(void)didReceiveJarNews:(NSNotification*)notification {
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        newsDataSource = [[notification.userInfo objectForKey:@"data"] mutableCopy];
        
        NSLog(@"JarNews %@", newsDataSource);
        
        
        if ([newsDataSource count]<=0) {
            
            [_loadingNewsLabel setHidden:NO];
            isJarNews = NO;
            [[VertxConnectionManager singleton] vertxCompanyNews:@""];
            
        } else {
            
            [_loadingNewsLabel setHidden:YES];
            [tableViewNews reloadData];
            
        }
        
    });
    
}

-(void)didReceiveExchangeList {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self loadChartMethod];
        
    });
}

-(void)didFinishGetArchiveNews:(NSNotification*)notification {
    
    
    NSArray *newsArray = [notification.userInfo objectForKey:@"News"];
    NSLog(@"archiveNews %@", newsArray);
    
    if ([newsArray count]>0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            archiveNewsTableDataArr = [NSArray arrayWithArray:newsArray];
            
            NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"dt" ascending: NO];
            archiveNewsTableDataArr =   [archiveNewsTableDataArr sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
            [_loadingNewsLabel setHidden:YES];
            [tableViewNews reloadData];
        });
    } else {
        // News not available
    }
}

-(void)didFinishGetElasticNews:(NSNotification*)notification {
    
    
    
}




#pragma mark -
#pragma mark VertxConnectionManager
//====================================================================
//                        VertxConnectionManager Delegate
//====================================================================

//Retrieval Selector
//- (void)didFinishedSortTop:(NSNotification *)notification
//{
//    NSDictionary * response = [notification.userInfo copy];
//    if([currentSortCmd isEqualToString:@"TopWinner"])
//    {
//        leftStkCodeArr = [[NSArray alloc]initWithArray:[response allKeys] copyItems:YES];
//        currentSortCmd = @"TopLoser";
//        [_vcm vertxSortWithExchange:[UserPrefConstants singleton].userCurrentExchange sortFID:FID_CUSTOM_F_CHANGE isDescending:NO];
//        
//    }
//    else if([currentSortCmd isEqualToString:@"TopLoser"])
//    {
//        
//        rightStkCodeArr = [[NSArray alloc]initWithArray:[response allKeys] copyItems:YES];
//        [self generateStockFlagToView:_viewBottomRight withStockStrArr:rightStkCodeArr];
//        [self generateStockFlagToView:_viewBottomLeft withStockStrArr:leftStkCodeArr];
//        
//        NSMutableArray *arr = [[leftStkCodeArr arrayByAddingObjectsFromArray:rightStkCodeArr]mutableCopy];
//        [UserPrefConstants singleton].userSelectingStockCodeArr = arr;
//        //if(arr)
//        //[UserPrefConstants singleton].userSelectingStockCode = [arr objectAtIndex:0];
//        [loadingBtmLeft stopAnimating];
//        [loadingBtmRight stopAnimating];
//    }
//}

- (void)didFinishedSortTopLeft:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    
    leftStkCodeArr = [[NSArray alloc]initWithArray:[response objectForKey:@"sortedresults"]copyItems:YES];
    
    //NSLog(@"leftStkCodeArr %@", leftStkCodeArr);
    
    if ([leftStkCodeArr count]>0) {
        [UserPrefConstants singleton].userSelectingStockCodeArr = [leftStkCodeArr mutableCopy];
        if ([UserPrefConstants singleton].userSelectingStockCode==nil) {
            [UserPrefConstants singleton].userSelectingStockCode = [leftStkCodeArr objectAtIndex:0];
        }
        lblNotavaialblergt.hidden=YES;
        lblNotavailable.hidden=YES;
    } else {
        lblNotavaialblergt.hidden=NO;
        lblNotavailable.hidden=NO;
    }
    [self generateStockFlagToView:TRUE withStockStrArr:leftStkCodeArr];
    
    
    
    // NSLog(@"didFinishedSortTopLeft [[QCData singleton]qcFeedDataDict] : %@",[[[QCData singleton]qcFeedDataDict] allKeys]);
    
}

- (void)didUpdate:(NSNotification *)notification
{
    NSString * response = [notification object];
    if (response!=nil) {
        if([leftStkCodeArr containsObject:response])
        {
            long x=[leftStkCodeArr indexOfObject:response];
            [self generateStockFlagToView:TRUE withStockAtIndex:x withStockcode:response];
        }
        
        // sortedArr is 10 stocks only
        if([sortedArr containsObject:response])
        {
            long x=[sortedArr indexOfObject:response];
            [self generateStockFlagToView:FALSE withStockAtIndex:x withStockcode:response];
        }
        //        if([rightStkCodeArr containsObject:response])
        //        {
        //            long y=[rightStkCodeArr indexOfObject:response];
        //            [self generateStockFlagToView:_viewBottomRight withStockAtIndex:y withStockcode:response];
        //        }
    }
    
    
}
- (void)didFinishedSortTopRight:(NSNotification *)notification
{
    //    NSDictionary * response = [notification.userInfo copy];
    //    rightStkCodeArr = [[NSArray alloc]initWithArray:[response objectForKey:@"sortedresults"]copyItems:YES];
    //    [self generateStockFlagToView:_viewBottomRight withStockStrArr:rightStkCodeArr];
    
}

- (void)didFinishedDoneGetKLCI:(NSNotification *)notification
{
    //NSDictionary * response = [notification.userInfo copy];
    //NSLog(@"response : %@",response);
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    //NSLog(@"qcKlciDict %@", [[QCData singleton]qcKlciDict]);
    long long totalVolume = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_110_I_SCORE_VOL]longLongValue];
    long long totalVal = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_111_D_SCORE_VAL]longLongValue];
    long long totalScore = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_133_D_SCORE_OF_TRD]longLongValue];
    
    lblTotalVolume.text         = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalVolume]];
    lblTotalValue.text          = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalVal]];
    lblTotalScoreOfTrade.text   = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:totalScore]];
    lblTotalUp.text             = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_105_I_SCORE_UP]stringValue];
    lblTotalDown.text           = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_106_I_SCORE_DW]stringValue];
    lblTotalUnChg.text          = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_107_I_SCORE_UNCHG]stringValue];
    lblTotalUnTrd.text          = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_108_I_SCORE_NOTRD]stringValue];
    
    
}

- (void)didFinishedDoneGetIndices:(NSNotification *)notification
{
    //NSDictionary * response = [notification.userInfo copy];
    ////NSLog(@"didFinishedDoneGetIndices Response : %@",response);
    
}



- (void)doneVertxGetScoreBoardWithExchange:(NSNotification *)notification
{
    
    if (([[[QCData singleton]qcScoreBoardDict] count]<=0)&&(!isFullScreenChart)) {
        // if there are totally scoreboard data, then show Composite Index+graph only
        [scrollViewTop setHidden:YES];
        [pageControlTop setHidden:YES];
        [_viewMarketActivities addSubview:_viewKLCI];
        _viewKLCI.center = scrollViewTop.center;
        pieChartsAdded = NO;
        return;
    }
    
    // NSLog(@"[[QCData singleton]qcScoreBoardDict] %@",[[QCData singleton]qcScoreBoardDict]);
    
    // CIMB SG sends 2201 twice and 2203. 2204 data missing.
    mainMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2201]];
    warrantMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2203]];
    aceMarketDict = [[[QCData singleton]qcScoreBoardDict]objectForKey:[NSNumber numberWithInt:2204]];
    
    if ([mainMarketDict count]>0) data2201 = 1; else data2201 = 0;
    if ([warrantMarketDict count]>0) data2203 = 1; else data2203 = 0;
    if ([aceMarketDict count]>0) data2204 = 1; else data2204 = 0;
    
    vertxMarketQueryCount  = data2201+data2203+data2204;
    
    if(vertxMarketQueryCount == 3) // 3 = Main, Warrant and Ace.
    {
        
        // NSLog(@"Warrant %ld", (unsigned long)[warrantMarketDict count]);
        
        vertxMarketQueryCount =0; // reset
        //Get dicts
        
        [self calculateCustomFields:mainMarketDict];
        [self calculateCustomFields:aceMarketDict];
        [self calculateCustomFields:warrantMarketDict];
        
        //Construct PieCharts
        [self constructMainMarketPieChart];
        [self constructWarrantMarketPieChart];
        [self constructAceMarketPieChart];
        
        //NSLog(@"totalMarketDict : %@",totalMarketDict);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        pieChartsAdded = YES;
    }
    
    if (scoreBoardUpdateCnt==1) {
        
        if (pieChartsAdded) {
            [scrollViewTop setHidden:NO];
            [pageControlTop setHidden:NO];
            [scrollViewTop addSubview:_viewKLCI];
            _viewKLCI.center = CGPointMake(scrollViewTop.frame.size.width*1.5, _viewKLCI.center.y);
        } else {
            [scrollViewTop setHidden:YES];
            [pageControlTop setHidden:YES];
            [_viewMarketActivities addSubview:_viewKLCI];
            _viewKLCI.center = scrollViewTop.center;
        }
    }
    
}

- (void)calculateCustomFields:(NSDictionary *)marketDict
{
    NSArray *headerFIDs = @[@"37",@"110",@"105",@"106",@"107",@"108",@"109",@"111"];
    
    
    
    for (NSDictionary *dict in [marketDict allValues])//Mining, REITS, IPC, Finance...
    {
        
        // NSLog(@"marketDict %@", dict);
        for(NSString *fid in headerFIDs)//UP, Down, Unchanged..
        {
            if([fid isEqualToString:@"105"])
            {
                totalUp += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"106"])
            {
                totalDown += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"107"])
            {
                totalUnchg += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"108"])
            {
                totalUntrd += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"109"])
            {
                totalTotal += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"110"])
            {
                totalVol += [[dict objectForKey:fid]integerValue];
            }
            else if([fid isEqualToString:@"111"])
            {
                totalValue += [[dict objectForKey:fid]integerValue];
            }
        }
    }
    
    NSString *marketID;
    if (marketDict)
    {
        marketID = [[[[marketDict allValues]objectAtIndex:0]objectForKey:@"35"]stringValue];
    }
    if([marketID isEqualToString:@"2201"])
    {
        ////NSLog(@"v2201: %@",[marketDict objectForKey:@"35"]);
        
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUp]     forKey:@"mainTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalDown]   forKey:@"mainTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUnchg]  forKey:@"mainTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUntrd]  forKey:@"mainTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalTotal]  forKey:@"mainTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"mainTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalValue]  forKey:@"mainTotalValue"];
    }
    else if([marketID isEqualToString:@"2203"])
        
    {
        ////NSLog(@"v2203 %@",[marketDict objectForKey:@"35"]);
        
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUp]     forKey:@"warTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalDown]   forKey:@"warTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUnchg]  forKey:@"warTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUntrd]  forKey:@"warTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalTotal]  forKey:@"warTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"warTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalValue]  forKey:@"warTotalValue"];
        
    }
    else if([marketID isEqualToString:@"2204"])
    {
        ////NSLog(@"v2204: %@",[marketDict objectForKey:@"35"]);
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUp]     forKey:@"aceTotalUp"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalDown]   forKey:@"aceTotalDown"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUnchg]  forKey:@"aceTotalUnchg"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalUntrd]  forKey:@"aceTotalUntraded"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalTotal]  forKey:@"aceTotalTotal"];
        [totalMarketDict setObject:[NSNumber numberWithLongLong:totalVol]    forKey:@"aceTotalVol"];
        [totalMarketDict setObject:[NSNumber numberWithInt:totalValue]  forKey:@"aceTotalValue"];
    }
    
    ////NSLog(@"totalUp : %d",totalUp);
    ////NSLog(@"totalDown : %d",totalDown);
    ////NSLog(@"totalUnchg : %d",totalUnchg);
    ////NSLog(@"totalUntrd : %d",totalUntrd);
    ////NSLog(@"totalTotal : %d",totalTotal);
    ////NSLog(@"totalVol : %d",totalVol);
    ////NSLog(@"totalValue : %d",totalValue);
    totalUp = 0;
    totalDown = 0;
    totalUnchg = 0;
    totalUntrd = 0;
    totalTotal = 0;
    totalVol = 0;
    totalValue = 0;
}



//====================================================================
//                        ScrollView Delegate
//====================================================================


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    switch (sender.tag) {
        case 0:
        {            // Switch the indicator when more than 50% of the previous/next page is visible
            CGFloat pageWidth = scrollViewLatestNotice.frame.size.width;
            int page = floor((scrollViewLatestNotice.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            pageControlLatestNotice.currentPage = page;
        }
            break;
        case 1:
        {
            CGFloat pageWidth = scrollViewTop.frame.size.width;
            int page = floor((scrollViewTop.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            pageControlTop.currentPage = page;
            
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark -
#pragma mark TableView Delegate
//====================================================================
//                        UITableView Delegate
//====================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (tableView.tag==0) {
        if (([newsDataSource count]>0)||([archiveNewsTableDataArr count]>0)) {
            tableView.backgroundView = nil;
            return 1;
        } else {
            /*UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
             messageLabel.text = @"Loading...";
             messageLabel.textColor = [UIColor whiteColor];
             messageLabel.numberOfLines = 0;
             messageLabel.textAlignment = NSTextAlignmentCenter;
             messageLabel.font = [UIFont boldSystemFontOfSize:19];
             [messageLabel sizeToFit];
             
             tableView.backgroundView = messageLabel;*/
            
            
            
            
            return 0;
        }
    } else if (tableView.tag==3) {
        if ([watchListArr count]>0) {
            tableView.backgroundView = nil;
            return 1;
            
        } else {
            
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            messageLabel.text = [LanguageManager stringForKey:@"Watchlist Not Found"];
            messageLabel.textColor = [UIColor whiteColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont boldSystemFontOfSize:19];
            [messageLabel sizeToFit];
            
            tableView.backgroundView = messageLabel;
        }
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag ==0)
        
        if ([UserPrefConstants singleton].isArchiveNews)
            return archiveNewsTableDataArr.count;
        else
            return newsDataSource.count;
    
        else if (tableView.tag ==1)
        {
            return [rankedByNameArr count];
        }
        else if (tableView.tag ==2)
        {
            return [rankedByNameArr count];
        }
        else if (tableView.tag ==3)
        {
            return [watchListArr count];
        }
    return 1;
}




-(long)getIndexForSelectedRank:(NSString*)rank {
    
    NSArray *arr = rankedByNameArr;
    
    
    __block long res = -1;
    [arr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *rankStr = (NSString*)obj;
        
        if ([rankStr isEqualToString:rank]) {
            
            res = idx;
            
        }
        
    }];
    return res;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView.tag ==0) {
        return 80;
    }
    
    return 44; // default
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell2";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // MARK: NEWS
    if (tableView.tag ==0)
    {
        static NSString *CellIdentifier = @"CellNewsID";
        HomeNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil){
            cell = [[HomeNewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        //changing row selected color
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(50/255.0) blue:(50/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
        cell.backgroundColor = [UIColor clearColor];
        
        NSInteger row = indexPath.row;
        
        
        if ([UserPrefConstants singleton].isArchiveNews) {
            NSDictionary *item = [archiveNewsTableDataArr objectAtIndex:row];
            
            if (item) {
                
                cell.newsExch.text = [item objectForKey:@"stk"];
                cell.newsTimestamp.text = [[UserPrefConstants singleton] getLogicalDate:[item objectForKey:@"dt"]];
                NSString *strippedTitle = [[UserPrefConstants singleton] stringByStrippingHTML: [item objectForKey:@"t"]];
                cell.newsTitle.text = strippedTitle;
            }
            
        } else {
            
            // NSLog(@"not archive");
            
            if(isJarNews){
                NSArray *item = [[newsDataSource objectAtIndex:row] componentsSeparatedByString:@"|"];
                if (item) {
                    NSString *titleStr = [item objectAtIndex:10];
                    if ([titleStr length]<=0) titleStr = [LanguageManager stringForKey:@"Announcement"];
                    
                    cell.newsTitle.text = [item objectAtIndex:4];
                    cell.newsTimestamp.text = [[UserPrefConstants singleton] getLogicalDate:[item objectAtIndex:3]];
                    cell.newsExch.text = titleStr;
                }
            }else{
                NSDictionary *item = [newsDataSource objectAtIndex:row];
                if (item) {
                    
                    cell.newsTitle.text = [item objectForKey:@"3"];
                    cell.newsTimestamp.text = [[UserPrefConstants singleton] getLogicalDate:[item objectForKey:@"4"]];
                    cell.newsExch.text = @"";
                    
                }
            }
        }
        
        
        return cell;
    }
    
    // MARK: LEFT SELECTION
    else if(tableView.tag ==1)
    {
        static NSString *CellIdentifier = @"CellLeft";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        UIFont *textLabelFont = [ UIFont fontWithName: @"Helvetica Neue" size: 16.0];
        
        //changing row selected color
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(50/255.0) blue:(50/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font  = textLabelFont;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.userInteractionEnabled = YES;
        
        NSInteger row = indexPath.row;
        
        NSString *name = [LanguageManager stringForKey:[rankedByNameArr objectAtIndex:row]];
        cell.textLabel.text = [LanguageManager stringForKey:@"Sort By: %@" withPlaceholders:@{@"%sortName%":name}];
        if ([[rankedByNameArr objectAtIndex:row] isEqualToString:currentSelectedRankedLeft]) {
            cell.textLabel.textColor = [UIColor cyanColor];
            cell.userInteractionEnabled = NO;
            //cell.hidden = YES;
        }
        return cell;
        
    }
    // MARK: RIGHT SELECTION
    else if(tableView.tag ==2)
    {
        static NSString *CellIdentifier = @"CellRight";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        UIFont *textLabelFont = [ UIFont fontWithName: @"Helvetica Neue" size: 16.0];
        
        //changing row selected color
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(50/255.0) blue:(50/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font  = textLabelFont;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.hidden = NO;
        NSInteger row = indexPath.row;
        NSString *name = [LanguageManager stringForKey:[rankedByNameArr objectAtIndex:row]];
        cell.textLabel.text = [LanguageManager stringForKey:@"Sort By: %@" withPlaceholders:@{@"%sortName%":name}];
        
        if ( [[rankedByNameArr objectAtIndex:row] isEqualToString:currentSelectedRankedLeft] || [[rankedByNameArr objectAtIndex:row] isEqualToString:currentSelectedRankedRight]) {
            //            cell.textLabel.textColor = [UIColor grayColor];
            //            cell.userInteractionEnabled = NO;
            cell.hidden = YES;
        }
        
        return cell;
        
    }
    // MARK: WATCHLIST SELECTION
    else if(tableView.tag ==3)
    {
        static NSString *CellIdentifier = @"CellWatchList";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        UIFont *textLabelFont = [ UIFont fontWithName: @"Helvetica Neue" size: 16.0];
        
        //changing row selected color
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(50/255.0) blue:(50/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font  = textLabelFont;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.userInteractionEnabled = YES;
        NSInteger row = indexPath.row;
        
        NSDictionary *selectedWLDict = [watchListArr objectAtIndex:row];
        
        cell.textLabel.text = [selectedWLDict objectForKey:@"Name"];
        
        if ([UserPrefConstants singleton].watchListName == [selectedWLDict objectForKey:@"Name"]) {
            cell.textLabel.textColor = [UIColor cyanColor];
            cell.userInteractionEnabled = NO;
            
        }
        return cell;
        
    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *selectedExchange = [[AppConstants ExchangeCodeList] objectAtIndex:indexPath.row];
    //////NSLog(@"selectedExchange : %@",selectedExchange);
    ////NSLog(@"Clicked : %@",indexPath);
    NSInteger row = indexPath.row;
    
    [atp timeoutActivity];
    
    // MARK: NEWS
    if(tableView.tag == 0) //News
    {
        
        [self animateNewsOpen:nil];
        
        NSString* js =
        @"var meta = document.createElement('meta'); " \
        "meta.setAttribute( 'name', 'viewport' ); " \
        "meta.setAttribute( 'content', 'width = device-width, initial-scale=1.0, minimum-scale=0.2, maximum-scale=5.0; user-scalable=1;' ); " \
        "document.getElementsByTagName('head')[0].appendChild(meta)";
        
        [webview stringByEvaluatingJavaScriptFromString: js];
        
        
        if ([UserPrefConstants singleton].isArchiveNews) {
            NSDictionary *item = [archiveNewsTableDataArr objectAtIndex:row];
            NSString *url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,[item objectForKey:@"id"]];
            NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval: 30.0];
            [webview loadRequest:request];
            [webview setScalesPageToFit:NO];
        } else {
            
            NSString *url;
            if (isJarNews) {
                NSArray *item = [[newsDataSource objectAtIndex:row] componentsSeparatedByString:@"|"];
                url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,[item objectAtIndex:0]];
            }else{
                NSDictionary *item = [newsDataSource objectAtIndex:row];
                url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,[item objectForKey:@"6"]];
            }
            NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval: 30.0];
            [webview loadRequest:request];
            [webview setScalesPageToFit:NO];
            
        }
        
    }
    // MARK: BOTTOM LEFT
    else if(tableView.tag == 1) //Bottom Left
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if ([cell.textLabel.text isEqualToString:currentSelectedRankedLeft] || [cell.textLabel.text isEqualToString:currentSelectedRankedRight]) {
            return;
        }
        
        [loadingBtmLeft startAnimating];
        
        // //NSLog(@"Selected Left: %@",[rankedByNameArr objectAtIndex:row]);
        NSString *name = [LanguageManager stringForKey:[rankedByNameArr objectAtIndex:row]];
        lblLeftTitle.text = [LanguageManager stringForKey:@"Sorted By %@" withPlaceholders:@{@"%sortName%":name}];
        
        NSArray *propertyDivideDirection = [[rankedByKeyArr objectAtIndex:row] componentsSeparatedByString:@"|"];
        NSString *property    = [propertyDivideDirection objectAtIndex:0];
        NSString *direction = [propertyDivideDirection objectAtIndex:1];
        [[VertxConnectionManager singleton]vertxSortByLeft:property Direction:direction exchg:[UserPrefConstants singleton].userCurrentExchange];
        
        currentSelectedRankedLeft = name;
        //[self enableAllTables];
        [self clickedBottomLeftSelectionButton:nil];
        [tableviewLeftSelection reloadData];
        [tableviewRightSelection reloadData];
    }
    // MARK: BOTTOM RIGHT
    else if(tableView.tag == 2) //Bottom Right
    {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if ([cell.textLabel.text isEqualToString:currentSelectedRankedLeft] || [cell.textLabel.text isEqualToString:currentSelectedRankedRight]) {
            return;
        }
        
        [loadingBtmRight startAnimating];
        
        //        //NSLog(@"Selected Right: %@",[rankedByNameArr objectAtIndex:row]);
        NSString *name = [rankedByNameArr objectAtIndex:row];
        lblRightTitle.text  = [NSString stringWithFormat:@"Sorted By %@",name];;
        
        NSArray *propertyDivideDirection = [[rankedByKeyArr objectAtIndex:row] componentsSeparatedByString:@"|"];
        NSString *property    = [propertyDivideDirection objectAtIndex:0];
        NSString *direction = [propertyDivideDirection objectAtIndex:1];
        [[VertxConnectionManager singleton]vertxSortByRight:property Direction:direction exchg:[UserPrefConstants singleton].userCurrentExchange];
        
        currentSelectedRankedRight = name;
        //[self enableAllTables];
        [self clickedBottomRightSelectionButton:nil];
        [tableviewLeftSelection reloadData];
        [tableviewRightSelection reloadData];
        
        //MARK: WATCHLIST
    }else if(tableView.tag == 3)
    {
        NSMutableArray *exceptionArray = [[NSMutableArray alloc] init];
        [exceptionArray addObjectsFromArray:[UserPrefConstants singleton].tickerStockCodes];
        [exceptionArray addObjectsFromArray:leftStkCodeArr];
        if (_userPrefs.userSelectingStockCode!=nil) [exceptionArray addObject:_userPrefs.userSelectingStockCode];
        [[QCData singleton] removeAllQcFeedExceptThese:exceptionArray];
        
        NSDictionary *WLselected = [watchListArr objectAtIndex:indexPath.row];
        int favID = [[WLselected objectForKey:@"FavID"] intValue];
        
        sortedArr = [[NSArray alloc] init];
        
        [UserPrefConstants singleton].prevWatchlistFavID = favID;
        [UserPrefConstants singleton].watchListName = [WLselected objectForKey:@"Name"];
        [atp getWatchListItems:favID];
        [UserPrefConstants singleton].quoteScreenFavID=[NSNumber numberWithInt:favID];
        
        
        _watchListTitle.text = [LanguageManager stringForKey:@"WatchList: %@" withPlaceholders:@{@"%WLName%":[UserPrefConstants singleton].watchListName}];
        
        for (UIView *i in _viewBottomWatchList.subviews){
            if([i isKindOfClass:[StockFlag class]])
            {
                [i removeFromSuperview];
            }
        }
        [_watchStockLoading startAnimating];
        
        [self clickedWatchListSelectionButton:nil];
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [loadingNews stopAnimating];
    
    
    if (indexPath.row % 2==0) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        cell.contentView.backgroundColor = kCellGray2;
    }
    
    
    
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        //end of loading
        [loadingNews stopAnimating];
        ////NSLog(@"Done Loading Table");
        if([newsDataSource count]>0){
            NSString *url = [NSString stringWithFormat:@"https://news.asiaebroker.com/ebcNews/index.jsp?&id=%@EN",[newsDataSource objectAtIndex:0]];
            NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [webview loadRequest:request];
        }
        
    }
}


- (IBAction)clickedWatchListSelectionButton:(UIButton *)sender
{
    
    [atp timeoutActivity];
    
    
    [UIView transitionWithView:_viewHolderWatchList
                      duration:0.5
                       options:(wListDisplayingFront ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        if(wListDisplayingFront)
                        {
                            _viewBottomWatchList.hidden = YES;
                            _viewWatchListSelection.hidden = NO;
                        }
                        else
                        {
                            _viewBottomWatchList.hidden = NO;
                            _viewWatchListSelection.hidden = YES;
                        }
                        
                        
                    }
     
                    completion:^(BOOL finished) {
                        if (finished) {
                            wListDisplayingFront = !wListDisplayingFront;
                            [_watchListTable reloadData];
                        }
                    }];
    
}


- (IBAction)clickedBottomLeftSelectionButton:(UIButton *)sender
{
    
    [atp timeoutActivity];
    
    if (!rightDisplayingFront) [self clickedBottomRightSelectionButton:nil];
    
    [UIView transitionWithView:viewHolderLeft
                      duration:0.5
                       options:(leftDisplayingFront ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        if(leftDisplayingFront)
                        {
                            _viewBottomLeft.hidden = YES;
                            viewBottomLeftSelection.hidden = NO;
                        }
                        else
                        {
                            _viewBottomLeft.hidden = NO;
                            viewBottomLeftSelection.hidden = YES;
                        }
                    }
     
                    completion:^(BOOL finished) {
                        if (finished) {
                            leftDisplayingFront = !leftDisplayingFront;
                        }
                    }];
    
}

- (IBAction)clickedBottomRightSelectionButton:(UIButton *)sender
{
    [atp timeoutActivity];
    
    if (!leftDisplayingFront) [self clickedBottomLeftSelectionButton:nil];
    
    [UIView transitionWithView:viewHolderRight
                      duration:0.5
                       options:(rightDisplayingFront ? UIViewAnimationOptionTransitionFlipFromRight :
                                UIViewAnimationOptionTransitionFlipFromLeft)
                    animations: ^{
                        if(rightDisplayingFront)
                        {
                            _viewBottomRight.hidden = true;
                            viewBottomRightSelection.hidden = false;
                        }
                        else
                        {
                            _viewBottomRight.hidden = false;
                            viewBottomRightSelection.hidden = true;
                        }
                    }
     
                    completion:^(BOOL finished) {
                        if (finished) {
                            rightDisplayingFront = !rightDisplayingFront;
                        }
                    }];
    
}

//- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//    return YES;
//}
//
//- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Add your Colour.
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    [self setCellColor:[UIColor greenColor] ForCell:cell];  //highlight colour
//}
//
//- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Reset Colour.
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    [self setCellColor:[UIColor clearColor] ForCell:cell]; //normal color
//
//}
//
//- (void)setCellColor:(UIColor *)color ForCell:(UITableViewCell *)cell {
//    cell.contentView.backgroundColor = color;
//    cell.backgroundColor = color;
//}


#pragma mark -
#pragma mark Plot Data Source Methods
//====================================================================
//                        Core-Plot Delegate
//====================================================================
-(CPTFill *)sliceFillForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)idx;
{
    CPTFill *areaGradientFill ;
    
    //    if([pieChart.identifier isEqual:[NSString stringWithFormat:@"MainMarketChart"]])
    // 87.0f/255.0f green:3.0f/255.0f blue:7.0f/255.0f alpha:1.0f
    //    {
    if (idx==0)
        
        return areaGradientFill= [CPTFill fillWithColor:kGreenColorPie];
    else if (idx==1)
        return areaGradientFill= [CPTFill fillWithColor:kRedColorPie];
    else if (idx==2)
        return areaGradientFill= [CPTFill fillWithColor:kGrayColorPie];
    else if (idx==3)
        return areaGradientFill= [CPTFill fillWithColor:kWhiteColorPie];
    //    }
    
    return areaGradientFill;
}

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    if ( [plot isKindOfClass:[CPTPieChart class]] ) {
        return [dataForMainChart count];
    }
    else
        return 0;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    if (CPTPieChartFieldSliceWidth == fieldEnum) {
        if([plot.identifier isEqual:[NSString stringWithFormat:@"MainMarketChart"]])
        {
            return [dataForMainChart objectAtIndex:index];
        }
        else if([plot.identifier isEqual:[NSString stringWithFormat:@"WarrantMarketChart"]])
        {
            return [dataForWarrantChart objectAtIndex:index];
        }
        else if([plot.identifier isEqual:[NSString stringWithFormat:@"AceMarketChart"]])
        {
            return [dataForAceChart objectAtIndex:index];
        }
    }
    return [NSDecimalNumber zero];
}


#pragma mark - Webview delegate


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    // NEWS WEBVIEW
    if (webView==self.webview) {
        
        [loadingNews stopAnimating];
        [mainNewsIndicator stopAnimating];
        
        UserPrefConstants *UPC = [UserPrefConstants singleton];
        NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                              (unsigned long)UPC.newsFontSize];
        [webView stringByEvaluatingJavaScriptFromString:jsString];
        
        if (webView.tag==5) {
            [scoreBoardIndicator stopAnimating];
            NSCachedURLResponse *urlResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:webView.request];
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) urlResponse.response;
            NSInteger statusCode = httpResponse.statusCode;
            
            // NSLog(@"%@", httpResponse.description);
            
            if (statusCode > 399) {
                //NSError *error = [NSError errorWithDomain:@"HTTP Error" code:httpResponse.statusCode userInfo:@{@"response":httpResponse}];
                // Forward the error to webView:didFailLoadWithError: or other
                //NSLog(@"%@", error);
                [_chartErrorMsg setHidden:NO];
                [_imgChart setHidden:YES];
                [webViewKLCI setHidden:YES];
                [_chartErrorMsg setHidden:NO];
            }
            else {
                // No HTTP error
                [_chartErrorMsg setHidden:YES];
                [_imgChart setHidden:YES];
                [webViewKLCI setHidden:NO];
                [_chartErrorMsg setHidden:YES];
            }
            
        }
        
    } else if (webView==self.announcementWebview) {
        
    } else if (webView==self.webViewKLCI) {
        
    }
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    // error code -999 is when loading is interrupted by another call (which is ok).
    if (error.code!=-999) {
        
        // News
        if (webView==self.webview) {
            
            // Announcement
        } else if (webView==self.announcementWebview) {
            
            // Chart
        } else if (webView==self.webViewKLCI) {
            // NSLog(@"fail chart: %@",error.description);
            [self setChartStatus:6];
        }
        
        
    }
    
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    // News
    if (webView==self.webview) {
        
        [mainNewsIndicator startAnimating];
        
        // Announcement
    } else if (webView==self.announcementWebview) {
        
        // Chart
    } else if (webView==self.webViewKLCI) {
        
    }
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    // News
    if (webView==self.webview) {
        
        [mainNewsIndicator startAnimating];
        
        // Announcement
    } else if (webView==self.announcementWebview) {
        
        NSString *string = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].innerHTML"];
        BOOL isEmpty = string==nil || [string length]==0;
        
        if (!isEmpty) {
            [self animateNewsOpen:nil];
            [self.webview loadRequest:request];
            return NO;
        }
        
        // Chart
    } else if (webView==self.webViewKLCI) {
        
    }
    
    //To avoid loading all the google ads.
    if([request.URL.absoluteString rangeOfString:@"googleads"].location != NSNotFound)
        return NO;
    
    return YES;
}

#pragma mark - Page Control

- (IBAction)pageControlClicked:(id)sender {
    pageControlTop=sender;
    [atp timeoutActivity];
    
    int page = (int)pageControlTop.currentPage;
    CGRect frame = scrollViewTop.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollViewTop scrollRectToVisible:frame animated:YES];
    
    
}

- (void)pageControlClickedWithPage:(int)pageControlTopPage {
    
    [atp timeoutActivity];
    
    CGRect frame = scrollViewTop.frame;
    frame.origin.x = frame.size.width * pageControlTopPage;
    frame.origin.y = 0;
    [scrollViewTop scrollRectToVisible:frame animated:YES];
    
}

- (IBAction)pageNoticeControlClicked:(id)sender {
    pageControlLatestNotice=sender;
    int page = (int)pageControlLatestNotice.currentPage;
    CGRect frame = scrollViewLatestNotice.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [scrollViewLatestNotice scrollRectToVisible:frame animated:YES];
    
}

- (IBAction)openWatchList:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openWatchListFromHome" object:nil];
}

- (IBAction)smallerFont:(id)sender {
    
    UserPrefConstants *UPC = [UserPrefConstants singleton];
    
    UPC.newsFontSize = (UPC.newsFontSize > 50) ? UPC.newsFontSize -5 : UPC.newsFontSize;
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                          (unsigned long)UPC.newsFontSize];
    [webview stringByEvaluatingJavaScriptFromString:jsString];
}

- (IBAction)biggerFont:(id)sender {
    
    UserPrefConstants *UPC = [UserPrefConstants singleton];
    
    UPC.newsFontSize = (UPC.newsFontSize < 160) ? UPC.newsFontSize +5 : UPC.newsFontSize;
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                          (unsigned long)UPC.newsFontSize];
    [webview stringByEvaluatingJavaScriptFromString:jsString];
}


#pragma mark - CollectionView Delegate/datasource

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == collectionView_Left) {
        return stockListArr.count;
    }
    
    return wlListArr.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    StockCell *cell  = nil;
    NSDictionary*dic = nil;
    
    if (collectionView == collectionView_Left) {
        dic = stockListArr[indexPath.row];
        cell = [collectionView_Left dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    }else{
        dic = wlListArr[indexPath.row];
        cell = [collectionView_Right dequeueReusableCellWithReuseIdentifier:identifier2 forIndexPath:indexPath];
    }
    
    
    NSLog(@">>>>>>>> %@",dic);
    
    [cell initWithStockDetails:dic[@"stkCode"]
                       stkName:dic[@"stkName"]
                 stkPercentage:dic[@"stkPercentage"]
                     stkVolume:dic[@"stkVolume"]
                   stkLastDone:dic[@"stkLastDone"]
                 stkLastChange:dic[@"stkLastChange"] ];
    
    return cell;
}
//ggtt
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    float w = CGRectGetWidth(collectionView_Left.frame);
    float h = CGRectGetHeight(collectionView_Left.frame);
    //372/337
    CGSize cellSize = CGSizeMake(w/2-6, 76);
    
    return cellSize;
}

#pragma mark -


- (void)generateStockFlagToView:(BOOL)isLeft withStockStrArr:(NSArray*)arr
{
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSize:3];
    
    isLeft == TRUE ?
    [stockListArr removeAllObjects] :
    [wlListArr removeAllObjects];
    
    for (NSString *stkCode in arr)
    {
        NSDictionary *stockDetail = [[NSDictionary alloc]initWithDictionary:[[[QCData singleton]qcFeedDataDict]objectForKey:stkCode]];
        
        NSString *stkNameStr = @"";
        if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
            stkNameStr = [stockDetail objectForKey:FID_130_S_SYSBOL_2];
        } else {
            stkNameStr = [stockDetail objectForKey:FID_38_S_STOCK_NAME];
        }
        
        
        NSString *lastDoneString = [priceFormatter stringFromNumber:[stockDetail
                                                                     objectForKey:FID_98_D_LAST_DONE_PRICE]];
        float valueLastDone = [[stockDetail objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
        if (lastDoneString==nil) {
            if ([UserPrefConstants singleton].pointerDecimal==4) {
                lastDoneString = [NSString stringWithFormat:@"%@",[priceFormatter stringFromNumber:[NSNumber numberWithFloat:valueLastDone]]];
            }else{
                lastDoneString = [NSString stringWithFormat:@"%@",[priceFormatter stringFromNumber:[NSNumber numberWithFloat:valueLastDone]]];
            }
        }
        
        NSDictionary *dic = @{
                              @"stkCode":stkCode,
                              @"stkName": stkNameStr,
                              @"stkPercentage" : [[stockDetail objectForKey:FID_CUSTOM_F_CHANGE_PER] stringValue],
                              @"stkVolume":[[UserPrefConstants singleton] abbreviateNumber:[[stockDetail objectForKey:FID_101_I_VOLUME] longLongValue]] ,
                              @"stkLastDone" : lastDoneString,
                              @"stkLastChange" :  [priceFormatter stringFromNumber:[stockDetail objectForKey:FID_CUSTOM_F_CHANGE]]
                              };
        
        isLeft == TRUE ?
        [stockListArr addObject:dic] :
        [wlListArr addObject:dic];
    }
    
    //Reload view.
    isLeft == TRUE ?
    [collectionView_Left reloadData] :
    [collectionView_Right reloadData];
    
    
    if ([loadingBtmLeft  isAnimating]) {
        [loadingBtmLeft stopAnimating];
    }
    if ([loadingBtmRight isAnimating]) {
        [loadingBtmRight stopAnimating];
    }
    
}

- (void)generateStockFlagToView:(BOOL)isLeft withStockAtIndex:(long)indexofsubview withStockcode:(NSString*)stkCode
{
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSize:3];
    
    NSDictionary *stockDetail = [[NSDictionary alloc]initWithDictionary:[[[QCData singleton]qcFeedDataDict]objectForKey:stkCode]];
    
    NSString *stkNameStr = @"";
    if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
        stkNameStr = [stockDetail objectForKey:FID_130_S_SYSBOL_2];
    } else {
        stkNameStr = [stockDetail objectForKey:FID_38_S_STOCK_NAME];
    }
    
    
    NSString *lastDoneString = [priceFormatter stringFromNumber:[stockDetail
                                                                 objectForKey:FID_98_D_LAST_DONE_PRICE]];
    float valueLastDone = [[stockDetail objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
    if (lastDoneString==nil) {
        if ([UserPrefConstants singleton].pointerDecimal==4) {
            lastDoneString = [NSString stringWithFormat:@"%@",[priceFormatter stringFromNumber:[NSNumber numberWithFloat:valueLastDone]]];
        }else{
            lastDoneString = [NSString stringWithFormat:@"%@",[priceFormatter stringFromNumber:[NSNumber numberWithFloat:valueLastDone]]];
        }
    }
    
    NSDictionary *dic = @{
                          @"stkCode":stkCode,
                          @"stkName": stkNameStr,
                          @"stkPercentage" : [[stockDetail objectForKey:FID_CUSTOM_F_CHANGE_PER] stringValue],
                          @"stkVolume":[[UserPrefConstants singleton] abbreviateNumber:[[stockDetail objectForKey:FID_101_I_VOLUME] longLongValue]] ,
                          @"stkLastDone" : lastDoneString,
                          @"stkLastChange" :  [priceFormatter stringFromNumber:[stockDetail objectForKey:FID_CUSTOM_F_CHANGE]]
                          };
    NSIndexPath *firstItem = [NSIndexPath indexPathForRow:indexofsubview inSection:0];
    
    if( isLeft == TRUE ){
        [stockListArr replaceObjectAtIndex:indexofsubview withObject:dic];
        //reload cell
        [collectionView_Left performBatchUpdates:^{
            [collectionView_Left reloadItemsAtIndexPaths:@[firstItem]];
        } completion:^(BOOL finished) {}];
        
    }
    else{
        [wlListArr replaceObjectAtIndex:indexofsubview withObject:dic];
        //reload cell
        [collectionView_Right performBatchUpdates:^{
            [collectionView_Right reloadItemsAtIndexPaths:@[firstItem]];
        } completion:^(BOOL finished) {}];
        
    }
    
    
}


@end
