//
//  DetailsTableViewCell.m
//  TCiPad
//
//  Created by n2n on 19/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "DetailsTableViewCell.h"
#import "AppMacro.h"
#import "ThemeManager.h"
@implementation DetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.detailsTitle.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_HightLightTextColor);
    self.detailsContent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_TextColor);
    
    self.detailsTitleRight.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_HightLightTextColor);
    self.detailsContentRight.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].ot_TextColor);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
