//
//  UIImageUtility.m
//  testTmx
//
//  Created by Apple on 2/6/10.
//  Copyright 2010 __Terato Tech Sdn Bhd__. All rights reserved.
//

#import "UIImageUtility.h"


@implementation UIImage(Loading)
+ (UIImage*)imageFromResource:(NSString*)name {
	return [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:nil]];
}
@end