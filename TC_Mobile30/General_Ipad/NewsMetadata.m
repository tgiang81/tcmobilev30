//
//  NewsMetadata.m
//  N2NTrader
//
//  Created by Adrian Lo on 4/9/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import "NewsMetadata.h"


@implementation NewsMetadata
@synthesize exchange, stockcode, newsID, newsTitle, date1, date2, cat, stockname;
@synthesize codeID,newsDescription,fullDate,sourceNews;

@end
