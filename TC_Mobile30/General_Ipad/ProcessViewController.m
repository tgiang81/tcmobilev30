//
//  ProcessViewController.m
//  TCiPad
//
//  Created by n2n on 06/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "ProcessViewController.h"
@interface ProcessViewController ()

@end


static ProcessViewController *_tcAlert;


@implementation ProcessViewController

@synthesize delegate;
- (instancetype)initWithMessage:(NSString *)message{
    self = NEW_VC_FROM_STORYBOARD(@"Main", @"ProcessViewController");
    if (self) {
        self.messageLabel.text = message;
        if (message && message.length > 0) {
            _messageLabel.hidden = NO;
        }
        [self initialFirst:message];
        //Set this for show like popup
        self.providesPresentationContextTransitionStyle = YES;
        self.definesPresentationContext = YES;
        [self setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        self.view.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for(UIView *v in [self.boxView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
            btn.layer.masksToBounds = YES;
            btn.layer.borderWidth = 1.0;
            btn.layer.borderColor = [UIColor darkGrayColor].CGColor;
        }
    }
    _boxView.layer.cornerRadius = 8.0;
    _boxView.layer.masksToBounds = YES;
    
    self.activityView = [[DGActivityIndicatorView alloc]
                         initWithType:DGActivityIndicatorAnimationTypeNineDots tintColor:[UIColor whiteColor]];
    
    _activityView.frame = CGRectMake(0, 0, 50, 50);
    [_boxView addSubview:_activityView];
    [_activityView startAnimating];
    [_activityView setCenter:CGPointMake(_messageLabel.center.x, 40)];
    [_activityView setHidden:YES];
    [_activityView setSize:50];
    
    // NSLog(@"setupDict %@",_setupDict);
    
    self.messageLabel.text = [_setupDict objectForKey:@"message"];
    [self.retryButton setHidden:![[_setupDict objectForKey:@"retry"] boolValue]];
    [self.closeButton setHidden:![[_setupDict objectForKey:@"close"] boolValue]];
    [self.activityView setHidden:![_setupDict objectForKey:@"activity"]];
    
    [self updateButton];
}

- (void)initialFirst:(NSString *)message{
    _setupDict = @{@"message":message, @"retry":@NO, @"close":@NO, @"activity": @YES};
    _retryButton.hidden = YES;
    _closeButton.hidden = YES;
}

-(void)updateButton {
    if (![_retryButton isHidden]&&![_closeButton isHidden]) {
        // NSLog(@"updated button");
        _retryButton.center = CGPointMake(self.boxView.frame.size.width/2.0 + _retryButton.frame.size.width*0.6,
                                          _retryButton.center.y);
        _closeButton.center = CGPointMake(self.boxView.frame.size.width/2.0 - _closeButton.frame.size.width*0.6,
                                          _closeButton.center.y);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^(void) {
        [self.delegate btnCloseDidClicked];
        //For block
        if (self.ringAction) {
            self.ringAction(self, TCAction_Close);
        }
    }];
    
}

- (IBAction)retry:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^(void) {
        [self.delegate btnRetryDidClicked];
        //For block
        if (self.ringAction) {
            self.ringAction(self, TCAction_Restry);
        }
    }];
}

#pragma mark - Using Block to show Popup
+ (void)showPopupFromVC:(UIViewController *)vc withMessage:(NSString *)message completionBlock:(TCAlertControlRingAction)completionBlock{
    _tcAlert = [[ProcessViewController alloc] initWithMessage:message];
    _tcAlert.view.bounds = vc.view.bounds;
    //Update outside VIEWDIDLOAD
    /*
     if ([_tcAlert isBeingPresented]) {
     //just update message again
     [_tcAlert initialFirst:message];
     return;
     }
     */
    [vc presentViewController:_tcAlert animated:YES completion:nil];
    _tcAlert.ringAction = completionBlock;
}

+ (void)updateError:(NSString *)errorMsg isShowCloseButton:(BOOL)isCloseShow isShowRestryButton:(BOOL)isRestryShow{
    if (_tcAlert) {
        _tcAlert.messageLabel.text = errorMsg;
        _tcAlert.retryButton.hidden = !isRestryShow;
        _tcAlert.closeButton.hidden = !isCloseShow;
        //Stop loading
        [_tcAlert.activityView stopAnimating];
    }
}

+ (void)updateStateMessage:(NSString *)message{
    if (_tcAlert) {
        _tcAlert.messageLabel.text = message;
    }
}

+ (void)dismiss{
    if (_tcAlert) {
        [_tcAlert dismissViewControllerAnimated:YES completion:nil];
        _tcAlert = nil;
    }
}

+ (void)dismssWithCompletion:(void (^)(void))completion{
    if (_tcAlert) {
        [_tcAlert dismissViewControllerAnimated:YES completion:completion];
        _tcAlert = nil;
    }
}
@end
