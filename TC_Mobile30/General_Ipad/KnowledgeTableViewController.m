//
//  KnowledgeTableViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/10/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "KnowledgeTableViewController.h"
#import "ATPAuthenticate.h"
#import "UserPrefConstants.h"
@interface KnowledgeTableViewController (){
     NSArray *keyArray;
}

@end

@implementation KnowledgeTableViewController
@synthesize tocDelegate;
@synthesize tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    id desiredColor = [UIColor darkTextColor];
    self.tableView.backgroundColor = desiredColor;
    self.tableView.backgroundView.backgroundColor = desiredColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"Technical Analysis";
            break;
        case 1:
            sectionName =  @"Charts";
            break;
        case 2:
            sectionName =  @"Chart Patterns";
            break;
        case 3:
            sectionName =  @"Candlestock Patterns";
            break;
        default:
            sectionName = @"Technical Analysis";
            break;
    }
    return sectionName;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return [self objectsForSection:0].count;
    }
    else if (section == 1) {
        return [self objectsForSection:1].count;
    }
    else if (section == 2) {
        return [self objectsForSection:2].count;
    }
    else if (section == 3) {
        return [self objectsForSection:3].count;
    }
    
    return 0;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;
}

- (NSArray *)objectsForSection:(int)section {
    if (section == 0) {
        return [NSArray arrayWithObjects:@"Dow Theory", @"Modern Portfolio Theory", @"Fundamental Analysis",@"Market Trend",@"Technical Analysis",@"Support and Resistance",@"Trend Line",@"Dead Cat Bounce",@"Elliott Wave Principle",@"Fibonacci Retracement",@"Pivot Point",nil];
    }
    else if (section == 1) {
        return [NSArray arrayWithObjects:@"Candlestick Charts",@"Chart Pattern",@"Open-high-low-close Chart",@"Line Chart",@"Point and Figure Chart",@"Kagi Chart",nil];
    }
    else if (section == 2) {
        return [NSArray arrayWithObjects:@"Head and Shoulders", @"Cup and Handle",@"Double top and double bottom",@"Triple top and triple bottom",@"Broadening top",@"Price channels",@"Wegde pattern",@"Triagle",@"Flag and pennant patterns",@"Gap",@"Island reversal",nil];
    }
    else if (section == 3) {
        return [NSArray arrayWithObjects:@"Advanced Candlestock Patterns", @"Doji",@"Hammer", @"Hanging Man", @"Inverted hammer", @"Shooting star", @"Marubozu",@"Spinning top",@"Three white soilders",@"Three Black Crows",@"Morning star",@"Hikkake pattern",nil];
    }
    return nil;
}

#pragma Tableview Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = 44.f;
    return cellHeight;
}

- (UILabel *)createLabelforNavBarWithTitle:(NSString *)title andFontSize:(CGFloat)fontSize {

    CGRect frame = CGRectMake(0, 0, 250, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = title;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont boldSystemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    
    return label;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   [[ATPAuthenticate singleton] timeoutActivity];
 
    if (indexPath.section == 0) {
        if (indexPath.row < [self objectsForSection:0].count) {
            
            [UserPrefConstants singleton].knowledgeTitle = [[self objectsForSection:0] objectAtIndex:indexPath.row];
            switch (indexPath.row) {
                case 0:
                     [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Dow_theory";
                    break;
                case 1:
                     [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Modern_portfolio_theory";
                    break;
                case 2:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Fundamental_analysis";
                    break;
                case 3:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Market_trend";
                    break;
                case 4:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Technical_analysis";
                    break;
                case 5:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Support_and_resistance";
                    break;
                case 6:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Trend_line_(technical_analysis)";
                    break;
                case 7:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Dead_cat_bounce";
                    break;
                case 8:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Elliott_wave_principle";
                    break;
                case 9:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Fibonacci_retracement";
                    break;
                case 10:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Pivot_point_(technical_analysis)";
                    break;
                default:
                    break;
            }
            
        }
    }
    else if (indexPath.section == 1) {
        if (indexPath.row < [self objectsForSection:1].count) {
                   [UserPrefConstants singleton].knowledgeTitle = [[self objectsForSection:1] objectAtIndex:indexPath.row];
            switch (indexPath.row) {
                case 0:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Candlestick_chart";
                    break;
                case 1:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Chart_pattern";
                    break;
                case 2:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Open-high-low-close_chart";
                    break;
                case 3:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Line_chart";
                    break;
                case 4:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Point_and_figure_chart";
                    break;
                case 5:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Kagi_chart";
                    break;
                default:
                    break;
            }
            
        }
    }
    else if (indexPath.section == 2) {
        if (indexPath.row < [self objectsForSection:2].count) {
                 [UserPrefConstants singleton].knowledgeTitle = [[self objectsForSection:2] objectAtIndex:indexPath.row];
            switch (indexPath.row) {
                case 0:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Head_and_shoulders_(chart_pattern)";
                    break;
                case 1:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Cup_and_handle";
                    break;
                case 2:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Double_top_and_double_bottom";
                    break;
                case 3:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Triple_top_and_triple_bottom";
                    break;
                case 4:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Broadening_top";
                    break;
                case 5:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Price_channels";
                    break;
                case 6:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Wedge_pattern";
                    break;
                case 7:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Triangle_(chart_pattern)";
                    break;
                case 8:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Flag_and_pennant_patterns";
                    break;
                case 9:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Gap_(chart_pattern)";
                    break;
                case 10:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Island_reversal";
                    break;
                default:
                    break;
            }
        }
    }
    else if (indexPath.section == 3) {
        if (indexPath.row < [self objectsForSection:3].count) {
                  [UserPrefConstants singleton].knowledgeTitle = [[self objectsForSection:3] objectAtIndex:indexPath.row];
            switch (indexPath.row) {
                case 0:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Candlestick_pattern";
                    break;
                case 1:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Doji";
                    break;
                case 2:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Hammer_(candlestick_pattern)";
                    break;
                case 3:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Hanging_man_(candlestick_pattern)";
                    break;
                case 4:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Inverted_hammer";
                    break;
                case 5:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Shooting_star_(candlestick_pattern)";
                    break;
                case 6:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Marubozu";
                    break;
                case 7:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Spinning_top_(candlestick_pattern)";
                    break;
                case 8:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Three_white_soldiers";
                    break;
                case 9:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Three_Black_Crows";
                    break;
                case 10:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Morning_star_(candlestick_pattern)";
                    break;
                case 11:
                    [UserPrefConstants singleton].knowledgeURL = @"https://en.wikipedia.org/wiki/Hikkake_pattern";
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadKnowledgeCentre" object:nil];

    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *SimpleCellIdentifier = @"Cell";
    UITableViewCell *simpleCell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:SimpleCellIdentifier];
    if (simpleCell == nil) {
        simpleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleCellIdentifier];
    }
    simpleCell.textLabel.backgroundColor = [UIColor darkTextColor];
    simpleCell.backgroundColor = [UIColor darkTextColor];
    simpleCell.textLabel.textColor = [UIColor whiteColor];
    simpleCell.textLabel.font =  [UIFont systemFontOfSize:16];
    simpleCell.textLabel.numberOfLines = 2;
    simpleCell.selectionStyle = UITableViewCellSelectionStyleBlue;
    [simpleCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    if (indexPath.section == 0) {
        if (indexPath.row < [[self objectsForSection:0] count]) {
            simpleCell.textLabel.text = (NSString *)[[self objectsForSection:0] objectAtIndex:indexPath.row];
        }
    }
    else if (indexPath.section == 1) {
        if (indexPath.row < [[self objectsForSection:1] count]) {
            simpleCell.textLabel.text = (NSString *)[[self objectsForSection:1] objectAtIndex:indexPath.row];
        }
    }
    else if (indexPath.section == 2) {
        if (indexPath.row < [[self objectsForSection:2] count]) {
            simpleCell.textLabel.text = (NSString *)[[self objectsForSection:2] objectAtIndex:indexPath.row];
        }
    }
    else if (indexPath.section == 3) {
        if (indexPath.row < [[self objectsForSection:3] count]) {
            simpleCell.textLabel.text = (NSString *)[[self objectsForSection:3] objectAtIndex:indexPath.row];
        }
    }
    
    return simpleCell;
}

#pragma mark -
#pragma mark UIScrollView delegates
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [[ATPAuthenticate singleton] timeoutActivity];
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
