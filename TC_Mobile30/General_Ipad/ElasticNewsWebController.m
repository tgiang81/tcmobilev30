//
//  ElasticNewsWebController.m
//  TCiPad
//
//  Created by n2n on 13/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "ElasticNewsWebController.h"

@interface ElasticNewsWebController ()

@end

@implementation ElasticNewsWebController

#pragma mark - View Lifecycles Delegates

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_newsWebView setHidden:YES];
    [_loadingActivity startAnimating];
    
    // load URL into webview
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Webview Delegates

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [_newsWebView setHidden:NO];
    [_loadingActivity stopAnimating];
    
}

@end
