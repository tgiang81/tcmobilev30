//
//  LabelEffect.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 29/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppControl.h"
typedef enum : NSUInteger {
    ST_LeftToRight,
    ST_RightToLeft,
    ST_AutoReverse,
    ST_ShimmerAll,
} EffectType;

@interface LabelEffect : UIView



@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) NSAttributedString *attributedText;
@property (assign, nonatomic) NSInteger numberOfLines;


@property (assign, nonatomic) EffectType effectType;
@property (assign, nonatomic) BOOL repeat;
@property (assign, nonatomic) CGFloat shimmerWidth;
@property (assign, nonatomic) CGFloat shimmerRadius;
@property (strong, nonatomic) UIColor *shimmerColor;
@property (assign, nonatomic) NSTimeInterval durationTime;
@property (assign, nonatomic) BOOL isPlaying;

- (void)startShimmer;
- (void)stopShimmer;   


@end
