//
//  NewsTableViewCell.h
//  TCiPad
//
//  Created by n2n on 17/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;
@property (weak, nonatomic) IBOutlet UILabel *newsDate;

@end
