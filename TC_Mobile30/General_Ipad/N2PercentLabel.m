//
//  N2PercentLabel.m
//  test
//
//  Created by Emir on 17/01/2017.
//  Copyright © 2017 n2n. All rights reserved.
//

#import "N2PercentLabel.h"

@implementation N2PercentLabel

@synthesize ratio = _ratio;
@synthesize color1 = _color1;
@synthesize color2 = _color2;


-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _ratio = 0.5;
        self.textAlignment = kCTTextAlignmentCenter;
    }
    return self;
}

//Setter methods
-(void)setRatio:(CGFloat)newRatio {
    
    _ratio = newRatio;
    [self setNeedsDisplay];
}

-(void)setColor1:(UIColor *)newColor1 {
    
    _color1 = newColor1;
    [self setNeedsDisplay];
}

-(void)setColor2:(UIColor *)newColor2 {
    
    _color2 = newColor2;
    [self setNeedsDisplay];
}




- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    CGRect firstRect = CGRectMake(0, 0, rect.size.width*_ratio, rect.size.height);
    CGContextSetFillColorWithColor(context, [_color1 CGColor]);
    CGContextFillRect(context, firstRect);
   
    CGRect secondRect = CGRectMake(rect.size.width*_ratio, 0, rect.size.width, rect.size.height);
    CGContextSetFillColorWithColor(context, [_color2 CGColor]);
    CGContextFillRect(context, secondRect);
    
    [super drawRect:rect];
}



@end
