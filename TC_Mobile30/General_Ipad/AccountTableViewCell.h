//
//  AccountTableViewCell.h
//  TCiPad
//
//  Created by n2n on 12/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserAccountClientData.h"
#import "UserPrefConstants.h"

@interface AccountTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *accountNumber;
@property (strong, nonatomic) IBOutlet UIView *selectedView;


-(void)populateItemsAtIndexPath:(NSIndexPath*)indexPath withArray:(NSMutableArray*)array;

@end
