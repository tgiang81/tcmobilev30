//
//  NSString+MD5.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/13/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(MD5)
- (NSString *)MD5;
@end
