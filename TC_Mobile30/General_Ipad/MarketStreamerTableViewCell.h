//
//  MarketStreamerTableViewCell.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 07/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketStreamerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *symbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *piLabel;
@property (weak, nonatomic) IBOutlet UILabel *lPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *lQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *lValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *changeLabel;
@property (weak, nonatomic) IBOutlet UILabel *changePercentageLabel;

@end
