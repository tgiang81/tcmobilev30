//
//  SelectorTableViewCell.m
//  TCiPad
//
//  Created by n2n on 11/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "SelectorTableViewCell.h"

@implementation SelectorTableViewCell

@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)expandTapped:(id)sender {
    
    [self.delegate expandBtnClicked:self.path];
}
@end
