//
//  N2DatePicker.h
//  TCiPad
//
//  Created by n2n on 16/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

// Created by Emir
// Purpose: a Datepicker that follows our language selection

// Key used in language.json = M-1 to M-12 for months naming.

#import <UIKit/UIKit.h>
#import "LanguageManager.h"

@protocol N2DatePickerDelegate <UIPickerViewDelegate>
-(void)n2datePickerSelected:(NSDate*)date;
@end

@interface N2DatePicker : UIPickerView 

@property (nonatomic, strong) NSMutableArray *dayArray, *monthArray, *yearArray;
@property (nonatomic, weak) id<N2DatePickerDelegate>myDelegate;
@property (nonatomic, strong) NSDate *selectedDate;

-(instancetype)initWithDate:(NSDate*)date;
-(void)setPickerDate:(NSDate*)date animated:(BOOL)animate;

@end
