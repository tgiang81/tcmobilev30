//
//  NewsMetadata.h
//  N2NTrader
//
//  Created by Adrian Lo on 4/9/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NewsMetadata : NSObject {
    NSString *exchange;
	NSString *stockcode;
	NSString *newsID;
	NSString *newsTitle;
	NSString *date1;
	NSString *date2;
	NSString *stockname;
	int cat;
    
    // for elastic news
    NSString *sourceNews;
    NSString *fullDate;
    NSString *newsDescription;
    NSString *codeID;
}

@property (nonatomic, strong) NSString *exchange;
@property (nonatomic, strong) NSString *stockcode;
@property (nonatomic, strong) NSString *newsID;
@property (nonatomic, strong) NSString *newsTitle;
@property (nonatomic, strong) NSString *date1;
@property (nonatomic, strong) NSString *date2;
@property (nonatomic, strong) NSString *stockname;
@property (nonatomic, assign) int cat;

@property (nonatomic, strong) NSString *sourceNews;
@property (nonatomic, strong) NSString *fullDate;
@property (nonatomic, strong) NSString *newsDescription;
@property (nonatomic, strong) NSString *codeID;

@end
