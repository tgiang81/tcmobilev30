//
//  ReleaseNotesView.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 12/31/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UIImage+ImageEffects.h"
#import "ReleaseNotesDownloadOperation.h"

@interface ReleaseNotesView : UIView

@property (assign, nonatomic) CGFloat overlayAlpha;
@property (assign, nonatomic) CGFloat textViewAlpha;
@property (strong, nonatomic) UIColor *textViewBackgroundColor;
@property (strong, nonatomic) UIColor *darkSeparatorColor;
@property (strong, nonatomic) UIColor *lightSeparatorColor;
@property (strong, nonatomic) UIColor *viewShadowColor;
@property (assign, nonatomic) CGSize viewShadowOffset;
@property (assign, nonatomic) CGFloat viewShadowRadius;
@property (assign, nonatomic) CGFloat viewShadowOpacity;
@property (strong, nonatomic) UIFont *titleFont;
@property (strong, nonatomic) UIColor *titleColor;
@property (strong, nonatomic) UIColor *titleShadowColor;
@property (assign, nonatomic) CGSize titleShadowOffset;
@property (strong, nonatomic) UIFont *releaseNotesFont;
@property (strong, nonatomic) UIColor *releaseNotesColor;
@property (strong, nonatomic) UIColor *releaseNotesShadowColor;
@property (assign, nonatomic) CGSize releaseNotesShadowOffset;
@property (strong, nonatomic) UIFont *closeButtonFont;
@property (strong, nonatomic) UIColor *closeButtonColor;
@property (strong, nonatomic) UIColor *closeButtonShadowColor;
@property (assign, nonatomic) CGSize closeButtonShadowOffset;

+ (ReleaseNotesView *)viewWithReleaseNotesTitle:(NSString *)releaseNotesTitle text:(NSString *)releaseNotesText closeButtonTitle:(NSString *)closeButtonTitle;


+ (void)setupViewWithAppIdentifier:(NSString *)appIdentifier releaseNotesTitle:(NSString *)releaseNotesTitle closeButtonTitle:(NSString *)closeButtonTitle completionBlock:(void (^)(ReleaseNotesView *releaseView, NSString *releaseNoteText, NSError *error))completionBlock;

+ (BOOL)isAppVersionUpdated;


+ (BOOL)isAppOnFirstLaunch;


- (void)showInView:(UIView *)containerView;

@end
