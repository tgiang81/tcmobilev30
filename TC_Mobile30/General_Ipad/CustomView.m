//
//  CustomView.m
//  TCiPad
//
//  Created by n2n on 23/11/2016.
//  Copyright © 2016 N2N. All rights reserved.
//


/*
 This customview is used to allow taps/interactions for objects
 outside the original view frame.
 
 Specifically used in this app to allow chart interaction when it is
 enlarged bigger than the StockPreViewController view and its ContainerView
 
 Both StockPreViewController's view and its ContainerView should be a subclass of this class.
 */

#import "CustomView.h"

@implementation CustomView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (!self.clipsToBounds && !self.hidden && self.alpha > 0) {
        for (UIView *subview in self.subviews.reverseObjectEnumerator) {
            CGPoint subPoint = [subview convertPoint:point fromView:self];
            UIView *result = [subview hitTest:subPoint withEvent:event];
            if (result != nil) {
               
                return result;
            }
        }
    }
    
    return nil;
}

@end
