//
//  PricePadViewController.m
//  TCiPad
//
//  Created by n2n on 20/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "PricePadViewController.h"

@interface PricePadViewController ()

@end

@implementation PricePadViewController

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for(UIView *v in [self.view subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = 5.0;
            btn.layer.masksToBounds = YES;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnPressed:(id)sender {
    [self.delegate priceBtnPressed:sender];
}
@end
