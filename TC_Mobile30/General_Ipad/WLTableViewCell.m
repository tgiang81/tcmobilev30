//
//  WLTableViewCell.m
//  TCiPad
//
//  Created by n2n on 21/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "WLTableViewCell.h"

@implementation WLTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
