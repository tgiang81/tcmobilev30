//
//  KnowledgeDetailViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 11/10/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "KnowledgeDetailViewController.h"
#import "UserPrefConstants.h"
@interface KnowledgeDetailViewController ()

@end

@implementation KnowledgeDetailViewController

- (void) reloadKnowledgeCentre{
      [self reloadPage:[UserPrefConstants singleton].knowledgeURL andTitle:[UserPrefConstants singleton].knowledgeTitle];
}

- (void)viewDidLoad {
    [super viewDidLoad];
     NSNotificationCenter *notCenter = [NSNotificationCenter defaultCenter];
     [notCenter addObserver:self selector:@selector(reloadKnowledgeCentre)       name:@"loadKnowledgeCentre" object:nil];
    
   
    // Do any additional setup after loading the view.
    
    [_backButton setHidden:YES];
}

- (IBAction)smallerFont:(id)sender {
    
    UserPrefConstants *UPC = [UserPrefConstants singleton];
    
    UPC.newsFontSize = (UPC.newsFontSize > 50) ? UPC.newsFontSize -5 : UPC.newsFontSize;
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                          (unsigned long)UPC.newsFontSize];
    [_webview stringByEvaluatingJavaScriptFromString:jsString];
}

- (IBAction)biggerFont:(id)sender {
    
    UserPrefConstants *UPC = [UserPrefConstants singleton];
    
    UPC.newsFontSize = (UPC.newsFontSize < 160) ? UPC.newsFontSize +5 : UPC.newsFontSize;
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%lu%%'",
                          (unsigned long)UPC.newsFontSize];
    [_webview stringByEvaluatingJavaScriptFromString:jsString];
}

- (IBAction)clickBackButton:(id)sender{
    if ([_webview canGoBack] ) {
        [_webview goBack];
    }
}

- (void) reloadPage:(NSString *)urlLink andTitle:(NSString *)title{
    
    [_activityView startAnimating];
    _activityView.hidden = NO;
    
    [_titleView setText:title];
    
    NSString* js =
    @"var meta = document.createElement('meta'); " \
    "meta.setAttribute( 'name', 'viewport' ); " \
    "meta.setAttribute( 'content', 'width = device-width, initial-scale=1.0, minimum-scale=0.2, maximum-scale=5.0; user-scalable=1;' ); " \
    "document.getElementsByTagName('head')[0].appendChild(meta)";
    
    [_webview stringByEvaluatingJavaScriptFromString: js];
    NSLog(@"Url Link %@",urlLink);
    NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlLink] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval: 20.0];
    [_webview loadRequest:request];
    _webview.scrollView.bounces = NO;
    [_webview setScalesPageToFit:YES];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadPage:@"https://en.wikipedia.org/wiki/Dow_theory" andTitle:@"Technical Analysis"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_activityView stopAnimating];
    _activityView.hidden = YES;
    [_backButton setEnabled:[_webview canGoBack]];
    [_backButton setHidden:![_webview canGoBack]];
    // Do whatever you want here
}

#pragma mark -
#pragma mark - UIScrollView Delegate Methods

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale
{
    self.webview.scrollView.maximumZoomScale = 20; // set similar to previous.
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [_activityView stopAnimating];
    _activityView.hidden = YES;
    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[NSString stringWithFormat:@" Can't Load Page at this Moment. Please Check Back Later."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
     [alert show];*/
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [_activityView startAnimating];
    _activityView.hidden = NO;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    
    //return FALSE; //to stop loading
    //To avoid link clicking.
    /* if(navigationType ==UIWebViewNavigationTypeLinkClicked)
     return  NO;*/
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked ) { // open any link in safari app
        
        if ([request.URL.absoluteString rangeOfString:@"#"].location != NSNotFound) {
            return YES;
        } else {
            [[UIApplication sharedApplication] openURL:[request URL]];
            return NO;
        }
        
    }
    
    //To avoid form submission.
    
    if([request.URL.absoluteString rangeOfString:@"googleads"].location != NSNotFound)
        return NO;
    
    
    return YES;
}
- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loadKnowledgeCentre" object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
