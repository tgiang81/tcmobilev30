//
//  StockPlate.m
//  TCUniversal
//
//  Created by Scott Thoo on 10/1/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "StockFlag.h"
#import "UserPrefConstants.h"
#import "MarqueeLabel.h"
#import "UIImageUtility.h"
#import "AppControl.h"

@implementation StockFlag
@synthesize lblLastChange,lblLastDone,lblPercentage,lblStockName,lblVolume;
@synthesize stockCode;
@synthesize userPref = _userPref;

//Programmatically

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockFlag" owner:self options:nil];
        // 2. adjust bounds
        
        // self.bounds = self.view.bounds;
        // 3. Add as a subview
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        
        [self addSubview:self.view];
        
        
        //Adding dummy animation
        // int randNum = arc4random() % (10 - 3) + 3; // rand num btw 10 and 3;
        //[NSTimer scheduledTimerWithTimeInterval:randNum target:self selector:@selector(dummyFlipAnimation) userInfo:nil repeats:YES];
        [self dummyFlipAnimation];
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        
        // Scott
        // 1. Load the xibarc
        [[NSBundle mainBundle] loadNibNamed:@"StockFlag" owner:self options:nil];
        // 2. Add as a subview
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        [self addSubview:self.view];
    }
    
    return self;
}

- (id)initWithStockDetails:(NSString *)stkCode
                   stkName:(NSString *)stkName
             stkPercentage:(NSString *)stkPercentage
                 stkVolume:(NSString *)stkVolume
               stkLastDone:(NSString *)stkLastDone
             stkLastChange:(NSString *)stkLastChange
{
    self = [super initWithFrame:self.frame];
    if (self) {
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockFlag" owner:self options:nil];
        // 2. adjust bounds
        //NSLog(@"%f",[UIScreen mainScreen].bounds.size.width);
        self.view.frame =  (([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && ([UIScreen mainScreen].bounds.size.height == 1024 || [UIScreen mainScreen].bounds.size.width == 1366)))?CGRectMake(0, 0, 189, 80):CGRectMake(0, 0, 139, 51);
        
        UIFont *myFont = [UIFont boldSystemFontOfSize:16];
        UIFont *myFontBig = [UIFont boldSystemFontOfSize:20];
        
        UIFont *myFont2 = [UIFont systemFontOfSize:13];
        UIFont *myFontBig2 = [UIFont systemFontOfSize:17];
        
        UIFont *myFont2Bold = [UIFont boldSystemFontOfSize:13];
        UIFont *myFontBig2Bold = [UIFont boldSystemFontOfSize:17];
        
        self.bounds =self.view.bounds;
        
        lblVolume.text      = stkVolume;
        lblPercentage.text  = [NSString stringWithFormat:@"%.3f%%",[stkPercentage floatValue]];
        lblLastDone.text    = stkLastDone;
        lblLastChange.text  = stkLastChange;
        
        CGFloat lastChangeFloat = [stkLastChange floatValue];
        
        
        MarqueeLabel *movingLabel=[[MarqueeLabel alloc]initWithFrame:lblStockName.frame duration:2.0 andFadeLength:0];
        [lblStockName removeFromSuperview];
        movingLabel.text=stkName;
        
        
        
        
        if ([UserPrefConstants singleton].isBigIpad) {
            [movingLabel setFont:myFontBig];
            [lblVolume setFont:myFontBig2];
            [lblPercentage setFont:myFontBig2];
            [lblLastDone setFont:myFontBig2Bold];
            [lblLastChange setFont:myFontBig2];
        } else {
            [movingLabel setFont:myFont];
            [lblVolume setFont:myFont2];
            [lblPercentage setFont:myFont2];
            [lblLastDone setFont:myFont2Bold];
            [lblLastChange setFont:myFont2];
        }
        
        [self.view addSubview:movingLabel];
        
        self.view.layer.cornerRadius = kButtonRadius*self.frame.size.height;
        self.view.layer.masksToBounds = NO; // dont change to YES. It affects ticker performance
        self.view.backgroundColor = [UIColor clearColor];
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        ////NSLog(@"firstCharStockPercentage %@",firstCharStockPercentage);
        if(lastChangeFloat < 0)
        {
            ////NSLog(@"firstCharStockPercentage : %@",firstCharStockPercentage);
            self.view.layer.backgroundColor = kRedColorF.CGColor;//[self colorWithHexString:@"570307"]; //Dark Red
            self.imgFlag.image = [UIImage imageNamed:@"downFlag.png"];
            //self.lblVolume.textColor=[UIColor redColor];
            movingLabel.textColor=[UIColor colorWithRed:0.86 green:0.46 blue:0.46 alpha:1.00];
        }
        else if(lastChangeFloat>0)
        {
            self.view.layer.backgroundColor = kGreenColorF.CGColor;//[self colorWithHexString:@"112A11"]; //Dark Green
            self.imgFlag.image = [UIImage imageNamed:@"upFlag.png"];
            //self.lblVolume.textColor=[UIColor greenColor];
            lblLastChange.text = [NSString stringWithFormat:@"+%@",lblLastChange.text];
            lblPercentage.text = [NSString stringWithFormat:@"+%@",lblPercentage.text];
            movingLabel.textColor=[UIColor colorWithRed:0.36 green:0.54 blue:0.30 alpha:1.00];
        }
        else
        {
            self.view.layer.backgroundColor = kGrayColorF.CGColor;//[self colorWithHexString:@"363535"]; //Dark Gray
            self.imgFlag.image = nil; //[UIImage imageNamed:@"unChangeFlag.png"];
            // self.lblVolume.textColor=[UIColor grayColor];
            movingLabel.textColor=[UIColor colorWithRed:0.56 green:0.56 blue:0.56 alpha:1.00];;
        }
        
        // 3. Add as a subview
        [self addSubview:self.view];
        
        //Adding dummy animation
        //int randNum = arc4random() % (10 - 3) + 3; // rand num btw 10 and 3;
        //[NSTimer scheduledTimerWithTimeInterval:randNum target:self selector:@selector(dummyFlipAnimation) userInfo:nil repeats:YES];
        [self dummyFlipAnimation];
        
        //assigning stockcode to local
        self.stockCode = stkCode;
        
        //assigning selected stockCode to UserPref
        _userPref = [UserPrefConstants singleton];
        
        if ([stkName length]<=0)  {
            movingLabel.text= stkCode;
            lblVolume.text = @"NO DATA";
            lblPercentage.text  = @"";
            lblLastDone.text    = @"";
            lblLastChange.text  = @"";
        }
    }
    return self;
}

- (void)dummyFlipAnimation
{
    
    //    int randNum = arc4random() % (10 - 5) + 5;
    //
    //    if (randNum >5)
    //    {
    //        self.view.backgroundColor = [self colorWithHexString:@"112A11"]; //Dark Green
    //        self.imgFlag.image = [UIImage imageNamed:@"upFlag.png"];
    //    }
    //    else
    //    {
    //        self.view.backgroundColor = [self colorWithHexString:@"570307"]; //Dark Red
    //        self.imgFlag.image = [UIImage imageNamed:@"downFlag.png"];
    //    }
    //__weak StockFlag *weakSelf = self;
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void) {
                         self.view.transform = CGAffineTransformMakeScale(1, 0);
                     }
                     completion:^ (BOOL finished){
                         if (finished) {
                             [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                                              animations:^(void) {
                                                  self.view.transform = CGAffineTransformMakeScale(1, 1);
                                                  
                                              }
                                              completion:nil];
                         }
                     }];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (IBAction)btnPressed:(id)sender
{
    //    int rand = arc4random()%100;
    //    ////NSLog(@"StockId : %d",rand);
    
    if ([lblVolume.text isEqualToString:@"NO DATA"]) return;
    
    if ([stockCode length]<=0) return;
    
    _userPref.userSelectingStockCode = stockCode;
    ////NSLog(@"_userPref.userSelectingStockCode : %@",_userPref.userSelectingStockCode);
    
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:stockCode forKey:@"stkCode"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
    
    
}
@end
