//
//  QuoteScreenViewController.m
//  TCUniversal
//
//  Created by Scott Thoo on 11/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "QuoteScreenViewController.h"
#import "QuoteScreenTableViewCell.h"
#import "QCConstants.h"
#import "QCData.h"
#import "UserPrefConstants.h"
#import "AppConstants.h"
#import "ActionSheetStringPicker.h"
#import "VertxConnectionManager.h"
#import "UIImageUtility.h"
#import "MainFrameViewController.h"
#import "StockNameTableViewCell.h"
#import "ATPAuthenticate.h"
#import "AppControl.h"
#import "WLTableViewCell.h"
#import "CollectionReusableHeaderView.h"
#import "LanguageKey.h"

@interface QuoteScreenViewController () <TraderDelegate, TradeAccountDelegate>
{
    NSString *selectedStkCodeStr;
    NSString *selectedStkName;
    NSArray *stkCodeAndExchangeArr;
    TradingRules *trdRules;
    QCData *_qcData;

    int lotSizeStringInt;

    
    
    NSArray *sortedArr ;
    BOOL isDecending;
    NSString *sortedByFID;
    UserPrefConstants *_userPref;

    NSInteger selectedButtonTag;
    NSString *dynamicFID1;
    NSString *dynamicFID2;
    NSString *dynamicFID3;
    NSString *dynamicFID4;
    NSString *dynamicFID5;
    NSString *dynamicFID6;
    NSString *dynamicFID7;
    NSString *dynamicFID8;
    NSString *dynamicFID9;
    NSString *dynamicFID10;
    NSString *dynamicFID11;
    NSString *dynamicFID12;
    NSString *dynamicFID13;
    NSString *dynamicFID14;
    NSString *dynamicFID15;
    NSString *dynamicFID16;
    NSString *dynamicFID17;
    NSString *dynamicFID18;
    NSString *dynamicFID19;
    ATPAuthenticate *atp;
    BOOL isUpdate, isCollectionUpdate;
    BOOL showAlert;
    BOOL runOnce;
    BOOL canPull;
    BOOL isWatchList;
    int canLoadData;
    int sortRow;
    int watchListID;
    BOOL showList; // show or hide watchlist names
    BOOL collectionViewResized;
    
    StockPreViewController * previewVC;
    TraderViewController *tradeVC;

   // NSTimer *updateTimer;
    CGFloat scrollViewWidth;
    BOOL stkContainerDisplayed;
    NSString *stkSelected;
    long selectedRow, prevRow, selectedWLRow, prevWLRow;
    
    CGPoint containerViewOriPos;
    CGFloat bufferPreviewX;
    CGRect selfFrame;
    CGFloat scrollHigh;
    CGFloat oriTablePosX;
    CGFloat scrollContentWidth;
}

@end

@implementation QuoteScreenViewController

@synthesize tableView = _tableView;
@synthesize frontTableView = _frontTableView;
@synthesize lblDynamic1,lblDynamic2,lblDynamic3,lblDynamic4,lblDynamic5,loadingBtn;
@synthesize titleLabel;
@synthesize delegate;



#define kScrollContentHeight 572 // normal (with sort by buttons)
#define kScrollContentHeight2 590 // if no sort by buttons

#define kWatchListColor [UIColor colorWithRed:0.17 green:0.17 blue:0.17 alpha:1.00]
#define kFrontTableColor [UIColor colorWithRed:0.12 green:0.12 blue:0.12 alpha:1.00]
#define kTableColor [UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.00]



#pragma mark - GeneralSelectorDelegate 


-(void)selectorItemSelected:(NSIndexPath*)indexPath selectorType:(GeneralSelectorType)type {
    if (type==TYPE_MARKET) {
        
        SectorInfo *eachInfo = [_mktInfoArray objectAtIndex:indexPath.section];
        [UserPrefConstants singleton].selectedMarket = eachInfo.sectorCode;
        
        [_boardLotButton setTitle:[LanguageManager stringForKey:[NSString stringWithFormat:@"%@",eachInfo.sectorName]] forState:UIControlStateNormal];
        
        selectedRow = -1;
        [self openStockContainer:NO animated:NO];
        [self executeVertxSort];
        
    } else if (type==TYPE_SECTOR) {

        SectorInfo *eachInfo = [_sectorInfoArray objectAtIndex:indexPath.section];

        if (indexPath.row>0) {
            // subitem selected
            SectorInfo *subInfo = [eachInfo.subItemsArray objectAtIndex:indexPath.row-1];
            [UserPrefConstants singleton].sectorToFilter = subInfo;
            selectedRow = -1;
            [self openStockContainer:NO animated:NO];
            [self executeVertxSort];
            [_sectorButton setTitle:[LanguageManager stringForKey:@"Sector:%@" withPlaceholders:@{@"%sectorName%":subInfo.sectorName}] forState:UIControlStateNormal];
            
        } else {
            // parent selected
            if ([eachInfo.sectorName isEqualToString:@"All"]) {
                [UserPrefConstants singleton].sectorToFilter = nil;
                 selectedRow = -1;
                [self openStockContainer:NO animated:NO];
                [self executeVertxSort];
            } else {
                [UserPrefConstants singleton].sectorToFilter = eachInfo;
                selectedRow = -1;
                [self openStockContainer:NO animated:NO];
                [self executeVertxSort];
            }
            [_sectorButton setTitle:[LanguageManager stringForKey:@"Sector:%@" withPlaceholders:@{@"%sectorName%":eachInfo.sectorName}] forState:UIControlStateNormal];
            
        }
    } else    if (type==TYPE_WLIST) {
        SectorInfo *eachWL = [self.sortedWLArr objectAtIndex:indexPath.section];
        //        NSLog(@"WL: %@",eachWL.sectorName);
        
        [atp addWatchlistCheckItem:[eachWL.sectorCode intValue]  withItem:_stkToAddToWL];
    }
}




#pragma mark - View LifeCycles Delegates


//UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Market"]
//                                                                         message:[LanguageManager stringForKey:@"Please select..."]
//                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
//
//// get board lot from current exchangeinfo
//
//for(int i =0;i<[_mktInfoArray count];i++)
//{
//    
//    MarketInfo *eachInf = [_mktInfoArray objectAtIndex:i];
//    UIAlertAction *watchListAction =
//    [UIAlertAction actionWithTitle:eachInf.marketName
//                             style:UIAlertActionStyleDefault
//                           handler:^(UIAlertAction *action)
//     {
//         [UserPrefConstants singleton].selectedMarket = eachInf.marketCode;
//         //NSLog(@"BoardLOT: %@",  [UserPrefConstants singleton].selectedMarket);
//         [_boardLotButton setTitle:[LanguageManager stringForKey:@"Market:%@" withPlaceholders:@{@"%mktName%":eachInf.marketName}] forState:UIControlStateNormal];
//         
//         selectedRow = -1;
//         [self openStockContainer:NO animated:NO];
//         [self executeVertxSort];
//         
//     }];
//    
//    [alertController addAction:watchListAction];
//}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString: @"segueAddToWatchlist2"]) {
        
        long cnt = [UserPrefConstants singleton].userWatchListArray.count;
        NSMutableArray *wlArray = [[NSMutableArray alloc] init];
        for (int i=0; i<cnt; i++) {
            SectorInfo *inf = [[SectorInfo alloc] init];
            NSDictionary *eachWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:i];
            inf.sectorCode = [eachWL objectForKey:@"FavID"];
            inf.sectorName = [eachWL objectForKey:@"Name"];
            [wlArray addObject:inf];
        }
        
        // sort
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sectorName"
                                                     ascending:YES];
        self.sortedWLArr = [wlArray sortedArrayUsingDescriptors:@[sortDescriptor]];
        
        GeneralSelectorViewController *vc = (GeneralSelectorViewController*)segue.destinationViewController;
        vc.selectorArray = [[NSMutableArray alloc] initWithArray:_sortedWLArr];
        vc.delegate = self;
        vc.selectorType = TYPE_WLIST;
        
    }
    
    if ([segue.identifier isEqualToString: @"embedPreview"]) {
        previewVC = (StockPreViewController *) [segue destinationViewController];
        previewVC.controller = @"QuoteScreen";
        self.delegate = previewVC;
        previewVC.delegate = self;
        
        //Click trade fom StockPreview

        [previewVC doBlock:^(int iSelect){
            //set position
            
            _traderContainer.frame = CGRectMake(self.view.frame.size.width-2*_stockContainerView.frame.size.width,
                                                   _myScrollView.frame.origin.y,
                                                   _stockContainerView.frame.size.width,
                                                   _stockContainerView.frame.size.height);

            
            
            [self executeTrade:iSelect];
            
        }];
    }

    if ([segue.identifier isEqualToString:@"tradeAccountSegueID3"]) {
        TradeAccountController *vc = (TradeAccountController*)segue.destinationViewController;
        vc.delegate = self;
    }

    if ([segue.identifier isEqualToString: @"embedOrderPadResize"]) {
        tradeVC = (TraderViewController*)segue.destinationViewController;
//        tradeVC.containerFrame = _traderContainer.frame;
        //NSLog(@"Stock Lot Size %d",lotSizeStringInt);
//        tradeVC.stockLotSize = lotSizeStringInt;
        tradeVC.delegate = self;
        tradeVC.parentController = self;
    }

    if ([segue.identifier isEqualToString: @"segueMarketSelector"]) {
        GeneralSelectorViewController *vc = (GeneralSelectorViewController*)segue.destinationViewController;
        
      //  NSLog(@"_mktInfoArray %@",_mktInfoArray);
        vc.selectorArray = [[NSMutableArray alloc] initWithArray:_mktInfoArray];
        vc.delegate = self;
        vc.selectorType = TYPE_MARKET;
        vc.selectedItem = @"Normal Board Lot";
        for(int i =0;i<[_mktInfoArray count];i++)
        {
            SectorInfo *eachInf = [_mktInfoArray objectAtIndex:i];
            
            if ([eachInf.sectorCode isEqualToString:[UserPrefConstants singleton].selectedMarket]) {
                vc.selectedItem = eachInf.sectorName;
            }
        }
    }
    
    if ([segue.identifier isEqualToString: @"segueSectorSelector"]) {
        GeneralSelectorViewController *vc = (GeneralSelectorViewController*)segue.destinationViewController;
        //NSLog(@"_sectorInfoArray %@",_sectorInfoArray);
        vc.selectorArray = [[NSMutableArray alloc] initWithArray:_sectorInfoArray];
        vc.delegate = self;
        vc.selectorType = TYPE_SECTOR;
        vc.selectedItem = [UserPrefConstants singleton].sectorToFilter.sectorName;
        
        if ( [UserPrefConstants singleton].sectorToFilter==nil) vc.selectedItem = @"All";
       
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setScrollViewContentSize {
    
    scrollContentWidth = (isWatchList)?1150:1150;
    
    [_myScrollView setContentSize:CGSizeMake(scrollContentWidth, scrollHigh)];
    [self.view bringSubviewToFront:_frontTableView];
    [self.view bringSubviewToFront:_stockContainerView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[UserPrefConstants singleton] overrideChartSettings];
    // NSLog(@"currentExchangeInfo %@",[UserPrefConstants singleton].currentExchangeInfo);
    
    _userPref = [UserPrefConstants singleton];
    if (_userPref.fromWatchlist) isWatchList = YES; else isWatchList = NO;
    
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    // Code below only for iOS9+
   // UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*) _cardCollectionView.collectionViewLayout;
   // layout.sectionHeadersPinToVisibleBounds = YES; // make collectionview header sticky to top
    
    self.detailPageDict = [[NSMutableDictionary alloc] init];
    
    _qcData = [QCData singleton];
    atp = [ATPAuthenticate singleton];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    showList = YES;
    collectionViewResized = NO;
    
    runOnce = YES;
    selectedRow = -1;
    prevRow = -1;
    
    selectedWLRow = -1;
    prevWLRow = -1;
    canLoadData = kDataLoadCannot;
    
    
    _frontTableView.delegate = self;
    _frontTableView.dataSource = self;
    
    scrollHigh = kScrollContentHeight;
    
    _frontTableView.separatorColor = [UIColor colorWithRed:255 green:242 blue:13 alpha:1.0];
    [self setScrollViewContentSize];
    
    
    //NSLog(@"Loaded QouteScreen");
    
    //Dynamically call
    _myScrollView.delegate = self;
    _tableView.separatorColor = [UIColor colorWithRed:255 green:242 blue:13 alpha:1.0];
    NSString *iosversion = [[UIDevice currentDevice] systemVersion];
    int version = [iosversion intValue];
    if(version>6)
    {
        _frontTableView.separatorInset = UIEdgeInsetsZero;
        _tableView.separatorInset = UIEdgeInsetsZero;
    }
    
    self.rankedByNameArr = [[NSMutableArray alloc] init];
    self.rankedByKeyArr = [[NSMutableArray alloc] init];
    for (int i=0; i<[[AppConstants SortByListArray] count]; i++) {
        NSDictionary *eachDict = (NSDictionary*)[[AppConstants SortByListArray] objectAtIndex:i];
        [self.rankedByNameArr addObject:[[eachDict allValues] objectAtIndex:0]];
    }
    for (int i=0; i<[[AppConstants SortByListArray] count]; i++) {
        NSDictionary *eachDict = (NSDictionary*)[[AppConstants SortByListArray] objectAtIndex:i];
        [self.rankedByKeyArr addObject:[[eachDict allKeys] objectAtIndex:0]];
    }
    
    self.bufferUpdateRow = [[NSMutableArray alloc] init];
    self.bufferUpdateStocks = [[NSMutableArray alloc] init];
    
    // Data initial sort
    sortedByFID = FID_101_I_VOLUME;
    sortRow = 0;
    
    // THESE ARE FIXED THO. DO NOT change them.!!!!!!
    dynamicFID1 =FID_101_I_VOLUME;
    dynamicFID2 =FID_56_D_HIGH_PRICE;
    dynamicFID3 =FID_57_D_LOW_PRICE;
    dynamicFID4 =FID_98_D_LAST_DONE_PRICE;
    dynamicFID5 =FID_55_D_OPEN_PRICE;
    dynamicFID6 =FID_123_D_PAR_VALUE;
    dynamicFID7 =FID_127_D_EPS;
    dynamicFID8 =FID_CUSTOM_F_CHANGE_PER;
    dynamicFID9 =FID_CUSTOM_F_CHANGE;
    dynamicFID10 =FID_78_I_SELL_QTY_1;
    dynamicFID11 =FID_88_D_SELL_PRICE_1;
    dynamicFID12 =FID_68_D_BUY_PRICE_1;
    dynamicFID13 =FID_58_I_BUY_QTY_1;
    dynamicFID14 =FID_50_D_CLOSE;
    dynamicFID15 =FID_153_D_THEORETICAL_PRICE;
    dynamicFID16 = FID_156_S_TRD_PHASE;
    dynamicFID17 = FID_102_D_VALUE;
    dynamicFID18 = FID_131_S_EXCHANGE;
    dynamicFID19 = FID_135_S_REMARK;
    
    [_btnCompactView setSelected:[UserPrefConstants singleton].isCollectionView];
    isDecending = YES;
    [_btnCompactView setImage:[UIImage imageNamed:@"MenuGrid"] forState:UIControlStateNormal];
    [_btnCompactView setImage:[UIImage imageNamed:@"menuSide"] forState:UIControlStateSelected];
    
    self.filteredDict = [[NSMutableDictionary alloc] init];
    self.tempDict=[[NSMutableDictionary alloc] init];
    ////NSLog(@"viewDidLoad [[QCData singleton] qcFeedDataDict] : %@",[[QCData singleton] qcFeedDataDict]);
    
    isUpdate=NO;
    isCollectionUpdate = NO;
    
    
    // add 3 finger gesture to dismiss stk preview
    UISwipeGestureRecognizer *threeFingerSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeStockPreview:)];
    threeFingerSwipe.delegate = self;
    threeFingerSwipe.numberOfTouchesRequired = 3;
    [self.view addGestureRecognizer:threeFingerSwipe];
}

#pragma mark - TapGesture delegates

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    return YES;
}


-(void)closeStockPreview:(UIGestureRecognizer*)recognizer {
    
    [self openStockContainer:NO animated:YES];
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
//    if (updateTimer!=nil) {
//        [updateTimer invalidate];
//        updateTimer = nil;
//    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"halfSecondTimerTick" object:nil];
  
    
    if (self.observingContentOffset) {
        
        for(UITableView *view in self.tableViewCollection)
        {
            [view removeObserver:self forKeyPath:@"contentOffset"];
        }
        self.observingContentOffset = NO;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GetStockCategoryReceived" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedSortByServer" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didReceivedWatchListItems" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"deleteWatchlistItemSuccessfully" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addOrUpdateWatchlistNameSuccessfully" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"getWatchlistSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"watchlistDeleted" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"watchListAddThisStock" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addWatchListItemsReply" object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_traderContainer setHidden:YES];

    if (!_userPref.fromWatchlist) {
        [_customPullActivity stopAnimating];
        
        if ([UserPrefConstants singleton].isCollectionView) {
            [_tableView setBackgroundView:nil];
            [_cardCollectionView setBackgroundView:_customPullView];
        } else {
            [_cardCollectionView setBackgroundView:nil];
            [_tableView setBackgroundView:_customPullView];
        }
        [_customPullView setBackgroundColor:[UIColor clearColor]];
        //        if ([UserPrefConstants singleton].quoteScrPage==0) _customPullContainer.hidden = YES;
        //        else _customPullContainer.hidden = NO;
    }
}

- (void) viewWillAppear:(BOOL)animated
{  // [self.view addSubview:loadingBtn];
    
    [super viewWillAppear:animated];
    [atp timeoutActivity];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdate:) name:@"didUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedSortByServer:) name:@"didFinishedSortByServer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceivedWatchListItems:) name:@"didReceivedWatchListItems" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteWatchlistItemSuccessfully:)  name:@"deleteWatchlistItemSuccessfully" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addOrUpdateWatchlistNameSuccessfully) name:@"addOrUpdateWatchlistNameSuccessfully" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestWatchListReceived) name:@"getWatchlistSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteWatchListSuccessful)  name:@"watchlistDeleted" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(watchListAddThisStock:)  name:@"watchListAddThisStock" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addWatchListItemsReply:) name:@"addWatchListItemsReply" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCellWithCategory:) name:@"GetStockCategoryReceived" object:nil];
    
    
    
    if (!self.observingContentOffset) {
        for(UITableView *view in self.tableViewCollection)
        {
            if (view.tag<12) // tag12 is watchlist table.
                [view addObserver:self
                       forKeyPath:@"contentOffset"
                          options:NSKeyValueObservingOptionNew
                          context:NULL];
        }
        self.observingContentOffset = YES;
    }
    
    
}

-(void)updateCellWithCategory:(NSNotification*)notification {
    NSMutableDictionary *dict = (NSMutableDictionary*)[notification object];
    
//
//    [_bufferUpdateRow enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        NSIndexPath *eachPath = (NSIndexPath*)obj;
//        if ([_tableView.indexPathsForVisibleRows containsObject:eachPath]) {
//            isUpdate=YES;
//            [_tableView reloadRowsAtIndexPaths:@[eachPath] withRowAnimation:UITableViewRowAnimationNone];
//        }
//    }];
    
    
    

    int rowCount = (int)[_tableView numberOfRowsInSection:0];

    NSMutableArray *keyObject = [[NSMutableArray alloc] init];
    for (NSString* key in dict) {
        NSMutableDictionary *keyDict = [dict objectForKey:key];
        for(int x=0;x<rowCount;x++){
            NSIndexPath *row = [NSIndexPath indexPathForRow:x inSection:0];
            QuoteScreenTableViewCell *cell = [_tableView cellForRowAtIndexPath:row];
                NSArray *stkNameDivide = [key componentsSeparatedByString:@"."];
           
            if ([[stkNameDivide objectAtIndex:0] isEqualToString:cell.stkName.text]) {
                
                NSString *categoryString = [keyDict objectForKey:FID_135_S_REMARK]==NULL ? @"":[keyDict objectForKey:FID_135_S_REMARK];
                NSLog(@"categoryString %@",categoryString);
                cell.lblCategory.text = categoryString;

                [keyObject addObject:keyDict];
                break;
            }
            
        }
    }
    
    [keyObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSIndexPath *eachPath = (NSIndexPath*)obj;
                if ([_tableView.indexPathsForVisibleRows containsObject:eachPath]) {
                    isUpdate=YES;
                    [_tableView reloadRowsAtIndexPaths:@[eachPath] withRowAnimation:UITableViewRowAnimationNone];
                }
    }];
        
    
   

    
   //  NSDictionary *detailDict = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
    
   // UITableViewCell *cell = [_frontTableView cellForRowAtIndexPath:i]
    
}

-(void)viewDidLayoutSubviews {
    
    if (runOnce) {
        
        // NSLog(@"didlayout");
        
        
        
        runOnce = NO;
        scrollViewWidth = _myScrollView.frame.size.width*(1024.0/977.0);
        
        [_stockContainerView setHidden:YES];
        selfFrame = self.view.frame;
        _sortBtnContainer.frame = CGRectMake(0, _sortBtnContainer.frame.origin.y,
                                             self.view.frame.size.width,
                                             _sortBtnContainer.frame.size.height);
        [self quoteScreenInitialSetup];
        // resize and reposition tableView and frontTableView
        
        _sectorButton.layer.cornerRadius = kButtonRadius*_sectorButton.frame.size.height;
        _sectorButton.layer.masksToBounds = YES;
        _boardLotButton.layer.cornerRadius = kButtonRadius*_boardLotButton.frame.size.height;
        _boardLotButton.layer.masksToBounds = YES;
        _warrantButton.layer.cornerRadius = kButtonRadius*_warrantButton.frame.size.height;
        _warrantButton.layer.masksToBounds = YES;
        
        oriTablePosX = _myScrollView.frame.origin.x;
        
        if (!canPull) { // ie. watchlist
            _frontTableView.frame = CGRectMake(_watchlistTable.frame.size.width, _sortBtnContainer.frame.origin.y,
                                               _frontTableView.frame.size.width,
                                               _frontTableView.frame.size.height+_sortBtnContainer.frame.size.height);
            
            _myScrollView.frame = CGRectMake(_myScrollView.frame.origin.x+_watchlistTable.frame.size.width, _sortBtnContainer.frame.origin.y,
                                             _myScrollView.frame.size.width-_watchlistTable.frame.size.width,
                                             _myScrollView.frame.size.height+_sortBtnContainer.frame.size.height);
            
            _cardCollectionView.frame = CGRectMake(_cardCollectionView.frame.origin.x+_watchlistTable.frame.size.width, _sortBtnContainer.frame.origin.y,
                                                   _cardCollectionView.frame.size.width-_watchlistTable.frame.size.width,
                                                   _cardCollectionView.frame.size.height+_sortBtnContainer.frame.size.height);
            
            _watchlistTable.frame = CGRectMake(_watchlistTable.frame.origin.x, _frontTableView.frame.origin.y,
                                               _watchlistTable.frame.size.width,
                                               _frontTableView.frame.size.height);
            
        }
        
        loadingBtn.center = _cardCollectionView.center;
        _noResultsMsgBtn.center = _cardCollectionView.center;
        
        [self openStockContainer:NO animated:NO];
        
        [self createSortButtons];
        [self updateSortButtons];
        
        
    }
    
    
    
    
}


-(void) btnSortDidClicked:(UIButton*)sender {
    [atp timeoutActivity];
    int selectedIndex = (int)sender.tag - 1000;
    sortRow = selectedIndex;
    // selectedRow = -1;
    [self openStockContainer:NO animated:NO];
    [self executeVertxSort];

    [self updateSortButtons];
}


-(void)updateSortButtons {
    
    for (long i=0; i<[_rankedByNameArr count]; i++) {
        UIButton *btn = (UIButton*)[_sortBtnContainer viewWithTag:i+1000];
        [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    }
    
    UIButton *btn2 = (UIButton*)[_sortBtnContainer viewWithTag:sortRow+1000];
    [btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}




-(void)createSortButtons {
    
    CGFloat gap = 5;
    CGFloat totBtnWid = _sortBtnContainer.frame.size.width - gap*([_rankedByNameArr count]-1);
    CGFloat btnWidth = totBtnWid / [_rankedByNameArr count];
    CGFloat offX = 0;
    for (long i=0; i<[_rankedByNameArr count]; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 1000+i;
        NSString *str = [LanguageManager stringForKey:[_rankedByNameArr objectAtIndex:i]];
        [btn setTitle:str forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnSortDidClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
        btn.backgroundColor = kPanelColor;
        [btn setFrame:CGRectMake(offX, 0, btnWidth, _sortBtnContainer.frame.size.height-10)];
        btn.center = CGPointMake(btn.center.x, _sortBtnContainer.frame.size.height/2.0);
        [_sortBtnContainer addSubview:btn];
        
        btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
        btn.layer.masksToBounds = YES;
        
        offX += gap+btnWidth;
    }
    
}



-(void)setQuoteScreenTitle:(NSInteger)total {
    
    int recNum = 1+([UserPrefConstants singleton].quoteScrPage)*20;
    
    NSString *fromRecStr = [NSString stringWithFormat:@"%d",recNum];
    NSString *toRecStr = [NSString stringWithFormat:@"%d",recNum+19];

    NSString *newTitle = [LanguageManager stringForKey:@"Quote Screen [Record %d - %d]" withPlaceholders:@{@"%fromRecord%":fromRecStr,@"%toRecord%":toRecStr}];

    NSUInteger idx = [newTitle rangeOfString:@"["].location;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:newTitle];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor cyanColor] range:NSMakeRange(idx,[newTitle length]-idx)];
    [titleLabel setAttributedText:string];
}


#pragma mark - Initial Setup (QuoteScr Mode or Watchlist Mode)


- (BOOL)doArraysContainTheSameObjects:(NSArray *)firstArray withArray:(NSArray *)secondArray {
    BOOL arraysContainTheSameObjects = YES;
    
    for (id myObject in firstArray) {
        if (![secondArray containsObject:myObject]) {
            // We have found an object that is not in the other array.
            arraysContainTheSameObjects = NO;
            break;
        }
    }
    return arraysContainTheSameObjects;
}

-(void)quoteScreenInitialSetup {
    
   // NSLog(@"Quote screen init setup");
    
    [UserPrefConstants singleton].currentExchangeInfo = [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:[UserPrefConstants singleton].userCurrentExchange];
    
    
    [UserPrefConstants singleton].quoteScrPage = 0;

    
    [_noResultsMsgBtn setHidden:YES];
    [loadingBtn startAnimating];
    
    [_showListBtn setHidden:YES];
    _stockTitle.frame = CGRectMake(_showListBtn.frame.origin.x, _stockTitle.frame.origin.y,
                                   _stockTitle.frame.size.width, _stockTitle.frame.size.height);
    
    
    // MARK: WatchList Screen Setup
    
    
    
    if (_userPref.fromWatchlist) {
        
        [UserPrefConstants singleton].collectionViewHeaderMode = WATCHLIST_NONHIDDENLIST;
        [_mktSectorGroup setHidden:YES];

        
        canPull = NO;
        [_hideWLbutton setHidden:NO];
        [titleLabel setFrame:CGRectMake(_hideWLbutton.frame.origin.x+_hideWLbutton.frame.size.width,
                                        titleLabel.frame.origin.y,
                                        titleLabel.frame.size.width,
                                        titleLabel.frame.size.height)];
        
        
        [self latestWatchListReceived];
        
        if ([_watchListArray count]<=0) {
            
            [titleLabel setText:[LanguageManager stringForKey:_WatchList]];
            
        } else {
            
            [titleLabel setText: [LanguageManager stringForKey:@"WatchList: %@" withPlaceholders:@{@"%WLName%":[UserPrefConstants singleton].watchListName}]];
            
        }
        
        [_sortBtnContainer setHidden:YES];
        _userPref.fromWatchlist=NO;
        sortedByFID = FID_101_I_VOLUME;
        
        scrollHigh = kScrollContentHeight2;
        _stockContainerView.frame = CGRectMake(_stockContainerView.frame.origin.x,
                                               _sortBtnContainer.frame.origin.y,
                                               _stockContainerView.frame.size.width,
                                               _stockContainerView.frame.size.height+_sortBtnContainer.frame.size.height);
        
    } else{
        // MARK: Normal Quote Screen Setup
        
        if ([UserPrefConstants singleton].enableWarrant) {
            [_warrantButton setTitle:[LanguageManager stringForKey:@"Show All"] forState:UIControlStateNormal];
            
        } else {
            [_warrantButton setTitle:[LanguageManager stringForKey:@"Show Warrant"] forState:UIControlStateNormal];
        }
        
        
        [UserPrefConstants singleton].collectionViewHeaderMode = QUOTESCREEN;
        [_mktSectorGroup setHidden:NO];

        ExchangeInfo *currInfo = [UserPrefConstants singleton].currentExchangeInfo;
        // Update Market Info indicator button (all stocks/odd lot/ etc)
        //NSLog(@"ExchgInfo %@", currInfo);
        
        // LOAD SECTOR INFO
        self.sectorInfoArray = [[NSMutableArray alloc] init];
        NSArray *sectArr = [currInfo.sectorParents allKeys];
        
        //NSLog(@"sectArr %@", sectArr);
        
        for (int i=0; i<[sectArr count]; i++) {
            NSString *key = [sectArr objectAtIndex:i];
            SectorInfo *inf = [[SectorInfo alloc] init];
            inf.sectorCode = key;
            inf.sectorName = [currInfo.sectorInfos objectForKey:key];
            inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"[" withString:@""];
            inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"]" withString:@""];
            
            if ([inf.sectorName isEqualToString:@"TOTAL"]) inf.sectorName = @"All";
            
            NSString *parent = [currInfo.sectorParents objectForKey:key];
            
            // add parents only
            if ([parent isEqualToString:@"0"])
                [_sectorInfoArray addObject:inf];
        };
        
        
        // then populate childrens
        for (int i=0; i<[_sectorInfoArray count]; i++) {
            SectorInfo *inf = [_sectorInfoArray objectAtIndex:i];
            
            for (int j=0; j<[sectArr count]; j++) {
                NSString *key = [sectArr objectAtIndex:j];
                NSString *parent = [currInfo.sectorParents objectForKey:key];
                
               // NSLog(@"parent %@ %@ %@",parent, key, inf.sectorCode);
                
                if ([parent isEqualToString:inf.sectorCode]) {
                    SectorInfo *subInf = [[SectorInfo alloc] init];
                    subInf.sectorName = [currInfo.sectorInfos objectForKey:key];
                    subInf.sectorCode = key;
                    [inf.subItemsArray addObject:subInf];
                    [_sectorInfoArray replaceObjectAtIndex:i withObject:inf];
                }
            }
        }
        
        // if sectorInfoArray still empty, add default value.
        if ([_sectorInfoArray count]<=0) {
            SectorInfo *subInf = [[SectorInfo alloc] init];
            subInf.sectorCode = @"10000";
            subInf.sectorName = @"All";
            [_sectorInfoArray addObject:subInf];
        }
        
        [_sectorInfoArray sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sectorCode" ascending:YES]]];
        
        //NSLog(@"sectorToFilter %@",[UserPrefConstants singleton].sectorToFilter);
        
        if ([UserPrefConstants singleton].sectorToFilter!=nil)
            [_sectorButton setTitle:[LanguageManager stringForKey:@"Sector:%@" withPlaceholders:@{@"%sectorName%":[LanguageManager stringForKey:[UserPrefConstants singleton].sectorToFilter.sectorName]}] forState:UIControlStateNormal];
        else
            [_sectorButton setTitle:[LanguageManager stringForKey:@"Sector:%@" withPlaceholders:@{@"%sectorName%":[LanguageManager stringForKey:@"All"]}] forState:UIControlStateNormal];
        
        __block BOOL allStockExist = NO;
        
        // LOAD MKT INFO (using same object SectorInfo but it is actually Market)
        
        //NSArray *filterMarkets = [AppConfigManager filterShowMarket];
        
        self.mktInfoArray = [[NSMutableArray alloc] init];
        NSDictionary *marketInfoDict = currInfo.marketInfos;
        NSArray *mktArr = [marketInfoDict allKeys];
        for (int i=0; i<[mktArr count]; i++) {
            NSString *key = [mktArr objectAtIndex:i];
            SectorInfo *inf = [[SectorInfo alloc] init];
            inf.sectorCode = key;
            inf.sectorName = [marketInfoDict objectForKey:key];
            
            inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"[" withString:@""];
            inf.sectorName = [inf.sectorName stringByReplacingOccurrencesOfString:@"]" withString:@""];
            if ([inf.sectorName isEqualToString:@"All Stocks"]) {
                inf.sectorName = @"Normal Board Lot";
                allStockExist = YES;
            }
    
            
            if ([[UserPrefConstants singleton].filterShowMarket containsObject:inf.sectorCode]) {
                // We have found an object that is not in the other array.
                if (![inf.sectorName containsString:@"Indices"])
                    [_mktInfoArray addObject:inf];
            }
            
            
        }
        
        
        //NSLog(@"_mktInfoArray %@",_mktInfoArray);
        // sort by key (so All Stocks is at top)
        

        
    
        
        [_mktInfoArray sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sectorCode" ascending:YES]]];
        
        
        if (!allStockExist) {
            // add 'Normal Board Lot' sector at 0 index
            SectorInfo *minf = [[SectorInfo alloc] init];
            minf.sectorName = @"Normal Board Lot"; minf.sectorCode = @"10";
            [_mktInfoArray insertObject:minf atIndex:0];
        }
        
        
        // select
        [_mktInfoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            SectorInfo *eachInfo = (SectorInfo *)obj;
            if ([eachInfo.sectorCode isEqualToString:[UserPrefConstants singleton].selectedMarket]) {
                
                [_boardLotButton setTitle:[LanguageManager stringForKey:[NSString stringWithFormat:@"%@",eachInfo.sectorName]] forState:UIControlStateNormal];
                
            }
        }];
        
        
        
        
        
        [_hideWLbutton setHidden:YES];
        [_stockEditBtn setHidden:YES];
        [_stockAddBtn setHidden:YES];
        [_watchlistTable setHidden:YES];
        canPull = YES;
        [_sortBtnContainer setHidden:NO];
        [self setQuoteScreenTitle:20];
        [self setupRankedFirstTime];
        scrollHigh = kScrollContentHeight;
    }
    
    [_compactHeader setHidden:YES];
    [_symbolHeader setHidden:YES];
    
    if ([UserPrefConstants singleton].isCollectionView) {
        [_frontTableView setHidden:YES];
        [_myScrollView setHidden:YES];
        [_cardCollectionView setHidden:NO];
    } else {
        [_frontTableView setHidden:NO];
        [_myScrollView setHidden:NO];
        [_cardCollectionView setHidden:YES];
    }
    
    
    // Set scrollView contentsize
    [self setScrollViewContentSize];
    
    
    for (NSString *stkCode in _selectedArr)
    {
        
        if (([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
            
            NSDictionary *dictToAdd = [[NSDictionary alloc] initWithDictionary:
                                       [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];
            
            [_filteredDict setObject:dictToAdd forKey:stkCode];
            
            
        }
    }
    self.tempDict= [[NSMutableDictionary alloc]initWithDictionary:_filteredDict copyItems:YES];
    
    [self sortData];
    
    if ([_cardCollectionView isHidden]) {
        [_tableView reloadData];
        [_frontTableView reloadData];
    } else {
        [[UserPrefConstants singleton].imgChartCache removeAllObjects];
        [_detailPageDict removeAllObjects];
        [_cardCollectionView reloadData];
    }
}



- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    static BOOL isObservingContentOffsetChange = NO;
    if([object isKindOfClass:[UITableView class]]
       && [keyPath isEqualToString:@"contentOffset"])
    {
        if(isObservingContentOffsetChange) return;
        
        isObservingContentOffsetChange = YES;
        for(UITableView *view in self.tableViewCollection)
        {
            if(view != object)
            {
                CGPoint offset =
                [[change valueForKey:NSKeyValueChangeNewKey] CGPointValue];
                view.contentOffset = offset;
            }
        }
        isObservingContentOffsetChange = NO;
        return;
    }
    
    [super observeValueForKeyPath:keyPath
                         ofObject:object
                           change:change
                          context:context];
}





-(void)executeVertxSort {
    
    [atp timeoutActivity];
    
    // remove all qcFeed stocks except the ticker stocks (cause ticker is always there)
    NSMutableArray *exceptionArray = [[NSMutableArray alloc] init];
    [exceptionArray addObjectsFromArray:[UserPrefConstants singleton].tickerStockCodes];
    //[exceptionArray addObject:[UserPrefConstants singleton].userSelectingStockCode];
    [[QCData singleton] removeAllQcFeedExceptThese:exceptionArray];
    
    
    NSArray *brokerListNameArr = _rankedByNameArr;
    NSArray *brokerListKeyArr = _rankedByKeyArr;
    
    [UserPrefConstants singleton].sortByProperty = [brokerListNameArr objectAtIndex:sortRow];
    
    NSArray *propertyDivideDirection = [[brokerListKeyArr objectAtIndex:sortRow] componentsSeparatedByString:@"|"];
    NSString *property    = [propertyDivideDirection objectAtIndex:0];
    
    // NSLog(@"sortedByFID %@", property);
    
    sortedByFID = property;
    NSString *direction = [propertyDivideDirection objectAtIndex:1];
    [[VertxConnectionManager singleton]vertxSortByServer:property Direction:direction exchg:[UserPrefConstants singleton].userCurrentExchange];
    
}

-(void)openStockContainer:(BOOL)open animated:(BOOL)animated {
    
    [atp timeoutActivity];
    
    [_stockContainerView setHidden:NO];
    
    // [titleLabel setFrame:CGRectMake(_scrollView.frame.origin.x, titleLabel.frame.origin.y,
    //                                 titleLabel.frame.size.width, titleLabel.frame.size.height)];
    
    
    
    void (^animationBlock)();
    animationBlock=^{
        if (open) {
            [_stockAddBtn setHidden:YES];
            [_stockEditBtn setHidden:YES];
            stkContainerDisplayed = YES;
            _stockContainerView.frame = CGRectMake(self.view.frame.size.width-_stockContainerView.frame.size.width,
                                                   _myScrollView.frame.origin.y,
                                                   _stockContainerView.frame.size.width,
                                                   _stockContainerView.frame.size.height);
            
            if (isWatchList) {
                
                CGFloat watchListRef = _watchlistTable.frame.origin.x+_watchlistTable.frame.size.width;
                
                _frontTableView.frame = CGRectMake(watchListRef, _frontTableView.frame.origin.y,
                                                   _frontTableView.frame.size.width,
                                                   _frontTableView.frame.size.height);
                
                
                
                _myScrollView.frame = CGRectMake(oriTablePosX,
                                                 _myScrollView.frame.origin.y,
                                                 scrollViewWidth-_stockContainerView.frame.size.width,
                                                 _myScrollView.frame.size.height);
                
                
            } else {
                _myScrollView.frame = CGRectMake(_myScrollView.frame.origin.x,
                                                 _myScrollView.frame.origin.y,
                                                 scrollViewWidth-_stockContainerView.frame.size.width,
                                                 _myScrollView.frame.size.height);
            }
            
        } else {
            
            stkContainerDisplayed = NO;
            _stockContainerView.frame = CGRectMake(self.view.frame.size.width,
                                                   _myScrollView.frame.origin.y,
                                                   _stockContainerView.frame.size.width,
                                                   _stockContainerView.frame.size.height);
            
            if (isWatchList) {
                
                CGFloat watchListRef = _watchlistTable.frame.origin.x+_watchlistTable.frame.size.width;
                
                _frontTableView.frame = CGRectMake(watchListRef,
                                                   _frontTableView.frame.origin.y,
                                                   _frontTableView.frame.size.width,
                                                   _frontTableView.frame.size.height);
                
                _myScrollView.frame = CGRectMake(oriTablePosX+watchListRef,
                                                 _myScrollView.frame.origin.y,
                                                 scrollViewWidth-watchListRef,
                                                 _myScrollView.frame.size.height);
                
            } else {
                _myScrollView.frame = CGRectMake(_myScrollView.frame.origin.x,
                                                 _myScrollView.frame.origin.y,
                                                 scrollViewWidth,
                                                 _myScrollView.frame.size.height);
                
            }
        }
    };
    
    // remove all gesture added bfore.
    for (UIGestureRecognizer *gr in _stockContainerView.gestureRecognizers) {
        [_stockContainerView removeGestureRecognizer:gr];
    }
    
    
    if (animated) {
        
        if (open) {
            [self showWatchList:NO]; showList = NO;
        }
        
        [UIView animateWithDuration:0.35 animations:animationBlock completion:^(BOOL finished) {
            if (open) {
                
                UIPanGestureRecognizer *panGestureNews = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(stockPreviewDidPanned:)];
                panGestureNews.delegate = self;
                panGestureNews.minimumNumberOfTouches = 2;
                panGestureNews.maximumNumberOfTouches = 2;
                [_stockContainerView addGestureRecognizer:panGestureNews];
            } else {
                long tmpRow = selectedRow;
                selectedRow = -1;
                if (tmpRow>=0) {
                    NSIndexPath *currentPath = [NSIndexPath indexPathForRow:tmpRow inSection:0];
                    [_frontTableView reloadRowsAtIndexPaths:@[currentPath] withRowAnimation:UITableViewRowAnimationNone];
                    [_tableView reloadRowsAtIndexPaths:@[currentPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            }
            containerViewOriPos = _stockContainerView.center;
        }];
    } else {
        animationBlock();
        if (open) {
            UIPanGestureRecognizer *panGestureNews = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(stockPreviewDidPanned:)];
            panGestureNews.delegate = self;
            panGestureNews.minimumNumberOfTouches = 2;
            panGestureNews.maximumNumberOfTouches = 2;
            [_stockContainerView addGestureRecognizer:panGestureNews];
        } else {
            long tmpRow = selectedRow;
            selectedRow = -1;
            if (tmpRow>=0) {
                NSIndexPath *currentPath = [NSIndexPath indexPathForRow:tmpRow inSection:0];
                [_frontTableView reloadRowsAtIndexPaths:@[currentPath] withRowAnimation:UITableViewRowAnimationNone];
                [_tableView reloadRowsAtIndexPaths:@[currentPath] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
        containerViewOriPos = _stockContainerView.center;
    }
    
}



-(void)stockPreviewDidPanned:(UIPanGestureRecognizer*)recognizer {
    
    UIView *view = recognizer.view;
    
    // CGPoint velocity = [recognizer velocityInView:view.superview];
    
    // if (velocity.x>0) {
    
    CGPoint translation = [recognizer translationInView:view.superview];
    
    if (translation.x>=50) {
        bufferPreviewX = translation.x-49;
    } else {
        bufferPreviewX = 0;
    }
    
    if (bufferPreviewX>0) {
        
        CGFloat distanceToDismiss = 150;
        
        if(recognizer.state == UIGestureRecognizerStateBegan){
            
            
            
        }else if(recognizer.state == UIGestureRecognizerStateEnded){
            
            if (_stockContainerView.center.x<=containerViewOriPos.x+distanceToDismiss) {
                // return back
                [UIView animateWithDuration:0.2 animations:^{
                    _stockContainerView.center = containerViewOriPos;
                }completion:^(BOOL finished) {
                    
                }];
            }
        } else {
            // swping
            _stockContainerView.center = CGPointMake(containerViewOriPos.x+bufferPreviewX, containerViewOriPos.y);
            
            _myScrollView.frame = CGRectMake(_myScrollView.frame.origin.x,
                                             _myScrollView.frame.origin.y,
                                             _stockContainerView.frame.origin.x - _myScrollView.frame.origin.x,
                                             _myScrollView.frame.size.height);
            
            if (_stockContainerView.center.x>containerViewOriPos.x+distanceToDismiss) {
                [view removeGestureRecognizer:recognizer];
                [self openStockContainer:NO animated:YES];
            }
            
        }
        
    } else {
        
        [UIView animateWithDuration:0.2 animations:^{
            _stockContainerView.center = containerViewOriPos;
        }completion:^(BOOL finished) {
            
        }];
        
    }
    
}

#pragma mark - StockPreViewDelegate

-(void)closePreviewBtnClicked {
    //Hide OrderPad...
    [self traderDidCancelled];
    [self openStockContainer:NO animated:YES];
}

-(void)chartWillEnlarge {
    
}

-(void)chartWillShrink {
    
}

-(void)chartDidEnlarge:(BOOL)enlarge {
    if (enlarge) {
        // disable tableview
        [self.tableView setUserInteractionEnabled:NO];
        [self.frontTableView setUserInteractionEnabled:NO];
        
    } else {
        //enable tableview
        [self.tableView setUserInteractionEnabled:YES];
        [self.frontTableView setUserInteractionEnabled:YES];
    }
}


#pragma mark - ScrollView delegate (Pull to load)


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    
    if (canLoadData!=kDataLoadCannot) {
        // do loading here
        // set offset, hide label, show activity
        
        
        if (canLoadData==kDataLoadNext) {
            if ([UserPrefConstants singleton].isCollectionView) {
                _cardCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 60.0, 0);
            } else {
                _tableView.contentInset = UIEdgeInsetsMake(0, 0, 60.0, 0);
                _frontTableView.contentInset = UIEdgeInsetsMake(0, 0, 60.0, 0);
            }
            [_customPullLabel setHidden:YES];
            [_pullImg1 setHidden:YES];[_pullImg2 setHidden:YES];
            [_customPullActivity startAnimating];
            [UserPrefConstants singleton].quoteScrPage += 1;
            
            [self performSelector:@selector(executeVertxSort) withObject:nil afterDelay:1.0];
        }
        if (canLoadData==kDataLoadPrev) {
            if ([UserPrefConstants singleton].isCollectionView) {
                _cardCollectionView.contentInset = UIEdgeInsetsMake(60, 0, 0.0, 0);
            } else {
                _tableView.contentInset = UIEdgeInsetsMake(60, 0, 0.0, 0);
                _frontTableView.contentInset = UIEdgeInsetsMake(60, 0, 0.0, 0);
            }
            [_customPullLabel setHidden:YES];
            [_pullImg1 setHidden:YES];[_pullImg2 setHidden:YES];
            [_customPullActivity startAnimating];
            [UserPrefConstants singleton].quoteScrPage -= 1;
            
            [self performSelector:@selector(executeVertxSort) withObject:nil afterDelay:1.0];
        }
        canLoadData = kDataLoadCannot;
    }
    
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat scrollViewHeight;
    CGFloat scrollContentSizeHeight;
    CGFloat bottomInset;
    CGFloat scrollViewBottomOffset;
    CGFloat currentYOffset;
    CGFloat pullContainerXPos;
    
    if ([UserPrefConstants singleton].isCollectionView) {
        // check offset of scrollview in CollectionView
        scrollViewHeight = _cardCollectionView.bounds.size.height;
        scrollContentSizeHeight = _cardCollectionView.contentSize.height;
        bottomInset = _cardCollectionView.contentInset.bottom;
        scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
        currentYOffset = _cardCollectionView.contentOffset.y;
        pullContainerXPos = self.view.frame.size.width/2.0;
    } else {
        // check offset of scrollview in Tableview
        scrollViewHeight = _tableView.bounds.size.height;
        scrollContentSizeHeight = _tableView.contentSize.height;
        bottomInset = _tableView.contentInset.bottom;
        scrollViewBottomOffset = scrollContentSizeHeight + bottomInset - scrollViewHeight;
        currentYOffset = _tableView.contentOffset.y;
        pullContainerXPos = _myScrollView.frame.size.width/2.0+200;
        NSLog(@"pullContainerXPos %.2f",pullContainerXPos);
    }
    
    [_customPullContainer setAlpha:0.0];
    
    // AT THE TOP
    if(currentYOffset < 0) {
        
        _pullImg1.image = _pullImg2.image = [UIImage imageNamed:@"pulldn"];
        _customPullContainer.center = CGPointMake(pullContainerXPos, _customPullContainer.frame.size.height/2.0);
        
        CGFloat draggedDist = - currentYOffset;
        if (draggedDist>=kPaginationTrigger) draggedDist = kPaginationTrigger;
        
        if (draggedDist>=kPaginationTrigger) {
            [_customPullContainer setAlpha:1.0];
            _pullImg1.transform = CGAffineTransformMakeRotation(M_PI);
            _pullImg2.transform = CGAffineTransformMakeRotation(-M_PI);
            _customPullLabel.text = [LanguageManager stringForKey:@"Release to Load"];
            if ([UserPrefConstants singleton].quoteScrPage>=1) canLoadData = kDataLoadPrev;
        } else {
            if ([_customPullActivity isAnimating]) {
                [_customPullContainer setAlpha:1.0];
            } else {
                [_customPullContainer setAlpha:draggedDist/kPaginationTrigger];
            }
            _pullImg1.transform = CGAffineTransformIdentity;
            _pullImg2.transform = CGAffineTransformIdentity;
            int recNum = 1+([UserPrefConstants singleton].quoteScrPage-1)*20;
            
            NSString *fromRecStr = [NSString stringWithFormat:@"%d",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%d",recNum+19];
            _customPullLabel.text = [LanguageManager stringForKey:@"Pull Down To Load Records %d-%d" withPlaceholders:@{@"%fromRecord%":fromRecStr,@"%toRecord%":toRecStr}];
            canLoadData = kDataLoadCannot;
            if ([UserPrefConstants singleton].quoteScrPage>=1) {
                [_customPullContainer setHidden:NO];
            } else {
                [_customPullContainer setHidden:YES];
            }
        }
        
    }
    
    // AT THE BOTTOM
    if(currentYOffset > scrollViewBottomOffset) {
        
        _pullImg1.image = _pullImg2.image = [UIImage imageNamed:@"pull"];
        _customPullContainer.center = CGPointMake(pullContainerXPos,
                                                  _tableView.frame.size.height-_customPullContainer.frame.size.height/2.0);
        
        CGFloat draggedDist = currentYOffset - scrollViewBottomOffset;
        if (draggedDist>=kPaginationTrigger) draggedDist = kPaginationTrigger;
        
        
        
        if (draggedDist>=kPaginationTrigger) {
            [_customPullContainer setAlpha:1.0];
            _pullImg1.transform = CGAffineTransformMakeRotation(M_PI);
            _pullImg2.transform = CGAffineTransformMakeRotation(-M_PI);
            _customPullLabel.text = [LanguageManager stringForKey:@"Release to Load"];
            if ([_filteredDict count]>=20) {
                if (!isWatchList)canLoadData = kDataLoadNext;
                if (!isWatchList)_customPullContainer.hidden = NO;
            } else {
                _customPullContainer.hidden = YES;
            }
        } else {
            if ([_customPullActivity isAnimating]) {
                [_customPullContainer setAlpha:1.0];
            } else {
                [_customPullContainer setAlpha:draggedDist/kPaginationTrigger];
            }
            _pullImg1.transform = CGAffineTransformIdentity;
            _pullImg2.transform = CGAffineTransformIdentity;
            int recNum = 1+([UserPrefConstants singleton].quoteScrPage+1)*20;
            NSString *fromRecStr = [NSString stringWithFormat:@"%d",recNum];
            NSString *toRecStr = [NSString stringWithFormat:@"%d",recNum+19];
            _customPullLabel.text = [LanguageManager stringForKey:@"Pull Up To Load Records %d-%d" withPlaceholders:@{@"%fromRecord%":fromRecStr,@"%toRecord%":toRecStr}];
            canLoadData = kDataLoadCannot;
            if ([_filteredDict count]<20) {
                [_customPullContainer setHidden:YES];
            } else {
                if (!isWatchList)[_customPullContainer setHidden:NO];
            }
        }
        
    }
    
    
    
}


- (IBAction)editStocks:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if ([_frontTableView isEditing]) {
        [_frontTableView setEditing:NO animated:YES];
        [btn setImage:[UIImage imageNamed:@"edit-button"] forState:UIControlStateNormal];
        [btn setTitle:@"" forState:UIControlStateNormal];
        [btn.layer removeAllAnimations];
    } else {
        [_frontTableView setEditing:YES animated:YES];
        [btn setImage:nil forState:UIControlStateNormal];
        [btn setTitle:[LanguageManager stringForKey:@"Done"] forState:UIControlStateNormal];
        CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        pulseAnimation.duration = .35;
        pulseAnimation.toValue = [NSNumber numberWithFloat:1.1];
        pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        pulseAnimation.autoreverses = YES;
        pulseAnimation.repeatCount = MAXFLOAT;
        [btn.layer addAnimation:pulseAnimation forKey:@"PulseButtonAnimation"];
    }
}

- (IBAction)editWatchList:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if ([_watchlistTable isEditing]) {
        [_watchlistTable setEditing:NO animated:YES];
        [btn setImage:[UIImage imageNamed:@"edit-button"] forState:UIControlStateNormal];
        [btn setTitle:@"" forState:UIControlStateNormal];
        [btn.layer removeAllAnimations];
    } else {
        [_watchlistTable setEditing:YES animated:YES];
        [btn setImage:nil forState:UIControlStateNormal];
        [btn setTitle:[LanguageManager stringForKey:@"Done"] forState:UIControlStateNormal];
        
        CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        pulseAnimation.duration = .35;
        pulseAnimation.toValue = [NSNumber numberWithFloat:1.1];
        pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        pulseAnimation.autoreverses = YES;
        pulseAnimation.repeatCount = MAXFLOAT;
        [btn.layer addAnimation:pulseAnimation forKey:@"PulseButtonAnimation"];
    }
}



- (IBAction)addWatchList:(id)sender {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Create"]
                                                                              message:[LanguageManager stringForKey:@"Enter a name for the new WatchList."]
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = [LanguageManager stringForKey:@"Enter Name"];
        textField.text = @"";
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        
        _keyboardLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        _keyboardLabel.backgroundColor = [UIColor whiteColor];
        _keyboardLabel.textColor = [UIColor blackColor];
        _keyboardLabel.text = [LanguageManager stringForKey:@" Create New WatchList?"];
        textField.inputAccessoryView = _keyboardLabel;
        
        [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Ok"]
                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
               
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        
        // get max ID
        int max = -1;
    
        for (int i=0;i<[_watchListArray count];i++)
        {
            NSDictionary *firstWL = [_watchListArray objectAtIndex:i];
            int ori = [[firstWL objectForKey:@"FavID"] intValue];
            
            if (ori>max)
            {
                max = ori;
            }
        }
        if (max==-1) max = 1; else max += 1; // cannot 0 server not accept
        
        if ([_watchListArray count]>=15) {
            
            [[UserPrefConstants singleton]
             popMessage:[LanguageManager stringForKey:@"We regret to inform that we are unable to create your watchlist as you have reached your maximum number of watchlists."]
             inController:self withTitle:@""];
            
            return;
        }
        
        // only add if id is more than 0. Id 0 is not recognized by backend
        
                                                        
        if (watchListID>0) {
            [atp addOrUpdateWatchlistName:max withFavName:namefield.text];
            NSDictionary *newWL = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%d",watchListID],@"FavID",
                                   namefield.text,@"Name",
                                   nil];
            [[UserPrefConstants singleton].userWatchListArray addObject:newWL];
        } else {
            
            if (_watchListArray.count==0 && watchListID==0) {
                watchListID = 1;
                [atp addOrUpdateWatchlistName:max withFavName:namefield.text];
                NSDictionary *newWL = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSString stringWithFormat:@"%d",watchListID],@"FavID",
                                       namefield.text,@"Name",
                                       nil];
                [[UserPrefConstants singleton].userWatchListArray addObject:newWL];
            }else{
               NSLog(@"Watchlist ID invalid! (%d)",watchListID);
            }
            

        }
        // [self editWatchList:_watchListEditBtn];
        [alertController dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    okBtn.enabled = NO;
    [alertController addAction:okBtn];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Cancel"]
                                                        style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark - Notification from SearchPanel

-(void)watchListAddThisStock:(NSNotification*)notification {
    NSDictionary *dict = (NSDictionary*)[notification userInfo];
    NSString *stkCode = [dict objectForKey:@"stkCode"];

    [atp addWatchlistCheckItem:watchListID  withItem:stkCode];
    
}


- (IBAction)addStock:(id)sender {
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:@"dummy" forKey:@"dummy"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"searchStockToAdd" object:nil userInfo:dict];
}



-(void)showWatchList:(BOOL)show {
    
    collectionViewResized = YES;
    
    [UIView animateWithDuration:0.35 animations:^(void) {
        
        if (show) {
            [_stockAddBtn setHidden:NO];
            [_stockEditBtn setHidden:NO];
            _watchlistTable.frame = CGRectMake(0,
                                               _watchlistTable.frame.origin.y,
                                               _watchlistTable.frame.size.width,
                                               _watchlistTable.frame.size.height);
            [_hideWLbutton setTitle:[LanguageManager stringForKey:@"< Hide List"] forState:UIControlStateNormal];
            
            [_showListBtn setHidden:YES];
            _stockTitle.frame = CGRectMake(_showListBtn.frame.origin.x, _stockTitle.frame.origin.y,
                                           _stockTitle.frame.size.width, _stockTitle.frame.size.height);
            [_hideListBtn setHidden:NO];
            _watchListTitle.frame =CGRectMake(_hideListBtn.frame.origin.x+_hideListBtn.frame.size.width,
                                              _watchListTitle.frame.origin.y,
                                              _watchListTitle.frame.size.width, _watchListTitle.frame.size.height);
            
            
            
            
        } else {
            
            [_stockAddBtn setHidden:YES];
            [_stockEditBtn setHidden:YES];
            _watchlistTable.frame = CGRectMake(-_watchlistTable.frame.size.width,
                                               _watchlistTable.frame.origin.y,
                                               _watchlistTable.frame.size.width,
                                               _watchlistTable.frame.size.height);
            [_hideWLbutton setTitle:[LanguageManager stringForKey:@"Show List >"] forState:UIControlStateNormal];
            
            if (isWatchList) {
                [_showListBtn setHidden:NO];
                _stockTitle.frame = CGRectMake(_showListBtn.frame.origin.x+_showListBtn.frame.size.width,
                                               _stockTitle.frame.origin.y,
                                               _stockTitle.frame.size.width, _stockTitle.frame.size.height);
                [_hideListBtn setHidden:YES];
                _watchListTitle.frame =CGRectMake(_hideListBtn.frame.origin.x,
                                                  _watchListTitle.frame.origin.y,
                                                  _watchListTitle.frame.size.width, _watchListTitle.frame.size.height);
            }
        }
        
        
        
        
        CGFloat watchListRef = _watchlistTable.frame.origin.x+_watchlistTable.frame.size.width;
        
        _frontTableView.frame = CGRectMake(watchListRef, _frontTableView.frame.origin.y,
                                           _frontTableView.frame.size.width,
                                           _frontTableView.frame.size.height);
        
        CGFloat totalWid = 0;
        CGFloat totalWidCV = self.view.frame.size.width - watchListRef;
        
        if (stkContainerDisplayed) {
            totalWid = scrollViewWidth-_stockContainerView.frame.size.width;
        } else {
            totalWid = scrollViewWidth;
        }
        
        
        _myScrollView.frame = CGRectMake(oriTablePosX+watchListRef,
                                         _myScrollView.frame.origin.y,
                                         totalWid,
                                         _myScrollView.frame.size.height);
        
        _cardCollectionView.frame = CGRectMake(watchListRef,
                                               _cardCollectionView.frame.origin.y,
                                               totalWidCV,
                                               _cardCollectionView.frame.size.height);
        
        loadingBtn.center = _cardCollectionView.center;
        _noResultsMsgBtn.center = _cardCollectionView.center;
        
        
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (IBAction)toggleWatchList:(id)sender {
    
    showList = !showList;
    
    
    if (showList)[UserPrefConstants singleton].collectionViewHeaderMode=WATCHLIST_NONHIDDENLIST; else
        [UserPrefConstants singleton].collectionViewHeaderMode=WATCHLIST_HIDDENLIST;
    
    [self showWatchList:showList];
    
    if (stkContainerDisplayed)  [self openStockContainer:NO animated:YES];
}

- (IBAction)toggleWarrant:(id)sender {
    
    [UserPrefConstants singleton].enableWarrant = ![UserPrefConstants singleton].enableWarrant;
    
    if ([UserPrefConstants singleton].enableWarrant) {
        [_warrantButton setTitle:[LanguageManager stringForKey:@"Show All"] forState:UIControlStateNormal];
        
    } else {
        [_warrantButton setTitle:[LanguageManager stringForKey:@"Show Warrant"] forState:UIControlStateNormal];
    }
    
   [self executeVertxSort];
    
}


#pragma mark - CollectionView Delegate - New Mode Jan 2017

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        CollectionReusableHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                                    UICollectionElementKindSectionHeader
                                                    withReuseIdentifier:@"CardHeaderID" forIndexPath:indexPath];
        [[LanguageManager defaultManager] translateStringsForView:headerView];
        headerView.backgroundColor = kPanelColor;
        reusableview = headerView;
    }
    
    return reusableview;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [_filteredDict count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    if (!canPull) [_customPullContainer setHidden:YES];
    
    // (Cannot set any background view here since bgview is the "pull to refresh" view!)
    if ([_filteredDict count]<=0) {
        return 1;
        
    } else {
        return 1;
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat ratio = 200.0/280.0; // w/h
    CGFloat cellWidth = (collectionView.frame.size.width-50)/4.0;
    CGFloat cellHeight = cellWidth / ratio;
    
    CGSize cellSize = CGSizeMake(cellWidth, cellHeight);
    
    return cellSize;
}





-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"CardCellID";
    CardCollectionViewCell *cell = (CardCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    if (cell==nil) {
        cell = [[CardCollectionViewCell alloc] initWithCoder:nil];
    }
    
    NSDictionary *eachStock = [_filteredDict objectForKey:[sortedArr objectAtIndex:indexPath.row]];
    NSDictionary *tmpDict = [_tempDict objectForKey:[sortedArr objectAtIndex:indexPath.row]];
    
    cell.delegate = self;
    
    NSInteger page = [[_detailPageDict objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] integerValue];
    
    [[LanguageManager defaultManager] translateStringsForView:cell.contentView];
    
    [cell populateCellsWithDictionary:eachStock
                             fromDict:tmpDict
                               forRow:indexPath.row
                              doFlash:isCollectionUpdate
                           detailPage:page
                             withCode:[sortedArr objectAtIndex:indexPath.row]
                          isWatchList:isWatchList];
    
    if(indexPath.row == ((NSIndexPath*)[[collectionView indexPathsForVisibleItems] lastObject]).row){
        isCollectionUpdate = NO;
    }
    
    return cell;
}



#pragma mark - CardCollectionDelegate

-(void)detailDidScrollToPage:(NSInteger)page atRow:(NSInteger)row {
    [atp timeoutActivity];
    [_detailPageDict setObject:[NSNumber numberWithLong:page] forKey:[NSString stringWithFormat:@"%ld",(long)row]];
}

-(void)cellGoEditMode:(NSInteger)row {
    [atp timeoutActivity];
    
    // show remove
    if (isWatchList) {
        
        NSString *stockToDelete = [sortedArr objectAtIndex:row];
        NSDictionary *detailDict = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
        
        NSString *delMsg = [LanguageManager stringForKey:@"Are you sure you want to delete %@?"
                                        withPlaceholders:@{@"%stkCode%":[detailDict objectForKey:FID_38_S_STOCK_NAME]}];
        
        UIAlertController * alert =   [UIAlertController
                                       alertControllerWithTitle:[LanguageManager stringForKey:@"Confirm Deletion"]
                                       message:delMsg
                                       preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:[LanguageManager stringForKey:@"Yes"]
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 // tell server to delete
                                 [self editStocks:_stockEditBtn];
                                 [atp deleteWatchlistItem:watchListID withItem:stockToDelete];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:[LanguageManager stringForKey:@"Cancel"]
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}

-(void)addToWatchListClicked:(NSInteger)row atButton:(UIButton*)sender {
    [atp timeoutActivity];
    
    NSDictionary *eachStock = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
    self.stkToAddToWL = [eachStock objectForKey:FID_33_S_STOCK_CODE];
     dispatch_async(dispatch_get_main_queue(), ^{
    
         [self performSegueWithIdentifier:@"segueAddToWatchlist2" sender:nil];
    
     });
    
//    NSDictionary *eachStock = [filteredDict objectForKey:[sortedArr objectAtIndex:row]];
//    NSString *stkCode = [eachStock objectForKey:FID_33_S_STOCK_CODE];
//    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Add to..."]
//                                                                             message:[LanguageManager stringForKey:@"WatchList"]
//                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    for(int i =0;i<[UserPrefConstants singleton].userWatchListArray.count;i++)
//    {
//        NSDictionary *eachWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:i];
//        UIAlertAction *watchListAction = [UIAlertAction actionWithTitle:[eachWL objectForKey:@"Name"]
//                                                                  style:UIAlertActionStyleDefault
//                                                                handler:^(UIAlertAction *action)
//                                          {
//                                              ////NSLog(@"Clicked WatchList item");
//                                              ////NSLog(@"Watchlist items fav id :  %@, stockcode:%@ ",watchListHeaderKey[i],_stkCode);
//                                              [atp addWatchlistCheckItem:[[eachWL objectForKey:@"FavID"] intValue]  withItem:stkCode];
//                                              
//                                          }];
//        
//        [alertController addAction:watchListAction];
//    }
//    
//    
//    
//    UIPopoverPresentationController *popover = alertController.popoverPresentationController;
//    if (popover)
//    {
//        popover.sourceView = sender;
//        popover.sourceRect = sender.bounds;
//        popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
//    }
//    [self presentViewController:alertController animated:YES completion:nil];
//    [alertController.view layoutIfNeeded];
    
    
}

-(void)reloadButtonDidClicked:(NSInteger)row {
    [atp timeoutActivity];
    CardCollectionViewCell *cell = (CardCollectionViewCell*)[_cardCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
    NSDictionary *eachStock = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
    [cell reloadChartWithDict:eachStock];
    
}


-(void)buyStockDidClicked:(NSInteger)row {
    [atp timeoutActivity];
    
    NSDictionary *eachStock = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
    NSString *stkCode = [eachStock objectForKey:FID_33_S_STOCK_CODE];
    _userPref.userSelectingStockCode = stkCode;
    
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:stkCode forKey:@"stkCode"];
    [UserPrefConstants singleton].callBuySellReviseCancel = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
}


-(void)sellStockDidClicked:(NSInteger)row {
    [atp timeoutActivity];
    
    NSDictionary *eachStock = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
    NSString *stkCode = [eachStock objectForKey:FID_33_S_STOCK_CODE];
    _userPref.userSelectingStockCode = stkCode;
    
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:stkCode forKey:@"stkCode"];
    [UserPrefConstants singleton].callBuySellReviseCancel = 2;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
}

-(void)goDetailDidClicked:(NSInteger)row {
    [atp timeoutActivity];
    
    NSDictionary *eachStock = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
    NSString *stkCode = [eachStock objectForKey:FID_33_S_STOCK_CODE];
    _userPref.userSelectingStockCode = stkCode;
    
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:stkCode forKey:@"stkCode"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
}

#pragma mark - TableView Delegate


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 57.0;
    
}


- (void)alertTextFieldDidChange:(UITextField *)sender {
    
    [atp timeoutActivity];
    
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController) {
        UITextField *nameTextField = alertController.textFields.firstObject;
        UIAlertAction *okAction = alertController.actions.firstObject;
        okAction.enabled = YES;
        _keyboardLabel.text = @" ";
        
        if (nameTextField.text.length <= 1) {
            okAction.enabled = NO;
            _keyboardLabel.text = [LanguageManager stringForKey:Minimum_2_characters_for_Watchlist];
            return;
        }
        
        NSError* regexError = nil;
                                                                                    // x2D = '-', x5F = '_', x20 = ' '
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"^[a-zA-Z0-9\x2D\x5F\x20]*$" options:0 error:&regexError];
        NSTextCheckingResult *match = [regex firstMatchInString:nameTextField.text options:0 range:NSMakeRange(0, [nameTextField.text length])];
        
        if (!match) {
            okAction.enabled = NO;
            _keyboardLabel.text = [LanguageManager stringForKey:@"WatchListNameRestriction"];
            return;
        }
        
        
        long counter = [[UserPrefConstants singleton].userWatchListArray count];
        for (int x=0; x<counter; x++) {
            NSDictionary *eachWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:x];
            NSString *currentString = [eachWL objectForKey:@"Name"];
            if([nameTextField.text isEqualToString:currentString]){
                okAction.enabled = NO;
                _keyboardLabel.text = [LanguageManager stringForKey:@" That name has already been used. Choose a different name."];
                return;
            }
        }
        
        
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [atp timeoutActivity];
    
    // MARK: Watchlist Renaming
    if (tableView.tag==12) {
        
        // Selecting a row while editing is Renaming
        if (tableView.editing == YES) {
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: [LanguageManager stringForKey:@"Rename"]
                                                                                      message: [LanguageManager stringForKey:@"Edit WatchList Name"]
                                                                               preferredStyle:UIAlertControllerStyleAlert];
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.placeholder = [LanguageManager stringForKey:@"WatchList Name"];
                textField.textColor = [UIColor blackColor];
                
                NSDictionary *eachWL = [_watchListArray objectAtIndex:indexPath.row];
                watchListID = [[eachWL objectForKey:@"FavID"] intValue];
                textField.text = [eachWL objectForKey:@"Name"];;
                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                textField.borderStyle = UITextBorderStyleRoundedRect;
                
                _keyboardLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
                _keyboardLabel.backgroundColor = [UIColor whiteColor];
                _keyboardLabel.textColor = [UIColor blackColor];
                _keyboardLabel.text = [LanguageManager stringForKey:@" Rename watchlist to a new name?"];
                textField.inputAccessoryView = _keyboardLabel;
                
                [textField addTarget:self action:@selector(alertTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            }];
            
            [alertController addAction:[UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                NSArray * textfields = alertController.textFields;
                UITextField * namefield = textfields[0];
                [atp addOrUpdateWatchlistName:watchListID withFavName:namefield.text];
                if (watchListID>0) {
                    NSDictionary *newWL = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [NSString stringWithFormat:@"%d",watchListID],@"FavID",
                                           namefield.text,@"Name",
                                           nil];
                    [[UserPrefConstants singleton].userWatchListArray addObject:newWL];

                }
                [self editWatchList:_watchListEditBtn];
                [alertController dismissViewControllerAnimated:YES completion:nil];
                
            }]];
            
            [alertController addAction:[UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [alertController dismissViewControllerAnimated:YES completion:nil];
                
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            // MARK: Watchlist show details
        } else {
            // selecting when not editing, show watchlist stockss
            prevWLRow = selectedWLRow;
            selectedWLRow = indexPath.row;
            
            
//            if (updateTimer!=nil) {
//                [updateTimer invalidate];
//                updateTimer = nil;
//            }
            
            
            
            if (prevWLRow>=0) {
                NSIndexPath *prevPath = [NSIndexPath indexPathForRow:prevWLRow inSection:0];
                [_watchlistTable reloadRowsAtIndexPaths:@[prevPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            [_watchlistTable reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            NSMutableArray *exceptionArray = [[NSMutableArray alloc] init];
            [exceptionArray addObjectsFromArray:[UserPrefConstants singleton].tickerStockCodes];
            //[exceptionArray addObject:[UserPrefConstants singleton].userSelectingStockCode];
            [[QCData singleton] removeAllQcFeedExceptThese:exceptionArray];
            
            NSDictionary *firstWL = [_watchListArray objectAtIndex:indexPath.row];
            int favID = [[firstWL objectForKey:@"FavID"] intValue];
            
            [UserPrefConstants singleton].prevWatchlistFavID = favID;
            [UserPrefConstants singleton].watchListName = [firstWL objectForKey:@"Name"];
            
            [loadingBtn startAnimating];
            [_noResultsMsgBtn setHidden:YES];
            
            [_filteredDict removeAllObjects];
            [_tableView reloadData];
            [_frontTableView reloadData];
            [_cardCollectionView reloadData];
            
            [atp getWatchListItems:favID];
            watchListID = favID;
            
            NSLog(@"watchListID %d",watchListID);
            
            [UserPrefConstants singleton].quoteScreenFavID=[NSNumber numberWithInt:favID];
            
            
            [titleLabel setText: [LanguageManager stringForKey:@"WatchList: %@" withPlaceholders:@{@"%WLName%":[UserPrefConstants singleton].watchListName}]];
            
        }
        
        // MARK: Stock selected
    } else {
        
        // go compact view
        [self setScrollViewContentSize];
        
        [_tableView reloadData];
        [_frontTableView reloadData];
        
        stkSelected = [sortedArr objectAtIndex:indexPath.row];
        
        prevRow = selectedRow;
        selectedRow = indexPath.row;
        
        if (prevRow>=0) {
            NSIndexPath *prevPath = [NSIndexPath indexPathForRow:prevRow inSection:0];
            [_frontTableView reloadRowsAtIndexPaths:@[prevPath] withRowAnimation:UITableViewRowAnimationNone];
            [_tableView reloadRowsAtIndexPaths:@[prevPath] withRowAnimation:UITableViewRowAnimationNone];
            
        }
        
        [_frontTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        NSDictionary *detailDict = [_filteredDict objectForKey:[sortedArr objectAtIndex:indexPath.row]];
        
        // if no data, dont need to open
        if ([detailDict.allKeys count]<=0) {
            return;
        }
        
        
        NSString *stkNameStr = @"";
        if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
            stkNameStr = [detailDict objectForKey:FID_130_S_SYSBOL_2];
        } else {
            stkNameStr = [detailDict objectForKey:FID_38_S_STOCK_NAME];
        }
        NSArray *stkNameDivide = [stkNameStr componentsSeparatedByString:@"."];
        NSString *stkName = [stkNameDivide objectAtIndex:0];
        
        
        NSString *stkCodeStr = [detailDict objectForKey:FID_33_S_STOCK_CODE];
        
        _userPref.userSelectingStockCode = stkCodeStr;
        
        selectedStkCodeStr = stkCodeStr;
        selectedStkName = stkName;
        stkCodeAndExchangeArr = [selectedStkCodeStr componentsSeparatedByString:@"."];
        trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
        lotSizeStringInt = [[[[_qcData qcFeedDataDict]objectForKey:stkCodeStr]objectForKey:FID_40_I_SHARE_PER_LOT] intValue];

        
        // fire delegate
        [self.delegate stockDidSelected:stkCodeStr andName:stkName watchList:isWatchList];
        [self openStockContainer:YES animated:YES];
        
        
    }
    
    
    ////NSLog(@"_userPref.userSelectingStockCode : %@",_userPref.userSelectingStockCode);
    //    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    //    [notificationData setObject:stockCode forKey:@"stkCode"];
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
}


-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if (tableView.tag==12) {
        if (selectedWLRow==indexPath.row) {
            cell.contentView.backgroundColor = kFrontTableColor;
            cell.backgroundColor = kFrontTableColor;
        } else {
            cell.contentView.backgroundColor = kWatchListColor;
            cell.backgroundColor = kWatchListColor;
        }
    } else {
        if (selectedRow==indexPath.row) {
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.17 green:0.17 blue:0.17 alpha:1.00];
            cell.backgroundColor = [UIColor colorWithRed:0.17 green:0.17 blue:0.17 alpha:1.00];
        }  else {
            
            if (tableView.tag==10) cell.contentView.backgroundColor = kFrontTableColor;
            if (tableView.tag==11) cell.contentView.backgroundColor = kTableColor;
            cell.backgroundColor = cell.contentView.backgroundColor;
        }
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 50;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==11) {
        //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
        /* Create custom view to display section header... */
        
        [_compactHeader setHidden:NO];
        
        [_compactHeader setBackgroundColor:[self.view backgroundColor]];
        
        return _compactHeader;
        
    }else if (tableView.tag==10) {
        /*  UIView *view;
         view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
         
         
         UIButton *stock =[[UIButton alloc ]initWithFrame:CGRectMake(0 ,0, 80, 22)];
         [stock addTarget:self action:@selector(sortButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
         [stock setTag:0];
         [stock.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16.0] ];
         [stock setTitle:@"Symbol" forState:UIControlStateNormal&UIControlStateSelected&UIControlStateHighlighted];
         [stock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal&UIControlStateSelected&UIControlStateHighlighted];
         [view addSubview:stock];
         Section header is in 0th index... */
        
        [_symbolHeader setHidden:NO];
        [_symbolHeader setBackgroundColor:kFrontTableColor];
        [_frontTableView setBackgroundColor:kFrontTableColor];
        return _symbolHeader;
        
    } else {
        [_watchHeader setBackgroundColor:kWatchListColor];
        [_watchlistTable setBackgroundColor:kWatchListColor];
        return _watchHeader;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!canPull) [_customPullContainer setHidden:YES];
    
    // WATCHLIST TABLE
    if (tableView.tag==12) {
        
        return 1;
    }
    
    // QUOTE SCREEN STOCK DETAILS (Warning: Cannot set background view here since bgview is the pull to refresh view!)
    if (tableView.tag==11) {
        if ([_filteredDict count]<=0) {
            
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
            
        } else {
            
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            return 1;
        }
    }
    
    // QUOTE SCREEN STOCK SYMBOL
    if (tableView.tag==10) {
        if ([_filteredDict count]<=0) {
            // Display a message when the table is empty
            
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
            
        } else {
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            return 1;
        }
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    ////NSLog(@"[[QCData singleton] qcFeedDataDict]  : %@",[[QCData singleton] qcFeedDataDict] );
    //    return [[[QCData singleton] qcFeedDataDict] count];
    ////NSLog(@"filteredDict Count : %lu",(unsigned long)[filteredDict count]);
    
    if (tableView.tag==12) {
        
        return [_watchListArray count];
        
    } else {
        
        return [_filteredDict count];
        
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView  editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Detemine if it's in editing mode
    if (tableView.editing)
    {
        return UITableViewCellEditingStyleDelete; //enable when editing mode is on
    }
    
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [atp timeoutActivity];
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Mark: Delete stock
        if (tableView.tag==10) {
            
            NSString *stockToDelete = [sortedArr objectAtIndex:indexPath.row];
            NSDictionary *detailDict = [_filteredDict objectForKey:[sortedArr objectAtIndex:indexPath.row]];
            
            NSString *delMsg;
            if ([detailDict count]<=0) {
                delMsg = [LanguageManager stringForKey:@"Are you sure you want to delete %@?"
                                      withPlaceholders:@{@"%stkCode%":stockToDelete}];
            } else {
                delMsg = [LanguageManager stringForKey:@"Are you sure you want to delete %@?"
                                      withPlaceholders:@{@"%stkCode%":[detailDict objectForKey:FID_38_S_STOCK_NAME]}];
            }
            
            
            
            
            UIAlertController * alert =   [UIAlertController
                                           alertControllerWithTitle:[LanguageManager stringForKey:@"Confirm Deletion"]
                                           message:delMsg
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:[LanguageManager stringForKey:@"Yes"]
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     // tell server to delete
                                     [self editStocks:_stockEditBtn];
                                     
                                     // retrieve watchlistID for this stock
                                     
                                     
                                     [atp deleteWatchlistItem:watchListID withItem:stockToDelete];
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:[LanguageManager stringForKey:@"Cancel"]
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        } else if (tableView.tag==12) { // Delete Watchlist
            
            NSDictionary *selectedWL = [[UserPrefConstants singleton].userWatchListArray objectAtIndex:indexPath.row];
            
            int whichWatchListID = [[selectedWL objectForKey:@"FavID" ] intValue];
            
            
            NSDictionary *wListToDelete = [_watchListArray objectAtIndex:indexPath.row];
            
            NSString *delMsg = [LanguageManager stringForKey:@"Are you sure you want to delete %@?"
                                            withPlaceholders:@{@"%stkCode%":[wListToDelete objectForKey:@"Name"]}];
            
            
            UIAlertController * alert =   [UIAlertController
                                           alertControllerWithTitle:[LanguageManager stringForKey:@"Confirm Deletion"]
                                           message: delMsg
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:[LanguageManager stringForKey:@"Yes"]
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     selectedWLRow = -1;
                                     [self editWatchList:_watchListEditBtn];
                                     [atp deleteWatchlist:whichWatchListID];
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:[LanguageManager stringForKey:@"Cancel"]
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            [alert addAction:ok];
            [alert addAction:cancel];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((tableView.tag==12)||(tableView.tag==10))
        return YES;
    else
        return NO;
}
- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // MARK: TABLEVIEW
    if (tableView.tag==11) {
        
        
        NSString *lastDone,*chg,*chgP,*d1,*d2,*d3,*d4,*d5;
        //Formatters
        NSNumber *value;
        NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
        [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
        [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        [priceFormatter setRoundingMode: NSNumberFormatterRoundDown];
        [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
        
        NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
        [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important
        [quantityFormatter setGroupingSeparator:@","];
        [quantityFormatter setGroupingSize:3];
        [quantityFormatter setGeneratesDecimalNumbers:YES];
        
        NSString *CellIdentifier = @"CellCompact";
        
        QuoteScreenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[QuoteScreenTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        int row = (int)indexPath.row;
        
        
        NSDictionary *detailDict = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
        NSString *stkNameStr;
        if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
            stkNameStr = [detailDict objectForKey:FID_130_S_SYSBOL_2];
        } else {
            stkNameStr = [detailDict objectForKey:FID_38_S_STOCK_NAME];
        }
        
        NSArray *stkNameDivide = [stkNameStr componentsSeparatedByString:@"."];
        
        NSString *stkCodeStr = [detailDict objectForKey:FID_33_S_STOCK_CODE];
        NSArray *stkCodeDivide = [stkCodeStr componentsSeparatedByString:@"."];
        NSString *stkCode = [stkCodeDivide objectAtIndex:0];
    
        cell.stkName.text = stkCode;
        
        
        //        stkName = [stkNameDivide objectAtIndex:0];
        //
        //        NSString *stkCodeStr = [detailDict objectForKey:FID_33_S_STOCK_CODE];
        //        NSArray *stkCodeDivide = [stkCodeStr componentsSeparatedByString:@"."];
        //        stkCode = [stkCodeDivide objectAtIndex:0];
        
        
        
        if ([detailDict.allKeys count]<=0) {
            // no feed data
            for(UIView *v in [cell.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                {
                    ((UILabel*)v).text = @"-";
                    ((UILabel*)v).textColor = [UIColor whiteColor];
                }
            }
            return cell;
        }
        
        if ([stkNameDivide count]>=2) {
        
            // Real Time or Delayed
            NSString *stkrd=[stkNameDivide objectAtIndex:1];
            if([stkrd containsString:@"D"]) {
                cell.lblRealofDelayed.text=[LanguageManager stringForKey:@"D"];
            }
            else
                cell.lblRealofDelayed.text=[LanguageManager stringForKey:@"R"];
            
        } else {
            cell.lblRealofDelayed.text= @"-";
        }
        
      
        
        
        // lblLastDone = LACP
        value = [detailDict objectForKey:FID_51_D_REF_PRICE];
        
        CGFloat lacpFloat = 0;
        lacpFloat = [[detailDict objectForKey:FID_51_D_REF_PRICE] floatValue];
    
        NSLog(@"lblLastDone value %.4f",lacpFloat);
        
       // lastDone = [priceFormatter stringFromNumber:[NSNumber numberWithFloat:lacpFloat]];
        
        if (lastDone==nil) {
            if ([UserPrefConstants singleton].pointerDecimal==4) {
                lastDone = [NSString stringWithFormat:@"%.4f",lacpFloat];
            }else{
                lastDone = [NSString stringWithFormat:@"%.3f",lacpFloat];
            }
        }
        
        cell.lblLastDone.text = lastDone;
        
        // lblDynamic1 = Volume
        value =[detailDict objectForKey:dynamicFID1];
        if(![self isPriceType:dynamicFID1])
            d1 = [quantityFormatter stringFromNumber:value];
        else
            d1 = [priceFormatter stringFromNumber:value];
        NSString *volume1 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID1]) {
            cell.lblDynamic1.text = volume1;
        }else{
            cell.lblDynamic1.text = d1;
        }
        float priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if ([[self Colorfortext:dynamicFID1 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblDynamic1.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID1 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblDynamic1.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID1 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblDynamic1.textColor=[UIColor whiteColor];
        }
        
        // lblDynamic2 = High
        value =[detailDict objectForKey:dynamicFID2];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if(![self isPriceType:dynamicFID2])
            d2 = [quantityFormatter stringFromNumber:value];
        else
            d2 = [priceFormatter stringFromNumber:value];
        
        NSString *volume2 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID2]) {
            cell.lblDynamic2.text = volume2;
        }else{
            cell.lblDynamic2.text = d2;
        }
        if ([[self Colorfortext:dynamicFID2 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblDynamic2.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID2 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblDynamic2.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID2 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblDynamic2.textColor=[UIColor whiteColor];
        }
        
        // lblDynamic3 = Low
        value =[detailDict objectForKey:dynamicFID3];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if(![self isPriceType:dynamicFID3])
            d3 = [quantityFormatter stringFromNumber:value];
        else
            d3 = [priceFormatter stringFromNumber:value];
        NSString *volume3 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID3]) {
            cell.lblDynamic3.text = volume3;
        }else{
            cell.lblDynamic3.text = d3;
        }
        if ([[self Colorfortext:dynamicFID3 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblDynamic3.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID3 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblDynamic3.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID3 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblDynamic3.textColor=[UIColor whiteColor];
        }
        
        // lblDynamic4 = Last
        value =[detailDict objectForKey:dynamicFID4];
        
        NSLog(@"lblDynamic4 %@",value);
        
        lacpFloat = [[detailDict objectForKey:FID_51_D_REF_PRICE] floatValue];
        
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if(![self isPriceType:dynamicFID4])
            d4 = [quantityFormatter stringFromNumber:value];
        else{
            
            d4 = [priceFormatter stringFromNumber:value];
            if (d4==nil) {
                if ([UserPrefConstants singleton].pointerDecimal==4) {
                    d4 = [NSString stringWithFormat:@"%@",value];
                }else{
                    d4 = [NSString stringWithFormat:@"%@",value];
                }
            }
        }
        
        
        NSString *volume4 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID4]) {
            cell.lblDynamic4.text = volume4;
        }else{
            cell.lblDynamic4.text = d4;
        }
        if ([[self Colorfortext:dynamicFID4 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblDynamic4.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID4 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblDynamic4.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID4 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblDynamic4.textColor=[UIColor whiteColor];
        }
        
        
        // lblDynamic5 = Open
        value =[detailDict objectForKey:dynamicFID5];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if(![self isPriceType:dynamicFID5])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume5 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID5]) {
            cell.lblDynamic5.text = volume5;
        }else{
            cell.lblDynamic5.text = d5;
        }
        if ([[self Colorfortext:dynamicFID5 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblDynamic5.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID5 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblDynamic5.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID5 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblDynamic5.textColor=[UIColor whiteColor];
        }
        
        
        
        
        value =[detailDict objectForKey:dynamicFID6];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
      
      //  NSLog(@"Value %@ price Change %",value,priceChanged);
        if(![self isPriceType:dynamicFID6])
           d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        

        NSString *volume6 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID6]) {
            cell.lblPar.text = volume6;
        }else{
            cell.lblPar.text = d5;
        }
        if ([[self Colorfortext:dynamicFID6 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblPar.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID6 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblPar.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID6 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblPar.textColor=[UIColor whiteColor];
        }
        
        
        
        value =[detailDict objectForKey:dynamicFID7];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if(![self isPriceType:dynamicFID7])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume7 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID7]) {
            cell.lblEps.text = volume7;
        }else{
            cell.lblEps.text = d5;
        }
        
        // Change
        value =[detailDict objectForKey:dynamicFID9];
        priceChanged = [value doubleValue];
        if ([[self Colorfortext:dynamicFID9 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblChange.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID9 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblChange.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID9 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblChange.textColor=[UIColor whiteColor];
        }

        d5 = [priceFormatter stringFromNumber:value];
        if (priceChanged>0) {
            cell.lblChange.text = d5;
        } else if (priceChanged<0) {
            cell.lblChange.text = d5;
        } else {
            cell.lblChange.text = d5;
        }
        
        
        
        
        
        value =[detailDict objectForKey:dynamicFID8];
        priceChanged = [value doubleValue];
        
        cell.lblPerChg.textColor = cell.lblChange.textColor;
        d5 = [priceFormatter stringFromNumber:value];
        
        if (priceChanged>0) {
            cell.lblPerChg.text = [NSString stringWithFormat:@"%@%%",d5];
        } else if (priceChanged<0) {
            cell.lblPerChg.text = [NSString stringWithFormat:@"%@%%",d5];
        } else {
            cell.lblPerChg.text = [NSString stringWithFormat:@"%@%%",d5];
        }
        
        NSString *exchangeValue =[NSString stringWithFormat:@"%@",[detailDict objectForKey:dynamicFID18]];
        //NSString *remarksValue =[NSString stringWithFormat:@"%@",[detailDict objectForKey:dynamicFID19]];
        
       // NSLog(@"detailDict %@",detailDict);
        cell.lblExchange.text = exchangeValue.length == 0 ? @"": exchangeValue;
        
       // cell.lblRemarks.text =remarksValue.length == 0 ? @"": remarksValue;
        

        
        
        
        value =[detailDict objectForKey:dynamicFID10];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if ([[self Colorfortext:dynamicFID10 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblAskQty.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID10 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblAskQty.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID10 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblAskQty.textColor=[UIColor whiteColor];
        }
        
        
        if(![self isPriceType:dynamicFID10])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume10 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID10]) {
            cell.lblAskQty.text = volume10;
        }else{
            cell.lblAskQty.text = d5;
        }
        
        
        
        // lblAsk = Ask Price
        value =[detailDict objectForKey:dynamicFID11];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if ([[self Colorfortext:dynamicFID11 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblAsk.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID11 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblAsk.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID11 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblAsk.textColor=[UIColor whiteColor];
        }
        if(![self isPriceType:dynamicFID11])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume11 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID11]) {
            cell.lblAsk.text = volume11;
        }else{
            cell.lblAsk.text = d5;
        }
        
        
        // lblBid = Bid price
        value =[detailDict objectForKey:dynamicFID12];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if ([[self Colorfortext:dynamicFID12 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblBid.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID12 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblBid.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID12 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblBid.textColor=[UIColor whiteColor];
        }
        if(![self isPriceType:dynamicFID12])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume12 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID12]) {
            cell.lblBid.text = volume12;
        }else{
            cell.lblBid.text = d5;
        }
        
        // lblBidQty = Bid Qty
        value =[detailDict objectForKey:dynamicFID13];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if ([[self Colorfortext:dynamicFID13 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblBidQty.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID13 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblBidQty.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID13 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblBidQty.textColor=[UIColor whiteColor];
        }
        if(![self isPriceType:dynamicFID13])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume13 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID13]) {
            cell.lblBidQty.text = volume13;
        }else{
            cell.lblBidQty.text = d5;
        }
        
        
        value =[detailDict objectForKey:dynamicFID14];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        
        if ([[self Colorfortext:dynamicFID14 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblClose.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID14 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblClose.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID14 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblClose.textColor=[UIColor whiteColor];
        }
        
        if(![self isPriceType:dynamicFID14])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume14 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID14]) {
            cell.lblClose.text = volume14;
        }else{
            cell.lblClose.text = d5;
        }
        
        value =[detailDict objectForKey:dynamicFID15];
        priceChanged = [value doubleValue]-[[detailDict objectForKey:FID_51_D_REF_PRICE]doubleValue];
        if ([[self Colorfortext:dynamicFID15 :priceChanged] isEqualToString:@"greenColor"]) {
            cell.lblTop.textColor=[UIColor greenColor];
        }
        else if ([[self Colorfortext:dynamicFID15 :priceChanged] isEqualToString:@"redColor"]) {
            cell.lblTop.textColor=[UIColor redColor];
        }
        else if ([[self Colorfortext:dynamicFID15 :priceChanged] isEqualToString:@"grayColor"]) {
            cell.lblTop.textColor=[UIColor whiteColor];
        }
        
        
        if(![self isPriceType:dynamicFID15])
            d5 = [quantityFormatter stringFromNumber:value];
        else
            d5 = [priceFormatter stringFromNumber:value];
        NSString *volume15 = [[UserPrefConstants singleton] abbreviateNumber:[value longLongValue]];
        if (![self isPriceType:dynamicFID15]) {
            cell.lblTop.text = volume15;
        }else{
            cell.lblTop.text = d5;
        }
        
        
        cell.lblTp.text = [detailDict objectForKey:dynamicFID16];
        
        
        
        ////NSLog(@"%@,%@,%@,%@,%@",dynamicFID1,dynamicFID2,dynamicFID3,dynamicFID4,dynamicFID5 );
        //Draw Yellow Color
        if([dynamicFID1 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID1 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID1 isEqualToString:FID_127_D_EPS]||[dynamicFID1 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID1 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblDynamic1.textColor = [UIColor yellowColor];
        }
        if([dynamicFID2 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID2 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID2 isEqualToString:FID_127_D_EPS]||[dynamicFID2 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID2 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblDynamic2.textColor = [UIColor yellowColor];
        }
        if([dynamicFID3 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID3 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID3 isEqualToString:FID_127_D_EPS]||[dynamicFID3 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID3 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblDynamic3.textColor = [UIColor yellowColor];
        }
        if([dynamicFID4 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID4 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID4 isEqualToString:FID_127_D_EPS]||[dynamicFID4 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID4 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblDynamic4.textColor = [UIColor yellowColor];
        }
        if([dynamicFID5 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID5 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID5 isEqualToString:FID_127_D_EPS]||[dynamicFID5 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID5 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblDynamic5.textColor = [UIColor yellowColor];
        }
        if([dynamicFID6 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID6 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID6 isEqualToString:FID_127_D_EPS]||[dynamicFID6 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID6 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblPar.textColor = [UIColor yellowColor];
        }
        if([dynamicFID7 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID7 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID7 isEqualToString:FID_127_D_EPS]||[dynamicFID7 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID7 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblEps.textColor = [UIColor yellowColor];
        }
        if([dynamicFID8 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID8 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID8 isEqualToString:FID_127_D_EPS]||[dynamicFID8 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID8 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblTradeVal.textColor = [UIColor yellowColor];
        }
        if([dynamicFID9 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID9 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID9 isEqualToString:FID_127_D_EPS]||[dynamicFID9 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID9 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblChange.textColor = [UIColor yellowColor];
        }
        if([dynamicFID10 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID10 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID10 isEqualToString:FID_127_D_EPS]||[dynamicFID10 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID10 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblAskQty.textColor = [UIColor yellowColor];
        }
        if([dynamicFID11 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID11 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID11 isEqualToString:FID_127_D_EPS]||[dynamicFID11 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID11 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblAsk.textColor = [UIColor yellowColor];
        }
        if([dynamicFID12 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID12 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID12 isEqualToString:FID_127_D_EPS]||[dynamicFID12 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID12 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblBid.textColor = [UIColor yellowColor];
        }
        if([dynamicFID13 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID13 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID13 isEqualToString:FID_127_D_EPS]||[dynamicFID13 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID13 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblBidQty.textColor = [UIColor yellowColor];
        }
        if([dynamicFID14 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID14 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID14 isEqualToString:FID_127_D_EPS]||[dynamicFID14 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID14 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblClose.textColor = [UIColor yellowColor];
        }
        if([dynamicFID15 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID15 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID15 isEqualToString:FID_127_D_EPS]||[dynamicFID15 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID15 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblTop.textColor = [UIColor yellowColor];
        }
        if([dynamicFID16 isEqualToString:FID_101_I_VOLUME]|| [dynamicFID16 isEqualToString:FID_123_D_PAR_VALUE]||[dynamicFID16 isEqualToString:FID_127_D_EPS]||[dynamicFID16 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID16 isEqualToString:FID_58_I_BUY_QTY_1])
        {
            cell.lblTp.textColor = [UIColor yellowColor];
        }
        
        //    float priceChanged = [[detailDict objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue] - [[detailDict objectForKey:FID_50_D_CLOSE]floatValue];
        priceChanged = [[detailDict objectForKey:FID_132_I_TOT_OF_TRD]longLongValue];
        chg = [[UserPrefConstants singleton] abbreviateNumber:priceChanged];
        
        
        cell.lblTrades.text =chg;
        //cell.lblTrades.text = [[detailDict objectForKey:FID_132_I_TOT_OF_TRD] stringValue];
        
        //    priceChanged = priceChanged / [[detailDict objectForKey:FID_50_D_CLOSE]floatValue] * 100;
        priceChanged = [[detailDict objectForKey:FID_102_D_VALUE]doubleValue] ;
        
        chgP = [[UserPrefConstants singleton] abbreviateNumber:priceChanged];
        cell.lblTradeVal.text = chgP; //  [NSString stringWithFormat:@"%0f",priceChanged];
        
        
        //        if (priceChanged >0)
        //        {
        //            // chgP= [@"+" stringByAppendingString:chgP];
        //            cell.imgPriceChange.image = [UIImage imageNamed:@"quoteUp.png"];
        //            //[cell.lblLastDone setTextColor:[UIColor greenColor]];
        //        }
        //        else if(priceChanged <0)
        //        {
        //            // chgP= [@"-" stringByAppendingString:chgP];
        //            cell.imgPriceChange.image = [UIImage imageNamed:@"quoteDown.png"];
        //            //[cell.lblLastDone setTextColor:[UIColor redColor]];
        //        }
        //        else
        //        {
        //            cell.imgPriceChange.image = [UIImage imageNamed:@"grayBox.png"];
        //            [cell.lblLastDone setTextColor:[UIColor whiteColor]];
        //        }
        
        
        // iterate all cell labels and if label is = 0.000, then  change to whitecolor
        for(UIView *v in [cell.contentView subviews])
        {
            if([v isKindOfClass:[UILabel class]])
            {
                UILabel *lbl = (UILabel*)v;
                if ([lbl.text isEqualToString:@"0.000"]||[lbl.text isEqualToString:@"0.000%"]||
                    [lbl.text isEqualToString:@"0"]) {
                    
                    // lbl.text = @"0/-";
                    lbl.textColor = [UIColor whiteColor];
                }
            }
        }
        
        
        NSDictionary *tmpDict = [_tempDict objectForKey:[sortedArr objectAtIndex:row]];
        
        if (isUpdate) {
            
            if([dynamicFID1 isEqualToString:FID_101_I_VOLUME]||[dynamicFID1 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID1 isEqualToString:FID_58_I_BUY_QTY_1])
                [self checkQuantity:detailDict withTmpDict:tmpDict andRow:row andCell:cell];
            else
                [self checkPrice:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            
            if([dynamicFID2 isEqualToString:FID_101_I_VOLUME]||[dynamicFID2 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID2 isEqualToString:FID_58_I_BUY_QTY_1])
                [self checkQuantity:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            else
                [self checkPrice:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            
            if([dynamicFID3 isEqualToString:FID_101_I_VOLUME]||[dynamicFID3 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID3 isEqualToString:FID_58_I_BUY_QTY_1])
                [self checkQuantity:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            else
                [self checkPrice:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            
            if([dynamicFID4 isEqualToString:FID_101_I_VOLUME]||[dynamicFID4 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID4 isEqualToString:FID_58_I_BUY_QTY_1])
                [self checkQuantity:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            else
                [self checkPrice:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            
            if([dynamicFID5 isEqualToString:FID_101_I_VOLUME]||[dynamicFID5 isEqualToString:FID_78_I_SELL_QTY_1]||[dynamicFID5 isEqualToString:FID_58_I_BUY_QTY_1])
                [self checkQuantity:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            else
                [self checkPrice:detailDict withTmpDict:tmpDict  andRow:row andCell:cell];
            
        }
        
        if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
            isUpdate = NO;
        }
        
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(50/255.0) blue:(50/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
       
        return cell;
    }
    
    // MARK: FRONT TABLEVIEW
    else if (tableView.tag==10) {
        static NSString *CellIdentifier = @"Cell2";
        StockNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[StockNameTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        NSInteger row = indexPath.row;
        NSDictionary *detailDict = [_filteredDict objectForKey:[sortedArr objectAtIndex:row]];
        
        if ([detailDict.allKeys count]<=0) {
            // no feed data
            [cell.labelStockArrow setHidden:YES];
            cell.labelStockName.text = [LanguageManager stringForKey:No_Data];
            cell.labelStockCode.text = [sortedArr objectAtIndex:row];
            return cell;
        }
        
        
        NSString *stkNameStr = @"";
        if ([[LanguageManager defaultManager].language isEqualToString:@"cn"]) {
            stkNameStr = [detailDict objectForKey:FID_130_S_SYSBOL_2];
        } else {
            stkNameStr = [detailDict objectForKey:FID_38_S_STOCK_NAME];
        }
        
        NSArray *stkNameDivide = [stkNameStr componentsSeparatedByString:@"."];
        NSString *stkName = [stkNameDivide objectAtIndex:0];
        
        cell.labelStockName.text = stkName;
        NSString *stkCodeStr = [detailDict objectForKey:FID_33_S_STOCK_CODE];
        NSArray *stkCodeDivide = [stkCodeStr componentsSeparatedByString:@"."];
        NSString *stkCode = [stkCodeDivide objectAtIndex:0];
        cell.labelStockCode.text = stkCode;
        
        
        if (row==selectedRow) {
            [cell.labelStockArrow setHidden:NO];
        } else {
            [cell.labelStockArrow setHidden:YES];
        }
        
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor colorWithRed:(50/255.0) green:(50/255.0) blue:(50/255.0) alpha:1];
        cell.selectedBackgroundView = selectionColor;
        
        return cell;
        
        // MARK: WATCHLIST
        
    } else if (tableView.tag==12) {
        
        static NSString *cellIdentifier = @"watchListCellID";
        long row=indexPath.row;
        WLTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[WLTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *firstWL = [_watchListArray objectAtIndex:row];

        cell.watchListName.text=[firstWL objectForKey:@"Name"];
        cell.watchListName.backgroundColor = [UIColor clearColor];
        
        if (row==selectedWLRow) {
            [cell.watchListName setTextColor:[UIColor cyanColor]];
            [cell.watchListArrow setHidden:NO];
        } else {
            [cell.watchListName setTextColor:[UIColor whiteColor]];
            [cell.watchListArrow setHidden:YES];
        }
        
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = selectionColor;
        
        
        return cell;
    }
    
    return nil;
}

-(BOOL)isPriceType :(NSString *)str
{
    if([str isEqualToString:FID_101_I_VOLUME]||[str isEqualToString:FID_78_I_SELL_QTY_1]||[str isEqualToString:FID_58_I_BUY_QTY_1])
        return NO;
    
    return YES;
}



-(IBAction)chooseColumnButtonTapped:(UIButton *)sender
{
    //    selectedButtonTag = sender.tag;
    //
    //    if (_columnPicker == nil)
    //    {
    //        _columnPicker = [[DynamicColumnSelectorViewController alloc] initWithStyle:UITableViewStylePlain];
    //        _columnPicker.delegate = self;
    //    }
    //
    //    //Show column picker
    //    if (_columnPickerPopover == nil)
    //    {
    //        _columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:_columnPicker];
    //        [_columnPickerPopover presentPopoverFromRect:sender.frame
    //                                              inView:_tableView
    //                            permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    //    }
    //    else
    //    {
    //        //The color picker popover is showing. Hide it.
    //        [_columnPickerPopover dismissPopoverAnimated:YES];
    //        _columnPickerPopover = nil;
    //    }
}

#pragma mark - Watchlist Items Notifications Received


-(void) addWatchListItemsReply:(NSNotification *)notification {
    
    NSDictionary *dict = notification.userInfo;
    NSArray *replyArr = [dict objectForKey:@"replyArr"];
    

    if (replyArr) {
        
        NSString *someDuplicate = [dict objectForKey:@"DuplicateStocks"];
        
        if ([someDuplicate length]>0) {
            
            [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:SomeStockAlreadyAddedError withPlaceholders:@{@"%stkCode%":someDuplicate}]
                                         inController:self withTitle:[LanguageManager stringForKey:Done]];
        } else {
            
            [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:Stock_has_been_successfully_added_]
                                         inController:self withTitle:[LanguageManager stringForKey:Done]];
            
        }
        
        if (isWatchList) {
            NSIndexPath *currentWL = [NSIndexPath indexPathForRow:selectedWLRow inSection:0];
            [self tableView:_watchlistTable didSelectRowAtIndexPath:currentWL];
            [atp getWatchListItems:watchListID];
            [UserPrefConstants singleton].quoteScreenFavID=[NSNumber numberWithInt:watchListID];
        }
            
        
    } else  {
        // NoData, Error, TimedOut
        NSString *replyStr = [dict objectForKey:@"replyDict"];
        
        if ([replyStr isEqualToString:@"NoData"]) {
            
        } else if ([replyStr isEqualToString:@"Error"]) {
            
        } else if ([replyStr isEqualToString:@"TimedOut"]) {
            
        } else if ([replyStr isEqualToString:@"AllDuplicate"]) {
            
            NSString *duplicates = [dict objectForKey:@"DuplicateStocks"];
            
            [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"StockAlreadyAddedError" withPlaceholders:@{@"%stkCode%":duplicates}]
                                         inController:self withTitle:@"Duplicate Stock"];
        }
    }
    
}





-(void)deleteWatchListSuccessful {
    
    [atp getWatchList];
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:[LanguageManager stringForKey:@"Done"]
                                  message:[LanguageManager stringForKey:@"WatchList has been successfully deleted."]
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:[LanguageManager stringForKey:@"Ok"]
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}



-(void)latestWatchListReceived {
    
    if (isWatchList)  {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // get the latest watchlist and reload into data
        self.watchListArray = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
        
        NSLog(@"new list received %@",self.watchListArray);
        
        [_watchlistTable reloadData];
        
        if ([_watchListArray count]>0){
            // select first watchlist
            NSDictionary *firstWL = [_watchListArray objectAtIndex:0];
            int favID = [[firstWL objectForKey:@"FavID"] intValue];
            [UserPrefConstants singleton].prevWatchlistFavID = favID;
            [UserPrefConstants singleton].watchListName = [firstWL objectForKey:@"Name"];
            watchListID = favID;
            [self tableView:_watchlistTable didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            [atp getWatchListItems:favID];
            [UserPrefConstants singleton].quoteScreenFavID=[NSNumber numberWithInt:favID];
        
        } else {
            [loadingBtn stopAnimating];
            //
            
        }
    });
        
    }
    
}

- (void)addOrUpdateWatchlistNameSuccessfully
{
    [atp getWatchList];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:[LanguageManager stringForKey:@"Done"]
                                  message:[LanguageManager stringForKey:@"WatchList has been successfully updated."]
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:[LanguageManager stringForKey:@"Ok"]
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)deleteWatchlistItemSuccessfully:(NSNotification*)notify
{
    
    [atp getWatchListItems:watchListID];
    
    NSString *message = @"";
    
    if (notify==nil) {
    
        [atp getWatchListItems:watchListID];
        message = [LanguageManager stringForKey:Stock_has_been_successfully_deleted__];
    
    } else {
        message = [notify.userInfo objectForKey:@"message"];
        if ([message length]<=0) {
            message = [LanguageManager stringForKey:Stock_has_been_successfully_deleted__];
        }
    }
    
    [UserPrefConstants singleton].watchlistFavID = watchListID;
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:[LanguageManager stringForKey:Done]
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:[LanguageManager stringForKey:Ok]
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                             
                         }];
    
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)didReceivedWatchListItems:(NSNotification *)notification
{
    // load watchlist stocks
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [_filteredDict removeAllObjects];
        [_tempDict removeAllObjects];
        
        [_bufferUpdateRow removeAllObjects];
        [_bufferUpdateStocks removeAllObjects];
        
        self.selectedArr=[[NSArray alloc]initWithArray:_userPref.userWatchListStockCodeArr];
        
        // No need to filter for watchlist, even stock has no feed, we need to show it's in the watchlist
        // June 2017- Follow TCPLus, if there are no feed no need show
        for (NSString *stkCode in _selectedArr)
        {
            if (([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
                
                NSMutableDictionary *dictToAdd = [[NSMutableDictionary alloc] initWithDictionary:
                                           [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];
                [_filteredDict setObject:dictToAdd forKey:stkCode];
                
                // add field data if not exist (it is used to sort in quote screen
                // price or qty set to 0
                // this is critical because these fields will be used in sorting
                // if it does not exist, sorting will not work properly.
                
                if ([dictToAdd objectForKey:FID_101_I_VOLUME]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_101_I_VOLUME];
                }
                
                // gainer/loser
                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE];
                }
                
                // gainer/loser %
                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE_PER]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE_PER];
                }
                
                // name
                if ([dictToAdd objectForKey:FID_38_S_STOCK_NAME]==nil) {
                    [dictToAdd setObject:@"-" forKey:FID_38_S_STOCK_NAME];
                }
                
                // trades
                if ([dictToAdd objectForKey:FID_132_I_TOT_OF_TRD]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_132_I_TOT_OF_TRD];
                }
                
                //value
                if ([dictToAdd objectForKey:FID_102_D_VALUE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_102_D_VALUE];
                }
                
                //close
                if ([dictToAdd objectForKey:FID_50_D_CLOSE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_50_D_CLOSE];
                }
                
                //lacp
                if ([dictToAdd objectForKey:FID_51_D_REF_PRICE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_51_D_REF_PRICE];
                }
                
                // bid qty
                if ([dictToAdd objectForKey:FID_58_I_BUY_QTY_1]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_58_I_BUY_QTY_1];
                }
                
                
            }
        }
        self.tempDict= [[NSMutableDictionary alloc]initWithDictionary:_filteredDict copyItems:YES];
        
        [loadingBtn stopAnimating];
        if ([_filteredDict count]<=0) {
            [_noResultsMsgBtn setHidden:NO];
        } else {
            [_noResultsMsgBtn setHidden:YES];
        }
        
        [self sortData];
        
        if ([_cardCollectionView isHidden]) {
            [_tableView reloadData];
            [_frontTableView reloadData];
        } else {
            [[UserPrefConstants singleton].imgChartCache removeAllObjects];
            [_detailPageDict removeAllObjects];
            [_cardCollectionView reloadData];
        }

        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"halfSecondTimerTick" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTimerCallBack)
                                                     name:@"halfSecondTimerTick" object:nil];
        
//        if (updateTimer!=nil) {
//            [updateTimer invalidate];
//            updateTimer = nil;
//        }
//        
//        updateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerCallBack)
//                                                     userInfo:nil repeats:YES];
        
    });
}

#pragma mark - IBActions
// This is called when Header title is pressed

-(IBAction)sortButtonPressed:(UIButton *)sender
{
    [atp timeoutActivity];
    
    
    
    switch (sender.tag) {
        case 0:
            sortedByFID = FID_38_S_STOCK_NAME;
            break;
        case 1:
            sortedByFID = FID_51_D_REF_PRICE;
            break;
        case 2:
            sortedByFID = FID_132_I_TOT_OF_TRD;
            break;
        case 3:
            sortedByFID = dynamicFID1;
            break;
        case 4:
            sortedByFID = dynamicFID2;
            break;
        case 5:
            sortedByFID = dynamicFID3;
            break;
        case 6:
            sortedByFID = dynamicFID4;
            break;
        case 7:
            sortedByFID = dynamicFID5;
            break;
        case 8:
            sortedByFID = dynamicFID6;
            break;
        case 9:
            sortedByFID = dynamicFID7;
            break;
        case 10:
            sortedByFID = dynamicFID8;
            break;
        case 11:
            sortedByFID = dynamicFID9;
            break;
        case 12:
            sortedByFID = dynamicFID10;
            break;
        case 13:
            sortedByFID = dynamicFID11;
            break;
        case 14:
            sortedByFID = dynamicFID12;
            break;
        case 15:
            sortedByFID = dynamicFID13;
            break;
        case 16:
            sortedByFID = dynamicFID14;
            break;
        case 17:
            sortedByFID = dynamicFID15;
            break;
        case 18:
            sortedByFID = dynamicFID16;
            break;
        case 19:
            sortedByFID = dynamicFID17;
            break;
            
        default:
            break;
    }
    isDecending =!isDecending;
    
    [self sortData];
    
    if ([_cardCollectionView isHidden]) {
        [_tableView reloadData];
        [_frontTableView reloadData];
    } else {
        // [_imgChartCache removeAllObjects]; sorting by header no need to clear chart
        [_cardCollectionView reloadData];
    }
}



#pragma mark -
#pragma mark Postnotification
//====================================================================
//                        Postnotification returned
//====================================================================




- (void)didUpdate:(NSNotification *)notification
{
    
    // NSLog(@"update");
    
    NSString * response = [notification object];
    
    ///    NSLog(@"update %@", response);
    
    //NSLog(@"qcdatafeed %@",[[[QCData singleton] qcFeedDataDict] objectForKey:[selectedArr objectAtIndex:0]]);
    
    if ([_filteredDict objectForKey:response]!=nil)
    {
        
        long indexToReload = [sortedArr indexOfObject:response];
        
        if (indexToReload!=NSNotFound) {            
            
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:indexToReload inSection:0];
            
            if (![_bufferUpdateRow containsObject:rowToReload]) {
                [_bufferUpdateRow addObject:rowToReload];
            }
            
            if (![_bufferUpdateStocks containsObject:response]) {
                [_bufferUpdateStocks addObject:response];
            }
        }
    }
    
}


// this ensures a good update flashes every 0.5s
-(void)updateTimerCallBack {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // RESIZING
        if (([UserPrefConstants singleton].isCollectionView)&&(collectionViewResized)&&([_filteredDict count]>0)) {
            collectionViewResized = NO;
            // update a collectionview item
            NSIndexPath *firstItem = [NSIndexPath indexPathForRow:0 inSection:0];
            [_cardCollectionView performBatchUpdates:^{
                [_cardCollectionView reloadItemsAtIndexPaths:@[firstItem]];
            } completion:^(BOOL finished) {}];
            
        }
        
        if ([_bufferUpdateRow count]>0) {
            
            for (int i=0; i<[_bufferUpdateStocks count]; i++) {
                
                NSString *response = [_bufferUpdateStocks objectAtIndex:i];
                
                // BEFORE UPDATE FILTERDICT, COPY TO TEMPDICT
                NSDictionary *dictToCopy = [[NSDictionary alloc] initWithDictionary:[_filteredDict objectForKey:response] copyItems:YES];
                [_tempDict setObject:dictToCopy forKey:response];
                
                // UPDATE WITH LATEST DATA BEING PUSHED
                NSDictionary *dictToUpdate = [[NSDictionary alloc] initWithDictionary:[[[QCData singleton] qcFeedDataDict] objectForKey:response] copyItems:YES];
                [_filteredDict setObject:dictToUpdate forKey:response];
                
                // update stock in Preview if displayed
                if (([response isEqualToString:stkSelected])&&(stkContainerDisplayed)) {
                    [self.delegate qcFeedDidUpdateForSelectedStock];
                    
                }
                
            }
            
            
            
            
            
            if (![UserPrefConstants singleton].isCollectionView) {
                // update normal quote screen
                
                [_bufferUpdateRow enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSIndexPath *eachPath = (NSIndexPath*)obj;
                    if ([_tableView.indexPathsForVisibleRows containsObject:eachPath]) {
                        isUpdate=YES;
                        [_tableView reloadRowsAtIndexPaths:@[eachPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                }];
                
                
                // stock name dont need to reload!
                //[_frontTableView reloadRowsAtIndexPaths:bufferUpdateRow withRowAnimation:UITableViewRowAnimationNone];
                
            } else {
                // update card quote screen
                
                [_bufferUpdateRow enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSIndexPath *eachPath = (NSIndexPath*)obj;
                    if ([_cardCollectionView.indexPathsForVisibleItems containsObject:eachPath]) {
                        isCollectionUpdate=YES;
                        [_cardCollectionView performBatchUpdates:^{
                            [_cardCollectionView reloadItemsAtIndexPaths:@[eachPath]];
                        } completion:^(BOOL finished) {}];
                    }
                    
                }];
            }
            [_bufferUpdateRow removeAllObjects];
            [_bufferUpdateStocks removeAllObjects];
            
        }
    });
}




-(void)sortData {
    
    if (isDecending) {
        sortedArr = [_filteredDict keysSortedByValueUsingComparator:
                     ^NSComparisonResult(NSDictionary* obj1, NSDictionary* obj2)
                     {

                         NSComparisonResult result = NSOrderedSame;
                         
                         if (obj1[sortedByFID] != nil && obj2[sortedByFID]!=nil) {
                             
                             result = [obj2[sortedByFID] compare:obj1[sortedByFID]];
                             
                             if (result != NSOrderedSame) {
                                 
                                 return result;
                                 
                             }
                             else {
                                  if (obj1[FID_38_S_STOCK_NAME] != nil && obj2[FID_38_S_STOCK_NAME]!=nil) {
                                      return [obj1[FID_38_S_STOCK_NAME] compare:obj2[FID_38_S_STOCK_NAME]];
                                  } else {
                                      result = NSOrderedSame;
                                  }
                             }
                         }
                         
                         return result;
                         
                     }];
        
    }
    else
    {
        sortedArr = [_filteredDict keysSortedByValueUsingComparator:
                     ^NSComparisonResult(NSDictionary* obj1, NSDictionary* obj2)
                     {

                         
                         NSComparisonResult result = NSOrderedSame;
                         
                         if (obj1[sortedByFID] != nil && obj2[sortedByFID]!=nil) {
                             
                             result = [obj1[sortedByFID] compare:obj2[sortedByFID]];
                             
                             if (result != NSOrderedSame) {
                                 return result;
                             } else {
                                 if (obj1[FID_38_S_STOCK_NAME] != nil && obj2[FID_38_S_STOCK_NAME]!=nil) {
                                     return [obj1[FID_38_S_STOCK_NAME] compare:obj2[FID_38_S_STOCK_NAME]];
                                 } else {
                                     result = NSOrderedSame;
                                 }
                             }
                         }
                         
                         return result;


                     }];
    }
}





- (void)didFinishedSortByServer:(NSNotification *)notification
{
    //NSLog(@"finishSort");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [self openStockContainer:NO animated:NO];
        
        [_filteredDict removeAllObjects];
        [_tempDict removeAllObjects];
        
        [_bufferUpdateRow removeAllObjects];
        [_bufferUpdateStocks removeAllObjects];
        
        // NSLog(@"sortedByFID %d",sortRow);
        // UPDATE THE PULL TO LOAD OBJECTS
        
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
        _frontTableView.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
        _cardCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 0.0, 0);
        
        _tableView.contentOffset = CGPointMake(0, 0);
        _frontTableView.contentOffset = CGPointMake(0, 0);
        _cardCollectionView.contentOffset = CGPointMake(0, 0);
        
        [_customPullLabel setHidden:NO];
        [_pullImg1 setHidden:NO];[_pullImg2 setHidden:NO];
        [_customPullActivity stopAnimating];
        if ([UserPrefConstants singleton].quoteScrPage==0) _customPullContainer.hidden = YES;
        else _customPullContainer.hidden = NO;
        
        // if sort by Loser/Loser%, we must do ascending sort because we need to have
        // lowest at top (top loser= the most losing at top) AND also by name
        
        if ((sortRow==3)||(sortRow==4)||(sortRow==5)) {
            isDecending = NO;
            // otherwise do normal descending
        } else {
            isDecending = YES;
        }
        
        NSDictionary *response = [notification.userInfo copy];
        NSMutableArray *sortByServerArr = [[NSMutableArray alloc]initWithArray:[response allKeys] copyItems:YES];
        // NSLog(@"sortByServerArr %@", sortByServerArr);
        
        [UserPrefConstants singleton].userSelectingStockCodeArr = sortByServerArr;
        _selectedArr = [[NSArray alloc] initWithArray:sortByServerArr];
        
        for (NSString *stkCode in _selectedArr) {
            if(([[[QCData singleton] qcFeedDataDict] objectForKey:stkCode])&&([stkCode length]>0)) {
                
                NSMutableDictionary *dictToAdd = [[NSMutableDictionary alloc] initWithDictionary:
                                           [[[QCData singleton] qcFeedDataDict] objectForKey:stkCode] copyItems:YES];
                
                // add field data if not exist (it is used to sort in quote screen
                // price or qty set to 0
                // this is critical because these fields will be used in sorting
                // if it does not exist, sorting will not work properly.
                
                if ([dictToAdd objectForKey:FID_101_I_VOLUME]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_101_I_VOLUME];
                }
                
                // gainer/loser
                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE];
                }
                
                // gainer/loser %
                if ([dictToAdd objectForKey:FID_CUSTOM_F_CHANGE_PER]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_CUSTOM_F_CHANGE_PER];
                }
                
                // name
                if ([dictToAdd objectForKey:FID_38_S_STOCK_NAME]==nil) {
                    [dictToAdd setObject:@"-" forKey:FID_38_S_STOCK_NAME];
                }
                
                // trades
                if ([dictToAdd objectForKey:FID_132_I_TOT_OF_TRD]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_132_I_TOT_OF_TRD];
                }
                
                //value
                if ([dictToAdd objectForKey:FID_102_D_VALUE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_102_D_VALUE];
                }
                
                //close
                if ([dictToAdd objectForKey:FID_50_D_CLOSE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_50_D_CLOSE];
                }
                
                //lacp
                if ([dictToAdd objectForKey:FID_51_D_REF_PRICE]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithDouble:0] forKey:FID_51_D_REF_PRICE];
                }
                
                // bid qty
                if ([dictToAdd objectForKey:FID_58_I_BUY_QTY_1]==nil) {
                    [dictToAdd setObject:[NSNumber numberWithInt:0] forKey:FID_58_I_BUY_QTY_1];
                }
                
                
                [_filteredDict setObject:dictToAdd forKey:stkCode];
            }
        }
        
        
        
        self.tempDict= [[NSMutableDictionary alloc]initWithDictionary:_filteredDict copyItems:YES];
       // NSLog(@"filteredDict %@", filteredDict);
        
        // sort MUST be here! (previously sort in cellForRow = fail)
        [self sortData];
        
        if ([_filteredDict count]<=0) {
            [_noResultsMsgBtn setHidden:NO];
        } else {
            [_noResultsMsgBtn setHidden:YES];
        }
        
        [loadingBtn stopAnimating];
        [self setQuoteScreenTitle:[_tempDict count]];
        
        if ([_cardCollectionView isHidden]) {
            [_tableView reloadData];
            [_frontTableView reloadData];
        } else {
            [[UserPrefConstants singleton].imgChartCache removeAllObjects];
            [_detailPageDict removeAllObjects];
            [_cardCollectionView reloadData];
        }
        
        if ([_filteredDict count]<=0) {
            [_sortBtnContainer setHidden:YES];
        } else {
            [_sortBtnContainer setHidden:NO];
        }
        
//        if (updateTimer!=nil) {
//            [updateTimer invalidate];
//            updateTimer = nil;
//        }
//        
//        
//        
//        updateTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerCallBack)
//                                                     userInfo:nil repeats:YES];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"halfSecondTimerTick" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTimerCallBack)
                                                     name:@"halfSecondTimerTick" object:nil];
        
    });
}




#pragma mark - Others

-(NSString *)Colorfortext:(NSString *)dynamicfid : (float)pricechanged
{
    
    if (([dynamicfid isEqualToString:FID_57_D_LOW_PRICE]||[dynamicfid isEqualToString:FID_CUSTOM_F_CHANGE_PER] ||[dynamicfid isEqualToString:FID_98_D_LAST_DONE_PRICE]||[dynamicfid isEqualToString:FID_55_D_OPEN_PRICE]||[dynamicfid isEqualToString:FID_56_D_HIGH_PRICE]||[dynamicfid isEqualToString:FID_CUSTOM_F_CHANGE]||[dynamicfid isEqualToString:FID_88_D_SELL_PRICE_1]||[dynamicfid isEqualToString:FID_68_D_BUY_PRICE_1]||[dynamicfid isEqualToString:FID_153_D_THEORETICAL_PRICE]) && pricechanged >0)
        return @"greenColor";
    else if (([dynamicfid isEqualToString:FID_57_D_LOW_PRICE]||[dynamicfid isEqualToString:FID_CUSTOM_F_CHANGE_PER] ||[dynamicfid isEqualToString:FID_98_D_LAST_DONE_PRICE]||[dynamicfid isEqualToString:FID_55_D_OPEN_PRICE]||[dynamicfid isEqualToString:FID_56_D_HIGH_PRICE]||[dynamicfid isEqualToString:FID_CUSTOM_F_CHANGE]||[dynamicfid isEqualToString:FID_88_D_SELL_PRICE_1]||[dynamicfid isEqualToString:FID_68_D_BUY_PRICE_1]||[dynamicfid isEqualToString:FID_153_D_THEORETICAL_PRICE]) && pricechanged <0)
        return @"redColor";
    else
        return @"grayColor";
    
    
}
- (void) setupRankedFirstTime{
    NSArray *brokerListNameArr = _rankedByNameArr;
    NSArray *brokerListKeyArr = _rankedByKeyArr;
    
    
    ////NSLog(@"broker List Key Arr %@",brokerListKeyArr);
    
    if ([UserPrefConstants singleton].sortByProperty==NULL) {
        [UserPrefConstants singleton].sortByProperty = @"Volume";
    }
    int chooseStock = 0;
    for (int x=0; x<brokerListNameArr.count;x++) {
        ////NSLog(@"left %@ vs Right %@",[UserPrefConstants singleton].sortByProperty,stkNameDiv[1]);
        
        if ([[UserPrefConstants singleton].sortByProperty isEqualToString:[brokerListNameArr objectAtIndex:x]]) {
            [UserPrefConstants singleton].sortByProperty = [brokerListNameArr objectAtIndex:x];
            chooseStock = x;
            break;
        }else{
            chooseStock = 2;
        }
    }
    
    sortRow = chooseStock;
    
    
    
    ////NSLog(@"Picker: %@", stkNameDiv[1]);
    ////NSLog(@"Selected Index: %ld", (long)selectedIndex);
    ////NSLog(@"Selected Value: %@", selectedValue);
    
    NSArray *propertyDivideDirection = [[brokerListKeyArr objectAtIndex:chooseStock] componentsSeparatedByString:@"|"];
    NSString *property    = [propertyDivideDirection objectAtIndex:0];
    sortedByFID = property;
    NSString *direction = [propertyDivideDirection objectAtIndex:1];
    
    [[VertxConnectionManager singleton] vertxSortByServer:property Direction:direction exchg:[UserPrefConstants singleton].userCurrentExchange];
}


- (void) dealloc{
    
    // //NSLog(@"goodbye QuoteScreen");
    
    // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didUpdate" object:nil];
    // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedSortByServer" object:nil];
    ////NSLog(@"viewDidDisappear QuoteScreenViewController");
    //    lblDynamic1 = nil;
    //    lblDynamic2= nil;
    //    lblDynamic3= nil;
    //    lblDynamic4= nil;
    //    lblDynamic5= nil;
    //    lblSortBy= nil;
    //    _Realtimeordelayedlabel= nil;
    //    lbNotAvaialble = nil;
    //    _tableView = nil;
    //    _columnPicker = nil;
    //    _columnPickerPopover = nil;
    //    loadingBtn = nil;
    //    for (CALayer* layer in [self.view.layer sublayers])
    //    {
    //        [layer removeAllAnimations];
    //    }
}


/*
 dynamicFID1 =FID_101_I_VOLUME;
 dynamicFID2 =FID_56_D_HIGH_PRICE;
 dynamicFID3 =FID_57_D_LOW_PRICE;
 dynamicFID4 =FID_98_D_LAST_DONE_PRICE;
 dynamicFID5 =FID_55_D_OPEN_PRICE;
 
 dynamicFID6 =FID_123_D_PAR_VALUE;
 dynamicFID7 =FID_127_D_EPS;
 dynamicFID8 =FID_CUSTOM_F_CHANGE_PER;
 dynamicFID9 =FID_CUSTOM_F_CHANGE;
 dynamicFID10 =FID_78_I_SELL_QTY_1;
 dynamicFID11 =FID_88_D_SELL_PRICE_1;
 dynamicFID12 =FID_68_D_BUY_PRICE_1;
 dynamicFID13 =FID_58_I_BUY_QTY_1;
 dynamicFID14 =FID_50_D_CLOSE;
 dynamicFID15 =FID_153_D_THEORETICAL_PRICE;
 dynamicFID16 = FID_156_S_TRD_PHASE;
 dynamicFID17 = FID_102_D_VALUE;
 
 FID_51_D_REF_PRICE lblLastDone
 FID_132_I_TOT_OF_TRD
 */

#pragma mark - Flashing Fields of Changed Data

- (void) checkQuantity:(NSDictionary *)detailDict withTmpDict:(NSDictionary *)tmpDict andRow:(int)row andCell:(QuoteScreenTableViewCell *)cell{
    
    // ============ new added in 1.8.5
    // revise 1.9.0 - int all chnge to long long
    
    if ([[detailDict objectForKey:FID_102_D_VALUE] longLongValue]> [[tmpDict objectForKey:FID_102_D_VALUE] longLongValue])
        cell.lblTradeVal.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:FID_102_D_VALUE] longLongValue]<[[tmpDict objectForKey:FID_102_D_VALUE] longLongValue])
        cell.lblTradeVal.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblTradeVal.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:FID_132_I_TOT_OF_TRD] longLongValue]> [[tmpDict objectForKey:FID_132_I_TOT_OF_TRD] longLongValue])
        cell.lblTrades.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:FID_132_I_TOT_OF_TRD] longLongValue]<[[tmpDict objectForKey:FID_132_I_TOT_OF_TRD] longLongValue])
        cell.lblTrades.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblTrades.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    
    if ([[detailDict objectForKey:dynamicFID10] longLongValue]> [[tmpDict objectForKey:dynamicFID10] longLongValue])
        cell.lblAskQty.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID10] longLongValue]<[[tmpDict objectForKey:dynamicFID10] longLongValue])
        cell.lblAskQty.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblAskQty.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID13] longLongValue]> [[tmpDict objectForKey:dynamicFID13] longLongValue])
        cell.lblBidQty.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID13] longLongValue]<[[tmpDict objectForKey:dynamicFID13] longLongValue])
        cell.lblBidQty.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblBidQty.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    // ============
    
    if ([[detailDict objectForKey:dynamicFID1] longLongValue]> [[tmpDict objectForKey:dynamicFID1] longLongValue])
        cell.lblDynamic1.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID1] longLongValue]<[[tmpDict objectForKey:dynamicFID1] longLongValue])
        cell.lblDynamic1.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic1.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID2] longLongValue]>[[tmpDict objectForKey:dynamicFID2] longLongValue])
        cell.lblDynamic2.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID2] longLongValue]<[[tmpDict objectForKey:dynamicFID2] longLongValue])
        cell.lblDynamic2.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic2.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID3] longLongValue]>[[tmpDict objectForKey:dynamicFID3] longLongValue])
        cell.lblDynamic3.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID3] longLongValue]<[[tmpDict objectForKey:dynamicFID3] longLongValue])
        cell.lblDynamic3.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic3.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID4] longLongValue]>[[tmpDict objectForKey:dynamicFID4] longLongValue])
        cell.lblDynamic4.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID4] longLongValue]<[[tmpDict objectForKey:dynamicFID4] longLongValue])
        cell.lblDynamic4.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic4.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID5] longLongValue]>[[tmpDict objectForKey:dynamicFID5] longLongValue])
        cell.lblDynamic5.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID5] longLongValue]<[[tmpDict objectForKey:dynamicFID5] longLongValue])
        cell.lblDynamic5.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic5.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
}


- (void) checkPrice:(NSDictionary *)detailDict withTmpDict:(NSDictionary *)tmpDict andRow:(int)row andCell:(QuoteScreenTableViewCell *)cell
{
    // added 1.8.5
    
    if ([[detailDict objectForKey:dynamicFID7] doubleValue]> [[tmpDict objectForKey:dynamicFID7] doubleValue])
        cell.lblEps.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID7] doubleValue]<[[tmpDict objectForKey:dynamicFID7] doubleValue])
        cell.lblEps.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblEps.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    if ([[detailDict objectForKey:dynamicFID6] doubleValue]> [[tmpDict objectForKey:dynamicFID6] doubleValue])
        cell.lblPar.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID6] doubleValue]<[[tmpDict objectForKey:dynamicFID6] doubleValue])
        cell.lblPar.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblPar.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID15] doubleValue]> [[tmpDict objectForKey:dynamicFID15] doubleValue])
        cell.lblTop.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID15] doubleValue]<[[tmpDict objectForKey:dynamicFID15] doubleValue])
        cell.lblTop.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblTop.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    if ([[detailDict objectForKey:dynamicFID8] doubleValue]> [[tmpDict objectForKey:dynamicFID8] doubleValue])
        cell.lblPerChg.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID8] doubleValue]<[[tmpDict objectForKey:dynamicFID8] doubleValue])
        cell.lblPerChg.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblPerChg.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID9] doubleValue]> [[tmpDict objectForKey:dynamicFID9] doubleValue])
        cell.lblChange.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID9] doubleValue]<[[tmpDict objectForKey:dynamicFID9] doubleValue])
        cell.lblChange.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblChange.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID11] doubleValue]> [[tmpDict objectForKey:dynamicFID11] doubleValue])
        cell.lblAsk.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID11] doubleValue]<[[tmpDict objectForKey:dynamicFID11] doubleValue])
        cell.lblAsk.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblAsk.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID12] doubleValue]> [[tmpDict objectForKey:dynamicFID12] doubleValue])
        cell.lblBid.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID12] doubleValue]<[[tmpDict objectForKey:dynamicFID12] doubleValue])
        cell.lblBid.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblBid.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:FID_51_D_REF_PRICE] doubleValue]> [[tmpDict objectForKey:FID_51_D_REF_PRICE] doubleValue])
        cell.lblLastDone.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:FID_51_D_REF_PRICE] doubleValue]<[[tmpDict objectForKey:FID_51_D_REF_PRICE] doubleValue])
        cell.lblLastDone.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblLastDone.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    // =======
    
    if ([[detailDict objectForKey:dynamicFID1] doubleValue]> [[tmpDict objectForKey:dynamicFID1] doubleValue])
        cell.lblDynamic1.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID1] doubleValue]<[[tmpDict objectForKey:dynamicFID1] doubleValue])
        cell.lblDynamic1.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic1.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID2] doubleValue]>[[tmpDict objectForKey:dynamicFID2] doubleValue])
        cell.lblDynamic2.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID2] doubleValue]<[[tmpDict objectForKey:dynamicFID2] doubleValue])
        cell.lblDynamic2.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic2.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID3] doubleValue]>[[tmpDict objectForKey:dynamicFID3] doubleValue])
        cell.lblDynamic3.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID3] doubleValue]<[[tmpDict objectForKey:dynamicFID3] doubleValue])
        cell.lblDynamic3.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic3.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    
    if ([[detailDict objectForKey:dynamicFID4] doubleValue]>[[tmpDict objectForKey:dynamicFID4] doubleValue])
        cell.lblDynamic4.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID4] doubleValue]<[[tmpDict objectForKey:dynamicFID4] doubleValue])
        cell.lblDynamic4.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic4.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
    
    
    
    if ([[detailDict objectForKey:dynamicFID5] doubleValue]>[[tmpDict objectForKey:dynamicFID5] doubleValue])
        cell.lblDynamic5.layer.backgroundColor=kGrnFlash.CGColor;
    else if([[detailDict objectForKey:dynamicFID5] doubleValue]<[[tmpDict objectForKey:dynamicFID5] doubleValue])
        cell.lblDynamic5.layer.backgroundColor=kRedFlash.CGColor;
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^(void) {
        cell.lblDynamic5.layer.backgroundColor=[UIColor clearColor].CGColor;
    } completion:nil];
}


- (IBAction)callSectorPicker:(UIButton*)sender {
    
    [atp timeoutActivity];
    
    [self performSegueWithIdentifier:@"segueSectorSelector" sender:nil];
    
}


- (IBAction)callBoardLotPicker:(UIButton*)sender {
    
    [atp timeoutActivity];
    
    [self performSegueWithIdentifier:@"segueMarketSelector" sender:nil];
    
}

- (IBAction)compactViewPressed:(UIButton *)sender {
    
    [atp timeoutActivity];
    
    if(sender.selected) // Normal view
    {
        [sender setSelected:NO];
        [UserPrefConstants singleton].isCollectionView = NO;
        [_myScrollView setHidden:NO];
        [_frontTableView setHidden:NO];
        [_cardCollectionView setHidden:YES];
        [_frontTableView reloadData];
        [_tableView reloadData];
        [_cardCollectionView setBackgroundView:nil];
        [_tableView setBackgroundView:_customPullView];
    }
    else // Card view
    {
        
        [self openStockContainer:NO animated:NO];
        [sender setSelected:YES];
        [UserPrefConstants singleton].isCollectionView = YES;
        [_myScrollView setHidden:YES];
        [_frontTableView setHidden:YES];
        [_cardCollectionView setHidden:NO];
        [[UserPrefConstants singleton].imgChartCache removeAllObjects];
        [_detailPageDict removeAllObjects];
        [_cardCollectionView reloadData];
        [_tableView setBackgroundView:nil];
        [_cardCollectionView setBackgroundView:_customPullView];
    }
    
    [self setScrollViewContentSize];
    
}

#pragma mark - Trader Delegate

-(void)traderDidCancelled {
    //Enable buy/sell traderPreview
    if (previewVC) {
        [previewVC fnControlButtons:TRUE];
    }
    

    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.traderContainer setAlpha:0.0];
        
    } completion:^(BOOL finished) {
        [self.traderContainer setHidden:YES];
//        [self enableTradeButtons:YES];
    }];
}

-(void)traderDidCancelledNormal {
    
    if (previewVC) {
        [previewVC fnControlButtons:TRUE];
    }
    
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.traderContainer setAlpha:0.0];
        
    } completion:^(BOOL finished) {
        [self.traderContainer setHidden:YES];
        //        [self enableTradeButtons:YES];
    }];
}

-(void)executeTrade:(int)iSelect {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    CGFloat defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:selectedStkCodeStr] objectForKey:FID_88_D_SELL_PRICE_1] floatValue];
    
    //NSLog(@"b4 %f",defPrice);
    if (defPrice<=0) {
        defPrice = [[[[[QCData singleton]qcFeedDataDict]objectForKey:selectedStkCodeStr] objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue];
        //  NSLog(@"zero %f",defPrice);
    }
    
    
    if (iSelect==10) { // BUY
        
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    @"BUY",@"Action",
                    selectedStkCodeStr,@"StockCode",
                    selectedStkName,@"StockName",
                    [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
                    [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], selectedStkName],@"stkNname",
                    trdRules, @"TradeRules",
                    nil];
        }else{
            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    @"BUY",@"Action",
                    
                    selectedStkCodeStr,@"StockCode",
                    selectedStkName,@"StockName",
                    [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
                    [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], selectedStkName],@"stkNname",
                    trdRules, @"TradeRules",
                    nil];
        }
        
        
        
    } else if (iSelect==11) { // SELL
        
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    @"SELL",@"Action",
                    selectedStkCodeStr,@"StockCode",
                    selectedStkName,@"StockName",
                    [NSString stringWithFormat:@"%.3f",defPrice],@"StockPrice",
                    [NSString stringWithFormat:@"%.3f",defPrice],@"DefaultPrice",
                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], selectedStkName],@"stkNname",
                    trdRules, @"TradeRules",
                    nil];
        }else{
            dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    @"SELL",@"Action",
                    selectedStkCodeStr,@"StockCode",
                    selectedStkName,@"StockName",
                    [NSString stringWithFormat:@"%.4f",defPrice],@"StockPrice",
                    [NSString stringWithFormat:@"%.4f",defPrice],@"DefaultPrice",
                    [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], selectedStkName],@"stkNname",
                    trdRules, @"TradeRules",
                    nil];
        }
        
        
        
        
    } else  if (iSelect==12) { // REVISE
        
        dict = [[UserPrefConstants singleton].reviseCancelDict mutableCopy];
        
        
    } else  if (iSelect==13) { // CANCEL
        
        dict = [[UserPrefConstants singleton].reviseCancelDict mutableCopy];
        
    }
    
    
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    TraderViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TraderViewController"];
    tradeVC.actionTypeDict = dict;
    //[vc setTransitioningDelegate:transitionController];
    self.providesPresentationContextTransitionStyle = YES;
    self.definesPresentationContext = YES; //
    //    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    //    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    //    [self presentViewController:vc animated:NO completion:nil];
    
    tradeVC.stockLotSize = lotSizeStringInt;
    [self.view bringSubviewToFront:_traderContainer];
    [_traderContainer setAlpha:0.0];
    [_traderContainer setHidden:NO];
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [_traderContainer setAlpha:1.0];
    } completion:^(BOOL finished) {
        
        [tradeVC callTrade];
//        [self enableTradeButtons:NO];
    }];
    
}


-(void)accountSelected:(UserAccountClientData *)account {
    // update account details in trader
    
    [tradeVC updateTradingAccount:account];
    
}

@end
