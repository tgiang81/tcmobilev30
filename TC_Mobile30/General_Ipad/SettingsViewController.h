//
//  SettingsViewController.h
//  TCiPad
//
//  Created by n2n on 02/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *TOCContainer;
@property (strong, nonatomic) IBOutlet UIView *tocContainerView;
@property (strong, nonatomic) IBOutlet UIView *contentContainerView;

@end
