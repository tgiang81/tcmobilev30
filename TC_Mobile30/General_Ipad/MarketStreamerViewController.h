//
//  MarketStreamerViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 05/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockStreamer.h"

@interface MarketStreamerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    IBOutlet UIView *leftSteamerView;
    UITableViewController *popupTableViewController;
    UITableViewController *popupFilterTableViewController;
    UIPopoverController *columnPickerPopover;
    int favID;
}

@property (nonatomic, strong) IBOutlet UIView *leftSteamerView;
@property (nonatomic, strong) IBOutlet UIView *rightStreamerView;
@property (nonatomic, weak) IBOutlet UILabel *watchListLabel;
@property (nonatomic, weak) IBOutlet UILabel *filterByLabel;

@property (nonatomic,weak) IBOutlet UILabel *bbhLabel;
@property (nonatomic, weak) IBOutlet UILabel *sbhLabel;

@property (nonatomic, weak) IBOutlet UIButton *buttonLine;
@property (nonatomic, weak) IBOutlet UIButton *buttonDirection;


@end
