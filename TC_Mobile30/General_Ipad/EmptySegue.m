//
//  EmptySegue.m
//  TCUniversal
//
//  Created by Scott Thoo on 11/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "EmptySegue.h"

@implementation EmptySegue

- (void)perform
{
    // Nothing. The ContainerViewController class handles all of the view
    // controller action.
}

@end
