//
//  UnitLotViewController.m
//  TCiPad
//
//  Created by n2n on 05/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "UnitLotViewController.h"
#import "UserPrefConstants.h"
#import "AppControl.h"
#import "StockAlertAPI.h"

@interface UnitLotViewController ()

@end

@implementation UnitLotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
  /*  NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
  
    [[StockAlertAPI instance] checkPushNotificationAPI:appDelegate.appData.userAccountInfo.username
                                         andUDID:[UserPrefConstants singleton].deviceUDID
                                      andExhange:[AppConfigManager getPrimaryDefaultExchange]
                                  andSponsorCode:[AppConfigManager getLMSSponsor]
                                       andBHCode:appDelegate.appData.userAccountInfo.brokerCode
                                        andAppID:@"MD"
                                     andBundleId:bundleIdentifier
                                   andClientCode:appDelegate.appData.userAccountInfo.senderCode
                                  andDeviceToken:[GlobalVariables sharedVariables].deviceToken
                                    andIPAddress:[GlobalVariables sharedVariables].ipAddress
                               andActivationCode:@""];
        */
    
    
    
    
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    
    
    if ([UserPrefConstants singleton].quantityIsLot) {
        _qtySegment.selectedSegmentIndex = 0;
        _qtyIndicator.text = @"x100";
        if ([[[NSBundle mainBundle] bundleIdentifier] isEqualToString:@"com.abacus.mytradetab"]||
            [[[NSBundle mainBundle] bundleIdentifier] isEqualToString:@"com.abacus.mytradeuat"]) {
              _qtyIndicator.text = @"x100000";
        }
    } else {
        _qtySegment.selectedSegmentIndex = 1;
        _qtyIndicator.text = @"x1";
    }
    
    
    if ([def objectForKey:@"QuoteScreenCardView"]==nil) {
        _qScrViewMode.selectedSegmentIndex = 0;
    } else {
        BOOL defVal = [def boolForKey:@"QuoteScreenCardView"];
        if (defVal) {
            _qScrViewMode.selectedSegmentIndex = 1;
        } else {
            _qScrViewMode.selectedSegmentIndex = 0;
        }
    }
    
    if ([def objectForKey:@"ScreenViewMode"]==nil) {
        _qViewMode.selectedSegmentIndex = 0;
    } else {
        int defVal = [[def objectForKey:@"ScreenViewMode"] intValue];
        _qViewMode.selectedSegmentIndex = defVal-1;
     
    }
    
    if ([def objectForKey:@"TouchIDMode"]==nil) {
        _touchIDMode.selectedSegmentIndex = 0;
    } else {
        BOOL defVal = [def boolForKey:@"TouchIDMode"];
        if (defVal) {
            _touchIDMode.selectedSegmentIndex = 1;
        } else {
            _touchIDMode.selectedSegmentIndex = 0;
        }
    }
    
    if ([def objectForKey:@"PushNotificationMode"]==nil) {
        _pushNotificationMode.selectedSegmentIndex = 0;
    } else {
        BOOL defVal = [def boolForKey:@"PushNotificationMode"];
        if (defVal) {
            _pushNotificationMode.selectedSegmentIndex = 1;
        } else {
            _pushNotificationMode.selectedSegmentIndex = 0;
        }
    }
    
    
     [self.view setBackgroundColor:kPanelColor];
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    // UISegmentedControl language change is special since there are multiple
    // texts per object
    
    for (UIView *view in self.view.subviews) {
        if ([view isKindOfClass:[UISegmentedControl class]]) {
            UISegmentedControl *segment = (UISegmentedControl*)view;
            // loop through its segments
            for (int i=0; i<segment.numberOfSegments; i++) {
                [segment setTitle:[LanguageManager stringForKey:[segment titleForSegmentAtIndex:i]] forSegmentAtIndex:i];
            }
        }
    }
    
    
    long status = [UserPrefConstants checkAuthenticateByTouchId];
    
    if (status==-6) {
        [_touchIDLabel setHidden:YES];
        [_touchIDMode setHidden:YES];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)qtyTypeChanged:(id)sender {
    UISegmentedControl *ctrl = (UISegmentedControl*)sender;
    
    if (ctrl.selectedSegmentIndex==0) {
        [UserPrefConstants singleton].quantityIsLot = YES;
        _qtyIndicator.text = @"x100";
    } else {
        [UserPrefConstants singleton].quantityIsLot = NO;
        _qtyIndicator.text = @"x1";
    }
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    [def setBool:[UserPrefConstants singleton].quantityIsLot forKey:@"QtyIsLot"];
    [def synchronize];
}

- (IBAction)quoteScrViewModeChanged:(id)sender {
    UISegmentedControl *ctrl = (UISegmentedControl*)sender;
    BOOL defVal = NO;
    
    if (ctrl.selectedSegmentIndex==0) {
        defVal = NO;
    } else {
        defVal = YES;
    }
    
    [UserPrefConstants singleton].isCollectionView = defVal;
    
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    [def setBool:defVal forKey:@"QuoteScreenCardView"];
    [def synchronize];
}


// user defined default starting screen.
- (IBAction)screenViewModeChanged:(id)sender{
    UISegmentedControl *ctrl = (UISegmentedControl*)sender;
    int screenViewMode = 1;
    
    if (ctrl.selectedSegmentIndex==0) {
        screenViewMode = 1;
        [UserPrefConstants singleton].defaultStartingScreen = @"Home";
    } else if(ctrl.selectedSegmentIndex==1){
        screenViewMode = 2;
        [UserPrefConstants singleton].defaultStartingScreen = @"Quote";
    } else {
        screenViewMode = 3;
        [UserPrefConstants singleton].defaultStartingScreen = @"WatchList";
    }
    
    [UserPrefConstants singleton].defaultView = screenViewMode;
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    [def setValue:[NSString stringWithFormat:@"%d",screenViewMode] forKey:@"ScreenViewMode"];
    [def synchronize];
}

- (IBAction)setPushNotificationModeChanged:(id)sender{
    UISegmentedControl *ctrl = (UISegmentedControl*)sender;
    BOOL defVal = NO;
    
    if (ctrl.selectedSegmentIndex==0) {
        defVal = NO;
    } else {
        defVal = YES;
    }
    
    [UserPrefConstants singleton].isActivatePushNotification = defVal;
    
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    [def setBool:defVal forKey:@"PushNotificationMode"];
    [def synchronize];
}





- (IBAction)setTouchIDModeChanged:(id)sender{
    UISegmentedControl *ctrl = (UISegmentedControl*)sender;
    BOOL defVal = NO;
    
    if (ctrl.selectedSegmentIndex==0) {
        defVal = NO;
    } else {
        defVal = YES;
    }
    
    if (defVal) {
        
        long status = [UserPrefConstants checkAuthenticateByTouchId];
        
        switch (status) {
                
            case 0: {
                // available and enabled. OK to turn on feature
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"TouchID enabled. To activate it, log out and press the red fingerprint icon and login again."]
                                             inController:self withTitle:[LanguageManager stringForKey:@"Info"]];
            }break;
            case -7: {
                // available but not enabled
                defVal = NO;
                ctrl.selectedSegmentIndex = 0;
                // alert
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"TouchID is not enabled. Please goto iOS Settings->Touch ID & Passcode and turn on Passcode."]
                                             inController:self withTitle:[LanguageManager stringForKey:@"Info"]];
                
            }break;
            case -6: {
                // not available
                // we already hide in viewdidload
                
//                defVal = NO;
//                ctrl.selectedSegmentIndex = 0;
//                // alert
//                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"TouchID is not available on this device."]
//                                            inController:self withTitle:[LanguageManager stringForKey:@"Info"]];
            } break;
                
            default:
                break;
        }
        

    }
    
    [UserPrefConstants singleton].isActivatingTouchID = defVal;
    
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    [def setBool:defVal forKey:@"TouchIDMode"];
    [def synchronize];
}

@end
