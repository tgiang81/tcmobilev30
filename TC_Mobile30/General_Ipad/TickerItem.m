//
//  TickerItem.m
//  TCiPad
//
//  Created by n2n on 03/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "TickerItem.h"
#import "UserPrefConstants.h"
#import "AppControl.h"

@implementation TickerItem


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


//-(CATextLayer*)textLayerWithParams:(NSArray*)params {
//    
//    NSString *fontName1 = @"HelveticaNeue-Bold";
//    NSString *fontName2 = @"HelveticaNeue";
//    
//    NSString *textToDisplay = [params objectAtIndex:0];
//    UIColor *clr = [params objectAtIndex:1];
//    
//    CGFloat fontSize = 18.0;
//    UIFont *font1 = [UIFont fontWithName:fontName1 size:fontSize];
//    UIFont *font2 = [UIFont fontWithName:fontName2 size:fontSize];
//
//    
//    CGSize stringsize = [textToDisplay sizeWithAttributes:
//                         @{NSFontAttributeName:font1}];
//    
//    
//    CGSize myFontSize = CGSizeMake(ceilf(stringsize.width), ceilf(stringsize.height));
//    
//    CATextLayer *layerLabel = [CATextLayer layer];
//    layerLabel.contentsScale = [[UIScreen mainScreen] scale];
//    layerLabel.frame = CGRectMake(0, 0, myFontSize.width, myFontSize.height);
//    layerLabel.font = CFBridgingRetain(font1);
//    layerLabel.fontSize = fontSize;
//    layerLabel.foregroundColor = clr.CGColor;
//    layerLabel.backgroundColor = [UIColor clearColor].CGColor;
//    layerLabel.alignmentMode = kCAAlignmentCenter;
//    layerLabel.string = textToDisplay;
//    
//    
//    return layerLabel;
//}


- (void)setParam:(NSArray *)params{
     if ([params count]<=0) return;
    
    NSString *stkName = [params objectAtIndex:0];
    float lastDone = [[params objectAtIndex:1] floatValue];
    float change = [[params objectAtIndex:2] floatValue];
    UIColor *clr;
    NSString *icon;
    
    
    if ([[UserPrefConstants singleton] isFloatZero:change]) {
        clr = [UIColor lightGrayColor];
        icon = @"=";
    }
    if (change>0) {
        clr = [UIColor greenColor];
        icon = @"▲";
    }
    if (change<0) {
        clr = [UIColor redColor];
        icon = @"▼";
    }
    if ([UserPrefConstants singleton].pointerDecimal==3) {
        [lastdoneLabel setText:[NSString stringWithFormat:@"%.3f", lastDone]];
    }else{
        [lastdoneLabel setText:[NSString stringWithFormat:@"%.4f", lastDone]];
    }
    [lastdoneLabel setTextColor:clr];
    
    NSString *changeStr;
    if ([UserPrefConstants singleton].pointerDecimal==3) {
        if (change>0)
            changeStr = [NSString stringWithFormat:@"(+%.3f)", change];
        else
            changeStr = [NSString stringWithFormat:@"(%.3f)", change];
    }else{
        if (change>0)
            changeStr = [NSString stringWithFormat:@"(+%.4f)", change];
        else
            changeStr = [NSString stringWithFormat:@"(%.4f)", change];
    }
    
     [changeLabel setText:changeStr];
     [changeLabel setTextColor:clr];
    
     [updownLabel setTextColor:clr];
     [updownLabel setText:icon];
    [nameLabel setTextColor:clr];
    
}

-(id)initWithParams:(NSArray*)params {
    self=[super init];
    if (self) {
        
        if ([params count]<=0) return nil;
        
        self.backgroundColor = [UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.00];
        // PARAMS: stockName,lastDone,change
        NSString *stkName = [params objectAtIndex:0];
        float lastDone = [[params objectAtIndex:1] floatValue];
        float change = [[params objectAtIndex:2] floatValue];
        self.stockCode = [params objectAtIndex:3];
        
        _tickerWidth = 0;
        
        UIColor *clr;
        NSString *icon;
        CGFloat gapBtwObj = 5.0;

        
        if ([[UserPrefConstants singleton] isFloatZero:change]) {
            clr = [UIColor lightGrayColor];
            icon = @"=";
        }
        if (change>0) {
            clr = [UIColor greenColor];
            icon = @"▲";
        }
        if (change<0) {
            clr = [UIColor redColor];
            icon = @"▼";
        }
        
        
        
        CGFloat fontSize = 18.0;
        UIFont *font1 = [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
        UIFont *font2 = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
        
//        NSArray *param1 = [NSArray arrayWithObjects:stkName, [UIColor whiteColor], nil];
//        CATextLayer *nameLabel = [self textLayerWithParams:param1];
//        nameLabel.position = CGPointMake(nameLabel.frame.size.width/2.0, 0);
//        [self addSublayer:nameLabel];

        
        
        nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        [nameLabel setTextColor:[UIColor whiteColor]];
        [nameLabel setFont:font1];
        [nameLabel setText:stkName];
        [nameLabel sizeToFit];
        nameLabel.tag = 1;
        nameLabel.center = CGPointMake(nameLabel.frame.size.width/2.0, nameLabel.frame.size.height/2.0);
        [self addSubview:nameLabel];

        
        _tickerWidth += nameLabel.frame.size.width + gapBtwObj;
        
        
//        NSArray *param2 = [NSArray arrayWithObjects:icon, clr, nil];
//        CATextLayer *updownLabel = [self textLayerWithParams:param2];
//        updownLabel.position = CGPointMake(_tickerWidth+updownLabel.frame.size.width/2.0, 0);
//        [self addSublayer:updownLabel];
        
        updownLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [updownLabel setBackgroundColor:[UIColor clearColor]];
        [updownLabel setTextColor:clr];
        [nameLabel setFont:font2];
        [updownLabel setText:icon];
        [updownLabel sizeToFit];
        updownLabel.tag = 1;
        updownLabel.center = CGPointMake(_tickerWidth+updownLabel.frame.size.width/2.0, nameLabel.frame.size.height/2.0);
        [self addSubview:updownLabel];

        _tickerWidth += updownLabel.frame.size.width + gapBtwObj;
        
//        NSArray *param3 = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%.3f", lastDone], clr, nil];
//        CATextLayer *lastdoneLabel = [self textLayerWithParams:param3];
//        lastdoneLabel.position = CGPointMake(_tickerWidth+lastdoneLabel.frame.size.width/2.0, 0);
//        [self addSublayer:lastdoneLabel];
        
        
        lastdoneLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [lastdoneLabel setBackgroundColor:[UIColor clearColor]];
        [lastdoneLabel setTextColor:clr];
        [nameLabel setFont:font2];
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            [lastdoneLabel setText:[NSString stringWithFormat:@"%.3f", lastDone]];
        }else{
             [lastdoneLabel setText:[NSString stringWithFormat:@"%.4f", lastDone]];
        }
        [lastdoneLabel sizeToFit];
        lastdoneLabel.tag = 1;
        lastdoneLabel.center = CGPointMake(_tickerWidth+lastdoneLabel.frame.size.width/2.0, nameLabel.frame.size.height/2.0);
        [self addSubview:lastdoneLabel];
        
        _tickerWidth += lastdoneLabel.frame.size.width + gapBtwObj;
        
        NSString *changeStr;
          if ([UserPrefConstants singleton].pointerDecimal==3) {
        if (change>0)
            changeStr = [NSString stringWithFormat:@"(+%.3f)", change];
        else
            changeStr = [NSString stringWithFormat:@"(%.3f)", change];
          }else{
              if (change>0)
                  changeStr = [NSString stringWithFormat:@"(+%.4f)", change];
              else
                  changeStr = [NSString stringWithFormat:@"(%.4f)", change];
          }
        
//        NSArray *param4 = [NSArray arrayWithObjects:changeStr, clr, nil];
//        CATextLayer *changeLabel = [self textLayerWithParams:param4];
//        changeLabel.position = CGPointMake(_tickerWidth+changeLabel.frame.size.width/2.0, 0);
//        [self addSublayer:changeLabel];
        
        
        changeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [changeLabel setBackgroundColor:[UIColor clearColor]];
        [changeLabel setTextColor:clr];
        [changeLabel setText:changeStr];
        [changeLabel sizeToFit];
        changeLabel.tag = 1;
        changeLabel.center = CGPointMake(_tickerWidth+changeLabel.frame.size.width/2.0, nameLabel.frame.size.height/2.0);
        [self addSubview:changeLabel];
    
        
        _tickerWidth += changeLabel.frame.size.width;
        
       self.frame = CGRectMake(0, 0, _tickerWidth, nameLabel.frame.size.height);
        
        self.userInteractionEnabled = YES;
        
        

    }
    return self;
}





@end
