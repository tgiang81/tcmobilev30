//
//  TradeStatusViewController.h
//  TCiPad
//
//  Created by n2n on 23/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"
#import "LanguageManager.h"
#import "BaseVC.h"
@class StockModel;
@class AroundShadowView;
@protocol TradeStatusMsgDelegate <NSObject>
-(void)closeBtnDidClicked;
-(void)closeAndOpenOrdBookDidClicked;
-(void)closeAndOpenPortfolioDidClicked;
-(void)closeAndOpenQuoteDidClicked;
@end

@interface TradeStatusViewController : BaseVC
@property (weak, nonatomic) IBOutlet UILabel *tsTitle;
@property (weak, nonatomic) IBOutlet UILabel *tsMessage;
@property (weak, nonatomic) id<TradeStatusMsgDelegate>delegate;
@property (nonatomic, strong) NSString *messageStr;
@property (nonatomic, strong) NSString *statusStr;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *openOrdBkBtn;
@property (weak, nonatomic) IBOutlet UIButton *openPortfolioBtn;
@property (weak, nonatomic) IBOutlet UIButton *openQuoteBtn;

@property (weak, nonatomic) IBOutlet UIImageView *img_status;
@property (weak, nonatomic) IBOutlet UILabel *lbl_status;

@property (weak, nonatomic) IBOutlet UILabel *lbStockName;
@property (weak, nonatomic) IBOutlet UILabel *lbCompany;

@property (weak, nonatomic) IBOutlet UILabel *changeValue;
@property (weak, nonatomic) IBOutlet UILabel *changePercent;
@property (weak, nonatomic) IBOutlet AroundShadowView *v_content;
@property (strong, nonatomic) StockModel *stock;
@property (assign, nonatomic) CGFloat changeValueContent;
- (IBAction)closeStatus:(id)sender;
- (IBAction)closeStatusAndOpenOrdBook:(id)sender;

@end
