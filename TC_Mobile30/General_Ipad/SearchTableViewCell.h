//
//  SearchTableViewCell.h
//  TCiPad
//
//  Created by n2n on 23/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface SearchTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *searchResultLabel;
@property (strong, nonatomic) IBOutlet UILabel *searchResultLabel2;
@property (strong, nonatomic) IBOutlet UIButton *btnCell;

@end
