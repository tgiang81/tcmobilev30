//
//  TimeAndSalesTableViewCell.h
//  TCUniversal
//
//  Created by Scott Thoo on 11/18/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeAndSalesTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTime;
@property (nonatomic, weak) IBOutlet UILabel *lblTradeAt;
@property (nonatomic, weak) IBOutlet UILabel *lblPrice;
@property (nonatomic, weak) IBOutlet UILabel *lblVolume;


@end
