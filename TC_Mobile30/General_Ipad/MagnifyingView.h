//
//  MagnifyingView.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 28/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ACMagnifyingGlass;

@interface MagnifyingView : UIView

@property (nonatomic, retain) ACMagnifyingGlass *magnifyingGlass;
@property (nonatomic, assign) CGFloat magnifyingGlassShowDelay;
- (void)touchesBegan:(CGPoint )touchPoint;
- (void) touchesMove:(CGPoint)translation;
- (void) touchesEnded:(CGPoint )touchPoint;
@end
