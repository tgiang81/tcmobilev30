//
//  ElasticNewsCell.m
//  TCiPad
//
//  Created by n2n on 13/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "ElasticNewsCell.h"
@implementation ElasticNewsCell
@synthesize dateLabel;
@synthesize descriptionsLabel;
@synthesize stockLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) loadData:(NewsMetadata *)dataCell{
    NSLog(@"Date Cell %@",dataCell.date1);
    dateLabel.text = dataCell.date1;
    descriptionsLabel.text = dataCell.newsTitle;
    stockLabel.text = dataCell.stockcode;
}

@end
