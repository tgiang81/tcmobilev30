//
//  ElasticNewsCell.h
//  TCiPad
//
//  Created by n2n on 13/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsMetadata.h"
@interface ElasticNewsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionsLabel;
@property (nonatomic, weak) IBOutlet UILabel *stockLabel;
- (void) loadData:(NewsMetadata *)dataCell;
@end
