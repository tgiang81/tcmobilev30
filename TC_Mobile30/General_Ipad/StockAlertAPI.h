//
//  StockAlertAPI.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 10/04/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StockAlertAPI : NSObject

+(StockAlertAPI *) instance;

- (void) getAESKeyAPI;
- (NSString *) getIPAddress;
- (NSString *) identifierForVendor;

- (void) storePushNotificationInfo:(NSString *)accountName
                           andUDID:(NSString *)deviceUDID
                        andExhange:(NSString *)exchange
                    andSponsorCode:(NSString *)sponsorCode
                         andBHCode:(NSString *)bhCode
                          andAppID:(NSString *)appID
                       andBundleId:(NSString *)bundleID
                     andClientCode:(NSString *)clientCode
                    andDeviceToken:(NSString *)deviceToken
                      andIPAddress:(NSString *)ipAddress
                 andActivationCode:(NSString *)activationCode;

//- (void) authenticateQRCodeLogin:(NSString *)brokerCode
//                  andServerToken:(NSString *)token
//                    andTimeStamp:(NSString *)timeStamp;


- (NSString *)getMobileToken;

@end
