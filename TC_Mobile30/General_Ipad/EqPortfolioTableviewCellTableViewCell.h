//
//  EqPortfolioTableviewCellTableViewCell.h
//  TCiPad
//
//  Created by Sri Ram on 12/15/15.
//  Copyright © 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EqPortfolioDelegate;

@interface EqPortfolioTableviewCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *stkName;
@property (weak, nonatomic) IBOutlet UILabel *mktValue;
@property (weak, nonatomic) IBOutlet UILabel *qtyOnHand;
@property (weak, nonatomic) IBOutlet UILabel *gainLossPer;
@property (weak, nonatomic) IBOutlet UILabel *gainLoss;

@property (weak, nonatomic) IBOutlet UILabel *labelStockArrow;

@property (nonatomic,retain) id <EqPortfolioDelegate> delegate;

- (IBAction)buyBtnPressed:(id)sender;
- (IBAction)sellButtonPressed:(id)sender;

@end

@protocol EqPortfolioDelegate <NSObject>

-(void)buyorderBtnPressed:(EqPortfolioTableviewCellTableViewCell *)cell;
-(void)sellorderBtnPressed:(EqPortfolioTableviewCellTableViewCell *)cell;

@end
