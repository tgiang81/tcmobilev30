//
//  CustomTabBarViewController.h
//  TCUniversal
//
//  Created by Scott Thoo on 10/8/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockFlag.h"
#import <SpriteKit/SpriteKit.h>
#import "TickerItem.h"
#import "MagnifyingView.h"
#import "LabelEffect.h"
#import "LanguageManager.h"
#import "TickerData.h"
#import "OBDisclaimerViewController.h"
#import "ValidatePINViewController.h"

@class DTLoupeView;

@interface MainFrameViewController : UIViewController <StockFlagDelegate,
UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,
UIBarPositioningDelegate, UIGestureRecognizerDelegate, ValidatePINDelegate, OBDisclaimerDelegate>
{
    ValidatePINViewController *validVC;
    OBDisclaimerViewController *OBDisclaimer;
    BOOL checkingLoop;
    BOOL justChangeExchange;
    BOOL tickerHasLoaded;
    
}
@property int selection;
@property int prev_selection;


// top of frame
@property (weak, nonatomic) IBOutlet UILabel *DateTimelbl;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblKlci;

// Bottom of frame
@property (weak, nonatomic) IBOutlet UIView *viewBottomTicker;
@property (weak, nonatomic) IBOutlet UIButton *languageBtn;
@property (weak, nonatomic) IBOutlet UILabel *tickerErrorLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imgNetwork;
@property (weak, nonatomic) IBOutlet UILabel *lblIndex;
@property (strong, nonatomic) LabelEffect *n2nconnectBhd;
@property (weak, nonatomic) IBOutlet UILabel *mainFrameTitle;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIView *Containerview;

// magnifier
@property (weak, nonatomic) IBOutlet MagnifyingView *magnifyingView;


// Side navigation button
@property (weak, nonatomic) IBOutlet UIView *viewSideBar; // container
@property (weak, nonatomic) IBOutlet UIScrollView *sideBarScrollView; // unused
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIButton *quotesButton;
@property (weak, nonatomic) IBOutlet UIButton *IndicesButton;
@property (weak, nonatomic) IBOutlet UIButton *stockDetailsButton;
@property (weak, nonatomic) IBOutlet UIButton *orderBookButton;
@property (weak, nonatomic) IBOutlet UIButton *watchlistButton;
@property (weak, nonatomic) IBOutlet UIButton *eSettlementButton;
@property (weak, nonatomic) IBOutlet UIButton *stockAlertButton;
@property (weak, nonatomic) IBOutlet UIButton *elasticNewsButton;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *marketStreamerButton;


-(void)doAutoLogout;
- (IBAction)callLanguageSelection:(UIButton*)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)btnSeguePressed:(id)sender;
- (IBAction)callSearch:(id)sender;

@end
