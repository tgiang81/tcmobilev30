//
//  N2DatePicker.m
//  TCiPad
//
//  Created by n2n on 16/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "N2DatePicker.h"


@interface N2DatePicker()<UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation N2DatePicker

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@synthesize myDelegate;

-(instancetype)initWithDate:(NSDate*)date {
    self = [super init];
    if (self) {
        // populate arrays
        
        self.selectedDate = date;

        self.dayArray = [[NSMutableArray alloc] init];
        self.monthArray = [[NSMutableArray alloc] init];
        self.yearArray = [[NSMutableArray alloc] init];
        
        [self loadMonths];
        [self loadYears];
        
        
        super.delegate = self;
        super.dataSource = self;
        
        [self setPickerDate:date animated:NO];
        
    }
    return self;
}


-(void)loadMonths {
    
    [self.monthArray removeAllObjects];
    
    for (int i=1; i<=12; i++) {
        [self.monthArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [self loadDays];
}

-(void)loadYears {
    [self.yearArray removeAllObjects];
    
    for (int i=1970; i<=2100; i++) {
        [self.yearArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
}

-(void)loadDays {
    
    [self.dayArray removeAllObjects];
    
//    NSString *dateStr = [NSString stringWithFormat:@"%ld/%ld/%ld",_selectedDay, _selectedMonth, _selectedYear];
//    
//    // Convert string to date object
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"d/M/yyyy"];
//    NSDate *currentDate = [dateFormat dateFromString:dateStr];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:self.selectedDate];
    NSUInteger numberOfDaysInMonth = range.length;
    
    for (int i=1; i<=numberOfDaysInMonth; i++) {
        [self.dayArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
}



-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSInteger rows = 0;
    switch (component) {
        case 0:
            rows = [_dayArray count];
            break;
        case 1:
            rows = [_monthArray count];
            break;
        case 2:
            rows = [_yearArray count];
            break;
        default:
            break;
    }
    return rows;
    
}



-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *title;
    
    //NSLog(@"called");
    
    switch (component) {
        case 0:{
         
            title = [_dayArray objectAtIndex:row];
            //NSLog(@"title %@",title);
        }break;
        case 1:{
            
            title = [LanguageManager stringForKey:[NSString stringWithFormat:@"M-%@",[_monthArray objectAtIndex:row]]];
            
        }break;
        case 2:{
            
            title = [_yearArray objectAtIndex:row];
            
        }break;
            
        default:
            title = @"-";
            break;
    }
    return title;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    CGFloat wid = 0;
    switch (component) {
        case 0:
            wid = self.frame.size.width/5.0;
            break;
        case 1:
            wid = 2.0*self.frame.size.width/5.0;
            break;
        case 2:
            wid = 2.0*self.frame.size.width/5.0;
            break;
        default:
            break;
    }
    return wid;
}


-(void)updateSelectedDate {
    
    NSString *dayStr = [_dayArray objectAtIndex:[self selectedRowInComponent:0]];
    NSString *monStr = [_monthArray objectAtIndex:[self selectedRowInComponent:1]];
    NSString *yearStr = [_yearArray objectAtIndex:[self selectedRowInComponent:2]];
    
    NSString *dateStr = [NSString stringWithFormat:@"%@/%@/%@",
                         dayStr, monStr, yearStr];
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d/M/yyyy"];
    self.selectedDate = [dateFormat dateFromString:dateStr];
    
    [self.myDelegate n2datePickerSelected:self.selectedDate];
}


-(void)setPickerDate:(NSDate*)date animated:(BOOL)animate {
    // select date
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"M"];
    NSString *monthStr = [formatter stringFromDate:date];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d"];
    NSString *dayStr = [formatter stringFromDate:date];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearStr = [formatter stringFromDate:date];
    
    long monthRow = [_monthArray indexOfObject:monthStr];
    long dayRow = [_dayArray indexOfObject:dayStr];
    long yearRow = [_yearArray indexOfObject:yearStr];
    
    [self selectRow:dayRow inComponent:0 animated:animate];
    [self selectRow:monthRow inComponent:1 animated:animate];
    [self selectRow:yearRow inComponent:2 animated:animate];
    
    self.selectedDate = date;
    [self.myDelegate n2datePickerSelected:self.selectedDate];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (component==0) {
        [self updateSelectedDate];
        
    } else if (component==1) {
        [self updateSelectedDate];
        
        long prevDay = [self selectedRowInComponent:0];
        NSString *prevDayStr = [_dayArray objectAtIndex:prevDay];
        [self loadDays];
        [self reloadComponent:0];
        
        if ([_dayArray containsObject:prevDayStr]) {
            [self selectRow:prevDay inComponent:0 animated:NO];
        } else {
            [self selectRow:0 inComponent:0 animated:NO];
        }
        
    } else if (component==2) {
        [self updateSelectedDate];
        
        long prevDay = [self selectedRowInComponent:0];
        NSString *prevDayStr = [_dayArray objectAtIndex:prevDay];
        [self loadDays];
        [self reloadComponent:0];
        
        if ([_dayArray containsObject:prevDayStr]) {
            [self selectRow:prevDay inComponent:0 animated:NO];
        } else {
            
            [self selectRow:0 inComponent:0 animated:NO];
        }
    }
    
}


@end
