//
//  TickerItem.h
//  TCiPad
//
//  Created by n2n on 03/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TickerItem : UIView{
    UILabel *nameLabel;
    UILabel *updownLabel;
    UILabel *lastdoneLabel;
    UILabel *changeLabel;
}

@property (nonatomic, strong) NSString *stockCode;
@property (nonatomic, assign) CGFloat tickerWidth;
-(id)initWithParams:(NSArray*)params;
- (void)setParam:(NSArray *)params;

@end
