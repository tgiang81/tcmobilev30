//
//  OrderBookandPortfolioViewController.m
//  TCiPad
//
//  Created by Sri Ram on 12/7/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import "OrderBookandPortfolioViewController.h"
#import "UserPrefConstants.h"
#import "OrderStatusTableViewCell.h"
#import "UserAccountClientData.h"
#import "UIImageUtility.h"
#import "OrderDetails.h"
#import "QCData.h"
#import "QCConstants.h"
#import "TradingRules.h"
#import "SkipConfirmationViewController.h"
#import "OBDisclaimerViewController.h"
#import "EqPortfolioTableviewCellTableViewCell.h"
#import "PortfolioData.h"
#import "QCData.h"
#import "VertxConnectionManager.h"
#import "TraderViewController.h"
#import "TransitionDelegate.h"

@interface OrderBookandPortfolioViewController ()
{
    ATPAuthenticate *_atp;
    long selectedRow, selectedRowPF;
    BOOL callOnce;
    BOOL isLoadingPF, isLoadingOB, stkContainerDisplayed;
    
    CGPoint containerViewOriPos;
    CGFloat bufferPreviewX;
    CGRect selfFrame;
    CGFloat oriTablePosX;
    CGFloat scrollContentWidth;
    
    UITableView *accTableView;
    UIPopoverPresentationController *popoverController;
    UIPopoverController *columnPickerPopover;
    UIViewController *viewcont;
    UserAccountClientData *uacd;
    NSMutableArray *acclist;
    NSNumberFormatter *priceFormatter;
    OrderDetails *orderDetails;
    TradeStatus *tradeStatusRevise;
    TradeStatus *tradeStatusCancel;
    
    NSString *_stkCode;
    TradingRules *trdRules;;
    SkipConfirmationViewController *skipVC;
    UserAccountClientData *userTradeConfirmData;
    NSMutableArray *portfolioList;
    BOOL isPortfolio;
    NSArray *stkCodeAndExchangeArr;
    NSMutableArray *pickerList;
    long tag, ordBookRefreshCounter;
    N2DatePicker *datePickerView;
    long updatingDate;
    NSDate *dateRow;
    NSInteger accountRow;
    NSInteger validityRow;
    NSInteger priceRow;
    UIPickerView *pickerView;
    NSMutableArray *marketDepthArr;
    float defaultPrice;
    
    QuantityPadViewController *qtyPadBtn;
    OBDisclaimerViewController *OBDisclaimer;
    VertxConnectionManager *_vcm;
    NSString *stockName;


    CGPoint orderPadOriginalPos;
    CGFloat bufferPadX;
    long totalAccToShow;
    
    double Amount;
    double BuyTrxFee;
    double Commission;
    int Min;
    double SCCPFee;
    double SECFee;
    double SalesTax;
    double SellTrxFee;
    double VAT;

}

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation OrderBookandPortfolioViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TradeAccount Delegates

-(void)accountSelected:(UserAccountClientData *)account {
    
    uacd = [UserPrefConstants singleton].userSelectedAccount;

    _accLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",
                      [account client_account_number],
                      [account client_name],
                      [account broker_branch_code]];
    
    [_btnLoading startAnimating];
    
    // update trade limit and user data
    if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
        [_atp getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount
                        forPaymentCUT:NO andCurrency:@""];

    [self reloadTabContents];
}

- (void) reloadTabInfo{
    NSLog(@"reloadTabInfo %d",[UserPrefConstants singleton].lastPFPage);
    switch ([UserPrefConstants singleton].lastPFPage) {
        case 0:{
            [self ordBkBtnPressed];
        }break;
        case 1:{
            _pfMode = 1;
            [self createPortfolioButtonsFromArr:@[[LanguageManager stringForKey:@"Unrealized G/L"],[LanguageManager stringForKey:@"Realized G/L"]]];
            [self portfolioBtnPressed];
        }break;
            
        default:
            break;
    }
    
    
}
-(void)reloadTabContents {
    
    
    switch ([UserPrefConstants singleton].orderTabIndex) {
        case 0:{
            [self ordBkBtnPressed];
        }break;
        case 1:{
            _pfMode = 1;
            [self createPortfolioButtonsFromArr:@[[LanguageManager stringForKey:@"Unrealized G/L"],[LanguageManager stringForKey:@"Realized G/L"]]];
            [self portfolioBtnPressed];
        }break;
            
        default:
            break;
    }
    
}


#pragma mark - N2Tabs Delegate
-(void)tabDidClicked:(UIButton *)tabButton atTab:(N2Tabs *)tab {
    [_atp timeoutActivity];
    
    selectedRowPF = -1;
    selectedRow = -1;
    
    
    [UserPrefConstants singleton].orderTabIndex = tabButton.tag;
    [UserPrefConstants singleton].lastPFPage = (int)[UserPrefConstants singleton].orderTabIndex;
    [self reloadTabContents];
    
}

#pragma mark - Views LifeCycles

-(void)viewDidLayoutSubviews {
    
    if (callOnce) {
        
        callOnce = NO;
        NSString *ordBkTitle = [NSString stringWithFormat:@" %@ ", [LanguageManager stringForKey:@"Order Book"]];
        NSString *portFoTitle = [NSString stringWithFormat:@" %@ ", [LanguageManager stringForKey:@"Portfolio"]];
        
        NSArray *params = [NSArray arrayWithObjects:@[ordBkTitle,portFoTitle],
                           [UIColor cyanColor],
                           kPanelColor, nil];
        self.tabs = [[N2Tabs alloc] initWithParams:params withFrame:_tabsPlaceHolder.frame andFontSize:17.0f];
        self.tabs.tabName = @"OBPF";
        self.tabs.tabDelegate = self;
        [self.view addSubview:self.tabs];
        
        [_derivativesContainer setBackgroundColor:kPanelColor];
        
        self.tabs.selectedTabIndex = [UserPrefConstants singleton].orderTabIndex;
        [self.tabs updateTabAppearance];
        
        [_btnLoading setCenter:_tableView.center];
        [_btnLoading startAnimating];
        
        [_portfolioTableView setBackgroundColor:kPanelColor];
        [_tableView setBackgroundColor:kPanelColor];
        [_portFolioPanel setBackgroundColor:kPanelColor];
        
        [_orderHistPanel setBackgroundColor:kPanelColor];
        [_fromDateTitle setBackgroundColor:kPanelColor];
        [_toDateTitle setBackgroundColor:kPanelColor];
        
        uacd = [UserPrefConstants singleton].userSelectedAccount;
        
        _ordBkAccountName.text = [NSString stringWithFormat:@"%@ - %@ - %@",
                                  [uacd client_account_number],
                                  [uacd client_name],
                                  [uacd broker_branch_code]];
        
        _reviseButton.layer.cornerRadius = kButtonRadius * _reviseButton.frame.size.height;
        _reviseButton.layer.masksToBounds = YES;
        _cancelButton.layer.cornerRadius = kButtonRadius * _cancelButton.frame.size.height;
        _cancelButton.layer.masksToBounds = YES;
        _buyButton.layer.cornerRadius = kButtonRadius * _buyButton.frame.size.height;
        _buyButton.layer.masksToBounds = YES;
        _sellButton.layer.cornerRadius = kButtonRadius * _sellButton.frame.size.height;
        _sellButton.layer.masksToBounds = YES;
        
        CGFloat dur = [_ordBkAccountName.text length]/10.0;
        if (_accLabel==nil)
            _accLabel=[[MarqueeLabel alloc]initWithFrame:_ordBkAccountName.frame duration:dur andFadeLength:5.0f];
        [_accLabel setScrollDuration:dur];
        [self.view addSubview:_accLabel];
        [_accLabel setFont:_ordBkAccountName.font];
        _accLabel.text = _ordBkAccountName.text;
        [_ordBkAccountName setHidden:YES];
        [_accLabel setTextColor:[UIColor whiteColor]];
        
        [[LanguageManager defaultManager] applyAccessibilityId:self];
        [[LanguageManager defaultManager] translateStringsForViewController:self];
        
        _detailsContainer.frame = CGRectMake(_orderHistPanel.frame.size.width, _orderHistPanel.frame.origin.y,
                                             self.view.frame.size.width-_orderHistPanel.frame.size.width,
                                             self.view.frame.size.height-_tabsPlaceHolder.frame.size.height);
        
         [self reloadTabContents];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;

    if ([segueName isEqualToString:@"tradeAccountSegueID1"]) {
        TradeAccountController *tradeController = (TradeAccountController*)[segue destinationViewController];
        tradeController.delegate = self;
    }
}





- (void)viewDidLoad {
    [super viewDidLoad];
    [UserPrefConstants singleton].lastPFPage = 0;
    _atp = [ATPAuthenticate singleton];
    _vcm = [VertxConnectionManager singleton];
    ordBookRefreshCounter = 0;
    callOnce = YES;
    isLoadingOB = isLoadingPF = NO;
    
    if (![UserPrefConstants singleton].isShowTradingLimit) {
        _ordBkTradeLimit.hidden = YES;
        _tradeLimitTitle.hidden = YES;
    }
    
    self.transitionController = [[TransitionDelegate alloc] init];
    
    [_portfolioTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [_eqPortHeaderView setBackgroundColor:kPanelColor];
    [_ordBkHeaderView setBackgroundColor:kPanelColor];
    [_SummaryHeaderView setBackgroundColor:kPanelColor];
   
    [_portfolioTableView setHidden:YES];
    [_eqPortHeaderView setHidden:YES];
    [_ordBkHeaderView setHidden:YES];
    [_orderHistPanel setHidden:YES];
    [_SummaryHeaderView setHidden:YES];

    // [self reloadTabContents];
    
    [_btnLoading stopAnimating];

    selectedRow = -1;
    selectedRowPF = -1;
    _pfMode = 0;
    
    _searchHistoryBtn.layer.cornerRadius = kButtonRadius*_searchHistoryBtn.frame.size.height;
    _searchHistoryBtn.layer.masksToBounds = YES;
    _clearHistoryBtn.layer.cornerRadius = kButtonRadius*_clearHistoryBtn.frame.size.height;
    _clearHistoryBtn.layer.masksToBounds = YES;

    acclist =[[UserPrefConstants singleton]userAccountlist];
    if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
        [_atp getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount
                        forPaymentCUT:NO andCurrency:@""];
    priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishMarketDepth:) name:@"didFinishMarketDepth" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeLimitReceived) name:@"tradeLimitReceived" object:nil];
   
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_atp timeoutActivity];

    //NSLog(@"OrderBook View will appear");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SummaryReportDone) name:@"SummaryReportDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DetailReportDone) name:@"DetailReportDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SubDetailReportDone) name:@"SubDetailReportDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eqtyUnrealPFReceived) name:@"eqtyUnrealPFReceived" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderListReceived)    name:@"orderListReceived" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(halfSecondTimerTick) name:@"halfSecondTimerTick" object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(futuresSummaryReceived) name:@"futuresPFSummaryReceived" object:nil];
    
}


-(void)viewWillDisappear:(BOOL)animated
{    [super viewWillDisappear:animated];
    //NSLog(@"OrderBook View will Disappear");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"orderListReceived" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"eqtyUnrealPFReceived" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SubDetailReportDone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DetailReportDone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SummaryReportDone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"halfSecondTimerTick" object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"futuresPFSummaryReceived" object:nil];
    
}




#pragma mark - Other Metthods

//-(void)TradeSubmitStatus:(NSNotification *)notification
//{
//     dispatch_async(dispatch_get_main_queue(), ^{
//         NSDictionary * response = [notification.userInfo copy];
//         UIAlertView *alert;
//         NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
//         [dateFormatter setDateFormat:@"yyyymmddhhmmss"];
//
//         if ([[response objectForKey:@"tradesubmitstatus"] isEqual:@"Failed"]) {
//             alert =[[UIAlertView alloc]initWithTitle:@"TradeStatus" message:[response objectForKey:@"error"] delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
//         }  else{
//             
//             response =[response objectForKey:@"data"];
//             NSDate *date = [NSDate date];
//             [dateFormatter setDateFormat:@"yyyy/mm/dd hh:mm:ss a"];
//             NSString *msg=[NSString stringWithFormat:@"Ticket No: %@ Your order %@ - %@ %@ unit(s)@ %@ %@, Validity for %@ is submitted on %@ For the status of submitted order, please go to order book",
//                            [response objectForKey:@"+"],[response objectForKey:@"5"],
//                            [response objectForKey:@"4"],[response objectForKey:@";"],
//                            [response objectForKey:@"6"],[response objectForKey:@"<"],
//                            [response objectForKey:@"9"], [dateFormatter stringFromDate:date]];
//             alert =[[UIAlertView alloc]initWithTitle:@"TradeStatus" message:msg delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
//             
//         }
//         
//         NSLog(@"what");
//         
//         [_atp preparePortfolio:0];
//         if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
//             [_atp getTradeLimitForAccount:[acclist objectAtIndex:[UserPrefConstants singleton].userSelectedAccount ]];
//         
////         [self clickCancelOrderCancelButton:self];
////         [self clickCancelRevise:self];
//         [alert show];
//    });
//    
//}


-(NSString *)formatString:(float)var
{
    NSString *str = [NSString stringWithFormat:@"%.4f", var];
    return str;
}





#pragma mark - Table View Delegates

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView==_tableView) {
        return _ordBkHeaderView.frame.size.height;
    } else if (tableView==_portfolioTableView) {
        return _eqPortHeaderView.frame.size.height;
    } else if (tableView==_detailsTableview) {
        return 0;
    } else if (tableView==_summaryTable) {
        return _SummaryHeaderView.frame.size.height;
    }
    return 0;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView==_tableView) {
        [_ordBkHeaderView setHidden:NO];
        return _ordBkHeaderView;
    } else if (tableView==_portfolioTableView) {
        [_eqPortHeaderView setHidden:NO];
        if (_pfMode==1) {
            [_mktValHeader setHidden:NO];
        } else {
            [_mktValHeader setHidden:YES];
        }
        return _eqPortHeaderView;
    } else if (tableView==_detailsTableview) {
        return nil;
    } else if (tableView==_summaryTable) {
        [_SummaryHeaderView setHidden:NO];
        return _SummaryHeaderView;
    }
    
    return nil;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView==_detailsTableview) {
        cell.contentView.backgroundColor = kCellGray1;
    } else {
        if (indexPath.row % 2==0) {
            cell.contentView.backgroundColor = kCellGray1;
        } else {
            cell.contentView.backgroundColor = kCellGray2;
        }
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==_tableView) {
        if ([[[UserPrefConstants singleton] orderbooklist] count]>0) {
            tableView.backgroundView = nil;
            return 1;
        } else {
            
            if (!isLoadingOB) {
                // Display a message when the table is empty
                UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
                
                messageLabel.text = [LanguageManager stringForKey:@"No Results."];
                messageLabel.textColor = [UIColor whiteColor];
                messageLabel.numberOfLines = 0;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                [messageLabel sizeToFit];
                tableView.backgroundView = messageLabel;
            } else {
                tableView.backgroundView = nil;
            }
            return 0;
        }
        
    } else if (tableView==_portfolioTableView) {
        
        if ([[[UserPrefConstants singleton] eqtyUnrealPFResultList] count]>0) {
            tableView.backgroundView = nil;
            return 1;
        } else {
            if (!isLoadingPF) {
            // Display a message when the table is empty
                UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
                
                messageLabel.text = [LanguageManager stringForKey:@"No Results."];
                messageLabel.textColor = [UIColor whiteColor];
                messageLabel.numberOfLines = 0;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                [messageLabel sizeToFit];
                tableView.backgroundView = messageLabel;
            } else {
                tableView.backgroundView = nil;
            }
            return 0;
        }
    }else if (tableView==_detailsTableview) {
        
        if ([_detailsArray count]>0) {
            tableView.backgroundView = nil;
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
            return 1;
        } else {
        
                UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
                
                messageLabel.text = [LanguageManager stringForKey:@"No Results."];
                messageLabel.textColor = [UIColor whiteColor];
                messageLabel.numberOfLines = 0;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                [messageLabel sizeToFit];
                tableView.backgroundView = messageLabel;
                tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                [self setDetailNoData];
            
            
            return 0;
        }
     
    } else if (tableView==_virtualPLtable) {
        if ([_virtualPLData count]>0) {
            tableView.backgroundView = nil;
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
            return 1;
        } else {
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            
            messageLabel.text = [LanguageManager stringForKey:@"No Results."];
            messageLabel.textColor = [UIColor whiteColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            [messageLabel sizeToFit];
            tableView.backgroundView = messageLabel;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
             return 0;
        }
    } else if (tableView==_summaryTable) {
        
        if ([[UserPrefConstants singleton].subDetailReportArray count]>0) {
            tableView.backgroundView = nil;
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
            return 1;
        } else {
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
            
            messageLabel.text = [LanguageManager stringForKey:@"No Results."];
            messageLabel.textColor = [UIColor whiteColor];
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            [messageLabel sizeToFit];
            tableView.backgroundView = messageLabel;
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
             return 0;
        }
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView ==_tableView)
    {
            return [[[UserPrefConstants singleton]orderbooklist]count];
    }
    else if (tableView==_portfolioTableView)
    {
            return [[[UserPrefConstants singleton]eqtyUnrealPFResultList] count];
    }
    else if (tableView==_detailsTableview)
    {
            return [_detailsArray count];
    }
    else if (tableView==_virtualPLtable)
    {
            return [_virtualPLData count];
    }
    else if (tableView==_summaryTable)
    {
            return [[UserPrefConstants singleton].subDetailReportArray count];
    } else
            return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (tableView==_tableView) { // ORDERBOOK
            return 50;
    }
    
   else if (tableView==_portfolioTableView) { // PORTFOLIO
       
            return 50;
    }
    
   else if (tableView==_detailsTableview) { // DETAILS
       
       NSDictionary *eachDict = [self.detailsArray objectAtIndex:indexPath.row];
       if ([[eachDict objectForKey:@"Title"] isEqualToString:[LanguageManager stringForKey:@"Remarks"]]) {
           return 200;
       } else {
            return 34;
       }
   }
    
    else if (tableView==_portfolioTableView) { // VIRTUAL PL
        return 34;
    }
    else if (tableView==_summaryTable) { // Summary
        return 50;
    }
    else
        return 44;
}



// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSNumberFormatter *priceFormattercell = [NSNumberFormatter new];
    [priceFormattercell setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [priceFormattercell setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormattercell setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    
    NSNumberFormatter *priceFormattercell2 = [NSNumberFormatter new];
    [priceFormattercell2 setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [priceFormattercell2 setMinimumFractionDigits:2];
    [priceFormattercell2 setMaximumFractionDigits:2];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    // MARK: Orderbook Cell and Orderhistory
    if (tableView==_tableView) {
        
        static NSString *CellIdentifier = @"cellOrderStatus";
        OrderStatusTableViewCell *cell = (OrderStatusTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        if (cell==nil)
        {
            cell = [[OrderStatusTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        TradeStatus *ts =[[TradeStatus alloc]init];
        ts = [[[UserPrefConstants singleton]orderbooklist] objectAtIndex:indexPath.row];
        
        
        if (selectedRow==indexPath.row) {
            [cell.selectedArrow setHidden:NO];
        } else {
             [cell.selectedArrow setHidden:YES];
        }
        
        [cell populateHistoryWithData:ts];
        
        
        return cell;
        
    // MARK: Portfolio Cells
    }  else if (tableView==_portfolioTableView) {
        
        static NSString *CellIdentifier = @"cellEqPortfolio";
        EqPortfolioTableviewCellTableViewCell *cell = (EqPortfolioTableviewCellTableViewCell *) [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];

        if (cell==nil)
        {
            cell = [[EqPortfolioTableviewCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        if (indexPath.row==selectedRowPF) {
            [cell.labelStockArrow setHidden:NO];
        } else {
            [cell.labelStockArrow setHidden:YES];
        }
        
        cell.delegate = self;
        PortfolioData *pd =[[PortfolioData alloc]init];
        pd = [[[UserPrefConstants singleton]eqtyUnrealPFResultList]objectAtIndex:indexPath.row];
        cell.stkName.text =pd.stockname;
        [priceFormattercell setMaximumFractionDigits:2];
       
        
      
        
        NSDictionary *stkData = [[[QCData singleton] qcFeedDataDict] objectForKey:pd.stockcode];
        double price_current = [[stkData objectForKey:FID_98_D_LAST_DONE_PRICE] doubleValue];
        double price_ref = [[stkData objectForKey:FID_51_D_REF_PRICE] doubleValue];
        
        double gainLoss;
        NSString *gainLossPer;

        // unrealized
        if (_pfMode==1) {
            
            for(NSString *formula in [UserPrefConstants singleton].trxFeeFormulaDict){
                NSString *keyFormula = [[formula componentsSeparatedByString:@"="] objectAtIndex:0];
                if ([keyFormula isEqualToString:@"Amount"]) {
                    Amount = (double)(price_current * [pd.quantity_on_hand longLongValue]);
                    break;
                }
            }
            
            SCCPFee = (double)Amount*0.0001;
            SECFee = (double)Amount*0.00005;
            SalesTax = (double)Amount * 0.006;
            Commission = (double)Amount*0.0025;
            VAT = (double)Commission*0.12;
            BuyTrxFee = (double)Commission+SCCPFee+SECFee+VAT;
            SellTrxFee = (double)BuyTrxFee+(double)SalesTax;
            
            
            double market_value = (double)((price_current != 0 ? price_current : price_ref) * [pd.quantity_on_hand longLongValue]);
            double unrealized_gain_loss_amount = (double)(((price_current != 0 ? price_current : price_ref) - [pd.price_avg_purchase doubleValue]) * [pd.quantity_on_hand longLongValue]);
            double quantityOnHand = [pd.quantity_on_hand intValue];
            if ([UserPrefConstants singleton].isEnabledTRXFee) {
                if (quantityOnHand<0) {
                    unrealized_gain_loss_amount = ABS(unrealized_gain_loss_amount) - SellTrxFee;
                }else{
                    unrealized_gain_loss_amount = unrealized_gain_loss_amount - BuyTrxFee;
                }
                
            }
            
            double unrealized_gain_loss_percentage = (double)(unrealized_gain_loss_amount / ([pd.price_avg_purchase doubleValue] * [pd.quantity_on_hand longLongValue]) * 100);
            

            //Special handling when data is not available
            if ((price_current == 0 && price_ref == 0) || [pd.quantity_on_hand longLongValue] == 0) {
                market_value = 0;
            }
            if ((price_current == 0 && price_ref == 0) || [pd.price_avg_purchase doubleValue] == 0 || [pd.quantity_on_hand longLongValue] == 0) {
                unrealized_gain_loss_amount = 0;
                unrealized_gain_loss_percentage = 0;
            }
            
        
            
            gainLossPer = [NSString stringWithFormat:@"%.2f%%",unrealized_gain_loss_percentage];
            gainLoss = unrealized_gain_loss_amount;
            [cell.mktValue setHidden:NO];
            cell.qtyOnHand.text = [[UserPrefConstants singleton] abbreviateNumber:[pd.quantity_on_hand longLongValue]];
            cell.mktValue.text = [[UserPrefConstants singleton] abbreviatePrice:market_value];
            if ([cell.mktValue.text isEqualToString:@"-0.00"]) cell.mktValue.text = @"0.00";
            
        //realized
        } else if (_pfMode==0) {
            [cell.mktValue setHidden:YES];
            gainLossPer = [NSString stringWithFormat:@"%.2f%%",pd.realized_gain_loss_percentage.floatValue];
            gainLoss = pd.realized_gain_loss_amount.doubleValue;
        }
        
        cell.gainLoss.text = [[UserPrefConstants singleton] abbreviatePrice:gainLoss];
        cell.gainLoss.textColor = (gainLoss>0)?[UIColor greenColor]:[UIColor redColor];
        
        if ([cell.gainLoss.text isEqualToString:@"0.00"]) {
            cell.gainLoss.textColor = [UIColor greenColor];
        }
        
        cell.gainLossPer.text = gainLossPer;
        cell.gainLossPer.textColor = cell.gainLoss.textColor;
        
        [cell setClipsToBounds:YES];
        return cell;
   
    // MARK: DETAILS Cell
    } else if (tableView==_detailsTableview) {
            
        static NSString *CellIdentifier = @"DetailCellID";
        DetailsTableViewCell *cell = (DetailsTableViewCell *) [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil)
        {
            cell = [[DetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }

        NSDictionary *dict = [_detailsArray objectAtIndex:indexPath.row];
        
        cell.detailsTitle.text = [dict objectForKey:@"Title"];
        cell.detailsContent.text = [dict objectForKey:@"Content"];
        
        return cell;
        
      // MARK: VirtualPL   
    } else if (tableView==_virtualPLtable) {
        
        static NSString *CellIdentifier = @"VirtualCellID";
        VirtualPLTableViewCell *cell = (VirtualPLTableViewCell *) [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil)
        {
            cell = [[VirtualPLTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        NSDictionary *dict = [_virtualPLData objectAtIndex:indexPath.row];
        
        cell.titleLabel.text = [dict objectForKey:@"Title"];
        cell.contentLabel.text = [dict objectForKey:@"Content"];
        
        return cell;

        // MARK: Summary
    } else if (tableView==_summaryTable) {
        
        static NSString *CellIdentifier = @"SummaryCellID";
        SummaryTableViewCell *cell = (SummaryTableViewCell *) [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell==nil)
        {
            cell = [[SummaryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        PrtfSubDtlRptData *psdrd = [[UserPrefConstants singleton].subDetailReportArray objectAtIndex:indexPath.row];

        cell.nameLabel.text = psdrd.stock_name;
        cell.nettPosLabel.text = [NSString stringWithFormat:@"%d", psdrd.nett_position];
        cell.lastLabel.text = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:psdrd.price_current]];
        
        
        return cell;
        
        
    } else
        return 0;
    
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_atp timeoutActivity];

     // MARK: ORDERBOOK TABLE
    if (tableView==_tableView) {
        
        TradeStatus *ts =[[TradeStatus alloc]init];
        ts = [[[UserPrefConstants singleton]orderbooklist] objectAtIndex:indexPath.row];
        
        NSInteger prevRow = selectedRow;
        selectedRow =indexPath.row;
        
        if (prevRow !=-1) {
            NSIndexPath *prevPath = [NSIndexPath indexPathForRow:prevRow inSection:0];
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:prevPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        
        _stkCode = ts.stock_code;
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        // populate detailstable
        [self populateDetailsTable:ts];
    }
    
    // MARK: PORTFOLIO TABLE
    else if (tableView==_portfolioTableView) {
        
        PortfolioData *pd =[[[UserPrefConstants singleton]eqtyUnrealPFResultList] objectAtIndex:indexPath.row];
        [self.delegate stockPFDidSelected:pd.stockcode andName:pd.stockname watchList:NO];

        
        NSInteger prevRow = selectedRowPF;
        selectedRowPF =indexPath.row;
        
        if (prevRow !=-1) {
            NSIndexPath *prevPath = [NSIndexPath indexPathForRow:prevRow inSection:0];
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:prevPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        _stkCode = pd.stockcode;
        // populat details
        [self populateDetailsTablePF:pd];
    }
    
    // MARK: DETAILS TABLE
    else if (tableView==_detailsTableview) {
        
//        TradeStatus *ts =[[[UserPrefConstants singleton] orderHistorylist] objectAtIndex:indexPath.row];
//        _stkCode = pd.stockcode;
//        [self.delegate stockPFDidSelected:pd.stockcode andName:pd.stockname watchList:NO];
//        [self openStockContainer:YES animated:YES];
//        
//        NSInteger prevRow = selectedRowPF;
//        selectedRowPF =indexPath.row;
//        
//        if (prevRow !=-1) {
//            NSIndexPath *prevPath = [NSIndexPath indexPathForRow:prevRow inSection:0];
//            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:prevPath] withRowAnimation:UITableViewRowAnimationFade];
//        }
//        
//        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }

}



#pragma mark - New Independent Trade VC

-(void)goTrade:(NSString*)action {
    // detects button sender and set SELL/BUY accordingly
    
    [_atp timeoutActivity];
    
    NSDictionary *dict;
    
    [UserPrefConstants singleton].userSelectingStockCode = _stkCode;
    
    if ([action isEqualToString:@"Buy"]) {
        
        NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
        [notificationData setObject:_stkCode forKey:@"stkCode"];
        [UserPrefConstants singleton].callBuySellReviseCancel = 1;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
        return;
    }
    
    if ([action isEqualToString:@"Sell"])  {
        
        NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
        [notificationData setObject:_stkCode forKey:@"stkCode"];
        [UserPrefConstants singleton].callBuySellReviseCancel = 2;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
        return;
     }
    
    if ([action isEqualToString:@"Revise"])  {
        
        NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
        [notificationData setObject:_stkCode forKey:@"stkCode"];
        [UserPrefConstants singleton].callBuySellReviseCancel = 3;
        
        tradeStatusRevise =[[TradeStatus alloc]init];
        tradeStatusRevise =[[[UserPrefConstants singleton]orderbooklist]objectAtIndex:selectedRow];
        _stkCode = tradeStatusRevise.stock_code;
        stkCodeAndExchangeArr= [ _stkCode componentsSeparatedByString:@"."];
        trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
        
        NSNumber *defPrice = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"];
        if (defPrice==nil) defPrice = [NSNumber numberWithFloat:0.0];
        
         if ([UserPrefConstants singleton].pointerDecimal==3) {
             dict = [NSDictionary dictionaryWithObjectsAndKeys:
                     @"REVISE",@"Action",
                     tradeStatusRevise.stock_code,@"StockCode",
                     tradeStatusRevise.stock_name,@"StockName",
                     [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                     [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                     defPrice,@"DefaultPrice",
                     [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], tradeStatusRevise.stock_name],@"stkNname",
                     trdRules, @"TradeRules",
                     tradeStatusRevise, @"TradeStatus",
                     nil];
         }else{
             dict = [NSDictionary dictionaryWithObjectsAndKeys:
                     @"REVISE",@"Action",
                     tradeStatusRevise.stock_code,@"StockCode",
                     tradeStatusRevise.stock_name,@"StockName",
                     [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                     [NSString stringWithFormat:@"%.4f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                     defPrice,@"DefaultPrice",
                     [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], tradeStatusRevise.stock_name],@"stkNname",
                     trdRules, @"TradeRules",
                     tradeStatusRevise, @"TradeStatus",
                     nil];
         }
        
        
        [UserPrefConstants singleton].reviseCancelDict = [dict copy];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
        return;
    }
    
    if ([action isEqualToString:@"Cancel"])  {
        
        NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
        [notificationData setObject:_stkCode forKey:@"stkCode"];
        [UserPrefConstants singleton].callBuySellReviseCancel = 3;
        
        tradeStatusCancel =[[TradeStatus alloc]init];
        tradeStatusCancel =[[[UserPrefConstants singleton]orderbooklist]objectAtIndex:selectedRow];
        
        // NSLog(@"CANCEL TS: %@",tradeStatusCancel.stock_name);
        
        _stkCode = tradeStatusCancel.stock_code;
        stkCodeAndExchangeArr= [ _stkCode componentsSeparatedByString:@"."];
        trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
        
        NSNumber *defPrice = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"];
        if (defPrice==nil) defPrice = [NSNumber numberWithFloat:0.0];
        
         if ([UserPrefConstants singleton].pointerDecimal==3) {
        
        dict = [NSDictionary dictionaryWithObjectsAndKeys:
                @"CANCEL",@"Action",
                tradeStatusCancel.stock_code,@"StockCode",
                tradeStatusCancel.stock_name,@"StockName",
                [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                defPrice,@"DefaultPrice",
                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], tradeStatusCancel.stock_name],@"stkNname",
                trdRules, @"TradeRules",
                tradeStatusCancel, @"TradeStatus",
                nil];
         }else{
             dict = [NSDictionary dictionaryWithObjectsAndKeys:
                     @"CANCEL",@"Action",
                     tradeStatusCancel.stock_code,@"StockCode",
                     tradeStatusCancel.stock_name,@"StockName",
                     [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
                     [NSString stringWithFormat:@"%.4f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
                     defPrice,@"DefaultPrice",
                     [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], tradeStatusCancel.stock_name],@"stkNname",
                     trdRules, @"TradeRules",
                     tradeStatusCancel, @"TradeStatus",
                     nil];
         }
        
        [UserPrefConstants singleton].reviseCancelDict = [dict copy];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
        return;
    }
    
//    if ([action isEqualToString:@"Buy"]) {
//        if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
//            [_atp getTradeLimitForAccount:[acclist objectAtIndex:[UserPrefConstants singleton].userSelectedAccount ]];
//        
//        PortfolioData *pd =[[[UserPrefConstants singleton]eqtyUnrealPFResultList] objectAtIndex:selectedRow];
//        _stkCode = pd.stockcode;
//        stkCodeAndExchangeArr= [ _stkCode componentsSeparatedByString:@"."];
//        trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
//        
//        [self getMarketDepth:_stkCode];
//        
//        dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                @"BUY",@"Action",
//                pd.stockcode,@"StockCode",
//                pd.stockname,@"StockName",
//                
//                [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
//                [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
//                [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"],@"DefaultPrice",
//                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], pd.stockname],@"stkNname",
//                trdRules, @"TradeRules",
//                nil];
//        
//    } else if ([action isEqualToString:@"Sell"])  {
//        
//        if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
//            [_atp getTradeLimitForAccount:[acclist objectAtIndex:[UserPrefConstants singleton].userSelectedAccount]];
//        
//        PortfolioData *pd =[[[UserPrefConstants singleton]eqtyUnrealPFResultList] objectAtIndex:selectedRow];
//        _stkCode = pd.stockcode;
//        stkCodeAndExchangeArr= [ _stkCode componentsSeparatedByString:@"."];
//        trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
//        
//        [self getMarketDepth:_stkCode];
//        
//        dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                @"SELL",@"Action",
//                pd.stockcode,@"StockCode",
//                pd.stockname,@"StockName",
//                [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
//                [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
//                [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"],@"DefaultPrice",
//                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], pd.stockname],@"stkNname",
//                trdRules, @"TradeRules",
//                nil];
//    } else
    
//        if ([action isEqualToString:@"Revise"])  {
//        
//
//        
//        tradeStatusRevise =[[TradeStatus alloc]init];
//        tradeStatusRevise =[[[UserPrefConstants singleton]orderbooklist]objectAtIndex:selectedRow];
//        
//        _stkCode = tradeStatusRevise.stock_code;
//        stkCodeAndExchangeArr= [ _stkCode componentsSeparatedByString:@"."];
//        trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
//       
//        _stkCode = tradeStatusRevise.stock_code;
//        [self getMarketDepth:_stkCode];
//        
//        dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                @"REVISE",@"Action",
//                tradeStatusRevise.stock_code,@"StockCode",
//                tradeStatusRevise.stock_name,@"StockName",
//                [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
//                [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
//                [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"],@"DefaultPrice",
//                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], tradeStatusRevise.stock_name],@"stkNname",
//                trdRules, @"TradeRules",
//                tradeStatusRevise, @"TradeStatus",
//                nil];
//        
//    } else
    
//    if ([action isEqualToString:@"Cancel"])  {
//        
//        if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
//            [_atp getTradeLimitForAccount:[acclist objectAtIndex:[UserPrefConstants singleton].userSelectedAccount]];
//        
//        tradeStatusCancel =[[TradeStatus alloc]init];
//        tradeStatusCancel =[[[UserPrefConstants singleton]orderbooklist]objectAtIndex:selectedRow];
//        
//        _stkCode = tradeStatusCancel.stock_code;
//        stkCodeAndExchangeArr= [ _stkCode componentsSeparatedByString:@"."];
//        trdRules = [[[UserPrefConstants singleton]tradingRules]objectForKey:[stkCodeAndExchangeArr objectAtIndex:1]];
//        
//        _stkCode = tradeStatusCancel.stock_code;
//        
//        dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                @"CANCEL",@"Action",
//                tradeStatusCancel.stock_code,@"StockCode",
//                tradeStatusCancel.stock_name,@"StockName",
//                [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle],@"Date",
//                [NSString stringWithFormat:@"%.3f",[[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"] floatValue]],@"StockPrice",
//                [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:@"88"],@"DefaultPrice",
//                [NSString stringWithFormat:@"%@ / %@",[stkCodeAndExchangeArr objectAtIndex:0], tradeStatusCancel.stock_name],@"stkNname",
//                trdRules, @"TradeRules",
//                tradeStatusCancel, @"TradeStatus",
//                nil];
//    } else {
//        
//        NSLog(@"Unknown Sender");
//        
//    }
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    TraderViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"TraderViewController"];
//    vc.actionTypeDict = [NSDictionary dictionaryWithDictionary:dict];
//    [vc setTransitioningDelegate:_transitionController];
//    vc.modalPresentationStyle= UIModalPresentationCustom;
//    [self presentViewController:vc animated:NO completion:nil];
    
}

#pragma mark - BUY / SELL / REVISE / CANCEL




- (IBAction)buyorderBtnPressed:(UIButton *)sender
{
    [_atp timeoutActivity];
    
   
    
     [self goTrade:@"Buy"];
    

    
}


- (IBAction)sellorderBtnPressed:(UIButton *)sender
{
    [_atp timeoutActivity];
    

    
    [self goTrade:@"Sell"];
    
}



-(IBAction)reviseOrderBtnPressed:(UIButton *)sender
{
    
    [_atp timeoutActivity];
    
  
    
    [self goTrade:@"Revise"];
}


-(IBAction)cancelOrderBtnPressed:(UIButton *)sender{
    
    [_atp timeoutActivity];
    
    
    [self goTrade:@"Cancel"];
    
}


- (void)getMarketDepth:(NSString *)stkcode
{
   NSArray *arrayWithTwoStrings = [stkcode componentsSeparatedByString:@"."];
    
    if (stkcode !=nil) {
        [_vcm vertxGetMarketDepth:stkcode  andCollarr:[arrayWithTwoStrings objectAtIndex:1]];
        
    }
    
}

-(void)didFinishMarketDepth:(NSNotification *)notification
{
    NSDictionary * response = [notification.userInfo copy];
    NSArray *data =[response objectForKey:@"marketdepthresults"];
    marketDepthArr = [NSMutableArray arrayWithArray:data];
    
}

#pragma mark - ATP REPLY

-(void)tradeLimitReceived {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        double _resolvedLimit = 0;
        
        uacd = [UserPrefConstants singleton].userSelectedAccount;
        
        // default:
        _resolvedLimit = uacd.credit_limit;
        _tradeLimitTitle.text = [LanguageManager stringForKey:@"Trading Limit:"];

        
        if (![UserPrefConstants singleton].CreditLimitOrdPad) {
            
            _tradeLimitTitle.text = @"";
            _ordBkTradeLimit.text = @"";
            
            
        } else {

            if ([uacd.account_type isEqualToString:@"B"]) {
                _tradeLimitTitle.text = [LanguageManager stringForKey:@"Investment Power:"];
                _resolvedLimit = uacd.credit_limit;
                
            } else {
                if ([[[UserPrefConstants singleton].clientLimitOptionDict objectForKey:uacd.client_acc_exchange] intValue] == 3) {
                    if (uacd.isMarginAccount==0) {
                        //                if ([actualAction isEqualToString:@"BUY"]) {
                        _resolvedLimit = uacd.buy_limit;
                        _tradeLimitTitle.text = [LanguageManager stringForKey:@"Buy Limit:"];
                        //                } else {
                        //                    _resolvedLimit = uacd.sell_limit;
                        //                    _tradeLimitTitle.text = [LanguageManager stringForKey:@"Sell Limit:"];
                        //                }
                    } else {
                        
                        _resolvedLimit = uacd.credit_limit;
                        _tradeLimitTitle.text = [LanguageManager stringForKey:@"Trading Limit:"];
                    }
                }
            }

            _ordBkTradeLimit.text = [NSString stringWithFormat:@"%@ %@" ,
                                     [UserPrefConstants singleton].defaultCurrency,
                                     [priceFormatter stringFromNumber:[NSNumber numberWithDouble:_resolvedLimit]]];

        }
        
        
        
        
       // NSLog(@"uacd: %@", uacd);
        
       // NSLog(@"_resolvedLimit %f",_resolvedLimit);
        
        
        // Adjust label sizes/position
        CGFloat titleHeight = _tradeLimitTitle.frame.size.height;
        [_tradeLimitTitle sizeToFit];
        _tradeLimitTitle.frame = CGRectMake(_tradeLimitTitle.frame.origin.x, _tradeLimitTitle.frame.origin.y,
                                            _tradeLimitTitle.frame.size.width, titleHeight);
        
        
        _ordBkTradeLimit.frame = CGRectMake(_tradeLimitTitle.frame.origin.x+_tradeLimitTitle.frame.size.width,
                                       _ordBkTradeLimit.frame.origin.y, _ordBkTradeLimit.frame.size.width, _ordBkTradeLimit.frame.size.height);
        
        
        
        
        
    });
    
   
    
}

-(void)orderBkStatus:(NSNotification *)notification
{
    
    NSDictionary * response = [notification.userInfo copy];
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[response objectForKey:@"loginStatus"]
                                                  message:[response objectForKey:@"msg"]
                                                 delegate:self
                                        cancelButtonTitle:[LanguageManager stringForKey:@"Done"] otherButtonTitles:nil];
    [_atp getTradeStatus:0];
    
    
    if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
        [_atp getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount
                        forPaymentCUT:NO andCurrency:@""];

    dispatch_async(dispatch_get_main_queue(), ^{
        

        [alert show];
    });

}

-(void)selectFirstOrderList {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    [_tableView selectRowAtIndexPath:indexPath
//                                     animated:NO
//                               scrollPosition:UITableViewScrollPositionNone];
    [self tableView:_tableView didSelectRowAtIndexPath:indexPath];
}

-(void)selectFirstPF {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    [_portfolioTableView selectRowAtIndexPath:indexPath
//                                     animated:NO
//                               scrollPosition:UITableViewScrollPositionNone];
    [self tableView:_portfolioTableView didSelectRowAtIndexPath:indexPath];
}

// Portfolio received
-(void)eqtyUnrealPFReceived
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"stockname" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[[UserPrefConstants singleton]eqtyUnrealPFResultList] sortedArrayUsingDescriptors:sortDescriptors];
        [UserPrefConstants singleton].eqtyUnrealPFResultList = [[NSMutableArray alloc] initWithArray:sortedArray];
        
        
        isLoadingPF = NO;
        [self.portfolioTableView reloadData];

        
        if ([[UserPrefConstants singleton].eqtyUnrealPFResultList count]>0) {
            [self selectFirstPF];
        }
        
        [_btnLoading stopAnimating];
        
    });
}
- (void) futuresSummaryReceived{
     dispatch_async(dispatch_get_main_queue(), ^{
         
         if ([[UserPrefConstants singleton].summaryReportArray count]>0) {
             
             [self populateVirtualPL];
             //  [self populateVirtualPL_withCalculation];
         }
     });
}



-(void)orderListReceived
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order_time" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[[UserPrefConstants singleton] orderbooklist] sortedArrayUsingDescriptors:sortDescriptors];
        
        [UserPrefConstants singleton].orderbooklist = [[NSMutableArray alloc] initWithArray:sortedArray];
        
        isLoadingOB = NO;
        [self.tableView reloadData];
        
        if ([[UserPrefConstants singleton].orderbooklist count]>0) {
            // then select first record
            [self selectFirstOrderList];
        }
        
        [_btnLoading stopAnimating];
    });
}


// SUMMARY
-(void)SubDetailReportDone {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"subDetailReportArray %@",[UserPrefConstants singleton].subDetailReportArray);
        
        if ([[UserPrefConstants singleton].subDetailReportArray count]>0) {
            
            [_summaryTable reloadData];
        }
        
    });
}

-(void)DetailReportDone {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"detailDayReportArray %@",[UserPrefConstants singleton].detailDayReportArray);
        NSLog(@"detailNightReportArray %@",[UserPrefConstants singleton].detailNightReportArray);
        

        
    });
}

// Virtual P/L
-(void)SummaryReportDone {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        NSLog(@"summaryReportArray %@",[UserPrefConstants singleton].summaryReportArray);
        // load first item into virtualPLData
        if ([[UserPrefConstants singleton].summaryReportArray count]>0) {
            
            [self populateVirtualPL];
          //  [self populateVirtualPL_withCalculation];
        }
        
    });
}

- (void) populateVirtualPL_withCalculation{
    [self calculate_TotalUnrealized_TotalGrBuySell];
}

-(void)calculate_TotalUnrealized_TotalGrBuySell{
    
  
}

#pragma mark - Button Pressed


- (IBAction)accoutnBtnPressed:(UIButton *)sender {
    [_atp timeoutActivity];
    
    [self performSegueWithIdentifier:@"tradeAccountSegueID1" sender:self];

//    viewcont =[[UIViewController alloc]init];
//    [accTableView reloadData];
//    
//    [viewcont.view addSubview:accTableView];
//    popoverController=viewcont.popoverPresentationController;
//    viewcont.preferredContentSize=CGSizeMake(300,totalAccToShow*44);
//    if (popoverController)
//    {
//        popoverController.sourceView = sender;
//        popoverController.sourceRect = sender.bounds;
//        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
//    }
//    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:viewcont];
//    [columnPickerPopover presentPopoverFromRect:sender.frame
//                                         inView:sender.superview
//                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)searchHistory:(id)sender {
    
    if ((_fromDate!=nil)&&(_toDate!=nil)) {
        
        [UserPrefConstants singleton].orderbooklist = [[NSMutableArray alloc] init];
        [UserPrefConstants singleton].orderbookliststkCodeArr = [[NSMutableArray alloc] init];
        
        isLoadingPF = NO; isLoadingOB = YES;
        [_btnLoading startAnimating];
        [_tableView reloadData];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMdd"];
        NSString *stringFromDate = [formatter stringFromDate:_fromDate];
        NSString *stringToDate = [formatter stringFromDate:_toDate];
        [_atp getTradeHistoryFromDate:stringFromDate toDate:stringToDate];
        
        // if date include today date then getTradeStatus too.
        
        NSString *todayDate = [formatter stringFromDate:[NSDate date]];
        if ([stringToDate isEqualToString:todayDate]) {
            [_atp getTradeStatus:0];
        }
        
    } else {
        [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"Select both dates first."] inController:self
          withTitle:@""];
        
    }
}

-(void)updateFromDate:(NSDate*)date {
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];
    _dateFromLabel.text = str;
    _fromDate = date;
    
    [self validateDates];
}

-(void)updateToDate:(NSDate*)date {
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];
    _dateToLabel.text = str;
    _toDate = date;
    
    [self validateDates];
    
}


-(void)validateDates {
    
    [_searchHistoryBtn setEnabled:NO];

    // dont validate if any date is still not selected
    if (([_dateToLabel.text length]<=0)||([_dateFromLabel.text length]<=0)) return;
    
    //1. check either date must be less or equal than today
    NSTimeInterval fromDifference = [[NSDate date] timeIntervalSince1970] - [_fromDate timeIntervalSince1970];
    if (fromDifference<0) {
        _dateFromLabel.textColor = [UIColor redColor];
        _errorLabel.text = [LanguageManager stringForKey:@"From Date must be Today or lesser!"];
        return;
    } else {
        _dateFromLabel.textColor = [UIColor whiteColor];
    }
    
    NSTimeInterval toDifference = [[NSDate date] timeIntervalSince1970] - [_toDate timeIntervalSince1970];
    if (toDifference<0) {
        _dateToLabel.textColor = [UIColor redColor];
        _errorLabel.text = [LanguageManager stringForKey:@"To Date must be Today or lesser!"];
        return;
    } else {
        _dateToLabel.textColor = [UIColor whiteColor];
    }
    
    //2. check fromDate must be less than toDate
    NSTimeInterval difference = [_toDate timeIntervalSince1970] - [_fromDate timeIntervalSince1970];
    if (difference<0) {
        _dateFromLabel.textColor = _dateToLabel.textColor = [UIColor redColor];
        _errorLabel.text = [LanguageManager stringForKey:@"From Date must be less than To Date!"];
        return;
    } else {
        // pass
        _dateFromLabel.textColor = _dateToLabel.textColor = [UIColor whiteColor];
    }
    
    // 3. check date range must be 30 days.
    NSInteger orderLimitDays = [[UserPrefConstants singleton].orderHistLimit integerValue];
    NSTimeInterval limitDays = (1+orderLimitDays)*24*60*60;
  
    if (difference>limitDays) {
        _errorLabel.text = [LanguageManager stringForKey:@"Range of days must be less than 30 days"];
         _dateFromLabel.textColor = [UIColor redColor];
         _dateToLabel.textColor = [UIColor redColor];
        return;
    } else {
        _dateFromLabel.textColor = [UIColor whiteColor];
        _dateToLabel.textColor = [UIColor whiteColor];
    }
    
    
    _errorLabel.text = @"";
    // if all checks passed, enable button
     [_searchHistoryBtn setEnabled:YES];
}


#pragma mark - DatePicker Delegate

-(void)n2datePickerSelected:(NSDate *)date {
    
    // check if date chosen is bigger than today
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd"];
    NSString *todayDateStr = [formatter stringFromDate:[NSDate date]];
    NSString *selectedDateStr = [formatter stringFromDate:date];
    
    NSDate *todayDate = [formatter dateFromString:todayDateStr];
    NSDate *selectedDate = [formatter dateFromString:selectedDateStr];

    NSTimeInterval dateDifference = [todayDate timeIntervalSince1970] - [selectedDate timeIntervalSince1970];
    if (dateDifference<0) {
        [datePickerView setPickerDate:todayDate animated:YES];
        return;
    }
    
    
    if (updatingDate==1) {
        [self updateFromDate:date];
    } else if (updatingDate==2) {
        [self updateToDate:date];
    }
}

- (IBAction)callDatePicker:(UIButton*)sender {
    
    UIViewController *temp =[[UIViewController alloc]init];
//    datePickerView = [[UIDatePicker alloc]init];
//    datePickerView.datePickerMode = UIDatePickerModeDate;
// datePickerView.tag = 2;

    
    // from date
    if (sender.tag==1) {
        updatingDate = 1;
       // [datePickerView addTarget:self action:@selector(updateFromDate:) forControlEvents:UIControlEventValueChanged];
        if (_fromDate==nil) {
            datePickerView = [[N2DatePicker alloc] initWithDate:[NSDate date]];
            datePickerView.myDelegate = self;
            _fromDate = [NSDate date];
        } else {
            datePickerView = [[N2DatePicker alloc] initWithDate:_fromDate];
            datePickerView.myDelegate = self;
        }
        [self updateFromDate:_fromDate];
    // to date
    } else if (sender.tag==2) {
        updatingDate = 2;
       // [datePickerView addTarget:self action:@selector(updateToDate:) forControlEvents:UIControlEventValueChanged];
        if (_toDate==nil) {
            datePickerView = [[N2DatePicker alloc] initWithDate:[NSDate date]];
            datePickerView.myDelegate = self;
            _toDate = [NSDate date];
        } else {
            datePickerView = [[N2DatePicker alloc] initWithDate:_toDate];
            datePickerView.myDelegate = self;
        }
        [self updateToDate:_toDate];
    }

    
   
   
    [temp.view addSubview:datePickerView];
    popoverController=temp.popoverPresentationController;
    temp.preferredContentSize=CGSizeMake(320,216);
    if (popoverController)
    {
        popoverController.sourceView = sender;
        popoverController.sourceRect = sender.bounds;
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:temp];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

- (IBAction)ClickSubmitButton:(UIButton *)sender {
//    [_atp timeoutActivity];
//
//    //TradeStatus *tempts;
//    if (isPortfolio) {
//       // PortfolioData *pd =[[[UserPrefConstants singleton]eqtyUnrealPFResultList] objectAtIndex:selectedRow];
//        
//
//    }
//    
//    
//    if([[NSUserDefaults standardUserDefaults]objectForKey:@"Rememberme"])
//    {
//        [[NSUserDefaults standardUserDefaults]setObject:self.cancelTradingPinfield.text forKey:@"TradingPin"];
//        [[NSUserDefaults standardUserDefaults]setObject:self.reviseTradingPinField.text forKey:@"TradingPin"];
//    }
//    else
//    {
//        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"TradingPin"];
//    }
//    [[NSUserDefaults standardUserDefaults]synchronize];
//    orderDetails = [[OrderDetails alloc] init];
//
//    if (sender.tag==1) {
//        if (isPortfolio)
//            orderDetails.order_action = @"Buy";
//        else
//        orderDetails.order_action = @"Revise";
//        orderDetails.price =[_reviseStkPrice.text doubleValue];
//        orderDetails.quantity =_reviseQuantity.text.intValue*100;
//        orderDetails.pin =_reviseTradingPinField.text;
//       
//        
//    }
//    else if(sender.tag==2)
//    {
//        if (isPortfolio)
//            orderDetails.order_action = @"Sell";
//        else
//        orderDetails.order_action = @"Cancel";
//        orderDetails.price =[_cancelStkPrice.text doubleValue];
//        orderDetails.quantity =_cancelQuantity.text.intValue*100;
//        orderDetails.pin =_cancelTradingPinfield.text;
//       
//        
//    }
//    [self.view endEditing:YES];
//    //NSLog(@"%@",_stkCode);
//    orderDetails.ticker = stockName;
//    orderDetails.stock_code =_stkCode;
//    orderDetails.company_name = [[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey:FID_39_S_COMPANY] ;
//    orderDetails.order_type =@"Limit";
//    orderDetails.validity = [trdRules.validityList objectAtIndex:validityRow];
//    orderDetails.payment_type =@"";
//    if ([orderDetails.validity isEqualToString:@"GTD"]) {
//        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
//        [ndf setDateFormat:@"yyyyMMdd"];
//        orderDetails.expiry = [ndf stringFromDate:datePickerView.date];
//    }
//    if (tradeStatusRevise.order_price.doubleValue == orderDetails.price && tradeStatusRevise.order_quantity.longLongValue==orderDetails.quantity && tradeStatusRevise.validity ==orderDetails.validity &&[orderDetails.order_action isEqual:@"Revise"]) {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Revise order is not Valid" delegate:self cancelButtonTitle:@"Done" otherButtonTitles: nil];
//        [alert show];
//        return ;
//    }
//    
//    orderDetails.currency =[[[[QCData singleton]qcFeedDataDict]objectForKey:_stkCode] objectForKey: FID_134_S_CURRENCY];
//    userTradeConfirmData = [[UserPrefConstants singleton].userAccountlist objectAtIndex:[UserPrefConstants singleton].userSelectedAccount];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    
//    if (_cancelSkipConfirmation.selected||_reviseBtnSkipConfirmation.selected) {
//        
//        if (isPortfolio)
//            [self doTrade];
//        else
//        {
//        if (sender.tag==1)
//        [self doReviseOrder];
//        else if (sender.tag==2)
//            [self doCancel];
//    }
//    }
//    else
//    {
//        skipVC = [storyboard instantiateViewControllerWithIdentifier:@"SkipConfirmationViewController"];
//        //[skipVC setTransitioningDelegate:transitionController];
//        skipVC.modalPresentationStyle= UIModalPresentationFormSheet;
//        skipVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
//        skipVC.preferredContentSize =CGSizeMake(320, 400);
//        skipVC.delegate =self;
//        [self presentViewController:skipVC animated:YES completion:nil];
//        [skipVC loadData:userTradeConfirmData.client_account_number orderdetails:orderDetails];
//    }
    
}



//-(void)doReviseOrder
//{
//    //NSLog(@"Called");
//     [self clickCancelRevise:self];
//    if (tradeStatusRevise == nil) {
//        tradeStatusRevise =[[[UserPrefConstants singleton]orderbooklist] objectAtIndex:selectedRow];
//
//    }
//[_atp prepareTradeRevise:NO forAccount:userTradeConfirmData forOrder:orderDetails forExistingOrder:tradeStatusRevise];
//    
//}
//
//-(void)doCancel
//{
//    
//    [self clickCancelOrderCancelButton:self];
//    [_atp prepareTradeCancel:nil forAccount:userTradeConfirmData forOrder:orderDetails forExistingOrder:tradeStatusCancel];
//
//
//}
// -(void)doTrade
//{
//    _atp.delegate =self;
//   [_atp prepareTradeSubmit:NO forAccount:userTradeConfirmData forOrder:orderDetails ];
//                
//}


#pragma mark - Setting up each tab contents
//
//-(void)orderHistoryPressed {
//    [_atp timeoutActivity];
//
//    
//    [_orderHistPanel setHidden:NO];
//    [_portFolioScroll setHidden:YES];
//    [_tableView setHidden:YES];
//    
//    isPortfolio = NO;
//    
//    [[[UserPrefConstants singleton] orderHistorylist] removeAllObjects];
//    [_tableView reloadData];
//    
//    // check both date if exist or not, if exist, set the label, and go ATP tradeHistory
//    // if not, do nothing (wait user press Search History then do something)
//    
//    if ((_fromDate!=nil)&&(_toDate!=nil)) {
//        
//        isLoadingPF = NO; isLoadingOB = NO;  isLoadingOH = YES;
//        
//        _btnLoading.center = _portfolioTableView.center;
//        [_btnLoading startAnimating];
//
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyyMMdd"];
//        NSString *stringFromDate = [formatter stringFromDate:_fromDate];
//        NSString *stringToDate = [formatter stringFromDate:_toDate];
//        
//        [_atp getTradeHistoryFromDate:stringFromDate toDate:stringToDate];
//    }
//}

-(void)pfButtonDidClicked:(UIButton*)sender {
    
    long which = sender.tag - 1000;
    // only allow loading if different tab is picked
    if (_pfMode==which) {
        _pfMode = (which==0)?1:0;
        selectedRowPF = -1;
        [self portfolioBtnPressed];
    }
}

-(void)updatePortfolioButtons {
    
    // reverse it
    int which = (_pfMode==1)?0:1;
    
    for (UIView *view in _portFolioPanel.subviews) {
        
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton*)view;
            
            if (which+1000==btn.tag) {
                 [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                UIView *line = [btn viewWithTag:1000+btn.tag];
                [line setBackgroundColor:[UIColor whiteColor]];
            } else {
                 [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                UIView *line = [btn viewWithTag:1000+btn.tag];
                [line setBackgroundColor:[UIColor darkGrayColor]];
            }
        }
    }
}

-(void)createPortfolioButtonsFromArr:(NSArray*)btnArray {
    
    // remove everything.
     for (UIView *view in _portFolioPanel.subviews) {
         [view removeFromSuperview];
     }
    
    CGFloat gap = 0;
    CGFloat totBtnWid = _portFolioPanel.frame.size.width - gap*([btnArray count]-1);
    CGFloat btnWidth = totBtnWid / [btnArray count];
    CGFloat offX = 0;
    CGFloat lineHeight = 2;
    
    for (long i=0; i<[btnArray count]; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 1000+i;
        NSString *str = [btnArray objectAtIndex:i];
        [btn setTitle:str forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(pfButtonDidClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
        btn.backgroundColor = kPanelColor;
        [btn setFrame:CGRectMake(offX, 0, btnWidth, _portFolioPanel.frame.size.height-10)];
        btn.center = CGPointMake(btn.center.x, _portFolioPanel.frame.size.height/2.0);
        [_portFolioPanel addSubview:btn];
    
        
        UIView *bottomLine =  [[UIView alloc] initWithFrame:CGRectMake(0, btn.frame.size.height-lineHeight,
                                                                       btn.frame.size.width, lineHeight)];
        bottomLine.tag = 2000+i;
        [bottomLine setBackgroundColor:[UIColor darkGrayColor]];
        [btn addSubview:bottomLine];
        
        offX += gap+btnWidth;
    }
    
}

#pragma mark - Details Data

-(void)setDetailNoData {
    
    [_cancelButton setHidden:YES];
    [_reviseButton setHidden:YES];
    [_buyButton setHidden:YES];
    [_sellButton setHidden:YES];
    _detailLastDone.text = @"";
    _detailStkCode.text = @"";
    _detailStkName.text = @"";
    _detailChange.text = @"";
}


- (double) calculateUnrealizedPLWith:(PrtfSubDtlRptData *)psdrd {
    
    NSLog(@"calculateUnrealizedPLWith %@", [[QCData singleton] qcFeedDataDict]);
    NSLog(@"PrtfSubDtlRptData %@",psdrd.stock_code);
    NSDictionary *stkData = [[[QCData singleton] qcFeedDataDict] objectForKey:psdrd.stock_code];
    double price_current = [[stkData objectForKey:FID_98_D_LAST_DONE_PRICE] doubleValue];
    double price_ref = [[stkData objectForKey:FID_51_D_REF_PRICE] doubleValue];
    double average_purchase_price = [psdrd.average_purchase_price doubleValue];
    
    double last_done_price = price_current != 0 ? price_current : price_ref;
    
    int nett_position = psdrd.nett_position;
    double multiplier = psdrd.contractPerVal;
    double result = 0;
    
    if (last_done_price == average_purchase_price) {
        result = 0;
    }
    else {
        result = (last_done_price - average_purchase_price) * nett_position * multiplier;
    }
    
    
    NSLog(@"Stock %@", psdrd.stock_name);
    NSLog(@"Last Done Price %f", last_done_price);
    NSLog(@"Average Price %f", average_purchase_price);
    NSLog(@"Nett Position %d", nett_position);
    NSLog(@"Multiplier %f", multiplier);
    NSLog(@"Unrealized PL = %f", result);
    
    return result;
    
}

-(void)populateVirtualPL {
    
    self.virtualPLData = [[NSMutableArray alloc] init];
    NSNumberFormatter *priceFormattercell = [NSNumberFormatter new];
    [priceFormattercell setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [priceFormattercell setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormattercell setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    
    PrtfSummRptData *dat = [[UserPrefConstants singleton].summaryReportArray objectAtIndex:0];
    
    double total_unrealized_pl = 0;
    int total_gross_buy = 0;
    int total_gross_sell = 0;
    
    
    for (PrtfSubDtlRptData *psdrd in [UserPrefConstants singleton].subDetailReportArray) {
        psdrd.unrealized_pl = [self calculateUnrealizedPLWith:psdrd];
        double unrealized_pl = psdrd.unrealized_pl;
        NSLog(@"unrealized_pl %.f",unrealized_pl);
        if (![psdrd.home_currency isEqualToString:psdrd.stock_currency]) {
            unrealized_pl = psdrd.unrealized_pl * [psdrd.exchange_rate doubleValue];
        }
        
        NSLog(@"psdrd.home_currency %.f",unrealized_pl);
        
        total_unrealized_pl += unrealized_pl;
        total_gross_buy += [psdrd.gross_buy intValue];
        total_gross_sell += [psdrd.gross_sell intValue];
    }
    NSLog(@"total_unrealized_pl %.f",total_unrealized_pl);
    dat.total_unrealized_pl = total_unrealized_pl;
    dat.total_gross_buy = total_gross_buy;
    dat.total_gross_sell = total_gross_sell;
    
    double equityFloat = (dat.current_balance + dat.total_unrealized_pl);
    double netLiquidFloat = equityFloat + dat.open_long.doubleValue + dat.open_short.doubleValue;
    
    NSLog(@"netLiquidFloat %.f, eligible_collateral %.f, initial_margin %.f",netLiquidFloat,dat.eligible_collateral.doubleValue,dat.initial_margin.doubleValue);
    
    double excessSf = netLiquidFloat + dat.eligible_collateral.doubleValue - dat.initial_margin.doubleValue;
    double eliPer = 0;
    if (dat.initial_margin.doubleValue >0) {
        eliPer = 100* (netLiquidFloat+dat.eligible_collateral.doubleValue)/dat.initial_margin.doubleValue;
    } else {
        eliPer = 0;
    }
    
    NSString *cbal = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.bf_cash_balance.doubleValue]];
    NSString *depo = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.deposit.doubleValue]];
    NSString *wdraw = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.withdrawal.doubleValue]];
    NSString *realiPL = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.realized_pl.doubleValue]];
    NSString *currBal = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.current_balance]];
    NSString *unrealPL = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.total_unrealized_pl]];
    NSString *equity = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:equityFloat]];
    NSString *buyOption = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.open_long.doubleValue]];
    NSString *sellOption = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.open_short.doubleValue]];
    NSString *netLiquid = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:netLiquidFloat]];
    NSString *elCol = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.eligible_collateral.doubleValue]];
    NSString *initMargin = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.initial_margin.doubleValue]];
    NSString *mainMargin = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.maintenance_margin.doubleValue]];
    NSString *exSF = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:excessSf]];
    NSString *marginCall = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:dat.margin_call.doubleValue]];
    NSString *eligibility = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:eliPer]];
    
    
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"B/f Cash Balance"],@"Content":cbal}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Deposit"],@"Content":depo}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Withdrawal"],@"Content":wdraw}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Realized P/L"],@"Content":realiPL}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Current Balance"],@"Content":currBal}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Unrealized P/L"],@"Content":unrealPL}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Equity"],@"Content":equity}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Buy Option Mkt Val"],@"Content":buyOption}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Sell Option Mkt Val"],@"Content":sellOption}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Net Liquidation"],@"Content":netLiquid}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Eligible Collateral"],@"Content":elCol}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Initial Margin"],@"Content":initMargin}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Maintenance Margin"],@"Content":mainMargin}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Excess / Shortfall"],@"Content":exSF}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Margin Call"],@"Content":marginCall}];
    [_virtualPLData addObject:@{@"Title":[LanguageManager stringForKey:@"Eligibility %"],@"Content":eligibility}];
    
  //  NSLog(@"virtualData %ld", [_virtualPLData count]);
    
    [_virtualPLtable reloadData];
}

-(void)populateDetailsTablePF:(PortfolioData*)pd {
    
    
    NSNumberFormatter *priceFormattercell = [NSNumberFormatter new];
    [priceFormattercell setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [priceFormattercell setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormattercell setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    NSDictionary *stkData = [[[QCData singleton] qcFeedDataDict] objectForKey:pd.stockcode];
    double price_current = [[stkData objectForKey:FID_98_D_LAST_DONE_PRICE] doubleValue];
    double price_ref = [[stkData objectForKey:FID_51_D_REF_PRICE] doubleValue];

    
    // header is stock data
    NSDictionary *stkDict = [[NSDictionary alloc] initWithDictionary:[[[QCData singleton] qcFeedDataDict] objectForKey:pd.stockcode] copyItems:YES];
    _detailStkName.text = [stkDict objectForKey:FID_39_S_COMPANY];
    _detailStkCode.text = [stkDict objectForKey:FID_38_S_STOCK_NAME];
    _detailLastDone.text = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:price_current]];
    
    CGFloat chg = [[stkDict objectForKey:FID_CUSTOM_F_CHANGE] floatValue];
    CGFloat chgPer = [[stkDict objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue];
    long long stkLotSize = [[stkDict objectForKey:FID_40_I_SHARE_PER_LOT] longLongValue];
    
    _detailChange.text = [NSString stringWithFormat:@"%@ (%.2f%%)",[priceFormattercell stringFromNumber:[NSNumber numberWithFloat:chg]],chgPer];
    if (chg>0) {
        [_detailChange setTextColor:[UIColor greenColor]];
        [_detailLastDone setTextColor:[UIColor greenColor]];
        _detailLastDone.text = [NSString stringWithFormat:@"▲ %@",_detailLastDone.text];
    } else if (chg<0) {
        [_detailChange setTextColor:[UIColor redColor]];
        [_detailLastDone setTextColor:[UIColor redColor]];
        _detailLastDone.text = [NSString stringWithFormat:@"▼ %@",_detailLastDone.text];
    } else {
        [_detailChange setTextColor:[UIColor whiteColor]];
        [_detailLastDone setTextColor:[UIColor whiteColor]];
    }
    
    

    self.detailsArray = [[NSMutableArray alloc] init];
    
    
    
    
    double gainLoss;
    NSString *gainLossPer;
    NSString *unrealOrNot;
    
    // UNREALIZED --------------------
    if (_pfMode==1) {
        
        // FORMAT DATA FIRST
        NSString *qtyOnHand = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong: pd.quantity_on_hand.longLongValue]];
        NSString *qtyAvail = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:  pd.quantity_available.longLongValue]];
        NSString *qtyQueued = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:  pd.sell_qty_in_proc.longLongValue]];
        NSString *avePrice = [priceFormattercell stringFromNumber:[NSNumber numberWithFloat: pd.price_avg_purchase.floatValue]];
        avePrice = [NSString stringWithFormat:@"%@ %@",pd.currency,avePrice];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Qty on Hand"],@"Content":qtyOnHand}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Qty Available"],@"Content":qtyAvail}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Qty Queued Sell"],@"Content":qtyQueued}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Average Buy Price"],@"Content":avePrice}];
        
        double quantityOnHand = [pd.quantity_on_hand intValue];
        double market_value = (double)((price_current != 0 ? price_current : price_ref) * [pd.quantity_on_hand longLongValue]);
        double unrealized_gain_loss_amount = (double)(((price_current != 0 ? price_current : price_ref) - [pd.price_avg_purchase doubleValue]) * [pd.quantity_on_hand longLongValue]);
        
        
        if ([UserPrefConstants singleton].isEnabledTRXFee) {
            if (quantityOnHand<0) {
                unrealized_gain_loss_amount = ABS(unrealized_gain_loss_amount) - SellTrxFee;
            }else{
                unrealized_gain_loss_amount = unrealized_gain_loss_amount - BuyTrxFee;
            }
            
        }
        
        double unrealized_gain_loss_percentage = (double)(unrealized_gain_loss_amount / ([pd.price_avg_purchase doubleValue] * [pd.quantity_on_hand longLongValue]) * 100);
        //Special handling when data is not available
        if ((price_current == 0 && price_ref == 0) || [pd.quantity_on_hand longLongValue] == 0) {
            market_value = 0;
        }
        if ((price_current == 0 && price_ref == 0) || [pd.price_avg_purchase doubleValue] == 0 || [pd.quantity_on_hand longLongValue] == 0) {
            unrealized_gain_loss_amount = 0;
            unrealized_gain_loss_percentage = 0;
        }
        
        gainLossPer = [NSString stringWithFormat:@"%.2f%%",unrealized_gain_loss_percentage];
        gainLoss = unrealized_gain_loss_amount;
        unrealOrNot = @"Unrealized";

        NSString *mktValue = [[UserPrefConstants singleton] abbreviatePrice:pd.gross_market_value.doubleValue];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Market Value"],@"Content":mktValue}];
        
    // REALIZED -----------------------
    } else if (_pfMode==0) {
        
        NSString *aggBuyPrice = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:pd.aggregated_buy_price.doubleValue]];
        NSString *aggSellPrice = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble: pd.aggregated_sell_price.doubleValue]];
        NSString *qtyHolding = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:  pd.total_quantity_P.longLongValue]];
        NSString *qtyShort = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:  pd.total_quantity_short.longLongValue]];
        NSString *qtySold = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:  pd.total_quantity_S.longLongValue]];
        NSString *brokerage = [priceFormattercell stringFromNumber:[NSNumber numberWithDouble: pd.total_comm_S.doubleValue]];
        
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Agg.Buy Price"],@"Content":aggBuyPrice}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Agg.Sell Price"],@"Content":aggSellPrice}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Qty.From Holding(Unit)"],@"Content":qtyHolding}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Qty.Short (Unit)"],@"Content":qtyShort}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Qty.Sold (Unit)"],@"Content":qtySold}];
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Brokerage"],@"Content":brokerage}];
        
        gainLossPer = [NSString stringWithFormat:@"%.2f%%",pd.realized_gain_loss_percentage.floatValue];
        gainLoss = pd.realized_gain_loss_amount.doubleValue;
        unrealOrNot = @"Realized";
    }
    
    NSString *gainLossStr = [[UserPrefConstants singleton] abbreviatePrice:gainLoss];



    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:[NSString stringWithFormat:@"%@ G/L",unrealOrNot]],@"Content":gainLossStr}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:[NSString stringWithFormat:@"%@ G/L %%",unrealOrNot]],@"Content":gainLossPer}];
    
    long long lotSize = (pd.lot_size == nil || [pd.lot_size isEqualToString:@""]) ?
    (long long)stkLotSize : [pd.lot_size longLongValue];
    NSString *lotSizeStr = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:lotSize]];
    
    
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Lot Size"],@"Content":lotSizeStr}];
    
    if ([pd.currency length]>0)
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Currency"],@"Content":pd.currency}];
    
    if ([pd.settlement_mode length]>0)
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Inv. Portfolio"],@"Content":[LanguageManager stringForKey:pd.settlement_mode]}];

    
    // NSLog(@"detailArray: %@",_detailsArray);
    
    [_buyButton setHidden:NO];
    [_sellButton setHidden:NO];
    
    if ([_detailStkName.text length]<=0) {
        [_buyButton setHidden:YES];
        [_sellButton setHidden:YES];
    }
    
    [self.detailsTableview reloadData];
    
}


-(void)populateDetailsTable:(TradeStatus*)ts {
    NSNumberFormatter *priceFormattercell = [NSNumberFormatter new];
    [priceFormattercell setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [priceFormattercell setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormattercell setMaximumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    
    NSNumberFormatter *quantityFormatter = [NSNumberFormatter new];
    [quantityFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important
    [quantityFormatter setGroupingSeparator:@","];
    [quantityFormatter setGroupingSize:3];
    [quantityFormatter setGeneratesDecimalNumbers:YES];
    
    
    // header is stock data
    NSDictionary *stkDict = [[NSDictionary alloc] initWithDictionary:[[[QCData singleton] qcFeedDataDict] objectForKey:ts.stock_code] copyItems:YES];
    _detailStkName.text = [stkDict objectForKey:FID_39_S_COMPANY];
    _detailStkCode.text = [stkDict objectForKey:FID_38_S_STOCK_NAME];
    
    //NSLog(@"stkDict %@",stkDict);
    
    NSString *stockCurrency =  [stkDict objectForKey:FID_134_S_CURRENCY];
    
    _detailLastDone.text = [priceFormattercell stringFromNumber:[NSNumber numberWithFloat:[[stkDict objectForKey:FID_98_D_LAST_DONE_PRICE] floatValue]]];
    
    CGFloat chg = [[stkDict objectForKey:FID_CUSTOM_F_CHANGE] floatValue];
    CGFloat chgPer = [[stkDict objectForKey:FID_CUSTOM_F_CHANGE_PER] floatValue];
    
    _detailChange.text = [NSString stringWithFormat:@"%@ (%.2f%%)",[priceFormattercell stringFromNumber:[NSNumber numberWithFloat:chg]],chgPer];
    if (chg>0) {
        [_detailChange setTextColor:[UIColor greenColor]];
        [_detailLastDone setTextColor:[UIColor greenColor]];
        _detailLastDone.text = [NSString stringWithFormat:@"▲ %@",_detailLastDone.text];
    } else if (chg<0) {
         [_detailChange setTextColor:[UIColor redColor]];
        [_detailLastDone setTextColor:[UIColor redColor]];
        _detailLastDone.text = [NSString stringWithFormat:@"▼ %@",_detailLastDone.text];
    } else {
         [_detailChange setTextColor:[UIColor whiteColor]];
        [_detailLastDone setTextColor:[UIColor whiteColor]];
    }
    
    // get all ts data and put in _detailsArray and reload detailtable
    self.detailsArray = [[NSMutableArray alloc] init];
    
    // Format data
    NSString *order_year =  [ts.order_time substringWithRange:NSMakeRange(0, 4)];
    NSString *order_mon =  [ts.order_time substringWithRange:NSMakeRange(4, 2)];
    NSString *order_day =  [ts.order_time substringWithRange:NSMakeRange(6, 2)];
    
    NSString *order_hour = [ts.order_time substringWithRange:NSMakeRange(8, 2)];
    NSString *order_minute = [ts.order_time substringWithRange:NSMakeRange(10, 2)];
    NSString *order_second = [ts.order_time substringWithRange:NSMakeRange(12, 2)];
    
    NSString *ordDate = [NSString stringWithFormat:@"%@/%@/%@", order_day, order_mon, order_year];
    NSString *ordTime = [NSString stringWithFormat:@"%@:%@:%@", order_hour, order_minute, order_second];
    NSString *ordNum = ([ts.order_number length]<=0)?@"-":ts.order_number;
    NSString *ordQty = [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong: ts.order_quantity.longLongValue]];
    

    
    NSString *ordPrice = [priceFormattercell stringFromNumber:[NSNumber numberWithFloat: ts.order_price.floatValue]];
    ordPrice = [NSString stringWithFormat:@"%@ %@",stockCurrency,ordPrice];
    // this is special rule
    if ([ts.order_type isEqualToString:@"Market"]||[ts.order_type isEqualToString:@"MarketToLimit"]) {
        if (ts.order_price.floatValue<=0) {
            ordPrice = @"-";
        }
    }
    
    
    double ordVal = ts.order_quantity.longLongValue * ts.order_price.floatValue;
    NSString *valStr =  [priceFormattercell stringFromNumber:[NSNumber numberWithDouble:ordVal]];
    valStr =  [NSString stringWithFormat:@"%@ %@",stockCurrency,valStr];
    
    NSString *matQty =  [quantityFormatter stringFromNumber:[NSNumber numberWithLongLong:[ts.mt_quantity longLongValue]]];

    NSString *matPrice = [priceFormattercell stringFromNumber:[NSNumber numberWithFloat:[ts.mt_value floatValue]]];
    matPrice = [NSString stringWithFormat:@"%@ %@",stockCurrency,matPrice];
    NSString *matVal = [priceFormattercell stringFromNumber:[NSNumber numberWithLongLong:[ts.mt_value longLongValue]]];
    matVal = [NSString stringWithFormat:@"%@ %@",stockCurrency,matVal];
    
    
    
    NSString *validityStr = [LanguageManager stringForKey:ts.validity];
    if ([ts.validity isEqualToString:@"GTD"]&&([ts.expiry length]>=8)) {
        //NSLog(@"expiry %@",ts.expiry); eg: 20170620170000.000
        
        NSString *dateStr = [ts.expiry substringToIndex:8];
        // NSLog(@"dateStr %@", dateStr);
        
        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
        [ndf setDateFormat:@"yyyyMMdd"];
        NSDate *date = [ndf dateFromString:dateStr];
        [ndf setDateFormat:@"dd/MM/yyyy"];
        
        validityStr = [NSString stringWithFormat:@"%@ (%@)", validityStr,[ndf stringFromDate:date]];
    
    }
    
    
    
    
    // TC Mobile dont have Avg Price?
//    if (ts.mt_quantity.floatValue<=0) {
//        cell.lblavgPrice.text = @"0.000";
//        cell.lblavgPrice.textColor = [UIColor whiteColor];
//    } else {
//        CGFloat avgPriceVal = ts.mt_value.floatValue/ts.mt_quantity.floatValue;
//        cell.lblavgPrice.text = [NSString stringWithFormat:@"%.3f",avgPriceVal];
//        if (avgPriceVal>0.0) {
//            cell.lblavgPrice.textColor = [UIColor greenColor];
//        } else if (avgPriceVal<0.0) {
//            cell.lblavgPrice.textColor = [UIColor redColor];
//        }
//    }
    
    NSString *ordSource = [[UserPrefConstants singleton] getOrderSourceStr:ts.order_src];
    
    // Add formatted data (if any) to array
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Action"],@"Content":[LanguageManager stringForKey:ts.action]}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Status"],@"Content":[LanguageManager stringForKey:ts.status_text]}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Qty"],@"Content":ordQty}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Price"],@"Content":ordPrice}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Value"],@"Content":valStr}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Type"],@"Content":[LanguageManager stringForKey:ts.order_type]}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Validity"],@"Content":validityStr}];
    if ([ts.sett_currency length]>0)
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Sett.Curr."],@"Content":ts.sett_currency}];
    if ([ts.payment_type length]>0) {
        if ([ts.payment_type isEqualToString:@"0"]) ts.payment_type = @"-";
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Payment"],@"Content":[LanguageManager stringForKey:ts.payment_type]}];
    }
    
    NSString *trigPrice = [priceFormattercell stringFromNumber:[NSNumber numberWithFloat: ts.trigger_price.floatValue]];
    trigPrice = [NSString stringWithFormat:@"%@ %@",stockCurrency,trigPrice];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Trigger Price"],@"Content":trigPrice}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Trigger Type"],@"Content":ts.triggerType}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Trigger Direction"],@"Content":ts.triggerDirection}];
    
    
    if ([ts.disclosed_quantity length]>0)
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Disclosed Qty"],@"Content":ts.disclosed_quantity}];
    if ([ts.min_quantity length]>0)
        [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Min.Qty"],@"Content":ts.min_quantity}];
    
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Unmatched Qty"],@"Content":ts.unmt_quantity}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Matched Qty"],@"Content":matQty}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Matched Price"],@"Content":matPrice}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Matched Value"],@"Content":matVal}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Source"],@"Content":ordSource}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order No."],@"Content":ordNum}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Date"],@"Content":ordDate}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Order Time"],@"Content":ordTime}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Account No."],@"Content":ts.account_number}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Lot Size"],@"Content":ts.lot_size}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Remarks"],@"Content":ts.message}];
    [_detailsArray addObject:@{@"Title":[LanguageManager stringForKey:@"Activity"],@"Content":ts.message}];
    BOOL hidebutton = NO;
    if ([ts.status isEqualToString:@"4"]) {
        hidebutton = YES;
    }
    if ([ts.status isEqualToString:@"8"]) {
        hidebutton = YES;
    }
    if ([ts.status isEqualToString:@"9"]) {
        hidebutton = YES;
    }
    if ([ts.status isEqualToString:@"C"]) {
        hidebutton = YES;
    }
    if ([ts.status isEqualToString:@"2"]) {
        hidebutton = YES;
    }
    
    [_cancelButton setHidden:hidebutton];
    [_reviseButton setHidden:hidebutton];
    
    [_buyButton setHidden:YES];
    [_sellButton setHidden:YES];
    
    [_detailsTableview reloadData];
}




#pragma mark - 5seconds refresh orderbook
-(void)halfSecondTimerTick {
    ordBookRefreshCounter += 1;
    // 5 seconds = 10 ticks
    
    if (ordBookRefreshCounter>=120) {
        ordBookRefreshCounter = 0;
        
        if ((!isPortfolio)&&(!isLoadingOB)) {
           [self reloadTabInfo];
        }
        
    }
}

#pragma mark - Portfolio or Orderbook navigate

-(void)openPortFolio {
    
    
    [_atp timeoutActivity];
    isLoadingPF = YES; isLoadingOB = NO;
    
    NSLog(@"uacd exchg %@", uacd.client_acc_exchange);
    //+++ Check account is in Derivative or not
    if ([[AppConstants DerivativeExchanges] containsObject:uacd.client_acc_exchange]) {
        
        [[UserPrefConstants singleton].futuresUnrealPFResultStkCodeArr removeAllObjects];
        [_derivativesContainer setHidden:NO];
        [_orderHistPanel setHidden:YES];
        [_portfolioTableView setHidden:YES];
        [_tableView setHidden:YES];
        [_portFolioPanel setHidden:YES];
        [_detailsContainer setHidden:YES];
        
        [UserPrefConstants singleton].detailReportType = 0; // day detail, 1 = overnight detail
        [_atp doTradePrtfSubDtlRpt:0 isDerivative:YES];
        [_atp doTradePrtfSummRpt:0 isDerivative:YES];
        
        
    } else {
        
        [_derivativesContainer setHidden:YES];
        [_orderHistPanel setHidden:YES];
        [_portfolioTableView setHidden:NO];
        [_tableView setHidden:YES];
        [_portFolioPanel setHidden:NO];
        
        [_detailsArray removeAllObjects];
        [self.detailsTableview reloadData];
        [self setDetailNoData];
        
        [_btnLoading setCenter:_portfolioTableView.center];
        [_btnLoading startAnimating];
        [[[UserPrefConstants singleton] eqtyUnrealPFResultList] removeAllObjects];
        [_portfolioTableView reloadData];
        
        [self updatePortfolioButtons];
        
        selectedRowPF = -1;
        isPortfolio = YES;
        [_atp getPortfolio:_pfMode];
    }
    
}


#pragma mark - OBDisclaimer Delegate

-(void)disclaimerClickedOk {
    [UserPrefConstants singleton].showDisclaimer = NO;
}

- (void)portfolioBtnPressed
{
    
    // if CIMB SG/MY, disclaimer show
    if ([UserPrefConstants singleton].PFDisclaimerWebView) {
        
        if ([UserPrefConstants singleton].showDisclaimer) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            OBDisclaimer = [storyboard instantiateViewControllerWithIdentifier:@"OBDisclaimerViewController"];
            OBDisclaimer.modalPresentationStyle= UIModalPresentationFormSheet;
            OBDisclaimer.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            OBDisclaimer.preferredContentSize = CGSizeMake(800, 500);
            OBDisclaimer.delegate = self;
            [self openPortFolio];
            [self presentViewController:OBDisclaimer animated:YES completion:nil];
            return;
        } else {
            [self openPortFolio];
            return;
        }
    }else{
        [self openPortFolio];
    }
    
    
}





- (void)ordBkBtnPressed
{
    
    [_atp timeoutActivity];
    isLoadingPF = NO; isLoadingOB = YES;
    
    [_detailsArray removeAllObjects];
    [self.detailsTableview reloadData];
    [self setDetailNoData];

    [UserPrefConstants singleton].orderbooklist = [[NSMutableArray alloc] init];
    [UserPrefConstants singleton].orderbookliststkCodeArr = [[NSMutableArray alloc] init];
    
    [_btnLoading setCenter:_tableView.center];
    [_btnLoading startAnimating];
    [_tableView reloadData];
    [_detailsContainer setHidden:NO];
    [_derivativesContainer setHidden:YES];
    [_orderHistPanel setHidden:NO];
    [_portfolioTableView setHidden:YES];
    [_tableView setHidden:NO];
    [_portFolioPanel setHidden:YES];

    selectedRow = -1;
    isPortfolio = NO;
    
    if ((_fromDate!=nil)&&(_toDate!=nil)) {
        
        
        [_btnLoading startAnimating];
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMdd"];
        NSString *stringFromDate = [formatter stringFromDate:_fromDate];
        NSString *stringToDate = [formatter stringFromDate:_toDate];
        
        [_atp getTradeHistoryFromDate:stringFromDate toDate:stringToDate];
        
        // if toDate includes today, then call tradeStatus too
        NSString *todayDate = [formatter stringFromDate:[NSDate date]];
        if ([stringToDate isEqualToString:todayDate]) {
            [_atp getTradeStatus:0];
        }
        
    } else {
    
        [_atp getTradeStatus:0];
    }
}


- (void)keyboardDidHide: (NSNotification *) notif{
    //[self clickCancelRevise:self];
   // [self clickCancelOrderCancelButton:self];
}




- (IBAction)clearHistory:(id)sender {
    
    _fromDate = nil;
    _toDate = nil;
    _dateToLabel.text = @"";
    _dateFromLabel.text = @"";
    _errorLabel.text = @"";
    [self ordBkBtnPressed];
}


-(void)updateOrderBookData {
    
    [self reloadTabInfo];
}

- (IBAction)refreshBtnPressed:(id)sender {
    [_atp timeoutActivity];

    if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
        [_atp getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount
                       forPaymentCUT:NO andCurrency:@""];
    
    [self reloadTabContents]; // this should be done by timer every 5 seconds
}

@end
