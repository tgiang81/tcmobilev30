//
//  HomeNewsTableViewCell.m
//  TCiPad
//
//  Created by n2n on 08/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "HomeNewsTableViewCell.h"

@implementation HomeNewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.newsTitle setTextColor:[UIColor whiteColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
