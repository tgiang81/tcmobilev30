//
//  TradeAccountController.h
//  TCiPad
//
//  Created by n2n on 12/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserAccountClientData.h"
#import "UserPrefConstants.h"
#import "AccountTableViewCell.h"
#import "ATPAuthenticate.h"
#import "LanguageManager.h"

@protocol TradeAccountDelegate <NSObject>
@required
-(void)accountSelected:(UserAccountClientData*)account;
@end

@interface TradeAccountController: UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,
UIGestureRecognizerDelegate>

@property (weak, nonatomic) id<TradeAccountDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (weak, nonatomic) IBOutlet UITableView *accTable;
@property (strong, nonatomic) NSMutableArray *tableDataArr;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchOptions;

- (IBAction)cancelAccount:(id)sender;
- (IBAction)searchByField:(id)sender;

@end
