//
//  UIImage.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 10/28/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (AddtionalFunctionalities)

//TintColor...
- (UIImage *)imageWithTint:(UIColor *)tintColor;
//scale and resize...
-(UIImage*)scaleToSize:(CGSize)size;

@end
