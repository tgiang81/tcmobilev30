//
//  N2Tabs.m
//  test
//
//  Created by Emir on 16/01/2017.
//  Copyright © 2017 n2n. All rights reserved.
//

#import "N2Tabs.h"

@implementation N2Tabs

@synthesize tabDelegate;

/*
 Param 0 - array of btn title str (NSString)
 Param 1 - text color of tabs (UIColor)
 Param 2 - tab selected color (UIColor)
*/

-(id)initWithParams:(NSArray*)params withFrame:(CGRect)frame andFontSize:(CGFloat)fontSize {
    self = [super initWithFrame:frame];
    if (self) {
        _tabButtons = [NSArray arrayWithArray:[params objectAtIndex:0]];
        _txtColor = [params objectAtIndex:1];
        _btnColor = [params objectAtIndex:2];
        
        // create buttons
        UIFont *btnFont = [UIFont boldSystemFontOfSize:fontSize];
        long totalBtn = [_tabButtons count];
        CGFloat xOffset = 0;

        for (long i=0; i<totalBtn; i++) {
            UIButton *tabBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [tabBtn setBackgroundColor:[UIColor clearColor]];
            tabBtn.tag = i;
            NSString *title = [_tabButtons objectAtIndex:i];
            [tabBtn setTitle:title forState:UIControlStateNormal];
            [tabBtn.titleLabel setNumberOfLines:0];
            [tabBtn.titleLabel setTextAlignment:NSTextAlignmentCenter];
            [tabBtn.titleLabel setFont:btnFont];
            [tabBtn setTitleColor:_txtColor forState:UIControlStateNormal];
            
            // get button size based on title length
            CGSize size = [title sizeWithAttributes: @{NSFontAttributeName:btnFont}];
            CGSize btnSize = CGSizeMake(ceilf(size.width)+30, ceilf(frame.size.height*1.2));
            [tabBtn.layer setCornerRadius:btnSize.height*0.2];
            
            // set position
            [tabBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, self.frame.size.height/6.0, 0)];
            tabBtn.frame = CGRectMake(xOffset, 0, btnSize.width, btnSize.height);
            xOffset += btnSize.width;
            
            // add method target
            [tabBtn addTarget:self action:@selector(tabButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            // add autoresizing mask
            tabBtn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            
            [self addSubview:tabBtn];
        }
        [self setClipsToBounds:YES];
        [self updateTabAppearance];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _selectedTabIndex = 0;
        self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    }
    return self;
}

-(void)tabButtonTapped:(UIButton*)sender {
    // check if vc implements delegate
    if ([self.tabDelegate respondsToSelector:@selector(tabDidClicked:atTab:)]) {
    
        // only select if its different tab
        if (_selectedTabIndex!=sender.tag) {
        
            [self.tabDelegate tabDidClicked:sender atTab:self];
            _selectedTabIndex = sender.tag;
            [self updateTabAppearance];
            
        }
        
    }
    
}

-(UIColor*)lightenColor:(UIColor*)clr withAmount:(CGFloat)reduction {
    CGFloat hue, sat, bright, alpha;
    [clr getHue:&hue saturation:&sat brightness:&bright alpha:&alpha];
    
    bright += reduction;
    if (bright>=1.0) bright = 1.0;
    
    return [UIColor colorWithHue:hue saturation:sat brightness:bright alpha:1.0];
}

-(UIColor*)darkenColor:(UIColor*)clr withAmount:(CGFloat)reduction {
    CGFloat hue, sat, bright, alpha;
    [clr getHue:&hue saturation:&sat brightness:&bright alpha:&alpha];
    
    bright -= reduction;
    if (bright<=0.0) bright = 0.0;
    
    return [UIColor colorWithHue:hue saturation:sat brightness:bright alpha:1.0];
}

-(void)turnShadow:(BOOL)on forButton:(UIButton*)btn {
    if (on) {
        [btn.layer setShadowColor:[UIColor blackColor].CGColor];
        [btn.layer setShadowOpacity:0.5];
        [btn.layer setShadowRadius:5];
        [btn.layer setShadowOffset:CGSizeMake(0, 5)];
    } else {
        [btn.layer setShadowOpacity:0.0];
        [btn.layer setShadowRadius:0];
    }
    
}

-(void)updateTabAppearance {

    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            
            UIButton *tabBtn = (UIButton*)view;
            if (tabBtn.tag == _selectedTabIndex) {
                [tabBtn.layer setBackgroundColor:_btnColor.CGColor];
                [tabBtn setTitleColor:_txtColor forState:UIControlStateNormal];
                [self bringSubviewToFront:tabBtn];
                [self turnShadow:YES forButton:tabBtn];
            } else {
                [tabBtn.layer setBackgroundColor:[self darkenColor:_btnColor withAmount:0.05].CGColor];
                [tabBtn setTitleColor:[self lightenColor:_btnColor withAmount:0.1] forState:UIControlStateNormal];
                [self turnShadow:NO forButton:tabBtn];
            }
        }
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
