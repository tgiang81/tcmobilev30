//
//  CollectionReusableHeaderView.m
//  TCiPad
//
//  Created by n2n on 08/02/2017.
//  Copyright © 2017 N2N. All rights reserved.
//


#import "CollectionReusableHeaderView.h"
#import "UserPrefConstants.h"

@implementation CollectionReusableHeaderView

-(void)hideSortButtons:(BOOL)hide {
    
    for (UIView *view in [self subviews]) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton*)view;
            if (btn.tag<50) {
                [btn setHidden:hide];
            }
        }
    }
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    switch ([UserPrefConstants singleton].collectionViewHeaderMode) {
        case QUOTESCREEN: {
            [self hideSortButtons:NO];
            [_toggleWLBtn setHidden:YES];
            [_hintLabel setHidden:YES];
            [_addStkBtn setHidden:YES];
        }break;
        case WATCHLIST_HIDDENLIST: {
            [self hideSortButtons:NO];
            [_toggleWLBtn setHidden:NO];
            [_hintLabel setHidden:YES];
            [_addStkBtn setHidden:NO];
        }break;
        case WATCHLIST_NONHIDDENLIST: {
            [self hideSortButtons:YES];
            [_toggleWLBtn setHidden:YES];
            [_hintLabel setHidden:NO];
            [_addStkBtn setHidden:NO];
        }break;
        default:
            break;
    }
    

}

@end
