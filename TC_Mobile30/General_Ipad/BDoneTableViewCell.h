//
//  BDoneTableViewCell.h
//  TCiPad
//
//  Created by n2n on 17/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "N2PercentLabel.h"
#import "AppControl.h"
#import "QCData.h"

/*
 This is used at 2 places
  - StockDetailViewController
  - StockPreviewViewController
 */

@interface BDoneTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bdPrice;
@property (weak, nonatomic) IBOutlet UILabel *bdBvol;
@property (weak, nonatomic) IBOutlet UILabel *bdSvol;
@property (weak, nonatomic) IBOutlet N2PercentLabel *bdBuyPercent;
@property (weak, nonatomic) IBOutlet UILabel *bdTotalVol;
@property (weak, nonatomic) IBOutlet UILabel *bdTotalVal;

-(void)updateCellWithDictionary:(NSDictionary*)dataDict withLACP:(CGFloat)lacpPrice;

@end
