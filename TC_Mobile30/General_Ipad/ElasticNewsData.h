//
//  ElasticNewsData.h
//  TCiPhone_N2NMultiLogin_UAT_V1.4.5
//
//  Created by Ahmad Ashraf Azman on 14/07/2017.
//
//

#import <Foundation/Foundation.h>

@interface ElasticNewsData : NSObject{
    NSString *sourceString;
    NSString *categoryString;
    NSString *categoryLevel;
    NSString *newsID;
}

@property (nonatomic, strong) NSString *sourceString;
@property (nonatomic, strong) NSString *categoryString;
@property (nonatomic, strong) NSString *categoryLevel;
@property (nonatomic, strong) NSString *newsID;

@end
