//
//  OBDisclaimerViewController.h
//  TCiPad
//
//  Created by n2n on 20/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPrefConstants.h"
#import "LanguageManager.h"
#import "ATPAuthenticate.h"

@protocol OBDisclaimerDelegate <NSObject>
-(void)disclaimerClickedOk;
@end

@interface OBDisclaimerViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *disclaimerWebview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivity;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;
@property (weak, nonatomic) id<OBDisclaimerDelegate> delegate;


- (IBAction)okClicked:(id)sender;

@end
