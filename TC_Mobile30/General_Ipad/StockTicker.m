//
//  StockTicker.m
//  TCUniversal
//
//  Created by Scott Thoo on 10/30/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "StockTicker.h"

@implementation StockTicker
@synthesize lblLastDone,lblPriceChanged,lblStkName,imgFlag;
@synthesize stockCode;
@synthesize userPref = _userPref;
@synthesize tickerButton;

//Programmatically
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockTicker" owner:self options:nil];
        // 2. adjust bounds
        self.bounds = self.view.bounds;
        // 3. Add as a subview
        [self addSubview:self.view];
        
    }
    return self;
}

//With Storyboard
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        
        // Scott
        // 1. Load the xib
        [[NSBundle mainBundle] loadNibNamed:@"StockTicker" owner:self options:nil];
        // 2. Add as a subview
        [self addSubview:self.view];
    }
    
    return self;
}



- (IBAction)clickedTicker:(id)sender
{
    _userPref.userSelectingStockCode = stockCode;
    //NSLog(@"Clicked Ticker _userPref.userSelectingStockCode : %@",_userPref.userSelectingStockCode);
    NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
    [notificationData setObject:stockCode forKey:@"stkCode"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
}
@end
