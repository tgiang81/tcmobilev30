//
//  ImgChartManager.h
//  TCiPad
//
//  Created by Emir on 19/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserPrefConstants.h"

@interface ImgChartManager : NSObject

@property (nonatomic, strong) NSString *stkCode;
@property (nonatomic, strong) NSURLSessionDataTask *urlTask;

-(id)init;
-(void)getImageChartWithURL:(NSString *)urlString
          completedWithData:(void(^)(NSData *data))completedWithData
                    failure:(void(^)(NSString *errorMessage))failure;

-(void)getImageChartURL:(NSString *)urlString
      completedWithLink:(void(^)(NSString *data))completedWithLink;

#pragma mark - Load image with cache
+ (void)loadImageFromURL:(NSString *)url completion:(void (^)(UIImage *image, NSError *error))completion;
+ (void)cleareCache;
@end
