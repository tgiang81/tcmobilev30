//
//  GeneralSelectorViewController.m
//  TCiPad
//
//  Created by n2n on 11/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "GeneralSelectorViewController.h"

@interface GeneralSelectorViewController () {
    CGSize scrSize;
    CGFloat bufferPreviewX;
    CGPoint panelViewOriPos;
}

@end

@implementation GeneralSelectorViewController


#pragma mark - TableView Delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_expandedSectionIndex==section) {
        SectorInfo *inf = [_selectorArray objectAtIndex:section];
        return [inf.subItemsArray count]+1;
    } else {
        return 1;
    }
    

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return [_selectorArray count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [[ATPAuthenticate singleton] timeoutActivity];
    [UIView animateWithDuration:0.2 animations:^(void) {
        _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^(void) {
            [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.0]];
        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:NO completion:^(void) {
                if (self.delegate) {
                    [self.delegate selectorItemSelected:indexPath selectorType:_selectorType];
                }
            }];
        }];
    }];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SelectorTableViewCell *cell = (SelectorTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SelectorCellID"];
    if (cell==nil) {
        cell = [[SelectorTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SelectorCellID"];
    }
    
    cell.path = indexPath;
    cell.delegate = self;
    
    [cell.selectedView.layer setBorderColor:[UIColor cyanColor].CGColor];
    [cell.selectedView.layer setBorderWidth:1];
    [cell.selectedView.layer setCornerRadius:cell.selectedView.frame.size.width/2.0];
    [cell.selectedView.layer setMasksToBounds:NO];
    
    SectorInfo *inf = [_selectorArray objectAtIndex:indexPath.section];
    
    NSString *item;
    
    if (indexPath.row==0) {
        
        item = inf.sectorName;
        if ([inf.subItemsArray count]>0) {
            [cell.expandButton setHidden:NO];
        } else {
            [cell.expandButton setHidden:YES];
        }
        
        [self indentCell:cell indent:NO];
        
    } else {
        
        [cell.expandButton setHidden:YES];
        
        SectorInfo *subInf = [inf.subItemsArray objectAtIndex:indexPath.row-1];
        item = subInf.sectorName;
        
        [self indentCell:cell indent:YES];
    }
    
    if (indexPath.section==self.expandedSectionIndex) {
        [cell.expandButton setTitle:SYMBOL_TRIANGLE_UP forState:UIControlStateNormal];
        [cell.expandButton setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    } else {
        [cell.expandButton setTitle:SYMBOL_TRIANGLE_DOWN forState:UIControlStateNormal];
        [cell.expandButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    if (_selectorType==TYPE_WLIST) {
        [cell.selectedView setHidden:YES];
        cell.selectionItem.text = item;
    } else {
        cell.selectionItem.text = [LanguageManager stringForKey:item];
    }
    
        
    if ([_selectedItem isEqualToString:item]) {
        [cell.selectedView.layer setBackgroundColor:[UIColor cyanColor].CGColor];
    } else {
        [cell.selectedView.layer setBackgroundColor:[UIColor clearColor].CGColor];
    }
    
    return cell;
}


-(void)indentCell:(SelectorTableViewCell*)cell indent:(BOOL)indent {
    
    if (indent) {
        cell.selectedView.frame = CGRectMake(18, 15, 18, 19);
        cell.selectionItem.frame = CGRectMake(42, 13, 237, 21);
    } else {
        cell.selectedView.frame = CGRectMake(8, 15, 18, 19);
        cell.selectionItem.frame = CGRectMake(32, 13, 247, 21);
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row>0) {
        cell.backgroundColor = [UIColor blackColor];
    } else {
        if (indexPath.section % 2 ==0) {
            cell.backgroundColor = [UIColor colorWithWhite:0.1 alpha:1.0];
        } else {
            cell.backgroundColor = [UIColor colorWithWhite:0.13 alpha:1.0];
        }
    }
}

-(void)expandBtnClicked:(NSIndexPath *)path {
    
    if (path.section==self.expandedSectionIndex) {
        
        long prevExpanded = self.expandedSectionIndex;
        self.expandedSectionIndex = -1;
        
        [_selectorTable beginUpdates];
        NSIndexSet *set = [NSIndexSet indexSetWithIndex:prevExpanded];
        [_selectorTable reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
        [_selectorTable endUpdates];
        
    } else {
        
        
        long prevExpanded = self.expandedSectionIndex;
        self.expandedSectionIndex = (int)path.section;
        
        [_selectorTable beginUpdates];
        if (prevExpanded>=0) {
            NSIndexSet *setP = [NSIndexSet indexSetWithIndex:prevExpanded];
            [_selectorTable reloadSections:setP withRowAnimation:UITableViewRowAnimationFade];
        }
        
        NSIndexSet *set = [NSIndexSet indexSetWithIndex:_expandedSectionIndex];
        [_selectorTable reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
        [_selectorTable endUpdates];
    }
    
}

#pragma mark - Views Lifecycle delegates

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.2 animations:^(void) {
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^(void) {
            _panelView.frame = CGRectMake(scrSize.width-_panelView.frame.size.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
            
        } completion:^(BOOL finished) {
            panelViewOriPos = _panelView.center;
            
//            scroll to current selection
//            long idx = [_selectorArray indexOfObject:[UserPrefConstants singleton].userSelectedAccount];
            
//            if (idx>0) {
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];
//                [_accTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
//            }
            
        }];
    }];
    
    [[LanguageManager defaultManager] translateStringsForViewController:self];

    if (_selectorType==TYPE_MARKET) {
        _titleLabel.text = [LanguageManager stringForKey:@"Market"];
    } else if (_selectorType==TYPE_SECTOR) {
        _titleLabel.text = [LanguageManager stringForKey:@"Sector"];
    }else if (_selectorType==TYPE_WLIST) {
        _titleLabel.text = [LanguageManager stringForKey:@"Add To Watchlist"];
    }
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _expandedSectionIndex = -1;
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    
    bufferPreviewX = 0;
    scrSize = [[UIScreen mainScreen] bounds].size;
    
    _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
    
    _panelView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _panelView.layer.shadowRadius = 10;
    _panelView.layer.shadowOpacity = 1.0;
    _panelView.layer.shadowOffset = CGSizeMake(-10, 0);
    
    UIPanGestureRecognizer *panGesturePanel = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(selectorPanelDidPanned:)];
    panGesturePanel.delegate = self;
    [_panelView addGestureRecognizer:panGesturePanel];
    
    // tap outside of news view, dismiss it
    UITapGestureRecognizer *touch = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
    touch.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:touch];
    [touch setDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelSelect:) name:@"dismissAllModalViews" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Gestures Delegate

-(void)selectorPanelDidPanned:(UIPanGestureRecognizer*)recognizer {
    
    UIView *view = recognizer.view;
    
    // CGPoint velocity = [recognizer velocityInView:view.superview];
    
    // if (velocity.x>0) {
    
    CGPoint translation = [recognizer translationInView:view.superview];
    
    if (translation.x>=50) {
        bufferPreviewX = translation.x-49;
    } else {
        bufferPreviewX = 0;
    }
    
    if (bufferPreviewX>0) {
        
        CGFloat distanceToDismiss = 150;
        
        if(recognizer.state == UIGestureRecognizerStateBegan){
            
        } else if(recognizer.state == UIGestureRecognizerStateEnded){
            
            if (_panelView.center.x<=panelViewOriPos.x+distanceToDismiss) {
                // return back
                [UIView animateWithDuration:0.2 animations:^{
                    _panelView.center = panelViewOriPos;
                }completion:^(BOOL finished) {
                    
                }];
            }
        } else {
            // swping
            _panelView.center = CGPointMake(panelViewOriPos.x+bufferPreviewX, panelViewOriPos.y);
            
            if (_panelView.center.x>panelViewOriPos.x+distanceToDismiss) {
                [view removeGestureRecognizer:recognizer];
                [self cancelSelect:nil];
            }
            
        }
        
    } else {
        
        [UIView animateWithDuration:0.2 animations:^{
            _panelView.center = panelViewOriPos;
        }completion:^(BOOL finished) {
            
        }];
        
    }
    
}

- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.view];
    if (CGRectContainsPoint([_panelView frame], p)) {
        ; // do nothing
    } else {
        [self cancelSelect:nil];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isDescendantOfView:_panelView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (IBAction)cancelSelect:(id)sender {
    [[ATPAuthenticate singleton] timeoutActivity];
    [UIView animateWithDuration:0.2 animations:^(void) {
        _panelView.frame = CGRectMake(scrSize.width, 0, _panelView.frame.size.width, _panelView.frame.size.height);
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^(void) {
            [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.0]];
        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
    }];
}
@end
