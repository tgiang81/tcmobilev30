//
//  FingerWaveView.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 29/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "FingerWaveView.h"
#import <QuartzCore/QuartzCore.h>

@interface FingerWaveView ()
{
    CGSize waveSize;
    NSTimeInterval duration;
}
@end

@implementation FingerWaveView

- (instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        waveSize = CGSizeMake(50, 50);
        duration = 0.5;
    }
    return self;
}
+ (instancetype)showInView:(UIView *)view center:(CGPoint)center {
    FingerWaveView *waveView = [FingerWaveView new];
    [waveView setframeWithCenter:center];
    [view addSubview:waveView];
    return waveView;
}
- (void)didMoveToSuperview{
    CAShapeLayer *waveLayer = [CAShapeLayer new];
    waveLayer.backgroundColor  = [UIColor clearColor].CGColor;
    waveLayer.opacity = 0.6;
    waveLayer.fillColor = [UIColor whiteColor].CGColor;
    [self.layer addSublayer:waveLayer];
    
    [self startAnimationInLayer:waveLayer];
}
- (void)startAnimationInLayer:(CALayer *)layer{
    
    [CATransaction begin];
    //remove layer after animation completed
    [CATransaction setCompletionBlock:^{
        [self removeFromSuperview];
    }];

    
    UIBezierPath *beginPath = [UIBezierPath bezierPathWithArcCenter:[self pathCenter] radius:[self animationBeginRadius] startAngle:0 endAngle:M_PI*2 clockwise:YES];
    UIBezierPath *endPath = [UIBezierPath bezierPathWithArcCenter:[self pathCenter] radius:[self animationEndRadius] startAngle:0 endAngle:M_PI*2 clockwise:YES];
    
    CABasicAnimation *rippleAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
    rippleAnimation.delegate = self;
    rippleAnimation.fromValue = (__bridge id _Nullable)(beginPath.CGPath);
    rippleAnimation.toValue = (__bridge id _Nullable)(endPath.CGPath);
    rippleAnimation.duration = duration;
    
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.delegate = self;
    opacityAnimation.fromValue = [NSNumber numberWithFloat:0.6];
    opacityAnimation.toValue = [NSNumber numberWithFloat:0.0];
    opacityAnimation.duration = duration;
    
    [layer addAnimation:rippleAnimation forKey:@"rippleAnimation"];
    [layer addAnimation:opacityAnimation forKey:@"opacityAnimation"];
    
    [CATransaction commit];
    
}
- (void)setframeWithCenter:(CGPoint)center{
    CGRect frame = CGRectMake(center.x-waveSize.width*0.5, center.y-waveSize.height*0.5, waveSize.width, waveSize.height);
    self.frame = frame;;
}
- (CGFloat)animationBeginRadius{
    return waveSize.width*0.5*0.2;
}
- (CGFloat)animationEndRadius{
    return waveSize.width*0.5;
}
- (CGPoint)pathCenter{
    return CGPointMake(waveSize.width*0.5, waveSize.height*0.5);
}
#pragma mark - CAAnimationDelegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (flag) {
        [self removeFromSuperview];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
