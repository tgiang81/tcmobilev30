//
//  ButtonEffect.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 29/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ButtonEffect : NSObject{
     UIColor *flareColor;
}
+ (ButtonEffect *)sharedCenter;
-(void) flarify: (UIView* ) chilView inParentView:(UIView*) rootView withColor : (UIColor *)fillColor enableUponCompletion:(BOOL)enabled;
@end
