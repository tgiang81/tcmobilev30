//
//  MagnifyingView.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 28/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "MagnifyingView.h"
#import "ACMagnifyingGlass.h"

static CGFloat const kACMagnifyingViewDefaultShowDelay = 0.5;

@interface MagnifyingView ()
@property (nonatomic, retain) NSTimer *touchTimer;
- (void)addMagnifyingGlassAtPoint:(CGPoint)point;
- (void)removeMagnifyingGlass;
- (void)updateMagnifyingGlassAtPoint:(CGPoint)point;
@end

@implementation MagnifyingView
@synthesize magnifyingGlass, magnifyingGlassShowDelay;
@synthesize touchTimer;


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.magnifyingGlassShowDelay = kACMagnifyingViewDefaultShowDelay;
        
      
    }
    return self;
}



#pragma mark - touch events

- (void)touchesBegan:(CGPoint )touchPoint {
    self.touchTimer = [NSTimer scheduledTimerWithTimeInterval:magnifyingGlassShowDelay
                                                       target:self
                                                     selector:@selector(addMagnifyingGlassTimer:)
                                                     userInfo:[NSValue valueWithCGPoint:touchPoint]
                                                      repeats:NO];
}

- (void) touchesMove:(CGPoint)translation{
        [self updateMagnifyingGlassAtPoint:translation];
}

- (void) touchesEnded:(CGPoint )touchPoint{
    [self.touchTimer invalidate];
    self.touchTimer = nil;
    [self removeMagnifyingGlass];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    [self updateMagnifyingGlassAtPoint:[touch locationInView:self]];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.touchTimer invalidate];
    self.touchTimer = nil;
    [self removeMagnifyingGlass];
}

#pragma mark - private functions

- (void)addMagnifyingGlassTimer:(NSTimer*)timer {
    NSValue *v = timer.userInfo;
    CGPoint point = [v CGPointValue];
    [self addMagnifyingGlassAtPoint:point];
}

#pragma mark - magnifier functions

- (void)addMagnifyingGlassAtPoint:(CGPoint)point {
    
    if (!magnifyingGlass) {
        magnifyingGlass = [[ACMagnifyingGlass alloc] init];
    }
    
    if (!magnifyingGlass.viewToMagnify) {
        magnifyingGlass.viewToMagnify = self;
        
    }
    
    magnifyingGlass.touchPoint = point;
    [self.superview addSubview:magnifyingGlass];
    [magnifyingGlass setNeedsDisplay];
}

- (void)removeMagnifyingGlass {
    [magnifyingGlass removeFromSuperview];
}

- (void)updateMagnifyingGlassAtPoint:(CGPoint)point {
    magnifyingGlass.touchPoint = point;
    [magnifyingGlass setNeedsDisplay];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
