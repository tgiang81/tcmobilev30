//
//  TickerData.m
//  TCiPad
//
//  Created by n2n on 16/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "TickerData.h"

/*
 @property (nonatomic, strong) NSString *stkCode;
 @property (nonatomic, strong) NSString *stkName;
 @property (nonatomic, strong) NSString *lastDone;
 @property (nonatomic, strong) NSString *chg;
 @property (nonatomic, strong) NSString *volume;
 */

@implementation TickerData

@synthesize stkCode, stkName,  lastDone, chg, volume;

-(id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}


-(NSString*)description {
    return [NSString stringWithFormat:
            @"<TickerData>stkCode: %@, stkName: %@, lastDone: %@, chg: %@, volume: %@",
            stkCode, stkName, lastDone, chg, volume];
}

@end
