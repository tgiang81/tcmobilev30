//
//  QuoteScreenTableViewCell.m
//  TCUniversal
//
//  Created by Scott Thoo on 11/11/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "QuoteScreenTableViewCell.h"

@implementation QuoteScreenTableViewCell
@synthesize lblTrades,lblTradeVal,lblDynamic1,lblDynamic2,lblDynamic3,lblDynamic4,lblDynamic5,lblLastDone,lblRealofDelayed;
@synthesize lblPerChg,lblAsk,lblBid,lblTp,lblEps,lblPar,lblTop,lblClose,lblAskQty,lblBidQty,lblChange;
@synthesize lblRemarks,lblCategory,lblExchange;
@synthesize stkName;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
      
        
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code

    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
