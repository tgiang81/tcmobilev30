

//
//  WatchListStk.h
//  TCiPad
//
//  Created by n2n on 15/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WatchListStk : NSObject

@property (nonatomic, strong) NSString *stkCode;
@property (nonatomic, strong) NSString *stkName;

-(id)init;

@end
