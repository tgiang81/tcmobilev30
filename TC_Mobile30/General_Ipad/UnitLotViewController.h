//
//  UnitLotViewController.h
//  TCiPad
//
//  Created by n2n on 05/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface UnitLotViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *qtyIndicator;
@property (weak, nonatomic) IBOutlet UILabel *touchIDLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *qtySegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *qScrViewMode;
@property (weak, nonatomic) IBOutlet UISegmentedControl *qViewMode;
@property (weak, nonatomic) IBOutlet UISegmentedControl *touchIDMode;
@property (weak, nonatomic) IBOutlet UISegmentedControl *pushNotificationMode;


- (IBAction)qtyTypeChanged:(id)sender;
- (IBAction)quoteScrViewModeChanged:(id)sender;
- (IBAction)screenViewModeChanged:(id)sender;

@end
