//
//  AppInfoViewController.h
//  TCiPad
//
//  Created by n2n on 04/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"

@interface AppInfoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *appLabel;
@property (strong, nonatomic) IBOutlet UITextView *termAndConditionTextView;
@property (strong, nonatomic) IBOutlet UITextView *contactsView;

- (IBAction)shareApp:(id)sender;
- (IBAction)clickFacebook:(id)sender;
- (IBAction)clickTwitter:(id)sender;
- (IBAction)clickLinkedIn:(id)sender;
- (IBAction)clickWebsite:(id)sender;

@end
