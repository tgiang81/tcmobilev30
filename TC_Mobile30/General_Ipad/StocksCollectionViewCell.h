//
//  StocksCollectionViewCell.h
//  TCiPad
//
//  Created by n2n on 15/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"

@protocol StockCellDelegate <NSObject>
-(void)removeButtonDidClicked:(NSIndexPath*)indexPath;
@end

@interface StocksCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *stockLabel;
@property (weak, nonatomic) id<StockCellDelegate>delegate;
@property (nonatomic, strong)NSIndexPath *myPath;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

- (IBAction)removeItem:(id)sender;

@end
