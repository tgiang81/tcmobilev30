//
//  Validate1FAViewController.h
//  TCiPad
//
//  Created by n2n on 30/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"
#import "AppControl.h"
#import "UserPrefConstants.h"
#import "ATPAuthenticate.h"

typedef enum ValType {
    VAL_1FA = 1,
    VAL_2FA,
    VAL_PIN,
    VAL_SMS,
    VAL_SMS_PIN
} ValType;

@protocol ValidatePINDelegate <NSObject>
-(void)validatePINokBtnClickedWithPassword:(NSString*)passwd andType:(ValType)type;
-(void)validatePINcancelClicked;
- (void)isDisableRequestSMSTOPBtn:(BOOL)isDisable;
@optional
- (void) showSLCAlertPopup;
@end

@interface ValidatePINViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cst_Bottom;

@property (weak, nonatomic) IBOutlet UILabel *myTitle;
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIView *dvDataView;
@property (weak, nonatomic) IBOutlet UILabel *dvDataLabel;
@property (nonatomic, strong) UIPickerView *myPickerView;
@property (nonatomic, strong) UIPopoverPresentationController *popoverController;
@property (nonatomic, strong) UIPopoverController *columnPickerPopover;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rememberPin;
@property (weak, nonatomic) IBOutlet UIButton *smsOTPBtn;
@property (weak, nonatomic) IBOutlet UIButton *pinOTPBtn;
@property (weak, nonatomic) IBOutlet UILabel *smsOTPLabel;
@property (weak, nonatomic) IBOutlet UIButton *btn_RememberPin;
@property (weak, nonatomic) IBOutlet UIView *v_rememberPin;

@property (weak, nonatomic) IBOutlet UIButton *requestSMSTOPBtn;
@property (weak, nonatomic) IBOutlet UITextField *smsOTPPinTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *keyTextField;
@property (weak, nonatomic) IBOutlet UIButton *infoSMSButton;

@property (weak, nonatomic) id<ValidatePINDelegate> delegate;
@property (nonatomic, assign) ValType validatingType;

- (IBAction)showDvDataSelector:(id)sender;
- (IBAction)passwordChanged:(id)sender;
- (IBAction)okTapped:(id)sender;
- (IBAction)cancelTapped:(id)sender;
- (IBAction)toggleRememberPin:(id)sender;
- (IBAction)requestSMSOTP:(id)sender;
- (IBAction)chooseOTPPinTab:(id)sender;
- (IBAction)chooseSMSOTPTab:(id)sender;
- (void)actionAfterClosing;
- (void)checkRememberPinState;
- (CGFloat)redundentHeight;
@end
