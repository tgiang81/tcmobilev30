//
//  MarketStreamerViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 05/11/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "MarketStreamerViewController.h"
#import "VertxConnectionManager.h"
#import "StockTicker.h"
#import "ATPAuthenticate.h"
#import "LanguageKey.h"

@interface MarketStreamerViewController (){
    VertxConnectionManager *vertxConnectionManager;
    ATPAuthenticate *atp;
    int leftCount;
    NSMutableArray *watchListArr;
    NSArray *filterByArray;
    int valueCount;
    int quantityCount;
    int numberOfLine;
    BOOL leftDirection;
}

@property (nonatomic, strong) NSMutableArray *leftStreamerArray;

@end

@implementation MarketStreamerViewController
@synthesize leftStreamerArray;
@synthesize leftSteamerView;
@synthesize rightStreamerView;
@synthesize buttonDirection;
@synthesize buttonLine;

- (IBAction) clickButtonDirection:(id)sender{
    leftDirection =! leftDirection;
}

- (IBAction) clickButtonLine:(id)sender{
    numberOfLine++;
    if (numberOfLine>=3) {
        numberOfLine=0;
    }
    
}

- (void) printRect:(CGRect)rect
{
 //   NSLog(@"printRect = %0.1f, %0.1f, %0.1f, %0.1f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

- (void) setTickerContent:(int)tag{
    //NSLog(@"ticker Tag %d",tag);
    StockStreamer *ticker;
    if (tag+1>13) {
        ticker = (StockStreamer *)[self.rightStreamerView viewWithTag:tag+1];
    }else{
        ticker = (StockStreamer *)[self.leftSteamerView viewWithTag:tag+1];
    }

    ticker.alpha = 1;
    
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
        
        //change = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"1001"]stringValue];
        //changeP = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"1003"]stringValue];
    }
    
    int timeValue = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"54"] intValue];
    NSDate *dateTraded = [NSDate dateWithTimeIntervalSince1970:timeValue];
    
    float lacp =  [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"51"] floatValue];
    float price = [[[leftStreamerArray objectAtIndex:tag] objectForKey:@"98"] floatValue];
    
    NSString *stockCode = [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"33"]];
    NSString *pi =  [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"104"]];
    NSString *priceString =  [NSString stringWithFormat:@"%.3f",price];
    NSString *quantity =  [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"99"]];
    NSString *value =  [NSString stringWithFormat:@"%@", [[leftStreamerArray objectAtIndex:tag] objectForKey:@"100"]];
    
    float changeValue = price - lacp;
    float changeValuePercentage = changeValue / lacp  * 100;
    
    NSString *changeValueString =  [NSString stringWithFormat:@"%.4f",changeValue];
    NSString *changeValuePercentageString =  [NSString stringWithFormat:@"%.3f%%",changeValuePercentage];
   
    //NSLog(@"Stock Code %@ and Tag %d",stockCode,tag);
     [ticker updateContent:stockCode
                    andPI:pi
                 andPrice:priceString
              andQuantity:quantity
                 andValue:value
                  andTime:[self formatDate:dateTraded]
                andChange:changeValueString
      andChangePercentage:changeValuePercentageString
            andChangeValue:changeValue];
    

}

- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"hh:mm:ss"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
- (void) latestWatchListContentReceived{
   // NSLog(@"latestWatchListContentReceived %@",[UserPrefConstants singleton].userWatchListStockCodeArr);
     [vertxConnectionManager vertxGetMarketStreamer:[UserPrefConstants singleton].userWatchListStockCodeArr andValue:valueCount andQuantity:quantityCount andCondition:0];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    popupTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    popupTableViewController.tableView.delegate=self;
    popupTableViewController.tableView.dataSource=self;
    [popupTableViewController.tableView setTag:1];
    
    popupFilterTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    popupFilterTableViewController.tableView.delegate=self;
    popupFilterTableViewController.tableView.dataSource=self;
     [popupFilterTableViewController.tableView setTag:2];
    leftStreamerArray = [[NSMutableArray alloc] init];
    
    vertxConnectionManager = [VertxConnectionManager singleton];
    [vertxConnectionManager vertxGetMarketStreamer:nil andValue:0 andQuantity:0 andCondition:0];
    
     atp = [ATPAuthenticate singleton];
    
    filterByArray = [[NSArray alloc] initWithObjects:@"Value >= 100K",@"Vol >= 30K",@"Vol >= 30K && Value >= 100K",@"Vol >= 30K && Value >= 30K",nil];
    
    //NSLog(@"leftSteamerView %f",leftSteamerView.frame.size.width);
//
//      NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
//
//    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS]) {
//        [_bbhLabel setText:@"BBH"];
//        [_sbhLabel setText:@"SBH"];
//    }
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    NSLog(@"Screen width %.2f, screen height %.2f",screenWidth,screenHeight);
    
    float sizeIncreaseHeight = screenHeight/768;
    float sizeIncreaseWidth = screenWidth/1024;
    
    float height = 0;
    for (int x=0;x<13;x++)
    {
        StockStreamer *ticker = [[StockStreamer alloc] initWithFrame:CGRectMake(0, 0, 510*sizeIncreaseWidth, 42*sizeIncreaseHeight)];
        height = ticker.frame.size.height;
        ticker.frame = CGRectMake(0,(10*sizeIncreaseHeight)+x*((height+5)*sizeIncreaseHeight),510*sizeIncreaseWidth,42*sizeIncreaseHeight);
        [ticker setTag:x+1];
        
        ticker.contentMode = UIViewContentModeScaleAspectFill;
    
        
        ticker.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        
        [self.leftSteamerView addSubview:ticker];
        ticker.alpha = 0;
    }
    
    for (int x=0;x<13;x++)
    {
        StockStreamer *ticker = [[StockStreamer alloc] initWithFrame:CGRectMake(0, 0, 510*sizeIncreaseWidth, 42*sizeIncreaseHeight)];
        height = ticker.frame.size.height;
        ticker.frame = CGRectMake(0,(10*sizeIncreaseHeight)+x*((height+5)*sizeIncreaseHeight),510*sizeIncreaseWidth,42*sizeIncreaseHeight);
        [ticker setTag:x+14];
        
        ticker.contentMode = UIViewContentModeScaleAspectFill;
        
        ticker.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        
        [self.rightStreamerView addSubview:ticker];
         ticker.alpha = 0;
    }
    

    [self loadWatchList];
    /*[NSTimer scheduledTimerWithTimeInterval: 1.0
                                     target: self
                                   selector: @selector(handleTimer)
                                   userInfo: nil
                                    repeats: YES];*/

//    for (int x=0;x<12;x++)
//    {
//
//        StreamerCellViewViewController *stockFlag =[[StreamerCellViewViewController alloc]initWithStockDetails:leftSteamerView];
//
//        stockFlag.tag = x++ ; //indicator to remove while refreshing stockFlag
//        stockFlag.autoresizesSubviews = NO;
//
//        stockFlag.frame = CGRectMake(0, 0, leftSteamerView.frame.size.width-100 ,40);
//        stockFlag.autoresizingMask = UIViewAutoresizingNone;
//
//        //NSLog(@"traget view class %@",[targetView class]);
//        [leftSteamerView addSubview:stockFlag];
//    }

    
    // Do any additional setup after loading the view.
}

-(void) loadWatchList{
    watchListArr = [[NSMutableArray alloc] initWithArray:[UserPrefConstants singleton].userWatchListArray copyItems:YES];
    [watchListArr insertObject:@"None" atIndex:0];
    //NSLog(@"watchListArr %@",watchListArr);
    // if watchlist exist, select the first one
}
- (void) handleTimer{
    [self testStreamer];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    leftCount = 0;
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestWatchListContentReceived) name:@"getWatchlistContentSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(marketStreamerReceived:)  name:@"marketStreamerReceived" object:nil];
}

#pragma mark - Receive notification from Vertx
- (void)marketStreamerReceived:(NSNotification *)notification
{
       dispatch_async(dispatch_get_main_queue(), ^{
     NSDictionary * response = [notification.userInfo copy];

    if(leftStreamerArray.count<26){
        [leftStreamerArray addObject:response];
       // NSLog(@"marketStreamerReceived %@ = %d",response,leftCount);
    }else{
        if (leftCount>=26) {
            leftCount=0;
        }
        [leftStreamerArray replaceObjectAtIndex:leftCount withObject:response];
        // NSLog(@"replaceObjectAtIndex %@ = %d",response,leftCount);
    }
      
   if (leftCount<26) {
              // NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:leftCount inSection:0];
              // NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
              // [tableViewLeft reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
       
       // Update Content
       [self setTickerContent:leftCount];
   }
     
        // populate the data to screen
       leftCount++;
    });
}


- (void) testStreamer{
    
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"TestStreamer" ofType:@"plist"];
    NSDictionary *response = [NSDictionary dictionaryWithContentsOfFile:file];

    if(leftStreamerArray.count<26){
        [leftStreamerArray addObject:response];
        //NSLog(@"marketStreamerReceived %@ = %d",response,leftCount);
    }else{
        if (leftCount>=26) {
            leftCount=0;
        }
        [leftStreamerArray replaceObjectAtIndex:leftCount withObject:response];
        //NSLog(@"replaceObjectAtIndex %@ = %d",response,leftCount);
    }
    
    if (leftCount<26) {
        // NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:leftCount inSection:0];
        // NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        // [tableViewLeft reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationFade];
        
        // Update Content
        [self setTickerContent:leftCount];
    }
    
    // populate the data to screen
    leftCount++;

}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag==1) {
        return watchListArr.count;
    }else{
        return 4;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *SimpleCellIdentifier = @"Cell";
    UITableViewCell *simpleCell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:SimpleCellIdentifier];
    if (simpleCell == nil) {
        simpleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleCellIdentifier];
    }
    simpleCell.textLabel.backgroundColor = [UIColor clearColor];
    simpleCell.textLabel.textColor = [UIColor darkTextColor];
    simpleCell.textLabel.font =  [UIFont systemFontOfSize:16];
    simpleCell.textLabel.numberOfLines = 2;
    simpleCell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    //NSLog(@"left Count = %d",leftCount);
    if (tableView.tag==1) {
        if (indexPath.row==0) {
            simpleCell.textLabel.text = @"None";
        }else{
            simpleCell.textLabel.text = [[watchListArr objectAtIndex:indexPath.row] objectForKey:@"Name"];
        }
    }else{
        simpleCell.textLabel.text = [filterByArray objectAtIndex:indexPath.row];
    }
    return simpleCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag==1) {
        
        if (indexPath.row==0) {
            _watchListLabel.text = @"None";
            favID = 0;
        }else{
            _watchListLabel.text = [[watchListArr objectAtIndex:indexPath.row] objectForKey:@"Name"];
            favID = [[[watchListArr objectAtIndex:indexPath.row] objectForKey:@"FavID"] intValue];
        }
        

    }else{
        _filterByLabel.text = [filterByArray objectAtIndex:indexPath.row];
        switch (indexPath.row) {
            case 0:
                valueCount = 100000;
                quantityCount = 0;
                break;
            case 1:
                valueCount = 0;
                quantityCount = 30000;
                break;
            case 2:
                valueCount = 100000;
                quantityCount = 30000;
                break;
            case 3:
                valueCount = 30000;
                quantityCount = 30000;
                break;
            default:
                break;
        }
    }
    [columnPickerPopover dismissPopoverAnimated:TRUE];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    [[NSNotificationCenter defaultCenter] removeObserver:@"getWatchlistContentSuccess"];
     [[NSNotificationCenter defaultCenter] removeObserver:@"marketStreamerReceived"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickApplyButton:(UIButton *)sender{
    leftCount = 0;
    [vertxConnectionManager vertxUnsubscribeMarketStreamer];
    for(int x=0;x<26;x++){
    StockStreamer *ticker;
    if (x+1>13) {
            ticker = (StockStreamer *)[self.rightStreamerView viewWithTag:x+1];
        }else{
            ticker = (StockStreamer *)[self.leftSteamerView viewWithTag:x+1];
        }
        ticker.alpha = 0;
    }
    [leftStreamerArray removeAllObjects];
    
    if (favID!=0) {
        [atp getWatchListItemsForStreamer:favID];
    }else{
          [vertxConnectionManager vertxGetMarketStreamer:nil andValue:valueCount andQuantity:quantityCount andCondition:0];
    }

}

- (IBAction)clickWatchList:(UIButton *)sender{
    [popupTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupTableViewController];
    navController.navigationBar.topItem.title= _WatchList;
    
    popupTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    NSInteger totalToDisplay = 5;
    if (totalToDisplay>=10) totalToDisplay = 10;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*60);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 40;
}

- (IBAction)clickFilterBy:(UIButton *)sender{
    [popupFilterTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupFilterTableViewController];
    navController.navigationBar.topItem.title= @"Filter By:";
    
    popupFilterTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    navController.preferredContentSize=CGSizeMake(325, 150);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
