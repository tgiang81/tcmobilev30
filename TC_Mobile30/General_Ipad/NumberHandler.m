//
//  NumberHandler.m
//  N2NTrader
//
//  Created by Adrian Lo on 3/20/11.
//  Copyright 2011 The Wabbit Studio. All rights reserved.
//

#import "NumberHandler.h"


@implementation NumberHandler

+ (NSString *) numberFormatCurrencyRate:(double)input {
    NSNumberFormatter *formatter =  [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setCurrencySymbol:@""];
    [formatter setUsesSignificantDigits:YES];
    [formatter setMinimumSignificantDigits:3];
    
    NSString *result = [formatter stringFromNumber:[NSNumber numberWithDouble:input]];
    [formatter release];
    
    return result;
}

+ (NSString *)numberFormatIndicesPrice:(double)input forExchange:(NSString *)exchg {
    NSString *output = nil;  
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setNumberStyle:kCFNumberFormatterDecimalStyle];
    
    if ([exchg isEqualToString:@"JK"] || [exchg isEqualToString:@"JKD"]) {
        [nf setMinimumFractionDigits:3];
        [nf setMaximumFractionDigits:3];
    }
    else if ([exchg isEqualToString:@"PH"] || [exchg isEqualToString:@"PHD"]) {
        [nf setMinimumFractionDigits:4];
        [nf setMaximumFractionDigits:4];
    }
    else {
        [nf setMinimumFractionDigits:2];
        [nf setMaximumFractionDigits:2];
    }
    
    if (input >= 1000000 || input <= -1000000) {
        output = [self shortenNumberWithDouble:input];
    }
    else {
        output = [nf stringFromNumber:[NSNumber numberWithDouble:input]];
    }
    
	[nf release];
	return output;
}

+ (NSString *)numberFormatStockPrice:(double)input isPrice:(BOOL)isPrice isPriceChange:(BOOL)isPriceChange forExchange:(NSString *)exchg {
    NSString *output = nil;  
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setNumberStyle:kCFNumberFormatterDecimalStyle];
    
    if ([exchg isEqualToString:@"JK"] || [exchg isEqualToString:@"JKD"]) {
        [nf setMinimumFractionDigits:0];
        [nf setMaximumFractionDigits:0];
    }
    else if ([exchg isEqualToString:@"PH"] || [exchg isEqualToString:@"PHD"]) {
        [nf setMinimumFractionDigits:4];
        [nf setMaximumFractionDigits:4];
    }
    else {
        if (isPrice) {
            if (input > 1000 || input < -1000) {
                [nf setMaximumFractionDigits:2];
                [nf setMinimumFractionDigits:2];
            }
            else {
                [nf setMaximumFractionDigits:3];
                [nf setMinimumFractionDigits:3];
            }
        }
        else if (isPriceChange) {
            [nf setMaximumFractionDigits:3];
            [nf setMinimumFractionDigits:3];
        }
    }
    
   // DLog(@"input price %f",input);
    
	output = [nf stringFromNumber:[NSNumber numberWithDouble:input]];
	
	[nf release];
	return output;
}

+ (NSString *)numberFormatStockPriceForOrderSubmission:(double)input forExchange:(NSString *)exchg {
    
    if ([exchg isEqualToString:@"JK"] || [exchg isEqualToString:@"JKD"]) {
        return [NSString stringWithFormat:@"%.0f", input];
    }
    else if ([exchg isEqualToString:@"PH"] || [exchg isEqualToString:@"PHD"]) {
        return [NSString stringWithFormat:@"%.4f", input];
    }
    else {
        if (input > 1000 || input < -1000) {
            return [NSString stringWithFormat:@"%.2f", input];
        }
        else {
            return [NSString stringWithFormat:@"%.3f", input];
        }
    }
    
    return nil;
}

+ (NSString *) shortenNumberWithLong:(long)input {
	NSString *output = nil;
	double val = 0;
	NSNumberFormatter *long_number_formatter = [[NSNumberFormatter alloc] init];
	[long_number_formatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	[long_number_formatter setMaximumFractionDigits:2];
	
    BOOL neg = NO;
	if (input < 0) {
		input *= -1;
		neg = YES;
	}
    
	if (input >= 1000000) {
		val = (double)input / 1000000;
		output = [NSString stringWithFormat:@"%@M", [long_number_formatter stringFromNumber:[NSNumber numberWithDouble:val]]];			
	}
	else {
		output = [NSString stringWithFormat:@"%@", [long_number_formatter stringFromNumber:[NSNumber numberWithLong:input]]];			
	}
    [long_number_formatter release];
    
    if (neg) {
		output = [NSString stringWithFormat:@"-%@", output];
	}
	
	return output;
}

+ (NSString *) shortenNumberWithLongLong:(long long)input {
	NSString *output = nil;
	double val = 0;
	NSNumberFormatter *long_number_formatter = [[NSNumberFormatter alloc] init];
	[long_number_formatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	[long_number_formatter setMaximumFractionDigits:2];
	
    BOOL neg = NO;
	if (input < 0) {
		input *= -1;
		neg = YES;
	}
    
	if (input >= 1000000) {
		val = (double)input / 1000000;
		output = [NSString stringWithFormat:@"%@M", [long_number_formatter stringFromNumber:[NSNumber numberWithDouble:val]]];			
	}
	else {
		output = [NSString stringWithFormat:@"%@", [long_number_formatter stringFromNumber:[NSNumber numberWithLongLong:input]]];			
	}
    [long_number_formatter release];
    
    if (neg) {
		output = [NSString stringWithFormat:@"-%@", output];
	}
	
	return output;
}


+ (NSString *) shortenNumberWithDouble:(double)input {
	NSString *output = nil;
	NSNumberFormatter *long_number_formatter = [[NSNumberFormatter alloc] init];
	[long_number_formatter setNumberStyle:kCFNumberFormatterDecimalStyle];
	[long_number_formatter setMinimumFractionDigits:2];
    [long_number_formatter setMaximumFractionDigits:2];
	
	BOOL neg = NO;
	if (input < 0) {
		input *= -1;
		neg = YES;
	}
	
	if (input >= 1000000) {
		input /= 1000000;
		output = [NSString stringWithFormat:@"%@M", [long_number_formatter stringFromNumber:[NSNumber numberWithDouble:input]]];			
	} 
    /*else if (input >= 10000 || input <= -10000) {
    		input /= 10000;
    		output = [NSString stringWithFormat:@"%@K", [long_number_formatter stringFromNumber:[NSNumber numberWithDouble:input]]];			
    }*/
	else {
		output = [NSString stringWithFormat:@"%@", [long_number_formatter stringFromNumber:[NSNumber numberWithDouble:input]]];			
	}
	[long_number_formatter release];
	
	if (neg) {
		output = [NSString stringWithFormat:@"-%@", output];
	}
	
	return output;
}

+ (NSString *) numberFormat:(double) input {
	NSString *output = nil;  
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setNumberStyle:kCFNumberFormatterDecimalStyle];
	if (input > 1000 || input < -1000) {
		[nf setMaximumFractionDigits:2];
		[nf setMinimumFractionDigits:2];
	}
	else {
		[nf setMaximumFractionDigits:3];
		[nf setMinimumFractionDigits:3];
	}
	output = [nf stringFromNumber:[NSNumber numberWithDouble:input]];
	
	[nf release];
	return output;
}

+ (NSString *) numberFormat2D:(double) input {
	NSString *output = nil;  
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setNumberStyle:kCFNumberFormatterDecimalStyle];

	[nf setMaximumFractionDigits:2];
	[nf setMinimumFractionDigits:2];
	output = [nf stringFromNumber:[NSNumber numberWithDouble:input]];
	
	[nf release];
	return output;
}

+ (NSString *) numberFormat0DWithDouble:(double) input {
	NSString *output = nil;  
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setNumberStyle:NSNumberFormatterDecimalStyle];
	[nf setMaximumFractionDigits:0];
    [nf setMinimumFractionDigits:0];
	output = [nf stringFromNumber:[NSNumber numberWithDouble:input]];	
	[nf release];
    output = [output stringByReplacingOccurrencesOfString:@"," withString:@""];
	return output;
}

+ (NSString *) numberFormat0D:(long long) input {
	NSString *output = nil;  
	NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
	[nf setNumberStyle:kCFNumberFormatterDecimalStyle];
	[nf setMaximumFractionDigits:0];
	output = [nf stringFromNumber:[NSNumber numberWithLongLong:input]];	
	[nf release];
	return output;
}

+(BOOL)isEmpty:(id)thing
{
	return thing == nil || 
	([thing respondsToSelector:@selector(length)] && [(NSData *)thing length] == 0) || 
	([thing respondsToSelector:@selector(count)] && [(NSArray *)thing count] == 0); 
}

+(NSString *)deleteDPValue:(NSString *)dataStr
{
	if ([dataStr length] > 0) {
		NSRange range = [dataStr rangeOfString:@"."];
		int index = range.location;
		if (index != NSNotFound) {
			if (index == 0) {
				return @"0";
			}else {
				return [dataStr substringToIndex:index];
			}
		}else {
			return dataStr;
		}
	}
	return dataStr;
}

+(NSString *)formatDecimalNumber:(double)dInput longInput:(long)lInput llInput:(long long)llInput withDecimalPlaces:(BOOL)withDP noOfDecimalPlaces:(int)noOfDP isDoubleInput:(BOOL)isDouble isLLInput:(BOOL)isLongLong withThousandsSeperator:(NSString *)tseparator shortenValue:(BOOL)shortenValue orgHasDP:(BOOL)orgHasDP
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *temp;
    NSString *returnStr = @"";
    NSString *appendStr = @"";
    if (isDouble) {
        temp = [NSNumber numberWithDouble:dInput];
    } else if (isLongLong)
    {
        temp = [NSNumber numberWithLongLong:llInput];
    }
    else {
        temp = [NSNumber numberWithLong:lInput];
    }
    if (withDP) {
        [formatter setMaximumFractionDigits:noOfDP];
        [formatter setMinimumFractionDigits:noOfDP];
    } else {
        [formatter setMaximumFractionDigits:0];
        [formatter setMinimumFractionDigits:0];
    }
    
    
    if (shortenValue) {
        NSNumber *decimalValue = [NSNumber numberWithDouble:0];
        NSString *originalDecimalValue;
        if (orgHasDP) {
            NSRange range = [[temp stringValue] rangeOfString:@"."];
            int index = range.location;
            if (index != NSNotFound) {
                if (index > 0) {
                    NSNumberFormatter *formatter3 = [[NSNumberFormatter alloc] init];
                    [formatter3 setNumberStyle:NSNumberFormatterDecimalStyle];
                    [formatter3 setMaximumFractionDigits:noOfDP];
                    [formatter3 setMinimumFractionDigits:noOfDP];

                    originalDecimalValue = [NSString stringWithFormat:@"%@%@", @"0", [[temp stringValue] substringFromIndex:index]];
                    decimalValue = [formatter3 numberFromString:originalDecimalValue];
                    if (isLongLong)
                    {
                        temp = [NSNumber numberWithLongLong:[[[temp stringValue] substringToIndex:index] longLongValue]];
                    } else {
                        temp = [NSNumber numberWithDouble:[[[temp stringValue] substringToIndex:index] doubleValue]];
                    }
                    [formatter3 release];
                }
            }
        }
        
        NSNumberFormatter *formatter2 = [[NSNumberFormatter alloc] init];
        [formatter2 setNumberStyle:NSNumberFormatterDecimalStyle];
        int noOfFrontDigits = 8;
        if ([temp doubleValue] < 0) {
            noOfFrontDigits = 7;
        } else if (orgHasDP) {
            noOfFrontDigits = 6;
        }
        if ([[temp stringValue] length] >= noOfFrontDigits) {
            appendStr = @"M";
            [formatter2 setMaximumFractionDigits:3];
            if ([[temp stringValue] length] == noOfFrontDigits+1) {
                //noOfFrontDigits += 1;
                [formatter2 setMaximumFractionDigits:2];
            }
            else if ([[temp stringValue] length] >= noOfFrontDigits+2) {
                //noOfFrontDigits += 2;
                [formatter2 setMaximumFractionDigits:0];
            }
            temp = [NSNumber numberWithDouble:[temp doubleValue]/1000000];
            
        } else if ([[temp stringValue] length] >= noOfFrontDigits-1){
            appendStr = @"K";
            //noOfFrontDigits -= 1;
            temp = [NSNumber numberWithDouble:[temp doubleValue]/1000];
            [formatter2 setMaximumFractionDigits:3];
        }
        if (![self isEmpty:appendStr]) {
            returnStr = [NSString stringWithFormat:@"%@%@", [formatter2 stringFromNumber:temp], appendStr];
        } 
        else {
            if (!orgHasDP) {
                returnStr = [formatter stringFromNumber:temp];
            } 
            else {
                if ([temp doubleValue] == 0) {
                    
                    if ([[temp stringValue] rangeOfString:@"-"].location != NSNotFound) {
                        
                        if (isLongLong)
                        {
                            temp = [NSNumber numberWithLongLong:([temp longLongValue] - [decimalValue longLongValue])];
                        } 
                        else {
                            temp = [NSNumber numberWithDouble:([temp doubleValue] - [decimalValue doubleValue])];
                        }
                    }
                    else {
                        
                        if (isLongLong)
                        {
                            temp = [NSNumber numberWithLongLong:([temp longLongValue] + [decimalValue longLongValue])];
                        } 
                        else {
                            temp = [NSNumber numberWithDouble:([temp doubleValue] + [decimalValue doubleValue])];
                        }
                    }
                }
                else if ([temp doubleValue] < 0) {
                    
                    if (isLongLong)
                    {
                        temp = [NSNumber numberWithLongLong:([temp longLongValue] - [decimalValue longLongValue])];
                    } 
                    else {
                        temp = [NSNumber numberWithDouble:([temp doubleValue] - [decimalValue doubleValue])];
                    }
                }
                else if ([temp doubleValue] > 0) {
                    
                    if (isLongLong)
                    {
                        temp = [NSNumber numberWithLongLong:([temp longLongValue] + [decimalValue longLongValue])];
                    } 
                    else {
                        temp = [NSNumber numberWithDouble:([temp doubleValue] + [decimalValue doubleValue])];
                    }
                }
                returnStr = [formatter stringFromNumber:temp];
            }
        }
        [formatter2 release];
    } else {
        returnStr = [formatter stringFromNumber:temp];
    }
    
    if (![self isEmpty:tseparator]) { 
        returnStr = [returnStr stringByReplacingOccurrencesOfString:@"," withString:tseparator];
    }
    [formatter release];
    return returnStr;
}

@end
