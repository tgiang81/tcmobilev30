//
//  UIImageUtility.h
//  testTmx
//
//  Created by Apple on 2/6/10.
//  Copyright 2010 __Terato Tech Sdn Bhd__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage(Loading)
+ (UIImage*)imageFromResource:(NSString*)name;
@end
