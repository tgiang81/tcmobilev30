//
//  PricePadViewController.h
//  TCiPad
//
//  Created by n2n on 20/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"

@protocol PricePadDelegate <NSObject>
-(void)priceBtnPressed:(UIButton *)btn;
@end

@interface PricePadViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *v_Content;

@property (nonatomic,weak) id <PricePadDelegate> delegate;

- (IBAction)btnPressed:(id)sender;


@end
