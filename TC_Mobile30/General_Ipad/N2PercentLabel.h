//
//  N2PercentLabel.h
//  test
//
//  Created by Emir on 17/01/2017.
//  Copyright © 2017 n2n. All rights reserved.
//

/*
 PURPOSE: To display Buy% data +visual representation for Business Done
 */

#import <UIKit/UIKit.h>


@interface N2PercentLabel : UILabel
@property (nonatomic)  CGFloat ratio;
@property (nonatomic)  UIColor *color1;
@property (nonatomic)  UIColor *color2;


- (void)drawRect:(CGRect)rect;
-(id)initWithFrame:(CGRect)frame;

@end
