//
//  ElasticNewsWebController.h
//  TCiPad
//
//  Created by n2n on 13/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ElasticNewsWebController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *newsWebView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivity;

@end
