//
//  IndicesTimeSalesTableViewCell.h
//  TCiPad
//
//  Created by Scott Thoo on 2/27/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndicesTimeSalesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *percent;
@property (weak, nonatomic) IBOutlet UILabel *lblVol;

@end
