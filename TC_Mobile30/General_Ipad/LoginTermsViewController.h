//
//  LoginTermsViewController.h
//  TCiPad
//
//  Created by n2n on 10/07/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageManager.h"

@interface LoginTermsViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextView *termsTextView;
@property (nonatomic, strong) NSString *tocText;
- (IBAction)dismissTerms:(id)sender;

@end
