//
//  LoginViewController.h
//  TCUniversal
//
//  Created by Scott Thoo on 9/25/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "LanguageManager.h"
#import "ProcessViewController.h"
#import "LoginTermsViewController.h"
#import <AVFoundation/AVFoundation.h>

typedef enum{
    AuthenticationErrorNone = 0,
    AuthenticationErrorFailed,
    AuthenticationErrorCancelled,
    AuthenticationErrorFallBack,
    AuthenticationErrorNotConfigured,
    AuthenticationErrorTouchIDNotEnrolled,
    AuthenticationErrorTouchIDNotAvailable
}AuthenticationError;

@interface LoginViewController : UIViewController<UITextFieldDelegate, UIAlertViewDelegate, UITableViewDelegate,
UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *releaseNotesView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *languageBtn;
@property (strong, nonatomic) UITableViewController *languageTableViewVC;
@property (strong, nonatomic) UIPopoverController *columnPickerPopover;
@property (weak, nonatomic) IBOutlet UIButton *deactivateBtn;
@property (nonatomic, assign) BOOL tncAccept;
@property (nonatomic, strong) ProcessViewController *processView;
@property (weak, nonatomic) IBOutlet UILabel *helpInfoTextView;
@property (weak, nonatomic) IBOutlet UIButton *termsButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UIWebView *annoucementWebView;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;


- (IBAction)openTerms:(id)sender;
- (IBAction)tncAcceptToggle:(id)sender;
- (IBAction)deactivateTouchID:(id)sender;
- (IBAction)callLanguageSelection:(UIButton*)sender;
- (IBAction)brokerBack:(id)sender;
- (IBAction) touchIDButtonClicked:(id)sender;
- (IBAction) cancelTouchID:(id)sender;
- (IBAction) view2TouchIDButtonClicked:(id)sender;
@end
