//
//  SelectorTableViewCell.h
//  TCiPad
//
//  Created by n2n on 11/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectorCellDelegate <NSObject>
-(void)expandBtnClicked:(NSIndexPath*)path;
@end

@interface SelectorTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *selectedView;
@property (weak, nonatomic) IBOutlet UILabel *selectionItem;
@property (weak, nonatomic) IBOutlet UIButton *expandButton;
@property (nonatomic, strong) NSIndexPath *path;
@property (weak, nonatomic) id<SelectorCellDelegate> delegate;

- (IBAction)expandTapped:(id)sender;

@end
