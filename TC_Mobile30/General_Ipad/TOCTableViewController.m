//
//  TOCTableViewController.m
//  TCiPad
//
//  Created by n2n on 04/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "TOCTableViewController.h"
#import "AppControl.h"
#import "UserPrefConstants.h"


@interface TOCTableViewController (){
    int rowNumber;
}

@end



@implementation TOCTableViewController


@synthesize tocDelegate;


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    
    //CGFloat xRatio = 1024.0/977.0;
   // CGFloat yRatio  = 768.0/656.0;
    
  //  CGRect newRect = CGRectMake(0,0,_frameToUse.size.width, _frameToUse.size.height/yRatio);
    
   // [self.tableView setFrame:newRect];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setBackgroundColor:[UIColor colorWithHue:0 saturation:0 brightness:0.1 alpha:1]];
    rowNumber = 3;
    NSLog(@"Row Number %d",rowNumber);
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = kCellGray1;
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor cyanColor]];
    
    [header.textLabel setText:[LanguageManager stringForKey:@"Settings"]];
    
    // Another way to set the background color
    // Note: does not preserve gradient effect of original header
    // header.contentView.backgroundColor = [UIColor blackColor];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return rowNumber;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    [[LanguageManager defaultManager] translateStringsForView:cell.contentView];
    
    cell.backgroundColor = kCellGray1;
    
}

-(NSString*)getSegueStrForIndex:(long)idx {
    NSString *res = @"";
    switch (idx) {
        case 0:
             res = @"segueUnitLot";
            break;
        case 1:
            res = @"segueAppInfo";
            break;
        case 2:
            res = @"segueKnowledgeCentre";
            break;
        default:
            res = @"segueAppInfo";
            break;
    }
    return res;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *segueStr = [self getSegueStrForIndex:indexPath.row];
    [tocDelegate tableRowDidClicked:segueStr];
    
  /*  if (indexPath.row==2) {
        rowNumber = 2;
        [self.tableView reloadData];
    }*/
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
    
}

//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *cellIdentifier = [NSString stringWithFormat:@"r%ld",  (long)indexPath.row];
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//        //you can customize your cell here because it will be used just for one row.
//       
//    }
//    
//    return cell;
//}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showDevTool:(UIGestureRecognizer*)recognizer {
    rowNumber = 3;
    [self.tableView reloadData];
}
@end
