//
//  TickerData.h
//  TCiPad
//
//  Created by n2n on 16/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TickerData : NSObject

@property (nonatomic, strong) NSString *stkCode;
@property (nonatomic, strong) NSString *stkName;
@property (nonatomic, strong) NSString *lastDone;
@property (nonatomic, strong) NSString *chg;
@property (nonatomic, strong) NSString *volume;


-(id)init;

@end
