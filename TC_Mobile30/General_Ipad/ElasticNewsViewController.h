//
//  ElasticNewsViewController.h
//  TCiPad
//
//  Created by n2n on 13/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ElasticNewsCell.h"
#import "LanguageManager.h"
#import "NSString_stripHtml.h"
#import "TransitionDelegate.h"
@interface ElasticNewsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    
    UIDatePicker *datePickerView;
    UIPopoverPresentationController *popoverController;
    UIPopoverController *columnPickerPopover;
    UITableViewController *popupSourceTableViewController;
    UITableViewController *popupCategoryTableViewController;
    BOOL isHeadLine;
    BOOL isArticles;
    NSString *keywordString;
    NSString *targetID;
    NSString *sourceID;
    NSString *categoryID;
    
    
}
@property (nonatomic, strong) TransitionDelegate *transitionController;
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UITextField *textFieldKeyword;
@property (weak, nonatomic) IBOutlet UITableView *newsTable;
@property (nonatomic, strong) NSMutableArray *newsArray;
@property (nonatomic, strong) NSMutableArray *sourceArray;
@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivity;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateToLabel;
@property (nonatomic, strong) NSDate *fromDate, *toDate;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *articlesButton;
@property (weak, nonatomic) IBOutlet UIButton *headerButton;

- (IBAction)callDatePicker:(UIButton *)sender;
- (IBAction)callCategory:(UIButton *)sender;
- (IBAction)callSource:(UIButton *)sender;
- (IBAction)clickSearch:(id)sender;
@end
