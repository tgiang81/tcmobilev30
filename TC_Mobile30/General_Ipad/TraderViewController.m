//
//  TraderViewController.m
//  TCiPad
//
//  Created by n2n on 10/10/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "TraderViewController.h"
#import "AppControl.h"

#import "StockDetailViewController.h"
#import "QuoteScreenViewController.h"
#import "LanguageKey.h"

@interface TraderViewController () {
    
    __weak IBOutlet UIButton *btnOrderType;
    __weak IBOutlet UIButton *btnValidity;
    __weak IBOutlet UIButton *btnCurrency;
    __weak IBOutlet UIButton *btnPayment;
    
    CGPoint padOriPos;
    CGFloat bufferPadX;
    CGFloat popupPickerWidth, popupPickerHeight;
    long totalAccToShow;
    NSString *action;
    CGFloat oriPriceX, oriQtyX;
    BOOL runOnce;
    TradeStatus *tradeStatusRevise, *tradeStatusCancel;
    NSString *actualAction;
    BOOL allowNegativePrice;
    
    int iCurrentFocusPicker;
}

@end

@implementation TraderViewController

@synthesize delegate;

#define  kBasicHeight 230.0
#define kFullHeight 390.0


//
// TRADER VC USED FOR ANY BUY,SELL,REVISE,CANCEL an order
//




#pragma mark - View LifeCycles delegate

// marketdepth for BUY and SELL and REVISE should be request by caller VC and passed in actionDict with @"MDArray" key

-(void)slideInOrderPad {
    
    // slide in the orderpad
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        _padView.center = CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
        
    } completion:^(BOOL finished) {
        padOriPos = _padView.center;
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(orderPadDidPanned:)];
        panGesture.delegate = self;
        [_padView addGestureRecognizer:panGesture];
    }];
}


-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (runOnce) {
        
        [_traderWorking setCenter:CGPointMake(_errorMsgLabel.center.x, 30)];
        
        UIVisualEffectView *beView = [self.view viewWithTag:100];
        beView.frame = self.view.frame;
        
        oriPriceX = _priceView.center.x;
        oriQtyX = _qtyView.center.x;
     //   if  (![UserPrefConstants singleton].orderPadAdvance) {

            padLarge = YES;
           // [self toggleMoreFields:nil]; default is big
//            [_padView setFrame:CGRectMake(_padView.frame.origin.x,
//                                          _padView.frame.origin.y,
//                                          _padView.frame.size.width,
//                                          kBasicHeight)];
//        } else {
//            
//            [_expandBtn setTitle:@"-" forState:UIControlStateNormal];
//        }
        [_padView setCenter:CGPointMake(self.view.frame.size.width*2.0,self.view.center.y)];
        
//        [_padView bringSubviewToFront:_toolBarView];
        
        runOnce = NO;

    }
    
    
}


-(void)callTrade {
    
    NSString *errMsg = @"";
    BOOL canTrade = YES;
    // validate if can trade or not
    
    // 1 check for useraccounts
    if ([acclist count]<=0) {
        canTrade = NO;
        errMsg = [LanguageManager stringForKey:@"Cannot trade because this user doesn't have Trading Accounts."];
    }
    
    if (canTrade) {
    
        [_loadingView setHidden:YES];
        [self updateOrderPad];
        [self refreshTrdLmtBtnPressed:nil];
        [self.loadingView setHidden:YES];
        [self slideInOrderPad];
        [_okBtn setHidden:NO];
        [_cancelBtn setHidden:YES];
    } else  {
        _errorMsgLabel.text = errMsg;
        [_okBtn setHidden:YES];
        [_cancelBtn setHidden:NO];
    }
}



-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    _cancelBtn.layer.cornerRadius = 5.0;
    _cancelBtn.layer.masksToBounds = YES;
    
    _padView.layer.shadowRadius = 10.0;
    _padView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _padView.layer.shadowOffset = CGSizeMake(-5, 5);
    _padView.layer.shadowOpacity = 0.7;
    
}

/*
 "[PaymentCfg_Payment]=SG|AccType:B|Payment:CUT|Sett:+",
 "[PaymentCfg_Payment]=SG|AccType:D|Payment:CFD|Sett:+",
 "[PaymentCfg_Payment]=SG|AccType:Def|Payment:Cash,CUT,CPF,SRS",
 
 B= CUT account
 D= CFD account
 */



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    self.pickerContainter.hidden = TRUE;
    
    allowNegativePrice = NO;
    
    _isShortSell = NO;
    [_cancelBtn setHidden:YES];
    [_okBtn setHidden:YES];
    [self.view bringSubviewToFront:_loadingView];
    
    NSString *stkCode = [UserPrefConstants singleton].userSelectingStockCode;
    _stockCurrency = [[[[QCData singleton]qcFeedDataDict]objectForKey:stkCode] objectForKey:FID_134_S_CURRENCY];

    
    for (ExchangeData *ed in [[UserPrefConstants singleton] userExchagelist]) {
        if ([ed.feed_exchg_code isEqual:[[UserPrefConstants singleton] userCurrentExchange]]) {
            self.currentED = ed;
            _exchangeCurrency = ed.currency;
            if ([_exchangeCurrency isEqualToString:@"RM"]) _exchangeCurrency = @"MYR";
        }
    }
    
    
    for(UIView *v in [self.boxView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
            btn.layer.masksToBounds = YES;
            btn.layer.borderWidth = 1.0;
            btn.layer.borderColor = [UIColor darkGrayColor].CGColor;
        }
    }
    
    _boxView.layer.cornerRadius = 8.0;
    _boxView.layer.masksToBounds = YES;
    
    self.traderWorking = [[DGActivityIndicatorView alloc]
                         initWithType:DGActivityIndicatorAnimationTypeNineDots tintColor:[UIColor blackColor]];
    
    _traderWorking.frame = CGRectMake(0, 0, 40, 40);
    [_boxView addSubview:_traderWorking];
    [_traderWorking startAnimating];
    [_traderWorking setCenter:CGPointMake(_errorMsgLabel.center.x, 30)];
    [_traderWorking setHidden:YES];
    [_traderWorking setSize:40];
    runOnce = YES;
    
    uacd = [UserPrefConstants singleton].userSelectedAccount;

    
     _errorMsgLabel.text = [LanguageManager stringForKey:Loading______];
    
    for(UIView *v in [self.toolBarView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
            btn.layer.masksToBounds = YES;
        }
    }
    
    for(UIView *v in [self.loadingView subviews])
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.layer.cornerRadius = kButtonRadius*btn.frame.size.height;
            btn.layer.masksToBounds = YES;
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(validateTrade2FAPinForDeviceDone:) name:@"validateTrade2FAPinForDeviceDone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(atpValidate1FADone:) name:@"atpValidate1FADone" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelTrade:) name:@"dismissAllModalViews" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeLimitReceived) name:@"tradeLimitReceived" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(TradeSubmitStatus:) name:@"TradeSubmitStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeReviseNotification:) name:@"tradeReviseNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tradeCancelNotification:) name:@"tradeCancelNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mdPriceTappedNotification:) name:@"mdPriceTappedNotification" object:nil];
    
    atp = [ATPAuthenticate singleton];
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *beView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    beView.tag = 100;
    [self.view insertSubview:beView atIndex:0];

    
    
    popupPickerWidth = 180.0;
    popupPickerHeight = 150.0;
    
    myPickerView =[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, popupPickerWidth, popupPickerHeight)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.tag = 1;
    padLarge = NO;
    
     priceFormatter = [NSNumberFormatter new];
    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter setGroupingSeparator:@","];
    [priceFormatter setGroupingSize:3];
    [priceFormatter setMinimumFractionDigits:[UserPrefConstants singleton].pointerDecimal];
    [priceFormatter setGeneratesDecimalNumbers:YES];

    validityRow=priceRow=ordTypeRow=triggerRow=directionRow=currencyRow=paymentRow=whichQty=triggerPriceRow=0;

    
//    totalAccToShow = [[UserPrefConstants singleton].userAccountlist count];
//    if (totalAccToShow>=10) totalAccToShow = 10; // limit show only 10 in tableview window
    
//    accTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0,300,totalAccToShow*44)];
//    accTableView.dataSource = self;
//    accTableView.delegate = self;
//    accTableView.tag = 3;
    
    acclist =[[UserPrefConstants singleton]userAccountlist];
    
    
    
    
}


-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"validateTrade2FAPinForDeviceDone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"atpValidate1FADone" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissAllModalViews" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"tradeLimitReceived" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"TradeSubmitStatus" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"tradeReviseNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"tradeCancelNotification" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"mdPriceTappedNotification" object:nil];
}



-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _padView.layer.cornerRadius = 8.0;
    _padView.layer.masksToBounds = YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Validify tradeStatus prices

-(void)validifyTradeStatus:(TradeStatus*)tradeStat {
    
      if ([UserPrefConstants singleton].pointerDecimal==3) {
    CGFloat ordPrice = [tradeStat.order_price floatValue];
    tradeStat.order_price = [NSString stringWithFormat:@"%.3f",ordPrice];
    CGFloat trigPrice = [tradeStat.trigger_price floatValue];
    tradeStat.trigger_price = [NSString stringWithFormat:@"%.3f",trigPrice];
      }else{
          CGFloat ordPrice = [tradeStat.order_price floatValue];
          tradeStat.order_price = [NSString stringWithFormat:@"%.4f",ordPrice];
          CGFloat trigPrice = [tradeStat.trigger_price floatValue];
          tradeStat.trigger_price = [NSString stringWithFormat:@"%.4f",trigPrice];
      }
    
}

#pragma mark - OrderPad Fields Logic

//Check Payment List for Current Selected Currency
- (void)currencyAndPaymentChecking {
    
    
    long idx = [_currencyList indexOfObject:btnCurrency.currentTitle];
    
    if ([action isEqualToString:@"BUY"]||[action isEqualToString:@"SELL"]) {
        
        if ( ([_selectorPicker selectedRowInComponent:2] == currencyPlusIndex) ||
            idx == currencyPlusIndex) {
            for (AdditionalPaymentData *payment in trdRules.paymentPlusList) {
                [self.paymentList addObject:payment.payment_name];
            }
        }
        else {
            for (AdditionalPaymentData *payment in trdRules.paymentPlusList) {
                [self.paymentList removeObjectIdenticalTo:payment.payment_name];
            }
        }
    }
    else if ([action isEqualToString:@"REVISE"]||[action isEqualToString:@"CANCEL"]) {
        
        if ( ([_selectorPicker selectedRowInComponent:2] == currencyPlusIndex)  ||
            idx == currencyPlusIndex) {
            for (AdditionalPaymentData *payment in trdRules.revisePaymentPlusParam) {
                [self.paymentList addObject:payment.payment_name];
            }
        }
        else {
            for (AdditionalPaymentData *payment in trdRules.revisePaymentPlusParam) {
                [self.paymentList removeObjectIdenticalTo:payment.payment_name];
            }
        }
        
    }
}

- (void)findAndReplacePlusCurrency {
    
    if ([self.currencyList containsObject:@"+"]) {
        
        currencyPlusIndex = -1;
        
        for (int i = 0; i < [self.currencyList count]; i++) {
            NSString *token = [self.currencyList objectAtIndex:i];
            if ([token isEqualToString:@"+"]) {
                currencyPlusIndex = i;
                
            }
        }
        if (![self.currencyList containsObject:_stockCurrency]&&(_stockCurrency!=nil)) {
            [self.currencyList replaceObjectAtIndex:currencyPlusIndex withObject:_stockCurrency];
            
        }
        else {
            [self.currencyList removeObjectAtIndex:currencyPlusIndex];
        }
    }
    [_selectorPicker reloadComponent:2];
}



- (void)doOrderCtrl:(BOOL)forRevise {
    
    //Get OrderControl or ReviseControl object from dictionary by current selected order type
    OrderControl *oc = nil;
    
   // NSLog(@"2 self.orderTypeList %@",self.orderTypeList);
    
    NSString *ordType = [self.orderTypeList objectAtIndex:ordTypeRow];
    
    NSLog(@"ordType %@", ordType);
    
    if (!forRevise) {
        oc = [trdRules.ordCtrlConditions objectForKey:ordType];
    }
    else {
        oc = [trdRules.reviseCtrlConditions objectForKey:ordType];
    }
    
    if (oc) {
        if (!forRevise) {
            //Validity List that are allowed to select
            if ([_orderValidityList count] > 0) {
                [_orderValidityList removeAllObjects];
            }
            [_orderValidityList addObjectsFromArray:oc.validityList];
            [_selectorPicker reloadComponent:1];
            
        }
        
        //Disable all Text Fields by default
        [self disableAllFields];
        
        //Enable Text Fields that are allowed to key in
        if ([oc.enable_fields containsObject:@"Price"]) {
            [self setFieldEnable:YES  forView:_priceView];
        }
        
        if ([oc.enable_fields containsObject:@"Qty"]) {
            [self setFieldEnable:YES  forView:_qtyView];
        }
        
        if ([oc.enable_fields containsObject:@"StopLimit"]) {
            [self setFieldEnable:YES  forView:_trigPriceView];
        }
        
        NSString *currentValidity = [_orderValidityList objectAtIndex:validityRow];
        
       // NSLog(@"currentValidity %@", currentValidity);
        
       // NSLog(@"oc.enable_fields %@",oc.enable_fields);
        
        if ([oc.enable_fields containsObject:@"Min"]) {
            if ([oc.min_validityList count] > 0) {
                if ([oc.min_validityList containsObject:currentValidity]) {
                    [self setFieldEnable:YES  forView:_minQtyView];
                }
            }
            else {
                [self setFieldEnable:YES  forView:_minQtyView];
            }
        }
        
        if ([oc.enable_fields containsObject:@"Disclosed"]) {
            if ([oc.disclosed_validityList count] > 0) {
                if ([oc.disclosed_validityList containsObject:currentValidity]) {
                    [self setFieldEnable:YES  forView:_discloseView];
                }
            }
            else {
                [self setFieldEnable:YES  forView:_discloseView];
            }
        }
        
        if ([oc.enable_fields containsObject:@"TriggerPriceType"]) {
            [self setFieldEnable:YES  forView:_triggerVIew];
            // and set to first item
            _triggerType.text = [trdRules.triggerPriceTypeList objectAtIndex:0];
        }
        
        
        if ([oc.enable_fields containsObject:@"TriggerPriceDirection"]) {
            [self setFieldEnable:YES  forView:_directionView];
            _triggerDirection.text = [trdRules.triggerPriceDirectionList objectAtIndex:0];
        }
        
        //if (!forRevise) {
        if ([currentValidity isEqualToString:@"GTD"]) {
            [self setFieldEnable:YES  forView:_dateView];
        }
        //}
    }
}


- (void)doReviseCtrlForPicker {
    
    //Parameters that might be provided in "[Revise]="
    GTMRegex *regOrderType = [GTMRegex regexWithPattern:@"O"];              //	O - Order Type
    GTMRegex *regValidity = [GTMRegex regexWithPattern:@"V"];               //	V - Validity
    GTMRegex *regCurrency = [GTMRegex regexWithPattern:@"T"];               //  T - Settlement Currency
    GTMRegex *regPayment = [GTMRegex regexWithPattern:@"Y"];                //  Y - Payment
    
     reviseOrdType = NO;
     reviseValidity = NO;
     reviseCurrency = NO;
     revisePayment = NO;
    
    TradeStatus *ts;
    if ([action isEqualToString:@"REVISE"]) {
        ts = tradeStatusRevise;
    } else if ([action isEqualToString:@"CANCEL"]) {
        ts = tradeStatusCancel;
    }
    
    for (NSString *param in trdRules.reviseParam) {
        if ([regOrderType matchesString:param]) {
            if ([trdRules.reviseOrdTypeParam containsObject:ts.order_type]) {
                reviseOrdType = YES;
            }
        }
        else if ([regValidity matchesString:param]) {
            if ([trdRules.reviseValidityParam containsObject:ts.validity]) {
                reviseValidity = YES;
            }
        }
        else if ([regCurrency matchesString:param]) {
            if ([trdRules.reviseCurrencyParam containsObject:ts.sett_currency]) {
                reviseCurrency = YES;
            }
        }
        else if ([regPayment matchesString:param]) {
            if ([trdRules.revisePaymentParam containsObject:ts.payment_type]) {
                revisePayment = YES;
            }
        }
    }
    
    //Order Type List that are allowed to revise
    if ([_orderTypeList count] > 0) {
        [_orderTypeList removeAllObjects];
    }
    if (reviseOrdType == NO) {
        [_orderTypeList addObject:ts.order_type];
    }
    else {
        [_orderTypeList addObjectsFromArray:trdRules.reviseOrdTypeParam];
    }
    //Validity List that are allowed to revise
    if ([_orderValidityList count] > 0) {
        [_orderValidityList removeAllObjects];
    }
    if (reviseValidity == NO) {
        [_orderValidityList addObject:ts.validity];
    }
    else {
        [_orderValidityList addObjectsFromArray:trdRules.reviseValidityParam];
    }
    //Currency List that are allowed to revise
    if ([_currencyList count] > 0) {
        [_currencyList removeAllObjects];
    }
    if (reviseCurrency == NO) {
        [_currencyList addObject:ts.sett_currency];
    }
    else {
        [_currencyList addObjectsFromArray:trdRules.reviseCurrencyParam];
    }
    //Payment List that are allowed to revise
    if ([_paymentList count] > 0) {
        [_paymentList removeAllObjects];
    }
    if (revisePayment == NO) {
        [_paymentList addObject:ts.payment_type];
    }
    else {
        [_paymentList addObjectsFromArray:trdRules.revisePaymentParam];
    }
    
    //Initialized Order Type, Validity, Currency and Payment selection in picker
    [_selectorPicker reloadAllComponents];
    [_selectorPicker selectRow:[_orderTypeList indexOfObject:ts.order_type] inComponent:0 animated:YES];
    [_selectorPicker selectRow:[_orderValidityList indexOfObject:ts.validity] inComponent:1 animated:YES];
    [_selectorPicker selectRow:[_currencyList indexOfObject:ts.sett_currency] inComponent:2 animated:YES];
    [_selectorPicker selectRow:[_paymentList indexOfObject:ts.payment_type] inComponent:3 animated:YES];
    
    //SmallSize
    [btnOrderType setTitle:ts.order_type forState:UIControlStateNormal];
    [btnValidity setTitle:ts.validity forState:UIControlStateNormal];
    [btnCurrency setTitle:ts.sett_currency forState:UIControlStateNormal];
    [btnPayment setTitle:ts.payment_type forState:UIControlStateNormal];
}

//Check Revise Rules for Revision Order
- (void)doReviseCtrlForTextfield {
    
    [self doOrderCtrl:YES];
    
    //Parameters that might be provided in "[Revise]="
    GTMRegex *regPrice = [GTMRegex regexWithPattern:@"P.*"];                //  P - Price +/-
    GTMRegex *regQty = [GTMRegex regexWithPattern:@"Q.*"];                  //  Q - Qty   +/-
    GTMRegex *regLimit = [GTMRegex regexWithPattern:@"L.*"];                //	L - Limit +/-
    GTMRegex *regMinQty = [GTMRegex regexWithPattern:@"M.*"];               //  M - Minimum Qty +/-
    GTMRegex *regDisclosedQty = [GTMRegex regexWithPattern:@"D.*"];         //  D - Disclosed Qty +/-
    GTMRegex *regTriggerType = [GTMRegex regexWithPattern:@"TT.*"];         //  TT - Trigger Type      Note:To be confirmed
    GTMRegex *regTriggerDir = [GTMRegex regexWithPattern:@"TD.*"];          //  TD - Trigger Direction Note:To be confirmed
    GTMRegex *regAdd = [GTMRegex regexWithPattern:@".*\\+.*"];
    GTMRegex *regMinus = [GTMRegex regexWithPattern:@".*\\-.*"];
    
    revisePrice = NO;
    priceAdd = NO;
    priceMinus = NO;
    reviseLimit = NO;
    limitAdd = NO;
    limitMinus = NO;
    reviseQty = NO;
    qtyAdd = NO;
    qtyMinus = NO;
    reviseMinQty = NO;
    minQtyAdd = NO;
    minQtyMinus = NO;
    reviseDisclosedQty = NO;
    disclosedQtyAdd = NO;
    disclosedQtyMinus = NO;
    reviseTriggerType = NO;
    reviseTriggerDirection = NO;

    
    //Enable Text Fields that are allowed to revise
    for (NSString *param in trdRules.reviseParam) {
        if ([regPrice matchesString:param]) {
            revisePrice = YES;
            if ([regAdd matchesString:param]) {
                priceAdd = YES;
            }
            if ([regMinus matchesString:param]) {
                priceMinus = YES;
            }
            if (![regAdd matchesString:param] && ![regMinus matchesString:param]) {
                priceAdd = YES;
                priceMinus = YES;
            }
        }
        else if ([regQty matchesString:param]) {
            reviseQty = YES;
            if ([regAdd matchesString:param]) {
                qtyAdd = YES;
            }
            if ([regMinus matchesString:param]) {
                qtyMinus = YES;
            }
            if (![regAdd matchesString:param] && ![regMinus matchesString:param]) {
                qtyAdd = YES;
                qtyMinus = YES;
            }
        }
        else if ([regLimit matchesString:param]) {
            reviseLimit = YES;
            if ([regAdd matchesString:param]) {
                limitAdd = YES;
            }
            if ([regMinus matchesString:param]) {
                limitMinus = YES;
            }
            if (![regAdd matchesString:param] && ![regMinus matchesString:param]) {
                limitAdd = YES;
                limitMinus = YES;
            }
        }
        else if ([regMinQty matchesString:param]) {
            reviseMinQty = YES;
            if ([regAdd matchesString:param]) {
                minQtyAdd = YES;
            }
            if ([regMinus matchesString:param]) {
                minQtyMinus = YES;
            }
            if (![regAdd matchesString:param] && ![regMinus matchesString:param]) {
                minQtyAdd = YES;
                minQtyMinus = YES;
            }
        }
        else if ([regDisclosedQty matchesString:param]) {
            reviseDisclosedQty = YES;
            if ([regAdd matchesString:param]) {
                disclosedQtyAdd = YES;
            }
            if ([regMinus matchesString:param]) {
                disclosedQtyMinus = YES;
            }
            if (![regAdd matchesString:param] && ![regMinus matchesString:param]) {
                disclosedQtyAdd = YES;
                disclosedQtyMinus = YES;
            }
        }
        else if ([regTriggerType matchesString:param]) {
            reviseTriggerType = YES;
        }
        else if ([regTriggerDir matchesString:param]) {
            reviseTriggerDirection = YES;
        }
    }
    
    if ([_priceView isUserInteractionEnabled] && !revisePrice) {
        [self setFieldEnable:NO forView:_priceView];
    }
    if ([_trigPriceView isUserInteractionEnabled] && !reviseLimit) {
        [self setFieldEnable:NO forView:_trigPriceView];
    }
    if ([_qtyView isUserInteractionEnabled] && !reviseQty) {
        [self setFieldEnable:NO forView:_qtyView];
    }
    if ([_minQtyView isUserInteractionEnabled] && !reviseMinQty) {
        [self setFieldEnable:NO forView:_minQtyView];
    }
    if ([_discloseView isUserInteractionEnabled] && !reviseDisclosedQty) {
        [self setFieldEnable:NO forView:_discloseView];
    }
    if ([_triggerVIew isUserInteractionEnabled] && !reviseTriggerType) {
        [self setFieldEnable:NO forView:_triggerVIew];
    }
    if ([_directionView isUserInteractionEnabled] && !reviseTriggerDirection) {
        [self setFieldEnable:NO forView:_directionView];
    }
    if ([_dateView isUserInteractionEnabled] && !reviseValidity) {
        [self setFieldEnable:NO forView:_dateView];
    }
}


#pragma mark - Order Pad Setup Here -------------



-(void) updateOrderPad {
    
    //NSLog(@"UpdatedOrderPad");
    
    UIColor *themeColor = [UIColor darkGrayColor];
    // NSLog(@"_stockCurrency %@",_stockCurrency);
    
    NSArray *stkArr = [[UserPrefConstants singleton].userSelectingStockCode componentsSeparatedByString:@"."];
    self.stkExchange = stkArr[1];
    
    self.paymentList = [[NSMutableArray alloc] init];
    self.currencyList = [[NSMutableArray alloc] init];
    
    
    
//    if ([stockData.instrument rangeOfString:@"|"].location != NSNotFound) {
//        allowNegativePrice = YES;
//    }
//    else {
//        allowNegativePrice = NO;
//    }
    
    long qtyFactor = 1;
    
    NSString *qtyStr = [LanguageManager stringForKey:@"Qty"];
    
    if ([UserPrefConstants singleton].quantityIsLot) {
        _titleQty.text =[NSString stringWithFormat:@"%@ x%ld",qtyStr,_stockLotSize];
        qtyFactor = _stockLotSize;
    } else {
        _titleQty.text = [NSString stringWithFormat:@"%@ x1",qtyStr];
        qtyFactor = 1;
    }
    
    CGFloat dur = [_accountName.text length]/10.0;
    if (_accLabel==nil)
        _accLabel=[[MarqueeLabel alloc]initWithFrame:_accountName.frame duration:dur andFadeLength:5.0f];
    [_accLabel setScrollDuration:dur];
    [_headerBg addSubview:_accLabel];
    [_accLabel setFont:_accountName.font];
    _accLabel.text = _accountName.text;
    [_accountName setHidden:YES];
    [_accLabel setTextColor:[UIColor whiteColor]];
    


    
    NSString *serverOutputName =  [[UserPrefConstants singleton].userSelectedAccount client_name];
    
   
    NSString *stripClientName = [[UserPrefConstants singleton] stringsBetweenString:@"(" andString:@")" fromString:serverOutputName];
    
    NSLog(@"stripClientName %@",stripClientName);

    _accLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",
                          [[UserPrefConstants singleton].userSelectedAccount client_account_number],
                         stripClientName,
                          [[UserPrefConstants singleton].userSelectedAccount broker_branch_code]];
    
    
   
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *dateStr=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:[NSDate date]]];
    
    trdRules = [_actionTypeDict objectForKey:@"TradeRules"];
    tradeStatusRevise =[[TradeStatus alloc]init];
    tradeStatusRevise = [_actionTypeDict objectForKey:@"TradeStatus"];
    
    if ([[_actionTypeDict objectForKey:@"Action"] isEqualToString:@"BUY"]) {
        
        action = @"BUY";
        actualAction = @"BUY";
        
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [_proceedButton setTitle:@"Submit" forState:UIControlStateNormal];
        
        themeColor = [UIColor colorWithRed:0.11 green:0.21 blue:0.02 alpha:1.00];
        [_headerTitle setTitleColor:[UIColor colorWithRed:0.61 green:0.71 blue:0.52 alpha:1.00] forState:UIControlStateNormal];
        
        _quantity.text = @"";
        _lblDate.text = dateStr;
        _stkPrice.text = [_actionTypeDict objectForKey:@"StockPrice"];
        [self setFieldEnable:NO forView:_dateView];
        [_orderNumLabel setHidden:YES];
        
        NSString *ordType = [trdRules.orderTypeList objectAtIndex:0];
        
        NSString *coloredLblStr = [NSString stringWithFormat:@"%@ (%@) \n%@",
                                   [LanguageManager stringForKey:action],ordType,[_actionTypeDict objectForKey:@"stkNname"]];
        [_stkNname setText:coloredLblStr];
        
        
    } else if ([[_actionTypeDict objectForKey:@"Action"] isEqualToString:@"SELL"]) {
    
        action = @"SELL";
        actualAction = @"SELL";
        
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [_proceedButton setTitle:@"Submit" forState:UIControlStateNormal];
        
        themeColor = [UIColor colorWithRed:0.28 green:0.04 blue:0.05 alpha:1.00];
        [_headerTitle setTitleColor:[UIColor colorWithRed:0.78 green:0.54 blue:0.55 alpha:1.00] forState:UIControlStateNormal];

        _quantity.text = @"";
        _lblDate.text = dateStr;
        _stkPrice.text = [_actionTypeDict objectForKey:@"StockPrice"];
        [self setFieldEnable:NO forView:_dateView];
        [_orderNumLabel setHidden:YES];
        
        NSString *ordType = [trdRules.orderTypeList objectAtIndex:0];
        NSString *coloredLblStr = [NSString stringWithFormat:@"%@ (%@) \n%@",[LanguageManager stringForKey:action], ordType,[_actionTypeDict objectForKey:@"stkNname"]];
        [_stkNname setText:coloredLblStr];
        
        
    } else  if ([[_actionTypeDict objectForKey:@"Action"] isEqualToString:@"REVISE"]) {
        
        action = @"REVISE";
        
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [_proceedButton setTitle:@"Submit" forState:UIControlStateNormal];
        
        themeColor = [UIColor colorWithRed:0.00 green:0.38 blue:0.51 alpha:1.00];
        [_headerTitle setTitleColor:[UIColor colorWithRed:0.50 green:0.88 blue:1.00 alpha:1.00] forState:UIControlStateNormal];
        [_headerTitle setTitle:[LanguageManager stringForKey:action] forState:UIControlStateNormal];
        
        // Price/Qty/Date/Min Qty/ Disc Qty
              if ([UserPrefConstants singleton].pointerDecimal==3) {
        _stkPrice.text = [NSString stringWithFormat:@"%.3f",[tradeStatusRevise.order_price floatValue]];
              }else{
                  _stkPrice.text = [NSString stringWithFormat:@"%.4f",[tradeStatusRevise.order_price floatValue]];
              }
        
        // check qty and qtyFactor
        long long ordQty = [tradeStatusRevise.unmt_quantity longLongValue];
        
        
        if (ordQty<qtyFactor) {
            
            _quantity.text = [NSString stringWithFormat:@"%.1f",(double)ordQty/qtyFactor];
        } else {
            
            _quantity.text = [NSString stringWithFormat:@"%lld",ordQty/qtyFactor];
        }
        

        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
        [ndf setDateFormat:@"yyyyMMddHHmmss.SSS"];
        NSDate *date = [ndf dateFromString:tradeStatusRevise.expiry];
        [ndf setDateFormat:@"dd/MM/yyyy"];
        _lblDate.text = [ndf stringFromDate:date];
        
        // 20170718153000.000
       // NSLog(@"tradeStatusRevise.expiry %@ %@",tradeStatusRevise.expiry, _lblDate.text);
        
        
        _minquantity.text = [NSString stringWithFormat:@"%ld",[tradeStatusRevise.min_quantity intValue]/qtyFactor];
        _discloseQty.text = [NSString stringWithFormat:@"%ld",[tradeStatusRevise.disclosed_quantity intValue]/qtyFactor];
        
        // TRIGGERS
             if ([UserPrefConstants singleton].pointerDecimal==3) {
        _triggerPrice.text = [NSString stringWithFormat:@"%.3f",[tradeStatusRevise.trigger_price floatValue]];
             }else{
                  _triggerPrice.text = [NSString stringWithFormat:@"%.4f",[tradeStatusRevise.trigger_price floatValue]];
             }
        _triggerType.text = tradeStatusRevise.triggerType;
        _triggerDirection.text = tradeStatusRevise.triggerDirection;
        
        
        [_selectorPicker reloadAllComponents];
        
        // show order number
        
        _orderNumLabel.text = [LanguageManager stringForKey:@"OrdNo: %@" withPlaceholders:@{@"%ordNo%":tradeStatusRevise.order_number}];
        [_orderNumLabel setHidden:NO];
        actualAction = [tradeStatusRevise.action uppercaseString]; // revising BUY/SELL
        
        NSString *ordType = tradeStatusRevise.order_type;
        NSString *coloredLblStr = [NSString stringWithFormat:@"%@ (%@) \n%@",[LanguageManager stringForKey:tradeStatusRevise.action],ordType,[_actionTypeDict objectForKey:@"stkNname"]];
        [_stkNname setText:coloredLblStr];
        
        
    } else  if ([[_actionTypeDict objectForKey:@"Action"] isEqualToString:@"CANCEL"]) {
        
        action = @"CANCEL";
        
        [_cancelButton setTitle:@"Close" forState:UIControlStateNormal];
        [_proceedButton setTitle:@"Proceed" forState:UIControlStateNormal];
        
        themeColor = [UIColor colorWithRed:0.59 green:0.21 blue:0.03 alpha:1.00];
        [_headerTitle setTitleColor:[UIColor colorWithRed:0.99 green:0.61 blue:0.43 alpha:1.00] forState:UIControlStateNormal];
        
        tradeStatusCancel =[[TradeStatus alloc]init];
        tradeStatusCancel = [_actionTypeDict objectForKey:@"TradeStatus"];
        
        // Price/Qty/Date/Min Qty/ Disc Qty
            if ([UserPrefConstants singleton].pointerDecimal==3) {
        _stkPrice.text = [NSString stringWithFormat:@"%.3f",[tradeStatusCancel.order_price floatValue]];
            }else{
                _stkPrice.text = [NSString stringWithFormat:@"%.4f",[tradeStatusCancel.order_price floatValue]];

            }
        
        // check qty and qtyFactor
        long long ordQty = [tradeStatusCancel.unmt_quantity longLongValue];
        if (ordQty<qtyFactor) {
            
            
            _quantity.text = [NSString stringWithFormat:@"%.1f",(double)ordQty/qtyFactor];
        } else {
            
            _quantity.text = [NSString stringWithFormat:@"%lld",ordQty/qtyFactor];
        }
        

        
        NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
        [ndf setDateFormat:@"yyyyMMddHHmmss.SSS"];
        NSDate *date = [ndf dateFromString:tradeStatusCancel.expiry];
        [ndf setDateFormat:@"dd/MM/yyyy"];
        _lblDate.text = [ndf stringFromDate:date];
        
        
        _minquantity.text = [NSString stringWithFormat:@"%ld",[tradeStatusCancel.min_quantity intValue]/qtyFactor];
        _discloseQty.text = [NSString stringWithFormat:@"%ld",[tradeStatusCancel.disclosed_quantity intValue]/qtyFactor];

        
        // TRIGGERS
        if ([UserPrefConstants singleton].pointerDecimal==3) {
        _triggerPrice.text = [NSString stringWithFormat:@"%.3f",[tradeStatusCancel.trigger_price floatValue]];
        }else{
                _triggerPrice.text = [NSString stringWithFormat:@"%.4f",[tradeStatusCancel.trigger_price floatValue]];
        }
        _triggerType.text = tradeStatusCancel.triggerType;
        _triggerDirection.text = tradeStatusCancel.triggerDirection;
        
        
        trdRules = [_actionTypeDict objectForKey:@"TradeRules"];
        [_selectorPicker reloadAllComponents];
        
        

        
        // show order number
        _orderNumLabel.text = [LanguageManager stringForKey:@"OrdNo: %@" withPlaceholders:@{@"%ordNo%":tradeStatusCancel.order_number}];
        [_orderNumLabel setHidden:NO];
        
        NSString *ordType = tradeStatusCancel.order_type;
        NSString *coloredLblStr = [NSString stringWithFormat:@"%@ (%@) \n%@",[LanguageManager stringForKey:tradeStatusCancel.action],
                                   ordType,[_actionTypeDict objectForKey:@"stkNname"]];
        [_stkNname setText:coloredLblStr];
        
        
    }
    
    [_headerTitle setTitle:[LanguageManager stringForKey:action] forState:UIControlStateNormal];
    
    NSLog(@"[self ShortSellEnable] %d, action %@",[self ShortSellEnable],action);
    
    if (![self ShortSellEnable] || ([action isEqualToString:@"CANCEL"]||[action isEqualToString:@"REVISE"]) == 1 || [action isEqualToString:@"BUY"]) {
        [_shortSellBtn setHidden:YES];
    }else{
         [_shortSellBtn setHidden:NO];
    }
    
    [_headerBg setBackgroundColor:themeColor];
    [_stkNname setTextColor:themeColor];
    
    for(UIView *v in [self.padView subviews])
    {
        if([v isKindOfClass:[UIView class]])
        {
            if (v.tag==100) {
                UIView *tmp = (UIView*)v;
                [tmp setBackgroundColor:themeColor];
            }
        }
    }
    
    self.orderTypeList = [[NSMutableArray alloc] initWithArray:trdRules.orderTypeList];
    self.orderValidityList = [[NSMutableArray alloc] initWithArray:trdRules.validityList];
    
    // NSLog(@"_orderTypeList %@", _orderTypeList);
    
    
    // CHECK FOR CUT PAYMENT CONFIGS
    NSString *stkCode = [UserPrefConstants singleton].userSelectingStockCode;
    NSRange range = [stkCode rangeOfString:@"."];
    NSString *stockCountry = [[stkCode substringFromIndex:NSMaxRange(range)]
                              stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // NSLog(@"stockCountry : %@", stockCountry);
    
    // override only if data "PaymentCfg_Payment" available and feed exchange is SG
    if (([trdRules.paymentCfgList count]>0)&&([self.stkExchange isEqualToString:@"SG"])) {
        
        long index = -1;
        
        for(int i=0;i<[trdRules.paymentCfgList count];i++)
        {
            PaymentCUT *cut = [trdRules.paymentCfgList objectAtIndex:i];
            if(([stockCountry isEqualToString:cut.paymentCountry]) &&([uacd.account_type  isEqualToString:cut.AccountType]))
            {
                index=i;
                self.paymentList =[[NSMutableArray alloc]initWithArray:[cut paymentOptions]];
                self.currencyList =[[NSMutableArray alloc]initWithObjects:[cut paymentSettings], nil];
                
            }
        }
        
        // if not exist for account types above, then add default Payment
        if (index==-1) {
            for(int i=0;i<[trdRules.paymentCfgList count];i++)
            {
                PaymentCUT *cut = [trdRules.paymentCfgList objectAtIndex:i];
                if(([stockCountry isEqualToString:[cut paymentCountry]]) && ([@"Def" isEqual:[cut AccountType]]) )
                {
                    index=i;
                    PaymentCUT *cut2 = [trdRules.paymentCfgList objectAtIndex:index];
                    self.paymentList=[[NSMutableArray alloc]initWithArray:[cut2 paymentOptions]];
                    self.currencyList =[[NSMutableArray alloc]initWithArray:trdRules.currencyList];
                    break;
                }
            }
        }
        
        
    } else {
        self.paymentList=[[NSMutableArray alloc]initWithArray:trdRules.paymentList];
        self.currencyList =[[NSMutableArray alloc]initWithArray:trdRules.currencyList];
    }
    
    
    if ([self.currencyList count] == 0 && _stockCurrency != nil) {
        [self.currencyList addObject:_stockCurrency];
    }
    
    
    // Apply trading rules
    if ([action isEqualToString:@"BUY"]||[action isEqualToString:@"SELL"]) {
        
        [self doOrderCtrl:NO];
        [_selectorPicker setUserInteractionEnabled:YES];
        
    } else if ([action isEqualToString:@"REVISE"]) {
        
        [self doReviseCtrlForPicker];
        [self doReviseCtrlForTextfield];
        [_selectorPicker setUserInteractionEnabled:YES];
        
    } else if ([action isEqualToString:@"CANCEL"]) {
        
        [self doReviseCtrlForPicker];
        [self doReviseCtrlForTextfield];
        
       
    }
    
    // others
    ordTypeRow = 0;
    paymentRow = 0;
    validityRow = 0;
    currencyRow = 0;
    
    if (_orderTypeList.count > 0) {
        [btnOrderType setTitle:_orderTypeList[ordTypeRow] forState:UIControlStateNormal];
    }
    if (_orderValidityList.count > 0) {
        [btnValidity setTitle:_orderValidityList[validityRow] forState:UIControlStateNormal];
    }
    if (_currencyList.count > 0) {
        [btnCurrency setTitle:_currencyList[currencyRow] forState:UIControlStateNormal];
    }
    if (_paymentList.count > 0) {
        [btnPayment setTitle:_paymentList[paymentRow] forState:UIControlStateNormal];
    }else{
        [btnPayment setTitle:@"" forState:UIControlStateNormal];
    }

    
    [self findAndReplacePlusCurrency];
    [self currencyAndPaymentChecking];
    if (([self.stkExchange isEqual:@"HKD"]||[self.stkExchange isEqual:@"HK"])&&[self.currencyList containsObject:trdRules.currencyNotSupported]) {
        [self.currencyList removeObjectAtIndex:[self.currencyList indexOfObject:trdRules.currencyNotSupported]];
    }
    
    
    [_selectorPicker reloadAllComponents];
    [self calculatePrice];
    
//    ordTypeRow = [_selectorPicker selectedRowInComponent:0];
//    validityRow = [_selectorPicker selectedRowInComponent:1];
//    currencyRow = [_selectorPicker selectedRowInComponent:2];
//    paymentRow = [_selectorPicker selectedRowInComponent:3];
    
    if ([action isEqualToString:@"REVISE"]) {
        // select the order options
        
        // UPDATE ORDER TYPE
        if ([self.orderTypeList containsObject:tradeStatusRevise.order_type]) {
            [btnOrderType setTitle:tradeStatusRevise.order_type forState:UIControlStateNormal];

            
            long idx = [self.orderTypeList indexOfObject:tradeStatusRevise.order_type];
            [_selectorPicker selectRow:idx inComponent:0 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:0]; // update current ordertype

        }
        
        // UPDATE VALIDITY
        if ([self.orderValidityList containsObject:tradeStatusRevise.validity]) {
            [btnValidity setTitle:tradeStatusRevise.validity forState:UIControlStateNormal];

            
            long idx = [self.orderValidityList indexOfObject:tradeStatusRevise.validity];
            [_selectorPicker selectRow:idx inComponent:1 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:1]; // update current validity

        }
        
        // UPDATE CURRENCY
        if ([self.currencyList containsObject:tradeStatusRevise.sett_currency]) {
            [btnCurrency setTitle:tradeStatusRevise.sett_currency forState:UIControlStateNormal];

            
            long idx = [self.currencyList indexOfObject:tradeStatusRevise.sett_currency];
            [_selectorPicker selectRow:idx inComponent:2 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:2]; // update current currency
        }
        
        // UPDATE PAYMENT
        if ([self.paymentList containsObject:tradeStatusRevise.payment_type]) {
            [btnPayment setTitle:tradeStatusRevise.payment_type forState:UIControlStateNormal];

            
            long idx = [self.paymentList indexOfObject:tradeStatusRevise.payment_type];
            [_selectorPicker selectRow:idx inComponent:3 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:3]; // update current currency

        }
        
    } else if ([action isEqualToString:@"CANCEL"]) {
        // select the order options
        
        // UPDATE ORDER TYPE
        if ([self.orderTypeList containsObject:tradeStatusCancel.order_type]) {
            [btnOrderType setTitle:tradeStatusCancel.order_type forState:UIControlStateNormal];

            
            long idx = [self.orderTypeList indexOfObject:tradeStatusCancel.order_type];
            [_selectorPicker selectRow:idx inComponent:0 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:0]; // update current ordertype
        }
        
        // UPDATE VALIDITY
        if ([self.orderValidityList containsObject:tradeStatusCancel.validity]) {
            [btnValidity setTitle:tradeStatusCancel.validity forState:UIControlStateNormal];

            
            long idx = [self.orderValidityList indexOfObject:tradeStatusCancel.validity];
            [_selectorPicker selectRow:idx inComponent:1 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:1]; // update current validity
        }
        
        // UPDATE CURRENCY
        if ([self.currencyList containsObject:tradeStatusCancel.sett_currency]) {
            [btnCurrency setTitle:tradeStatusCancel.sett_currency forState:UIControlStateNormal];

            
            long idx = [self.currencyList indexOfObject:tradeStatusCancel.sett_currency];
            [_selectorPicker selectRow:idx inComponent:2 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:2]; // update current currency
        }
        
        // UPDATE PAYMENT
        if ([self.paymentList containsObject:tradeStatusCancel.payment_type]) {
            [btnPayment setTitle:tradeStatusCancel.payment_type forState:UIControlStateNormal];

            
            long idx = [self.paymentList indexOfObject:tradeStatusCancel.payment_type];
            [_selectorPicker selectRow:idx inComponent:3 animated:NO];
            [self pickerView:_selectorPicker didSelectRow:idx inComponent:3]; // update current currency
        }
        
        
        // DISABLE EDITING FOR ALL "ENABLED" FIELDS
        [self disableEnabledFields];
        // DISABLE ALSO SELECTOR PICKERVIEW
        [_selectorPicker setUserInteractionEnabled:NO];
    }
    
    
}


-(BOOL)ShortSellEnable {
    
    BOOL result = NO;
    NSString *tradeExchgCode = _currentED.trade_exchange_code;
    result = [[[UserPrefConstants singleton].TrdShortSellIndicator objectForKey:tradeExchgCode] boolValue];
    return result;
}

#pragma mark - Gesture methods

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void)orderPadDidPanned:(UIPanGestureRecognizer*)recognizer {
    
    UIView *view = recognizer.view;
    
    // CGPoint velocity = [recognizer velocityInView:view.superview];
    
    // if (velocity.x>0) {
    
    CGPoint translation = [recognizer translationInView:view.superview];
    
    if (translation.x>=80) {
        bufferPadX = translation.x-79;
    } else {
        bufferPadX = 0;
    }
    
    if (bufferPadX>0) {
        
        CGFloat distanceToDismiss = 250;
        
        if(recognizer.state == UIGestureRecognizerStateBegan){
            
            
            
        }else if(recognizer.state == UIGestureRecognizerStateEnded){
            
            if (_padView.center.x<=padOriPos.x+distanceToDismiss) {
                // return back
                [UIView animateWithDuration:0.2 animations:^{
                    _padView.center = padOriPos;
                }completion:^(BOOL finished) {
                    
                }];
            }
        } else {
            // swping
            
            _padView.center = CGPointMake(padOriPos.x+bufferPadX, padOriPos.y);
            
            if (_padView.center.x>padOriPos.x+distanceToDismiss) {
                // DISMISS
                [_padView removeGestureRecognizer:recognizer];
                [self cancelTrade:nil];
            }
            
        }
        
    } else {
        
        [UIView animateWithDuration:0.2 animations:^{
            _padView.center = padOriPos;
        }completion:^(BOOL finished) {
            
        }];
        
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)closeLoading:(id)sender {
    [atp timeoutActivity];
    [self clearLoadingView];
}

- (IBAction)cancelTrade:(id)sender {
    [atp timeoutActivity];
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
//            [_padView setCenter:CGPointMake(self.view.frame.size.width*2, _padView.center.y)];
            
        } completion:^(BOOL finished) {
            [self.delegate traderDidCancelledNormal];
            
        }];
}

-(void)calculatePrice
{
    long qtyFactor = 1;
    
    if ([UserPrefConstants singleton].quantityIsLot) {
        qtyFactor = _stockLotSize;
    } else {
        qtyFactor = 1;
    }
    
    NSString *settcurrency;
    
    if ([self.currencyList count]>currencyRow+1) {
       settcurrency = [self.currencyList objectAtIndex:currencyRow];
    }
    
    if (settcurrency==nil) settcurrency = [UserPrefConstants singleton].defaultCurrency;
    
    double stkPrice = _stkPrice.text.doubleValue;
    
    if ([_stockCurrency length]<=0) _stockCurrency = [UserPrefConstants singleton].defaultCurrency;
    
    CurrencyRate *cr = [[UserPrefConstants singleton].currencyRateDict objectForKey:_stockCurrency];
    
    
    // convert price if settcurrency is not equal to stockcurrency
    if (![settcurrency isEqualToString:_stockCurrency]) {
        
        stkPrice = cr.buy_rate * stkPrice; // use buy rate or sell rate???? idk man too many rules
    }
    
    
    NSLog(@"%.f %@", cr.buy_rate, _stockCurrency);
    
    long long convertedQty = [_quantity.text longLongValue]*qtyFactor;
    NSString *qtyText = [NSString stringWithFormat:@"%lld",convertedQty];
    
    _unitsnprice.text=  [LanguageManager stringForKey:@"%@ unit(s) @ %@" withPlaceholders:@{@"%stkQty%":qtyText,
                                                                                            @"%stkPrice%":_stkPrice.text}];
    
    NSString *total = @"";
       if ([UserPrefConstants singleton].pointerDecimal==3) {
           total = [NSString stringWithFormat:@"%.3f",_quantity.text.intValue*qtyFactor*stkPrice];
       }else{
           total = [NSString stringWithFormat:@"%.4f",_quantity.text.intValue*qtyFactor*stkPrice];

       }
    
    _totalValue.text =  [LanguageManager stringForKey:@"Value = %@ %@" withPlaceholders:@{@"%curr%":settcurrency,
                                                                                           @"%total%":total}];
    NSString *paytype;
    if ([self.paymentList count]>=paymentRow+1)
        paytype = [self.paymentList objectAtIndex:paymentRow];
    
    if ([paytype isEqualToString:@"CUT"]) {
        _tradeLimitTitle.text = [LanguageManager stringForKey:@"Investment Power:"];
        self.resolvedLimit = uacd.credit_limit;
        
        NSString *selectedCurr;
        if ([self.currencyList count]>=currencyRow+1)
            selectedCurr = [self.currencyList objectAtIndex:currencyRow];
        
        _tradeLimit.text = [NSString stringWithFormat:@"%@ %@",selectedCurr,
                            [priceFormatter stringFromNumber:[NSNumber numberWithDouble:self.resolvedLimit]]];
    }
}



#pragma mark - Pickerview

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    if (pickerView == _selectorPickerSmallSize) {
        return 1;
    }else{
        if (pickerView.tag==20) {
            return 4;
        } else {
            return 1;
        }
    }
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    
    
    return  30;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _selectorPickerSmallSize) {
        switch (iCurrentFocusPicker) {
            case 10:
                return [self.orderTypeList count];
                break;
            case 11:
                return [self.orderValidityList count];
                break;
            case 12:
                return [self.currencyList count];
                break;
            case 13:
                return [self.paymentList count];
                break;
        }
    }

    if (pickerView.tag==20) {
            switch (component) {
                case 0:
                    return [self.orderTypeList count];
                    break;
                case 1:
                    return [self.orderValidityList count];
                    break;
                case 2:
                    return [self.currencyList count];
                    break;
                case 3:
                    return [self.paymentList count];
                    break;
                    
                default: return 0;
                    break;
            }
            
        } else
            return  [pickerList count];

}


//-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
//    
//    UILabel *label = [[UILabel alloc] init];
//    label.textColor = [UIColor blackColor];
//    label.backgroundColor = [UIColor clearColor];
//    label.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
//    
//    switch (pickerView.tag) {
//        case 0: // buy
//            label.text = [pickerList objectAtIndex:row];
//            break;
//        case 1: // sell
//            label.text = [pickerList objectAtIndex:row];
//            break;
//        case 10: // buy (triggerprice)
//            label.text = [pickerList objectAtIndex:row];
//            break;
//        case 11: // sell (triggerprice)
//            label.text = [pickerList objectAtIndex:row];
//            break;
//        case 3: // trigger type
//            label.text = [pickerList objectAtIndex:row];
//            break;
//        case 4: // trigger direction
//            label.text = [pickerList objectAtIndex:row];
//            break;
//            
//        case 20: {
//            switch (component) {
//                case 0:
//                    label.text = [self.orderTypeList objectAtIndex:row];
//                    break;
//                case 1:
//                    label.text = [self.orderValidityList objectAtIndex:row];
//                    break;
//                case 2:
//                    label.text = [self.currencyList objectAtIndex:row];
//                    break;
//                case 3: {
//                    label.text = [self.paymentList objectAtIndex:row];
//                }break;
//                    
//                default:label.text = @"";
//                    break;
//            }
//            
//            
//        }
//        default:
//            label.text =  @"";
//            break;
//    }
//    
//    NSLog(@"picker %@",label.text);
//    
//    return label;
//    
//}
//
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView == _selectorPickerSmallSize) {
        switch (iCurrentFocusPicker) {
            case 10:
                return  self.orderTypeList[row];
                break;
            case 11:
                return self.orderValidityList[row];
                break;
            case 12:
                return self.currencyList[row];
                break;
            case 13:
                return self.paymentList[row];
                break;
        }
    }
    
    switch (pickerView.tag) {
        case 0: // buy
            return [pickerList objectAtIndex:row];
            break;
        case 1: // sell
            return [pickerList objectAtIndex:row];
            break;
        case 10: // buy (triggerprice)
            return [pickerList objectAtIndex:row];
            break;
        case 11: // sell (triggerprice)
            return [pickerList objectAtIndex:row];
            break;
        case 3: // trigger type
            return [pickerList objectAtIndex:row];
            break;
        case 4: // trigger direction
            return [pickerList objectAtIndex:row];
            break;
            
        case 20: {
            switch (component) {
                case 0:
                    return [self.orderTypeList objectAtIndex:row];
                    break;
                case 1:
                    return [self.orderValidityList objectAtIndex:row];
                    break;
                case 2:
                    return [self.currencyList objectAtIndex:row];
                    break;
                case 3: {
                    return [self.paymentList objectAtIndex:row];
                }break;
                    
                default:return @"";
                    break;
            }
            
            
        }
        default:
            return  @"";
            break;
    }
    
}


-(void)updateOrdTypeAndValidity {
    
    NSString *ordType = [self.orderTypeList objectAtIndex:ordTypeRow];
    
    NSString *coloredLblStr;
    
    if ([action isEqualToString:@"BUY"]||[action isEqualToString:@"SELL"]) {
        
        [self doOrderCtrl:NO];
        
        coloredLblStr = [NSString stringWithFormat:@"%@ (%@) \n%@",[LanguageManager stringForKey:action],ordType,[_actionTypeDict objectForKey:@"stkNname"]];
        
    } else if ([action isEqualToString:@"REVISE"]||[action isEqualToString:@"CANCEL"]) {
        
        [self doReviseCtrlForTextfield];
        
        TradeStatus *selectedStatus;
        if ([action isEqualToString:@"REVISE"]) {
            selectedStatus = tradeStatusRevise;
        } else {
            selectedStatus = tradeStatusCancel;
        }
        
        coloredLblStr = [NSString stringWithFormat:@"%@ (%@) \n%@",[LanguageManager stringForKey:selectedStatus.action],ordType,[_actionTypeDict objectForKey:@"stkNname"]];
        
    }
    
    [_stkNname setText:coloredLblStr];
    [self calculatePrice];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (pickerView == _selectorPickerSmallSize) {
        switch (iCurrentFocusPicker) {
            case 10: { // Ord Type
                ordTypeRow =row;
                [self updateOrdTypeAndValidity];
                [btnOrderType setTitle:_orderTypeList[ordTypeRow] forState:UIControlStateNormal];

            }
                break;
            case 11: { // Validity
                validityRow = row;
                [self updateOrdTypeAndValidity];
                [btnValidity setTitle:_orderValidityList[validityRow] forState:UIControlStateNormal];

            } break;
                
                
            case 12: { // Currency
                
                currencyRow =row;
                [self calculatePrice];
                [btnCurrency setTitle:_currencyList[currencyRow] forState:UIControlStateNormal];

                
            } break;
                
            case 13: { // Payment
                
                paymentRow =row;
                [btnPayment setTitle:_paymentList[paymentRow] forState:UIControlStateNormal];

                if ([self.paymentList count]>0) {
                    // need to call API again because depends on isCUT or not
                    [self refreshTrdLmtBtnPressed:nil];
                    
                }
                
                
            } break;
        }
        
        return;
    }

    
    switch (pickerView.tag) {
        case 0 :{ //BUY
             if ([UserPrefConstants singleton].pointerDecimal==3) {
            _stkPrice.text =[NSString stringWithFormat:@"%.3f",[[pickerList objectAtIndex:row] floatValue]];
             }else{
                 _stkPrice.text =[NSString stringWithFormat:@"%.4f",[[pickerList objectAtIndex:row] floatValue]];
             }
            [self calculatePrice];
            priceRow =row;
        }break;
        case 1:{ // SELL
            if ([UserPrefConstants singleton].pointerDecimal==3) {
            _stkPrice.text =[NSString stringWithFormat:@"%.3f",[[pickerList objectAtIndex:row] floatValue]];
            }else{
                  _stkPrice.text =[NSString stringWithFormat:@"%.4f",[[pickerList objectAtIndex:row] floatValue]];
            }
            [self calculatePrice];
            priceRow =row;
        }break;
            
        case 10 :{ //BUY
            if ([UserPrefConstants singleton].pointerDecimal==3) {
            _triggerPrice.text =[NSString stringWithFormat:@"%.3f",[[pickerList objectAtIndex:row] floatValue]];
            }else{
                 _triggerPrice.text =[NSString stringWithFormat:@"%.4f",[[pickerList objectAtIndex:row] floatValue]];
            }
            triggerPriceRow =row;
        }break;
        case 11:{ // SELL
            if ([UserPrefConstants singleton].pointerDecimal==3) {
            _triggerPrice.text =[NSString stringWithFormat:@"%.3f",[[pickerList objectAtIndex:row] floatValue]];
            }else{
             _triggerPrice.text =[NSString stringWithFormat:@"%.4f",[[pickerList objectAtIndex:row] floatValue]];
            }
            triggerPriceRow =row;
        }break;
            
        case 3: { // Trigger Type
            _triggerType.text = [pickerList objectAtIndex:row];
            triggerRow = row;
        } break;
            
        case 4: { // Trigger Direction
            _triggerDirection.text = [pickerList objectAtIndex:row];
            directionRow = row;
        } break;
            
        case 20: {
            
            switch (component) {
                    
                case 0: { // Ord Type
                    ordTypeRow =row;
                    [self updateOrdTypeAndValidity];
                }
                break;
                case 1: { // Validity
                    validityRow = row;
                    [self updateOrdTypeAndValidity];
                } break;
                    
                    
                case 2: { // Currency
                    
                    currencyRow =row;
                    [self calculatePrice];
                    
                } break;
                    
                case 3: { // Payment
                    
                    paymentRow =row;
                    if ([self.paymentList count]>0) {
                        // need to call API again because depends on isCUT or not
                        [self refreshTrdLmtBtnPressed:nil];
                      
                    }
                 
                    
                } break;
                    
                default:
                    break;
            }
            
        }
        default:
            break;
    }
    
}


#pragma mark - TableView

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    if (indexPath.row % 2==0) {
//        
//        cell.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.90 alpha:1];;
//    } else {
//        
//        cell.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:0.80 alpha:1];
//    }
//    
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    _accountName.text = [[acclist objectAtIndex:indexPath.row] client_name];
//    
//    [UserPrefConstants singleton].userSelectedAccount=(int)indexPath.row;
//    
//    [accTableView reloadData];
//    [columnPickerPopover dismissPopoverAnimated:YES];
//
//    if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
//        [atp getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount];
//    
//    
//}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return [[[UserPrefConstants singleton]userAccountlist]count];
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return 44;
//
//}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell3";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//    if (indexPath.row == [UserPrefConstants singleton].userSelectedAccount) {
//        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
//    }
//    else {
//        [cell setAccessoryType:UITableViewCellAccessoryNone];
//    }
//    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
////    if (indexPath.row % 2 == 0) {
////        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageFromResource:@"TableRow_Dark.png"]];
////        [cell setBackgroundView:imgView];
////    } else {
////        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageFromResource:@"TableRow_Light.png"]];
////        [cell setBackgroundView:imgView];
////    }
//    
//    UserAccountClientData *uacd;
//    cell.textLabel.font = [ UIFont fontWithName: @"Helvetica Bold" size: 16.0];
//    cell.detailTextLabel.font = [ UIFont fontWithName: @"Helvetica Neue" size: 16.0];
//    uacd =[[[UserPrefConstants singleton]userAccountlist]objectAtIndex:indexPath.row];
//    cell.textLabel.text =[NSString stringWithFormat:@"%@-%@",uacd.broker_branch_code,uacd.client_account_number];
//    cell.detailTextLabel.text = uacd.client_name;
//    return cell;
//}

#pragma mark - Hide or Show fields method


-(void)disableForView:(UIView*)view {
    
    if (view.alpha>=1.0) { // disable all input with alpha>=1.0;
    
        for(UIView *v in [view subviews])
        {
            if([v isKindOfClass:[UITextField class]])
            {
                UITextField *tf = (UITextField*)v;
                tf.enabled = NO;
            }
            
            if([v isKindOfClass:[UIButton class]])
            {
                UIButton *btn = (UIButton*)v;
                btn.enabled = NO;
            }
            
            if([v isKindOfClass:[UIImageView class]])
            {
                UIImageView *imgView = (UIImageView*)v;
                [imgView setHidden:YES];
            }
        }
    }
}

-(void)disableEnabledFields {
    
    [self disableForView:_priceView];
    [self disableForView:_qtyView];
    [self disableForView:_dateView];
    [self disableForView:_minQtyView];
    
    [self disableForView:_trigPriceView];
    [self disableForView:_triggerVIew];
    [self disableForView:_directionView];
    [self disableForView:_discloseView];
    
}



-(void)disableAllFields {
    
    [self setFieldEnable:NO forView:_priceView];
    [self setFieldEnable:NO forView:_qtyView];
    [self setFieldEnable:NO forView:_dateView];
    [self setFieldEnable:NO forView:_minQtyView];
    
    [self setFieldEnable:NO forView:_trigPriceView];
    [self setFieldEnable:NO forView:_triggerVIew];
    [self setFieldEnable:NO forView:_directionView];
    [self setFieldEnable:NO forView:_discloseView];
}




-(void)setFieldEnable:(BOOL)enable forView:(UIView*)view {

    view.userInteractionEnabled = enable;

    [view setAlpha:(enable?1.0:0.5)];
    
    for(UIView *v in [view subviews])
    {
        if([v isKindOfClass:[UITextField class]])
        {
            UITextField *tf = (UITextField*)v;
            [tf setHidden:!enable];
        }
        
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *lbl = (UILabel*)v;
            [lbl setHidden:!enable];
            if (lbl.tag==1) {
                [lbl setHidden:NO];
                if (enable) {
                    [lbl setTextColor:[UIColor blackColor]];
                } else {
                
                    [lbl setTextColor:[UIColor grayColor]];
                }
            }
        }
        
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton*)v;
            btn.enabled = enable;
        }
        
        if([v isKindOfClass:[UIImageView class]])
        {
            UIImageView *imgView = (UIImageView*)v;
            [imgView setHidden:!enable];
        }
    }
}




#pragma mark - Options Buttons pressed

//-(void)LabelTitle:(id)sender
//{
//    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
//    dateFormat.dateStyle=NSDateFormatterMediumStyle;
//    [dateFormat setDateFormat:@"dd/MM/yyyy"];
//    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:datePickerView.date]];
//    //assign text to label
//    _lblDate.text = str;
//    dateRow =[datePickerView date];
//    
//}


-(void)n2datePickerSelected:(NSDate *)date {
    
    // verify date chosen
    // In case of GTD on today date and onwards are allowed
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd"];
    NSString *todayDateStr = [formatter stringFromDate:[NSDate date]];
    NSString *selectedDateStr = [formatter stringFromDate:date];
    
    NSDate *todayDate = [formatter dateFromString:todayDateStr];
    NSDate *selectedDate = [formatter dateFromString:selectedDateStr];
    
    NSTimeInterval dateDifference = [todayDate timeIntervalSince1970] - [selectedDate timeIntervalSince1970];
    if (dateDifference>0) {
        [datePicker setPickerDate:todayDate animated:YES];
        return;
    }

    
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];
    //assign text to label
    _lblDate.text = str;
    dateRow =date;
}

- (IBAction)dateBtnPressed:(UIButton *)sender {
    
    [atp timeoutActivity];
    
    UIViewController *temp =[[UIViewController alloc]init];
//    datePickerView =[[UIDatePicker alloc]init];
//    datePickerView.datePickerMode=UIDatePickerModeDate;
//    if (dateRow==nil)
//        datePickerView.date =[NSDate date];
//    else
//        datePickerView.date =dateRow;
//    [datePickerView addTarget:self action:@selector(LabelTitle:) forControlEvents:UIControlEventValueChanged];
//    datePickerView.tag = 2;
//    [temp.view addSubview:datePickerView];
    
    if (dateRow==nil)
        datePicker = [[N2DatePicker alloc] initWithDate:[NSDate date]];
    else
        datePicker = [[N2DatePicker alloc] initWithDate:dateRow];
    datePicker.myDelegate = self;
    [temp.view addSubview:datePicker];
    
    popoverController=temp.popoverPresentationController;
    temp.preferredContentSize=CGSizeMake(320,216);
    if (popoverController)
    {
        popoverController.sourceView = sender;
        popoverController.sourceRect = sender.bounds;
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:temp];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

#pragma mark - QtyBtn Delegate
-(void)quantityBtnPressed:(UIButton *)btn
{
    
    if (whichQty==0) {
        int quantityCount = _quantity.text.intValue + (int)btn.tag;
        if (quantityCount<=0) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[LanguageManager stringForKey:@"Note"]
                                                          message:[LanguageManager stringForKey:@"Quantity must be more than zero."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        long tot = _quantity.text.intValue + btn.tag;
        _quantity.text = [NSString stringWithFormat:@"%ld", tot];
    } else if (whichQty==1) {
//        int quantityCount = _minquantity.text.intValue + (int)btn.tag;
//        if (quantityCount<=0) {
//            
//            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[LanguageManager stringForKey:@"Note"]
//                                                          message:[LanguageManager stringForKey:@"Quantity must be more than zero."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
//            [alert show];
//            
//            return;
//        }
        long tot = _minquantity.text.intValue + btn.tag;
        _minquantity.text = [NSString stringWithFormat:@"%ld",tot];
    } else if (whichQty==2) {
        
//        int quantityCount = _discloseQty.text.intValue + (int)btn.tag;
//        if (quantityCount<=0) {
//            
//            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[LanguageManager stringForKey:@"Note"]
//                                                          message:[LanguageManager stringForKey:@"Quantity must be more than zero."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
//            [alert show];
//            
//            return;
//        }
        long tot = _discloseQty.text.intValue + btn.tag;
        _discloseQty.text = [NSString stringWithFormat:@"%ld",tot];
    }
    [self calculatePrice];
    //NSLog(@"%ld", (long)btn.tag);
}

- (IBAction)openQtyoptions:(UIButton *)sender {
    [atp timeoutActivity];
    whichQty = sender.tag;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    qtyPadBtn = [storyboard instantiateViewControllerWithIdentifier:@"QuantityPadViewController"];
    [qtyPadBtn setDelegate:self];
    popoverController=qtyPadBtn.popoverPresentationController;
    qtyPadBtn.preferredContentSize= CGSizeMake(400,120);
    
    if (popoverController)
    {
        popoverController.sourceView = sender;
        popoverController.sourceRect = sender.bounds;
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:qtyPadBtn];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}



-(CGFloat)getPriceValue:(long)tag {
    
    CGFloat result = 0;
    switch (tag) {
        case -1:
            result = -1.0;
            break;
        case -2:
            result = -0.1;
            break;
        case -3:
            result = -0.01;
            break;
        case -4:
            result = -2.0;
            break;
        case -5:
            result = -5.0;
            break;
        case -6:
            result = -0.001;
            break;
        case 1:
            result = 1.0;
            break;
        case 2:
            result = 0.1;
            break;
        case 3:
            result = 0.01;
            break;
        case 4:
            result = 2.0;
            break;
        case 5:
            result = 5.0;
            break;
        case 6:
            result = 0.001;
            break;
        default:
            break;
    }
    
    return result;
}

#pragma mark - PricePad Delegates

-(void)priceBtnPressed:(UIButton *)btn {
    
    // 0 = trade price
    if (whichPrice==0) {
        CGFloat priceToBe = _stkPrice.text.floatValue + [self getPriceValue:btn.tag];
        if (priceToBe<=0) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[LanguageManager stringForKey:@"Note"]
                                                          message:[LanguageManager stringForKey:@"Price must be more than zero."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
            [alert show];
            
            return;
        }
                 if ([UserPrefConstants singleton].pointerDecimal==3) {
            _stkPrice.text = [NSString stringWithFormat:@"%.3f", priceToBe];
                 }else{
                     _stkPrice.text = [NSString stringWithFormat:@"%.4f", priceToBe];
                 }
        
        
    // 1 = trigger price
    } else {
        CGFloat priceToBe = _triggerPrice.text.floatValue + [self getPriceValue:btn.tag];
        if (priceToBe<=0) {
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:[LanguageManager stringForKey:@"Note"]
                                                          message:[LanguageManager stringForKey:@"Price must be more than zero."] delegate:self cancelButtonTitle:[LanguageManager stringForKey:@"Ok"] otherButtonTitles:nil];
            [alert show];
            
            return;
        }
          if ([UserPrefConstants singleton].pointerDecimal==3) {
        _triggerPrice.text = [NSString stringWithFormat:@"%.3f",priceToBe];
          }else{
               _triggerPrice.text = [NSString stringWithFormat:@"%.4f",priceToBe];
          }
    }
    [self calculatePrice];
}

- (IBAction)openPricePad:(id)sender {
    [atp timeoutActivity];
    
    UIButton *btn = (UIButton*)sender;
    whichPrice = btn.tag;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    pricePad = [storyboard instantiateViewControllerWithIdentifier:@"PricePadViewController"];
    [pricePad setDelegate:self];
    popoverController=pricePad.popoverPresentationController;
    pricePad.preferredContentSize= CGSizeMake(400,120);
    
    if (popoverController)
    {
        popoverController.sourceView = btn;
        popoverController.sourceRect = btn.bounds;
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:pricePad];
    [columnPickerPopover presentPopoverFromRect:btn.frame
                                         inView:btn.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}



- (IBAction)optionBtnPressed:(UIButton *)sender {
    
    [atp timeoutActivity];
    
    UIViewController *temp =[[UIViewController alloc]init];
    pickerList =[NSMutableArray array];
    
    switch (sender.tag) {
            
        case 3: { // Trigger Type
            
            for (int i =0; i<=trdRules.triggerPriceTypeList.count-1;i++) {
                if (![pickerList containsObject:[trdRules.triggerPriceTypeList objectAtIndex:i]])
                    [pickerList addObject:[trdRules.triggerPriceTypeList objectAtIndex:i]];
                
            }
            myPickerView.tag = 3;
             if ([pickerList count]>0)[myPickerView selectRow:triggerRow inComponent:0 animated:YES];
        }break;
        case 4: { // Direction
            
            for (int i =0; i<=trdRules.triggerPriceDirectionList.count-1;i++) {
                if (![pickerList containsObject:[trdRules.triggerPriceDirectionList objectAtIndex:i]])
                    [pickerList addObject:[trdRules.triggerPriceDirectionList objectAtIndex:i]];
                
            }
            myPickerView.tag = 4;
            if ([pickerList count]>0)[myPickerView selectRow:directionRow inComponent:0 animated:YES];
        }break;
        default:
            break;
    }
    
    
    [temp.view addSubview:myPickerView];
    [myPickerView reloadAllComponents];
    popoverController=temp.popoverPresentationController;
    temp.preferredContentSize= CGSizeMake(popupPickerWidth,popupPickerHeight);
    if (popoverController)
    {
        popoverController.sourceView = sender;
        popoverController.sourceRect = sender.bounds;
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:temp];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    
    
    
}


#pragma mark - MD Price Clicked Notification

-(void)mdPriceTappedNotification:(NSNotification*)notification {
    
    NSDictionary *dict = notification.userInfo;
    NSString *priceStr = [dict objectForKey:@"Price"];
    
    if ([priceStr floatValue]<=0) {
        return;
    }
    
    CGFloat priceFloat = [priceStr floatValue];
    if ([UserPrefConstants singleton].pointerDecimal==3) {
    _stkPrice.text = [NSString stringWithFormat:@"%.3f",priceFloat];
    }else{
         _stkPrice.text = [NSString stringWithFormat:@"%.4f",priceFloat];
    }
    
}

#pragma mark - TExtField delegate


- (IBAction)priceEditingChanged:(id)sender {
    [atp timeoutActivity];
    [self calculatePrice];
}

- (IBAction)qtyEditingChanged:(id)sender {
    [atp timeoutActivity];
     [self calculatePrice];
}

- (IBAction)minQtyEditingChanged:(id)sender {
}

- (IBAction)discQtyEditingChanged:(id)sender {
}

- (IBAction)triggerPriceEditingChanged:(id)sender {
}

- (IBAction)toggleShortSell:(id)sender {
    [atp timeoutActivity];
    
    UIButton *btn = (UIButton*)sender;
    _isShortSell = !_isShortSell;
    
    if (_isShortSell) {
        [btn setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
    } else {
        [btn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [atp timeoutActivity];
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - Other buttons pressed

- (IBAction)refreshTrdLmtBtnPressed:(id)sender {
    
    [atp timeoutActivity];
    
    NSString *paytype;
    if ([self.paymentList count]>=paymentRow+1)
        paytype = [self.paymentList objectAtIndex:paymentRow];
    
    BOOL isCut = ([paytype isEqualToString:@"CUT"])?YES:NO;
    
    NSString *selectedCurr;
    if ([self.currencyList count]>=currencyRow+1)
        selectedCurr = [self.currencyList objectAtIndex:currencyRow];
    
    NSLog(@"isCut %d",isCut);
    
    if ([[[UserPrefConstants singleton]userAccountlist] count]>0)
        [atp getTradeLimitForAccount:[UserPrefConstants singleton].userSelectedAccount
                       forPaymentCUT:isCut andCurrency:selectedCurr];
       
    
    
}

- (IBAction)accountBtnPressed:(UIButton *)sender {
    [atp timeoutActivity];
    
    // only allow change account on Buy and Sell action only
    if (([action isEqualToString:@"BUY"]||[action isEqualToString:@"SELL"])) {

        if ([self.parentController isKindOfClass: [StockDetailViewController class]]) {
            [self.parentController performSegueWithIdentifier:@"tradeAccountSegueID2" sender:self];
        }else if ([self.parentController isKindOfClass: [QuoteScreenViewController class]]) {
            [self.parentController performSegueWithIdentifier:@"tradeAccountSegueID3" sender:self];
        }
    }
    
//    [self performSegueWithIdentifier:@"tradeAccountSegueID2" sender:self];
    
//    viewcont =[[UIViewController alloc]init];
//    [accTableView reloadData];
//    [viewcont.view addSubview:accTableView];
//    popoverController=viewcont.popoverPresentationController;
//    viewcont.preferredContentSize=CGSizeMake(300,totalAccToShow*44);
//    
//    if (popoverController)
//    {
//        popoverController.sourceView = sender;
//        popoverController.sourceRect = sender.bounds;
//        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
//    }
//    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:viewcont];
//    [columnPickerPopover presentPopoverFromRect:sender.frame
//                                         inView:sender.superview
//                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}


- (IBAction)btnSkipConfirmationPressed:(UIButton *)sender {
    [atp timeoutActivity];
    
    if(sender.selected)
    {
        [_btnSkipConfirmation setSelected:NO];
    }
    else
    {
        [_btnSkipConfirmation setSelected:YES];
    }
}



- (IBAction)toggleMoreFields:(id)sender {
    [atp timeoutActivity];
    
    UIButton *btn = (UIButton*)sender;
    padLarge = !padLarge;
    [_padView bringSubviewToFront:_toolBarView];
    
    [self refreshTrdLmtBtnPressed:nil];
    
    if (padLarge) {
        [_priceView setCenter:CGPointMake(oriPriceX,
                                          _priceView.center.y)];
        [_qtyView setCenter:CGPointMake(oriQtyX,
                                        _qtyView.center.y)];
        
        
        
    } else {
        
        [_priceView setCenter:CGPointMake(self.padView.frame.size.width/2.0-_priceView.frame.size.width*0.7,
                                          _priceView.center.y)];
        [_qtyView setCenter:CGPointMake(self.padView.frame.size.width/2.0+_qtyView.frame.size.width*0.7,
                                        _qtyView.center.y)];
    }
    
    [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^(void){
        
            if (padLarge) {
                
             
                
                [_dateView setHidden:NO];
                [_minQtyView setHidden:NO];
                
                [_padView setFrame:CGRectMake(_padView.frame.origin.x,
                                             _padView.frame.origin.y,
                                             _padView.frame.size.width,
                                              kFullHeight)];
                [_padView setCenter:self.view.center];
                [_toolBarView setCenter:CGPointMake(_padView.frame.size.width/2.0, _padView.frame.size.height-_toolBarView.frame.size.height/2.0)];
                
                [btn setImage:[UIImage imageNamed:@"tradeMin"] forState:UIControlStateNormal];
                
                [_selectorPicker selectRow:ordTypeRow inComponent:0 animated:YES];
                [_selectorPicker selectRow:validityRow inComponent:1 animated:YES];
                [_selectorPicker selectRow:currencyRow inComponent:2 animated:YES];
                [_selectorPicker selectRow:paymentRow inComponent:3 animated:YES];
                
                [btnOrderType setTitle:_orderTypeList[ordTypeRow] forState:UIControlStateNormal];
                [btnValidity setTitle:_orderValidityList[validityRow] forState:UIControlStateNormal];
                [btnCurrency setTitle:_currencyList[currencyRow] forState:UIControlStateNormal];
                [btnPayment setTitle:_paymentList[paymentRow] forState:UIControlStateNormal];
                
                [UserPrefConstants singleton].orderPadAdvance =YES;
               
            } else {
                
               
                
                [_dateView setHidden:YES];
                [_minQtyView setHidden:YES];
                [_padView setFrame:CGRectMake(_padView.frame.origin.x,
                                              _padView.frame.origin.y,
                                              _padView.frame.size.width,
                                              kBasicHeight)];
                [_padView setCenter:self.view.center];
                [_toolBarView setCenter:CGPointMake(_padView.frame.size.width/2.0, _padView.frame.size.height-_toolBarView.frame.size.height/2.0)];
                  [btn setImage:[UIImage imageNamed:@"tradeMax"] forState:UIControlStateNormal];
                 [UserPrefConstants singleton].orderPadAdvance =NO;
                
                [_selectorPicker selectRow:0 inComponent:0 animated:YES];
                [_selectorPicker selectRow:0 inComponent:1 animated:YES];
                
                
                [btnOrderType setTitle:_orderTypeList[0] forState:UIControlStateNormal];
                [btnValidity setTitle:_orderValidityList[0] forState:UIControlStateNormal];

            }
        
        }
     
        completion:^(BOOL finished) {
                         
            
        }
     ];
    
    
}



-(BOOL)checkAccountAndPayment {
    
    BOOL result = YES;
    
    // check payment type and account type matrix
    if ([[UserPrefConstants singleton].brokerCode isEqualToString:@"066"]) {
        
        if ([uacd.account_type isEqualToString:@"B"]) {
            if ([[self.paymentList objectAtIndex:paymentRow] isEqualToString:@"CUT"]) {
                
            } else {
                
            }
        } else {
            if ([[self.paymentList objectAtIndex:paymentRow] isEqualToString:@"CUT"]) {
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"InformationPayCUTAccNonCUT"]
                                             inController:self withTitle:@""];
                [self clearLoadingView];
                result = NO;
            } else {
                
            }
        }
    }
    
    long qtyFactor = 1;
    
    if ([UserPrefConstants singleton].quantityIsLot) {
        qtyFactor = _stockLotSize;
    } else {
        qtyFactor = 1;
    }
    
    double price = [_stkPrice.text doubleValue];
    double qty = [_quantity.text longLongValue]*qtyFactor;
    
    // resolved limit is either buy limit or credit limit
    // in default currency
    
    double limitCheck = self.resolvedLimit;
    double priceCheck = price;
    
    
    
    // get currency rate for settlement currency
    CurrencyRate *cr = [[UserPrefConstants singleton].currencyRateDict objectForKey:_myOrderDetails.currency];
    
    // userpref defaultcurrency = broker currency
    // stockCurrency = currency of stock data (price)
    // exchangeCurrency = currency of selected exchange
    // orderDetails.currency = settlement currency (user selected)
    
    //convert limit to sett curr.
    if (![[UserPrefConstants singleton].defaultCurrency isEqualToString:_myOrderDetails.currency]) {
        limitCheck = self.resolvedLimit / cr.buy_rate;
    }
    
    // convert price to sett curr
    if (![_stockCurrency isEqualToString:_myOrderDetails.currency]) {
        priceCheck =  price / cr.buy_rate;
    }
    
    // NSLog(@"CHECK: %f, %f",priceCheck*qty, limitCheck);
    
    BOOL notEnough = NO;
    
    if (priceCheck*qty>limitCheck) notEnough = YES;
    
    if (self.resolvedLimit<=0) notEnough = YES;
    
    // if simple order pad there is no orderDetails.currency! so use stockCurrency
    if (!padLarge) {
        if (price*qty>self.resolvedLimit) notEnough = YES;
    }

    
    // only for CIMB SG.
    if (notEnough&&[[UserPrefConstants singleton].brokerCode isEqualToString:@"066"]) {
        
        if ([[self.paymentList objectAtIndex:paymentRow] isEqualToString:@"CUT"]) {
            // for CUT account
            if ([uacd.account_type isEqualToString:@"B"]) {
                
                [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"InsufficientCashCUT"]
                                             inController:self withTitle:@""];
                [self clearLoadingView];
                result = NO;
                
                
                // non cut account
            } else {
                
                [self alertWithMsg:[LanguageManager stringForKey:@"InformationCUT"]
                    andButtonTitle:[LanguageManager stringForKey:@"F.A.Q."] andLink:@"https://www.itradecimb.com.sg/app/help.client.services.z?cat=01&subcat=01_03&subsubcat=8a3e938c4761fbe0014766fbe86b017d"];
                [self clearLoadingView];
                result = NO;
                
            }
            
            
        }
        
    }
    
    return result;
}

#pragma  mark - Server Replies

-(void)atpValidate1FADone:(NSNotification*)notification {
    dispatch_async(dispatch_get_main_queue(), ^{

        if (![self checkAccountAndPayment]) return;
        
        NSDictionary *replyDict = notification.userInfo;
        NSString *statusCode = [replyDict objectForKey:@"statusCode"];
        NSString *statusMsg = [replyDict objectForKey:@"statusMsg"];
        NSString *errorMsg = [replyDict objectForKey:@"errorMsg"];
        
        if (![statusCode isEqualToString:@"200"]) {
            
            [_traderWorking stopAnimating];
            [_okBtn setHidden:NO];
            if ([errorMsg length]<=0) errorMsg = statusMsg;
            _errorMsgLabel.text = [NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg];
            
        } else {
            
            if (_btnSkipConfirmation.selected) {
                [self doTrade];
            } else {
                [self showConfirmation];
            }
            
        }
        
    });
    
}


-(void)validateTrade2FAPinForDeviceDone:(NSNotification*)notification {
    

    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (![self checkAccountAndPayment]) return;
        
        NSDictionary *replyDict = notification.userInfo;
        NSString *statusCode = [replyDict objectForKey:@"statusCode"];
        NSString *statusMsg = [replyDict objectForKey:@"statusMsg"];
        NSString *errorMsg = [replyDict objectForKey:@"errorMsg"];
        
        // statuscode 0 is success for CIMB SG 2FA. WTF
        if ([statusCode isEqualToString:@"200"]||[statusCode isEqualToString:@"0"]) {
            
            // success!
            [UserPrefConstants singleton].verified2FA = YES;
            [UserPrefConstants singleton].orderDetails.otpPin = [UserPrefConstants singleton].dvData2FA.otpPin;
            [UserPrefConstants singleton].orderDetails.deviceID = [UserPrefConstants singleton].dvData2FA.deviceID;
            
            if (_btnSkipConfirmation.selected) {
                [self doTrade];
            } else {
                [self showConfirmation];
            }

        } else {
        
            [_traderWorking stopAnimating];
            [_okBtn setHidden:NO];
            if ([errorMsg length]<=0) errorMsg = statusMsg;
            _errorMsgLabel.text = [NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg];
            
        }
        
    });
}




-(void)tradeLimitReceived {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        
        uacd = [UserPrefConstants singleton].userSelectedAccount;
        
        // default:
        self.resolvedLimit = uacd.credit_limit;
        _tradeLimitTitle.text = [LanguageManager stringForKey:@"Trading Limit:"];
        
        NSString *paytype;
        if ([self.paymentList count]>=paymentRow+1)
            paytype = [self.paymentList objectAtIndex:paymentRow];
        
        
        if (![UserPrefConstants singleton].CreditLimitOrdPad) {
            
            _tradeLimitTitle.text = @"";
            _tradeLimit.text = @"";
            
            
        } else {
            // CUT
            if ([paytype isEqualToString:@"CUT"]) {
                _tradeLimitTitle.text = [LanguageManager stringForKey:@"Investment Power:"];
                self.resolvedLimit = uacd.credit_limit;
                
                NSString *selectedCurr;
                if ([self.currencyList count]>=currencyRow+1)
                    selectedCurr = [self.currencyList objectAtIndex:currencyRow];
                
                _tradeLimit.text = [NSString stringWithFormat:@"%@ %@",selectedCurr,
                                    [priceFormatter stringFromNumber:[NSNumber numberWithDouble:self.resolvedLimit]]];
            } else {
            // NON CUT
                if ([[[UserPrefConstants singleton].clientLimitOptionDict objectForKey:self.stkExchange] intValue] == 3) {
                    if (uacd.isMarginAccount==0) {
                        if ([actualAction isEqualToString:@"BUY"]) {
                            self.resolvedLimit = uacd.buy_limit;
                            _tradeLimitTitle.text = [LanguageManager stringForKey:@"Buy Limit:"];
                        } else {
                            self.resolvedLimit = uacd.sell_limit;
                            _tradeLimitTitle.text = [LanguageManager stringForKey:@"Sell Limit:"];
                        }
                    } else {
                        
                        self.resolvedLimit = uacd.credit_limit;
                        _tradeLimitTitle.text = [LanguageManager stringForKey:@"Trading Limit:"];
                    }
                }
                _tradeLimit.text = [NSString stringWithFormat:@"%@ %@" ,
                                    [UserPrefConstants singleton].defaultCurrency,
                                    [priceFormatter stringFromNumber:[NSNumber numberWithDouble:self.resolvedLimit]]];
            }
            
        }

        
        
        // NSLog(@"uacd: %@", uacd);
        
        //NSLog(@"_resolvedLimit %f",_resolvedLimit);
        
        
        // Adjust label sizes/position
        CGFloat titleHeight = _tradeLimitTitle.frame.size.height;
        [_tradeLimitTitle sizeToFit];
        _tradeLimitTitle.frame = CGRectMake(_tradeLimitTitle.frame.origin.x, _tradeLimitTitle.frame.origin.y,
                                            _tradeLimitTitle.frame.size.width, titleHeight);
        
        
        
        _tradeLimit.frame = CGRectMake(_tradeLimitTitle.frame.origin.x+_tradeLimitTitle.frame.size.width,
                                       _tradeLimit.frame.origin.y, _tradeLimit.frame.size.width, _tradeLimit.frame.size.height);
        

        
        
        
    });
    
}

-(void)tradeCancelNotification:(NSNotification *)notification
{

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
//            [_padView setCenter:CGPointMake(self.view.frame.size.width*2, _padView.center.y)];
            
        } completion:^(BOOL finished) {
            
            [_boxView setHidden:YES];
            // show window trade done and option to go orderbook.
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            tsVC = [storyboard instantiateViewControllerWithIdentifier:@"TradeStatusViewController"];
            tsVC.modalPresentationStyle= UIModalPresentationFormSheet;
            tsVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
            tsVC.preferredContentSize =CGSizeMake(320, 330);
            tsVC.delegate =self;
            
            NSDictionary *responseDict = notification.userInfo;
            tsVC.messageStr = [responseDict objectForKey:@"msg"];
            [self presentViewController:tsVC animated:YES completion:nil];
            
        }];
    });
    
}

-(void)tradeReviseNotification:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
//            [_padView setCenter:CGPointMake(self.view.frame.size.width*2, _padView.center.y)];
            
        } completion:^(BOOL finished) {
            
            [_boxView setHidden:YES];
            // show window trade done and option to go orderbook.
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            tsVC = [storyboard instantiateViewControllerWithIdentifier:@"TradeStatusViewController"];
            tsVC.modalPresentationStyle= UIModalPresentationFormSheet;
            tsVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
            tsVC.preferredContentSize =CGSizeMake(320, 330);
            tsVC.delegate =self;
            
            NSDictionary *responseDict = notification.userInfo;
            tsVC.messageStr = [responseDict objectForKey:@"msg"];
            [self presentViewController:tsVC animated:YES completion:nil];
            
        }];
    });
    
    
    
}


-(void)TradeSubmitStatus:(NSNotification *)notification
{
   
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
//*At Stock Detail page, when user click trade (success or failed) should open back those Orderpad.

            // [_padView setCenter:CGPointMake(self.view.frame.size.width*2, _padView.center.y)];
            
        } completion:^(BOOL finished) {
            
            NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
            
            
            [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
            
            NSDictionary * response = [notification.userInfo copy];
            
            [_boxView setHidden:YES];
            // show window trade done and option to go orderbook.
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            tsVC = [storyboard instantiateViewControllerWithIdentifier:@"TradeStatusViewController"];
            tsVC.modalPresentationStyle= UIModalPresentationFormSheet;
            tsVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
            tsVC.preferredContentSize =CGSizeMake(320, 330);
            tsVC.delegate =self;
            
            if ([[response objectForKey:@"tradesubmitstatus"] isEqual:@"Failed"]) {
                NSString *errorMsg =  [response objectForKey:@"error"];
                
                //NSLog(@"errorMsg %@",errorMsg);
                tsVC.messageStr = errorMsg;
                
                
                BOOL errorPin = !([[errorMsg lowercaseString] rangeOfString:@"pin"].location == NSNotFound);
                
                if (errorPin) {
                    // clear pin
                    //NSLog(@"Pin cleared");
                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"TradingPin"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
                
            } else {
                
                response = [response objectForKey:@"data"];
                NSDate *date = [NSDate date];
                [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                
                
                
                tsVC.messageStr =   [LanguageManager stringForKey:@"TradeTicketReceipt"
                                                 withPlaceholders:@{@"%1%":[response objectForKey:@"+"],
                                                                    @"%2%":[response objectForKey:@"5"],
                                                                    @"%3%":[response objectForKey:@"4"],
                                                                    @"%4%":[response objectForKey:@";"],
                                                                    @"%5%":[response objectForKey:@"6"],
                                                                    @"%6%":[response objectForKey:@"<"],
                                                                    @"%7%":[response objectForKey:@"9"],
                                                                    @"%8%":[dateFormatter stringFromDate:date]}];
                
                
            }

            [self presentViewController:tsVC animated:YES completion:nil];
            
            
        }];
    });
    
}


#pragma mark - TradeStatusVC Delegates
-(void)closeBtnDidClicked {
    
    [_boxView setHidden:NO];
    
    [_traderWorking stopAnimating];
    [_loadingView setHidden:YES];
    
//*At Stock Detail page, when user click trade (success or failed) should open back those Orderpad.
    [self.delegate traderDidCancelled];
}

-(void)closeAndOpenOrdBookDidClicked {
    
    //NSLog(@"Called OpenOrderBook");
    
    [_boxView setHidden:NO];
    [_traderWorking stopAnimating];
    [_loadingView setHidden:YES];
    
    [self.delegate traderDidCancelled];
    // open orderbook
    [UserPrefConstants singleton].orderTabIndex = 0; // orderbook index
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openOrderBookPortFolio" object:nil];
    
}

-(void)doTrade
{
    [atp timeoutActivity];
    
    [_boxView setHidden:NO];
    _errorMsgLabel.text =  [LanguageManager stringForKey:@"Submitting Order..."];
    [_loadingView setHidden:NO];
    [_traderWorking startAnimating];
    [_okBtn setHidden:YES];
 
    
    // NSLog(@"DO TRADE CALLED");
    
    if ([action isEqualToString:@"REVISE"]) {
        
        [self validifyTradeStatus:tradeStatusRevise];
        [atp prepareTradeRevise:NO forAccount:[UserPrefConstants singleton].userTradeConfirmData
                       forOrder:[UserPrefConstants singleton].orderDetails
               forExistingOrder:tradeStatusRevise];
        
    } else if ([action isEqualToString:@"CANCEL"]) {
        
        [self validifyTradeStatus:tradeStatusCancel];
        [atp prepareTradeCancel:nil forAccount:[UserPrefConstants singleton].userTradeConfirmData
                       forOrder:[UserPrefConstants singleton].orderDetails
               forExistingOrder:tradeStatusCancel];
    } else {
        [atp prepareTradeSubmit:NO forAccount:[UserPrefConstants singleton].userTradeConfirmData
                       forOrder:[UserPrefConstants singleton].orderDetails];

    }
    
    
}



#pragma mark - Skip Dialog Delegates
-(void)skipDialogDidSubmit:(NSString *)currentAction {
    
    [self doTrade];
    
}


-(void)clearLoadingView {
    
    [_loadingView setHidden:YES];
    [_traderWorking stopAnimating];
    [_cancelBtn setHidden:YES];
    [_okBtn setHidden:YES];
}


-(void)skipDialogDidCancel {
    [self clearLoadingView];
    [_boxView setHidden:NO];
}

#pragma mark - update trade account (called by delegate in Stk Detail)

-(void)updateTradingAccount:(UserAccountClientData*)account {
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [UserPrefConstants singleton].userSelectedAccount = account;
        uacd = account;
        
        
        [self updateOrderPad];
        
        
        _accLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",
                          uacd.client_account_number,
                          uacd.client_name,
                          uacd.broker_branch_code];
        
        
        if ([uacd.account_type isEqualToString:@"X"]) {
            // autoselect cash if it exist
            if ([self.paymentList containsObject:@"Cash"]) {
                NSUInteger idx = [self.paymentList indexOfObject:@"Cash"];
                NSLog(@"selectRow");
                [_selectorPicker selectRow:idx inComponent:3 animated:YES];
                
                [btnPayment setTitle:_paymentList[idx] forState:UIControlStateNormal];

            }
            
        } else if ([uacd.account_type isEqualToString:@"B"]) {
            // autoselect CUT if it exist
            if ([self.paymentList containsObject:@"CUT"]) {
                NSUInteger idx = [self.paymentList indexOfObject:@"CUT"];
                [_selectorPicker selectRow:idx inComponent:3 animated:YES];
                
                [btnPayment setTitle:_paymentList[idx] forState:UIControlStateNormal];

            }
            
        }
        
        [self refreshTrdLmtBtnPressed:nil];
    
    });
    
}


#pragma mark - SUBMIT ORDERPAD


-(void)alertWithMsg:(NSString*)msg andButtonTitle:(NSString*)title andLink:(NSString*)linkStr {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Info"] message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btnOk = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       // dismiss
    }];
    
    UIAlertAction *btnWithLink = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        // open link in newsmodal
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
        vc.view.backgroundColor = [UIColor clearColor];
        vc.newsTitleLabel.text = [LanguageManager stringForKey:@"F.A.Q."];
        
        vc.currentEd = nil;

        [vc openUrl:linkStr withJavascript:NO];
        [vc setTransitioningDelegate:_transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    
    [alert addAction:btnWithLink];
    [alert addAction:btnOk];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (IBAction)submitButton:(id)sender {
    
    [atp timeoutActivity];
    
    [self.loadingView setHidden:NO];
    [_traderWorking startAnimating];
    [_errorMsgLabel setText:[LanguageManager stringForKey:@"Working..."]];
    [_okBtn setHidden:YES];
    
    NSString *currExch = [[UserPrefConstants singleton] stripDelayedSymbol:[UserPrefConstants singleton].userCurrentExchange];
    
    // check if current exchange is supported to trade
    NSArray *supportedExch = [NSArray arrayWithArray:[UserPrefConstants singleton].userSelectedAccount.supported_exchanges];
    
    if ([supportedExch count]>0) {
        __block BOOL canTrade = NO;
        __block NSString *suppExchgStr = @"";
        [supportedExch enumerateObjectsUsingBlock:^(id x, NSUInteger idx, BOOL *stop) {
            NSString *supported = (NSString*)x; //NSLog(@"each Supported: %@",supported);
            suppExchgStr = [NSString stringWithFormat:@"%@ %@",suppExchgStr,supported];
            if ([supported isEqualToString:currExch]) canTrade=YES;
            
            //=================== ??
            //NOTE: temporary assume SG equals to SI
            if (([supported isEqualToString:@"SG"]&&[currExch isEqualToString:@"SI"])||
                ([supported isEqualToString:@"SI"]&&[currExch isEqualToString:@"SG"])) canTrade = YES;
        }];
        
        if (!canTrade) {
            [_errorMsgLabel setText:[LanguageManager stringForKey:@"This account can only trade in Exchanges %@"
                                                 withPlaceholders:@{@"%exchList%":suppExchgStr}]];
            [_traderWorking stopAnimating];
            [_okBtn setHidden:NO];
            
            return;
        }
        
    } else {
         [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"No supported exchanges available for this account to trade."]
                                      inController:self withTitle:@""];
        return;
    }
    


    
    self.myOrderDetails = [[OrderDetails alloc] init];
    self.myTradeStatus = [[TradeStatus alloc] init];
    
    if ([action isEqualToString:@"BUY"]) {
        _myOrderDetails.order_action = @"Buy";
        _myTradeStatus.action = @"Buy";
    }
    
    if ([action isEqualToString:@"SELL"]) {
        _myOrderDetails.order_action = @"Sell";
        _myTradeStatus.action = @"Sell";
    }
    
    if ([action isEqualToString:@"REVISE"]) {
        _myOrderDetails.order_action = @"Revise";
        _myTradeStatus = tradeStatusRevise;
    }
    
    if ([action isEqualToString:@"CANCEL"]) {
        _myOrderDetails.order_action = @"Cancel";
        _myTradeStatus = tradeStatusCancel;
    }
    
    
    
    NSString *stkCode = [UserPrefConstants singleton].userSelectingStockCode;
    
    
    _myOrderDetails.ticker =[[[[QCData singleton]qcFeedDataDict]objectForKey:stkCode] objectForKey:FID_38_S_STOCK_NAME];
    _myOrderDetails.stock_code =[[[[QCData singleton]qcFeedDataDict]objectForKey:stkCode] objectForKey:FID_33_S_STOCK_CODE] ;
    _myOrderDetails.company_name = [[[[QCData singleton]qcFeedDataDict]objectForKey:stkCode] objectForKey:FID_39_S_COMPANY] ;
    
    
    long qtyFactor = 1;
    
    if ([UserPrefConstants singleton].quantityIsLot) {
        qtyFactor = _stockLotSize;
    } else {
        qtyFactor = 1;
    }
    
    if ([_priceView isUserInteractionEnabled]) {
        
        if ([UserPrefConstants singleton].pointerDecimal==3) {
            _myOrderDetails.price = [NSString stringWithFormat:@"%.3f", [_stkPrice.text doubleValue]];
        }else{
            _myOrderDetails.price = [NSString stringWithFormat:@"%.4f", [_stkPrice.text doubleValue]];
        }
        
        
        if (([_stkPrice.text doubleValue]<=0)&&(![action isEqualToString:@"CANCEL"])) {
            [_errorMsgLabel setText:[LanguageManager stringForKey:@"Price must be more than zero."]];
            [_traderWorking stopAnimating];
            [_okBtn setHidden:NO];
            return;
        }
    } 
    
    if ([_qtyView isUserInteractionEnabled]) {
        
        _myOrderDetails.quantity = [NSString stringWithFormat:@"%.0f", [_quantity.text doubleValue]*qtyFactor];
        if (([_quantity.text doubleValue]*qtyFactor<=0)&&(![action isEqualToString:@"CANCEL"])) {
            [_errorMsgLabel setText:[LanguageManager stringForKey:@"Quantity must be more than zero."]];
            [_traderWorking stopAnimating];
            [_okBtn setHidden:NO];
            return;
        }
        
    }
    
    // FULL ORDERs
    if (padLarge) {
        
        // MAIN CONTROL FIELDS
        _myOrderDetails.order_type =[self.orderTypeList objectAtIndex:ordTypeRow];
        _myOrderDetails.validity = [self.orderValidityList objectAtIndex:validityRow];
        
        
        if ([self.currencyList count]>0)
            _myOrderDetails.currency =[self.currencyList objectAtIndex:currencyRow];
        else
            _myOrderDetails.currency = _exchangeCurrency;
        
        
        if ([self.paymentList count]>0)
            _myOrderDetails.payment_type = [self.paymentList objectAtIndex:paymentRow];
        else
            _myOrderDetails.payment_type =@"";
        
        
        if ([_myOrderDetails.validity isEqualToString:@"GTD"]) {
            NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
            [ndf setDateFormat:@"dd/MM/yyyy"];
            NSDate *date = [ndf dateFromString:_lblDate.text];
            [ndf setDateFormat:@"yyyyMMdd"];
            _myOrderDetails.expiry = [ndf stringFromDate:date];
            
            NSString *todayStr = [ndf stringFromDate:[NSDate date]];
            NSDate *todayDate = [ndf dateFromString:todayStr];
            
            // NSLog(@"DATES: %@ %@", date, todayDate);
            
            // validate date if lesser than today
            if( [todayDate timeIntervalSinceDate:date] > 0 ) {
                [_errorMsgLabel setText:[LanguageManager stringForKey:@"Date must be today or later."]];
                [_traderWorking stopAnimating];
                [_okBtn setHidden:NO];
                return;
            }
            
        }
        
        if ([_minQtyView isUserInteractionEnabled]) {
            
            if ([_minquantity.text length]<=0) { // not entered
                ; // if not entered, then minQty no need to add
            } else {
                _myOrderDetails.minQuantity = [NSString stringWithFormat:@"%lld", [_minquantity.text longLongValue]*qtyFactor];
            }
        }
        
        if ([_trigPriceView isUserInteractionEnabled]) {
            if ([_triggerPrice.text length]<=0) { // not entered
                ; // if not entered, then  no need to add
            } else {
                    if ([UserPrefConstants singleton].pointerDecimal==3) {
                _myOrderDetails.trigger_price = [NSString stringWithFormat:@"%.3f", [_triggerPrice.text doubleValue]];
                    }else{
                         _myOrderDetails.trigger_price = [NSString stringWithFormat:@"%.4f", [_triggerPrice.text doubleValue]]; 
                    }
            }
            
            if (_myOrderDetails.trigger_price.floatValue <= 0 && !allowNegativePrice) {
                    [_errorMsgLabel setText:[LanguageManager stringForKey:@"Trigger Price cannot be less than or equal to 0."]];
                    [_traderWorking stopAnimating];
                    [_okBtn setHidden:NO];
                    return;
            }
        }
        
        if ([_triggerVIew isUserInteractionEnabled]) {
            _myOrderDetails.triggerType = _triggerType.text;
        }
        
        if ([_directionView isUserInteractionEnabled])
            _myOrderDetails.triggerDirection = _triggerDirection.text;
        
        if ([_discloseView isUserInteractionEnabled])  {
            if ([_discloseQty.text length]<=0) { // not entered
                ; // if not entered, then minQty no need to add

            } else {
                _myOrderDetails.disclosedQuantity = [NSString stringWithFormat:@"%lld", [_discloseQty.text longLongValue]*qtyFactor];
            }
        }
        
        
    
    } else { // SIMPLE ORDER

        _myOrderDetails.order_type = @"Limit"; // Limit
        _myOrderDetails.validity = @"Day";
    }
    
    
    if (_isShortSell) {
        _myOrderDetails.shortSellState = YES;
    }
    
    
    

    
    
    [UserPrefConstants singleton].userTradeConfirmData =[UserPrefConstants singleton].userSelectedAccount;
    [UserPrefConstants singleton].orderDetails =_myOrderDetails;
    
    [self doRDSAgreement];
    
}


- (void) doRDSAgreement {
//    if ([UserPrefConstants singleton].isEnableRDS &&
//        ([[UserPrefConstants singleton].rdsData.rdsStatus isEqualToString:@"N"] ||
//         [[UserPrefConstants singleton].rdsData.rdsStatus isEqualToString:@"n"]) &&
//        ![[UserPrefConstants singleton].localExchgList containsObject:[UserPrefConstants singleton].userCurrentExchange]) {
//        
//        NSLog(@"doRDS");
    
        // Handle RDS
        
//        labelRdsTitle.text = [AppConfigManager getRDSStatus_N_Title];
//        NSURL *url = [NSURL URLWithString:[appData.userAccountInfo.rdsData.rdsURL stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
//        NSMutableURLRequest *req = [[[NSMutableURLRequest alloc] initWithURL:url] autorelease];
//        if ([[GlobalVariables sharedVariables].systemVersion intValue] < 5) {
//        }
//        else {
//            [rdsWebView.scrollView setBounces:NO];
//        }
//        [rdsWebView loadRequest:req];
//        rdsView.alpha = 1;
//        
//    }
//    else {
        [self do1FA2FARequest];
//    }
}


- (void) do1FA2FARequest {
    
    //NSLog(@"byPASS2FA: %@", [UserPrefConstants singleton].byPass2FA);
    
    if ([[UserPrefConstants singleton].byPass2FA rangeOfString:@"ALL" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        //1FA
        [self show1FAAlert:YES];
    }
    else {
        if ([UserPrefConstants singleton].is2FARequired) {
            if (![UserPrefConstants singleton].verified2FA) {
                 NSLog(@"deviceListForSMSOTP %lu",(unsigned long)[[UserPrefConstants singleton].deviceListForSMSOTP count]);
                if ([[UserPrefConstants singleton].deviceListFor2FA count] != 0 && [[UserPrefConstants singleton].deviceListForSMSOTP count] !=0) {
                    //2FA
                    [self showBothOTPAlert];
                }
                
                else if ([[UserPrefConstants singleton].deviceListFor2FA count] != 0) {
                    //2FA
                    [self show2FAAlert];
                }
                else if([[UserPrefConstants singleton].deviceListForSMSOTP count] !=0) {
                   
                    [self showSMSOTPAlert];
                }
                else {
                    if ([[UserPrefConstants singleton].byPass2FA rangeOfString:@"Y" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        //1FA
                        
                        [self show1FAAlert:YES];
  
                    }
                    else {
                        //Error Message
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[LanguageManager stringForKey:@"CIMBOneTokenMessage"] delegate:nil cancelButtonTitle:@"OK"	otherButtonTitles:nil];
                        [alert show];
                        return;
                    }
                }
            }
            else {
                
                [UserPrefConstants singleton].orderDetails.otpPin = [UserPrefConstants singleton].dvData2FA.otpPin;
                [UserPrefConstants singleton].orderDetails.deviceID = [UserPrefConstants singleton].dvData2FA.deviceID;
                if (_btnSkipConfirmation.selected) {
                    [self doTrade];
                } else {
                    [self showConfirmation];
                }
            }
        }
        else {
            [self doTradingPinRequest];
        }
    }

}



-(void)showConfirmation {
    
    [_boxView setHidden:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    skipVC = [storyboard instantiateViewControllerWithIdentifier:@"SkipConfirmationViewController"];
    skipVC.modalPresentationStyle= UIModalPresentationFormSheet;
    skipVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    skipVC.preferredContentSize =CGSizeMake(320, 570);
    skipVC.skipDelegate =self;
    
    [self presentViewController:skipVC animated:YES completion:nil];
    [skipVC loadData:[UserPrefConstants singleton].userTradeConfirmData.client_account_number
        orderdetails:_myOrderDetails tradeStat:_myTradeStatus fullMode:padLarge
   withStockCurrency:_stockCurrency];
    
    
    
}


- (void)show1FAAlert:(BOOL)promptNotice {
    
    [_boxView setHidden:YES];
    
    if (promptNotice) {
    
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[LanguageManager stringForKey:@"Important Notice"]
                                                                       message:[LanguageManager stringForKey:@"RegisterOneKeyFor2FAMsg"] preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *btnRegister = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Register Now"]
                                                              style:UIAlertActionStyleDefault
            handler:^(UIAlertAction *action) {
                [_boxView setHidden:NO];
                
                if ([[UserPrefConstants singleton].Register2FAPgURL length]<=0) {
                    // URL not setup in PLIST
                } else {
                    [self clearLoadingView];
                    NSURL *url = [NSURL URLWithString:[[UserPrefConstants singleton].Register2FAPgURL stringByAddingPercentEscapesUsingEncoding:NSISOLatin1StringEncoding]];
                    [[UIApplication sharedApplication] openURL:url];
                }
            }];
        
        UIAlertAction *btnLater = [UIAlertAction actionWithTitle:[LanguageManager stringForKey:@"Later"]
                                                           style:UIAlertActionStyleDefault
            handler:^(UIAlertAction *action) {
                [self show1FAAlert:NO];
            }];
        

         [alert addAction:btnRegister]; [alert addAction:btnLater];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_1FA;
    [self presentViewController:validVC animated:YES completion:nil];
    
}

#pragma mark - Validate1FA2FAandPIN prompt delegate

-(void)validatePINcancelClicked {
    [_boxView setHidden:NO];
    [self clearLoadingView];
}


-(void)validatePINokBtnClickedWithPassword:(NSString*)passwd  andType:(ValType)type {
    
     dispatch_async(dispatch_get_main_queue(), ^{
         
         [_boxView setHidden:NO];
    
         switch (type) {
             case VAL_1FA:{
                 [atp doValidate1FAPassword:passwd];
                 [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating Password..."]];
                 [_loadingView setHidden:NO];
                 [_traderWorking startAnimating];
                 [_okBtn setHidden:YES];
             } break;
             case VAL_2FA:{
                 
                 [UserPrefConstants singleton].dvData2FA.otpPin = passwd;
                 [atp validate2FAPinForDevice:[UserPrefConstants singleton].dvData2FA forTrade:YES];
                 [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                 [_loadingView setHidden:NO];
                 [_traderWorking startAnimating];
                 [_okBtn setHidden:YES];
             } break;
             case VAL_PIN:{
                 [UserPrefConstants singleton].orderDetails.pin = passwd;
                 if (_btnSkipConfirmation.selected) {
                     [self doTrade];
                 } else {
                     [self showConfirmation];
                 }
                 
             } break;
            case VAL_SMS:
                [UserPrefConstants singleton].orderDetails.pin = passwd;
                 [atp doValidateSMSOTP:passwd andTrade:YES];
                 [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                 [_loadingView setHidden:NO];
                 [_traderWorking startAnimating];
                 [_okBtn setHidden:YES];break;
                 
            case  VAL_SMS_PIN:
                 
                 if ([UserPrefConstants singleton].currentOTPView==0) {
                     [UserPrefConstants singleton].dvData2FA.otpPin = passwd;
                     [atp validate2FAPinForDevice:[UserPrefConstants singleton].dvData2FA forTrade:YES];
                     [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                     [_loadingView setHidden:NO];
                     [_traderWorking startAnimating];
                     [_okBtn setHidden:YES];
                 }else{
                     [UserPrefConstants singleton].orderDetails.pin = passwd;
                     [atp doValidateSMSOTP:passwd andTrade:YES];
                     [_errorMsgLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
                     [_loadingView setHidden:NO];
                     [_traderWorking startAnimating];
                     [_okBtn setHidden:YES];
                 }
                 
                 break;
             default:
                 break;
         }
      
     });
}

- (void) show2FAAlert {
    
    [_boxView setHidden:YES];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_2FA;
    [self presentViewController:validVC animated:YES completion:nil];
}

- (void) showSMSOTPAlert {
    
    [_boxView setHidden:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_SMS;
    [self presentViewController:validVC animated:YES completion:nil];
}

- (void) showBothOTPAlert {
    
    [_boxView setHidden:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_SMS_PIN;
    [self presentViewController:validVC animated:YES completion:nil];
}

- (void) doTradingPinRequest {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    

    if (![UserPrefConstants singleton].skipPin) {
        if (([def objectForKey:@"TradingPin"] == NO)||([def objectForKey:@"TradingPin"]==nil)) {
            [self showTradingPinAlert];
        }
        else {
            [UserPrefConstants singleton].orderDetails.pin = [def objectForKey:@"TradingPin"];
            if (_btnSkipConfirmation.selected) {
                [self doTrade];
            } else {
                [self showConfirmation];
            }
        }
    }
    else {
        if (_btnSkipConfirmation.selected) {
            [self doTrade];
        } else {
            [self showConfirmation];
        }
    }
}

- (void) showTradingPinAlert {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_PIN;
    [self presentViewController:validVC animated:YES completion:nil];
    
}


//MARK:- For Resize TraderView

//4
-(IBAction)fnBtnShowPicker:(UIButton*)sender
{
    iCurrentFocusPicker  = (int)sender.tag;
    
    self.pickerContainter.hidden = NO;
 
    //switch content. _selectorPickerSmallSize
 
    [_selectorPickerSmallSize reloadAllComponents];
    
    //fill updata
    
    switch (sender.tag) {
        case 10:
            {
                if (![btnOrderType.currentTitle isEqualToString:@""]) {
                    [_selectorPickerSmallSize selectRow: [_orderTypeList indexOfObject: btnOrderType.currentTitle]
                                            inComponent:0 animated:YES];
                }
            }
            break;
        case 11:
        {
            if (![btnValidity.currentTitle isEqualToString:@""]) {
                [_selectorPickerSmallSize selectRow: [_orderValidityList indexOfObject: btnValidity.currentTitle]
                                        inComponent:0 animated:YES];

            }
        }
            break;
        case 12:
        {
            if (![btnCurrency.currentTitle isEqualToString:@""]) {
                [_selectorPickerSmallSize selectRow: [_currencyList indexOfObject: btnCurrency.currentTitle]
                                        inComponent:0 animated:YES];

            }
        }
            break;
        case 13:
        {
            if (![btnPayment.currentTitle isEqualToString:@""]) {
                [_selectorPickerSmallSize selectRow: [_paymentList indexOfObject: btnPayment.currentTitle]
                                        inComponent:0 animated:YES];
            }
        }
            break;

        default:
            break;
    }
}

-(IBAction)fnDoneSelectedPicker:(UIButton*)sender
{
    self.pickerContainter.hidden = TRUE;
}

@end
