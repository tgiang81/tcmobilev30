//
//  DevToolViewController.h
//  TCiPad
//
//  Created by n2n on 14/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserPrefConstants.h"


/*
 NOTE:
 
 I added this so we can check any of the app plist settings like which user is logged in,
 atp server, vertx server URL, etc. You can add more data as you need.
 
 To activate this menu, long tap on the left panel of Settings page (>5 seconds) and
 a menu item "Dev Tool" will appear. Click on it to open the data displayer on the right
 and the menu item will be immediately hidden.
 
 */


@interface DevToolViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *devTable;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) UserPrefConstants *UPC;

@end
