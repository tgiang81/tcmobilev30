//
//  NewsModalViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 10/8/15.
//  Copyright (c) 2015 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExchangeData.h"
#import "LanguageManager.h"

@interface NewsModalViewController : UIViewController <UIWebViewDelegate,UIGestureRecognizerDelegate>
    
@property (strong, nonatomic) IBOutlet UIView *newsContainerView;
@property (nonatomic, strong) IBOutlet UIWebView *newsWebView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) IBOutlet UIButton *closebUtton;
@property (strong, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (strong, nonatomic) IBOutlet UIView *fundamentalView;
@property (strong, nonatomic) ExchangeData *currentEd;
@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;

- (void) openUrl:(NSString *)urlString withJavascript:(BOOL)jsEnabled;
- (IBAction)closeWebView:(id)sender;
- (IBAction)smallerFont:(id)sender;
- (IBAction)biggerFont:(id)sender;

@end
