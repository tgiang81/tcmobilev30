//
//  Validate1FAViewController.m
//  TCiPad
//
//  Created by n2n on 30/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "ValidatePINViewController.h"
#import "UserPrefConstants.h"
#import "SCLAlertView.h"
#import "UIViewController+Popup.h"
#import "ThemeManager.h"
#import "LanguageKey.h"
@interface ValidatePINViewController (){
    NSTimer *smsTimer;
    int timerCount;
    CGFloat defaultCornerRadius;
}

@end

@implementation ValidatePINViewController

@synthesize delegate;


-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
 
//    [_passTextField becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    defaultCornerRadius = 4;
    timerCount = [UserPrefConstants singleton].smsOTPInterval;
    self.myPickerView =[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 250, 200)];
    self.myPickerView.dataSource = self;
    self.myPickerView.delegate = self;
    
    [_okBtn setEnabled:NO];
    
    _okBtn.layer.cornerRadius = kButtonRadius*_okBtn.frame.size.height;
    _okBtn.clipsToBounds = YES;
    _cancelBtn.layer.cornerRadius = kButtonRadius*_cancelBtn.frame.size.height;
    _cancelBtn.clipsToBounds = YES;
    
    if(IS_IPHONE){
        _okBtn.layer.cornerRadius = defaultCornerRadius;
        _cancelBtn.layer.cornerRadius = defaultCornerRadius;
        
    }
    
    switch (_validatingType) {
        case VAL_1FA:{
            [_smsOTPBtn setHidden:YES];
            [_pinOTPBtn setHidden:YES];
            [_keyTextField setHidden:YES];
            [_dvDataView setHidden:YES];
            [_smsOTPLabel setHidden:YES];
            [_requestSMSTOPBtn setHidden:YES];
            [_smsOTPPinTextFiled setHidden:YES];
            _userNameLabel.text = [LanguageManager stringForKey:@"TradeUserName"
                                            withPlaceholders:@{@"%userName%":[UserPrefConstants singleton].userName}];
            _myTitle.text = [LanguageManager stringForKey:@"Enter Login ID Password"];
            _passTextField.placeholder = [LanguageManager stringForKey:@"Enter Password"];
            _passTextField.keyboardType = UIKeyboardTypeAlphabet;
            [_v_rememberPin setHidden:YES];
             [_infoSMSButton setHidden:YES];
            if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
                [self.delegate isDisableRequestSMSTOPBtn:YES];
            }
        }break;
        case VAL_2FA:{
            [_smsOTPBtn setHidden:YES];
            [_pinOTPBtn setHidden:YES];
            [_keyTextField setHidden:YES];
            [_dvDataView setHidden:NO];
            _userNameLabel.text = @"";
                [_smsOTPLabel setHidden:YES];
            [_requestSMSTOPBtn setHidden:YES];
            [_smsOTPPinTextFiled setHidden:YES];
            _myTitle.text = [LanguageManager stringForKey:@"OTP Pin"];
            _passTextField.placeholder = [LanguageManager stringForKey:@"Please Enter Your Pin Number"];
            [self pickerView:_myPickerView didSelectRow:0 inComponent:0];
            _passTextField.keyboardType = UIKeyboardTypeNumberPad;
            [_v_rememberPin setHidden:YES];
             [_infoSMSButton setHidden:YES];
            if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
                [self.delegate isDisableRequestSMSTOPBtn:YES];
            }
        }break;
        case VAL_PIN:{
            [_smsOTPBtn setHidden:YES];
            [_pinOTPBtn setHidden:YES];
            [_keyTextField setHidden:YES];
            [_requestSMSTOPBtn setHidden:YES];
            [_smsOTPPinTextFiled setHidden:YES];
            [_dvDataView setHidden:YES];
            _userNameLabel.text = @"";
              [_smsOTPLabel setHidden:YES];
            _myTitle.text = [LanguageManager stringForKey:@"Trading Pin"];
            _passTextField.placeholder = [LanguageManager stringForKey:@"Please Enter Your Pin Number"];
            _passTextField.keyboardType = UIKeyboardTypeNumberPad;
            [_v_rememberPin setHidden:NO];
             [_infoSMSButton setHidden:YES];
            if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
                [self.delegate isDisableRequestSMSTOPBtn:YES];
            }
        }break;
        case VAL_SMS:{
      
            _dvDataLabel.text = [[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:0];
            
            [_smsOTPBtn setHidden:YES];
            [_pinOTPBtn setHidden:YES];
            [_keyTextField setHidden:NO];
            [_passTextField setHidden:NO];
            [_passTextField setHidden:YES];
            [_requestSMSTOPBtn setHidden:NO];
            [_smsOTPPinTextFiled setHidden:NO];
            _myTitle.text = [LanguageManager stringForKey:@"Enter SMS OTP Pin"];
           _smsOTPPinTextFiled.keyboardType = UIKeyboardTypeNumberPad;
            _smsOTPPinTextFiled.placeholder = [LanguageManager stringForKey:@"Please Enter Your Pin Number"];
            [_v_rememberPin setHidden:YES];
            [_smsOTPLabel setHidden:NO];
            [_smsOTPLabel setText:[NSString stringWithFormat:@"If you do not receive your OTP via SMS, please try again in: %d seconds.",[UserPrefConstants singleton].smsOTPInterval]];
            [_smsOTPLabel setHidden:YES];
            [_dvDataView setHidden:NO];
             [_infoSMSButton setHidden:NO];
            if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
                [self.delegate isDisableRequestSMSTOPBtn:NO];
            }
        }
            break;
        case VAL_SMS_PIN:{
            if ([UserPrefConstants singleton].currentOTPView==0) {
                [_smsOTPBtn setHidden:NO];
                [_pinOTPBtn setHidden:NO];
                [_smsOTPBtn setEnabled:YES];
                [_pinOTPBtn setEnabled:NO];
                [_keyTextField setHidden:YES];
                [_dvDataView setHidden:NO];
                _userNameLabel.text = @"";
                [_smsOTPLabel setHidden:YES];
                [_requestSMSTOPBtn setHidden:YES];
                [_smsOTPPinTextFiled setHidden:YES];
                _myTitle.text = [LanguageManager stringForKey:@"OTP Pin"];
                _passTextField.placeholder = [LanguageManager stringForKey:@"Please Enter Your Pin Number"];
                [self pickerView:_myPickerView didSelectRow:0 inComponent:0];
                _passTextField.keyboardType = UIKeyboardTypeNumberPad;
                [_v_rememberPin setHidden:YES];
                 [_infoSMSButton setHidden:YES];
                if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
                    [self.delegate isDisableRequestSMSTOPBtn:YES];
                }
            }else{
                _dvDataLabel.text = [[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:0];
                [_smsOTPBtn setEnabled:NO];
                [_pinOTPBtn setEnabled:YES];
                [_smsOTPBtn setHidden:NO];
                [_pinOTPBtn setHidden:NO];
                [_keyTextField setHidden:NO];
                [_passTextField setHidden:YES];
                [_requestSMSTOPBtn setHidden:NO];
                [_smsOTPPinTextFiled setHidden:NO];
                _myTitle.text = [LanguageManager stringForKey:@"Enter SMS OTP Pin"];
                  _smsOTPPinTextFiled.keyboardType = UIKeyboardTypeNumberPad;
                _smsOTPPinTextFiled.placeholder = [LanguageManager stringForKey:@"Please Enter Your Pin Number"];
                [_v_rememberPin setHidden:YES];
                [_smsOTPLabel setHidden:NO];
                [_smsOTPLabel setText:[NSString stringWithFormat:[LanguageManager stringForKey:@"If you do not receive your OTP via SMS, please try again in: %d second(s)."],[UserPrefConstants singleton].smsOTPInterval]];
                [_smsOTPLabel setHidden:YES];
                [_dvDataView setHidden:NO];
                 [_infoSMSButton setHidden:NO];
                if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
                    [self.delegate isDisableRequestSMSTOPBtn:NO];
                }
            }
        }break;
        default:
            break;
    }
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelTapped:) name:@"dismissAllModalViews" object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestSMSOTPPinForDeviceDone:) name:@"requestSMSOTPPinForDeviceDone" object:nil];
    
    [self checkRememberPinState];
}

- (IBAction)requestSMSOTP:(id)sender{
    [[ATPAuthenticate singleton] doRequestSMSOTP];
    [_requestSMSTOPBtn setEnabled:NO];
    [_requestSMSTOPBtn setBackgroundColor:[UIColor darkGrayColor]];
    [_smsOTPLabel setHidden:NO];
    [_smsOTPLabel setText:[NSString stringWithFormat:[LanguageManager stringForKey:@"If you do not receive your OTP via SMS, please try again in: %d second(s)."],timerCount]];
    smsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkTimerTick) userInfo:nil repeats:YES];
}

- (void) checkTimerTick{
    
    timerCount--;
    
    [_smsOTPLabel setText:[NSString stringWithFormat:[LanguageManager stringForKey:@"If you do not receive your OTP via SMS, please try again in: %d second(s)."],timerCount]];
    if (timerCount<=0) {
        [_requestSMSTOPBtn setBackgroundColor:[UIColor blackColor]];
        _requestSMSTOPBtn.enabled = YES;
        timerCount = 0;
        _smsOTPLabel.hidden = YES;
        [smsTimer invalidate];
        smsTimer = nil;
    }
}

- (IBAction)chooseOTPPinTab:(id)sender{
  
    [UserPrefConstants singleton].currentOTPView = 0;
      [self pickerView:_myPickerView didSelectRow:0 inComponent:0];
    [_smsOTPBtn setEnabled:YES];
    [_pinOTPBtn setEnabled:NO];
     [_keyTextField setHidden:YES];
    _myTitle.text = [LanguageManager stringForKey:@"OTP Pin"];
    [_passTextField setHidden:NO];
    [_requestSMSTOPBtn setHidden:YES];
    [_smsOTPPinTextFiled setHidden:YES];
    [_infoSMSButton setHidden:YES];
    if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
        [self.delegate isDisableRequestSMSTOPBtn:YES];
    }
}
- (IBAction)chooseSMSOTPTab:(id)sender{
    [UserPrefConstants singleton].currentOTPView = 1;
      _dvDataLabel.text = [[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:0];
    [_smsOTPBtn setEnabled:NO];
    [_pinOTPBtn setEnabled:YES];
    [_keyTextField setHidden:NO];
     _myTitle.text = [LanguageManager stringForKey:@"Enter SMS OTP Pin"];
    [_passTextField setHidden:YES];
    [_requestSMSTOPBtn setHidden:NO];
    [_smsOTPPinTextFiled setHidden:NO];
     [_infoSMSButton setHidden:NO];
    if([self.delegate respondsToSelector:@selector(isDisableRequestSMSTOPBtn:)]){
        [self.delegate isDisableRequestSMSTOPBtn:NO];
    }
}

- (IBAction)doHelp:(id)sender{
    
  //  [self.delegate showSLCAlertPopup];
  //  [_passTextField resignFirstResponder];
    
    UIButton *rect = (UIButton*)sender;
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"PopUp"];
    
    UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:controller];
    [popup presentPopoverFromRect:rect.frame
                           inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showDvDataSelector:(id)sender {
    UIButton *btn = (UIButton*)sender;
    UIViewController *temp =[[UIViewController alloc]init];
    
    [temp.view addSubview:_myPickerView];
    [_myPickerView reloadAllComponents];
    self.popoverController= temp.popoverPresentationController;
    temp.preferredContentSize= CGSizeMake(250,200);
    if (self.popoverController)
    {
        self.popoverController.sourceView = btn;
        self.popoverController.sourceRect = btn.bounds;
        self.popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    self.columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:temp];
    [self.columnPickerPopover presentPopoverFromRect:btn.frame
                                         inView:btn.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)passwordChanged:(id)sender {
    
    if (_validatingType==VAL_SMS_PIN) {
        
        if ([UserPrefConstants singleton].currentOTPView==0) {
            if ([_passTextField.text length]<=0) {
                [_okBtn setEnabled:NO];
            } else {
                [_okBtn setEnabled:YES];
            }
        }else{
            if ([_smsOTPPinTextFiled.text length]<=0) {
                [_okBtn setEnabled:NO];
            } else {
                [_okBtn setEnabled:YES];
            }
        }
        
    }else
    
    if (_validatingType==VAL_SMS) {
        if ([_smsOTPPinTextFiled.text length]<=0) {
            [_okBtn setEnabled:NO];
        } else {
            [_okBtn setEnabled:YES];
        }
    }else{
        if ([_passTextField.text length]<=0) {
            [_okBtn setEnabled:NO];
        } else {
            [_okBtn setEnabled:YES];
        }
    }
    
}

- (IBAction)okTapped:(id)sender {

    [[ATPAuthenticate singleton] timeoutActivity];

    [_passTextField resignFirstResponder];
    
    if (![_v_rememberPin isHidden]) {
        
        if ([_passTextField.text length]!=6) {
            [[UserPrefConstants singleton] popMessage:[LanguageManager stringForKey:@"Trading pin must be 6 digits."]
                                         inController:self withTitle:@""];
            return;
        }
    }
    
    if(IS_IPAD){
        [self dismissViewControllerAnimated:YES completion:^(void) {
            [self actionAfterClosing];
        }];
    }else{
        [self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
            [self actionAfterClosing];
        }];
    }
    
    
}

- (void)actionAfterClosing{
    if (_validatingType==VAL_SMS_PIN) {
        
        if ([UserPrefConstants singleton].currentOTPView==0) {
            [self.delegate validatePINokBtnClickedWithPassword:_passTextField.text andType:_validatingType];
        }else{
            [self.delegate validatePINokBtnClickedWithPassword:_smsOTPPinTextFiled.text andType:_validatingType];
        }
        
    }else
        
        if (_validatingType==VAL_SMS) {
            [self.delegate validatePINokBtnClickedWithPassword:_smsOTPPinTextFiled.text andType:_validatingType];
        }else{
            [self.delegate validatePINokBtnClickedWithPassword:_passTextField.text andType:_validatingType];
        }
}

- (IBAction)cancelTapped:(id)sender {
    [[ATPAuthenticate singleton] timeoutActivity];
    
    [_passTextField resignFirstResponder];
    
    if(IS_IPAD){
        [self dismissViewControllerAnimated:YES completion:^(void) {
            [self.delegate validatePINcancelClicked];
        }];
    }else{
        [self dismissPopupWithCompletion:^(MZFormSheetController *formSheetController) {
            [self.delegate validatePINcancelClicked];
        }];
    }
    
    
}

- (IBAction)toggleRememberPin:(id)sender {
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if([def objectForKey:@"TradingPin"]==nil)
    {
        [def setObject:_passTextField.text forKey:@"TradingPin"];
        [_btn_RememberPin setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
    }
    else
    {
        [def removeObjectForKey:@"TradingPin"];
        [_btn_RememberPin setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
    [def synchronize];
    
}
- (void)checkRememberPinState{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    if([def objectForKey:@"TradingPin"]==nil)
    {
        [_btn_RememberPin setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
    else
    {
        _passTextField.text = [def objectForKey:@"TradingPin"];
        [_btn_RememberPin setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
    }
}
- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:@"requestSMSOTPPinForDeviceDone"];
     [[NSNotificationCenter defaultCenter] removeObserver:@"dismissAllModalViews"];

}


#pragma mark - PickerView


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return  30;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (_validatingType==VAL_SMS_PIN) {
        if ([UserPrefConstants singleton].currentOTPView==0) {
            return [[UserPrefConstants singleton].deviceListFor2FA count];
        }else{
             return [[UserPrefConstants singleton].deviceListForSMSOTP count];
        }
    }else
    if (_validatingType==VAL_SMS) {
        return [[UserPrefConstants singleton].deviceListForSMSOTP count];
    }else{
        return [[UserPrefConstants singleton].deviceListFor2FA count];
    }
  
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (_validatingType==VAL_SMS_PIN) {
         if ([UserPrefConstants singleton].currentOTPView==0) {
             DeviceDataFor2FA *dat = [[UserPrefConstants singleton].deviceListFor2FA objectAtIndex:row];
             return dat.deviceID;
         }else{
             NSString *dat =[[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:row];
             return dat;
         }
    }else
    
       if (_validatingType==VAL_SMS) {
           
           NSString *dat =[[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:row];
           return dat;
           
       }else{
           DeviceDataFor2FA *dat = [[UserPrefConstants singleton].deviceListFor2FA objectAtIndex:row];
           return dat.deviceID;
       }
 
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (_validatingType==VAL_SMS_PIN) {
        if ([UserPrefConstants singleton].currentOTPView==0) {
            DeviceDataFor2FA *dat = [[UserPrefConstants singleton].deviceListFor2FA objectAtIndex:row];
            _dvDataLabel.text = dat.deviceID;
            [UserPrefConstants singleton].dvData2FA = dat;
        }else{
            NSString *dat =[[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:row];
            _dvDataLabel.text = dat;
        }
    }
    else if (_validatingType==VAL_SMS) {
         NSString *dat =[[UserPrefConstants singleton].deviceListForSMSOTP objectAtIndex:row];
         _dvDataLabel.text = dat;
     }else{
         DeviceDataFor2FA *dat = [[UserPrefConstants singleton].deviceListFor2FA objectAtIndex:row];
         _dvDataLabel.text = dat.deviceID;
         [UserPrefConstants singleton].dvData2FA = dat;
     }
}

-(void)requestSMSOTPPinForDeviceDone:(NSNotification*)notification {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSDictionary *replyDict = notification.userInfo;
        NSString *statusCode = [replyDict objectForKey:@"statusCode"];
        NSString *statusMsg = [replyDict objectForKey:@"statusMsg"];
        NSString *errorMsg = [replyDict objectForKey:@"errorMsg"];
         NSString *messageSMSOTP = [replyDict objectForKey:@"messageSMSOTP"];
        
        // statuscode 0 is success for CIMB SG. WTF
        if ([statusCode isEqualToString:@"200"]||[statusCode isEqualToString:@"0"]) {
            
            _keyTextField.text = messageSMSOTP;
            [UserPrefConstants singleton].smsOTPHeaderKey = messageSMSOTP;
            
            
        } else {
            
            //TODO   [_loadingActivity stopAnimating];
            //TODO   _errorMessageLabel.text = @"";
            
            // alert wrong password
            if ([errorMsg length]<=0) errorMsg = statusMsg;
            [[UserPrefConstants singleton] popMessage:[NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg]
                                         inController:self withTitle:[LanguageManager stringForKey:TC_Pro_Mobile]];
            
        }
        
    });
}


- (CGFloat)redundentHeight{
    CGFloat total = 0;
    
    if([_pinOTPBtn isHidden] && [_smsOTPBtn isHidden]){
        total += 37;
    }
    if([_myTitle isHidden]){
        total += 48;
    }
    if(([_userNameLabel isHidden] || [_userNameLabel.text isEqualToString:@""]) && [_dvDataView isHidden]){
        total += 40;
    }
    if([_passTextField isHidden] && [_keyTextField isHidden]){
        total += 30;
    }
    if([_rememberPin isHidden] && [_smsOTPLabel isHidden]){
        total += 50;
    }
    if([_requestSMSTOPBtn isHidden]){
        total += 60;
        self.cst_Bottom.constant = -60;
    }
        return total;
}

@end
