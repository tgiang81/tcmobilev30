//
//  StockPreViewController.h
//  TCiPad
//
//  Created by n2n on 17/11/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuoteScreenViewController.h"
#import "ATPAuthenticate.h"
#import "MutualDelegate.h"
#import "TraderViewController.h"
#import "VertxConnectionManager.h"
#import "OrderBookandPortfolioViewController.h"
#import "ImgChartManager.h"
#import "LanguageManager.h"
#import "MDInfo.h"
#import "GeneralSelectorViewController.h"


@interface StockPreViewController : UIViewController <QuoteScreenDelegate, UIGestureRecognizerDelegate,
                    PortfolioDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource,
                    GeneralSelectorDelegate>

// GENERAL ITEMS
@property (weak, nonatomic) IBOutlet UIPageControl *pgControl;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *btnLoading;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;
@property (weak, nonatomic) id <StockPreViewDelegate> delegate;
@property (strong ,nonatomic) NSString *controller;

// STK DETAILS
@property (weak, nonatomic) IBOutlet UIScrollView *detailScroll;
@property (weak, nonatomic) IBOutlet UIView *detailsView;
@property (weak, nonatomic) IBOutlet UIView *detail2View;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *stockName;
@property (weak, nonatomic) IBOutlet UILabel *stockCode;

// LEFT SIDE
@property (weak, nonatomic) IBOutlet UILabel *stockLastDone;
@property (weak, nonatomic) IBOutlet UILabel *stockChange;
@property (weak, nonatomic) IBOutlet UILabel *stockChangePer;
@property (weak, nonatomic) IBOutlet UILabel *stockOpen;
@property (weak, nonatomic) IBOutlet UILabel *stockPrevClose;
@property (weak, nonatomic) IBOutlet UILabel *stockHigh;
@property (weak, nonatomic) IBOutlet UILabel *stockLow;
@property (weak, nonatomic) IBOutlet UILabel *stockCeiling;
@property (weak, nonatomic) IBOutlet UILabel *stockFloor;
@property (weak, nonatomic) IBOutlet UILabel *stockLacp;
@property (weak, nonatomic) IBOutlet UILabel *stockVolume;
@property (weak, nonatomic) IBOutlet UILabel *stockValue;
@property (weak, nonatomic) IBOutlet UILabel *stockTrades;
@property (weak, nonatomic) IBOutlet UILabel *stockSector;
@property (weak, nonatomic) IBOutlet UILabel *stockStatus;

// RIGHT SIDE
@property (weak, nonatomic) IBOutlet UILabel *stockBid;
@property (weak, nonatomic) IBOutlet UILabel *stockBidQty;
@property (weak, nonatomic) IBOutlet UILabel *stockAsk;
@property (weak, nonatomic) IBOutlet UILabel *stockAskQty;
@property (weak, nonatomic) IBOutlet UILabel *stockBuyTotal;
@property (weak, nonatomic) IBOutlet UILabel *stockSellTotal;
@property (weak, nonatomic) IBOutlet UILabel *stockBuyTrans;
@property (weak, nonatomic) IBOutlet UILabel *stockSellTrans;
@property (weak, nonatomic) IBOutlet UILabel *stockTradingBoard;
@property (weak, nonatomic) IBOutlet UILabel *stockSellVolume;
@property (weak, nonatomic) IBOutlet UILabel *stockISIN;
@property (weak, nonatomic) IBOutlet UILabel *stockParVal;
@property (weak, nonatomic) IBOutlet UILabel *stockLotSize;
@property (weak, nonatomic) IBOutlet UILabel *stockShareIssued;
@property (weak, nonatomic) IBOutlet UILabel *stockMktCap;
@property (weak, nonatomic) IBOutlet UILabel *stockShareIssuedTitle;
@property (weak, nonatomic) IBOutlet UILabel *stockMktCapTitle;

// WATCHLIST
@property (nonatomic, strong) NSArray *sortedWLArr;

// BOTTOM BUTTONS
@property (weak, nonatomic) IBOutlet UIButton *btnBuy;
@property (weak, nonatomic) IBOutlet UIButton *btnSell;
@property (weak, nonatomic) IBOutlet UIButton *watchListBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnStkDetail;

// CHARTS (IMG/MODULUS/TELETRADER)
@property (weak, nonatomic) IBOutlet UIView *chartView;
@property (nonatomic, assign) CGRect parentViewFrame;
@property (weak, nonatomic) IBOutlet UIImageView *imgChart;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *chartErrorMsg;

// FUNDAMENTALS
@property (weak, nonatomic) IBOutlet UILabel *fundamentalTitle;
@property (weak, nonatomic) IBOutlet UIView *fundamentalView;

// NEWS
@property (strong, nonatomic) NSMutableArray *newsArr;
@property (strong, nonatomic) NSMutableArray *newsArcArr;
@property (weak, nonatomic) IBOutlet UIView *newsView;
@property (weak, nonatomic) IBOutlet UITableView *newsTable;

// BIZ DONE
@property (strong, nonatomic) NSMutableArray *bizDoneTableDataArr;
@property (weak, nonatomic) IBOutlet UIView *bizDoneView;
@property (weak, nonatomic) IBOutlet UITableView *bizDoneTable;
@property (weak, nonatomic) IBOutlet UILabel *bizDoneLastUpdate;


// TIME & SALES
@property (strong, nonatomic) NSMutableArray *timeSalesArr;
@property (weak, nonatomic) IBOutlet UIView *timeSalesView;
@property (weak, nonatomic) IBOutlet UILabel *timeSalesLastUpdate;
@property (weak, nonatomic) IBOutlet UITableView *timeSalesTable;

// MARKET DEPTH
@property (strong, nonatomic) NSMutableArray *mktDepthArr;
@property (strong, nonatomic) NSMutableArray *MDBuyArr;
@property (strong, nonatomic) NSMutableArray *MDBuyPrevArr;
@property (strong, nonatomic) NSMutableArray *MDSellArr;
@property (strong, nonatomic) NSMutableArray *MDSellPrevArr;
@property (weak, nonatomic) IBOutlet UIView *mktDepthView;
@property (weak, nonatomic) IBOutlet UITableView *mktDepthTable;
@property (weak, nonatomic) IBOutlet UILabel *totBidSplit;
@property (weak, nonatomic) IBOutlet UILabel *totBidQty;
@property (weak, nonatomic) IBOutlet UILabel *totBid;
@property (weak, nonatomic) IBOutlet UILabel *totAsk;
@property (weak, nonatomic) IBOutlet UILabel *totAskQty;
@property (weak, nonatomic) IBOutlet UILabel *totAskSplit;
@property (weak, nonatomic) IBOutlet UILabel *totalBidLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAskLabel;
@property (weak, nonatomic) IBOutlet UILabel *lacpLabel;

// REFRESHCONTROL
@property (strong, nonatomic) UIRefreshControl *refreshControl;

-(void) doBlock: ( void (^)(int) ) stkPreviewClick;

- (IBAction)closeStkPreview:(id)sender;
- (IBAction)goTrade:(id)sender;
- (IBAction)addToWatchlist:(UIButton*)sender;
- (IBAction)gotoStockDetails:(id)sender;
- (IBAction)loadChart:(id)sender;
-(void) fnControlButtons :(BOOL) isEnable;

@end
