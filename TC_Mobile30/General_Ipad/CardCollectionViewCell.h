//
//  CardCollectionViewCell.h
//  TCiPad
//
//  Created by n2n on 19/01/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"
#import "QCConstants.h"
#import "ImgChartManager.h"
#import "UserPrefConstants.h"

@protocol CardCollectionDelegate <NSObject>
@optional
-(void)reloadButtonDidClicked:(NSInteger)row;
-(void)buyStockDidClicked:(NSInteger)row;
-(void)sellStockDidClicked:(NSInteger)row;
-(void)goDetailDidClicked:(NSInteger)row;
-(void)addToWatchListClicked:(NSInteger)row atButton:(UIButton*)sender;
-(void)cellGoEditMode:(NSInteger)row;
-(void)detailDidScrollToPage:(NSInteger)page atRow:(NSInteger)row;
@end

@interface CardCollectionViewCell : UICollectionViewCell <UIScrollViewDelegate>


@property (nonatomic,weak) id<CardCollectionDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *stkName;
@property (weak, nonatomic) IBOutlet UIImageView *stkImgChart;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imgLoading;
@property (weak, nonatomic) IBOutlet UILabel *lastDone;
@property (weak, nonatomic) IBOutlet UILabel *change;
@property (weak, nonatomic) IBOutlet UILabel *changePer;
@property (weak, nonatomic) IBOutlet UILabel *volume;
@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UILabel *lacp;
@property (weak, nonatomic) IBOutlet UILabel *high;
@property (weak, nonatomic) IBOutlet UILabel *low;
@property (weak, nonatomic) IBOutlet UILabel *open;
@property (weak, nonatomic) IBOutlet UILabel *trades;
@property (weak, nonatomic) IBOutlet UILabel *prevClose;
@property (weak, nonatomic) IBOutlet UILabel *mktCap;
@property (weak, nonatomic) IBOutlet UILabel *bid;
@property (weak, nonatomic) IBOutlet UILabel *bidQty;
@property (weak, nonatomic) IBOutlet UILabel *ask;
@property (weak, nonatomic) IBOutlet UILabel *askQty;


@property (weak, nonatomic) IBOutlet UIScrollView *detailScroll;

@property (weak, nonatomic) IBOutlet UIButton *reloadBtn;
@property (weak, nonatomic) IBOutlet UIButton *butBtn;
@property (weak, nonatomic) IBOutlet UIButton *sellBtn;
@property (weak, nonatomic) IBOutlet UIButton *favBtn;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (weak, nonatomic) IBOutlet UIPageControl *pgCtrl;

@property (weak, nonatomic) IBOutlet UIView *detail1View;
@property (weak, nonatomic) IBOutlet UIView *detail2View;
@property (strong, nonatomic) UITapGestureRecognizer *doubleTap;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPress;
@property (strong, nonatomic) ImgChartManager *chartManager;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)reloadChartWithDict:(NSDictionary*)dict;
- (IBAction)buyStock:(UIButton*)sender;
- (IBAction)sellStock:(UIButton*)sender;
- (IBAction)addToWatchList:(UIButton*)sender;
- (IBAction)goDetail:(UIButton*)sender;

-(void)populateCellsWithDictionary:(NSDictionary*)dataDict
                          fromDict:(NSDictionary*)oldDict
                            forRow:(long)row
                           doFlash:(BOOL)isUpdate
                        detailPage:(NSInteger)page
                          withCode:(NSString*)stockCode
                       isWatchList:(BOOL)isWatchList;

@end
