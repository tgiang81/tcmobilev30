//
//  ESettlementWebViewController.h
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 19/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ESettlementWebViewController : UIViewController <UIWebViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webview;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityView;
@property (nonatomic, weak) IBOutlet UILabel *titleView;
@property (nonatomic, strong) IBOutlet UIButton *backButton;

- (void) reloadPage:(NSString *)urlLink andTitle:(NSString *)title;

@end

