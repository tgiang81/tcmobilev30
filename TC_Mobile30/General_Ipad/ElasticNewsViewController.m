//
//  ElasticNewsViewController.m
//  TCiPad
//
//  Created by n2n on 13/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "ElasticNewsViewController.h"
#import "ElasticNewsData.h"
#import "NSData-Zlib.h"
#import "UserPrefConstants.h"
#import "NewsMetadata.h"
#import "NewsModalViewController.h"
#import "ATPAuthenticate.h"
#import "LanguageKey.h"
@interface ElasticNewsViewController (){
    ATPAuthenticate *_atp;
}

@end

@implementation ElasticNewsViewController
@synthesize transitionController;

#pragma mark - View Lifecycle delegates


- (void)viewDidLoad {
    [super viewDidLoad];
    _atp = [ATPAuthenticate singleton];
    // Do any additional setup after loading the view.
    
   //  NSString *elasticNews = [NSString stringWithFormat:@"http://%@/[%@]NEWSCATEGORY?[SOURCE]=999|||", [UserP getQCServer], appData.userAccountInfo.userparamQC];
    [self.newsTable setBackgroundColor:[UIColor clearColor]];
    self.newsArray = [[NSMutableArray alloc] init];
    self.categoryArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *tempSourceArray = [[NSMutableArray alloc] init];
    NSLog(@"[UserPrefConstants singleton].elasticCat %@",[UserPrefConstants singleton].elasticCat);
    for (ElasticNewsData *source in [UserPrefConstants singleton].elasticCat) {
          NSLog(@"sourceArrayDist %@",source.sourceString);
        if (source.sourceString.length>0) {
            [tempSourceArray addObject:source.sourceString];
            NSString *stringInformation = [NSString stringWithFormat:@"%@|%@|%@|%@",source.sourceString,source.categoryLevel,source.categoryString,source.newsID];
            
            [self.categoryArray addObject:stringInformation];
        }
       
    }
   // NSLog(@"sourceArrayDist %@",tempSourceArray);
    NSArray *sourceArrayDist = [tempSourceArray valueForKeyPath:@"@distinctUnionOfObjects.self"];
    self.sourceArray = [[NSMutableArray alloc] initWithArray:sourceArrayDist];
    
    [self.sourceArray insertObject:@"ALL" atIndex:0];
    [self.categoryArray insertObject:@"ALL" atIndex:0];
    
    _statusView.layer.borderWidth = 3.0f;
    _statusView.layer.borderColor = [UIColor whiteColor].CGColor;
    _statusView.layer.cornerRadius = 10.0f;
    
    NSDate *fromDate = [NSDate date];//[[NSDate date] dateByAddingTimeInterval:-7*24*60*60];
    _fromDate = fromDate;
    _toDate = [NSDate date];
    sourceID = @"";
    categoryID = @"";
    popupSourceTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    popupSourceTableViewController.tableView.delegate=self;
    popupSourceTableViewController.tableView.dataSource=self;
    [popupSourceTableViewController.tableView setTag:1];
    
    popupCategoryTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    popupCategoryTableViewController.tableView.delegate=self;
    popupCategoryTableViewController.tableView.dataSource=self;
    [popupCategoryTableViewController.tableView setTag:2];
    
    //_dateFromLabel.text = [NSString stringWithFormat:@"%@",[self formatDateWithTimeZone:fromDate]];
    //_dateToLabel.text = [NSString stringWithFormat:@"%@",[self formatDateWithTimeZone:[NSDate date]]];
    
   // [self updateFromDate:nil];
   // [self updateFromDate:nil];
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *fromDateStr=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_fromDate]];
      NSString *toDateStr=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:_toDate]];
    
    _dateFromLabel.text = fromDateStr;
    _dateToLabel.text = toDateStr;
    [self validateDates];
    
    isArticles = YES;
    isHeadLine = YES;
    keywordString = @"";
    
    if (isArticles && isHeadLine) {
        targetID = @"3";
        [self.articlesButton setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        [self.headerButton setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        
    }else if(isArticles && !isHeadLine){
        targetID = @"1";
        [self.articlesButton setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        
        [self.headerButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        
       
    }else if(!isArticles && isHeadLine){
         targetID = @"2";
        [self.articlesButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        
        [self.headerButton setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        
    
    }else{
        targetID = @"";
        [self.articlesButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        [self.headerButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        
    }
   
    [self doAllElasticNews:fromDate toDate:[NSDate date] andSource:@"" andCategory:@"" andKeyword:keywordString andTargetID:targetID];
    
    [self.statusView setHidden:NO];
    _statusLabel.text = [LanguageManager stringForKey:Loading______ ];
    [_loadingActivity startAnimating];
    [_newsTable setHidden:NO];
    
    NSLog(@"Source Array %@",_sourceArray);
}

- (IBAction)clickArticles:(id)sender{
    isArticles = !isArticles;
    if (isArticles) {
        [self.articlesButton setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        
    }else{
        [self.articlesButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)clickHeader:(id)sender{
    isHeadLine = !isHeadLine;
    if (isHeadLine) {
        [self.headerButton setImage:[UIImage imageNamed:@"checkbox_on"] forState:UIControlStateNormal];
        
    }else{
        [self.headerButton setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)formatDateWithTimeZone:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

- (NSString *)qcDecompressionProcess:(NSString *)qcResponseStr {
    
    if ([UserPrefConstants singleton].isQCCompression) {
        NSRange range1 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)13]];
        NSRange range2 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%C", (unichar)129]];
        NSRange range3 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)30]];
        NSRange range4 = [qcResponseStr rangeOfString:[NSString stringWithFormat:@"%c", (unsigned char)10]];
        
        if (range1.location != NSNotFound && range2.location != NSNotFound && range3.location != NSNotFound && range4.location != NSNotFound) {
            
            qcResponseStr = [qcResponseStr substringWithRange:NSMakeRange(range3.location+1, [qcResponseStr length]-range3.location-2)];
            NSString *resultStr = [[NSString alloc] initWithData:[[qcResponseStr dataUsingEncoding:NSISOLatin1StringEncoding] zlibInflate] encoding:NSISOLatin1StringEncoding];
            return resultStr;
        }
        else {
            return qcResponseStr;
        }
    }
    else {
        return qcResponseStr;
    }
}


- (void) doAllElasticNews:(NSDate *)fromDate toDate:(NSDate *)toDate andSource:(NSString *)source andCategory:(NSString *)category andKeyword:(NSString *)keyword andTargetID:(NSString *)targetID{
    _statusView.hidden = NO;
    if (_newsArray.count!=0) {
        [_newsArray removeAllObjects];
    }
    
    NSString *dataUrl = [NSString stringWithFormat:@"http://%@/%@V2NEWS?[KWORD]=%@&[TARGET]=%@&[GUID]=&[CAT]=%@&[SOURCE]=%@&[ACTIVE]=&[FRDT]=%@&[TODT]=%@&[SIZE]=100&[FROM]=0|||", [UserPrefConstants singleton].qcServer, [UserPrefConstants singleton].tempUserParamQC,keyword,targetID,category,source,[self formatDateWithTimeZone:fromDate],[self formatDateWithTimeZone:toDate]];
    
    NSString* webName = [dataUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:webName];
    
    NSLog(@"url elastic News %@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    sessionConfig.timeoutIntervalForRequest = 10.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    
    // Configure Session Configuration
    [sessionConfig setAllowsCellularAccess:YES];
    [sessionConfig setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
    
    // NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    
    NSURLSessionDataTask *downloadTask = [session
                                          dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              // 4: Handle response here
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                NSLog(@"Status code: %ld",(long)[httpResponse statusCode]);
                                                
                                                NSLog(@"dataStr %@",data);
                                               NSString *responseData= [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                              
                                              NSLog(@"dataStr %@",responseData);
                                                NSLog(@"dataStr %lu",(unsigned long)responseData.length);
                                                
                                                if (responseData.length==0) {
                                                    [self performSelector:@selector(addingNewsArray) withObject:nil afterDelay:0.5f];
                                                }
                                              
                                              NSString *content = [self qcDecompressionProcess:responseData];
                                              
                                            
                                              
                                              NSArray *lineArr = [content componentsSeparatedByString:@"\r\n"];
                                              
                                              //Find the index of [] and [END]
                                              int header = -1;
                                              int footer = -1;
                                              for (int j = 0; j < [lineArr count]; j++) {
                                                  NSString *line = [lineArr objectAtIndex:j];
                                                  if ([line isEqualToString:@"[]"]) {
                                                      header = j;
                                                  }
                                                  if ([line isEqualToString:@"[END]"]) {
                                                      footer = j;
                                                  }
                                                  if([line localizedCaseInsensitiveContainsString:@"[END]"]){
                                                      footer = j;
                                                  }
                                                  
                                              }
                                              
                                              
                                              if (header != -1 && footer != -1) {
                                                  for (int i = header+1; i < footer; i++) {
                                                      NSString *dataRow = [lineArr objectAtIndex:i];
                                                      NSArray *tokens = [dataRow componentsSeparatedByString:@","];
                                                      if ([tokens count] >= 3) {
                                                          NewsMetadata *nmd = [[NewsMetadata alloc] init];
                                                          int i = 0;
                                                          for (NSString *token in tokens) {
                                                              switch (i) {
                                                                  case 0: nmd.newsID = token;
                                                                      break;
                                                                  case 1: nmd.codeID = token;
                                                                      break;
                                                                  case 2: nmd.date1 = token;
                                                                      break;
                                                                
                                                                  case 4:
                                                                      //  DLog(@"NEWS Stock Token %@",token);
                                                                      nmd.stockcode = token;
                                                                      break;
                                                                  case 5: nmd.newsTitle = [token stripHtml];
                                                                      break;
                                                                  case 8: nmd.sourceNews = token;
                                                                  default:
                                                                      break;
                                                              }
                                                              i++;
                                                          }
                                                          
                                                          BOOL found = NO;
                                                          for (NewsMetadata *current_nmd in _newsArray) {
                                                              if ([current_nmd.newsID isEqualToString:nmd.newsID]) {
                                                                  found = YES;
                                                              }
                                                          }
                                                          if (!found) {
                                                              
                                                              [_newsArray addObject:nmd];
                                                          }
                                                        
                                                      }
                                                  }
                                                  
                                                  NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date1" ascending:NO];
                                                  [_newsArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                                                  //NSLog(@"All News %@",[UserPrefConstants singleton].allNewsResultList);
                                                 
                                                  
                                                      
                                                      [self performSelector:@selector(addingNewsArray) withObject:nil afterDelay:1.0f];
                                                  
                                              }
                                              
                                                });
                                              
                                          }];
    // 3
    [downloadTask resume];
    
}

- (void) addingNewsArray{
    
    [self.newsTable reloadData];
    _statusView.hidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView Delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag==1) {
            return [self.sourceArray count];
    }else if(tableView.tag==2){
            return [self.categoryArray count];
    }else{
            return _newsArray.count;
    }

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView.tag==1 || tableView.tag == 2) {
        static NSString *SimpleCellIdentifier = @"Cell";
        UITableViewCell *simpleCell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:SimpleCellIdentifier];
        if (simpleCell == nil) {
            simpleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleCellIdentifier];
        }
        simpleCell.textLabel.backgroundColor = [UIColor clearColor];
        simpleCell.textLabel.textColor = [UIColor darkTextColor];
        simpleCell.textLabel.font =  [UIFont systemFontOfSize:16];
        simpleCell.textLabel.numberOfLines = 2;
        simpleCell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
        // NSLog(@"left Count = %d",counter);
        if (tableView.tag==1) {
            NSString *sourceArrayString = [self.sourceArray objectAtIndex:indexPath.row];
            if ([sourceArrayString isEqualToString:@"PH"]) {
                sourceArrayString = @"PSE News";
            }else if([sourceArrayString isEqualToString:@"UTRADE"]){
                sourceArrayString = @"UTrade Research";
            }else if([sourceArrayString isEqualToString:@"ABACUS"]){
                sourceArrayString = @"Abacus Research";
            }else{
                 simpleCell.textLabel.text = [self.sourceArray objectAtIndex:indexPath.row];
            }
            simpleCell.textLabel.text = sourceArrayString;
            
           
            
        }else{
            if (indexPath.row==0) {
                simpleCell.textLabel.text = [self.categoryArray objectAtIndex:0];
            }else{
                NSArray *arrayCategory = [[self.categoryArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
        
                simpleCell.textLabel.text = [arrayCategory objectAtIndex:2];
            }
            
            
        }
        return simpleCell;
    }else{
    
        ElasticNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"newsCellID"];
        if (cell==nil) {
            cell = [[ElasticNewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"newsCellID"];
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        NewsMetadata *nmd;
        
        if (_newsArray.count!=0) {
            nmd = (NewsMetadata *) [_newsArray objectAtIndex:indexPath.row];
            [cell loadData:nmd];
        }else{
            return cell;
        }
        
        
    
    // populate data below
    
        return cell;
    }
}


#pragma mark - Other methods

-(void)updateFromDate:(id)sender {
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:datePickerView.date]];
    _dateFromLabel.text = str;
    _fromDate = datePickerView.date;
    
    [self validateDates];
}

-(void)updateToDate:(id)sender {
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:datePickerView.date]];
    _dateToLabel.text = str;
    _toDate = datePickerView.date;
    
    [self validateDates];
    
}




-(void)validateDates {
    
   // [_searchHistoryBtn setEnabled:NO];
    
    // dont check if any date is still not selected
    if (([_dateToLabel.text length]<=0)||([_dateFromLabel.text length]<=0)) return;
    
    //1. check either date must be less or equal than today
    NSTimeInterval fromDifference = [[NSDate date] timeIntervalSince1970] - [_fromDate timeIntervalSince1970];
    if (fromDifference<0) {
        _dateFromLabel.textColor = [UIColor redColor];
        _errorLabel.text = [LanguageManager stringForKey:@"From Date must be Today or lesser!"];
        return;
    } else {
        _dateFromLabel.textColor = [UIColor whiteColor];
    }
    
    NSTimeInterval toDifference = [[NSDate date] timeIntervalSince1970] - [_toDate timeIntervalSince1970];
    if (toDifference<0) {
        _dateToLabel.textColor = [UIColor redColor];
        _errorLabel.text = [LanguageManager stringForKey:@"To Date must be Today or lesser!"];
        return;
    } else {
        _dateToLabel.textColor = [UIColor whiteColor];
    }
    
    //2. check fromDate must be less than toDate
    NSTimeInterval difference = [_toDate timeIntervalSince1970] - [_fromDate timeIntervalSince1970];
    if (difference<0) {
        _dateFromLabel.textColor = _dateToLabel.textColor = [UIColor redColor];
        _errorLabel.text = [LanguageManager stringForKey:@"From Date must be less than To Date!"];
        return;
    } else {
        // pass
        _dateFromLabel.textColor = _dateToLabel.textColor = [UIColor whiteColor];
    }
    
    // 3. check date range must be 30 days.
//    NSInteger orderLimitDays = [[UserPrefConstants singleton].orderHistLimit integerValue];
//    NSTimeInterval limitDays = (1+orderLimitDays)*24*60*60;
//    
//    if (difference>limitDays) {
//        _errorLabel.text = [LanguageManager stringForKey:@"Range of days must be less than 30 days"];
//        _dateFromLabel.textColor = [UIColor redColor];
//        _dateToLabel.textColor = [UIColor redColor];
//        return;
//    } else {
//        _dateFromLabel.textColor = [UIColor whiteColor];
//        _dateToLabel.textColor = [UIColor whiteColor];
//    }
    
    
    _errorLabel.text = @"";
    // if all checks passed, enable button
    //[_searchHistoryBtn setEnabled:YES];
}

- (IBAction)callSource:(UIButton *)sender{
    [popupSourceTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupSourceTableViewController];
    navController.navigationBar.topItem.title= @"Source";
    
    popupSourceTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    NSInteger totalToDisplay = _sourceArray.count;
    if (totalToDisplay>=10) totalToDisplay = 10;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*60);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)callCategory:(UIButton *)sender{
    [popupCategoryTableViewController.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:popupCategoryTableViewController];
    navController.navigationBar.topItem.title= @"Category";
    
    popupSourceTableViewController.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    NSInteger totalToDisplay = 5;
    if (totalToDisplay>=10) totalToDisplay = 10;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*60);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (IBAction)callDatePicker:(UIButton*)sender {
    
    UIViewController *temp =[[UIViewController alloc]init];
    datePickerView = [[UIDatePicker alloc]init];
    datePickerView.datePickerMode = UIDatePickerModeDate;
    
    // from date
    if (sender.tag==1) {
        [datePickerView addTarget:self action:@selector(updateFromDate:) forControlEvents:UIControlEventValueChanged];
        if (_fromDate==nil) {
            datePickerView.date =[NSDate date];
            _fromDate = [NSDate date];
        } else {
            datePickerView.date = _fromDate;
        }
        [self updateFromDate:nil];
        // to date
    } else if (sender.tag==2) {
        [datePickerView addTarget:self action:@selector(updateToDate:) forControlEvents:UIControlEventValueChanged];
        if (_toDate==nil) {
            datePickerView.date =[NSDate date];
            _toDate = [NSDate date];
        } else {
            datePickerView.date = _toDate;
        }
        [self updateToDate:nil];
    }
    
    
    
    datePickerView.tag = 2;
    [temp.view addSubview:datePickerView];
    popoverController=temp.popoverPresentationController;
    temp.preferredContentSize=CGSizeMake(320,216);
    if (popoverController)
    {
        popoverController.sourceView = sender;
        popoverController.sourceRect = sender.bounds;
        popoverController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    }
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:temp];
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:sender.superview
                       permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat cellHeight = 66.f;

    return cellHeight;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing");
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    keywordString = textField.text;
   
    return YES;
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    NSLog(@"textFieldShouldClear:");
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    
    [textField resignFirstResponder];
    
    keywordString = textField.text;
    
    return YES;
}

- (IBAction)clickSearch:(id)sender{
    
    NSLog(@"_fromDate %@, _toDate %@, sourceID %@, categoryID %@, keyword %@, targetID %@",_fromDate,_toDate,sourceID,categoryID,keywordString,targetID);
    
    [self doAllElasticNews:_fromDate toDate:_toDate andSource:sourceID andCategory:categoryID andKeyword:keywordString andTargetID:targetID];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView.tag==1) {
        _sourceLabel.text = [_sourceArray objectAtIndex:indexPath.row];
        if (indexPath.row==0) {
            sourceID = @"";
        }else{
            sourceID = [_sourceArray objectAtIndex:indexPath.row];
        }
    }else if(tableView.tag==2){
        if(indexPath.row==0) {
             _typeLabel.text = [_categoryArray objectAtIndex:0];
            categoryID = @"";
        }else{
            NSArray *arrayCategory = [[self.categoryArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"|"];
            _typeLabel.text = [arrayCategory objectAtIndex:2];
             categoryID = [arrayCategory objectAtIndex:1];
            NSLog(@"categoryID %@",categoryID);
        }
    }else{
        // click the table to see the details.
        NewsMetadata *nmd = (NewsMetadata *) [_newsArray objectAtIndex:indexPath.row];
        NSString *url;
    
        if ([UserPrefConstants singleton].isEnabledElasticNews && ![[UserPrefConstants singleton].brokerName isEqualToString:@"CIMB MY"]) {
            
            if(![[nmd.sourceNews uppercaseString] isEqualToString:@"CIMB"]){
                url = [NSString stringWithFormat:@"%@?t=7&nid=%@&ns=%@&ex=%@&spc=%@&bh=%@&appId=MI&rdt=P&ts=%@",[UserPrefConstants singleton].ElasticNewsServerAddress,nmd.newsID,[nmd.sourceNews uppercaseString],[UserPrefConstants singleton].userCurrentExchange,[UserPrefConstants singleton].SponsorID,[UserPrefConstants singleton].bhCode,[self getCurrentTimeStamp]];
            }else{
                url = [NSString stringWithFormat:@"%@?id=%@",[UserPrefConstants singleton].researchReportDetailNewsAddress, nmd.newsID];
            }
            
        }else{
             url = [NSString stringWithFormat:@"%@/ebcNews/index.jsp?&id=%@EN",[UserPrefConstants singleton].NewsServerAddress,nmd.newsID];
        }
        
        NSLog(@"url elastic News %@",url);
        
         [self openNews:url withTitle:[LanguageManager stringForKey:@"News"]];
       
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [columnPickerPopover dismissPopoverAnimated:YES];
}

- (NSString *) getCurrentTimeStamp{
    NSDate *todaysDate = [NSDate new];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *strDateTime = [formatter stringFromDate:todaysDate];
    return strDateTime;
}


- (void) openNews:(NSString *)url withTitle:(NSString*)title{
    
    [_atp timeoutActivity];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewsModalViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewsModalViewController"];
    vc.view.backgroundColor = [UIColor clearColor];
    vc.newsTitleLabel.text = title;

    vc.currentEd = nil;
    
    [vc openUrl:url withJavascript:YES];
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle= UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
    
}

@end
