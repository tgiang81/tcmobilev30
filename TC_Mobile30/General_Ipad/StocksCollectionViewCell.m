//
//  StocksCollectionViewCell.m
//  TCiPad
//
//  Created by n2n on 15/06/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "StocksCollectionViewCell.h"

@implementation StocksCollectionViewCell


-(void)awakeFromNib {
    [super awakeFromNib];
    
    _stockLabel.layer.cornerRadius = kButtonRadius *self.frame.size.height;
    _stockLabel.layer.masksToBounds = YES;
    
    _deleteButton.layer.cornerRadius = _deleteButton.frame.size.height/2.0;
    _deleteButton.layer.masksToBounds = YES;
}

- (IBAction)removeItem:(id)sender {
    
    [self.delegate removeButtonDidClicked:_myPath];

}


@end
