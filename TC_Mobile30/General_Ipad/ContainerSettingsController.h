//
//  ContainerSettingsController.h
//  TCiPad
//
//  Created by n2n on 02/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerSettingsController : UIViewController


- (void)switchToViewController : (NSString*)segueString;

@end
