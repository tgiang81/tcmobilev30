//
//  QuoteScreenTableViewCell.h
//  TCUniversal
//
//  Created by Scott Thoo on 11/11/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuoteScreenTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *stkName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDone;
@property (weak, nonatomic) IBOutlet UILabel *lblTrades;
@property (weak, nonatomic) IBOutlet UILabel *lblTradeVal;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic1;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic2;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic3;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic4;
@property (weak, nonatomic) IBOutlet UILabel *lblDynamic5;

@property (strong, nonatomic) IBOutlet UILabel *lblPar;
@property (strong, nonatomic) IBOutlet UILabel *lblBidQty;
@property (strong, nonatomic) IBOutlet UILabel *lblTop;

@property (strong, nonatomic) IBOutlet UILabel *lblTp;

@property (strong, nonatomic) IBOutlet UILabel *lblEps;

@property (strong, nonatomic) IBOutlet UILabel *lblClose;

@property (strong, nonatomic) IBOutlet UILabel *lblPerChg;

@property (strong, nonatomic) IBOutlet UILabel *lblChange;


@property (strong, nonatomic) IBOutlet UILabel *lblAskQty;

@property (strong, nonatomic) IBOutlet UILabel *lblAsk;

@property (strong, nonatomic) IBOutlet UILabel *lblBid;

@property (strong, nonatomic) IBOutlet UILabel *lblRealofDelayed;

@property (strong, nonatomic) IBOutlet UILabel *lblCategory;

@property (strong, nonatomic) IBOutlet UILabel *lblExchange;

@property (strong, nonatomic) IBOutlet UILabel *lblRemarks;



@end
