//
//  NSData-AES.h
//  TCiPhone_CIMB
//
//  Created by Don Lee on 12/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData(AES) 
    
- (NSData *)AES128EncryptWithKey:(NSString *)key;
- (NSData *)AES128DecryptWithKey:(NSString *)key;

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

- (NSData *)AES128EncryptedDataWithKey:(NSString *)key iv:(NSString *)iv;
- (NSData *)AES128DecryptedDataWithKey:(NSString *)key iv:(NSString *)iv;

- (NSData *)AES256EncryptWithKeyCBC:(NSString *)key;

- (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key;

- (NSData *)AES128DecryptWithKeyUTF8:(NSString *)key;
- (NSData *)AES128EncryptWithKeyUTF8:(NSString *)key;
- (NSData *)AES256DecryptWithKeyCBC:(NSString *)key;
@end
