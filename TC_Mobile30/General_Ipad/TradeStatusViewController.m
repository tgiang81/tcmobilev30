//
//  TradeStatusViewController.m
//  TCiPad
//
//  Created by n2n on 23/05/2017.
//  Copyright © 2017 N2N. All rights reserved.
//

#import "TradeStatusViewController.h"
#import "UIViewController+Popup.h"
#import "AroundShadowView.h"
#import "ThemeManager.h"
#import "SettingManager.h"
#import "StockModel.h"
#import "NSNumber+Formatter.h"
@interface TradeStatusViewController ()
{
    CGFloat defaultCornerRadius;
}
@end

@implementation TradeStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setBackButtonDefault];
    self.customBarTitle = [LanguageManager stringForKey:@"Order Pad"];
    defaultCornerRadius = 4;
    _closeBtn.layer.cornerRadius = kButtonRadius*_closeBtn.frame.size.height;
    _closeBtn.clipsToBounds = YES;
    _openOrdBkBtn.layer.cornerRadius = kButtonRadius*_openOrdBkBtn.frame.size.height;
    _openOrdBkBtn.clipsToBounds = YES;
    
    
    
    if(IS_IPHONE){
        _closeBtn.layer.cornerRadius = defaultCornerRadius;
        _openOrdBkBtn.layer.cornerRadius = defaultCornerRadius;
        _openPortfolioBtn.layer.cornerRadius = defaultCornerRadius;
        _openQuoteBtn.layer.cornerRadius = defaultCornerRadius;
    }
    
    if([[_statusStr lowercaseString] containsString:@"reject"]){
        self.img_status.image = [UIImage imageNamed:@"ic_rejected"];
    }else{
        self.img_status.image = [UIImage imageNamed:@"ic_accepted"];
        //[self.openOrdBkBtn setHidden:YES];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeStatus:) name:@"dismissAllModalViews" object:nil];
    
    
    self.v_content.backgroundColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoBg);
    self.lbStockName.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.lbCompany.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.changeValue.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
    self.changePercent.textColor = TC_COLOR_FROM_HEX([ThemeManager shareInstance].trade_StockInfoText);
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.lbl_status.text = _statusStr;
    _tsMessage.text = _messageStr;
    self.lbStockName.text = self->_stock.stockName;
    self.lbCompany.text = [_stock.stockCode stringByDeletingPathExtension];
    self.changeValue.text = [_stock.lastDonePrice toCurrencyNumber];
    self.changePercent.text = [_stock changePercentValue];
    self.changeValueContent = [_stock.fChange doubleValue];
    
}
- (void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
}
- (void)setChangeValueContent:(CGFloat)changeValueContent{
    if (changeValueContent>0) {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].upPriceColor)];
    } else if (changeValueContent<0) {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].downPriceColor)];
    } else {
        [_changeValue setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
        [_changePercent setTextColor:TC_COLOR_FROM_HEX([SettingManager shareInstance].unChangedPriceColor)];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)closeStatus:(id)sender {
    if(IS_IPAD){
        [self dismissViewControllerAnimated:YES completion:^(void) {
            
            [self.delegate closeBtnDidClicked];
        }];
    }else{
        [self.delegate closeBtnDidClicked];
    }
    
}
- (IBAction)closeStatusAndOpenPortfolio:(id)sender {
    [self backButtonTapped:nil];
    [self.delegate closeAndOpenPortfolioDidClicked];
    
}
- (IBAction)closeStatusAndOpenQuote:(id)sender {
    [self backButtonTapped:nil];
    [self.delegate closeAndOpenQuoteDidClicked];
}

- (IBAction)closeStatusAndOpenOrdBook:(id)sender {
    if(IS_IPAD){
        [self dismissViewControllerAnimated:YES completion:^(void) {
            
            [self.delegate closeAndOpenOrdBookDidClicked];
        }];
    }else{
        [self backButtonTapped:nil];
        [self.delegate closeAndOpenOrdBookDidClicked];
    }

}
@end
