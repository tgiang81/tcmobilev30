//
//  EmptySegue.h
//  TCUniversal
//
//  Created by Scott Thoo on 11/6/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptySegue : UIStoryboardSegue

@end
