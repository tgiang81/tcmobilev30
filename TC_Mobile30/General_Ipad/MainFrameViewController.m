//
//  CustomTabBarViewController.m
//  TCUniversal
//
//  Created by Scott Thoo on 10/8/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "MainFrameViewController.h"
#import "VertxConnectionManager.h"
#import "ContainerViewController.h"
#import "UserPrefConstants.h"
#import "ATPAuthenticate.h"
#import "QCData.h"
#import "QCConstants.h"
#import "ExchangeData.h"
#import "AppConstants.h"
#import "UIImageUtility.h"
#import "AppControl.h"
#import "MagnifyEffect.h"
#import "FingerWaveView.h"
#import "ButtonEffect.h"
#import "TickerItem.h"
#import "SearchViewController.h"
#import "SCLAlertView.h"
#import "LanguageKey.h"

// register button tags

#define kPageHome 1
#define kPageQuote 2
#define kPageIndex 3
#define kPageDetail 4
#define kPageOrderBk 6
#define kPageSettings 7
#define kPageESettlement 8
#define kPageStockAlert 9
#define kPageWatchList 10
#define kMarketStreamer 12
#define kStockTracker 13

#define kLimitStackVC 5

// Category

@interface UIView (AnimationsHandler)

- (void)pauseAnimations;
- (void)resumeAnimations;

@end

@implementation UIView (AnimationsHandler)



- (void)pauseAnimations
{
    if (self.layer.speed>0) {
        CFTimeInterval paused_time = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil];
        self.layer.speed = 0.0;
        self.layer.timeOffset = paused_time;
    }
}

- (void)resumeAnimations
{
    if (self.layer.speed<=0) {
        CFTimeInterval paused_time = [self.layer timeOffset];
        self.layer.speed = 1.0f;
        self.layer.timeOffset = 0.0f;
        self.layer.beginTime = 0.0f;
        CFTimeInterval time_since_pause = [self.layer convertTime:CACurrentMediaTime() fromLayer:nil] - paused_time;
        self.layer.beginTime = time_since_pause;
    }
}

@end


@interface MainFrameViewController () <UIGestureRecognizerDelegate>
{
    ATPAuthenticate *atp;
    UIView *tickerView;
    SCLAlertView *alertInfo;
    NSMutableDictionary *tempWatchList_Fav_Name; // Dictionary that holds changes made by user.
    int targetDeleteItemFav;
    ExchangeData *ED;
    UITableViewController *Exchangetablevc, *LanguageTableVC;
    NSMutableArray *userExchangelist;
    
    NSMutableArray *arrStackVCs;
    
//    UITableView *WatchList;
    UIViewController *WatchListView;
    UIPopoverController *columnPickerPopover;
    NSTimer *timer;

    NSArray *WatchListArray;
    
    NSMutableArray *cells;
    BOOL fromWatchList;
    BOOL fromSearch;
    BOOL runOnce;
    BOOL tickerDidLoad;
    BOOL isOpenVCFromBtnBACK;
    
    long atpTimerCnt;
    long checkTimeCount;
}

@property (nonatomic, weak) ContainerViewController *containerViewController;

@end

@implementation MainFrameViewController
@synthesize magnifyingView;
@synthesize viewBottomTicker = _viewBottomTicker;
@synthesize imgNetwork;
@synthesize lblIndex;





#pragma mark - TapGesture delegates

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
    
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    return YES;
}



- (void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    // NSLog(@"disappaer");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"openQuoteScreenFromIndices" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"openOrderBookPortFolio" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"clickedStockFlag" object:nil];
    //  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doneVertxSearch" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pnWebSocketConnected" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pnWebSocketDisconnected" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didReceivedWatchListItems" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loadTicker" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DoneGetKLCI" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"subscribeKLCI" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"validate2FAPinForDeviceDone" object:nil];
    // WATCHLIST FUNCTIONALITY MOVED TO QUOTESCREEN - DEC 2016
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"deleteWatchList" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"watchlistDeleted" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addOrUpdateWatchlistNameSuccessfully" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"callWatchlist" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"goToDetailWatchlist" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"deleteWatchlistItem" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didReceivedWatchListItems2" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"deleteWatchlistItemSuccessfully" object:nil];
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"addWatchlistItemAdded" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doKickout" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"doAutoLogout" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateNaviButtons" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"openWatchListFromHome" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didReceiveExchangeListMain" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchStockToAdd" object:nil];
}




- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture
{
    CGPoint touchPoint = [gesture locationInView:self.view];
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            [self.magnifyingView touchesBegan:touchPoint];
            break;
        case UIGestureRecognizerStateChanged:
            [self.magnifyingView touchesMove:touchPoint];
            break;
        default:
            [self.magnifyingView touchesEnded:touchPoint];
            
            break;
            
    }
}


-(void)openWatchList {
    [self btnSeguePressed:_watchlistButton];
}

- (void)onTap:(UITapGestureRecognizer*)sender {
    CGPoint center = [sender locationInView:sender.view];
    [FingerWaveView  showInView:self.view center:center];
}

#pragma mark - View Lifecycles Delegates


-(void)viewDidLayoutSubviews {
    if (runOnce) {
        
       
        
        
        _mainFrameTitle.text = [UserPrefConstants singleton].appName;
        [_mainFrameTitle sizeToFit];
        [_mainFrameTitle setHidden:YES];
        
        _n2nconnectBhd = [[LabelEffect alloc] init];
        _n2nconnectBhd.frame = _mainFrameTitle.frame;
        _n2nconnectBhd.text = [UserPrefConstants singleton].appName;
        
        NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//        NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
        if (([bundleIdentifier isEqualToString:BUNDLEID_CIMBMY_UAT])||([bundleIdentifier isEqualToString:BUNDLEID_CIMBSG_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_CIMBSGSTAG])) {
            _mainFrameTitle.textColor = [UIColor redColor];
            _n2nconnectBhd.textColor = [UIColor redColor];
        }
        
        if (DEBUG_MODE==1) {
             _n2nconnectBhd.text = @"TCPro Tab";
        }
        _n2nconnectBhd.textColor = [UIColor grayColor];
        _n2nconnectBhd.font = [UIFont boldSystemFontOfSize:20];
        [_n2nconnectBhd startShimmer];
        [self.view addSubview:_n2nconnectBhd];
       
        _n2nconnectBhd.center = CGPointMake(_n2nconnectBhd.center.x, imgNetwork.center.y);
        [self showBackBtn:NO];
        [self updateNaviButtons];
        runOnce = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrStackVCs = [NSMutableArray new];
    
    
    _selection = -1; //initialize
    justChangeExchange = NO;
    
    [[LanguageManager defaultManager] applyAccessibilityId:self];
    [[LanguageManager defaultManager] translateStringsForViewController:self];
    
    [_languageBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"lang_%@.png",[LanguageManager defaultManager].language]] forState:UIControlStateNormal];
    
    if (![UserPrefConstants singleton].isSupportedMultipleLanguage) {
        _languageBtn.enabled = NO;
    }
    
    NSString *bundleIdentifier = BUNDLEID_HARDCODE_TESTING;

//      NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleIdentifier isEqualToString:BUNDLEID_ABACUS_UAT] || [bundleIdentifier isEqualToString:BUNDLEID_ABACUS] ) {
        _lblKlci.text = @"PSEi INDEX:";
    }
    
    if (DEBUG_MODE==1) {
        [_lblKlci setHidden:YES];
        [lblIndex setHidden:YES];
    }
    
    
    // loupe
    /*MagnifyEffect *magnifyEffect = [[MagnifyEffect alloc] init];
    self.magnifyingView.magnifyingGlass = magnifyEffect;
    self.magnifyingView.magnifyingGlassShowDelay = 0.5f;
    magnifyEffect.scaleAtTouchPoint = NO;
    
    UILongPressGestureRecognizer *longTouch = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(handleLongPress:)];
    longTouch.delegate = self;
    [self.Containerview addGestureRecognizer:longTouch];
    */
    
    
    //  UILongPressGestureRecognizer *longTouch = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    //  [self.view addGestureRecognizer:longTouch];
    
    tickerDidLoad = NO;
    _exchangeButton.enabled = NO;
    _orderBookButton.enabled = NO;
    
    runOnce = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    _sideBarScrollView.backgroundColor = [UIColor colorWithRed:0.10 green:0.10 blue:0.10 alpha:1.00];
    //  _searchBar.delegate = self;
    
    
    
    UILongPressGestureRecognizer *tapTicker = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(tapTickerView:)];
    tapTicker.delegate = self;
    [tapTicker setMinimumPressDuration:0.2f];
    [self.viewBottomTicker addGestureRecognizer:tapTicker];
    
    
    _searchBtn.layer.cornerRadius = kButtonRadius*_searchBtn.frame.size.height;
    _searchBtn.layer.masksToBounds = YES;
    
    NSNotificationCenter *notCenter = [NSNotificationCenter defaultCenter];
    
    [notCenter addObserver:self selector:@selector(openOrderBookPortFolio)       name:@"openOrderBookPortFolio" object:nil];
    
    [notCenter addObserver:self selector:@selector(updateNaviButtons) name:@"updateNaviButtons" object:nil];

    [notCenter addObserver:self selector:@selector(openQuoteScreenFromIndices)       name:@"openQuoteScreenFromIndices" object:nil];
    [notCenter addObserver:self selector:@selector(stockCodeReceived:)               name:@"clickedStockFlag" object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchDataDictReceived:)          name:@"doneVertxSearch" object:nil];
    [notCenter addObserver:self selector:@selector(connected:)                       name:@"pnWebSocketConnected" object:nil];
    [notCenter addObserver:self selector:@selector(disconnected:)                    name:@"pnWebSocketDisconnected" object:nil];
    [notCenter addObserver:self selector:@selector(didReceivedWatchListItems:)       name:@"didReceivedWatchListItems" object:nil];
    [notCenter addObserver:self selector:@selector(loadTickerFirstTime)              name:@"loadTicker" object:nil];
    [notCenter addObserver:self selector:@selector(didFinishedDoneGetKLCI:)          name:@"DoneGetKLCI" object:nil];
    [notCenter addObserver:self selector:@selector(didReceiveFromSubscribeKLCI:)     name:@"subscribeKLCI" object:nil];
    [notCenter addObserver:self selector:@selector(doKickout:)                       name:@"doKickout" object:nil];
    [notCenter addObserver:self selector:@selector(doAutoLogout)                     name:@"doAutoLogout" object:nil];
    [notCenter addObserver:self selector:@selector(didReceiveUserAccountsList)       name:@"didReceiveUserAccountsList" object:nil];
    [notCenter addObserver:self selector:@selector(openWatchList)                    name:@"openWatchListFromHome" object:nil];
    [notCenter addObserver:self selector:@selector(callSearch:)                      name:@"searchStockToAdd" object:nil];
    [notCenter addObserver:self selector:@selector(didReceiveExchangeList)           name:@"didReceiveExchangeListMain" object:nil];
    [notCenter addObserver:self selector:@selector(validate2FAPinForDeviceDone:) name:@"validate2FAPinForDeviceDone" object:nil];
    
    
    //UI tweak
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    
    //Call start functions
    [self checkConnection];
    
    //atp
    atp = [ATPAuthenticate singleton];
    lblIndex.textColor = [UIColor grayColor];
    

    tempWatchList_Fav_Name = [NSMutableDictionary dictionary];
    Exchangetablevc = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    Exchangetablevc.tableView.delegate=self;
    Exchangetablevc.tableView.dataSource=self;
    Exchangetablevc.tableView.tag=2;
    
    
    LanguageTableVC = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    LanguageTableVC.tableView.delegate=self;
    LanguageTableVC.tableView.dataSource=self;
    LanguageTableVC.tableView.tag=10;
    
    //    [homeButton setImage:[UIImage imageNamed:@"home_enable.png"] forState:UIControlStateNormal];
    //    [homeButton setImage:[UIImage imageNamed:@"home_disable"] forState:UIControlStateDisabled];
    //    [homeButton setImage:[UIImage imageNamed:@"home_highlighted.png"] forState:UIControlStateSelected];
    //
    //    [quotesButton setImage:[UIImage imageNamed:@"quotes_enable.png"] forState:UIControlStateNormal];
    //    [quotesButton setImage:[UIImage imageNamed:@"quotes_disable"] forState:UIControlStateDisabled];
    //    [quotesButton setImage:[UIImage imageNamed:@"quotes_highlighted.png"] forState:UIControlStateSelected];
    //
    //    [IndicesButton setImage:[UIImage imageNamed:@"indices_enable.png"] forState:UIControlStateNormal];
    //    [IndicesButton setImage:[UIImage imageNamed:@"indices_disable"] forState:UIControlStateDisabled];
    //    [IndicesButton setImage:[UIImage imageNamed:@"indices_highlighted.png"] forState:UIControlStateSelected];
    //
    //    [stockDetailsButton setImage:[UIImage imageNamed:@"stock-details_enable.png"] forState:UIControlStateNormal];
    //    [stockDetailsButton setImage:[UIImage imageNamed:@"stock-details_disable"] forState:UIControlStateDisabled];
    //    [stockDetailsButton setImage:[UIImage imageNamed:@"stock-details_highlighted.png"] forState:UIControlStateSelected];
    //
    //    [_orderBookButton setImage:[UIImage imageNamed:@"portfolio_enable.png"] forState:UIControlStateNormal];
    //    [_orderBookButton setImage:[UIImage imageNamed:@"portfolio_disable"] forState:UIControlStateDisabled];
    //    [_orderBookButton setImage:[UIImage imageNamed:@"portfolio_highlighted.png"] forState:UIControlStateSelected];
    
    cells =[NSMutableArray array];
    NSUserDefaults *def= [NSUserDefaults standardUserDefaults];
    [UserPrefConstants singleton].defaultView =  [[def objectForKey:@"ScreenViewMode"] intValue];
    
    switch ( [UserPrefConstants singleton].defaultView) {
        case 1:
            [_homeButton setUserInteractionEnabled:NO];
            [_homeButton setSelected:YES];
            break;
        case 2:
            [_quotesButton setUserInteractionEnabled:NO];
            [_quotesButton setSelected:YES];
            break;
        case 3:
            [_watchlistButton setUserInteractionEnabled:NO];
            [_watchlistButton setSelected:YES];
            
            break;
            
        default:
            
            break;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   /*
    
    if ([ReleaseNotesView isAppOnFirstLaunch]) {
        [[UserPrefConstants singleton] showLocalReleaseNotesInView:self.view];
    }
    else if(![ReleaseNotesView isAppOnFirstLaunch] && [ReleaseNotesView isAppVersionUpdated])
    {
        [[UserPrefConstants singleton] showLocalReleaseNotesInView:self.view];
    }*/
    
    
    [UIView animateWithDuration:2.0 delay:0 options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^(void) {
        [_tickerErrorLabel setAlpha:0.0];
    } completion:nil];
    
   //  NSLog(@"Current Exchg %@",[UserPrefConstants singleton].userCurrentExchange);
    
    // set current exchange info
    [UserPrefConstants singleton].currentExchangeInfo = [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:[UserPrefConstants singleton].userCurrentExchange];
    
    // NSLog(@"Current Exchange info: %@",[UserPrefConstants singleton].currentExchangeInfo);
    
    [[VertxConnectionManager singleton] vertxSortForTicker:@"101" Direction:@"DESC" exchg:[UserPrefConstants singleton].userCurrentExchange];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [[VertxConnectionManager singleton] vertxGetKLCI];
    
     [atp getWatchList];
    
    timer=[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateDateandTime) userInfo:nil repeats:YES];
    [atp timeoutActivity];
    
    _backBtn.hidden = YES;
    _backBtn.layer.cornerRadius = kButtonRadius*_backBtn.frame.size.height;
    _backBtn.clipsToBounds = YES;
    
    [_backBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 3.5, 0)];
    imgNetwork.center = CGPointMake(_n2nconnectBhd.center.x+_n2nconnectBhd.frame.size.width/2.0+imgNetwork.frame.size.width/2.0, imgNetwork.center.y);
    dispatch_async(dispatch_get_main_queue(), ^{
        _n2nconnectBhd.isPlaying = false;
        [_n2nconnectBhd startShimmer];
    });
    
    //[self didReceiveExchangeList];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishedSortForTicker:) name:@"didFinishedSortForTicker" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didFinishedSortForTicker" object:nil];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)updateNaviButtons {
    
    
    __block  BOOL IndicesEnable = NO;
    NSArray *exchArr = [UserPrefConstants singleton].indicesEnableByExchange;
    [exchArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *str = (NSString*)obj;
        if ([str isEqualToString:[UserPrefConstants singleton].userCurrentExchange]) IndicesEnable = YES;
        
    }];
    
    [_IndicesButton setEnabled:IndicesEnable];
    [self rearrangeButtons];
}


-(void)rearrangeButtons {
    
    NSMutableArray *enabledBtnArr = [[NSMutableArray alloc] init];

    // sort buttons (you can arrange order it appears here - which is added to enabledBtnArr first will appear at the top)
    
    if (_homeButton.enabled) { [enabledBtnArr addObject:_homeButton]; [_homeButton setHidden:NO];} else { [_homeButton setHidden:YES]; }
    if (_quotesButton.enabled) { [enabledBtnArr addObject:_quotesButton]; [_quotesButton setHidden:NO];} else { [_quotesButton setHidden:YES]; }
    if (_IndicesButton.enabled) { [enabledBtnArr addObject:_IndicesButton]; [_IndicesButton setHidden:NO];} else { [_IndicesButton setHidden:YES]; }
    if (_stockDetailsButton.enabled) { [enabledBtnArr addObject:_stockDetailsButton]; [_stockDetailsButton setHidden:NO];} else { [_stockDetailsButton setHidden:YES]; }
   
    if (_marketStreamerButton.enabled) { [enabledBtnArr addObject:_marketStreamerButton]; [_marketStreamerButton setHidden:NO];} else { [_marketStreamerButton setHidden:YES]; }
    
    if (_orderBookButton.enabled) { [enabledBtnArr addObject:_orderBookButton]; [_orderBookButton setHidden:NO];} else { [_orderBookButton setHidden:YES]; }
    if (_watchlistButton.enabled) { [enabledBtnArr addObject:_watchlistButton]; [_watchlistButton setHidden:NO];} else { [_watchlistButton setHidden:YES]; }
    
    _eSettlementButton.enabled = [UserPrefConstants singleton].isEnabledESettlement;
    
    if (_eSettlementButton.enabled) { [enabledBtnArr addObject:_eSettlementButton]; [_eSettlementButton setHidden:NO];} else { [_eSettlementButton setHidden:YES]; }
    
    _stockAlertButton.enabled = [UserPrefConstants singleton].isEnabledStockAlert;
    if (_stockAlertButton.enabled){[enabledBtnArr addObject:_stockAlertButton]; [_stockAlertButton setHidden:NO];} else { [_stockAlertButton setHidden:YES]; }
    if([UserPrefConstants singleton].qcServer.length>0){
        _elasticNewsButton.enabled = [UserPrefConstants singleton].isEnabledElasticNews;
        if (_elasticNewsButton.enabled)  { [enabledBtnArr addObject:_elasticNewsButton]; [_elasticNewsButton setHidden:NO];} else { [_elasticNewsButton setHidden:YES]; }
    }
    
    if(![UserPrefConstants singleton].isShowMarketStreamer){
        [_marketStreamerButton setEnabled:NO];
         if (_marketStreamerButton.enabled)  { [enabledBtnArr addObject:_marketStreamerButton]; [_marketStreamerButton setHidden:NO];} else { [_marketStreamerButton setHidden:YES]; }
    }
    
    // TODO: waiting elastic news API info
   //  [_elasticNewsButton setHidden:YES];
   
    
    if (_exchangeButton.enabled) { [enabledBtnArr addObject:_exchangeButton]; [_exchangeButton setHidden:NO];} else { [_exchangeButton setHidden:YES]; }
    if (_settingsButton.enabled) { [enabledBtnArr addObject:_settingsButton]; [_settingsButton setHidden:NO];} else { [_settingsButton setHidden:YES]; }
    if (_logoutButton.enabled) { [enabledBtnArr addObject:_logoutButton]; [_logoutButton setHidden:NO];} else { [_logoutButton setHidden:YES]; }
    
    
    
    long totButtons = [enabledBtnArr count];
    
    // set top and btm button
    CGFloat topBtnYPos = _homeButton.frame.size.height/2.0;
    CGFloat btmBtnYPos = _viewSideBar.frame.size.height - _homeButton.frame.size.height/2.0;
    CGFloat gapToFill = btmBtnYPos - topBtnYPos;
    
    UIButton*topBtn = [enabledBtnArr objectAtIndex:0];
    UIButton*btmBtn = [enabledBtnArr objectAtIndex:[enabledBtnArr count]-1];
    
    topBtn.center = CGPointMake(topBtn.center.x, topBtnYPos);
    btmBtn.center = CGPointMake(btmBtn.center.x, btmBtnYPos);
    
    CGFloat btnGaps = gapToFill / (totButtons-1);
    
    // arrange other buttons in middle
    CGFloat offSetY = topBtnYPos;
    
    if (totButtons>0) {
        
        for (long i=1; i<totButtons-1; i++) {
            offSetY += btnGaps;
            UIButton*btn = [enabledBtnArr objectAtIndex:i];
            btn.center = CGPointMake(btn.center.x, offSetY);
            
        }
        
    }
    
}


- (NSDate *)todaysDateFromString:(NSString *)time
{
    // Split hour/minute into separate strings:
    NSArray *array = [time componentsSeparatedByString:@":"];
    
    // Get year/month/day from today:
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comp = [cal components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    // Set hour/minute from the given input:
    [comp setHour:[array[0] integerValue]];
    [comp setMinute:[array[1] integerValue]];
    
    return [cal dateFromComponents:comp];
}

- (void) checkDeviceTime{
    NSString *strOpenTime = @"9:00";
    NSString *strCloseTime = @"9:30";
    
    NSDate *openTime = [self todaysDateFromString:strOpenTime];
    NSDate *closeTime = [self todaysDateFromString:strCloseTime];
    
    if ([closeTime compare:openTime] != NSOrderedDescending) {
        // closeTime is less than or equal to openTime, so add one day:
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *comp = [[NSDateComponents alloc] init];
        [comp setDay:1];
        closeTime = [cal dateByAddingComponents:comp toDate:closeTime options:0];
    }
    
    NSDate *now = [NSDate date];
    
    if ([now compare:openTime] != NSOrderedAscending &&
        [now compare:closeTime] != NSOrderedDescending) {
        [[VertxConnectionManager singleton]   vertxGetKLCI];
    } else {
        // now is outside = Close
    }
}

-(void)updateDateandTime
{
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-YYYY HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:currDate];
    _DateTimelbl.text=dateString;
    
    atpTimerCnt += 1;
    checkTimeCount +=1;
    
    if (atpTimerCnt>=10) { //this callback every 0.5s, counter=10 means every 5s
        atpTimerCnt = 0;
        [atp doCheckSingleLogon];
    }
    
    if (checkTimeCount>=500) { // this callback every 0.5s, counter=100 means every 250 sec.
        checkTimeCount = 0;
        
        [self checkDeviceTime];
    }
    
    // broadcast timer ticks for other VC usage
    [[NSNotificationCenter defaultCenter] postNotificationName:@"halfSecondTimerTick" object:nil];
}

#pragma mark - Handle ticker touches


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:_viewBottomTicker];
    
    if ([touch tapCount]==2) {
        for (TickerItem *tickers in _viewBottomTicker.subviews)
        {
            
            if ([tickers.layer.presentationLayer hitTest:touchLocation]&&(tickers.tag>=100))
            {
                [UserPrefConstants singleton].userSelectingStockCode = tickers.stockCode;
                NSMutableDictionary *notificationData = [NSMutableDictionary dictionary];
                [notificationData setObject:tickers.stockCode forKey:@"stkCode"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"clickedStockFlag" object:self userInfo:notificationData];
                break;
            }
        }
    }
}

#pragma mark - Stock Ticker


-(void)removeTickers {
    tickerDidLoad = NO;
    for (UIView *view in _viewBottomTicker.subviews) {
        
        if ([view isKindOfClass:[TickerItem class]]) {
            TickerItem *tickers = (TickerItem*)view;
            if (tickers.tag>=100) [tickers removeFromSuperview];
        }
        
    }
    [[UserPrefConstants singleton].tickerStocks removeAllObjects];
    
}

-(void)tapTickerView:(UIGestureRecognizer*)recognizer {
    
    if (recognizer.state==UIGestureRecognizerStateBegan) {
        
        [_viewBottomTicker pauseAnimations];
        
    } else if (recognizer.state==UIGestureRecognizerStateEnded)  {
        
        [_viewBottomTicker resumeAnimations];
    }
}





-(void)loadTickerFirstTime {
    
    NSLog(@"loadTickerFirstTime");
    
    if (![_viewBottomTicker viewWithTag:100]) {
        [self generateTickerWithWatchListTop5];
    }
}



- (void) updateTickerValue{
    
    NSArray *tickerArr = [NSArray arrayWithArray:[UserPrefConstants singleton].tickerStocks];
    int total= (int)[tickerArr count];
    if ((total<=0)||(![UserPrefConstants singleton].isConnected)) {
        tickerDidLoad = NO;
        return;
    }
    
    if (tickerDidLoad) {
        // [[[QCData singleton]qcFeedDataDict] allKeys];
        
        
        for (long i=0; i<total; i++) {
            
            TickerData *tick = [[UserPrefConstants singleton].tickerStocks objectAtIndex:i];
            
            // NSLog(@"tick %@", tick);
            
            if (tick.lastDone==nil) tick.lastDone = @"-";
            if (tick.chg==nil) tick.chg = @"-";
            if (tick.volume==nil) tick.volume = @"-";
            
            NSArray *tickerParams = [NSArray arrayWithObjects:
                                     tick.stkName,
                                     tick.lastDone,
                                     tick.chg,
                                     tick.stkCode,nil];
            
            for (TickerItem *tickers in _viewBottomTicker.subviews)
            {
                if ([tickers.stockCode isEqualToString:tick.stkCode])
                {
                    NSLog(@"tickers.stockCode %@",tickers.stockCode);
                    [tickers setParam:tickerParams];
                    break;
                }
            }
        }
    }
}

- (void)generateTickerWithWatchListTop5 // TOP 10 actually
{
    int numberOfTickerToShow = 10;
    
    CGFloat prevWidth = _viewBottomTicker.frame.size.width;
    CGFloat gap = 30;
    NSArray *tickerArr = [NSArray arrayWithArray:[UserPrefConstants singleton].tickerStocks];// [[[QCData singleton]qcFeedDataDict] allKeys];
    
    int total= (int)[tickerArr count];
    if ([tickerArr count]>numberOfTickerToShow) total = numberOfTickerToShow;
    
    if ((total<=0)||(![UserPrefConstants singleton].isConnected)) {
        tickerDidLoad = NO;
        return;
    }
    tickerDidLoad = YES;
    
    for (long i=0; i<total; i++) {
        
        TickerData *tick = [[UserPrefConstants singleton].tickerStocks objectAtIndex:i];
        
        // NSLog(@"tick %@", tick);
        
        if (tick.lastDone==nil) tick.lastDone = @"-";
        if (tick.chg==nil) tick.chg = @"-";
        if (tick.volume==nil) tick.volume = @"-";
        
        NSArray *tickerParams = [NSArray arrayWithObjects:
                                 tick.stkName,
                                 tick.lastDone,
                                 tick.chg,
                                 tick.stkCode,nil];
        TickerItem *ticker = [[TickerItem alloc] initWithParams:tickerParams];
        ticker.tag = 100+i;
        ticker.stockCode = tick.stkCode;
        prevWidth += gap + ticker.tickerWidth/2.0;
        ticker.center = CGPointMake(prevWidth, _viewBottomTicker.frame.size.height/2.0);
        [_viewBottomTicker addSubview:ticker];
        
        prevWidth += ticker.tickerWidth/2.0;
        
        
    }
    
    CGFloat tickerSpeed = 20;
    CGFloat duration = _viewBottomTicker.frame.size.width/tickerSpeed;
    
    for (UIView *view in [_viewBottomTicker subviews]) {
        
        
        
        if ((view.tag>100)&&(view.tag<100+numberOfTickerToShow)) {
            __block TickerItem *ticker = (TickerItem*)view;
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction
                             animations:^(void){
                                 
                                 ticker.center = CGPointMake(ticker.center.x-prevWidth, ticker.center.y);
                                 
                             } completion:^(BOOL finished) {
                                 [ticker removeFromSuperview];
                                 ticker = nil;
                             }];
            
        }
        
        
        
        
        // recall this method only by one of the ticker
        if (view.tag==100) {
            __block TickerItem *ticker = (TickerItem*)view;
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionAllowUserInteraction
                             animations:^(void){
                                 
                                 ticker.center = CGPointMake(ticker.center.x-prevWidth, ticker.center.y);
                                 
                             } completion:^(BOOL finished) {
                                 [ticker removeFromSuperview];
                                 ticker = nil;
                                 // reload ticker
                                 [[VertxConnectionManager singleton] vertxSortForTicker:@"101" Direction:@"DESC" exchg:[UserPrefConstants singleton].userCurrentExchange];
                                 
                                // [self generateTickerWithWatchListTop5];
                             }];
        }
        
    }
    
    
    
    
    //    [tickerView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //
    //    // NSArray *watchListArr = [UserPrefConstants singleton].userWatchListStockCodeArr;
    //    NSArray *watchListArr = [[[QCData singleton]qcFeedDataDict] allKeys];
    //    // NSLog(@"watchListArr = %@",watchListArr);
    //    int i = 0;
    //    NSNumberFormatter *priceFormatter = [NSNumberFormatter new];
    //    [priceFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    //    [priceFormatter setMinimumFractionDigits:3];
    //    [priceFormatter setMaximumFractionDigits:3];
    //
    //    for(NSString *stkCode in watchListArr)
    //    {
    //        if(i <5) //limit to show 5 only for ticker
    //        {
    //            NSDictionary *stockData = [[QCData singleton].qcFeedDataDict objectForKey:stkCode];
    //            StockTicker *ticker     = [[StockTicker alloc]initWithFrame:CGRectMake(i*300, 0, 100, 40)];
    //            [ticker.tickerButton setEnabled:YES];
    //
    //            NSNumber *lastDone    =[stockData objectForKey:FID_98_D_LAST_DONE_PRICE];
    //            NSNumber *change =[stockData objectForKey:FID_CUSTOM_F_CHANGE];
    //
    //            ticker.stockCode = stkCode;
    //            ticker.lblStkName.text = [stockData objectForKey:FID_38_S_STOCK_NAME];
    //            [ticker.lblStkName sizeToFit];
    //
    //            ticker.lblLastDone.text = [NSString stringWithFormat:@"%@",[priceFormatter stringFromNumber:lastDone]];
    //            ticker.lblPriceChanged.text = [NSString stringWithFormat:@"(%@)",[priceFormatter stringFromNumber:change]];
    //
    //            float perChange = [[stockData objectForKey:FID_CUSTOM_F_CHANGE_PER]floatValue];
    //            if(perChange <0)
    //            {
    //                ticker.lblLastDone.textColor = [UIColor redColor];
    //                ticker.lblPriceChanged.textColor = [UIColor redColor];
    //                ticker.imgFlag.image = [UIImage imageNamed:@"downFlag.png"];
    //            }
    //            else if(perChange >0)
    //            {
    //                ticker.lblPriceChanged.textColor = [UIColor greenColor];
    //                ticker.lblLastDone.textColor = [UIColor greenColor];
    //                ticker.imgFlag.image = [UIImage imageNamed:@"upFlag.png"];
    //            }
    //            else
    //            {
    //                ticker.lblPriceChanged.textColor = [UIColor grayColor];
    //                ticker.lblLastDone.textColor = [UIColor grayColor];
    //                ticker.imgFlag.image = [UIImage imageNamed:@"flatFlag"];
    //            }
    //
    //            ////NSLog(@"ticker.lblStkName.text %@",ticker.lblStkName.text);
    //
    //            [tickerView addSubview:ticker];
    //            i++;
    //        }
    //    }
}





- (IBAction)clickedLogout:(id)sender
{
    
    [[ButtonEffect sharedCenter]flarify:_logoutButton inParentView:self.view withColor:[UIColor redColor] enableUponCompletion:YES];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:[LanguageManager stringForKey:Log_out]
                                  message:[LanguageManager stringForKey:Are_you_sure_want_to_log_out]
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:[LanguageManager stringForKey:Yes]
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [timer invalidate];
                             [self logout];
                             
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:[LanguageManager stringForKey:Cancel]
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)doKickout:(NSNotification *)notification
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissAllModalViews" object:nil];
        
        [UserPrefConstants singleton].logoutDueToKicked = [notification.userInfo copy];
        
        [self logout];
        
        [timer invalidate];
        
        [[VertxConnectionManager singleton] stopVertxTimerKeepAlive];
        
    });
}

-(void)doAutoLogout
{
    [timer invalidate];
    [self logout];
}


-(void)logout {
    [self removeTickers];
    [atp atpDoLogout];
    [[[VertxConnectionManager singleton]vertxTimer] invalidate];
    [[UserPrefConstants singleton].userWatchListDict_FavId_Name removeAllObjects];
    [UserPrefConstants singleton].userWatchListDict_FavId_Name = nil;
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Vertx Reply
//====================================================================
//                 NSNotificationCenter Returned
//====================================================================
- (void)connected:(NSNotification *)notification
{
    [self checkConnection];
    
}
- (void)disconnected:(NSNotification *)notification
{
    [self checkConnection];
}

- (void)checkConnection
{
    [imgNetwork stopAnimating];
    
    if([UserPrefConstants singleton].isConnected)
    {
        imgNetwork.animationImages =[NSArray arrayWithObjects:
                                     [UIImage imageNamed:@"Connected1.png"],
                                     [UIImage imageNamed:@"Connected3.png"],
                                     [UIImage imageNamed:@"Connected2.png"],
                                     [UIImage imageNamed:@"Connected3.png"],
                                     nil];
        imgNetwork.animationDuration = 1.0f;
        imgNetwork.animationRepeatCount = 0;
        [imgNetwork startAnimating];
        [_tickerErrorLabel setHidden:YES];
 
        [self loadTickerFirstTime];
        
    } else {
        
        imgNetwork.animationImages =[NSArray arrayWithObjects:
                                     [UIImage imageNamed:@"Disconnected.png"],
                                     [UIImage imageNamed:@"Disconnected2.png"],
                                     nil];
        imgNetwork.animationDuration = 1.0f;
        imgNetwork.animationRepeatCount = 0;
        [imgNetwork startAnimating];
        [_tickerErrorLabel setHidden:NO];
        [self removeTickers];
    }
}

-(void)openOrderBookPortFolio {
    [atp timeoutActivity];
    [self showBackBtn:YES];
    [self btnSeguePressed:_orderBookButton];
}

// OPEN QUOTE SCREEN FROM INDICES (FILTERED BY SECTOR)
-(void)openQuoteScreenFromIndices {
    [atp timeoutActivity];
    [self showBackBtn:YES];
    
    [self btnSeguePressed:_quotesButton];
}

// USED TO OPEN STOCK DETAIL FROM OTHER SCREEN:
- (void)stockCodeReceived:(NSNotification *)notification
{
    [atp timeoutActivity];
    [self showBackBtn:YES];
    
    [self btnSeguePressed:_stockDetailsButton];
}

- (void)didReceivedWatchListItems:(NSNotification *)notification
{
    
    //    [UserPrefConstants singleton].fromWatchlist=YES;
    //    fromWatchList = YES;
    //    _searchBar.text = @"";
    //    [homeButton setUserInteractionEnabled:YES];
    //    [homeButton setSelected:NO];
    //    [quotesButton setUserInteractionEnabled:YES];
    //    [quotesButton setSelected:NO];
    //    [stockDetailsButton setUserInteractionEnabled:YES];
    //    [stockDetailsButton setSelected:NO];
    //    [IndicesButton setUserInteractionEnabled:YES];
    //    [IndicesButton setSelected:NO];
    //    [_orderBookButton setSelected:NO];
    //    [_orderBookButton setUserInteractionEnabled:YES];
    //
    //
    //    [UserPrefConstants singleton].currentSideBtnTag = kPageWatchList;
    //    [self.containerViewController switchToViewController:@"segueQuoteScreen"];
    
}
- (void)didFinishedDoneGetKLCI:(NSNotification *)notification
{
    //    NSDictionary * response = [notification.userInfo copy];
    //    ////NSLog(@"MainFrame KLCI response : %@",response);
    [self generateKlciIndex];
}

- (void)didReceiveFromSubscribeKLCI:(NSNotification *)notification
{
    // NSDictionary * response = [notification.userInfo copy];
    // NSLog(@"MainFrame KLCI Received : %@",response);
    [self generateKlciIndex];
    
}

- (void)didFinishedSortForTicker:(NSNotification *)notification
{
    
    NSDictionary *responseDict = notification.userInfo;
    
     //NSLog(@"didFinishedSortForTicker %@", responseDict);
    
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    [[UserPrefConstants singleton].tickerStockCodes removeAllObjects];
    
    // save ticker stock array
    NSArray *qcFeedArr = [[NSArray alloc] initWithArray:[responseDict objectForKey:@"sortedresults"] copyItems:YES];
    [qcFeedArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TickerData *tick = [[TickerData alloc] init];
        tick.stkCode = (NSString *)obj;
        NSDictionary *stockData = [[NSDictionary alloc] initWithDictionary:
                                   [[QCData singleton].qcFeedDataDict objectForKey:tick.stkCode] copyItems:YES];
        
       
        
        tick.stkName= [stockData objectForKey:FID_38_S_STOCK_NAME];
        tick.lastDone= [stockData objectForKey:FID_98_D_LAST_DONE_PRICE] ;
        tick.chg= [stockData objectForKey:FID_CUSTOM_F_CHANGE];
        tick.volume= [stockData objectForKey:FID_101_I_VOLUME];
        
        // NSLog(@"tick %@",tick);
        
        [[UserPrefConstants singleton].tickerStockCodes addObject:obj];
        
        [tmpArray addObject:tick];
    }];
    
    // SORT THE TICKER BASED ON VOLUME
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"volume"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [tmpArray sortedArrayUsingDescriptors:sortDescriptors];
    
    

    
    // input tickerstocks only if it is empty
    if ([[UserPrefConstants singleton].tickerStocks count]<=0){
        [UserPrefConstants singleton].tickerStocks = [NSMutableArray arrayWithArray:sortedArray];
    }
    
    // if still empty, then return (sometimes exchange server not return any data (empty feed!)
    if ([[UserPrefConstants singleton].tickerStocks count]<=0) {
        return;
    }
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadTicker" object:nil];
    
    // reload stkdetail with first ticker
    if (justChangeExchange) {
        justChangeExchange = NO;
        TickerData *first = [sortedArray objectAtIndex:0];
        [UserPrefConstants singleton].userSelectingStockCode = first.stkCode;
        [self btnSeguePressed:_stockDetailsButton];
    }
}

- (void)generateKlciIndex
{
    
    //    NSLog(@"qcKlciDict: %@", [[QCData singleton]qcKlciDict]);
    
    // NSLog(@"what %@", [[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]);
    
    if ([[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]] count]<=0) {
        _lblKlci.text=@"";
        lblIndex.text =@"";
        
        return;
    }
    
    if ([[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_39_S_COMPANY] length]<1) {
        _lblKlci.text= [NSString stringWithFormat:@"%@:",[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]] objectForKey:FID_33_S_STOCK_CODE]];
    } else {
        _lblKlci.text=[NSString stringWithFormat:@"%@:",[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_39_S_COMPANY]];
    }
    float close     = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_50_D_CLOSE]floatValue];
    float lastDone  = [[[[[QCData singleton]qcKlciDict]objectForKey:[[AppConstants FixedExchangeListIndex] valueForKey:[UserPrefConstants singleton].userCurrentExchange]]objectForKey:FID_98_D_LAST_DONE_PRICE]floatValue];
    
    float chg = lastDone - close;
    float chgPer = chg/close * 100;
    if (close<=0) chgPer = 0;
    
    
   //// NSLog(@"qcKlciDict - %@", [[QCData singleton]qcKlciDict]);
    
    //    ////NSLog(@"[QCData singleton]qcKlciDict]objectForKey:FID_50_D_CLOSE] : %@",[[[[QCData singleton]qcKlciDict]objectForKey:@"020000000.KL"]objectForKey:FID_50_D_CLOSE]);
    //    ////NSLog(@"close : %f",close);
    //    ////NSLog(@"lastDone : %f",lastDone);
    //    ////NSLog(@"chg : %f",chg);
    //    ////NSLog(@"chgPer : %f",chgPer);
    
    NSNumberFormatter *priceFormatter3 = [NSNumberFormatter new];
    [priceFormatter3 setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter3 setMinimumFractionDigits:3];
    [priceFormatter3 setMaximumFractionDigits:3];
    
    NSNumberFormatter *priceFormatter2 = [NSNumberFormatter new];
    [priceFormatter2 setNumberStyle:NSNumberFormatterDecimalStyle];
    [priceFormatter2 setMinimumFractionDigits:2];
    [priceFormatter2 setMaximumFractionDigits:2];
    
    NSString *chgStr = [priceFormatter2 stringFromNumber:[NSNumber numberWithFloat:chg]];
    NSString *chgPerStr = [priceFormatter2 stringFromNumber:[NSNumber numberWithFloat:chgPer]];
    chgPerStr = [chgPerStr stringByAppendingString:@"%"];
    NSString *lastDoneStr = [priceFormatter3 stringFromNumber:[NSNumber numberWithFloat:lastDone]];
    
    if (chg>0) {
        lblIndex.text = [NSString stringWithFormat:@"%@ (+%@, +%@)",lastDoneStr,chgStr,chgPerStr];
    } else {
        lblIndex.text = [NSString stringWithFormat:@"%@ (%@, %@)",lastDoneStr,chgStr,chgPerStr];
    }
    
    if (chg<0)
        lblIndex.textColor = [UIColor redColor];
    else if(chg>0)
        lblIndex.textColor = [UIColor greenColor];
    else
        lblIndex.textColor = [UIColor grayColor];
    
    if (chgPer==-100) {
        lblIndex.hidden = YES;
    }else{
        lblIndex.hidden = NO;
    }
}





#pragma mark -
#pragma mark TableView Delegate
//====================================================================
//                        TableView Delegate
//====================================================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    ////NSLog(@"%d", selection);
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (tableView.tag)
    {
        case 0:
        {
            ////NSLog(@"favId_name count: %lu",(unsigned long)[[[UserPrefConstants singleton]userWatchListDict_FavId_Name]count]);
            cells= [NSMutableArray array];
            return [[[UserPrefConstants singleton]userWatchListDict_FavId_Name]count];
            
            break;
        }
            
        case 1:
        {
            ////NSLog(@"userWatchListStockCodeArr count: %lu",(unsigned long)[[[UserPrefConstants singleton]userWatchListStockCodeArr]count]);
            return [[[UserPrefConstants singleton]userWatchListStockCodeArr]count];
            break;
        }
            
        case 2:
            return [[[UserPrefConstants singleton]userExchagelist]count];
        case 10:
            return [[[LanguageManager defaultManager] supportedLanguages] count];
        case  3:
        {
            WatchListArray=[[[UserPrefConstants singleton]userWatchListDict_FavId_Name]allValues];
            
            return [WatchListArray count];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellDefault";
    UITableViewCell *defaultCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    switch (tableView.tag) {
            
            // MARK: Languages
        case 10: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"default"];
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"default"];
            }
            
            NSString *key = [[[LanguageManager defaultManager] supportedLanguages] objectAtIndex:indexPath.row];
            cell.textLabel.text = [LanguageManager getLanguage:key andKey:@"LanguageTitle"];
            
            if ([key isEqualToString:[LanguageManager defaultManager].language]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
           // cell.textLabel.textColor = [UIColor whiteColor];
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"lang_%@.png",key]];
            
            return cell;
        }
            break;
            
            // MARK: Exchanges
        case 2:
        {   ED=[[ExchangeData alloc]init];
            ED=[userExchangelist objectAtIndex:indexPath.row];
            static NSString *cellIdentifier = @"cell";
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            }
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            //            if (indexPath.row % 2 == 0) {
            //                UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TableRow_Dark.png"]];
            //                [cell setBackgroundView:imgView];
            //            } else {
            //                UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TableRow_Light.png"]];
            //                [cell setBackgroundView:imgView];
            //
            //            }
            
            [cell.imageView setHidden:YES];
            NSLog(@"ED Feed Exchange Code %@",ED.feed_exchg_code);
            if ([ED.feed_exchg_code isEqualToString:@"KL"] || [ED.feed_exchg_code isEqualToString:@"KLD"] || [ED.feed_exchg_code isEqualToString:@"MY"]|| [ED.feed_exchg_code isEqualToString:@"KLL"]) {
                [cell.imageView setImage:[UIImage imageNamed:@"Flag_MY.png"]];
            }
            else if ([ED.feed_exchg_code isEqualToString:@"SG"] || [ED.feed_exchg_code isEqualToString:@"SGD"] || [ED.feed_exchg_code isEqualToString:@"SI"] || [ED.feed_exchg_code isEqualToString:@"SID"]) {
                [cell.imageView  setImage:[UIImage imageNamed:@"Flag_SG.png"]];
            }
            else if ([ED.feed_exchg_code isEqualToString:@"JK"] || [ED.feed_exchg_code isEqualToString:@"JKD"]) {
                [cell.imageView  setImage:[UIImage imageNamed:@"Flag_JK.png"]];
            }
            else if ([ED.feed_exchg_code isEqualToString:@"HK"] || [ED.feed_exchg_code isEqualToString:@"HKD"]) {
                [cell.imageView  setImage:[UIImage imageNamed:@"Flag_HK.png"]];
            }
            else if ([ED.feed_exchg_code isEqualToString:@"A"] || [ED.feed_exchg_code isEqualToString:@"AD"] || [ED.feed_exchg_code isEqualToString:@"O"] || [ED.feed_exchg_code isEqualToString:@"OD"] || [ED.feed_exchg_code isEqualToString:@"N"] || [ED.feed_exchg_code isEqualToString:@"ND"]) {
                [cell.imageView  setImage:[UIImage imageNamed:@"Flag_US.png"]];
            }
            else if ([ED.feed_exchg_code isEqualToString:@"UC"] || [ED.feed_exchg_code isEqualToString:@"ZS"] || [ED.feed_exchg_code isEqualToString:@"YM"] || [ED.feed_exchg_code isEqualToString:@"ES"]) {
                [cell.imageView  setImage:[UIImage imageNamed:@"Flag_CME.png"]];
            }
            else if ([ED.feed_exchg_code isEqualToString:@"BK"] || [ED.feed_exchg_code isEqualToString:@"BKD"] || [ED.feed_exchg_code isEqualToString:@"PH"]) {
                [cell.imageView  setImage:[UIImage imageNamed:@"Flag_BK.png"]];
            }else{
                 [cell.imageView  setImage:[UIImage imageNamed:@"Flag_MY.png"]];
            }
            CALayer *cellImageLayer = cell.imageView.layer;
            [cellImageLayer setCornerRadius:9];
            [cellImageLayer setMasksToBounds:NO]; // setting to YES will cause performance issue (lag scroll)
            cell.imageView.clipsToBounds = YES; // use clipstobounds instead anywhere possible
            NSString *currentValue =[NSString stringWithFormat:@" %@",ED.exchange_name];
            cell.textLabel.text = currentValue;
            if ([ED.feed_exchg_code isEqualToString:@"HC"]) {
                 cell.detailTextLabel.text = [NSString stringWithFormat:@" (HSX)"];
            }else
            if ([ED.feed_exchg_code isEqualToString:@"HN"] ) {
                 cell.detailTextLabel.text = [NSString stringWithFormat:@" (HNX)"];
            }else{
                 cell.detailTextLabel.text = [NSString stringWithFormat:@" (%@)",ED.feed_exchg_code];
            }
           
            cell.detailTextLabel.textColor = [UIColor grayColor];
            // cell.textLabel.textColor = [UIColor whiteColor];
            
            if (indexPath.row == _selection) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            return cell;
        }
            break;
            
    }
    return defaultCell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag==10)
        return 40;
    else
        return 60;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
 
    if (indexPath.row % 2==0) {
        
        cell.backgroundColor = kCellLightGray1;
    } else {
        
        cell.backgroundColor = kCellLightGray2;
    }
    
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSString *selectedExchange = [[AppConstants ExchangeCodeList  ] objectAtIndex:indexPath.row];
    //    ////NSLog(@"selectedExchange : %@",selectedExchange);
    
    // MARK: SELECTING EXCHANGE
    if (tableView.tag==2) {
        
        [self unSelectAllButtons];
        
        [UserPrefConstants singleton].sectorToFilter = nil;
        
        [columnPickerPopover dismissPopoverAnimated:YES];
        ED= [userExchangelist objectAtIndex:indexPath.row];
        
        [[VertxConnectionManager singleton] vertxUnsubscribeAllInQcFeed];
        
        // CLEAR PREVIOUS QCFEED DATA WHEN CHANGING EXCHANGE
        [[[QCData singleton]qcFeedDataDict] removeAllObjects];
        [[UserPrefConstants singleton].tickerStocks removeAllObjects];
        
        
        // CLEAR PREVIOUS SCOREBOARD DATA WHEN CHANGING EXCHANGE
        [QCData singleton].qcScoreBoardDict = [[NSMutableDictionary alloc] init];
        
        [UserPrefConstants singleton].userCurrentExchange =ED.feed_exchg_code;
        //IndicesButton.enabled=[[UserPrefConstants singleton].userCurrentExchange isEqual:@"KL"]?(YES):(NO);
        [self updateNaviButtons];
        
        
        NSString *exchgStr = [NSString stringWithFormat:@"Exchange%@",[UserPrefConstants singleton].brokerCode];
        [[NSUserDefaults standardUserDefaults]setObject:ED.feed_exchg_code forKey:exchgStr];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[VertxConnectionManager singleton]   vertxGetKLCI];
        
        [[UserPrefConstants singleton] overrideChartSettings];
        
        [self showBackBtn:YES];
//        [UserPrefConstants singleton].currentSideBtnTag = kPageHome;
//        [self.containerViewController switchToViewController:@"segueHome"];


         
        _prev_selection = _selection;
        _selection = (int)indexPath.row;
        // [self enableAllButtonsExcept:_homeButton];
        [UserPrefConstants singleton].userSelectingStockCode = nil;
        [Exchangetablevc.tableView reloadData];
        
         [[VertxConnectionManager singleton] vertxSortForTicker:@"101" Direction:@"DESC" exchg:[UserPrefConstants singleton].userCurrentExchange];
        
        // update current Exchange Info
        [UserPrefConstants singleton].currentExchangeInfo =
        [[UserPrefConstants singleton].vertxExchangeInfo objectForKey:[UserPrefConstants singleton].userCurrentExchange];
        
        
        // no need to go to Home screen, but reload only current page
        // request by CIMB SG (change exchange without reset to Home screen) - June 2017
        
        int iCurrentSideBtnTag = [[arrStackVCs lastObject] intValue ];

        if ( iCurrentSideBtnTag == 10) {
            fromWatchList = YES;
            [UserPrefConstants singleton].fromWatchlist = YES;
        } else {
            fromWatchList = NO;
            [UserPrefConstants singleton].fromWatchlist = NO;
        }
        
        UIButton *selectedBtn = (UIButton*)[self.viewSideBar viewWithTag: iCurrentSideBtnTag];
        [self enableAllButtonsExcept:selectedBtn];
        [selectedBtn setSelected:YES];
        NSString *segueStr = [self checkTag: iCurrentSideBtnTag];
        
        
        // If in stock detail, then wait till get ticker stocks first only then reload the stk detail!
        if (iCurrentSideBtnTag==kPageDetail){
            // do nothing
            justChangeExchange = YES;
        } else {
            [self.containerViewController switchToViewController:segueStr];
        }
        
        
    } else if (tableView.tag==10)  {
        // MARK: SELECTING LANGUAGE
        //
        NSString *selectedLang = [[[LanguageManager defaultManager] supportedLanguages] objectAtIndex:indexPath.row];
      //  NSLog(@"Selected Language%@",selectedLang);
        
        [[LanguageManager defaultManager] setLanguage:selectedLang];
        
        // [UserPrefConstants singleton].currentLanguage = selectedLang;
        [columnPickerPopover dismissPopoverAnimated:YES];
        
        //  LanguageManager *lm = [LanguageManager defaultManager];
         [[LanguageManager defaultManager] translateStringsForViewController:self];
        
        [_languageBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"lang_%@.png",selectedLang]] forState:UIControlStateNormal];
        
        
        // Must reload the current page because data from server also need to be
        // reloaded to selected language
        [self reloadCurrentPage];
    }
    
    [atp timeoutActivity];
    
}

// After language changed:
-(void)reloadCurrentPage {
    
    int iCurrentSideBtnTag = [[arrStackVCs lastObject] intValue ];

    switch (iCurrentSideBtnTag) {
        case kPageHome:
            [self btnSeguePressed:_homeButton];
            break;
        case kPageQuote:
            [self btnSeguePressed:_quotesButton];
            break;
        case kPageWatchList:
            [self btnSeguePressed:_watchlistButton];
            break;
        case kPageIndex:
            [self btnSeguePressed:_IndicesButton];
            break;
        case kPageDetail:
            [self btnSeguePressed:_stockDetailsButton];
            break;
        case kPageOrderBk:
            [self btnSeguePressed:_orderBookButton];
            break;
        case kPageESettlement:
            [self btnSeguePressed:_eSettlementButton];
            break;
        case kPageStockAlert:
            [self btnSeguePressed:_stockAlertButton];
            break;
        case kPageSettings:
            [self btnSeguePressed:_settingsButton];
        break;
        case kMarketStreamer:
             [self btnSeguePressed:_marketStreamerButton];
            break;
        default:
        ;// dont reload anything
            break;
    }
}


#pragma mark - ATP reply


-(void) didReceiveExchangeList {
    NSLog(@"UserPrefConstants singleton].userExchagelist %@",[UserPrefConstants singleton].userExchagelist);
    if ([[UserPrefConstants singleton].userExchagelist count]>0) {
        
        _exchangeButton.enabled = YES;
        userExchangelist =[UserPrefConstants singleton].userExchagelist;
        // if current exchange not available in list, select first exchange in list and reload current page
        
        BOOL found = NO;
        for (int i=0; i<userExchangelist.count; i++)
        {
            if ([[[userExchangelist objectAtIndex:i] feed_exchg_code] isEqualToString:
                 [UserPrefConstants singleton].userCurrentExchange]) {
                found = YES;
                break;
            }
        }

        if (!found) {
            [UserPrefConstants singleton].userCurrentExchange = [[userExchangelist objectAtIndex:0] feed_exchg_code];
            NSString *exchgStr = [NSString stringWithFormat:@"Exchange%@",[UserPrefConstants singleton].brokerCode];
            [[NSUserDefaults standardUserDefaults]setObject:[UserPrefConstants singleton].userCurrentExchange forKey:exchgStr];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            NSString *segueStr = [self checkTag:  [[arrStackVCs lastObject] intValue] ];
            
            [self.containerViewController switchToViewController:segueStr];
        }
        
    } else {
        _exchangeButton.enabled = NO;
        [atp serverLogIn];
    }
    
    [self updateNaviButtons];
}

-(void)didReceiveUserAccountsList {
    if ([[UserPrefConstants singleton].userAccountlist count]>0) {
        
        _orderBookButton.enabled = YES;
    } else {
        _orderBookButton.enabled = NO;
    }
    
    [self updateNaviButtons];
}


- (void) dealloc{
    
    //NSLog(@"goodbye");
    //
}

//====================================================================
//                              Ends
//====================================================================

- (void)animateLabels :(UIView *)viewAnim delay:(float)delay
{
    [UIView animateWithDuration:40.0f
                          delay:delay
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         // animate the container frame to the right 320 points
                         viewAnim.frame = CGRectOffset(viewAnim.frame, -3645, 0);
                     }
                     completion:^(BOOL finished) {
                         // move it back to where it started
                         tickerView.frame = CGRectMake(1665, 0, 100, 40);
                         [self animateLabels:viewAnim delay:0];
                     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    // //NSLog(@"%s", __PRETTY_FUNCTION__);
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // //NSLog(@"%s", __PRETTY_FUNCTION__);
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        _containerViewController = segue.destinationViewController;
    }
    
    if ([segue.identifier isEqualToString:@"searchSegueID"]) {
        // setup Search Mode or AddStock Mode
        SearchViewController *vc =  (SearchViewController*)segue.destinationViewController;
        NSString *str = (NSString*)sender;
        if ([str isEqualToString:@"Search"]) {
            vc.panelMode = PANEL_MODE_SEARCH;
        } else {
            vc.panelMode = PANEL_MODE_ADDSTOCK;
        }
        
    }
}


-(void)unSelectAllButtons {
    
    [_homeButton setSelected:NO];
    [_quotesButton setSelected:NO];
    [_stockDetailsButton setSelected:NO];
    [_marketStreamerButton setSelected:NO];
    [_orderBookButton setSelected:NO];
    [_IndicesButton setSelected:NO];
    [_settingsButton setSelected:NO];
    [_eSettlementButton setSelected:NO];
    [_watchlistButton setSelected:NO];
    [_stockAlertButton setSelected:NO];
    [_elasticNewsButton setSelected:NO];
}


-(void)enableAllButtons:(BOOL)enable {
    
    [_homeButton setUserInteractionEnabled:enable];
    [_quotesButton setUserInteractionEnabled:enable];
    [_stockDetailsButton setUserInteractionEnabled:enable];
    [_marketStreamerButton setUserInteractionEnabled:enable];
    [_orderBookButton setUserInteractionEnabled:enable];
    [_IndicesButton setUserInteractionEnabled:enable];
    [_settingsButton setUserInteractionEnabled:enable];
    [_eSettlementButton setUserInteractionEnabled:enable];
    [_watchlistButton setUserInteractionEnabled:enable];
    [_stockAlertButton setUserInteractionEnabled:enable];
    [_elasticNewsButton setUserInteractionEnabled:enable];
}


-(void)enableAllButtonsExcept:(UIButton*)btn {
    
    [self enableAllButtons:YES];
    [btn setUserInteractionEnabled:NO];
    
}


- (void) show2FAAlert {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_2FA;
    [self presentViewController:validVC animated:YES completion:nil];
}

- (void) showSMSOTPAlert {
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_SMS;
    [self presentViewController:validVC animated:YES completion:nil];
}

- (void) showBothOTPAlert {
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    validVC = [storyboard instantiateViewControllerWithIdentifier:@"ValidatePINViewController"];
    validVC.modalPresentationStyle= UIModalPresentationFormSheet;
    validVC.modalTransitionStyle =UIModalTransitionStyleCrossDissolve;
    validVC.preferredContentSize =CGSizeMake(320, 300);
    validVC.delegate = self;
    validVC.validatingType = VAL_SMS_PIN;
    [self presentViewController:validVC animated:YES completion:nil];
}

- (void) showSLCAlertPopup{
    if (alertInfo!=nil) {
        return;
    }
    
    alertInfo = [[SCLAlertView alloc] init];
    alertInfo.attributedFormatBlock = ^NSAttributedString* (NSString *value)
    {
        
        NSString *string = @"1. Click the OTP via SMS button to retrieve your One-Time Password (OTP)\n2. Enter the One-Time Password sent to your mobile number\n3. Click ""OK"" to proceed";
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
        [paragraphStyle setAlignment:NSTextAlignmentLeft];
        
        NSMutableAttributedString *subTitle = [[NSMutableAttributedString alloc]initWithString:string];
        [subTitle addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
        return subTitle;
    };
    
    
    alertInfo.hideAnimationType = SCLAlertViewHideAnimationSimplyDisappear;
    alertInfo.customViewColor = [UIColor redColor];
    alertInfo.backgroundType = SCLAlertViewBackgroundBlur;
    alertInfo.shouldDismissOnTapOutside = YES;
    [alertInfo showInfo:self title:TC_Pro_Mobile subTitle:@"1. Click the OTP via SMS button to retrieve your One-Time Password (OTP)\n2. Enter the One-Time Password sent to your mobile number\n3. Click ""OK"" to proceed" closeButtonTitle:@"Close" duration:0.0f]; // Notice
}

#pragma mark - ValidatePIN Delegate

-(void)validatePINcancelClicked {
    [[ATPAuthenticate singleton] timeoutActivity];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)validatePINokBtnClickedWithPassword:(NSString *)passwd andType:(ValType)type {
    // in this VC only VAL_2FA used so no need to check for type
    
    if (type==VAL_SMS_PIN) {
        if ([UserPrefConstants singleton].currentOTPView==0) {
            [UserPrefConstants singleton].dvData2FA.otpPin = passwd;
            [[ATPAuthenticate singleton] validate2FAPinForDevice:[UserPrefConstants singleton].dvData2FA forTrade:NO];
        }else{
               [[ATPAuthenticate singleton] doValidateSMSOTP:passwd andTrade:NO];
        }
        
    }else if (type==VAL_SMS) {
        [[ATPAuthenticate singleton] doValidateSMSOTP:passwd andTrade:NO];
        
        
    }else{
        [UserPrefConstants singleton].dvData2FA.otpPin = passwd;
        [[ATPAuthenticate singleton] validate2FAPinForDevice:[UserPrefConstants singleton].dvData2FA forTrade:NO];
    }
    
 
    
   // TODO [_errorMessageLabel setText:[LanguageManager stringForKey:@"Validating OTP Pin..."]];
    
}

#pragma mark - Validate SMS OTP Notification

-(void)validateSMSOTPPinForDeviceDone:(NSNotification*)notification {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSDictionary *replyDict = notification.userInfo;
        NSString *statusCode = [replyDict objectForKey:@"statusCode"];
        NSString *statusMsg = [replyDict objectForKey:@"statusMsg"];
        NSString *errorMsg = [replyDict objectForKey:@"errorMsg"];
        
        // statuscode 0 is success for CIMB SG. WTF
        if ([statusCode isEqualToString:@"200"]||[statusCode isEqualToString:@"0"]) {
            
            // success!
            [UserPrefConstants singleton].verified2FA = YES;
            [UserPrefConstants singleton].orderDetails.otpPin = [UserPrefConstants singleton].dvData2FA.otpPin;
            [UserPrefConstants singleton].orderDetails.deviceID = [UserPrefConstants singleton].dvData2FA.deviceID;
            [self openScreenWithSender:_orderBookButton];
            
        } else {
            
            //TODO   [_loadingActivity stopAnimating];
            //TODO   _errorMessageLabel.text = @"";
            
            // alert wrong password
            if ([errorMsg length]<=0) errorMsg = statusMsg;
            [[UserPrefConstants singleton] popMessage:[NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg]
                                         inController:self withTitle:[LanguageManager stringForKey:TC_Pro_Mobile]];
            
        }
        
    });
}

#pragma mark - Validate2FA ATP Notification


-(void)validate2FAPinForDeviceDone:(NSNotification*)notification {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSDictionary *replyDict = notification.userInfo;
        NSString *statusCode = [replyDict objectForKey:@"statusCode"];
        NSString *statusMsg = [replyDict objectForKey:@"statusMsg"];
        NSString *errorMsg = [replyDict objectForKey:@"errorMsg"];
        
        // statuscode 0 is success for CIMB SG. WTF
        if ([statusCode isEqualToString:@"200"]||[statusCode isEqualToString:@"0"]) {
            
            // success!
            [UserPrefConstants singleton].verified2FA = YES;
            [UserPrefConstants singleton].orderDetails.otpPin = [UserPrefConstants singleton].dvData2FA.otpPin;
            [UserPrefConstants singleton].orderDetails.deviceID = [UserPrefConstants singleton].dvData2FA.deviceID;
            [self openScreenWithSender:_orderBookButton];
            
        } else {
            
         //TODO   [_loadingActivity stopAnimating];
         //TODO   _errorMessageLabel.text = @"";
            
            // alert wrong password
            if ([errorMsg length]<=0) errorMsg = statusMsg;
            [[UserPrefConstants singleton] popMessage:[NSString stringWithFormat:@"%@ - %@",statusCode, errorMsg]
                                         inController:self withTitle:[LanguageManager stringForKey:TC_Pro_Mobile]];
            
        }
        
    });
}





-(void)checkAuthentication {
    
    
    if ([[UserPrefConstants singleton].byPass2FA rangeOfString:@"ALL" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        //Non 2FA
        [self openScreenWithSender:_orderBookButton];
    }
    else {
        if ([UserPrefConstants singleton].is2FARequired) {
            if (![UserPrefConstants singleton].verified2FA) {
                 NSLog(@"deviceListForSMSOTP %lu",(unsigned long)[[UserPrefConstants singleton].deviceListForSMSOTP count]);
                if ([[UserPrefConstants singleton].deviceListFor2FA count] != 0 && [[UserPrefConstants singleton].deviceListForSMSOTP count] !=0) {
                    //2FA
                    [self showBothOTPAlert];
                }
                
                else if ([[UserPrefConstants singleton].deviceListFor2FA count] != 0) {
                    //2FA
                    [self show2FAAlert];
                }
                else if([[UserPrefConstants singleton].deviceListForSMSOTP count] !=0) {
                    NSLog(@"deviceListForSMSOTP %lu",(unsigned long)[[UserPrefConstants singleton].deviceListForSMSOTP count]);
                    [self showSMSOTPAlert];
                }
                else {
                    if ([[UserPrefConstants singleton].byPass2FA rangeOfString:@"Y" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        //Non 2FA
                        [self openScreenWithSender:_orderBookButton];
                    }
                    else {
                        //Error Message
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[LanguageManager stringForKey:@"CIMBOneTokenMessage"] delegate:nil cancelButtonTitle:@"OK"	otherButtonTitles:nil];
                        [alert show];
                        return;
                    }
                }
            }
            else {
                
                [UserPrefConstants singleton].orderDetails.otpPin = [UserPrefConstants singleton].dvData2FA.otpPin;
                [UserPrefConstants singleton].orderDetails.deviceID = [UserPrefConstants singleton].dvData2FA.deviceID;
                [self openScreenWithSender:_orderBookButton];
            }
        }
        else {
            [self openScreenWithSender:_orderBookButton];
        }
    }
    
}

#pragma mark - Side buttons pressed

- (IBAction)btnSeguePressed:(id)sender {
    
    isOpenVCFromBtnBACK = FALSE;
    
    [self fnOpenScreen:sender];
}

- (void) pressFromBackBtn:(id)sender {
    isOpenVCFromBtnBACK = TRUE;
    
    [self fnOpenScreen:sender];
}

-(void) fnOpenScreen :(id)sender {
    [atp timeoutActivity];
    
    
    if ([sender tag]==6) {
        
        [self checkAuthentication];

    } else {
    
        [self openScreenWithSender:sender];
    }
}

-(void)openScreenWithSender:(id)sender {
    [self unSelectAllButtons];
    UIButton *btn = (UIButton*)sender;
    [btn setUserInteractionEnabled:NO];
    [btn setSelected:YES];
    
    [self enableAllButtons:NO];
    // prevent spam
    [self performSelector:@selector(enableAllButtonsExcept:) withObject:btn afterDelay:1.0];
    
    NSString *segueStr = [self checkTag:[sender tag]];
    
    NSLog(@"Segue Str %@",segueStr);
    
    [[ButtonEffect sharedCenter]flarify:sender inParentView:self.view withColor:[UIColor redColor] enableUponCompletion:NO];
    // NSLog(@"segue: %@",segueStr);
    //NSLog(@"Previous Tag %d and CurrentTag %d", previousTagButton, currentTagButton);
    
    if ([sender tag]==10) {
        fromWatchList = YES;
        [UserPrefConstants singleton].fromWatchlist = YES;
    } else {
        fromWatchList = NO;
        [UserPrefConstants singleton].fromWatchlist = NO;
    }
    
    [self showBackBtn:YES];
    
    [self.containerViewController switchToViewController:segueStr];
    
    //If click on Default screen => Refresh list stack...Hidden btn Back
    
    switch ( [UserPrefConstants singleton].defaultView) {
        case 1:
//            _homeButton
            if ([segueStr isEqualToString:@"segueHome" ]) {
                [self resetListStackVCs];
            }
            break;
        case 2:
//            _quotesButton
            if ([segueStr isEqualToString:@"segueQuoteScreen" ]) {
                [self resetListStackVCs];
            }

            break;
        case 3:
//            _watchlistButton
            if ([segueStr isEqualToString:@"segueHome" ]) {
                [self resetListStackVCs];
            }
            break;
            
        default:
            break;
    }
    
    if (!isOpenVCFromBtnBACK) {
        
        //append new screen into the stack
        if (arrStackVCs.count > kLimitStackVC) {
            //remove the oldest:
            [arrStackVCs removeObjectAtIndex:0];
        }
        //add the new one:
        [arrStackVCs addObject: [NSNumber numberWithInt: (int)[sender tag] ] ];
    }
}

-(void) resetListStackVCs
{
    [self showBackBtn:NO];
    [arrStackVCs removeAllObjects];
}

- (IBAction)backPressed:(id)sender {
    
    isOpenVCFromBtnBACK = TRUE;
    
    //remove last screen from stack
    [arrStackVCs removeLastObject];

    //go to previous one
    NSNumber* num = [arrStackVCs lastObject];

        switch ( [num intValue] ) {
            case kPageHome:
                [self pressFromBackBtn:_homeButton];
                break;
            case kPageQuote:
                [self pressFromBackBtn:_quotesButton];
                break;
            case kPageIndex:
                [self pressFromBackBtn:_IndicesButton];
                break;
            case kPageDetail:
                [self pressFromBackBtn:_stockDetailsButton];
                break;
                
            case kPageOrderBk:
                [self pressFromBackBtn:_orderBookButton];
                break;
            case kPageSettings:
                [self pressFromBackBtn:_settingsButton];
                break;
            case kPageWatchList:{
                [self pressFromBackBtn:_watchlistButton];
            }break;
            case kPageESettlement:
            {
                [self pressFromBackBtn:_eSettlementButton];
            }
                break;
            case kPageStockAlert:
            {
                [self pressFromBackBtn:_stockAlertButton];
            }
                break;
                
            case kMarketStreamer:
                 [self pressFromBackBtn:_marketStreamerButton];
                break;
            case kStockTracker:
                [self pressFromBackBtn:_marketStreamerButton];
                break;
            default:{
                ////NSLog(@"default");
                [self pressFromBackBtn:_homeButton];
            }
                break;
        }
        
    //Back to Default screen ? => Hidden Btn back.
    if (arrStackVCs.count == 0) {
        [self resetListStackVCs];
    }
}

- (IBAction)testUnsub:(id)sender {
    
    //test
    [[VertxConnectionManager singleton] vertxUnsubscribeKLCI];
}

- (IBAction)settingsClicked:(id)sender {
}


-(void)showBackBtn:(BOOL)showBack {
    
    CGFloat gap = 10;
    
    if (showBack) {
        _backBtn.hidden = NO;
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _n2nconnectBhd.center = CGPointMake(gap+_backBtn.center.x+_backBtn.frame.size.width/2.0+_n2nconnectBhd.frame.size.width/2.0, _n2nconnectBhd.center.y);
            imgNetwork.center = CGPointMake(_n2nconnectBhd.center.x+_n2nconnectBhd.frame.size.width/2.0+imgNetwork.frame.size.width/2.0, imgNetwork.center.y);
        } completion:^(BOOL finished) {
            
        }];
        
    } else {
        [UIView animateWithDuration:0.35 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            _backBtn.hidden = YES;
            _n2nconnectBhd.center = CGPointMake(_backBtn.center.x-_backBtn.frame.size.width/2.0+_n2nconnectBhd.frame.size.width/2.0, _n2nconnectBhd.center.y);
            imgNetwork.center = CGPointMake(_n2nconnectBhd.center.x+_n2nconnectBhd.frame.size.width/2.0+imgNetwork.frame.size.width/2.0, imgNetwork.center.y);
        } completion:^(BOOL finished) {
            
        }];
    }
    
}


- (NSString *)checkTag :(long)tag
{
    NSString *segueString = @"segueHome";
    switch(tag)
    {
        case 1: segueString = @"segueHome";
            break;
        case 2: segueString = @"segueQuoteScreen";
            break;
        case 3: segueString = @"segueIndices";
            break;
        case 4: segueString = @"segueStockDetail";
            break;
        case 10: segueString = @"segueQuoteScreen"; // new watchlist
            break;
        case 6: segueString =@"segueOrderbook";
            break;
        case 7: segueString =@"segueSettings";
            break;
        case 8:
            segueString = @"segueESettlement";
            break;
        case 9:
            segueString = @"segueStockAlert";
            break;
        case 11:
            segueString = @"segueElasticNews";
            break;
        case 12:
            if ([[UserPrefConstants singleton].userCurrentExchange isEqualToString:@"PH"]) {
                segueString = @"segueStockTracker";
            }else{
                segueString = @"segueMarketStreamer";
            }
   
            break;
        case 13:
            segueString = @"segueStockTracker";
            break;
            
    }
    return segueString;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //    ////NSLog(@"Search btn Clicked");
    //    [homeButton setUserInteractionEnabled:YES];
    //    [quotesButton setUserInteractionEnabled:YES];
    //    [stockDetailsButton setUserInteractionEnabled:YES];
    //    [_orderBookButton setUserInteractionEnabled:YES];
    //    [homeButton setSelected:NO];
    //    [quotesButton setSelected:NO];
    //    [IndicesButton setSelected:NO];
    //    [stockDetailsButton setSelected:NO];
    //    [_orderBookButton setSelected:NO];
    //    [_eSettlementButton setSelected:NO];
    //    //_orderBookButton.enabled =[UserPrefConstants singleton].userAccountlist.count>0?(YES):(NO);
    //
    //    //IndicesButton.enabled=[[UserPrefConstants singleton].userCurrentExchange isEqual:@"KL"]?(YES):(NO);
    //    [[VertxConnectionManager singleton] vertxSearch:_searchBar.text];
    //    [atp timeoutActivity];
    
}

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText
{
    ////NSLog(@"Search did Changed");
    //        [[VertxConnectionManager singleton] vertxSearch:@"ma"];
    

}


- (IBAction)callLanguageSelection:(UIButton*)sender {
    
    [LanguageTableVC.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:LanguageTableVC];
    navController.navigationBar.topItem.title=[LanguageManager stringForKey:@"Languages"];
    
//  [navController.navigationBar
//  setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor cyanColor]}];
//  navController.navigationBar.barTintColor = kPanelColor;
//  LanguageTableVC.tableView.backgroundColor = kPanelColor;
    LanguageTableVC.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    NSInteger totalToDisplay = [[[LanguageManager defaultManager] supportedLanguages] count];
    if (totalToDisplay>=5) totalToDisplay = 5;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*40);
    
    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
   // columnPickerPopover.backgroundColor = kPanelColor;
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:self.view
                       permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
}

- (IBAction)ExchangeBtnClicked:(UIButton *)sender {
    
    
    NSString *exchgStr = [NSString stringWithFormat:@"Exchange%@",[UserPrefConstants singleton].brokerCode];
    
    userExchangelist=[UserPrefConstants singleton].userExchagelist;
    
    if (_selection==-1) {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:exchgStr]!=nil) {
            for (int i=0; i<userExchangelist.count; i++)
            {
                if ([[[userExchangelist objectAtIndex:i] feed_exchg_code] isEqualToString:
                     [[NSUserDefaults standardUserDefaults] objectForKey:exchgStr]]) {
                    _selection=i;
                    break;
                }
            }
        }
    }
    
    [Exchangetablevc.tableView reloadData];
    
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:Exchangetablevc];
    navController.navigationBar.topItem.title= [LanguageManager stringForKey:@"Exchanges"];
    
//    [navController.navigationBar
//     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor cyanColor]}];
//    navController.navigationBar.barTintColor = kPanelColor;
//    Exchangetablevc.tableView.backgroundColor = kPanelColor;
    Exchangetablevc.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [navController setToolbarHidden:YES];
    
    NSInteger totalToDisplay = [userExchangelist count];
    if (totalToDisplay>=10) totalToDisplay = 10;
    navController.preferredContentSize=CGSizeMake(325, totalToDisplay*60);

    columnPickerPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
   // columnPickerPopover.backgroundColor = kPanelColor;
    [columnPickerPopover presentPopoverFromRect:sender.frame
                                         inView:self.view
                       permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
}



- (IBAction)orderBookPressed:(id)sender {
    
}

//- (void)enableButton{
//    
//    [homeButton setUserInteractionEnabled:YES];
//    [quotesButton setUserInteractionEnabled:YES];
//    [IndicesButton setUserInteractionEnabled:YES];
//    [IndicesButton setSelected:NO];
//    [homeButton setSelected:NO];
//    [quotesButton setSelected:NO];
//    //IndicesButton.enabled=[[UserPrefConstants singleton].userCurrentExchange isEqual:@"KL"]?(YES):(NO);
//    [stockDetailsButton setUserInteractionEnabled:NO];
//    [stockDetailsButton setSelected:YES];
//}


- (IBAction)callSearch:(id)sender {
    
    NSString *panelMode = @"";
    if ([sender isKindOfClass:[UIButton class]]) {
        panelMode = @"Search";
    } else {
        panelMode = @"AddStock";
    }
    
    [self performSegueWithIdentifier:@"searchSegueID" sender:panelMode];
    
}

// Call JQC

@end
