//
//  ESettlementViewController.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 18/12/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "ESettlementViewController.h"
#import "ESettlementContainerViewController.h"
#import "ESettlementTableViewController.h"

@interface ESettlementViewController (){
    BOOL runOnce;
}

@property (nonatomic, weak) ESettlementContainerViewController *containerViewController;

@end

@implementation ESettlementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    runOnce = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    
    if (runOnce) {
        runOnce = NO;
        CGFloat xRatio = 1024.0/977.0;
        CGSize scrSize = [[UIScreen mainScreen] bounds].size;
        
        _tocContainerView.frame = CGRectMake(_tocContainerView.frame.origin.x, _tocContainerView.frame.origin.y,
                                             300.0, _tocContainerView.frame.size.height);
        
        _contentContainerView.frame = CGRectMake(_tocContainerView.frame.size.width,_tocContainerView.frame.origin.y,
                                                 (scrSize.width-_tocContainerView.frame.size.width)/xRatio,
                                                 _contentContainerView.frame.size.height);

    }
    [super viewDidLayoutSubviews];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"segueESettlement"])
    {
        ESettlementTableViewController *tocVC = (ESettlementTableViewController*)[segue destinationViewController];
        tocVC.frameToUse = _TOCContainer.frame;
        tocVC.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"embedESettlementContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }
    
}

- (void) didClickTableView:(NSString *)index andTitle:(NSString *)title{
    [self.containerViewController reloadPage:index andTitle:title];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
