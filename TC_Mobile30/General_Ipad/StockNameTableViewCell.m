//
//  StockNameTableViewCell.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 1/4/16.
//  Copyright (c) 2016 N2N. All rights reserved.
//

#import "StockNameTableViewCell.h"

@implementation StockNameTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
