//
//  UserDefaultData.m
//  TCiPad
//
//  Created by Ahmad Ashraf Azman on 27/10/2016.
//  Copyright © 2016 N2N. All rights reserved.
//

#import "UserDefaultData.h"
#import "UserPrefConstants.h"
#import "NSData-AES.h"
#import "FDKeychain.h"
#import "Base64.h"
#import "NSData+Base64.h"


@implementation UserDefaultData

//====================================================================
//               User Defaults Key = Bundle ID + Broker ID
//====================================================================

+ (NSString *)getUserDefaultsKey {
    NSString *userDefaultsKey = [NSString stringWithFormat:@"%@%@", [UserPrefConstants singleton].bundleID, ( [UserPrefConstants singleton].brokerCode!=nil && ![ [UserPrefConstants singleton].brokerCode isEqualToString:@""])? [UserPrefConstants singleton].brokerCode:@""];
    return userDefaultsKey;
}

+ (NSDictionary *)getUserDefaults {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:[self getUserDefaultsKey]] != nil) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:[self getUserDefaultsKey]];
    }
    
    return [NSDictionary dictionary];
}

+ (void)setUserDefaults:(NSDictionary *)userDefaultsDict {
    
    [[NSUserDefaults standardUserDefaults] setObject:userDefaultsDict forKey:[self getUserDefaultsKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//====================================================================
//               Touch ID Preferences
//====================================================================

+ (NSString *)generateRandomString:(int)num {
    NSMutableString* string = [NSMutableString stringWithCapacity:num];
    for (int i = 0; i < num; i++) {
        [string appendFormat:@"%C", (unichar)('a' + arc4random_uniform(25))];
    }
    return string;
}

+ (void)createTouchIDSecretKey{
    
    NSString *keyString = [self generateRandomString:50];
    NSMutableDictionary *userDefaultsDict = [NSMutableDictionary dictionaryWithDictionary:[self getUserDefaults]];
    [userDefaultsDict setObject:keyString forKey:[NSString stringWithFormat:@"appSecretkey+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]]];
    [self setUserDefaults:userDefaultsDict];
    
}

+ (NSString *)getTouchIDSecretKey{
    return (NSString *)[[self getUserDefaults] objectForKey:[NSString stringWithFormat:@"appSecretkey+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]]];
}


+(void)removeTouchIDFlagForUser:(NSString*)brokerCode {
    NSMutableDictionary *userDefaultsDict = [NSMutableDictionary dictionaryWithDictionary:[self getUserDefaults]];
    [userDefaultsDict removeObjectForKey:[NSString stringWithFormat:@"appTouchIDFlag+%@+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"],brokerCode]];
    [self setUserDefaults:userDefaultsDict];
   // NSLog(@"userDefaultsDict %@", userDefaultsDict);
    
}

+ (void)setTouchIDFlag:(BOOL)touchIDFlag forUser:(NSString*)brokerCode {
    NSMutableDictionary *userDefaultsDict = [NSMutableDictionary dictionaryWithDictionary:[self getUserDefaults]];
    [userDefaultsDict setObject:[NSNumber numberWithBool:touchIDFlag]
                         forKey:[NSString stringWithFormat:@"appTouchIDFlag+%@+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"],brokerCode]];
    [self setUserDefaults:userDefaultsDict];
}

+ (BOOL)getTouchIDFlagForUser:(NSString*)brokerCode {
    return [[[self getUserDefaults] objectForKey:[NSString stringWithFormat:@"appTouchIDFlag+%@+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"],brokerCode]] boolValue];
}


+ (NSString *) getUsername{
    NSError *error = nil;
    
    NSString *username = [FDKeychain itemForKey: [NSString stringWithFormat:@"%@+appUsernameTouchID",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]]
                                     forService: [NSString stringWithFormat:@"%@+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"],[UserPrefConstants singleton].brokerCode]
                                          error: &error];

    if (username.length==0) {
        return @"";
    }
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:username options:0];
    
    NSData *encryptedData = [decodedData AES256DecryptWithKey:kEncryptKey];
    
    NSString *decodedString = [[NSString alloc] initWithData:encryptedData encoding:NSUTF8StringEncoding];
    
    return decodedString;
    
}

+ (void) savePassword:(NSString *)password{
   
    NSData *nsdata = [password dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *encryptedData = [nsdata AES256EncryptWithKey:kEncryptKey];
    
    NSLog(@"1: %@", encryptedData);
    
    NSString *dataString = [encryptedData base64EncodedString];
    
     NSError *error = nil;
    
    [FDKeychain saveItem: dataString
                  forKey: [NSString stringWithFormat:@"%@+appPasswordTouchID",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]]
              forService: [NSString stringWithFormat:@"%@+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"],[UserPrefConstants singleton].brokerCode]
                   error: &error];
}
+ (NSString *) getPassword{
    NSError *error = nil;
    
    NSString *password = [FDKeychain itemForKey: [NSString stringWithFormat:@"%@+appPasswordTouchID",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]]
                                     forService: [NSString stringWithFormat:@"%@+%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"],[UserPrefConstants singleton].brokerCode]
                                          error: &error];
    
    if (password.length==0) {
        return @"";
    }

    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:password options:0];
    
    NSData *encryptedData = [decodedData AES256DecryptWithKey:kEncryptKey];
    
    NSString *decodedString = [[NSString alloc] initWithData:encryptedData encoding:NSUTF8StringEncoding];
    
    return decodedString;
}

+ (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key {
    return [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES128EncryptWithKeyUTF8:key];
}




@end
