//
//  AppDelegate.m
//  TCUniversal
//
//  Created by Scott Thoo on 8/28/14.
//  Copyright (c) 2014 N2N. All rights reserved.
//

#import "AppDelegate.h"
#import "VertxConnectionManager.h"
#import <AdSupport/ASIdentifierManager.h>
#import "UserPrefConstants.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LanguageManager.h"
#import "StockAlertAPI.h"
#import "AppMacro.h"
#import "SplashVC.h"
#import "TCMenuVC.h"
#import "BaseNavigationController.h"
#import "LoginMobileVC.h"
#import "Utils.h"
#import "HomeVC.h"
#import "VertxConnectionManager.h"
#import "ChartVC.h"
#import "BaseTabBarViewController.h"
#import "MStockStreamerVC.h"
#import "MDashBoardVC.h"

#import "MMainMenuVC.h"
#import "MPortfolioViewController.h"

//For NewVC with New Design
#import "AlertVC.h"
#import "ResearchVC.h"
#import "MTradeViewController.h"
#import "MStockPreviewController.h"
#import "StockAlertViewController.h"
#import "MarketsVC.h"

#import "MQuoteViewController.h"
#import "HomeVC.h"
#import "MHomeVC.h"
#import "MIndicesVC.h"
#import "MWatchlistVC.h"
#import "EmptyTradeVC.h"
#import "MOrderBookVCNew.h"

#import "MDashBoardVCV3.h"
#import "MOrderBookFilterVC.h"
#import "UIViewController+Popup.h"

#import "NetworkManager.h"
#import "ImageCacheManager.h"
#import "DataManager.h"
#import "LanguageKey.h"
#import "AuthenticationAPI.h"
@import Firebase;

@interface AppDelegate ()<UITabBarControllerDelegate, UITabBarDelegate, FIRMessagingDelegate>
{
    VertxConnectionManager *vcm;
    BaseNavigationController *_mainRootNav;
    MOrderBookFilterVC *filterVC;
}
@end

@implementation AppDelegate

@synthesize  delegateDelegate;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    //Check network
    [[NetworkManager shared] startNotifying];
    //Check Font:
    //     NSArray *fontFamilies = [UIFont familyNames];
    //     for (int i = 0; i < [fontFamilies count]; i++) {
    //     NSString *fontFamily = [fontFamilies objectAtIndex:i];
    //     NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
    //		 NSLog (@"Font Famili: %@: \nFONTNAME: %@", fontFamily, fontNames);
    //     }
    
    [Fabric with:@[[Crashlytics class]]];
    [[ImageCacheManager shareInstance] clearAllCache];
    [[DataManager shared] cleareDataLineChart];
    [LanguageManager loadFromJSONFile:[[NSBundle mainBundle] pathForResource:@"language.json" ofType:nil] defaultLanguage:@"en"];
    [LanguageManager defaultManager].noKeyPlaceholder = @"{key}"; // if no key, just show the key value.
    
    //For Mobile
    [[UserSession shareInstance] restoreSessionIfNeeded];
    [UserSession shareInstance].didLogin = NO;
    [[SettingManager shareInstance] restoreSessionIfNeeded];
    [[ThemeManager shareInstance] restoreSessionIfNeeded];
    if (IS_IPHONE) {
        [[LanguageManager defaultManager] setLanguage:[Utils languageByType:[UserSession shareInstance].languageType]];
    }
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    // This line is important to remove any cached data in webviews (specifically the charts)
    // Charts sometimes stopped working and keeps on increasing the webview memory till app crash
    // the data remains in the app cache folder and making the webview unable to load when the chart is working properly
    
    
    //
    //    [Parse setApplicationId:@"EBzQyk6eaKeYSWtgH2RBLGqUfClkTpEzsVlcC7up"
    //                  clientKey:@"tWXDg5V6CP1jLfcGYG8s1Do2Rbsyf4Df79tdOeln"];
    //    //[Parse enableLocalDatastore];
    //    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // Initialize Vertx Connection
    vcm = [VertxConnectionManager singleton];
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else
    {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    [FIRMessaging messaging].delegate = self;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")){
        UNUserNotificationCenter *notifiCenter = [UNUserNotificationCenter currentNotificationCenter];
        notifiCenter.delegate = self;
        [notifiCenter requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"inside dispatch async block main thread from main thread");
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }else{
        if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            // use registerUserNotificationSettings
            UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                            UIUserNotificationTypeBadge |
                                                            UIUserNotificationTypeSound);
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                     categories:nil];
            [application registerUserNotificationSettings:settings];
            [application registerForRemoteNotifications];
            
        } else {
            // use registerForRemoteNotificationTypes:
            [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
        }
    }
    
    //initial first
    [self initialFirstLaunch];
    return YES;
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    //save fcmToken...
    NSLog(@"content---%@", fcmToken);
    [UserPrefConstants singleton].deviceToken = fcmToken;
    
    [[StockAlertAPI instance] getAESKeyAPI];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    if ([self.delegateDelegate respondsToSelector:@selector(delegateWillResignActive)]) [self.delegateDelegate delegateWillResignActive];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    //    if (currentInstallation.badge != 0) {
    //        currentInstallation.badge = 0;
    //        [currentInstallation saveEventually];
    //    }
    
    if ([self.delegateDelegate respondsToSelector:@selector(delegateDidBecomeActive)]) [self.delegateDelegate delegateDidBecomeActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    
}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark - Notification delegates

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Do Your Code.................Enjoy!!!!
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"content---%@", token);
    [UserPrefConstants singleton].deviceToken = token;
    //    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    //    [currentInstallation setDeviceTokenFromData:deviceToken];
    //    currentInstallation.channels = @[ @"global" ];
    //    [currentInstallation saveInBackground];
    // [[API instance] getAESKeyAPI];
    
    [[StockAlertAPI instance] getAESKeyAPI];
    //NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    ////NSLog(@"idfa %d",[idfaString length]);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    //[PFPush handlePush:userInfo];
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    ////NSLog(@"Error %@", err);
    
}

#pragma mark - Main
- (void)initialFirstLaunch{
    if (IS_IPHONE) {
        SplashVC *_splashVC = [[SplashVC alloc] initWithNibName:@"SplashVC" bundle:nil];
        if (!self.window) {
            self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        }
        self.window.rootViewController = _splashVC;
        [self.window makeKeyAndVisible];
    }
    //Nothings to do: Show default ipad version
}

- (void)showCommonMenu{
    //Prepare menu
    TCMenuVC *_menuVC = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [TCMenuVC storyboardID]);
    _menuVC.menuType = TCMenuType_Common;
    LoginMobileVC *_loginVC = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, @"LoginMobileCustomVC");
    _mainRootNav = [[BaseNavigationController alloc] initWithRootViewController:_loginVC];
    if (self.container) {
        self.container = nil;
    }
    if (self.rootTabbarVC) {
        self.rootTabbarVC = nil;
    }
    _container = [[MyContainerVC alloc] initWithRootViewController:_mainRootNav leftViewController:nil rightViewController:nil];
    
    //Statusbar
    
    _container.rootViewStatusBarHidden = NO;
    _container.rootViewStatusBarStyle = [[ThemeManager shareInstance] rootViewStatusBarStyle];
    _container.rootViewStatusBarUpdateAnimation = UIStatusBarAnimationNone;
    
    //Configure
    _container.leftViewStatusBarHidden = YES;
    _container.rightViewStatusBarHidden = YES;
    
    _container.leftViewWidth = SCREEN_WIDTH_PORTRAIT - 60;
    self.window.rootViewController = self.container;
    [self.window makeKeyAndVisible];
    [UIView transitionWithView:self.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:^(BOOL finished) {
                    }];
    /*
     [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
     self.window.rootViewController.view.alpha = 0.6;
     } completion:^(BOOL finished) {
     //Show Main Menu here
     self.window.rootViewController = self.container;
     [self.window makeKeyAndVisible];
     }];
     */
    //self.window.rootViewController = _container;
    //[self.window makeKeyAndVisible];
}
- (void)setupControllers{
    //Create new root for new design - May/04/2018
    //1: DashBoard
    if (self.rootTabbarVC) {
        self.rootTabbarVC = nil;
    }
    _rootTabbarVC = [[BaseTabBarViewController alloc] init];
    _rootTabbarVC.delegate = self;
    BaseNavigationController *aHomeNav = [self setupHomeVC];
    
    //2Quote
    MQuoteViewController *_quoteVC = NEW_VC_FROM_STORYBOARD(kMQuoteStoryboardName, [MQuoteViewController storyboardID]);
    BaseNavigationController *aQuoteNav = [[BaseNavigationController alloc] initWithRootViewController:_quoteVC];
    _quoteVC.title = [LanguageManager stringForKey:Quote_Screen];
    _quoteVC.tabBarItem.title=[LanguageManager stringForKey:Quote];
    _quoteVC.tabBarItem.image = [UIImage imageNamed:@"tabbar_quote_icon"];
    //_quoteVC.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_quote_highlight_icon"];
    //OrderBook
    MOrderBookVCNew *orderVC = NEW_VC_FROM_NIB([MOrderBookVCNew class], @"MOrderBookVCNew");
    BaseNavigationController *orderNav = [[BaseNavigationController alloc] initWithRootViewController:orderVC];
    orderVC.title = [LanguageManager stringForKey:@"Order Book"];
    orderVC.tabBarItem.image = [UIImage imageNamed:@"tabbar_trade_icon"];
    //orderVC.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_indices_highlight_icon"];
    
    //Portfolio
    MPortfolioViewController *pfVC = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [MPortfolioViewController storyboardID]);
    BaseNavigationController *pfNav = [[BaseNavigationController alloc] initWithRootViewController:pfVC];
    pfVC.title = [LanguageManager stringForKey:@"Portfolio"];
    pfVC.tabBarItem.image = [UIImage imageNamed:@"tabbar_portfolio_icon"];
    //pfVC.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_indices_highlight_icon"];
    
    //Markets --- Not for now
    /*
     MarketsVC *_marketVC = NEW_VC_FROM_STORYBOARD(kMMarketStoryboardName, [MarketsVC storyboardID]);
     BaseNavigationController *aMarketNav = [[BaseNavigationController alloc] initWithRootViewController:_marketVC];
     _marketVC.title = [LanguageManager stringForKey:@"Indices"];
     _marketVC.tabBarItem.image = [UIImage imageNamed:@"tabbar_indices_icon"];
     _marketVC.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_indices_highlight_icon"];
     */
    NSArray *viewcontrollers = @[aHomeNav, aQuoteNav, orderNav, pfNav].copy;
    [_rootTabbarVC setViewControllers:viewcontrollers];
}
- (void)setHomeVC{
    BaseNavigationController *aHomeNav = [self setupHomeVC];
    NSMutableArray *vcs = [NSMutableArray arrayWithArray:_rootTabbarVC.viewControllers];
    if(vcs.count > 0){
        [vcs replaceObjectAtIndex:0 withObject:aHomeNav];
        [_rootTabbarVC setViewControllers:vcs];
    }
}
- (BaseNavigationController *)setupHomeVC{
    BaseVC *homeVC = [SettingManager getDefaultPage];
    homeVC.isHome = YES;
    homeVC.title = [LanguageManager stringForKey:Home];
    BaseNavigationController *aHomeNav = [[BaseNavigationController alloc] initWithRootViewController:homeVC];
    homeVC.tabBarItem.image = [UIImage imageNamed:@"tabbar_home_icon"]; //7_db can chinh sua cho nay
    //homeVC.tabBarItem.selectedImage = [UIImage imageNamed:@"tabbar_home_highlight_icon"]; //7_db can chinh sua cho nay
    return aHomeNav;
}
//========== Start Main App ==============================
- (void)startMainApp{
    [self setupControllers];
    UIViewController *rootContentVC = _rootTabbarVC;
    //Make default Indices VC
    TabbarItemView selectedItem = [Utils shouldSelectedTabbarItemFrom:[SettingManager shareInstance].defaultPage];
    if (selectedItem != TabbarItemView_Unknow) {
        //[_rootTabbarVC setSelectedIndex:selectedItem];
    }
    MMainMenuVC *_menuVC = NEW_VC_FROM_STORYBOARD(@"MMenu", @"MMainMenuSSIVC");
    filterVC = [[MOrderBookFilterVC alloc] initWithNibName:@"MOrderBookFilterVC" bundle:nil];
    if (self.container) {
        self.container = nil;
    }
    _container = [[MyContainerVC alloc] initWithRootViewController:rootContentVC leftViewController:_menuVC rightViewController:nil];
    
    //Statusbar
    _container.rootViewStatusBarHidden = NO;
    _container.rootViewStatusBarStyle = [[ThemeManager shareInstance] rootViewStatusBarStyle];
    _container.rootViewStatusBarUpdateAnimation = UIStatusBarAnimationNone;
    
    _container.rightViewStatusBarHidden = YES;
    _container.rightViewStatusBarUpdateAnimation = UIStatusBarAnimationFade;
    
    _container.leftViewStatusBarHidden = YES;
    _container.leftViewStatusBarUpdateAnimation = UIStatusBarAnimationFade;
    _container.rightViewSwipeGestureEnabled = NO;
    self.window.rootViewController = self.container;
    [self.window makeKeyAndVisible];
    //Animation with transition
    [UIView transitionWithView:self.window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
                    completion:^(BOOL finished) {
                    }];
    /*
     [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
     self.window.rootViewController.view.alpha = 0.6;
     } completion:^(BOOL finished) {
     //Show Main Menu here
     self.window.rootViewController = self.container;
     [self.window makeKeyAndVisible];
     }];
     */
}

- (void)logout{
    //Remove cache
    [[ImageCacheManager shareInstance] clearAllCache];
    [[DataManager shared] cleareDataLineChart];
    //2:Logout Session
    [UserSession shareInstance].didLogin = NO;
    [[UserSession shareInstance] save];
    [[NetworkManager shared] stopCheckingSigleSignOn];
    
    [[[VertxConnectionManager singleton]vertxTimer] invalidate];
    [[VertxConnectionManager singleton] unsubscribeAll];
    [[VertxConnectionManager singleton] stopVertxTimerKeepAlive];
    // +++ No need use this now, because have doKickOut now
    [[AuthenticationAPI shared] doLogout:^(id result, NSError *error) {
        DLog(@"++++ Did log out: %@ - Error: %@", result, error);
    }];
    
    [[UserPrefConstants singleton].userWatchListDict_FavId_Name removeAllObjects];
    [UserPrefConstants singleton].userWatchListDict_FavId_Name = nil;
    
    [[VertxConnectionManager singleton] close];
    [UserPrefConstants singleton].currentExchangeInfo = nil;
    [[UserPrefConstants singleton].userExchagelist removeAllObjects];
    [[UserPrefConstants singleton].vertxExchangeInfo removeAllObjects];
    //self.rootTabbarVC = nil;
    //self.container = nil;
    //1: Logout UI
    [self showCommonMenu];
}

- (void)doKickOut:(NSString *)message{
    //Show Alert
    [self logout];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self showAlertMessage:message];
    });
}

- (void)showAlertMessage:(NSString *)message{
    if (self.window.rootViewController) {
        [self.window.rootViewController showCustomAlertWithMessage:message];
    }
}
//================= CONTROL CONTAINER ====================
- (void)toogleLeftMenu:(void (^)(void))completion{
    [_container toggleLeftViewAnimated:YES completionHandler:completion];
}
- (void)toogleRightMenu:(void (^)(void))completion{
    [filterVC initData];
    [_container toggleRightViewAnimated:YES completionHandler:completion];
}

//+++ Handle for new design
- (void)openHomePage{
    id rootVC = _container.rootViewController;
    if ([rootVC isKindOfClass:[BaseTabBarViewController class]]) {
        [(BaseTabBarViewController *)rootVC setSelectedIndex:TabbarItemView_Home];
    }else{
        self->_container.rootViewController = self->_rootTabbarVC;
        [self->_rootTabbarVC setSelectedIndex:TabbarItemView_Home];
    }
}
- (void)openNewContentWithoutCheckVisible:(UIViewController *)newContentVC atType:(MenuItemType)type{
    id rootVC = _container.rootViewController;
    
    switch (type) {
        case Dashboard:{
            if([SettingManager shareInstance].defaultPage == DefaultPage_Dashboard){
                [self openHomePage];
                return;
            }
            break;
        }
        case Watchlist:{
            if([SettingManager shareInstance].defaultPage == DefaultPage_Watchlists){
                [self openHomePage];
                return;
            }
            break;
            
        }
        case News:{
            if([SettingManager shareInstance].defaultPage == DefaultPage_News){
                [self openHomePage];
                return;
            }
             break;
        }
        case Indices:{
            if([SettingManager shareInstance].defaultPage == DefaultPage_Markets){
                [self openHomePage];
                return;
            }
            break;
        }
        case QuoteScreen:{
            if ([rootVC isKindOfClass:[BaseTabBarViewController class]]) {
                [(BaseTabBarViewController *)rootVC setSelectedIndex:TabbarItemView_Quote];
            }else{
                self->_container.rootViewController = self->_rootTabbarVC;
                [self->_rootTabbarVC setSelectedIndex:TabbarItemView_Quote];
            }
        }
            return;
        case OrderBook:{
            if ([rootVC isKindOfClass:[BaseTabBarViewController class]]) {
                [(BaseTabBarViewController *)rootVC setSelectedIndex:TabbarItemView_OrderBook];
            }else{
                self->_container.rootViewController = self->_rootTabbarVC;
                [self->_rootTabbarVC setSelectedIndex:TabbarItemView_OrderBook];
            }
        }
            return;
            
        case Portfolio:{
            if ([rootVC isKindOfClass:[BaseTabBarViewController class]]) {
                [(BaseTabBarViewController *)rootVC setSelectedIndex:TabbarItemView_Portfolio];
            }else{
                self->_container.rootViewController = self->_rootTabbarVC;
                [self->_rootTabbarVC setSelectedIndex:TabbarItemView_Portfolio];
            }
        }
            return;
        default:{
        }
            break;
    }
    if ([self isRootViewSameClassFromClass:[newContentVC class]]) {
        //Do nothings now
    }else{
        BaseNavigationController *newRootVC = [[BaseNavigationController alloc] initWithRootViewController:newContentVC];
        self->_container.rootViewController = newRootVC;
    }
}
- (void)openNewContent:(UIViewController *)newContentVC atType:(MenuItemType)type{
    if (self.container.leftViewVisible) {
        [self.container hideLeftViewAnimated:YES completionHandler:^{
            [self openNewContentWithoutCheckVisible:newContentVC atType:type];
        }];
    }else{
        [self openNewContentWithoutCheckVisible:newContentVC atType:type];
    }
}

//Show Login to continue app
- (void)enableFilterVC:(BOOL)isEnable{
    _container.rightViewSwipeGestureEnabled = isEnable;
}
- (void)showContinueLogin{
    if (_container) {
        //ContinueLoginVC *_continueVC = NEW_VC_FROM_STORYBOARD(kCommonMobileStoryboardName, [ContinueLoginVC storyboardID]);
        //[_container presentViewController:_continueVC animated:YES completion:nil];
        //+++ Use new
        [_container showCustomAlert:TC_Pro_Mobile message:[LanguageManager stringForKey:@"Your session is time out. Please re-login to use app."] withOKAction:^{
            [self logout];
        }];
    }
}

#pragma mark - UTILS
- (BOOL)isRootViewSameClassFromClass:(Class)class{
    id rootVC = _container.rootViewController;
    if ([rootVC isKindOfClass:[BaseNavigationController class]]) {
        return [[[(BaseNavigationController *)rootVC viewControllers] firstObject] isKindOfClass:class];
    }else{
        return [rootVC isKindOfClass:class];
    }
}

- (UIViewController *)middleControllerFrom:(DefaultPage)dfPage{
    UIViewController *controller;
    switch (dfPage) {
        case DefaultPage_Portfolios:
            controller = NEW_VC_FROM_STORYBOARD(kMPortfolioStoryboardName, [MPortfolioViewController storyboardID]);
            break;
        case DefaultPage_Watchlists:
            controller = NEW_VC_FROM_STORYBOARD(kMWatchlistStoryboardName, [MWatchlistVC storyboardID]);
            break;
        default:
            //controller = NEW_VC_FROM_NIB([EmptyTradeVC class], [EmptyTradeVC nibName]);
            break;
    }
    return controller;
}

#pragma mark - UITabBarControllerDelegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    /*
     NSInteger index = [tabBarController.viewControllers indexOfObject:viewController];
     if (index == TabbarItemView_Trade) {
     return NO;
     }
     */
    return YES;
}

#pragma mark - Orientation

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (IS_IPHONE) {
        if(self.restrictRotation == NO){
            return UIInterfaceOrientationMaskAll;
        }
        else if ([self.window.rootViewController.presentedViewController isKindOfClass:[ChartVC class]])
        {
            ChartVC *_chartVC = (ChartVC *) self.window.rootViewController.presentedViewController;
            if (_chartVC.isPresented){
                return UIInterfaceOrientationMaskAll;//UIInterfaceOrientationMaskLandscape;
            }
            else return UIInterfaceOrientationMaskPortrait;
        }
        else {
            return UIInterfaceOrientationMaskPortrait;
        }
    }
    return UIInterfaceOrientationMaskLandscape;
}


#pragma mark - FIRMessagingDelegate
- (void)applicationReceivedRemoteMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage{
    DLog(@"===== Firebase Remote Message: %@", remoteMessage.description);
}

- (void)messaging:(nonnull FIRMessaging *)messaging didReceiveMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage{
    DLog(@"===== Firebase message: %@", messaging.description);
}
@end
